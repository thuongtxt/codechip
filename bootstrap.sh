#!/bin/bash
#This file will automate the process to create 'configure' and 'Makefile.in' files

sdkDir=`pwd`

libtoolize
aclocal
automake --add-missing

cd $sdkDir/cli/libs/klish/
libtoolize
aclocal
automake --add-missing

cd $sdkDir/cli/libs/tcl-8.5/
libtoolize
aclocal
automake --add-missing

cd $sdkDir

find . \( -iname aclocal* -o -iname autom4te* -o -iname aclocal.m4 -o -iname configure -o -iname Makefile.in -o -iname libtool -o -iname config.status -o -iname config.h.in \) -print | xargs rm -fR

autoreconf --force --install

#automake --add-missing --copy
 

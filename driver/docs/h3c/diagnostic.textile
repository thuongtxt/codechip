h1. Test clock

Check if clock is ready: expect 0xFFF00 == 0xaf61

h1. Check FPGA information

*Check FPGA build time:*
Year : expect 0xf00000[31:24] == 0x12
Month: expect 0xf00000[23:16] == 0x05
Day  : expect 0xf00000[15:8]  == 0x02
Hour : expect 0xf00000[7:0]   == 0x00

*Check product code:* expect 0xf00001[31:0] == 0x60010020

h1. Diagnostic DDR3

h2. DDR normal operation

The register 0xf20001 will always be 0x4 to indicate the DDR has been initialized properly. During error forcing period, this register still hold this value. If it is different from 0x4, DDR die.

* Clear sticky: DiagHalWrite(0xf20001, 0xf)
* Expect: 0xf20001[3:0] == 0x4
* Timeout waiting for 0xf20050[31:0] == 0xffffffff
* Wait for a moment (configurable), then expect 0xf20051[31:0] == 0
* Expect: 0xf20001[3:0] == 0x4

h2. Change DDR mode

* Change DDR mode: 0xf20010[31:0] = [0x8033338C, 0x8303338C, 0x8330338C, 0x8330348C, 0x8330358C, 0x8330368C]
* For each mode, wait for a moment then expect:
** 0xf20051[31:0] == 0
** 0xf20001[3:0] == 0x4

h2. Force error 

* Force error: DiagHalWrite(0xf20000, 0x3)
* Expect: 
** 0xf20051[31:0] > 0
** 0xf20001[3:0] == 0x6
* Unforce error: DiagHalWrite(0xf20000, 0x1). Refer "DDR normal operation" for expecting sequence

h1. Diagnostic SGMII

* Clear LED status: DiagHalWrite(0xf00040, 0xf), DiagHalWrite(0xf00040, 0x0)
* Clear stictky: DiagHalWrite(0xf00050, 0x1)
* Expect normal operation: 0xf00050[1:0] == 2
* Force error: DiagHalWrite(0xf00042, 0xC)
* Expect: 0xf00050[1:0] == 3
* Unforce error: DiagHalWrite(0xf00042, 0x0)
* Clear LED status: DiagHalWrite(0xf00040, 0xf), DiagHalWrite(0xf00040, 0x0)
* Clear sticky: DiagHalWrite(0xf00050, 0x1)
* Expect normal operation: 0xf00050[1:0] == 2 

h1. Diagnostic ZBT

TBD 

h1. Diagnostic LIUs

* Expect no LOS for all ports: 0xf40006[15:0] == 0
* Expect no loopout: 0xf40001[15:0] == 0 
* Enable PRBS: DiagHalWrite(0xf40002, 0xffff) then check if it is applied by reading it.
* Clear PRBS sticky: DiagHalWrite(0xf40005, 0xffff)
* Wait a moment for traffic running
* Expect PRBS Sync: 0xf40005[15:0] == 0
* Force error: DiagHalWrite(0xf40004, 0xffff)
* Expect PRBS Not Sync: 0xf40005[15:0] == 0xffff
* Unforce error: DiagHalWrite(0xf40004, 0x0)
* Clear PRBS sticky: DiagHalWrite(0xf40005, 0xffff)
* Expect PRBS Sync: 0xf40005[15:0] == 0

* Support a function to enable/disable loopout:
** Loopout: DiagHalWrite(0xf40001, 0xffff) 
** No loopout: DiagHalWrite(0xf40001, 0x0)

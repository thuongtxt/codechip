A) Register description
1) Control #1
- address: fff02
- indrctrl_reg[15:0]
2) Control #2
- address: fff03
- indrctrl_reg[31:16]

3) Data #1
- address: fff04
- indrdata_reg[15:0]
4) Data #2
- address: fff05
- indrdata_reg[31:16]

wire            oeupce_     = ~indrctrl_reg[31];//CPU set to 1 to request read/write; HW will set to 0 when read/write done. CPU need poll this register to know Read/Write Process done
wire            oeuprnw     = indrctrl_reg[30]; //CPU set to 1 for Read and set to 0 for write
wire [23:0]     oeupa       = indrctrl_reg[23:0]; //CPU set to Access Address
wire [31:0]     oeupdi      = indrdata_reg[31:0];//CPU set to Write Data for Write Process and get to Read Data for Read Process

B) Read cycle
1) write to control register, 
- write to control#1 first: bit[15:0] = addr[15:0]
- write to control#2 second: bit[15] = 1, bit[14] = 1, bit[7:0] = addr[23:16]
2) Read control #2 register, if bit[15] is clear to 0, read done
3) Read data register to get data
- read data#1 first: bit[15:0] = data[15:0]
- read data#2 second: bit[15:0] = data[31:16]

C) Write cycle
1) Write to data register
- write to data#1 first: bit[15:0] = data[15:0]
- write to data#2 second: bit[15:0] = data[31:16]
2) write to control register, write to control#1 first.
- write to control#1 first: bit[15:0] = addr[15:0]
- write to control#2 second: bit[15] = 1, bit[14] = 0, bit[7:0] = addr[23:16]
3) Read control #2 register, if bit[15] is clear to 0, write done

h1. Overview

This product supports:

* 1024 PWs
* 8 STM-1s
* 2 GEs

Each PW can be bound to any SMT port and GE port with constrain:
* STM port 1-4 must go with GE#1
* STM port 5-8 must go with GE#2

So, assume we have a PW#1, its circuit is channel of port 1-4, then it can only bound to GE#1. And vice versa, if PW is already bound to GE#1, then its circuit must be channel of port 1-4.

h1. Problem and solving

h2. Creating

h3.  When a PW is created, what should we do?

When PW has just been created, it cannot work without circuit and GE port. So, just keep this object in database, do not assign it to any HW PW.

h3.  After a PW is created, it has no circuit and ETH port. Then application configures it, what should we do?

No hardware entity, just keep configuration temporary in RAM.

h2. Binding

h3. After PW is created, it has no circuit and GE port, what should we do when it binds to a circuit?

PW still cannot work yet, just keep this binding information in database. Do not allocate any hardware PW

h3. After PW is create, it has no circuit and GE port, what should we do when it binds to a GE port.

PW still cannot work yet, just keep this binding information in database. Do not allocate any hardware PW

h3. Assume PW has already bound to a circuit, then it binds to GE port, what should we do?

Need to check if PW can work with this circuit and GE port. If it can work:
* Allocate one hardware PW that can work with this circuit and GE. Assume that we have one,
* Apply application configuration. If application has not configured any thing, just use default configuration.

h3. Assume PW has already bound to a GE, then it binds to circuit, what should we do?

h2. Unbinding

# Assume PW only has circuit, no GE. What to do to unbind circuit?
# Assume PW only has GE, no circuit. What to do to unbind a GE
# Assume PW has circuit and GE port, that means it is running. What to do to unbind a circuit?
# Assume PW has circuit and GE port, that means it is running. What to do to unbind a GE?

h2. Deleting

With current SDK, a PW can be deleted when it has not been bound to any circuit. So, the following problems are only for the case that circuit does not exist.

# Assume PW has just been created with no circuit and GE, what to do to delete it?
# Assume PW only binds to GE, what to do to delete PW

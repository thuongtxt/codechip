/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : TODO Module Name
 *
 * File        : cla_hash_pseudo.c
 *
 * Created Date: Mar 5, 2013
 *
 * Description : TODO Descriptions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HashIndexGet(AtPw pw, const uint32 *hwLabel)
    {
    uint32 longReg[cLongRegSize];
    uint16  codeLevel;
    uint32 hbcePatern59_50;
    uint32 hbcePatern49_40;
    uint32 hbcePatern39_30;
    uint32 swHbcePatern29_20;
    uint32 swHbcePatern19_10;
    uint32 swHbcePatern9_0;
    uint32 msb, lsb;

    LongRead(pw, cThaRegCLAHBCEGlbCtrl, longReg);
    mFieldGet(longReg[cThaClaHbceCodingSelectedModeDwIndex],
              cThaClaHbceCodingSelectedModeMask,
              cThaClaHbceCodingSelectedModeShift,
              uint16,
              &codeLevel);

    mFieldGet(longReg[cThaHbcePatern59_50DwIndex],
              cThaHbcePatern59_50Mask,
              cThaHbcePatern59_50Shift,
              uint32,
              &hbcePatern59_50);

    mFieldGet(longReg[cThaHbcePatern49_40DwIndex],
              cThaHbcePatern49_40Mask,
              cThaHbcePatern49_40Shift,
              uint32,
              &hbcePatern49_40);

    mFieldGet(longReg[cThaHbcePatern39_30DwIndex + 1],
              cThaHbcePatern39_30HwHeadMask,
              cThaHbcePatern39_30HwHeadShift,
              uint32,
              &msb);

    mFieldGet(longReg[cThaHbcePatern39_30DwIndex],
              cThaHbcePatern39_30HwTailMask,
              cThaHbcePatern39_30HwTailShift,
              uint32,
              &lsb);

    mFieldIns(&hbcePatern39_30,
              cThaHbcePatern39_30SwHeadMask,
              cThaHbcePatern39_30SwHeadShift,
              msb);

    mFieldIns(&hbcePatern39_30,
              cThaHbcePatern39_30SwTailMask,
              cThaHbcePatern39_30SwTailShift,
              lsb);

    mFieldGet(hwLabel[cThaHbcePatern29_20DwIndex],
              cThaHbcePatern29_20Mask,
              cThaHbcePatern29_20Shift,
              uint32,
              &swHbcePatern29_20);

    mFieldGet(hwLabel[cThaHbcePatern19_10DwIndex],
              cThaHbcePatern19_10Mask,
              cThaHbcePatern19_10Shift,
              uint32,
              &swHbcePatern19_10);

    mFieldGet(hwLabel[cThaHbcePatern9_0DwIndex],
              cThaHbcePatern9_0Mask,
              cThaHbcePatern9_0Shift,
              uint32,
              &swHbcePatern9_0);

    /* Calculate hash index */
    if (codeLevel == 0)
        return swHbcePatern9_0;
    if (codeLevel == 1)
        return swHbcePatern19_10 ^ swHbcePatern9_0;
    if (codeLevel == 2)
        return swHbcePatern29_20 ^ swHbcePatern19_10 ^ swHbcePatern9_0;
    if (codeLevel == 3)
        return hbcePatern39_30 ^ swHbcePatern29_20 ^ swHbcePatern19_10 ^ swHbcePatern9_0;
    if (codeLevel == 4)
        return hbcePatern49_40 ^ hbcePatern39_30 ^ swHbcePatern29_20 ^ swHbcePatern19_10 ^ swHbcePatern9_0;
    if (codeLevel == 5)
        return hbcePatern59_50 ^ hbcePatern49_40 ^ hbcePatern49_40 ^ swHbcePatern29_20 ^ swHbcePatern19_10 ^ swHbcePatern9_0;

    return 0x0;
    }

static eAtRet RemainBitsGet(AtPw pw, const uint32 *hwLabel, uint32 *remainBits)
    {
    uint32 msb, lsb;

    mFieldGet(hwLabel[1],
              cThaHbceMemoryPool1HwTailMask,
              cThaHbceMemoryPool1HwTailShift,
              uint32,
              &lsb);
    mFieldIns(&remainBits[1],
              cThaHbceMemoryPool1SwTailMask,
              cThaHbceMemoryPool1SwTailShift,
              lsb);

    mFieldGet(hwLabel[1],
              cThaHbceMemoryPool0HwHeadMask,
              cThaHbceMemoryPool0HwHeadShift,
              uint32,
              &msb);
    mFieldIns(&remainBits[0],
              cThaHbceMemoryPool0SwHeadMask,
              cThaHbceMemoryPool0SwHeadShift,
              msb);

    mFieldGet(hwLabel[0],
              cThaHbceMemoryPool0HwTailMask,
              cThaHbceMemoryPool0HwTailShift,
              uint32,
              &lsb);
    mFieldIns(&remainBits[0],
              cThaHbceMemoryPool0SwTailMask,
              cThaHbceMemoryPool0SwTailShift,
              lsb);
    }

static void HwHcbeLabel(AtPw pw, uint32 *hwLabel)
    {
    uint32 swHbceLabel;
    uint32 msb, lsb;

    swHbceLabel = HbceLabel(pDevHandle, pwId, pPwPsnCfg);
    mFieldIns(&hwLabel[1], cThaHbceLblId1HwTailMask, cThaHbceLblId1HwTailShift, 0);

    mFieldGet(swHbceLabel, cThaHbceLblId0SwHeadMask, cThaHbceLblId0SwHeadShift, dword, &msb);
    mFieldIns(&hwLabel[1], cThaHbceLblId0HwHeadMask, cThaHbceLblId0HwHeadShift, msb);

    mFieldGet(swHbceLabel, cThaHbceLblId0SwTailMask, cThaHbceLblId0SwTailShift, dword, &lsb);
    mFieldIns(&hwLabel[0], cThaHbceLblId0HwTailMask, cThaHbceLblId0HwTailShift, lsb);

    mFieldIns(&hwLabel[0], cThaHbcePktTypeMask, cThaHbcePktTypeShift, HbcePktType(pPwPsnCfg->psnType));
    mFieldIns(&hwLabel[0], cThaHbceGePortMask, cThaHbceGePortShift, gePort);
    }

static uint32 PwHash(AtPw pw, const uint32 *hwLabel, uint32 *pRemainBits)
    {
    uint32 hashIndex = HashIndexGet(pw, hwLabel);
    RemainBitsGet(pw, hwLabel, pRemainBits);

    return hashIndex;
    }

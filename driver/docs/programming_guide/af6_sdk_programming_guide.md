
## Document History

|Revision    |Date        |Description                                   |
|------------------------------------------------------------------------|
|1.0 Alpha   |Aug 15, 2012|Initial version                               |
|1.0.2 Alpha |Sep 22, 2012|Add [Pseudowire programming guide] [pseudowire]<br>Add [Interrupt processing] [interrupt]<br>Update class diagrams|
|1.0.2 Alpha |Sep 22, 2012|Add [Pseudowire programming guide] [pseudowire]<br>Add [Interrupt processing] [interrupt]<br>Update class diagrams|
|1.0.3 Alpha |Oct 08, 2012|Add [ATM/IMA programming guide] [atm_ima]<br>Deprecate `AtModulePwCreatePw` by `AtModulePwSAToPCreate`, `AtModulePwCESoPCreate`, `AtModulePwAtmCreate`, `AtModulePwHdlcPppCreate`<br>Deprecate `AtHdlcChannelScrambleEnable/AtHdlcChannelScrambleIsEnabled` by `AtEncapChannelScrambleEnable/AtEncapChannelScrambleIsEnabled`<br>Udpate [SDH programming guide] [sdh] for clock scheme |
|1.0.4 Alpha |Oct 27, 2012|Update class diagrams for iterator pattern and missing methods<br>Add [How to compile and start the SDK] [startSdk]|
|1.0.5 Alpha |Nov 07, 2012|Update [How to compile and start the SDK] [startSdk], use sampleapp instead of epapp|
|1.0.10 Alpha |Dec 28, 2012|Add [Common resource management] [resouceManagement]<br>Update [PDH programming guide] [pdh] to mention about NxDS0 management<br>Update [Ethernet programming guide] [ethernet] to mention about flow management<br>Update [Encapsulation programming guide] [encap] for OAM processing, address/control/FCS field handling<br>Add [Iterator] [iterator] section to explain how to iterate all managed objects |
|1.0.12       |Feb 22, 2013|Add [RAM management] [ram] and its class diagram<br>AtObject: add new method `AtObjectClone` to clone an object<br>AtChannel: add new method `AtChannelBoundEncapChannelGet` to get the bound encapsulation channel<br>AtHdlcLink: Add new method `AtHdlcLinkOamFlowGet` to bypass OAM to ETH side, Remove `AtHdlcLinkISISMacSet`/`AtHdlcLinkISISMacGet`<br>AtModuleEncap: Remove `AtModuleEncapHdlcISISMacSet`/`AtModuleEncapHdlcISISMacGet`<br>AtModuleEth: Add `AtModuleEthISISMacSet`/`AtModuleEthISISMacGet`<br>AtDevice: add new method `AtDeviceVersion`, add new method `AtDeviceMemoryTest`<br>AtDriver: add new method `AtDriverVersion`<br>AtModule: add new method `AtModuleMemoryTest`<br>AtModulePdh: Add new APIs `AtModulePdhOutputClockSourceSet`/`AtModulePdhOutputClockSourceGet` to control output clock<br>AtSdhChannel: Add new attribute `timMonitorEnabled` to enable / disable TIM monitoring |
|1.0.13       |Mar 06, 2013|Add [BER monitoring] [ber]|
|2.0          |Mar 18, 2013|Update PW class diagram for CEP|
|2.1          |Jun 06, 2013|Add [Product notes] [productNote] section<br>Update [How to integrate and start CLI library on application] [cli] to include TextUI class diagram and how to control Text-UI<br>Update [sample app][sampleapp] for more detail<br>Add [Packet analyzer] [packetAnalyzer] section<br>Update [RAM management] [ram] section<br>Correct typo<br>Add more sample CLIs for PW|
|2.2.14       |            |Update class diagrams<br>Add [How to use this document] [how_to_use] section|
|2.3          |            |Update OSAL class diagram<br>Update HAL implementation sample code<br>Update Driver initialization sample code<br>Update PW programming guide, add these new sections: <br>+ [Configuring Packet Switch Network (PSN)][Configuring Packet Switch Network (PSN)]<br>+  [Configuring Ethernet layer][Configuring Ethernet layer]<br>+ [Controling payload and jitter buffer][Controling payload and jitter buffer]<br>+ [Configure ACR/DCR timing][Configure ACR/DCR timing]<br>Update [CLI section][cli] for latest class diagrams, sample code, sample CLIs.|


## Table of Contents

* [Overview] [overview]
* [How to use this document] [how_to_use]
* [Class diagrams] [class_diagrams]
* [How traffics flow] [trafficFlow]
* [Initialize Device Driver and access device modules] [initialize]
* [PDH programming guide] [pdh]
* [Ethernet programming guide] [ethernet]
* [Encapsulation programming guide] [encap]
* [SDH programming guide] [sdh]
* [Pseudowire programming guide] [pseudowire]
* [ATM/IMA programming guide] [atm_ima]
* [RAM management] [ram]
* [Packet analyzer] [packetAnalyzer]
* [Iterator] [iterator]
* [Interrupt processing] [interrupt]
* [How to compile and start the SDK] [startSdk]
* [Product notes] [productNote]

## Overview [overview]

This document shows how Telecom objects are supported by AT Device and how to 
control them to create any specific application.

Although this document mentions about object, class and class diagram and using Object Oriented concept, this SDK is implemented by C, not C++. So all of function calls are C call, mapping between class diagrams and C call is mentioned at following sections. 

## How to use this document [how_to_use]

This document discuss how to control all of features that Arrive products can support. For one product, it is not necessary to read all of sections because not all of features can be supported for one product instance. See the following to refer what sections need to be read for each kind of products.

Common sections, the following sections should be read for all of products

* [Initialize Device Driver and access device modules] [initialize]
* [Common resource management] [resouceManagement]
* [PDH programming guide] [pdh]
* [Ethernet programming guide] [ethernet]
* [RAM management] [ram]
* [Packet analyzer] [packetAnalyzer]
* [Iterator] [iterator]
* [Interrupt processing] [interrupt]
* [How to compile and start the SDK] [startSdk]

Pseudowire products

* [PW traffic flow] [trafficFlow_pw]
* [Pseudowire programming guide] [pseudowire] 

PPP/MLPPP products

* [HDLC traffic flow] [trafficFlow_hdlc]
* [Encapsulation programming guide] [encap]
* [SDH programming guide] [sdh]

ATM/IMA product

* [ATM/IMA programming guide] [atm_ima]

## Class diagrams [class_diagrams]

### Class diagram legends

![Class diagram legends](images/oo_pattern_legends.svg)

**How to map attributes and methods to C calls**

* Set attribute: `ClassNameAttributeSet(classInstance, attributeValue)`
* Get attribute: `ClassNameAttributeGet(classInstance)`
* Method calling: `ClassNameMethod(classInstance, parameters, ...)`

For example, assume that there is one instance of `AtSdhLine` class which is SONET/SDH Line object. This object has `rate` attribute and method `eBool RateIsSupported(rate)` to check if a rate can be applied. The following code will change line rate.

     extern AtSdhLine line; /* Assume that this line exists */
     
     /* Check if new rate can be supported by this line */
     uint8 lineRate = cAtSdhLineRateStm4;
     if (AtSdhLineRateIsSupported(line, cAtSdhLineRateStm4))
        AtSdhLineRateSet(line, lineRate);
        
     /* Get line rate */
     printf('Line rate %d\n', AtSdhLineRateGet(line));

### Top class diagram

![Top class diagram](images/class_diagram_top.svg)

### AtDriver class

The `AtDriver` class manages a set of devices with different types. It is created by `AtDriverCreate(uint8 maxNumberOfDevices, AtOsal osal)` method. An OSAL object is asked to help Driver access OS service for memory operations and semaphore/mutex to protect itself from multi-threading, without OSAL, Driver does not know how to allocate those resources and it cannot be created

Only one Driver can be created, it can be retrieved by `AtDriver AtDriverSharedDriverGet()`. And device can be added by `AtDevice AtDriverDeviceCreate(AtDriver self, uint32 productCode)`. The product code is input to create correct device instance.

All of created devices can be accessed by using `AtDriverAddedDevicesGet()`.

### AtDevice class [AtDevice]

This class is to abstract all kinds of AT Device Device. Corresponding device instance is created via Driver interface `AtDevice AtDriverDeviceCreate(AtDriver self, uint32 productCode)`

For one AT Device Device, it supports a set of modules:

* PDH
* SONET/SDH
* Ethernet
* Encapsulation
* PPP/MLPPP
* FR
* RAM
* ... Refer `eAtModule` for all of module IDs that one AT Device device may support

>**NOTE**: Not all of device can support all of modules, the method
`eAtModule* AtDeviceAllSupportedModulesGet(uint8 *numModules)` will return IDs of all supported modules.

Device's modules can be accessed via `AtModule AtDeviceModuleGet(AtDevice self, eAtModule moduleId)`. If a module is not supported, a NULL object is returned.

On deployment, a AT Device device may be separated into IP cores. Each core can be an ASIC or a FPGA depending on target platform. These APIs are to access all of IP Cores:

* `uint8 AtDeviceNumIpCoresGet(AtDevice self)`
* `AtIpCore AtDeviceIpCoreGet(AtDevice self, uint8 coreId)`

One core can have different way to access its local registers and Hardware Abstraction Layer must be installed for each IP Core. Device interface also supports methods to fast install HALs to each IP Core:

* Set HAL for IP Core: `eAtRet AtDeviceIpCoreHalSet(AtDevice self, uint8 coreId, AtHal hal)`
* Get HAL of IP Core: `AtHal AtDeviceIpCoreHalGet(AtDevice self, uint8 coreId)`

Of course, IP Core's HAL can be accessed via IP Core interfaces. 

### AtIpCore

As mention in [AtDevice class] [AtDevice], a device is formed by IP Cores which can be FPGA or ASIC. And each IP Core may have appropriate way to access its registers and these actions closely depend on target hardware platform. This is why Hardware Abstract Layer needs to be installed to this IP Core to break this dependency.

That means application must implement HAL interfaces and install to IP Core by `eAtRet AtIpCoreHalSet(AtHal hal)`

### AtModule class

This is the abstract class of all of modules in device. After a device is created, all of modules will be initialized. By the way, it can be initialized by application via `AtModuleInit(AtModule module)` method.

Refer appropriate sections below to know how to control each module

### AtChannel class

This class is to abstract all of channel types in AT Device. A channel can be SONET/SDH Line, AUG, AU, VC, ..... Or PDH DS1/E1, DS3, E3 and so on.

A set of common methods are defined to access:

* Timing mode
* Alarm
* Interrupt mask
* Loop-back
* Performance counters
* ...

>**NOTE**: it is not required that all of concrete classes of `AtChannel` must implement all of interfaces. And for none-supported interfaces, concrete classes must implement and do nothing or return invalid value. 

### OS Abstraction Layer (OSAL)

![OSAL class diagram](images/class_diagram_osal.svg)

Because the Device Driver need OS resources to perform its jobs. Memory operations, semaphore, mutex and delay are basic functions that it requires. Those operations are different between OS (Linux, VxWorks, ...) so this abstract layer is to break OS dependency.

Device Driver package already build in OS implementation for Linux and VxWorks and these APIs below are to access them.

* Linux OSAL: `AtOsal AtOsalLinux()`
* VxWorks OSAL: `AtOsal AtOsalVxWorks()`

Of course, application can have different implementation of OSAL and it must be installed to Driver when Driver is created.

### Hardware Abstraction Layer (HAL)

This layer is to abstract the way Device Driver access to registers of Device's  IP Cores. The following source code is recommended when a new concrete AtHal is implemented.
    
    /*--------------------------- Include files ----------------------------------*/
    #include "AtOsal.h"
    #include "AtHal.h"

    /*--------------------------- Define -----------------------------------------*/

    /*--------------------------- Macros -----------------------------------------*/
    #define mThis(self) ((tAtHalXxx *)self)

    /*--------------------------- Local typedefs ---------------------------------*/
    typedef struct tAtHalXxx
        {
        tAtHal super;

        /* Private data. This sample implementation use baseAddress as private data. */
        uint32 baseAddress;
        }tAtHalXxx;

    /*--------------------------- Global variables -------------------------------*/

    /*--------------------------- Local variables --------------------------------*/
    static uint8 m_methodsInit = 0;

    /* Override */
    static tAtHalMethods m_AtHalOverride;

    /* Save super implementation */
    static const tAtHalMethods *m_AtHalMethods = NULL;

    /*--------------------------- Forward declarations ---------------------------*/

    /*--------------------------- Implementation ---------------------------------*/
    static void Write(AtHal self, uint32 address, uint32 value)
        {
        uint32 baseAddress = mThis(self)->baseAddress;

        /* TODO: code to write a register. */
        }

    static uint32 Read(AtHal self, uint32 address)
        {
        uint32 baseAddress = mThis(self)->baseAddress;

        /* TODO: code to read a register */
        return 0x0;
        }

    static void OverrideAtHal(AtHal self)
        {
        if (!m_methodsInit)
            {
            /* Save super implementation */
            m_AtHalMethods = self->methods;
            AtOsalMemCpy(&m_AtHalOverride, (void *)m_AtHalMethods, sizeof(tAtHalMethods));

            m_AtHalOverride.Write = Write;
            m_AtHalOverride.Read  = Read;
            }

        self->methods = &m_AtHalOverride;
        }

    static void Override(AtHal self)
        {
        OverrideAtHal(self);
        }

    static uint32 ObjectSize(void)
        {
        return sizeof(tAtHalXxx);
        }

    AtHal AtHalXxxObjectInit(AtHal self, uint32 baseAddress)
        {
        AtOsalMemInit(self, 0, ObjectSize());

        /* Super construction */
        if (AtHalObjectInit(self) == NULL)
            return NULL;

        /* Override */
        Override(self);
        m_methodsInit = 1;

        /* Save base address */
        mThis(self)->baseAddress = baseAddress;

        return self;
        }

    AtHal AtHalXxxNew(uint32 baseAddress)
        {
        /* Allocate memory */
        AtHal newHal = AtOsalMemAlloc(ObjectSize());

        /* Construct it */
        return AtHalXxxObjectInit(newHal, baseAddress);
        }

The above implementation use base address as HAL attribute. For systems that have more than one cards, each card has an `AtDevice` and a base address. 

For each product that needs be integrated to target platform, there will be a phase to bring up hardware and SDK will provide one concrete HAL class to work on this.

## How traffics flow [trafficFlow]

### HDLC related technologies [trafficFlow_hdlc]

![How traffic flow](images/data_flow.svg)

![Flow construction](images/flow_construction.svg)

VLAN traffics from Ethernet side come to Encapsulation module via flows: 

* For MLPPP bundle, if bundle works without Multi-Class Extension (RFC-1990), only one flow can be carried. If it works with Multi-Class Extension, more than one flows can be carried, and these flows are identified by class number in bundle.
* For MFR bundle, more than one flows be carried and these flows are identified by DLCI value.
* PPP/HDLC link and Cisco HDLC link can carry only one traffic flow
* FR link can carry more than one traffic flows and these flows are identified by DLCI value
* Flow can have more than one VLANs on direction from Ethernet to Encapsulation module.
* On direction from Encapsulation to Ethernet, VLAN tag is inserted to traffic flow

As depicted in the above figure, HDLC channel use DS1/E1 as physical interface. And DS1/E1 can come from SDH VC-1x or LIU.

#### Traffic flow to HDLC link (PPP or Cisco)

![Traffic flow to HDLC Link](images/data_flow_to_hdlc_link.svg)

* From Ethernet to Encapsulation direction, VLANs are encapsulated to HDLC link, the encapsulation can be PPP or Cisco HDLC. These frames are then encapsulated into HDLC frames before coming to DS1/E1.
* From Encapsulation to Ethernet direction, VLAN will be inserted to Ethernet traffic stream 

#### Traffic flow to MLPPP bundle

![Traffic flow to MLPPP bundle](images/data_flow_to_mlppp_bundle.svg)

* If bundle works without Multi-Class Extension, it can carry only one traffic flow.
* If bundle works with Multi-Class Extension, it can carry more than one traffic flows identified by class number.
* All of MLPPP frames will be distributed to PPP links that added to it.

#### Traffic flow to FR link

![Traffic flow to FR link](images/data_flow_to_fr_link.svg)

More than one traffic flows can be carried by FR link and each traffic flow will be assigned one DLCI value.

#### Traffic flow to MFR bundle

![Traffic flow to MFR bundle](images/data_flow_to_fr_bundle.svg)

Like FR Link, more than one traffic flows can be carried by MFR bundle and each traffic flow will be assigned one DLCI value.

### Pseudowire  [trafficFlow_pw]

![Pseudowire traffic flow](images/pseudowire.svg)

AT products support these kinds of Pseudowires:

* CEP: Synchronous Optical Network/Synchronous Digital Hierarchy (SONET/SDH) Circuit Emulation over Packet
* SAToP: Structure-Agnostic Time Division Multiplexing (TDM) over Packet
* CESoPSN: Structure-Aware Time Division Multiplexed (TDM) Circuit Emulation Service over Packet Switched Network

A circuit must be attached to PW in order to be emulated. Circuit binding is as following
 
* SDH VC (VC11/VC12, VC-3, VC-4, ...) can only be bound to CEP Pseudowire
* PDH DS1/E1 can only be bound to SAToP Pseudowire
* NxDS0 group can only bound to CESoP Pseudowire

On direction from TDM to Ethernet, PSN header is inserted before coming to Ethernet module. At Ethernet module, Ethernet header (DMAC, SMAC, VLANs, ETHTYPE) is inserted and PW packets are then transmitted on physical layer.

On direction from Ethernet to TDM, Ethernet module strips all of Ethernet header of incoming frames to have PSN packets. PSN header is extracted and then examined to look up what Pseudowire it should belong to and Pseudowire payload is then replayed on circuit.

PSN layer can be MPLS, MEF, UDP/IPv4, UDP/IPv6. Refer [Pseudowire programming guide] [pseudowire] to see how Pseudowire module is handled.

## Initialize Device Driver and access device modules [initialize]

Application can follow these steps to initialize Device Driver and create its supported devices

1. Create Driver
1. Create Devices
1. For each Device, application must now how the device is deployed to install HAL for all of its IP Cores 

The sample code below will create Driver and add one AT Device device. This device may be deployed into more than one FPGAs, it means there are more than one IP Cores. Focus to two functions `AtDriverInit` and `AtDriverCleanup`
    
    /*--------------------------- Include files ----------------------------------*/
    #include "atclib.h"
    #include "AtDriver.h"
    #include "AtList.h"

    /*--------------------------- Define -----------------------------------------*/

    /*--------------------------- Macros -----------------------------------------*/

    /*--------------------------- Local typedefs ---------------------------------*/

    /*--------------------------- Global variables -------------------------------*/

    /*--------------------------- Local variables --------------------------------*/

    /*--------------------------- Forward declarations ---------------------------*/

    /*--------------------------- Implementation ---------------------------------*/
    /* TODO: Return correct base address of FPGA */
    static uint32 BaseAddressOfFpga(uint8 fpgaId)
        {
        return 0;
        }

    static AtHal CreateHal(uint32 baseAddress)
        {
        /* TODO: user correct HAL for specific hardware platform. The following code is
         * just example */
        return AtHalIndirectDefaultNew(baseAddress);
        }

    static uint32 ReadProductCode(uint32 baseAddress)
        {
        AtHal hal = CreateHal(baseAddress);
        uint32 productCode = AtProductCodeGetByHal(hal);

        AtHalDelete(hal);

        if (!AtDriverProductCodeIsValid(AtDriverSharedDriverGet(), productCode))
            AtPrintc(cSevCritical, "ERROR: product code 0x%08x is invalid\r\n", productCode);

        return productCode;
        }

    /* To setup HALs for all of IP Cores */
    static void DeviceSetup(AtDevice device)
        {
        uint8 fpga_i;

        for (fpga_i = 0; fpga_i < AtDeviceNumIpCoresGet(device); fpga_i++)
            {
            uint32 fpgaBaseAddress = BaseAddressOfFpga(fpga_i);
            AtIpCoreHalSet(AtDeviceIpCoreGet(device, 0), CreateHal(fpgaBaseAddress));
            }
        }

    /* Save all HALs installed to this device */
    static AtList AllDeviceHals(AtDevice device)
        {
        uint8 coreId;
        AtList hals;

        if (device == NULL)
            return NULL;

        hals = AtListCreate(0);
        for (coreId = 0; coreId < AtDeviceNumIpCoresGet(device); coreId++)
            {
            AtObject hal = (AtObject)AtIpCoreHalGet(AtDeviceIpCoreGet(device, coreId));
            if (!AtListContainsObject(hals, hal))
                AtListObjectAdd(hals, hal);
            }

        return hals;
        }

    /*
     * Sample device clean up function.
     *
     * Application create HALs and install them to AtDevice, so application must be
     * responsible for deleting these HALs. But, during device deleting, driver may
     * use HAL objects for special purposes, so HAL should be deleted after device
     * is deleted.
     *
     * The following sample code will backup all HALs that installed to device,
     * after deleting device, all of them are deleted
     */
    static eAtRet DeviceCleanUp(AtDevice device)
        {
        AtList hals;

        if (device == NULL)
            return cAtOk;

        /* Save all HALs that are installed so far */
        hals = AllDeviceHals(device);
        AtDriverDeviceDelete(AtDriverSharedDriverGet(), device);

        /* Delete all HALs */
        while (AtListLengthGet(hals) > 0)
            AtHalDelete((AtHal)AtListObjectRemoveAtIndex(hals, 0));
        AtObjectDelete((AtObject)hals);

        return cAtOk;
        }

    eAtRet AtDriverCleanup(AtDriver driver)
        {
        uint8 numDevices, i;

        if (driver == NULL)
            return cAtOk;

        /* Delete all devices */
        AtDriverAddedDevicesGet(driver, &numDevices);
        for (i = 0; i < numDevices; i++)
            {
            AtDevice device = AtDriverDeviceGet(driver, 0);
            DeviceCleanUp(device);
            }

        /* Delete driver then delete all saved HALs */
        AtDriverDelete(driver);

        return cAtOk;
        }

    eAtRet AtDriverInit(void)
        {
        AtDriver newDriver = AtDriverCreate(1, AtOsalLinux());
        AtDevice newDevice = AtDriverDeviceCreate(AtDriverSharedDriverGet(), ReadProductCode(BaseAddressOfFpga(0)));

        if (newDevice == NULL)
            return cAtErrorRsrcNoAvail;

        /* And setup it */
        DeviceSetup(newDevice);

        /* Optional: initialize device */
        AtDeviceInit(newDevice);

        return cAtOk;
        }
    
At the time when device is created, the API asks product code to create correct device instance and application will know where to get it. Or just simplify input a hard product code which is unique for all of AT Devices.

## Common resource management [resouceManagement]

One product may not support full capacity and it has a set of resources and application must be responsible for creating and destroying these resources. Once resource object is created, it will live until it is destroyed, or when device or driver is deleted. A resource can only be created one time so if creating API is called more than one time, NULL is returned from the second time.

For each module, the SDK supports some APIs to:

* Get maximum number of resources
* Get a free resource identifier
* Create a resource object
* Get a resource object
* Delete a resource object
* Resource iterator.

One resource is identified by a unique identifier and there may be more than one APIs to create resources that work in different modes. Resource ID is also used for deleting. The following sample code will use all encapsulation resource management APIs. 

    uint32 freeResourceId;
    AtIterator resourceIterator;
    AtEncapChannel encapChannel;

    /* Use encapsulation of first device */
    AtDevice device = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), NULL)[0];
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);

    /* Resource status */
    AtPrintc(cSevInfo, "Max number of encapsulation channel: %d\r\n", AtModuleEncapMaxChannelsGet(encapModule));
    AtPrintc(cSevInfo, "Next free encapsulation channel: %d\r\n", AtModuleEncapFreeChannelGet(encapModule));

    /* Create a resource */
    freeResourceId = AtModuleEncapFreeChannelGet(encapModule);
    encapChannel   = (AtEncapChannel)AtModuleEncapHdlcPppChannelCreate(encapModule, freeResourceId);
    assert(encapChannel != NULL);
    assert(AtModuleEncapChannelGet(encapModule, freeResourceId) == encapChannel);

    /* Create other resources */
    assert(AtModuleEncapHdlcFrChannelCreate   (encapModule, AtModuleEncapFreeChannelGet(encapModule)) != NULL);
    assert(AtModuleEncapCiscoHdlcChannelCreate(encapModule, AtModuleEncapFreeChannelGet(encapModule)) != NULL);
    assert(AtModuleEncapAtmTcCreate           (encapModule, AtModuleEncapFreeChannelGet(encapModule)) != NULL);

    /* Try creating a resource more than one times */
    /* The first time is OK */
    freeResourceId = AtModuleEncapFreeChannelGet(encapModule);
    assert(AtModuleEncapHdlcPppChannelCreate  (encapModule, freeResourceId) != NULL);

    /* But after that, all fail */
    assert(AtModuleEncapHdlcPppChannelCreate  (encapModule, freeResourceId) == NULL);
    assert(AtModuleEncapHdlcFrChannelCreate   (encapModule, freeResourceId) == NULL);
    assert(AtModuleEncapCiscoHdlcChannelCreate(encapModule, freeResourceId) == NULL);
    assert(AtModuleEncapAtmTcCreate           (encapModule, freeResourceId) == NULL);

    /* Other source code may go then... */

    /* Iterate on created resources */
    resourceIterator = AtModuleEncapChannelIteratorCreate(encapModule);
    while ((encapChannel = (AtEncapChannel)AtIteratorNext(resourceIterator)) != NULL)
        {
        AtChannel channel = (AtChannel)encapChannel;
        AtPrintc(cSevInfo, "Channel: %s.%d\r\n", AtChannelTypeString(channel), AtChannelIdString(channel));
        AtPrintc(cSevInfo, "Encapsulation type: %d\r\n", AtEncapChannelEncapTypeGet(encapChannel));

        /* Show physical interface */
        channel = AtEncapChannelBoundPhyGet(encapChannel);
        AtPrintc(cSevInfo, "Bound physical: %s.%d\r\n", AtChannelTypeString(channel), AtChannelIdString(channel));
        }
    AtObjectDelete(resourceIterator); /* Remember to delete iterator after using */

    /* Delete a resource is so straight forward. The following code delete resource ID 1 */
    assert(AtModuleEncapChannelDelete(encapModule, 1) == cAtOk);
    assert(AtModuleEncapChannelGet(encapModule, 1)    == NULL);

## PDH programming guide [pdh]

![PDH class diagram](images/class_diagram_pdh.svg)

All of objects in PDH module are abstracted by the `AtPdhChannel` class which 
extends from `AtChannel`. That means all of all of methods of `AtChannel` are 
applicable for `AtPdhChannel`.

By the way, `AtPdhChannel` is an abstract class which concrete class `AtPdhDe1`
extends from. `AtPdhDe1` has some additional methods to control signaling and 
NxDS0

>**NOTE**: The two methods `AtModulePdhDe1Get` and `AtModulePdhNumberOfDe1sGet` are to get 
DS1/E1 resources that come to LIU. For DS1s/E1s that are mapped to SDH VC-11/VC-12, 
the `AtSdhChannelMapChannelGet` is used to get the internal DS1/E1.

The following sample code below will set default configuration for a DS1/E1 and 
show status

    #include "AtDriver.h"
    #include "AtDevice.h"
    #include "AtModulePdh.h"
    
    /* Helper function to access DS1/E1 object by its flat ID. This function assumes
     * that only one device is created */
    static AtPdhChannel GetDe1ByFlatId(uint32 de1Id)
        {
        /* Normally, the device object or PDH module object is stored
         * by application so it does not need to look up device from shared
         * driver object like the following code below. The following codes is
         * to show that all of objects can be retrieved from shared driver instance */
        AtDriver driver = AtDriverSharedDriverGet();
        AtDevice device = AtDriverAddedDevicesGet(driver, null)[0];
        AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);

        return (AtPdhChannel)AtModulePdhDe1Get(pdhModule, de1Id);
        }
    
    /* Set default configuration for input DS1/E1. */
    void SetDefaultDe1Configuration(uint32 de1Id)
        {
        /* Get object to configure */
        AtPdhChannel de1 = GetDe1ByFlatId(de1Id);
    
        /* Set default configuration */
        AtPdhChannelFrameTypeSet(de1, cAtPdhE1UnFrm);
        AtPdhChannelLineCodeSet(de1, cAtPdhDe1LineCodeNrz);
    
        /* Enable signaling */
        AtPdhDe1SignalingEnable((AtPdhDe1)de1);
        }
    
    /* Show status of DS1/E1 */
    void ShowDe1Status(uint32 de1Id)
        {
        /* Get object to show status */
        AtChannel de1 = (AtChannel)GetDe1ByFlatId(de1Id);
    
        /* Show alarm information */
        printf("Alarm: 0x%08x\n", AtChannelAlarmGet(de1));
        printf("Alarm history: 0x%08x\n", AtChannelAlarmHistoryGet(de1));
    
        /* Show counter information (read to clear mode).
         * The API AtChannelCounter() can be used for read only mode.
         */
        printf("BPV EXZ counter: %d\n", AtChannelCounterClear(de1, cAtPdhDe1CounterBpvExz));
        printf("CRR error counter: %d\n", AtChannelCounterClear(de1, cAtPdhDe1CounterCrc));
        printf("FE counter: %d\n", AtChannelCounterClear(de1, cAtPdhDe1CounterFe));
        printf("REI counter: %d\n", AtChannelCounterClear(de1, cAtPdhDe1CounterRei));
    
        /* Clear alarm history */
        AtChannelAlarmHistoryClear(de1);
        }

### NxDS0 management

For a DS1/E1 that channellizes to NxDS0s, [common resource management scheme] [resouceManagement] is applied. Related APIs:

* AtPdhDe1NxDs0Create: to create NxDS0 group with specified bit mask
* AtPdhDe1NxDs0Delete: Delete NxDS0 group
* AtPdhDe1NxDs0Get: Get NxDS0 object by bit mask
* AtPdhDe1nxDs0IteratorCreate: Iterate NxDS0s that are created in DS1/E1

## Ethernet programming guide [ethernet]

![Ethernet class diagram](images/class_diagram_eth.svg)

Ethernet module manages a set of Ethernet ports and responsible for creating 
traffic flows. 

Number of supported Ethernet ports can be accessed via `AtModuleEthMaxNumberOfPorts` 
and a port can be retrieved by `AtModuleEthPortGet()`. Refer class diagram of 
`AtEthPort` to see attributes and methods of one Ethernet port.

AT Device supports a number of traffic flows. Each flow is identified by a flat ID and 
it is abstracted by class `AtEthFlow`. There are some factory methods to create a flow
for specific purposes.

|Flow type         | Factory method             |
|-----------------------------------------------|
|NP over PPP       | `AtModuleEthNopFlowCreate` |
|Ethernet over PPP | `AtModuleEthEopFlowCreate` |
[Ethernet flow factory methods]

>**NOTE**: Ethernet flow is only applicable for PPP/MLPPP product.

To create a flow, application should call `AtModuleEthFreeFlowIdGet` to query a 
free flow ID to create a flow so that traffic can be setup. If not using this 
API, application must manage flow resources.

[Resource management] [resourceManagement] section is also applied for Ethernet flow management.

APIs to setup VLAN traffic:

* `AtEthVlanTagConstruct`: to construct a VLAN tag from priority, CFI and VLAN ID.
* `AtEthFlowVlanDescConstruct`: to create VLAN traffic descriptor
* `AtEthFlowIngressVlanTrafficAdd`: to add VLAN traffic described by traffic 
  descriptor to flow from Ethernet side to Encapsulation side
* `AtEthFlowEgressVlanTrafficSet`: to set VLAN traffic described by traffic 
  descriptor to flow from Encapsulation side to Ethernet side.

The different between "NP over PPP" and "Ethernet over PPP" is:

* NP over PPP: Only IP packet is encapsulated into PPP frame
* Ethernet over PPP: full Ethernet frame is encapsulated into PPP frame. 

The sample code below create a flow with two VLANs from Ethernet side to 
Encapsulation side and one VLAN for the reverse direction.
    
    #include "AtDriver.h"
    #include "AtModuleEth.h"

    tAtEthVlanDesc vlan1, vlan2;
    tAtEthVlanTag sVlan, cVlan;
    uint8 ethPortId = 0;

    /* Get device instance first */
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverAddedDevicesGet(driver, null)[0];

    /* Create traffic flow */
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    AtEthFlow flow = AtModuleEthNopFlowCreate(ethModule, AtModuleEthFreeFlowIdGet(ethModule));

    /* Add VLAN1 to this flow, direction from Ethernet to Encapsulation */
    AtEthFlowVlanDescConstruct(ethPortId,
                               AtEthVlanTagConstruct(1, 0, 1, &sVlan),
                               AtEthVlanTagConstruct(1, 0, 2, &cVlan),
                               &vlan1);
    AtEthFlowIngressVlanAdd(flow, &vlan1);

    /* Add VLAN2 to this flow, direction from Ethernet to Encapsulation */
    AtEthFlowVlanDescConstruct(ethPortId,
                               AtEthVlanTagConstruct(2, 0, 1, &sVlan),
                               AtEthVlanTagConstruct(2, 0, 2, &cVlan),
                               &vlan2);
    AtEthFlowIngressVlanAdd(flow, &vlan2);

    /* Set VLAN1 for direction from Encapsulation to Ethernet side */
    AtEthFlowEgressVlanSet(flow, &vlan1);

    /* And can configure Source MAC, Dest MAC for this direction */
    AtEthFlowEgressDestMacSet(flow, (uint8[6]){0xC0, 0xCA, 0xC0, 0xCA, 0xC0, 0xCA});
    AtEthFlowEgressSourceMacSet(flow, (uint8[6]){0xCA, 0xFE, 0xCA, 0xFE, 0xCA, 0xFE});

    /* Application can also remove VLAN traffic on direction from Ethernet
     * to Encapsulation module. The following code removes VLAN2 */
    AtEthFlowIngressVlanRemove(flow, &vlan2);

## Encapsulation programming guide [encap]

![Encapsulation class diagram](images/class_diagram_enc.svg)

Encapsulation module is responsible for encapsulating HDLC, ATM or other kinds 
of encapsulation.
The hardware can can only support a number of of encapsulation channels. Each 
channel can support one of following types:

* PPP/HDLC
* FR/HDLC
* ATM
* TBD (in the future)

To have a encapsulation channel for specific type, application must call 
corresponding factory method.

|Encapsulation type|Factory method                          |
|-----------------------------------------------------------|
|PPP/HDLC          |  `AtModuleEncapHdlcPppChannelCreate`   |
|Cisco HDLC        |  `AtModuleEncapCiscoHdlcChannelCreate` |
|FR/HDLC           |  `AtModuleEncapHdlcFrChannelCreate`    |
[Encapsulation channel factory methods]

After creating, encapsulation channel is bound with a physical interface which 
can be PDH channel (DS1/E1) or SDH channel (VC). The example code below will 
access Encapsulation module and PDH module to create Encapsulation channels and
 bind DS1s/E1s as physical interfaces

    #include "AtDriver.h"
    #include "AtModuleEncap.h"
    #include "AtModulePdh.h"

    eAtRet ret = cAtOk;

    /* Access Encapsulation and PDH modules */
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverAddedDevicesGet(driver, null)[0];
    AtModuleEncap moduleEncap = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtModulePdh modulePdh = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);

    /* Create 3 links and use first 3xDS1/E1 as physical interfaces */
    ret = AtEncapChannelPhyBind((AtEncapChannel)AtModuleEncapHdlcPppChannelCreate(moduleEncap, 0),
                                (AtChannel)AtModulePdhDe1Get(modulePdh, 0));
    ret |= AtEncapChannelPhyBind((AtEncapChannel)AtModuleEncapHdlcFrChannelCreate(moduleEncap, 1),
                                 (AtChannel)AtModulePdhDe1Get(modulePdh, 1));
    ret |= AtEncapChannelPhyBind((AtEncapChannel)AtModuleEncapCiscoHdlcChannelCreate(moduleEncap, 2),
                                 (AtChannel)AtModulePdhDe1Get(modulePdh, 2));

    /* Handle error code */
    HandleErrorCode(ret);
    
### PPP/MLPPP programming guide

![PPP/MLPPP class diagram](images/class_diagram_ppp.svg)

This module is responsible for managing PPP links and MLPPP bundles. This module can be retrieved from Device handle via API `ThaDeviceModuleGet()`. After encapsulation channel is created by `AtModuleEncapHdlcPppChannelCreate` factory method, it holds a link object which can be accessed via the API `AtHdlcChannelPppLinkGet`. This link can be setup to carry Ethernet traffics or it is provisioned to a MLPPP bundle to carry MLPPP traffics.

A bundle is created by this module by the API `AtModulePppMpBundleCreate` then PPP links can be added via API `AtEncapBundleLinkAdd`

Both PPP link and bundle can carry traffic. Traffics come to this module via flows.

#### How to create a link and setup link traffic [setupLinkTraffic]
    
The sample code below will create one PPP link carries traffic. This link uses E1/DS1 as physical layer. This code also assumes that there is only device is added to driver. The code for Cisco HDLC link is similar with PPP link, just the way it is created is different, `AtModuleEncapCiscoHdlcChannelCreate` is used to create it.

    #include "AtDriver.h"
    #include "AtModuleEncap.h"
    #include "AtModulePdh.h"
    #include "AtModuleEth.h"
    #include "AtModulePpp.h"
     
    tAtEthVlanDesc vlanTrafficDesc;
    tAtEthVlanTag sVlan, cVlan;
    uint8 ethPortId = 0;
    uint8 de1Id = 0;

    /* Get device instance first */
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverAddedDevicesGet(driver, null)[0];

    /* Allocate HDLC channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapHdlcPppChannelCreate(
                                    encapModule,
                                    AtModuleEncapFreeChannelGet(encapModule));

    /* Or create an encap channel with Cisco HDLC mode
     * AtHdlcChannel encapChannel = AtModuleEncapCiscoHdlcChannelCreate(encapModule, encapChannelId);
     */

    /* Use DS1/E1 as physical channel */
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)AtModulePdhDe1Get(pdhModule, de1Id));

    /* Create flow and setup VLAN traffic descriptor for two directions */
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    AtEthFlow flow = AtModuleEthNopFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId,
                               AtEthVlanTagConstruct(1, 0, 1, &sVlan),
                               AtEthVlanTagConstruct(1, 0, 2, &cVlan),
                               &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow, &vlanTrafficDesc);
    AtEthFlowEgressVlanSet(flow, &vlanTrafficDesc);

    /* Bring this flow to this link */
    AtPppLink link = (AtPppLink)AtHdlcChannelHdlcLinkGet(encapChannel);
    AtHdlcLinkFlowBind((AtHdlcLink)link, flow);

    /* Traffic on link is disabled at both direction as default. Application PPP
     * protocol will determine when to enable traffic flow like below */
    AtHdlcLinkTxTrafficEnable((AtHdlcLink)link, cAtTrue);
    AtHdlcLinkRxTrafficEnable((AtHdlcLink)link, cAtTrue);

#### How to create bundle and setup bundle traffic

The sample code below will create one bundle with two links. These two links use DS1/E1 as their physical interfaces. Two traffic flows are added to bundle with different assigned classes. If bundle does not work in Multi-Class Extension, only one traffic it can carry and class number should be input as 0.

    #include "AtDriver.h"
    #include "AtModuleEncap.h"
    #include "AtModulePdh.h"
    #include "AtModuleEth.h"
    #include "AtModulePpp.h"
    
    tAtEthVlanDesc vlanTrafficDesc;
    tAtEthVlanTag sVlan, cVlan;
    uint8 ethPortId = 0;

    /* Get device instance first */
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverAddedDevicesGet(driver, null)[0];

    /* Allocate HDLC channels, must be PPP/HDLC */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel1 = AtModuleEncapHdlcPppChannelCreate(encapModule, AtModuleEncapFreeChannelGet(encapModule));
    AtHdlcChannel encapChannel2 = AtModuleEncapHdlcPppChannelCreate(encapModule, AtModuleEncapFreeChannelGet(encapModule));

    /* Use DS1s/E1s as physical interfaces */
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel1, (AtChannel)AtModulePdhDe1Get(pdhModule, 0));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel2, (AtChannel)AtModulePdhDe1Get(pdhModule, 1));

    /* Create MLPPP bundle and add these links */
    AtModulePpp pppModule = (AtModulePpp)AtDeviceModuleGet(device, cAtModulePpp);
    AtMpBundle bundle = (AtMpBundle)AtModulePppMpBundleCreate(pppModule, 0); /* Use the first bundle */
    AtHdlcBundleLinkAdd((AtHdlcBundle)bundle, AtHdlcChannelHdlcLinkGet(encapChannel1));
    AtHdlcBundleLinkAdd((AtHdlcBundle)bundle, AtHdlcChannelHdlcLinkGet(encapChannel2));

    /* Enable traffic on links */
    AtHdlcLinkTxTrafficEnable(AtHdlcChannelHdlcLinkGet(encapChannel1), cAtTrue);
    AtHdlcLinkRxTrafficEnable(AtHdlcChannelHdlcLinkGet(encapChannel1), cAtTrue);
    AtHdlcLinkTxTrafficEnable(AtHdlcChannelHdlcLinkGet(encapChannel2), cAtTrue);
    AtHdlcLinkRxTrafficEnable(AtHdlcChannelHdlcLinkGet(encapChannel2), cAtTrue);

    /* Create traffic flow 1 */
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    AtEthFlow flow1 = AtModuleEthNopFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId,
                                      AtEthVlanTagConstruct(1, 0, 1, &sVlan),
                                      AtEthVlanTagConstruct(1, 0, 2, &cVlan),
                                      &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow1, &vlanTrafficDesc);
    AtEthFlowEgressVlanSet(flow1, &vlanTrafficDesc);

    /* Create traffic flow 2 with only one VLAN */
    AtEthFlow flow2 = AtModuleEthNopFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId,
                               null,
                               AtEthVlanTagConstruct(1, 0, 3, &cVlan),
                               &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow2, &vlanTrafficDesc);
    AtEthFlowEgressVlanSet(flow2, &vlanTrafficDesc);

    /* Bring these flows to this bundle */
    AtMpBundleFlowAdd(bundle, flow1, 1); /* Add and assign flow 1 to class 1 */
    AtMpBundleFlowAdd(bundle, flow2, 2); /* Add and assign flow 2 to class 2 */

#### How to enable OAM inserting and receiving OAM notification

The SDK itself does not support PPP/MLPPP protocol, it just supports a set of APIs so that application can send OAM, receive OAM notification. And application must be responsible for handling OAM packet format.

OAM packets will be carried on PPP link and there are two modes of OAM receiving:

* Terminate so that CPU can read. In this mode, application can receive OAM notification from driver via listener.
* Bypass to PSN (Ethernet side). In this mode, PSN header is built along with OAM message and come to PSN side. This mode is not fully supported by the current version of SDK (1.0.10).

To terminate OAM to CPU, application needs to call `AtHdlcLinkOamPacketModeSet` with `cAtHdlcLinkOamModeToCpu` mode.
To send OAM, it needs to build PPP frame format then input OAM packet buffer to the API `AtHdlcLinkOamSend` and hardware will send immediately.
To receive OAM notification, application must register event listener by implementing the interface OamReceived of tAtChannelEventListener and call AtChannelEventListenerAdd to listen on OAM event.

When OAMs come, interrupt happen and application must call `AtDeviceInterruptProcess` so that the driver can handle interrupt and notify OAM to application.
For some products, interrupt may not be supported and application must call `AtDevicePeriodicProcess` to help driver poll OAM and notify new OAM.

When an OAM come, it may be dropped by hardware instead of terminating to CPU depending on link phase which is controlled by `AtPppLinkPhaseSet`. The purpose of this feature is to help software reduce load when many unnecessary OAMs come at one phase. Phase transition is as following: 

* `cAtPppLinkPhaseDead`: Go to Dead phase. No data packet nor control packet flows
* `cAtPppLinkPhaseEstablish`: Go to establish phase. Only LCP control packets flow. Data and NCP packets are dropped
* `cAtPppLinkPhaseAuthenticate`: Go to Authenticate phase. Only LCP, authentication protocol and link quality monitoring packets are allowed. This phase has not been supported by current hardware. It is defined for future use.
* `cAtPppLinkPhaseEnterNetwork`: Go to Network phase, only LCP/NCP control packets flow. Data packets are dropped
* `cAtPppLinkPhaseNetworkActive`: Still in Network phase and all of packets can flow

If application does not want hardware early drop unnecessary packets, it may only care about `cAtPppLinkPhaseNetworkActive` phase.

The sample code below will register OAM event listener and configure link phase:

    /* This function will be called by driver when OAM come. Application protocol
     * logic should be added here */
    static void OamReceived(AtChannel channel, uint8 *data, uint32 length)
        {
        AtPrintc(cSevInfo, "Application!, I (driver) have just received this OAM, examine variable data and length and do your own way\r\n");
        }
    
    void OamSetup(AtPppLink link)
        {
        static tAtChannelEventListener oamListener;
    
        oamListener.AlarmChangeState = NULL;
        oamListener.OamReceived      = OamReceived;
    
        assert(AtChannelEventListenerAdd((AtChannel)link, &oamListener) == cAtOk);
        }

### FR/MFR programming guide

![Frame relay class diagram](images/class_diagram_fr.svg)

Frame relay module manages a set of bundles and each bundle is created from Frame Relay link

Like PPP/MLPPP module, this module also manages a set of bundles and Frame Relay links. Frame Relay links create from Encapsulation module and this module uses these links as input.

Ethernet flows can be carried by Frame Relay link or bundle. When a flow is added to link or bundle, it is assigned DLCI.

#### How to create Frame relay link and add traffic

Configure FR link and setup traffic look like as the way PPP link does. Using different factory method `AtModuleEncapHdlcFrChannelCreate` of Encapsulation module to create a channel that supports frame relay, flow is added to link with assigned DLCI number.

The sample code below will create one FR link and setup one traffic on it.

    #include "AtDriver.h"
    #include "AtModuleEncap.h"
    #include "AtModulePdh.h"
    #include "AtModuleEth.h"
    #include "AtModuleFr.h"
    #include "AtFrLink.h"

    tAtEthVlanDesc vlanTrafficDesc;
    tAtEthVlanTag sVlan, cVlan;
    uint8 ethPortId = 0;
    uint8 de1Id = 0;

    /* Get device instance first */
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverAddedDevicesGet(driver, null)[0];

    /* Allocate HDLC channel */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapHdlcFrChannelCreate(
                                    encapModule,
                                    AtModuleEncapFreeChannelGet(encapModule));

    /* Use DS1/E1 as physical channel */
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)AtModulePdhDe1Get(pdhModule, de1Id));

    /* Create flow and setup VLAN traffic descriptor for two directions */
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    AtEthFlow flow = AtModuleEthNopFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId,
                               AtEthVlanTagConstruct(1, 0, 1, &sVlan),
                               AtEthVlanTagConstruct(1, 0, 2, &cVlan),
                               &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow, &vlanTrafficDesc);
    AtEthFlowEgressVlanSet(flow, &vlanTrafficDesc);

    /* Bring this flow to this link */
    AtFrLink link = (AtFrLink)AtHdlcChannelHdlcLinkGet(encapChannel);
    AtFrLinkFlowAdd(link, flow, 1 /* DLCI */);

    /* Traffic on link is disabled at both direction as default. Application PPP
     * protocol will determine when to enable traffic flow like below */
    AtHdlcLinkTxTrafficEnable((AtHdlcLink)link, cAtTrue);
    AtHdlcLinkRxTrafficEnable((AtHdlcLink)link, cAtTrue);

#### How to create Frame relay bundle and add traffic

Like PPP bundle, MFR bundle is created from FR links and these links can use any AtChannel for their physical layers. Traffic flows are added to bundle, each of them are assigned DLCI value.

The sample code below create one MFR bundle with two MFR links and add two traffic flows to bundle.

    #include "AtDriver.h"
    #include "AtModuleEncap.h"
    #include "AtModulePdh.h"
    #include "AtModuleEth.h"
    #include "AtModuleFr.h"
    #include "AtHdlcChannel.h"
    #include "AtHdlcBundle.h"

    tAtEthVlanDesc vlanTrafficDesc;
    tAtEthVlanTag sVlan, cVlan;
    uint8 ethPortId = 0;

    /* Get device instance first */
    AtDriver driver = AtDriverSharedDriverGet();
    AtDevice device = AtDriverAddedDevicesGet(driver, null)[0];

    /* Allocate HDLC channels, must be PPP/HDLC */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel1 = AtModuleEncapHdlcFrChannelCreate(encapModule, AtModuleEncapFreeChannelGet(encapModule));
    AtHdlcChannel encapChannel2 = AtModuleEncapHdlcFrChannelCreate(encapModule, AtModuleEncapFreeChannelGet(encapModule));

    /* Use DS1s/E1s as physical interfaces */
    AtModulePdh pdhModule = (AtModulePdh)AtDeviceModuleGet(device, cAtModulePdh);
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel1, (AtChannel)AtModulePdhDe1Get(pdhModule, 0));
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel2, (AtChannel)AtModulePdhDe1Get(pdhModule, 1));

    /* Create MFR bundle and add these links */
    AtModuleFr frModule = (AtModuleFr)AtDeviceModuleGet(device, cAtModuleFr);
    AtMfrBundle bundle = (AtMfrBundle)AtModuleFrMfrBundleCreate(frModule, 0); /* Use the first bundle */
    AtHdlcBundleLinkAdd((AtHdlcBundle)bundle, AtHdlcChannelHdlcLinkGet(encapChannel1));
    AtHdlcBundleLinkAdd((AtHdlcBundle)bundle, AtHdlcChannelHdlcLinkGet(encapChannel2));

    /* Enable traffic on links */
    AtHdlcLinkTxTrafficEnable(AtHdlcChannelHdlcLinkGet(encapChannel1), cAtTrue);
    AtHdlcLinkRxTrafficEnable(AtHdlcChannelHdlcLinkGet(encapChannel1), cAtTrue);
    AtHdlcLinkTxTrafficEnable(AtHdlcChannelHdlcLinkGet(encapChannel2), cAtTrue);
    AtHdlcLinkRxTrafficEnable(AtHdlcChannelHdlcLinkGet(encapChannel2), cAtTrue);

    /* Create traffic flow 1 */
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    AtEthFlow flow1 = AtModuleEthNopFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId,
                               AtEthVlanTagConstruct(1, 0, 1, &sVlan),
                               AtEthVlanTagConstruct(1, 0, 2, &cVlan),
                               &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow1, &vlanTrafficDesc);
    AtEthFlowEgressVlanSet(flow1, &vlanTrafficDesc);

    /* Create traffic flow 2 with only one VLAN */
    AtEthFlow flow2 = AtModuleEthNopFlowCreate(ethModule, 0);
    AtEthFlowVlanDescConstruct(ethPortId,
                               null,
                               AtEthVlanTagConstruct(1, 0, 3, &cVlan),
                               &vlanTrafficDesc);
    AtEthFlowIngressVlanAdd(flow2, &vlanTrafficDesc);
    AtEthFlowEgressVlanSet(flow2, &vlanTrafficDesc);

    /* Bring these flows to this bundle */
    AtMfrBundleFlowAdd(bundle, flow1, 1); /* Add and assign flow 1 to DLCI 1 */
    AtMfrBundleFlowAdd(bundle, flow2, 2); /* Add and assign flow 2 to DLCI 2 */

### How to change configuration

There are two ways to change configuration:

* Using SET/GET to change attributes. Refer class diagrams for attributes of relative objects
* Using function AtChannelAllConfigSet(). See the table below for the data structure that hold all of configuration.

|Channel         | All configuration structure |
|----------------------------------------------|
| AtHdlcChannel  | tAtHdlcChannelAllConfig     |
| AtMfrBundle    | tAtMfrBundleAllConfig       |
| AtMpBundle     | tAtMpBundleAllConfig        |
[All configuration data structure for Encapsulation related objects]

### How to access performance statistic

Counters of all HDLC channels, links and bundles in this module can be retrieved via API: AtChannelAllCountersGet().

The table below summary counter types and counter structure for all of channel types

|Channel         | All counters structure |
|-----------------------------------------|
|AtHdlcBundle    | tAtHdlcBundleCounters  |
|AtHdlcChannel   | tAtHdlcChannelCounters |  
|AtPppLink       | tAtPppLinkCounters     |
|AtFrLink        | tAtFrLinkCounters      |
[Counters data structure for Encapsulation related objects]

### Controlling HDLC address/control/FCS fields

From SDK version 1.0.10, APIs to control address / control and FCS fields are added so that application can change:

* Inserted address / control.
* Enable / disable address / control bypassing to PSN side
* Enable / disable address / control field comparison
* Error handling: drop error packets or bypass them to PSN side. Error packets include packets that address / control mismatch or FCS error

These configurations are configured as default when an HDLC encapsulation channel is created and of course, they do not need to be changed. But for some reasons (debugging, for example), application may want to change. Refer API manual of `AtHdlcChannel` class.

## SDH programming guide [sdh]

![SDH class diagram](images/class_diagram_sdh.svg)

The SDH module supports a number of Lines - `AtSdhLine` and this capacity is exposed by the API `AtModuleSdhMaxLinesGet()`. A line object can be retrieved by its flat identifier and the API `AtModuleSdhLineGet()` is used for this purpose.

All of channels in SDH module are abstracted by `AtSdhChannel` which extends from `AtChannel`. That means all of methods of `AtChannel` and additional methods of `AtSdhChannel` can be used for all of SDH objects.

>**NOTE**: For `AtSdhAug` and `AtSdhTug` classes, only mapping methods can be called, they have no alarm, overhead, timing mode, .... Calling other methods on these class are just silently ignored or error code is returned.

### Control a Line

Configuring a Line is so straight forward, just set its rate and setup J0 messages. See the sample code below for more information. 

    #include <string.h>

    #include "AtDriver.h"
    #include "AtModuleSdh.h"
    #include "AtSdhChannel.h"
    
    uint8 lineId = 0; /* Line needs to be configured. */

    /* Get SDH module first */
    AtDevice device = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), null)[0];
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    /* Get line object and configure its rate to STM-1 */
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhLineRateSet(line, cAtSdhLineRateStm1);

    /* Configure TTI, both transmitted and expected */
    const char * ttiMessage = "Line#0";
    tAtSdhTti tti;
    AtSdhTtiMake(cAtSdhTtiMode16Byte, ttiMessage, strlen(ttiMessage), &tti);
    AtSdhChannelTxTtiSet((AtSdhChannel)line, &tti);
    AtSdhChannelExpectedTtiSet((AtSdhChannel)line, &tti);

Alarm status and performance counters can be retrieved. Line is `AtChannel`, so `AtChannelCounterGet()` and `AtChannelAlarmGet()` are used. Line alarm masks are defined by the enumeration `eAtSdhLineAlarmType` and counter types are defined by `eAtSdhLineCounterType`. The following sample code access line alarms and performance counters and display.

    #include "AtDriver.h"
    #include "AtModuleSdh.h"
    #include "AtSdhChannel.h"
    
    uint8 lineId = 0;

    /* Get SDH module first */
    AtDevice device = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), null)[0];
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    /* Get line object and configure its rate to STM-1 */
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhLineRateSet(line, cAtSdhLineRateStm1);

    /* Show Line alarms */
    AtChannel channel = (AtChannel)line;
    uint32 alarm = AtChannelAlarmGet(channel);
    printf("Alarms: ");
    if (alarm & cAtSdhLineAlarmLos)
        printf("LOS ");
    if (alarm & cAtSdhLineAlarmLof)
        printf("LOF ");

    /* So on ... */

    /* Show Line performance counters */
    printf("B1 = %d\n", AtChannelCounterGet(channel, cAtSdhLineCounterTypeB1));
    printf("B2 = %d\n", AtChannelCounterGet(channel, cAtSdhLineCounterTypeB2));
    printf("REI = %d\n", AtChannelCounterGet(channel, cAtSdhLineCounterTypeRei));

    /* Or use another way to read counters in read-to-clear mode */
    printf("B1 = %d\n", AtChannelCounterClear(channel, cAtSdhLineCounterTypeB1));
    printf("B2 = %d\n", AtChannelCounterClear(channel, cAtSdhLineCounterTypeB2));
    printf("REI = %d\n", AtChannelCounterClear(channel, cAtSdhLineCounterTypeRei));

### Setting mapping [sdhMapping]

![SDH mapping tree](images/sdh_mapping.png)

AT Device support the above mapping tree. The following table shows how one node is represented by class in SDH module and associated mapping modes.

|Node type        | SDH class  |Mapping types                      |Description             |
|-------------------------------------------------------------------------------------------|
|AUG-16           |`AtSdhAug`  | `cAtSdhAugMapTypeAug16MapVc4_16c` | AUG-16 maps 1xVC-4-16c |
|                 |            | `cAtSdhAugMapTypeAug16Map4xAug4s` | AUG-16 maps 4xAUG-4s   |
|AUG-4            |`AtSdhAug`  | `cAtSdhAugMapTypeAug4MapVc4_4c`   | AUG-4 maps 1xVC-4-4c   |
|                 |            | `cAtSdhAugMapTypeAug4Map4xAug1s`  | AUG-4 maps 4xAUG-1s    |
|AUG-1            |`AtSdhAug`  | `cAtSdhAugMapTypeAug1MapVc4`      | AUG-1 maps VC-4        |
|                 |            | `cAtSdhAugMapTypeAug1Map3xVc3s`   | AUG-1 maps 3xVC-3s     |
|AU               |`AtSdhAu`   | Always VC                         |                        |
|VC4-16c/VC4-4c   |`AtSdhVc`   | Always C                          |                        |
|VC4              |`AtSdhVc`   | `cAtSdhVcMapTypeVc4MapC4`         | VC-4 maps C4           |
|                 |            | `cAtSdhVcMapTypeVc4Map3xTug3s`    | VC-4 maps 3xTUG-3s     |
|VC3              |`AtSdhVc`   | `cAtSdhVcMapTypeVc3MapC3`         | VC-3 maps C3           |
|                 |            | `cAtSdhVcMapTypeVc3MapDe3`        | VC-3 maps DS3/E3       |
|                 |            | `cAtSdhVcMapTypeVc3Map7xTug2s`    | VC-3 maps 7xTUG-2s     |
|TUG-3            |`AtSdhTug`  | `cAtSdhTugMapTypeTug3MapVc3`      | TUG-3 maps VC-3        |
|                 |            | `cAtSdhTugMapTypeTug3Map7xTug2s`  | TUG-3 maps 7xTUG-2s    |
|TUG-2            |`AtSdhTug`  | `cAtSdhTugMapTypeTug2Map4xTu11s`  | TUG-2 maps 4xTU-11     |
|                 |            | `cAtSdhTugMapTypeTug2Map3xTu12s`  | TUG-2 maps 3xTU-12     |
|TU               |`AtSdhTu`   | Always VC                         |                        |
|VC-12/VC-11      |`AtSdhVc`   | `cAtSdhVcMapTypeVc1xMapC1x`       | Map C-11/C-12          |
|                 |            | `cAtSdhVcMapTypeVc1xMapDe1`       | Map DS1/E1             |
[SDH Mapping]

The following sample code will set mapping from AUG to VC-12 and E1s will be mapped to some VC-12s    

    #include "AtDriver.h"
    #include "AtModuleSdh.h"
    #include "AtSdhPath.h"
    
    uint8 lineId = 0, aug1 = 0, tug3 = 0, tug2, tu;

    /* Get SDH module first */
    AtDevice device = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), null)[0];
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    /* Get line object and configure its rate to STM-4 */
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
    AtSdhLineRateSet(line, cAtSdhLineRateStm4);

    /* Get AUG-4 of this line and map it to 4xAUG-1 */
    AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineAug4Get(line, 0), cAtSdhAugMapTypeAug4Map4xAug1s);

    /* Map desired AUG-1 to VC-4 */
    AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineAug1Get(line, aug1), cAtSdhAugMapTypeAug1MapVc4);

    /* Map this VC-4 to 3xTUG-3s */
    AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineVc4Get(line, aug1), cAtSdhVcMapTypeVc4Map3xTug3s);

    /* Map the first TUG-3 to VC-3 */
    AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineTug3Get(line, aug1, tug3), cAtSdhTugMapTypeTug3MapVc3);

    /* Configure the second TUG-3 to carry E1s */
    /* Map the second TUG-3 to 7xTUG-2s */
    tug3 = 1;
    AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineTug3Get(line, aug1, tug3), cAtSdhTugMapTypeTug3Map7xTug2s);

    /* Map all of TUG-2s in this TUG-3 to TU-12 */
    for (tug2 = 0; tug2 < 7; tug2++)
        AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineTug2Get(line, aug1, tug3, tug2), cAtSdhTugMapTypeTug2Map3xTu12s);

    /* Just for demo purpose, just configure TU-12s of one TUG-2 to carry E1s */
    tug2 = 0;
    for (tu = 0; tu < 3; tu++) /* There are 3 TU-12s in TUG-2 */
        AtSdhChannelMapTypeSet((AtSdhChannel)AtSdhLineVc1xGet(line, aug1, tug3, tug2, tu), cAtSdhVcMapTypeVc1xMapDe1);

### Control Path

In SDH, a path can be AU, VC and TU, all of them are abstracted by the class `AtSdhPath`. For those channel, Path overhead (POH) needs to be configured. APIs to handle alarms, performance counters, interrupt masks, ... of `AtChannel` are applicable for them. The following sample code access VC-3 and VC-12 paths which are created by the above mapping sample code, then configure path overhead for those channels  

Assume that there is one function to configure a path with input TTI and PSL. Like following:

    /* This sample function is to configure a path with input TTI and PSL */
    void ConfigurePathWithTtiAndPsl(AtSdhPath path, const tAtSdhTti *tti, uint8 psl)
        {
        AtSdhChannelTxTtiSet((AtSdhChannel)path, tti);
        AtSdhChannelExpectedTtiSet((AtSdhChannel)path, tti);
    
        /* Or change the Path Signal Label */
        AtSdhPathTxPslSet(path, psl);
        AtSdhPathExpectedPslSet(path, psl);
        }
    
The following code will configure VC-3 and VC-12s that are created by above [mapping sample code] [sdhMapping]

    /* Access the VC-3 that is mapped to the first TUG-3 of AUG-1 */
    AtSdhPath vc3 = (AtSdhPath)AtSdhLineVc3Get(line, aug1, 0);
    const char * ttiMessage = "VC-3#0.0.0";
    tAtSdhTti tti;
    AtSdhTtiMake(cAtSdhTtiMode16Byte, ttiMessage, strlen(ttiMessage), &tti);
    ConfigurePathWithTtiAndPsl(vc3, &tti, 0xFE);

    /* Get one VC-12 of second TUG-3 and configure it */
    AtSdhPath vc12 = (AtSdhPath)AtSdhLineVc1xGet(line, aug1, 1, 0, 0);
    ttiMessage = "VC-12#0.0.1.0.0";
    AtSdhTtiMake(cAtSdhTtiMode16Byte, ttiMessage, strlen(ttiMessage), &tti);
    ConfigurePathWithTtiAndPsl(vc12, &tti, 0x3);

Like Line, Path alarms and performance counters can be retrieved by `AtChannel` methods `AtChannelCounterGet()` and `AtChannelAlarmGet()`. Counter types are defined by `eAtSdhPathCounterType` and alarm marks are defined by `eAtSdhPathAlarmType`. Source code to handle alarms or performance counters are the same for all path types. So the following sample code show alarms and performance counters for VC-12 path .

    AtChannel channel = (AtChannel)AtSdhLineVc1xGet(line, aug1, 1, 0, 0);
    uint32 alarms = AtChannelAlarmGet(channel);
    printf("Alarms: ");
    if (alarms & cAtSdhPathAlarmAis)
        printf("AIS ");
    if (alarms & cAtSdhPathAlarmLop)
        printf("LOP ");

    /* So on ... */

    /* Access performance counters */
    printf("BIP = %d\n", AtChannelCounterGet(channel, cAtSdhPathCounterTypeBip));
    printf("REI = %d\n", AtChannelCounterGet(channel, cAtSdhPathCounterTypeRei));

    /* Or use another way to read counters in read-to-clear mode */
    printf("BIP = %d\n", AtChannelCounterClear(channel, cAtSdhPathCounterTypeBip));
    printf("REI = %d\n", AtChannelCounterClear(channel, cAtSdhPathCounterTypeRei));

### BER monitoring [ber]

![BER class diagram](images/class_diagram_ber.svg)

BER module is designed to monitor Bit-Error-Rate on a channel. A channel can be SDH channel (Line, Path) or PDH channel (DS1/E1/DS3/E3). There is a set of BER controler `AtBerController` that this module manages. These controllers are responsible for monitoring BER on a channel. 

There are two kinds of controller, hard and soft controllers. For hard controller, the hardware will monitor BER on a channel. For soft controller, software has to do this job. Both hard and soft controllers are abstracted to `AtBerController` class to share the same set of APIs. There is no different on controlling them, but if soft controller is used, application need to call `AtModuleBerPeriodicProcess()` or `AtDevicePeriodicProcess()` periodically, and the required period is 50ms.

Although the BER module interfaces factory methods to create BER controller for specific channel, application should not called them. ernal modules of device will know how to interact with this module to allocate controllers for channels that BER monitoring is applicable. For example, SDH module will ask BER module for Line BER controller.

For a module that has channels need to monitor BER, they all have built-in controller, and there is a method to access that controller. For example, for SDH module, BER can be monitored for Line and Path, both Line and Path are `AtSdhChannel` so the method `AtSdhChannelBerControllerGet()` is used for this purpose.

By default, BER controller of a channel is disabled to save CPU usage and application has to determine when to enable it. A built-in controller is already initialized to a valid configuration, that means BER-SD and BER-SF thresholds are set to a valid threshold, BER-SD threshold is usually set to 1E-4 and BER-SF is set to 1E-3. Application just simply call `AtBerControllerEnable()` to enable a controller.

### Access DS1/E1 that is mapped to VC-11/VC-12 [mapDe1ToVc1x]

When VC-11/VC-12 maps DS1/E1, that internal DS1/E1 can be retrieved by the API `AtSdhChannelMapChannelGet()`. The returned DS1/E1 is a instance of `AtPdhDe1` and HDLC channel can bind this DS1/E1 as its physical interface.

With [the sample code that set mapping] [sdhMapping] above, E1s are mapped to TU-12s. For simple demo purpose, the following code will get one E1 and use it as physical interface of HDLC channel. 
    
    /* Additional headers */
    #include "AtModuleEncap.h"
    #include "AtModulePdh.h"
    
    /* Get DS1/E1 that is mapped to VC-12 and configure its framing type first */
    AtSdhChannel channel = (AtSdhChannel)AtSdhLineVc1xGet(line, aug1, 1, 0, 0);
    AtPdhDe1 de1 = (AtPdhDe1)AtSdhChannelMapChannelGet(channel);
    AtPdhChannelFrameTypeSet((AtPdhChannel)de1, cAtPdhE1UnFrm);

    /* Allocate HDLC channel that maps PPP link */
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtHdlcChannel encapChannel = AtModuleEncapHdlcPppChannelCreate(
                                    encapModule,
                                    AtModuleEncapFreeChannelGet(encapModule));

    /* Use DS1/E1 that is mapped to VC-12 as physical interface */
    AtEncapChannelPhyBind((AtEncapChannel)encapChannel, (AtChannel)de1);

Handle PPP traffic is the job of Encapsulation module, refer [How to create a link and setup link traffic] [setupLinkTraffic] for more detail.

### Linear APS programming

Hardware just support functions to send / receive K-bytes, switch, bridge function. Software can use these functions to implement Linear APS protocol.
When a software Linear APS engine is provisioned:

* If architecture is 1+1, `AtSdhLineBridge()` is called to bridge (or duplicate) traffic from working line to protection line. Note, working and protection role are of application, API does not know whether a line is working or protection.
* If architecture is 1:1, it does not need to call `AtSdhLineBridge()` API. This function is called if alarm happen on working line and bridging needs to be made.

When alarm happen on working line, switching will take place and `AtSdhLineSwitch()` is used to switch traffic from working to protection.
When alarm is clear on working line, switching and bridging may be released in case of revertive or force command is issued to release bridge / switch. In this case, `AtSdhLineSwitchRelease()` and `AtSdhLineBridgeRelease()` are used.

### SDH module clock scheme

![SDH clock scheme](images/clock_scheme.svg)

As depicted by above figure, there are 3 port groups, each group has two ports and these two ports use corresponding REFCLK_OCNP clock from External PLL for TX direction. 3 REFCLK_OCNP clocks are controlled by External PLL and they are derived from 3 REFOUT_EXTs output from AT chip set. 

Inside AT Chip set, all of received clocks from 6 ports come to **Clock selector** function, three of them are selected to come to **External PLL** as 8K clock signal. The API `AtModuleSdhOutputClockSelect` is used for this purpose.
    
    /* The following sample code will select:    
     * - REFOUT_EXT 1: select from port 1
     * - REFOUT_EXT 2: select from port 4
     * - REFOUT_EXT 3: select from port 5
     */
    /* Headers */
    #include "AtDriver.h"
    #include "AtModuleSdh.h"
    
    /* Get SDH module */
    AtDevice device = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), null)[0];
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    /*
     * REFOUT_EXT 1: select from port 1
     * REFOUT_EXT 2: select from port 4
     * REFOUT_EXT 3: select from port 5
     */
    AtModuleSdhOutputClockSourceSet(sdhModule, 0, AtModuleSdhLineGet(sdhModule, 0));
    AtModuleSdhOutputClockSourceSet(sdhModule, 1, AtModuleSdhLineGet(sdhModule, 3));
    AtModuleSdhOutputClockSourceSet(sdhModule, 2, AtModuleSdhLineGet(sdhModule, 4));

## Pseudowire programming guide [pseudowire]

![Pseudowire class diagram](images/class_diagram_pw.svg)

The `AtModulePw` manages a set of Pseudowires by a set of APIs that create / delete / access all Pseudowires. 
A Pseudowire is created by Pseudowire factory methods as the table below.

|API                     |Pseudowire|
|-----------------------------------|
|AtModulePwSAToPCreate   |SAToP     |
|AtModulePwCESoPCreate   |CESoPSN   |
|AtModulePwCepCreate     |CEP       |
|AtModulePwAtmCreate     |ATM       |
|AtModulePwHdlcPppCreate |HDLC/PPP  |
[Pseudowire factory]
 
After creating, it has a set of default configuration but bound circuit, PSN and  Ethernet layer are all invalid. Application must call Pseudowire methods to:

* Set PSN
* Ethernet port and header
* Attachment circuit. A circuit can be any object of `AtChannel` sub-class that can be attached to Pseudowire, for example, channel of `AtPdhDe3`, `AtPdhDe1`, `AtPdhNxDS0`. Note, in case of ATM Pseudowire, it always works with ATM cross-connect. One ATM pseudowire can work with many ATM cross-connects, so attachment circuit is not applicable for it. Refer [ATM/IMA programming guide] [atm_ima] to see how it can work with ATM cross-connect 

A Pseudowire should be activated when PSN, Ethernet and circuit are all valid. To activate a Pseudowire, `AtChannelEnable()` is used.

### Creating a Pseudowire [Creating a Pseudowire]

The following sample code below will create one SAToP Pseudowire, attachment circuit is the DS1/E1 mapped to VC-12 as mentioned in [Access DS1/E1 that is mapped to VC-11/VC-12] [mapDe1ToVc1x] section. It will set PSN and Ethernet layer for that Pseudowire
    
    #include "AtDriver.h"
    #include "AtModulePw.h"
    #include "AtModuleEth.h"
    #include "AtPdhDe1.h"
    
    /* Assume that DS1/E1 object already exists */
    extern AtPdhDe1 de1;
    uint16 pwId = 0;
    tAtPwMplsLabel label;
    tAtEthVlanTag cVlan, sVlan;

    /* Get PW module and create a SAToP PW */
    AtDevice device = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), null)[0];
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    AtPw pw = (AtPw)AtModulePwSAToPCreate(pwModule, pwId);

    /* Create MPLS PSN */
    AtPwMplsPsn mplsPsn = AtPwMplsPsnNew();
    AtPwMplsPsnInnerLabelSet(mplsPsn, AtPwMplsLabelMake(1 /* Label */, 2 /* EXP */, 100 /* TTL */, &label));
    AtPwMplsPsnOuterLabelAdd(mplsPsn, AtPwMplsLabelMake(1 /* Label */, 2 /* EXP */, 100 /* TTL */, &label));
    AtPwMplsPsnOuterLabelAdd(mplsPsn, AtPwMplsLabelMake(2 /* Label */, 3 /* EXP */, 200 /* TTL */, &label));
    AtPwMplsPsnExpectedLabelSet(mplsPsn, 1);

    /* Configure MPLS PSN for Pseudowire */
    AtPwPsnSet(pw, (AtPwPsn)mplsPsn);
    
    /* IMPORTANT: the driver will clone PSN for its internal usage, so application
     * must delete this PSN after assigning it to PW to avoid memory leak */
    AtObjectDelete((AtObject)mplsPsn);

    /* Configure Ethernet layer */
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    AtPwEthPortSet(pw, AtModuleEthPortGet(ethModule, 0)); /* Use the first Ethernet port */
    uint8 destMac[] = {0x76, 0xE6, 0xB0, 0xA4, 0x6B, 0xCE};
    AtPwEthHeaderSet(pw, destMac,
                     AtEthVlanTagConstruct(1, 1, 3, &cVlan),
                     AtEthVlanTagConstruct(2, 3, 4, &sVlan));

    /* Bind circuit */
    AtPwCircuitBind(pw, (AtChannel)de1);

    /* Activate this Pseudowire */
    AtChannelEnable((AtChannel)pw, cAtTrue);

### Configuring Packet Switch Network (PSN)

![Pseudowire PSN](images/class_diagram_pw_psn.svg)

The above class diagram describes what PSNs that Pseudowire module can support and how PSN is constructed. `AtPwPsn` is to abstract all of PSN types that Pseudowire can work with. Every `AtPwPsn` may have a `lowerPsn`, for example, UDP/IPv4 is constructed by `AtPwUdpPsn` concrete object and its `lowerPsn` set to an `AtPwIpV4Psn` concrete one.

Application can configure any `AtPwPsn` object to Pseudowire by calling `AtPwPsnSet`. Pseudowire does not care how an `AtPwPsn` object is constructed (its `lowerPsn`) and just simply works with the closest PSN assigned to. For example, UDP/IPv4, although `AtPwUdpPsn` has a `AtPwIpV4Psn` lower PSN but Pseudowire just cares `AtPwUdpPsn`.

To create a concrete `AtPwPsn`, the following APIs are used:

|API               |Purpose                 |
|-------------------------------------------|
|`AtPwMplsPsnNew`  |Create a new MPLS PSN   |
|`AtPwMefPsnNew`   |Create a new MEF PSN    |
|`AtPwUdpPsnNew`   |Create a new UDP PSN    |
|`AtPwIpV4PsnNew`  |Create a new IPv4 PSN   |
|`AtPwIpV6PsnNew`  |Create a new IPv6 PSN   |

After creating a PSN, application can set it to Pseudowire by calling `AtPwPsnSet()` API. The assigned PSN object can be retrieved by `AtPwPsnGet`.

>**IMPORTANT** : after creating a new PSN, application can configure and assign it to Pseudowire, the SDK always clone this object for internal usage, so application should delete this new PSN after assigning. This is to avoid memory leak. The [above sample code][Creating a Pseudowire] already mentions about this. By the way, if application creates an object, it must be responsible for deleting it.

After a PSN is assigned to Pseudowire, application may want to change PSN header, to do so, it must retrieve the assigned PSN object, call PSN APIs to configure header and then assign it back to Pseudowire. See the following codes for more detail:
    
    /* The following lines will not cause any change in hardware */
    AtPwMefPsn mef = (AtPwMefPsn)AtPwPsnGet(pw);
    AtPwMefPsnTxEcIdSet(mef, 0x2);
    AtPwMefPsnExpectedEcIdSet(mef, 0x2);

    /* And these new changes will only be applied to hardware when this PSN is assigned back to Pseudowire */
    AtPwPsnSet(pw, (AtPwPsn)mef);

Every concrete PSN has an expected label and application must configure this attribute for PW lookup. For every incomming PSN packets, hardware extracts corresponding label field and then looks for PW that these packet target to. For example, when MEF packets come, hardware extracts ECID field, use its value to look for PW that has MEF expected ECID matches this value. If PW is found, packets are then put to PW jitter buffer for processing. If no PW is found, these packets are dropped.

### Configuring Ethernet layer

Application must specify what Ethernet port that PW should send and receive. Source MAC, Destination MAC along with VLANs should also be configured. The following APIs are used to control Ethernet layer.

|API                                   |Purpose                                            |
|------------------------------------------------------------------------------------------|
|`AtPwEthPortSet/AtPwEthPortGet`       |SET/GET Ethernet port that PW send/receive packets |
|`AtPwEthDestMacSet/AtPwEthDestMacGet` |SET/GET Destination MAC                            |
|`AtPwEthSrcMacSet/AtPwEthSrcMacGet`   |SET/GET Source MAC                                 |
|`AtPwEthVlanSet`                      |Configure C-VLAN, S-VLAN                           |
|`AtPwEthCVlanGet`                     |Get C-VLAN                                         |
|`AtPwEthSVlanGet`                     |Get S-VLAN                                         |

At receive direction, hardware first checks for valid Ethernet frames and discard invalid frames. For applications that use PSN for PW lookup, MACs, VLANs are cut and PSN header is then used to detemine what PW packets should belong to. But for some applications that C-VLAN/S-VLAN instead of PSN for lookup, expected VLANs should be configured by the APIs `AtPwEthExpectedCVlanSet/AtPwEthExpectedSVlanSet`. Using VLANs lookup requires specification that describe hows VLANs sub fields are used to lookup and just some of current products support this feature.

### Controling payload and jitter buffer

The CE-bound TDM IWF has a jitter buffer where the payload of the received TDM packets is stored prior to play-out to the local TDM attachment circuit.  The size of this buffer can be locally configurable to allow accommodation to the PSN-specific packet delay variation. 

Once the PW has been set up, the CE-bound IWF begins to receive TDM packets and to store their payload in the jitter buffer but continues to play out the "all ones" pattern to its TDM attachment circuit. This intermediate state persists until a preconfigured amount of TDM data (usually half of the jitter buffer) has been received in consecutive TDM packets, this is called Jitter Delay in SDK. Application can also configure this jitter delay and it must be less than jitter buffer size.

Application can specify jitter buffer parameters (buffer size, delay) in time unit (microseconds) or packet unit, see the table below:

|API                                                                |Purpose                                       |
|------------------------------------------------------------------------------------------------------------------|
|`AtPwJitterBufferSizeSet/AtPwJitterBufferSizeGet`                  |SET/GET jitter buffer size in microseconds    |
|`AtPwJitterBufferDelaySet/AtPwJitterBufferDelayGet`                |SET/GET jitter buffer delay in microseconds   |
|`AtPwJitterBufferSizeInPacketSet/AtPwJitterBufferSizeInPacketGet`  |SET/GET jitter buffer size in packets         |
|`AtPwJitterBufferDelayInPacketSet/AtPwJitterBufferDelayInPacketGet`|SET/GET jitter buffer delay in packets        |
|`AtPwMinJitterBufferSize/AtPwMaxJitterBufferSize`                  |Get min/max jitter buffer size in microseconds|

Application can configure Pseudowire payload size and each Pseudowire type has different payload size unit as the table below:

|PW type        |Payload size unit           |
|--------------------------------------------|
|SAToP          |Bytes                       |
|CESoP          |Number of frames per packet |
|CEP            |Bytes                       |
|CEP fractional |Number of rows in 9-rows SPE|

The following APIs are used to control payload size.

|API                                                      |Purpose                     |
|--------------------------------------------------------------------------------------|
|`AtPwPayloadSizeSet/AtPwPayloadSizeGet`                  |SET/GET payload size        |
|`AtPwMinPayloadSize/AtPwMaxPayloadSize`                  |Get min/max payload size    |

To implement jitter buffer, hardware may use internal limitted resource and a Pseudowire may have a maximum number of bytes for it's jitter buffer, so changing payload size will also change capacity of this buffer. APIs that SET jitter buffer and payload will check this constrain and return error for out-of-range values.

Before applying a new jitter buffer or payload size, application can check if the new value is in range by calling Min/Max APIs. And to apply a valid combination of payload size and jitter buffer parameters, the APIs `AtPwJitterBufferAndPayloadSizeSet/AtPwJitterBufferInPacketAndPayloadSizeSet` can be used.

And application can also query jitter buffer status such as number of packets are currently in buffer and whether a buffer is overrun/underrun. See the table below:

|API                                           |Purpose                                |
|--------------------------------------------------------------------------------------|
|`AtPwNumCurrentPacketsInJitterBuffer`         |Number of packets are in buffer        |
|`AtPwNumCurrentAdditionalBytesInJitterBuffer` |Number of additional bytes in buffer   |
|`AtChannelAlarmGet`<br>`AtChannelAlarmInterruptGet`<br>`AtChannelAlarmInterruptClear` |Get PW current alarm status and history status.<br>See `eAtPwAlarmType` for jitter buffer related bit fields|

### Configure ACR/DCR timing

For TDM channels that are attached to Pseudowire, Adaptive Clock Recovery (ACR) and differential clock recovery (DCR) are supported. The `AtChannelTimingSet()` is used to configure one of this two timing modes. This API requires two parameters and they are explained by the following table:

|Param                |Type              |Value                                        |
|--------------------------------------------------------------------------------------|
|`timingMode`         |`eAtTimingMode`   |`cAtTimingModeAcr` - ACR timing<br>`cAtTimingModeDcr` - DCR timing||
|`timingSource`       |`AtChannel`       |Pseudowire that ACR/DCR engine should work on|

If a TDM circuit is already bound to Pseudowire, the `timingSource` channel is ignored and ACR/DCR engine will work with this bound Pseudowire.

For CESoP service, one DS1/E1 may have more than one NxDS0 groups and these groups are bound to different CESoP Pseudowires, to specify ACR/DCR timing mode for DS1/E1, one of these CESoP Pseudowires should be selected and specified as `timingSource`.

>**NOTE**: If a DS1/E1 or SDH VC has not been bound to any Pseudowire, calling `AtChannelTimingSet()` with `cAtTimingModeAcr` or `cAtTimingModeDcr` is not allowed and error code will be returned.

When DCR is used, these things are required:

* PRC clock source, applications can change this source by using `AtModulePwDcrClockSourceSet()` but it is not recommended. Applications should also let hardware know what frequency of this PRC is by calling `AtModulePwDcrClockFrequencySet`.
* RTP should be enabled by using the API `AtPwRtpEnable()` and timestamp frequency should be also configured by `AtModulePwRtpTimestampFrequencySet()` API to match with the remote side.

### Access Pseudowire performance statistic and alarm

There are many types of Pseudowire and not all of them have the same counter set. So, all counters are modeled to class. By the way, almost pseudowires do not support accessing one counter at a time, and all of counters are retrieved at one time for instead. The APIs `AtChannelAllCountersGet()/AtChannelAllCountersClear()` are also used. See the sample code below to access counters of SAToP Pseudowire created by the above example code.
    
    #include "AtPwCounters.h"  /* Additional include to have counter class */
    
    /* Get all counters */
    AtPwCounters counter; /* Variable to hold Pseudowire counter object */
    AtChannelAllCountersGet((AtChannel)pw, &counter);

    /* Access common counters */
    printf("TxPackets            : %d\n", AtPwCountersTxPacketsGet(counter));
    printf("TxPayloadBytes       : %d\n", AtPwCountersTxPayloadBytesGet(counter));
    printf("RxPackets            : %d\n", AtPwCountersRxPacketsGet(counter));
    printf("RxPayloadBytes       : %d\n", AtPwCountersRxPayloadBytesGet(counter));
    printf("RxDiscardedPackets   : %d\n", AtPwCountersRxDiscardedPacketsGet(counter));
    printf("RxMalformedPackets   : %d\n", AtPwCountersRxMalformedPacketsGet(counter));
    printf("RxReorderedPackets   : %d\n", AtPwCountersRxReorderedPacketsGet(counter));
    printf("RxLostPackets        : %d\n", AtPwCountersRxLostPacketsGet(counter));
    printf("RxOutOfSeqDropPackets: %d\n", AtPwCountersRxOutOfSeqDropPacketsGet(counter));
    printf("RxOamPackets         : %d\n", AtPwCountersRxOamPacketsGet(counter));

    /* Access TDM PW counter */
    AtPwTdmCounters tdmPwCounter = (AtPwTdmCounters)counter;
    printf("TxLbitPackets        : %d\n", AtPwTdmCountersTxLbitPacketsGet(tdmPwCounter));
    printf("TxRbitPackets        : %d\n", AtPwTdmCountersTxRbitPacketsGet(tdmPwCounter));
    printf("RxLbitPackets        : %d\n", AtPwTdmCountersRxLbitPacketsGet(tdmPwCounter));
    printf("RxRbitPackets        : %d\n", AtPwTdmCountersRxRbitPacketsGet(tdmPwCounter));
    printf("RxJitBufOverrun      : %d\n", AtPwTdmCountersRxJitBufOverrunGet(tdmPwCounter));
    printf("RxJitBufUnderrun     : %d\n", AtPwTdmCountersRxJitBufUnderrunGet(tdmPwCounter));
    printf("RxLops               : %d\n", AtPwTdmCountersRxLopsGet(tdmPwCounter));
    printf("RxPacketsSentToTdm   : %d\n", AtPwTdmCountersRxPacketsSentToTdmGet(tdmPwCounter));

## ATM/IMA programming guide [atm_ima]

### ATM

![ATM class diagram](images/class_diagram_atm.svg) 

The `AtModuleAtm` manages a set of virtual connections which are responsible for encapsulating packets traffic to ATM cells to be transmitted to ATM Transmission Convergence - `AtAtmTc`. Due to lack of resources, hardware only supports a number of virtual connections identified by a unique flat number. A virtual connection is created with specified VPI/VPI and API `AtModuleAtmVirtualConnectionCreate` is used for this purpose. In case of no free resource, that function returns NULL.

A virtual connection is constructed as the figure below:

![ATM Virtual Connection](images/atm_virtual_connection.svg)

`AtAtmVirtualConnection` encapsulates packets from `AtEthFlow`. It then binds to an `AtAtmTc` - Transmission Convergence. An `AtAtmTc` can bind to any physical channel which can be `AtSdhChannel` or `AtPdhChannel`. The sample code below setup a flow between Ethernet and TDM that uses ATM virtual connection

    #include "AtDriver.h"

    #include "AtModuleEth.h"
    #include "AtEthFlow.h"
    
    #include "AtModuleEncap.h"
    #include "AtModuleAtm.h"
    #include "AtModuleSdh.h"
    
    uint16 vpi = 8, vci = 35;
    tAtEthVlanDesc vlanDescriptor;
    tAtEthVlanTag sVlan, cVlan;
    uint8 destMac[6] = {0x76, 0xE6, 0xB0, 0xA4, 0x6B, 0xCE};

    /* Get related modules */
    AtDevice device = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), null)[0];
    AtModuleAtm atmModule = (AtModuleAtm)AtDeviceModuleGet(device, cAtModuleAtm);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    AtModuleEncap encModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    /* Create a virtual connection with specified VPI and VCI */
    uint32 virtualConnectionId = AtModuleAtmFreeVirtualConnectionGet(atmModule);
    AtAtmVirtualConnection virtualConnection = AtModuleAtmVirtualConnectionCreate(atmModule, virtualConnectionId, vpi, vci);
    AtAtmVirtualConnectionAalTypeSet(virtualConnection, cAtAtmAal5TypeSnap);

    /* Create one Ethernet flow */
    AtEthFlow ethFlow = AtModuleEthNopFlowCreate(ethModule, AtModuleEthFreeFlowIdGet(ethModule));
    AtEthFlowVlanDescConstruct(0 /* Ethernet port */, 
                               AtEthVlanTagConstruct(1 /* Priority */, 1 /* CFI */, 100 /* VLAN ID */, &sVlan),
                               AtEthVlanTagConstruct(2 /* Priority */, 2 /* CFI */, 101 /* VLAN ID */, &cVlan),
                               &vlanDescriptor);
    AtEthFlowIngressVlanAdd(ethFlow, &vlanDescriptor);
    AtEthFlowEgressVlanSet(ethFlow, &vlanDescriptor);
    AtEthFlowEgressDestMacSet(ethFlow, destMac);

    /* Create a ATM TC, assume that we need to use VC-12 as physical layer, ask
     * Encapsulation module to create an ATM TC, then bind it to VC-12 */
    AtAtmTc atmTc = AtModuleEncapAtmTcCreate(encModule, AtModuleEncapFreeChannelGet(encModule));
    AtAtmTcNetworkInterfaceTypeSet(atmTc, cAtAtmNetworkInterfaceTypeUni);
    AtAtmTcCellMappingModeSet(atmTc, cAtAtmCellMappingModeDirectly);

    /* Get a VC-12 as physical interface */
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, 0); /* First line */
    AtSdhVc vc12 = AtSdhLineVc1xGet(line, 0, 0, 0, 0); /* Use the first VC-12 */
    AtEncapChannelPhyBind((AtEncapChannel)atmTc, (AtChannel)vc12);

    /* Now, connect Ethernet side and TDM side */
    AtAtmVirtualConnectionEthFlowSet(virtualConnection, ethFlow);
    AtAtmVirtualConnectionBind(virtualConnection, (AtChannel)atmTc);

    /* Activate this virtual connection */
    AtChannelEnable((AtChannel)virtualConnection, cAtTrue);
    
`AtModuleAtm` also supports a number of cross-connect engines to cross ATM cells among ATM Transmission Convergence, ATM Pseudowire and IMA group. A cross-connect engine needs to be created before using, factory method `AtModuleAtmCrossConnectCreate` is used for this. To make this engine work, a source and destination ports must be setup. Although source and destination ports are of `AtChannel`, but currently, only object of `AtAtmTc`, `AtImaGroup` or `AtPwAtm` can be assigned.

By the way, after creating, cross-connect engine is setup with default configuration that disable policing and shaping. Service category is also configured to a correct one. The figure below shows how a cross-connect engine work with the outside.

![ATM Cross-Connect](images/atm_cross_connect.svg)

Refer Pseudowire module for ATM Pseudowire and the below section for IMA group. The sample code below cross-connect cells between `AtPwAtm` and `AtAtmTc`
    
    #include "AtDriver.h"
    
    #include "AtModuleAtm.h"
    #include "AtModuleEncap.h"
    #include "AtModuleSdh.h"
    #include "AtModulePw.h"
    
    /* Get related modules */
    AtDevice device = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), null)[0];
    AtModuleAtm atmModule = (AtModuleAtm)AtDeviceModuleGet(device, cAtModuleAtm);
    AtModuleEncap encModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    /* Create a ATM TC, assume that we need to use VC-12 as physical layer, ask
     * Encapsulation module to create an ATM TC, then bind it to VC-12 */
    AtAtmTc atmTc = AtModuleEncapAtmTcCreate(encModule, AtModuleEncapFreeChannelGet(encModule));
    AtAtmTcNetworkInterfaceTypeSet(atmTc, cAtAtmNetworkInterfaceTypeUni);
    AtAtmTcCellMappingModeSet(atmTc, cAtAtmCellMappingModeDirectly);

    /* Get a VC-12 as physical interface */
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, 0); /* First line */
    AtSdhVc vc12 = AtSdhLineVc1xGet(line, 0, 0, 0, 0); /* Use the first VC-12 */
    AtEncapChannelPhyBind((AtEncapChannel)atmTc, (AtChannel)vc12);

    /* Create ATM Pseudowire. */
    AtPwAtm atmPw = AtModulePwAtmCreate(pwModule, 0, cAtAtmPwTypeVcc1To1);

    /* Refer Pseudowire programming guide to setup PSN and Ethernet for this
     * Pseudowire */
    /* ... */

    /* Create cross-connect. In this example, cross-connect is used to cross cells by both VPI/VCI */
    /* Cross cells from ATM PW to ATM TC */
    uint32 xcId = AtModuleAtmFreeCrossConnectGet(atmModule);
    AtAtmCrossConnect atmXc = AtModuleAtmCrossConnectCreate(atmModule, xcId);
    AtAtmCrossConnectTypeSet(atmXc, cAtAtmCrossConnectTypeVcc);

    /* Configure source channel, its VPI/VCI */
    AtAtmCrossConnectSourcePortSet(atmXc, (AtChannel)atmPw);
    AtAtmCrossConnectSourceVpiSet(atmXc, 8);
    AtAtmCrossConnectSourceVciSet(atmXc, 35);

    /* Configure dest channel, its VPI/VCI */
    AtAtmCrossConnectDestPortSet(atmXc, (AtChannel)atmTc);
    AtAtmCrossConnectDestVpiSet(atmXc, 8);
    AtAtmCrossConnectDestVciSet(atmXc, 36); /* Change VCI at destination */

    /* Activate this cross-connect */
    AtChannelEnable((AtChannel)atmXc, cAtTrue);

    /* Cross cells from ATM TC to ATM PW */
    xcId = AtModuleAtmFreeCrossConnectGet(atmModule);
    atmXc = AtModuleAtmCrossConnectCreate(atmModule, xcId);
    AtAtmCrossConnectTypeSet(atmXc, cAtAtmCrossConnectTypeVcc);

    /* Configure source channel, its VPI/VCI */
    AtAtmCrossConnectSourcePortSet(atmXc, (AtChannel)atmTc);
    AtAtmCrossConnectSourceVpiSet(atmXc, 8);
    AtAtmCrossConnectSourceVciSet(atmXc, 37);

    /* Configure dest channel, its VPI/VCI */
    AtAtmCrossConnectDestPortSet(atmXc, (AtChannel)atmPw);
    AtAtmCrossConnectDestVpiSet(atmXc, 8);
    AtAtmCrossConnectDestVciSet(atmXc, 38); /* Change VCI at destination */

    /* Activate this cross-connect */
    AtChannelEnable((AtChannel)atmXc, cAtTrue);

### IMA

![IMA class diagram](images/class_diagram_ima.svg)

This module manages a set of IMA groups and links, all of them are identified by a unique number. `AtModuleImaGroupCreate` is for creating new IMA group while `AtModuleImaLinkCreate` is for creating new IMA link. 

IMA group always works with cross-connect to carry cells from other cell streams from ATM Pseudowire, ATM TC or another IMA group. And a number of IMA links must be added to group in order to bring it up. Its cell stream will be activated when number of active links is greater than a configured threshold. This threshold is configured as 1 as default when a group is created and application can change by accessing `txMinNumLink`and `rxMinNumLink` attributes of that group. Cell stream can be stopped by `Inhibit()` method. Cell stream are not inhibited as default.

For IMA link, TX and RX directions can bind to different ATM TC - `AtAtmTc` and its cell stream can be disabled / enabled by `Inhibit()` method. Cell stream is not inhibited as default. Note, two `AtAtmTc` of two directions must be bound to physical interfaces that have the same type (E1, DS1, VC-1x, VC-3, VC-4, VC-4-nc)

The following sample code will create one IMA group with three links. These three links are bound to VC-12s.
    
    #include "AtDriver.h"

    #include "AtModuleIma.h"
    #include "AtModuleSdh.h"
    #include "AtModuleEncap.h"
    
    uint16 groupId = 0, linkId;

    /* Get related modules */
    AtDevice device = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), null)[0];
    AtModuleIma imaModule = (AtModuleIma)AtDeviceModuleGet(device, cAtModuleIma);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtModuleEncap encapModule = (AtModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);

    /* Create a group */
    AtImaGroup imaGroup = AtModuleImaGroupCreate(imaModule, groupId);

    /* Create 3 links and add to this group. */
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, 0); /* Use VC-12 of line 0 */
    for (linkId = 0; linkId < 3; linkId++)
        {
        /* Create ATM TC for IMA link, also bind its physical interface */
        AtAtmTc atmTc = AtModuleEncapAtmTcCreate(encapModule, AtModuleEncapFreeChannelGet(encapModule));
        AtSdhVc vc12 = AtSdhLineVc1xGet(line, 0, 0, 0, linkId);
        AtEncapChannelPhyBind((AtEncapChannel)atmTc, (AtChannel)vc12);

        /* Create link and bind to this TC */
        AtImaLink link = AtModuleImaLinkCreate(imaModule, linkId);
        AtImaLinkTxBind(link, atmTc);
        AtImaLinkRxBind(link, atmTc);

        /* Add it to group */
        AtImaGroupLinkAdd(imaGroup, link, link);
        }

    /* This IMA group can work via ATM cross-connect with ATM PW, other IMA
     * groups. Refer ATM programming guide for cross-connect operation */
    
As mentioned in ATM section, cells of IMA group can be crossed to other IMA groups, ATM PW or ATM TC. 

## RAM management [ram]

![RAM class diagram](images/class_diagram_ram.svg)

The RAM module of one device can manage a set of DDRs (`AtDdr`) or ZBTs (`AtZbt`). Number of DDRs can be retrieved by `AtModuleRamNumDdrGet()` and each DDR can be accessed via `AtModuleRamDdrGet()`. Controlling ZBTs are similar as DDRs, `AtModuleRamNumZbtGet()` and `AtModuleRamZbtGet()` are used for instead. 

DDR and ZBT are both RAM but different in hardware characteristic and may be different in the way that software control them. So two abstract classes `AtDdr` and `AtZbt` are defined to make the design be extend-able. They all inherit from `AtRam` abstract class to share the same RAM interface.

One AT product can support two methods to test a RAM, hardware auto-testing and software testing. But application may not need to care about this. It just simply calls `AtRamMemoryTest` and see the test result. For each concrete product, there may be a note whether it uses hardware of software testing method.

When hardware testing method is used, the `AtRamMemoryTest` request hardware start testing, then wait for N seconds (N is 30 as default). During this time, this function will check if there is any failing point and exit the routine with error code. Application can control this checking time (N) by using `AtRamTestDurationSet()/AtRamTestDurationGet()`. Note, to change N `AtRamTestDurationSet()` must be called before calling `AtRamMemoryTest()`.

For software testing, hardware interface two methods `AtRamRead` and `AtRamWrite` to let application access local registers of a RAM. Application can use these two APIs to implement memory testing by its own algorithm. By the way, this SDK has a built-in software algorithm to test RAM.

Software base testing may take some hours to finish testing. To reduce this time, hybric method is used. With this method, hardware supports some ways to write or check a chunk of memory. Rules for writting and checking are input by software. When hardware finishes its job, an indication is notified to software. Software then check wether the process is success or fail. With this method, full RAM testing just take somes minutes. All of these things are encapsulated by the `AtRamMemoryTest`, application just calls and sees how fast it is.

By default, all of products support software testing, some of them support hardware testing and some of them support hybrid testing. Refer [Product notes] [productNote] for more detail.

## Packet analyzer [packetAnalyzer]

![Packet analyzer class diagram](images/class_diagram_packet_analyzer.svg)

`AtModulePktAnalyzer` module is responsible for analyzing packets transmitted, received on one channel. This module is designed for debugging purpose, so only one analyzer can support at a time. By default, this module has one default analyzer at device is initialized. And normally, this default analyzer is used to analyzer packets on Ethernet port.

For PPP/MLPPP related products, packets on Ethernet Flow or PPP Link can be captured and analyzed, but not all products can support this feature. To have these analyzers, `AtModulePktAnalyzerEthFlowPktAnalyzerCreate()`, `AtModulePktAnalyzerPppLinkPktAnalyzerCreate()` are used. For products that do not support these analyzers, NULL object is returned.

>**NOTE**: There is only one packet analyzer can be created, so when one creating API is called, the previous analyzer is internally deleted. Application must not call `AtObjectDelete` to delete analyzer, let `AtModulePktAnalyzer` fully manage.

The following code will show packets on Ethernet port:

    /* Get default analyzer - which is Ethernet port packet analyzer */
    AtModulePktAnalyzer pktAnalyzerModule = AtDeviceModuleGet(device, cAtModulePktAnalyzer);
    AtPktAnalyzer analyzer = AtModulePktAnalyzerCurrentAnalyzerGet(pktAnalyzerModule);
    
    /* Analyzer RX/TX packets */
    AtPktAnalyzerAnalyzeRxPackets(analyzer);
    AtPktAnalyzerAnalyzeTxPackets(analyzer);

In class diagram, there is `AtPacket` class and its sub classes, all of them are internally used by `AtPktAnalyzer` and application does not need to care.

## Iterator [iterator]

Iterator is supported from SDK version 1.0.4 to iterate all of objects that one object manages. For example, a device manage a set of modules, so it support a module iterator to iterate all of modules. The table below summary all classes and iterator that they support

|Class           | Iterator creating API                | Iteratee         |
|--------------------------------------------------------------------------|
|`AtHdlcBundle`  | `AtHdlcBundleLinkIteratorCreate`     | `AtHdlcLink`     |
|`AtModuleEncap` | `AtModuleEncapChannelIteratorCreate` | `AtEncapChannel` |
|`AtModuleEth`   | `AtModuleEthFlowIteratorCreate`      | `AtEthFlow`      |
|                | `AtModuleEthPortIteratorCreate`      | `AtEthPort`      |
|`AtFrLink`      | `AtFrLinkFlowIteratorCreate`         | `AtEthFlow`      |
|`AtMfrBundle`   | `AtMfrBundleFlowIteratorCreate`      | `AtEthFlow`      |
|`AtModuleFr`    | `AtModuleFrMfrBundleIteratorCreate`  | `AtMfrBundle`    |
|`AtDriver`      | `AtDriverDeviceIteratorCreate`       | `AtDevice`       |
|`AtDevice`      | `AtDeviceModuleIteratorCreate`       | `AtModule`       |      
|`AtDevice`      | `AtDeviceCoreIteratorCreate`         | `AtIpCore`       |
|`AtPdhDe1`      | `AtPdhDe1nxDs0IteratorCreate`        | `AtPdhNxDs0`     |
|`AtModulePpp`   | `AtModulePppMpBundleIteratorCreate`  | `AtMpBundle`     |
|`AtMpBundle`    | `AtMpBundleFlowIteratorCreate`       | `AtEthFlow`      |
|`AtModulePw`    | `AtModulePwIteratorCreate`           | `AtPw`           |

>**NOTE**: Application calls iterator creating APIs to create iterators so it must delete iterator after using. The driver will not delete created iterators even when it is deleted by `AtDriverDelete` API.

The following sample code iterates all links in bundle.

    void IterateLinksInBundle(AtMpBundle bundle)
        {
        AtIterator linkIterator = AtHdlcBundleLinkIteratorCreate((AtHdlcBundle)bundle);
        AtPppLink pppLink;
    
        AtPrintc(cSevInfo, "Number of links: %d\r\n", AtIteratorCount(linkIterator));
        while ((pppLink = (AtPppLink)AtIteratorNext(linkIterator)) != NULL)
            {
            /* Handle this link object */
            AtPrintc("Link %d\r\n", AtChannelIdGet((AtChannel)pppLink));
    
            /* Or can get current object */
            assert(AtIteratorCurrent(linkIterator) == pppLink);
            }
    
        /* Delete iterator when done */
        AtObjectDelete((AtObject)linkIterator);
        }
        

## Interrupt processing [interrupt]

When there is any event happen on a channel, an event can be alarm change status or OAM come, AT Chip-set can notify CPU via interrupt PIN. To process interrupt, application should call `AtDeviceInterruptProcess()` function.
Application can call that function periodically in a polling task, and task speed should be fast enough to so that critical event can be handled soon. 

The API `AtDeviceInterruptProcess` will scan internal interrupt tree for fast locating channels that interrupts happen. When there is an event on a channel, all of event listeners registered by `AtChannelEventListenerAdd()` will be notified via callback functions defined in corresponding listener interface of each channel type (for example, `tAtChannelEventListener` interface).

The follow sample code will add application event listener for SDH Line to receive Line events.

    /* Application callback function to receive Line alarms interrupt. For demo
     * purpose, this function just displays what alarms are changed */
    static void LineAlarmChangeState(AtChannel channel, uint32 alarms, uint32 currentStatus)
        {
        /* Get line that alarm change status */
        uint8 lineId = AtChannelIdGet(channel);
    
        /* In this context, we can safely cast this channel to AtSdhLine, like this:
         * AtSdhLine sdhLine = (AtSdhLine)channel;
         */
    
        printf("Interrupt is changed on line %d\n", lineId);
        if (alarms & cAtSdhLineAlarmLos)
            printf("    LOS: %s\n", (currentStatus & cAtSdhLineAlarmLos) ? "Raise" : "Clear");
        if (alarms & cAtSdhLineAlarmLof)
            printf("    LOF: %s\n", (currentStatus & cAtSdhLineAlarmLof) ? "Raise" : "Clear");
    
        /* So on .... */
        }
        
    /* Event listener is added as following */
    /* Line event listener */
    static tAtChannelEventListener m_lineListener =
            {.AlarmChangeState = LineAlarmChangeState, .OamReceived = null};

    /* Listen alarm on line */
    uint8 lineId = 0;
    AtDevice device = AtDriverAddedDevicesGet(AtDriverSharedDriverGet(), null)[0];
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    AtSdhLine line = AtModuleSdhLineGet(sdhModule, lineId);
    AtChannelEventListenerAdd((AtChannel)line, &m_lineListener);

## How to compile and start the SDK [startSdk]

### Compile the SDK [compile]

The SDK contains some makefiles to compile the whole SDK:

* `atsdk/Rules.Make`: this file contains common compile flags, common dependency rules to generate SDK library (no CLI). This file can be included to any Makefile to generate SDK library object file. It includes these targets
    * `atsdk`: to have `atsdk.o`. This object file must be linked to application
    * `cleanatsdk`: to clean all of objects output by `atsdk` target, including `atsdk.o`
* `atsdk/cli/Rules.Make`: this file contains dependencies to compile AT CLI library. This file can be included to any Makefile to generate SDK CLI library object file. It includes these targets
    * `atcli`: to have `atcli.o` and `debugutil/debugutil.o`. These object files can be linked to application to have AT CLI environment.
    * `cleanatcli`: to clean all of objects output by `atcli` target, including `atcli.o` and `debugutil/debugutil.o`

* `atsdk/Makefile`: This file just includes all of dependency files `atsdk/Rules.Make` and `atsdk/cli/Rules.Make` so the `make` command can be executed. Outputs of this process are SDK Library object files
    * `atsdk.o`: AT SDK library with no built-in CLI
    * `atcli.o, debugutil/debugutil.o`: AT CLI library
    * No executable generated by this process

* `apps/sample_app/Makefile`: this makefile is to compile sample application `sampleapp` which can run on real Evaluation Platform and Simulation Platform. Its output is `sampleapp` application

>**NOTE**: for development environment that uses different tool chain, a compiler prefix need to be explicitly specified on `make` command. For example, if this SDK is compiled to run on MIPS architecture, `make COMPILE_PREFIX=mips-` is used. By the way, additional flags may be added by inputing the ADDITIONAL_FLAGS. For example: `make COMPILE_PREFIX=mips- ADDITIONAL_FLAGS="-fpic -pipe -G 0 -mno-branch-likely -ffunction-sections -fdata-sections -fomit-frame-pointer -fno-builtin -ffreestanding -mips64r2 -march=xlp -mabi=64"`

Example, see what objects are generated by the following commands. Standing at the top folder.

|Build command     |Output                              |
|-------------------------------------------------------|
|`make atsdk`      |`atsdk.o`                           |
|`make cleanatsdk` |`atsdk.o` is removed                |
|`make atcli`      |`atcli.o`                           | 
|`make cleanatcli` |`atcli.o` is removed                |
|`make all`        |`atsdk.o` and `atcli.o`             |
|`make clean`      |`atsdk.o` and `atcli.o` are removed |

If standing at `apps/sample_app`, the `make all` command will generate `sampleapp` executable file.

### How to integrate and start CLI library on application [cli]

![Text-UI class diagram](images/class_diagram_text_ui.svg)

The `AtTinyTextUI` and `AtDefaultTinyTextUI` are concrete TextUIs, they extend from the most abstract class `AtTextUI` and can be used by application. The different between them is that `AtTinyTextUI` does not support prompt or running a script file while `AtDefaultTinyTextUI` can. For platform that does not support file system, `AtTinyTextUI` is used. For system that supports file system, `AtDefaultTinyTextUI` is recommended.

Because they are all tiny to be portable among different OS platforms, many features that a text UI should have are cut off, such as:

* Auto complete
* History
* Online manual

The following APIs are used to create concrete `AtTextUI`. When a new `AtTextUI` is created, all of built-in commands will be installed.

|API name                  |Purpose                       |
|---------------------------------------------------------|
|`AtTinyTextUINew`         |Create a Tiny TextUI          |
|`AtDefaultTinyTextUINew`  |Create a default Tiny Text UI |

Basic APIs that applications may want to work with AtTextUI object.

|API name                  |Purpose                       |
|---------------------------------------------------------|
|`AtTextUIStart`         |Setup environment and start the prompt to receive users CLIs|
|`AtTextUIDelete`        |Delete `AtTextUI` instance to free up memory resources|
|`AtTextUICmdProcess`    |Execute a command. It is used when application does not want to use `AtTextUIStart` to start the prompt and use its own way to execute AT CLIs.|
|`AtTextUIScriptRun`    |To run a script file that contains AT CLIs. This API will not work on `AtTinyTextUI` since it does not support file system.|

And there is a CLI module on top of these classes, this module supports a set of APIs to start up concrete text UI easily. 

|API name              |Purpose                         |
|-------------------------------------------------------|
|`AtCliStart`          |Start default Text UI - `AtTinyTextUI`|
|`AtCliStartWithTextUI`|Start with specified Text UI object|
|`AtCliSharedTextUI`   |Get the current Text UI|
|`AtCliStop`           |Stop the current Text UI|
|`AtCliExecute`        |Execute a CLI string|

It is recommended to use `AtCliStartWithTextUI()` to explicitly sepecify what concrete `AtTextUI` to be used. The `AtCliStart()` always use `AtTinyTextUI` as default. When `AtCliStart/AtCliStartWithTextUI` are called, CLI environment is setup and `AtCliExecute` can be called to execute CLIs. 

>**NOTE**: When `AtCliExecute` is called, if CLI environment has not been setup, it will automatically setup this environment.

For some applications that do not need to start the CLI prompt and use their own prompt to receive AT CLIs, use only the `AtCliExecute()` is enough. But when applications exit, the `AtCliStop` should be called to free up memory resources.

The SDK is released with a set of CLIs, application can implement and install additional CLIs that follow Arrive CLI style to this module dynamically by using the API `AtTextUICmdAdd()`.

After [compiling] [compile], `atcli.o` needs to be linked to application. And the below sample code is to start AT CLI with prompt. It also installs user defined command:
    
    #include "atclib.h"
    #include "AtCliModule.h"

    /*
     * Application specific command (for demo purpose).
     * Example: hello name Arrive Technologies
     *          + hello name: command name which has two levels
     *          + Arrive: value of parameter 1
     *          + Technologies: value of parameter 2
     *          + Result on console: Hello Arrive Technologies
     */
    static eBool CmdHelloWith2Params(char argc, char **argv)
        {
        AtPrintc(cSevInfo, "Hello %s %s\r\n", argv[0], argv[1]);
        return cAtTrue;
        }

    int TestAtCli()
        {
        AtTextUI textUI = NULL;

        /* Need to setup OSAL if it has not been */
        AtOsalSharedSet(AtOsalLinux());

        /* Create default tiny text UI, add additional commands and then start it */
        textUI = AtDefaultTinyTextUINew();
        AtTinyTextUICmdAdd(textUI, "hello name", CmdHelloWith2Params, 2, 2);

        /* Start a prompt where user can input command */
        AtCliStartWithTextUI(textUI); 

        /* When user types "exit", the prompt will be gone and following line is
         * executed to delete this object */
        AtTextUIDelete(textUI);

        return 0;
        }

And the following example code does not start the prompt but still can run CLI

    int TestAtCli()
        {
        AtTextUI textUI = NULL;

        /* Need to setup OSAL if it has not been */
        AtOsalSharedSet(AtOsalLinux());

        /* Create default tiny text UI, add additional commands and then start it */
        textUI = AtTinyTextUINew();
        AtTinyTextUICmdAdd(textUI, "hello name", CmdHelloWith2Params, 2, 2);

        /* Prompt will not appear since Tiny Text UI is used */
        AtCliStartWithTextUI(textUI);

        /* Try executing some CLIs */
        AtCliExecute("hello name Arrive Technologies");
        AtCliExecute("show driver");

        /* Clean up */
        AtTextUIDelete(textUI);

        return 0;
        }

If application does not need to install any additional commands, it would be more simple like the following sample code:

    int TestAtCli()
        {
        /* Need to setup OSAL if it has not been */
        AtOsalSharedSet(AtOsalLinux());

        /* Just execute any CLI, the environment will be automatically setup */
        AtCliExecute("show driver");
        AtCliExecute("show device modules");

        /* And this API should be called to free memory resource */
        AtCliStop();

        return 0;
        }

See "CLI manual" document for all of built-in CLIs.

### Start the sample application [sampleapp]

The `sampleapp` can run on real platform or in simulation platform (the host). Its source code `main.c` should be modified to interact correctly with real platform, refer TODO tags in that file for more detail. And for the sake of simplicity, this sample just create driver with one device. Developer may change the code to support different devices by one driver.

There are two variables used to determine whether this app runs in real or simulation mode, `m_isSimulate` and `m_simulateProductCode`. `m_isSimulate` must be set to `cAtTrue` and `m_simulateProductCode` must be set to product code of device want to simulate.

>**NOTE**: By default, almost devices use `AtHalSim` HAL (created by AtHalSimNew()) for simulation, but there are some products use another simulation HAL. Refer [Product notes] [productNote] for more detail.

### Explore all AT CLIs [cli]

After starting `sampleapp` application, the first screen is:

    AT SDK >
    
* Run `listcmd all` to show all of CLIs.
* Run `listcmd <module> <subType1> <subType2> ...` to show all of CLIs relates with `<subType1> <subType2> ...` of `<module>`. For example:
** `listcmd sdh`: show all of CLIs of SDH Module
** `listcmd sdh line`: show all of CLIs of SDH Line
** `listcmd sdh line serdes`: show all of CLIs to control SERDES of SDH Line

See "CLI manual" for full commmand details.

The following simple CLIs configure create one Ethernet flow and bind it to HDLC PPP link.
    
    # Create PPP/HDLC channel and bind with DS1/E1
    pdh de1 framing 1 ds1_sf
    encap channel create 1 ppp
    encap channel bind 1 de1.1
    
    # Create Ethernet traffic flow
    eth flow create 1 nop
    eth flow ingress vlan add 1 1 1.1.123
    eth flow egress vlan 1 1 1.1.123
    eth flow egress destmac 1 c0.ca.c0.ca.c0.ca
    
    # Bind this flow to HDLC PPP link and enable traffic
    encap hdlc link flowbind 1 1
    encap hdlc link traffic rxenable 1
    encap hdlc link traffic txenable 1

And the following CLIs are to create CEP PW

    # Configure SDH
    sdh line rate 1 stm1
    sdh map "aug1.1.1 vc4"
    
    # Create and configure PW
    pw create cep 1
    pw circuit bind 1 vc4.1.1
    
    # Configure Ethernet
    eth port srcmac 1 C0.CA.C0.CA.C0.CA
    pw ethport 1 1
    pw ethheader 1 C0.CA.C0.CA.C0.CA 1.0.1 none
    
    # Configure PSN. Use MPLS in this case
    pw psn 1 mpls
    pw mpls innerlabel 1 1.1.1
    pw mpls outerlabel add  1 1.1.1
    pw mpls outerlabel add  1 2.2.2
    pw mpls expectedlabel 1 1
    
    # Enable PW
    pw enable 1
    
    # Timing
    sdh path timing vc4.1.1 epar xxx

The following CLIs are to create SAToP PW

    # Configure E1 circuit
    pdh de1 framing 1 e1_unframed
    
    # Create and configure PW
    pw create satop 1
    pw circuit bind 1 de1.1
    
    # Configure Ethernet
    eth port srcmac 1 C0.CA.C0.CA.C0.CA
    pw ethport 1 1
    pw ethheader 1 C0.CA.C0.CA.C0.CA 1.0.1 none
    
    # Configure PSN. Use MPLS in this case
    pw psn 1 mpls
    pw mpls innerlabel 1 1.1.1
    pw mpls outerlabel add  1 1.1.1
    pw mpls outerlabel add  1 2.2.2
    pw mpls expectedlabel 1 1
    
    # Enable PW
    pw enable 1
    
    # Timing
    pdh de1 timing 1 acr xxx
        
The following CLIs are to create CESoP PW

    # Create NxDS0 circuit, use timeslot 1-3
    pdh de1 framing 1 e1_basic
    pdh de1 nxds0create 1 1-3
    
    # Create and configure PW
    pw create cesop 1 basic
    pw circuit bind 1 nxds0.1.1-3
    
    # Configure Ethernet
    eth port srcmac 1 C0.CA.C0.CA.C0.CA
    pw ethport 1 1
    pw ethheader 1 C0.CA.C0.CA.C0.CA 1.0.1 none
    
    # Configure PSN. Use MPLS in this case
    pw psn 1 mpls
    pw mpls innerlabel 1 1.1.1
    pw mpls outerlabel add  1 1.1.1
    pw mpls outerlabel add  1 2.2.2
    pw mpls expectedlabel 1 1
    
    # Enable PW
    pw enable 1
    
    # Timing. Note, in case of CESoP, ACR/DCR timing is configured per E1, and 
    # one PW of its NxDS0 is selected
    pdh de1 timing 1 acr 1

## Product notes [productNote]
### 60031031 and 60031021

* Simulation HAL: `AtHalSimGlobalHold` must be used instead of the defautl `AtHalSim`.
* RAM testing method: hybrid 

---

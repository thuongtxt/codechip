1. spi hal r <addr>
2. spi hal w <addr> <value>
3. spi hal slot <slot> <page>
4. spi hal times <times>

NOTE:
-- <addr>, <value>, <page> must be hex with the prefix '0x'
-- <slot>, <times> should be decimal

for example:
-- spi hal r 0x12345678
-- spi hal w 0x12345678 0x55aa55aa
-- spi hal slot 4 0x11
-- spi hal times 100

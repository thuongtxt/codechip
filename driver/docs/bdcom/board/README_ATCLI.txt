1. spi at add <slot> <page>
2. spi at clean
3. spi at cli <string>

NOTE:
-- <slot> should be decimal
-- <page> must be hex with the prefix '0x'
-- <string> should be a command string, if including more than one word, should be with ""

for example:
-- spi at add 1 0x10
-- spi at clean
-- spi at cli xxx
-- spi at cli "xxx yyy zzz"



A) Register description
1) Control #1
- address: 0xF0
- indrctrl_reg[7:0]
2) Control #2
- address: 0xF1
- indrctrl_reg[15:8]
2) Control #3
- address: 0xF2
- indrctrl_reg[23:16]
2) Control #4
- address: 0xF3
- indrctrl_reg[31:16]
3) Data #1
- address: 0xF4
- indrdata_reg[7:0]
4) Data #2
- address: 0xF5
- indrdata_reg[15:8]
3) Data #3
- address: 0xF6
- indrdata_reg[23:16]
4) Data #4
- address: 0xF7
- indrdata_reg[31:24]

wire                oeupce_     = ~indrctrl_reg[31];//CPU set to 1 to request read/write; HW will set to 0 when read/write done. CPU need poll this register to know Read/Write Process done
wire                oeuprnw     = indrctrl_reg[30]; //CPU set to 1 for Read and set to 0 for write
wire [23:0]         oeupa       = indrctrl_reg[23:0]; //CPU set to Access Address
wire [31:0]         oeupdi      = indrdata_reg[31:0];//CPU set to Write Data for Write Process and get to Read Data for Read Process


B) Read cycle
1) write to control register, 
- write to control#1 : bit[7:0] = addr24bit[7:0]
- write to control#2 : bit[7:0] = addr24bit[15:8]
- write to control#3 : bit[7:0] = addr24bit[23:16]
- write to control#4 : bit[7] = 1, bit[6] = 1, bit[5:0] = xx(don't use)
 2) Read control #4 register, if bit[7] is clear to 0, read done
3) Read data register to get data
- read data#1 : bit[7:0] = data32bit[7:0]
- read data#2 : bit[7:0] = data32bit[15:8]
- read data#3 : bit[7:0] = data32bit[23:16]
- read data#4 : bit[7:0] = data32bit[31:24]
C) Write cycle
1) Write to data register
- write data#1 : bit[7:0] = data32bit[7:0]
- write data#2 : bit[7:0] = data32bit[15:8]
- write data#3 : bit[7:0] = data32bit[23:16]
- write data#4 : bit[7:0] = data32bit[31:24]
2) write to control register, write to control#1 first.
- write to control#1 : bit[7:0] = addr24bit[7:0]
- write to control#2 : bit[7:0] = addr24bit[15:8]
- write to control#3 : bit[7:0] = addr24bit[23:16]
- write to control#4 : bit[7] = 1, bit[6] = 0, bit[5:0] = xx(don't use)
3) Read control #4 register, if bit[7] is clear to 0, write done

================================================================================

void bsp_SpiWrite(unsigned int Addr, unsigned char Data)
unsigned char bsp_SpiRead(unsigned int Addr)

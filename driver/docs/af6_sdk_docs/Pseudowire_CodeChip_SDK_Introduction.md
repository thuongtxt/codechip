## SDK Features

<div class="two_col_container">
  <div class="two_col_containee">

#### SDK Structure

![][img_sdk_sw_structure]

  </div>
  <div class="two_col_containee" >

#### Highlights

* Platform independent via a Hardware Abstraction Layer (HAL) module
* Operating System independent via Operating System Abstraction Layer (OSAL)
  module
* Modular device driver allowing transparent interface to higher level
  application layer
* Compliant to telecommunications specifications by various standard bodies:
  IEEE, MEF, ITU-T, IETF
* Supports interrupt handler for time-sensitive events
* Compliant to ANSI-C
* Includes a command line for debugging during product development phase
* Comprehensive documentation and guidelines

  </div>
</div>

## Device Driver Overview

<div class="two_col_container">
  <div class="two_col_containee">

#### OSAL - Designed for OS PORTABILITY

* Provides necessary functions that are used by SDK includes: Memory Management,
  Semaphore/Mutex, Timer
* POSIX-compliant interface
* Effortless porting to Linux, VxWorks or AMX

#### HAL - Designed for PLATFORM PORTABILITY

* Uniformed hardware access (register read/write) for all version of 
  Arrive CodeChip™ devices of the same family
* Support RAM-based simulation

#### Command Line Interface (CLI)

* Rich set of text-based command line to configure, monitor, or diagnose hardware device.
* Portable and easy to use.

  </div>
  <div class="two_col_containee">

#### Low-Level API - Designed for PERFORMANCE

* Fine-grained API to accommodate complex yet feature-rich register set offered
  by underlying CodeChip™ device
* Optimized for performance. i.e. direct/indirect access, hardware read/write
  cycle, CPU workload

#### High-Level API - Designed for FLEXIBILITY

* Consistent interface across all Arrive CodeChip™ products
* Separates users from the complex low-level register read/write sequence. 
* Aligns with telecom specifications (IETF, IEEE, BELLCORE, ITU-T, ATM Forum,
  etc.)
* Object-oriented design (e.g., modular and loosely-coupling) implemented in C
  (ANSI-C compliant)
* Accommodated by extensive API Programming Guide and sample code

  </div>
</div>

---

## SDK release package

* SDK source code.
* SDK Programing Guide with example code.
* API Manual.
* CLI Manual.
* Release Note.

## Software development support

* Provide online or on-site support during development/verification phase.
* Assist customer in integrating SDK customer's software.
* Diagnose and troubleshoot SDK's and Codechip's issues.

## About Arrive

Arrive is a broadband semiconductor solutions company with a broad portfolio of
highly integrated systems-on-a-chip products combining voice, data, Internet and
multimedia content for worldwide telecommunications companies.

Our CodeChip™ replaces inflexible fixed-silicon ASICs with a programmable
carrier-class FPGA solution.  It includes a SoC FPGA Image with a full software
development kit, including APIs and Drivers, and is backed by the integration
and testing experience of Arrive.

For more information on our CodeChip™ product, go to:
[www.arrivetechnologies.com](http://www.arrivetechnologies.com)

<!-- End of document -->

[img_sdk_sw_structure]: images/sdk_structure.png

## SDK Architecture

<div class="two_col_container">
  <div class="two_col_containee">

#### SDK Architecture

![][img_sdk_sw_structure]

  </div>
  <div class="two_col_containee" >

#### Portability

* SDK is platform independent via a Hardware Abstraction Layer (HAL) and OS Abstraction Layer (OSAL)
* OSAL: 
    - Provides necessary functions that are used by SDK includes: Memory Management,
   Semaphore/Mutex, Timer
    - POSIX-compliant interface.
    - Available for Linux, VxWorks or AMX.
* HAL:
    - Uniformed hardware access (register read/write) for all version of 
   Arrive CodeChip™ devices of the same family.
    - Support RAM-based simulation.

  </div>
</div>

## Porting task

* Due to SDK interfaces with OS and Hardware device through OSAL and HAL, so task of porting is just re-implemention of these libraries.
* OSAL porting: 
    - SDK provides available libraries for Linux, VxWorks and AMX.
    - OSAL library uses POSIX interfaces, if your OS which is not Linux/VxWorks/AMX but supports POSIX, OSAL library of Linux can be reused entirely as a base code.
    - Otherwise, Arrive will help to implement specific library for your OS.
    - OSAL interfaces are described in API Description document.
* HAL porting: 
    - Customer need to implement read/write functions to access FPGA device. 
    - Based on those functions, Arrive will help to implement specific HAL library.

## Compile SDK

* Compile with SDK's makefiles: < step and command to compile >


* Compile with IDE: < list include paths >


* Output object files: < list object files to be used >


## Run Arrive's CLI


<!-- End of document -->

[img_sdk_sw_structure]: images/sdk_structure.png

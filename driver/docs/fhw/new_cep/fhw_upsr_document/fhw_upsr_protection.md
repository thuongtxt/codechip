
# UPSR engine provision [upsr_engine_provision]

For 20G XC at low order path and suppose support only VT2, CodeChip need to support 4032 UPSR engines as following:

* 4032 registers to configure look up from working VT flat ID to UPSR engine ID
* 4032 registers to configure look up from protection VT flat ID to UPSR engine ID

>**NOTE**: Flat VT ID has range from 0 to 8063

![UPSR look up tables](images/upsr_lookup.svg)

With the resource of 4032 UPSR engines, application can provision and deprovision for each couble of working and protection VT. The UPSR engine resource can be managed by application. Example for UPSR engine provision by Arrive API/CLI as following:

Draft API:

* AtUpsrEngineProvision(uint32 upsrId, uint32 workingVtId, uint32 protectVtId)
* AtUpsrEngineDeprovision(uint32 upsrId)

Draft CLI:

* upsr engine provision "upsrId" "workingVt" "protectVt"
* upsr engine deprovision "upsrId"

# UPSR alarm monitoring [upsr_alarm_mon]

About UPSR alarm monitoring, this product requires some alarm tables that work independently with current defect tree provided by the Arrive Codechip. These tables are controlled via Arrive SDK APIs.

* One table of 126 registers (32bits) provides SF alarm status for working VT
* One table of 126 registers (32bits) provides SF alarm status for protection VT
* One table of 126 registers (32bits) provides SD alarm status for working VT
* One table of 126 registers (32bits) provides SD alarm status for protection VT
* One table of 126 registers (32bits) provide capacity to switch between working VT and protection VT

SF alarm is ORed by the following alarms: 

* AIS, LOP, PLM, BER SF

SD alarm is: 

* BER SD

![UPSR alarm process](images/upsr_alarm.svg)

Each bit in registers of alarm table delegates for alarm state of one UPSR engine.
Each bit in registers of switch mask table delegates for switching between working VT and protection VT. The flat index of each bit in table is the UPSR engine ID.

Arrive SDK supports some APIs to:

* Process alarm depend on above alarm tables
* Switching between working and protection VT

Draft public APIs as following:

* void AtUpsrAlarmProcess(void);
* void AtUpsrDelegateInstall(void (*DeligateFunction)());

Application should have a periodic polling task to call `AtUpsrAlarmProcess` and install delegate function to `AtUpsrDelegateInstall`.

Processing alarm in Arrive API should do as following pseudo code:

```
typedef enum eUpsrEvent
    {
    cUpsrEventClear = 0,
    cUpsrEventSf = cBit0,
    cUpsrEventSd = cBit1
    }eUpsrEvent;

void AtUpsrAlarmProcess(void)
    {
    /* Scan all UPSR engines from alarm tables */
    for each UPSR engine
        workingChannel    = LookupWorkingChannel();
        protectionChannel = LookupProtectionChannel();
        
        /* Assume working change from SF -> SD
         * protection change from SD -> Clear */
        workingChangedEvents = cUpsrEventSf | cUpsrEventSd
        workingNewEvents = cUpsrEventSd;
        protectionChangedEvents = cUpsrEventClear | cUpsrEventSd
        workingNewEvents = cUpsrEventClear;

        eBool shouldSwitch = AtUpsrDelegateShouldSwitch(upsrEngine, workingChannel, protectionChannel, workingChangedEvents, workingNewEvents, protectionChangedEvents, protectionNewEvents);
        if shouldSwitch:
            build switch action mask
            
    /* Apply action mask now */

    }
```



When **AtUpsrDelegateShouldSwitch** is called, the delegate function will have responsibility to decide whether need to switch between working and protection VT and return a boolean for this decision.


With this proposal, the number of reading register for polling all VT channels is 504 times (126 * 4). The maximum number of writing register for switching all VT channels is 126 times.
### CEP configuration (1.9.3.2) 

There are some thresholds defined in section 1.9.3.2:

* Buffer overrun threshold (range 1-300) 
* Buffer underrun threshold (range 1-300) 
* Remote packets loss (RDI) threshold (range 1-300) 
* Loss of packet synchronization (LOPS) threshold (range 1-300) 
* Packet loss state threshold (range 1-300)

As required, there must be APIs for application to configured these thresholds but what are their purposes?

### CEP Defects and Alarms (1.9.5.2)

* Missing Packets above pre-defined configurable threshold (let's call MP): shall be declared as a loss of a preconfigured number of consecutive CEP packets in one second time window. To implement this, the following things are necessary.
    * New API to configure number of consecutive CEP packets in one second time window. Let's call it MP threshold.
    * Need to add this new defect type to SDK
    * Hardware needs to support defect and alarm declarations
* Loss of packet synchronization (LOPS): set/clear thresholds are already supported. Defect declaration is also supported by hardware.
* Buffer overrun: defect declaration is supported. But what about "The system shall use one second time window to calculate the number of packets dropped due to buffer overrun", whether this is done by application layer or device?
* Buffer underrun: defect declaration is supported. But what about "The system shall use one second time window to calculate the number of times a packet needed to be played out and the jitter buffer did not have enough data", whether this is done by application layer or device?
* Remote packet loss (RDI): defect declaration is supported. But what about "The system shall use one second time window to calculate the number of detected packets with R bit set.", whether this is done by application layer or device?

As common requirement, alarm is declared as following rules:

* Raise: when defect condition persists for 2.5s
* Clear: when the defect condition has disappeared for 10s.

Current support status:

* Hardware does not support alarm declaration but defect declaration.
* SDK does not fully support retrieving defects and alarms. The API `AtChannelAlarmGet()` does not do exactly as its name, it internally gets current defect status.

So there may be some updates:

* Hardware support alarm declaration as above common requirement.
* SDK need to support APIs to get current defect status (let's call it AtChannelDefectGet) and correct implementation of `AtChannelAlarmGet()`

### Performance monitoring

New APIs to get performance monitoring counters need be supported:

* NE-ES-CEP
* NE-SES-CEP
* NE-UAS-CEP
* FE-ES-CEP
* FE-SES-CEP.

New APIs to configure SES thresholds:

* NE-SES threshold: the number of Missing Packets above pre-defined configurable threshold in a one-second interval
* FE-SES threshold: the number of received RDI indication in a one-second interval 

The device shall support the PM counter structure depicted in the following figure, in order to interoperate seamlessly with external ALU devices devoted to PM processing:
* Is this feature for hardware interworking?
* Do new APIs need be supported to retrieve value of these registers?



									TEST GUIDELINE


//-----------------------------------------------------------------------------
I. Login to Command shell
	- Telnet to Boad
	- Run Command shell: ./thashell

//-----------------------------------------------------------------------------
II. Common setup
  - Download FPGA: fpgaload AF6FHW0011_EP6C1V2_2_07_0030_SYS19.rbf
  - Configure LIU for E1 mode: bp tha_initial_e1.script
  - Configure LIU for DS1 mode: bp tha_initial_ds1.script
  - Run script file to setup datapath.
  - Clear all history of errors, that are caused in setup time, by using command "show and clear" counters, alarm interrupts.
		Refer to section "5. Check status and counter" below.
	Or run a script file to clear all history: bp clear_history.script
  - Check status and counter to know whether datapath is good or not.

III. Script files
	- Command to run script file: bp <Script file name>
	
	- Script file structure includes:
		- Initialize interface. Example: initialize E1: tha cfg intfinit 1-16 e1 1 used
		
		- Configure physical interfaces:
			+ Configure TDM interface:
				Example: Configure port#1 is E1 unframed and use ACR clock recovered from PW#1
							tha cfg tdmintf liu 1 e1unfrm acr 1
						 Configure port#1 is E1 PCM31 (no CRC), use system clock
							tha cfg tdmintf liu 1 e1bsc 1-31 dis sys
						 Configure port#1 is DS1 SF and use ACR clock recovered from PW#1
							tha cfg tdmintf liu  1   ds1sf 	1-24  dis   acr     1
			+ Configure GE interface:
				Example: Configure GE port 1 has configurations: 
						Source MAC is C0.CA.C0.CA.C0.CA
						Source IPv4 is 172.33.34.35
						GMII
						Speed: 1000 Mb
						Full duplex
						Tx IPG (inter-packet gap): 15 byte
						Rx IPG: 12 bytes
						PHP: disable
					
						tha cfg geintf 1 C0.CA.C0.CA.C0.CA ipv4 172.33.34.35 17 gmii 1000m full_duplex 15 12 dis
		
		- Add Pseudowire service:
				Example: Add Pseudowire#1 has configurations:
						Type: CESoP
						PSN protocol: MEF
						Destination MAC address: C0.CA.C0.CA.C0.CA
						VLAN: 1 tag
						ECID (PW label): 1
						RTP: disable
						Re-orderring: enable
						Sequence numbering mode: wrap zero (count start from 0)
						Packet replacing mode if lost packet: AIS
						LOPS set threshold: 4 packet
						LOPS clear threshold: 4 packet
						Number of E1 frame was packed in PW packets: 8 frame
						Jitter buffer size: 64000 micro seconds. NOTE: step is 125 micro second
						Jitter buffer delay: 32000 micro seconds. NOTE: step is 125 micro second
						Timeslot map: LIU#1, timeslot 1-2
						
				tha add pseudowire 1 cesbsc mef C0.CA.C0.CA.C0.CA 1tag 16 5 16 1 dis en dis wrapzero ais 4 4 8 64000 32000 liu 1 1-2

//-----------------------------------------------------------------------------
1. E1 Script files
       1.1 SAToP Testcases: TDM --> E1 SAToP --> MEF/MPLS/IPv4/IPv6 --> GbE
        - Config 1.1: 16 SAToP PW, E1 Un-frame, Sys Clock
                      * Script name: at_test01-1_satop_e1_sys.txt
                      * Sys Clock mode
                      * E1 Un-frame
                      * MEF
                      * Payload length = 256 bytes
                      * {Jitter buffer,Jitter delay} = {16000,8000}
                      * 1 vlan tag

        - Config 1.2: 16 SAToP PW, E1-Unframe, Looptime Clock
                      * Script name: at_test01-2_satop_e1_loop.txt
                      * Looptime Clock mode
                      * E1 Un-frame
                      * MEF
                      * Payload length = 256 bytes
                      * {Jitter buffer,Jitter delay} = {16000,8000}
                      * 1 vlan tag

        - Config 1.3: 16 SAToP PW, E1-Unframe, ACR Clock
                      * Script name: at_test01-3_satop_e1_acr.txt
                      * ACR Clock mode
                      * E1 Un-frame
                      * MEF
                      * Payload length = 256 bytes
                      * {Jitter buffer,Jitter delay} = {16000,8000}
                      * 1 vlan tag

        - Config 1.4: 16 SAToP PW, E1-Unframe, DCR Clock 
                      * Script name: at_test01-4_satop_e1_dcr_xxx.txt
                      * DCR Clock mode
					  * To change frequency of external reference clock (ie. 10000KHz): tha cfg dcrclksrc prcrefclk 10000
					  * To change frequency to generate RTP timestamp (ie. 2048 Khz): tha cfg dcrrtpfreq 2048
					  * To show DCR status: tha show dcrstat 1
                      * E1 Un-frame
                      * MEF
                      * Payload length = 256 bytes
                      * {Jitter buffer,Jitter delay} = {16000,8000}
                      * 1 vlan tag

       1.2 CESoP Testcases: TDM --> E1 CESoP --> MEF/MPLS/IPv4/IPv6 --> GbE
        - Config 3.1: 128 CESoP PW, E1 Basic PCM31, Sys Clock
                      * Script name: at_test03-1_cesopsn_e1_ccs31.txt
                      * Sys Clock mode
                      * E1 Basic PCM31
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 3.2: 128 CESoP PW, E1CRC PCM31, Sys Clock
                      * Script name: at_test03-2_cesopsn_e1_ccs31crc.txt
                      * Sys Clock mode
                      * E1 CRC PCM31
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 3.3: 128 CESoP PW, E1CRC PCM30, Sys Clock
                      * Script name: at_test03-3_cesopsn_e1_ccs30crc.txt
                      * Sys Clock mode
                      * E1 CRC PCM30
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 3.4: 128 CESoP PW, E1 Basic PCM30, Looptime Clock
                      * Script name: at_test03-4_cesopsn_e1_ccs30_loop.txt
                      * Looptime Clock mode
                      * E1 Basic PCM30
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 3.5: 2 CESoP PW, 1 LIU, E1 Basic PCM31, Looptime Clock
                      * Script name: at_test03-5_cesopsn_e1_ccs31_2pw_1liu.txt
                      * Looptime Clock mode
                      * E1 Basic PCM31
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 3.6: 128 CESoP PW, E1 Basic PCM31, Looptime Clock
                      * Script name: at_test03-6_cesopsn_e1_ccs31_loop.txt
                      * Looptime Clock mode
                      * E1 Basic PCM31
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 3.7: 128 CESoP PW, E1 CRC PCM31, Looptime Clock
                      * Script name: at_test03-7_cesopsn_e1_ccs31crc_loop.txt
                      * Looptime Clock mode
                      * E1 CRC PCM31
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 3.8: 128 CESoP PW, E1 CRC PCM30, Looptime Clock
                      * Script name: at_test03-8_cesopsn_e1_ccs30crc_loop.txt
                      * Looptime Clock mode
                      * E1 CRC PCM30
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag


       1.3 Mixed SAToP-CESoP Testcases: TDM --> E1 SAToP/CESoP --> MEF/MPLS/IPv4/IPv6 --> GbE
        - Config 5.1: 14 SAToP PW, 32 CESoP PW -> 1TimeSlot/1PW , Looptime Clock
                      * Script name: at_test05-1_mix_satces_e1_crc_pcm31_loop.txt
                      * Looptime Clock mode
                      * CEsOP Mode: E1 CRC PCM31
                      * SAToP Mode: E1 Un-frame
                      * MEF
                      * CESoP Payload length = 8 frames
                      * SAToP Payload length = 256 bytes
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

//-----------------------------------------------------------------------------
2. DS1 Script files
       2.1 SAToP Testcases: TDM --> DS1 SAToP --> MEF/MPLS/IPv4/IPv6 --> GbE
        - Config 2.1: 16 SAToP PW, DS1 Un-frame, Sys Clock
                      * Script name: at_test02-1_satop_ds1_sys.txt
                      * Sys Clock mode
                      * DS1 Un-frame
                      * MEF
                      * Payload length = 192 bytes
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 2.2: 16 SAToP PW, DS1-Unframe, Looptime Clock
                      * Script name: at_test02-2_satop_ds1_loop.txt
                      * Looptime Clock mode
                      * DS1 Un-frame
                      * MEF
                      * Payload length = 192 bytes
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 2.3: 16 SAToP PW, DS1-Unframe, ACR Clock
                      * Script name: at_test02-3_satop_ds1_acr.txt
                      * Looptime Clock mode
                      * DS1 Un-frame
                      * MEF
                      * Payload length = 192 bytes
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 2.4: 16 SAToP PW, DS1-Unframe,DCR Clock
                      * Script name: at_test02-4_satop_ds1_dcr_xxx.txt
                      * DCR Clock mode
					  * To change frequency of external reference clock (ie. 10000KHz): tha cfg dcrclksrc prcrefclk 10000
					  * To change frequency to generate RTP timestamp (ie. 2048 Khz): tha cfg dcrrtpfreq 2048
					  * To show DCR status: tha show acrstat 1
                      * DS1 Un-frame
                      * MEF
                      * Payload length = 192 bytes
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

       2.2 CESoP Testcases: TDM --> DS1 CESoP --> MEF/MPLS/IPv4/IPv6 --> GbE
        - Config 4.1: 128 CESoP PW, DS1 SF, Looptime Clock
                      * Script name: at_test04-1_cesopsn_ds1_sf_loop.txt
                      * Looptime Clock mode
                      * DS1 SF
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 4.2: 128 CESoP PW, DS1 ESF, Looptime Clock
                      * Script name: at_test04-2_cesopsn_ds1_esf_loop.txt
                      * Looptime Clock mode
                      * DS1 ESF
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 4.3: 1 CESoP PW, 1 LIU, DS1 SF, Looptime Clock
                      * Script name: at_test04-3_cesopsn_ds1_sf_1pw.txt
                      * Looptime Clock mode
                      * DS1 SF
                      * 24 Timeslot --> 1 PW
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 4.4: 1 CESoP PW, 1 LIU, DS1 ESF, Looptime Clock
                      * Script name: at_test04-4_cesopsn_ds1_esf_loop_1pw.txt
                      * Looptime Clock mode
                      * DS1 ESF
                      * 24 Timeslot --> 1 PW
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 4.5: 128 CESoP PW, DS1 ESF, Sys Clock
                      * Script name: at_test04-5_cesopsn_ds1_esf.txt
                      * Sys Clock mode
                      * DS1 ESF
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 4.6: 128 CESoP PW, DS1 SF, Sys Clock
                      * Script name: at_test04-6_cesopsn_ds1_sf.txt
                      * Sys Clock mode
                      * DS1 SF
                      * MEF
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

        - Config 4.7: 16 CESoP PW, 5Timeslot-->1PW, DS1 SF, ACR Clock, UDP/IPv4
                      * Script name: at_test04-7_cesopsn_ds1_sf_acr_16pw_5ts.txt
                      * ACR Clock mode
                      * DS1 SF
                      * UDP/IPv4
                      * Payload length = 8 frames
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * no vlan tag

       2.3 Mixed SAToP-CESoP Testcase: TDM --> DS1 SAToPCESoP --> MEF/MPLS/IPv4/IPv6 --> GbE
        - Config 6.1: 14 SAToP PW, 32 CESoP PW, Looptime Clock
                      * Script name: at_test06-1_mix_satces_ds1_sf_loop.txt
                      * Looptime Clock mode
                      * CEsOP Mode: DS1 SF
                      * SAToP Mode: DS1 Un-frame
                      * MEF
                      * CESoP Payload length = 8 frames
                      * SAToP Payload length = 192 bytes
                      * {Jitter buffer,Jitter delay} = {64000,32000}
                      * 1 vlan tag

IV. Command guide					  
//-----------------------------------------------------------------------------
3. Command usage:
	There are two methods to input the command:
		- Input the command with suggestion (RECOMMENDED): this is easiest way to use command.
			NOTE: After input command, a full command is printed to screen, 
				  to save this command to use later, just copy full command to a script file.
		
		- Input the complete command.
	
	NOTE: For more command guide, refer to document Thalassa_AF6013_SDK_Cli_User_Manual_Rev01.00.pdf
		
//-----------------------------------------------------------------------------
5. Check status and counter
  * NOTE 1: There are two modes to show counters/interrupts: 
        + Show and clear. 
			Example: Show and clear counter: tha show pseudowirecnt 1-16 en
			         Show and clear alarm interrupt: tha show pseudowireintr 1-16 en
        + Show and not clear. 
			Example: Show and not clear counter: tha show pseudowirecnt 1-16 dis
			         Show and not clear alarm interrupt: tha show pseudowireintr 1-16 dis
  
  * NOTE 2: An interrupt is an event to report the change of current status from Raised to Cleared or Cleared to Raised
  
  5.1 Check TDM status and counter
  - Check current TDM status: tha show tdmstatus liu 1-16
    + Expected result: No error
			
  - Check TDM alarm interrupt: tha show tdmintr liu 1-16 en
    + Expected result: No error

  - Check TDM counter: tha show tdmcnt liu 1-16 en
    + Expected result: Ignore BPVEXZ counter (because this counter is in FPGA test mode), remaining counters are 0.

  5.2 Check Pseudo-wire status and counter:
  - Check counters of Pseudo-wire #1-16:
                tha show pseudowirecnt 1-16 en
    + Expected result: 
                TxPackets          :   Count
                TxPayloadBytes     :   Count
                RxPackets          :   Count
                RxPayloadBytes     :   Count
                Remaining counters :   0
              
  - Check alarm interrupt of Pseudo-wire #1-16:
                tha show pseudowireintr 1-16 en
    + Expected result:
                LOFS               :   Clear
                OVERRUN            :   Clear
                UNDERRUN           :   Clear

  - Check current status of Pseudo-wire #1-16:
                tha show pseudowirestatus 1-16 en
    + Expected result:
                LOFS               :   Clear
                OVERRUN            :   Clear
                UNDERRUN           :   Clear

  5.3 Check Ethernet status and counter:
  - Check Ethernet counter:
                tha show gecnt 1 en	
    + Expected result: No error counter, normal counters must be counted
                
  - Check current Ethernet status:
                tha show gestatus 1
    + Expected result: No error
	
  5.4 Show ACR/DCR state:
                tha show acrstat 1-16
				tha show dcrstat 1-16
  
  5.5 Show Jitter buffer status:
                tha show jitterbufstat 1-16

//-----------------------------------------------------------------------------
6. Show configuration
  6.1 Show configuration of TDM #1-16:
                tha show tdmintf liu 1-16	
  
  6.2 Show configuration of Pseudo-wire#1-16:
                tha show pseudowire 1-16
  
  6.3 Show configuration of Ethernet port:
                tha show geintf 1
				
//-----------------------------------------------------------------------------
7. Modify configuration:
  7.1 Modify TDM configuration:
                tha modify tdmintf liu                  
				
		-> Select what configuration you want to modify, and input new value
				
  7.2 Modify Pseudowire configuration:
                tha modify pseudowire

		-> Select what configuration you want to modify, and input new value

  7.3 Modify Ethernet port configuration:
                tha modify geintf

		-> Select what configuration you want to modify, and input new value
		
		NOTE: About PHP (Penultimate Hop Popping) mode: Incase no MLPS outer label, PHP must be enabled. 
		                                                Incase outer label exist, PHP must be disabled. 

	NOTE: After you complete input command, a full command is printed to screen, 
		to save this command to use later, just copy full command to a script file.
														
//-----------------------------------------------------------------------------
8. Loop-back:
  8.1 TDM interface loopback:
      - Line loop out:
				tha cfg tdmloopback liu 1-16 lineloopout
      - Line loop in:
				tha cfg tdmloopback liu 1-16 lineloopin
      - No loop:
				tha cfg tdmloopback liu 1-16 noloop
		
  8.2 Ethernet port loopback:
      - Loop out:
				tha cfg geloopback 1 loopout
      - Loop in:
				tha cfg geloopback 1 loopin
      - No loop:
				tha cfg geloopback 1 noloop
			  
//-----------------------------------------------------------------------------
9. Add/Remove Pseudowire service:
  9.2 Add Pseudowire:
		tha add pseudowire

		-> and input Pseudowire configurations.
		
	NOTE: After you complete inputting command, a full command is printed to screen, 
		to save this command to use later, just copy full command to a script file.
		
  9.1 Remove Pseudowire:
            tha remove pseudowire 1
            tha remove pseudowire 1-16

//-----------------------------------------------------------------------------
IV. Board bring up:
	- Power on board.
	- Telnet to the board.
	- Start software: ./thalassa -D -p 5678
	- Wait some seconds.
	- Login to Command (CLI) shell: ./thashell
	
//-----------------------------------------------------------------------------
V. Upgrade Software, FPGA and script file to Board using FTP:
	- In Windows, open Windows Explorer
	- Type address in Address bar: ftp://user@<IP address of Board> . Example: ftp://user@172.33.44.113
	- Open directory "user".
	- Copy software, FPGA image, and script file to directory "user".
	- Telnet to Board, and type command: chmod 777 *
	- Start software: ./thalassa -D -p 5678
	- Wait some seconds.
	- Login to Command (CLI) shell: ./thashell
	

function elec_port6_100m()

mcemonDo("cmn test reg write32_nl 1101010 1400")
 
mcemonDo("cmn test reg write32_nl 1101004 0170")
mcemonDo("cmn test reg write32_nl 1101008 0001")
mcemonDo("cmn test reg write32_nl 1101040 0000")
mcemonDo("cmn test reg write32_nl 1101000 1")
 
mcemonDo("cmn test reg write32_nl 1101004 0171")
mcemonDo("cmn test reg write32_nl 1101008 0001")
mcemonDo("cmn test reg write32_nl 1101040 0000")
mcemonDo("cmn test reg write32_nl 1101000 1")
 
mcemonDo("cmn test reg write32_nl 1101004 0172")
mcemonDo("cmn test reg write32_nl 1101008 0001")
mcemonDo("cmn test reg write32_nl 1101040 0000")
mcemonDo("cmn test reg write32_nl 1101000 1")
 
mcemonDo("cmn test reg write32_nl 1101004 0170")
mcemonDo("cmn test reg write32_nl 1101008 0001")
mcemonDo("cmn test reg write32_nl 1101040 0020")
mcemonDo("cmn test reg write32_nl 1101000 1")
 
print("***** select copper PHY (BASE-T) *****")
mcemonDo("cmn test reg write32_nl 1101004 0156")
mcemonDo("cmn test reg write32_nl 1101008 0004")
mcemonDo("cmn test reg write32_nl 1101040 0016")
mcemonDo("cmn test reg write32_nl 1101044 0000")
mcemonDo("cmn test reg write32_nl 1101000 0001")
 
print("***** Enable SGMII mode (without bypass An) *****")
mcemonDo("cmn test reg write32_nl 1101004 0156")
mcemonDo("cmn test reg write32_nl 1101008 0004")
mcemonDo("cmn test reg write32_nl 1101040 801b")
mcemonDo("cmn test reg write32_nl 1101044 0084")
mcemonDo("cmn test reg write32_nl 1101000 0001")
 
 
print("***** select fiber PHY facing MAC (BASE-X) *****")
mcemonDo("cmn test reg write32_nl 1101004 0156")
mcemonDo("cmn test reg write32_nl 1101008 0004")
mcemonDo("cmn test reg write32_nl 1101040 0016")
mcemonDo("cmn test reg write32_nl 1101044 0001")
mcemonDo("cmn test reg write32_nl 1101000 0001")
 
print("***** Enable AN and restart PHY *****")
mcemonDo("cmn test reg write32_nl 1101004 0156")
mcemonDo("cmn test reg write32_nl 1101008 0004")
mcemonDo("cmn test reg write32_nl 1101040 9100")
mcemonDo("cmn test reg write32_nl 1101044 0040")
mcemonDo("cmn test reg write32_nl 1101000 0001")
 
 
print("***** select copper PHY (BASE-T) *****")
mcemonDo("cmn test reg write32_nl 1101004 0156")
mcemonDo("cmn test reg write32_nl 1101008 0004")
mcemonDo("cmn test reg write32_nl 1101040 0016")
mcemonDo("cmn test reg write32_nl 1101044 0000")
mcemonDo("cmn test reg write32_nl 1101000 0001")
 
print("***** Disable AN. set sped to 100M, full duplex and restart PHY *****")
mcemonDo("cmn test reg write32_nl 1101004 0156")
mcemonDo("cmn test reg write32_nl 1101008 0004")
mcemonDo("cmn test reg write32_nl 1101040 a100")
mcemonDo("cmn test reg write32_nl 1101044 0000")
mcemonDo("cmn test reg write32_nl 1101000 0001")

end

function elec_port8_100m()

mcemonDo("cmn test reg write32_nl 1101010 1400")
 
mcemonDo("cmn test reg write32_nl 1101004 0170")
mcemonDo("cmn test reg write32_nl 1101008 0001")
mcemonDo("cmn test reg write32_nl 1101040 0000")
mcemonDo("cmn test reg write32_nl 1101000 1")
 
mcemonDo("cmn test reg write32_nl 1101004 0171")
mcemonDo("cmn test reg write32_nl 1101008 0001")
mcemonDo("cmn test reg write32_nl 1101040 0000")
mcemonDo("cmn test reg write32_nl 1101000 1")
 
mcemonDo("cmn test reg write32_nl 1101004 0172")
mcemonDo("cmn test reg write32_nl 1101008 0001")
mcemonDo("cmn test reg write32_nl 1101040 0000")
mcemonDo("cmn test reg write32_nl 1101000 1")

mcemonDo("cmn test reg write32_nl 1101004 0170")
mcemonDo("cmn test reg write32_nl 1101008 0001")
mcemonDo("cmn test reg write32_nl 1101040 0080")
mcemonDo("cmn test reg write32_nl 1101000 1")


print("***** select copper PHY (BASE-T) *****")
mcemonDo("~ cmn test reg write32_nl 1101004 0156")
mcemonDo("~ cmn test reg write32_nl 1101008 0004")
mcemonDo("~ cmn test reg write32_nl 1101040 0016")
mcemonDo("~ cmn test reg write32_nl 1101044 0000")
mcemonDo("~ cmn test reg write32_nl 1101000 0001")
 
print("***** Enable SGMII mode (without bypass An) *****")
mcemonDo("~ cmn test reg write32_nl 1101004 0156")
mcemonDo("~ cmn test reg write32_nl 1101008 0004")
mcemonDo("~ cmn test reg write32_nl 1101040 801b")
mcemonDo("~ cmn test reg write32_nl 1101044 0084")
mcemonDo("~ cmn test reg write32_nl 1101000 0001")
 
 
print("***** select fiber PHY facing MAC (BASE-X) *****")
mcemonDo("~ cmn test reg write32_nl 1101004 0156")
mcemonDo("~ cmn test reg write32_nl 1101008 0004")
mcemonDo("~ cmn test reg write32_nl 1101040 0016")
mcemonDo("~ cmn test reg write32_nl 1101044 0001")
mcemonDo("~ cmn test reg write32_nl 1101000 0001")
 
print("***** Enable AN and restart PHY *****")
mcemonDo("~ cmn test reg write32_nl 1101004 0156")
mcemonDo("~ cmn test reg write32_nl 1101008 0004")
mcemonDo("~ cmn test reg write32_nl 1101040 9100")
mcemonDo("~ cmn test reg write32_nl 1101044 0040")
mcemonDo("~ cmn test reg write32_nl 1101000 0001")
 
 
print("***** select copper PHY (BASE-T) *****")
mcemonDo("cmn test reg write32_nl 1101004 0156")
mcemonDo("cmn test reg write32_nl 1101008 0004")
mcemonDo("cmn test reg write32_nl 1101040 0016")
mcemonDo("cmn test reg write32_nl 1101044 0000")
mcemonDo("cmn test reg write32_nl 1101000 0001")
 
print("***** Disable AN. set sped to 100M, full duplex and restart PHY *****")
mcemonDo("cmn test reg write32_nl 1101004 0156")
mcemonDo("cmn test reg write32_nl 1101008 0004")
mcemonDo("cmn test reg write32_nl 1101040 a100")
mcemonDo("cmn test reg write32_nl 1101044 0000")
mcemonDo("cmn test reg write32_nl 1101000 0001")

end


elec_port6_100m();
elec_port8_100m();


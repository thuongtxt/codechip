#The following to guide to setup Ciena PDH system to:
- load fpga, 
- configure PDH CEM DIM ports, 
- configure DIM CARD 
- run CEM data path
0) prerequisity:
- the PDH CEM board DIM port#1 connected to DIM board#1 LinkA (left most)
- the PDH CEM board DIM port#2 connected to DIM board#2 LinkA (left most)

1) To load fpga: 
- telnet to the PDH CEM board
- copy pdh_fpga_load.txt to the board at /root/
- copy fpga bin file (example:AF6CNC0011_PDHCEM_1.4.14.000.bin ) to /root/
- the copy the file into 2 different file names, example:
cp AF6CNC0011_PDHCEM_1.4.14.000.bin ARRIVE_LOAD_1
cp AF6CNC0011_PDHCEM_1.4.14.000.bin ARRIVE_LOAD_2
sh pdh_fpga_load.txt

2) configure PDH CEM DIM ports:
- copy marvell_2_5G_phy_startup_added_sleep.sh to the board at /root/
- sh marvell_2_5G_phy_startup_added_sleep.sh

3) configure DIM cards:
- connect the labtop and DIM board#1 by the Xilinx USB blaster. 
- open the "Xilinx SDK 2016.4" from the labtop
- open SXCT Console from the Xilinx SDK
- input the 3 following commands into the SXCT command text box:
connect -url TCP:127.0.0.1:3121
target 2
jtagterminal
- the latest command will pop up a terminal command console, then input the following command into this console:
# Assumes PDH CEM FPGA configured for MAC = 0xABCDABCDABCD for DA and SA
wr 460008 abcd
wr 46000c abcdabcd
wr 460010 abcd
wr 460014 abcdabcd

wr c00520 0
wr c00520 1
wr c00520 0
rd c00520

wr c00560 0
wr c00560 1
wr c00560 0
rd c00560

########################
# Rx Datapath Link Select
########################
# Select Link
wr c00580 0         #<- None
wr c00580 1         #<- Link A
#wr c00580 2         <- Link B
rd c00580

4) run CEM data path:
- telnet to the PDH CEM board to setup CEM device#1, assumed you are staying at your epapp:
./epapp -m /dev/mem:0xfd0000000:0x8000000
run the ds1 scripts, example: run sample_scripts_84_ds1s_satop.txt
- telnet to the PDH CEM board to setup CEM device#2, assumed you are staying at your epapp:
./epapp -m /dev/mem:0xfc0000000:0x8000000
run the ds1 scripts, example: run sample_scripts_84_ds1s_satop.txt


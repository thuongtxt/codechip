#!sh
export PATH=/etc/init.d/drivers:$PATH
rc.pcp_prusik start

sleep 1

#(1) Setup Marvel PHY 
#====================
pcp write 10f2000 80000000
pcp write 10f2800 80000000
sleep 1
pcp write 10f2020 13
pcp write 10f2820 13
sleep 1
#/* take X-unit out of LPM, port 1 */
pcp write 10f2010 3
pcp write 10f2030 2000
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 403
pcp write 10f2030 9140
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
#/* take X-unit out of LPM, port 2 */
pcp write 10f2810 23
pcp write 10f2830 2000
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 423
pcp write 10f2830 9140
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
#/* take H-unit out of LPM, port 1 */
pcp write 10f2010 4
pcp write 10f2030 2000
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 404
pcp write 10f2030 9140
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
#/* take H-unit out of LPM, port 2 */
pcp write 10f2810 24
pcp write 10f2830 2000
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 424
pcp write 10f2830 9140
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
#/* take T-unit out of LPM, port 1 */
pcp write 10f2010 1
pcp write 10f2030 0
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 401
pcp write 10f2030 2040
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
#/* take T-unit out of LPM, port 2 */
pcp write 10f2810 21
pcp write 10f2830 0
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 421
pcp write 10f2830 2040
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
#/* take T-unit out of LPM, port 1 */
pcp write 10f2010 3
pcp write 10f2030 0
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 403
pcp write 10f2030 2040
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
#/* take T-unit out of LPM, port 2 */
pcp write 10f2810 23
pcp write 10f2830 0
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 423
pcp write 10f2830 2040
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
#/* Set MAC Type = 4, bit 15 = 1, port 1*/
pcp write 10f2010 1f
pcp write 10f2030 f001
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 41f
pcp write 10f2030 0004
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
#/* Set MAC Type = 4, bit 15 = 1, port 2 */
pcp write 10f2810 3f
pcp write 10f2830 f001
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 43f
pcp write 10f2830 0004
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
#/* Set MAC Type = 4, bit 15 = 1, port 1*/
pcp write 10f2010 1f
pcp write 10f2030 f001
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 41f
pcp write 10f2030 8004
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
#/* Set MAC Type = 4, bit 15 = 1, port 2 */
pcp write 10f2810 3f
pcp write 10f2830 f001
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 43f
pcp write 10f2830 8004
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
#/* T-unit Soft Reset, port 1 */
pcp write 10f2010 01
pcp write 10f2030 0000
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 401
pcp write 10f2030 a040
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
#/* T-unit Soft Reset, port 2 */
pcp write 10f2810 21
pcp write 10f2830 0000
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 421
pcp write 10f2830 a040
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
#/* Set Link Speed to 2500BaseT, port 1 */
pcp write 10f2010 07
pcp write 10f2030 0020
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 407
pcp write 10f2030 00a1
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
#/* Set Link Speed to 2500BaseT, port 2 */
pcp write 10f2810 27
pcp write 10f2830 0020
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 427
pcp write 10f2830 00a1
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
#/* Restart Auto-Neg, port 1 */
pcp write 10f2010 07
pcp write 10f2030 0000
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 407
pcp write 10f2030 b200
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
#/* Restart Auto-Neg, port 2 */
pcp write 10f2810 27
pcp write 10f2830 0000
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 427
pcp write 10f2830 b200
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1

echo "(2) Check Marvel PHY Link Status"
echo "================================"

echo "Port 1:"

pcp write 10f2010 01
pcp write 10f2030 1
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 c01
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
pcp read 10f2040
sleep 1
pcp write 10f2010 01
pcp write 10f2030 1
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 c01
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
pcp read 10f2040
sleep 1
echo "IMPORTANT:expect 0x86 is returned"


echo "Port 2:"

pcp write 10f2810 21
pcp write 10f2830 1
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 c21
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
pcp read 10f2840
sleep 1
pcp write 10f2810 21
pcp write 10f2830 1
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 c21
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
pcp read 10f2840
sleep 1
echo "IMPORTANT: expect 0x86 is returned"


echo "(2) Power up the DIM"
echo "===================="
pcp write dd0000 3



echo "(3) Wait 10 Seconds"
echo "==================="
sleep 10


echo "(4) Check Marvel PHY Link Status"
echo "================================"
echo "Port 1:"

pcp write 10f2010 01
pcp write 10f2030 1
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 c01
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
pcp read 10f2040
sleep 1
pcp write 10f2010 01
pcp write 10f2030 1
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
sleep 1
pcp write 10f2010 c01
pcp write 10f2000 80000000
pcp write 10f2000 80000001
pcp write 10f2000 80000000
pcp read 10f2040
sleep 1
echo "(expect 0x86 is returned)"


echo "Port 2:"

pcp write 10f2810 21
pcp write 10f2830 1
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 c21
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
pcp read 10f2840
sleep 1
pcp write 10f2810 21
pcp write 10f2830 1
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
sleep 1
pcp write 10f2810 c21
pcp write 10f2800 80000000
pcp write 10f2800 80000001
pcp write 10f2800 80000000
pcp read 10f2840
sleep 1
echo "(expect 0x86 is returned)"

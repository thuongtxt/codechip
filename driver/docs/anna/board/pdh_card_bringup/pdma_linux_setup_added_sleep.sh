echo "After 2.5G PHY are up (after script run by marvel_2_5G_phy_startup.txt)"
echo "======================================================================="



echo "(1) Detup MAC filter in Phlox"

echo "MAC Filter setup in Phlox"
echo "========================="
pcp write 1980708 0
pcp write 198070c 0
sleep 1
pcp write 1980710 0
pcp write 1980714 0
pcp write 198071c e288
pcp write 198073c 0
pcp write 1980750 0
pcp write 1980754 0
pcp write 198075c ffff
pcp write 198077c 0
sleep 1
pcp write 198070c 1
sleep 1
pcp write 1990708 0
pcp write 199070c 0
sleep 1
pcp write 1990710 0
pcp write 1990714 0
pcp write 199071c e288
pcp write 199073c 0
pcp write 1990750 0
pcp write 1990754 0
pcp write 199075c ffff
pcp write 199077c 0
sleep 1
pcp write 199070c 1
sleep 1

echo "/* PDH Port 1 - READ Operation */"
pcp write d00004 1
pcp write d00000 3
pcp write d00008 c00104
sleep 1
pcp write d00010 0
pcp write d00010 1
pcp write d00010 0
sleep 1
echo "/* Check Dashboard RAM */"
pcp read 19affc0
sleep 1

echo "/* PDH Port 2 - READ Operation */"
pcp write d80004 1
pcp write d80000 3
pcp write d80008 c00104
sleep 1
pcp write d80010 0
pcp write d80010 1
pcp write d80010 0
sleep 1
echo "/* Check Dashboard RAM */"
pcp read 19bffc0
sleep 1


echo "(3) DIM MAC Authorization"

echo "DIM Authorization (Port 1)"
echo "=========================="
pcp write 19affe0 abcd
pcp write 19affe4 abcdabcd
pcp write 19affe8 abcd
pcp write 19affec abcdabcd
sleep 1
echo "/* WRITE Operation */"
pcp write d00004 4
pcp write d00000 2
pcp write d00008 460008
sleep 1
pcp write d00010 0
pcp write d00010 1
pcp write d00010 0
sleep 1

echo "Toggle the go bit"
echo "================="
pcp write 19affe0 0
sleep 1
echo "/* WRITE Operation */"
pcp write d00004 1
pcp write d00000 2
pcp write d00008 c00520
sleep 1
pcp write d00010 0
pcp write d00010 1
pcp write d00010 0
sleep 1
pcp write 19affe0 1
sleep 1
echo "/* WRITE Operation */"
pcp write d00004 1
pcp write d00000 2
pcp write d00008 c00520
sleep 1
pcp write d00010 0
pcp write d00010 1
pcp write d00010 0
sleep 1
pcp write 19affe0 0
sleep 1
echo "/* WRITE Operation */"
pcp write d00004 1
pcp write d00000 2
pcp write d00008 c00520
sleep 1
pcp write d00010 0
pcp write d00010 1
pcp write d00010 0
sleep 1
pcp write 19affe0 0
sleep 1
echo "/* WRITE Operation */"
pcp write d00004 1
pcp write d00000 2
pcp write d00008 c00560
sleep 1
pcp write d00010 0
pcp write d00010 1
pcp write d00010 0
sleep 1
pcp write 19affe0 1
sleep 1
echo "/* WRITE Operation */"
pcp write d00004 1
pcp write d00000 2
pcp write d00008 c00560
sleep 1
pcp write d00010 0
pcp write d00010 1
pcp write d00010 0
sleep 1
pcp write 19affe0 0
sleep 1
echo "/* WRITE Operation */"
pcp write d00004 1
pcp write d00000 2
pcp write d00008 c00560
sleep 1
pcp write d00010 0
pcp write d00010 1
pcp write d00010 0
sleep 1


echo "DIM Authorization (Port 2)"
echo "=========================="
pcp write 19bffe0 abcd
pcp write 19bffe4 abcdabcd
pcp write 19bffe8 abcd
pcp write 19bffec abcdabcd

echo "/* WRITE Operation */"
pcp write d80004 4
pcp write d80000 2
pcp write d80008 460008
sleep 1
pcp write d80010 0
pcp write d80010 1
pcp write d80010 0
sleep 1

echo "Toggle the go bit"
echo "================="
pcp write 19bffe0 0
sleep 1
echo "/* WRITE Operation */"
pcp write d80004 1
pcp write d80000 2
pcp write d80008 c00520
sleep 1
pcp write d80010 0
pcp write d80010 1
pcp write d80010 0
sleep 1
pcp write 19bffe0 1
sleep 1
echo "/* WRITE Operation */"
pcp write d80004 1
pcp write d80000 2
pcp write d80008 c00520
sleep 1
pcp write d80010 0
pcp write d80010 1
pcp write d80010 0
sleep 1
pcp write 19bffe0 0
sleep 1
echo "/* WRITE Operation */"
pcp write d80004 1
pcp write d80000 2
pcp write d80008 c00520
sleep 1
pcp write d80010 0
pcp write d80010 1
pcp write d80010 0
sleep 1

pcp write 19bffe0 0
sleep 1
echo "/* WRITE Operation */"
pcp write d80004 1
pcp write d80000 2
pcp write d80008 c00560
sleep 1
pcp write d80010 0
pcp write d80010 1
pcp write d80010 0
sleep 1
pcp write 19bffe0 1

echo "/* WRITE Operation */"
pcp write d80004 1
pcp write d80000 2
pcp write d80008 c00560
sleep 1
pcp write d80010 0
pcp write d80010 1
pcp write d80010 0
sleep 1
pcp write 19bffe0 0
sleep 1
echo "/* WRITE Operation */"
pcp write d80004 1
pcp write d80000 2
pcp write d80008 c00560
sleep 1
pcp write d80010 0
pcp write d80010 1
pcp write d80010 0
sleep 1



echo "(4) Start PDH CEM 25 DS3 PRBS script"
echo "sdk device 1: /home/ATVN_Eng/af6xxx/atsdk/anna/2.2.1/2.2.1.b000/anna/epapp -m /dev/mem:0xfd0000000:0x8000000"
echo "sdk device 2: /home/ATVN_Eng/af6xxx/atsdk/anna/2.2.1/2.2.1.b000/anna/epapp -m /dev/mem:0xfc0000000:0x8000000"



echo "(5) Select DIM uplink A or B (depend on cable connection)"

echo "Select Link A"
echo "============="
pcp write 19affe0 1
sleep 1
echo "/* WRITE Operation */"
pcp write d00004 1
pcp write d00000 2
pcp write d00008 c00580
sleep 1
pcp write d00010 0
pcp write d00010 1
pcp write d00010 0
sleep 1
pcp write 19bffe0 1
sleep 1
echo "/* WRITE Operation */"
pcp write d80004 1
pcp write d80000 2
pcp write d80008 c00580
sleep 1
pcp write d80010 0
pcp write d80010 1
pcp write d80010 0
sleep 1

#Select Link B
#=============
#pcp write 19affe0 2

#/* WRITE Operation */
#pcp write d00004 1
#pcp write d00000 2
#pcp write d00008 c00580

#pcp write d00010 0
#pcp write d00010 1
#pcp write d00010 0

#pcp write 19bffe0 2

#/* WRITE Operation */
#pcp write d80004 1
#pcp write d80000 2
#pcp write d80008 c00580

#pcp write d80010 0
#pcp write d80010 1
#pcp write d80010 0




echo "(6) Do PDH CEM PRBS check"





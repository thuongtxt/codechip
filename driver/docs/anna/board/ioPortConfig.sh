#!/bin/bash

# to run in post-pivot
cd /etc/init.d/drivers
./rc.pcp_prusik start

# set port 1-8 laser to be output port
echo 0x1100004 0x0121 > /proc/ciena/oaf/poke
echo 0x1100008 0x0002 > /proc/ciena/oaf/poke
echo 0x1100040 0x0018 > /proc/ciena/oaf/poke
echo 0x1100000 1 > /proc/ciena/oaf/poke

# set port 9-16 laser to be output port
echo 0x1100004 0x0121 > /proc/ciena/oaf/poke
echo 0x1100008 0x0002 > /proc/ciena/oaf/poke
echo 0x1100040 0x0019 > /proc/ciena/oaf/poke
echo 0x1100000 1 > /proc/ciena/oaf/poke

# turn on port 1-8 laser
echo 0x1100004 0x0121 > /proc/ciena/oaf/poke
echo 0x1100008 0x0002 > /proc/ciena/oaf/poke
echo 0x1100040 0x0008 > /proc/ciena/oaf/poke
echo 0x1100000 1 > /proc/ciena/oaf/poke

# turn on port 9-16 laser
echo 0x1100004 0x0121 > /proc/ciena/oaf/poke
echo 0x1100008 0x0002 > /proc/ciena/oaf/poke
echo 0x1100040 0x0009 > /proc/ciena/oaf/poke
echo 0x1100000 1 > /proc/ciena/oaf/poke


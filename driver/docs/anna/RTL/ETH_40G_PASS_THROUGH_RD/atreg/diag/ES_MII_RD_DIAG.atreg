##################################################################
# Arrive Technologies
#
# Revision 1.0 - 2013.Mar.23

##################################################################
#This section is for guideline 
#File name: filename.atreg (atreg = ATVN register define)
#Comment out a line: Please use (# or //#) character at the beginning of the line
#Delimiter character: (%%)

##################################################################
#Common rules for register:
#Access types:
#R/W : Read / Write
#R/W/C: Read / Write 1 to clear (also called as sticky bit)
#R_O: Read Only, No clear on read
#R2C: Read Only, Read to Clear
#W_O: write only
#W1C: Read / Write 1 to clear (also called as sticky bit)
##################################################################
// Register Full Name: MiiAlarmGlb
// RTL Instant Name  : MiiAlarmGlb
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x00
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : 
// Where        : 
// Description  : This is status of packet diagnostic
// Width        : 32
// Register Type: {Config}
//# Field : [Bit:Bit] %% Name          %% Description                                 %% Type %% Reset %% Default
//  Field : [31:13]   %% Unused        %%                                             %%      %%       %%    
//  Fieldx: [12:12]   %% opkterr	   %% Packet Error 
//                                      {1} : Error
//                                      {0} : OK                          %% R1W  %% 0x0   %% 0x0
//  Field : [11:09]   %% Unused        %%                                             %%      %%       %%    
//  Fieldx: [08:08]   %% olenerr	   %% Packet Len Error 
//                                      {1} : Error
//                                      {0} : OK                          %% R1W  %% 0x0   %% 0x0
//  Field : [07:05]   %% Unused        %%                                             %%      %%       %% 
//  Fieldx: [04:04]   %% odaterr	   %% PRBS payload Data Error ( Not Syn) 
//                                      {1} : Not Syn
//                                      {0} : Syn                          %% R1W  %% 0x0   %% 0x0
//  Field : [03:01]   %% Unused        %%                                             %%      %%       %% 
//  Fieldx: [00:00]   %% link_sta	   %% PHY Link up status 
//                                      {1} : Link Down
//                                      {0} : Link Up                          %% R1W  %% 0x0   %% 0x0
##################################################################
// Register Full Name: MiiStatusGlb
// RTL Instant Name  : MiiStatusGlb
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x10
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : 
// Where        : 
// Description  : This is status of packet diagnostic
// Width        : 32
// Register Type: {Config}
//# Field : [Bit:Bit] %% Name          %% Description                                 %% Type %% Reset %% Default
//  Field : [31:13]   %% Unused        %%                                             %%      %%       %%    
//  Fieldx: [12:12]   %% opkterr	   %% Packet Error 
//                                      {1} : Error
//                                      {0} : OK                           %% RO  %% 0x0   %% 0x0
//  Field : [11:09]   %% Unused        %%                                             %%      %%       %%    
//  Fieldx: [08:08]   %% olenerr	   %% Packet Len Error 
//                                      {1} : Error
//                                      {0} : OK                           %% RO  %% 0x0   %% 0x0
//  Field : [07:05]   %% Unused        %%                                             %%      %%       %% 
//  Fieldx: [04:04]   %% odaterr	   %% PRBS payload Data Error ( Not Syn) 
//                                      {1} : Not Syn
//                                      {0} : Syn                          %% RO  %% 0x0   %% 0x0
//  Field : [03:01]   %% Unused        %%                                             %%      %%       %% 
//  Fieldx: [00:00]   %% link_sta	   %% PHY Link up status 
//                                      {1} : Link Down
//                                      {0} : Link Up                      %% RO  %% 0x0   %% 0x0

##################################################################
// Register Full Name: EthPacketConfig
// RTL Instant Name  : EthPacketConfig
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x01
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : 
// Where        : 
// Description  : Configuration Packet Diagnostic,  
// Width        : 32
// Register Type: {Config}
//# Field : [Bit:Bit] %% Name          %% Description                                 %% Type %% Reset %% Default
//  Field : [31:28]   %% Unused        %%                                    %% R/W  %% 0x00   %% 0x00 
//  Fieldx: [27:20]   %% icfgpat	   %% Data fix value for  idatamod = 0x2 %% R/W  %% 0xBC   %% 0xBC
//  Field : [19:18]   %% Unused        %%                                    %% R/W  %% 0x00   %% 0x00 
//  Fieldx: [17:16]   %% ibandwidth	   %% Band Width 
//										{0} full
//										{1} 50%
//										{2} 75%                        		 %% R/W  %% 0x00   %% 0x00
//  Field : [15:15]   %% Unused        %%                                    %% R/W  %% 0x00   %% 0x00    
//  Fieldx: [14:12]   %% idatamod	   %% Payload Data Mode
//																 0:PRBS7
//																 1:PRBS15
//																 2:PRBS23
//																 3:PRBS31  %% R/W     %% 0x1   %% 0x1
//  Fieldx: [11:00]   %% ipklen		   %% Packet len                         %% R/W  %% 0x40   %% 0x40
##################################################################
// Register Full Name: MiiEthTestControl
// RTL Instant Name  : MiiEthTestControl
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x03
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : 
// Where        : 
// Description  : Configuration Diagnostic,  
// Width        : 32
// Register Type: {Config}
//# Field : [Bit:Bit] %% Name          %% Description                                 %% Type %% Reset %% Default
//  Field : [31:03]   %% Unused        %%                                             %%      %%       %%    
//  Fieldx: [2]   	  %% start_diag    %% Config start Diagnostic trigger 0 to 1 for Start auto run
//							               with Gatetime Configuration %% RW  %% 0x0 %% 0x0
//  Fieldx: [01:01]   %% iforceerr	   %% PRBS Data Error Insert   
//                                      {0} : None
//                                      {1} : Insert             %% R/W  %% 0x0   %% 0x0
//  Fieldx: [00:00]   %% test_en	   %% PRBS test Enable  
//                                      {0} : Disable 
//                                      {1} : Enable             %% R/W  %% 0x0   %% 0x0
##################################################################
// Register Full Name: gatetime config value
// RTL Instant Name  : gatetime_cfg
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x04
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : 
// Where        : 
// Description  : This is Mate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 4
// Width        : 32
// Register Type: {Configure}
//# Field : [Bit:Bit] %% Name        %% Description            %% Type %% Reset %% Default
//  Fieldx: [16:00]   %% time_cfg     %% Gatetime Configuration 1-86400 second  %% RW  %% 0x0 %% 0x0
##################################################################
// Register Full Name: Gatetime Current
// RTL Instant Name  : Gatetime_current
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x05 
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : 
// Where        :
// Description  : 
// Width        : 32
// Register Type: {Configure}
//# Field : [Bit:Bit] %% Name        %% Description            %% Type %% Reset %% Default
//  Field:  [17]      %% status_gatetime_diag   %% Status Gatetime diagnostic port 16 to port1 bit per port
//												1:Running 
//												0:Done-Ready	%% RO  %% 0x0 %% 0x0
//  Field:  [16:0]    %% currert_gatetime_diag   %% Current running time of Gatetime diagnostic	%% RO  %% 0x0 %% 0x0

##################################################################
// Begin:
// Register Full Name: MiiTxCounterR2C.
// RTL Instant Name  : itxpkr2c
//# {Functionname,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0C
// Formula      : 
// Where        : 
// Description  : Register to test MII interface
// Width        : 32
// Register Type: {Counter}
//# Field : [Bit:Bit] %% Name   	         %% Description                %% Type    %% Reset %% Default
//  Fieldx: [31:0]    %% Tx_counter_R2C      %% Tx Packet counter R2C       %% R2C     %% 0x0   %% 0x0
// End :
##################################################################
// Begin:
// Register Full Name: MiiRxCounterR2C..
// RTL Instant Name  : counter2
//# {Functionname,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0D
// Formula      : 
// Where        : 
// Description  : Register to test MII interface
// Width        : 32
// Register Type: {Counter}
//# Field : [Bit:Bit] %% Name   	         %% Description                %% Type    %% Reset %% Default
//  Fieldx: [31:0]    %% Rx_counter_R2C      %% Rx counter R2C             %% R2C     %% 0x0   %% 0x0
##################################################################
// Begin:
// Register Full Name: MiiTxCounterRO.
// RTL Instant Name  : counter4
//# {Functionname,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x08
// Formula      : 
// Where        : 
// Description  : Register to test MII interface
// Width        : 32
// Register Type: {Counter}
//# Field : [Bit:Bit] %% Name   	         %% Description                %% Type    %% Reset %% Default
//  Fieldx: [31:0]    %% Tx_counter_RO      %% Tx counter RO             %% RO     %% 0x0   %% 0x0
##################################################################
// Begin:
// Register Full Name: MiiRxCounterRO.
// RTL Instant Name  : counter5
//# {Functionname,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x09
// Formula      : 
// Where        : 
// Description  : Register to test MII interface
// Width        : 32
// Register Type: {Counter}
//# Field : [Bit:Bit] %% Name   	         %% Description                %% Type    %% Reset %% Default
//  Fieldx: [31:0]    %% Rx_counter_RO       %% Rx counter RO             %% RO     %% 0x0   %% 0x0

######################################################################################
# Arrive Technologies
# Register Description Document
# Revision 1.0 - 2015.Feb.03

######################################################################################
# This section is for guideline
# Refer to \\Hcmcserv3\Projects\internal_tools\atvn_vlibs\bin\rgm_gen\docs\REGISTER_DESCRIPTION_GUIDELINE.pdf
# File name: filename.atreg (atreg = ATVN register define)
# Comment out a line: Please use (# or //#) character at the beginning of the line
# Delimiter character: (%%)

######################################################################################
#| Access Policy| Use Case| Description                | Effect of a Write on Current Field Value  | Effect of a Read on Current Field Value | Read-back Value |
#| ------|-----------|---------------------------------| ------------------------------------------|-----------------------------------------|-----------------|
#| RO    | Status    | Read Only                       | No effect                | No effect            | Current Value |
#| RW    | Config    | Read, Write                     | Changed to written value | No effect            | Current value |
#| RC    | Status    | Read Clears All                 | No effect                | Sets all bits to 0   | Current value |
#| RS    | UNKNOW    | Read Sets All                   | No effect                | Sets all bits to 1   | Current value |
#| WRC   | UNKNOW    | Write, Read Clears All          | Changed to written value | Sets all bits to 0   | Current value |
#| WRS   | UNKNOW    | Write, Read Sets All            | Changed to written value | Sets all bits to 1   | Current value |
#| WC    | Status    | Write Clears All                | Sets all bits to 0       | No effect            | Current value |
#| WS    | Status    | Write Sets All                  | Sets all bits to 1       | No effect            | Current value |
#| WSRC  | UNKNOW    | Write Sets All, Read Clears All | Sets all bits to 1       | Sets all bits to 0   | Current value |
#| WCRS  | UNKNOW    | Write Clears All,Read Sets All  | Sets all bits to 0       | Sets all bits to 1   | Current value |
#| W1C   | Interrupt | Write 1 to Clear                | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current Value |
#| W1S   | Interrupt | Write 1 to Set                  | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W1T   | UNKNOW    | Write 1 to Toggle               | If the bit in the wr value is a 1, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W0C   | Interrupt | Write 0 to Clear                | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current value |
#| W0S   | Interrupt | Write 0 to Set                  | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W0T   | UNKNOW    | Write 0 to Toggle               | If the bit in the wr value is a 0, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W1SRC | UNKNOW    | Write 1 to Set, Read Clears All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W1CRS | UNKNOW    | Write 1 to Clear, Read Sets All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| W0SRC | UNKNOW    | Write 0 to Set, Read Clears All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W0CRS | UNKNOW    | Write 0 to Clear, Read Sets All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| WO    | Config    | Write Only                      | Changed to written value | No effect | Undefined |
#| WOC   | UNKNOW    | Write Only Clears All           | Sets all bits to 0     | No effect | Undefined |
#| WOS   | UNKNOW    | Write Only Sets All             | Sets all bits to 1     | No effect | Undefined |
#| W1    | UNKNOW    | Write Once                      | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Current value |
#| WO1   | UNKNOW    | Write Only, Once                | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Undefined |
#=============================================================================
# Sample
#=============================================================================

######################################################################################
#SAMPLE> // Begin:
#SAMPLE> //# Some description
#SAMPLE> // Register Full Name: Device Version ID
#SAMPLE> // RTL Instant Name: VersionID
#SAMPLE> //# {FunctionName,SubFunction_InstantName_Description}
#SAMPLE> //# RTL Instant Name and Full Name MUST be unique within a register description file
#SAMPLE> // Address: 0x00_0000
#SAMPLE> //# Address is relative address, will be sum with Base_Address for each function block
#SAMPLE> // Formula      : Address + $portid 
#SAMPLE> // Where        : {$portid(0-7): port identification} 
#SAMPLE> // Description  : This register indicates the module' name and version. 
#SAMPLE> // Width: 32
#SAMPLE> // Register Type: {Status}
#SAMPLE> //# Field: [Bit:Bit] %%  Name     %% Description            %% Type %% SW_Reset %% HW_Reset
#SAMPLE> // Field: [31:24]    %%  Year     %% 2013                   %% RO   %% 0x0D     %% 0x0D
#SAMPLE> // Field: [23:16]    %%  Week     %% Week Synthesized FPGA  %% RO   %% 0x0C     %% 0x0C
#SAMPLE> // Field: [15:8]     %%  Day      %% Day Synthesized FPGA   %% RO   %% 0x16     %% 0x16
#SAMPLE> // Field: [7:4]      %%  VerID    %% FPGA Version           %% RO   %% 0x0      %% 0x0
#SAMPLE> // Field: [3:0]      %%  NumID    %% Number of FPGA Version %% RO   %% 0x1      %% 0x1
#SAMPLE> // End:
######################################################################################

#=============================================================================
#define address offset and name rule of sub-core inside CodeChip(8xTFI5)
#=============================================================================

ADDR_DEFINE TOP       [24:0]  0x0F0_0000-0x0FF_FFFF AF6CCI0011_RD_TOP                           //TOP global register, SAME OLD PROJECTS  

ADDR_DEFINE GLB       [24:0]  0x000_0000-0x000_FFFF AF6CCI0011_RD_GLB                           //CORE global register 
ADDR_DEFINE TENG1     [24:0]  0x002_0000-0x002_FFFF AF6CCI0011_RD_ETH10G                        //ETH pass through port 1: XFI                     
ADDR_DEFINE GE16_1    [24:0]  0x003_0000-0x003_FFFF AF6CNC0011_RD_SGMII_Multirate               //ETH pass through port 1 to 16: 10/100/1000M SGMII                 
ADDR_DEFINE TENG2     [24:0]  0x004_0000-0x004_FFFF AF6CCI0011_RD_ETH10G                        //ETH pass through port 9: XFI
ADDR_DEFINE GE32_17   [24:0]  0x005_0000-0x005_FFFF AF6CNC0011_RD_SGMII_Multirate               //ETH pass through port 17 to 32: 10/100/1000M SGMII      
ADDR_DEFINE TENG3     [24:0]  0x006_0000-0x006_FFFF AF6CCI0011_RD_ETH10G                        //ETH pass through port 17: XFI
ADDR_DEFINE TENG4     [24:0]  0x007_0000-0x007_FFFF AF6CCI0011_RD_ETH10G                        //ETH pass through port 24: XFI
ADDR_DEFINE MACDUMP   [24:0]  0x008_0000-0x008_FFFF AF6CCI0011_RD_MAC_DUMP                      //MAC DUMP
ADDR_DEFINE ETHPASS   [24:0]  0x00C_0000-0x00C_FFFF AF6CNC0021_RD_ETHPASS                        //ETHPASS
ADDR_DEFINE PTP       [24:0]  0x00E_0000-0x00F_FFFF AF6CCI0012_RD_PTP                           //PTP
ADDR_DEFINE INTR      [24:0]  0x009_0000-0x009_FFFF AF6CCI0011_RD_INTR                          //Global interrupt mask control
       
ADDR_DEFINE TOP_CS              [24:0]  0xF0_0000-0xFF_FFFF     AF6CNC0021_RD_TOP_GLB                       //TOP global register, SAME OLD PROJECTS  
ADDR_DEFINE DDR4_1              [24:0]  0xF20_000-0xF20_FFF     AF6CNC0021_RD_DDR4_1                        //DDR4#1 Diagnostic 
ADDR_DEFINE DDR4_2              [24:0]  0xF21_000-0xF21_FFF     AF6CNC0021_RD_DDR4_2                        //DDR4#2 Diagnostic 
ADDR_DEFINE DDR4_3              [24:0]  0xF22_000-0xF22_FFF     AF6CNC0021_RD_DDR4_3                        //DDR4#3 Diagnostic        
ADDR_DEFINE DDR4_4              [24:0]  0xF23_000-0xF23_FFF     AF6CNC0021_RD_DDR4_4                        //DDR4#4 Diagnostic        
ADDR_DEFINE QDR2#1              [24:0]  0xF24_000-0xF24_FFF     AF6CNC0021_RD_QDR2_1                        //QDR2#1 Diagnostic        
    
ADDR_DEFINE MDIO1G              [24:0]  0xF54_000-0xF54_FFF     AF6CNC0021_RD_MDIO1G                        //MDIO Control for 1Gbe 1000Base Serdes Port 1:K-SGMII & Port 2:DCC-SGMII & Port 3-4 : SPARE SGMII      
ADDR_DEFINE SYS_MON             [24:0]  0xF44_000-0xF44_0FF     DRP address,See ug580-ultrascale-sysmon.pdf //SYS_MON for FPGA
ADDR_DEFINE CIENA_VER           [24:0]  0xF45_000-0xF45_FFF     AF6CNC0021_RD_TOP_GLB                       //FPGA Version for CIENA
//ADDR_DEFINE HIGH_CLK_MON      [24:0]  0xF46_000-0xF46_FFF     AF6CNC0021_RD_TOP_GLB                       //High Frequency Clock monitor for Clock input FPGA and User clk out from FPGA IP
//ADDR_DEFINE LOW_CLK_MON       [24:0]  0xF47_000-0xF47_FFF     AF6CNC0021_RD_TOP_GLB                       //LOW Frequency Clock monitor for Clock input FPGA 
    
ADDR_DEFINE SGMII_K             [24:0]  0xF50_000-0xF50_FFF     AF6CNC0021_Xilinx_Serdes_Turning_Configuration_RD.atreg//1*K_SGMII Serdes Turning Configuration 
ADDR_DEFINE SGMII_DCC           [24:0]  0xF51_000-0xF51_FFF     AF6CNC0021_Xilinx_Serdes_Turning_Configuration_RD.atreg//1*DCC_SGMII Serdes Turning Configuration 
ADDR_DEFINE SGMII_SP            [24:0]  0xF52_000-0xF52_FFF     AF6CNC0021_Xilinx_Serdes_Turning_Configuration_RD.atreg//2*SPARE_SGMII Serdes Turning Configuration 
//ADDR_DEFINE SGMIID#1-4            [24:0]  0xF56_000-0xF56_FFF     AF6CNC0021_GMII_RD_DIAG.atreg                           //SGMII#1-4  Diagnostic Port 1:K-SGMII & Port 2:DCC-SGMII & Port 3-4 : SPARE SGMII
ADDR_DEFINE SGMIID#1-4          [24:0]  0xF53_000-0xF53_FFF     AF6CNC0021_GMII_RD_DIAG.atreg                           //SGMII#1-4  Diagnostic Port 1:K-SGMII & Port 2:DCC-SGMII & Port 3-4 : SPARE SGMII                                  
    
ADDR_DEFINE OCNGBESER           [24:0]  0xF6_0000-0xF6_1FFF     AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD.atreg           //OC192_OC48_OC12_10G_1G_100FX_16ch Serdes Turning Configuration + Serdes Type Configuration      
ADDR_DEFINE TSI_MATE_1_4        [24:0]  0xF7_0000-0xF7_1FFF     AF6CNC0021_Xilinx_Serdes_Turning_Configuration_RD//TSI_MATE  Serdes Turning Configuration 
ADDR_DEFINE TSI_MATE_5_8        [24:0]  0xF7_2000-0xF7_3FFF     AF6CNC0021_Xilinx_Serdes_Turning_Configuration_RD//TSI_MATE  Serdes Turning Configuration     
ADDR_DEFINE TSI_MATE_OC48_1_4   [24:0]  0xF7_4000-0xF7_5FFF     AF6CNC0021_OC192_OC48_OC12_OC3_RD_DIAG.atreg           //TSI_MATE  OC48 Diagnostic  
ADDR_DEFINE TSI_MATE_OC48_5_8   [24:0]  0xF7_6000-0xF7_7FFF     AF6CNC0021_OC192_OC48_OC12_OC3_RD_DIAG.atreg           //TSI_MATE  OC48 Diagnostic   

ADDR_DEFINE ETH40G_1            [24:0]  0xF8_0000-0xF8_FFFF     AF6CNC0021_ETH40G_RD                     //ETH40G#1  Serdes Turning Configuration + 40G Diagnostic                           
ADDR_DEFINE ETH40G_2            [24:0]  0xF9_0000-0xF9_FFFF     AF6CNC0021_ETH40G_RD                      //ETH40G#2  Serdes Turning Configuration + 40G Diagnostic   

ADDR_DEFINE SFP_OC192_D_01_02   [24:0]  0xFA_0000-0xFA_07FF     AF6CNC0021_OC192_OC48_OC12_OC3_RD_DIAG            //SFP OC192 Port #1#9 Diagnostic     

ADDR_DEFINE SFP_OC192_GT_01_16  [24:0]  0xFA_8000-0xFA_0FFF     AF6CNC0021_RD_TOP_GLB						            //Faceplate serdeses Gatetime for PRBS Raw Diagnostic 
                                                                                                            
ADDR_DEFINE SFP_XFI_D_4         [24:0]  0xFA_2000-0xFA_27FF     AF6CNC0021_XFI_RD_DIAG                          //SFP XFI Port #1 Diagnostic     
ADDR_DEFINE SFP_XFI_D_12        [24:0]  0xFA_6000-0xFA_67FF     AF6CNC0021_XFI_RD_DIAG                           //SFP XFI Port #9 Diagnostic    
    
ADDR_DEFINE SFP_OC48_D_01_04    [24:0]  0xFA_A000-0xFA_A7FF     AF6CNC0021_OC192_OC48_OC12_OC3_RD_DIAG           //SFP OC48 Port #1#2#3#4 Diagnostic      
ADDR_DEFINE SFP_OC48_D_05_08    [24:0]  0xFA_A800-0xFA_AFFF     AF6CNC0021_OC192_OC48_OC12_OC3_RD_DIAG           //SFP OC48 Port #5#6#7#8 Diagnostic      
ADDR_DEFINE SFP_OC48_D_09_12    [24:0]  0xFA_B000-0xFA_B7FF     AF6CNC0021_OC192_OC48_OC12_OC3_RD_DIAG           //SFP OC48 Port #9#10#11#12 Diagnostic
ADDR_DEFINE SFP_OC48_D_13_16    [24:0]  0xFA_B800-0xFA_BFFF     AF6CNC0021_OC192_OC48_OC12_OC3_RD_DIAG           //SFP OC48 Port #13#14#15#16 Diagnostic          


######################################################################################
#    *********
#   * Device Product ID *
#    *********
// Begin:
// Register Full Name: Device Product ID
// RTL Instant Name  : ProductID
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x00_0000
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : {N/A}
// Description  : This register indicates Product ID. 
// Width: 32
// Register Type: {Status}
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]     %%  ProductID      %% ProductId               %% RO %% 0x60210051 %% 0x60210051
// End:

######################################################################################
#    *********
#   * Device Year Month Day Version ID *
#    *********
// Begin:
// Register Full Name: Device Year Month Day Version ID
// RTL Instant Name  : YYMMDD_VerID
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x00_0002
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : {N/A}
// Description  : This register indicates Year Month Day and main version ID. 
// Width: 32
// Register Type: {Status}
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:24]    %%  Year           %% Year                    %% RO %% 0x16 %% 0x16
// Field: [23:16]    %%  Month          %% Month                   %% RO %% 0x01 %% 0x01
// Field: [15:8 ]    %%  Day            %% Day                     %% RO %% 0x30 %% 0x30
// Field: [7:0 ]     %%  Version        %% Version                 %% RO %% 0x40 %% 0x40
// End:

######################################################################################
#    *********
#   * Device Internal ID *
#    *********
// Begin:
// Register Full Name: Device Internal ID
// RTL Instant Name  : InternalID
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x00_0003
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : {N/A}
// Description  : This register indicates internal ID. 
// Width: 16
// Register Type: {Status}
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [15:0]     %%  InternalID     %% InternalID              %% RO %% 0x0 %% 0x0
// End:

######################################################################################
#    *************
#   * Sub-Core Active *
#    *************
// Begin:
// Register Full Name: GLB Sub-Core Active  
// RTL Instant Name  : Active
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x00_0001
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : {N/A}
// Description  : This register indicates the active ports.
// Width: 32
// Register Type: {Config|Status}
//# Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31: 0] %% SubCoreEn %% Enable per sub-core {1}: Enable {0}: Disable %% RW %% 0x00000000 %% 0x00000000
// End:
#####################################################################################
#    **************
#   * Chip Temperature Sticky *
#    **************
// Begin:
// Register Full Name: Chip Temperature Sticky 
// RTL Instant Name  : ChipTempSticky
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x00_0020
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : {N/A}
// Description  : This register is used to sticky temperature
// Width: 3
// Register Type: {Status}
//# Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [2] %% ChipSlr2TempStk %% Chip SLR2 temperature %% WC %% 0x0 %% 0x0
// Field: [1] %% ChipSlr1TempStk %% Chip SLR1 temperature %% WC %% 0x0 %% 0x0
// Field: [0] %% ChipSlr0TempStk %% Chip SLR0 temperature %% WC %% 0x0 %% 0x0
// End:


#####################################################################################
#    **************
#   * Debug MAC LoopBack Control *
#    **************
// Begin:
// Register Full Name: GLB Debug Mac Loopback Control 
// RTL Instant Name  : DebugMacLoopControl
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x00_0013
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : {N/A}
// Description  : This register is used to configure debug loopback MAC parameters
// Width: 2
// Register Type: {Config|Status}
//# Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [1] %% MacMroLoopOut %% Set 1 for Mac 10G/1G/100Fx Loop Out %% RW %% 0x0 %% 0x0
// Field: [0] %% Mac40gLoopIn  %% Set 1 for Mac40gLoopIn  %% RW %% 0x0 %% 0x0
// End:

#####################################################################################
# **************
# * Global 1G/100M/10M Ethernet Clock EER Squelching Control *
# **************
// Begin:
// Register Full Name: GLB Eth1G Clock EER Squelching Control 
// RTL Instant Name  : GlbEth1GClkEerSquelControl
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x00_0010
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : {N/A}
// Description  : This register is used to configure debug PW parameters
// Width: 32
// Register Type: {Config|Status}
//# Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31:0] %% EthGeExcessErrRateSchelEn  %% Each Bit represent for each ETH port, value each bit is as below
//      1: Disable 8Khz refout when GE Excessive Error Rate occur
//      0: Enable 8Khz refout when GE Excessive Error Rate occur  %% RW %% 0x0 %% 0x0
// End:

#####################################################################################
# **************
# * Global 1G/100M/10M Ethernet Clock Link Down Squelching Control *
# **************
// Begin:
// Register Full Name: GLB Eth1G Clock Squelching Control 
// RTL Instant Name  : GlbEth1GClkLinkDownSquelControl
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x00_0011
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : {N/A}
// Description  : This register is used to configure debug PW parameters
// Width: 32
// Register Type: {Config|Status}
//# Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31:0] %% EthGeLinkDownSchelEn %% Each Bit represent for each ETH port, value each bit is as below
//      1: Disable 8Khz refout when GE link down occur
//      0: Enable 8Khz refout when GE link down occur  %% RW %% 0x0 %% 0x0

#####################################################################################
# **************
# * Global 10G Ethernet Clock Squelching Control *
# **************
// Begin:
// Register Full Name: GLB Eth10G Clock Squelching Control 
// RTL Instant Name  : GlbEth10GClkSquelControl
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x00_0012
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : {N/A}
// Description  : This register is used to configure debug PW parameters
// Width: 16
// Register Type: {Config|Status}
//# Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [15:12] %% TenGeLocalFaultSchelEn  %% Each Bit represent for each TenGe port0 and port8, value each bit is as below
//      1: Disable 8Khz refout when TenGe Local Fault occur
//      0: Enable 8Khz refout when TenGe Local Fault occur  %% RW %% 0x0 %% 0x0
// Field: [11:8] %% TenGeRemoteFaultSchelEn  %% Each Bit represent for each TenGe port0 and port8, value each bit is as below
//      1: Disable 8Khz refout when TenGe Remote Fault occur
//      0: Enable 8Khz refout when TenGe Remote Fault occur  %% RW %% 0x0 %% 0x0
// Field: [7:4] %% TenGeLossDataSyncSchelEn  %% Each Bit represent for each TenGe port0 and port8, value each bit is as below
//      1: Disable 8Khz refout when TenGe Loss of Data Sync occur
//      0: Enable 8Khz refout when TenGe Loss of Data Sync occur  %% RW %% 0x0 %% 0x0
// Field: [3:0] %% TenGeExcessErrRateSchelEn  %% Each Bit represent for each TenGe port0 and port8, value each bit is as below
//      1: Disable 8Khz refout when TenGe Excessive Error Rate occur
//      0: Enable 8Khz refout when TenGe Excessive Error Rate occur  %% RW %% 0x0 %% 0x0
// End:

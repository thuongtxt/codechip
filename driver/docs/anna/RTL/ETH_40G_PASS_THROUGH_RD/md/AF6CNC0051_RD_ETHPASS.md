## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-12-19|AF6Project|Initial version|




##AF6CNC0051_RD_ETHPASS
####Register Table

|Name|Address|
|-----|-----|
|`CONFIG VLAN REMOVED PER GROUP 2 PORT`|`0x00 + ($PID)/2`|
|`CONFIG ENABLE REMOVE VLAN TPID`|`0x10`|
|`CONFIG ENABLE REMOVE VLAN TPID`|`0x11`|
|`CONFIG VLAN INSERTED`|`0x120-0x13F`|
|`CONFIG ENABLE INSERT VLAN TPID`|`0x110`|
|`CONFIG ENABLE INSERT VLAN TPID`|`0x111`|
|`CONFIG ENABLE INSERT VLAN TPID`|`0x112`|
|`CONFIG THRESHOLD`|`0x200-0x21F`|
|`CONFIG ENABLE FLOW CONTROL`|`0x220`|
|`CONFIG THRESHOLD`|`0x860-0x87F`|
|`STATUS MAX MIN BUFFER`|`0x880-0x89F`|
|`Ethernet Pass Through Hold Register Status`|`0x80`|
|`Ethernet Pass Through Group1 Counter`|`0xA00 - 0xA3F(RO)`|
|`Ethernet Pass Through Group2 Counter`|`0xA80 - 0xABF(RO)`|
|`Ethernet Pass Through Group3 Counter`|`0xB00 - 0xB3F(RC)`|
|`Ethernet Pass Through Group1 Counter`|`0xE00 - 0xE3F(RO)`|
|`Ethernet Pass Through Group2 Counter`|`0xE80 - 0xEBF(RO)`|
|`Ethernet Pass Through Group3 Counter`|`0xF00 - 0xF3F(RO)`|
|`Ethernet Pass Through Group4 Counter`|`0xF80 - 0xFBF(RO)`|
|`Ethernet Pass Through MAC Address Control`|`0xC60 - 0xC7F #The address format for these registers is 0xC30 + ethpid`|
|`Ethernet Pass Through Port Enable and MTU Control`|`0xC80 - 0xC9F #The address format for these registers is 0xC40 + ethpid`|
|`Ethernet Pass Through Global Diagnostic Enable Control`|`0x300`|
|`Ethernet Pass Through Diagnostic Port Enable and Speed Control`|`0x320 - 0x33F #The address format for these registers is 0x320 + ethpid`|
|`Ethernet Pass Through Diagnostic Port Burst Control`|`0x360 - 0x37F #The address format for these registers is 0x360 + ethpid`|
|`Ethernet Pass Through Diagnostic Port Packet Length Control`|`0x3A0 - 0x3BF #The address format for these registers is 0x360 + ethpid`|
|`Ethernet Pass Through Diagnostic Port Generate Packet Mode Control`|`0x3E0 - 0x3FF #The address format for these registers is 0x3E0 + ethpid`|
|`Ethernet Pass Through Diagnostic Port Monitor Packet Mode Control`|`0x400 - 0x41F #The address format for these registers is 0x400 + ethpid`|
|`Ethernet Pass Through Diagnostic Port Monitor Error Counter`|`0x440 - 0x47F #The address format for these registers is 0x440 + ethpid`|
|`Ethernet Pass Through Diagnostic Port Monitor Packet Counter`|`0x480 - 0x4BF #The address format for these registers is 0x480 + ethpid`|
|`Ethernet Pass Through Diagnostic Port Monitor Byte Counter`|`0x4C0 - 0x4FF #The address format for these registers is 0x4C0 + ethpid`|


###CONFIG VLAN REMOVED PER GROUP 2 PORT

* **Description**           




* **RTL Instant Name**    : `upen_cfgvlan`

* **Address**             : `0x00 + ($PID)/2`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_odd_port`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_odd_port`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_odd_port`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_even_port`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_even_port`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_even_port`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE REMOVE VLAN TPID

* **Description**           




* **RTL Instant Name**    : `upen_enbtpid`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`out_cfg_tpid`| bit corresponding port to config ( bit 0 is port 0),<br>{0}  use config TPID1, <br>{1} use config TPID 2| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE REMOVE VLAN TPID

* **Description**           




* **RTL Instant Name**    : `upen_cfgrmtpid`

* **Address**             : `0x11`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`out_cfgtpid2`| value TPID 2| `R/W`| `0x0`| `0x0`|
|`[15:0]`|`out_cfgtpid1`| value TPID 1| `R/W`| `0x8100`| `0x8100 End: Begin:`|

###CONFIG VLAN INSERTED

* **Description**           




* **RTL Instant Name**    : `upen_cfginsvlan`

* **Address**             : `0x120-0x13F`

* **Formula**             : `0x120+ $PID`

* **Where**               : 

    * `$PID(0-31) : Port ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:13]`|`prior`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE INSERT VLAN TPID

* **Description**           




* **RTL Instant Name**    : `upen_enbinstpid`

* **Address**             : `0x110`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[32:0]`|`out_enbtpid`| bit corresponding port to config ( bit 0 is port 0),(0)  use config TPID1, (1) use config TPID 2| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE INSERT VLAN TPID

* **Description**           




* **RTL Instant Name**    : `upen_cfginstpid`

* **Address**             : `0x111`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`out_cfgtpid2`| valud TPID 2| `R/W`| `0x0`| `0x0`|
|`[15:0]`|`out_cfgtpid1`| value TPID 1| `R/W`| `0x8100`| `0x8100 End: Begin:`|

###CONFIG ENABLE INSERT VLAN TPID

* **Description**           




* **RTL Instant Name**    : `upen_enbins`

* **Address**             : `0x112`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`out_enbins`| bit corresponding port to config ( bit 0 is port 0), (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG THRESHOLD

* **Description**           

Theshold in 1-kbyte unit to report STARVING/HUNGRY/SATISFIED via OOF bus of MAC40G to FACE ETH buffer


* **RTL Instant Name**    : `upen_cfgthreshold`

* **Address**             : `0x200-0x21F`

* **Formula**             : `0x200+ $PID`

* **Where**               : 

    * `$PID(0-31) : Port ID`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:08]`|`hungry_thres`| report HUNGRY if below this threshold and over starving_thres, report SATISFIED of over this threshold, default 3/4 of max buffer| `R/W`| `0xC0`| `0x0`|
|`[07:00]`|`starving_thres`| report STARVING if below this threshold, default 1/4 of max buffer| `R/W`| `0x40`| `0x0 End: Begin:`|

###CONFIG ENABLE FLOW CONTROL

* **Description**           




* **RTL Instant Name**    : `upen_enbflwctrl`

* **Address**             : `0x220`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00:00]`|`out_enbflw`| (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG THRESHOLD

* **Description**           

Currest status buffer in 1-kbyte unit to report STARVING/HUNGRY/SATISFIED via OOF bus of MAC40G to FACE ETH buffer


* **RTL Instant Name**    : `upen_stathreshold`

* **Address**             : `0x860-0x87F`

* **Formula**             : `0x860+ $PID`

* **Where**               : 

    * `$PID(0-31) : Port ID`

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[07:00]`|`bufferlength`| Current buffer length in 1-byte unit| `R/W`| `0x0`| `0x0 End: Begin:`|

###STATUS MAX MIN BUFFER

* **Description**           

Watermakr max min status buffer in 1-kbyte unit to report STARVING/HUNGRY/SATISFIED via OOF


* **RTL Instant Name**    : `upen_stabufwater`

* **Address**             : `0x880-0x89F`

* **Formula**             : `0x880+ $PID`

* **Where**               : 

    * `$PID(0-31) : Port ID`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:08]`|`maxbufferlength`| Max buffer length in 1-byte unit| `R/W`| `0x0`| `0x0`|
|`[07:00]`|`minbufferlength`| Min buffer length in 1-byte unit| `R/W`| `0xFF`| `0x0 End: Begin:`|

###Ethernet Pass Through Hold Register Status

* **Description**           

This register using for hold remain that more than 128bits


* **RTL Instant Name**    : `epa_hold_status`

* **Address**             : `0x80`

* **Formula**             : `0x80 +  HID`

* **Where**               : 

    * `$HID(0-0): Hold ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`epaholdstatus`| Hold 32bits| `RW`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group1 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_txepagrp1cnt`

* **Address**             : `0xA00 - 0xA3F(RO)`

* **Formula**             : `0xA00 + 32*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-31)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:32]`|`txethgrp1cnt1`| cntoffset = 0 : txpkt255byte cntoffset = 1 : txpkt1023byte cntoffset = 2 : txpktjumbo| `RC`| `0x0`| `0x0`|
|`[31:00]`|`txethgrp1cnt0`| cntoffset = 0 : txpkt127byte cntoffset = 1 : txpkt511byte cntoffset = 2 : txpkt1518byte| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group2 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_txepagrp2cnt`

* **Address**             : `0xA80 - 0xABF(RO)`

* **Formula**             : `0xA80 + 32*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-31)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:32]`|`txethgrp2cnt1`| cntoffset = 0 : txpktgood cntoffset = 1 : unused cntoffset = 2 : txoversize| `RC`| `0x0`| `0x0`|
|`[31:00]`|`txethgrp2cnt0`| cntoffset = 0 : txbytecnt cntoffset = 1 : txpkttotaldis cntoffset = 2 : txundersize| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group3 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_txepagrp3cnt`

* **Address**             : `0xB00 - 0xB3F(RC)`

* **Formula**             : `0xB00 + 32*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-31)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:32]`|`txethgrp1cnt1`| cntoffset = 0 : txpktoverrun cntoffset = 1 : txpktmulticast cntoffset = 2 : unused| `RC`| `0x0`| `0x0`|
|`[31:00]`|`txethgrp1cnt0`| cntoffset = 0 : txpktpausefrm cntoffset = 1 : txpktbroadcast cntoffset = 2 : txpkt64byte| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group1 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_rxepagrp1cnt`

* **Address**             : `0xE00 - 0xE3F(RO)`

* **Formula**             : `0xE00 + 32*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-31)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:32]`|`rxethgrp1cnt1`| cntoffset = 0 : rxpkt255byte cntoffset = 1 : rxpkt1023byte cntoffset = 2 : rxpktjumbo| `RC`| `0x0`| `0x0`|
|`[31:00]`|`rxethgrp1cnt0`| cntoffset = 0 : rxpkt127byte cntoffset = 1 : rxpkt511byte cntoffset = 2 : rxpkt1518byte| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group2 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_rxepagrp2cnt`

* **Address**             : `0xE80 - 0xEBF(RO)`

* **Formula**             : `0xE80 + 32*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-31)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:40]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[63:32]`|`rxethgrp2cnt1`| cntoffset = 0 : rxpkttotal cntoffset = 1 : unused cntoffset = 2 : rxpktdaloop| `RC`| `0x0`| `0x0`|
|`[31:00]`|`rxethgrp2cnt0`| cntoffset = 0 : rxbytecnt cntoffset = 1 : rxpktfcserr cntoffset = 2 : rxpkttotaldis| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group3 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_rxepagrp3cnt`

* **Address**             : `0xF00 - 0xF3F(RO)`

* **Formula**             : `0xF00 + 32*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-31)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:32]`|`rxethgrp2cnt1`| cntoffset = 0 : rxpktjabber cntoffset = 1 : rxpktpausefrm cntoffset = 2 : rxpktoversize| `RC`| `0x0`| `0x0`|
|`[31:00]`|`rxethgrp2cnt0`| cntoffset = 0 : rxpktpcserr cntoffset = 1 : rxpktfragment cntoffset = 2 : rxpktundersize| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group4 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_rxepagrp4cnt`

* **Address**             : `0xF80 - 0xFBF(RO)`

* **Formula**             : `0xF80 + 32*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-31)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:32]`|`rxethgrp2cnt1`| cntoffset = 0 : rxpktbroadcast cntoffset = 1 : rxpkt64byte cntoffset = 2 : unused| `RC`| `0x0`| `0x0`|
|`[31:00]`|`rxethgrp2cnt0`| cntoffset = 0 : rxpktoverrun cntoffset = 1 : rxpktmulticast cntoffset = 2 : unused| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through MAC Address Control

* **Description**           

This register configures parameters per ethernet pass through port


* **RTL Instant Name**    : `ramrxepacfg0`

* **Address**             : `0xC60 - 0xC7F #The address format for these registers is 0xC30 + ethpid`

* **Formula**             : `0xC60 +  ethpid`

* **Where**               : 

    * `$ethpid(0-31): Ethernet port ID`

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:0]`|`ethpassmacadd`| Ethernet pass through MAC address used for Loop DA counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Port Enable and MTU Control

* **Description**           

This register configures parameters per ethernet pass through port


* **RTL Instant Name**    : `ramrxepacfg1`

* **Address**             : `0xC80 - 0xC9F #The address format for these registers is 0xC40 + ethpid`

* **Formula**             : `0xC80 +  ethpid`

* **Where**               : 

    * `$ethpid(0-31): Ethernet port ID`

* **Width**               : `21`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[20]`|`ethpassporten`| Ethernet pass port enable| `RW`| `0x0`| `0x0 1: Enable 0: Disable`|
|`[19]`|`ethpasspcserrdropen`| Ethernet PCS error drop enable| `RW`| `0x0`| `0x0 1: Enable drop PCS error packet 0: Disable drop PCS error packet`|
|`[18]`|`ethpassfcserrdropen`| Ethernet FCS error drop enable| `RW`| `0x0`| `0x0 1: Enable drop FCS error packet 0: Disable drop FCS error packet`|
|`[17]`|`ethpassundersizedropen`| Ethernet Undersize drop enable| `RW`| `0x0`| `0x0 1: Enable drop undersize packet 0: Disable drop undersize packet`|
|`[16]`|`ethpassoversizedropen`| Ethernet Oversize drop enable| `RW`| `0x0`| `0x0 1: Enable drop oversize packet 0: Disable drop oversize packet`|
|`[15]`|`ethpasspausefrmdropen`| Ethernet Pausefrm drop enable| `RW`| `0x0`| `0x0 1: Enable drop pause frame 0: Disable drop pause frame`|
|`[14]`|`ethpassdaloopdropen`| Ethernet Daloop drop enable| `RW`| `0x0`| `0x0 1: Enable drop DA loop packet 0: Disable drop DA loop packet`|
|`[13:0]`|`ethpassmtusize`| Ethernet pass through MTU size| `RW`| `0x2588`| `0x0 End: Begin:`|

###Ethernet Pass Through Global Diagnostic Enable Control

* **Description**           




* **RTL Instant Name**    : `upen_epadiagglb`

* **Address**             : `0x300`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `5`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[4]`|`diagselect_en`| Set 1 to select source data to tx Faceplate port from diagnostic engine, set 0 for  normal| `R/W`| `0x0`| `0x0`|
|`[3]`|`diagtengport24_en`| set 1 indicate Port 24 of group port 24 to 31 is 10G| `R/W`| `0x8100`| `0x8100`|
|`[2]`|`diagtengport16_en`| set 1 indicate Port 16 of group port 16 to 23 is 10G| `R/W`| `0x8100`| `0x8100`|
|`[1]`|`diagtengport08_en`| set 1 indicate Port 8 of group port 8 to 15 is 10G| `R/W`| `0x8100`| `0x8100`|
|`[0]`|`diagtengport00_en`| set 1 indicate Port 0 of group port 0 to 7 is 10G| `R/W`| `0x8100`| `0x8100 End: Begin:`|

###Ethernet Pass Through Diagnostic Port Enable and Speed Control

* **Description**           

This register configures parameters per ethernet pass through port


* **RTL Instant Name**    : `ramdiag1`

* **Address**             : `0x320 - 0x33F #The address format for these registers is 0x320 + ethpid`

* **Formula**             : `0x320 +  ethpid`

* **Where**               : 

    * `$ethpid(0-31): Ethernet port ID`

* **Width**               : `27`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[26:14]`|`diagethncospeed`| NCO of Ethernet clock corresponding to system clock 194.4Mhz ETH 10 Mbps: value 25 coressponding to 100% bandwidth, smaller value will be at lower bandwidth ETH 100 Mbps: value 125 coressponding to 100% bandwidth, smaller value will be at lower bandwidth ETH 1000 Mbps: value 625 coressponding to 100% bandwidth, smaller value will be at lower bandwidth ETH 10 Gbps: value 6250 coressponding to 100% bandwidth, smaller value will be at lower bandwidth| `RW`| `0x0`| `0x3CC`|
|`[13:1]`|`diagsysncospeed`| NCO of System clock 194.4Mhz corresponding to each ETH speed ETH 10 Mbps: value must be 3888 ETH 100 Mbps: value must be 1944 ETH 1000 Mbps: value must be 972 ETH 10 Gbps: value must be 972| `RW`| `0x0`| `0x3CC`|
|`[0]`|`diagpkgen_en`| Diagnostic packet generator enable| `RW`| `0x0`| `0x0 1: Enable generate packet from rx MAC40G to ETH transmit buffer 0: Disable generate packet from rx MAC40G to ETH transmit buffer`|

###Ethernet Pass Through Diagnostic Port Burst Control

* **Description**           

This register configures parameters per ethernet pass through port


* **RTL Instant Name**    : `ramdiag2`

* **Address**             : `0x360 - 0x37F #The address format for these registers is 0x360 + ethpid`

* **Formula**             : `0x360 +  ethpid`

* **Where**               : 

    * `$ethpid(0-31): Ethernet port ID`

* **Width**               : `20`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`diagburstbyte`| Number of byte generated per burst, value 0 will represent no burst| `RW`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Diagnostic Port Packet Length Control

* **Description**           

This register configures parameters per ethernet pass through port


* **RTL Instant Name**    : `ramdiag3`

* **Address**             : `0x3A0 - 0x3BF #The address format for these registers is 0x360 + ethpid`

* **Formula**             : `0x3A0 +  ethpid`

* **Where**               : 

    * `$ethpid(0-31): Ethernet port ID`

* **Width**               : `28`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:14]`|`diaggenofflen`| Offset packet length equal to Maximum packet lenth subtract minimum packet length| `RW`| `0x0`| `0x0`|
|`[13:0]`|`diaggenminlen`| Minimum packet length| `RW`| `0x80`| `0x0 End: Begin:`|

###Ethernet Pass Through Diagnostic Port Generate Packet Mode Control

* **Description**           

This register configures parameters per ethernet pass through port


* **RTL Instant Name**    : `ramdiag4`

* **Address**             : `0x3E0 - 0x3FF #The address format for these registers is 0x3E0 + ethpid`

* **Formula**             : `0x3E0 +  ethpid`

* **Where**               : 

    * `$ethpid(0-31): Ethernet port ID`

* **Width**               : `28`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:12]`|`diaggennonpldbyte`| Total none payload byte in an eth frame include gap preamble sfd fcs| `RW`| `0x1A`| `0x0`|
|`[11:4]`|`diaggenfixpat`| Fix pattern value used in fix data mode| `RW`| `0xAA`| `0x0`|
|`[3:2]`|`diaggenlenmode`| Diagnostic generate length mode 0: fix length mode equal to DiagGenMinLen 1: increase length mode 2: deccrease length mode 3: randome length mode| `RW`| `0x0`| `0x0`|
|`[1:0]`|`diaggendatmode`| Diagnostic generate data mode 0: Sequential data mode others: fix pattern mode| `RW`| `0x80`| `0x0 End: Begin:`|

###Ethernet Pass Through Diagnostic Port Monitor Packet Mode Control

* **Description**           

This register configures parameters per ethernet pass through port


* **RTL Instant Name**    : `ramdiag5`

* **Address**             : `0x400 - 0x41F #The address format for these registers is 0x400 + ethpid`

* **Formula**             : `0x400 +  ethpid`

* **Where**               : 

    * `$ethpid(0-31): Ethernet port ID`

* **Width**               : `28`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11:4]`|`diagmonfixpat`| Fix pattern value used in fix data mode| `RW`| `0xAA`| `0x0`|
|`[3:2]`|`diagmonlenmode`| Diagnostic monitoring length mode 0: fix length mode equal to DiagMonMinLen 1: increase length mode 2: deccrease length mode 3: randome length mode| `RW`| `0x0`| `0x0`|
|`[1:0]`|`diagmondatmode`| Diagnostic monitoring data mode 0: Sequential data mode others: fix pattern mode| `RW`| `0x80`| `0x0 End: Begin:`|

###Ethernet Pass Through Diagnostic Port Monitor Error Counter

* **Description**           

This register counter per ethernet pass through port


* **RTL Instant Name**    : `ramdiag6`

* **Address**             : `0x440 - 0x47F #The address format for these registers is 0x440 + ethpid`

* **Formula**             : `0x440 +  ethpid`

* **Where**               : 

    * `$ethpid(0-31): Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`errorcounter`| Error counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Diagnostic Port Monitor Packet Counter

* **Description**           

This register counter per ethernet pass through port


* **RTL Instant Name**    : `ramdiag7`

* **Address**             : `0x480 - 0x4BF #The address format for these registers is 0x480 + ethpid`

* **Formula**             : `0x480 +  ethpid`

* **Where**               : 

    * `$ethpid(0-31): Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetcounter`| Packet counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Diagnostic Port Monitor Byte Counter

* **Description**           

This register counter per ethernet pass through port


* **RTL Instant Name**    : `ramdiag7`

* **Address**             : `0x4C0 - 0x4FF #The address format for these registers is 0x4C0 + ethpid`

* **Formula**             : `0x4C0 +  ethpid`

* **Where**               : 

    * `$ethpid(0-31): Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`bytecounter`| Byte counter| `RW`| `0x0`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-12-19|AF6Project|Initial version|




##AF6CNC0022_RD_SGMII_Multirate
####Register Table

|Name|Address|
|-----|-----|
|`Pedit Block Version`|`0x0800`|
|`Auto-neg-group0 mode`|`0x0801`|
|`Auto-neg-group0 reset`|`0x0802`|
|`Auto-neg-group0 enable`|`0x0803`|
|`Config_speed-group0`|`0x0804`|
|`Auto-neg-group00 status`|`0x0805`|
|`Auto-neg-group01 status`|`0x0806`|
|`Auto-neg RX00-RX01 SGMII ability`|`0x0A00`|
|`Auto-neg RX02-RX03 SGMII ability`|`0x0A01`|
|`Auto-neg RX04-RX05 SGMII ability`|`0x0A02`|
|`Auto-neg RX06-RX07 SGMII ability`|`0x0A03`|
|`Auto-neg RX08-RX09 SGMII ability`|`0x0A04`|
|`Auto-neg RX10-RX11 SGMII ability`|`0x0A05`|
|`Auto-neg RX12-RX13 SGMII ability`|`0x0A06`|
|`Auto-neg RX14-RX15 SGMII ability`|`0x0A07`|
|`Auto-neg RX00-RX01 1000Basex ability`|`0x0A00`|
|`Auto-neg RX02-RX03 1000Basex ability`|`0x0A01`|
|`Auto-neg RX04-RX05 1000Basex ability`|`0x0A02`|
|`Auto-neg RX06-RX07 1000Basex ability`|`0x0A03`|
|`Auto-neg RX08-RX09 1000Basex ability`|`0x0A04`|
|`Auto-neg RX10-RX11 1000Basex ability`|`0x0A05`|
|`Auto-neg RX12-RX13 1000Basex ability`|`0x0A06`|
|`Auto-neg RX14-RX15 1000Basex ability`|`0x0A07`|
|`TX IPG Config Part0`|`0x0810`|
|`TX IPG Config Part1`|`0x0811`|
|`Ge Link State Current Status`|`0x0808`|
|`Ge Link State Sticky`|`0x0809`|
|`Ge Link State Interrupt Enb`|`0x0815`|
|`GE Loss Of Synchronization Sticky`|`0x080B`|
|`GE Loss Of Synchronization Status`|`0x080A`|
|`GE Loss Of Synchronization Interrupt Enb`|`0x0818`|
|`GE Auto-neg Sticky`|`0x0807`|
|`GE Auto-neg Interrupt Enb`|`0x0817`|
|`GE Remote Fault Sticky`|`0x0819`|
|`GE Remote Fault Interrupt Enb`|`0x081A`|
|`GE Excessive Error Ratio Status`|`0x081C`|
|`GE Excessive Error Ratio Sticky`|`0x081D`|
|`GE Excessive Error Ratio Enble`|`0x081E`|
|`GE Excessive Error Ratio Threshold`|`0x0835`|
|`GE Interrupt OR`|`0x0830`|
|`GE Loss Of Sync AND MASK`|`0x0831`|
|`GE Auto_neg State Change AND MASK`|`0x0832`|
|`GE Remote Fault AND MASK`|`0x0833`|
|`GE Excessive Error Ratio AND MASK`|`0x0834`|
|`Ge Link State AND MASK`|`0x083A`|
|`GE 1000basex Force K30_7`|`0x0822`|
|`ENABLE TX 100base FX`|`0x0A08`|
|`Sticky Rx 100fx DETECT`|`0x0A16`|
|`CURRENT STATUS Rx 100fx DETECT`|`0x0A17`|
|`ENABLE FEFD INTTERUPT`|`0x0A18`|


###Pedit Block Version

* **Description**           

Pedit Block Version


* **RTL Instant Name**    : `version_pen`

* **Address**             : `0x0800`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`day`| day| `R_O`| `0x0E`| `0x0E`|
|`[23:16]`|`month`| month| `R_O`| `0x0C`| `0x0C`|
|`[15:08]`|`year`| year| `R_O`| `0x0D`| `0x0D`|
|`[07:00]`|`number`| number| `R_O`| `0x01`| `0x01`|

###Auto-neg-group0 mode

* **Description**           

Select Auto-neg mode for 1000Basex or SGMII


* **RTL Instant Name**    : `an_mod_pen0`

* **Address**             : `0x0801`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`tx_enb0`| Enable TX side bit per port, bit[16] port 0, bit[31] port 15<br>{1} : enable <br>{0} : disable| `R/W`| `0x0`| `0x0`|
|`[15:00]`|`an_mod0`| Auto-neg mode, bit per port, bit[0] port 0, bit[15] port 15<br>{1} : SGMII <br>{0} : 1000basex| `R/W`| `0x0`| `0x0`|

###Auto-neg-group0 reset

* **Description**           

Restart Auto-neg process of group0 from port 0 to port 15


* **RTL Instant Name**    : `an_rst_pen0`

* **Address**             : `0x0802`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`an_rst0`| Auto-neg reset, sw write 1 then write 0 to restart auto-neg bit per port, bit[0] port 0, bit[15] port 15<br>{1} : restart on <br>{0} : restart off| `R/W`| `0x0`| `0x0`|

###Auto-neg-group0 enable

* **Description**           

enable/disbale Auto-neg of group 0 from port 0 to port 15


* **RTL Instant Name**    : `an_enb_pen0`

* **Address**             : `0x0803`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`an_enb0`| Auto-neg enb , bit per port, bit[0] port 0, bit[15] port 15<br>{1} : enable Auto-neg <br>{0} : disable Auto-neg| `R/W`| `0xFFFF`| `0xFFFF`|

###Config_speed-group0

* **Description**           

configure speed when disable Auto-neg of group 0 from port 0 to 15


* **RTL Instant Name**    : `an_spd_pen0`

* **Address**             : `0x0804`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`cfg_spd15`| Speed port 15<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[29:28]`|`cfg_spd14`| Speed port 14<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[27:26]`|`cfg_spd13`| Speed port 13<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[25:24]`|`cfg_spd12`| Speed port 12<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[23:22]`|`cfg_spd11`| Speed port 11<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[21:20]`|`cfg_spd10`| Speed port 10<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[19:18]`|`cfg_spd09`| Speed port 9<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[17:16]`|`cfg_spd08`| Speed port 8<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[15:14]`|`cfg_spd07`| Speed port 7<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[13:12]`|`cfg_spd06`| Speed port 6<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[11:10]`|`cfg_spd05`| Speed port 5<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[09:08]`|`cfg_spd04`| Speed port 4<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[07:06]`|`cfg_spd03`| Speed port 3<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[05:04]`|`cfg_spd02`| Speed port 2<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[03:02]`|`cfg_spd01`| Speed port 1<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[01:00]`|`cfg_spd00`| Speed port 0<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|

###Auto-neg-group00 status

* **Description**           

status of Auto-neg of group0 from port 0 to port 7


* **RTL Instant Name**    : `an_sta_pen00`

* **Address**             : `0x0805`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`an_sta07`| Speed port 7<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[27:24]`|`an_sta06`| Speed port 6<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[23:20]`|`an_sta05`| Speed port 5<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[19:16]`|`an_sta04`| Speed port 4<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[15:12]`|`an_sta03`| Speed port 3<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[11:08]`|`an_sta02`| Speed port 2<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[07:04]`|`an_sta01`| Speed port 1<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[03:00]`|`an_sta00`| Speed port 0<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|

###Auto-neg-group01 status

* **Description**           

status of Auto-neg of group0 from port 8 to port 15


* **RTL Instant Name**    : `an_sta_pen01`

* **Address**             : `0x0806`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`an_sta15`| Speed port 15<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[27:24]`|`an_sta14`| Speed port 14<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[23:20]`|`an_sta13`| Speed port 13<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[19:16]`|`an_sta12`| Speed port 12<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[15:12]`|`an_sta11`| Speed port 11<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[11:08]`|`an_sta10`| Speed port 10<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[07:04]`|`an_sta09`| Speed port 09<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[03:00]`|`an_sta09`| Speed port 09<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|

###Auto-neg RX00-RX01 SGMII ability

* **Description**           

RX ability of RX-Port00-01 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen00`

* **Address**             : `0x0A00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`link01`| Link status of port1<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[30:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`duplex01`| duplex mode of port1<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[27:26]`|`speed01`| speed of SGMII of port1<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[25:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`link00`| Link status of port0<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[14:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`duplex00`| duplex mode of port0<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[11:10]`|`speed00`| speed of SGMII of port0<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[09:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX02-RX03 SGMII ability

* **Description**           

RX ability of RX-Port02-03 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen01`

* **Address**             : `0x0A01`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`link03`| Link status of port3<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[30:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`duplex03`| duplex mode of port3<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[27:26]`|`speed03`| speed of SGMII of port3<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[25:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`link02`| Link status of port2<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[14:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`duplex02`| duplex mode of port2<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[11:10]`|`speed02`| speed of SGMII of port2<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[09:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX04-RX05 SGMII ability

* **Description**           

RX ability of RX-Port04-05 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen02`

* **Address**             : `0x0A02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`link05`| Link status of port5<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[30:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`duplex05`| duplex mode of port5<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[27:26]`|`speed05`| speed of SGMII of port5<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[25:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`link04`| Link status of port4<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[14:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`duplex04`| duplex mode of port4<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[11:10]`|`speed04`| speed of SGMII of port4<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[09:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX06-RX07 SGMII ability

* **Description**           

RX ability of RX-Port06-07 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen03`

* **Address**             : `0x0A03`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`link07`| Link status of port7<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[30:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`duplex07`| duplex mode of port7<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[27:26]`|`speed07`| speed of SGMII of port7<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[25:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`link06`| Link status of port6<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[14:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`duplex06`| duplex mode of port6<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[11:10]`|`speed06`| speed of SGMII of port6<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[09:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX08-RX09 SGMII ability

* **Description**           

RX ability of RX-Port08-09 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen04`

* **Address**             : `0x0A04`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`link09`| Link status of port9<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[30:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`duplex09`| duplex mode of port9<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[27:26]`|`speed09`| speed of SGMII of port9<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[25:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`link08`| Link status of port8<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[14:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`duplex08`| duplex mode of port8<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[11:10]`|`speed08`| speed of SGMII of port8<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[09:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX10-RX11 SGMII ability

* **Description**           

RX ability of RX-Port10-11 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen05`

* **Address**             : `0x0A05`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`link11`| Link status of port11<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[30:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`duplex11`| duplex mode of port11<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[27:26]`|`speed11`| speed of SGMII of port11<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[25:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`link10`| Link status of port10<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[14:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`duplex10`| duplex mode of port10<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[11:10]`|`speed10`| speed of SGMII of port10<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[09:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX12-RX13 SGMII ability

* **Description**           

RX ability of RX-Port12-13 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen06`

* **Address**             : `0x0A06`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`link13`| Link status of port13<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[30:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`duplex13`| duplex mode of port13<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[27:26]`|`speed13`| speed of SGMII of port13<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[25:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`link12`| Link status of port12<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[14:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`duplex12`| duplex mode of port12<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[11:10]`|`speed12`| speed of SGMII of port12<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[09:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX14-RX15 SGMII ability

* **Description**           

RX ability of RX-Port14-15 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen07`

* **Address**             : `0x0A07`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`link15`| Link status of port15<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[30:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`duplex15`| duplex mode of port15<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[27:26]`|`speed15`| speed of SGMII of port15<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[25:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`link14`| Link status of port14<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[14:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`duplex14`| duplex mode of port14<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[11:10]`|`speed14`| speed of SGMII of port14<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[09:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX00-RX01 1000Basex ability

* **Description**           

RX ability of RX-Port00-01 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen00`

* **Address**             : `0x0A00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`np01`| Next page of port1| `R_O`| `0x0`| `0x0`|
|`[30:30]`|`ack01`| Acknowledge of port1| `R_O`| `0x0`| `0x0`|
|`[29:28]`|`refault01`| Remote Fault of port1<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[27:23]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`hduplex01`| Half duplex of port1| `R_O`| *n/a*| *n/a*|
|`[21:21]`|`fduplex01`| Full duplex of port1| `R_O`| *n/a*| *n/a*|
|`[20:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`np00`| Next page of port0| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`ack00`| Acknowledge of port0| `R_O`| `0x0`| `0x0`|
|`[13:12]`|`refault00`| Remote Fault of port0<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[11:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`hduplex00`| Half duplex of port0| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fduplex00`| Full duplex of port0| `R_O`| `0x0`| `0x0`|
|`[04:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX02-RX03 1000Basex ability

* **Description**           

RX ability of RX-Port02-03 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen01`

* **Address**             : `0x0A01`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`np03`| Next page  of port3| `R_O`| `0x0`| `0x0`|
|`[30:30]`|`ack03`| Acknowledge of port3| `R_O`| `0x0`| `0x0`|
|`[29:28]`|`refault03`| Remote Fault of port3<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[27:23]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`hduplex03`| Half duplex of port3| `R_O`| *n/a*| *n/a*|
|`[21:21]`|`fduplex03`| Full duplex of port3| `R_O`| *n/a*| *n/a*|
|`[20:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`np02`| Next page of port2| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`ack02`| Acknowledge of port2| `R_O`| `0x0`| `0x0`|
|`[13:12]`|`refault02`| Remote Fault of port2<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[11:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`hduplex02`| Half duplex of port2| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fduplex02`| Full duplex of port2| `R_O`| `0x0`| `0x0`|
|`[04:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX04-RX05 1000Basex ability

* **Description**           

RX ability of RX-Port04-05 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen02`

* **Address**             : `0x0A02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`np05`| Next page  of port5| `R_O`| `0x0`| `0x0`|
|`[30:30]`|`ack05`| Acknowledge of port5| `R_O`| `0x0`| `0x0`|
|`[29:28]`|`refault05`| Remote Fault of port5<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[27:23]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`hduplex05`| Half duplex of port5| `R_O`| *n/a*| *n/a*|
|`[21:21]`|`fduplex05`| Full duplex of port5| `R_O`| *n/a*| *n/a*|
|`[20:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`np04`| Next page of port4| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`ack04`| Acknowledge of port4| `R_O`| `0x0`| `0x0`|
|`[13:12]`|`refault04`| Remote Fault of port4<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[11:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`hduplex04`| Half duplex of port4| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fduplex04`| Full duplex of port4| `R_O`| `0x0`| `0x0`|
|`[04:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX06-RX07 1000Basex ability

* **Description**           

RX ability of RX-Port06-07 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen03`

* **Address**             : `0x0A03`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`np07`| Next page  of port7| `R_O`| `0x0`| `0x0`|
|`[30:30]`|`ack07`| Acknowledge of port7| `R_O`| `0x0`| `0x0`|
|`[29:28]`|`refault07`| Remote Fault of port7<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[27:23]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`hduplex07`| Half duplex of port7| `R_O`| *n/a*| *n/a*|
|`[21:21]`|`fduplex07`| Full duplex of port7| `R_O`| *n/a*| *n/a*|
|`[20:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`np06`| Next page of port6| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`ack06`| Acknowledge of port6| `R_O`| `0x0`| `0x0`|
|`[13:12]`|`refault06`| Remote Fault of port6<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[11:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`hduplex06`| Half duplex of port6| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fduplex06`| Full duplex of port6| `R_O`| `0x0`| `0x0`|
|`[04:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX08-RX09 1000Basex ability

* **Description**           

RX ability of RX-Port08-09 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen04`

* **Address**             : `0x0A04`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`np09`| Next page  of port9| `R_O`| `0x0`| `0x0`|
|`[30:30]`|`ack09`| Acknowledge of port9| `R_O`| `0x0`| `0x0`|
|`[29:28]`|`refault09`| Remote Fault of port9<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[27:23]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`hduplex09`| Half duplex of port9| `R_O`| *n/a*| *n/a*|
|`[21:21]`|`fduplex09`| Full duplex of port9| `R_O`| *n/a*| *n/a*|
|`[20:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`np08`| Next page of port8| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`ack08`| Acknowledge of port8| `R_O`| `0x0`| `0x0`|
|`[13:12]`|`refault08`| Remote Fault of port8<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[11:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`hduplex08`| Half duplex of port8| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fduplex08`| Full duplex of port8| `R_O`| `0x0`| `0x0`|
|`[04:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX10-RX11 1000Basex ability

* **Description**           

RX ability of RX-Port10-11 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen05`

* **Address**             : `0x0A05`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`np11`| Next page  of port11| `R_O`| `0x0`| `0x0`|
|`[30:30]`|`ack11`| Acknowledge of port11| `R_O`| `0x0`| `0x0`|
|`[29:28]`|`refault11`| Remote Fault of port11<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[27:23]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`hduplex11`| Half duplex of port11| `R_O`| *n/a*| *n/a*|
|`[21:21]`|`fduplex11`| Full duplex of port11| `R_O`| *n/a*| *n/a*|
|`[20:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`np10`| Next page of port10| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`ack10`| Acknowledge of port10| `R_O`| `0x0`| `0x0`|
|`[13:12]`|`refault10`| Remote Fault of port10<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[11:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`hduplex10`| Half duplex of port10| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fduplex10`| Full duplex of port10| `R_O`| `0x0`| `0x0`|
|`[04:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX12-RX13 1000Basex ability

* **Description**           

RX ability of RX-Port12-13 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen06`

* **Address**             : `0x0A06`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`np13`| Next page  of port13| `R_O`| `0x0`| `0x0`|
|`[30:30]`|`ack13`| Acknowledge of port13| `R_O`| `0x0`| `0x0`|
|`[29:28]`|`refault13`| Remote Fault of port13<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[27:23]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`hduplex13`| Half duplex of port13| `R_O`| *n/a*| *n/a*|
|`[21:21]`|`fduplex13`| Full duplex of port13| `R_O`| *n/a*| *n/a*|
|`[20:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`np12`| Next page of port12| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`ack12`| Acknowledge of port12| `R_O`| `0x0`| `0x0`|
|`[13:12]`|`refault12`| Remote Fault of port12<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[11:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`hduplex12`| Half duplex of port12| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fduplex12`| Full duplex of port12| `R_O`| `0x0`| `0x0`|
|`[04:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX14-RX15 1000Basex ability

* **Description**           

RX ability of RX-Port12-13 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen07`

* **Address**             : `0x0A07`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`np15`| Next page  of port15| `R_O`| `0x0`| `0x0`|
|`[30:30]`|`ack15`| Acknowledge of port15| `R_O`| `0x0`| `0x0`|
|`[29:28]`|`refault15`| Remote Fault of port15<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[27:23]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`hduplex15`| Half duplex of port15| `R_O`| *n/a*| *n/a*|
|`[21:21]`|`fduplex15`| Full duplex of port15| `R_O`| *n/a*| *n/a*|
|`[20:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`np14`| Next page of port14| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`ack14`| Acknowledge of port14| `R_O`| `0x0`| `0x0`|
|`[13:12]`|`refault14`| Remote Fault of port14<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[11:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`hduplex14`| Half duplex of port14| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fduplex14`| Full duplex of port14| `R_O`| `0x0`| `0x0`|
|`[04:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###TX IPG Config Part0

* **Description**           

TX IPG Config Part0, port#00 to port#07


* **RTL Instant Name**    : `TX_IPG_Config_Part0`

* **Address**             : `0x0810`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`txipg_port07`| TX IPG port07, IPG = txipg_port07 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[27:24]`|`txipg_port06`| TX IPG port06, IPG = txipg_port06 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[23:20]`|`txipg_port05`| TX IPG port05, IPG = txipg_port05 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[19:16]`|`txipg_port04`| TX IPG port04, IPG = txipg_port04 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[15:12]`|`txipg_port03`| TX IPG port03, IPG = txipg_port03 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[11:08]`|`txipg_port02`| TX IPG port02, IPG = txipg_port02 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[07:04]`|`txipg_port01`| TX IPG port01, IPG = txipg_port01 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[03:00]`|`txipg_port00`| TX IPG port00, IPG = txipg_port00 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|

###TX IPG Config Part1

* **Description**           

TX IPG Config Part1, port#08 to port#15


* **RTL Instant Name**    : `TX_IPG_Config_Part1`

* **Address**             : `0x0811`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`txipg_port15`| TX IPG port15, IPG = txipg_port15 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[27:24]`|`txipg_port14`| TX IPG port14, IPG = txipg_port14 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[23:20]`|`txipg_port13`| TX IPG port13, IPG = txipg_port13 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[19:16]`|`txipg_port12`| TX IPG port12, IPG = txipg_port12 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[15:12]`|`txipg_port11`| TX IPG port11, IPG = txipg_port11 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[11:08]`|`txipg_port10`| TX IPG port10, IPG = txipg_port10 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[07:04]`|`txipg_port09`| TX IPG port09, IPG = txipg_port09 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|
|`[03:00]`|`txipg_port08`| TX IPG port08, IPG = txipg_port08 * 2, default value is 6, it means IPG is 12bytes| `R/W`| `0x6`| `0x6`|

###Ge Link State Current Status

* **Description**           

Link status of group 0 from port 0  to port 15


* **RTL Instant Name**    : `Ge_Link_State_Current_Status`

* **Address**             : `0x0808`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`lnk_sta_cur`| Link status, bit per port, bit[0] port  0, bit[7] port 7 ... , bit[15] port 15<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|

###Ge Link State Sticky

* **Description**           

Link status of group 0 from port 0  to port 15


* **RTL Instant Name**    : `Ge_Link_State_Sticky`

* **Address**             : `0x0809`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`lnk_sta_stk`| Link status, bit per port, bit[0] port  0, bit[7] port 7 ... , bit[15] port 15<br>{1} : link up <br>{0} : link down| `W1C`| `0x0`| `0x0`|

###Ge Link State Interrupt Enb

* **Description**           

Link status of group 0 from port 0  to port 15


* **RTL Instant Name**    : `Ge_Link_State_Interrupt_enb`

* **Address**             : `0x0815`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`lnk_sta_enb`| Link status, bit per port, bit[0] port  0, bit[7] port 7 ... , bit[15] port 15<br>{1} : enb <br>{0} : dis| `R/W`| `0x0`| `0x0`|

###GE Loss Of Synchronization Sticky

* **Description**           

Sticky state change Loss of synchronization


* **RTL Instant Name**    : `los_sync_stk_pen`

* **Address**             : `0x080B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`gelossofsync_stk`| Sticky state change of Loss Of Synchronization, bit per port, bit0 is port0, bit7 is port7 ,..., bit15 is port15| `W1C`| `0x0`| `0x0`|

###GE Loss Of Synchronization Status

* **Description**           

Current status of Loss of synchronization


* **RTL Instant Name**    : `los_sync_pen`

* **Address**             : `0x080A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`gelossofsync_cur`| Current status of Loss Of Synchronization, bit per port, bit0 is port0, bit7 is port7 ,..., bit15 is port15| `R_O`| `0x0`| `0x0`|

###GE Loss Of Synchronization Interrupt Enb

* **Description**           

Interrupt enable of Loss of synchronization


* **RTL Instant Name**    : `los_sync_int_enb_pen`

* **Address**             : `0x0818`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`gelossofsync_enb`| Interrupt enable of Loss of synchronization, bit per port, bit0 is port0, bit7 is port7 ,..., bit15 is port15| `R/W`| `0x0`| `0x0`|

###GE Auto-neg Sticky

* **Description**           

Sticky state change Auto-neg


* **RTL Instant Name**    : `an_sta_stk_pen`

* **Address**             : `0x0807`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`geauto_neg_stk`| Sticky state change of Auto-neg , bit per port, bit0 is port0, bit7 is port7 ,..., bit15 is port15| `W1C`| `0x0`| `0x0`|

###GE Auto-neg Interrupt Enb

* **Description**           

Interrupt enable of Loss of synchronization


* **RTL Instant Name**    : `an_int_enb_pen`

* **Address**             : `0x0817`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`geauto_neg_enb`| Interrupt enable of Auto-neg, bit per port, bit0 is port0, bit7 is port7 ,... , bit15 is port15| `R/W`| `0x0`| `0x0`|

###GE Remote Fault Sticky

* **Description**           

Sticky state change Remote Fault


* **RTL Instant Name**    : `an_rfi_stk_pen`

* **Address**             : `0x0819`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`geremote_fault_stk`| Sticky state change of Remote Fault , bit per port, bit0 is port0, bit7 is port7 ,..., bit15 is port15| `W1C`| `0x0`| `0x0`|

###GE Remote Fault Interrupt Enb

* **Description**           

Interrupt enable of Remote Fault


* **RTL Instant Name**    : `an_rfi_int_enb_pen`

* **Address**             : `0x081A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`geremote_fault_enb`| Interrupt enable of Remote Fault, bit per port, bit0 is port0, bit7 is port7 , bit15 is port15| `R/W`| `0x0`| `0x0`|

###GE Excessive Error Ratio Status

* **Description**           

Interrupt Excessive Error Ratio Status


* **RTL Instant Name**    : `rxexcer_sta_pen`

* **Address**             : `0x081C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`rxexcer_sta`| Interrupt status of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7| `RO`| `0x0`| `0x0`|

###GE Excessive Error Ratio Sticky

* **Description**           

Interrupt Excessive Error Ratio Sticky


* **RTL Instant Name**    : `rxexcer_stk_pen`

* **Address**             : `0x081D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`rxexcer_stk`| Interrupt sticky of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7| `W1C`| `0x0`| `0x0`|

###GE Excessive Error Ratio Enble

* **Description**           

Interrupt Excessive Error Ratio Enable


* **RTL Instant Name**    : `rxexcer_enb_pen`

* **Address**             : `0x081E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`rxexcer_enb`| Interrupt Enable of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7| `R/W`| `0x0`| `0x0`|

###GE Excessive Error Ratio Threshold

* **Description**           

Interrupt Excessive Error Ratio Threshold


* **RTL Instant Name**    : `rxexcer_thres_pen`

* **Address**             : `0x0835`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`rxexcer_thres_up`| Upper threshold of Excessive Error Ratio, max value is 32767 Excessive error alarm will raise when value error counter is greater than upper threshold| `R/W`| `0xF`| `0xF`|
|`[15:00]`|`rxexcer_thres_low`| Lower threshold of Excessive Error Ratio, max value is 32767 Excessive error alarm will clear when value error counter is lower than upper threshold| `R/W`| `0x9`| `0x9`|

###GE Interrupt OR

* **Description**           

Interrupt OR of all port per interrupt type


* **RTL Instant Name**    : `GE_Interrupt_OR`

* **Address**             : `0x0830`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`link_sta_int_or`| Interrupt OR of Ge Link Sate| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fx100base_int_or`| Interrupt OR of 100Base Fx| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`rxexcer_int_or`| Interrupt OR of Excessive Error Ratio| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`an_rfi_int_or`| Interrupt OR of Remote fault| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`ant_int_or`| Interrupt OR of Auto-neg state change| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`los_sync_int_or`| Interrupt OR of Loss of sync| `R_O`| `0x0`| `0x0`|

###GE Loss Of Sync AND MASK

* **Description**           

Interrupt Loss of sync current status AND MASK


* **RTL Instant Name**    : `GE_Loss_Of_Sync_AND_MASK`

* **Address**             : `0x0831`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`gelossofsync_and_mask`| Interrupt Loss of sync, bit per port, bit0 is port0, bit7 is port7| `R_O`| `0x0`| `0x0`|

###GE Auto_neg State Change AND MASK

* **Description**           

Interrupt Auto-neg state change current status AND MASK


* **RTL Instant Name**    : `GE_Auto_neg_State_Change_AND_MASK`

* **Address**             : `0x0832`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`geauto_neg_and_mask`| Interrupt Auto-neg state change, bit per port, bit0 is port0, bit7 is port7| `R_O`| `0x0`| `0x0`|

###GE Remote Fault AND MASK

* **Description**           

Interrupt Remote Fault current status AND MASK


* **RTL Instant Name**    : `GE_Remote_Fault_AND_MASK`

* **Address**             : `0x0833`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`geremote_fault_and_mask`| Interrupt Enable of Remote Fault, bit per port, bit0 is port0, bit7 is port7| `R_O`| `0x0`| `0x0`|

###GE Excessive Error Ratio AND MASK

* **Description**           

Interrupt Excessive Error Ratio current status AND MASK


* **RTL Instant Name**    : `GE_Excessive_Error_Ratio_AND_MASK`

* **Address**             : `0x0834`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`rxexcer_and_mask`| Interrupt Enable of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7| `R_O`| `0x0`| `0x0`|

###Ge Link State AND MASK

* **Description**           

Interrupt Excessive Error Ratio current status AND MASK


* **RTL Instant Name**    : `Ge_Link_State_AND_MASK`

* **Address**             : `0x083A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`linksta_and_mask`| Ge Link State AND MASK, bit per port, bit0 is port0, bit7 is port7| `R_O`| `0x0`| `0x0`|

###GE 1000basex Force K30_7

* **Description**           

Forcing countinuous K30.7 for 1000basex mode


* **RTL Instant Name**    : `GE_1000basex_Force_K30_7`

* **Address**             : `0x0822`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`tx_fk30_7`| Force K30.7 code, bit per port, bit0 is port0, bit7 is port7<br>{1} : enable force K30.7| `R/W`| `0x0`| `0x0`|

###ENABLE TX 100base FX

* **Description**           

Interrupt enable of Remote Fault


* **RTL Instant Name**    : `fx_txenb`

* **Address**             : `0x0A08`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:00]`|`fx_entx`| (1) is enable, (0) is disable,  bit0 is port0, bit15 is port15| `R/W`| `0x0`| `0x0`|

###Sticky Rx 100fx DETECT

* **Description**           

Interrupt Alarm of Remote Fault and Local fault


* **RTL Instant Name**    : `fx_stk_pen`

* **Address**             : `0x0A16`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`fx_stk_rf15`| state change sticky far end detect port 15 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[30:30]`|`fx_stk_lf15`| state change sticky local fault port 15| `RWC`| `0x0`| `0x0`|
|`[29:29]`|`fx_stk_rf14`| state change sticky far end detect port 14 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[28:28]`|`fx_stk_lf14`| state change sticky local fault port 14| `RWC`| `0x0`| `0x0`|
|`[27:27]`|`fx_stk_rf13`| state change sticky far end detect port 13 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[26:26]`|`fx_stk_lf13`| state change sticky local fault port 13| `RWC`| `0x0`| `0x0`|
|`[25:25]`|`fx_stk_rf12`| state change sticky far end detect port 12 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[24:24]`|`fx_stk_lf12`| state change sticky local fault port 12| `RWC`| `0x0`| `0x0`|
|`[23:23]`|`fx_stk_rf11`| state change sticky far end detect port 11 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[22:22]`|`fx_stk_lf11`| state change sticky local fault port 11| `RWC`| `0x0`| `0x0`|
|`[21:21]`|`fx_stk_rf10`| state change sticky far end detect port 10 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[20:20]`|`fx_stk_lf10`| state change sticky local fault port 10| `RWC`| `0x0`| `0x0`|
|`[19:19]`|`fx_stk_rf9`| state change sticky far end detect port 9 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[18:18]`|`fx_stk_lf9`| state change sticky local fault port 9| `RWC`| `0x0`| `0x0`|
|`[17:17]`|`fx_stk_rf8`| state change sticky far end detect port 8 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[16:16]`|`fx_stk_lf8`| state change sticky local fault port 8| `RWC`| `0x0`| `0x0`|
|`[15:15]`|`fx_stk_rf7`| state change sticky far end detect port 7 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[14:14]`|`fx_stk_lf7`| state change sticky local fault port 7| `RWC`| `0x0`| `0x0`|
|`[13:13]`|`fx_stk_rf7`| state change sticky far end detect port 6 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[12:12]`|`fx_stk_lf6`| state change sticky local fault port 6| `RWC`| `0x0`| `0x0`|
|`[11:11]`|`fx_stk_rf5`| state change sticky far end detect port 5 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[10:10]`|`fx_stk_lf5`| state change sticky local fault port 5| `RWC`| `0x0`| `0x0`|
|`[09:09]`|`fx_stk_rf4`| state change sticky far end detect port 4 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[08:08]`|`fx_stk_lf4`| state change sticky local fault port 4| `RWC`| `0x0`| `0x0`|
|`[07:07]`|`fx_stk_rf3`| state change sticky far end detect port 3 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[06:06]`|`fx_stk_lf3`| state change sticky local fault port 3| `RWC`| `0x0`| `0x0`|
|`[05:05]`|`fx_stk_rf2`| state change sticky far end detect port 2 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[04:04]`|`fx_stk_lf2`| state change sticky local fault port 2| `RWC`| `0x0`| `0x0`|
|`[03:03]`|`fx_stk_rf1`| state change sticky far end detect port 1 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[02:02]`|`fx_stk_lf1`| state change sticky local fault port 1| `RWC`| `0x0`| `0x0`|
|`[01:01]`|`fx_stk_rf0`| state change sticky far end detect port 0 ( remote fault)| `RWC`| `0x0`| `0x0`|
|`[00:00]`|`fx_stk_lf0`| state change sticky local fault port 0| `RWC`| `0x0`| `0x0`|

###CURRENT STATUS Rx 100fx DETECT

* **Description**           

Interrupt current status of Remote Fault


* **RTL Instant Name**    : `fx_alm_pen`

* **Address**             : `0x0A17`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`fx_alm_rf15`| current status far end detect port 15 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[30:30]`|`fx_alm_lf15`| current status local fault port 15| `RO`| `0x0`| `0x0`|
|`[29:29]`|`fx_alm_rf14`| current status far end detect port 14 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[28:28]`|`fx_alm_lf14`| current status local fault port 14| `RO`| `0x0`| `0x0`|
|`[27:27]`|`fx_alm_rf13`| current status far end detect port 13 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[26:26]`|`fx_alm_lf13`| current status local fault port 13| `RO`| `0x0`| `0x0`|
|`[25:25]`|`fx_alm_rf12`| current status far end detect port 12 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[24:24]`|`fx_alm_lf12`| current status local fault port 12| `RO`| `0x0`| `0x0`|
|`[23:23]`|`fx_alm_rf11`| current status far end detect port 11 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[22:22]`|`fx_alm_lf11`| current status local fault port 11| `RO`| `0x0`| `0x0`|
|`[21:21]`|`fx_alm_rf10`| current status far end detect port 10 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[20:20]`|`fx_alm_lf10`| current status local fault port 10| `RO`| `0x0`| `0x0`|
|`[19:19]`|`fx_alm_rf9`| current status far end detect port 9 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[18:18]`|`fx_alm_lf9`| current status local fault port 9| `RO`| `0x0`| `0x0`|
|`[17:17]`|`fx_alm_rf8`| current status far end detect port 8 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[16:16]`|`fx_alm_lf8`| current status local fault port 8| `RO`| `0x0`| `0x0`|
|`[15:15]`|`fx_alm_rf7`| current status far end detect port 7 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[14:14]`|`fx_alm_lf7`| current status local fault port 7| `RO`| `0x0`| `0x0`|
|`[13:13]`|`fx_alm_rf7`| current status far end detect port 6 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[12:12]`|`fx_alm_lf6`| current status local fault port 6| `RO`| `0x0`| `0x0`|
|`[11:11]`|`fx_alm_rf5`| current status far end detect port 5 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[10:10]`|`fx_alm_lf5`| current status local fault port 5| `RO`| `0x0`| `0x0`|
|`[09:09]`|`fx_alm_rf4`| current status far end detect port 4 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[08:08]`|`fx_alm_lf4`| current status local fault port 4| `RO`| `0x0`| `0x0`|
|`[07:07]`|`fx_alm_rf3`| current status far end detect port 3 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[06:06]`|`fx_alm_lf3`| current status local fault port 3| `RO`| `0x0`| `0x0`|
|`[05:05]`|`fx_alm_rf2`| current status far end detect port 2 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[04:04]`|`fx_alm_lf2`| current status local fault port 2| `RO`| `0x0`| `0x0`|
|`[03:03]`|`fx_alm_rf1`| current status far end detect port 1 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[02:02]`|`fx_alm_lf1`| current status local fault port 1| `RO`| `0x0`| `0x0`|
|`[01:01]`|`fx_alm_rf0`| current status far end detect port 0 ( remote fault)| `RO`| `0x0`| `0x0`|
|`[00:00]`|`fx_alm_lf0`| current status local fault port 0| `RO`| `0x0`| `0x0`|

###ENABLE FEFD INTTERUPT

* **Description**           

Interrupt enable of Remote Fault


* **RTL Instant Name**    : `fx_fefd_inten`

* **Address**             : `0x0A18`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`fx_int_msk_rf15`| enable interrupt far end detect port 15 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[30:30]`|`fx_int_msk_lf15`| enable interrupt local fault port 15| `R/W`| `0x0`| `0x0`|
|`[29:29]`|`fx_int_msk_rf14`| enable interrupt far end detect port 14 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`fx_int_msk_lf14`| enable interrupt local fault port 14| `R/W`| `0x0`| `0x0`|
|`[27:27]`|`fx_int_msk_rf13`| enable interrupt far end detect port 13 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[26:26]`|`fx_int_msk_lf13`| enable interrupt local fault port 13| `R/W`| `0x0`| `0x0`|
|`[25:25]`|`fx_int_msk_rf12`| enable interrupt far end detect port 12 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[24:24]`|`fx_int_msk_lf12`| enable interrupt local fault port 12| `R/W`| `0x0`| `0x0`|
|`[23:23]`|`fx_int_msk_rf11`| enable interrupt far end detect port 11 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[22:22]`|`fx_int_msk_lf11`| enable interrupt local fault port 11| `R/W`| `0x0`| `0x0`|
|`[21:21]`|`fx_int_msk_rf10`| enable interrupt far end detect port 10 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[20:20]`|`fx_int_msk_lf10`| enable interrupt local fault port 10| `R/W`| `0x0`| `0x0`|
|`[19:19]`|`fx_int_msk_rf9`| enable interrupt far end detect port 9 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[18:18]`|`fx_int_msk_lf9`| enable interrupt local fault port 9| `R/W`| `0x0`| `0x0`|
|`[17:17]`|`fx_int_msk_rf8`| enable interrupt far end detect port 8 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[16:16]`|`fx_int_msk_lf8`| enable interrupt local fault port 8| `R/W`| `0x0`| `0x0`|
|`[15:15]`|`fx_int_msk_rf7`| enable interrupt far end detect port 7 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[14:14]`|`fx_int_msk_lf7`| enable interrupt local fault port 7| `R/W`| `0x0`| `0x0`|
|`[13:13]`|`fx_int_msk_rf7`| enable interrupt far end detect port 6 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`fx_int_msk_lf6`| enable interrupt local fault port 6| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`fx_int_msk_rf5`| enable interrupt far end detect port 5 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`fx_int_msk_lf5`| enable interrupt local fault port 5| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`fx_int_msk_rf4`| enable interrupt far end detect port 4 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`fx_int_msk_lf4`| enable interrupt local fault port 4| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`fx_int_msk_rf3`| enable interrupt far end detect port 3 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`fx_int_msk_lf3`| enable interrupt local fault port 3| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`fx_int_msk_rf2`| enable interrupt far end detect port 2 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fx_int_msk_lf2`| enable interrupt local fault port 2| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fx_int_msk_rf1`| enable interrupt far end detect port 1 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fx_int_msk_lf1`| enable interrupt local fault port 1| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fx_int_msk_rf0`| enable interrupt far end detect port 0 ( remote fault)| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fx_int_msk_lf0`| enable interrupt local fault port 0| *n/a*| *n/a*| *n/a*|

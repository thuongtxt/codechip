## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-12-19|AF6Project|Initial version|




##AF6CNC0051_RD_GLB
####Register Table

|Name|Address|
|-----|-----|
|`Device Product ID`|`0x00_0000`|
|`Device Year Month Day Version ID`|`0x00_0002`|
|`Device Internal ID`|`0x00_0003`|
|`GLB Sub-Core Active`|`0x00_0001`|
|`Chip Temperature Sticky`|`0x00_0020`|
|`GLB Debug Mac Loopback Control`|`0x00_0013`|
|`GLB Eth1G Clock EER Squelching Control`|`0x00_0010`|
|`GLB Eth1G Clock Squelching Control`|`0x00_0011`|
|`GLB Eth10G Clock Squelching Control`|`0x00_0012`|


###Device Product ID

* **Description**           

This register indicates Product ID.


* **RTL Instant Name**    : `ProductID`

* **Address**             : `0x00_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`productid`| ProductId| `RO`| `0x60210051`| `0x60210051 End: Begin:`|

###Device Year Month Day Version ID

* **Description**           

This register indicates Year Month Day and main version ID.


* **RTL Instant Name**    : `YYMMDD_VerID`

* **Address**             : `0x00_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`year`| Year| `RO`| `0x16`| `0x16`|
|`[23:16]`|`month`| Month| `RO`| `0x01`| `0x01`|
|`[15:8]`|`day`| Day| `RO`| `0x30`| `0x30`|
|`[7:0]`|`version`| Version| `RO`| `0x40`| `0x40 End: Begin:`|

###Device Internal ID

* **Description**           

This register indicates internal ID.


* **RTL Instant Name**    : `InternalID`

* **Address**             : `0x00_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`internalid`| InternalID| `RO`| `0x0`| `0x0 End: Begin:`|

###GLB Sub-Core Active

* **Description**           

This register indicates the active ports.


* **RTL Instant Name**    : `Active`

* **Address**             : `0x00_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`subcoreen`| Enable per sub-core<br>{1}: Enable <br>{0}: Disable| `RW`| `0x00000000`| `0x00000000 End: Begin:`|

###Chip Temperature Sticky

* **Description**           

This register is used to sticky temperature


* **RTL Instant Name**    : `ChipTempSticky`

* **Address**             : `0x00_0020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `3`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2]`|`chipslr2tempstk`| Chip SLR2 temperature| `WC`| `0x0`| `0x0`|
|`[1]`|`chipslr1tempstk`| Chip SLR1 temperature| `WC`| `0x0`| `0x0`|
|`[0]`|`chipslr0tempstk`| Chip SLR0 temperature| `WC`| `0x0`| `0x0 End: Begin:`|

###GLB Debug Mac Loopback Control

* **Description**           

This register is used to configure debug loopback MAC parameters


* **RTL Instant Name**    : `DebugMacLoopControl`

* **Address**             : `0x00_0013`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`macmroloopout`| Set 1 for Mac 10G/1G/100Fx Loop Out| `RW`| `0x0`| `0x0`|
|`[0]`|`mac40gloopin`| Set 1 for Mac40gLoopIn| `RW`| `0x0`| `0x0 End: Begin:`|

###GLB Eth1G Clock EER Squelching Control

* **Description**           

This register is used to configure debug PW parameters


* **RTL Instant Name**    : `GlbEth1GClkEerSquelControl`

* **Address**             : `0x00_0010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`ethgeexcesserrrateschelen`| Each Bit represent for each ETH port, value each bit is as below 1: Disable 8Khz refout when GE Excessive Error Rate occur 0: Enable 8Khz refout when GE Excessive Error Rate occur| `RW`| `0x0`| `0x0 End: Begin:`|

###GLB Eth1G Clock Squelching Control

* **Description**           

This register is used to configure debug PW parameters


* **RTL Instant Name**    : `GlbEth1GClkLinkDownSquelControl`

* **Address**             : `0x00_0011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`ethgelinkdownschelen`| Each Bit represent for each ETH port, value each bit is as below 1: Disable 8Khz refout when GE link down occur 0: Enable 8Khz refout when GE link down occur| `RW`| `0x0`| `0x0 Begin:`|

###GLB Eth10G Clock Squelching Control

* **Description**           

This register is used to configure debug PW parameters


* **RTL Instant Name**    : `GlbEth10GClkSquelControl`

* **Address**             : `0x00_0012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:12]`|`tengelocalfaultschelen`| Each Bit represent for each TenGe port0 and port8, value each bit is as below 1: Disable 8Khz refout when TenGe Local Fault occur 0: Enable 8Khz refout when TenGe Local Fault occur| `RW`| `0x0`| `0x0`|
|`[11:8]`|`tengeremotefaultschelen`| Each Bit represent for each TenGe port0 and port8, value each bit is as below 1: Disable 8Khz refout when TenGe Remote Fault occur 0: Enable 8Khz refout when TenGe Remote Fault occur| `RW`| `0x0`| `0x0`|
|`[7:4]`|`tengelossdatasyncschelen`| Each Bit represent for each TenGe port0 and port8, value each bit is as below 1: Disable 8Khz refout when TenGe Loss of Data Sync occur 0: Enable 8Khz refout when TenGe Loss of Data Sync occur| `RW`| `0x0`| `0x0`|
|`[3:0]`|`tengeexcesserrrateschelen`| Each Bit represent for each TenGe port0 and port8, value each bit is as below 1: Disable 8Khz refout when TenGe Excessive Error Rate occur 0: Enable 8Khz refout when TenGe Excessive Error Rate occur| `RW`| `0x0`| `0x0 End:`|

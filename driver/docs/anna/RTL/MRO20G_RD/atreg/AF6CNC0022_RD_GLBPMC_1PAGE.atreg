######################################################################################
# Arrive Technologies
# Register Description Document
# Revision 1.0 - 2015.Feb.03

######################################################################################
# This section is for guideline
# Refer to \\Hcmcserv3\Projects\internal_tools\atvn_vlibs\bin\rgm_gen\docs\REGISTER_DESCRIPTION_GUIDELINE.pdf
# File name: filename.atreg (atreg = ATVN register define)
# Comment out a line: Please use (# or //#) character at the beginning of the line
# Delimiter character: (%%)

######################################################################################
#| Access Policy| Use Case| Description                | Effect of a Write on Current Field Value  | Effect of a Read on Current Field Value | Read-back Value |
#| ------|-----------|---------------------------------| ------------------------------------------|-----------------------------------------|-----------------|
#| RO    | Status    | Read Only                       | No effect                | No effect            | Current Value |
#| RW    | Config    | Read, Write                     | Changed to written value | No effect            | Current value |
#| RC    | Status    | Read Clears All                 | No effect                | Sets all bits to 0   | Current value |
#| RS    | UNKNOW    | Read Sets All                   | No effect                | Sets all bits to 1   | Current value |
#| WRC   | UNKNOW    | Write, Read Clears All          | Changed to written value | Sets all bits to 0   | Current value |
#| WRS   | UNKNOW    | Write, Read Sets All            | Changed to written value | Sets all bits to 1   | Current value |
#| WC    | Status    | Write Clears All                | Sets all bits to 0       | No effect            | Current value |
#| WS    | Status    | Write Sets All                  | Sets all bits to 1       | No effect            | Current value |
#| WSRC  | UNKNOW    | Write Sets All, Read Clears All | Sets all bits to 1       | Sets all bits to 0   | Current value |
#| WCRS  | UNKNOW    | Write Clears All,Read Sets All  | Sets all bits to 0       | Sets all bits to 1   | Current value |
#| W1C   | Interrupt | Write 1 to Clear                | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current Value |
#| W1S   | Interrupt | Write 1 to Set                  | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W1T   | UNKNOW    | Write 1 to Toggle               | If the bit in the wr value is a 1, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W0C   | Interrupt | Write 0 to Clear                | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current value |
#| W0S   | Interrupt | Write 0 to Set                  | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W0T   | UNKNOW    | Write 0 to Toggle               | If the bit in the wr value is a 0, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W1SRC | UNKNOW    | Write 1 to Set, Read Clears All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W1CRS | UNKNOW    | Write 1 to Clear, Read Sets All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| W0SRC | UNKNOW    | Write 0 to Set, Read Clears All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W0CRS | UNKNOW    | Write 0 to Clear, Read Sets All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| WO    | Config    | Write Only                      | Changed to written value | No effect | Undefined |
#| WOC   | UNKNOW    | Write Only Clears All           | Sets all bits to 0     | No effect | Undefined |
#| WOS   | UNKNOW    | Write Only Sets All             | Sets all bits to 1     | No effect | Undefined |
#| W1    | UNKNOW    | Write Once                      | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Current value |
#| WO1   | UNKNOW    | Write Only, Once                | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Undefined |
#=============================================================================
# Sample
#=============================================================================

######################################################################################
#SAMPLE> // Begin:
#SAMPLE> //# Some description
#SAMPLE> // Register Full Name: Device Version ID
#SAMPLE> // RTL Instant Name: VersionID
#SAMPLE> //# {FunctionName,SubFunction_InstantName_Description}
#SAMPLE> //# RTL Instant Name and Full Name MUST be unique within a register description file
#SAMPLE> // Address: 0x00_0000
#SAMPLE> //# Address is relative address, will be sum with Base_Address for each function block
#SAMPLE> // Formula      : Address + $portid 
#SAMPLE> // Where        : {$portid(0-7): port identification} 
#SAMPLE> // Description  : This register indicates the module' name and version. 
#SAMPLE> // Width: 32
#SAMPLE> // Register Type: {Status}
#SAMPLE> //# Field: [Bit:Bit] %%  Name     %% Description            %% Type %% SW_Reset %% HW_Reset
#SAMPLE> // Field: [31:24]    %%  Year     %% 2013                   %% RO   %% 0x0D     %% 0x0D
#SAMPLE> // Field: [23:16]    %%  Week     %% Week Synthesized FPGA  %% RO   %% 0x0C     %% 0x0C
#SAMPLE> // Field: [15:8]     %%  Day      %% Day Synthesized FPGA   %% RO   %% 0x16     %% 0x16
#SAMPLE> // Field: [7:4]      %%  VerID    %% FPGA Version           %% RO   %% 0x0      %% 0x0
#SAMPLE> // Field: [3:0]      %%  NumID    %% Number of FPGA Version %% RO   %% 0x1      %% 0x1
#SAMPLE> // End:

######################################################################################
# OFFSET INT PMc : 0x1F0_0000....-0x1FF_FFFF
######################################################################################


######################################################################################
# Pseudowire Rate up to VC4 Counter, higher rate may reach saturation soon
######################################################################################

# ******************
# Pseudowire Transmit Group0 Counter
# ******************
// Begin:
// Register Full Name: Pseudowire Transmit Group0 Counter  
// RTL Instant Name  : upen_txpwcnt0
// Address: 0x2_0000-0x2_FFFF
// Formula: Address + $type*32768 + $rdmode*16384 + $pwid
// Where: 	{$pwid(0-10751) : Pseudowire ID} %% {$type(0-1): counter type}  %% {$rdmode(0-1): 0 is R2C and 1 is RO} 
// Description: The register count Tx PW group0 of rate up to VC4
// Width: 28
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   	%% Description    						%% Type %% SW_Reset %% HW_Reset
// Field: [27:00]	%% txpwcnt0  %% transmit PW group0 counter 
//									+ type = 0: tx PW byte counter 
//									+ type = 1: tx PW packet counter %% RW 	%% 0x0 		%% 0x0 
//End:

# ******************
# Pseudowire Transmit Group1 Counter
# ******************
// Begin:
// Register Full Name: Pseudowire Transmit Group1 Counter  
// RTL Instant Name  : upen_txpwcnt1
// Address: 0x0_0000-0x1_FFFF
// Formula: Address + $type*32768 + $rdmode*16384 + $pwid
// Where: 	{$pwid(0-10751) : Pseudowire ID} %% {$type(0-3): counter type}  %% {$rdmode(0-3): 0 is R2C and 1 is RO} 
// Description: The register count Tx PW group1 of rate up to VC4
// Width: 18
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   	%% Description    						%% Type %% SW_Reset %% HW_Reset
// Field: [17:00]	%% txpwcnt1  %% transmit PW group1 counter 
//									+ type = 0: tx PW Rbit counter 
//									+ type = 1: tx PW Pbit counter 
//									+ type = 2: tx PW Nbit/Mbit counter 
//									+ type = 3: tx PW Lbit counter %% RW 	%% 0x0 		%% 0x0 
//End:

# ******************
# Pseudowire Receive Group0 Counter
# ******************
// Begin:
// Register Full Name: Pseudowire Receive Group0 Counter  
// RTL Instant Name  : upen_rxpwcnt0
// Address: 0x3_0000-0x3_FFFF
// Formula: Address + $type*32768 + $rdmode*16384 + $pwid
// Where: 	{$pwid(0-10751) : Pseudowire ID} %% {$type(0-1): counter type}  %% {$rdmode(0-1): 0 is R2C and 1 is RO} 
// Description: The register count Rx PW group0 of rate up to VC4
// Width: 28
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   	%% Description    						%% Type %% SW_Reset %% HW_Reset
// Field: [27:00]	%% rxpwcnt0  %% receive PW group0 counter 
//									+ type = 0: rx PW byte counter 
//									+ type = 1: rx PW packet counter %% RW 	%% 0x0 		%% 0x0 
//End:

# ******************
# Pseudowire Receive Group1 Counter
# ******************
// Begin:
// Register Full Name: Pseudowire Receive Group1 Counter  
// RTL Instant Name  : upen_rxpwcnt1
// Address: 0x4_0000-0x5_FFFF
// Formula: Address + $type*32768 + $rdmode*16384 + $pwid
// Where: 	{$pwid(0-10751) : Pseudowire ID} %% {$type(0-3): counter type}  %% {$rdmode(0-3): 0 is R2C and 1 is RO} 
// Description: The register count Rx PW group1 of rate up to VC4
// Width: 18
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   	%% Description    						%% Type %% SW_Reset %% HW_Reset
// Field: [17:00]	%% rxpwcnt1  %% receive PW group1 counter 
//									+ type = 0: rx PW Rbit counter 
//									+ type = 1: rx PW Pbit counter 
//									+ type = 2: rx PW Nbit/Mbit counter 
//									+ type = 3: rx PW Lbit counter %% RW 	%% 0x0 		%% 0x0 
//End:

# ******************
# Pseudowire Receive Group2 Counter
# ******************
// Begin:
// Register Full Name: Pseudowire Receive Group2 Counter  
// RTL Instant Name  : upen_rxpwcnt2
// Address: 0x6_0000-0x7_FFFF
// Formula: Address + $type*32768 + $rdmode*16384 + $pwid
// Where: 	{$pwid(0-10751) : Pseudowire ID} %% {$type(0-3): counter type}  %% {$rdmode(0-3): 0 is R2C and 1 is RO} 
// Description: The register count Rx PW group2 of rate up to VC4
// Width: 18
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   	%% Description    						%% Type %% SW_Reset %% HW_Reset
// Field: [17:00]	%% rxpwcnt2  %% receive PW group2 counter 
//									+ type = 0: rx PW Stray counter 
//									+ type = 1: rx PW Malform counter 
//									+ type = 2: rx PW Underrun counter 
//									+ type = 3: rx PW Overrun counter %% RW 	%% 0x0 		%% 0x0 
//End:

# ******************
# Pseudowire Receive Group3 Counter
# ******************
// Begin:
// Register Full Name: Pseudowire Receive Group3 Counter  
// RTL Instant Name  : upen_rxpwcnt3
// Address: 0x8_0000-0x9_FFFF
// Formula: Address + $type*32768 + $rdmode*16384 + $pwid
// Where: 	{$pwid(0-10751) : Pseudowire ID} %% {$type(0-3): counter type}  %% {$rdmode(0-3): 0 is R2C and 1 is RO} 
// Description: The register count Rx PW group2 of rate up to VC4
// Width: 18
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   	%% Description    						%% Type %% SW_Reset %% HW_Reset
// Field: [17:00]	%% rxpwcnt3  %% receive PW group2 counter 
//									+ type = 0: rx PW LOPS counter 
//									+ type = 1: rx PW ReorderLost counter 
//									+ type = 2: rx PW ReorderOk counter 
//									+ type = 3: rx PW ReorderDrop counter %% RW 	%% 0x0 		%% 0x0 
//End:

######################################################################################
# Pseudowire Rate from VC4_4C Counter
######################################################################################

#********************* 
#4.1.6. Pseudowire High Rate Counter Look Up Control
#********************* 
// Begin: 
// Register Full Name: Pseudowire Count High Order Look Up Control
// RTL Instant Name  : ramcnthotdmlkupcfg
// Address: 0xF8_000 - 0xFA_9FF
// Formula: Address + $pwid 
// Where: {$pwid(0-10751): Pseodowire ID} 
// Description: This register configure lookup from PWID to a pool high speed ID allocated by SW
// Width: 6
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [5]  %% PwCntHighRateEn %% Set 1 to indicate the rate of PW is higher than VC4  %% RW %% 0x0 %% 0x0  
// Field: [4:0]  %% HighRateSwPwID %% Allocated high rate PW ID by solfware  %% RW %% 0x0 %% 0x0  
//End:

# ******************
# Pseudowire High Rate Transmit Counter
# ******************
// Begin:
// Register Full Name: Pseudowire High Rate Transmit Byte Counter  
// RTL Instant Name  : upen_txpwcntbyte
// Address: 0xF_5000-0xF_5FFF
// Formula: Address + $type*64 + $rdmode*32 + $hipwid
// Where: 	{$pwid(0-31) : Sw High Rate Pseudowire ID} %% {$rdmode(0-1): 0 is R2C and 1 is RO} %% {$type(0-3): counter type}   
// Description: The register count Tx PW byte of rate over VC4
// Width: 32
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   	%% Description    						%% Type %% SW_Reset %% HW_Reset
// Field: [32:00]	%% hitxpwbytecnt  %% high rate transmit PW byte counter 
//										 + type = 0 : high rate tx PW byte counter 
//										 + type = 1 : high rate tx PW packet counter 
//										 + type = 2 : high rate tx PW Rbit counter 
//										 + type = 3 : high rate tx PW Lbit counter %% RW 	%% 0x0 		%% 0x0 
//End:

# ******************
# Pseudowire High Rate Receive Counter
# ******************
// Begin:
// Register Full Name: Pseudowire High Rate Receive Byte Counter  
// RTL Instant Name  : upen_rxpwcntbyte
// Address: 0xF_6000-0xF_6FFF
// Formula: Address + $type*64 + $rdmode*32 + $hipwid
// Where: 	{$pwid(0-31) : Sw High Rate Pseudowire ID} %% {$rdmode(0-1): 0 is R2C and 1 is RO} %% {$type(0-5): counter type}   
// Description: The register count Rx PW byte of rate over VC4
// Width: 32
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   	%% Description    						%% Type %% SW_Reset %% HW_Reset
// Field: [32:00]	%% hirxpwbytecnt  %% high rate receive PW byte counter 
//										 + type = 0 : high rate rx PW byte counter 
//										 + type = 1 : high rate rx PW packet counter 
//										 + type = 2 : high rate rx PW Rbit counter 
//										 + type = 3 : high rate rx PW Lbit counter 
//										 + type = 4 : high rate rx PW Stray counter 
//										 + type = 5 : high rate rx PW Malform counter %% RW 	%% 0x0 		%% 0x0 
//End:

######################################################################################
# section/line Counter
######################################################################################

# ******************
# PMR Error Counter 
# ******************
// Begin:
// Register Full Name: PMR Error Counter  
// RTL Instant Name  : upen_poh_pmr_cnt
// Address: 0xF_3000 - 0xF_3FFF
// Formula: Address + 32*$type + $rdmode*16 + $lineid
// Where: 	{$type(0-5): counter type} %% {$rdmode(0-1): 0 is R2C and 1 is RO}  %% {$lineid(0-15)} 
// Description: The register count information as below. Depending on type it 's the events specify.
// Width: 20
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   		    %% Description    		      %% Type %% SW_Reset %% HW_Reset
// Field: [19:00]	%% pmr_err_cnt    %%  + type = 0 : rei_l_block_err 
//                                        + type = 1 : rei_l 
//                                        + type = 2 : b2_block_err  
//                                        + type = 3 : b2  
//                                        + type = 4 : b1_block_err  
//                                        + type = 5 : b1     %% RW 	%% 0x0 	  %% 0x0
// End:

######################################################################################
# POH Path Counter
######################################################################################

# ******************
# POH Path STS Counter
# ******************
// Begin:
// Register Full Name: POH Path STS Counter
// RTL Instant Name  : upen_sts_pohpm
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0xF_4000-0xF_4FFF
// Formula: Address + 1024*$type + $rdmode*512 + 8*$stsid + $slcid
// Where: 	{$type(0-1): counter type} %% {$rdmode(0-1): 0 is R2C and 1 is RO} %% {$stsid(0-47)} %% {$slcid(0-7)} 
// Description: The register count information as below. Depending on offset it 's the events specify.
// Width: 18
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   		       %% Description    		       %% Type %% SW_Reset %% HW_Reset
// Field: [17:00]	%% pohpath_sts_cnt     %% pohpath_sts_cnt
//                                            type = 0:  sts_rei
//                                            type = 1:  sts_b3 %% RW 	%% 0x0 	   %% 0x0 
// End:

# ******************
# POH Path VT Counter
# ******************
// Begin:
// Register Full Name: POH Path VT Counter
// RTL Instant Name  : upen_vt_pohpm
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0xE_0000 - 0xE_FFFF(RW)   
// Formula: Address + 32768*$type + $rdmode*16384 + 256*$stsid + 32*$vtgid + 8*vtid + slcid 
// Where: 	 {$type(0-1): counter type} %% {$rdmode(0-1): 0 is R2C and 1 is RO} %% {{$stsid(0-48)} %% {$vtgid(0-6)} %% {$vtid(0-3)} %% {$slcid(0-7)}}
// Description: The register count information as below. Depending on offset it 's the events specify.
// Width: 12
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   		         %% Description    		       %% Type %% SW_Reset %% HW_Reset
// Field: [11:00]	%% pohpath_vt_cnt %% pohpath_vt_cnt
//                                            type = 0:  vt_rei
//                                            type = 1:  vt_bip %% RW 	%% 0x0 	   %% 0x0 
// End:

######################################################################################
# POH Pointer
######################################################################################
# ******************
# POH vt pointer Counter
# ******************
// Begin:
// Register Full Name: POH vt pointer Counter
// RTL Instant Name  : upen_poh_vt_cnt
// Address: 0xC_0000-0xD_FFFF(RW)   
// Formula: Address + $type*32768 + $rdmode*16384 + 256*$stsid + 32*$vtgid + 8*vtid + slcid 
// Where: 	 {$type(0-3): counter type} %% {$rdmode(0-1): 0 is R2C and 1 is RO} %% {{$stsid(0-48)} %% {$vtgid(0-6)} %% {$vtid(0-3)} %% {$slcid(0-7)}}
// Description: The register count information as below. Depending on offset it 's the events specify.
// Width: 12
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   		    %% Description    		      %% Type %% SW_Reset %% HW_Reset
// Field: [11:00]	%% poh_vt_cnt    %% poh vt pointer 
//										+ type = 0 :  pohtx_vt_dec                                  		                                 
//										+ type = 1 :  pohtx_vt_inc                                  		                                 
//										+ type = 2 :  pohrx_vt_dec                                  		                                 
//										+ type = 3 :  pohrx_vt_inc %% RW 	%% 0x0 	  %% 0x0 
// End:

# ******************
# POH sts pointer Counter 0
# ******************
// Begin:
// Register Full Name: POH sts pointer Counter 
// RTL Instant Name  : upen_poh_sts_cnt0
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0xF_000-0xF_0FFF
// Formula: Address + 1024*$type + $rdmode*512 + 8*$stsid + $slcid 
// Where: 	 {$type(0-1): counter type} %% {$rdmode(0-1): 0 is R2C and 1 is RO} %% {$stsid(0-47)} %% {$slcid(0-7)} 
// Description: The register count information as below. Depending on offset it 's the events specify.
// Width: 16
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   		    %% Description    		      %% Type %% SW_Reset %% HW_Reset
// Field: [15:00]	%% poh_sts_cnt    %% poh sts pointer
//										+ type = 0 :  pohtx_sts_dec                                  		                                 
//										+ type = 1 :  pohtx_sts_inc %% RW 	%% 0x0 	  %% 0x0 
// End:


# ******************
# POH sts pointer Counter 1
# ******************
// Begin:
// Register Full Name: POH sts pointer Counter 
// RTL Instant Name  : upen_poh_sts_cnt1
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0xF_1000-0xF_1FFF
// Formula: Address + 1024*$type + $rdmode*512 + 8*$stsid + $slcid 
// Where: 	 {$type(0-1): counter type} %% {$rdmode(0-1): 0 is R2C and 1 is RO} %% {$stsid(0-47)} %% {$slcid(0-7)} 
// Description: The register count information as below. Depending on offset it 's the events specify.
// Width: 16
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   		    %% Description    		      %% Type %% SW_Reset %% HW_Reset
// Field: [15:00]	%% poh_sts_cnt    %% poh sts pointer
//										+ type = 0 :  pohrx_sts_dec                                  		                                 
//										+ type = 1 :  pohrx_sts_inc %% RW 	%% 0x0 	  %% 0x0 
// End:

######################################################################################
# PDH DS1/DS3 counter
######################################################################################

# ******************
# PDH ds3 cntval Counter
# ******************
// Begin:
// Register Full Name: PDH ds3 cntval Counter 
// RTL Instant Name  : upen_pdh_de3cnt
// Address: 0xF_2000-0xF_2FFF
// Formula: Address + $type*1024 + $rdmode*512 + 8*$stsid + $slcid
// Where: 	{$type(0-3): counter type} %% {$rdmode(0-1): 0 is R2C and 1 is RO} %% {$stsid(0-47)} %% {$slcid(0-7)} 
// Description: The register count DS3
// Width: 18
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   		    %% Description	   %% Type %% SW_Reset %% HW_Reset
// Field: [17:00]	%% ds3_cnt_val      %% ds3_cnt_val     
// 										    + type = 0 : FE 
//                                          + type = 1 : REI 
//                                          + type = 2 : PB  
//                                          + type = 3 : CB %% RW 	%% 0x0 	  %% 0x0
// End:

# ******************
# PDH ds1 cntval Counter 
# ******************
// Begin:
// Register Full Name: PDH ds1 cntval Counter
// RTL Instant Name  : upen_pdh_de1cntval
// Address: 0xA_0000-0xA_FFFF
// Formula: Address + $type*32768 + $rdmode*16384 + 256*$stsid + 32*$vtgid + 8*vtid + slcid
// Where: 	{$type(0-2): counter type} %% {$rdmode(0-1): 0 is R2C and 1 is RO} %% {{$stsid(0-48)} %% {$vtgid(0-6)} %% {$vtid(0-3)} %% {$slcid(0-7)}}
// Description: The register count DS1  
// Width: 14
// Register Type: {Config}
//#Field: [Bit:Bit] %%  Name   			       %% Description    		       %% Type %% SW_Reset %% HW_Reset
// Field: [13:00]	%% ds1_cnt_val    %% ds1_cnt_val 
// 										    + type = 0 : FE 
//                                          + type = 1 : CRC 
//                                          + type = 2 : REI %% RW 	%% 0x0 	  %% 0x0
// End:

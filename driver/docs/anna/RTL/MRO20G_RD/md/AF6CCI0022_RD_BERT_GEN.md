## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-07-10|AF6Project|Initial version|




##AF6CCI0022_RD_BERT_GEN
####Register Table

|Name|Address|
|-----|-----|
|`Sel Timeslot Gen`|`0x8340 - 0xBB5F`|
|`Sel Timeslot Mon tdm`|`0x8500 - 0xBD00`|
|`Sel Timeslot Mon PW`|`0x8700 - 0xBF00`|
|`Sel Ho Bert Gen`|`0x0_200 - 0x0_21f`|
|`Sel TDM Bert mon`|`0x0_000 - 0x0_01f`|
|`Sel PW Bert Gen`|`0x0_600 - 0x0_61f`|
|`Sel Mode Bert Gen`|`0x8_300 - 0xB_B1F`|
|`Sel Mode Bert Mon TDM`|`0x8_520 - 0xB_D3F`|
|`Sel Mode Bert Mon PW`|`0x8_720 - 0xB_F3F`|
|`Inser Error`|`0x8_320 - 0xB_B3F`|
|`Counter num of bit gen`|`0x8_380 - 0xB_B80`|
|`good counter tdm`|`0x8_580 - 0xB_D9F`|
|`TDM err sync`|`0x8_540 - 0xB_D5F`|
|`Counter loss bit in TDM mon`|`0x8_400 - 0xB_C00`|
|`Counter loss bit in TDM mon`|`0x8_5C0 - 0xB_DDF`|
|`counter good bit PW mon`|`0x8_780 - 0xB_F9F`|
|`counter error bit in PW mon`|`0x8_740 - 0xB_F5F`|
|`Counter loss bit in PW mon`|`0x8_7C0 - 0xB_FDF`|
|`Counter loss bit in TDM mon`|`0x8_600 - 0xB_E00`|


###Sel Timeslot Gen

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ts_gen`

* **Address**             : `0x8340 - 0xBB5F`

* **Formula**             : `0x8340 + 2048*slice  + engid`

* **Where**               : 

    * `$engid(0-31): id of HO_BERT`

    * `$slice(0-7) : OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`gen_tsen`| bit map indicase 32 timeslot,"1" : enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Timeslot Mon tdm

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_mon_tstdm`

* **Address**             : `0x8500 - 0xBD00`

* **Formula**             : `0x8500 + engid + 2048*slice`

* **Where**               : 

    * `$engid(0-31): id of HO_BERT`

    * `$slice(0-7) : OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tdm_tsen`| bit map indicase 32 timeslot,"1" : enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Timeslot Mon PW

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_mon_tspw`

* **Address**             : `0x8700 - 0xBF00`

* **Formula**             : `0x8700 + engid + 2048*slice`

* **Where**               : 

    * `$engid(0-31): id of HO_BERT`

    * `$slice(0-7) : OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pw_tsen`| bit map indicase 32 timeslot,"1" : enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert Gen

* **Description**           

The registers select id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_gen`

* **Address**             : `0x0_200 - 0x0_21f`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`gen_en`| set "1" to enable bert gen| `RW`| `0x0`| `0x0`|
|`[13:11]`|`lineid`| line id OC48| `RW`| `0x0`| `0x0`|
|`[10:0]`|`id`| TDM ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel TDM Bert mon

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_bert_montdm`

* **Address**             : `0x0_000 - 0x0_01f`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`tdmmon_en`| set "1" to enable bert gen| `RW`| `0x0`| `0x0`|
|`[13:11]`|`tdm_lineid`| line id OC48| `RW`| `0x0`| `0x0`|
|`[10:0]`|`id`| tdm ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel PW Bert Gen

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_bert_monpw`

* **Address**             : `0x0_600 - 0x0_61f`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`pwmon_en`| set "1" to enable bert gen| `RW`| `0x0`| `0x0`|
|`[13:11]`|`pw_lineid`| line id OC48| `RW`| `0x0`| `0x0`|
|`[10:0]`|`pw_lkid`| pwlk id| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Mode Bert Gen

* **Description**           

The registers select mode bert gen


* **RTL Instant Name**    : `ctrl_pen_gen`

* **Address**             : `0x8_300 - 0xB_B1F`

* **Formula**             : `0x8_300 + 2048*slice + id`

* **Where**               : 

    * `$slice(0-7): slice id`

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[09]`|`swapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[08]`|`invmode`| invert data| `RW`| `0x0`| `0x0`|
|`[07:00]`|`patt_mode`| sel pattern gen # 0x01 : all0 # 0x02 : prbs15 # 0x10 : prbs23 # 0x20 : prbs31 # 0x80 : all1| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Mode Bert Mon TDM

* **Description**           

The registers select mode bert gen


* **RTL Instant Name**    : `ctrl_pen_montdm`

* **Address**             : `0x8_520 - 0xB_D3F`

* **Formula**             : `0x8_520 + 2048*slice + id`

* **Where**               : 

    * `$slice(0-7): slice id of HO_BERT`

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[09]`|`swapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[08]`|`invmode`| invert data| `RW`| `0x0`| `0x0`|
|`[07:00]`|`patt_mode`| sel pattern gen # 0x02 : all0 # 0x04 : prbs15 # 0x20 : prbs23 # 0x40 : prbs31 # 0x80 : all1| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Mode Bert Mon PW

* **Description**           

The registers select mode bert gen


* **RTL Instant Name**    : `ctrl_pen_monpw`

* **Address**             : `0x8_720 - 0xB_F3F`

* **Formula**             : `0x8_720 + 2048*slice`

* **Where**               : 

    * `$slice(0-7): slice id of HO_BERT`

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[09]`|`swapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[08]`|`invmode`| invert data| `RW`| `0x0`| `0x0`|
|`[07:00]`|`patt_mode`| sel pattern gen # 0x02 : all0 # 0x04 : prbs15 # 0x20 : prbs23 # 0x40 : prbs31 # 0x80 : all1| `RW`| `0x0`| `0x0 End: Begin:`|

###Inser Error

* **Description**           

The registers select rate inser error


* **RTL Instant Name**    : `ctrl_ber_pen`

* **Address**             : `0x8_320 - 0xB_B3F`

* **Formula**             : `0x8_320 + 2048*slice + id`

* **Where**               : 

    * `$slice(0-7): slice id of HO_BERT`

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ber_rate`| TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to Pattern Generator [31:0] == 32'd0          :disable  Incase N DS0 mode : n        = num timeslot use, BER_level_val recalculate by CFG_DS1,CFG_E1 CFG_DS1  = ( 8n  x  BER_level_val) / 193 CFG_E1   = ( 8n  x  BER_level_val) / 256 Other : BER_level_val	BER_level 1000:        	BER 10e-3 10_000:      	BER 10e-4 100_000:     	BER 10e-5 1_000_000:   	BER 10e-6 10_000_000:  	BER 10e-7| `RW`| `0x0`| `0x0 Begin:`|

###Counter num of bit gen

* **Description**           

The registers counter bit genertaie in tdm side


* **RTL Instant Name**    : `goodbit_ber_pen`

* **Address**             : `0x8_380 - 0xB_B80`

* **Formula**             : `0x8_380 + 2048*slice + id`

* **Where**               : 

    * `$slice(0-7): slice id of BERT`

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`goodbit`| counter goodbit| `R2C`| `0x0`| `0x0 Begin:`|

###good counter tdm

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `goodbit_pen_tdm_mon`

* **Address**             : `0x8_580 - 0xB_D9F`

* **Formula**             : `0x8_580 + 2048*slice + id`

* **Where**               : 

    * `$slice(0-7): slice of BERT`

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mongood`| moniter goodbit| `R2C`| `0x0`| `0x0 End: Begin:`|

###TDM err sync

* **Description**           

The registers indicate bert mon err in tdm side


* **RTL Instant Name**    : `err_tdm_mon`

* **Address**             : `0x8_540 - 0xB_D5F`

* **Formula**             : `0x8_540 + 2048*slice + id`

* **Where**               : 

    * `$slice(0-7): slice id of HO_BERT`

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_errbit`| counter err bit| `R2C`| `0x0`| `0x0 End: Begin:`|

###Counter loss bit in TDM mon

* **Description**           

The registers count lossbit in  mon tdm side


* **RTL Instant Name**    : `lossyn_pen_tdm_mon`

* **Address**             : `0x8_400 - 0xB_C00`

* **Formula**             : `0x8_400 + 2048*slice`

* **Where**               : 

    * `$slice(0-7): slice id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`stk_losssyn`| sticky loss sync| `W2C`| `0x0`| `0x0 End: Begin:`|

###Counter loss bit in TDM mon

* **Description**           

The registers count lossbit in  mon tdm side


* **RTL Instant Name**    : `lossbit_pen_tdm_mon`

* **Address**             : `0x8_5C0 - 0xB_DDF`

* **Formula**             : `0x8_5C0 + 2048*slice + id`

* **Where**               : 

    * `$slice(0-7): slice id of HO_BERT`

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_lossbit`| counter lossbit| `R2C`| `0x0`| `0x0 End: Begin:`|

###counter good bit PW mon

* **Description**           

The registers count goodbit in  mon pw side


* **RTL Instant Name**    : `goodbit_pen_pw_mon`

* **Address**             : `0x8_780 - 0xB_F9F`

* **Formula**             : `0x8_780 + 2048*slice + id`

* **Where**               : 

    * `$slice(0-7): slice of BERT`

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_pwgoodbit`| counter goodbit| `R2C`| `0x0`| `0x0 End: Begin:`|

###counter error bit in PW mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `errbit_pen_pw_mon`

* **Address**             : `0x8_740 - 0xB_F5F`

* **Formula**             : `0x8_740 + 2048*slice + id`

* **Where**               : 

    * `$slice(0-7): slice id of HO_BERT`

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_pwerrbit`| counter err bit| `R2C`| `0x0`| `0x0 End: Begin:`|

###Counter loss bit in PW mon

* **Description**           

The registers count lossbit in  mon tdm side


* **RTL Instant Name**    : `lossbit_pen_pw_mon`

* **Address**             : `0x8_7C0 - 0xB_FDF`

* **Formula**             : `0x8_7C0 + 2048*slice + id`

* **Where**               : 

    * `$slice(0-7): slice id of HO_BERT`

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_pwlossbit`| counter loss bit| `R2C`| `0x0`| `0x0 End: Begin:`|

###Counter loss bit in TDM mon

* **Description**           

The registers count lossbit in  mon tdm side


* **RTL Instant Name**    : `lossyn_pen_pw_mon`

* **Address**             : `0x8_600 - 0xB_E00`

* **Formula**             : `0x8_600 + 2048*slice`

* **Where**               : 

    * `$slice(0-7): slice id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`stk_losssyn`| sticky loss sync| `W2C`| `0x0`| `0x0 End:`|

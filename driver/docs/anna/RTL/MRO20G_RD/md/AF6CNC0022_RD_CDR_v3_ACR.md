## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_CDR_v3_ACR
####Register Table

|Name|Address|
|-----|-----|
|`CDR ACR CPU  Reg Hold Control`|`0x070000-0x070002`|
|`CDR ACR Engine Timing control`|`0x0000800-0x000FFF`|
|`CDR Adjust State status`|`0x0001000-0x0017FF`|
|`CDR Adjust Holdover value status`|`0x0001800-0x001FFF`|
|`DCR TX Engine Active Control`|`0x0040000`|
|`DCR PRC Source Select Configuration`|`0x0040001`|
|`DCR PRC Frequency Configuration`|`0x004000b`|
|`DCR RTP Frequency Configuration`|`0x004000C`|
|`RAM ACR Parity Force Control`|`0x0000c`|
|`RAM ACR Parity Disable Control`|`0x000d`|
|`RAM ACR parity Error Sticky`|`0x000e`|
|`Read HA Address Bit3_0 Control`|`0x70200`|
|`Read HA Address Bit7_4 Control`|`0x70210`|
|`Read HA Address Bit11_8 Control`|`0x70220`|
|`Read HA Address Bit15_12 Control`|`0x70230`|
|`Read HA Address Bit19_16 Control`|`0x70240`|
|`Read HA Address Bit23_20 Control`|`0x70250`|
|`Read HA Address Bit24 and Data Control`|`0x70260`|
|`Read HA Hold Data63_32`|`0x70270`|
|`Read HA Hold Data95_64`|`0x70271`|
|`Read HA Hold Data127_96`|`0x70272`|
|`CDR per Channel Interrupt Enable Control`|`0x0006000-0x00067FF`|
|`CDR per Channel Interrupt Status`|`0x0006800-0x0006FFF`|
|`CDR per Channel Current Status`|`0x0007000-0x00077FF`|
|`CDR per Channel Interrupt OR Status`|`0x0007800-0x000782F`|
|`CDR per STS/VC Interrupt OR Status`|`0x0007BFF`|
|`CDR per STS/VC Interrupt Enable Control`|`0x0007BFE`|
|`CDR per Group Interrupt OR Status`|`0x0000010`|


###CDR ACR CPU  Reg Hold Control

* **Description**           

The register provides hold register for three word 32-bits MSB when CPU access to engine.


* **RTL Instant Name**    : `cdr_acr_cpu_hold_ctrl`

* **Address**             : `0x070000-0x070002`

* **Formula**             : `0x070000 + HoldId`

* **Where**               : 

    * `$HoldId(0-2): Hold register`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdreg0`| Hold 32 bits| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR ACR Engine Timing control

* **Description**           

This register is used to configure timing mode for per STS


* **RTL Instant Name**    : `cdr_acr_eng_timing_ctrl`

* **Address**             : `0x0000800-0x000FFF`

* **Formula**             : `0x0000800 + OC48*32768 + STS*32 + TUG*4 + VT`

* **Where**               : 

    * `$OC48(0-7):`

    * `$STS(0-47):`

    * `$TUG(0 -6):`

    * `$VT(0-3): (0-2)in E1 and (0-3) in DS1 HDL_PATH     : engcdr.slc0_cdrnco_engacr[OC48].ramcfg.array.ram[$STS*32 + $TUG*4 + $VT]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[26]`|`vc4_4c`| VC4_4c/TU3 mode 0: STS1/VC4/DS3 1: TU3/VC4-4c/OC48 STS192c (VC4_64c = 4xOC48 => payload / 4, for support full payload size of STS192c STS192c (VC4_64c), STS48c (VC4_16c), STS24c (VC4_8c), STS12c (VC4_4c), thi phai config theo VC4_4c rate (bit26 =1) STS192c (VC4_64c = 16xVC4_4c => payload / 16, STS48c (VC4_16c = 4xVC4_4c => payload / 4), STS24c (VC4_8c = 2xVC4_4c => payload / 2), STS12c (VC4_4c, payload real) Sample: VC4_8c (STS24c) with paylaod zie = 783byte =  783x8 = 6264 bit => payload config to CDR = 6264/2 = 3132bit = 0xC3C| `RW`| `0x0`| `0x0`|
|`[25]`|`pktlen_ind`| The payload packet  length for jumbo frame| `RW`| `0x0`| `0x0`|
|`[24]`|`holdvalmode`| Hold value mode of NCO, default value 0 0: Hardware calculated and auto update 1: Software calculated and update, hardware is disabled| `RW`| `0x0`| `0x0`|
|`[23]`|`seqmode`| Sequence mode mode, default value 0 0: Wrap zero 1: Skip zero| `RW`| `0x0`| `0x0`|
|`[22:20]`|`linetype`| Line type mode 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3/OC48 6: STS1/TU3 7: VC4/VC4-4c| `RW`| `0x0`| `0x0`|
|`[19:4]`|`pktlen`| The payload packet  length parameter to create a packet. SAToP mode: The number payload of bit. CESoPSN mode: The number of bit which converted to full DS1/E1 rate mode. In CESoPSN mode, the payload is assembled by NxDS0 with M frame, the value configured to this register is Mx256 bits for E1 mode, Mx193 bits for DS1 mode.| `RW`| `0x0`| `0x0`|
|`[3:0]`|`cdrtimemode`| CDR time mode 0: System mode 1: Loop timing mode, transparency service Rx clock to service Tx clock 2: LIU timing mode, using service Rx clock for CDR source to generate service Tx clock 3: Prc timing mode, using Prc clock for CDR source to generate service Tx clock 4: Ext#1 timing mode, using Ext#1 clock for CDR source to generate service Tx clock 5: Ext#2 timing mode, using Ext#2 clock for CDR source to generate service Tx clock 6: Tx SONET timing mode, using Tx Line OCN clock for CDR source to generate service Tx clock 7: Free timing mode, using system clock for CDR source to generate service Tx clock 8: ACR timing mode 9: Reserve 10: DCR timing mode 11: Reserve 12: ACR Fast lock mode| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Adjust State status

* **Description**           

This register is used to store status or configure  some parameter of per CDR engine


* **RTL Instant Name**    : `cdr_adj_state_stat`

* **Address**             : `0x0001000-0x0017FF`

* **Formula**             : `0x0001000 + OC48*32768 + STS*32 + TUG*4 + VT`

* **Where**               : 

    * `$OC48(0-7):`

    * `$STS(0-47):`

    * `$TUG(0 -6):`

    * `$VT(0-3): (0-2)in E1 and (0-3) in DS1`

* **Width**               : `8`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2:0]`|`adjstate`| Adjust state 0: Load state 1: Wait state 2: Init state 3: Learn State 4: ReLearn State 5: Lock State 6: ReInit State 7: Holdover State| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Adjust Holdover value status

* **Description**           

This register is used to store status or configure  some parameter of per CDR engine


* **RTL Instant Name**    : `cdr_adj_holdover_value_stat`

* **Address**             : `0x0001800-0x001FFF`

* **Formula**             : `0x0001800 + OC48*32768 + STS*32 + TUG*4 + VT`

* **Where**               : 

    * `$OC48(0-7):`

    * `$STS(0-47):`

    * `$TUG(0 -6):`

    * `$VT(0-3): (0-2)in E1 and (0-3) in DS1`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdval`| NCO Holdover value parameter E1RATE   = 32'd3817748707;   Resolution =    0.262 parameter DS1RATE  = 32'd3837632815;   Resolution =    0.260 parameter VT2RATE  = 32'd4175662648;   Resolution =    0.239 parameter VT15RATE = 32'd4135894433;   Resolution =    0.241 parameter E3RATE   = 32'd2912117977;   Resolution =    0.343 parameter DS3RATE  = 32'd3790634015;   Resolution =    0.263 parameter TU3RATE  = 32'd4148547956;   Resolution =    0.239 parameter STS1RATE = 32'd4246160849;   Resolution =    0.239| `RO`| `0x0`| `0x0 End: Begin:`|

###DCR TX Engine Active Control

* **Description**           

This register is used to activate the DCR TX Engine. Active high.


* **RTL Instant Name**    : `dcr_tx_eng_active_ctrl`

* **Address**             : `0x0040000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`data`| DCR TX Engine Active Control| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR PRC Source Select Configuration

* **Description**           

This register is used to configure to select PRC source for PRC timer. The PRC clock selected must be less than 19.44 Mhz.


* **RTL Instant Name**    : `dcr_prc_src_sel_cfg`

* **Address**             : `0x0040001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2:0]`|`dcrprcsourcesel`| PRC source selection. 0: PRC Reference Clock 1: System Clock 19Mhz 2: External Reference Clock 1. 3: External Reference Clock 2. 4: Ocn Line Clock Port 1 5: Ocn Line Clock Port 2 6: Ocn Line Clock Port 3 7: Ocn Line Clock Port 4| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR PRC Frequency Configuration

* **Description**           

This register is used to configure the frequency of PRC clock.


* **RTL Instant Name**    : `dcr_prc_freq_cfg`

* **Address**             : `0x004000b`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`dcrprcfrequency`| Frequency of PRC clock. This is used to configure the frequency of PRC clock in Khz. Unit is Khz. Exp: The PRC clock is 19.44Mhz. This register will be configured to 19440.| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR RTP Frequency Configuration

* **Description**           

This register is used to configure the frequency of PRC clock.


* **RTL Instant Name**    : `dcr_rtp_freq_cfg`

* **Address**             : `0x004000C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`dcrrtpfreq`| Frequency of RTP clock. This is used to configure the frequency of RTP clock in Khz. Unit is Khz. Exp: The RTP clock is 19.44Mhz. This register will be configured to 19440.| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM ACR Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_ACR_Parity_Force_Control`

* **Address**             : `0x0000c`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8]`|`cdracrddr_parerrfrc`| Force parity For DDR of CDR| `RW`| `0x0`| `0x0`|
|`[7:0]`|`cdracrtimingctrl_parerrfrc`| Force parity For RAM Control "CDR ACR Engine Timing control", each bit for per OC48| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM ACR Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_ACR_Parity_Disable_Control`

* **Address**             : `0x000d`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8]`|`cdracrddr_parerrdis`| Disable parity For DDR of CDR| `RW`| `0x0`| `0x0`|
|`[7:0]`|`cdracrtimingctrl_parerrdis`| Disable parity For RAM Control "CDR ACR Engine Timing control" , each bit for per OC48| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM ACR parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_ACR_Parity_Error_Sticky`

* **Address**             : `0x000e`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8]`|`cdracrddr_parerrstk`| Error parity For DDR of CDR| `W1C`| `0x0`| `0x0`|
|`[7:0]`|`cdracrtimingctrl_parerrstk`| Error parity For RAM Control "CDR ACR Engine Timing control"  , each bit for per OC48| `W1C`| `0x0`| `0x0 End: Begin:`|

###Read HA Address Bit3_0 Control

* **Description**           

This register is used to send HA read address bit3_0 to HA engine


* **RTL Instant Name**    : `rdha3_0_control`

* **Address**             : `0x70200`

* **Formula**             : `0x70200 + HaAddr3_0`

* **Where**               : 

    * `$HaAddr3_0(0-15): HA Address Bit3_0`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr3_0`| Read value will be 0x01000 plus HaAddr3_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit7_4 Control

* **Description**           

This register is used to send HA read address bit7_4 to HA engine


* **RTL Instant Name**    : `rdha7_4_control`

* **Address**             : `0x70210`

* **Formula**             : `0x70210 + HaAddr7_4`

* **Where**               : 

    * `$HaAddr7_4(0-15): HA Address Bit7_4`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr7_4`| Read value will be 0x01000 plus HaAddr7_4| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit11_8 Control

* **Description**           

This register is used to send HA read address bit11_8 to HA engine


* **RTL Instant Name**    : `rdha11_8_control`

* **Address**             : `0x70220`

* **Formula**             : `0x70220 + HaAddr11_8`

* **Where**               : 

    * `$HaAddr11_8(0-15): HA Address Bit11_8`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr11_8`| Read value will be 0x01000 plus HaAddr11_8| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit15_12 Control

* **Description**           

This register is used to send HA read address bit15_12 to HA engine


* **RTL Instant Name**    : `rdha15_12_control`

* **Address**             : `0x70230`

* **Formula**             : `0x70230 + HaAddr15_12`

* **Where**               : 

    * `$HaAddr15_12(0-15): HA Address Bit15_12`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr15_12`| Read value will be 0x01000 plus HaAddr15_12| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit19_16 Control

* **Description**           

This register is used to send HA read address bit19_16 to HA engine


* **RTL Instant Name**    : `rdha19_16_control`

* **Address**             : `0x70240`

* **Formula**             : `0x70240 + HaAddr19_16`

* **Where**               : 

    * `$HaAddr19_16(0-15): HA Address Bit19_16`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr19_16`| Read value will be 0x01000 plus HaAddr19_16| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit23_20 Control

* **Description**           

This register is used to send HA read address bit23_20 to HA engine


* **RTL Instant Name**    : `rdha23_20_control`

* **Address**             : `0x70250`

* **Formula**             : `0x70250 + HaAddr23_20`

* **Where**               : 

    * `$HaAddr23_20(0-15): HA Address Bit23_20`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr23_20`| Read value will be 0x01000 plus HaAddr23_20| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit24 and Data Control

* **Description**           

This register is used to send HA read address bit24 to HA engine to read data


* **RTL Instant Name**    : `rdha24data_control`

* **Address**             : `0x70260`

* **Formula**             : `0x70260 + HaAddr24`

* **Where**               : 

    * `$HaAddr24(0-1): HA Address Bit24`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata31_0`| HA read data bit31_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data63_32

* **Description**           

This register is used to read HA dword2 of data.


* **RTL Instant Name**    : `rdha_hold63_32`

* **Address**             : `0x70270`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata63_32`| HA read data bit63_32| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data95_64

* **Description**           

This register is used to read HA dword3 of data.


* **RTL Instant Name**    : `rdindr_hold95_64`

* **Address**             : `0x70271`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata95_64`| HA read data bit95_64| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data127_96

* **Description**           

This register is used to read HA dword4 of data.


* **RTL Instant Name**    : `rdindr_hold127_96`

* **Address**             : `0x70272`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata127_96`| HA read data bit127_96| `RW`| `0xx`| `0xx End: Begin:`|

###CDR per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable of CDR


* **RTL Instant Name**    : `cdr_per_chn_intr_en_ctrl`

* **Address**             : `0x0006000-0x00067FF`

* **Formula**             : `0x0006000 +  StsID*32 + VtnID`

* **Where**               : 

    * `$StsID(0-47): STS-1/VC-3 ID`

    * `$VtnID(0-31): VT/TU number ID in the Group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`cdrunlokcedintren`| Set 1 to enable change UnLocked te event to generate an interrupt. Each bit for per OC48| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per Channel Interrupt Status

* **Description**           

This is the per Channel interrupt tus of CDR


* **RTL Instant Name**    : `cdr_per_chn_intr_stat`

* **Address**             : `0x0006800-0x0006FFF`

* **Formula**             : `0x0006800 +  StsID*32 + VtnID`

* **Where**               : 

    * `$StsID(0-47): STS-1/VC-3 ID`

    * `$VtnID(0-31): VT/TU number ID in the Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`cdrunlockedintr`| Set 1 if there is a change in UnLocked the event. Each bit for per OC48| `W1C`| `0x0`| `0x0 End: Begin:`|

###CDR per Channel Current Status

* **Description**           

This is the per Channel Current tus of CDR


* **RTL Instant Name**    : `cdr_per_chn_curr_stat`

* **Address**             : `0x0007000-0x00077FF`

* **Formula**             : `0x0007000 +  StsID*32 + VtnID`

* **Where**               : 

    * `$StsID(0-47): STS-1/VC-3 ID`

    * `$VtnID(0-31): VT/TU number ID in the Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`cdrunlockedcurrsta`| Current tus of UnLocked event. Each bit for per OC48| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per Channel Interrupt OR Status

* **Description**           

The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the CDR. Each bit is used to store Interrupt OR tus of the related DS1/E1.


* **RTL Instant Name**    : `cdr_per_chn_intr_or_stat`

* **Address**             : `0x0007800-0x000782F`

* **Formula**             : `0x0007800 +  GrpID*1024 + STSID`

* **Where**               : 

    * `$GrpID(0-1): Group 0 for STS 0-31, 1 for STS 32-47`

    * `$STSID(0-31)`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cdrvtintrorsta`| Set to 1 if any interrupt status bit of corresponding DS1/E1 is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per STS/VC Interrupt OR Status

* **Description**           

The register consists of 24 bits for 24 STS/VCs of the CDR. Each bit is used to store Interrupt OR tus of the related STS/VC.


* **RTL Instant Name**    : `cdr_per_stsvc_intr_or_stat`

* **Address**             : `0x0007BFF`

* **Formula**             : `0x0007BFF +  GrpID*1024`

* **Where**               : 

    * `$GrpID(0-1): Group 0 for STS 0-31, 1 for STS 32-47`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cdrstsintrorsta`| Set to 1 if any interrupt status bit of corresponding STS/VC is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per STS/VC Interrupt Enable Control

* **Description**           

The register consists of 24 interrupt enable bits for 24 STS/VCs in the Rx DS1/E1/J1 Framer.


* **RTL Instant Name**    : `cdr_per_stsvc_intr_en_ctrl`

* **Address**             : `0x0007BFE`

* **Formula**             : `0x0007BFE +  GrpID*1024`

* **Where**               : 

    * `$GrpID(0-1): Group 0 for STS 0-31, 1 for STS 32-47`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cdrstsintren`| Set to 1 to enable the related STS/VC to generate interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per Group Interrupt OR Status

* **Description**           

The register consists of 2 bits for 2 Group of the CDR


* **RTL Instant Name**    : `CDR_per_grp_intr_or_stat`

* **Address**             : `0x0000010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`rxde1grpintrorsta`| Set to 1 if any interrupt tus bit of corresponding Group is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End:`|

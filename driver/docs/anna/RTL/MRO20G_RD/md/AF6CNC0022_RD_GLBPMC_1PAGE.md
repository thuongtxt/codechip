## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_GLBPMC_1PAGE
####Register Table

|Name|Address|
|-----|-----|
|`Pseudowire Transmit Group0 Counter`|`0x2_0000-0x2_FFFF`|
|`Pseudowire Transmit Group1 Counter`|`0x0_0000-0x1_FFFF`|
|`Pseudowire Receive Group0 Counter`|`0x3_0000-0x3_FFFF`|
|`Pseudowire Receive Group1 Counter`|`0x4_0000-0x5_FFFF`|
|`Pseudowire Receive Group2 Counter`|`0x6_0000-0x7_FFFF`|
|`Pseudowire Receive Group3 Counter`|`0x8_0000-0x9_FFFF`|
|`Pseudowire Count High Order Look Up Control`|`0xF8_000 - 0xFA_9FF`|
|`Pseudowire High Rate Transmit Byte Counter`|`0xF_5000-0xF_5FFF`|
|`Pseudowire High Rate Receive Byte Counter`|`0xF_6000-0xF_6FFF`|
|`PMR Error Counter`|`0xF_3000 - 0xF_3FFF`|
|`POH Path STS Counter`|`0xF_4000-0xF_4FFF`|
|`POH Path VT Counter`|`0xE_0000 - 0xE_FFFF(RW)`|
|`POH vt pointer Counter`|`0xC_0000-0xD_FFFF(RW)`|
|`POH sts pointer Counter`|`0xF_000-0xF_0FFF`|
|`POH sts pointer Counter`|`0xF_1000-0xF_1FFF`|
|`PDH ds3 cntval Counter`|`0xF_2000-0xF_2FFF`|
|`PDH ds1 cntval Counter`|`0xA_0000-0xA_FFFF`|


###Pseudowire Transmit Group0 Counter

* **Description**           

The register count Tx PW group0 of rate up to VC4


* **RTL Instant Name**    : `upen_txpwcnt0`

* **Address**             : `0x2_0000-0x2_FFFF`

* **Formula**             : `0x2_0000 + $type*32768 + $rdmode*16384 + $pwid`

* **Where**               : 

    * `$pwid(0-10751) : Pseudowire ID`

    * `$type(0-1): counter type`

    * `$rdmode(0-1): 0 is R2C and 1 is RO`

* **Width**               : `28`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:00]`|`txpwcnt0`| transmit PW group0 counter + type = 0: tx PW byte counter + type = 1: tx PW packet counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Group1 Counter

* **Description**           

The register count Tx PW group1 of rate up to VC4


* **RTL Instant Name**    : `upen_txpwcnt1`

* **Address**             : `0x0_0000-0x1_FFFF`

* **Formula**             : `0x0_0000 + $type*32768 + $rdmode*16384 + $pwid`

* **Where**               : 

    * `$pwid(0-10751) : Pseudowire ID`

    * `$type(0-3): counter type`

    * `$rdmode(0-3): 0 is R2C and 1 is RO`

* **Width**               : `18`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:00]`|`txpwcnt1`| transmit PW group1 counter + type = 0: tx PW Rbit counter + type = 1: tx PW Pbit counter + type = 2: tx PW Nbit/Mbit counter + type = 3: tx PW Lbit counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Group0 Counter

* **Description**           

The register count Rx PW group0 of rate up to VC4


* **RTL Instant Name**    : `upen_rxpwcnt0`

* **Address**             : `0x3_0000-0x3_FFFF`

* **Formula**             : `0x3_0000 + $type*32768 + $rdmode*16384 + $pwid`

* **Where**               : 

    * `$pwid(0-10751) : Pseudowire ID`

    * `$type(0-1): counter type`

    * `$rdmode(0-1): 0 is R2C and 1 is RO`

* **Width**               : `28`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:00]`|`rxpwcnt0`| receive PW group0 counter + type = 0: rx PW byte counter + type = 1: rx PW packet counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Group1 Counter

* **Description**           

The register count Rx PW group1 of rate up to VC4


* **RTL Instant Name**    : `upen_rxpwcnt1`

* **Address**             : `0x4_0000-0x5_FFFF`

* **Formula**             : `0x4_0000 + $type*32768 + $rdmode*16384 + $pwid`

* **Where**               : 

    * `$pwid(0-10751) : Pseudowire ID`

    * `$type(0-3): counter type`

    * `$rdmode(0-3): 0 is R2C and 1 is RO`

* **Width**               : `18`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:00]`|`rxpwcnt1`| receive PW group1 counter + type = 0: rx PW Rbit counter + type = 1: rx PW Pbit counter + type = 2: rx PW Nbit/Mbit counter + type = 3: rx PW Lbit counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Group2 Counter

* **Description**           

The register count Rx PW group2 of rate up to VC4


* **RTL Instant Name**    : `upen_rxpwcnt2`

* **Address**             : `0x6_0000-0x7_FFFF`

* **Formula**             : `0x6_0000 + $type*32768 + $rdmode*16384 + $pwid`

* **Where**               : 

    * `$pwid(0-10751) : Pseudowire ID`

    * `$type(0-3): counter type`

    * `$rdmode(0-3): 0 is R2C and 1 is RO`

* **Width**               : `18`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:00]`|`rxpwcnt2`| receive PW group2 counter + type = 0: rx PW Stray counter + type = 1: rx PW Malform counter + type = 2: rx PW Underrun counter + type = 3: rx PW Overrun counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Group3 Counter

* **Description**           

The register count Rx PW group2 of rate up to VC4


* **RTL Instant Name**    : `upen_rxpwcnt3`

* **Address**             : `0x8_0000-0x9_FFFF`

* **Formula**             : `0x8_0000 + $type*32768 + $rdmode*16384 + $pwid`

* **Where**               : 

    * `$pwid(0-10751) : Pseudowire ID`

    * `$type(0-3): counter type`

    * `$rdmode(0-3): 0 is R2C and 1 is RO`

* **Width**               : `18`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:00]`|`rxpwcnt3`| receive PW group2 counter + type = 0: rx PW LOPS counter + type = 1: rx PW ReorderLost counter + type = 2: rx PW ReorderOk counter + type = 3: rx PW ReorderDrop counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Count High Order Look Up Control

* **Description**           

This register configure lookup from PWID to a pool high speed ID allocated by SW


* **RTL Instant Name**    : `ramcnthotdmlkupcfg`

* **Address**             : `0xF8_000 - 0xFA_9FF`

* **Formula**             : `0xF8_000 + $pwid`

* **Where**               : 

    * `$pwid(0-10751): Pseodowire ID`

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[5]`|`pwcnthighrateen`| Set 1 to indicate the rate of PW is higher than VC4| `RW`| `0x0`| `0x0`|
|`[4:0]`|`highrateswpwid`| Allocated high rate PW ID by solfware| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire High Rate Transmit Byte Counter

* **Description**           

The register count Tx PW byte of rate over VC4


* **RTL Instant Name**    : `upen_txpwcntbyte`

* **Address**             : `0xF_5000-0xF_5FFF`

* **Formula**             : `0xF_5000 + $type*64 + $rdmode*32 + $hipwid`

* **Where**               : 

    * `$pwid(0-31) : Sw High Rate Pseudowire ID`

    * `$rdmode(0-1): 0 is R2C and 1 is RO`

    * `$type(0-3): counter type`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[32:00]`|`hitxpwbytecnt`| high rate transmit PW byte counter + type = 0 : high rate tx PW byte counter + type = 1 : high rate tx PW packet counter + type = 2 : high rate tx PW Rbit counter + type = 3 : high rate tx PW Lbit counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire High Rate Receive Byte Counter

* **Description**           

The register count Rx PW byte of rate over VC4


* **RTL Instant Name**    : `upen_rxpwcntbyte`

* **Address**             : `0xF_6000-0xF_6FFF`

* **Formula**             : `0xF_6000 + $type*64 + $rdmode*32 + $hipwid`

* **Where**               : 

    * `$pwid(0-31) : Sw High Rate Pseudowire ID`

    * `$rdmode(0-1): 0 is R2C and 1 is RO`

    * `$type(0-5): counter type`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[32:00]`|`hirxpwbytecnt`| high rate receive PW byte counter + type = 0 : high rate rx PW byte counter + type = 1 : high rate rx PW packet counter + type = 2 : high rate rx PW Rbit counter + type = 3 : high rate rx PW Lbit counter + type = 4 : high rate rx PW Stray counter + type = 5 : high rate rx PW Malform counter| `RW`| `0x0`| `0x0 End: Begin:`|

###PMR Error Counter

* **Description**           

The register count information as below. Depending on type it 's the events specify.


* **RTL Instant Name**    : `upen_poh_pmr_cnt`

* **Address**             : `0xF_3000 - 0xF_3FFF`

* **Formula**             : `0xF_3000 + 32*$type + $rdmode*16 + $lineid`

* **Where**               : 

    * `$type(0-5): counter type`

    * `$rdmode(0-1): 0 is R2C and 1 is RO`

    * `$lineid(0-15)`

* **Width**               : `20`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:00]`|`pmr_err_cnt`| + type = 0 : rei_l_block_err + type = 1 : rei_l + type = 2 : b2_block_err + type = 3 : b2 + type = 4 : b1_block_err + type = 5 : b1| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Path STS Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_sts_pohpm`

* **Address**             : `0xF_4000-0xF_4FFF`

* **Formula**             : `0xF_4000 + 1024*$type + $rdmode*512 + 8*$stsid + $slcid`

* **Where**               : 

    * `$type(0-1): counter type`

    * `$rdmode(0-1): 0 is R2C and 1 is RO`

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `18`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:00]`|`pohpath_sts_cnt`| pohpath_sts_cnt type = 0:  sts_rei type = 1:  sts_b3| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Path VT Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_vt_pohpm`

* **Address**             : `0xE_0000 - 0xE_FFFF(RW)`

* **Formula**             : `0xE_0000 + 32768*$type + $rdmode*16384 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$type(0-1): counter type`

    * `$rdmode(0-1): 0 is R2C and 1 is RO`

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `12`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11:00]`|`pohpath_vt_cnt`| pohpath_vt_cnt type = 0:  vt_rei type = 1:  vt_bip| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt`

* **Address**             : `0xC_0000-0xD_FFFF(RW)`

* **Formula**             : `0xC_0000 + $type*32768 + $rdmode*16384 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$type(0-3): counter type`

    * `$rdmode(0-1): 0 is R2C and 1 is RO`

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `12`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11:00]`|`poh_vt_cnt`| poh vt pointer + type = 0 :  pohtx_vt_dec + type = 1 :  pohtx_vt_inc + type = 2 :  pohrx_vt_dec + type = 3 :  pohrx_vt_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt0`

* **Address**             : `0xF_000-0xF_0FFF`

* **Formula**             : `0xF_000 + 1024*$type + $rdmode*512 + 8*$stsid + $slcid`

* **Where**               : 

    * `$type(0-1): counter type`

    * `$rdmode(0-1): 0 is R2C and 1 is RO`

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:00]`|`poh_sts_cnt`| poh sts pointer + type = 0 :  pohtx_sts_dec + type = 1 :  pohtx_sts_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt1`

* **Address**             : `0xF_1000-0xF_1FFF`

* **Formula**             : `0xF_1000 + 1024*$type + $rdmode*512 + 8*$stsid + $slcid`

* **Where**               : 

    * `$type(0-1): counter type`

    * `$rdmode(0-1): 0 is R2C and 1 is RO`

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:00]`|`poh_sts_cnt`| poh sts pointer + type = 0 :  pohrx_sts_dec + type = 1 :  pohrx_sts_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds3 cntval Counter

* **Description**           

The register count DS3


* **RTL Instant Name**    : `upen_pdh_de3cnt`

* **Address**             : `0xF_2000-0xF_2FFF`

* **Formula**             : `0xF_2000 + $type*1024 + $rdmode*512 + 8*$stsid + $slcid`

* **Where**               : 

    * `$type(0-3): counter type`

    * `$rdmode(0-1): 0 is R2C and 1 is RO`

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `18`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:00]`|`ds3_cnt_val`| ds3_cnt_val + type = 0 : FE + type = 1 : REI + type = 2 : PB + type = 3 : CB| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 cntval Counter

* **Description**           

The register count DS1


* **RTL Instant Name**    : `upen_pdh_de1cntval`

* **Address**             : `0xA_0000-0xA_FFFF`

* **Formula**             : `0xA_0000 + $type*32768 + $rdmode*16384 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$type(0-2): counter type`

    * `$rdmode(0-1): 0 is R2C and 1 is RO`

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `14`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13:00]`|`ds1_cnt_val`| ds1_cnt_val + type = 0 : FE + type = 1 : CRC + type = 2 : REI| `RW`| `0x0`| `0x0 End:`|

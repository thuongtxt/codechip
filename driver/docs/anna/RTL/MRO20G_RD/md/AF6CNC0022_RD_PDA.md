## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_PDA
####Register Table

|Name|Address|
|-----|-----|
|`Pseudowire PDA Jitter Buffer Control`|`0x00010000 - 0x00012A00 #The address format for these registers is 0x00010000 + PWID`|
|`Pseudowire PDA Jitter Buffer Status`|`0x00014000 - 0x00016A00 #The address format for these registers is 0x00014000 + PWID`|
|`Pseudowire PDA Reorder Control`|`0x00020000 - 0x00022A00 #The address format for these registers is 0x00020000 + PWID`|
|`Pseudowire PDA TDM mode Control`|`0x00030000 - 0x00032A00 #The address format for low order path is 0x00030000 + Oc48ID + TdmOc48Pwid*8`|
|`Pseudowire PDA TDM Look Up Control`|`0x00040000 - 0x000429FF #The address format for low order path is 0x00040000 +  Oc48ID + TdmOc48Pwid*8`|
|`PDA Hold Register Status`|`0x000000 - 0x000002`|
|`Pseudowire PDA per OC48 Low Order TDM PRBS J1 V5 Monitor Status`|`0x00061000 - 0x0006F53F`|
|`Pseudowire PDA ECC CRC Parity Control`|`0x00008`|
|`Pseudowire PDA ECC CRC Parity Disable Control`|`0x00009`|
|`Pseudowire PDA ECC CRC Parity Sticky`|`0x0000A`|
|`Read HA Address Bit3_0 Control`|`0x01000`|
|`Read HA Address Bit7_4 Control`|`0x01010`|
|`Read HA Address Bit11_8 Control`|`0x01020`|
|`Read HA Address Bit15_12 Control`|`0x01030`|
|`Read HA Address Bit19_16 Control`|`0x01040`|
|`Read HA Address Bit23_20 Control`|`0x01050`|
|`Read HA Address Bit24 and Data Control`|`0x01060`|
|`Read HA Hold Data63_32`|`0x01070`|
|`Read HA Hold Data95_64`|`0x01071`|
|`Read HA Hold Data127_96`|`0x01072`|
|`Pseudowire PDA per OC48 Low Order TDM CESoP Small DS0 Control`|`0x00060000 - 0x0006E53F`|
|`Pseudowire PDA TDM OC192 Look Up Control`|`0x00043FFB - 0x00043FFF`|
|`Pseudowire PDA TDM OC192 Mode Control`|`0x00033FFB - 0x00033FFF`|
|`Pseudowire PDA Jitter Buffer Center Control`|`0x10`|
|`Pseudowire PDA Jitter Buffer Center Status`|`0x11`|
|`BERT GEN CONTROL`|`0x7_0000 - 0x7_1000`|
|`BERT GEN CONTROL`|`0x7_0001 - 0x7_1001`|
|`BERT GEN CONTROL`|`0x7_0004 - 0x7_1004`|
|`good counter tdm gen ro`|`0x7_0002 - 0x7_1002`|
|`good counter tdm gen r2c`|`0x7_0003 - 0x7_1003`|
|`BERT GEN CONTROL`|`0x7_2000 - 0x7_3000`|
|`good counter psn  r2c`|`0x7_2001 - 0x7_3001`|
|`good counter psn  ro`|`0x7_2002 - 0x7_3002`|
|`error counter psn  r2c`|`0x7_2003 - 0x7_3003`|
|`error counter psn  ro`|`0x7_2004 - 0x7_3004`|
|`lost counter psn  r2c`|`0x7_2005 - 0x7_3005`|
|`lost counter psn  ro`|`0x7_2006 - 0x7_3006`|
|`lost counter psn  ro`|`0x7_2007 - 0x7_3007`|
|`lost counter psn  ro`|`0x7_2008 - 0x7_3008`|


###Pseudowire PDA Jitter Buffer Control

* **Description**           

This register configures jitter buffer parameters per pseudo-wire

HDL_PATH: rtljitbuf.ramjitbufcfg.ram.ram[$PWID]


* **RTL Instant Name**    : `ramjitbufcfg`

* **Address**             : `0x00010000 - 0x00012A00 #The address format for these registers is 0x00010000 + PWID`

* **Formula**             : `0x00010000 +  PWID`

* **Where**               : 

    * `$PWID(0-10751): Pseudowire ID`

* **Width**               : `58`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[57]`|`pwlowds0mode`| Pseudo-wire CES Low DS0 mode| `RW`| `0x0`| `0x0 1: CESoP Pseodo-wire with NxDS0 <= 3 0: Other Pseodo-wire mode`|
|`[56]`|`pwcepmode`| Pseudo-wire CEP mode indication| `RW`| `0x0`| `0x0 1: CEP Pseodo-wire 0: CES Pseodo-wire`|
|`[55:53]`|`pwholooc48id`| Indicate 8x Hi order OC48 or 8x Low order OC48 slice| `RW`| `0x0`| `0x0`|
|`[52:42]`|`pwtdmlineid`| Pseudo-wire(PW) corresponding TDM line ID. If the PW belong to Low order path, this is the OC48 TDM line ID that is using in Lo CDR,PDH and MAP. If the PW belong to Hi order CEP path, this is the OC48 master STS ID| `RW`| `0x0`| `0x0`|
|`[41]`|`pweparen`| Pseudo-wire EPAR timing mode enable 1: Enable EPAR timing mode 0: Disable EPAR timing mode| `RW`| `0x0`| `0x0`|
|`[40]`|`pwhilopathind`| Pseudo-wire belong to Hi-order or Lo-order path 1: Pseudo-wire belong to Hi-order path 0: Pseudo-wire belong to Lo-order path| `RW`| `0x0`| `0x0`|
|`[39:26]`|`pwpayloadlen`| TDM Payload of pseudo-wire in byte unit| `RW`| `0x0`| `0x0`|
|`[25:13]`|`pdvsizeinpkunit`| Pdv size in packet unit. This parameter is to prevent packet delay variation(PDV) from PSN. PdvSizePk is packet delay variation measured in packet unit. The formula is as below: PdvSizeInPkUnit = (PwSpeedinKbps * PdvInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps * PdvInUsus)%(8000*PwPayloadLen) !=0)| `RW`| `0x0`| `0x0`|
|`[12:0]`|`jitbufsizeinpkunit`| Jitter buffer size in packet unit. This parameter is mostly double of PdvSizeInPkUnit. The formula is as below: JitBufSizeInPkUnit = (PwSpeedinKbps * JitBufInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps * JitBufInUsus)%(8000*PwPayloadLen) !=0)| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA Jitter Buffer Status

* **Description**           

This register shows jitter buffer status per pseudo-wire


* **RTL Instant Name**    : `ramjitbufsta`

* **Address**             : `0x00014000 - 0x00016A00 #The address format for these registers is 0x00014000 + PWID`

* **Formula**             : `0x00014000 +  PWID`

* **Where**               : 

    * `$PWID(0-10751): Pseudowire ID`

* **Width**               : `55`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[54:20]`|`jitbufhwcomsta`| Harware debug only status| `RO`| `0x0`| `0x0`|
|`[19:4]`|`jitbufnumpk`| Current number of packet in jitter buffer| `RW`| `0x0`| `0x0`|
|`[3]`|`jitbuffull`| Jitter Buffer full status| `RW`| `0x0`| `0x0`|
|`[2:0]`|`jitbufstate`| Jitter buffer state machine status 0: START 1: FILL 2: READY 3: READ 4: LOST 5: NEAR_EMPTY Others: NOT VALID| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA Reorder Control

* **Description**           

This register configures reorder parameters per pseudo-wire


* **RTL Instant Name**    : `ramreorcfg`

* **Address**             : `0x00020000 - 0x00022A00 #The address format for these registers is 0x00020000 + PWID`

* **Formula**             : `0x00020000 +  PWID`

* **Where**               : 

    * `$PWID(0-10751): Pseudowire ID HDL_PATH: ramreorcfg.ram.ram[$PWID]`

* **Width**               : `26`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[25:17]`|`pwsetlofsinpk`| Number of consecutive lost packet to declare lost of packet state| `RW`| `0x0`| `0x0`|
|`[16:9]`|`pwsetlopsinmsec`| Number of empty time to declare lost of packet synchronization| `RW`| `0x0`| `0x0`|
|`[8]`|`pwreoren`| Set 1 to enable reorder| `RW`| `0x0`| `0x0`|
|`[7:0]`|`pwreortimeout`| Reorder timeout in 512us unit to detect lost packets. The formula is as below: ReorTimeout = ((Min(31,PdvSizeInPk) * PwPayloadLen * 16)/PwSpeedInKbps| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA TDM mode Control

* **Description**           

This register configure TDM mode for interworking between Pseudowire and Lo TDM


* **RTL Instant Name**    : `ramtdmmodecfg`

* **Address**             : `0x00030000 - 0x00032A00 #The address format for low order path is 0x00030000 + Oc48ID + TdmOc48Pwid*8`

* **Formula**             : `0x00030000 + $Oc48ID + $TdmOc48Pwid*8`

* **Where**               : 

    * `$Oc48ID(0-7): OC48 ID`

    * `$TdmOc48Pwid(0-1343): TDM OC48 slice PWID HDL_PATH: rtlpdatdm.rtldeas.ramdeascfg.ram.ram[$Oc48ID + $TdmOc48Pwid*8] #HDL_PATH: rtlpdatdm.rtlencfifo[$Oc48ID].rtlencfifo48.memcepcfg.ram.ram[$TdmOc48Pwid]`

* **Width**               : `29`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28]`|`pdardioff`| Set 1 to disable sending RDI from CES PW to TDM in case Mbit/Rbit of CESoP PW| `RW`| `0x0`| `0x0`|
|`[27:23]`|`reserved`| *n/a*| `RO`| `0x0`| `0x0`|
|`[22]`|`pdalbitrepmode`| Mode to replace Lbit packet 0: Replace by AIS (default) 1: Replace by a configuration idle code Replace by a configuration idle code| `RW`| `0x0`| `0x0`|
|`[21:14]`|`pdaidlecode`| Idle pattern to replace data in case of Lost/Lbit packet| `RW`| `0x0`| `0x0`|
|`[13]`|`pdaaisoff`| Set 1 to disable sending AIS from CES PW to TDM in case Lbit or Lost packet replace AIS| `RW`| `0x0`| `0x0`|
|`[12:11]`|`pdarepmode`| Mode to replace lost packet 0: Replace by replaying previous good packet (often for voice or video application) 1: Replace by AIS (default) 2: Replace by a configuration idle code Replace by a configuration idle code| `RW`| `0x0`| `0x0`|
|`[10:5]`|`pdastsid`| Used in DS3/E3 SAToP  and TU3/VC3/Vc4/VC4-4C/VC4-16C CEP basic, per slice48 corresponding master STSID of of PW circuit| `RW`| `0x0`| `0x0`|
|`[4:2]`|`pdasigtype`| TDM Payload De-Assembler signal type 0: E3,DS3,TU3,VC3 1: VC4 2: VC4-4C 3: VC4-8C,Vc4-16C 4: VC11,VC12,E1,T1,NxDS0| `RW`| `0x0`| `0x0 Others: reserved`|
|`[1:0]`|`pdamode`| TDM Payload De-Assembler modes 0: DS1/E1 SAToP 1: CESoP without CAS 2: CEP 3: reserved| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA TDM Look Up Control

* **Description**           

This register configure lookup from TDM PWID to global 10752 PWID


* **RTL Instant Name**    : `ramtdmlkupcfg`

* **Address**             : `0x00040000 - 0x000429FF #The address format for low order path is 0x00040000 +  Oc48ID + TdmOc48Pwid*8`

* **Formula**             : `0x00040000 + $Oc48ID + $TdmOc48Pwid*8`

* **Where**               : 

    * `$Oc48ID(0-7): OC48 ID`

    * `$TdmOc48Pwid(0-1343): TDM OC48 slice PWID HDL_PATH: rtlpdatdm.rtlrdca.pwlkcfgram.ram.ram[$Oc48ID + $TdmOc48Pwid*8]`

* **Width**               : `15`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14:1]`|`pwid`| Flat 10752 CES PWID| `RW`| `0x0`| `0x0`|
|`[0]`|`pwlkenable`| Enable Lookup| `RW`| `0x0`| `0x0 End: Begin:`|

###PDA Hold Register Status

* **Description**           

This register using for hold remain that more than 128bits


* **RTL Instant Name**    : `pda_hold_status`

* **Address**             : `0x000000 - 0x000002`

* **Formula**             : `0x000000 +  HID`

* **Where**               : 

    * `$HID(0-2): Hold ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pdaholdstatus`| Hold 32bits| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA per OC48 Low Order TDM PRBS J1 V5 Monitor Status

* **Description**           

This register show the PRBS monitor status after PDA


* **RTL Instant Name**    : `ramlotdmprbsmon`

* **Address**             : `0x00061000 - 0x0006F53F`

* **Formula**             : `0x00061000 + Oc48ID*8192 + TdmOc48PwID`

* **Where**               : 

    * `$Oc48ID(0-7): OC48 ID`

    * `$TdmOc48PwID(0-1343): TDM OC48 slice PWID`

* **Width**               : `18`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17]`|`pdaprbsb3v5err`| PDA PRBS or B3 or V5 monitor error| `RC`| `0x0`| `0x0`|
|`[16]`|`pdaprbssyncsta`| PDA PRBS sync status| `RW`| `0x0`| `0x0`|
|`[15:0]`|`pdaprbsvalue`| PDA PRBS monitor value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA ECC CRC Parity Control

* **Description**           

This register configures PDA ECC CRC and Parity.


* **RTL Instant Name**    : `pda_config_ecc_crc_parity_control`

* **Address**             : `0x00008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `7`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`pdaforceecccor`| PDA force link list Ecc error correctable   1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[5]`|`pdaforceeccerr`| PDA force link list Ecc error noncorrectable   1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[4]`|`pdaforcecrcerr`| PDA force data CRC error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[3]`|`pdaforcelotdmparerr`| Pseudowire PDA Low Order TDM mode Control force parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[2]`|`pdaforcetdmlkparerr`| Pseudowire PDA Lo and Ho TDM Look Up Control force parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[1]`|`pdaforcejitbufparerr`| Pseudowire PDA Jitter Buffer Control force parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[0]`|`pdaforcereorderparerr`| Pseudowire PDA Reorder Control force parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA ECC CRC Parity Disable Control

* **Description**           

This register configures PDA ECC CRC and Parity Disable.


* **RTL Instant Name**    : `pda_config_ecc_crc_parity_disable_control`

* **Address**             : `0x00009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `7`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`pdadisableecccor`| PDA disable link list Ecc error correctable   1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[5]`|`pdadisableeccerr`| PDA disable link list Ecc error noncorrectable   1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[4]`|`pdadisablecrcerr`| PDA disable data CRC error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[3]`|`pdadisablelotdmparerr`| Pseudowire PDA Low Order TDM mode Control disable parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[2]`|`pdadisabletdmlkparerr`| Pseudowire PDA Lo and Ho TDM Look Up Control disable parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[1]`|`pdadisablejitbufparerr`| Pseudowire PDA Jitter Buffer Control disable parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[0]`|`pdadisablereorderparerr`| Pseudowire PDA Reorder Control disable parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA ECC CRC Parity Sticky

* **Description**           

This register configures PDA ECC CRC and Parity.


* **RTL Instant Name**    : `pda_config_ecc_crc_parity_sticky`

* **Address**             : `0x0000A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `7`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`pdastickyecccor`| PDA sticky link list Ecc error correctable   1: Set  0: Clear| `W1C`| `0x0`| `0x0`|
|`[5]`|`pdastickyeccerr`| PDA sticky link list Ecc error noncorrectable   1: Set  0: Clear| `W1C`| `0x0`| `0x0`|
|`[4]`|`pdastickycrcerr`| PDA sticky data CRC error  1: Set  0: Clear| `W1C`| `0x0`| `0x0`|
|`[3]`|`pdastickylotdmparerr`| Pseudowire PDA Low Order TDM mode Control sticky parity error  1: Set  0: Clear| `W1C`| `0x0`| `0x0`|
|`[2]`|`pdastickytdmlkparerr`| Pseudowire PDA Lo and Ho TDM Look Up Control sticky parity error  1: Set  0: Clear| `W1C`| `0x0`| `0x0`|
|`[1]`|`pdastickyjitbufparerr`| Pseudowire PDA Jitter Buffer Control sticky parity error  1: Set  0: Clear| `W1C`| `0x0`| `0x0`|
|`[0]`|`pdastickyreorderparerr`| Pseudowire PDA Reorder Control sticky parity error  1: Set  0: Clear| `W1C`| `0x0`| `0x0 End: Begin:`|

###Read HA Address Bit3_0 Control

* **Description**           

This register is used to send HA read address bit3_0 to HA engine


* **RTL Instant Name**    : `rdha3_0_control`

* **Address**             : `0x01000`

* **Formula**             : `0x01000 + HaAddr3_0`

* **Where**               : 

    * `$HaAddr3_0(0-15): HA Address Bit3_0`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr3_0`| Read value will be 0x01000 plus HaAddr3_0| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit7_4 Control

* **Description**           

This register is used to send HA read address bit7_4 to HA engine


* **RTL Instant Name**    : `rdha7_4_control`

* **Address**             : `0x01010`

* **Formula**             : `0x01010 + HaAddr7_4`

* **Where**               : 

    * `$HaAddr7_4(0-15): HA Address Bit7_4`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr7_4`| Read value will be 0x01000 plus HaAddr7_4| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit11_8 Control

* **Description**           

This register is used to send HA read address bit11_8 to HA engine


* **RTL Instant Name**    : `rdha11_8_control`

* **Address**             : `0x01020`

* **Formula**             : `0x01020 + HaAddr11_8`

* **Where**               : 

    * `$HaAddr11_8(0-15): HA Address Bit11_8`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr11_8`| Read value will be 0x01000 plus HaAddr11_8| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit15_12 Control

* **Description**           

This register is used to send HA read address bit15_12 to HA engine


* **RTL Instant Name**    : `rdha15_12_control`

* **Address**             : `0x01030`

* **Formula**             : `0x01030 + HaAddr15_12`

* **Where**               : 

    * `$HaAddr15_12(0-15): HA Address Bit15_12`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr15_12`| Read value will be 0x01000 plus HaAddr15_12| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit19_16 Control

* **Description**           

This register is used to send HA read address bit19_16 to HA engine


* **RTL Instant Name**    : `rdha19_16_control`

* **Address**             : `0x01040`

* **Formula**             : `0x01040 + HaAddr19_16`

* **Where**               : 

    * `$HaAddr19_16(0-15): HA Address Bit19_16`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr19_16`| Read value will be 0x01000 plus HaAddr19_16| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit23_20 Control

* **Description**           

This register is used to send HA read address bit23_20 to HA engine


* **RTL Instant Name**    : `rdha23_20_control`

* **Address**             : `0x01050`

* **Formula**             : `0x01050 + HaAddr23_20`

* **Where**               : 

    * `$HaAddr23_20(0-15): HA Address Bit23_20`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr23_20`| Read value will be 0x01000 plus HaAddr23_20| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit24 and Data Control

* **Description**           

This register is used to send HA read address bit24 to HA engine to read data


* **RTL Instant Name**    : `rdha24data_control`

* **Address**             : `0x01060`

* **Formula**             : `0x01060 + HaAddr24`

* **Where**               : 

    * `$HaAddr24(0-1): HA Address Bit24`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata31_0`| HA read data bit31_0| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data63_32

* **Description**           

This register is used to read HA dword2 of data.


* **RTL Instant Name**    : `rdha_hold63_32`

* **Address**             : `0x01070`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata63_32`| HA read data bit63_32| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data95_64

* **Description**           

This register is used to read HA dword3 of data.


* **RTL Instant Name**    : `rdindr_hold95_64`

* **Address**             : `0x01071`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata95_64`| HA read data bit95_64| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data127_96

* **Description**           

This register is used to read HA dword4 of data.


* **RTL Instant Name**    : `rdindr_hold127_96`

* **Address**             : `0x01072`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata127_96`| HA read data bit127_96| `RO`| `0xx`| `0xx End: Begin:`|

###Pseudowire PDA per OC48 Low Order TDM CESoP Small DS0 Control

* **Description**           

This register show the PRBS monitor status after PDA


* **RTL Instant Name**    : `ramlotdmsmallds0control`

* **Address**             : `0x00060000 - 0x0006E53F`

* **Formula**             : `0x00060000 + Oc48ID*8192 + TdmOc48PwID`

* **Where**               : 

    * `$Oc48ID(0-3): OC48 ID`

    * `TdmOc48PwID(0-1343): TDM OC48 slice PWID`

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`pdasmallds0en`| PDA CESoP small DS0 enable 1: CESoP Pseodo-wire with NxDS0 <= 3 0: Other Pseodo-wire modes| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA TDM OC192 Look Up Control

* **Description**           

This register configure lookup from TDM 192 ID to global 10752 PWID


* **RTL Instant Name**    : `ramtdm192lkupcfg`

* **Address**             : `0x00043FFB - 0x00043FFF`

* **Formula**             : `0x00043FFB + $Oc192ID*4`

* **Where**               : 

    * `$Oc192ID(0-1): OC192 ID HDL_PATH: rtlpdatdm.rtlrdca.pwlkcfgram.ram.ram[$Oc192ID*8 + 16379]`

* **Width**               : `15`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14:1]`|`pwid`| Flat 10752 CES PWID| `RW`| `0x0`| `0x0`|
|`[0]`|`pwlkenable`| Enable Lookup| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA TDM OC192 Mode Control

* **Description**           

This register configure TDM mode enable for interworking between Pseudowire and OCN OC192 line


* **RTL Instant Name**    : `ramtdmmode192cfg`

* **Address**             : `0x00033FFB - 0x00033FFF`

* **Formula**             : `0x00033FFB + $Oc192ID*4`

* **Where**               : 

    * `$Oc192ID(0-1): OC192 ID HDL_PATH: rtlpdatdm.rtldeas.ramdeascfg.ram.ram[$Oc192ID*8 + 16379]`

* **Width**               : `28`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[21:14]`|`pdaidlecode`| Idle pattern to replace data in case of Lost/Lbit packet| `RW`| `0x0`| `0x0`|
|`[13]`|`pdaaisrdioff`| Set 1 to disable sending AIS/Unequip/RDI from CES PW to TDM| `RW`| `0x0`| `0x0`|
|`[12:11]`|`pdarepmode`| Mode to replace lost and Lbit packet 0: Replace by replaying previous good packet (often for voice or video application) 1: Replace by AIS (default) 2: Replace by a configuration idle code Replace by a configuration idle code| `RW`| `0x0`| `0x0`|
|`[10:5]`|`pdastsid`| Unused for 192C CEP case| `RW`| `0x0`| `0x0`|
|`[4:2]`|`pdasigtype`| Unused for 192C CEP case| `RW`| `0x0`| `0x0 Others: reserved`|
|`[1:0]`|`pdamode`| TDM Payload De-Assembler modes 0: DS1/E1 SAToP 1: CESoP without CAS 2: CEP 3: reserved| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA Jitter Buffer Center Control

* **Description**           

This register configures jitter buffer centering enable


* **RTL Instant Name**    : `ramjitbufcentercfg`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `15`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14:1]`|`centerpwid`| Pseudowire need to be centered| `RW`| `0x0`| `0x0`|
|`[0]`|`centerjbreq`| Set 1 to request center jitter buffer, clear 0 when finish centering| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA Jitter Buffer Center Status

* **Description**           

This register show jitter buffer centering status


* **RTL Instant Name**    : `ramjitbufcentersta`

* **Address**             : `0x11`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`centerjbsta`| Sw poll value 0 then start centering and poll again until 0 to finish center jitter buffer| `RO`| `0x0`| `0x0 End: Begin:`|

###BERT GEN CONTROL

* **Description**           

This register config mode for bert


* **RTL Instant Name**    : `bert_gen_config`

* **Address**             : `0x7_0000 - 0x7_1000`

* **Formula**             : `0x7_0000 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:5]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[4]`|`bertinv`| Set 1 to select invert mode| `RW`| `0x0`| `0x0`|
|`[3]`|`bertena`| Set 1 to enable BERT| `RW`| `0x0`| `0x0`|
|`[2:0]`|`bertmode`| "0" prbs15, "1" prbs23, "2" prbs31,"3" prbs20, "4" prbs20r, "5" all1, "6" all0| `RW`| `0x0`| `0x0 End: Begin:`|

###BERT GEN CONTROL

* **Description**           

This register config rate for force error


* **RTL Instant Name**    : `bert_gen_force`

* **Address**             : `0x7_0001 - 0x7_1001`

* **Formula**             : `0x7_0001 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ber_rate`| TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to Pattern Generator [31:0] == 32'd0          :disable BER_level_val	BER_level 1000:        	BER 10e-3 10_000:      	BER 10e-4 100_000:     	BER 10e-5 1_000_000:   	BER 10e-6 10_000_000:  	BER 10e-7| `RW`| `0x0`| `0x0 End: Begin:`|

###BERT GEN CONTROL

* **Description**           

This register config rate for force error


* **RTL Instant Name**    : `bert_gen_force_sing`

* **Address**             : `0x7_0004 - 0x7_1004`

* **Formula**             : `0x7_0004 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`singe_err`| sw write "1" to force, hw auto clear| `RW`| `0x0`| `0x0 End: Begin:`|

###good counter tdm gen ro

* **Description**           

Counter


* **RTL Instant Name**    : `goodbit_pen_tdm_gen_ro`

* **Address**             : `0x7_0002 - 0x7_1002`

* **Formula**             : `0x7_0002 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `34`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[33:00]`|`bertgenro`| gen goodbit mode read only| `RO`| `0x0`| `0x0 End: Begin:`|

###good counter tdm gen r2c

* **Description**           

Counter


* **RTL Instant Name**    : `goodbit_pen_tdm_gen_r2c`

* **Address**             : `0x7_0003 - 0x7_1003`

* **Formula**             : `0x7_0003 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `34`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[33:00]`|`bertgenr2c`| gen goodbit mode read 2 clear| `RC`| `0x0`| `0x0 End: Begin:`|

###BERT GEN CONTROL

* **Description**           

This register config mode for bert


* **RTL Instant Name**    : `bert_monpsn_config`

* **Address**             : `0x7_2000 - 0x7_3000`

* **Formula**             : `0x7_2000 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10]`|`bert_inv`| Set 1 to select invert mode| `RW`| `0x0`| `0x0`|
|`[9:7]`|`thr_err`| Thrhold declare lost syn sta| `RW`| `0x3`| `0x3`|
|`[6:4]`|`thr_syn`| Thrhold declare syn sta| `RW`| `0x3`| `0x3`|
|`[3]`|`bertena`| Set 1 to enable BERT| `RW`| `0x0`| `0x0`|
|`[2:0]`|`bertmode`| "0" prbs15, "1" prbs23, "2" prbs31,"3" prbs20, "4" prbs20r, "5" all1, "6" all0| `RW`| `0x0`| `0x0 End: Begin:`|

###good counter psn  r2c

* **Description**           

Counter


* **RTL Instant Name**    : `goodbit_pen_psn_mon_r2c`

* **Address**             : `0x7_2001 - 0x7_3001`

* **Formula**             : `0x7_2001 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `34`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[33:00]`|`goodmonr2c`| mon goodbit mode read 2clear| `RC`| `0x0`| `0x0 End: Begin:`|

###good counter psn  ro

* **Description**           

Counter


* **RTL Instant Name**    : `goodbit_pen_psn_mon_ro`

* **Address**             : `0x7_2002 - 0x7_3002`

* **Formula**             : `0x7_2002 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `34`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[33:00]`|`goodmonro`| mon goodbit mode read only| `RO`| `0x0`| `0x0 End: Begin:`|

###error counter psn  r2c

* **Description**           

Counter


* **RTL Instant Name**    : `errorbit_pen_psn_mon_r2c`

* **Address**             : `0x7_2003 - 0x7_3003`

* **Formula**             : `0x7_2003 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`errormonr2c`| mon errorbit mode read 2clear| `RC`| `0x0`| `0x0 End: Begin:`|

###error counter psn  ro

* **Description**           

Counter


* **RTL Instant Name**    : `errorbit_pen_psn_mon_ro`

* **Address**             : `0x7_2004 - 0x7_3004`

* **Formula**             : `0x7_2004 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`errormonro`| mon errorbit mode read only| `RO`| `0x0`| `0x0 End: Begin:`|

###lost counter psn  r2c

* **Description**           

Counter


* **RTL Instant Name**    : `lostbit_pen_psn_mon_r2c`

* **Address**             : `0x7_2005 - 0x7_3005`

* **Formula**             : `0x7_2005 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`lostmonr2c`| mon lostbit mode read 2clear| `RC`| `0x0`| `0x0 End: Begin:`|

###lost counter psn  ro

* **Description**           

Counter


* **RTL Instant Name**    : `lostbit_pen_psn_mon_ro`

* **Address**             : `0x7_2006 - 0x7_3006`

* **Formula**             : `0x7_2006 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`lostmonro`| mon lostbit mode read only| `RO`| `0x0`| `0x0 End: Begin:`|

###lost counter psn  ro

* **Description**           

Status


* **RTL Instant Name**    : `lostbit_pen_psn_mon_ro`

* **Address**             : `0x7_2007 - 0x7_3007`

* **Formula**             : `0x7_2007 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`staprbs`| "3" SYNC , other LOST| `R0`| `0x0`| `0x0 End: Begin:`|

###lost counter psn  ro

* **Description**           

Sticky


* **RTL Instant Name**    : `lostbit_pen_psn_mon_ro`

* **Address**             : `0x7_2008 - 0x7_3008`

* **Formula**             : `0x7_2008 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0:0]`|`stickyprbs`| "1" LOSTSYN| `W1C`| `0x0`| `0x1 End:`|

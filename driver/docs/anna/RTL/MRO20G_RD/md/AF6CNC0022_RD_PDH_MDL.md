## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_PDH_MDL
####Register Table

|Name|Address|
|-----|-----|
|`Buff Message MDL BUFFER1`|`0x0000 - 0x025F`|
|`Buff Message MDL BUFFER1_2`|`0x0260 - 0x038F`|
|`Config Buff Message MDL BUFFER2`|`0x0400 - 0x065F`|
|`Config Buff Message MDL BUFFER2_2`|`0x0660 - 0x078F`|
|`SET TO HAVE PACKET MDL BUFFER 1`|`0x0840 - 0x86F`|
|`SET TO HAVE PACKET MDL BUFFER 2`|`0x0880 - 0x8AF`|
|`CONFIG MDL Tx`|`0x08C0 - 0x8EF`|
|`COUNTER VALID MESSAGE TX MDL IDLE`|`0x0C00 - 0xD2F`|
|`COUNTER VALID MESSAGE TX MDL PATH`|`0x0C40 - 0x0D6F`|
|`COUNTER VALID MESSAGE TX MDL TEST`|`0x0C80 - 0xDAF`|
|`COUNTER BYTE MESSAGE TX MDL IDLE`|`0x0E00 - 0x0F2F`|
|`COUNTER BYTE MESSAGE TX MDL PATH`|`0x0E40 - 0x0F6F`|
|`COUNTER BYTE MESSAGE TX MDL TEST R2C`|`0x0E80 - 0x0FAF`|
|`Buff Message MDL with configuration type`|`0x44000 - 0x47FFF`|
|`Config Buff Message MDL TYPE`|`0x4A800 - 0x4A97F`|
|`CONFIG CONTROL DESTUFF 0`|`0x4AA00`|
|`STICKY TO RECEIVE PACKET MDL BUFF CONFIG1`|`0x4AA10`|
|`STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 2`|`0x4AA11`|
|`STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 3`|`0x4AA12`|
|`STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 4`|`0x4AA13`|
|`STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 5`|`0x4AA14`|
|`STICKY TO RECEIVE PACKET MDL BUFF CONFIG 6`|`0x4AA15`|
|`STICKY TO RECEIVE PACKET MDL BUFF CONFIG7`|`0x4AA16`|
|`STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 8`|`0x4AA17`|
|`STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 9`|`0x4AA18`|
|`STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 10`|`0x4AA19`|
|`STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 11`|`0x4AA1A`|
|`STICKY TO RECEIVE PACKET MDL BUFF CONFIG 12`|`0x4AA1B`|
|`COUNTER BYTE MESSAGE RX MDL IDLE`|`0x4D000 - 0x4D97F`|
|`COUNTER BYTE MESSAGE RX MDL PATH`|`0x4D200 - 0x4DB7F`|
|`COUNTER BYTE MESSAGE RX MDL TEST`|`0x4D400 - 0x4DF7F`|
|`COUNTER GOOD MESSAGE RX MDL IDLE`|`0x4E000 - 0x4F17F`|
|`COUNTER GOOD MESSAGE RX MDL PATH`|`0x4E200 - 0x4F37F`|
|`COUNTER GOOD MESSAGE RX MDL TEST`|`0x4E400 - 0x4F57F`|
|`COUNTER DROP MESSAGE RX MDL IDLE`|`0x4E600 - 0x4F77F`|
|`COUNTER DROP MESSAGE RX MDL PATH`|`0x4E800 - 0x4F97F`|
|`COUNTER DROP MESSAGE RX MDL TEST R2C`|`0x4EA00 - 0x4FBFF`|
|`RX MDL INT STA`|`0x4AA06`|
|`MDL per Channel Interrupt1 Enable Control`|`0x4A000-0x4A0FF`|
|`MDL Interrupt1 per Channel Interrupt Status`|`0x4A200-0x4A2FF`|
|`MDL Interrupt per Channel Current Status`|`0x4A400-0x4A4FF`|
|`MDL Interrupt1 per Channel Interrupt OR Status`|`0x4A600-0x4A607`|
|`MDL Interrupt1 OR Status`|`0x4A6FF`|
|`MDL Interrupt1 Enable Control`|`0x4A6FE`|
|`MDL per Channel Interrupt2 Enable Control`|`0x4A100-0x4A1FF`|
|`MDL Interrupt2 per Channel Interrupt Status`|`0x4A300-0x4A3FF`|
|`MDL Interrupt per Channel Current Status`|`0x4A500-0x4A5FF`|
|`MDL Interrupt2 per Channel Interrupt OR Status`|`0x4A700-0x4A707`|
|`MDL Interrupt2 OR Status`|`0x4A7FF`|
|`MDL Interrupt2 Enable Control`|`0x4A7FE`|


###Buff Message MDL BUFFER1

* **Description**           

config message MDL BUFFER Channel ID 0-31


* **RTL Instant Name**    : `upen_mdl_buffer1`

* **Address**             : `0x0000 - 0x025F`

* **Formula**             : `0x0000+ $SLICEID*0x1000 + $DWORDID*32 + $DE3ID1`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DWORDID (0-18) : DWORD ID`

    * `$DE3ID1(0-31)  : DE3 ID1`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`idle_byte13`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`ilde_byte12`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`idle_byte11`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`idle_byte10`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###Buff Message MDL BUFFER1_2

* **Description**           

config message MDL BUFFER Channel ID 32-47


* **RTL Instant Name**    : `upen_mdl_buffer1_2`

* **Address**             : `0x0260 - 0x038F`

* **Formula**             : `0x0260+ $SLICEID*0x1000 + $DWORDID*16 + $DE3ID2`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DWORDID (0-18) : DWORD ID`

    * `$DE3ID2 (0-15) : DE3 ID2`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`idle_byte23`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`idle_byte22`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`idle_byte21`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`idle_byte20`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###Config Buff Message MDL BUFFER2

* **Description**           

config message MDL BUFFER2 Channel ID 0-15


* **RTL Instant Name**    : `upen_mdl_tp1`

* **Address**             : `0x0400 - 0x065F`

* **Formula**             : `0x0400+$SLICEID*0x1000 + $DE3ID1 + $DWORDID*16`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DWORDID (0-18) : DWORD ID`

    * `$DE3ID1(0-31)  : DS3 E3 ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`tp_byte13`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`tp_byte12`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`tp_byte11`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`tp_byte10`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###Config Buff Message MDL BUFFER2_2

* **Description**           

config message MDL BUFFER2 Channel ID 16-23


* **RTL Instant Name**    : `upen_mdl_tp2`

* **Address**             : `0x0660 - 0x078F`

* **Formula**             : `0x0660+$SLICEID*0x1000 + $DWORDID*8+ $DE3ID2`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DWORDID (0-18) : DWORD ID`

    * `$DE3ID2 (0-15) : DE3 ID2`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`tp_byte23`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`tp_byte22`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`tp_byte21`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`tp_byte20`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###SET TO HAVE PACKET MDL BUFFER 1

* **Description**           

SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1


* **RTL Instant Name**    : `upen_sta_idle_alren`

* **Address**             : `0x0840 - 0x86F`

* **Formula**             : `0x0840+$SLICEID*0x1000 +$DE3ID`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DE3ID (0-47) : DE3 ID`

* **Width**               : `1`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00:00]`|`idle_cfgen`| (0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 0| `R/W`| `0x0`| `0x0 End: Begin:`|

###SET TO HAVE PACKET MDL BUFFER 2

* **Description**           

SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 2


* **RTL Instant Name**    : `upen_sta_tp_alren`

* **Address**             : `0x0880 - 0x8AF`

* **Formula**             : `0x0880+$SLICEID*0x1000 +$DE3ID`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DE3ID (0-47) : DE3 ID`

* **Width**               : `1`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00:00]`|`tp_cfgen`| (0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 1| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG MDL Tx

* **Description**           

Config Tx MDL


* **RTL Instant Name**    : `upen_cfg_mdl`

* **Address**             : `0x08C0 - 0x8EF`

* **Formula**             : `0x08C0+$SLICEID*0x1000 +$DE3ID`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DE3ID (0-47) : DE3 ID`

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`cfg_seq_tx`| config enable Tx continous, (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`cfg_entx`| config enable Tx, (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`cfg_fcs_tx`| config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`cfg_mdlstd_tx`| config standard Tx, (0) is ANSI, (1) is AT&T| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfg_mdl_cr`| config bit command/respond MDL message, bit 0 is channel 0 of DS3| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER VALID MESSAGE TX MDL IDLE

* **Description**           

counter valid read to clear IDLE message MDL


* **RTL Instant Name**    : `upen_txmdl_cntr2c_valididle1`

* **Address**             : `0x0C00 - 0xD2F`

* **Formula**             : `0x0C00+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256`

* **Where**               : 

    * `$DE3ID (0-47) : DE3 ID`

    * `$UPRO (0-1) : UP READ ONLY`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_valid_idle_mdl`| value counter valid| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER VALID MESSAGE TX MDL PATH

* **Description**           

counter valid read to clear PATH message MDL


* **RTL Instant Name**    : `upen_txmdl_cntr2c_validpath`

* **Address**             : `0x0C40 - 0x0D6F`

* **Formula**             : `0x0C40+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256`

* **Where**               : 

    * `$DE3ID (0-47) : DE3 ID`

    * `$UPRO (0-1) : UP READ ONLY`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_valid_path_mdl`| value counter valid| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER VALID MESSAGE TX MDL TEST

* **Description**           

counter valid read to clear TEST message MDL


* **RTL Instant Name**    : `upen_txmdl_cntr2c_validtest`

* **Address**             : `0x0C80 - 0xDAF`

* **Formula**             : `0x0C80+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256`

* **Where**               : 

    * `$DE3ID (0-47) : DE3 ID`

    * `$UPRO (0-1) : UP READ ONLY`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_valid_test_mdl`| value counter valid| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE TX MDL IDLE

* **Description**           

counter byte read to clear IDLE message MDL


* **RTL Instant Name**    : `upen_txmdl_cntr2c_byteidle`

* **Address**             : `0x0E00 - 0x0F2F`

* **Formula**             : `0x0E00+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256`

* **Where**               : 

    * `$DE3ID (0-47) : DE3 ID`

    * `$UPRO (0-1) : UP READ ONLY`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `18`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:00]`|`cntr2c_byte_idle_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE TX MDL PATH

* **Description**           

counter byte read to clear PATH message MDL


* **RTL Instant Name**    : `upen_txmdl_cntr2c_bytepath`

* **Address**             : `0x0E40 - 0x0F6F`

* **Formula**             : `0x0E40+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256`

* **Where**               : 

    * `$DE3ID (0-47) : DE3 ID`

    * `$UPRO (0-1) : UP READ ONLY`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:00]`|`cntr2c_byte_path_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE TX MDL TEST R2C

* **Description**           

counter byte read to clear TEST message MDL


* **RTL Instant Name**    : `upen_txmdl_cntr2c_bytetest`

* **Address**             : `0x0E80 - 0x0FAF`

* **Formula**             : `0x0E80+ $SLICEID*0x1000 + $DE3ID + $UPRO * 256`

* **Where**               : 

    * `$DE3ID (0-47) : DE3 ID`

    * `$UPRO (0-1) : UP READ ONLY`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:00]`|`cntr2c_byte_test_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###Buff Message MDL with configuration type

* **Description**           

buffer message MDL which is configured type


* **RTL Instant Name**    : `upen_rxmdl_typebuff`

* **Address**             : `0x44000 - 0x47FFF`

* **Formula**             : `0x44000 + $STSID* 256 + $SLICEID*32 + $RXDWORDID`

* **Where**               : 

    * `$STSID (0-47) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$RXDWORDID (0-19) : RX Double WORD ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`mdl_tbyte3`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`mdl_tbyte2`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`mdl_tbyte1`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`mdl_tbyte0`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###Config Buff Message MDL TYPE

* **Description**           

config type message MDL


* **RTL Instant Name**    : `upen_rxmdl_cfgtype`

* **Address**             : `0x4A800 - 0x4A97F`

* **Formula**             : `0x4A800+ $STSID*8 + $SLICEID`

* **Where**               : 

    * `$STSID (0-47) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`cfg_fcs_rx`| config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`cfg_mdl_cr`| config C/R bit expected| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`cfg_mdl_std`| (0) is ANSI, (1) is AT&T| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`cfg_mdl_mask_test`| config enable mask to moitor test massage (1): enable, (0): disable| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`cfg_mdl_mask_path`| config enable mask to moitor path massage (1): enable, (0): disable| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfg_mdl_mask_idle`| config enable mask to moitor idle massage (1): enable, (0): disable| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG CONTROL DESTUFF 0

* **Description**           

config control DeStuff global


* **RTL Instant Name**    : `upen_destuff_ctrl0`

* **Address**             : `0x4AA00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`cfg_crmon`| (0) : disable, (1) : enable| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`reserve1`| reserve| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fcsmon`| (0) : disable monitor, (1) : enable| `R/W`| `0x1`| `0x1`|
|`[02:01]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`headermon_en`| (1) : enable monitor, (0) disable| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF CONFIG1

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg1`

* **Address**             : `0x4AA10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 0-31| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 2

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg2`

* **Address**             : `0x4AA11`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 32 -63| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 3

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg3`

* **Address**             : `0x4AA12`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 64 -95| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 4

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg4`

* **Address**             : `0x4AA13`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 96 -127| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 5

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg5`

* **Address**             : `0x4AA14`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 128 -159| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF CONFIG 6

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg6`

* **Address**             : `0x4AA15`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 160 -191| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF CONFIG7

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg7`

* **Address**             : `0x4AA16`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 192-223| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 8

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg8`

* **Address**             : `0x4AA17`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 224 -255| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 9

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg9`

* **Address**             : `0x4AA18`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 256 -287| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 10

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg10`

* **Address**             : `0x4AA19`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 288 -319| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 11

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg11`

* **Address**             : `0x4AA1A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 320 -351| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF CONFIG 12

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg12`

* **Address**             : `0x4AA1B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 352 -383| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE RX MDL IDLE

* **Description**           

counter byte read to clear IDLE message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_byteidle`

* **Address**             : `0x4D000 - 0x4D97F`

* **Formula**             : `0x4D000+ $STSID*8 + $SLICEID + $UPRO * 2048`

* **Where**               : 

    * `$STSID (0-47) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_byte_idle_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE RX MDL PATH

* **Description**           

counter byte read to clear PATH message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_bytepath`

* **Address**             : `0x4D200 - 0x4DB7F`

* **Formula**             : `0x4D200+ $STSID*8 + $SLICEID + $UPRO * 2048`

* **Where**               : 

    * `$STSID (0-47) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_byte_path_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE RX MDL TEST

* **Description**           

counter byte read to clear TEST message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_bytetest`

* **Address**             : `0x4D400 - 0x4DF7F`

* **Formula**             : `0x4D400+ $STSID*8 + $SLICEID + $UPRO * 2048`

* **Where**               : 

    * `$STSID (0-47) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_byte_test_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER GOOD MESSAGE RX MDL IDLE

* **Description**           

counter good read to clear IDLE message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_goodidle`

* **Address**             : `0x4E000 - 0x4F17F`

* **Formula**             : `0x4E000+ $STSID*8 + $SLICEID + $UPRO * 4096`

* **Where**               : 

    * `$STSID (0-47) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_good_idle_mdl`| value counter good| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER GOOD MESSAGE RX MDL PATH

* **Description**           

counter good read to clear PATH message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_goodpath`

* **Address**             : `0x4E200 - 0x4F37F`

* **Formula**             : `0x4E200+ $STSID*8 + $SLICEID + $UPRO * 4096`

* **Where**               : 

    * `$STSID (0-47) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_good_path_mdl`| value counter good| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER GOOD MESSAGE RX MDL TEST

* **Description**           

counter good read to clear TEST message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_goodtest`

* **Address**             : `0x4E400 - 0x4F57F`

* **Formula**             : `0x4E400+ $STSID*8 + $SLICEID + $UPRO * 4096`

* **Where**               : 

    * `$STSID (0-47) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_good_test_mdl`| value counter good| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER DROP MESSAGE RX MDL IDLE

* **Description**           

counter drop read to clear IDLE message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_dropidle`

* **Address**             : `0x4E600 - 0x4F77F`

* **Formula**             : `0x4E600+ $STSID*8 + $SLICEID + $UPRO * 4096`

* **Where**               : 

    * `$STSID (0-47) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_drop_idle_mdl`| value counter drop| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER DROP MESSAGE RX MDL PATH

* **Description**           

counter drop read to clear PATH message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_droppath`

* **Address**             : `0x4E800 - 0x4F97F`

* **Formula**             : `0x4E800+ $STSID*8 + $SLICEID + $UPRO * 4096`

* **Where**               : 

    * `$STSID (0-47) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_drop_path_mdl`| value counter drop| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER DROP MESSAGE RX MDL TEST R2C

* **Description**           

counter drop read to clear TEST message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_droptest`

* **Address**             : `0x4EA00 - 0x4FBFF`

* **Formula**             : `0x4EA00+ $STSID*8 + $SLICEID + $UPRO * 4096`

* **Where**               : 

    * `$STSID (0-47) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_drop_test_mdl`| value counter drop| `R/W`| `0x0`| `0x0 End: Begin:`|

###RX MDL INT STA

* **Description**           

config control DeStuff global


* **RTL Instant Name**    : `upen_int_ndlsta`

* **Address**             : `0x4AA06`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[01:01]`|`mdllb_intsta2`| interrupt sta MDL 2| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`mdllb_intsta1`| interrupt sta MDL 1| `R/W`| `0x0`| `0x0 End Begin:`|

###MDL per Channel Interrupt1 Enable Control

* **Description**           

This is the per Channel interrupt enable


* **RTL Instant Name**    : `mdl_cfgen_int1`

* **Address**             : `0x4A000-0x4A0FF`

* **Formula**             : `0x4A000 +  $STSID1 + $SLICEID * 32`

* **Where**               : 

    * `$STSID1 (0-31) : STS1 ID`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mdl_cfgen_int1`| Set 1 to enable change event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt1 per Channel Interrupt Status

* **Description**           

This is the per Channel interrupt status.


* **RTL Instant Name**    : `mdl_int1_sta`

* **Address**             : `0x4A200-0x4A2FF`

* **Formula**             : `0x4A200 +  $STSID1 + $SLICEID * 32`

* **Where**               : 

    * `$STSID1 (0-31) : STS1 ID`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mdlint1_sta`| Set 1 if there is a change event.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt per Channel Current Status

* **Description**           

This is the per Channel Current status.


* **RTL Instant Name**    : `mdl_int1_crrsta`

* **Address**             : `0x4A400-0x4A4FF`

* **Formula**             : `0x4A400 +  $STSID1 + $SLICEID * 32`

* **Where**               : 

    * `$STSID1 (0-31) : STS1 ID`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mdlint1_crrsta`| Current status of event.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt1 per Channel Interrupt OR Status

* **Description**           




* **RTL Instant Name**    : `mdl_int1sta`

* **Address**             : `0x4A600-0x4A607`

* **Formula**             : `0x4A600 +  $GID`

* **Where**               : 

    * `$GID (0-7) : group ID`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`mdlint1sta`| Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt1 OR Status

* **Description**           

The register consists of 2 bits. Each bit is used to store Interrupt OR status.


* **RTL Instant Name**    : `mdl_sta_int1`

* **Address**             : `0x4A6FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`mdlsta_int1`| Set to 1 if any interrupt status bit is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt1 Enable Control

* **Description**           

The register consists of 2 interrupt enable bits .


* **RTL Instant Name**    : `mdl_en_int1`

* **Address**             : `0x4A6FE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`mdlen_int1`| Set to 1 to enable to generate interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL per Channel Interrupt2 Enable Control

* **Description**           

This is the per Channel interrupt enable


* **RTL Instant Name**    : `mdl_cfgen_int2`

* **Address**             : `0x4A100-0x4A1FF`

* **Formula**             : `0x4A100 +  $STSID2 + $SLICEID * 32`

* **Where**               : 

    * `$STSID2 (0-15) : STS2 ID`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mdl_cfgen_int2`| Set 1 to enable change event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt2 per Channel Interrupt Status

* **Description**           

This is the per Channel interrupt status.


* **RTL Instant Name**    : `mdl_int2_sta`

* **Address**             : `0x4A300-0x4A3FF`

* **Formula**             : `0x4A300 +  $STSID2 + $SLICEID * 32`

* **Where**               : 

    * `$STSID2 (0-15) : STS2 ID`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mdlint2_sta`| Set 1 if there is a change event.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt per Channel Current Status

* **Description**           

This is the per Channel Current status.


* **RTL Instant Name**    : `mdl_int2_crrsta`

* **Address**             : `0x4A500-0x4A5FF`

* **Formula**             : `0x4A500 +  $STSID2 + $SLICEID * 32`

* **Where**               : 

    * `$STSID2 (0-15) : STS2 ID`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mdlint2_crrsta`| Current status of event.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt2 per Channel Interrupt OR Status

* **Description**           




* **RTL Instant Name**    : `mdl_int2sta`

* **Address**             : `0x4A700-0x4A707`

* **Formula**             : `0x4A700 +  $GID`

* **Where**               : 

    * `$GID (0-7) : group ID`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`mdlint2sta`| Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt2 OR Status

* **Description**           

The register consists of 2 bits. Each bit is used to store Interrupt OR status.


* **RTL Instant Name**    : `mdl_sta_int2`

* **Address**             : `0x4A7FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`mdlsta_int2`| Set to 1 if any interrupt status bit is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt2 Enable Control

* **Description**           

The register consists of 2 interrupt enable bits .


* **RTL Instant Name**    : `mdl_en_int2`

* **Address**             : `0x4A7FE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`mdlen_int2`| Set to 1 to enable to generate interrupt.| `RW`| `0x0`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_GLB
####Register Table

|Name|Address|
|-----|-----|
|`Device Product ID`|`0x00_0000`|
|`Device Year Month Day Version ID`|`0x00_0002`|
|`Device Internal ID`|`0x00_0003`|
|`GLB Sub-Core Active`|`0x00_0001`|
|`GLB Debug PW Control`|`0x00_0010`|
|`GLB Debug PW Control`|`0x00_0024`|
|`GLB Debug PW PLA HO Speed`|`0x00_0030`|
|`GLB Debug PW PDA HO Speed`|`0x00_0031`|
|`GLB Debug PW PWE Speed`|`0x00_0034`|
|`GLB Debug PW CLA Speed`|`0x00_0035`|
|`Chip Temperature Sticky`|`0x00_0020`|
|`GLB Debug Mac Loopback Control`|`0x00_0013`|
|`GLB Eth1G Clock Squelching Control`|`0x00_0011`|
|`GLB Eth10G Clock Squelching Control`|`0x00_0012`|


###Device Product ID

* **Description**           

This register indicates Product ID.


* **RTL Instant Name**    : `ProductID`

* **Address**             : `0x00_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`productid`| ProductId| `RO`| `0x60210051`| `0x60210051 End: Begin:`|

###Device Year Month Day Version ID

* **Description**           

This register indicates Year Month Day and main version ID.


* **RTL Instant Name**    : `YYMMDD_VerID`

* **Address**             : `0x00_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`year`| Year| `RO`| `0x16`| `0x16`|
|`[23:16]`|`month`| Month| `RO`| `0x01`| `0x01`|
|`[15:8]`|`day`| Day| `RO`| `0x30`| `0x30`|
|`[7:0]`|`version`| Version| `RO`| `0x40`| `0x40 End: Begin:`|

###Device Internal ID

* **Description**           

This register indicates internal ID.


* **RTL Instant Name**    : `InternalID`

* **Address**             : `0x00_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`internalid`| InternalID| `RO`| `0x0`| `0x0 End: Begin:`|

###GLB Sub-Core Active

* **Description**           

This register indicates the active ports.


* **RTL Instant Name**    : `Active`

* **Address**             : `0x00_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`subcoreen`| Enable per sub-core<br>{1}: Enable <br>{0}: Disable| `RW`| `0x00000000`| `0x00000000 End: Begin:`|

###GLB Debug PW Control

* **Description**           

This register is used to configure debug PW parameters


* **RTL Instant Name**    : `DebugPwControl`

* **Address**             : `0x00_0010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`oc192cepena`| Set 1 to enable debug OC192c CEP PW| `RW`| `0x0`| `0x0`|
|`[30:20]`|`lotdmpwid`| Lo TDM PW ID or HO Master STS ID corresponding to global PW ID| `RW`| `0x0`| `0x0`|
|`[19:7]`|`globalpwid12_0`| Global PW ID bit12_0 need to debug| `RW`| `0x0`| `0x0`|
|`[6:4]`|`oc48id`| OC48 ID, OC192 ID| `RW`| `0x0`| `0x0`|
|`[3]`|`looc24slice`| Low Order OC24 slice if the PW belong to low order path| `RW`| `0x0`| `0x0`|
|`[3]`|`globalpwid13`| Global PW ID bit13 need to debug| `RW`| `0x0`| `0x0`|
|`[2:0]`|`hopwtype`| High Order PW type if the PW belong to high order path 0: STS1/VC3 CEP 1: STS3/VC4 CEP 2: STS12/VC4-4C CEP 3: STS48/VC4-16C CEP 4: VC11/VC12 CEP 5: TU3 CEP 6: STS24/VC4-8C CEP 7: SAToP/CESoP| `RW`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW Control

* **Description**           

This register is used to debug PW modules


* **RTL Instant Name**    : `DebugPwSticky`

* **Address**             : `0x00_0024`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `29`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28]`|`claprbserr`| CLA output PRBS error| `WC`| `0x0`| `0x0`|
|`[27:20]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[19]`|`pweprbserr`| PWE input PRBS error| `RC`| `0x0`| `0x0`|
|`[18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17]`|`pdaprbserr`| High Order PDA output PRBS error| `RC`| `0x0`| `0x0`|
|`[16]`|`plaprbserr`| High Order PLA input PRBS error| `RC`| `0x0`| `0x0`|
|`[15:13]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[12]`|`claprbssyn`| CLA output PRBS sync| `WC`| `0x0`| `0x0`|
|`[11:4]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[3]`|`pweprbssyn`| PWE input PRBS sync| `RC`| `0x0`| `0x0`|
|`[2]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[1]`|`pdaprbssyn`| High Order PDA output PRBS sync| `RC`| `0x0`| `0x0`|
|`[0]`|`plaprbssyn`| High Order PLA input PRBS sync| `RC`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW PLA HO Speed

* **Description**           

This register is used to show PLA HO PW speed


* **RTL Instant Name**    : `DebugPwPlaSpeed`

* **Address**             : `0x00_0030`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`plapwspeed`| PLA input PW speed| `RO`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW PDA HO Speed

* **Description**           

This register is used to show PDA HO PW speed


* **RTL Instant Name**    : `DebugPwPdaHoSpeed`

* **Address**             : `0x00_0031`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pdapwspeed`| PDA output PW speed| `RO`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW PWE Speed

* **Description**           

This register is used to show PWE PW speed


* **RTL Instant Name**    : `DebugPwPweSpeed`

* **Address**             : `0x00_0034`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pwepwspeed`| PWE input PW speed| `RO`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW CLA Speed

* **Description**           

This register is used to show PWE PW speed


* **RTL Instant Name**    : `DebugPwClaLoSpeed`

* **Address**             : `0x00_0035`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`clapwspeed`| CLA output PW speed| `RO`| `0x0`| `0x0 End: Begin:`|

###Chip Temperature Sticky

* **Description**           

This register is used to sticky temperature


* **RTL Instant Name**    : `ChipTempSticky`

* **Address**             : `0x00_0020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `3`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2]`|`chipslr2tempstk`| Chip SLR2 temperature| `WC`| `0x0`| `0x0`|
|`[1]`|`chipslr1tempstk`| Chip SLR1 temperature| `WC`| `0x0`| `0x0`|
|`[0]`|`chipslr0tempstk`| Chip SLR0 temperature| `WC`| `0x0`| `0x0 End: Begin:`|

###GLB Debug Mac Loopback Control

* **Description**           

This register is used to configure debug loopback MAC parameters


* **RTL Instant Name**    : `DebugMacLoopControl`

* **Address**             : `0x00_0013`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`macmroloopout`| Set 1 for Mac 10G/1G/100Fx Loop Out| `RW`| `0x0`| `0x0`|
|`[0]`|`mac40gloopin`| Set 1 for Mac40gLoopIn| `RW`| `0x0`| `0x0 End: Begin:`|

###GLB Eth1G Clock Squelching Control

* **Description**           

This register is used to configure debug PW parameters


* **RTL Instant Name**    : `GlbEth1GClkSquelControl`

* **Address**             : `0x00_0011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`ethgelinkdownschelen`| Each Bit represent for each ETH port, value each bit is as below 1: Disable 8Khz refout when GE link down occur 0: Enable 8Khz refout when GE link down occur| `RW`| `0x0`| `0x0`|
|`[15:0]`|`ethgeexcesserrrateschelen`| Each Bit represent for each ETH port, value each bit is as below 1: Disable 8Khz refout when GE Excessive Error Rate occur 0: Enable 8Khz refout when GE Excessive Error Rate occur| `RW`| `0x0`| `0x0 End: Begin:`|

###GLB Eth10G Clock Squelching Control

* **Description**           

This register is used to configure debug PW parameters


* **RTL Instant Name**    : `GlbEth10GClkSquelControl`

* **Address**             : `0x00_0012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:6]`|`tengelocalfaultschelen`| Each Bit represent for each TenGe port0 and port8, value each bit is as below 1: Disable 8Khz refout when TenGe Local Fault occur 0: Enable 8Khz refout when TenGe Local Fault occur| `RW`| `0x0`| `0x0`|
|`[5:4]`|`tengeremotefaultschelen`| Each Bit represent for each TenGe port0 and port8, value each bit is as below 1: Disable 8Khz refout when TenGe Remote Fault occur 0: Enable 8Khz refout when TenGe Remote Fault occur| `RW`| `0x0`| `0x0`|
|`[3:2]`|`tengelossdatasyncschelen`| Each Bit represent for each TenGe port0 and port8, value each bit is as below 1: Disable 8Khz refout when TenGe Loss of Data Sync occur 0: Enable 8Khz refout when TenGe Loss of Data Sync occur| `RW`| `0x0`| `0x0`|
|`[1:0]`|`tengeexcesserrrateschelen`| Each Bit represent for each TenGe port0 and port8, value each bit is as below 1: Disable 8Khz refout when TenGe Excessive Error Rate occur 0: Enable 8Khz refout when TenGe Excessive Error Rate occur| `RW`| `0x0`| `0x0 End:`|

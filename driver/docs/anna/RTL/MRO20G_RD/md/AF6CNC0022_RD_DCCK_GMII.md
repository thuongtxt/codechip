## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_DCCK_GMII
####Register Table

|Name|Address|
|-----|-----|
|`DCCK GMII Control`|`0x10`|
|`DCCK GMII Diagnostic Enable Control`|`0x11`|
|`DCCK GMII Datapath and Diagnostic sticky`|`0x12`|
|`DCCK GMII Tx Packet Counter`|`0x00`|
|`DCCK GMII Tx Packet Counter`|`0x01`|
|`DCCK GMII Tx Packet FCS Error Counter`|`0x02`|
|`DCCK GMII Rx Packet Counter`|`0x03`|
|`DCCK GMII Rx Packet Counter`|`0x04`|
|`DCCK GMII Rx Packet FCS Error Counter`|`0x05`|
|`Dcck Gmii dump direction control`|`0x0000C00`|
|`Dcck Gmii dump data status`|`0x000800 - 0x000BFF The address format for these registers is 0x000800 + entry`|


###DCCK GMII Control

* **Description**           

This register configures parameters for DCCK GMII


* **RTL Instant Name**    : `dcckgmiicfg`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `11`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[10]`|`dcckgmiiloopout`| DCCK GMII Remote Loop Out| `RW`| `0x0`| `0x0 1: Remote loop out from rx GMII to tx GMII 0: Normal operation`|
|`[9]`|`dcckgmiiloopin`| DCCK GMII Local Loop In| `RW`| `0x0`| `0x0 1: Local loop in from tx GMII to rx GMII 0: Normal operation`|
|`[8:5]`|`dcckgmiiprenum`| DCCK GMII Preamble Number of Byte| `RW`| `0x7`| `0x7`|
|`[4:0]`|`dcckgmiiipgnum`| DCCK GMII Interpacket GAP Number of Byte| `RW`| `0xF`| `0xF End: Begin:`|

###DCCK GMII Diagnostic Enable Control

* **Description**           

This register configures parameters for DCCK GMII


* **RTL Instant Name**    : `dcckgmiidiagcfg`

* **Address**             : `0x11`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`dcckgmiidiagmaxlen`| DCCK GMII Diagnostic Max length count from zero| `RW`| `0x5E7`| `0x5E7`|
|`[17:4]`|`dcckgmiidiagminlen`| DCCK GMII Diagnostic Min length count from zero| `RW`| `0x3F`| `0x3F`|
|`[3]`|`dcckgmiiforcefcserr`| DCCK GMII Force FCS error| `RW`| `0x0`| `0x0 1: Force Tx FCS error 0: Normal operation`|
|`[2]`|`dcckgmiidiagforcedaterr`| DCCK GMII Diagnostic Force Data error| `RW`| `0x0`| `0x0 1: Diagnostic Force Tx data error 0: Normal operation`|
|`[1]`|`dcckgmiidiaggenpken`| DCCK GMII Diagnostic Generate packet enable| `RW`| `0x0`| `0x0 1: Enable generate diagnostic packets 0: Disable generate diagnostic packets`|
|`[0]`|`dcckgmiidiagen`| DCCK GMII Diagnostic enable| `RW`| `0x0`| `0x0 1: Enable diagnostic, select diagnostic packet to send and receive over GMII 0: Normal operation End: Begin:`|

###DCCK GMII Datapath and Diagnostic sticky

* **Description**           

This register show alarms of DCCK GMII


* **RTL Instant Name**    : `dcckgmiistk`

* **Address**             : `0x12`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `12`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11]`|`dcckgmiitxfifoconverr`| DCCK GMII Tx FIFO Convert Error| `RWC`| `0x0`| `0x0`|
|`[10]`|`dcckgmiitxfcserr`| DCCK GMII Tx FCS Error| `RWC`| `0x0`| `0x0`|
|`[9]`|`dcckgmiitxlostsop`| DCCK GMII Tx Lost SOP| `RWC`| `0x0`| `0x0`|
|`[8]`|`dcckgmiirxpreerr`| DCCK GMII Rx Preamble Error| `RWC`| `0x0`| `0x0`|
|`[7]`|`dcckgmiirxsfderr`| DCCK GMII Rx SFD Error| `RWC`| `0x0`| `0x0`|
|`[6]`|`dcckgmiirxdverr`| DCCK GMII Rx DV Error| `RWC`| `0x0`| `0x0`|
|`[5]`|`dcckgmiirxfcserr`| DCCK GMII Rx FCS Error| `RWC`| `0x0`| `0x0`|
|`[4]`|`dcckgmiirxerr`| DCCK GMII Rx Error| `RWC`| `0x0`| `0x0`|
|`[3]`|`dcckgmiirxfifoconverr`| DCCK GMII Rx FIFO Convert Error| `RWC`| `0x0`| `0x0`|
|`[2]`|`dcckgmiirxminlenerr`| DCCK GMII Rx Min Len Error| `RWC`| `0x0`| `0x0`|
|`[1]`|`dcckgmiirxmaxlenerr`| DCCK GMII Rx Max Len Error| `RWC`| `0x0`| `0x0`|
|`[0]`|`dcckgmiidiagrxdaterr`| DCCK GMII Diagnostic Rx Data Error| `RWC`| `0x0`| `0x0 End: Begin:`|

###DCCK GMII Tx Packet Counter

* **Description**           

This register show GMII Tx Packet Counter


* **RTL Instant Name**    : `dcckgmiitxpktcnt`

* **Address**             : `0x00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`dcckgmiitxpktcnt`| DCCK GMII Tx Packet Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###DCCK GMII Tx Packet Counter

* **Description**           

This register show GMII Tx Byte Counter


* **RTL Instant Name**    : `dcckgmiitxbytecnt`

* **Address**             : `0x01`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcckgmiitxbytecnt`| DCCK GMII Tx Byte Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###DCCK GMII Tx Packet FCS Error Counter

* **Description**           

This register show GMII Tx Packet FCS Error Counter


* **RTL Instant Name**    : `dcckgmiitxfcserrcnt`

* **Address**             : `0x02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`dcckgmiitxpktfcserrcnt`| DCCK GMII Tx Packet FCS Error Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###DCCK GMII Rx Packet Counter

* **Description**           

This register show GMII Rx Packet Counter


* **RTL Instant Name**    : `dcckgmiirxpktcnt`

* **Address**             : `0x03`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`dcckgmiirxpktcnt`| DCCK GMII Rx Packet Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###DCCK GMII Rx Packet Counter

* **Description**           

This register show GMII Rx Byte Counter


* **RTL Instant Name**    : `dcckgmiirxbytecnt`

* **Address**             : `0x04`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcckgmiirxbytecnt`| DCCK GMII Rx Byte Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###DCCK GMII Rx Packet FCS Error Counter

* **Description**           

This register show GMII Rx Packet FCS Error Counter


* **RTL Instant Name**    : `dcckgmiirxfcserrcnt`

* **Address**             : `0x05`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`dcckgmiirxpktfcserrcnt`| DCCK GMII Rx Packet FCS Error Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###Dcck Gmii dump direction control

* **Description**           

This register configures direction to dump packet


* **RTL Instant Name**    : `ramdumtypecfg`

* **Address**             : `0x0000C00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`directiondump`| Tx or Rx MAC direction 1: Dump Tx DCCK GMII 0: Dump Rx DCCK GMII| `RW`| `0x0`| `0x0 End: Begin:`|

###Dcck Gmii dump data status

* **Description**           

This register shows data value to be dump, first entry always contain SOP


* **RTL Instant Name**    : `ramdumpdata`

* **Address**             : `0x000800 - 0x000BFF The address format for these registers is 0x000800 + entry`

* **Formula**             : `0x000800 +  entry`

* **Where**               : 

    * `$entry(0-1023): Data entry`

* **Width**               : `19`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[18]`|`dumpsop`| Start of packet indication| `RO`| `0x0`| `0x0`|
|`[17]`|`dumpeop`| End of packet indication| `RO`| `0x0`| `0x0`|
|`[16]`|`dumpnob`| Number of byte indication 0 means 1 byte| `RW`| `0x0`| `0x0`|
|`[15:0]`|`dumpdata`| Data value Bit15_8 is MSB| `RW`| `0x0`| `0x0 End:`|

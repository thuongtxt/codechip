## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_GLBPMC_ver2
####Register Table

|Name|Address|
|-----|-----|
|`Pseudowire Transmit Counter`|`0x0_0000-0x0_7DFF(RW)`|
|`Pseudowire Transmit Counter`|`0x0_8000-0x0_FDFF(RW)`|
|`Pseudowire TX Byte Counter`|`0x8_0000-0x8_29FF (RW)`|
|`Pseudowire Receiver Counter Info0`|`0x1_0000-0x1_29FF (RW)`|
|`Pseudowire Receiver Counter Info0`|`0x1_4000-0x1_69FF (RW)`|
|`Pseudowire Receiver Counter Info1`|`0x2_0000-0x2_7DFF(RW)`|
|`Pseudowire Receiver Counter Info1`|`0x2_8000-0x2_FDFF(RW)`|
|`Pseudowire Receiver Counter Info2`|`0x3_0000-0x3_7DFF(RW)`|
|`Pseudowire Receiver Counter Info2`|`0x3_8000-0x3_FDFF(RW)`|
|`PMR Error Counter`|`0x9_1800-0x9_182F(RW)`|
|`PMR Error Counter`|`0x9_1840-0x9_186F(RW)`|
|`POH Path STS Counter`|`0x9_2000-0x9_21FF (RW)`|
|`POH Path STS Counter`|`0x9_2200-0x9_23FF (RW)`|
|`POH Path VT Counter`|`0x7_0000 - 0x7_3FFF(RW)`|
|`POH Path VT Counter`|`0x7_4000 - 0x7_7FFF(RW)`|
|`POH vt pointer Counter 0`|`0x5_0000-0x5_3FFF(RW)`|
|`POH vt pointer Counter 0`|`0x5_4000-0x5_7FFF(RW)`|
|`POH vt pointer Counter 1`|`0x6_0000-0x6_3FFF(RW)`|
|`POH vt pointer Counter 1`|`0x6_4000-0x6_7FFF(RW)`|
|`POH sts pointer Counter`|`0x9_0800-0x9_09FF(RW)`|
|`POH sts pointer Counter`|`0x9_0A00-0x9_0BFF(RW)`|
|`POH sts pointer Counter`|`0x9_1000-0x9_11FF(RW)`|
|`POH sts pointer Counter`|`0x9_1200-0x9_13FF(RW)`|
|`PDH ds1 cntval Counter`|`0x4_0000-0x4_3FFF(RW)`|
|`PDH ds1 cntval Counter`|`0x4_4000-0x4_7FFF(RW)`|
|`PDH ds3 cntval Counter`|`0x9_0000-0x9_01FF(RW)`|
|`PDH ds3 cntval Counter`|`0x9_0200-0x9_03FF(RW)`|


###Pseudowire Transmit Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_info1_rw`

* **Address**             : `0x0_0000-0x0_7DFF(RW)`

* **Formula**             : `0x0_0000 + 10752*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-10751)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`txpwcnt`| in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : txpkt in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : unused| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_info1_rw`

* **Address**             : `0x0_8000-0x0_FDFF(RW)`

* **Formula**             : `0x0_8000 + 10752*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-10751)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`txpwcnt`| in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : txpkt in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : unused| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire TX Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_info0`

* **Address**             : `0x8_0000-0x8_29FF (RW)`

* **Formula**             : `0x8_0000 + $pwid`

* **Where**               : 

    * `$pwid(0-10751)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:00]`|`pwcntbyte`| txpwcntbyte| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info0_rw`

* **Address**             : `0x1_0000-0x1_29FF (RW)`

* **Formula**             : `0x1_0000 + $pwid`

* **Where**               : 

    * `$pwid(0-10751)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of rxpwcnt_info0_cnt1 side: + rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info0_rw`

* **Address**             : `0x1_4000-0x1_69FF (RW)`

* **Formula**             : `0x1_4000 + $pwid`

* **Where**               : 

    * `$pwid(0-10751)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of rxpwcnt_info0_cnt1 side: + rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info1_rw`

* **Address**             : `0x2_0000-0x2_7DFF(RW)`

* **Formula**             : `0x2_0000 + 10752*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-10751)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxstray in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxpbit + offset = 1 : rxlbit + offset = 2 : rxmalform| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info1_rw`

* **Address**             : `0x2_8000-0x2_FDFF(RW)`

* **Formula**             : `0x2_8000 + 10752*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-10751)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxstray in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxpbit + offset = 1 : rxlbit + offset = 2 : rxmalform| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info2

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info2_rw`

* **Address**             : `0x3_0000-0x3_7DFF(RW)`

* **Formula**             : `0x3_0000 + 10752*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-10751)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info1`| in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxunderrun + offset = 1 : rxlops + offset = 2 : rxearly in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxoverrun + offset = 1 : rxlost + offset = 2 : rxlate| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info2

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info2_rw`

* **Address**             : `0x3_8000-0x3_FDFF(RW)`

* **Formula**             : `0x3_8000 + 10752*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-10751)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info1`| in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxunderrun + offset = 1 : rxlops + offset = 2 : rxearly in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxoverrun + offset = 1 : rxlost + offset = 2 : rxlate| `RW`| `0x0`| `0x0 End: Begin:`|

###PMR Error Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_pmr_cnt0_rw`

* **Address**             : `0x9_1800-0x9_182F(RW)`

* **Formula**             : `0x9_1800 + 16*$offset + $lineid`

* **Where**               : 

    * `$offset(0-2)`

    * `$lineid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`pmr_err_cnt`| in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 : rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err| `RW`| `0x0`| `0x0 End: Begin:`|

###PMR Error Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_pmr_cnt0_rw`

* **Address**             : `0x9_1840-0x9_186F(RW)`

* **Formula**             : `0x9_1840 + 16*$offset + $lineid`

* **Where**               : 

    * `$offset(0-2)`

    * `$lineid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`pmr_err_cnt`| in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 : rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Path STS Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_sts_pohpm_rw`

* **Address**             : `0x9_2000-0x9_21FF (RW)`

* **Formula**             : `0x9_2000 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`pohpath_vt_cnt`| in case of pohpath_cnt0 side: + sts_rei in case of pohpath_cnt1 side: + sts_bip(B3)| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Path STS Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_sts_pohpm_rw`

* **Address**             : `0x9_2200-0x9_23FF (RW)`

* **Formula**             : `0x9_2200 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`pohpath_vt_cnt`| in case of pohpath_cnt0 side: + sts_rei in case of pohpath_cnt1 side: + sts_bip(B3)| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Path VT Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_vt_pohpm_rw`

* **Address**             : `0x7_0000 - 0x7_3FFF(RW)`

* **Formula**             : `0x7_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`pohpath_vt_cnt`| in case of pohpath_cnt0 side: + vt_rei in case of pohpath_cnt1 side: + vt_bip| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Path VT Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_vt_pohpm_rw`

* **Address**             : `0x7_4000 - 0x7_7FFF(RW)`

* **Formula**             : `0x7_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`pohpath_vt_cnt`| in case of pohpath_cnt0 side: + vt_rei in case of pohpath_cnt1 side: + vt_bip| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter 0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt0_rw`

* **Address**             : `0x5_0000-0x5_3FFF(RW)`

* **Formula**             : `0x5_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + pohtx_vt_dec in case of poh_vt_cnt1 side: + pohtx_vt_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter 0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt0_rw`

* **Address**             : `0x5_4000-0x5_7FFF(RW)`

* **Formula**             : `0x5_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + pohtx_vt_dec in case of poh_vt_cnt1 side: + pohtx_vt_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter 1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt1_rw`

* **Address**             : `0x6_0000-0x6_3FFF(RW)`

* **Formula**             : `0x6_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + pohrx_vt_dec in case of poh_vt_cnt1 side: + pohrx_vt_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter 1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt1_rw`

* **Address**             : `0x6_4000-0x6_7FFF(RW)`

* **Formula**             : `0x6_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + pohrx_vt_dec in case of poh_vt_cnt1 side: + pohrx_vt_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt0_rw`

* **Address**             : `0x9_0800-0x9_09FF(RW)`

* **Formula**             : `0x9_0800 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`poh_sts_cnt`| in case of poh_vt_cnt0 side: + pohtx_sts_dec in case of poh_vt_cnt1 side: + pohtx_sts_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt0_rw`

* **Address**             : `0x9_0A00-0x9_0BFF(RW)`

* **Formula**             : `0x9_0A00 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`poh_sts_cnt`| in case of poh_vt_cnt0 side: + pohtx_sts_dec in case of poh_vt_cnt1 side: + pohtx_sts_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt1_rw`

* **Address**             : `0x9_1000-0x9_11FF(RW)`

* **Formula**             : `0x9_1000 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`poh_sts_cnt`| in case of poh_vt_cnt0 side: + pohrx_sts_dec in case of poh_vt_cnt1 side: + pohrx_sts_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt1_rw`

* **Address**             : `0x9_1200-0x9_13FF(RW)`

* **Formula**             : `0x9_1200 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`poh_sts_cnt`| in case of poh_vt_cnt0 side: + pohrx_sts_dec in case of poh_vt_cnt1 side: + pohrx_sts_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 cntval Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de1cntval30_rw`

* **Address**             : `0x4_0000-0x4_3FFF(RW)`

* **Formula**             : `0x4_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`ds1_cntval_bus1_cnt`| (rei/fe/crc)| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 cntval Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de1cntval30_rw`

* **Address**             : `0x4_4000-0x4_7FFF(RW)`

* **Formula**             : `0x4_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`ds1_cntval_bus1_cnt`| (rei/fe/crc)| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds3 cntval Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de3cnt0_rw`

* **Address**             : `0x9_0000-0x9_01FF(RW)`

* **Formula**             : `0x9_0000 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`ds3_cntval_cnt`| (cb/pb/rei/fe)| `RW`| `0x0`| `0x0 End:`|

###PDH ds3 cntval Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de3cnt0_rw`

* **Address**             : `0x9_0200-0x9_03FF(RW)`

* **Formula**             : `0x9_0200 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:00]`|`ds3_cntval_cnt`| (cb/pb/rei/fe)| `RW`| `0x0`| `0x0 End:`|

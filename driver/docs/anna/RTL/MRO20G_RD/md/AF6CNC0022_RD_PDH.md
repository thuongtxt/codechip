## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_PDH
####Register Table

|Name|Address|
|-----|-----|
|`PDH Global Registers`|`0x00000000 - 0x00000000`|
|`STS/VT Demap Control`|`0x00024000 - 0x000247FF`|
|`STS/VT Demap HW Status`|`0x00024800 - 0x00024FFF`|
|`PDH Rx Per STS/VC Payload Control`|`0x00030080 - 0x000300BF`|
|`RxM23E23 Control`|`0x00040000 - 0x0004003F`|
|`RxM23E23 HW Status`|`0x00040080-0x000400BF`|
|`RxDS3E3 OH Pro Control`|`0x040840-0x04087F`|
|`Rx DS3E3 SSM Invert`|`0x04040B`|
|`Rx DS3E3 SSM status`|`0x040EC0`|
|`RxDS3E3 OH Pro Threshold`|`0x040801`|
|`RxM23E23 Framer REI Threshold Control`|`0x00040803`|
|`RxM23E23 Framer FBE Threshold Control`|`0x00040804`|
|`RxM23E23 Framer Parity Threshold Control`|`0x00040805`|
|`RxM23E23 Framer Cbit Parity Threshold Control`|`0x00040806`|
|`RxM23E23 Framer REI Counter`|`0x00040880 - 0x000408BF`|
|`RxM23E23 Framer FBE Counter`|`0x00040900 - 0x0004093F`|
|`RxM23E23 Framer Parity Counter`|`0x00040980 - 0x000409BF`|
|`RxM23E23 Framer Cbit Parity Counter`|`0x00040A00 - 0x00040A3F`|
|`RxDS3E3 FEAC Buffer`|`0x040B40-0x040B7F`|
|`RxM23E23 Framer per Channel Interrupt Enable Control`|`0x00040D00-0x00040D3F`|
|`RxM23E23 Framer per Channel Interrupt Change Status`|`0x00040D40-0x00040D7F`|
|`RxM23E23 Framer per Channel Current Status`|`0x00040D80-0x00040DBF`|
|`RxM23E23 Framer per Channel Interrupt OR Status`|`0x00040DC0-0x00040DC1`|
|`RxM23E23 Framer per Group Interrupt OR Status`|`0x00040DFF`|
|`RxM23E23 Framer per Group Interrupt Enable Control`|`0x00040DFE`|
|`RxDS3E3 OH Pro DLK Swap Bit Enable`|`0x04080B`|
|`RxM12E12 Control`|`0x00041000 - 0x000411FF`|
|`PDH RxM12E12 Per STS/VC Payload Control`|`0x00041600 - 0x0004163F`|
|`RxM12E12 HW Status`|`0x00041400 - 0x000415FF`|
|`RxM12E12 Framer per Channel Interrupt Enable Control`|`0x00041700-0x0004173F`|
|`RxM12E12 Framer per Channel Interrupt Change Status`|`0x00041740-0x0004177F`|
|`RxM12E12 Framer per Channel Current Status`|`0x00041780-0x000417BF`|
|`RxM12E12 Framer per Channel Interrupt OR Status`|`0x000417C0-0x000417C1`|
|`RxM12E12 Framer per Group Interrupt OR Status`|`0x000417FF`|
|`RxM12E12 Framer per Group Interrupt Enable Control`|`0x000417FE`|
|`DS1/E1/J1 Rx Framer FBE Threshold Control`|`0x00050003 - 0x00050003`|
|`DS1/E1/J1 Rx Framer Control`|`0x00054000 - 0x000547FF`|
|`DS1/E1/J1 Rx Framer HW Status`|`0x00054800-0x00054FFF`|
|`DS1/E1/J1 Rx Framer FBE Counter`|`0x0005C000-0x0005CFFF`|
|`DS1/E1/J1 Rx Framer CRC Error Counter`|`0x0005D000 - 0x0005DFFF`|
|`DS1/E1/J1 Rx Framer REI Counter`|`0x0005E000 - 0x0005EFFF`|
|`DS1/E1/J1 Rx Framer per Channel Interrupt Enable Control`|`0x00056000-0x000567FF`|
|`DS1/E1/J1 Rx Framer per Channel Interrupt Status`|`0x00056800-0x00056FFF`|
|`DS1/E1/J1 Rx Framer per Channel Current Status`|`0x00057000-0x000577FF`|
|`DS1/E1/J1 Rx Framer per Channel Interrupt OR Status`|`0x00057800`|
|`DS1/E1/J1 Rx Framer per STS/VC Interrupt OR Status`|`0x00057BFF`|
|`DS1/E1/J1 Rx Framer per STS/VC Interrupt Enable Control`|`0x00057BFE`|
|`DS1/E1/J1 Rx Framer per Group Interrupt OR Status`|`0x00050002`|
|`STS/VT Map Control`|`0x00076000 - 0x000767FF`|
|`STS/VT Map HW Status`|`0x00074000-0x000747FF`|
|`DS1/E1/J1 Tx Framer Control`|`0x00094000 - 0x000947FF`|
|`DS1/E1/J1 Tx Framer Status`|`0x00095000 - 0x000957FF`|
|`DS1/E1/J1 Tx Framer Signaling Insertion Control`|`0x00094800 - 0x00094FFF`|
|`DS1/E1/J1 Payload Error Insert Enable Configuration`|`0x00090002`|
|`DS1/E1/J1 Payload Error Insert threshold Configuration`|`0x00090003`|
|`DS1/E1/J1 Tx Framer DLK Insertion BOM Control`|`0x097000-0x0977FF`|
|`TxM23E23 Control`|`0x00080000 - 0x0008003F`|
|`TxM23E23 HW Status`|`0x00080080-0x000800BF`|
|`TxM12E12 Control`|`0x00081000 - 0x000811FF`|
|`TxM12E12 HW Status`|`0x00081200 - 0x000813FF`|
|`Config Limit error`|`0xE_0002`|
|`Config limit match`|`0xE_0003`|
|`Config thresh fdl parttern`|`0xE_0004`|
|`Config thersh error`|`0xE_0005`|
|`Current Inband Code`|`0xE_2100 - 0xE_27FF`|
|`Config User programmble Pattern User Codes`|`0xE_08F8`|
|`Currenr Message FDL Detected`|`0xE_1800 - 0xE_1FFF`|
|`Config Theshold Pattern Report`|`0xE_0006`|
|`RAM Parity Force Control`|`0x10`|
|`RAM Parity Disable Control`|`0x11`|
|`RAM parity Error Sticky`|`0x12`|


###PDH Global Registers

* **Description**           

This is the global configuration register for the PDH


* **RTL Instant Name**    : `glb`

* **Address**             : `0x00000000 - 0x00000000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9]`|`pdhe13idconv`| Set 1 to enable ID conversion of the E1 in an E3 as following ID in an E3 (bit 4:0) ID at Data Map (bit 4:0) 5'd0                  5'd0 5'd1                  5'd1 5'd2                  5'd2 5'd3                  5'd4 5'd4                  5'd5 5'd5                  5'd6 5'd6                  5'd8 5'd7                  5'd9 5'd8                  5'd10 5'd9                  5'd12 5'd10                 5'd13 5'd11                 5'd14 5'd12                 5'd16 5'd13                 5'd17 5'd14                 5'd18 5'd15                 5'd20| `RW`| `0x0`| `0x0`|
|`[8]`|`pdhfullsonetsdh`| Set 1 to enable full SONET/SDH mode (no traffic on PDH interface)| `RW`| `0x0`| `0x0`|
|`[7]`|`pdhfulllineloop`| Set 1 to enable full Line Loop Back at Data map side| `RW`| `0x0`| `0x0`|
|`[6]`|`pdhfullpayloop`| Set 1 to enable full Payload Loop back at Data map side| `RW`| `0x0`| `0x0`|
|`[5]`|`pdhfullpdhbus`| Set 1 to enable Full traffic on PDH Bus| `RW`| `0x0`| `0x0`|
|`[4]`|`pdhparallelpdhmd`| Set 1 to enable Parallel PDH Bus mode| `RW`| `0x0`| `0x0`|
|`[3]`|`pdhsaturation`| Set 1 to enable Saturation mode of all PDH counters. Set 0 for Roll-Over mode| `RW`| `0x0`| `0x0`|
|`[2]`|`pdhreservedbit`| Transmit Reserved Bit| `RW`| `0x0`| `0x0`|
|`[1]`|`pdhstufftbit`| Transmit Stuffing Bit| `RW`| `0x0`| `0x0`|
|`[0]`|`pdhnoidconv`| Set 1 to enable no ID convert at PDH Block| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Demap Control

* **Description**           

The STS/VT Demap Control is use to configure for per channel STS/VT demap operation.


* **RTL Instant Name**    : `stsvt_demap_ctrl`

* **Address**             : `0x00024000 - 0x000247FF`

* **Formula**             : `0x00024000 +  32*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$stsid(0-47):`

    * `$vtgid(0-6):`

    * `$vtid(0-3) HDL_PATH     : ctrlbuf.array.ram[$stsid*32 + $vtgid*4 + $vtid]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9]`|`pdhoversdhmd`| Set 1 to PDH LIU over OCN| `RW`| `0x0`| `0x0`|
|`[8]`|`stsvtdemapfrcais`| This bit is set to 1 to force the AIS to downstream data. 1: force AIS. 0: not force AIS.| `RW`| `0x0`| `0x0`|
|`[7]`|`stsvtdemapautoais`| This bit is set to 1 to automatically generate AIS to downstream data whenever the high-order STS/VT is in AIS. 1: auto AIS generation. 0: not auto AIS generation| `RW`| `0x0`| `0x0`|
|`[6]`|`stsvtdemapcepmode`| STS/VC CEP fractional mode| `RW`| `0x0`| `0x0`|
|`[5:4]`|`stsvtdemapfrmtype`| STS/VT Frmtype configure| `RW`| `0x0`| `0x0`|
|`[3:0]`|`stsvtdemapsigtype`| STS/VT Sigtype configure Sigtype[3:0] Frmtype[1:0] CepMode Operation Mode 0 x x VT/TU to DS1 Demap 1 x x VT/TU to E1 Demap 2 x 0 STS1/VC3 to DS3 Demap 2 x 1 TUG3/TU3 to DS3 Demap 3 x 0 STS1/VC3 to E3 Demap 3 x 1 TUG3/TU3 to E3 Demap 4 0 x Packet over VT1.5/TU11 4 1 x Reserved 4 2 x Reserved 4 3 x VT1.5/TU11 Basic CEP 5 0 x Packet over VT2/TU12 5 1 x Reserved 5 2 x Reserved 5 3 x VT2/TU12 Basic CEP 6 x x Reserved 7 x x Reserved 8 0 x Packet over STS1/VC3 8 1 0 STS1/VC3 Fractional CEP carrying VT2/TU12 8 1 1 STS1/VC3 Fractional CEP carrying VT15/TU11 8 2 0 STS1/VC3 Fractional CEP carrying E3 8 2 1 STS1/VC3 Fractional CEP carrying DS3 8 3 x STS1/VC3 Basic CEP 9 0 x Packet over STS3c/VC4 9 1 0 STS3c/VC4 Fractional CEP carrying VT2/TU12 9 1 1 STS3c/VC4 Fractional CEP carrying VT15/TU11 9 2 0 STS3c/VC4 Fractional CEP carrying E3 9 2 1 STS3c/VC4 Fractional CEP carrying DS3 9 3 x STS3c/VC4 Basic CEP 10 3 x STS12c/VC4_4c Basic CEP 11 x x Reserved 12 x x Packet over TU3 13 x x Reserved 14 x x Reserved 15 x x Disable STS/VC/VT/TU/DS1/E1/DS3/E3| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Demap HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `stsvt_demap_hw_stat`

* **Address**             : `0x00024800 - 0x00024FFF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `27`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[26:0]`|`stsvtdemapstatus`| for HW debug only| `RO`| `0x0`| `0x0 End: Begin:`|

###PDH Rx Per STS/VC Payload Control

* **Description**           

The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1.


* **RTL Instant Name**    : `rx_stsvc_payload_ctrl`

* **Address**             : `0x00030080 - 0x000300BF`

* **Formula**             : `0x00030080 +  stsid`

* **Where**               : 

    * `$stsid(0-47): HDL_PATH     : pdhrxmux.vtcnt.stsctrlbuf.array.ram[$stsid]`

* **Width**               : `14`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13:12]`|`pdhvtrxmaptug26type`| Define types of VT/TUs in TUG-2-6. 0: TU11/VT1.5 type 1: TU12/VT2 type 2: reserved 3: VC/STS/DS3/E3| `RW`| `0x0`| `0x0`|
|`[11:10]`|`pdhvtrxmaptug25type`| Define types of VT/TUs in TUG-2-5| `RW`| `0x0`| `0x0`|
|`[9:8]`|`pdhvtrxmaptug24type`| Define types of VT/TUs in TUG-2-4| `RW`| `0x0`| `0x0`|
|`[7:6]`|`pdhvtrxmaptug23type`| Define types of VT/TUs in TUG-2-3| `RW`| `0x0`| `0x0`|
|`[5:4]`|`pdhvtrxmaptug22type`| Define types of VT/TUs in TUG-2-2| `RW`| `0x0`| `0x0`|
|`[3:2]`|`pdhvtrxmaptug21type`| Define types of VT/TUs in TUG-2-1| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pdhvtrxmaptug20type`| Define types of VT/TUs in TUG-2-0| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Control

* **Description**           

The RxM23E23 Control is use to configure for per channel Rx DS3/E3 operation.


* **RTL Instant Name**    : `rxm23e23_ctrl`

* **Address**             : `0x00040000 - 0x0004003F`

* **Formula**             : `0x00040000 +  de3id`

* **Where**               : 

    * `$de3id(0-47): HDL_PATH     : rxm13e13.rxm23e23.ctrlbufarb.membuf.array.ram[$de3id]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[22]`|`de3aisall1sfrwdis`| Forward DE3 AIS all-1s to generate L-bit to PSN Disable, set 1 to disable| `RW`| `0x0`| `0x0`|
|`[21]`|`ds3loffrwdis`| Forward DS3 LOF to generate L-bit to PSN in case of MonOnly Disable, set 1 to disable| `RW`| `0x0`| `0x0`|
|`[20]`|`ds3aissigfrwenb`| Forward DS3 AIS signal to generate L-bit to PSN in case of MonOnly Enable, set 1 to enable| `RW`| `0x0`| `0x0`|
|`[19]`|`rxds3forceaislbit`| Force DS3 AIS, L-bit to PSN, data all one to PSN for Payload/Line Remote Loopback| `RW`| `0x0`| `0x0`|
|`[18]`|`rxde3mononly`| Rx Framer Monitor only| `RW`| `0x0`| `0x0`|
|`[17]`|`rxds3e3chenb`| | `RW`| `0x0`| `0x0`|
|`[16:14]`|`rxds3e3losthres`| | `RW`| `0x0`| `0x0`|
|`[13:11]`|`rxds3e3aisthres`| | `RW`| `0x0`| `0x0`|
|`[10:8]`|`rxds3e3lofthres`| | `RW`| `0x0`| `0x0`|
|`[7]`|`rxds3e3payallone`| This bit is set to 1 to force the DS3/E3 framer payload AIS downstream| `RW`| `0x0`| `0x0`|
|`[6]`|`rxds3e3lineallone`| This bit is set to 1 to force the DS3/E3 framer line AIS downstream| `RW`| `0x0`| `0x0`|
|`[5]`|`rxds3e3lineloop`| | `RW`| `0x0`| `0x0`|
|`[4]`|`rxds3e3frclof`| This bit is set to 1 to force the Rx DS3/E3 framer into LOF status. 1: force LOF. 0: not force LOF.| `RW`| `0x0`| `0x0`|
|`[3:0]`|`rxds3e3md`| Rx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3 C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111: E3 G.751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `rxm23e23_hw_stat`

* **Address**             : `0x00040080-0x000400BF`

* **Formula**             : `0x00040080 +  de3id`

* **Where**               : 

    * `$de3id(0-47):`

* **Width**               : `32`
* **Register Type**       : `Stat`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`rxds3_crraic`| Current AIC Bit value| `RW`| `0x0`| `0x0`|
|`[5]`|`rxds3_crraisdownstr`| AIS Down Stream due to HI_Level AIS of DS3E3| `RW`| `0x0`| `0x0`|
|`[4]`|`rxds3_crrlosallzero`| LOS All zero of DS3E3| `RW`| `0x0`| `0x0`|
|`[3]`|`rxds3_crraisallone`| AIS All one of DS3E3| `RW`| `0x0`| `0x0`|
|`[2]`|`rxds3_crraissignal`| AIS signal of DS3| `RW`| `0x0`| `0x0`|
|`[1:0]`|`rxds3e3state`| 0:LOF, 1:Searching, 2: Verify, 3:InFrame| `RW`| `0x0`| `0x0 End: Begin:`|

###RxDS3E3 OH Pro Control

* **Description**           




* **RTL Instant Name**    : `rx_de3_oh_ctrl`

* **Address**             : `0x040840-0x04087F`

* **Formula**             : `0x040840 +  de3id`

* **Where**               : 

    * `$de3id(0-47): HDL_PATH     : rxm13e13.rxm23e23.ctrlbufarb.membuf.array.ram[$de3id]`

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[5]`|`dlen`| DLEn 1: Data Link send to HDLC 0: Data Link not send to HDLC| `RW`| `0x0`| `0x0`|
|`[4:3]`|`ds3e3dlsl`| 00: DL 01: UDL 10: FEAC 11: NA| `RW`| `0x0`| `0x0`|
|`[2]`|`ds3feacen`| Enable detection FEAC state change| `RW`| `0x0`| `0x0`|
|`[1]`|`fbefbitcnten`| Enable FBE counts F-Bit| `RW`| `0x0`| `0x0`|
|`[0]`|`fbembitcnten`| Enable FBE counts M-Bit| `RW`| `0x0`| `0x0 End: Begin:`|

###Rx DS3E3 SSM Invert

* **Description**           




* **RTL Instant Name**    : `upen_invert_ssm`

* **Address**             : `0x04040B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:0]`|`ssmmessinv`| SSM message Invert - 1: invert 11111111_11_0xxxxxx0(bit0-5) - 0: non_invert 0xxxxxx0_11_11111111(bit5-0)| `RO`| `0x0`| `0x0 End: Begin:`|

###Rx DS3E3 SSM status

* **Description**           




* **RTL Instant Name**    : `upen_sta_ssm`

* **Address**             : `0x040EC0`

* **Formula**             : `0x040EC0 +  de3id`

* **Where**               : 

    * `$de3id(0-47):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:12]`|`ssmmesssta`| SSM Status - 0: Seaching Status - 1: Checking Status - 2: Inframe Status| `RO`| `0x0`| `0x0`|
|`[11:8]`|`ssmmesscnt`| SSM message matching counter| `RO`| `0x0`| `0x0`|
|`[7:0]`|`ssmmessval`| SSM message - [3:0]: SSM E3 G832 Message - [5:0]: SSM DS3 C-bit Message| `RO`| `0x0`| `0x0 End: Begin:`|

###RxDS3E3 OH Pro Threshold

* **Description**           




* **RTL Instant Name**    : `rxde3_oh_thrsh_cfg`

* **Address**             : `0x040801`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:21]`|`idlethresds3`| | `RW`| `0x0`| `0x0`|
|`[20:18]`|`aisthresds3`| | `RW`| `0x0`| `0x0`|
|`[17:15]`|`timingmakere3g832`| | `RW`| `0x0`| `0x0`|
|`[14:12]`|`newpaytypee3g832`| | `RW`| `0x0`| `0x0`|
|`[11:9]`|`aicthres`| | `RW`| `0x0`| `0x0`|
|`[8:6]`|`rdie3g832thres`| | `RW`| `0x0`| `0x0`|
|`[5:3]`|`rdie3g751thres`| | `RW`| `0x0`| `0x0`|
|`[2:0]`|`rdids3thres`| | `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer REI Threshold Control

* **Description**           

This is the REI threshold configuration register for the PDH DS3/E3 Rx framer


* **RTL Instant Name**    : `rxm23e23_rei_thrsh_cfg`

* **Address**             : `0x00040803`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23reithres`| Threshold to generate interrupt if the REI counter exceed this threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer FBE Threshold Control

* **Description**           

This is the REI threshold configuration register for the PDH DS3/E3 Rx framer


* **RTL Instant Name**    : `rxm23e23_fbe_thrsh_cfg`

* **Address**             : `0x00040804`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23fbethres`| Threshold to generate interrupt if the FBE counter exceed this threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer Parity Threshold Control

* **Description**           

This is the REI threshold configuration register for the PDH DS3/E3 Rx framer


* **RTL Instant Name**    : `rxm23e23_parity_thrsh_cfg`

* **Address**             : `0x00040805`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23parthres`| Threshold to generate interrupt if the PAR counter exceed this threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer Cbit Parity Threshold Control

* **Description**           

This is the REI threshold configuration register for the PDH DS3/E3 Rx framer


* **RTL Instant Name**    : `rxm23e23_cbit_parity_thrsh_cfg`

* **Address**             : `0x00040806`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23cparthres`| Threshold to generate interrupt if the Cbit Parity counter exceed this threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer REI Counter

* **Description**           

This is the per channel REI counter for DS3/E3 receive framer


* **RTL Instant Name**    : `rxm23e23_rei_cnt`

* **Address**             : `0x00040880 - 0x000408BF`

* **Formula**             : `0x00040880 +  64*Rd2Clr + de3id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only`

    * `$de3id(0-47):)`

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23reicnt`| DS3/E3 REI error accumulator| `RO`| `RC`| `0x0`|

###RxM23E23 Framer FBE Counter

* **Description**           

This is the per channel FBE counter for DS3/E3 receive framer


* **RTL Instant Name**    : `rxm23e23_fbe_cnt`

* **Address**             : `0x00040900 - 0x0004093F`

* **Formula**             : `0x00040900 +  64*Rd2Clr + de3id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.`

    * `$de3id(0-23):`

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`count`| frame FBE counter| `RO`| `RC`| `0x0`|

###RxM23E23 Framer Parity Counter

* **Description**           

This is the per channel Parity counter for DS3/E3 receive framer


* **RTL Instant Name**    : `rxm23e23_parity_cnt`

* **Address**             : `0x00040980 - 0x000409BF`

* **Formula**             : `0x00040980 +  64*Rd2Clr + de3id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.`

    * `$de3id(0-23):`

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23parcnt`| DS3/E3 Parity error accumulator| `RO`| `RC`| `0x0`|

###RxM23E23 Framer Cbit Parity Counter

* **Description**           

This is the per channel Cbit Parity counter for DS3/E3 receive framer


* **RTL Instant Name**    : `rxm23e23_cbit_parity_cnt`

* **Address**             : `0x00040A00 - 0x00040A3F`

* **Formula**             : `0x00040A00 +  64*Rd2Clr + de3id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.`

    * `$de3id(0-23):`

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23cparcnt`| DS3/E3 Cbit Parity error accumulator| `RO`| `RC`| `0x0`|

###RxDS3E3 FEAC Buffer

* **Description**           




* **RTL Instant Name**    : `rx_de3_feac_buffer`

* **Address**             : `0x040B40-0x040B7F`

* **Formula**             : `0x040B40 +  de3id`

* **Where**               : 

    * `$de3id(0-47):`

* **Width**               : `9`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:6]`|`feac_state`| 00: idle codeword 01: active codeword 10: de-active codeword 11: alarm/status codeword| `RW`| `0x0`| `0x0`|
|`[5:0]`|`feac_codeword`| 6 x-bits cw in format 111111110xxxxxx0| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer.


* **RTL Instant Name**    : `rxm23e23_per_chn_intr_cfg`

* **Address**             : `0x00040D00-0x00040D3F`

* **Formula**             : `0x00040D00 +  de3id`

* **Where**               : 

    * `$de3id(0-47):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`de3bertca_en`| Set 1 to enable change DE3 BER TCA event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[14]`|`de3bersd_en`| Set 1 to enable change DE3 BER SD event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[13]`|`de3bersf_en`| Set 1 to enable change DE3 BER SF event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[12]`|`de3lcvcnt_en`| Set 1 to enable change DE3 LCV counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[11]`|`ds3cp_e3tr_en`| Set 1 to enable change DS3 CP counter or E3G831 Trace event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[10]`|`de3parcnt_en`| Set 1 to enable change DE3 Parity counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[9]`|`de3fbecnt_en`| Set 1 to enable change DE3 FBE counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[8]`|`de3reicnt_en`| Set 1 to enable change DE3 REI counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[7]`|`ds3feac_e3ssm_en`| Set 1 to enable change DS3 FEAC or E3G832 SSM event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[6]`|`ds3aic_e3tm_en`| Set 1 to enable change DS3 AIC or E3G832 TM event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[5]`|`ds3idlesig_e3pt_en`| Set 1 to enable change DS3 IDLE signal or E3G832 PayloadType event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[4]`|`de3aisalloneintren`| Set 1 to enable change AIS All One event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[3]`|`de3losallzerointren`| Set 1 to enable change LOS All Zero event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[2]`|`ds3aissigintren`| Set 1 to enable change AIS Signal event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[1]`|`de3rdiintren`| Set 1 to enable change RDI event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[0]`|`de3lofintren`| Set 1 to enable change LOF event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer per Channel Interrupt Change Status

* **Description**           

This is the per Channel interrupt tus of DS3/E3 Rx Framer.


* **RTL Instant Name**    : `rxm23e23_per_chn_intr_stat`

* **Address**             : `0x00040D40-0x00040D7F`

* **Formula**             : `0x00040D40 +  de3id`

* **Where**               : 

    * `$de3id(0-47):`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`de3bertca_intr`| Set 1 if there is change DE3 BER TCA event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[14]`|`de3bersd_intr`| Set 1 if there is change DE3 BER SD event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[13]`|`de3bersf_intr`| Set 1 if there is change DE3 BER SF event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[12]`|`de3lcvcnt_intr`| Set 1 if there is change DE3 LCV counter event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[11]`|`ds3cp_e3tr_intr`| Set 1 if there is change DS3 CP counter or E3G831 Trace event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[10]`|`de3parcnt_intr`| Set 1 if there is change DE3 Parity counter event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[9]`|`de3fbecnt_intr`| Set 1 if there is change DE3 FBE counter event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[8]`|`de3reicnt_intr`| Set 1 to enable change DE3 REI counter event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[7]`|`ds3feac_e3ssm_intr`| Set 1 if there is a change DS3 FEAC or E3G832 SSM event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[6]`|`ds3aic_e3tm_intr`| Set 1 if there is a change DS3 AIC or E3G832 TM event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[5]`|`ds3idlesig_e3pt_intr`| Set 1 if there is a change DS3 IDLE signal or E3G832 PayloadType event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[4]`|`de3aisallone_intr`| Set 1 if there is a change AIS All One event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[3]`|`de3losallzero_intr`| Set 1 if there is a change LOS All Zero event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[2]`|`ds3aissig_intr`| Set 1 if there is a change AIS Signal event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[1]`|`de3rdi_intr`| Set 1 if there is a  change RDI event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[0]`|`de3lof_intr`| Set 1 if there is a  change LOF event to generate an interrupt.| `W1C`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer per Channel Current Status

* **Description**           

This is the per Channel Current tus of DS3/E3 Rx Framer.


* **RTL Instant Name**    : `rxm23e23_per_chn_curr_stat`

* **Address**             : `0x00040D80-0x00040DBF`

* **Formula**             : `0x00040D80 +  de3id`

* **Where**               : 

    * `$de3id(0-47):`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`de3bertca_currsta`| Current status DE3 BER TCA event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[14]`|`de3bersd_currsta`| Current status DE3 BER SD event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[13]`|`de3bersf_currsta`| Current status DE3 BER SF event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[12]`|`de3lcvcnt_currsta`| Current status DE3 LCV counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[11]`|`ds3cp_e3tr_currsta`| Current status DS3 CP counter or E3G831 Trace event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[10]`|`de3parcnt_currsta`| Current status DE3 Parity counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[9]`|`de3fbecnt_currsta`| Current status DE3 FBE counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[8]`|`de3reicnt_currsta`| Current status DE3 REI counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[7]`|`ds3feac_e3ssm_currsta`| Current status of DS3 FEAC or E3G832 SSM event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[6]`|`ds3aic_e3tm_currsta`| Current status of DS3 AIC or E3G832 TM event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[5]`|`ds3idlesig_e3pt_currsta`| Current status of DS3 IDLE signal or E3G832 PayloadType event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[4]`|`de3aisallonecurrsta`| Current status of AIS All One event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[3]`|`de3losallzerocurrsta`| Current status of LOS All Zero event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[2]`|`ds3aissigcurrsta`| Current status of AIS Signal event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[1]`|`de3rdicurrsta`| Current status of RDI event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[0]`|`de3lofcurrsta`| Current status of LOF event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer per Channel Interrupt OR Status

* **Description**           

The register consists of 2 bits for 2 Group of the related STS/VC in the RxM23E23. Each bit is used to store Interrupt OR status of the related Group DS3/E3.


* **RTL Instant Name**    : `RxM23E23_Framer_per_chn_intr_or_stat`

* **Address**             : `0x00040DC0-0x00040DC1`

* **Formula**             : `0x00040DC0 +  GrpID`

* **Where**               : 

    * `$GrpID(0-1): grpid = 0 for de3id 0-31, grpid = 1 for de3id 32-47`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`de3grpintrorsta`| Set to 1 if any interrupt status bit of corresponding DS3/E3 is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer per Group Interrupt OR Status

* **Description**           

The register consists of 2 bits for 2 Group of the RxM23E23 Framer. Each bit is used to store Interrupt OR status of the related Group.


* **RTL Instant Name**    : `RxM23E23_Framer_per_grp_intr_or_stat`

* **Address**             : `0x00040DFF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`de3grpintrorsta`| Set to 1 if any interrupt status bit of corresponding Group is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer per Group Interrupt Enable Control

* **Description**           

The register consists of 2 interrupt enable bits for 2 Group in the Rx DS3/E3 Framer.


* **RTL Instant Name**    : `RxM23E23_Framer_per_Group_intr_en_ctrl`

* **Address**             : `0x00040DFE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`de3grpintren`| Set to 1 to enable the related Group to generate interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###RxDS3E3 OH Pro DLK Swap Bit Enable

* **Description**           




* **RTL Instant Name**    : `rxde3_oh_dlk_swap_bit_cfg`

* **Address**             : `0x04080B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `3`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2]`|`dlkswapbitds3en`| | `RW`| `0x0`| `0x0`|
|`[1]`|`dlkswapbite3g751en`| | `RW`| `0x0`| `0x0`|
|`[0]`|`dlkswapbite3g823en`| | `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Control

* **Description**           

The RxM12E12 Control is use to configure for per channel Rx DS2/E2 operation.


* **RTL Instant Name**    : `rxm12e12_ctrl`

* **Address**             : `0x00041000 - 0x000411FF`

* **Formula**             : `0x00041000 +  8*de3id + de2id`

* **Where**               : 

    * `$de3id(0-47)`

    * `$de2id(0-6): HDL_PATH     : rxm13e13.rxm12e12.ctrlbufarb.membuf.array.ram[$de3id*8 + $de2id]`

* **Width**               : `3`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2]`|`rxds2e2frclof`| This bit is set to 1 to force the Rx DS3/E3 framer into LOF tus. 1: force LOF. 0: not force LOF.| `RW`| `0x0`| `0x0`|
|`[1:0]`|`rxds2e2md`| Rx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2 G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH RxM12E12 Per STS/VC Payload Control

* **Description**           

The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1.


* **RTL Instant Name**    : `rxm12e12_stsvc_payload_ctrl`

* **Address**             : `0x00041600 - 0x0004163F`

* **Formula**             : `0x00041600 +  stsid`

* **Where**               : 

    * `$stsid(0-47):`

* **Width**               : `14`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13:12]`|`pdhvtrxmaptug26type`| Define types of VT/TUs in TUG-2-6. 0: TU11/VT1.5 type 1: TU12/VT2 type 2: reserved 3: reserved| `RW`| `0x0`| `0x0`|
|`[11:10]`|`pdhvtrxmaptug25type`| Define types of VT/TUs in TUG-2-5| `RW`| `0x0`| `0x0`|
|`[9:8]`|`pdhvtrxmaptug24type`| Define types of VT/TUs in TUG-2-4| `RW`| `0x0`| `0x0`|
|`[7:6]`|`pdhvtrxmaptug23type`| Define types of VT/TUs in TUG-2-3| `RW`| `0x0`| `0x0`|
|`[5:4]`|`pdhvtrxmaptug22type`| Define types of VT/TUs in TUG-2-2| `RW`| `0x0`| `0x0`|
|`[3:2]`|`pdhvtrxmaptug21type`| Define types of VT/TUs in TUG-2-1| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pdhvtrxmaptug20type`| Define types of VT/TUs in TUG-2-0| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `rxm12e12_hw_stat`

* **Address**             : `0x00041400 - 0x000415FF`

* **Formula**             : `0x00041400 +  8*de3id + de2id`

* **Where**               : 

    * `$de3id(0-47)`

    * `$de2id(0-6):`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`rxm12e12status`| for HW debug only| `RO`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Framer per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable of DS2/E2 Rx Framer.


* **RTL Instant Name**    : `RxM12E12_per_chn_intr_cfg`

* **Address**             : `0x00041700-0x0004173F`

* **Formula**             : `0x00041700 +  de3id`

* **Where**               : 

    * `$de3id(0-47):`

* **Width**               : `7`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6:0]`|`de2lofintren`| Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Framer per Channel Interrupt Change Status

* **Description**           

This is the per Channel interrupt status of DS2/E2 Rx Framer.


* **RTL Instant Name**    : `RxM12E12_per_chn_intr_stat`

* **Address**             : `0x00041740-0x0004177F`

* **Formula**             : `0x00041740 +  de3id`

* **Where**               : 

    * `$de3id(0-47):`

* **Width**               : `7`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6:0]`|`de2lofintr`| Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Framer per Channel Current Status

* **Description**           

This is the per Channel Current status of DS2/E2 Rx Framer.


* **RTL Instant Name**    : `RxM12E12_per_chn_curr_stat`

* **Address**             : `0x00041780-0x000417BF`

* **Formula**             : `0x00041780 +  de3id`

* **Where**               : 

    * `$de3id(0-47):`

* **Width**               : `7`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6:0]`|`de2lofcurrsta`| Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Framer per Channel Interrupt OR Status

* **Description**           

The register consists of 32 bits for 2 Group of the related STS/VC in the RxM12E12. Each bit is used to store Interrupt OR tus of the related Group DS2/E2.


* **RTL Instant Name**    : `RxM12E12_Framer_per_chn_intr_or_stat`

* **Address**             : `0x000417C0-0x000417C1`

* **Formula**             : `0x000417C0 +  GrpID`

* **Where**               : 

    * `$GrpID(0-1): grpid = 0 for de3id 0-31, grpid = 1 for de3id 32-47`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`de2grpintrorsta`| Set to 1 if any interrupt status bit of corresponding Group DS3/E3 is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Framer per Group Interrupt OR Status

* **Description**           

The register consists of 2 bits for 2 Group of the RxM12E12 Framer. Each bit is used to store Interrupt OR status of the related Group.


* **RTL Instant Name**    : `RxM12E12_Framer_per_grp_intr_or_stat`

* **Address**             : `0x000417FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`de2grpintrorsta`| Set to 1 if any interrupt status bit of corresponding Group is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Framer per Group Interrupt Enable Control

* **Description**           

The register consists of 2 interrupt enable bits for 2 Group in the Rx DS2/E2 Framer.


* **RTL Instant Name**    : `RxM12E12_Framer_per_Group_intr_en_ctrl`

* **Address**             : `0x000417FE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`de2grpintren`| Set to 1 to enable the related Group to generate interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer FBE Threshold Control

* **Description**           

This is the FBE threshold configuration register for the PDH DS1/E1/J1 Rx framer


* **RTL Instant Name**    : `dej1_fbe_thrsh_cfg`

* **Address**             : `0x00050003 - 0x00050003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:0]`|`rxde1fbethres`| Threshold to generate interrupt if the FBE counter exceed this threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer Control

* **Description**           

DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer.


* **RTL Instant Name**    : `dej1_rx_framer_ctrl`

* **Address**             : `0x00054000 - 0x000547FF`

* **Formula**             : `0x00054000 +  32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$de3id(0-47)`

    * `$de2id(0-6):`

    * `$de1id(0-3): HDL_PATH     : ds1e1rxfrm.ctrlbuf.array.ram[$de3id*32 + $de2id*4 + $de1id]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`rxde1loffrwdis`| Forward DE1 LOF to generate L-bit to PSN in case of MonOnly, set 1 to disable| `RW`| `0x0`| `0x0`|
|`[30]`|`rxde1frcaislbit`| Force AIS alarm here,L-bit, data all one to PSN when Line/Payload Remote Loopback| `RW`| `0x0`| `0x0`|
|`[29]`|`rxde1frcaislinedwn`| Set 1 to force AIS Line DownStream| `RW`| `0x0`| `0x0`|
|`[28]`|`rxde1frcaisplddwn`| Set 1 to force AIS payload DownStream| `RW`| `0x0`| `0x0`|
|`[27]`|`rxde1frclos`| Set 1 to force LOS| `RW`| `0x0`| `0x0`|
|`[26]`|`rxde1lomfdstren`| Set 1 to generate AIS forwarding dowstream in case E1CRC LOMF defect is present. This bit set 1 also to backwarding RDI upstream if E1 path is terminated in case E1CRC LOMF defect is present| `RW`| `0x0`| `0x0`|
|`[25:23]`|`rxde1loscntthres`| Threshold of LOS monitoring block. This function always monitors the number of bit 1 of the incoming signal in each of two consecutive frame periods (256*2 bits in E1, 193*3 bits in DS1). If it is less than the desire number, LOS will be set then.| `RW`| `0x0`| `0x0`|
|`[22]`|`rxde1mononly`| Set 1 to enable the Monitor Only mode at the RXDS1/E1 framer| `RW`| `0x0`| `0x0`|
|`[21]`|`rxde1locallineloop`| Set 1 to enable Local Line loop back| `RW`| `0x0`| `0x0`|
|`[20]`|`rxde1localpayloop`| Set 1 to enable Local Payload loop back. Sharing for VT Loopback in case of CEP path| `RW`| `0x0`| `0x0`|
|`[19:17]`|`rxde1e1sacfg`| Incase E1 Sa used for FDL configuration 0x0: No Sa bit used 0x3: Sa4 is used for SSM 0x4: Sa5 is used for SSM 0x5: Sa6 is used for SSM 0x6: Sa7 is used for SSM 0x7: Sa8 is used for SSM Incase DS1 bit[19:18] dont care bit [17]   set " 1" to enable Rx DLK| `RW`| `0x0`| `0x0`|
|`[16:13]`|`rxde1lofthres`| Threshold for FAS error to declare LOF in DS1/E1/J1 mode| `RW`| `0x0`| `0x0`|
|`[12:10]`|`rxde1aiscntthres`| Threshold of AIS monitoring block. This function always monitors the number of bit 0 of the incoming signal in each of two consecutive frame periods (256*2 bits in E1, 193*3 bits in DS1). If it is less than the desire number, AIS will be set then.| `RW`| `0x0`| `0x0`|
|`[9:7]`|`rxde1raicntthres`| Threshold of RAI monitoring block| `RW`| `0x0`| `0x0`|
|`[6]`|`rxde1aismasken`| Forward DE1 AIS to generate L-bit to PSN, set 1 to enable| `RW`| `0x0`| `0x0`|
|`[5]`|`rxde1en`| Set 1 to enable the DS1/E1/J1 framer.| `RW`| `0x0`| `0x0`|
|`[4]`|`rxde1frclof`| Set 1 to force Re-frame| `RW`| `0x0`| `0x0`|
|`[3:0]`|`rxds1md`| Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF (D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1 Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer HW Status

* **Description**           

These registers are used for Hardware tus only


* **RTL Instant Name**    : `dej1_rx_framer_hw_stat`

* **Address**             : `0x00054800-0x00054FFF`

* **Formula**             : `0x00054800 +  32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$de3id(0-47)`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `32`
* **Register Type**       : `Stat`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[10:7]`|`rxds1e1_crre1ssm`| Current E1 SSM value| `RO`| `0x0`| `0x0`|
|`[6]`|`rxds1e1_crraisdownstr`| AIS Down Stream due to HI_Level AIS of DS1E1| `RO`| `0x0`| `0x0`|
|`[5]`|`rxds1e1_crrlosallzero`| LOS All zero of DS1E1| `RO`| `0x0`| `0x0`|
|`[4]`|`rxds1e1_crraisallone`| AIS All one of DS1E1| `RO`| `0x0`| `0x0`|
|`[3]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[2:0]`|`rxde1sta`| RxDE1Sta 000: Search 001: Shift 010: Wait 011: Verify 100: Fs search 101: Inframe| `RO`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer FBE Counter

* **Description**           

This is the per channel REI counter for DS1/E1/J1 receive framer


* **RTL Instant Name**    : `dej1_rx_framer_fbe_cnt`

* **Address**             : `0x0005C000-0x0005CFFF`

* **Formula**             : `0x0005C000 +  2048*Rd2Clr + 32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only`

    * `$de3id(0-47):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `8`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`de1fbecnt`| DS1/E1/J1 F-bit error accumulator| `RO`| `RC`| `0x0`|

###DS1/E1/J1 Rx Framer CRC Error Counter

* **Description**           

This is the per channel CRC error counter for DS1/E1/J1 receive framer


* **RTL Instant Name**    : `dej1_rx_framer_crcerr_cnt`

* **Address**             : `0x0005D000 - 0x0005DFFF`

* **Formula**             : `0x0005D000 +  2048*Rd2Clr + 32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only`

    * `$de3id(0-47):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `8`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`de1crcerrcnt`| DS1/E1/J1 CRC Error Accumulator| `RO`| `RC`| `0x0`|

###DS1/E1/J1 Rx Framer REI Counter

* **Description**           

This is the per channel REI counter for DS1/E1/J1 receive framer


* **RTL Instant Name**    : `dej1_rx_framer_rei_cnt`

* **Address**             : `0x0005E000 - 0x0005EFFF`

* **Formula**             : `0x0005E000 +  2048*Rd2Clr + 32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only`

    * `$de3id(0-47):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `8`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`de1reicnt`| DS1/E1/J1 REI error accumulator| `RO`| `RC`| `0x0`|

###DS1/E1/J1 Rx Framer per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer.


* **RTL Instant Name**    : `dej1_rx_framer_per_chn_intr_en_ctrl`

* **Address**             : `0x00056000-0x000567FF`

* **Formula**             : `0x00056000 +  StsID*32 + Tug2ID*4 + VtnID`

* **Where**               : 

    * `$StsID(0-47): STS-1/VC-3 ID`

    * `$Tug2ID(0-6): TUG-2/VTG ID`

    * `$VtnID(0-3): VT/TU number ID in the TUG-2/VTG`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13]`|`de1bersdintren`| Set 1 to enable the BER-SD interrupt.| `RW`| `0x0`| `0x0`|
|`[12]`|`de1bersfintren`| Set 1 to enable the BER-SF interrupt.| `RW`| `0x0`| `0x0`|
|`[11]`|`de1oblcintren`| Set 1 to enable the new Outband LoopCode detected to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[10]`|`de1iblcintren`| Set 1 to enable the new Inband LoopCode detected to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[9]`|`de1sigraiintren`| Set 1 to enable Signalling RAI event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[8]`|`de1siglofintren`| Set 1 to enable Signalling LOF event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[7]`|`de1fbeintren`| Set 1 to enable change FBE te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[6]`|`de1crcintren`| Set 1 to enable change CRC te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[5]`|`de1reiintren`| Set 1 to enable change REI te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[4]`|`de1lomfintren`| Set 1 to enable change LOMF te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[3]`|`de1losintren`| Set 1 to enable change LOS te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[2]`|`de1aisintren`| Set 1 to enable change AIS te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[1]`|`de1raiintren`| Set 1 to enable change RAI te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[0]`|`de1lofintren`| Set 1 to enable change LOF te event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer per Channel Interrupt Status

* **Description**           

This is the per Channel interrupt tus of DS1/E1/J1 Rx Framer.


* **RTL Instant Name**    : `dej1_rx_framer_per_chn_intr_stat`

* **Address**             : `0x00056800-0x00056FFF`

* **Formula**             : `0x00056800 +  StsID*32 + Tug2ID*4 + VtnID`

* **Where**               : 

    * `$StsID(0-47): STS-1/VC-3 ID`

    * `$Tug2ID(0-6): TUG-2/VTG ID`

    * `$VtnID(0-3): VT/TU number ID in the TUG-2/VTG`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13]`|`de1bersdintr`| Set 1 if there is a change of BER-SD.| `W1C`| `0x0`| `0x0`|
|`[12]`|`de1bersfintr`| Set 1 if there is a change of BER-SF.| `W1C`| `0x0`| `0x0`|
|`[11]`|`de1oblcintr`| Set 1 if there is a change the new Outband LoopCode detected to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[10]`|`de1iblcintr`| Set 1 if there is a change the new Inband LoopCode detected to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[9]`|`de1sigraiintr`| Set 1 if there is a change Signalling RAI event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[8]`|`de1siglofintr`| Set 1 if there is a change Signalling LOF event to generate an interrupt.| `W1C`| `0x0`| `0x0`|
|`[7]`|`de1fbeintr`| Set 1 if there is a change in FBE exceed threshold te event.| `W1C`| `0x0`| `0x0`|
|`[6]`|`de1crcintr`| Set 1 if there is a change in CRC exceed threshold te event.| `W1C`| `0x0`| `0x0`|
|`[5]`|`de1reiintr`| Set 1 if there is a change in REI exceed threshold te event.| `W1C`| `0x0`| `0x0`|
|`[4]`|`de1lomfintr`| Set 1 if there is a change in LOMF te event.| `W1C`| `0x0`| `0x0`|
|`[3]`|`de1losintr`| Set 1 if there is a change in LOS te event.| `W1C`| `0x0`| `0x0`|
|`[2]`|`de1aisintr`| Set 1 if there is a change in AIS te event.| `W1C`| `0x0`| `0x0`|
|`[1]`|`de1raiintr`| Set 1 if there is a change in RAI te event.| `W1C`| `0x0`| `0x0`|
|`[0]`|`de1lofintr`| Set 1 if there is a change in LOF te event.| `W1C`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer per Channel Current Status

* **Description**           

This is the per Channel Current tus of DS1/E1/J1 Rx Framer.


* **RTL Instant Name**    : `dej1_rx_framer_per_chn_curr_stat`

* **Address**             : `0x00057000-0x000577FF`

* **Formula**             : `0x00057000 +  StsID*32 + Tug2ID*4 + VtnID`

* **Where**               : 

    * `$StsID(0-47): STS-1/VC-3 ID`

    * `$Tug2ID(0-6): TUG-2/VTG ID`

    * `$VtnID(0-3): VT/TU number ID in the TUG-2/VTG`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13]`|`de1bersdintr`| Set 1 if there is a change of BER-SD.| `RW`| `0x0`| `0x0`|
|`[12]`|`de1bersfintr`| Set 1 if there is a change of BER-SF.| `RW`| `0x0`| `0x0`|
|`[11]`|`de1oblcintrsta`| Current status the new Outband LoopCode detected to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[10]`|`de1iblcintrsta`| Current status the new Inband LoopCode detected to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[9]`|`de1sigraiintrsta`| Current status Signalling RAI event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[8]`|`de1siglofintrsta`| Current status Signalling LOF event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[7]`|`de1fbecurrsta`| Current tus of FBE exceed threshold event.| `RW`| `0x0`| `0x0`|
|`[6]`|`de1crccurrsta`| Current tus of CRC exceed threshold event.| `RW`| `0x0`| `0x0`|
|`[5]`|`de1reicurrsta`| Current tus of REI exceed threshold event.| `RW`| `0x0`| `0x0`|
|`[4]`|`de1lomfcurrsta`| Current tus of LOMF event.| `RW`| `0x0`| `0x0`|
|`[3]`|`de1loscurrsta`| Current tus of LOS event.| `RW`| `0x0`| `0x0`|
|`[2]`|`de1aiscurrsta`| Current tus of AIS event.| `RW`| `0x0`| `0x0`|
|`[1]`|`de1raicurrsta`| Current tus of RAI event.| `RW`| `0x0`| `0x0`|
|`[0]`|`de1lofcurrsta`| Current tus of LOF event.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer per Channel Interrupt OR Status

* **Description**           

The register consists of 28 bits for 28 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1.


* **RTL Instant Name**    : `dej1_rx_framer_per_chn_intr_or_stat`

* **Address**             : `0x00057800`

* **Formula**             : `0x00057800 +  GrpID*1024 + STSID`

* **Where**               : 

    * `$GrpID(0-1): Group 0 for STS 0-31, 1 for STS 32-47`

    * `$STSID(0-31)`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`rxde1vtintrorsta`| Set to 1 if any interrupt tus bit of corresponding DS1/E1/J1 is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer per STS/VC Interrupt OR Status

* **Description**           

The register consists of 12 bits for 12 STS/VCs of the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related STS/VC.


* **RTL Instant Name**    : `dej1_rx_framer_per_stsvc_intr_or_stat`

* **Address**             : `0x00057BFF`

* **Formula**             : `0x00057BFF +  GrpID*1024`

* **Where**               : 

    * `$GrpID(0-1): Group 0 for STS 0-31, 1 for STS 32-47`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxde1stsintrorsta`| Set to 1 if any interrupt tus bit of corresponding STS/VC is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer per STS/VC Interrupt Enable Control

* **Description**           

The register consists of 12 interrupt enable bits for 12 STS/VCs in the Rx DS1/E1/J1 Framer.


* **RTL Instant Name**    : `dej1_rx_framer_per_stsvc_intr_en_ctrl`

* **Address**             : `0x00057BFE`

* **Formula**             : `0x00057BFE +  GrpID*1024`

* **Where**               : 

    * `$GrpID(0-1): Group 0 for STS 0-31, 1 for STS 32-47`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxde1stsintren`| Set to 1 to enable the related STS/VC to generate interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer per Group Interrupt OR Status

* **Description**           

The register consists of 2 bits for 2 Group of the Rx DS1/E1/J1 Framer


* **RTL Instant Name**    : `dej1_rx_framer_per_grp_intr_or_stat`

* **Address**             : `0x00050002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`rxde1grpintrorsta`| Set to 1 if any interrupt tus bit of corresponding Group is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Map Control

* **Description**           

The STS/VT Map Control is use to configure for per channel STS/VT Map operation.


* **RTL Instant Name**    : `stsvt_map_ctrl`

* **Address**             : `0x00076000 - 0x000767FF`

* **Formula**             : `0x00076000 +  32*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$stsid(0-47):`

    * `$vtgid(0-6):`

    * `$vtid(0-3): HDL_PATH     : stsvtmap.ctrlbuf.array.ram[$stsid*32 + $vtgid*4 + $vtid]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[20]`|`pdhoversdhmd`| Set 1 to PDH LIU over OCN| `RW`| `0x0`| `0x0`|
|`[19]`|`stsvtmapaismask`| Set 1 to mask AIS from PDH to OCN| `RW`| `0x0`| `0x0`|
|`[18:15]`|`stsvtmapjitidcfg`| STS/VT Mapping Jitter ID configure, default value is 0| `RW`| `0x0`| `0x0`|
|`[14:10]`|`stsvtmapstuffthres`| STS/VT Mapping Stuff Threshold Configure, default value is 12| `RW`| `0x0`| `0x0`|
|`[9:6]`|`stsvtmapjittercfg`| STS/VT Mapping Jitter configure - Vtmap: 4'b0000: looptiming mode 4'b1000: fine_stuff_mode, default - Spe Map: 4'b0000: full_stuff_mode 4'b1000: fine_stuff_mode, default| `RW`| `0x0`| `0x0`|
|`[5]`|`stsvtmapcenterfifo`| This bit is set to 1 to force center the mapping FIFO.| `RW`| `0x0`| `0x0`|
|`[4]`|`stsvtmapbypass`| This bit is set to 1 to bypass the STS/VT map| `RW`| `0x0`| `0x0`|
|`[3:0]`|`stsvtmapmd`| STS/VT Mapping mode StsVtMapMd	StsVtMapBypass	Operation Mode 0	          0	E1 to VT2 Map 0	          1	E1 to Bus 1	          0	DS1 to VT1.5 Map 1	          1	DS1 to Bus 2	          0	E3 to VC3 Map 2	          1	E3 to TU3 Map 3	          0	DS3 to VC3 Map 3	          1	DS3 to TU3 Map 4	          x	VT15/VT2 Basic CEP 5	          x	Packet over STS1/VC3 6	          0	STS1/VC3 Fractional CEP carrying VT2/TU12 6	          1	STS1/VC3 Fractional CEP carrying VT15/TU11 7	          0	STS1/VC3 Fractional CEP carrying E3 7	          1	STS1/VC3 Fractional CEP carrying DS3 8	          x	STS1/VC3 Basic CEP 9	          x	Packet over STS3c/VC4 10	        0	STS3c/VC4 Fractional CEP carrying VT2/TU12 10	        1	STS3c/VC4 Fractional CEP carrying VT15/TU11 11	        0	STS3c/VC4 Fractional CEP carrying E3 11	        1	STS3c/VC4 Fractional CEP carrying DS3 12	        x	STS3c/VC4/STS12c/VC4_4c Basic CEP 13	        x	Packet over TU3 14	        0	E3 to Bus 14	        1	DS3 to Bus 15	        x	Disable STS/VC/VT/TU/DS1/E1/DS3/E3| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Map HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `stsvt_map_hw_stat`

* **Address**             : `0x00074000-0x000747FF`

* **Formula**             : `0x00074000 +  32*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$stsid(0-47):`

    * `$vtgid(0-6):`

    * `$vtid(0-3):`

* **Width**               : `48`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[35:0]`|`stsvtstatus`| for HW debug only| `RO`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer Control

* **Description**           

DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer.


* **RTL Instant Name**    : `dej1_tx_framer_ctrl`

* **Address**             : `0x00094000 - 0x000947FF`

* **Formula**             : `0x00094000 +  32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$de3id(0-47):`

    * `$de2id(0-6):`

    * `$de1id(0-3): HDL_PATH     : de1txfrm.ctrlbuf.array.ram[$de3id*32 + $de2id*4 + $de1id]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[24]`|`txde1forceallone`| Force all one to TDM side when Line Local Loopback only side| `RW`| `0x0`| `0x0`|
|`[23:22]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[21]`|`txde1fbitbypass`| Set 1 to bypass Fbit insertion in Tx DS1/E1 Framer| `RW`| `0x0`| `0x0`|
|`[20]`|`txde1lineaisins`| Set 1 to enable Line AIS Insert| `RW`| `0x0`| `0x0`|
|`[19]`|`txde1payaisins`| Set 1 to enable Payload AIS Insert| `RW`| `0x0`| `0x0`|
|`[18]`|`txde1rmtlineloop`| Set 1 to enable remote Line Loop back| `RW`| `0x0`| `0x0`|
|`[17]`|`txde1rmtpayloop`| Set 1 to enable remote Payload Loop back. Sharing for VT Loopback in case of CEP path| `RW`| `0x0`| `0x0`|
|`[16]`|`txde1autoais`| Set 1 to enable AIS indication from data map block to automatically transmit all 1s at DS1/E1/J1 Tx framer| `RW`| `0x0`| `0x0`|
|`[15:9]`|`txde1dlcfg`| Incase E1 mode : use for SSM in Sa, bit[15:13] is Sa position 0=disable, 3=Sa4, 4=Sa5, 5=Sa6, 6=Sa7,7=Sa8. Bit[12:9] is SSM value. Incase DS1 mode : bit[12] Set 1 to Enable FDL transmit , dont care other bits| `RW`| `0x0`| `0x0`|
|`[8]`|`txde1autoyel`| Auto Yellow generation enable 1: Yellow alarm detected from Rx Framer will be automatically transmitted in Tx Framer 0: No automatically Yellow alarm transmitted| `RW`| `0x0`| `0x0`|
|`[7]`|`txde1frcyel`| SW force to Tx Yellow alarm| `RW`| `0x0`| `0x0`|
|`[6]`|`txde1autocrcerr`| Auto CRC error enable 1: CRC Error detected from Rx Framer will be automatically transmitted in REI bit of Tx Framer 0: No automatically CRC Error alarm transmitted| `RW`| `0x0`| `0x0`|
|`[5]`|`txde1frccrcerr`| SW force to Tx CRC Error alarm (REI bit)| `RW`| `0x0`| `0x0`|
|`[4]`|`txde1en`| Set 1 to enable the DS1/E1/J1 framer.| `RW`| `0x0`| `0x0`|
|`[3:0]`|`rxde1md`| Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF (D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1 Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer Status

* **Description**           

These registers are used for Hardware tus only


* **RTL Instant Name**    : `dej1_tx_framer_stat`

* **Address**             : `0x00095000 - 0x000957FF`

* **Formula**             : `0x00095000 +  32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$de3id(0-47):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txde1sta`| HW status only| `RO`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer Signaling Insertion Control

* **Description**           

DS1/E1/J1 Rx framer signaling insertion control is used to configure for signaling operation modes at the DS1/E1 framer.


* **RTL Instant Name**    : `dej1_tx_framer_sign_insertion_ctrl`

* **Address**             : `0x00094800 - 0x00094FFF`

* **Formula**             : `0x00094800 +  32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$de3id(0-47):`

    * `$de2id(0-6):`

    * `$de1id(0-3): HDL_PATH     : de1txfrm.siginsbuf.array.ram[$de3id*32 + $de2id*4 + $de1id]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[30]`|`txde1sigmfrmen`| Set 1 to enable the Tx DS1/E1/J1 framer to sync the signaling multiframe to the incoming data flow. No applicable for DS1| `RW`| `0x0`| `0x0`|
|`[29:0]`|`txde1sigbypass`| 30 signaling bypass bit for 32 DS0 in an E1 frame. In DS1 mode, only 24 LSB bits is used. Set 1 to bypass Signaling insertion in Tx DS1/E1 Framer| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Payload Error Insert Enable Configuration

* **Description**           




* **RTL Instant Name**    : `dej1_errins_en_cfg`

* **Address**             : `0x00090002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11]`|`txde1errorins_en`| Force payload error, inclued CRC| `RW`| `0x0`| `0x0`|
|`[10]`|`txde1errorfbit_en`| Force Fbit error| `RW`| `0x0`| `0x0`|
|`[9:0]`|`txde1errinsid`| Force ID channel| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Payload Error Insert threshold Configuration

* **Description**           




* **RTL Instant Name**    : `dej1_errins_thr_cfg`

* **Address**             : `0x00090003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txde1payerrinsthr`| Error Threshold in byte unit| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer DLK Insertion BOM Control

* **Description**           

This memory is used to control transmitting Bit-Oriented Message (BOM). This memory is accessed indirectly via indirect Access registers


* **RTL Instant Name**    : `dej1_tx_framer_dlk_ins_bom_ctrl`

* **Address**             : `0x097000-0x0977FF`

* **Formula**             : `0x097000 +  de3id*32 + de2id*4 + de1id`

* **Where**               : 

    * `$de3id(0-47):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[10]`|`dlkbomen`| enable transmitting BOM. By setting this field to 1, DLK engine will rt to insert 6-bit into the BOM pattern 111111110xxxxxx0 with the MSB is transmitted first. When transmitting finishes, engine automatically clears this bit to inform to CPU that the BOM message has been already transmitted.| `RW`| `0x0`| `0x0`|
|`[9:6]`|`dlkbomrpttime`| indicate the number of time that the BOM is transmitted repeatedly.| `RW`| `0x0`| `0x0`|
|`[5:0]`|`dlkbommsg`| 6-bit BOM message in pattern BOM 111111110xxxxxx0 in which the MSB is transmitted first.| `RW`| `0x0`| `0x0 End: Begin:`|

###TxM23E23 Control

* **Description**           

The TxM23E23 Control is use to configure for per channel Tx DS3/E3 operation.


* **RTL Instant Name**    : `txm23e23_ctrl`

* **Address**             : `0x00080000 - 0x0008003F`

* **Formula**             : `0x00080000 +  de3id`

* **Where**               : 

    * `$de3id(0-47) HDL_PATH     : txm13e13.txm23e23.ctrlbufarb.membuf.array.ram[$de3id]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[30]`|`txds3unfrmaismode`| Set 1 to send out AIS signal pattern in DS3Unframe, default set 0 to send out AIS all 1's pattern in DS3Unframe| `RW`| `0x0`| `0x0`|
|`[29]`|`txds3e3frcallone`| Force All One to remote only for Line Local Loopback(control bit5 of RX M23 0x40000)| `RW`| `0x0`| `0x0`|
|`[28]`|`txds3e3chenb`| | `RW`| `0x0`| `0x0`|
|`[27:26]`|`txds3e3dlkmode`| 0:DS3DL/E3G751NA/E3G832GC; 1:DS3UDL/E3G832NR; 2: DS3FEAC; 3:DS3NA| `RW`| `0x0`| `0x0`|
|`[25]`|`txds3e3dlkenb`| Set 1 to enable DLK DS3| `RW`| `0x0`| `0x0`|
|`[24:21]`|`txds3e3feacthres`| Number of word repeat, 0xF for send continuos| `RW`| `0x0`| `0x0`|
|`[20]`|`txds3e3lineallone`| | `RW`| `0x0`| `0x0`|
|`[19]`|`txds3e3payloop`| | `RW`| `0x0`| `0x0`|
|`[18]`|`txds3e3lineloop`| | `RW`| `0x0`| `0x0`|
|`[17]`|`txds3e3frcyel`| | `RW`| `0x0`| `0x0`|
|`[16]`|`txds3e3autoyel`| | `RW`| `0x0`| `0x0`|
|`[15:14]`|`txds3e3loopmd`| DS3 C-bit mode: [15:14]: FE Control: 00: idle code; 01: activate cw; 10: de-activate cw; 11: alrm/status E3G832 mode: [15:14]: DL mode Normal: Loop Mode configuration| `RW`| `0x0`| `0x0`|
|`[13:7]`|`txds3e3loopen`| DS3 C-bit mode: [12:7]: FEAC Control Word E3G832 mode: [13:11]: Payload type [10:7]:  SSM Config Normal: [13:7]: Loop en configuration per channel| `RW`| `0x0`| `0x0`|
|`[6]`|`txds3e3ohbypass`| | `RW`| `0x0`| `0x0`|
|`[5]`|`txds3e3idleset`| | `RW`| `0x0`| `0x0`|
|`[4]`|`txds3e3aisset`| | `RW`| `0x0`| `0x0`|
|`[3:0]`|`txds3e3md`| Tx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3 C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111: E3 G751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)| `RW`| `0x0`| `0x0 End: Begin:`|

###TxM23E23 HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `txm23e23_hw_stat`

* **Address**             : `0x00080080-0x000800BF`

* **Formula**             : `0x00080080 +  de3id`

* **Where**               : 

    * `$de3id(0-47):`

* **Width**               : `48`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[42:0]`|`txm23e23status`| for HW debug only| `RO`| `0x0`| `0x0 End: Begin:`|

###TxM12E12 Control

* **Description**           

The TxM12E12 Control is use to configure for per channel Tx DS2/E2 operation.


* **RTL Instant Name**    : `txm12e12_ctrl`

* **Address**             : `0x00081000 - 0x000811FF`

* **Formula**             : `0x00081000 +  8*de3id + de2id`

* **Where**               : 

    * `$de3id(0-47):`

    * `$de2id(0-6): HDL_PATH     : txm13e13.txm12e12.ctrlbufarb.membuf.array.ram[$de3id*8 + $de2id]`

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`txds2e2md`| Tx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2 G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved| `RW`| `0x0`| `0x0 End: Begin:`|

###TxM12E12 HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `txm12e12_hw_stat`

* **Address**             : `0x00081200 - 0x000813FF`

* **Formula**             : `0x00081200 +  8*de3id + de2id`

* **Where**               : 

    * `$de3id(0-47):`

    * `$de2id(0-6):`

* **Width**               : `96`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[80:0]`|`txm12e12status`| for HW debug only| `RO`| `0x0`| `0x0 End: Begin:`|

###Config Limit error

* **Description**           

Check pattern error in state matching pattern


* **RTL Instant Name**    : `upen_lim_err`

* **Address**             : `0xE_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3:0]`|`cfg_lim_err`| if cnt err more than limit error,search failed| `RW`| `0xF`| `0xF End: Begin:`|

###Config limit match

* **Description**           

Used to check parttern match


* **RTL Instant Name**    : `upen_lim_mat`

* **Address**             : `0xE_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:00]`|`cfg_lim_mat`| if cnt match more than limit mat, serch ok| `RW`| `0xFFE`| `0xFFE End: Begin:`|

###Config thresh fdl parttern

* **Description**           

Check fdl message codes match


* **RTL Instant Name**    : `upen_th_fdl_mat`

* **Address**             : `0xE_0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`cfg_th_fdl_mat`| if message codes repeat more than thresh, search ok,| `RW`| `0x5`| `0x5 End: Begin:`|

###Config thersh error

* **Description**           

Max error for one pattern


* **RTL Instant Name**    : `upen_th_err`

* **Address**             : `0xE_0005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[03:00]`|`cfg_th_err`| if pattern error more than thresh , change to check another pattern| `RW`| `0xF`| `0xF End: Begin:`|

###Current Inband Code

* **Description**           

Used to report current status of chid


* **RTL Instant Name**    : `upen_info`

* **Address**             : `0xE_2100 - 0xE_27FF`

* **Formula**             : `0xE_2100 + id`

* **Where**               : 

    * `$id (0-1535):`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[02:00]`|`updo_sta`| bit [2:0] == 0x0  CSU UP bit [2:0] == 0x1  CSU DOWN bit [2:0] == 0x2  FAC1 UP bit [2:0] == 0x3  FAC1 DOWN bit [2:0] == 0x4  FAC2 UP bit [2:0] == 0x5  FAC2 DOWN bit [2:0] == 0x7  Change from LoopCode to nomal| `RO`| `0x0`| `0x0 End: Begin:`|

###Config User programmble Pattern User Codes

* **Description**           

check fdl message codes


* **RTL Instant Name**    : `upen_fdlpat`

* **Address**             : `0xE_08F8`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:56]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[55:00]`|`updo_fdl_mess`| mess codes| `RO`| `0x284C_701C`| `0x284C_701C End: Begin:`|

###Currenr Message FDL Detected

* **Description**           

Info fdl message detected


* **RTL Instant Name**    : `upen_fdl_info`

* **Address**             : `0xE_1800 - 0xE_1FFF`

* **Formula**             : `0xE_1800 + $fdl_len`

* **Where**               : 

    * `$fdl_len (0-1535)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`mess_info_l`| message info bit[15:8] :	8bit BOM pattern format 0xxxxxx0 bit[4]    : set 1 indicate BOM message  bit[3:0]== 0x0 11111111_01111110 bit[3:0]== 0x1 line loop up bit[3:0]== 0x2 line loop down bit[3:0]== 0x3 payload loop up bit[3:0]== 0x4 payload loop down bit[3:0]== 0x5 smartjack act bit[3:0]== 0x6 smartjact deact bit[3:0]== 0x7 idle| `RO`| `0x0`| `0x0 End: Begin:`|

###Config Theshold Pattern Report

* **Description**           

Maximum Pattern Report


* **RTL Instant Name**    : `upen_th_stt_int`

* **Address**             : `0xE_0006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`updo_th_stt_int`| if number of pattern detected more than threshold, interrupt enable| `RW`| `0xFF`| `0xFF End: Begin:`|

###RAM Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_Parity_Force_Control`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:13]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[12]`|`pdhvtasyncmapctrl_parerrfrc`| Force parity For RAM Control "PDH VT Async Map Control"| `RW`| `0x0`| `0x0`|
|`[11]`|`pdhstsvtmapctrl_parerrfrc`| Force parity For RAM Control "PDH STS/VT Map Control"| `RW`| `0x0`| `0x0`|
|`[10]`|`pdhtxm23e23trace_parerrfrc`| Force parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"| `RW`| `0x0`| `0x0`|
|`[9]`|`pdhtxm23e23ctrl_parerrfrc`| Force parity For RAM Control "PDH TxM23E23 Control"| `RW`| `0x0`| `0x0`|
|`[8]`|`pdhtxm12e12ctrl_parerrfrc`| Force parity For RAM Control "PDH TxM12E12 Control"| `RW`| `0x0`| `0x0`|
|`[7]`|`pdhtxde1sigctrl_parerrfrc`| Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling Control"| `RW`| `0x0`| `0x0`|
|`[6]`|`pdhtxde1frmctrl_parerrfrc`| Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"| `RW`| `0x0`| `0x0`|
|`[5]`|`pdhrxde1frmctrl_parerrfrc`| Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"| `RW`| `0x0`| `0x0`|
|`[4]`|`pdhrxm12e12ctrl_parerrfrc`| Force parity For RAM Control "PDH RxM12E12 Control"| `RW`| `0x0`| `0x0`|
|`[3]`|`pdhrxds3e3ohctrl_parerrfrc`| Force parity For RAM Control "PDH RxDS3E3 OH Pro Control"| `RW`| `0x0`| `0x0`|
|`[2]`|`pdhrxm23e23ctrl_parerrfrc`| Force parity For RAM Control "PDH RxM23E23 Control"| `RW`| `0x0`| `0x0`|
|`[1]`|`pdhstsvtdemapctrl_parerrfrc`| Force parity For RAM Control "PDH STS/VT Demap Control"| `RW`| `0x0`| `0x0`|
|`[0]`|`pdhmuxctrl_parerrfrc`| Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux Control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_Parity_Disable_Control`

* **Address**             : `0x11`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:13]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[12]`|`pdhvtasyncmapctrl_parerrdis`| Disable parity For RAM Control "PDH VT Async Map Control"| `RW`| `0x0`| `0x0`|
|`[11]`|`pdhstsvtmapctrl_parerrdis`| Disable parity For RAM Control "PDH STS/VT Map Control"| `RW`| `0x0`| `0x0`|
|`[10]`|`pdhtxm23e23trace_parerrdis`| Disable parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"| `RW`| `0x0`| `0x0`|
|`[9]`|`pdhtxm23e23ctrl_parerrdis`| Disable parity For RAM Control "PDH TxM23E23 Control"| `RW`| `0x0`| `0x0`|
|`[8]`|`pdhtxm12e12ctrl_parerrdis`| Disable parity For RAM Control "PDH TxM12E12 Control"| `RW`| `0x0`| `0x0`|
|`[7]`|`pdhtxde1sigctrl_parerrdis`| Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling Control"| `RW`| `0x0`| `0x0`|
|`[6]`|`pdhtxde1frmctrl_parerrdis`| Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"| `RW`| `0x0`| `0x0`|
|`[5]`|`pdhrxde1frmctrl_parerrdis`| Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"| `RW`| `0x0`| `0x0`|
|`[4]`|`pdhrxm12e12ctrl_parerrdis`| Disable parity For RAM Control "PDH RxM12E12 Control"| `RW`| `0x0`| `0x0`|
|`[3]`|`pdhrxds3e3ohctrl_parerrdis`| Disable parity For RAM Control "PDH RxDS3E3 OH Pro Control"| `RW`| `0x0`| `0x0`|
|`[2]`|`pdhrxm23e23ctrl_parerrdis`| Disable parity For RAM Control "PDH RxM23E23 Control"| `RW`| `0x0`| `0x0`|
|`[1]`|`pdhstsvtdemapctrl_parerrdis`| Disable parity For RAM Control "PDH STS/VT Demap Control"| `RW`| `0x0`| `0x0`|
|`[0]`|`pdhmuxctrl_parerrdis`| Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux Control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_Parity_Error_Sticky`

* **Address**             : `0x12`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12]`|`pdhvtasyncmapctrl_parerrstk`| Error parity For RAM Control "PDH VT Async Map Control"| `W1C`| `0x0`| `0x0`|
|`[11]`|`pdhstsvtmapctrl_parerrstk`| Error parity For RAM Control "PDH STS/VT Map Control"| `W1C`| `0x0`| `0x0`|
|`[10]`|`pdhtxm23e23trace_parerrstk`| Error parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"| `W1C`| `0x0`| `0x0`|
|`[9]`|`pdhtxm23e23ctrl_parerrstk`| Error parity For RAM Control "PDH TxM23E23 Control"| `W1C`| `0x0`| `0x0`|
|`[8]`|`pdhtxm12e12ctrl_parerrstk`| Error parity For RAM Control "PDH TxM12E12 Control"| `W1C`| `0x0`| `0x0`|
|`[7]`|`pdhtxde1sigctrl_parerrstk`| Error parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling Control"| `W1C`| `0x0`| `0x0`|
|`[6]`|`pdhtxde1frmctrl_parerrstk`| Error parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"| `W1C`| `0x0`| `0x0`|
|`[5]`|`pdhrxde1frmctrl_parerrstk`| Error parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"| `W1C`| `0x0`| `0x0`|
|`[4]`|`pdhrxm12e12ctrl_parerrstk`| Error parity For RAM Control "PDH RxM12E12 Control"| `W1C`| `0x0`| `0x0`|
|`[3]`|`pdhrxds3e3ohctrl_parerrstk`| Error parity For RAM Control "PDH RxDS3E3 OH Pro Control"| `W1C`| `0x0`| `0x0`|
|`[2]`|`pdhrxm23e23ctrl_parerrstk`| Error parity For RAM Control "PDH RxM23E23 Control"| `W1C`| `0x0`| `0x0`|
|`[1]`|`pdhstsvtdemapctrl_parerrstk`| Error parity For RAM Control "PDH STS/VT Demap Control"| `W1C`| `0x0`| `0x0`|
|`[0]`|`pdhmuxctrl_parerrstk`| Error parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux Control"| `W1C`| `0x0`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_PM
####Register Table

|Name|Address|
|-----|-----|
|`FMPM Block Version`|`0x00000`|
|`FMPM Read DDR Control`|`0x00001`|
|`FMPM Change Page DDR`|`0x00002`|
|`FMPM Parity Force`|`0x00060`|
|`FMPM Parity Disable`|`0x00061`|
|`FMPM Parity Sticky`|`0x00062`|
|`FMPM Force Reset Core`|`0x00005`|
|`FMPM Config Number Clock Of 125us`|`0x00007`|
|`FMPM Syn Get Time`|`0x00008`|
|`FMPM Time Value`|`0x00009`|
|`FMPM Monitor Timer`|`0x0001C`|
|`FMPM Hold00 Data Of Read DDR`|`0x00020`|
|`FMPM Hold01 Data Of Read DDR`|`0x00021`|
|`FMPM Hold02 Data Of Read DDR`|`0x00022`|
|`FMPM Hold03 Data Of Read DDR`|`0x00023`|
|`FMPM Hold04 Data Of Read DDR`|`0x00024`|
|`FMPM Hold05 Data Of Read DDR`|`0x00025`|
|`FMPM Hold06 Data Of Read DDR`|`0x00026`|
|`FMPM Hold07 Data Of Read DDR`|`0x00027`|
|`FMPM Hold08 Data Of Read DDR`|`0x00028`|
|`FMPM Hold09 Data Of Read DDR`|`0x00029`|
|`FMPM Hold10 Data Of Read DDR`|`0x0002A`|
|`FMPM Hold11 Data Of Read DDR`|`0x0002B`|
|`FMPM Hold12 Data Of Read DDR`|`0x0002C`|
|`FMPM Hold13 Data Of Read DDR`|`0x0002D`|
|`FMPM Hold14 Data Of Read DDR`|`0x0002E`|
|`FMPM Hold15 Data Of Read DDR`|`0x0002F`|
|`FMPM Hold16 Data Of Read DDR`|`0x00030`|
|`FMPM Hold17 Data Of Read DDR`|`0x00031`|
|`FMPM Hold18 Data Of Read DDR`|`0x00032`|
|`FMPM Hold19 Data Of Read DDR`|`0x00033`|
|`FMPM Hold20 Data Of Read DDR`|`0x00034`|
|`FMPM Hold21 Data Of Read DDR`|`0x00035`|
|`FMPM Hold22 Data Of Read DDR`|`0x00036`|
|`FMPM Hold23 Data Of Read DDR`|`0x00037`|
|`FMPM Hold24 Data Of Read DDR`|`0x00038`|
|`FMPM K threshold of Section`|`0x000D0-0x000D7`|
|`FMPM K threshold of STS`|`0x00080-0x00087`|
|`FMPM K Threshold of VT`|`0x00090-0x00097`|
|`FMPM K Threshold of DS3`|`0x000A0-0x000A7`|
|`FMPM K Threshold of DS1`|`0x000B0-0x000B7`|
|`FMPM K Threshold of PW`|`0x000C0-0x000C7`|
|`FMPM TCA threshold of Section Part0`|`0xB0200-0xB020E`|
|`FMPM TCA threshold of Section Part1`|`0xB0210-0xB021E`|
|`FMPM TCA threshold of Section Part2`|`0xB0220-0xB022E`|
|`FMPM TCA threshold of Section Part3`|`0xB0230-0xB023E`|
|`FMPM TCA threshold of Section Part4`|`0xB0240-0xB024E`|
|`FMPM TCA threshold of Section Part5`|`0xB0250-0xB025E`|
|`FMPM TCA threshold of Section Part6`|`0xB0260-0xB026E`|
|`FMPM TCA threshold of HO Part0`|`0xB0000-0xB000E`|
|`FMPM TCA threshold of HO Part1`|`0xB0010-0xB001E`|
|`FMPM TCA threshold of HO Part2`|`0xB0020-0xB002E`|
|`FMPM TCA threshold of HO Part3`|`0xB0030-0xB003E`|
|`FMPM TCA threshold of HO Part4`|`0xB0040-0xB004E`|
|`FMPM TCA threshold of HO Part5`|`0xB0050-0xB005E`|
|`FMPM TCA threshold of HO Part6`|`0xB0060-0xB006E`|
|`FMPM TCA threshold of HO Part7`|`0xB0070-0xB007E`|
|`FMPM TCA threshold of HO Part8`|`0xB0080-0xB008E`|
|`FMPM TCA threshold of HO Part9`|`0xB0090-0xB009E`|
|`FMPM TCA threshold of HO Part10`|`0xB00A0-0xB00AE`|
|`FMPM TCA threshold of LO Part0`|`0xB0300-0xB030E`|
|`FMPM TCA threshold of LO Part1`|`0xB0310-0xB031E`|
|`FMPM TCA threshold of LO Part2`|`0xB0320-0xB032E`|
|`FMPM TCA threshold of LO Part3`|`0xB0330-0xB033E`|
|`FMPM TCA threshold of LO Part4`|`0xB0340-0xB034E`|
|`FMPM TCA threshold of LO Part5`|`0xB0350-0xB035E`|
|`FMPM TCA threshold of LO Part6`|`0xB0360-0xB036E`|
|`FMPM TCA threshold of LO Part7`|`0xB0370-0xB037E`|
|`FMPM TCA threshold of LO Part8`|`0xB0380-0xB038E`|
|`FMPM TCA threshold of LO Part9`|`0xB0390-0xB039E`|
|`FMPM TCA threshold of LO Part10`|`0xB03A0-0xB03AE`|
|`FMPM TCA threshold of DS3 Part0`|`0xB0100-0xB010E`|
|`FMPM TCA threshold of DS3 Part1`|`0xB0110-0xB011E`|
|`FMPM TCA threshold of DS3 Part2`|`0xB0120-0xB012E`|
|`FMPM TCA threshold of DS3 Part3`|`0xB0130-0xB013E`|
|`FMPM TCA threshold of DS3 Part4`|`0xB0140-0xB014E`|
|`FMPM TCA threshold of DS3 Part5`|`0xB0150-0xB015E`|
|`FMPM TCA threshold of DS3 Part6`|`0xB0160-0xB016E`|
|`FMPM TCA threshold of DS3 Part7`|`0xB0170-0xB017E`|
|`FMPM TCA threshold of DS3 Part8`|`0xB0180-0xB018E`|
|`FMPM TCA threshold of DS3 Part9`|`0xB0190-0xB019E`|
|`FMPM TCA threshold of DS3 Part10`|`0xB01A0-0xB01AE`|
|`FMPM TCA threshold of DS3 Part11`|`0xB01B0-0xB01BE`|
|`FMPM TCA threshold of DS3 Part12`|`0xB01C0-0xB01CE`|
|`FMPM TCA threshold of DS1 Part0`|`0xB0400-0xB040E`|
|`FMPM TCA threshold of DS1 Part1`|`0xB0410-0xB041E`|
|`FMPM TCA threshold of DS1 Part2`|`0xB0420-0xB042E`|
|`FMPM TCA threshold of DS1 Part3`|`0xB0430-0xB043E`|
|`FMPM TCA threshold of DS1 Part4`|`0xB0440-0xB044E`|
|`FMPM TCA threshold of DS1 Part5`|`0xB0450-0xB045E`|
|`FMPM TCA threshold of DS1 Part6`|`0xB0460-0xB046E`|
|`FMPM TCA threshold of DS1 Part7`|`0xB0470-0xB047E`|
|`FMPM TCA threshold of PW Part0`|`0xB0500-0xB050E`|
|`FMPM TCA threshold of PW Part1`|`0xB0510-0xB051E`|
|`FMPM TCA threshold of PW Part2`|`0xB0520-0xB052E`|
|`FMPM Enable PM Collection Section`|`0xB1800-0xB19FF`|
|`FMPM Enable PM Collection STS`|`0xB1000-0xB11FF`|
|`FMPM Enable PM Collection DS3`|`0xB1400-0xB15FF`|
|`FMPM Enable PM Collection VT`|`0xB4000-0xB7FFF`|
|`FMPM Enable PM Collection DS1`|`0xB8000-0xBBFFF`|
|`FMPM Enable PM Collection PW`|`0xBC000-0xBFFFF`|
|`FMPM Threshold Type of Section`|`0x00600-0x006FF`|
|`FMPM Threshold Type of STS`|`0x00200-0x003FF`|
|`FMPM Threshold Type of DS3`|`0x00400-0x005FF`|
|`FMPM Threshold Type of VT`|`0x04000-0x07FFF`|
|`FMPM Threshold Type of DS1`|`0x08000-0x0BFFF`|
|`FMPM Threshold Type of PW`|`0x0C000-0x0FFFF`|
|`FMPM FM Interrupt`|`0x14000`|
|`FMPM FM Interrupt Mask`|`0x14001`|
|`FMPM FM EC1 Interrupt OR`|`0x18005`|
|`FMPM FM EC1 Interrupt OR AND MASK Per STS1`|`0x18100-0x1810F`|
|`FMPM FM EC1 Interrupt MASK Per STS1`|`0x18110-0x1811F`|
|`FMPM FM EC1STM0 Interrupt Sticky Per Type Of Per STS1`|`0x1B600-0x1B7FF`|
|`FMPM FM EC1STM0 Interrupt MASK Per Type Of Per STS1`|`0x1B800-0x1B9FF`|
|`FMPM FM EC1STM0 Interrupt Current Status Per Type Of Per STS1`|`0x1BA00-0x1BBFF`|
|`FMPM FM STS24 Interrupt OR`|`0x18000`|
|`FMPM FM STS24 Interrupt OR AND MASK Per STS1`|`0x18080-0x1808F`|
|`FMPM FM STS24 Interrupt MASK Per STS1`|`0x18090-0x1809F`|
|`FMPM FM STS24 Interrupt Sticky Per Type Of Per STS1`|`0x1A000-0x1A1FF`|
|`FMPM FM STS24 Interrupt MASK Per Type Of Per STS1`|`0x1A200-0x1A3FF`|
|`FMPM FM STS24 Interrupt Current Status Per Type Of Per STS1`|`0x1A400-0x1A5FF`|
|`FMPM FM DS3E3 Group Interrupt OR`|`0x18001`|
|`FMPM FM DS3E3 Framer Interrupt OR AND MASK Per DS3E3`|`0x180A0-0x180AF`|
|`FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3`|`0x180B0-0x180BF`|
|`FMPM FM DS3E3 Framer Interrupt Sticky Per Type of Per DS3E3`|`0x1A600-0x1A7FF`|
|`FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3`|`0x1A800-0x1A9FF`|
|`FMPM FM DS3E3 Framer Interrupt Current Status Per Type of Per DS3E3`|`0x1AA00-0x1ABFF`|
|`FMPM FM STS24 LO Interrupt OR`|`0x18002`|
|`FMPM FM STS1 LO Interrupt OR`|`0x180C0-0x180CF`|
|`FMPM FM VTTU Interrupt OR AND MASK Per VTTU`|`0x1AC00-0x1ADFF`|
|`FMPM FM VTTU Interrupt MASK Per VTTU`|`0x1AE00-0x1AFFF`|
|`FMPM FM VTTU Interrupt Sticky Per Type Per VTTU`|`0x24000-0x27FFF`|
|`FMPM FM VTTU Interrupt MASK Per Type Per VTTU`|`0x28000-0x2BFFF`|
|`FMPM FM VTTU Interrupt Current Status Per Type Per VTTU`|`0x2C000-0x2FFFF`|
|`FMPM FM DS1E1 Level1 Group Interrupt OR`|`0x18003`|
|`FMPM FM DS1E1 Level2 Group Interrupt OR`|`0x180D0-0x180DF`|
|`FMPM FM DS1E1 Framer Interrupt OR AND MASK Per DS1E1`|`0x1B000-0x1B1FF`|
|`FMPM FM DS1E1 Framer Interrupt MASK Per DS1E1`|`0x1B200-0x1B3FF`|
|`FMPM FM DS1E1 Interrupt Sticky Per Type Per DS1E1`|`0x30000-0x33FFF`|
|`FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1`|`0x34000-0x37FFF`|
|`FMPM FM DS1E1 Interrupt Current Status Per Type Per DS1E1`|`0x38000-0x3BFFF`|
|`FMPM FM PW Level1 Group Interrupt OR`|`0x18004`|
|`FMPM FM PW Level2 Group Interrupt OR`|`0x180E0-0x180EF`|
|`FMPM FM PW Framer Interrupt OR AND MASK Per PW`|`0x1BC00-0x1BDFF`|
|`FMPM FM PW Framer Interrupt MASK Per PW`|`0x1BE00-0x1BFFF`|
|`FMPM FM PW Interrupt Sticky Per Type Per PW`|`0x1C000-0x1DFFF`|
|`FMPM FM PW Interrupt MASK Per Type Per PW`|`0x20000-0x23FFF`|
|`FMPM FM PW Interrupt Current Status Per Type Per PW`|`0x3C000-0x3FFFF`|
|`FMPM TCA Interrupt`|`0x54000`|
|`FMPM TCA Interrupt Mask`|`0x54001`|
|`FMPM TCA EC1 Interrupt OR`|`0x58005`|
|`FMPM TCA EC1 Interrupt OR AND MASK Per STS1`|`0x58100-0x5810F`|
|`FMPM TCA EC1 Interrupt MASK Per STS1`|`0x58110-0x5811F`|
|`FMPM TCA EC1STM0 Interrupt Sticky Per Type Of Per STS1`|`0x5B600-0x5B7FF`|
|`FMPM TCA EC1STM0 Interrupt MASK Per Type Of Per STS1`|`0x5B800-0x5B9FF`|
|`FMPM TCA EC1STM0 Interrupt Current Status Per Type Of Per STS1`|`0x5BA00-0x5BBFF`|
|`FMPM TCA STS24 Interrupt OR`|`0x58000`|
|`FMPM TCA STS24 Interrupt OR AND MASK Per STS1`|`0x58080-0x5808F`|
|`FMPM TCA STS24 Interrupt MASK Per STS1`|`0x58090-0x5809F`|
|`FMPM TCA STS24 Interrupt Sticky Per Type Of Per STS1`|`0x5A000-0x5A1FF`|
|`FMPM TCA STS24 Interrupt MASK Per Type Of Per STS1`|`0x5A200-0x5A3FF`|
|`FMPM TCA STS24 Interrupt Current Status Per Type Of Per STS1`|`0x5A400-0x5A5FF`|
|`FMPM TCA DS3E3 Group Interrupt OR`|`0x58001`|
|`FMPM TCA DS3E3 Framer Interrupt OR AND MASK Per DS3E3`|`0x580A0-0x580AF`|
|`FMPM TCA DS3E3 Framer Interrupt MASK Per DS3E3`|`0x580B0-0x580BF`|
|`FMPM TCA DS3E3 Framer Interrupt Sticky Per Type of Per DS3E3`|`0x5A600-0x5A7FF`|
|`FMPM TCA DS3E3 Framer Interrupt MASK Per Type of Per DS3E3`|`0x5A800-0x5A9FF`|
|`FMPM TCA DS3E3 Framer Interrupt Current Status Per Type of Per DS3E3`|`0x5AA00-0x5ABFF`|
|`FMPM TCA STS24 LO Interrupt OR`|`0x58002`|
|`FMPM TCA STS1 LO Interrupt OR`|`0x580C0-0x580CF`|
|`FMPM TCA VTTU Interrupt OR AND MASK Per VTTU`|`0x5AC00-0x5ADFF`|
|`FMPM TCA VTTU Interrupt MASK Per VTTU`|`0x5AE00-0x5AFFF`|
|`FMPM TCA VTTU Interrupt Sticky Per Type Per VTTU`|`0x64000-0x67FFF`|
|`FMPM TCA VTTU Interrupt MASK Per Type Per VTTU`|`0x68000-0x6BFFF`|
|`FMPM TCA VTTU Interrupt Current Status Per Type Per VTTU`|`0x6C000-0x6FFFF`|
|`FMPM TCA DS1E1 Level1 Group Interrupt OR`|`0x58003`|
|`FMPM TCA DS1E1 Level2 Group Interrupt OR`|`0x580D0-0x580DF`|
|`FMPM TCA DS1E1 Framer Interrupt OR AND MASK Per DS1E1`|`0x5B000-0x5B1FF`|
|`FMPM TCA DS1E1 Framer Interrupt MASK Per DS1E1`|`0x5B200-0x5B3FF`|
|`FMPM TCA DS1E1 Interrupt Sticky Per Type Per DS1E1`|`0x70000-0x73FFF`|
|`FMPM TCA DS1E1 Interrupt MASK Per Type Per DS1E1`|`0x74000-0x77FFF`|
|`FMPM TCA DS1E1 Interrupt Current Status Per Type Per DS1E1`|`0x78000-0x7BFFF`|
|`FMPM TCA PW Level1 Group Interrupt OR`|`0x58004`|
|`FMPM TCA PW Level2 Group Interrupt OR`|`0x580E0-0x580EF`|
|`FMPM TCA PW Framer Interrupt OR AND MASK Per PW`|`0x5BC00-0x5BDFF`|
|`FMPM TCA PW Framer Interrupt MASK Per PW`|`0x5BE00-0x5BFFF`|
|`FMPM TCA PW Interrupt Sticky Per Type Per PW`|`0x5C000-0x5FFFF`|
|`FMPM TCA PW Interrupt MASK Per Type Per PW`|`0x60000-0x63FFF`|
|`FMPM TCA PW Interrupt Current Status Per Type Per PW`|`0x7C000-0x7FFFF`|


###FMPM Block Version

* **Description**           

PM Block Version


* **RTL Instant Name**    : `FMPM_Block_Version`

* **Address**             : `0x00000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`day`| day, hexdecimal format| `R_O`| `0x0`| `0x0`|
|`[23:16]`|`month`| month, hexdecimal format| `R_O`| `0x0`| `0x0`|
|`[15:08]`|`year`| year, hexdecimal format| `R_O`| `0x0`| `0x0`|
|`[07:04]`|`project`| Project ID, hexdecimal format<br>{1} : OCN Cisco project <br>{2} : DS1 Cisco project <br>{3} : DS3 Cisco project <br>{4} : 3G  Cisco project <br>{5} : 5G  Cisco project <br>{6} : DS3 Ciena project <br>{7} : 10G Ciena project <br>{8} : 20G Ciena project| `R_O`| `0x0`| `0x0`|
|`[03:00]`|`number`| number of synthesis FPGA each day, hexdecimal format| `R_O`| `0x0`| `0x0`|

###FMPM Read DDR Control

* **Description**           

Read Parameters or Statistic counters of DDR


* **RTL Instant Name**    : `FMPM_Read_DDR_Control`

* **Address**             : `0x00001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`done`| Access DDR Is Done<br>{1} : Done| `R/W/C`| `0x0`| `0x0`|
|`[30:30]`|`start`| Trigger 0->1 to start access DDR| `R/W`| `0x0`| `0x0`|
|`[29:29]`|`page`| Page access<br>{0} : Page 0 <br>{1} : Page 1| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`r2c`| Read mode<br>{0} : Read Only <br>{1} : Read to clear| `R/W`| `0x0`| `0x0`|
|`[27:20]`|`type`| Counter Type<br>{0} : PW's Parameter <br>{2} : VT's Parameter <br>{3} : DS1's Parameter <br>{4} : STS's Parameter <br>{5} : DS3's Parameter <br>{6} : EC1's Parameter| `R/W`| `0x0`| `0x0`|
|`[19:14]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[13:00]`|`adr`| Address of Counter<br>{Case PW's Parameter} <br>{+ [13:00]} : PW ID <br>{Case VT's Paramater} <br>{+ [13:10]} : STS24-LO ID (0-15) <br>{+ [09:05]} : STS1-LO ID (0-23) <br>{+ [04:00]} : VT/TU ID (0-27) <br>{Case DS1/E1's Paramater} <br>{+ [13:10]} : DS1/E1 Level-1 Group ID (0-15) <br>{+ [09:05]} : DS1/E1 Level-2 Group ID (0-23) <br>{+ [04:00]} : DS1/E1 Framer ID (0-27) <br>{Case STS's Paramater} <br>{+ [13:10]} : STS-24 group (0-15) <br>{+ [09:05]} : STS1 ID (0-23) <br>{Case DS3/E3's Paramater} <br>{+ [13:10]} : DS3/E3 Group  (0-15) <br>{+ [09:05]} : DS3/E3 Framer (0-23) <br>{Case EC1's Paramater} <br>{+ [13:10]} : STS-24 group (0-0) <br>{+ [09:05]} : STS1 ID (0-15)| `R/W`| `0x0`| `0x0`|

###FMPM Change Page DDR

* **Description**           

Change Page for DDR when SW want to get Current Period Parameters


* **RTL Instant Name**    : `FMPM_Change_Page_DDR`

* **Address**             : `0x00002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:09]`|`chg_page_enb`| Enable change page<br>{0} : HW doesn't support Previous period, HW has only one page, it is current page or Current period so HW is disable change page action <br>{1} : HW support Previous period, HW has two pages, they are current and previous pages (Current and Previous period) so HW is enable change page action| `R_O`| `0x0`| `0x0`|
|`[08:08]`|`hw_page`| Get Current Page of DDR<br>{0} : Page 0 <br>{1} : Page 1| `R_O`| `0x0`| `0x0`|
|`[07:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:04]`|`chg_done`| Change page is done<br>{1} : Done| `R/W/C`| `0x0`| `0x0`|
|`[03:02]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:01]`|`sel_chg_page`| Select change page mode<br>{0} : change page by sw <br>{1} : change page by hw when timer reachs 900s| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`chg_start`| Trigger 0->1 to request change page, this bit is available when bit SEL_CHG_PAGE is 0| `R/W`| `0x0`| `0x0`|

###FMPM Parity Force

* **Description**           

Force parity for RAM configuration


* **RTL Instant Name**    : `FMPM_Pariry_Force`

* **Address**             : `0x00060`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:19]`|`par_for_sts_msk_chn`| FMPM FM STS24 Interrupt MASK Per STS1<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[18:18]`|`par_for_sts_msk_typ`| FMPM FM STS24 Interrupt MASK Per Type Of Per STS1<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[17:17]`|`par_for_vt_msk_chn`| FMPM FM VTTU Interrupt MASK Per VTTU<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[16:16]`|`par_for_vt_msk_typ`| FMPM FM VTTU Interrupt MASK Per Type Per VTTU<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[15:15]`|`par_for_ds3_msk_chn`| FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[14:14]`|`par_for_ds3_msk_typ`| FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[13:13]`|`par_for_ds1_msk_chn`| FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`par_for_ds1_msk_typ`| FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`par_for_pw_msk_chn`| FMPM FM PW Framer Interrupt MASK Per PW<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`par_for_pw_msk_typ`| FMPM FM PW Interrupt MASK Per Type Per PW<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`par_for_config_sts_typ`| FMPM Threshold Type of STS<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`par_for_config_vt_typ`| FMPM Threshold Type of VT<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`par_for_config_ds3_typ`| FMPM Threshold Type of DS3<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`par_for_config_ds1_typ`| FMPM Threshold Type of DS1<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`par_for_config_pw_typ`| FMPM Threshold Type of PW<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`par_for_config_sts_k`| FMPM K threshold of STS<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`par_for_config_vt_k`| FMPM K Threshold of VT<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`par_for_config_ds3_k`| FMPM K Threshold of DS3<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`par_for_config_ds1_k`| FMPM K Threshold of DS1<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`par_for_config_pw_k`| FMPM K Threshold of PW<br>{1} : Force parity error| `R/W`| `0x0`| `0x0`|

###FMPM Parity Disable

* **Description**           

Force parity for RAM configuration


* **RTL Instant Name**    : `FMPM_Pariry_Disable`

* **Address**             : `0x00061`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:19]`|`par_for_sts_msk_chn`| FMPM FM STS24 Interrupt MASK Per STS1<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[18:18]`|`par_for_sts_msk_typ`| FMPM FM STS24 Interrupt MASK Per Type Of Per STS1<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[17:17]`|`par_for_vt_msk_chn`| FMPM FM VTTU Interrupt MASK Per VTTU<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[16:16]`|`par_for_vt_msk_typ`| FMPM FM VTTU Interrupt MASK Per Type Per VTTU<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[15:15]`|`par_for_ds3_msk_chn`| FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[14:14]`|`par_for_ds3_msk_typ`| FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[13:13]`|`par_for_ds1_msk_chn`| FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`par_for_ds1_msk_typ`| FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`par_for_pw_msk_chn`| FMPM FM PW Framer Interrupt MASK Per PW<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`par_for_pw_msk_typ`| FMPM FM PW Interrupt MASK Per Type Per PW<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`par_for_config_sts_typ`| FMPM Threshold Type of STS<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`par_for_config_vt_typ`| FMPM Threshold Type of VT<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`par_for_config_ds3_typ`| FMPM Threshold Type of DS3<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`par_for_config_ds1_typ`| FMPM Threshold Type of DS1<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`par_for_config_pw_typ`| FMPM Threshold Type of PW<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`par_for_config_sts_k`| FMPM K threshold of STS<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`par_for_config_vt_k`| FMPM K Threshold of VT<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`par_for_config_ds3_k`| FMPM K Threshold of DS3<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`par_for_config_ds1_k`| FMPM K Threshold of DS1<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`par_for_config_pw_k`| FMPM K Threshold of PW<br>{1} : Disable parity error| `R/W`| `0x0`| `0x0`|

###FMPM Parity Sticky

* **Description**           

Change Page for DDR when SW want to get Current Period Parameters


* **RTL Instant Name**    : `FMPM_Pariry_Sticky`

* **Address**             : `0x00062`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:19]`|`par_stk_sts_msk_chn`| FMPM FM STS24 Interrupt MASK Per STS1<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[18:18]`|`par_stk_sts_msk_typ`| FMPM FM STS24 Interrupt MASK Per Type Of Per STS1<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[17:17]`|`par_stk_vt_msk_chn`| FMPM FM VTTU Interrupt MASK Per VTTU<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[16:16]`|`par_stk_vt_msk_typ`| FMPM FM VTTU Interrupt MASK Per Type Per VTTU<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[15:15]`|`par_stk_ds3_msk_chn`| FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[14:14]`|`par_stk_ds3_msk_typ`| FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[13:13]`|`par_stk_ds1_msk_chn`| FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[12:12]`|`par_stk_ds1_msk_typ`| FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[11:11]`|`par_stk_pw_msk_chn`| FMPM FM PW Framer Interrupt MASK Per PW<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`par_stk_pw_msk_typ`| FMPM FM PW Interrupt MASK Per Type Per PW<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`par_stk_config_sts_typ`| FMPM Threshold Type of STS<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`par_stk_config_vt_typ`| FMPM Threshold Type of VT<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`par_stk_config_ds3_typ`| FMPM Threshold Type of DS3<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`par_stk_config_ds1_typ`| FMPM Threshold Type of DS1<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`par_stk_config_pw_typ`| FMPM Threshold Type of PW<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`par_stk_config_sts_k`| FMPM K threshold of STS<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`par_stk_config_vt_k`| FMPM K Threshold of VT<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`par_stk_config_ds3_k`| FMPM K Threshold of DS3<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`par_stk_config_ds1_k`| FMPM K Threshold of DS1<br>{1} : parity error| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`par_stk_config_pw_k`| FMPM K Threshold of PW<br>{1} : parity error| `W1C`| `0x0`| `0x0`|

###FMPM Force Reset Core

* **Description**           

Force Reset PM Core


* **RTL Instant Name**    : `FMPM_Force_Reset_Core`

* **Address**             : `0x00005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[18:18]`|`sel_1s_mode`| Select source of 1s for PM function<br>{1} : select PM_tick 1s on board <br>{0} : select free running| `R/W`| `0x1`| `0x1`|
|`[17:17]`|`param_enb`| Enable Parameter counters<br>{1} : enable <br>{0} : disable| `R/W`| `0x1`| `0x1`|
|`[16:09]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[08:08]`|`drst_enb`| Enable PM block, SW has to do reset PM block when this bit is transition from 0/1 to 1/0<br>{1} : enable, SW has to reset PM block after SW configures this bit from 0 to 1 <br>{0} : disable, SW has to reset PM block after SW configures this bit from 1 to 0| `R/W`| `0x0`| `0x0`|
|`[07:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`drdy_chg`| Ready signal has changed 0/1<br>{1} : has changed| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`drst`| PM Core reset done<br>{1} : Ready to work| `R_O`| `0x0`| `0x0`|
|`[03:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`frst`| Trigger 0->1 to reset PM Core| `R/W`| `0x0`| `0x0`|

###FMPM Config Number Clock Of 125us

* **Description**           

Configure number clock of 125us. This register is used by HW only.


* **RTL Instant Name**    : `FMPM_Config_Number_Clock_Of_125us`

* **Address**             : `0x00007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`time2p5s`| Threshold for Failure set  (Ref 2.5s), step 100ms, SW has to minus 1 before configuring to HW| `R/W`| `0x15`| `0x15`|
|`[23:16]`|`time10s`| Threshold for Failure clear(Ref  10s), step 100ms, SW has to minus 1 before configuring to HW| `R/W`| `0x61`| `0x61`|
|`[15:00]`|`config125us`| Number of clocks for one 125us unit. N(mhz) is operation clock of PM block then the configuration value is (N*125-2)| `R/W`| `0x4BEE`| `0x4BEE`|

###FMPM Syn Get Time

* **Description**           

SW request HW synchronize timing.


* **RTL Instant Name**    : `FMPM_Syn_Get_time`

* **Address**             : `0x00008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:21]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[20:20]`|`syntime`| Trigger 0->1 to synchronize timing| `R/W`| `0x0`| `0x0`|
|`[19:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`gettime`| Trigger 0->1 to get time value| `R/W`| `0x0`| `0x0`|
|`[15:15]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[14:00]`|`numclk`| number of clocks for one 125us unit| `R_O`| `0x0`| `0x0`|

###FMPM Time Value

* **Description**           

Time value after SW request to get time


* **RTL Instant Name**    : `FMPM_Time_Value`

* **Address**             : `0x00009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:27]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[26:16]`|`cnt1s`| counter 1s| `R_O`| `0x0`| `0x0`|
|`[15:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:04]`|`cnt1ms`| counter 1ms| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[02:00]`|`cnt125us`| counter 125us| `R_O`| `0x0`| `0x0`|

###FMPM Monitor Timer

* **Description**           

Time value after SW request to get time


* **RTL Instant Name**    : `FMPM_Monitor_Timer`

* **Address**             : `0x0001C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mon125us`| monitor timer of HW, resolution is 125us, counter will be roll-over when it get maximum value| `R_O`| `0x0`| `0x0`|

###FMPM Hold00 Data Of Read DDR

* **Description**           

Hold00 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold00_Data_Of_Read_DDR`

* **Address**             : `0x00020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold00`| Missing bit is unused<br>{Case STS} <br>{+   [23:00]} : CV_P <br>{Case VT} <br>{+   [23:00]} : CV_V <br>{Case DS3} <br>{+   [23:00]} : CV_L <br>{Case DS1} <br>{+   [23:00]} : CV_L <br>{Case PW's Parameter } <br>{+   [25:16]} : NE_ES <br>{+   [09:00]} : FE_ES <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : TX_NOB <br>{Case EC1} <br>{+   [23:00]} : CV_S| `R_O`| *n/a*| *n/a*|

###FMPM Hold01 Data Of Read DDR

* **Description**           

Hold01 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold01_Data_Of_Read_DDR`

* **Address**             : `0x00021`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold01`| Missing bit is unused<br>{Case STS} <br>{+   [29:20]} : ES_P <br>{+   [19:00]} : PPJC_PDet <br>{Case VT} <br>{+   [29:20]} : ES_V <br>{+   [14:00]} : PPJC_VDet <br>{Case DS3} <br>{+   [29:20]} : ES_L <br>{+   [19:10]} : SES_L <br>{+   [09:00]} : LOSS_L <br>{Case DS1} <br>{+   [19:10]} : ES_L <br>{+   [09:00]} : SES_L <br>{Case PW's Parameter} <br>{+   [25:16]} : NE_SES <br>{+   [09:00]} : FE_SES <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : TX_LBIT <br>{Case EC1} <br>{+   [29:20]} : SEFS_S <br>{+   [19:10]} : ES_S <br>{+   [09:00]} : SES_S| `R_O`| *n/a*| *n/a*|

###FMPM Hold02 Data Of Read DDR

* **Description**           

Hold02 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold02_Data_Of_Read_DDR`

* **Address**             : `0x00022`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold02`| Missing bit is unused<br>{Case STS} <br>{+   [29:20]} : SES_P <br>{+   [19:00]} : NPJC_PDet <br>{Case VT} <br>{+   [29:20]} : SES_V <br>{+   [14:00]} : NPJC_VDet <br>{Case DS3} <br>{+   [19:10]} : ESA_L <br>{+   [09:00]} : ESB_L <br>{Case DS1} <br>{+   [19:10]} : LOSS_L <br>{+   [09:00]} : ES_LFE <br>{Case PW's Parameter} <br>{+   [25:16]} : NE_UAS <br>{+   [09:00]} : FE_UAS <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : TX_NBIT <br>{Case EC1 } <br>{+   [23:00]} : CV_L| `R_O`| *n/a*| *n/a*|

###FMPM Hold03 Data Of Read DDR

* **Description**           

Hold03 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold03_Data_Of_Read_DDR`

* **Address**             : `0x00023`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold03`| Missing bit is unused<br>{Case STS} <br>{+   [29:20]} : UAS_P <br>{+   [19:00]} : PPJC_PGEN <br>{Case VT} <br>{+   [29:20]} : UAS_V <br>{+   [14:00]} : PPJC_VGEN <br>{Case DS3} <br>{+   [23:00]} : CVP_P <br>{Case DS1} <br>{+   [23:00]} : CV_P <br>{Case PW's Parameter} <br>{+   [31:31]} : near-end UAS current status, 1 is unavailable time <br>{+   [30:30]} : near-end UAS switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : near-end UAS positive/negative and SES negative adjustment register use for SW adjust previous page <br>{+   [11:08]} : near-end UAS positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : TX_PBIT <br>{Case EC1} <br>{+   [29:20]} : ES-L <br>{+   [19:10]} : SES_L <br>{+   [09:00]} : FC_L| `R_O`| *n/a*| *n/a*|

###FMPM Hold04 Data Of Read DDR

* **Description**           

Hold04 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold04_Data_Of_Read_DDR`

* **Address**             : `0x00024`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold04`| Missing bit is unused<br>{Case STS} <br>{+   [22:16]} : FC_P <br>{+   [09:00]} : FC_PFE <br>{Case VT} <br>{+   [22:16]} : FC_V <br>{+   [09:00]} : FC_VFE <br>{Case DS3} <br>{+   [29:20]} : ESP_P <br>{+   [19:10]} : ESAP_P <br>{+   [09:00]} : ESBP_P <br>{Case DS1} <br>{+   [29:20]} : ES_P <br>{+   [19:10]} : SES_P <br>{+   [09:00]} : AISS_P <br>{Case PW's Parameter} <br>{+   [31:31]} : far-end UAS current status, 1 is unavailable time <br>{+   [30:30]} : far-end UAS switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : far-end UAS positive/negative and SES negative adjustment register use for SW adjust previous page <br>{+   [11:08]} : far-end UAS positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : TX_RBIT <br>{Case EC1 } <br>{+   [23:00]} : CV_LFE| `R_O`| *n/a*| *n/a*|

###FMPM Hold05 Data Of Read DDR

* **Description**           

Hold05 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold05_Data_Of_Read_DDR`

* **Address**             : `0x00025`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold05`| Missing bit is unused<br>{Case STS} <br>{+   [29:20]} : PJCS_PDet <br>{+   [19:00]} : NPJC_PGEN <br>{Case VT} <br>{+   [29:20]} : PJCS_VDET <br>{+   [14:00]} : NPJC_VGEN <br>{Case DS3} <br>{+   [29:20]} : SESP_P <br>{+   [19:10]} : SAS_P <br>{+   [09:00]} : AISS_P <br>{Case DS1} <br>{+   [29:20]} : SAS_P <br>{+   [19:10]} : CSS_P <br>{+   [09:00]} : UAS_P <br>{Case PW's Parameter} <br>{+   [27:24]} : ES-P positive/negative adjustment <br>{+   [19:16]} : ES-PFE positive/negative adjustment <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_NOB <br>{Case EC1} <br>{+   [29:20]} : ES-LFE <br>{+   [19:10]} : SES_LFE <br>{+   [09:00]} : FC_LFE| `R_O`| *n/a*| *n/a*|

###FMPM Hold06 Data Of Read DDR

* **Description**           

Hold06 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold06_Data_Of_Read_DDR`

* **Address**             : `0x00026`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold06`| Missing bit is unused<br>{Case STS} <br>{+   [31:12]} : PJCDIFF_P <br>{+   [09:00]} : PJCS_PGen <br>{Case VT} <br>{+   [26:12]} : PJCDIFF_V <br>{+   [09:00]} : PJCS_VGEN <br>{Case DS3} <br>{+   [29:20]} : UAS_P <br>{+   [19:10]} : FC_P <br>{+   [09:00]} : FCCP_PFE <br>{Case DS1} <br>{+   [19:10]} : FC_PFE <br>{+   [09:00]} : FC_P <br>{Case PW's Parameter} <br>{+   [09:00]} : FC_P <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_LBIT <br>{Case EC1} <br>{+   [31:31]} : near-end UAS-L current status, 1 is unavailable time <br>{+   [30:30]} : near-end UAS-L switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : near-end UAS-L positive/negative and SES negative adjustment register use for SW adjust previous page <br>{+   [11:08]} : near-end UAS-L positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state| `R_O`| *n/a*| *n/a*|

###FMPM Hold07 Data Of Read DDR

* **Description**           

Hold07 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold07_Data_Of_Read_DDR`

* **Address**             : `0x00027`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold07`| Missing bit is unused<br>{Case STS} <br>{+   [23:00]} : CV_PFE <br>{Case VT} <br>{+   [23:00]} : CV_VFE <br>{Case DS3} <br>{+   [23:00]} : CVCP_P <br>{Case DS1} <br>{+   [19:10]} : SEFS_PFE <br>{+   [09:00]} : ES_PFE <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_STRAY <br>{Case EC1} <br>{+   [31:31]} : far-end UAS-L current status, 1 is unavailable time <br>{+   [30:30]} : far-end UAS-L switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : far-end UAS-L positive/negative and SES negative adjustment register use for SW adjust previous page <br>{+   [11:08]} : far-end UAS-L positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state| `R_O`| *n/a*| *n/a*|

###FMPM Hold08 Data Of Read DDR

* **Description**           

Hold08 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold08_Data_Of_Read_DDR`

* **Address**             : `0x00028`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold08`| Missing bit is unused<br>{Case STS} <br>{+   [29:20]} : ES_PFE <br>{+   [19:10]} : SES_PFE <br>{+   [09:00]} : UAS_PFE <br>{Case VT} <br>{+   [29:20]} : ES_VFE <br>{+   [19:10]} : SES_VFE <br>{+   [09:00]} : UAS_VFE <br>{Case DS3} <br>{+   [29:20]} : ESCP_P <br>{+   [19:10]} : ESACP_P <br>{+   [09:00]} : ESBCP_P <br>{Case DS1} <br>{+   [29:20]} : SES_PFE <br>{+   [19:10]} : CSS_PFE <br>{+   [09:00]} : UAS_PFE <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_MALF <br>{Case EC1} <br>{+   [27:24]} : CV-L positive/negative adjustment <br>{+   [19:16]} : ES-L positive/negative adjustment <br>{+   [11:08]} : CV-LFE positive/negative adjustment <br>{+   [03:00]} : ES-LFE positive/negative adjustment| `R_O`| *n/a*| *n/a*|

###FMPM Hold09 Data Of Read DDR

* **Description**           

Hold09 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold09_Data_Of_Read_DDR`

* **Address**             : `0x00029`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold09`| Missing bit is unused<br>{Case STS} <br>{+   [31:31]} : near-end UAS current status of UAS, 1 is unavailable time <br>{+   [30:30]} : near-end UAS switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : near-end UAS positive/negative and negative SES adjustment register use for SW adjust previous page <br>{+   [11:08]} : near-end UAS positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state <br>{Case VT} <br>{+   [31:31]} : near-end UAS current status of UAS, 1 is unavailable time <br>{+   [30:30]} : near-end UAS switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : near-end UAS positive/negative and SES negative adjustment register use for SW adjust previous page <br>{+   [11:08]} : near-end UAS positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state <br>{Case DS3} <br>{+   [19:10]} : SESCP_P <br>{+   [09:00]} : UASCP_P <br>{Case DS1} <br>{+   [31:00]} : FECNT <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_RBIT <br>{Case EC1} <br>{+   [04:04]} : invalid EC1-LFE , 1 is invalid| `R_O`| *n/a*| *n/a*|

###FMPM Hold10 Data Of Read DDR

* **Description**           

Hold10 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold10_Data_Of_Read_DDR`

* **Address**             : `0x0002A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold10`| Missing bit is unused<br>{Case STS} <br>{+   [31:31]} : far-end UAS current status of UAS, 1 is unavailable time <br>{+   [30:30]} : far-end UAS switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : far-end UAS positive/negative and SES negative adjustment register use for SW adjust previous page <br>{+   [11:08]} : far-end UAS positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state <br>{Case VT} <br>{+   [31:31]} : far-end UAS current status of UAS, 1 is unavailable time <br>{+   [30:30]} : far-end UAS switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : far-end UAS positive/negative and SES negative adjustment register use for SW adjust previous page <br>{+   [11:08]} : far-end UAS positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state <br>{Case DS3} <br>{+   [23:00]} : CVCP_PFE <br>{Case DS1} <br>{+   [23:00]} : REICNT <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_NBIT <br>{Case EC1} <br>{+   [25:16]} : UAS-L <br>{+   [09:00]} : UAS-LFE| `R_O`| *n/a*| *n/a*|

###FMPM Hold11 Data Of Read DDR

* **Description**           

Hold11 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold11_Data_Of_Read_DDR`

* **Address**             : `0x0002B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold11`| Missing bit is unused<br>{Case STS} <br>{+   [27:24]} : CV-P positive/negative adjustment <br>{+   [19:16]} : ES-P positive/negative adjustment <br>{+   [11:08]} : CV-PFE positive/negative adjustment <br>{+   [03:00]} : ES-PFE positive/negative adjustment <br>{Case VT} <br>{+   [27:24]} : CV-P positive/negative adjustment <br>{+   [19:16]} : ES-P positive/negative adjustment <br>{+   [11:08]} : CV-PFE positive/negative adjustment <br>{+   [03:00]} : ES-PFE positive/negative adjustment <br>{Case DS3} <br>{+   [29:20]} : ESCP_PFE <br>{+   [19:10]} : ESACP_PFE <br>{+   [09:00]} : ESBCP_PFE <br>{Case DS1} <br>{+   [31:31]} : near-end UAS current status of UAS, 1 is unavailable time <br>{+   [30:30]} : near-end UAS switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : near-end UAS positive/negative and SES negative adjustment register use for SW adjust previous page <br>{+   [11:08]} : near-end UAS positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_PBIT| `R_O`| *n/a*| *n/a*|

###FMPM Hold12 Data Of Read DDR

* **Description**           

Hold12 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold12_Data_Of_Read_DDR`

* **Address**             : `0x0002C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold12`| Missing bit is unused<br>{Case STS} <br>{+   [04:04]} : invalid STS-PFE , 1 is invalid <br>{Case VT} <br>{+   [04:04]} : invalid VT-PFE , 1 is invalid <br>{Case DS3} <br>{+   [29:20]} : SESCP_PFE <br>{+   [19:10]} : SASCP_PFE <br>{+   [09:00]} : UASCP_PFE <br>{Case DS1} <br>{+   [31:31]} : far-end UAS current status of UAS, 1 is unavailable time <br>{+   [30:30]} : far-end UAS switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : far-end UAS positive/negative and SES negative adjustment register use for SW adjust previous page <br>{+   [11:08]} : far-end UAS positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_LATE| `R_O`| *n/a*| *n/a*|

###FMPM Hold13 Data Of Read DDR

* **Description**           

Hold13 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold13_Data_Of_Read_DDR`

* **Address**             : `0x0002D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold13`| Missing bit is unused<br>{Case DS3} <br>{+   [23:00]} : FECNT <br>{Case DS1} <br>{+   [15:00]} : CV-P positive/negative adjustment <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_EARLY| `R_O`| *n/a*| *n/a*|

###FMPM Hold14 Data Of Read DDR

* **Description**           

Hold14 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold14_Data_Of_Read_DDR`

* **Address**             : `0x0002E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold14`| Missing bit is unused<br>{Case DS3} <br>{+   [31:31]} : near-end UASP current status of UAS, 1 is unavailable time <br>{+   [30:30]} : near-end UASP switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : near-end UASP positive/negative and SESP negative adjustment register use for SW adjust previous page <br>{+   [11:08]} : near-end UASP positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state <br>{Case DS1} <br>{+   [27:24]} : ES-P positive/negative adjustment <br>{+   [19:16]} : AISS-P positive/negative adjustment <br>{+   [11:08]} : SAS-P positive/negative adjustment <br>{+   [03:00]} : CSS-P positive/negative adjustment <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_LOST| `R_O`| *n/a*| *n/a*|

###FMPM Hold15 Data Of Read DDR

* **Description**           

Hold15 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold15_Data_Of_Read_DDR`

* **Address**             : `0x0002F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold15`| Missing bit is unused<br>{Case DS3} <br>{+   [31:31]} : near-end UASCP current status of UAS, 1 is unavailable time <br>{+   [30:30]} : near-end UASCP switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : near-end UASCP positive/negative and SESCP negative adjustment register use for SW adjust previous page <br>{+   [11:08]} : near-end UASCP positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state <br>{Case DS1} <br>{+   [19:16]} : SEFS-PFE positive/negative adjustment <br>{+   [11:08]} : ES-PFE positive/negative adjustment <br>{+   [03:00]} : CSS-PFE positive/negative adjustment <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_LOPSYN| `R_O`| *n/a*| *n/a*|

###FMPM Hold16 Data Of Read DDR

* **Description**           

Hold16 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold16_Data_Of_Read_DDR`

* **Address**             : `0x00030`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold16`| Missing bit is unused<br>{Case DS3} <br>{+   [31:31]} : far-end UASCP-PFE current status of UAS, 1 is unavailable time <br>{+   [30:30]} : far-end UASCP-PFE switch page status, this bit will be set 1 when switch new page and will be cleared at the first entering or exiting UAS state <br>{+   [19:16]} : far-end UASCP-PFE positive/negative and SESCP-PFE negative adjustment register use for SW adjust previous page <br>{+   [11:08]} : far-end UASCP-PFE positive/negative adjustment register after switching page, this register will be latched at the first entering or exiting UAS state <br>{Case DS1} <br>{+   [04:04]} : invalid Line-FE , 1 is invalid <br>{+   [03:03]} : invalid Path-FE , 1 is invalid <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_LOPSTA| `R_O`| *n/a*| *n/a*|

###FMPM Hold17 Data Of Read DDR

* **Description**           

Hold17 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold17_Data_Of_Read_DDR`

* **Address**             : `0x00031`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold17`| Missing bit is unused<br>{Case DS3} <br>{+   [19:00]} : CV-P positive/negative adjustment <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_OVERUN| `R_O`| *n/a*| *n/a*|

###FMPM Hold18 Data Of Read DDR

* **Description**           

Hold18 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold18_Data_Of_Read_DDR`

* **Address**             : `0x00032`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold18`| Missing bit is unused<br>{Case DS3} <br>{+   [19:00]} : CVCP-P positive/negative adjustment <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_UNDERUN| `R_O`| *n/a*| *n/a*|

###FMPM Hold19 Data Of Read DDR

* **Description**           

Hold19 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold19_Data_Of_Read_DDR`

* **Address**             : `0x00033`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold19`| Missing bit is unused<br>{Case DS3} <br>{+   [27:24]} : ESP_P positive/negative adjustment <br>{+   [19:16]} : ESCP_P positive/negative adjustment <br>{+   [11:08]} : ESAP_P positive/negative adjustment <br>{+   [03:00]} : ESACP_P positive/negative adjustment <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : TX_PACKET| `R_O`| *n/a*| *n/a*|

###FMPM Hold20 Data Of Read DDR

* **Description**           

Hold20 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold20_Data_Of_Read_DDR`

* **Address**             : `0x00034`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold20`| Missing bit is unused<br>{Case DS3} <br>{+   [19:16]} : ESBP_P  positive/negative adjustment <br>{+   [11:08]} : ESBCP_P positive/negative adjustment <br>{+   [03:00]} : SESP_P  positive/negative adjustment <br>{Case PW's Counter Statistic} <br>{+   [31:00]} : RX_PACKET| `R_O`| *n/a*| *n/a*|

###FMPM Hold21 Data Of Read DDR

* **Description**           

Hold21 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold21_Data_Of_Read_DDR`

* **Address**             : `0x00035`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold21`| Missing bit is unused<br>{Case DS3} <br>{+   [27:24]} : SESCP  positive/negative adjustment <br>{+   [19:16]} : SAS_P  positive/negative adjustment <br>{+   [11:08]} : AISS_P positive/negative adjustment| `R_O`| *n/a*| *n/a*|

###FMPM Hold22 Data Of Read DDR

* **Description**           

Hold22 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold22_Data_Of_Read_DDR`

* **Address**             : `0x00036`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold22`| Missing bit is unused<br>{Case DS3} <br>{+   [19:00]} : CVCP_PFE positive/negative adjustment| `R_O`| *n/a*| *n/a*|

###FMPM Hold23 Data Of Read DDR

* **Description**           

Hold23 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold23_Data_Of_Read_DDR`

* **Address**             : `0x00037`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold23`| Missing bit is unused<br>{Case DS3} <br>{+   [27:24]} : ESCP_PFE  positive/negative adjustment <br>{+   [19:16]} : ESACP_PFE positive/negative adjustment <br>{+   [11:08]} : ESBCP_PFE positive/negative adjustment <br>{+   [03:00]} : SASCP_PFE positive/negative adjustment| `R_O`| *n/a*| *n/a*|

###FMPM Hold24 Data Of Read DDR

* **Description**           

Hold24 Data Of Read DDR


* **RTL Instant Name**    : `FMPM_Hold24_Data_Of_Read_DDR`

* **Address**             : `0x00038`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`hold24`| Missing bit is unused<br>{Case DS3} <br>{+   [04:04]} : invalid Path-FE , 1 is invalid| `R_O`| *n/a*| *n/a*|

###FMPM K threshold of Section

* **Description**           

K threshold of Section and Line


* **RTL Instant Name**    : `FMPM_K_threshold_of_Section`

* **Address**             : `0x000D0-0x000D7`

* **Formula**             : `0x000D0+$id`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`line_kval`| K threshold of Line| `R/W`| `0x0`| `0x0`|
|`[15:00]`|`section_kval`| K threshold of Section| `R/W`| `0x0`| `0x0`|

###FMPM K threshold of STS

* **Description**           

K threshold of STS


* **RTL Instant Name**    : `FMPM_K_threshold_of_STS`

* **Address**             : `0x00080-0x00087`

* **Formula**             : `0x00080+$id`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`kval`| K threshold of STS| `R/W`| `0x0`| `0x0`|

###FMPM K Threshold of VT

* **Description**           




* **RTL Instant Name**    : `FMPM_K_Threshold_of_VT`

* **Address**             : `0x00090-0x00097`

* **Formula**             : `0x00090+$id`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`kval`| K threshold of VT| `R/W`| `0x0`| `0x0`|

###FMPM K Threshold of DS3

* **Description**           

K threshold of DS3


* **RTL Instant Name**    : `FMPM_K_Threshold_of_DS3`

* **Address**             : `0x000A0-0x000A7`

* **Formula**             : `0x000A0+$id`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`line_kval`| K threshold of Line DS3| `R/W`| `0x0`| `0x0`|
|`[15:00]`|`path_kval`| K threshold of Path DS3| `R/W`| `0x0`| `0x0`|

###FMPM K Threshold of DS1

* **Description**           

K threshold of DS1


* **RTL Instant Name**    : `FMPM_K_Threshold_of_DS1`

* **Address**             : `0x000B0-0x000B7`

* **Formula**             : `0x000B0+$id`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`line_kval`| K threshold of Line DS1| `R/W`| `0x0`| `0x0`|
|`[15:00]`|`path_kval`| K threshold of Path DS1| `R/W`| `0x0`| `0x0`|

###FMPM K Threshold of PW

* **Description**           

K threshold of PW


* **RTL Instant Name**    : `FMPM_K_Threshold_of_PW`

* **Address**             : `0x000C0-0x000C7`

* **Formula**             : `0x000C0+$id`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`kval`| K threshold of PW| `R/W`| `0x0`| `0x0`|

###FMPM TCA threshold of Section Part0

* **Description**           

TCA threshold part0 of Section and Line


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_Section_Part0`

* **Address**             : `0xB0200-0xB020E`

* **Formula**             : `0xB0200+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_sec_cv_s`| TCA threshold of CV_S| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of Section Part1

* **Description**           

TCA threshold part1 of Section and Line


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_Section_Part1`

* **Address**             : `0xB0210-0xB021E`

* **Formula**             : `0xB0210+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_sec_sefs_s`| TCA threshold of SEFS_S| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_sec_es_s`| TCA threshold of ES_S| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_sec_ses_s`| TCA threshold of SES_S| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of Section Part2

* **Description**           

TCA threshold part2 of Section and Line


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_Section_Part2`

* **Address**             : `0xB0220-0xB022E`

* **Formula**             : `0xB0220+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_sec_cv_l`| TCA threshold of CV_L| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of Section Part3

* **Description**           

TCA threshold part3 of Section and Line


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_Section_Part3`

* **Address**             : `0xB0230-0xB023E`

* **Formula**             : `0xB0230+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_sec_es_l`| TCA threshold of ES_L| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_sec_ses_l`| TCA threshold of SES_L| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_sec_uas_l`| TCA threshold of UAS_L| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of Section Part4

* **Description**           

TCA threshold part4 of Section and Line


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_Section_Part4`

* **Address**             : `0xB0240-0xB024E`

* **Formula**             : `0xB0240+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_sec_cv_lfe`| TCA threshold of CV_LFE| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of Section Part5

* **Description**           

TCA threshold part5 of Section and Line


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_Section_Part5`

* **Address**             : `0xB0250-0xB025E`

* **Formula**             : `0xB0250+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_sec_fc_l`| TCA threshold of FC_L| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_sec_es_lfe`| TCA threshold of ES_LFE| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_sec_ses_lfe`| TCA threshold of SES_LFE| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of Section Part6

* **Description**           

TCA threshold part6 of Section and Line


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_Section_Part6`

* **Address**             : `0xB0260-0xB026E`

* **Formula**             : `0xB0260+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:10]`|`tca_thres_sec_uas_lfe`| TCA threshold of UAS_LFE| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_sec_fc_lfe`| TCA threshold of FC_LFE| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of HO Part0

* **Description**           

TCA threshold part0 of HO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_HO_Part0`

* **Address**             : `0xB0000-0xB000E`

* **Formula**             : `0xB0000+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_sts_cv_p`| TCA threshold of CV_P| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of HO Part1

* **Description**           

TCA threshold part1 of HO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_HO_Part1`

* **Address**             : `0xB0010-0xB001E`

* **Formula**             : `0xB0010+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_sts_es_p`| TCA threshold of ES_P| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_sts_ses_p`| TCA threshold of SES_P| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_sts_uas_p`| TCA threshold of UAS_P| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of HO Part2

* **Description**           

TCA threshold part2 of HO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_HO_Part2`

* **Address**             : `0xB0020-0xB002E`

* **Formula**             : `0xB0020+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:00]`|`tca_thres_sts_ppjc_pdet`| TCA threshold of PPJC_PDet| `R/W`| `0xFFFFF`| `0xFFFFF`|

###FMPM TCA threshold of HO Part3

* **Description**           

TCA threshold part3 of HO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_HO_Part3`

* **Address**             : `0xB0030-0xB003E`

* **Formula**             : `0xB0030+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:00]`|`tca_thres_sts_npjc_pdet`| TCA threshold of NPJC_PDet| `R/W`| `0xFFFFF`| `0xFFFFF`|

###FMPM TCA threshold of HO Part4

* **Description**           

TCA threshold part4 of HO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_HO_Part4`

* **Address**             : `0xB0040-0xB004E`

* **Formula**             : `0xB0040+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:00]`|`tca_thres_sts_ppjc_pgen`| TCA threshold of PPJC_PGen| `R/W`| `0xFFFFF`| `0xFFFFF`|

###FMPM TCA threshold of HO Part5

* **Description**           

TCA threshold part5 of HO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_HO_Part5`

* **Address**             : `0xB0050-0xB005E`

* **Formula**             : `0xB0050+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:00]`|`tca_thres_sts_npjc_pgen`| TCA threshold of NPJC_PGen| `R/W`| `0xFFFFF`| `0xFFFFF`|

###FMPM TCA threshold of HO Part6

* **Description**           

TCA threshold part6 of HO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_HO_Part6`

* **Address**             : `0xB0060-0xB006E`

* **Formula**             : `0xB0060+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:00]`|`tca_thres_sts_pjcdiff_p`| TCA threshold of PJCDiff_P| `R/W`| `0xFFFFF`| `0xFFFFF`|

###FMPM TCA threshold of HO Part7

* **Description**           

TCA threshold part7 of HO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_HO_Part7`

* **Address**             : `0xB0070-0xB007E`

* **Formula**             : `0xB0070+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:10]`|`tca_thres_sts_pjcs_pdet`| TCA threshold of PJCS_PDet| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_sts_pjcs_pgen`| TCA threshold of PJCS_PGen| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of HO Part8

* **Description**           

TCA threshold part8 of HO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_HO_Part8`

* **Address**             : `0xB0080-0xB008E`

* **Formula**             : `0xB0080+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_sts_cv_pfe`| TCA threshold of CV_PFE| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of HO Part9

* **Description**           

TCA threshold part9 of HO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_HO_Part9`

* **Address**             : `0xB0090-0xB009E`

* **Formula**             : `0xB0090+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_sts_es_pfe`| TCA threshold of ES_PFE| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_sts_ses_pfe`| TCA threshold of SES_PFE| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_sts_uas_pfe`| TCA threshold of USE_PFE| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of HO Part10

* **Description**           

TCA threshold part10 of HO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_HO_Part10`

* **Address**             : `0xB00A0-0xB00AE`

* **Formula**             : `0xB00A0+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:10]`|`tca_thres_sts_fc_p`| TCA threshold of FC_P| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_sts_fc_pfe`| TCA threshold of FC_PFE| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of LO Part0

* **Description**           

TCA threshold part0 of LO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_LO_Part0`

* **Address**             : `0xB0300-0xB030E`

* **Formula**             : `0xB0300+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_vt_cv_v`| TCA threshold of CV_V| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of LO Part1

* **Description**           

TCA threshold part1 of LO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_LO_Part1`

* **Address**             : `0xB0310-0xB031E`

* **Formula**             : `0xB0310+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_vt_es_v`| TCA threshold of ES_V| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_vt_ses_v`| TCA threshold of SES_V| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_vt_uas_v`| TCA threshold of UAS_V| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of LO Part2

* **Description**           

TCA threshold part2 of LO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_LO_Part2`

* **Address**             : `0xB0320-0xB032E`

* **Formula**             : `0xB0320+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[14:00]`|`tca_thres_vt_ppjc_vdet`| TCA threshold of PPJC_VDet| `R/W`| `0x7FFF`| `0x7FFF`|

###FMPM TCA threshold of LO Part3

* **Description**           

TCA threshold part3 of LO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_LO_Part3`

* **Address**             : `0xB0330-0xB033E`

* **Formula**             : `0xB0330+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[14:00]`|`tca_thres_vt_npjc_vdet`| TCA threshold of NPJC_VDet| `R/W`| `0x7FFF`| `0x7FFF`|

###FMPM TCA threshold of LO Part4

* **Description**           

TCA threshold part4 of LO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_LO_Part4`

* **Address**             : `0xB0340-0xB034E`

* **Formula**             : `0xB0340+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[14:00]`|`tca_thres_vt_ppjc_vgen`| TCA threshold of PPJC_VGen| `R/W`| `0x7FFF`| `0x7FFF`|

###FMPM TCA threshold of LO Part5

* **Description**           

TCA threshold part5 of LO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_LO_Part5`

* **Address**             : `0xB0350-0xB035E`

* **Formula**             : `0xB0350+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[14:00]`|`tca_thres_vt_npjc_vgen`| TCA threshold of NPJC_VGen| `R/W`| `0x7FFF`| `0x7FFF`|

###FMPM TCA threshold of LO Part6

* **Description**           

TCA threshold part6 of LO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_LO_Part6`

* **Address**             : `0xB0360-0xB036E`

* **Formula**             : `0xB0360+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[14:00]`|`tca_thres_vt_pjcdiff_v`| TCA threshold of PJCDiff_V| `R/W`| `0x7FFF`| `0x7FFF`|

###FMPM TCA threshold of LO Part7

* **Description**           

TCA threshold part7 of LO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_LO_Part7`

* **Address**             : `0xB0370-0xB037E`

* **Formula**             : `0xB0370+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:10]`|`tca_thres_vt_pjcs_vdet`| TCA threshold of PJCS_VDET| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_vt_pjcs_vgen`| TCA threshold of PJCS_VGEN| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of LO Part8

* **Description**           

TCA threshold part8 of LO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_LO_Part8`

* **Address**             : `0xB0380-0xB038E`

* **Formula**             : `0xB0380+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_vt_cv_pfe`| TCA threshold of CV_VFE| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of LO Part9

* **Description**           

TCA threshold part9 of LO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_LO_Part9`

* **Address**             : `0xB0390-0xB039E`

* **Formula**             : `0xB0390+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_vt_es_vfe`| TCA threshold of ES_VFE| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_vt_ses_vfe`| TCA threshold of SES_VFE| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_vt_uas_vfe`| TCA threshold of USE_VFE| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of LO Part10

* **Description**           

TCA threshold part10 of LO


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_LO_Part10`

* **Address**             : `0xB03A0-0xB03AE`

* **Formula**             : `0xB03A0+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:10]`|`tca_thres_vt_fc_v`| TCA threshold of FC_V| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_vt_fc_vfe`| TCA threshold of FC_VFE| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS3 Part0

* **Description**           

TCA threshold part0 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part0`

* **Address**             : `0xB0100-0xB010E`

* **Formula**             : `0xB0100+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_ds3_cv_l`| TCA threshold of CV_L| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of DS3 Part1

* **Description**           

TCA threshold part1 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part1`

* **Address**             : `0xB0110-0xB011E`

* **Formula**             : `0xB0110+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_ds3_es_l`| TCA threshold of ES_L| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_ds3_esa_l`| TCA threshold of ESA_L| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds3_esb_l`| TCA threshold of ESB_L| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS3 Part2

* **Description**           

TCA threshold part2 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part2`

* **Address**             : `0xB0120-0xB012E`

* **Formula**             : `0xB0120+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:10]`|`tca_thres_ds3_ses_l`| TCA threshold of SES_L| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds3_loss_l`| TCA threshold of LOSS_L| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS3 Part3

* **Description**           

TCA threshold part3 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part3`

* **Address**             : `0xB0130-0xB013E`

* **Formula**             : `0xB0130+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_ds3_cvp_p`| TCA threshold of CVP_P| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of DS3 Part4

* **Description**           

TCA threshold part4 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part4`

* **Address**             : `0xB0140-0xB014E`

* **Formula**             : `0xB0140+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_ds3_cvcp_p`| TCA threshold of CVCP_P| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of DS3 Part5

* **Description**           

TCA threshold part5 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part5`

* **Address**             : `0xB0150-0xB015E`

* **Formula**             : `0xB0150+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_ds3_esp_p`| TCA threshold of ESP_P| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_ds3_escp_p`| TCA threshold of ESCP_P| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds3_esacp_p`| TCA threshold of ESACP_P| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS3 Part6

* **Description**           

TCA threshold part6 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part6`

* **Address**             : `0xB0160-0xB016E`

* **Formula**             : `0xB0160+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_ds3_esacp_p`| TCA threshold of ESACP_P| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_ds3_esbp_p`| TCA threshold of ESBP_P| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds3_esbcp_p`| TCA threshold of ESBCP_P| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS3 Part7

* **Description**           

TCA threshold part7 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part7`

* **Address**             : `0xB0170-0xB017E`

* **Formula**             : `0xB0170+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_ds3_sesp_p`| TCA threshold of SESP_P| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_ds3_sescp_p`| TCA threshold of SESCP_P| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds3_sas_p`| TCA threshold of SAS_P| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS3 Part8

* **Description**           

TCA threshold part8 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part8`

* **Address**             : `0xB0180-0xB018E`

* **Formula**             : `0xB0180+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_ds3_aiss_p`| TCA threshold of AISS_P| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_ds3_uasp_p`| TCA threshold of UASP_P| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds3_uascp_p`| TCA threshold of UASCP_P| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS3 Part9

* **Description**           

TCA threshold part9 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part9`

* **Address**             : `0xB0190-0xB019E`

* **Formula**             : `0xB0190+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_ds3_cvcp_pfe`| TCA threshold of CVCP_PFE| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of DS3 Part10

* **Description**           

TCA threshold part10 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part10`

* **Address**             : `0xB01A0-0xB01AE`

* **Formula**             : `0xB01A0+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_ds3_escp_pfe`| TCA threshold of ESCP_PFE| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_ds3_esacp_pfe`| TCA threshold of ESACP_PFE| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds3_esbcp_pfe`| TCA threshold of ESBCP_PFE| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS3 Part11

* **Description**           

TCA threshold part11 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part11`

* **Address**             : `0xB01B0-0xB01BE`

* **Formula**             : `0xB01B0+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_ds3_sescp_pfe`| TCA threshold of SESCP_PFE| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_ds3_sascp_pfe`| TCA threshold of SASCP_PFE| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds3_uascp_pfe`| TCA threshold of UASCP_PFE| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS3 Part12

* **Description**           

TCA threshold part12 of DS3


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS3_Part12`

* **Address**             : `0xB01C0-0xB01CE`

* **Formula**             : `0xB01C0+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:10]`|`tca_thres_ds3_fc_p`| TCA threshold of FC_P| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds3_fccp_pfe`| TCA threshold of FCCP_PFE| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS1 Part0

* **Description**           

TCA threshold part0 of DS1


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS1_Part0`

* **Address**             : `0xB0400-0xB040E`

* **Formula**             : `0xB0400+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_ds1_cv_l`| TCA threshold of CV_L| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of DS1 Part1

* **Description**           

TCA threshold part1 of DS1


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS1_Part1`

* **Address**             : `0xB0410-0xB041E`

* **Formula**             : `0xB0410+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_ds1es_l`| TCA threshold of ES_L| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_ds1ses_l`| TCA threshold of SES_L| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds1loss_l`| TCA threshold of LOSS_L| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS1 Part2

* **Description**           

TCA threshold part2 of DS1


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS1_Part2`

* **Address**             : `0xB0420-0xB042E`

* **Formula**             : `0xB0420+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`tca_thres_ds1_cv_p`| TCA threshold of CV_P| `R/W`| `0xFFFFFF`| `0xFFFFFF`|

###FMPM TCA threshold of DS1 Part3

* **Description**           

TCA threshold part3 of DS1


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS1_Part3`

* **Address**             : `0xB0430-0xB043E`

* **Formula**             : `0xB0430+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_ds1_es_p`| TCA threshold of ES_P| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_ds1_ses_p`| TCA threshold of SES_P| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds1_aiss_p`| TCA threshold of AISS_P| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS1 Part4

* **Description**           

TCA threshold part4 of DS1


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS1_Part4`

* **Address**             : `0xB0440-0xB044E`

* **Formula**             : `0xB0440+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_ds1_sas_p`| TCA threshold of SAS_P| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_ds1_css_p`| TCA threshold of CSS_P| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds1_uas_p`| TCA threshold of UAS_P| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS1 Part5

* **Description**           

TCA threshold part5 of DS1


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS1_Part5`

* **Address**             : `0xB0450-0xB045E`

* **Formula**             : `0xB0450+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_ds1_fc_p`| TCA threshold of FC_P| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_ds1_es_lfe`| TCA threshold of ES_LFE| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds1_sefs_pfe`| TCA threshold of SEFS_PFE| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS1 Part6

* **Description**           

TCA threshold part6 of DS1


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS1_Part6`

* **Address**             : `0xB0460-0xB046E`

* **Formula**             : `0xB0460+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_ds1_es_pfe`| TCA threshold of ES_PFE| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_ds1_ses_pfe`| TCA threshold of SES_PFE| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds1_css_pfe`| TCA threshold of CSS_PFE| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of DS1 Part7

* **Description**           

TCA threshold part6 of DS1


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_DS1_Part7`

* **Address**             : `0xB0470-0xB047E`

* **Formula**             : `0xB0470+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:10]`|`tca_thres_ds1_fc_pfe`| TCA threshold of FC-PFE| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_ds1_uas_pfe`| TCA threshold of UAS_PFE| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of PW Part0

* **Description**           

TCA threshold part0 of PW


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_PW_Part0`

* **Address**             : `0xB0500-0xB050E`

* **Formula**             : `0xB0500+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_pw_es`| TCA threshold of ES| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_pw_ses`| TCA threshold of SES| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_pw_uas`| TCA threshold of UAS| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of PW Part1

* **Description**           

TCA threshold part1 of PW


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_PW_Part1`

* **Address**             : `0xB0510-0xB051E`

* **Formula**             : `0xB0510+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:20]`|`tca_thres_pw_es_fe`| TCA threshold of ES_FE| `R/W`| `0x3FF`| `0x3FF`|
|`[19:10]`|`tca_thres_pw_ses_fe`| TCA threshold of SES_FE| `R/W`| `0x3FF`| `0x3FF`|
|`[09:00]`|`tca_thres_pw_uas_fe`| TCA threshold of UAS_CNT| `R/W`| `0x3FF`| `0x3FF`|

###FMPM TCA threshold of PW Part2

* **Description**           

TCA threshold part2 of PW


* **RTL Instant Name**    : `FMPM_TCA_threshold_of_PW_Part2`

* **Address**             : `0xB0520-0xB052E`

* **Formula**             : `0xB0520+$id*0x2`

* **Where**               : 

    * `$id(0-7) : Type ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:00]`|`tca_thres_pw_fc`| TCA threshold of FC| `R/W`| `0x3FF`| `0x3FF`|

###FMPM Enable PM Collection Section

* **Description**           

Enable PM Collection Section


* **RTL Instant Name**    : `FMPM_Enable_PM_Collection_Section`

* **Address**             : `0xB1800-0xB19FF`

* **Formula**             : `0xB1800+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-0) : is the TFI-5 line identifier`

    * `$M(0-0) : is 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s  (belong to that TFI-5 line)`

    * `$N(0-15) : is STS-1s in a STS-24 group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`enb_col_sec`| Enable PM Collection Section<br>{1} : enable| `R/W`| `0x1`| `0x1`|

###FMPM Enable PM Collection STS

* **Description**           

Enable PM STS


* **RTL Instant Name**    : `FMPM_Enable_PM_Collection_STS`

* **Address**             : `0xB1000-0xB11FF`

* **Formula**             : `0xB1000+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-7) : is the TFI-5 line identifier`

    * `$M(0-1) : is 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s  (belong to that TFI-5 line)`

    * `$N(0-23) : is STS-1s in a STS-24 group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`enb_col_sts`| Enable PM Collection STS<br>{1} : enable| `R/W`| `0x1`| `0x1`|

###FMPM Enable PM Collection DS3

* **Description**           

Enbale PM DS3


* **RTL Instant Name**    : `FMPM_Enable_PM_Collection_DS3`

* **Address**             : `0xB1400-0xB15FF`

* **Formula**             : `0xB1400+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : DS3/E3 Group`

    * `$H(0-23) : DS3/E3 Framer`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`enb_col_ds3`| Enable PM Collection DS3<br>{1} : enable| `R/W`| `0x1`| `0x1`|

###FMPM Enable PM Collection VT

* **Description**           

Enable PM VT


* **RTL Instant Name**    : `FMPM_Enable_PM_Collection_VT`

* **Address**             : `0xB4000-0xB7FFF`

* **Formula**             : `0xB4000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

    * `$H(0-23) : STS-1 LO group`

    * `$I(0-27) : VT/TU ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`enb_col_vt`| Enable PM Collection VT<br>{1} : enable| `R/W`| `0x1`| `0x1`|

###FMPM Enable PM Collection DS1

* **Description**           

Enable PM DS1


* **RTL Instant Name**    : `FMPM_Enable_PM_Collection_DS1`

* **Address**             : `0xB8000-0xBBFFF`

* **Formula**             : `0xB8000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

    * `$H(0-23) : DS1/E1 Level-2 Group`

    * `$I(0-27) : DS1/E1 Framer`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`enb_col_ds1`| Enable PM Collection DS1<br>{1} : enable| `R/W`| `0x1`| `0x1`|

###FMPM Enable PM Collection PW

* **Description**           

Enable PM PW


* **RTL Instant Name**    : `FMPM_Enable_PM_Collection_PW`

* **Address**             : `0xBC000-0xBFFFF`

* **Formula**             : `0xBC000+$id`

* **Where**               : 

    * `$id(0-10752) : PW ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`enb_col_pw`| Enable PM Collection PW<br>{1} : enable| `R/W`| `0x1`| `0x1`|

###FMPM Threshold Type of Section

* **Description**           

Threshold Type of Section


* **RTL Instant Name**    : `FMPM_Threshold_Type_of_Section`

* **Address**             : `0x00600-0x006FF`

* **Formula**             : `0x00600+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-0) : is the TFI-5 line identifier`

    * `$M(0-0) : is 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s  (belong to that TFI-5 line)`

    * `$N(0-15) : is STS-1s in a STS-24 group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[10:08]`|`k_type`| Use to read K threshold table| `R/W`| `0x0`| `0x0`|
|`[07:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[02:00]`|`tca_type`| Use to read TCA threshold table| `R/W`| `0x0`| `0x0`|

###FMPM Threshold Type of STS

* **Description**           

Threshold Type of STS


* **RTL Instant Name**    : `FMPM_Threshold_Type_of_STS`

* **Address**             : `0x00200-0x003FF`

* **Formula**             : `0x00200+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-7) : is the TFI-5 line identifier`

    * `$M(0-1) : is 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s  (belong to that TFI-5 line)`

    * `$N(0-23) : is STS-1s in a STS-24 group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[10:08]`|`k_type`| Use to read K threshold table| `R/W`| `0x0`| `0x0`|
|`[07:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[02:00]`|`tca_type`| Use to read TCA threshold table| `R/W`| `0x0`| `0x0`|

###FMPM Threshold Type of DS3

* **Description**           

Threshold Type of DS3


* **RTL Instant Name**    : `FMPM_Threshold_Type_of_DS3`

* **Address**             : `0x00400-0x005FF`

* **Formula**             : `0x00400+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : DS3/E3 Group`

    * `$H(0-23) : DS3/E3 Framer`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[10:08]`|`k_type`| Use to read K threshold table| `R/W`| `0x0`| `0x0`|
|`[07:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[02:00]`|`tca_type`| Use to read TCA threshold table| `R/W`| `0x0`| `0x0`|

###FMPM Threshold Type of VT

* **Description**           

Threshold Type of VT


* **RTL Instant Name**    : `FMPM_Threshold_Type_of_VT`

* **Address**             : `0x04000-0x07FFF`

* **Formula**             : `0x04000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

    * `$H(0-23) : STS-1 LO group`

    * `$I(0-27) : VT/TU ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[10:08]`|`k_type`| Use to read K threshold table| `R/W`| `0x0`| `0x0`|
|`[07:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[02:00]`|`tca_type`| Use to read TCA threshold table| `R/W`| `0x0`| `0x0`|

###FMPM Threshold Type of DS1

* **Description**           

Threshold Type of DS1


* **RTL Instant Name**    : `FMPM_Threshold_Type_of_DS1`

* **Address**             : `0x08000-0x0BFFF`

* **Formula**             : `0x08000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

    * `$H(0-23) : DS1/E1 Level-2 Group`

    * `$I(0-27) : DS1/E1 Framer`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[10:08]`|`k_type`| Use to read K threshold table| `R/W`| `0x0`| `0x0`|
|`[07:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[02:00]`|`tca_type`| Use to read TCA threshold table| `R/W`| `0x0`| `0x0`|

###FMPM Threshold Type of PW

* **Description**           

Threshold Type of PW


* **RTL Instant Name**    : `FMPM_Threshold_Type_of_PW`

* **Address**             : `0x0C000-0x0FFFF`

* **Formula**             : `0x0C000+$id`

* **Where**               : 

    * `$id(0-10752) : PW ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[10:08]`|`k_type`| Use to read K threshold table| `R/W`| `0x0`| `0x0`|
|`[07:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[02:00]`|`tca_type`| Use to read TCA threshold table| `R/W`| `0x0`| `0x0`|

###FMPM FM Interrupt

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_Interrupt`

* **Address**             : `0x14000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`fmpm_fm_intr_pw`| PW    Interrupt| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_intr_ds1e1`| DS1E1 interrupt| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_intr_ds3e3`| DS3E3  interrupt| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_intr_vtgtug`| VTGTUGinterrupt| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_intr_stsau`| STSAU interrupt| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_intr_ecstm`| ECSTM interrupt| `R_O`| `0x0`| `0x0`|

###FMPM FM Interrupt Mask

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_Interrupt_Mask`

* **Address**             : `0x14001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`fmpm_fm_intr_msk_pw`| PW    Interrupt| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_intr_msk_ds1e1`| DS1E1 interrupt| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_intr_msk_ds3e3`| DS3E3  interrupt| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_intr_msk_vtgtug`| VTGTUG interrupt| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_intr_msk_stsau`| STSAU interrupt| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_intr_msk_ecstm`| ECSTM interrupt| `R/W`| `0x0`| `0x0`|

###FMPM FM EC1 Interrupt OR

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_EC1_Interrupt_OR`

* **Address**             : `0x18005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:00]`|`fmpm_fm_ec1_or`| Interrupt EC1 OR<br>{+ [00]} : EC1 Interrupt (0, 0) <br>{+ [01]} : EC1 Interrupt (0, 1)| `R_O`| `0x0`| `0x0`|

###FMPM FM EC1 Interrupt OR AND MASK Per STS1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_EC1_Interrupt_OR_AND_MASK_Per_STS1`

* **Address**             : `0x18100-0x1810F`

* **Formula**             : `0x18100+$L*0x2+$M`

* **Where**               : 

    * `$L(0-0) : TFI-5 line identifier`

    * `$M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`fmpm_fm_ec1_oamsk`| Interrupt STS1 OR AND MASK<br>{+ [00]} : EC1STM0 Interrupt (L, M, 00) <br>{+ [01]} : EC1STM0 Interrupt (L, M, 01) <br>{+ [25]} : EC1STM0 Interrupt (L, M, 15)| `R_O`| `0x0`| `0x0`|

###FMPM FM EC1 Interrupt MASK Per STS1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_EC1_Interrupt_MASK_Per_STS1`

* **Address**             : `0x18110-0x1811F`

* **Formula**             : `0x18110+$L*0x2+$M`

* **Where**               : 

    * `$L(0-0) : TFI-5 line identifier`

    * `$M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`fmpm_fm_ec1_msk`| Interrupt MASK per STS1<br>{+ [00]} : EC1STM0 Interrupt (L, M, 00) <br>{+ [01]} : EC1STM0 Interrupt (L, M, 01) <br>{+ [15]} : EC1STM0 Interrupt (L, M, 15)| `R/W`| `0x0`| `0x0`|

###FMPM FM EC1STM0 Interrupt Sticky Per Type Of Per STS1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1`

* **Address**             : `0x1B600-0x1B7FF`

* **Formula**             : `0x1B600+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-0) : TFI-5 line identifier`

    * `$M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

    * `$N(0-15) : STS-1s in a STS-24 group will be numbered from 0 to 23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:07]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[06:06]`|`fmpm_fm_ec1stm0_stk_los_s`| LOS_S| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_ec1stm0_stk_lof_s`| LOF_S| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_ec1stm0_stk_ais_l`| AIS_L| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_ec1stm0_stk_rfi_l`| RFI_L| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_ec1stm0_stk_tim_l`| TIM_L| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_ec1stm0_stk_bersd_l`| BERSD_L| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_ec1stm0_stk_bersf_l`| BERSF_L| `W1C`| `0x0`| `0x0`|

###FMPM FM EC1STM0 Interrupt MASK Per Type Of Per STS1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1`

* **Address**             : `0x1B800-0x1B9FF`

* **Formula**             : `0x1B800+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-0) : TFI-5 line identifier`

    * `$M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

    * `$N(0-15) : STS-1s in a STS-24 group will be numbered from 0 to 23`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:07]`|`fmpm_fm_ec1stm0_hi_ais`| High level AIS| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_fm_ec1stm0_los_s`| LOS_S| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_ec1stm0_lof_s`| LOF_S| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_ec1stm0_ais_l`| AIS_L| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_ec1stm0_rfi_l`| RFI_L| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_ec1stm0_tim_l`| TIM_L| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_ec1stm0_bersd_l`| BERSD_L| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_ec1stm0_bersf_l`| BERSF_L| `R/W`| `0x0`| `0x0`|

###FMPM FM EC1STM0 Interrupt Current Status Per Type Of Per STS1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1`

* **Address**             : `0x1BA00-0x1BBFF`

* **Formula**             : `0x1BA00+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-0) : TFI-5 line identifier`

    * `$M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

    * `$N(0-15) : STS-1s in a STS-24 group will be numbered from 0 to 23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:23]`|`fmpm_fm_ec1stm0_amsk_hi_ais`| High level AIS| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`fmpm_fm_ec1stm0_amsk_los_s`| LOS_S| `R_O`| `0x0`| `0x0`|
|`[21:21]`|`fmpm_fm_ec1stm0_amsk_lof_s`| LOF_S| `R_O`| `0x0`| `0x0`|
|`[20:20]`|`fmpm_fm_ec1stm0_amsk_ais_l`| AIS_L| `R_O`| `0x0`| `0x0`|
|`[19:19]`|`fmpm_fm_ec1stm0_amsk_rfi_l`| RFI_L| `R_O`| `0x0`| `0x0`|
|`[18:18]`|`fmpm_fm_ec1stm0_amsk_tim_l`| TIM_L| `R_O`| `0x0`| `0x0`|
|`[17:17]`|`fmpm_fm_ec1stm0_amsk_bersd_l`| BERSD_L| `R_O`| `0x0`| `0x0`|
|`[16:16]`|`fmpm_fm_ec1stm0_amsk_bersf_l`| BERSF_L| `R_O`| `0x0`| `0x0`|
|`[15:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:07]`|`fmpm_fm_ec1stm0_cur_hi_ais`| High level AIS| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_fm_ec1stm0_cur_los_s`| LOS_S| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_ec1stm0_cur_lof_s`| LOF_S| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_ec1stm0_cur_ais_l`| AIS_L| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_ec1stm0_cur_rfi_l`| RFI_L| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_ec1stm0_cur_tim_l`| TIM_L| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_ec1stm0_cur_bersd_l`| BERSD_L| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_ec1stm0_cur_bersf_l`| BERSF_L| `R_O`| `0x0`| `0x0`|

###FMPM FM STS24 Interrupt OR

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_STS24_Interrupt_OR`

* **Address**             : `0x18000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:00]`|`fmpm_fm_sts24_or`| Interrupt STS24 OR<br>{+ [00]} : STS-24 Interrupt (0, 0) <br>{+ [01]} : STS-24 Interrupt (0, 1)| `R_O`| `0x0`| `0x0`|

###FMPM FM STS24 Interrupt OR AND MASK Per STS1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_STS24_Interrupt_OR_AND_MASK_Per_STS1`

* **Address**             : `0x18080-0x1808F`

* **Formula**             : `0x18080+$L*0x2+$M`

* **Where**               : 

    * `$L(0-7) : TFI-5 line identifier`

    * `$M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`fmpm_fm_sts1_oamsk`| Interrupt STS1 OR AND MASK<br>{+ [00]} : STS-1 Interrupt (L, M, 00) <br>{+ [01]} : STS-1 Interrupt (L, M, 01) <br>{+ [23]} : STS-1 Interrupt (L, M, 23)| `R_O`| `0x0`| `0x0`|

###FMPM FM STS24 Interrupt MASK Per STS1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_STS24_Interrupt_MASK_Per_STS1`

* **Address**             : `0x18090-0x1809F`

* **Formula**             : `0x18090+$L*0x2+$M`

* **Where**               : 

    * `$L(0-7) : TFI-5 line identifier`

    * `$M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`fmpm_fm_sts1_msk`| Interrupt MASK per STS1<br>{+ [00]} : STS-1 Interrupt (L, M, 00) <br>{+ [01]} : STS-1 Interrupt (L, M, 01) <br>{+ [23]} : STS-1 Interrupt (L, M, 23)| `R/W`| `0x0`| `0x0`|

###FMPM FM STS24 Interrupt Sticky Per Type Of Per STS1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1`

* **Address**             : `0x1A000-0x1A1FF`

* **Formula**             : `0x1A000+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-7) : TFI-5 line identifier`

    * `$M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

    * `$N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[11:11]`|`fmpm_fm_sts1_stk_rfi_ser`| RFI server| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_fm_sts1_stk_rfi_con`| RFI connectivity| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_fm_sts1_stk_rfi_pay`| RFI payload| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_fm_sts1_stk_lom`| LOM| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_fm_sts1_stk_rfi`| one-bit RFI| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_fm_sts1_stk_ais`| AIS| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_sts1_stk_tim`| TIM| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_sts1_stk_uneq`| UNEQ| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_sts1_stk_plm`| PLM| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_sts1_stk_lop`| LOP| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_sts1_stk_bersd`| BERSD| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_sts1_stk_bersf`| BERSF| `W1C`| `0x0`| `0x0`|

###FMPM FM STS24 Interrupt MASK Per Type Of Per STS1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1`

* **Address**             : `0x1A200-0x1A3FF`

* **Formula**             : `0x1A200+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-7) : TFI-5 line identifier`

    * `$M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

    * `$N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`fmpm_fm_sts1_msk_hi_ais`| High level AIS| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_fm_sts1_msk_rfi_ser`| RFI server| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_fm_sts1_msk_rfi_con`| RFI connectivity| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_fm_sts1_msk_rfi_pay`| RFI payload| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_fm_sts1_msk_lom`| LOM| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_fm_sts1_msk_rfi`| one-bit RFI| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_fm_sts1_msk_ais`| AIS| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_sts1_msk_tim`| TIM| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_sts1_msk_uneq`| UNEQ| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_sts1_msk_plm`| PLM| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_sts1_msk_lop`| LOP| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_sts1_msk_bersd`| BERSD| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_sts1_msk_bersf`| BERSF| `R/W`| `0x0`| `0x0`|

###FMPM FM STS24 Interrupt Current Status Per Type Of Per STS1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1`

* **Address**             : `0x1A400-0x1A5FF`

* **Formula**             : `0x1A400+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-7) : TFI-5 line identifier`

    * `$M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

    * `$N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`fmpm_fm_sts1_amsk_hi_ais`| High level AIS| `R_O`| `0x0`| `0x0`|
|`[27:27]`|`fmpm_fm_sts1_amsk_rfi_ser`| RFI server| `R_O`| `0x0`| `0x0`|
|`[26:26]`|`fmpm_fm_sts1_amsk_rfi_con`| RFI connectivity| `R_O`| `0x0`| `0x0`|
|`[25:25]`|`fmpm_fm_sts1_amsk_rfi_pay`| RFI payload| `R_O`| `0x0`| `0x0`|
|`[24:24]`|`fmpm_fm_sts1_amsk_lom`| LOM| `R_O`| `0x0`| `0x0`|
|`[23:23]`|`fmpm_fm_sts1_amsk_rfi`| one-bit RFI| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`fmpm_fm_sts1_amsk_ais`| AIS| `R_O`| `0x0`| `0x0`|
|`[21:21]`|`fmpm_fm_sts1_amsk_tim`| TIM| `R_O`| `0x0`| `0x0`|
|`[20:20]`|`fmpm_fm_sts1_amsk_uneq`| UNEQ| `R_O`| `0x0`| `0x0`|
|`[19:19]`|`fmpm_fm_sts1_amsk_plm`| PLM| `R_O`| `0x0`| `0x0`|
|`[18:18]`|`fmpm_fm_sts1_amsk_lop`| LOP| `R_O`| `0x0`| `0x0`|
|`[17:17]`|`fmpm_fm_sts1_amsk_bersd`| BERSD| `R_O`| `0x0`| `0x0`|
|`[16:16]`|`fmpm_fm_sts1_amsk_bersf`| BERSF| `R_O`| `0x0`| `0x0`|
|`[15:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`fmpm_fm_sts1_cur_hi_ais`| High level AIS| `R_O`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_fm_sts1_cur_rfi_ser`| RFI server| `R_O`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_fm_sts1_cur_rfi_con`| RFI connectivity| `R_O`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_fm_sts1_cur_rfi_pay`| RFI payload| `R_O`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_fm_sts1_cur_lom`| LOM| `R_O`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_fm_sts1_cur_rfi`| one-bit RFI| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_fm_sts1_cur_ais`| AIS| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_sts1_cur_tim`| TIM| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_sts1_cur_uneq`| UNEQ| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_sts1_cur_plm`| PLM| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_sts1_cur_lop`| LOP| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_sts1_cur_bersd`| BERSD| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_sts1_cur_bersf`| BERSF| `R_O`| `0x0`| `0x0`|

###FMPM FM DS3E3 Group Interrupt OR

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS3E3_Group_Interrupt_OR`

* **Address**             : `0x18001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`fmpm_fm_ds3e3_or`| Interrupt DS3/E3 OR<br>{+ [00]} : DS3/E3 Group Interrupt (00)| `R_O`| `0x0`| `0x0`|

###FMPM FM DS3E3 Framer Interrupt OR AND MASK Per DS3E3

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3`

* **Address**             : `0x180A0-0x180AF`

* **Formula**             : `0x180A0+$G`

* **Where**               : 

    * `$G(0-15) : DS3/E3 Group  Interrupt`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`fmpm_fm_ds3e3_oamsk`| Interrupt DS3/E3 Group<br>{+ [00]} : DS3/E3 Framer Interrupt (G, 00) <br>{+ [01]} : DS3/E3 Framer Interrupt (G, 01) <br>{+ [23]} : DS3/E3 Framer Interrupt (G, 23)| `R_O`| `0x0`| `0x0`|

###FMPM FM DS3E3 Framer Interrupt MASK Per DS3E3

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_DS3E3`

* **Address**             : `0x180B0-0x180BF`

* **Formula**             : `0x180B0+$G`

* **Where**               : 

    * `$G(0-15) : DS3/E3 Group  Interrupt`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`fmpm_fm_ds3e3_msk`| MASK per DS3/E3 Framer<br>{+ [00]} : DS3/E3 Framer Interrupt (G, 00) <br>{+ [01]} : DS3/E3 Framer Interrupt (G, 01) <br>{+ [23]} : DS3/E3 Framer Interrupt (G, 23)| `R/W`| `0x0`| `0x0`|

###FMPM FM DS3E3 Framer Interrupt Sticky Per Type of Per DS3E3

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3`

* **Address**             : `0x1A600-0x1A7FF`

* **Formula**             : `0x1A600+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : DS3/E3 Group  Interrupt`

    * `$H(0-23) : DS3/E3 Group  Interrupt`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:04]`|`fmpm_fm_ds3e3_stk_rai`| RAI| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_ds3e3_stk_ais`| AIS| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_ds3e3_stk_sef`| SEF| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_ds3e3_stk_lof`| LOF| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_ds3e3_stk_los`| LOS| `W1C`| `0x0`| `0x0`|

###FMPM FM DS3E3 Framer Interrupt MASK Per Type of Per DS3E3

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3`

* **Address**             : `0x1A800-0x1A9FF`

* **Formula**             : `0x1A800+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : DS3/E3 Group  Interrupt`

    * `$H(0-23) : DS3/E3 Group  Interrupt`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`fmpm_fm_ds3e3_msk_hi_ais`| High level AIS| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_ds3e3_msk_rai`| RAI| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_ds3e3_msk_ais`| AIS| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_ds3e3_msk_sef`| SEF| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_ds3e3_msk_lof`| LOF| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_ds3e3_msk_los`| LOS| `R/W`| `0x0`| `0x0`|

###FMPM FM DS3E3 Framer Interrupt Current Status Per Type of Per DS3E3

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3`

* **Address**             : `0x1AA00-0x1ABFF`

* **Formula**             : `0x1AA00+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : DS3/E3 Group  Interrupt`

    * `$H(0-23) : DS3/E3 Group  Interrupt`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:22]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[21:21]`|`fmpm_fm_ds3e3_amsk_hi_ais`| High level AIS| `R_O`| `0x0`| `0x0`|
|`[20:20]`|`fmpm_fm_ds3e3_amsk_rai`| RAI| `R_O`| `0x0`| `0x0`|
|`[19:19]`|`fmpm_fm_ds3e3_amsk_ais`| AIS| `R_O`| `0x0`| `0x0`|
|`[18:18]`|`fmpm_fm_ds3e3_amsk_sef`| SEF| `R_O`| `0x0`| `0x0`|
|`[17:17]`|`fmpm_fm_ds3e3_amsk_lof`| LOF| `R_O`| `0x0`| `0x0`|
|`[16:16]`|`fmpm_fm_ds3e3_amsk_los`| LOS| `R_O`| `0x0`| `0x0`|
|`[15:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`fmpm_fm_ds3e3_cur_hi_ais`| High level AIS| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_ds3e3_cur_rai`| RAI| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_ds3e3_cur_ais`| AIS| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_ds3e3_cur_sef`| SEF| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_ds3e3_cur_lof`| LOF| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_ds3e3_cur_los`| LOS| `R_O`| `0x0`| `0x0`|

###FMPM FM STS24 LO Interrupt OR

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_STS24_LO_Interrupt_OR`

* **Address**             : `0x18002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`fmpm_fm_sts24lo_or`| Interrupt STS24 LO OR<br>{+ [00]} : STS-24 LO Interrupt (00)| `R_O`| `0x0`| `0x0`|

###FMPM FM STS1 LO Interrupt OR

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_STS1_LO_Interrupt_OR`

* **Address**             : `0x180C0-0x180CF`

* **Formula**             : `0x180C0+$G`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`fmpm_fm_sts1lo_or`| Interrupt STS1 LO OR<br>{+ [00]} : STS-1 LO Interrupt (G, 00) <br>{+ [01]} : STS-1 LO Interrupt (G, 01) <br>{+ [23]} : STS-1 LO Interrupt (G, 23)| `R_O`| `0x0`| `0x0`|

###FMPM FM VTTU Interrupt OR AND MASK Per VTTU

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_VTTU_Interrupt_OR_AND_MASK_Per_VTTU`

* **Address**             : `0x1AC00-0x1ADFF`

* **Formula**             : `0x1AC00+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

    * `$H(0-23) : STS-1 LO group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[27:00]`|`fmpm_fm_vttu_oamsk`| VT/TU Interrupt<br>{+ [00]} : VT/TU Interrupt (G,H, 00) <br>{+ [01]} : VT/TU Interrupt (G,H, 01) <br>{+ [27]} : VT/TU Interrupt (G,H, 27)| `R_O`| `0x0`| `0x0`|

###FMPM FM VTTU Interrupt MASK Per VTTU

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_VTTU_Interrupt_MASK_Per_VTTU`

* **Address**             : `0x1AE00-0x1AFFF`

* **Formula**             : `0x1AE00+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

    * `$H(0-23) : STS-1 LO group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[27:00]`|`fmpm_fm_vttu_msk`| VT/TU Interrupt<br>{+ [00]} : VT/TU Interrupt (G,H, 00) <br>{+ [01]} : VT/TU Interrupt (G,H, 01) <br>{+ [27]} : VT/TU Interrupt (G,H, 27)| `R/W`| `0x0`| `0x0`|

###FMPM FM VTTU Interrupt Sticky Per Type Per VTTU

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU`

* **Address**             : `0x24000-0x27FFF`

* **Formula**             : `0x24000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

    * `$H(0-23) : STS-1 LO group`

    * `$I(0-27) : VT/TU`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[10:10]`|`fmpm_fm_vttu_stk_rfi_ser`| RFI server| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_fm_vttu_stk_rfi_con`| RFI connectivity| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_fm_vttu_stk_rfi_pay`| RFI payload| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_fm_vttu_stk_rfi`| one-bit RFI| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_fm_vttu_stk_ais`| AIS| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_vttu_stk_tim`| TIM| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_vttu_stk_uneq`| UNEQ| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_vttu_stk_plm`| PLM| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_vttu_stk_lop`| LOP| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_vttu_stk_bersd`| BERSD| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_vttu_stk_bersf`| BERSF| `W1C`| `0x0`| `0x0`|

###FMPM FM VTTU Interrupt MASK Per Type Per VTTU

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_VTTU_Interrupt_MASK_Per_Type_Per_VTTU`

* **Address**             : `0x28000-0x2BFFF`

* **Formula**             : `0x28000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

    * `$H(0-23) : STS-1 LO group`

    * `$I(0-27) : VT/TU`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[11:11]`|`fmpm_fm_vttu_msk_hi_ais`| High level AIS| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_fm_vttu_msk_rfi_ser`| RFI server| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_fm_vttu_msk_rfi_con`| RFI connectivity| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_fm_vttu_msk_rfi_pay`| RFI payload| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_fm_vttu_msk_rfi`| one-bit RFI| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_fm_vttu_msk_ais`| AIS| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_vttu_msk_tim`| TIM| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_vttu_msk_uneq`| UNEQ| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_vttu_msk_plm`| PLM| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_vttu_msk_lop`| LOP| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_vttu_msk_bersd`| BERSD| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_vttu_msk_bersf`| BERSF| `R/W`| `0x0`| `0x0`|

###FMPM FM VTTU Interrupt Current Status Per Type Per VTTU

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU`

* **Address**             : `0x2C000-0x2FFFF`

* **Formula**             : `0x2C000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

    * `$H(0-23) : STS-1 LO group`

    * `$I(0-27) : VT/TU`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[27:27]`|`fmpm_fm_vttu_amsk_hi_ais`| High level AIS| `R_O`| `0x0`| `0x0`|
|`[26:26]`|`fmpm_fm_vttu_amsk_rfi_ser`| RFI server| `R_O`| `0x0`| `0x0`|
|`[25:25]`|`fmpm_fm_vttu_amsk_rfi_con`| RFI connectivity| `R_O`| `0x0`| `0x0`|
|`[24:24]`|`fmpm_fm_vttu_amsk_rfi_pay`| RFI payload| `R_O`| `0x0`| `0x0`|
|`[23:23]`|`fmpm_fm_vttu_amsk_rfi`| one-bit| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`fmpm_fm_vttu_amsk_ais`| AIS| `R_O`| `0x0`| `0x0`|
|`[21:21]`|`fmpm_fm_vttu_amsk_tim`| TIM| `R_O`| `0x0`| `0x0`|
|`[20:20]`|`fmpm_fm_vttu_amsk_uneq`| UNEQ| `R_O`| `0x0`| `0x0`|
|`[19:19]`|`fmpm_fm_vttu_amsk_plm`| PLM| `R_O`| `0x0`| `0x0`|
|`[18:18]`|`fmpm_fm_vttu_amsk_lop`| LOP| `R_O`| `0x0`| `0x0`|
|`[17:17]`|`fmpm_fm_vttu_amsk_bersd`| BERSD| `R_O`| `0x0`| `0x0`|
|`[16:16]`|`fmpm_fm_vttu_amsk_bersf`| BERSF| `R_O`| `0x0`| `0x0`|
|`[15:12]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[11:11]`|`fmpm_fm_vttu_cur_hi_ais`| High level AIS| `R_O`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_fm_vttu_cur_rfi_ser`| RFI server| `R_O`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_fm_vttu_cur_rfi_con`| RFI connectivity| `R_O`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_fm_vttu_cur_rfi_pay`| RFI payload| `R_O`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_fm_vttu_cur_rfi`| one-bit| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_fm_vttu_cur_ais`| AIS| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_vttu_cur_tim`| TIM| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_vttu_cur_uneq`| UNEQ| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_vttu_cur_plm`| PLM| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_vttu_cur_lop`| LOP| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_vttu_cur_bersd`| BERSD| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_vttu_cur_bersf`| BERSF| `R_O`| `0x0`| `0x0`|

###FMPM FM DS1E1 Level1 Group Interrupt OR

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS1E1_Level1_Group_Interrupt_OR`

* **Address**             : `0x18003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`fmpm_fm_ds1e1_lev1_or`| Interrupt DS1E1 level1 group OR<br>{+ [00]} : DS1/E1 Level-1 Group Interrupt (00)| `R_O`| `0x0`| `0x0`|

###FMPM FM DS1E1 Level2 Group Interrupt OR

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS1E1_Level2_Group_Interrupt_OR`

* **Address**             : `0x180D0-0x180DF`

* **Formula**             : `0x180D0+$G`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`fmpm_fm_ds1e1_lev2_or`| Level2 Group Interrupt OR<br>{+ [00]} : DS1/E1 Level-2 Group Interrupt (G, 00) <br>{+ [01]} : DS1/E1 Level-2 Group Interrupt (G, 01) <br>{+ [23]} : DS1/E1 Level-2 Group Interrupt (G, 23)| `R_O`| `0x0`| `0x0`|

###FMPM FM DS1E1 Framer Interrupt OR AND MASK Per DS1E1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1`

* **Address**             : `0x1B000-0x1B1FF`

* **Formula**             : `0x1B000+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

    * `$H(0-23) : DS1/E1 Level-2 Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[27:00]`|`fmpm_fm_ds1e1_oamsk`| DS1/E1 Framer Interrupt<br>{+ [00]} : DS1/E1 Framer Interrupt (G,H, 00) <br>{+ [01]} : DS1/E1 Framer Interrupt (G,H, 01) <br>{+ [27]} : DS1/E1 Framer Interrupt (G,H, 27)| `R_O`| `0x0`| `0x0`|

###FMPM FM DS1E1 Framer Interrupt MASK Per DS1E1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS1E1_Framer_Interrupt_MASK_Per_DS1E1`

* **Address**             : `0x1B200-0x1B3FF`

* **Formula**             : `0x1B200+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

    * `$H(0-23) : DS1/E1 Level-2 Group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[27:00]`|`fmpm_fm_ds1e1_msk`| DS1/E1 Framer Interrupt<br>{+ [00]} : DS1/E1 Framer Interrupt (G,H, 00) <br>{+ [01]} : DS1/E1 Framer Interrupt (G,H, 01) <br>{+ [27]} : DS1/E1 Framer Interrupt (G,H, 27)| `R/W`| `0x0`| `0x0`|

###FMPM FM DS1E1 Interrupt Sticky Per Type Per DS1E1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1`

* **Address**             : `0x30000-0x33FFF`

* **Formula**             : `0x30000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

    * `$H(0-23) : DS1/E1 Level-2 Group`

    * `$I(0-27) : DS1/E1`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`fmpm_fm_ds1e1_stk_rai_ci`| RAI_CI| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_ds1e1_stk_rai`| RAI| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_ds1e1_stk_lof`| LOF| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_ds1e1_stk_ais_ci`| AIS_CI| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_ds1e1_stk_ais`| AIS| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_ds1e1_stk_los`| LOS| `W1C`| `0x0`| `0x0`|

###FMPM FM DS1E1 Interrupt MASK Per Type Per DS1E1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1`

* **Address**             : `0x34000-0x37FFF`

* **Formula**             : `0x34000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

    * `$H(0-23) : DS1/E1 Level-2 Group`

    * `$I(0-27) : DS1/E1`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:07]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[06:06]`|`fmpm_fm_ds1e1_msk_hi_ais`| High level AIS| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_ds1e1_msk_rai_ci`| RAI_CI| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_ds1e1_msk_rai`| RAI| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_ds1e1_msk_lof`| LOF| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_ds1e1_msk_ais_ci`| AIS_CI| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_ds1e1_msk_ais`| AIS| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_ds1e1_msk_los`| LOS| `R/W`| `0x0`| `0x0`|

###FMPM FM DS1E1 Interrupt Current Status Per Type Per DS1E1

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1`

* **Address**             : `0x38000-0x3BFFF`

* **Formula**             : `0x38000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

    * `$H(0-23) : DS1/E1 Level-2 Group`

    * `$I(0-27) : DS1/E1`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[22:22]`|`fmpm_fm_ds1e1_amsk_hi_ais`| High level AIS| `R_O`| `0x0`| `0x0`|
|`[21:21]`|`fmpm_fm_ds1e1_amsk_rai_ci`| RAI_CI| `R_O`| `0x0`| `0x0`|
|`[20:20]`|`fmpm_fm_ds1e1_amsk_rai`| RAI| `R_O`| `0x0`| `0x0`|
|`[19:19]`|`fmpm_fm_ds1e1_amsk_lof`| LOF| `R_O`| `0x0`| `0x0`|
|`[18:18]`|`fmpm_fm_ds1e1_amsk_ais_ci`| AIS_CI| `R_O`| `0x0`| `0x0`|
|`[17:17]`|`fmpm_fm_ds1e1_amsk_ais`| AIS| `R_O`| `0x0`| `0x0`|
|`[16:16]`|`fmpm_fm_ds1e1_amsk_los`| LOS| `R_O`| `0x0`| `0x0`|
|`[15:07]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[06:06]`|`fmpm_fm_ds1e1_cur_hi_ais`| High level AIS| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_ds1e1_cur_rai_ci`| RAI_CI| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_ds1e1_cur_rai`| RAI| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_ds1e1_cur_lof`| LOF| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_ds1e1_cur_ais_ci`| AIS_CI| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_ds1e1_cur_ais`| AIS| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_ds1e1_cur_los`| LOS| `R_O`| `0x0`| `0x0`|

###FMPM FM PW Level1 Group Interrupt OR

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_PW_Level1_Group_Interrupt_OR`

* **Address**             : `0x18004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`fmpm_fm_pw_lev1_or`| Interrupt PW level1 group OR<br>{+ [00]} : PW Level-1 Group Interrupt (00)| `R_O`| `0x0`| `0x0`|

###FMPM FM PW Level2 Group Interrupt OR

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_PW_Level2_Group_Interrupt_OR`

* **Address**             : `0x180E0-0x180EF`

* **Formula**             : `0x180E0+$D`

* **Where**               : 

    * `$D(0-10) : PW  Level-1 Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`fmpm_fm_pw_lev2_or`| PW Level2 Group Interrupt OR, bit per group, the last Level-1 group has 12 Level-2 group<br>{+ [00]} : PW Level-2 Group Interrupt (D, 00) <br>{+ [01]} : PW Level-2 Group Interrupt (D, 01) <br>{+ [31]} : PW Level-2 Group Interrupt (D, 31)| `R_O`| `0x0`| `0x0`|

###FMPM FM PW Framer Interrupt OR AND MASK Per PW

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_PW_Framer_Interrupt_OR_AND_MASK_Per_PW`

* **Address**             : `0x1BC00-0x1BDFF`

* **Formula**             : `0x1BC00+$D*0x20+$E`

* **Where**               : 

    * `$D(0-10) : PW  Level-1 Group`

    * `$E(0-31) : PW  Level-2 Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`fmpm_fm_pw_oamsk`| Interrupt OR AND MASK per PW, bit per PW<br>{+ [00]} : PW Channel Interrupt (D, E, 00) <br>{+ [01]} : PW Channel Interrupt (D, E, 01) <br>{+ [31]} : PW Channel Interrupt (D, E, 31)| `R_O`| `0x0`| `0x0`|

###FMPM FM PW Framer Interrupt MASK Per PW

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_PW_Framer_Interrupt_MASK_Per_PW`

* **Address**             : `0x1BE00-0x1BFFF`

* **Formula**             : `0x1BE00+$D*0x20+$E`

* **Where**               : 

    * `$D(0-10) : PW  Level-1 Group`

    * `$E(0-31) : PW  Level-2 Group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`fmpm_fm_pw_msk`| Interrupt OR AND MASK per PW, bit per PW<br>{+ [00]} : PW Channel Interrupt (D, E, 00) <br>{+ [01]} : PW Channel Interrupt (D, E, 01) <br>{+ [31]} : PW Channel Interrupt (D, E, 31)| `R/W`| `0x0`| `0x0`|

###FMPM FM PW Interrupt Sticky Per Type Per PW

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_PW_Interrupt_Sticky_Per_Type_Per_PW`

* **Address**             : `0x1C000-0x1DFFF`

* **Formula**             : `0x1C000+$D*0x400+$E*0x20+$F`

* **Where**               : 

    * `$D(0-10) : PW  Level-1 Group`

    * `$E(0-31) : PW  Level-2 Group`

    * `$F(0-31) : PW-ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:07]`|`fmpm_fm_pw_stk_rbit`| RBIT| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_fm_pw_stk_stray`| STRAY| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_pw_stk_malf`| MALF| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_pw_stk_bufuder`| BufUder| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_pw_stk_bufover`| BufOver| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_pw_stk_lbit`| LBIT| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_pw_stk_lopsyn`| LOPSYN| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_pw_stk_lopsta`| LOPSTA| `W1C`| `0x0`| `0x0`|

###FMPM FM PW Interrupt MASK Per Type Per PW

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_PW_Interrupt_MASK_Per_Type_Per_PW`

* **Address**             : `0x20000-0x23FFF`

* **Formula**             : `0x20000+$D*0x400+$E*0x20+$F`

* **Where**               : 

    * `$D(0-10) : PW  Level-1 Group`

    * `$E(0-31) : PW  Level-2 Group`

    * `$F(0-31) : PW-ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:07]`|`fmpm_fm_pw_msk_rbit`| RBIT| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_fm_pw_msk_stray`| STRAY| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_pw_msk_malf`| MALF| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_pw_msk_bufuder`| BufUder| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_pw_msk_bufover`| BufOver| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_pw_msk_lbit`| LBIT| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_pw_msk_lopsyn`| LOPSYN| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_pw_msk_lopsta`| LOPSTA| `R/W`| `0x0`| `0x0`|

###FMPM FM PW Interrupt Current Status Per Type Per PW

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_FM_PW_Interrupt_Current_Status_Per_Type_Per_PW`

* **Address**             : `0x3C000-0x3FFFF`

* **Formula**             : `0x3C000+$D*0x400+$E*0x20+$F`

* **Where**               : 

    * `$D(0-10) : PW  Level-1 Group`

    * `$E(0-31) : PW  Level-2 Group`

    * `$F(0-31) : PW-ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:23]`|`fmpm_fm_pw_amsk_rbit`| RBIT| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`fmpm_fm_pw_amsk_stray`| STRAY| `R_O`| `0x0`| `0x0`|
|`[21:21]`|`fmpm_fm_pw_amsk_malf`| MALF| `R_O`| `0x0`| `0x0`|
|`[20:20]`|`fmpm_fm_pw_amsk_bufuder`| BufUder| `R_O`| `0x0`| `0x0`|
|`[19:19]`|`fmpm_fm_pw_amsk_bufover`| BufOver| `R_O`| `0x0`| `0x0`|
|`[18:18]`|`fmpm_fm_pw_amsk_lbit`| LBIT| `R_O`| `0x0`| `0x0`|
|`[17:17]`|`fmpm_fm_pw_amsk_lopsyn`| LOPSYN| `R_O`| `0x0`| `0x0`|
|`[16:16]`|`fmpm_fm_pw_amsk_lopsta`| LOPSTA| `R_O`| `0x0`| `0x0`|
|`[15:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:07]`|`fmpm_fm_pw_cur_rbit`| RBIT| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_fm_pw_cur_stray`| STRAY| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_fm_pw_cur_malf`| MALF| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_fm_pw_cur_bufuder`| BufUder| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_fm_pw_cur_bufover`| BufOver| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_fm_pw_cur_lbit`| LBIT| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_fm_pw_cur_lopsyn`| LOPSYN| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_fm_pw_cur_lopsta`| LOPSTA| `R_O`| `0x0`| `0x0`|

###FMPM TCA Interrupt

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_Interrupt`

* **Address**             : `0x54000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`fmpm_tca_intr_pw`| PW     interrupt| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_intr_ds1e1`| DS1E1  interrupt| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_intr_ds3e3`| DS3E3  interrupt| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_intr_vtgtug`| VTGTUG interrupt| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_intr_stsau`| STSAU  interrupt| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_intr_ecstm`| ECSTM  interrupt| `R_O`| `0x0`| `0x0`|

###FMPM TCA Interrupt Mask

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_Interrupt_Mask`

* **Address**             : `0x54001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`fmpm_tca_intr_msk_pw`| PW    Interrupt| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_intr_msk_ds1e1`| DS1E1 interrupt| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_intr_msk_ds3e3`| DS3E3  interrupt| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_intr_msk_vtgtug`| VTGTUG interrupt| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_intr_msk_stsau`| STSAU interrupt| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_intr_msk_ecstm`| ECSTM interrupt| `R/W`| `0x0`| `0x0`|

###FMPM TCA EC1 Interrupt OR

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_EC1_Interrupt_OR`

* **Address**             : `0x58005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:00]`|`fmpm_tca_ec1_or`| Interrupt EC1 OR<br>{+ [00]} : EC1 Interrupt (0, 0) <br>{+ [01]} : EC1 Interrupt (0, 1)| `R_O`| `0x0`| `0x0`|

###FMPM TCA EC1 Interrupt OR AND MASK Per STS1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_EC1_Interrupt_OR_AND_MASK_Per_STS1`

* **Address**             : `0x58100-0x5810F`

* **Formula**             : `0x58100+$L*0x2+$M`

* **Where**               : 

    * `$L(0-0) : TFI-5 line identifier`

    * `$M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`fmpm_tca_ec1_oamsk`| Interrupt STS1 OR AND MASK<br>{+ [00]} : EC1STM0 Interrupt (L, M, 00) <br>{+ [01]} : EC1STM0 Interrupt (L, M, 01) <br>{+ [15]} : EC1STM0 Interrupt (L, M, 15)| `R_O`| `0x0`| `0x0`|

###FMPM TCA EC1 Interrupt MASK Per STS1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_EC1_Interrupt_MASK_Per_STS1`

* **Address**             : `0x58110-0x5811F`

* **Formula**             : `0x58110+$L*0x2+$M`

* **Where**               : 

    * `$L(0-0) : TFI-5 line identifier`

    * `$M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`fmpm_tca_ec1_msk`| Interrupt MASK per STS1<br>{+ [00]} : EC1STM0 Interrupt (L, M, 00) <br>{+ [01]} : EC1STM0 Interrupt (L, M, 01) <br>{+ [15]} : EC1STM0 Interrupt (L, M, 23)| `R/W`| `0x0`| `0x0`|

###FMPM TCA EC1STM0 Interrupt Sticky Per Type Of Per STS1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_EC1STM0_Interrupt_Sticky_Per_Type_Of_Per_STS1`

* **Address**             : `0x5B600-0x5B7FF`

* **Formula**             : `0x5B600+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-0) : TFI-5 line identifier`

    * `$M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

    * `$N(0-15) : STS-1s in a STS-24 group will be numbered from 0 to 23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[13:13]`|`fmpm_tca_ec1stm0_stk_cv_s`| CV_S| `W1C`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_ec1stm0_stk_sefs_s`| SEFS_S| `W1C`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_ec1stm0_stk_es_s`| ES_S| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_ec1stm0_stk_ses_s`| SES_S| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_ec1stm0_stk_cv_l`| CV_L| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_ec1stm0_stk_es_l`| ES_L| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_ec1stm0_stk_ses_l`| SES_L| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_ec1stm0_stk_uas_l`| UAS_L| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_ec1stm0_stk_cv_lfe`| CV_LFE| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_ec1stm0_stk_fc_l`| FC_L| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_ec1stm0_stk_es_lfe`| ES_LFE| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_ec1stm0_stk_ses_lfe`| SES_LFE| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_ec1stm0_stk_uas_lfe`| UAS_LFE| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_ec1stm0_stk_fc_lfe`| FC_LFE| `W1C`| `0x0`| `0x0`|

###FMPM TCA EC1STM0 Interrupt MASK Per Type Of Per STS1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_EC1STM0_Interrupt_MASK_Per_Type_Of_Per_STS1`

* **Address**             : `0x5B800-0x5B9FF`

* **Formula**             : `0x5B800+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-0) : TFI-5 line identifier`

    * `$M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

    * `$N(0-15) : STS-1s in a STS-24 group will be numbered from 0 to 23`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[13:13]`|`fmpm_tca_ec1stm0_msk_cv_s`| CV_S| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_ec1stm0_msk_sefs_s`| SEFS_S| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_ec1stm0_msk_es_s`| ES_S| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_ec1stm0_msk_ses_s`| SES_S| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_ec1stm0_msk_cv_l`| CV_L| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_ec1stm0_msk_es_l`| ES_L| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_ec1stm0_msk_ses_l`| SES_L| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_ec1stm0_msk_uas_l`| UAS_L| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_ec1stm0_msk_cv_lfe`| CV_LFE| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_ec1stm0_msk_fc_l`| FC_L| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_ec1stm0_msk_es_lfe`| ES_LFE| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_ec1stm0_msk_ses_lfe`| SES_LFE| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_ec1stm0_msk_uas_lfe`| UAS_LFE| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_ec1stm0_msk_fc_lfe`| FC_LFE| `R/W`| `0x0`| `0x0`|

###FMPM TCA EC1STM0 Interrupt Current Status Per Type Of Per STS1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_EC1STM0_Interrupt_Current_Status_Per_Type_Of_Per_STS1`

* **Address**             : `0x5BA00-0x5BBFF`

* **Formula**             : `0x5BA00+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-0) : TFI-5 line identifier`

    * `$M(0-0) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

    * `$N(0-15) : STS-1s in a STS-24 group will be numbered from 0 to 23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[13:13]`|`fmpm_tca_ec1stm0_cur_cv_s`| CV_S| `R_O`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_ec1stm0_cur_sefs_s`| SEFS_S| `R_O`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_ec1stm0_cur_es_s`| ES_S| `R_O`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_ec1stm0_cur_ses_s`| SES_S| `R_O`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_ec1stm0_cur_cv_l`| CV_L| `R_O`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_ec1stm0_cur_es_l`| ES_L| `R_O`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_ec1stm0_cur_ses_l`| SES_L| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_ec1stm0_cur_uas_l`| UAS_L| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_ec1stm0_cur_cv_lfe`| CV_LFE| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_ec1stm0_cur_fc_l`| FC_L| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_ec1stm0_cur_es_lfe`| ES_LFE| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_ec1stm0_cur_ses_lfe`| SES_LFE| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_ec1stm0_cur_uas_lfe`| UAS_LFE| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_ec1stm0_cur_fc_lfe`| FC_LFE| `R_O`| `0x0`| `0x0`|

###FMPM TCA STS24 Interrupt OR

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_STS24_Interrupt_OR`

* **Address**             : `0x58000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:00]`|`fmpm_tca_sts24_or`| Interrupt STS24 OR<br>{+ [00]} : STS-24 Interrupt (0, 0) <br>{+ [01]} : STS-24 Interrupt (0, 1)| `R_O`| `0x0`| `0x0`|

###FMPM TCA STS24 Interrupt OR AND MASK Per STS1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_STS24_Interrupt_OR_AND_MASK_Per_STS1`

* **Address**             : `0x58080-0x5808F`

* **Formula**             : `0x58080+$L*0x2+$M`

* **Where**               : 

    * `$L(0-7) : TFI-5 line identifier`

    * `$M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`fmpm_tca_sts1_oamsk`| Interrupt STS1 OR AND MASK<br>{+ [00]} : STS-1 Interrupt (L, M, 00) <br>{+ [01]} : STS-1 Interrupt (L, M, 01) <br>{+ [23]} : STS-1 Interrupt (L, M, 23)| `R_O`| `0x0`| `0x0`|

###FMPM TCA STS24 Interrupt MASK Per STS1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_STS24_Interrupt_MASK_Per_STS1`

* **Address**             : `0x58090-0x5809F`

* **Formula**             : `0x58090+$L*0x2+$M`

* **Where**               : 

    * `$L(0-7) : TFI-5 line identifier`

    * `$M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`fmpm_tca_sts1_msk`| Interrupt MASK per STS1<br>{+ [00]} : STS-1 Interrupt (L, M, 00) <br>{+ [01]} : STS-1 Interrupt (L, M, 01) <br>{+ [23]} : STS-1 Interrupt (L, M, 23)| `R/W`| `0x0`| `0x0`|

###FMPM TCA STS24 Interrupt Sticky Per Type Of Per STS1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_STS24_Interrupt_Sticky_Per_Type_Of_Per_STS1`

* **Address**             : `0x5A000-0x5A1FF`

* **Formula**             : `0x5A000+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-7) : TFI-5 line identifier`

    * `$M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

    * `$N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`fmpm_tca_sts1_stk_cv_p`| CV_P| `W1C`| `0x0`| `0x0`|
|`[15:15]`|`fmpm_tca_sts1_stk_es_p`| ES_P| `W1C`| `0x0`| `0x0`|
|`[14:14]`|`fmpm_tca_sts1_stk_ses_p`| SES_P| `W1C`| `0x0`| `0x0`|
|`[13:13]`|`fmpm_tca_sts1_stk_uas_p`| UAS_P| `W1C`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_sts1_stk_ppjc_pdet`| PPJC_PDet| `W1C`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_sts1_stk_npjc_pdet`| NPJC_PDet| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_sts1_stk_ppjc_pgen`| PPJC_PGen| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_sts1_stk_npjc_pgen`| NPJC_PGen| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_sts1_stk_pjcdiff_p`| PJCDiff_P| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_sts1_stk_pjcs_det`| PJCS_Det| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_sts1_stk_pjcs_gen`| PJCS_Gen| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_sts1_stk_cv_pfe`| CV_PFE| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_sts1_stk_es_pfe`| ES_PFE| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_sts1_stk_ses_pfe`| SES_PFE| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_sts1_stk_uas_pfe`| UAS_PFE| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_sts1_stk_fc_p`| FC_P| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_sts1_stk_fc_pfe`| FC_PFE| `W1C`| `0x0`| `0x0`|

###FMPM TCA STS24 Interrupt MASK Per Type Of Per STS1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_STS24_Interrupt_MASK_Per_Type_Of_Per_STS1`

* **Address**             : `0x5A200-0x5A3FF`

* **Formula**             : `0x5A200+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-7) : TFI-5 line identifier`

    * `$M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

    * `$N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`fmpm_tca_sts1_msk_cv_p`| CV_P| `R/W`| `0x0`| `0x0`|
|`[15:15]`|`fmpm_tca_sts1_msk_es_p`| ES_P| `R/W`| `0x0`| `0x0`|
|`[14:14]`|`fmpm_tca_sts1_msk_ses_p`| SES_P| `R/W`| `0x0`| `0x0`|
|`[13:13]`|`fmpm_tca_sts1_msk_uas_p`| UAS_P| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_sts1_msk_ppjc_pdet`| PPJC_PDet| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_sts1_msk_npjc_pdet`| NPJC_PDet| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_sts1_msk_ppjc_pgen`| PPJC_PGen| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_sts1_msk_npjc_pgen`| NPJC_PGen| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_sts1_msk_pjcdiff_p`| PJCDiff_P| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_sts1_msk_pjcs_det`| PJCS_Det| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_sts1_msk_pjcs_gen`| PJCS_Gen| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_sts1_msk_cv_pfe`| CV_PFE| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_sts1_msk_es_pfe`| ES_PFE| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_sts1_msk_ses_pfe`| SES_PFE| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_sts1_msk_uas_pfe`| UAS_PFE| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_sts1_msk_fc_p`| FC_P| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_sts1_msk_fc_pfe`| FC_PFE| `R/W`| `0x0`| `0x0`|

###FMPM TCA STS24 Interrupt Current Status Per Type Of Per STS1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_STS24_Interrupt_Current_Status_Per_Type_Of_Per_STS1`

* **Address**             : `0x5A400-0x5A5FF`

* **Formula**             : `0x5A400+$L*0x40+$M*0x20+$N`

* **Where**               : 

    * `$L(0-7) : TFI-5 line identifier`

    * `$M(0-1) : 0 for STS-24 comprising of even-identifier STS-1s and 1 for STS-24 comprising of odd-identifier STS-1s`

    * `$N(0-23) : STS-1s in a STS-24 group will be numbered from 0 to 23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`fmpm_tca_sts1_cur_cv_p`| CV_P| `R_O`| `0x0`| `0x0`|
|`[15:15]`|`fmpm_tca_sts1_cur_es_p`| ES_P| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`fmpm_tca_sts1_cur_ses_p`| SES_P| `R_O`| `0x0`| `0x0`|
|`[13:13]`|`fmpm_tca_sts1_cur_uas_p`| UAS_P| `R_O`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_sts1_cur_ppjc_pdet`| PPJC_PDet| `R_O`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_sts1_cur_npjc_pdet`| NPJC_PDet| `R_O`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_sts1_cur_ppjc_pgen`| PPJC_PGen| `R_O`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_sts1_cur_npjc_pgen`| NPJC_PGen| `R_O`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_sts1_cur_pjcdiff_p`| PJCDiff_P| `R_O`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_sts1_cur_pjcs_det`| PJCS_Det| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_sts1_cur_pjcs_gen`| PJCS_Gen| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_sts1_cur_cv_pfe`| CV_PFE| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_sts1_cur_es_pfe`| ES_PFE| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_sts1_cur_ses_pfe`| SES_PFE| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_sts1_cur_uas_pfe`| UAS_PFE| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_sts1_cur_fc_p`| FC_P| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_sts1_cur_fc_pfe`| FC_PFE| `R_O`| `0x0`| `0x0`|

###FMPM TCA DS3E3 Group Interrupt OR

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS3E3_Group_Interrupt_OR`

* **Address**             : `0x58001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`fmpm_tca_ds3e3_or`| Interrupt DS3/E3 OR<br>{+ [00]} : DS3/E3 Group Interrupt (00)| `R_O`| `0x0`| `0x0`|

###FMPM TCA DS3E3 Framer Interrupt OR AND MASK Per DS3E3

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS3E3_Framer_Interrupt_OR_AND_MASK_Per_DS3E3`

* **Address**             : `0x580A0-0x580AF`

* **Formula**             : `0x580A0+$G`

* **Where**               : 

    * `$G(0-15) : DS3/E3 Group  Interrupt`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`fmpm_tca_ds3e3_oamsk`| Interrupt DS3/E3 Group<br>{+ [00]} : DS3/E3 Framer Interrupt (G, 00) <br>{+ [01]} : DS3/E3 Framer Interrupt (G, 01) <br>{+ [23]} : DS3/E3 Framer Interrupt (G, 23)| `R_O`| `0x0`| `0x0`|

###FMPM TCA DS3E3 Framer Interrupt MASK Per DS3E3

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_DS3E3`

* **Address**             : `0x580B0-0x580BF`

* **Formula**             : `0x580B0+$G`

* **Where**               : 

    * `$G(0-15) : DS3/E3 Group  Interrupt`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`fmpm_tca_ds3e3_msk`| MASK per DS3/E3 Framer<br>{+ [00]} : DS3/E3 Framer Interrupt (G, 00) <br>{+ [01]} : DS3/E3 Framer Interrupt (G, 01) <br>{+ [23]} : DS3/E3 Framer Interrupt (G, 23)| `R/W`| `0x0`| `0x0`|

###FMPM TCA DS3E3 Framer Interrupt Sticky Per Type of Per DS3E3

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS3E3_Framer_Interrupt_Sticky_Per_Type_of_Per_DS3E3`

* **Address**             : `0x5A600-0x5A7FF`

* **Formula**             : `0x5A600+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : DS3/E3 Group  Interrupt`

    * `$H(0-23) : DS3/E3 Group  Interrupt`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`fmpm_tca_ds3e3_stk_cv_l`| CV_L| `W1C`| `0x0`| `0x0`|
|`[27:27]`|`fmpm_tca_ds3e3_stk_es_l`| ES_L| `W1C`| `0x0`| `0x0`|
|`[26:26]`|`fmpm_tca_ds3e3_stk_esa_l`| ESA_L| `W1C`| `0x0`| `0x0`|
|`[25:25]`|`fmpm_tca_ds3e3_stk_esb_l`| ESB_L| `W1C`| `0x0`| `0x0`|
|`[24:24]`|`fmpm_tca_ds3e3_stk_ses_l`| SES_L| `W1C`| `0x0`| `0x0`|
|`[23:23]`|`fmpm_tca_ds3e3_stk_loss_l`| LOSS_L| `W1C`| `0x0`| `0x0`|
|`[22:22]`|`fmpm_tca_ds3e3_stk_cvp_p`| CVP_P| `W1C`| `0x0`| `0x0`|
|`[21:21]`|`fmpm_tca_ds3e3_stk_cvcp_p`| CVCP_P| `W1C`| `0x0`| `0x0`|
|`[20:20]`|`fmpm_tca_ds3e3_stk_esp_p`| ESP_P| `W1C`| `0x0`| `0x0`|
|`[19:19]`|`fmpm_tca_ds3e3_stk_escp_p`| ESCP_P| `W1C`| `0x0`| `0x0`|
|`[18:18]`|`fmpm_tca_ds3e3_stk_esap_p`| ESAP_P| `W1C`| `0x0`| `0x0`|
|`[17:17]`|`fmpm_tca_ds3e3_stk_esacp_p`| ESACP_P| `W1C`| `0x0`| `0x0`|
|`[16:16]`|`fmpm_tca_ds3e3_stk_esbp_p`| ESBP_P| `W1C`| `0x0`| `0x0`|
|`[15:15]`|`fmpm_tca_ds3e3_stk_esbcp_p`| ESBCP_P| `W1C`| `0x0`| `0x0`|
|`[14:14]`|`fmpm_tca_ds3e3_stk_sesp_p`| SESP_P| `W1C`| `0x0`| `0x0`|
|`[13:13]`|`fmpm_tca_ds3e3_stk_sescp_p`| SESCP_P| `W1C`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_ds3e3_stk_sas_p`| SAS_P| `W1C`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_ds3e3_stk_aiss_p`| AISS_P| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_ds3e3_stk_uas_p`| UAS_P| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_ds3e3_stk_uascp_p`| UASCP_P| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_ds3e3_stk_cvcp_pfe`| CVCP_PFE| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_ds3e3_stk_escp_pfe`| ESCP_PFE| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_ds3e3_stk_esacp_pfe`| ESACP_PFE| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_ds3e3_stk_esbcp_pfe`| ESBCP_PFE| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_ds3e3_stk_sescp_pfe`| SESCP_PFE| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_ds3e3_stk_sascp_pfe`| SASCP_PFE| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_ds3e3_stk_uascp_pfe`| UASCP_PFE| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_ds3e3_stk_fc_p`| FC_P| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_ds3e3_stk_fccp_pfe`| FCCP_PFE| `W1C`| `0x0`| `0x0`|

###FMPM TCA DS3E3 Framer Interrupt MASK Per Type of Per DS3E3

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS3E3_Framer_Interrupt_MASK_Per_Type_of_Per_DS3E3`

* **Address**             : `0x5A800-0x5A9FF`

* **Formula**             : `0x5A800+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : DS3/E3 Group  Interrupt`

    * `$H(0-23) : DS3/E3 Group  Interrupt`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`fmpm_tca_ds3e3_msk_cv_l`| CV_L| `R/W`| `0x0`| `0x0`|
|`[27:27]`|`fmpm_tca_ds3e3_msk_es_l`| ES_L| `R/W`| `0x0`| `0x0`|
|`[26:26]`|`fmpm_tca_ds3e3_msk_esa_l`| ESA_L| `R/W`| `0x0`| `0x0`|
|`[25:25]`|`fmpm_tca_ds3e3_msk_esb_l`| ESB_L| `R/W`| `0x0`| `0x0`|
|`[24:24]`|`fmpm_tca_ds3e3_msk_ses_l`| SES_L| `R/W`| `0x0`| `0x0`|
|`[23:23]`|`fmpm_tca_ds3e3_msk_loss_l`| LOSS_L| `R/W`| `0x0`| `0x0`|
|`[22:22]`|`fmpm_tca_ds3e3_msk_cvp_p`| CVP_P| `R/W`| `0x0`| `0x0`|
|`[21:21]`|`fmpm_tca_ds3e3_msk_cvcp_p`| CVCP_P| `R/W`| `0x0`| `0x0`|
|`[20:20]`|`fmpm_tca_ds3e3_msk_esp_p`| ESP_P| `R/W`| `0x0`| `0x0`|
|`[19:19]`|`fmpm_tca_ds3e3_msk_escp_p`| ESCP_P| `R/W`| `0x0`| `0x0`|
|`[18:18]`|`fmpm_tca_ds3e3_msk_esap_p`| ESAP_P| `R/W`| `0x0`| `0x0`|
|`[17:17]`|`fmpm_tca_ds3e3_msk_esacp_p`| ESACP_P| `R/W`| `0x0`| `0x0`|
|`[16:16]`|`fmpm_tca_ds3e3_msk_esbp_p`| ESBP_P| `R/W`| `0x0`| `0x0`|
|`[15:15]`|`fmpm_tca_ds3e3_msk_esbcp_p`| ESBCP_P| `R/W`| `0x0`| `0x0`|
|`[14:14]`|`fmpm_tca_ds3e3_msk_sesp_p`| SESP_P| `R/W`| `0x0`| `0x0`|
|`[13:13]`|`fmpm_tca_ds3e3_msk_sescp_p`| SESCP_P| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_ds3e3_msk_sas_p`| SAS_P| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_ds3e3_msk_aiss_p`| AISS_P| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_ds3e3_msk_uas_p`| UAS_P| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_ds3e3_msk_uascp_p`| UASCP_P| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_ds3e3_msk_cvcp_pfe`| CVCP_PFE| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_ds3e3_msk_escp_pfe`| ESCP_PFE| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_ds3e3_msk_esacp_pfe`| ESACP_PFE| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_ds3e3_msk_esbcp_pfe`| ESBCP_PFE| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_ds3e3_msk_sescp_pfe`| SESCP_PFE| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_ds3e3_msk_sascp_pfe`| SASCP_PFE| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_ds3e3_msk_uascp_pfe`| UASCP_PFE| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_ds3e3_msk_fc_p`| FC_P| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_ds3e3_msk_fccp_pfe`| FCCP_PFE| `R/W`| `0x0`| `0x0`|

###FMPM TCA DS3E3 Framer Interrupt Current Status Per Type of Per DS3E3

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS3E3_Framer_Interrupt_Current_Status_Per_Type_of_Per_DS3E3`

* **Address**             : `0x5AA00-0x5ABFF`

* **Formula**             : `0x5AA00+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : DS3/E3 Group  Interrupt`

    * `$H(0-23) : DS3/E3 Group  Interrupt`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`fmpm_tca_ds3e3_cur_cv_l`| CV_L| `R_O`| `0x0`| `0x0`|
|`[27:27]`|`fmpm_tca_ds3e3_cur_es_l`| ES_L| `R_O`| `0x0`| `0x0`|
|`[26:26]`|`fmpm_tca_ds3e3_cur_esa_l`| ESA_L| `R_O`| `0x0`| `0x0`|
|`[25:25]`|`fmpm_tca_ds3e3_cur_esb_l`| ESB_L| `R_O`| `0x0`| `0x0`|
|`[24:24]`|`fmpm_tca_ds3e3_cur_ses_l`| SES_L| `R_O`| `0x0`| `0x0`|
|`[23:23]`|`fmpm_tca_ds3e3_cur_loss_l`| LOSS_L| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`fmpm_tca_ds3e3_cur_cvp_p`| CVP_P| `R_O`| `0x0`| `0x0`|
|`[21:21]`|`fmpm_tca_ds3e3_cur_cvcp_p`| CVCP_P| `R_O`| `0x0`| `0x0`|
|`[20:20]`|`fmpm_tca_ds3e3_cur_esp_p`| ESP_P| `R_O`| `0x0`| `0x0`|
|`[19:19]`|`fmpm_tca_ds3e3_cur_escp_p`| ESCP_P| `R_O`| `0x0`| `0x0`|
|`[18:18]`|`fmpm_tca_ds3e3_cur_esap_p`| ESAP_P| `R_O`| `0x0`| `0x0`|
|`[17:17]`|`fmpm_tca_ds3e3_cur_esacp_p`| ESACP_P| `R_O`| `0x0`| `0x0`|
|`[16:16]`|`fmpm_tca_ds3e3_cur_esbp_p`| ESBP_P| `R_O`| `0x0`| `0x0`|
|`[15:15]`|`fmpm_tca_ds3e3_cur_esbcp_p`| ESBCP_P| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`fmpm_tca_ds3e3_cur_sesp_p`| SESP_P| `R_O`| `0x0`| `0x0`|
|`[13:13]`|`fmpm_tca_ds3e3_cur_sescp_p`| SESCP_P| `R_O`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_ds3e3_cur_sas_p`| SAS_P| `R_O`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_ds3e3_cur_aiss_p`| AISS_P| `R_O`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_ds3e3_cur_uas_p`| UAS_P| `R_O`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_ds3e3_cur_uascp_p`| UASCP_P| `R_O`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_ds3e3_cur_cvcp_pfe`| CVCP_PFE| `R_O`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_ds3e3_cur_escp_pfe`| ESCP_PFE| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_ds3e3_cur_esacp_pfe`| ESACP_PFE| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_ds3e3_cur_esbcp_pfe`| ESBCP_PFE| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_ds3e3_cur_sescp_pfe`| SESCP_PFE| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_ds3e3_cur_sascp_pfe`| SASCP_PFE| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_ds3e3_cur_uascp_pfe`| UASCP_PFE| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_ds3e3_cur_fc_p`| FC_P| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_ds3e3_cur_fccp_pfe`| FCCP_PFE| `R_O`| `0x0`| `0x0`|

###FMPM TCA STS24 LO Interrupt OR

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_STS24_LO_Interrupt_OR`

* **Address**             : `0x58002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`fmpm_tca_sts24lo_or`| Interrupt STS24 LO OR<br>{+ [00]} : STS-24 LO Interrupt (00)| `R_O`| `0x0`| `0x0`|

###FMPM TCA STS1 LO Interrupt OR

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_STS1_LO_Interrupt_OR`

* **Address**             : `0x580C0-0x580CF`

* **Formula**             : `0x580C0+$G`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`fmpm_tca_sts1lo_or`| Interrupt STS1 LO OR<br>{+ [00]} : STS-1 LO Interrupt (G, 00) <br>{+ [01]} : STS-1 LO Interrupt (G, 01) <br>{+ [23]} : STS-1 LO Interrupt (G, 23)| `R_O`| `0x0`| `0x0`|

###FMPM TCA VTTU Interrupt OR AND MASK Per VTTU

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_VTTU_Interrupt_OR_AND_MASK_Per_VTTU`

* **Address**             : `0x5AC00-0x5ADFF`

* **Formula**             : `0x5AC00+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

    * `$H(0-23) : STS-1 LO group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[27:00]`|`fmpm_tca_vttu_oamsk`| VT/TU Interrupt<br>{+ [00]} : VT/TU Interrupt (G,H, 00) <br>{+ [01]} : VT/TU Interrupt (G,H, 01) <br>{+ [27]} : VT/TU Interrupt (G,H, 27)| `R_O`| `0x0`| `0x0`|

###FMPM TCA VTTU Interrupt MASK Per VTTU

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_VTTU_Interrupt_MASK_Per_VTTU`

* **Address**             : `0x5AE00-0x5AFFF`

* **Formula**             : `0x5AE00+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

    * `$H(0-23) : STS-1 LO group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[27:00]`|`fmpm_tca_vttu_msk`| VT/TU Interrupt<br>{+ [00]} : VT/TU Interrupt (G,H, 00) <br>{+ [01]} : VT/TU Interrupt (G,H, 01) <br>{+ [27]} : VT/TU Interrupt (G,H, 27)| `R/W`| `0x0`| `0x0`|

###FMPM TCA VTTU Interrupt Sticky Per Type Per VTTU

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_VTTU_Interrupt_Sticky_Per_Type_Per_VTTU`

* **Address**             : `0x64000-0x67FFF`

* **Formula**             : `0x64000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

    * `$H(0-23) : STS-1 LO group`

    * `$I(0-27) : VT/TU`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`fmpm_tca_vttu_stk_cv_v`| CV_V| `W1C`| `0x0`| `0x0`|
|`[15:15]`|`fmpm_tca_vttu_stk_es_v`| ES_V| `W1C`| `0x0`| `0x0`|
|`[14:14]`|`fmpm_tca_vttu_stk_ses_v`| SES_V| `W1C`| `0x0`| `0x0`|
|`[13:13]`|`fmpm_tca_vttu_stk_uas_v`| UAS_V| `W1C`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_vttu_stk_ppjc_vdet`| PPJC_VDet| `W1C`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_vttu_stk_npjc_vdet`| NPJC_VDet| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_vttu_stk_ppjc_vgen`| PPJC_VGen| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_vttu_stk_npjc_vgen`| NPJC_VGen| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_vttu_stk_pjcdiff_v`| PJCDiff_V| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_vttu_stk_pjcs_vdet`| PJCS_VDet| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_vttu_stk_pjcs_vgen`| PJCS_VGen| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_vttu_stk_cv_vfe`| CV_VFE| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_vttu_stk_es_vfe`| ES_VFE| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_vttu_stk_ses_vfe`| SES_VFE| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_vttu_stk_uas_vfe`| UAS_VFE| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_vttu_stk_fc_v`| FC_V| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_vttu_stk_fc_vfe`| FC_VFE| `W1C`| `0x0`| `0x0`|

###FMPM TCA VTTU Interrupt MASK Per Type Per VTTU

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_VTTU_Interrupt_MASK_Per_Type_Per_VTTU`

* **Address**             : `0x68000-0x6BFFF`

* **Formula**             : `0x68000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

    * `$H(0-23) : STS-1 LO group`

    * `$I(0-27) : VT/TU`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`fmpm_tca_vttu_msk_cv_v`| CV_V| `R/W`| `0x0`| `0x0`|
|`[15:15]`|`fmpm_tca_vttu_msk_es_v`| ES_V| `R/W`| `0x0`| `0x0`|
|`[14:14]`|`fmpm_tca_vttu_msk_ses_v`| SES_V| `R/W`| `0x0`| `0x0`|
|`[13:13]`|`fmpm_tca_vttu_msk_uas_v`| UAS_V| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_vttu_msk_ppjc_vdet`| PPJC_VDet| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_vttu_msk_npjc_vdet`| NPJC_VDet| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_vttu_msk_ppjc_vgen`| PPJC_VGen| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_vttu_msk_npjc_vgen`| NPJC_VGen| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_vttu_msk_pjcdiff_v`| PJCDiff_V| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_vttu_msk_pjcs_vdet`| PJCS_VDet| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_vttu_msk_pjcs_vgen`| PJCS_VGen| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_vttu_msk_cv_vfe`| CV_VFE| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_vttu_msk_es_vfe`| ES_VFE| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_vttu_msk_ses_vfe`| SES_VFE| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_vttu_msk_uas_vfe`| UAS_VFE| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_vttu_msk_fc_v`| FC_V| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_vttu_msk_fc_vfe`| FC_VFE| `R/W`| `0x0`| `0x0`|

###FMPM TCA VTTU Interrupt Current Status Per Type Per VTTU

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_VTTU_Interrupt_Current_Status_Per_Type_Per_VTTU`

* **Address**             : `0x6C000-0x6FFFF`

* **Formula**             : `0x6C000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : STS-24 LO group`

    * `$H(0-23) : STS-1 LO group`

    * `$I(0-27) : VT/TU`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`fmpm_tca_vttu_cur_cv_v`| CV_V| `R_O`| `0x0`| `0x0`|
|`[15:15]`|`fmpm_tca_vttu_cur_es_v`| ES_V| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`fmpm_tca_vttu_cur_ses_v`| SES_V| `R_O`| `0x0`| `0x0`|
|`[13:13]`|`fmpm_tca_vttu_cur_uas_v`| UAS_V| `R_O`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_vttu_cur_ppjc_vdet`| PPJC_VDet| `R_O`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_vttu_cur_npjc_vdet`| NPJC_VDet| `R_O`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_vttu_cur_ppjc_vgen`| PPJC_VGen| `R_O`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_vttu_cur_npjc_vgen`| NPJC_VGen| `R_O`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_vttu_cur_pjcdiff_v`| PJCDiff_V| `R_O`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_vttu_cur_pjcs_vdet`| PJCS_VDet| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_vttu_cur_pjcs_vgen`| PJCS_VGen| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_vttu_cur_cv_vfe`| CV_VFE| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_vttu_cur_es_vfe`| ES_VFE| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_vttu_cur_ses_vfe`| SES_VFE| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_vttu_cur_uas_vfe`| UAS_VFE| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_vttu_cur_fc_v`| FC_V| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_vttu_cur_fc_vfe`| FC_VFE| `R_O`| `0x0`| `0x0`|

###FMPM TCA DS1E1 Level1 Group Interrupt OR

* **Description**           

FMPM FM Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS1E1_Level1_Group_Interrupt_OR`

* **Address**             : `0x58003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`fmpm_fm_ds1e1_lev1_or`| Interrupt DS1E1 level1 group OR<br>{+ [00]} : DS1/E1 Level-1 Group Interrupt (00)| `R_O`| `0x0`| `0x0`|

###FMPM TCA DS1E1 Level2 Group Interrupt OR

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS1E1_Level2_Group_Interrupt_OR`

* **Address**             : `0x580D0-0x580DF`

* **Formula**             : `0x580D0+$G`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`fmpm_tca_ds1e1_lev2_or`| Level2 Group Interrupt OR<br>{+ [00]} : DS1/E1 Level-2 Group Interrupt (G, 00) <br>{+ [01]} : DS1/E1 Level-2 Group Interrupt (G, 01) <br>{+ [23]} : DS1/E1 Level-2 Group Interrupt (G, 23)| `R_O`| `0x0`| `0x0`|

###FMPM TCA DS1E1 Framer Interrupt OR AND MASK Per DS1E1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS1E1_Framer_Interrupt_OR_AND_MASK_Per_DS1E1`

* **Address**             : `0x5B000-0x5B1FF`

* **Formula**             : `0x5B000+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

    * `$H(0-23) : DS1/E1 Level-2 Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[27:00]`|`fmpm_tca_ds1e1_oamsk`| DS1/E1 Framer Interrupt<br>{+ [00]} : DS1/E1 Framer Interrupt (G,H, 00) <br>{+ [01]} : DS1/E1 Framer Interrupt (G,H, 01) <br>{+ [27]} : DS1/E1 Framer Interrupt (G,H, 27)| `R_O`| `0x0`| `0x0`|

###FMPM TCA DS1E1 Framer Interrupt MASK Per DS1E1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS1E1_Framer_Interrupt_MASK_Per_DS1E1`

* **Address**             : `0x5B200-0x5B3FF`

* **Formula**             : `0x5B200+$G*0x20+$H`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

    * `$H(0-23) : DS1/E1 Level-2 Group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[27:00]`|`fmpm_tca_ds1e1_msk`| DS1/E1 Framer Interrupt<br>{+ [00]} : DS1/E1 Framer Interrupt (G,H, 00) <br>{+ [01]} : DS1/E1 Framer Interrupt (G,H, 01) <br>{+ [27]} : DS1/E1 Framer Interrupt (G,H, 27)| `R/W`| `0x0`| `0x0`|

###FMPM TCA DS1E1 Interrupt Sticky Per Type Per DS1E1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS1E1_Interrupt_Sticky_Per_Type_Per_DS1E1`

* **Address**             : `0x70000-0x73FFF`

* **Formula**             : `0x70000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

    * `$H(0-23) : DS1/E1 Level-2 Group`

    * `$I(0-27) : DS1/E1`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[18:18]`|`fmpm_tca_ds1e1_stk_cv_l`| CV_L| `W1C`| `0x0`| `0x0`|
|`[17:17]`|`fmpm_tca_ds1e1_stk_es_l`| ES_L| `W1C`| `0x0`| `0x0`|
|`[16:16]`|`fmpm_tca_ds1e1_stk_ses_l`| SES_L| `W1C`| `0x0`| `0x0`|
|`[15:15]`|`fmpm_tca_ds1e1_stk_loss_l`| LOSS_L| `W1C`| `0x0`| `0x0`|
|`[14:14]`|`fmpm_tca_ds1e1_stk_cv_p`| CV_P| `W1C`| `0x0`| `0x0`|
|`[13:13]`|`fmpm_tca_ds1e1_stk_es_p`| ES_P| `W1C`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_ds1e1_stk_ses_p`| SES_P| `W1C`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_ds1e1_stk_aiss_p`| AISS_P| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_ds1e1_stk_sas_p`| SAS_P| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_ds1e1_stk_css_p`| CSS_P| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_ds1e1_stk_uas_p`| UAS_P| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_ds1e1_stk_fc_p`| FC_P| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_ds1e1_stk_es_lfe`| ES_LFE| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_ds1e1_stk_sefs_pfe`| SEFS_PFE| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_ds1e1_stk_es_pfe`| ES_PFE| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_ds1e1_stk_ses_pfe`| SES_PFE| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_ds1e1_stk_css_pfe`| CSS_PFE| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_ds1e1_stk_fc_pfe`| FC_PFE| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_ds1e1_stk_uas_pfe`| UAS_PFE| `W1C`| `0x0`| `0x0`|

###FMPM TCA DS1E1 Interrupt MASK Per Type Per DS1E1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS1E1_Interrupt_MASK_Per_Type_Per_DS1E1`

* **Address**             : `0x74000-0x77FFF`

* **Formula**             : `0x74000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

    * `$H(0-23) : DS1/E1 Level-2 Group`

    * `$I(0-27) : DS1/E1`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[18:18]`|`fmpm_tca_ds1e1_msk_cv_l`| CV_L| `R/W`| `0x0`| `0x0`|
|`[17:17]`|`fmpm_tca_ds1e1_msk_es_l`| ES_L| `R/W`| `0x0`| `0x0`|
|`[16:16]`|`fmpm_tca_ds1e1_msk_ses_l`| SES_L| `R/W`| `0x0`| `0x0`|
|`[15:15]`|`fmpm_tca_ds1e1_msk_loss_l`| LOSS_L| `R/W`| `0x0`| `0x0`|
|`[14:14]`|`fmpm_tca_ds1e1_msk_cv_p`| CV_P| `R/W`| `0x0`| `0x0`|
|`[13:13]`|`fmpm_tca_ds1e1_msk_es_p`| ES_P| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_ds1e1_msk_ses_p`| SES_P| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_ds1e1_msk_aiss_p`| AISS_P| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_ds1e1_msk_sas_p`| SAS_P| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_ds1e1_msk_css_p`| CSS_P| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_ds1e1_msk_uas_p`| UAS_P| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_ds1e1_msk_fc_p`| FC_P| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_ds1e1_msk_es_lfe`| ES_LFE| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_ds1e1_msk_sefs_pfe`| SEFS_PFE| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_ds1e1_msk_es_pfe`| ES_PFE| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_ds1e1_msk_ses_pfe`| SES_PFE| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_ds1e1_msk_css_pfe`| CSS_PFE| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_ds1e1_msk_fc_pfe`| FC_PFE| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_ds1e1_msk_uas_pfe`| UAS_PFE| `R/W`| `0x0`| `0x0`|

###FMPM TCA DS1E1 Interrupt Current Status Per Type Per DS1E1

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_DS1E1_Interrupt_Current_Status_Per_Type_Per_DS1E1`

* **Address**             : `0x78000-0x7BFFF`

* **Formula**             : `0x78000+$G*0x400+$H*0x20+$I`

* **Where**               : 

    * `$G(0-15) : DS1/E1 Level-1 Group`

    * `$H(0-23) : DS1/E1 Level-2 Group`

    * `$I(0-27) : DS1/E1`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[18:18]`|`fmpm_tca_ds1e1_cur_cv_l`| CV_L| `R_O`| `0x0`| `0x0`|
|`[17:17]`|`fmpm_tca_ds1e1_cur_es_l`| ES_L| `R_O`| `0x0`| `0x0`|
|`[16:16]`|`fmpm_tca_ds1e1_cur_ses_l`| SES_L| `R_O`| `0x0`| `0x0`|
|`[15:15]`|`fmpm_tca_ds1e1_cur_loss_l`| LOSS_L| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`fmpm_tca_ds1e1_cur_cv_p`| CV_P| `R_O`| `0x0`| `0x0`|
|`[13:13]`|`fmpm_tca_ds1e1_cur_es_p`| ES_P| `R_O`| `0x0`| `0x0`|
|`[12:12]`|`fmpm_tca_ds1e1_cur_ses_p`| SES_P| `R_O`| `0x0`| `0x0`|
|`[11:11]`|`fmpm_tca_ds1e1_cur_aiss_p`| AISS_P| `R_O`| `0x0`| `0x0`|
|`[10:10]`|`fmpm_tca_ds1e1_cur_sas_p`| SAS_P| `R_O`| `0x0`| `0x0`|
|`[09:09]`|`fmpm_tca_ds1e1_cur_css_p`| CSS_P| `R_O`| `0x0`| `0x0`|
|`[08:08]`|`fmpm_tca_ds1e1_cur_uas_p`| UAS_P| `R_O`| `0x0`| `0x0`|
|`[07:07]`|`fmpm_tca_ds1e1_cur_fc_p`| FC_P| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`fmpm_tca_ds1e1_cur_es_lfe`| ES_LFE| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_ds1e1_cur_sefs_pfe`| SEFS_PFE| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_ds1e1_cur_es_pfe`| ES_PFE| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_ds1e1_cur_ses_pfe`| SES_PFE| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_ds1e1_cur_css_pfe`| CSS_PFE| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_ds1e1_cur_fc_pfe`| FC_PFE| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_ds1e1_cur_uas_pfe`| UAS_PFE| `R_O`| `0x0`| `0x0`|

###FMPM TCA PW Level1 Group Interrupt OR

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_PW_Level1_Group_Interrupt_OR`

* **Address**             : `0x58004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`fmpm_tca_pw_lev1_or`| Interrupt PW level1 group OR<br>{+ [00]} : PW Level-1 Group Interrupt (00)| `R_O`| `0x0`| `0x0`|

###FMPM TCA PW Level2 Group Interrupt OR

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_PW_Level2_Group_Interrupt_OR`

* **Address**             : `0x580E0-0x580EF`

* **Formula**             : `0x580E0+$D`

* **Where**               : 

    * `$D(0-10) : PW  Level-1 Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`fmpm_tca_pw_lev2_or`| PW Level2 Group Interrupt OR, bit per group, the last Level-1 group has 12 Level-2 group<br>{+ [00]} : PW Level-2 Group Interrupt (D, 00) <br>{+ [01]} : PW Level-2 Group Interrupt (D, 01) <br>{+ [31]} : PW Level-2 Group Interrupt (D, 31)| `R_O`| `0x0`| `0x0`|

###FMPM TCA PW Framer Interrupt OR AND MASK Per PW

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_PW_Framer_Interrupt_OR_AND_MASK_Per_PW`

* **Address**             : `0x5BC00-0x5BDFF`

* **Formula**             : `0x5BC00+$D*0x20+$E`

* **Where**               : 

    * `$D(0-10) : PW  Level-1 Group`

    * `$E(0-31) : PW  Level-2 Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`fmpm_tca_pw_oamsk`| Interrupt OR AND MASK per PW, bit per PW<br>{+ [00]} : PW Channel Interrupt (D, E, 00) <br>{+ [01]} : PW Channel Interrupt (D, E, 01) <br>{+ [31]} : PW Channel Interrupt (D, E, 31)| `R_O`| `0x0`| `0x0`|

###FMPM TCA PW Framer Interrupt MASK Per PW

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_PW_Framer_Interrupt_MASK_Per_PW`

* **Address**             : `0x5BE00-0x5BFFF`

* **Formula**             : `0x5BE00+$D*0x20+$E`

* **Where**               : 

    * `$D(0-10) : PW  Level-1 Group`

    * `$E(0-31) : PW  Level-2 Group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`fmpm_tca_pw_msk`| Interrupt OR AND MASK per PW, bit per PW<br>{+ [00]} : PW Channel Interrupt (D, E, 00) <br>{+ [01]} : PW Channel Interrupt (D, E, 01) <br>{+ [31]} : PW Channel Interrupt (D, E, 31)| `R/W`| `0x0`| `0x0`|

###FMPM TCA PW Interrupt Sticky Per Type Per PW

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_PW_Interrupt_Sticky_Per_Type_Per_PW`

* **Address**             : `0x5C000-0x5FFFF`

* **Formula**             : `0x5C000+$D*0x400+$E*0x20+$F`

* **Where**               : 

    * `$D(0-10) : PW  Level-1 Group`

    * `$E(0-31) : PW  Level-2 Group`

    * `$F(0-31) : PW-ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:07]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[06:06]`|`fmpm_tca_pw_stk_es`| ES| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_pw_stk_ses`| SES| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_pw_stk_uas`| UAS| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_pw_stk_es_fe`| ES_FE| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_pw_stk_ses_fe`| SES_FE| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_pw_stk_uas_fe`| UAS_FE| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_pw_stk_fc`| FC| `W1C`| `0x0`| `0x0`|

###FMPM TCA PW Interrupt MASK Per Type Per PW

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_PW_Interrupt_MASK_Per_Type_Per_PW`

* **Address**             : `0x60000-0x63FFF`

* **Formula**             : `0x60000+$D*0x400+$E*0x20+$F`

* **Where**               : 

    * `$D(0-10) : PW  Level-1 Group`

    * `$E(0-31) : PW  Level-2 Group`

    * `$F(0-31) : PW-ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:07]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[06:06]`|`fmpm_tca_pw_msk_es`| ES| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_pw_msk_ses`| SES| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_pw_msk_uas`| UAS| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_pw_msk_es_fe`| ES_FE| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_pw_msk_ses_fe`| SES_FE| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_pw_msk_uas_fe`| UAS_FE| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_pw_msk_fc`| FC| `R/W`| `0x0`| `0x0`|

###FMPM TCA PW Interrupt Current Status Per Type Per PW

* **Description**           

FMPM TCA Interrupt


* **RTL Instant Name**    : `FMPM_TCA_PW_Interrupt_Current_Status_Per_Type_Per_PW`

* **Address**             : `0x7C000-0x7FFFF`

* **Formula**             : `0x7C000+$D*0x400+$E*0x20+$F`

* **Where**               : 

    * `$D(0-10) : PW  Level-1 Group`

    * `$E(0-31) : PW  Level-2 Group`

    * `$F(0-31) : PW-ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:07]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[06:06]`|`fmpm_tca_pw_cur_es`| ES| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fmpm_tca_pw_cur_ses`| SES| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`fmpm_tca_pw_cur_uas`| UAS| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`fmpm_tca_pw_cur_es_fe`| ES_FE| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fmpm_tca_pw_cur_ses_fe`| SES_FE| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`fmpm_tca_pw_cur_uas_fe`| UAS_FE| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`fmpm_tca_pw_cur_fc`| FC| `R_O`| `0x0`| `0x0`|

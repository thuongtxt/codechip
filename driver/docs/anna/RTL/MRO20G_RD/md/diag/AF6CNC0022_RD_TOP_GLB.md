## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_TOP_GLB
####Register Table

|Name|Address|
|-----|-----|
|`Device Product ID`|`0xF0_0000`|
|`Device Year Month Day Version ID`|`0xF0_0001`|
|`Device Internal ID`|`0xF0_0003`|
|`general purpose inputs for TX Uart`|`0x5`|
|`general purpose inputs for TX Uart`|`0x40`|
|`Flow Control Mechanism for Ethernet pass-through Diagnostic Value`|`0x41`|
|`Force Error for Flow Control Mechanism Diagnostic`|`0x42`|
|`Diagnostic Enable Control`|`0x43`|
|`OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Bus Type port 1 to 8`|`0x44`|
|`Raw PRSB test for OC192_OC48_OC12_10G_1G_100FX_16ch Serdes`|`0x45`|
|`unused`|`0x46`|
|`8Khz clock out enable for Diagnostic`|`0x47`|
|`Configure Frequency of Faceplate_Serdeses RX CLK`|`0x48`|
|`Configure Clock monitor output`|`0x49`|
|`Configure MII test enable`|`0x4A`|
|`Configure Enable Clock Of SEM IP`|`0x4C`|
|`Configure Lost of Clock Threshold For 10Ge`|`0x4D`|
|`Configure Out of Frequency Threshold For 10Ge`|`0x4E`|
|`general purpose outputs from RX Uart`|`0x60`|
|`unused`|`0x61`|
|`Top Interface Alarm Sticky`|`0x50`|
|`Raw PRSB test for OC192_OC48_OC12_10G_1G_100FX_16ch Serdes Status`|`0x51`|
|`Clock Monitoring Status`|`0x46000 - 0x4601F`|
|`Clock Monitoring Status`|`0x43000 - 0x4301F`|
|`Clock Monitoring Status`|`0x47000 - 0x4701f`|
|`TOP Status Refout 8Khz ppm Measurement`|`0x6E-0x6F`|


###Device Product ID

* **Description**           

This register indicates Product ID.


* **RTL Instant Name**    : `ProductID`

* **Address**             : `0xF0_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`productid`| ProductId| `RO`| `0x60290021`| `0x60290021 End: Begin:`|

###Device Year Month Day Version ID

* **Description**           

This register indicates Year Month Day and main version ID.


* **RTL Instant Name**    : `YYMMDD_VerID`

* **Address**             : `0xF0_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`year`| Year| `RO`| `0x16`| `0x16`|
|`[23:16]`|`month`| Month| `RO`| `0x09`| `0x09`|
|`[15:8]`|`day`| Day| `RO`| `0x17`| `0x17`|
|`[7:0]`|`version`| Version| `RO`| `0x01`| `0x01 End: Begin:`|

###Device Internal ID

* **Description**           

This register indicates internal ID.


* **RTL Instant Name**    : `InternalID`

* **Address**             : `0xF0_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`internalid`| InternalID| `RO`| `0x1`| `0x1 End: Begin:`|

###general purpose inputs for TX Uart

* **Description**           

This is the global configuration register for the MIG Reset Control


* **RTL Instant Name**    : `o_reset_ctr`

* **Address**             : `0x5`

* **Formula**             : `0x5`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:5]`|`unused`| *n/a*| `RW`| `0x7`| `0x7`|
|`[4]`|`qdr1_reset`| Reset Control for QDR#1 0: Disable 1: Enable| `RW`| `0x1`| `0x1`|
|`[3]`|`ddr4_reset`| Reset Control for DDR#4 0: Disable 1: Enable| `RW`| `0x1`| `0x1`|
|`[2]`|`ddr3_reset`| Reset Control for DDR#3 0: Disable 1: Enable| `RW`| `0x1`| `0x1`|
|`[1]`|`ddr2_reset`| Reset Control for DDR#2 0: Disable 1: Enable| `RW`| `0x1`| `0x1`|
|`[0]`|`ddr1_reset`| Reset Control for DDR#1 0: Disable 1: Enable| `RW`| `0x1`| `0x1 End : Begin:`|

###general purpose inputs for TX Uart

* **Description**           

This is the global configuration register for the Global Serdes Control


* **RTL Instant Name**    : `o_control0`

* **Address**             : `0x40`

* **Formula**             : `0x40`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`eth_40g_tx_dup`| Duplicate TX for 2 eth 40G<br>{0} : disable duplicate <br>{1} : enable duplicate| `RW`| `0x0`| `0x0`|
|`[30:30]`|`fsm_eth_40g_rx_gsel`| global selection between external FSM-RXMAC and inernal selection<br>{0} : internal selection <br>{1} : external FSM rxmac selection| `RW`| `0x0`| `0x0`|
|`[29:29]`|`fsm_eth_40g_rx_isel`| internal selection<br>{0} : eth 40G 0 <br>{1} : eth 40G 1| `RW`| `0x0`| `0x0`|
|`[28:28]`|`fsm_card_pw_tx_gsel`| global selection between external FSM-CARD and inernal selection<br>{0} : internal card selection <br>{1} : external FSM card selection| `RW`| `0x0`| `0x0`|
|`[27:27]`|`fsm_card_pw_tx_isel`| internal selection<br>{0} : internal card enable tx PW <br>{1} : internal card disable tx PW| `RW`| `0x0`| `0x0`|
|`[26:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16:16]`|`multirate_sel`| select 2 two RX group of Multirate serdes to CORE<br>{0} : select group 0 (0-7) <br>{1} : select group 1 (8-15)| `RW`| `0x0`| `0x0`|
|`[15:08]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[07:00]`|`o_control0`| Uart TX 8-bit data out| `RW`| `0x0`| `0x0 End : Begin:`|

###Flow Control Mechanism for Ethernet pass-through Diagnostic Value

* **Description**           

This is the global configuration register for Flow Control Mechanism for Ethernet pass-through Diagnostic


* **RTL Instant Name**    : `o_control1`

* **Address**             : `0x41`

* **Formula**             : `0x41`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`flowctl_gr2`| FIFO status of Group 2| `RW`| `0x0`| `0x0`|
|`[15:00]`|`flowctl_gr1`| FIFO status of Group 1| `RW`| `0x0`| `0x0 End : Begin:`|

###Force Error for Flow Control Mechanism Diagnostic

* **Description**           

This is the global configuration Error register for Flow Control Mechanism for Ethernet pass-through Diagnostic


* **RTL Instant Name**    : `o_control2`

* **Address**             : `0x42`

* **Formula**             : `0x42`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1:1]`|`flowctl_diagen`| Ethernet pass through flow Control Diagnostic Enable 0: Normal operation, select engine flow control 1: Diagnostic operation, select diagnostic flow control| `RW`| `0x0`| `0x0`|
|`[0:0]`|`flowctl_force`| Force Error for Flow Control Mechanism Diagnostic 0: Release 1: Insert Error| `RW`| `0x0`| `0x0 End : Begin:`|

###Diagnostic Enable Control

* **Description**           

This is the global configuration register for the MIG (DDR/QDR) Diagnostic Control


* **RTL Instant Name**    : `o_control3`

* **Address**             : `0x43`

* **Formula**             : `0x43`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`sfp_ocn_eth_diagen`| Enable Daignostic for SFP_OCN_ETH #1-#16 (Per bit per port) 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[15:8]`|`tsi_mate_diagen`| Enable Daignostic for TSI_Mate #1-#8 (Per bit per port) 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[7:5]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[4]`|`ddr4_diagen`| Enable Daignostic for DDR#2 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[3]`|`ddr3_diagen`| Enable Daignostic for DDR#1 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[2]`|`ddr2_diagen`| Enable Daignostic for DDR#2 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[1]`|`ddr1_diagen`| Enable Daignostic for DDR#1 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[0]`|`qdr1_diagen`| Enable Daignostic for QDR#1 0: Disable 1: Enable| `RW`| `0x0`| `0x0 End : Begin:`|

###OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Bus Type port 1 to 8

* **Description**           

This is the global configuration register for the Global Serdes Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag port 1 to 8


* **RTL Instant Name**    : `o_control4`

* **Address**             : `0x44`

* **Formula**             : `0x44`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`bus_type_sel8`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 8 0: OC192 1: XFI 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[27:24]`|`bus_type_sel7`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 7 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[23:20]`|`bus_type_sel6`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 6 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[19:16]`|`bus_type_sel5`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 5 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[15:12]`|`bus_type_sel4`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 4 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[11:8]`|`bus_type_sel3`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 3 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[7:4]`|`bus_type_sel2`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 2 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[3:0]`|`bus_type_sel1`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 1 0: OC192 1: XFI 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0 End : Begin:`|

###Raw PRSB test for OC192_OC48_OC12_10G_1G_100FX_16ch Serdes

* **Description**           

This is the TOP global configuration register unused


* **RTL Instant Name**    : `o_control5`

* **Address**             : `0x45`

* **Formula**             : `0x45`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`prbs_ier`| Insert Error for Raw PRBS for port 16 to port 1 (per port per bit) 1: Insert 0: Normal| `RW`| `0x0`| `0x0`|
|`[15:0]`|`prbs_en`| Enable test for Raw PRBS ( Serdes should init mode OC192 )for port 16 to port 1 (per port per bit) 1: Enable (Must config PRBS Bus_Type_Sel at  o_control4) 0: Disable| `RW`| `0x0`| `0x0 End : Begin:`|

###unused

* **Description**           

This is the TOP global configuration registerunused


* **RTL Instant Name**    : `o_control6`

* **Address**             : `0x46`

* **Formula**             : `0x46`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`bus_type_sel16`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 16 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[27:24]`|`bus_type_sel15`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 15 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[23:20]`|`bus_type_sel14`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 14 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[19:16]`|`bus_type_sel13`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 13 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[15:12]`|`bus_type_sel12`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 12 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[11:8]`|`bus_type_sel11`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 11 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[7:4]`|`bus_type_sel10`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 10 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0`|
|`[3:0]`|`bus_type_sel9`| Configure Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 9 0: unused 1: unused 2: OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31| `RW`| `0x0`| `0x0 End : Begin:`|

###8Khz clock out enable for Diagnostic

* **Description**           

This is the TOP global configuration register


* **RTL Instant Name**    : `o_control7`

* **Address**             : `0x47`

* **Formula**             : `0x47`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:25]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[24]`|`cfgref2mod`| Configure refout2 is output from rxlclk of Faceplate_Serdeses or refout2 is PLL lock status<br>{0} :from rxlclk of Faceplate_Serdeses <br>{1} :refout2 is System PLL locked status| `RW`| `0x0`| `0x0`|
|`[23:20]`|`cfgref2pid`| Configure refout2 source from Faceplate_Serdeses value 0 to 15 for 16 port| `RW`| `0x0`| `0x0`|
|`[19:16]`|`cfgref1pid`| Configure refout1 source from Faceplate_Serdeses value 0 to 15 for 16 port| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfgdiagen`| select diag refout8k or user refout8k from core,bit per sub port,<br>{0} : user refout8k from core <br>{1} : select diag refout8k from RX clock of Faceplate_Serdeses| `RW`| `0x0`| `0x0 End : Begin:`|

###Configure Frequency of Faceplate_Serdeses RX CLK

* **Description**           

This is the TOP global configuration register


* **RTL Instant Name**    : `o_control8`

* **Address**             : `0x48`

* **Formula**             : `0x48`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfgrate`| Configure rate for Faceplate Serdeses RX CLK  2 bit per sub port <exp : [1:0] for port 0.....[31:30] for port 15><br>{0x0} : 38.88  Mhz (STM4) <br>{0x1} : 62.5   Mhz (1G/100FX) <br>{0x2} : 155.52 Mhz (STM64/STM16) <br>{0x3} : 156.25 Mhz (10GE/XFI)| `RW`| `0x0`| `0x0 End : Begin:`|

###Configure Clock monitor output

* **Description**           

This is the TOP global configuration register


* **RTL Instant Name**    : `o_control9`

* **Address**             : `0x49`

* **Formula**             : `0x49`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[04:00]`|`cfgrefpid`| Configure Source Clock monitor output for clock_mon<br>{0x0} : spare_serdes_refclk_div2 <br>{0x1} : overhead_refclk_clk_div2 <br>{0x2} : ocn_eth_155p52_refclk#0_div2 <br>{0x3} : ocn_eth_155p52_refclk#1_div2 <br>{0x4} : ocn_eth_156p25_refclk#0_div2 <br>{0x5} : ocn_eth_156p25_refclk#1_div2 <br>{0x6} : eth_40g_refclk#0_div2 <br>{0x7} : eth_40g_refclk#1_div2 <br>{0x8} : qdr_refclk_div2 <br>{0x9} : ddr4_refclk#1_div2 <br>{0xa} : ddr4_refclk#2_div2 <br>{0xb} : ddr4_refclk#3_div2 <br>{0xc} : ddr4_refclk#4_div2 <br>{0xd} : pcie_refclk_div2 <br>{0xe} : refin_prc <br>{0xf} : ext_ref_timing <br>{0x10}: System PLL 311.04Mhz output <br>{0x11-0x1F}: unused clock_mon is low| `RW`| `0x0`| `0x0 End : Begin:`|

###Configure MII test enable

* **Description**           

This is the TOP global configuration register


* **RTL Instant Name**    : `o_control10`

* **Address**             : `0x4A`

* **Formula**             : `0x4A`

* **Where**               : 

* **Width**               : `4`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`channel0_testen`| Set 1 to enable test MII channel 0| `R/W`| `0x0`| `0x0`|
|`[1]`|`channel1_testen`| Set 1 to enable test MII channel 1| `R/W`| `0x0`| `0x0`|
|`[2]`|`channel2_testen`| Set 1 to enable test MII channel 2| `R/W`| `0x0`| `0x0`|
|`[3]`|`channel3_testen`| Set 1 to enable test MII channel 3| `R/W`| `0x0`| `0x0 End : Begin:`|

###Configure Enable Clock Of SEM IP

* **Description**           

This is the TOP global configuration register


* **RTL Instant Name**    : `o_control12`

* **Address**             : `0x4C`

* **Formula**             : `0x4C`

* **Where**               : 

* **Width**               : `1`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`conf_enb_clk_sem`| Set 1 to enable clock of SEM IP| `R/W`| `0x0`| `0x0 End : Begin:`|

###Configure Lost of Clock Threshold For 10Ge

* **Description**           

This is the TOP global configuration register


* **RTL Instant Name**    : `o_control13`

* **Address**             : `0x4D`

* **Formula**             : `0x4D`

* **Where**               : 

* **Width**               : `28`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`lost_clock_thres_10g`| This value will be multiply PPM value by 156| `R/W`| `0x0`| `0x0 End : Begin:`|

###Configure Out of Frequency Threshold For 10Ge

* **Description**           

This is the TOP global configuration register


* **RTL Instant Name**    : `o_control14`

* **Address**             : `0x4E`

* **Formula**             : `0x4E`

* **Where**               : 

* **Width**               : `28`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`out_of_freq_thres_10g`| This value will be multiply PPM value by 156| `R/W`| `0x0`| `0x0 End : Begin:`|

###general purpose outputs from RX Uart

* **Description**           

This is value output from RX Uart


* **RTL Instant Name**    : `c_uart`

* **Address**             : `0x60`

* **Formula**             : `0x60`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`fsm_rxmac`| 0: external FSM pin tongle rate 1Mhz to select first mac40g 1: external FSM pin tongle rate 2Mhz to select second mac40g| `RW`| `0x0`| `0x0`|
|`[30:30]`|`fsm_rxmac_cur`| 0: current selected first mac40g 1: current selected second mac40g| `RW`| `0x0`| `0x0`|
|`[29:29]`|`fsm_txpw`| 0: external FSM pin tongle rate 1Mhz to active card 1: external FSM pin tongle rate 2Mhz to standby card| `RW`| `0x0`| `0x0`|
|`[28:28]`|`fsm_txpw_cur`| 0: current card is in active state 1: current card is in standby state| `RW`| `0x0`| `0x0`|
|`[27:08]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[07:00]`|`i_status0`| Uart RX 8-bit data in| `RO`| `0x0`| `0x0 End : Begin:`|

###unused

* **Description**           




* **RTL Instant Name**    : `unused`

* **Address**             : `0x61`

* **Formula**             : `0x61`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End : Begin:`|

###Top Interface Alarm Sticky

* **Description**           

Top Interface Alarm Sticky


* **RTL Instant Name**    : `i_sticky0`

* **Address**             : `0x50`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:26]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[25]`|`ot_out`| Set 1 When Over-Temperature alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[24]`|`vbram_alarm_out`| Set 1 When VCCBRAM-sensor alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[23]`|`vccaux_alarm_out`| Set 1 When VCCAUX-sensor alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[22]`|`vccint_alarm_out`| Set 1 When VCCINT-sensor alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[21]`|`user_temp_alarm_out`| Set 1 When Temperature-sensor alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[20]`|`alarm_out`| Set 1 When SysMon Error Happens.| `W1C`| `0x0`| `0x0`|
|`[19:17]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[16]`|`systempll`| Set 1 while PLL Locked state change event happens when System PLL not locked.| `W1C`| `0x0`| `0x0`|
|`[15:13]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[12]`|`qdrurst`| Set 1 while user Reset state Change Happens when QDR   Reset User.| `W1C`| `0x0`| `0x0`|
|`[11]`|`ddr44urst`| Set 1 while user Reset State Change Happens When DDR#4 Reset User.| `W1C`| `0x0`| `0x0`|
|`[10]`|`ddr43urst`| Set 1 while user Reset State Change Happens When DDR#3 Reset User.| `W1C`| `0x0`| `0x0`|
|`[9]`|`ddr42urst`| Set 1 while user Reset State Change Happens When DDR#2 Reset User.| `W1C`| `0x0`| `0x0`|
|`[8]`|`ddr41urst`| Set 1 while user Reset State Change Happens When DDR#1 Reset User.| `W1C`| `0x0`| `0x0`|
|`[7:5]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[4]`|`qdrcalib`| Set 1 while Calib state change event happens when QDR Calib Fail.| `W1C`| `0x0`| `0x0`|
|`[3]`|`ddr44calib`| Set 1 while Calib State Change Event Happens When DDR#4 Calib Fail.| `W1C`| `0x0`| `0x0`|
|`[2]`|`ddr43calib`| Set 1 while Calib State Change Event Happens When DDR#3 Calib Fail.| `W1C`| `0x0`| `0x0`|
|`[1]`|`ddr42calib`| Set 1 while Calib State Change Event Happens When DDR#2 Calib Fail.| `W1C`| `0x0`| `0x0`|
|`[0]`|`ddr41calib`| Set 1 while Calib State Change Event Happens When DDR#1 Calib Fail.| `W1C`| `0x0`| `0x0 End : Begin:`|

###Raw PRSB test for OC192_OC48_OC12_10G_1G_100FX_16ch Serdes Status

* **Description**           

Top Interface Alarm Sticky


* **RTL Instant Name**    : `i_sticky1`

* **Address**             : `0x51`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[15:0]`|`prbs_sta`| Per bit Set 1 while PRBS Monitor not syn Event Happens(per port per bit).| `W1C`| `0x0`| `0x0 End : Begin:`|

###Clock Monitoring Status

* **Description**           



0x1f : mate_txclk[7]        (155.52 Mhz)

0x1e : mate_txclk[6]        (155.52 Mhz)

0x1d : mate_txclk[5]        (155.52 Mhz)

0x1c : mate_txclk[4]        (155.52 Mhz)

0x1b : mate_txclk[3]        (155.52 Mhz)

0x1a : mate_txclk[2]        (155.52 Mhz)

0x19 : mate_txclk[1]        (155.52 Mhz)

0x18 : mate_txclk[0]        (155.52 Mhz)

0x17 : mate_rxclk[7]        (155.52 Mhz)

0x16 : mate_rxclk[6]        (155.52 Mhz)

0x15 : mate_rxclk[5]        (155.52 Mhz)

0x14 : mate_rxclk[4]        (155.52 Mhz)

0x13 : mate_rxclk[3]        (155.52 Mhz)

0x12 : mate_rxclk[2]        (155.52 Mhz)

0x11 : mate_rxclk[1]        (155.52 Mhz)

0x10 : mate_rxclk[0]        (155.52 Mhz)

0x0f : unused               (0 Mhz)

0x0e : ext_ref clock        (19.44 Mhz)

0x0d : prc_ref clock        (19.44 Mhz)

0x0c : spgmii_clk           (125 Mhz)

0x0b : kbgmii_clk           (125 Mhz)

0x0a : dccgmii_clk          (125 Mhz)

0x09 : eth_40g_1_rxclk      (312.5 Mhz)

0x08 : eth_40g_0_rxclk      (312.5 Mhz)

0x07 : eth_40g_1_rxclk      (312.5 Mhz)

0x06 : eth_40g_0_rxclk      (312.5 Mhz)

0x05 : DDR4#4 user clock    (300 Mhz)

0x04 : DDR4#3 user clock    (300 Mhz)

0x03 : DDR4#2 user clock    (300 Mhz)

0x02 : DDR4#1 user clock    (300 Mhz)

0x01 : qdr1_clk user clock  (250 Mhz)

0x00 : pcie_upclk clock     (62.5 Mhz)


* **RTL Instant Name**    : `clock_mon_high`

* **Address**             : `0x46000 - 0x4601F`

* **Formula**             : `0x46000 + port_id`

* **Where**               : 

    * `$port_id(0-31`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`i_statusx`| Clock Value Monitor Change from hex format to DEC format to get Clock Value| `RO`| `0x0`| `0x0 End : Begin:`|

###Clock Monitoring Status

* **Description**           



0x1f : serdes_multi_rate_txclk[15] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x1e : serdes_multi_rate_txclk[14] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x1d : serdes_multi_rate_txclk[13] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x1c : serdes_multi_rate_txclk[12] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x1b : serdes_multi_rate_txclk[11] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x1a : serdes_multi_rate_txclk[10] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x19 : serdes_multi_rate_txclk[9]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x18 : serdes_multi_rate_txclk[8]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x17 : serdes_multi_rate_txclk[7]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x16 : serdes_multi_rate_txclk[6]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x15 : serdes_multi_rate_txclk[5]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x14 : serdes_multi_rate_txclk[4]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x13 : serdes_multi_rate_txclk[3]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x12 : serdes_multi_rate_txclk[2]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x11 : serdes_multi_rate_txclk[1]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x10 : serdes_multi_rate_txclk[0]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x0f : serdes_multi_rate_rxclk[15] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x0e : serdes_multi_rate_rxclk[14] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x0d : serdes_multi_rate_rxclk[13] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x0c : serdes_multi_rate_rxclk[12] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x0b : serdes_multi_rate_rxclk[11] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x0a : serdes_multi_rate_rxclk[10] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x09 : serdes_multi_rate_rxclk[9]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x08 : serdes_multi_rate_rxclk[8]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x07 : serdes_multi_rate_rxclk[7]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x06 : serdes_multi_rate_rxclk[6]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x05 : serdes_multi_rate_rxclk[5]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x04 : serdes_multi_rate_rxclk[4]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x03 : serdes_multi_rate_rxclk[3]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x02 : serdes_multi_rate_rxclk[2]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x01 : serdes_multi_rate_rxclk[1]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

0x00 : serdes_multi_rate_rxclk[0]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)


* **RTL Instant Name**    : `clock_mon_highclock_mon_multi`

* **Address**             : `0x43000 - 0x4301F`

* **Formula**             : `0x43000 + port_id`

* **Where**               : 

    * `$port_id(0-31`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`i_statusx`| Clock Value Monitor Change from hex format to DEC format to get Clock Value| `RO`| `0x0`| `0x0 End : Begin:`|

###Clock Monitoring Status

* **Description**           



0x1f : unused (0 Mhz)

0x1e : unused (0 Mhz)

0x1d : pm_tick              (1 Hz) for get value frequency SW must use : F = 155520000/register value

0x1c : one_pps              (1 Hz) for get value frequency SW must use : F = 155520000/register value

0x1b : unused (0 Mhz)

0x1a : unused (0 Mhz)

0x19 : unused (0 Mhz)

0x18 : unused (0 Mhz)

0x17 : unused (0 Mhz)

0x16 : unused (0 Mhz)

0x15 : refout8k_2 (0 Mhz)

0x14 : refout8k_1 (0 Mhz)

0x13 : fsm_pcp[2]			 (1 Mhz or 2 Mhz  frequency clk Diagnostic)

0x12 : fsm_pcp[1]			 (1 Mhz or 2 Mhz  frequency clk Diagnostic)

0x11 : fsm_pcp[0]			 (1 Mhz or 2 Mhz  frequency clk Diagnostic)

0x10 : spare_gpio[13]       (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x0f : spare_gpio[12]       (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x0e : spare_gpio[11]       (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x0d : spare_gpio[10]       (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x0c : spare_gpio[9]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x0b : spare_gpio[8]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x0a : spare_gpio[7]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x09 : spare_gpio[6]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x08 : spare_gpio[5]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x07 : spare_gpio[4]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x06 : spare_gpio[3]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x05 : spare_gpio[2]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x04 : spare_gpio[1]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x03 : spare_gpio[0]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x02 : spare_clk[3]         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x01 : spare_clk[2]         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x00 : spare_clk0 clock     (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)


* **RTL Instant Name**    : `clock_mon_slow`

* **Address**             : `0x47000 - 0x4701f`

* **Formula**             : `0x47000 + port_id`

* **Where**               : 

    * `$port_id(0-31`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`i_statusx`| Clock Value Monitor Change from hex format to DEC format to get Clock Value| `RO`| `0x0`| `0x0 End : Begin:`|

###TOP Status Refout 8Khz ppm Measurement

* **Description**           

Top Interface status Refout 8Khz ppm Measurement


* **RTL Instant Name**    : `i_status`

* **Address**             : `0x6E-0x6F`

* **Formula**             : `0x6E +  PWID`

* **Where**               : 

    * `$RefoutID(0-1): Refout 8Khz ID`

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[23:0]`|`refppmcycle`| Number if system clock in 200 cycle 8Khz. SW calculate ppm by below formalar: Number of real system clock cycle in 200 cycle 8Khz: SYSPPMCYCLE = (194400*200)/8 = 4860000 if (RefPpmCycle > SYSPPMCYCLE) then NEGATIVE_PPM = (Ref1PpmCycle - SYSPPMCYCLE)*1000000/SYSPPMCYCLE else  POSITIVE_PPM = (SYSPPMCYCLE - RefPpmCycle)*1000000/SYSPPMCYCLE| `RO`| `0x0`| `0x0 End :`|

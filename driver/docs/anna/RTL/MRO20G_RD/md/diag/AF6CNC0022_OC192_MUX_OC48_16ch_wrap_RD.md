## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_OC192_MUX_OC48_16ch_wrap_RD
####Register Table

|Name|Address|
|-----|-----|
|`OC192 MUX OC48 Change Mode`|`0x0080-0x6080`|
|`OC192 MUX OC48 Current Mode`|`0x0090-0x6090`|
|`OC192 MUX OC48 DRP`|`0x1000-0x7FFF`|
|`OC192 MUX OC48 LoopBack`|`0x0002-0x6002`|
|`Async GearBox Enable of 10Ge mode`|`0x107C-0x7C7C`|
|`OC192 MUX OC48 PLL Status`|`0x000B-0x600B`|
|`OC192 MUX OC48 TX Reset`|`0x000C-0x600C`|
|`OC192 MUX OC48 RX Reset`|`0x000D-0x600D`|
|`OC192 MUX OC48 LPMDFE Mode`|`0x000E-0x600E`|
|`OC192 MUX OC48 LPMDFE Reset`|`0x000F-0x600F`|
|`OC192 MUX OC48 TXDIFFCTRL`|`0x0010-0x6010`|
|`OC192 MUX OC48 TXPOSTCURSOR`|`0x0011-0x6011`|
|`OC192 MUX OC48 TXPRECURSOR`|`0x0012-0x6012`|
|`SERDES POWER DOWN`|`0x0014-0x6014`|
|`Force Running Disparity Error`|`0x0015-0x6015`|
|`RX CDR Lock to Reference`|`0x0017-0x6017`|
|`RX Monitor PPM Ref Value`|`0x001C-0x601C`|
|`RX Monitor PPM Mon Value`|`0x001D-0x601D`|
|`RX Monitor PPM Control`|`0x001E-0x601E`|
|`RX Auto Reset Masking Data`|`0x001F-0x601F`|
|`RX RXLPMHFOVRDEN Control`|`0x0018-0x6018`|
|`RX RXLPMLFKLOVRDEN Control`|`0x0019-0x6019`|
|`RX RXOSOVRDEN Control`|`0x001A-0x601A`|
|`RX RXLPMOSHOLD Control`|`0x0030-0x6030`|
|`RX RXLPMOSOVRDEN Control`|`0x0031-0x6031`|
|`RX RXLPM_OS_CFG1 Control`|`0x1038-0x7C38`|


###OC192 MUX OC48 Change Mode

* **Description**           

Configurate Port mode, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_Change_Mode`

* **Address**             : `0x0080-0x6080`

* **Formula**             : `0x0080+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`chg_done`| Change mode has done<br>{1} : Done| `W1C`| `0x0`| `0x0`|
|`[11:09]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[08:08]`|`chg_trig`| Trigger 0->1 to start changing mode| `R/W`| `0x0`| `0x0`|
|`[07:04]`|`chg_mode`| Port mode<br>{0} : OC192 <br>{1} : OC48 <br>{2} : OC12 <br>{4} : 10Ge <br>{5} : 1Ge <br>{6} : 100Fx| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`chg_port`| Sub Port ID<br>{0-3} : Group0 => Port 0-3 <br>{0-3} : Group1 => Port 4-7 <br>{0-3} : Group2 => Port 8-11 <br>{0-3} : Group3 => Port 12-15| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 Current Mode

* **Description**           

Current Port mode, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_Current_Mode`

* **Address**             : `0x0090-0x6090`

* **Formula**             : `0x0090+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`cur_mode_subport3`| Current mode subport3, Group 0 => Port3, Group 1 => Port 7, Group 2 => Port 11, Group 3 => Port 15<br>{0} : OC192 <br>{1} : OC48 <br>{2} : OC12 <br>{4} : 10Ge <br>{5} : 1Ge <br>{6} : 100Fx| `R_O`| `0x0`| `0x0`|
|`[11:08]`|`cur_mode_subport2`| Current mode subport2, Group 0 => Port2, Group 1 => Port 6, Group 2 => Port 10, Group 3 => Port 14<br>{0} : OC192 <br>{1} : OC48 <br>{2} : OC12 <br>{4} : 10Ge <br>{5} : 1Ge <br>{6} : 100Fx| `R_O`| `0x0`| `0x0`|
|`[07:04]`|`cur_mode_subport1`| Current mode subport1, Group 0 => Port1, Group 1 => Port 5, Group 2 => Port 9, Group 3 => Port 13<br>{0} : OC192 <br>{1} : OC48 <br>{2} : OC12 <br>{4} : 10Ge <br>{5} : 1Ge <br>{6} : 100Fx| `R_O`| `0x0`| `0x0`|
|`[03:00]`|`cur_mode_subport0`| Current mode subport0, Group 0 => Port0, Group 1 => Port 4, Group 2 => Port 8, Group 3 => Port 12<br>{0} : OC192 <br>{1} : OC48 <br>{2} : OC12 <br>{4} : 10Ge <br>{5} : 1Ge <br>{6} : 100Fx| `R_O`| `0x0`| `0x0`|

###OC192 MUX OC48 DRP

* **Description**           

Read/Write DRP address of SERDES, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_DRP`

* **Address**             : `0x1000-0x7FFF`

* **Formula**             : `0x1000+$G*0x2000+$P*0x400+$DRP`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

    * `$P(0-3) : Sub Port ID`

    * `$DRP(0-1023) : DRP address, see UG578`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:00]`|`drp_rw`| DRP read/write value| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 LoopBack

* **Description**           

Configurate LoopBack, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_LoopBack`

* **Address**             : `0x0002-0x6002`

* **Formula**             : `0x0002+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`lpback_subport3`| Loopback subport 3, Group 0 => Port3, Group 1 => Port 7, Group 2 => Port 11, Group 3 => Port 15<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Remote Loopback far-end PMA, timing mode of far-end device has to configure internal mode , refer to reg "Async GearBox Enable of 10Ge mode" <br>{8} : Remote Loopback behind PCS, timing mode of far-end device has to configure recovery mode| `R/W`| `0x0`| `0x0`|
|`[11:08]`|`lpback_subport2`| Loopback subport 2, Group 0 => Port2, Group 1 => Port 6, Group 2 => Port 10, Group 3 => Port 14<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Remote Loopback far-end PMA, timing mode of far-end device has to configure internal mode , refer to reg "Async GearBox Enable of 10Ge mode" <br>{8} : Remote Loopback behind PCS, timing mode of far-end device has to configure recovery mode| `R/W`| `0x0`| `0x0`|
|`[07:04]`|`lpback_subport1`| Loopback subport 1, Group 0 => Port1, Group 1 => Port 5, Group 2 => Port 9, Group 3 => Port 13<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Remote Loopback far-end PMA, timing mode of far-end device has to configure internal mode, refer to reg "Async GearBox Enable of 10Ge mode" <br>{8} : Remote Loopback behind PCS, timing mode of far-end device has to configure recovery mode| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`lpback_subport0`| Loopback subport 0, Group 0 => Port0, Group 1 => Port 4, Group 2 => Port 8, Group 3 => Port 12<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Remote Loopback far-end PMA, timing mode of far-end device has to configure internal mode, refer to reg "Async GearBox Enable of 10Ge mode" <br>{8} : Remote Loopback behind PCS, timing mode of far-end device has to configure recovery mode| `R/W`| `0x0`| `0x0`|

###Async GearBox Enable of 10Ge mode

* **Description**           

Configurate Enable/Disable Async GearBox of 10Ge mode, Async GearBox has to be disable when using remote loopback far-end PMA


* **RTL Instant Name**    : `Async_GearBox_Enable_of_10Ge_mode`

* **Address**             : `0x107C-0x7C7C`

* **Formula**             : `0x107C+$G*0x2000`

* **Where**               : 

    * `$G(0,2) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[13:13]`|`async_gearbix_enb`| Eanble/disable async gearbox of 10Ge mode<br>{1} : Async GearBox 10Ge mode is normal operation <br>{0} : Disable async gearbox, it uses for remote loopback far-end PMA| `R/W`| `0x0`| `0x0`|
|`[12:00]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 PLL Status

* **Description**           

QPLL/CPLL status, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_PLL_Status`

* **Address**             : `0x000B-0x600B`

* **Formula**             : `0x000B+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:29]`|`qpll1_lock_change`| QPLL1 has transition lock/unlock, Group 0-3<br>{1} : QPLL1_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[28:28]`|`qpll0_lock_change`| QPLL0 has transition lock/unlock, Group 0-3<br>{1} : QPLL0_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[27:26]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[25:25]`|`qpll1_lock`| QPLL0 is Locked, Group 0-3<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[24:24]`|`qpll0_lock`| QPLL0 is Locked, Group 0-3<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[23:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`cpll_lock_change`| CPLL has transition lock/unlock, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15<br>{1} : CPLL_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[11:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`cpll_lock`| CPLL is Locked, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15<br>{1} : Locked| `R_O`| `0x0`| `0x0`|

###OC192 MUX OC48 TX Reset

* **Description**           

Reset TX SERDES, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_TX_Reset`

* **Address**             : `0x000C-0x600C`

* **Formula**             : `0x000C+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:16]`|`txrst_done`| TX Reset Done, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`txrst_trig`| Trige 0->1 to start reset TX SERDES, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 RX Reset

* **Description**           

Reset RX SERDES, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_RX_Reset`

* **Address**             : `0x000D-0x600D`

* **Formula**             : `0x000D+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:16]`|`rxrst_done`| RX Reset Done, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`rxrst_trig`| Trige 0->1 to start reset RX SERDES, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 LPMDFE Mode

* **Description**           

Configure LPM/DFE mode , there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_LPMDFE_Mode`

* **Address**             : `0x000E-0x600E`

* **Formula**             : `0x000E+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`lpmdfe_mode`| bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15<br>{0} : DFE mode <br>{1} : LPM mode| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 LPMDFE Reset

* **Description**           

Reset LPM/DFE , there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_LPMDFE_Reset`

* **Address**             : `0x000F-0x600F`

* **Formula**             : `0x000F+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`lpmdfe_reset`| bit per sub port, Must be toggled after switching between modes to initialize adaptation, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15<br>{1} : reset| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 TXDIFFCTRL

* **Description**           

Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_TXDIFFCTRL`

* **Address**             : `0x0010-0x6010`

* **Formula**             : `0x0010+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:15]`|`txdiffctrl_subport3`| Group 3 => Port 12-15| `R/W`| `0x18`| `0x0`|
|`[14:10]`|`txdiffctrl_subport2`| Group 2 => Port 8-11| `R/W`| `0x18`| `0x0`|
|`[09:05]`|`txdiffctrl_subport1`| Group 1 => Port 4-7| `R/W`| `0x18`| `0x0`|
|`[04:00]`|`txdiffctrl_subport0`| Group 0 => Port 0-3| `R/W`| `0x18`| `0x0`|

###OC192 MUX OC48 TXPOSTCURSOR

* **Description**           

Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_TXPOSTCURSOR`

* **Address**             : `0x0011-0x6011`

* **Formula**             : `0x0011+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:15]`|`txpostcursor_subport3`| Group 3 => Port 12-15| `R/W`| `0x0`| `0x0`|
|`[14:10]`|`txpostcursor_subport2`| Group 2 => Port 8-11| `R/W`| `0x0`| `0x0`|
|`[09:05]`|`txpostcursor_subport1`| Group 1 => Port 4-7| `R/W`| `0x0`| `0x0`|
|`[04:00]`|`txpostcursor_subport0`| Group 0 => Port 0-3| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 TXPRECURSOR

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_TXPRECURSOR`

* **Address**             : `0x0012-0x6012`

* **Formula**             : `0x0012+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:15]`|`txprecursor_subport3`| Group 3 => Port 12-15| `R/W`| `0x0`| `0x0`|
|`[14:10]`|`txprecursor_subport2`| Group 2 => Port 8-11| `R/W`| `0x0`| `0x0`|
|`[09:05]`|`txprecursor_subport1`| Group 1 => Port 4-7| `R/W`| `0x0`| `0x0`|
|`[04:00]`|`txprecursor_subport0`| Group 0 => Port 0-3| `R/W`| `0x0`| `0x0`|

###SERDES POWER DOWN

* **Description**           

TX/RX power down control, see "UG578 for more detail, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `SERDES_POWER_DOWN`

* **Address**             : `0x0014-0x6014`

* **Formula**             : `0x0014+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:07]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`txrxpd3`| Power Down subport 3<br>{0} : Nomoral <br>{1} : Power-down| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`txrxpd2`| Power Down subport 2<br>{0} : Nomoral <br>{1} : Power-down| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`txrxpd1`| Power Down subport 1<br>{0} : Nomoral <br>{1} : Power-down| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`txrxpd0`| Power Down subport 0<br>{0} : Nomoral <br>{1} : Power-down| `R/W`| `0x0`| `0x0`|

###Force Running Disparity Error

* **Description**           

Force running disparity error, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `Force_Running_Disparity_Error`

* **Address**             : `0x0015-0x6015`

* **Formula**             : `0x0015+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[07:06]`|`fdisp0_pid3`| Force running disparity error for port 3<br>{0} : Nomoral <br>{1/2}: Reserved <br>{3} : error running disparity| `R/W`| `0x0`| `0x0`|
|`[05:04]`|`fdisp0_pid2`| Force running disparity error for port 2<br>{0} : Nomoral <br>{1/2}: Reserved <br>{3} : error running disparity| `R/W`| `0x0`| `0x0`|
|`[03:02]`|`fdisp0_pid1`| Force running disparity error for port 1<br>{0} : Nomoral <br>{1/2}: Reserved <br>{3} : error running disparity| `R/W`| `0x0`| `0x0`|
|`[01:00]`|`fdisp0_pid0`| Force running disparity error for port 0<br>{0} : Nomoral <br>{1/2}: Reserved <br>{3} : error running disparity| `R/W`| `0x0`| `0x0`|

###RX CDR Lock to Reference

* **Description**           

RX CDR Lock to Reference, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `RX_CDR_Lock_to_Reference`

* **Address**             : `0x0017-0x6017`

* **Formula**             : `0x0017+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`lock2ref_pid3`| RX CDR Lock to Reference for port 3<br>{0} : Lock to data <br>{1} : Lock to ref| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`lock2ref_pid2`| RX CDR Lock to Reference for port 2<br>{0} : Lock to data <br>{1} : Lock to ref| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`lock2ref_pid1`| RX CDR Lock to Reference for port 1<br>{0} : Lock to data <br>{1} : Lock to ref| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`lock2ref_pid0`| RX CDR Lock to Reference for port 0<br>{0} : Lock to data <br>{1} : Lock to ref| `R/W`| `0x0`| `0x0`|

###RX Monitor PPM Ref Value

* **Description**           

RX Monitor PPM Ref Value, this reg is used to reset RX side base PPM difference between TX & RX, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `RX_Monitor_PPM_Ref_Value`

* **Address**             : `0x001C-0x601C`

* **Formula**             : `0x001C+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`ppmref_pid3`| RX Monitor PPM Ref Value for port 3| `R/W`| `0x64`| `0x64`|
|`[23:16]`|`ppmref_pid2`| RX Monitor PPM Ref Value for port 2| `R/W`| `0x64`| `0x64`|
|`[15:08]`|`ppmref_pid1`| RX Monitor PPM Ref Value for port 1| `R/W`| `0x64`| `0x64`|
|`[07:00]`|`ppmref_pid0`| RX Monitor PPM Ref Value for port 0| `R/W`| `0x64`| `0x64`|

###RX Monitor PPM Mon Value

* **Description**           

RX Monitor PPM Value, this reg is used to reset RX side base PPM difference between TX & RX, it will be compared to reg "RX Monitor PPM Ref Value", there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `RX_Monitor_PPM_Mon_Value`

* **Address**             : `0x001D-0x601D`

* **Formula**             : `0x001D+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`ppmval_pid3`| RX Monitor PPM Mon Value for port 3| `R_O`| `0x0`| `0x0`|
|`[23:16]`|`ppmval_pid2`| RX Monitor PPM Mon Value for port 2| `R_O`| `0x0`| `0x0`|
|`[15:08]`|`ppmval_pid1`| RX Monitor PPM Mon Value for port 1| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`ppmval_pid0`| RX Monitor PPM Mon Value for port 0| `R_O`| `0x0`| `0x0`|

###RX Monitor PPM Control

* **Description**           

RX Monitor PPM Control, this reg is used to reset RX side base PPM difference between TX & RX, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `RX_Monitor_PPM_Control`

* **Address**             : `0x001E-0x601E`

* **Formula**             : `0x001E+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[19:16]`|`ppmctrl_err`| RX Monitor PPM gets an error, bit per port of each group, bit0 is port0, bit3 is port3<br>{1} : PPM difference is greater than 100ppm| `W1C`| `0x0`| `0x0`|
|`[15:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[03:00]`|`ppmctrl_enb`| Enable monitor PPM difference, bit per port of each group, bit0 is port0, bit3 is port3<br>{1} : enable monitor PPM difference to reset RX side| `R/W`| `0xF`| `0xF`|

###RX Auto Reset Masking Data

* **Description**           

RX Monitor PPM Control, this reg is used to reset RX side base PPM difference between TX & RX, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `RX_Auto_Reset_Masking_Data`

* **Address**             : `0x001F-0x601F`

* **Formula**             : `0x001F+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[03:00]`|`auto_rst_disdat`| Masking RX DATA is 0 when auto-reset, bit per port of each group, bit0 is port0, bit3 is port3<br>{1} : enable masking| `R/W`| `0xF`| `0xF`|

###RX RXLPMHFOVRDEN Control

* **Description**           

RX RXLPMHFOVRDEN Control is used for lock-to-ref mode, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `RX_RXLPMHFOVRDEN_Control`

* **Address**             : `0x0018-0x6018`

* **Formula**             : `0x0018+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[03:00]`|`rxlpmhfovrden`| RX RXLPMHFOVRDEN Control, bit per port of each group, bit0 is port0, bit3 is port3<br>{1} : CDR uses lock-to-ref mode <br>{0} : CDR uses lock-to-data mode| `R/W`| `0x0`| `0x0`|

###RX RXLPMLFKLOVRDEN Control

* **Description**           

RX RXLPMLFKLOVRDEN Control is used for lock-to-ref mode, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `RX_RXLPMLFKLOVRDEN_Control`

* **Address**             : `0x0019-0x6019`

* **Formula**             : `0x0019+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[03:00]`|`rxlpmlfklovrden`| RX RXLPMLFKLOVRDEN Control, bit per port of each group, bit0 is port0, bit3 is port3<br>{1} : CDR uses lock-to-ref mode <br>{0} : CDR uses lock-to-data mode| `R/W`| `0x0`| `0x0`|

###RX RXOSOVRDEN Control

* **Description**           

RX RXOSOVRDEN Control is used for lock-to-ref mode, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `RX_RXOSOVRDEN_Control`

* **Address**             : `0x001A-0x601A`

* **Formula**             : `0x001A+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[03:00]`|`rxosovrden`| RX RXOSOVRDEN Control, bit per port of each group, bit0 is port0, bit3 is port3<br>{1} : CDR uses lock-to-ref mode <br>{0} : CDR uses lock-to-data mode| `R/W`| `0x0`| `0x0`|

###RX RXLPMOSHOLD Control

* **Description**           

RX RXLPMOSHOLD Control, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `RX_RXLPMOSHOLD_Control`

* **Address**             : `0x0030-0x6030`

* **Formula**             : `0x0030+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[03:00]`|`rxlpmoshold`| RX RXLPMOSHOLD Control, bit per port of each group, bit0 is port0, bit3 is port3, refer to UG578 for usage| `R/W`| `0x0`| `0x0`|

###RX RXLPMOSOVRDEN Control

* **Description**           

RX RXLPMOSOVRDEN Control, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `RX_RXLPMOSOVRDEN_Control`

* **Address**             : `0x0031-0x6031`

* **Formula**             : `0x0031+$G*0x2000`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[03:00]`|`rxlpmosovrden`| RX RXLPMOSOVRDEN Control, bit per port of each group, bit0 is port0, bit3 is port3, refer to UG578 for usage| `R/W`| `0x0`| `0x0`|

###RX RXLPM_OS_CFG1 Control

* **Description**           

RX RXLPM_OS_CFG1 Control, there are 4 groups (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `RX_RXLPM_OS_CFG1_Control`

* **Address**             : `0x1038-0x7C38`

* **Formula**             : `0x1038+$G*0x2000+$P*400`

* **Where**               : 

    * `$G(0-3) : Group of Ports,$P(0-3) : Port of each group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[15:00]`|`rxlpm_os_cfg1`| RX RXLPM_OS_CFG1 Control, refer to UG578 for usage| `R/W`| `0x0`| `0x0`|

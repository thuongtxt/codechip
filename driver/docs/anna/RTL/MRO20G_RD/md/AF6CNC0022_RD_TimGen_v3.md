## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_TimGen_v3
####Register Table

|Name|Address|
|-----|-----|
|`CDR CPU  Reg Hold Control`|`0x030000-0x030002`|
|`CDR line mode control`|`0x00000200 - 0x0000022F`|
|`CDR Timing External reference control`|`0x0000003`|
|`CDR STS Timing control`|`0x0001000-0x000102F`|
|`CDR VT Timing control`|`0x0001800-0x0001FFF`|
|`CDR ToP Timing Frame sync 8K control`|`0x0001040`|
|`CDR Reference Sync 8K Master Output control`|`0x0001041`|
|`CDR Reference Sync 8k Slaver Output control`|`0x0001042`|
|`RAM TimingGen Parity Force Control`|`0x104c`|
|`RAM TimingGen Parity Disable Control`|`0x104d`|
|`RAM TimingGen parity Error Sticky`|`0x104e`|
|`Read HA Address Bit3_0 Control`|`0x30200`|
|`Read HA Address Bit7_4 Control`|`0x30210`|
|`Read HA Address Bit11_8 Control`|`0x30220`|
|`Read HA Address Bit15_12 Control`|`0x30230`|
|`Read HA Address Bit19_16 Control`|`0x30240`|
|`Read HA Address Bit23_20 Control`|`0x30250`|
|`Read HA Address Bit24 and Data Control`|`0x30260`|
|`Read HA Hold Data63_32`|`0x30270`|
|`Read HA Hold Data95_64`|`0x30271`|
|`Read HA Hold Data127_96`|`0x30272`|


###CDR CPU  Reg Hold Control

* **Description**           

The register provides hold register for three word 32-bits MSB when CPU access to engine.


* **RTL Instant Name**    : `cdr_cpu_hold_ctrl`

* **Address**             : `0x030000-0x030002`

* **Formula**             : `0x030000 + HoldId`

* **Where**               : 

    * `$HoldId(0-2): Hold register`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdreg0`| Hold 32 bits| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR line mode control

* **Description**           

The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1.


* **RTL Instant Name**    : `cdr_line_mode_ctrl`

* **Address**             : `0x00000200 - 0x0000022F`

* **Formula**             : `0x00000200 +  STS`

* **Where**               : 

    * `$STS(0-47): HDL_PATH     : engidcnt.vtcnt.stsctrlbuf.array.ram[$STS]`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9]`|`vc3mode`| VC3 or TU3 mode, each bit is used configured for each STS (only valid in TU3 CEP) 1: TU3 CEP 0: Other mode| `RW`| `0x0`| `0x0`|
|`[8]`|`de3mode`| DS3 or E3 mode, each bit is used configured for each STS. 1: DS3 mode 0: E3 mod| `RW`| `0x0`| `0x0`|
|`[7]`|`sts1stsmode`| STS mode of STS1 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[6:0]`|`sts1vttype`| VT Type of 7 VTG in STS1, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Timing External reference control

* **Description**           

This register is used to configure for the 2Khz coefficient of two external reference signal in order to generate timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)


* **RTL Instant Name**    : `cdr_timing_extref_ctrl`

* **Address**             : `0x0000003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:32]`|`ext3n2k`| The 2Khz coefficient of the third external reference signal| `RW`| `0x0`| `0x0`|
|`[31:16]`|`ext2n2k`| The 2Khz coefficient of the second external reference signal| `RW`| `0x0`| `0x0`|
|`[15:0]`|`ext1n2k`| The 2Khz coefficient of the first external reference signal| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR STS Timing control

* **Description**           

This register is used to configure timing mode for per STS


* **RTL Instant Name**    : `cdr_sts_timing_ctrl`

* **Address**             : `0x0001000-0x000102F`

* **Formula**             : `0x0001000+STS`

* **Where**               : 

    * `$STS(0-47): HDL_PATH     : engidcnt.timgen.ram1.array.ram[$STS]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[29:20]`|`cdrchid`| CDR channel ID in CDR mode.| `RW`| `0x0`| `0x0`|
|`[17]`|`vc3eparen`| VC3 EPAR mode 0 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[16]`|`mapstsmode`| Map STS mode 0: Payload STS mode 1: Payload DE3 mode| `RW`| `0x0`| `0x0`|
|`[7:4]`|`de3timemode`| DE3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode 9: OCN System timing mode 10: DCR timing mode| `RW`| `0x0`| `0x0`|
|`[3:0]`|`vc3timemode`| VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP mode| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR VT Timing control

* **Description**           

This register is used to configure timing mode for per VT


* **RTL Instant Name**    : `cdr_vt_timing_ctrl`

* **Address**             : `0x0001800-0x0001FFF`

* **Formula**             : `0x0001800 + STS*32 + TUG*4 + VT`

* **Where**               : 

    * `$STS(0-47):`

    * `$TUG(0 -6):`

    * `$VT(0-3): (0-2)in E1 and (0-3) in DS1 HDL_PATH     : engidcnt.timgen.ram2.array.ram[$STS*32 + $TUG*4 + $VT]`

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[21:12]`|`cdrchid`| CDR channel ID in CDR mode| `RW`| `0x0`| `0x0`|
|`[9]`|`vteparen`| VT EPAR mode 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[8]`|`mapvtmode`| Map VT mode 0: Payload VT15/VT2 mode 1: Payload DS1/E1 mode| `RW`| `0x0`| `0x0`|
|`[7:4]`|`de1timemode`| DE1 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode 9: OCN System timing mode 10: DCR timing mode| `RW`| `0x0`| `0x0`|
|`[3:0]`|`vttimemode`| VT time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP mode| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR ToP Timing Frame sync 8K control

* **Description**           

This register is used to configure timing source to generate the 125us signal frame sync for ToP


* **RTL Instant Name**    : `cdr_top_timing_frmsync_ctrl`

* **Address**             : `0x0001040`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[18:16]`|`toppdhlinetype`| Line type of DS1/E1 loop time or PDH CDR 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1 7: Reserve| `RW`| `0x0`| `0x0`|
|`[13:4]`|`toppdhlineid`| Line ID of DS1/E1 loop time or PDH/CEP CDR| `RW`| `0x0`| `0x0`|
|`[3:0]`|`toptimemode`| Time mode  for ToP 0: System timing mode 1: DS1/E1 Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: PDH CDR timing mode 9: OCN System timing mode| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Reference Sync 8K Master Output control

* **Description**           

This register is used to configure timing source to generate the reference sync master output signal


* **RTL Instant Name**    : `cdr_refsync_master_octrl`

* **Address**             : `0x0001041`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[18:16]`|`refout1pdhlinetype`| Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1 7: Reserve| `RW`| `0x0`| `0x0`|
|`[13:4]`|`refout1pdhlineid`| Line ID of DS1/E1 loop time or PDH CDR| `RW`| `0x0`| `0x0`|
|`[3:0]`|`refout1timemode`| Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: PDH CDR timing mode 9: External Clock sync| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Reference Sync 8k Slaver Output control

* **Description**           

This register is used to configure timing source to generate the reference sync slaver output signal


* **RTL Instant Name**    : `cdr_refsync_slaver_octrl`

* **Address**             : `0x0001042`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[18:16]`|`refout2pdhlinetype`| Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1 7: Reserve| `RW`| `0x0`| `0x0`|
|`[13:4]`|`refout2pdhlineid`| Line ID of DS1/E1 loop time or PDH CDR| `RW`| `0x0`| `0x0`|
|`[3:0]`|`refout2timemode`| Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: PDH CDR timing mode 9: External Clock sync| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM TimingGen Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_TimingGen_Parity_Force_Control`

* **Address**             : `0x104c`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`cdrvttimingctrl_parerrfrc`| Force parity For RAM Control "CDR VT Timing control"| `RW`| `0x0`| `0x0`|
|`[0]`|`cdrststimingctrl_parerrfrc`| Force parity For RAM Control "CDR STS Timing control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM TimingGen Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_TimingGen_Parity_Disbale_Control`

* **Address**             : `0x104d`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`cdrvttimingctrl_parerrdis`| Disable parity For RAM Control "CDR VT Timing control"| `RW`| `0x0`| `0x0`|
|`[0]`|`cdrststimingctrl_parerrdis`| Disable parity For RAM Control "CDR STS Timing control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM TimingGen parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_TimingGen_Parity_Error_Sticky`

* **Address**             : `0x104e`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`cdrvttimingctrl_parerrstk`| Error parity For RAM Control "CDR VT Timing control"| `W1C`| `0x0`| `0x0`|
|`[0]`|`cdrststimingctrl_parerrstk`| Error parity For RAM Control "CDR STS Timing control"| `W1C`| `0x0`| `0x0 End: Begin:`|

###Read HA Address Bit3_0 Control

* **Description**           

This register is used to send HA read address bit3_0 to HA engine


* **RTL Instant Name**    : `rdha3_0_control`

* **Address**             : `0x30200`

* **Formula**             : `0x30200 + HaAddr3_0`

* **Where**               : 

    * `$HaAddr3_0(0-15): HA Address Bit3_0`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr3_0`| Read value will be 0x01000 plus HaAddr3_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit7_4 Control

* **Description**           

This register is used to send HA read address bit7_4 to HA engine


* **RTL Instant Name**    : `rdha7_4_control`

* **Address**             : `0x30210`

* **Formula**             : `0x30210 + HaAddr7_4`

* **Where**               : 

    * `$HaAddr7_4(0-15): HA Address Bit7_4`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr7_4`| Read value will be 0x01000 plus HaAddr7_4| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit11_8 Control

* **Description**           

This register is used to send HA read address bit11_8 to HA engine


* **RTL Instant Name**    : `rdha11_8_control`

* **Address**             : `0x30220`

* **Formula**             : `0x30220 + HaAddr11_8`

* **Where**               : 

    * `$HaAddr11_8(0-15): HA Address Bit11_8`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr11_8`| Read value will be 0x01000 plus HaAddr11_8| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit15_12 Control

* **Description**           

This register is used to send HA read address bit15_12 to HA engine


* **RTL Instant Name**    : `rdha15_12_control`

* **Address**             : `0x30230`

* **Formula**             : `0x30230 + HaAddr15_12`

* **Where**               : 

    * `$HaAddr15_12(0-15): HA Address Bit15_12`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr15_12`| Read value will be 0x01000 plus HaAddr15_12| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit19_16 Control

* **Description**           

This register is used to send HA read address bit19_16 to HA engine


* **RTL Instant Name**    : `rdha19_16_control`

* **Address**             : `0x30240`

* **Formula**             : `0x30240 + HaAddr19_16`

* **Where**               : 

    * `$HaAddr19_16(0-15): HA Address Bit19_16`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr19_16`| Read value will be 0x01000 plus HaAddr19_16| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit23_20 Control

* **Description**           

This register is used to send HA read address bit23_20 to HA engine


* **RTL Instant Name**    : `rdha23_20_control`

* **Address**             : `0x30250`

* **Formula**             : `0x30250 + HaAddr23_20`

* **Where**               : 

    * `$HaAddr23_20(0-15): HA Address Bit23_20`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr23_20`| Read value will be 0x01000 plus HaAddr23_20| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit24 and Data Control

* **Description**           

This register is used to send HA read address bit24 to HA engine to read data


* **RTL Instant Name**    : `rdha24data_control`

* **Address**             : `0x30260`

* **Formula**             : `0x30260 + HaAddr24`

* **Where**               : 

    * `$HaAddr24(0-1): HA Address Bit24`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata31_0`| HA read data bit31_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data63_32

* **Description**           

This register is used to read HA dword2 of data.


* **RTL Instant Name**    : `rdha_hold63_32`

* **Address**             : `0x30270`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata63_32`| HA read data bit63_32| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data95_64

* **Description**           

This register is used to read HA dword3 of data.


* **RTL Instant Name**    : `rdindr_hold95_64`

* **Address**             : `0x30271`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata95_64`| HA read data bit95_64| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data127_96

* **Description**           

This register is used to read HA dword4 of data.


* **RTL Instant Name**    : `rdindr_hold127_96`

* **Address**             : `0x30272`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata127_96`| HA read data bit127_96| `RW`| `0xx`| `0xx End:`|

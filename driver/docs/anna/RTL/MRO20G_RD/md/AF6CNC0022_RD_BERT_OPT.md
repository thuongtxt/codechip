## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_BERT_OPT
####Register Table

|Name|Address|
|-----|-----|
|`Sel Timeslot Gen`|`0x4102 - 0x7702`|
|`Sel Timeslot Mon tdm`|`0x8102 - 0xB102`|
|`Sel Ho Bert Gen`|`0x0_200 - 0x0_21B`|
|`Sel TDM Bert mon`|`0x0_000 - 0x0_01b`|
|`Sel Mode Bert Gen`|`0x4100 - 0x7700`|
|`Sel Value of fix pattern`|`0x4104 - 0xB704`|
|`Sel Mode Bert Mon TDM`|`0x8100 - 0xB700`|
|`Sel Value of fix pattern`|`0x8104 - 0x8704`|
|`Inser Error`|`0x4101 - 0xB_701`|
|`good counter tdm gen`|`0x0600 - 0x063F`|
|`good counter tdm`|`0x8600 - 0x863F`|
|`TDM err sync`|`0x8640 - 0x867f`|
|`Counter loss bit in TDM mon`|`0x8105-0xB705`|
|`Counter loss bit in TDM mon`|`0x8680 - 0x86BF`|
|`Single Inser Error`|`0x4105-0x7105`|
|`tdm mon stt`|`0x8103 - 0xB703`|


###Sel Timeslot Gen

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ts_gen`

* **Address**             : `0x4102 - 0x7702`

* **Formula**             : `0x4102 + 512*engid`

* **Where**               : 

    * `$engid(0-27): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`gen_tsen`| bit map indicase 32 timeslot,"1" : enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Timeslot Mon tdm

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_mon_tstdm`

* **Address**             : `0x8102 - 0xB102`

* **Formula**             : `0x8102 + 512*engid`

* **Where**               : 

    * `$engid(0-27): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tdm_tsen`| bit map indicase 32 timeslot,"1" : enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert Gen

* **Description**           

The registers select id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_gen`

* **Address**             : `0x0_200 - 0x0_21B`

* **Formula**             : `0x0_200 + engid`

* **Where**               : 

    * `$engid(0-27): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`gen_en`| set "1" to enable bert gen| `RW`| `0x0`| `0x0`|
|`[13:11]`|`lineid`| line id OC48| `RW`| `0x0`| `0x0`|
|`[10:0]`|`id`| TDM_ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel TDM Bert mon

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_bert_montdm`

* **Address**             : `0x0_000 - 0x0_01b`

* **Formula**             : `0x0_000 + engid`

* **Where**               : 

    * `$engid(0-27): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15]`|`tdmmon_en`| set "1" to enable bert gen| `RW`| `0x0`| `0x0`|
|`[14]`|`mon_side`| "0" tdm side, "1" psn side moniter| `RW`| `0x0`| `0x0`|
|`[13:11]`|`tdm_lineid`| line id OC48| `RW`| `0x0`| `0x0`|
|`[10:0]`|`id`| TDM_ID for TDM Side or MAP_PWID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Mode Bert Gen

* **Description**           

The registers select mode bert gen


* **RTL Instant Name**    : `ctrl_pen_gen`

* **Address**             : `0x4100 - 0x7700`

* **Formula**             : `0x4100 + 512*engid`

* **Where**               : 

    * `$engid(0-27): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[05]`|`swapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[04]`|`invmode`| invert data| `RW`| `0x0`| `0x0`|
|`[03:00]`|`patt_mode`| sel pattern gen # 0x01 : all0 # 0x02 : prbs15 # 0x03 : prbs23 # 0x05 : prbs31 # 0x7  : all1 # 0x8  : SEQ(SIM)/prbs20(SYN) # 0x4  : prbs20r # 0x06 : fixpattern 4byte| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Value of fix pattern

* **Description**           

The registers select mode bert gen


* **RTL Instant Name**    : `txfix_gen`

* **Address**             : `0x4104 - 0xB704`

* **Formula**             : `0x4104 + 512*engid`

* **Where**               : 

    * `$engid(0-27): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txpatten_val`| value of fix paatern| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Mode Bert Mon TDM

* **Description**           

The registers select mode bert gen


* **RTL Instant Name**    : `ctrl_pen_montdm`

* **Address**             : `0x8100 - 0xB700`

* **Formula**             : `0x8100 + 512*engid`

* **Where**               : 

    * `$engid(0-27): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[05]`|`swapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[04]`|`invmode`| invert data| `RW`| `0x0`| `0x0`|
|`[03:00]`|`patt_mode`| sel pattern gen # 0x01 : fixpattern # 0x02 : all0 # 0x04 : prbs15 # 0x03 : prbs23 # 0x05 : prbs31 # 0x00 : all1 / SEQ(SIM) # 0x06 : prbs20(SYN) # 0x08  :prbs20r| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Value of fix pattern

* **Description**           

The registers select mode bert gen


* **RTL Instant Name**    : `rxfix_gen`

* **Address**             : `0x8104 - 0x8704`

* **Formula**             : `0x8104 + 512*engid`

* **Where**               : 

    * `$engid(0-27): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxpatten_val`| value of fix paatern| `RW`| `0x0`| `0x0 End: Begin:`|

###Inser Error

* **Description**           

The registers select rate inser error


* **RTL Instant Name**    : `ctrl_ber_pen`

* **Address**             : `0x4101 - 0xB_701`

* **Formula**             : `0x4101 + 512*engid`

* **Where**               : 

    * `$engid(0-27): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ber_rate`| TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to Pattern Generator [31:0] == 32'd0          :disable  Incase N DS0 mode : n        = num timeslot use, BER_level_val recalculate by CFG_DS1,CFG_E1 CFG_DS1  = ( 8n  x  BER_level_val) / 193 CFG_E1   = ( 8n  x  BER_level_val) / 256 Other : BER_level_val	BER_level 1000:        	BER 10e-3 10_000:      	BER 10e-4 100_000:     	BER 10e-5 1_000_000:   	BER 10e-6 10_000_000:  	BER 10e-7| `RW`| `0x0`| `0x0 Begin:`|

###good counter tdm gen

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `goodbit_pen_tdm_gen`

* **Address**             : `0x0600 - 0x063F`

* **Formula**             : `0x0600 + id`

* **Where**               : 

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`bertgen`| gen goodbit| `RC`| `0x0`| `0x0 End: Begin:`|

###good counter tdm

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `goodbit_pen_tdm_mon`

* **Address**             : `0x8600 - 0x863F`

* **Formula**             : `0x8600 + id`

* **Where**               : 

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mongood`| moniter goodbit| `RC`| `0x0`| `0x0 End: Begin:`|

###TDM err sync

* **Description**           

The registers indicate bert mon err in tdm side


* **RTL Instant Name**    : `err_tdm_mon`

* **Address**             : `0x8640 - 0x867f`

* **Formula**             : `0x8640 + id`

* **Where**               : 

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_errbit`| counter err bit| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter loss bit in TDM mon

* **Description**           

The registers count lossbit in  mon tdm side


* **RTL Instant Name**    : `lossyn_pen_tdm_mon`

* **Address**             : `0x8105-0xB705`

* **Formula**             : `0x8105 + engid*512`

* **Where**               : 

    * `$engid(0-27): slice id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`stk_losssyn`| sticky loss sync| `W1C`| `0x0`| `0x0 End: Begin:`|

###Counter loss bit in TDM mon

* **Description**           

The registers count lossbit in  mon tdm side


* **RTL Instant Name**    : `lossbit_pen_tdm_mon`

* **Address**             : `0x8680 - 0x86BF`

* **Formula**             : `0x8680 + id`

* **Where**               : 

    * `$id(0-31): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_lossbit`| counter lossbit| `RC`| `0x0`| `0x0 End: Begin:`|

###Single Inser Error

* **Description**           

The registers select rate inser error


* **RTL Instant Name**    : `singe_ber_pen`

* **Address**             : `0x4105-0x7105`

* **Formula**             : `0x4105 + 512*engid`

* **Where**               : 

    * `$engid(0-27): bert id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`ena`| sw write "1" to force, hw auto clear| `RW`| `0x0`| `0x0 End: Begin:`|

###tdm mon stt

* **Description**           

The registers indicate bert mon state in pw side


* **RTL Instant Name**    : `stt_tdm_mon`

* **Address**             : `0x8103 - 0xB703`

* **Formula**             : `0x8103 + 512*engid`

* **Where**               : 

    * `$engid(0-7): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1:0]`|`tdmstate`| Status [1:0]: Prbs status LOPSTA = 2'd0; SRCSTA = 2'd1; VERSTA = 2'd2; INFSTA = 2'd3;| `RO`| `0x0`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_MAP
####Register Table

|Name|Address|
|-----|-----|
|`MAP CPU  Reg Hold Control`|`0x00F000-0x00F002`|
|`Demap Channel Control`|`0x000000-0x001FFF`|
|`Demap HO Channel Control`|`0x8300`|
|`Map Line Control`|`0x010000-0x0107FF`|
|`map Channel Control`|`0x018000-0x019FFF`|
|`Map IDLE Code`|`0x010800`|
|`RAM Map Parity Force Control`|`0x10808`|
|`RAM Map Parity Disable Control`|`0x10809`|
|`RAM Map parity Error Sticky`|`0x1080a`|
|`RAM DeMap Parity Force Control`|`0x08218`|
|`RAM DeMap Parity Disable Control`|`0x08219`|
|`RAM DeMap parity Error Sticky`|`0x0821a`|


###MAP CPU  Reg Hold Control

* **Description**           

The register provides hold register for three word 32-bits MSB when CPU access to engine.


* **RTL Instant Name**    : `map_cpu_hold_ctrl`

* **Address**             : `0x00F000-0x00F002`

* **Formula**             : `0x00F000 + HoldId`

* **Where**               : 

    * `$HoldId(0-2): Hold register`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdreg0`| Hold 32 bits| `RW`| `0x0`| `0x0 End: Begin:`|

###Demap Channel Control

* **Description**           

The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number.


* **RTL Instant Name**    : `demap_channel_ctrl`

* **Address**             : `0x000000-0x001FFF`

* **Formula**             : `0x000000 + 168*stsid + 24*vtgid + 6*vtid + slotid`

* **Where**               : 

    * `$stsid(0-47): is STS identification number)`

    * `$vtgid(0-6): is VT Group identification number`

    * `$vtid(0-3): is VT identification number`

    * `$slotid(0-5): is time slot number. It varies from 0 to 5 in DS1 and from 0 to 7 in E1`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63]`|`demapfirstts3`| | `RW`| `0x0`| `0x0`|
|`[62:60]`|`demapchanneltype3`| | `RW`| `0x0`| `0x0`|
|`[59]`|`demappwen3`| | `RW`| `0x0`| `0x0`|
|`[58:48]`|`demappwidfield3`| | `RW`| `0x0`| `0x0`|
|`[47]`|`demapfirstts2`| | `RW`| `0x0`| `0x0`|
|`[46:44]`|`demapchanneltype2`| | `RW`| `0x0`| `0x0`|
|`[43]`|`demappwen2`| | `RW`| `0x0`| `0x0`|
|`[42:32]`|`demappwidfield2`| | `RW`| `0x0`| `0x0`|
|`[31]`|`demapfirstts1`| | `RW`| `0x0`| `0x0`|
|`[30:28]`|`demapchanneltype1`| | `RW`| `0x0`| `0x0`|
|`[27]`|`demappwen1`| | `RW`| `0x0`| `0x0`|
|`[26:16]`|`demappwidfield1`| | `RW`| `0x0`| `0x0`|
|`[15]`|`demapfirstts0`| First timeslot indication. Use for CESoP mode only| `RW`| `0x0`| `0x0`|
|`[14:12]`|`demapchanneltype0`| Specify which type of mapping for this. Specify which type of mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: ATM over DS1/ E1, SONET/SDH (unused) 3: IMA over DS1/E1, SONET/SDH(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: VCAT/LCAS/PLCP| `RW`| `0x0`| `0x0`|
|`[11]`|`demappwen0`| PW/Channels Enable| `RW`| `0x0`| `0x0`|
|`[10:0]`|`demappwidfield0`| PW ID field that uses for Encapsulation| `RW`| `0x0`| `0x0  End: Begin:`|

###Demap HO Channel Control

* **Description**           

The registers are used by the hardware to configure PW channel


* **RTL Instant Name**    : `demapho_channel_ctrl`

* **Address**             : `0x8300`

* **Formula**             : `0x8300 + stsid`

* **Where**               : 

    * `$stsid(0-47): is STS in OC48 HDL_PATH     : rtldemap1.hodemap.demap_pw_cfg_ins.membuf.array.ram[$stsid]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18:8]`|`demappwidfield`| PW ID field that uses for Encapsulation| `RW`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`demapchen`| 0:Disable,  1:Enable as HO path| `RW`| `0x0`| `0x0`|
|`[5:3]`|`demapchanneltype`| Specify which type of mapping for this. Specify which type of mapping for this. 0: Unused 1: Unused 2: Unused 3: Unused 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: VCAT/LCAS/PLCP| `RW`| `0x0`| `0x0`|
|`[2]`|`demapsr_vc3n3c`| 0:slave of VC3_N3c  1:master of VC3-N3c| `RW`| `0x0`| `0x0`|
|`[1]`|`demapsrctype`| 0:VC3 1:VC3-3c| `RW`| `0x0`| `0x0`|
|`[0]`|`demappwen`| PW Channels Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Map Line Control

* **Description**           

The registers provide the per line configurations for STS/VT/DS1/E1 line.


* **RTL Instant Name**    : `map_line_ctrl`

* **Address**             : `0x010000-0x0107FF`

* **Formula**             : `0x010000 + 32*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$stsid(0-47): is STS identification number`

    * `$vtgid(0-6): is VT Group identification number`

    * `$vtid(0-3): is VT identification number HDL_PATH     : rtlmap1.map_src_cfg_ram.array.ram[32*stsid + 4*vtgid + vtid]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[29]`|`idleforce`| Force IDLE Signal| `RW`| `0x0`| `0x0`|
|`[28:21]`|`idlecode`| IDLECode pattern| `RW`| `0x0`| `0x0`|
|`[20:18]`|`mapds1lcen`| DS1E1 Loop code insert enable 0: Disable 1: CSU Loop up 2: CSU Loop down 3: FA1 Loop up 4: FA1 Loop down 5: FA2 Loop up 6: FA2 Loop down| `RW`| `0x0`| `0x0`|
|`[17:16]`|`mapframetype`| depend on MapSigType[3:0], the MapFrameType[1:0] bit field can have difference meaning. 0: DS1 SF/E1 BF/E3 G.751(unused) /DS3 framing(unused)/VT1.5,VT2,VT6 normal/STS no POH, no Stuff 1: DS1 ESF/E1 CRC/E3 G.832(unused)/DS1,E1,VT1.5,VT2 fractional 2: CEP DS3/E3/VC3 fractional 3:CEP basic mode or VT with POH/STS with POH, Stuff/DS1,E1,DS3,E3 unframe mode| `RW`| `0x0`| `0x0`|
|`[15:12]`|`mapsigtype`| 0: DS1 1: E1 2: DS3 (unused) 3: E3 (unused) 4: VT 1.5 5: VT 2 6: unused 7: unused 8: VC3 9: VC4 10: STS12c/VC4_4c 11: STS48c/VC4_16c 12: TU3| `RW`| `0x0`| `0x0`|
|`[11]`|`maptimesrcmaster`| This bit is used to indicate the master timing or the master VC3 in VC4/VC4-Xc| `RW`| `0x0`| `0x0`|
|`[10:0]`|`maptimesrcid`| The reference line ID used for timing reference or the master VC3 ID in VC4/VC4-Xc| `RW`| `0x0`| `0x0 End: Begin:`|

###map Channel Control

* **Description**           

The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number.


* **RTL Instant Name**    : `map_channel_ctrl`

* **Address**             : `0x018000-0x019FFF`

* **Formula**             : `0x018000 + 168*stsid + 24*vtgid + 6*vtid + slotid`

* **Where**               : 

    * `$stsid(0-47): is STS identification number)`

    * `$vtgid(0-6): is VT Group identification number`

    * `$vtid(0-3): is VT identification number`

    * `$slotid(0-5): is time slot number. It varies from 0 to 5 in DS1 and from 0 to 7 in E1`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63]`|`mapfirstts3`| | `RW`| `0x0`| `0x0`|
|`[62:60]`|`mapchanneltype3`| | `RW`| `0x0`| `0x0`|
|`[59]`|`mappwen3`| | `RW`| `0x0`| `0x0`|
|`[58:48]`|`mappwidfield3`| | `RW`| `0x0`| `0x0`|
|`[47]`|`mapfirstts2`| | `RW`| `0x0`| `0x0`|
|`[46:44]`|`mapchanneltype2`| | `RW`| `0x0`| `0x0`|
|`[43]`|`mappwen2`| | `RW`| `0x0`| `0x0`|
|`[42:32]`|`mappwidfield2`| | `RW`| `0x0`| `0x0`|
|`[31]`|`mapfirstts1`| | `RW`| `0x0`| `0x0`|
|`[30:28]`|`mapchanneltype1`| | `RW`| `0x0`| `0x0`|
|`[27]`|`mappwen1`| | `RW`| `0x0`| `0x0`|
|`[26:16]`|`mappwidfield1`| | `RW`| `0x0`| `0x0`|
|`[15]`|`mapfirstts0`| First timeslot indication. Use for CESoP mode only| `RW`| `0x0`| `0x0`|
|`[14:12]`|`mapchanneltype0`| Specify which type of mapping for this. Specify which type of mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: ATM over DS1/ E1, SONET/SDH (unused) 3: IMA over DS1/E1, SONET/SDH(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: VCAT/LCAS/PLCP| `RW`| `0x0`| `0x0`|
|`[11]`|`mappwen0`| PW/Channels Enable| `RW`| `0x0`| `0x0`|
|`[10:0]`|`mappwidfield0`| PW ID field that uses for Encapsulation| `RW`| `0x0`| `0x0  End: Begin:`|

###Map IDLE Code

* **Description**           

The registers provide the idle pattern


* **RTL Instant Name**    : `map_idle_code`

* **Address**             : `0x010800`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`idlecode`| Idle code| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM Map Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_Map_Parity_Force_Control`

* **Address**             : `0x10808`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`maplinectrl_parerrfrc`| Force parity For RAM Control "MAP Thalassa Map Line Control"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapchlctrl_parerrfrc`| Force parity For RAM Control "MAP Thalassa Map Channel Control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM Map Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_Map_Parity_Disable_Control`

* **Address**             : `0x10809`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`maplinectrl_parerrdis`| Disable parity For RAM Control "MAP Thalassa Map Line Control"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapchlctrl_parerrdis`| Disable parity For RAM Control "MAP Thalassa Map Channel Control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM Map parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_Map_Parity_Error_Sticky`

* **Address**             : `0x1080a`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`maplinectrl_parerrstk`| Error parity For RAM Control "MAP Thalassa Map Line Control"| `W1C`| `0x0`| `0x0`|
|`[0]`|`mapchlctrl_parerrstk`| Error parity For RAM Control "MAP Thalassa Map Channel Control"| `W1C`| `0x0`| `0x0 End: Begin:`|

###RAM DeMap Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_DeMap_Parity_Force_Control`

* **Address**             : `0x08218`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`demapchlctrl_parerrfrc`| Force parity For RAM Control "DeMAP Thalassa DeMap Channel Control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM DeMap Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_DeMap_Parity_Disable_Control`

* **Address**             : `0x08219`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`demapchlctrl_parerrdis`| Disable parity For RAM Control "DeMAP Thalassa DeMap Channel Control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM DeMap parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_DeMap_Parity_Error_Sticky`

* **Address**             : `0x0821a`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`demapchlctrl_parerrstk`| Error parity For RAM Control "DeMAP Thalassa DeMap Channel Control"| `W1C`| `0x0`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_CDR_v3_TxShapper
####Register Table

|Name|Address|
|-----|-----|
|`DCR Tx Engine Timing control`|`0x0010400`|


###DCR Tx Engine Timing control

* **Description**           

This register is used to configure timing mode for per STS


* **RTL Instant Name**    : `dcr_tx_eng_timing_ctrl`

* **Address**             : `0x0010400`

* **Formula**             : `0x0010400 + OC48*4096 + STS*32 + TUG*4 + VT`

* **Where**               : 

    * `$OC48(0-7):`

    * `$STS(0-47):`

    * `$TUG(0 -6):`

    * `$VT(0-3): (0-2)in E1 and (0-3) in DS1`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[21:19]`|`pktlen_bit`| The payload packet length BIT, PktLen[2:0]| `RW`| `0x0`| `0x0`|
|`[18:16]`|`linetype`| Line type mode 0: DS1 1: E1 2: DS3 3: E3 4: VT15 5: VT2 6: STS1/TU3 7: Reserve| `RW`| `0x0`| `0x0`|
|`[15:0]`|`pktlen_byte`| The payload packet length Byte, PktLen[17:3]| `RW`| `0x0`| `0x0 End:`|

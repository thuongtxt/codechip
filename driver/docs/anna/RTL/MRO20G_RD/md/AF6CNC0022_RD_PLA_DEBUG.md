## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_PLA_DEBUG
####Register Table

|Name|Address|
|-----|-----|
|`Payload Assembler clk311 Control`|`0x0_0000 - 0x1A000`|
|`Payload Assembler OC48 clk311 Sticky`|`0x0_0001 - 0x1_A001`|
|`Payload Assembler Buffer Sticky 1`|`0x4_2000`|
|`Payload Assembler Buffer Sticky 2`|`0x4_2001`|
|`Payload Assembler Buffer Sticky 3`|`0x4_2002`|
|`Payload Assembler Buffer Block Number Status`|`0x4_2010`|
|`Payload Assembler Buffer Cache Number Status`|`0x4_2011`|


###Payload Assembler clk311 Control

* **Description**           

This register is used to control pwid for debug


* **RTL Instant Name**    : `pla_out_clk311_ctrl`

* **Address**             : `0x0_0000 - 0x1A000`

* **Formula**             : `0x0_0000 + $Oc96Slice*32768 + $Oc48Slice*8192`

* **Where**               : 

    * `$Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G`

    * `$Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[22:12]`|`pwid_for_debug`| | `RW`| `0x0`| `0x0`|
|`[11:0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 Begin:`|

###Payload Assembler OC48 clk311 Sticky

* **Description**           

This register is used to used to sticky some alarms for debug per OC-48 @clk311.02 domain


* **RTL Instant Name**    : `pla_oc48_clk311_stk`

* **Address**             : `0x0_0001 - 0x1_A001`

* **Formula**             : `0x0_0001 + $Oc96Slice*32768 + $Oc48Slice*8192`

* **Where**               : 

    * `$Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G`

    * `$Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice`

* **Width**               : `14`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11]`|`pla311oc48_converr`| OC48 conver clock error| `W2C`| `0x0`| `0x0`|
|`[10]`|`unused`| *n/a*| `W2C`| `0x0`| `0x0`|
|`[9]`|`pla311oc48_inpospw`| OC48 input CEP Pos based PW| `W2C`| `0x0`| `0x0`|
|`[8]`|`pla311oc48_innegpw`| OC48 input CEP Neg based PW| `W2C`| `0x0`| `0x0`|
|`[7]`|`pla311oc48_invalodpw`| OC48 input Valid based PW| `W2C`| `0x0`| `0x0`|
|`[6]`|`pla311oc48_invalid`| OC48 input valid| `W2C`| `0x0`| `0x0`|
|`[5]`|`pla311oc48_inaispw`| OC48 input AIS based PW| `W2C`| `0x0`| `0x0`|
|`[4]`|`pla311oc48_inais`| OC48 input AIS| `W2C`| `0x0`| `0x0`|
|`[3]`|`pla311oc48_inj1pospw`| OC48 input CEP J1 Posiion based PW| `W2C`| `0x0`| `0x0`|
|`[2]`|`pla311oc48_inpos`| OC48 input CEP Pos Pointer| `W2C`| `0x0`| `0x0`|
|`[1]`|`pla311oc48_inneg`| OC48 input CEP Neg Pointer| `W2C`| `0x0`| `0x0`|
|`[0]`|`pla311oc48_inj1pos`| OC48 input CEP J1 Position| `W2C`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Buffer Sticky 1

* **Description**           

This register is used to used to sticky some alarms for debug#1


* **RTL Instant Name**    : `pla_buf_stk1`

* **Address**             : `0x4_2000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[30]`|`plagetallblkerr`| Get All Block Type Error| `W2C`| `0x0`| `0x0`|
|`[29]`|`plagethiblkerr`| Get Hi-Block Error| `W2C`| `0x0`| `0x0`|
|`[28]`|`plagetloblkerr`| Get Lo-Block Error| `W2C`| `0x0`| `0x0`|
|`[27]`|`plaallcaempt`| All Cache Empty Error| `W2C`| `0x0`| `0x0`|
|`[26]`|`plagetca512err`| Get cache-512 Error| `W2C`| `0x0`| `0x0`|
|`[25]`|`plagetca256err`| Get cache-256 Error| `W2C`| `0x0`| `0x0`|
|`[24]`|`plagetca128err`| Get cache-128 Error| `W2C`| `0x0`| `0x0`|
|`[23]`|`plawrhiblknearful`| Monitor high block near full| `W2C`| `0x0`| `0x0`|
|`[22]`|`plawrloblknearful`| Monitor low block near full| `W2C`| `0x0`| `0x0`|
|`[21]`|`plawrcanearful`| Monitor total cache near full| `W2C`| `0x0`| `0x0`|
|`[20]`|`plawrblkerr`| block fail due to near empty cache/block| `W2C`| `0x0`| `0x0`|
|`[19]`|`plawrddrfifoful`| Write DDR Fifo Full (due to 1st or 2nd buffer)| `W2C`| `0x0`| `0x0`|
|`[18]`|`plawrddrlenerr`| Write DDR Check Length Error| `W2C`| `0x0`| `0x0`|
|`[17]`|`plahiblkempt`| High Block Empty Error| `W2C`| `0x0`| `0x0`|
|`[16]`|`plaloblkempt`| Low Block Empty Error| `W2C`| `0x0`| `0x0`|
|`[15]`|`plawrddrreorrdfifoful`| Write DDR Reorder Mux Read Full| `W2C`| `0x0`| `0x0`|
|`[14]`|`plawrddrreorwrfifoful`| Write DDR Reorder Mux Write Full| `W2C`| `0x0`| `0x0`|
|`[13]`|`plahiblksame`| High Block Same Error| `W2C`| `0x0`| `0x0`|
|`[12]`|`plaloblksame`| Low Block Same Error| `W2C`| `0x0`| `0x0`|
|`[11]`|`unused`| *n/a*| `W2C`| `0x0`| `0x0`|
|`[10]`|`placa512empt`| Cache 512 Empty Error| `W2C`| `0x0`| `0x0`|
|`[9]`|`placa256empt`| Cache 256 Empty Error| `W2C`| `0x0`| `0x0`|
|`[8]`|`placa128empt`| Cache 128 Empty Error| `W2C`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `W2C`| `0x0`| `0x0`|
|`[6]`|`placa512same`| Cache 512 Same Error| `W2C`| `0x0`| `0x0`|
|`[5]`|`placa256same`| Cache 256 Same Error| `W2C`| `0x0`| `0x0`|
|`[4]`|`placa128same`| Cache 128 Same Error| `W2C`| `0x0`| `0x0`|
|`[3]`|`plamaxlengtherr`| Max Length Error| `W2C`| `0x0`| `0x0`|
|`[2]`|`plareqwrddrnearful`| Request write DDR near full| `W2C`| `0x0`| `0x0`|
|`[1]`|`plafreeblockset`| Free Block Set| `W2C`| `0x0`| `0x0`|
|`[0]`|`plafreecacheset`| Free Cache Set| `W2C`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Buffer Sticky 2

* **Description**           

This register is used to used to sticky some alarms for debug#2


* **RTL Instant Name**    : `pla_buf_stk2`

* **Address**             : `0x4_2001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`plawrddrreordererr`| Write DDR Reorder Error| `W2C`| `0x0`| `0x0`|
|`[30]`|`plawrddrbuf1stfifoful`| Write DDR Buffer 1st Fifo Full| `W2C`| `0x0`| `0x0`|
|`[29]`|`plawrddrbuf2ndfifoful`| Write DDR Buffer 2nd Fifo Full| `W2C`| `0x0`| `0x0`|
|`[28]`|`plawrddrackfifoful`| Write DDR ACK Fifo Full| `W2C`| `0x0`| `0x0`|
|`[27]`|`plapkbufffful`| Packet Buffer Fifo Full| `W2C`| `0x0`| `0x0`|
|`[26]`|`plapkbufffrdy`| Packet Buffer Fifo Ready| `W2C`| `0x0`| `0x0`|
|`[25]`|`plapkbuffull`| Packet Buffer Full| `W2C`| `0x0`| `0x0`|
|`[24]`|`plapkbufnotempt`| Packet Buffer Not Empty| `W2C`| `0x0`| `0x0`|
|`[23:20]`|`unused`| *n/a*| `W2C`| `0x0`| `0x0`|
|`[19]`|`plardportpktdis`| Read Port Side Packet Disabled| `W2C`| `0x0`| `0x0`|
|`[18]`|`plardportpktfferr`| Read Port Side Packet Fifo Error| `W2C`| `0x0`| `0x0`|
|`[17]`|`plardportdatfferr`| Read Port Side Data Fifo Error| `W2C`| `0x0`| `0x0`|
|`[16]`|`plardportinffferr`| Read Port Side Info Fifo Error| `W2C`| `0x0`| `0x0`|
|`[15]`|`plardbufdatpkvldfferr`| Read Buf Side Pkt Vld Fifo Error| `W2C`| `0x0`| `0x0`|
|`[14]`|`plardbufdatreqvldfferr`| Read Buf Side Data Request Vld Fifo Error| `W2C`| `0x0`| `0x0`|
|`[13]`|`plardbufpsnackfferr`| Read Buf Side PSN Ack Fifo Error| `W2C`| `0x0`| `0x0`|
|`[12]`|`plardbufpsnvldfferr`| Read Buf Side PSN Vld Fifo Error| `W2C`| `0x0`| `0x0`|
|`[11]`|`plamuxpwsl96_3_sl48_1_err`| Fifo Mux PW SLice48#1 of Slice96#3 Error| `W2C`| `0x0`| `0x0`|
|`[10]`|`plamuxpwsl96_3_sl48_0_err`| Fifo Mux PW Slice48#0 of Slice96#3 Error| `W2C`| `0x0`| `0x0`|
|`[9]`|`plamuxpwsl96_2_sl48_1_err`| Fifo Mux PW Slice48#1 of Slice96#2 Error| `W2C`| `0x0`| `0x0`|
|`[8]`|`plamuxpwsl96_2_sl48_0_err`| Fifo Mux PW Slice48#0 of Slice96#2 Error| `W2C`| `0x0`| `0x0`|
|`[7]`|`plamuxpwsl96_1_sl48_1_err`| Fifo Mux PW SLice48#1 of Slice96#1 Error| `W2C`| `0x0`| `0x0`|
|`[6]`|`plamuxpwsl96_1_sl48_0_err`| Fifo Mux PW Slice48#0 of Slice96#1 Error| `W2C`| `0x0`| `0x0`|
|`[5]`|`plamuxpwsl96_0_sl48_1_err`| Fifo Mux PW Slice48#1 of Slice96#0 Error| `W2C`| `0x0`| `0x0`|
|`[4]`|`plamuxpwsl96_0_sl48_0_err`| Fifo Mux PW Slice48#0 of Slice96#0 Error| `W2C`| `0x0`| `0x0`|
|`[3]`|`plamuxpwsl96_3_err`| Fifo Mux PW Slice96#3 Error| `W2C`| `0x0`| `0x0`|
|`[2]`|`plamuxpwsl96_2_err`| Fifo Mux PW Slice96#2 Error| `W2C`| `0x0`| `0x0`|
|`[1]`|`plamuxpwsl96_1_err`| Fifo Mux PW Slice96#1 Error| `W2C`| `0x0`| `0x0`|
|`[0]`|`plamuxpwsl96_0_err`| Fifo Mux PW Slice96#0 Error| `W2C`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Buffer Sticky 3

* **Description**           

This register is used to used to sticky some alarms for debug#3


* **RTL Instant Name**    : `pla_buf_stk3`

* **Address**             : `0x4_2002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`plawrddrbak7fifoful`| Write DDR Bak#7 Fifo Full| `W2C`| `0x0`| `0x0`|
|`[30]`|`plawrddrbak6fifoful`| Write DDR Bak#6 Fifo Full| `W2C`| `0x0`| `0x0`|
|`[29]`|`plawrddrbak5fifoful`| Write DDR Bak#5 Fifo Full| `W2C`| `0x0`| `0x0`|
|`[28]`|`plawrddrbak4fifoful`| Write DDR Bak#4 Fifo Full| `W2C`| `0x0`| `0x0`|
|`[27]`|`plawrddrbak3fifoful`| Write DDR Bak#3 Fifo Full| `W2C`| `0x0`| `0x0`|
|`[26]`|`plawrddrbak2fifoful`| Write DDR Bak#2 Fifo Full| `W2C`| `0x0`| `0x0`|
|`[25]`|`plawrddrbak1fifoful`| Write DDR Bak#1 Fifo Full| `W2C`| `0x0`| `0x0`|
|`[24]`|`plawrddrbak0fifoful`| Write DDR Bak#0 Fifo Full| `W2C`| `0x0`| `0x0`|
|`[23]`|`plapktinfofifonearful`| Packet Info Fifo near full| `W2C`| `0x0`| `0x0`|
|`[22]`|`plaqdrpsnreqset`| QDR PSN Request set| `W2C`| `0x0`| `0x0`|
|`[21]`|`plaqdrpsnackset`| QDR PSN Ack set| `W2C`| `0x0`| `0x0`|
|`[20]`|`plaqdrpsnvldset`| QDR PSN valid set| `W2C`| `0x0`| `0x0`|
|`[19]`|`plafreblkrefifoerr`| Free Block Return Fifo Error| `W2C`| `0x0`| `0x0`|
|`[18]`|`pladdrrdreqset`| DDR Read Request set| `W2C`| `0x0`| `0x0`|
|`[17]`|`pladdrrdackset`| DDR Read Ack set| `W2C`| `0x0`| `0x0`|
|`[16]`|`pladdrrdvldset`| DDR Read valid set| `W2C`| `0x0`| `0x0`|
|`[15]`|`plablkmdouteopset`| Output block module EOP set| `W2C`| `0x0`| `0x0`|
|`[14]`|`pladdrwrreqset`| DDR Write Request set| `W2C`| `0x0`| `0x0`|
|`[13]`|`pladdrwrackset`| DDR Write Ack set| `W2C`| `0x0`| `0x0`|
|`[12]`|`pladdrwrvldset`| DDR Write valid set| `W2C`| `0x0`| `0x0`|
|`[11]`|`plablkmdinvldset`| Input block module valid set| `W2C`| `0x0`| `0x0`|
|`[10]`|`plablkmdineopset`| Input block module EOP set| `W2C`| `0x0`| `0x0`|
|`[9]`|`plapwegetpsnerr1`| PWE to get PSN Error due to empty| `W2C`| `0x0`| `0x0`|
|`[8]`|`plapwegetpsnerr0`| PWE to get PSN Error due to PSN len| `W2C`| `0x0`| `0x0`|
|`[7]`|`plapwegetpsn`| PWE to get PSN from PLA| `W2C`| `0x0`| `0x0`|
|`[6]`|`plareadydat4pwe`| Ready data for PWE| `W2C`| `0x0`| `0x0`|
|`[5]`|`plapwegetdat`| PWE to get data from PLA| `W2C`| `0x0`| `0x0`|
|`[4]`|`plaready2rdpkt`| Read Port ready to read packet Info| `W2C`| `0x0`| `0x0`|
|`[3]`|`plardpkinfvldset`| Read Packet Info Valid Set| `W2C`| `0x0`| `0x0`|
|`[2]`|`plawrpkinfvldset`| Write Packet Info Valid Set| `W2C`| `0x0`| `0x0`|
|`[1]`|`plablkmdoutvldset`| Output block module valid set| `W2C`| `0x0`| `0x0`|
|`[0]`|`plapwcoreoutvldset`| Output pwcore valid set| `W2C`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Buffer Block Number Status

* **Description**           

This register is used to store current blocks number


* **RTL Instant Name**    : `pla_buf_blk_num_sta`

* **Address**             : `0x4_2010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[35:20]`|`hiblknum`| Current High Block Number (init value is 0x6000)| `RO`| `0x0`| `0x0`|
|`[19:17]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[16:0]`|`loblknum`| Current Low Block Number (init value is 0x10000)| `RO`| `0x0`| `0x0 Begin:`|

###Payload Assembler Buffer Cache Number Status

* **Description**           

This register is used to store current caches number


* **RTL Instant Name**    : `pla_buf_blk_cache_sta`

* **Address**             : `0x4_2011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[43:32]`|`ca512num`| Current Cache-512 Number (init value is 0x800)| `RO`| `0x0`| `0x0`|
|`[31:30]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[29:16]`|`ca256num`| Current Cache-256 Number (init value is 0x2000)| `RO`| `0x0`| `0x0`|
|`[15]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[14:0]`|`ca128num`| Current Cache-128 Number (init value is 0x2A00)| `RO`| `0x0`| `0x0`|

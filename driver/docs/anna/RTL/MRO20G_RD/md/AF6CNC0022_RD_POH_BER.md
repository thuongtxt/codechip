## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_POH_BER
####Register Table

|Name|Address|
|-----|-----|
|`POH Hold Register`|`0x00_000A`|
|`POH Hold Register`|`0x01_000A`|
|`POH Global Parity Force`|`0x00_0010`|
|`POH Global Parity Disable`|`0x00_0011`|
|`POH Global Parity Alarm`|`0x00_0012`|
|`POH Global Control`|`0x00_0000`|
|`POH Global Control`|`0x00_0020`|
|`POH Global Alarm`|`0x00_0002`|
|`POH CPE Global Control`|`0x01_0002`|
|`POH TER Global Control`|`0x01_0003`|
|`POH Threshold Global Control`|`0x00_0003`|
|`POH Hi-order Path Over Head Grabber`|`0x02_0000`|
|`POH Lo-order VT Over Head Grabber`|`0x02_2000`|
|`POH Hi-order Path Over Head Grabber 1`|`0x02_4000`|
|`POH Lo-order VT Over Head Grabber 1`|`0x02_6000`|
|`POH CPE STS/TU3 Control Register`|`0x02_B000`|
|`POH CPE VT Control Register`|`0x02_8000`|
|`POH CPE STS Status Register`|`0x02_EA00`|
|`POH CPE VT/TU3 Status Register`|`0x02_C000`|
|`POH CPE J1 STS Expected Message buffer`|`0x0F_8000`|
|`POH CPE J1 STS Current Message buffer`|`0x0F_A000`|
|`POH CPE J2 Expected Message buffer`|`0x08_0000`|
|`POH CPE J2 Current Message buffer`|`0x0A_0000`|
|`POH CPE J1 Insert Message buffer`|`0x0F_C000`|
|`POH CPE J2 Insert Message buffer`|`0x0C_0000`|
|`POH Termintate Insert Control STS`|`0x04_0400`|
|`POH Termintate Insert Control VT/TU3`|`0x04_4000`|
|`POH Termintate Insert Buffer STS`|`0x01_0800`|
|`POH Termintate Insert Buffer TU3/VT`|`0x01_8000`|
|`POH BER Global Control`|`0x06_0000`|
|`POH BER Error Sticky`|`0x06_0001`|
|`POH BER Threshold 1`|`0x06_2180`|
|`POH BER Threshold 2`|`0x06_0400`|
|`POH BER Control VT/DSN`|`0x06_2000`|
|`POH BER Control STS/TU3`|`0x06_2007`|
|`POH BER Report VT/DSN`|`0x06_8000`|
|`POH BER Report STS/TU3`|`0x06_D400`|
|`POH Counter Report STS`|`0x0F_2A00`|
|`POH Counter Report TU3/VT`|`0x0F_0000`|
|`POH Alarm Status Mask Report STS`|`0x0F_4000`|
|`POH Alarm Status Report STS`|`0x0F_4800`|
|`POH Interrupt Status Report STS`|`0x0F_4400`|
|`POH Interrupt Or Status Report STS`|`0x0F_4C00`|
|`POH Interrupt Global Status Report STS`|`0x0F_4FFE`|
|`POH Interrupt Global Mask Report STS`|`0x0F_4FFC`|
|`POH Alarm Status Mask Report VT/TU3`|`0x0E_0000`|
|`POH Alarm Status Report VT/TU3`|`0x0E_1000`|
|`POH Interrupt Status Report VT/TU3`|`0x0E_0800`|
|`POH Interrupt Or Status Report VT/TU3`|`0x0E_1800`|
|`POH Interrupt Global Status Report VT/TU3`|`0x0E_FFF0`|
|`POH Interrupt Global Mask Report VT/TU3`|`0x0E_FFE0`|
|`POH Interrupt Global Mask Report`|`0x00_0004`|
|`POH Interrupt Global Mask Report`|`0x00_0024`|
|`POH Interrupt Global Status Report`|`0x00_0005`|
|`POH Interrupt Global Status Report`|`0x00_0025`|
|`POH Interrupt  Global Status Out Report`|`0x00_0006`|
|`POH BER Measured STS/VT Channel Control 1`|`0x06_0003`|
|`POH BER Measured STS/VT Channel Control 2`|`0x06_0013`|
|`PDH Alarm Status Mask Report DE3`|`0x06_6000`|
|`PDH Alarm Status Report DE3`|`0x06_6800`|
|`PDH Interrupt Status Report DE3`|`0x06_6400`|
|`PDH Interrupt Or Status Report DE3`|`0x06_6C00`|
|`PDH Interrupt Global Status Report DE3`|`0x06_6FFE`|
|`PDH Interrupt Global Mask Report DE3`|`0x06_6FFC`|
|`PDH Alarm Status Mask Report DE1`|`0x07_0000`|
|`PDH Alarm Status Report DE1`|`0x07_1000`|
|`PDH Interrupt Status Report DE1`|`0x07_0800`|
|`PDH Interrupt Or Status Report DE1`|`0x07_1800`|
|`PDH Interrupt Global Status Report DE1`|`0x07_FFF0`|
|`PDH Interrupt Global Mask Report DE1`|`0x07_FFE0`|


###POH Hold Register

* **Description**           

This register is used for access long register of Ber, Message.


* **RTL Instant Name**    : `holdreg`

* **Address**             : `0x00_000A`

* **Formula**             : `0x00_000A + holdnum`

* **Where**               : 

    * `$holdnum(0-1): Hold register number`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdvalue`| Hold value| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Hold Register

* **Description**           

This register is used for access long register of Grabber.


* **RTL Instant Name**    : `holdregclk2`

* **Address**             : `0x01_000A`

* **Formula**             : `0x01_000A + holdnum`

* **Where**               : 

    * `$holdnum(0-1): Hold register number`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdvalue`| Hold value| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Global Parity Force

* **Description**           

This register is used to control Parity , Message Jn request.


* **RTL Instant Name**    : `pcfg_glbparfrc`

* **Address**             : `0x00_0010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:6]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[5]`|`berparctrlfrc`| Force parity POH BER Control VT/DSN, POH BER Control STS/TU3| `RW`| `0x0`| `0x0`|
|`[4]`|`berpartrshfrc`| Force parity  POH BER Threshold 2| `RW`| `0x0`| `0x0`|
|`[3]`|`terparvttu3frc`| Force parity POH Termintate Insert Control VT/TU3| `RW`| `0x0`| `0x0`|
|`[2]`|`terparstsfrc`| Force parity POH Termintate Insert Control STS| `RW`| `0x0`| `0x0`|
|`[1]`|`cpeparvtfrc`| Force parity POH CPE VT Control Register| `RW`| `0x0`| `0x0`|
|`[0]`|`cpeparststu3frc`| Force parity POH CPE STS/TU3 Control Register| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Global Parity Disable

* **Description**           

This register is used to control Parity , Message Jn request.


* **RTL Instant Name**    : `pcfg_glbpardis`

* **Address**             : `0x00_0011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:6]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[5]`|`berparctrldis`| Disable parity POH BER Control VT/DSN, POH BER Control STS/TU3| `RW`| `0x0`| `0x0`|
|`[4]`|`berpartrshdis`| Disable parity POH BER Threshold 2| `RW`| `0x0`| `0x0`|
|`[3]`|`terparvttu3dis`| Disable parity POH Termintate Insert Control VT/TU3| `RW`| `0x0`| `0x0`|
|`[2]`|`terparstsdis`| Disable parity POH Termintate Insert Control STS| `RW`| `0x0`| `0x0`|
|`[1]`|`cpeparvtdis`| Disable parity POH CPE VT Control Register| `RW`| `0x0`| `0x0`|
|`[0]`|`cpeparststu3dis`| Disable parity POH CPE STS/TU3 Control Register| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Global Parity Alarm

* **Description**           

This register is used to show alarm of Parity and Debug


* **RTL Instant Name**    : `pcfg_glbparalm`

* **Address**             : `0x00_0012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`hwberbusy`| Hardware BER is busy<br>{1} busy <br>{0} idle| `RO`| `0xX`| `0xX`|
|`[30:12]`|`unused`| *n/a*| `W1C`| `0xX`| `0xX`|
|`[11]`|`hwbusy`| Hardware Jn is busy<br>{1} busy <br>{0} idle| `RO`| `0xX`| `0xX`|
|`[10:6]`|`unused`| *n/a*| `W1C`| `0xX`| `0xX`|
|`[5]`|`berparctrlstk`| Parity sticky POH BER Control VT/DSN, POH BER Control STS/TU3| `W1C`| `0x0`| `0x0`|
|`[4]`|`berpartrshstk`| Parity sticky  POH BER Threshold 2| `W1C`| `0x0`| `0x0`|
|`[3]`|`terparvttu3stk`| Parity sticky POH Termintate Insert Control VT/TU3| `W1C`| `0x0`| `0x0`|
|`[2]`|`terparstsstk`| Parity sticky POH Termintate Insert Control STS| `W1C`| `0x0`| `0x0`|
|`[1]`|`cpeparvtstk`| Parity sticky POH CPE VT Control Register| `W1C`| `0x0`| `0x0`|
|`[0]`|`cpeparststu3stk`| Parity sticky POH CPE STS/TU3 Control Register| `W1C`| `0x0`| `0x0 End: Begin:`|

###POH Global Control

* **Description**           

This register is used to control Parity , Message Jn request.


* **RTL Instant Name**    : `pcfg_glbctr`

* **Address**             : `0x00_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`cpujnreqen`| Enable JN CPU request| `RW`| `0x1`| `0x0`|
|`[30:28]`|`cpejnreqstsen`| Enable JN CPE request for 3 group STS, 16 each| `RW`| `0x7`| `0x0`|
|`[27:13]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[12:4]`|`cpejnreqlineen`| Enable JN CPE request for 9 line| `RW`| `0x1FF`| `0x0`|
|`[3:0]`|`jnbdebound`| Debound Threshold for Jn 1byte| `RW`| `0x5`| `0x5 End: Begin:`|

###POH Global Control

* **Description**           

This register is used to control Parity , Message Jn request.


* **RTL Instant Name**    : `pcfg_glbctr2`

* **Address**             : `0x00_0020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[15:0]`|`terjnreqslen`| Enable JN Ter request for 8 Line| `RW`| `0xFF`| `0x0 End: Begin:`|

###POH Global Alarm

* **Description**           

This register is used to show alarm of Parity and Debug


* **RTL Instant Name**    : `pcfg_glbalm`

* **Address**             : `0x00_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:7]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[6]`|`pmstk`| PM monitor sticky| `W1C`| `0x0`| `0x0`|
|`[5]`|`berparctrlstk`| Parity sticky POH BER Control VT/DSN, POH BER Control STS/TU3| `W1C`| `0x0`| `0x0`|
|`[4]`|`berpartrshstk`| Parity sticky  POH BER Threshold 2| `W1C`| `0x0`| `0x0`|
|`[3]`|`terparvttu3stk`| Parity sticky POH Termintate Insert Control VT/TU3| `W1C`| `0x0`| `0x0`|
|`[2]`|`terparstsstk`| Parity sticky POH Termintate Insert Control STS| `W1C`| `0x0`| `0x0`|
|`[1]`|`cpeparvtstk`| Parity sticky POH CPE VT Control Register| `W1C`| `0x0`| `0x0`|
|`[0]`|`cpeparststu3stk`| Parity sticky POH CPE STS/TU3 Control Register| `W1C`| `0x0`| `0x0 End: Begin:`|

###POH CPE Global Control

* **Description**           

This register is used to control whether receive bytes from OCN block .


* **RTL Instant Name**    : `pcfg_glbcpectr`

* **Address**             : `0x01_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`cpevtline7en`| Enable recevive bytes for VTs Line 7| `RW`| `0x1`| `0x1`|
|`[30]`|`cpevtline6en`| Enable recevive bytes for VTs Line 6| `RW`| `0x1`| `0x1`|
|`[29]`|`cpevtline5en`| Enable recevive bytes for VTs Line 5| `RW`| `0x1`| `0x1`|
|`[28]`|`cpevtline4en`| Enable recevive bytes for VTs Line 4| `RW`| `0x1`| `0x1`|
|`[27]`|`cpevtline3en`| Enable recevive bytes for VTs Line 3| `RW`| `0x1`| `0x1`|
|`[26]`|`cpevtline2en`| Enable recevive bytes for VTs Line 2| `RW`| `0x1`| `0x1`|
|`[25]`|`cpevtline1en`| Enable recevive bytes for VTs Line 1| `RW`| `0x1`| `0x1`|
|`[24]`|`cpevtline0en`| Enable recevive bytes for VTs Line 0| `RW`| `0x1`| `0x1`|
|`[23:9]`|`unused`| *n/a*| `Unused`| `RW`| `0xX`|
|`[8]`|`cpestsline8en`| Enable recevive bytes for STSs Line 8| `RW`| `0x1`| `0x1`|
|`[7]`|`cpestsline7en`| Enable recevive bytes for STSs Line 7| `RW`| `0x1`| `0x1`|
|`[6]`|`cpestsline6en`| Enable recevive bytes for STSs Line 6| `RW`| `0x1`| `0x1`|
|`[5]`|`cpestsline5en`| Enable recevive bytes for STSs Line 5| `RW`| `0x1`| `0x1`|
|`[4]`|`cpestsline4en`| Enable recevive bytes for STSs Line 4| `RW`| `0x1`| `0x1`|
|`[3]`|`cpestsline3en`| Enable recevive bytes for STSs Line 3| `RW`| `0x1`| `0x1`|
|`[2]`|`cpestsline2en`| Enable recevive bytes for STSs Line 2| `RW`| `0x1`| `0x1`|
|`[1]`|`cpestsline1en`| Enable recevive bytes for STSs Line 1| `RW`| `0x1`| `0x1`|
|`[0]`|`cpestsline0en`| Enable recevive bytes for STSs Line 0| `RW`| `0x1`| `0x1 End: Begin:`|

###POH TER Global Control

* **Description**           

This register is used to control whether transmit bytes from OCN block .


* **RTL Instant Name**    : `pcfg_glbterctr`

* **Address**             : `0x01_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[15]`|`tervtline7en`| Enable transmit bytes for VTs Line 7| `RW`| `0x1`| `0x1`|
|`[14]`|`tervtline6en`| Enable transmit bytes for VTs Line 6| `RW`| `0x1`| `0x1`|
|`[13]`|`tervtline5en`| Enable transmit bytes for VTs Line 5| `RW`| `0x1`| `0x1`|
|`[12]`|`tervtline4en`| Enable transmit bytes for VTs Line 4| `RW`| `0x1`| `0x1`|
|`[11]`|`tervtline3en`| Enable transmit bytes for VTs Line 3| `RW`| `0x1`| `0x1`|
|`[10]`|`tervtline2en`| Enable transmit bytes for VTs Line 2| `RW`| `0x1`| `0x1`|
|`[9]`|`tervtline1en`| Enable transmit bytes for VTs Line 1| `RW`| `0x1`| `0x1`|
|`[8]`|`tervtline0en`| Enable transmit bytes for VTs Line 0| `RW`| `0x1`| `0x1`|
|`[7]`|`terstsline7en`| Enable transmit bytes for STSs Line 7| `RW`| `0x1`| `0x1`|
|`[6]`|`terstsline6en`| Enable transmit bytes for STSs Line 6| `RW`| `0x1`| `0x1`|
|`[5]`|`terstsline5en`| Enable transmit bytes for STSs Line 5| `RW`| `0x1`| `0x1`|
|`[4]`|`terstsline4en`| Enable transmit bytes for STSs Line 4| `RW`| `0x1`| `0x1`|
|`[3]`|`terstsline3en`| Enable transmit bytes for STSs Line 3| `RW`| `0x1`| `0x1`|
|`[2]`|`terstsline2en`| Enable transmit bytes for STSs Line 2| `RW`| `0x1`| `0x1`|
|`[1]`|`terstsline1en`| Enable transmit bytes for STSs Line 1| `RW`| `0x1`| `0x1`|
|`[0]`|`terstsline0en`| Enable transmit bytes for STSs Line 0| `RW`| `0x1`| `0x1 End: Begin:`|

###POH Threshold Global Control

* **Description**           

This register is used to set Threshold for stable detection.


* **RTL Instant Name**    : `pcfg_trshglbctr`

* **Address**             : `0x00_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`jnmsgdebound`| Debound Threshold for Jn 16/64byte| `RW`| `0x5`| `0x5`|
|`[27:24]`|`v5rfistbtrsh`| V5 RDI Stable Thershold| `RW`| `0x5`| `0x5`|
|`[23:20]`|`v5rdistbtrsh`| V5 RDI Stable Thershold| `RW`| `0x5`| `0x5`|
|`[19:16]`|`v5slbstbtrsh`| V5 Signal Lable Stable Thershold| `RW`| `0x5`| `0x5`|
|`[15:12]`|`g1rdistbtrsh`| G1 RDI Path Stable Thershold| `RW`| `0x5`| `0x5`|
|`[11:8]`|`c2plmstbtrsh`| C2 Path Signal Lable Stable Thershold| `RW`| `0x5`| `0x5`|
|`[7:4]`|`jnstbtrsh`| J1/J2 Message Stable Threshold| `RW`| `0x5`| `0x5`|
|`[3:0]`|`debound`| Debound Threshold for bytes exclude Jn| `RW`| `0xD`| `0xD End: Begin:`|

###POH Hi-order Path Over Head Grabber

* **Description**           

This register is used to grabber Hi-Order Path Overhead


* **RTL Instant Name**    : `pohstspohgrb`

* **Address**             : `0x02_0000`

* **Formula**             : `0x02_0000 + $stsid + $sliceid*$stsmax+1`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification for Line 0-3`

    * `$stsmax(47-47): STS max`

    * `$stsid(0-stsmax): STS Identification`

* **Width**               : `68`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[67]`|`hlais`| High-level AIS from OCN| `RO`| `0x0`| `0x0`|
|`[66]`|`lom`| LOM  from OCN| `RO`| `0x0`| `0x0`|
|`[65]`|`lop`| LOP from OCN| `RO`| `0x0`| `0x0`|
|`[64]`|`ais`| AIS from OCN| `RO`| `0x0`| `0x0`|
|`[63:56]`|`k3`| K3 byte| `RO`| `0x0`| `0x0`|
|`[55:48]`|`f3`| F3 byte| `RO`| `0x0`| `0x0`|
|`[47:40]`|`h4`| H4 byte| `RO`| `0x0`| `0x0`|
|`[39:32]`|`f2`| F2 byte| `RO`| `0x0`| `0x0`|
|`[31:24]`|`g1`| G1 byte| `RO`| `0x0`| `0x0`|
|`[23:16]`|`c2`| C2 byte| `RO`| `0x0`| `0x0`|
|`[15:8]`|`n1`| N1 byte| `RO`| `0x0`| `0x0`|
|`[7:0]`|`j1`| J1 byte| `RO`| `0x0`| `0x0 End: Begin:`|

###POH Lo-order VT Over Head Grabber

* **Description**           

This register is used to grabber Lo-Order Path Overhead. Incase the TU3 mode, the $vtid = 0, using for Tu3 POH grabber. %%

Incase VT mode, the $vtid = 0-27, using for VT POH grabber.


* **RTL Instant Name**    : `pohvtpohgrb`

* **Address**             : `0x02_2000`

* **Formula**             : `0x02_2000 + $sliceid*32 + $stsid*128 + $vtid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification for Line 0-3`

    * `$stsid(0-47): STS Identification`

    * `$vtid(0-27): VT Identification`

* **Width**               : `36`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[35]`|`hlais`| High-level AIS from OCN| `RO`| `0x0`| `0x0`|
|`[34]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[33]`|`lop`| LOP from OCN| `RO`| `0x0`| `0x0`|
|`[32]`|`ais`| AIS from OCN| `RO`| `0x0`| `0x0`|
|`[31:24]`|`byte3`| G1 byte or K3 byte or K4 byte| `RO`| `0x0`| `0x0`|
|`[23:16]`|`byte2`| C2 byte or F3 byte or N2 byte| `RO`| `0x0`| `0x0`|
|`[15:8]`|`byte1`| N1 byte or H4 byte or J2 byte| `RO`| `0x0`| `0x0`|
|`[7:0]`|`byte0`| J1 byte or F2 byte or V5 byte| `RO`| `0x0`| `0x0 End: Begin:`|

###POH Hi-order Path Over Head Grabber 1

* **Description**           

This register is used to grabber Hi-Order Path Overhead


* **RTL Instant Name**    : `pohstspohgrb1`

* **Address**             : `0x02_4000`

* **Formula**             : `0x02_4000 + $stsid + $sliceid *$stsmax+1`

* **Where**               : 

    * `$sliceid(0-4): Slice Identification for Line 4-8, slice 4 for Line level`

    * `$stsmax(47-47): STS max`

    * `$stsid(0-stsmax): STS Identification`

* **Width**               : `68`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[67]`|`hlais`| High-level AIS from OCN| `RO`| `0x0`| `0x0`|
|`[66]`|`lom`| LOM  from OCN| `RO`| `0x0`| `0x0`|
|`[65]`|`lop`| LOP from OCN| `RO`| `0x0`| `0x0`|
|`[64]`|`ais`| AIS from OCN| `RO`| `0x0`| `0x0`|
|`[63:56]`|`k3`| K3 byte| `RO`| `0x0`| `0x0`|
|`[55:48]`|`f3`| F3 byte| `RO`| `0x0`| `0x0`|
|`[47:40]`|`h4`| H4 byte| `RO`| `0x0`| `0x0`|
|`[39:32]`|`f2`| F2 byte| `RO`| `0x0`| `0x0`|
|`[31:24]`|`g1`| G1 byte| `RO`| `0x0`| `0x0`|
|`[23:16]`|`c2`| C2 byte| `RO`| `0x0`| `0x0`|
|`[15:8]`|`n1`| N1 byte| `RO`| `0x0`| `0x0`|
|`[7:0]`|`j1`| J1 byte| `RO`| `0x0`| `0x0 End: Begin:`|

###POH Lo-order VT Over Head Grabber 1

* **Description**           

This register is used to grabber Lo-Order Path Overhead. Incase the TU3 mode, the $vtid = 0, using for Tu3 POH grabber. %%

Incase VT mode, the $vtid = 0-27, using for VT POH grabber.


* **RTL Instant Name**    : `pohvtpohgrb1`

* **Address**             : `0x02_6000`

* **Formula**             : `0x02_6000 + $sliceid*32 + $stsid*128 + $vtid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification for Line 4-7`

    * `$stsid(0-47): STS Identification`

    * `$vtid(0-27): VT Identification`

* **Width**               : `36`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[35]`|`hlais`| High-level AIS from OCN| `RO`| `0x0`| `0x0`|
|`[34]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[33]`|`lop`| LOP from OCN| `RO`| `0x0`| `0x0`|
|`[32]`|`ais`| AIS from OCN| `RO`| `0x0`| `0x0`|
|`[31:24]`|`byte3`| G1 byte or K3 byte or K4 byte| `RO`| `0x0`| `0x0`|
|`[23:16]`|`byte2`| C2 byte or F3 byte or N2 byte| `RO`| `0x0`| `0x0`|
|`[15:8]`|`byte1`| N1 byte or H4 byte or J2 byte| `RO`| `0x0`| `0x0`|
|`[7:0]`|`byte0`| J1 byte or F2 byte or V5 byte| `RO`| `0x0`| `0x0 End: Begin:`|

###POH CPE STS/TU3 Control Register

* **Description**           

This register is used to configure the POH Hi-order Path Monitoring.


* **RTL Instant Name**    : `pohcpestsctr`

* **Address**             : `0x02_B000`

* **Formula**             : `0x02_B000 + $sliceid*$stsmax+1*2 + $stsid*2 + $tu3en`

* **Where**               : 

    * `$sliceid(0-8): Slice Identification`

    * `$stsmax(47-47): STS max`

    * `$stsid(0-stsmax): STS Identification`

    * `$tu3en(0-1): Tu3enable, 0: STS, 1:Tu3`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:21]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[20]`|`monenb`| Alarm monitor enable| `RW`| `0x0`| `0x0`|
|`[19]`|`plmenb`| PLM enable| `RW`| `0x0`| `0x0`|
|`[18]`|`vcaisdstren`| VcaisDstren| `RW`| `0x0`| `0x0`|
|`[17]`|`plmdstren`| PlmDstren| `RW`| `0x0`| `0x0`|
|`[16]`|`uneqdstren`| UneqDstren| `RW`| `0x0`| `0x0`|
|`[15]`|`timdstren`| TimDstren| `RW`| `0x0`| `0x0`|
|`[14]`|`sdhmode`| SDH mode| `RW`| `0x0`| `0x0`|
|`[13]`|`blkmden`| Block mode BIP| `RW`| `0x0`| `0x0`|
|`[12]`|`erdienb`| Enable E-RDI| `RW`| `0x0`| `0x0`|
|`[11:4]`|`pslexp`| C2 Expected Path Signal Lable Value| `RW`| `0x0`| `0x0`|
|`[3]`|`timenb`| Enable check expect for TIM, Out of frame or unstable alwayse raise TIM| `RW`| `0x0`| `0x0`|
|`[2]`|`reiblkmden`| Block mode REI| `RW`| `0x0`| `0x0`|
|`[1:0]`|`j1mode`| 0: 1Byte 1:16Byte 2:64byte 3:Floating| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE VT Control Register

* **Description**           

This register is used to configure the POH Lo-order Path Monitoring.


* **RTL Instant Name**    : `pohcpevtctr`

* **Address**             : `0x02_8000`

* **Formula**             : `0x02_8000 + $sliceid*$stsmax+1*$vtmax+1 + $stsid*$vtmax+1 +$vtid`

* **Where**               : 

    * `$sliceid(0-7): Slice Identification`

    * `$stsmax(47-47): STS max`

    * `$vtmax(27-27): VT max`

    * `$stsid(0-stsmax): STS Identification`

    * `$vtid(0-vtmax):Vt Identification`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[15]`|`monenb`| Alarm monitor enable| `RW`| `0x0`| `0x0`|
|`[14]`|`plmenb`| VcaisDstren| `RW`| `0x0`| `0x0`|
|`[13]`|`vcaisdstren`| VcaisDstren| `RW`| `0x0`| `0x0`|
|`[12]`|`plmdstren`| PlmDstren| `RW`| `0x0`| `0x0`|
|`[11]`|`uneqdstren`| UneqDstren| `RW`| `0x0`| `0x0`|
|`[10]`|`timdstren`| TimDstren| `RW`| `0x0`| `0x0`|
|`[9]`|`vsdhmode`| SDH mode| `RW`| `0x0`| `0x0`|
|`[8]`|`vblkmden`| Block mode BIP| `RW`| `0x0`| `0x0`|
|`[7]`|`erdienb`| Enable E-RDI| `RW`| `0x0`| `0x0`|
|`[6:4]`|`vslexp`| V5 Expected Path Signal Lable Value| `RW`| `0x0`| `0x0`|
|`[3]`|`timenb`| Enable Monitor TIM| `RW`| `0x0`| `0x0`|
|`[2]`|`reiblkmden`| Block mode REI| `RW`| `0x0`| `0x0`|
|`[1:0]`|`j2mode`| 0: 1Byte 1:16Byte 2:64byte 3:Floating| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE STS Status Register

* **Description**           

This register is used to get POH Hi-order status of monitoring.


* **RTL Instant Name**    : `pohcpestssta`

* **Address**             : `0x02_EA00`

* **Formula**             : `0x02_EA00 + $sliceid*48 + $stsid`

* **Where**               : 

    * `$sliceid(0-7): Slice Identification`

    * `$stsid(0-47): STS Identification`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[30:27]`|`jnstbcntoo`| Jn state machine| `RO`| `0x0`| `0x0`|
|`[26]`|`jntimstao`| Jn tim status| `RO`| `0x0`| `0x0`|
|`[25]`|`jnmismsgo`| Jn mis-match current message| `RO`| `0x0`| `0x0`|
|`[24]`|`jnmispato`| Jn mis-match expect message| `RO`| `0x0`| `0x0`|
|`[23:22]`|`jnstate`| Jn state machine| `RO`| `0x0`| `0x0`|
|`[21:16]`|`jnfrmcnt`| Jn frame counter| `RO`| `0x0`| `0x0`|
|`[15]`|`jntimalm`| Jn tim alarm| `RO`| `0x0`| `0x0`|
|`[14]`|`jnstableoo`| Jn stable| `RO`| `0x0`| `0x0`|
|`[13]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[12]`|`c2stbsta`| C2 stable status| `RO`| `0x0`| `0x0`|
|`[11:8]`|`c2stbcnt`| C2 stable counter| `RO`| `0x0`| `0x0`|
|`[7:0]`|`c2acpt`| C2 accept byte| `RO`| `0x0`| `0x0 End: Begin:`|

###POH CPE VT/TU3 Status Register

* **Description**           

This register is used to get POH Lo-order status of monitoring.


* **RTL Instant Name**    : `pohcpevtsta`

* **Address**             : `0x02_C000`

* **Formula**             : `0x02_C000 + $sliceid*1344 + $stsid*28 +$vtid`

* **Where**               : 

    * `$sliceid(0-7): Slice Identification`

    * `$stsid(0-47): STS Identification`

    * `$vtid(0-27):Vt Identification`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[30:27]`|`jnstbcntoo`| Jn state machine| `RO`| `0x0`| `0x0`|
|`[26]`|`jntimstao`| Jn tim status| `RO`| `0x0`| `0x0`|
|`[25]`|`jnmismsgo`| Jn mis-match current message| `RO`| `0x0`| `0x0`|
|`[24]`|`jnmispato`| Jn mis-match expect message| `RO`| `0x0`| `0x0`|
|`[23:22]`|`jnstate`| Jn state machine| `RO`| `0x0`| `0x0`|
|`[21:16]`|`jnfrmcnt`| Jn frame counter| `RO`| `0x0`| `0x0`|
|`[15]`|`jntimalm`| Jn tim alarm| `RO`| `0x0`| `0x0`|
|`[14]`|`jnstableoo`| Jn stable| `RO`| `0x0`| `0x0`|
|`[13]`|`vslstbsta`| VSL stable status| `RO`| `0x0`| `0x0`|
|`[12:9]`|`vslstbcnt`| VSL stable counter| `RO`| `0x0`| `0x0`|
|`[8:6]`|`vslacpt`| VSL accept byte| `RO`| `0x0`| `0x0`|
|`[5:0]`|`rfistatus`| RFI status| `RO`| `0x0`| `0x0 End: Begin:`|

###POH CPE J1 STS Expected Message buffer

* **Description**           

The J1 Expected Message Buffer.


* **RTL Instant Name**    : `pohmsgstsexp`

* **Address**             : `0x0F_8000`

* **Formula**             : `0x0F_8000 + $sliceid*$stsmax+1*$msgmax+1 + $stsid*$msgmax+1 + $msgid`

* **Where**               : 

    * `$sliceid(0-8): Slice Identification`

    * `$stsmax(47-47): STS max`

    * `$msgmax(7-7): MSG max`

    * `$stsid(0-stsmax): STS Identification`

    * `$msgid(0-msgmax): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j1expmsg`| J1 Expected Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J1 STS Current Message buffer

* **Description**           

The J1 Current Message Buffer.


* **RTL Instant Name**    : `pohmsgstscur`

* **Address**             : `0x0F_A000`

* **Formula**             : `0x0F_A000 + $sliceid*$stsmax+1*$msgmax+1 + $stsid*$msgmax+1 + $msgid`

* **Where**               : 

    * `$sliceid(0-8): Slice Identification`

    * `$stsmax(47-47): STS max`

    * `$msgmax(7-7): MSG max`

    * `$stsid(0-stsmax): STS Identification`

    * `$msgid(0-msgmax): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j1curmsg`| J1 Current Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J2 Expected Message buffer

* **Description**           

The J2 Expected Message Buffer.


* **RTL Instant Name**    : `pohmsgvtexp`

* **Address**             : `0x08_0000`

* **Formula**             : `0x08_0000 + $sliceid*$stsmax+1*$vtmax+1*$msgmax+1 + $stsid*$vtmax+1*$msgmax+1 + $vtid*$msgmax+1 + $msgid`

* **Where**               : 

    * `$sliceid(0-7): Slice Identification`

    * `$stsmax(47-47): STS max`

    * `$vtmax(27-27): VT max`

    * `$msgmax(7-7): MSG max`

    * `$stsid(0-stsmax): STS Identification`

    * `$vtid(0-vtmax): VT Identification`

    * `$msgid(0-msgmax): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j2expmsg`| J2 Expected Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J2 Current Message buffer

* **Description**           

The J2 Current Message Buffer.


* **RTL Instant Name**    : `pohmsgvtcur`

* **Address**             : `0x0A_0000`

* **Formula**             : `0x0A_0000 + $sliceid*$stsmax+1*$vtmax+1*$msgmax+1 + $stsid*$vtmax+1*$msgmax+1 + $vtid*$msgmax+1 + $msgid`

* **Where**               : 

    * `$sliceid(0-7): Slice Identification`

    * `$stsmax(47-47): STS max`

    * `$vtmax(27-27): VT max`

    * `$msgmax(7-7): MSG max`

    * `$stsid(0-stsmax): STS Identification`

    * `$vtid(0-vtmax): VT Identification`

    * `$msgid(0-msgmax): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j2curmsg`| J2 Current Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J1 Insert Message buffer

* **Description**           

The J1 Current Message Buffer.


* **RTL Instant Name**    : `pohmsgstsins`

* **Address**             : `0x0F_C000`

* **Formula**             : `0x0F_C000 + $sliceid*$stsmax+1*$msgmax+1 + $stsid*$msgmax+1 + $msgid`

* **Where**               : 

    * `$sliceid(0-7): Slice Identification`

    * `$stsmax(47-47): STS max`

    * `$msgmax(7-7): MSG max`

    * `$stsid(0-stsmax): STS Identification`

    * `$msgid(0-msgmax): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j1insmsg`| J1 Insert Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J2 Insert Message buffer

* **Description**           

The J2 Insert Message Buffer.


* **RTL Instant Name**    : `pohmsgvtins`

* **Address**             : `0x0C_0000`

* **Formula**             : `0x0C_0000 + $sliceid*$stsmax+1*$vtmax+1*$msgmax+1 + $stsid*$vtmax+1*$msgmax+1 + $vtid*$msgmax+1 + $msgid`

* **Where**               : 

    * `$sliceid(0-7): Slice Identification`

    * `$stsmax(47-47): STS max`

    * `$vtmax(27-27): VT max`

    * `$msgmax(7-7): MSG max`

    * `$stsid(0-stsmax): STS Identification`

    * `$vtid(0-vtmax): VT Identification`

    * `$msgid(0-msgmax): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j2insmsg`| J2 Insert Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Termintate Insert Control STS

* **Description**           

This register is used to control STS POH insert .


* **RTL Instant Name**    : `ter_ctrlhi`

* **Address**             : `0x04_0400`

* **Formula**             : `0x04_0400 + $STS + $OCID*$stsmax+1`

* **Where**               : 

    * `$stsmax(47-47): STS max`

    * `$STS(0-stsmax)  : STS`

    * `$OCID(0-7)  : Line ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:9]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[8]`|`rdien`| 1: send old version RDI code G1 bit6,7 : send ERDI version code G1 bit6,7| `RW`| `0x0`| `0x0`|
|`[7]`|`autoreidis`| 1: auto insert rei disable, 0: auto insert rei enable| `RW`| `0x0`| `0x0`|
|`[6]`|`g1spare`| G1 spare value| `RW`| `0x0`| `0x0`|
|`[5]`|`plm`| 0 : Enable, 1: Disable send ERDI if PLM detected| `RW`| `0x0`| `0x0`|
|`[4]`|`uneq`| 0 : Enable, 1: Disable send ERDI if UNEQ detected| `RW`| `0x0`| `0x0`|
|`[3]`|`timmsk`| 0 : Enable, 1: Disable send ERDI if TIM detected| `RW`| `0x0`| `0x0`|
|`[2]`|`aislopmsk`| 0 : Enable, 1: Disable send ERDI if AIS,LOP detected| `RW`| `0x0`| `0x0`|
|`[1:0]`|`jnfrmd`| 0:1 byte, 1: 16/64byte| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Termintate Insert Control VT/TU3

* **Description**           

This register is used to control STS POH insert. TU3 is at VT ID = 0. Fields must be the same as ter_ctrlhi


* **RTL Instant Name**    : `ter_ctrllo`

* **Address**             : `0x04_4000`

* **Formula**             : `0x04_4000 + $STS*$vtmax+1 + $OCID*$stsmax+1*$vtmax+1 + $VT`

* **Where**               : 

    * `$stsmax(47-47): STS max`

    * `$vtmax(27-27): VT max`

    * `$STS(0-stsmax)  : STS`

    * `$OCID(0-7)  : Line ID`

    * `$VT(0-vtmax)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[16]`|`rdien`| 1: send old version RDI code Z7 bit5,6,7 : send ERDI version code Z7 bit5,6,7| `RW`| `0x0`| `0x0`|
|`[15]`|`autoreidis`| 1: auto insert rei disable, 0: auto insert rei enable| `RW`| `0x0`| `0x0`|
|`[14:13]`|`k4b0b1`| K4b0b1 value| `RW`| `0x0`| `0x0`|
|`[12:11]`|`k4aps`| K4aps value| `RW`| `0x0`| `0x0`|
|`[10]`|`k4spare`| K4spare value| `RW`| `0x0`| `0x0`|
|`[9]`|`rfival`| RFI value| `RW`| `0x0`| `0x0`|
|`[8:6]`|`vslval`| VT signal label value| `RW`| `0x0`| `0x0`|
|`[5]`|`plm`| 0 : Enable, 1: Disable send ERDI if PLM detected| `RW`| `0x0`| `0x0`|
|`[4]`|`uneq`| 0 : Enable, 1: Disable send ERDI if UNEQ detected| `RW`| `0x0`| `0x0`|
|`[3]`|`timmsk`| 0 : Enable, 1: Disable send ERDI if TIM detected| `RW`| `0x0`| `0x0`|
|`[2]`|`aislopmsk`| 0 : Enable, 1: Disable send ERDI if AIS,LOP detected| `RW`| `0x0`| `0x0`|
|`[1:0]`|`jnfrmd`| 0:1 byte, 1: 16/64byte| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Termintate Insert Buffer STS

* **Description**           

This register is used for storing POH BYTEs inserted to Sonet/SDH. %%

BGRP = 0 : G1,J1  %%

BGRP = 1 : N1,C2  %%

BGRP = 2 : H4,F2  %%

BGRP = 3 : K3,F3


* **RTL Instant Name**    : `rtlpohccterbufhi`

* **Address**             : `0x01_0800`

* **Formula**             : `0x01_0800 + $OCID*256 + $STS*4 + $BGRP`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-7)  : Line ID`

    * `$BGRP(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[17]`|`byte1msk`| Enable/Disable (1/0)write to buffer| `WO`| `0x0`| `0x0`|
|`[16:9]`|`byte1`| Byte1 (G1/N1/H4/K3)| `RW`| `0x0`| `0x0`|
|`[8]`|`byte0msk`| Enable/Disable (1/0) write to buffer| `WO`| `0x0`| `0x0`|
|`[7:0]`|`byte0`| Byte0 (J1/C2/F2/F3)| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Termintate Insert Buffer TU3/VT

* **Description**           

This register is used for storing POH BYTEs inserted to Sonet/SDH. TU3 is at VT ID = 0,1 %%

For VT %%

BGRP = 0 : J2,V5 %%

BGRP = 1 : K4,N2 %%

For TU3 %%

VT = 0, BGRP = 0 : G1,J1 %%

VT = 0, BGRP = 1 : N1,C2 %%

VT = 1, BGRP = 0 : H4,F2 %%

VT = 1, BGRP = 1 : K3,F3


* **RTL Instant Name**    : `rtlpohccterbuflo`

* **Address**             : `0x01_8000`

* **Formula**             : `0x01_8000 + $OCID*4096 + $STS*64 + $VT*2 + $BGRP`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-3)  : Line ID`

    * `$VT(0-27)`

    * `$BGRP(0-1)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[17]`|`byte1msk`| Enable/Disable (1/0)write to buffer| `WO`| `0x0`| `0x0`|
|`[16:9]`|`byte1`| Byte1 (J2/K4)| `RW`| `0x0`| `0x0`|
|`[8]`|`byte0msk`| Enable/Disable (1/0) write to buffer| `WO`| `0x0`| `0x0`|
|`[7:0]`|`byte0`| Byte0 (V5/N2)| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Global Control

* **Description**           

This register is used to enable STS,VT,DSN globally.


* **RTL Instant Name**    : `pcfg_glbenb`

* **Address**             : `0x06_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[3]`|`timerenb`| Enable timer| `RW`| `0x1`| `0x0`|
|`[2]`|`stsenb`| Enable STS/TU3 channel| `RW`| `0x1`| `0x0`|
|`[1]`|`vtenb`| Enable VT channel| `RW`| `0x1`| `0x0`|
|`[0]`|`dsnsenb`| Enable DSN channel| `RW`| `0x1`| `0x0 End: Begin:`|

###POH BER Error Sticky

* **Description**           

This register is used to check error in BER engine.


* **RTL Instant Name**    : `stkalarm`

* **Address**             : `0x06_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[1]`|`stserr`| STS error| `W1C`| `0x0`| `0x0`|
|`[0]`|`vterr`| VT error| `W1C`| `0x0`| `0x0 End: Begin:`|

###POH BER Threshold 1

* **Description**           

This register is used to configure threshold of BER level 3.


* **RTL Instant Name**    : `imemrwptrsh1`

* **Address**             : `0x06_2180`

* **Formula**             : `0x06_2180 + $Rate`

* **Where**               : 

    * `$Rate(0-127): STS Rate for rate from STS1,STS3,STS6...STS48,STS192(0-16,47)... VT1.5,VT2,DS1,E1(65,67,69,71).... OC3,OC12,OC48,OC96,OC192(48-52)....`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[18:9]`|`setthres`| SetThreshold,  SDH line with bit field [31:15]| `RW`| `0x0`| `0x0`|
|`[8:0]`|`winthres`| WindowThreshold, SDH line with bit field [15:0]| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Threshold 2

* **Description**           

This register is used to configure threshold of BER level 4 to level 8. %%

Rate STS1,STS3,STS6...STS48,STS192 : 0-16,47 %%

Rate Ds3,E3G832,E3G751,DS3Line,E3Line,EC1,DS1Line,E1Line : 17,18,19,20,21,22,24,25 %%

Rate VT1.5,VT2,DS1,E1: 60-63 %%

Rate OC3,OC12,OC48,OC96,OC192 : 48-52 %%

Thresloc = 0:   win_thres5, win_thres6 %%

Thresloc = 1:   win_thres7, win_thres8 %%

Thresloc = 2:   unused    , win_thres4 %%

Thresloc = 3:   set_thres4  clr_thres4 %%

Thresloc = 4:   set_thres5  clr_thres5 %%

Thresloc = 5:   set_thres6  clr_thres6 %%

Thresloc = 6:   set_thres7  clr_thres7 %%

Thresloc = 7:   set_thres8  clr_thres8 .


* **RTL Instant Name**    : `imemrwptrsh2`

* **Address**             : `0x06_0400`

* **Formula**             : `0x06_0400 + $Rate*8 + $Thresloc`

* **Where**               : 

    * `$Rate(0-63): STS Rate for rate from STS1,STS3,STS6...STS48,....VT1.5,VT2,DS1,E1....`

    * `$Thresloc(0-7): Set/Clear/Window threshold for BER level from 4 to 8`

* **Width**               : `34`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[33:17]`|`scwthres1`| Set/Clear/Window Threshold| `RW`| `0x0`| `0x0`|
|`[16:0]`|`scwthres2`| Set/Clear/Window Threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Control VT/DSN

* **Description**           

This register is used to enable and set threshold SD SF .


* **RTL Instant Name**    : `imemrwpctrl1`

* **Address**             : `0x06_2000`

* **Formula**             : `0x06_2000 + $STS*8 + $OCID*512+ $VTG`

* **Where**               : 

    * `$STS(0-47): STS`

    * `$OCID(0-15)  : Line ID, 0-7: VT, 8-15: DE1`

    * `$VTG(0-6)   : VT group`

* **Width**               : `48`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:46]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[45:43]`|`tcatrsh4`| TCA threshold raise channel 4| `RW`| `0x0`| `0x0`|
|`[42:40]`|`tcatrsh3`| TCA threshold raise channel 3| `RW`| `0x0`| `0x0`|
|`[39:37]`|`tcatrsh2`| TCA threshold raise channel 2| `RW`| `0x0`| `0x0`|
|`[36:34]`|`tcatrsh1`| TCA threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[33:32]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[31]`|`etype4`| 0: DS1/VT1.5 1: E1/VT2 channel 3| `RW`| `0x0`| `0x0`|
|`[30:28]`|`sftrsh4`| SF threshold raise channel 3| `RW`| `0x0`| `0x0`|
|`[27:25]`|`sdtrsh4`| SD threshold raise channel 3| `RW`| `0x0`| `0x0`|
|`[24]`|`ena4`| Enable channel 3| `RW`| `0x0`| `0x0`|
|`[23]`|`etype3`| 0: DS1/VT1.5 1: E1/VT2 channel 2| `RW`| `0x0`| `0x0`|
|`[22:20]`|`sftrsh3`| SF threshold raise channel 2| `RW`| `0x0`| `0x0`|
|`[19:17]`|`sdtrsh3`| SD threshold raise channel 2| `RW`| `0x0`| `0x0`|
|`[16]`|`ena3`| Enable channel 2| `RW`| `0x0`| `0x0`|
|`[15]`|`etype2`| 0: DS1/VT1.5 1: E1/VT2 channel 1| `RW`| `0x0`| `0x0`|
|`[14:12]`|`sftrsh2`| SF threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[11:9]`|`sdtrsh2`| SD threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[8]`|`ena2`| Enable channel 1| `RW`| `0x0`| `0x0`|
|`[7]`|`etype1`| 0: DS1/VT1.5 1: E1/VT2 channel 0| `RW`| `0x0`| `0x0`|
|`[6:4]`|`sftrsh1`| SF threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[3:1]`|`sdtrsh1`| SD threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[0]`|`ena1`| Enable channel 0| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Control STS/TU3

* **Description**           

This register is used to enable and set threshold SD SF. %%

BER STS used with OCID 0-7 channel 0  %%

BER EC1 pointer used with OCID - not supported, channel 0  %%

TU3 used with OCID 0-7 channel 1 %%

BER SDH Line used with OCID 8, channel 0%%

BER SDH EC1 framer used with OCID - not supported, channel 0%%

BER DE3 used with OCID 8-15 , channel 1.


* **RTL Instant Name**    : `imemrwpctrl2`

* **Address**             : `0x06_2007`

* **Formula**             : `0x06_2007 + $STS*8 + $OCID*512`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-15)  : Line ID`

* **Width**               : `48`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:40]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[39:37]`|`tcatrsh2`| TCA threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[36:34]`|`tcatrsh1`| TCA threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[33:32]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:21]`|`rate2`| STS Rate 0-63 type| `RW`| `0x0`| `0x0`|
|`[20:18]`|`sftrsh2`| SF threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[17:15]`|`sdtrsh2`| SD threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[14]`|`ena2`| Enable channel 1| `RW`| `0x0`| `0x0`|
|`[13:7]`|`rate1`| STS Rate 0-63 type| `RW`| `0x0`| `0x0`|
|`[6:4]`|`sftrsh1`| SF threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[3:1]`|`sdtrsh1`| SD threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[0]`|`ena1`| Enable channel 0| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Report VT/DSN

* **Description**           

This register is used to get current BER rate .


* **RTL Instant Name**    : `ramberratevtds`

* **Address**             : `0x06_8000`

* **Formula**             : `0x06_8000 + $STS*28 + $OCID*1344 + $VT`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-15)     : Line ID, VT:0-7,DSN:8-15`

    * `$VT(0-27)   : VT/DS1 ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[3]`|`hwsta`| Hardware status| `RW`| `0x0`| `0x0`|
|`[2:0]`|`rate`| BER rate 3'd1: BER = 10-3 3'd2: BER = 10-4 3'd3: BER = 10-5 3'd4: BER = 10-6 3'd5: BER = 10-7 3'd6: BER = 10-8 3'd7: BER = 10-9 3'd0: BER = 10-10| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Report STS/TU3

* **Description**           

This register is used to get current BER rate. %%

BER STS used with OCID 0-7 TU3TYPE 0  %%

BER EC1 pointer used with OCID - not supported, TU3TYPE 0  %%

TU3 used with OCID 0-7 TU3TYPE 1 %%

BER SDH Line used with OCID 8, TU3TYPE 0%%

BER SDH EC1 framer used with OCID - not supported, TU3TYPE 0%%

BER DE3 used with OCID 8-15 , TU3TYPE 1.


* **RTL Instant Name**    : `ramberrateststu3`

* **Address**             : `0x06_D400`

* **Formula**             : `0x06_D400 + $STS*2 + $OCID*128 + $TU3TYPE`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-15)     : Line ID`

    * `$TU3TYPE(0-1)  : Type TU3:1, STS:0`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[3]`|`hwsta`| Hardware status| `RW`| `0x0`| `0x0`|
|`[2:0]`|`rate`| BER rate 3'd1: BER = 10-3 3'd2: BER = 10-4 3'd3: BER = 10-5 3'd4: BER = 10-6 3'd5: BER = 10-7 3'd6: BER = 10-8 3'd7: BER = 10-9 3'd0: BER = 10-10| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Counter Report STS

* **Description**           

This register is used to get POH Counter, Rx SDH pointer increase, decrease counter.


* **RTL Instant Name**    : `ipm_cnthi`

* **Address**             : `0x0F_2A00`

* **Formula**             : `0x0F_2A00 + $STS + $OCID*$stsmax+1`

* **Where**               : 

    * `$stsmax(47-47): STS max`

    * `$STS(0-stsmax)  : STS`

    * `$OCID(0-8)     : Line ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`reicnt`| REI counter| `RC`| `0x0`| `0x0`|
|`[15:0]`|`bipcnt`| BIP counter| `RC`| `0x0`| `0x0 End: Begin:`|

###POH Counter Report TU3/VT

* **Description**           

This register is used to get POH Counter, Rx SDH pointer increase, decrease counter.


* **RTL Instant Name**    : `ipm_cntlo`

* **Address**             : `0x0F_0000`

* **Formula**             : `0x0F_0000 + $STS*$vtmax+1 + $OCID*$stsmax+1*$vtmax+1 + $VT`

* **Where**               : 

    * `$stsmax(47-47): STS max`

    * `$vtmax(27-27): VT max`

    * `$STS(0-stsmax)  : STS`

    * `$OCID(0-7)   : Line ID`

    * `$VT(0-vtmax)   : VT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`reicnt`| REI counter| `RC`| `0x0`| `0x0`|
|`[15:0]`|`bipcnt`| BIP counter| `RC`| `0x0`| `0x0 End: Begin:`|

###POH Alarm Status Mask Report STS

* **Description**           

This register is used to get POH alarm mask report.


* **RTL Instant Name**    : `alm_mskhi`

* **Address**             : `0x0F_4000`

* **Formula**             : `0x0F_4000 + $STS*$SINTOFF + $OCID`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-8) : Line ID`

    * `$SINTOFF(16-16): sts interrupt offset`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[14]`|`rdimsk`| rdi mask| `RW`| `0x0`| `0x0`|
|`[13]`|`jnstachgmsk`| pslstachg mask| `RW`| `0x0`| `0x0`|
|`[12]`|`pslstachgmsk`| jnstachg mask| `RW`| `0x0`| `0x0`|
|`[11]`|`bersdmsk`| bersd mask| `RW`| `0x0`| `0x0`|
|`[10]`|`bersfmsk`| bersf  mask| `RW`| `0x0`| `0x0`|
|`[9]`|`erdimsk`| erdis mask| `RW`| `0x0`| `0x0`|
|`[8]`|`bertcamsk`| bertca mask| `RW`| `0x0`| `0x0`|
|`[7]`|`erdicmsk`| erdicmsk  mask| `RW`| `0x0`| `0x0`|
|`[6]`|`erdipmsk`| erdipmsk  mask| `RW`| `0x0`| `0x0`|
|`[5]`|`rfimsk`| rfi/lom mask| `RW`| `0x0`| `0x0`|
|`[4]`|`timmsk`| tim mask| `RW`| `0x0`| `0x0`|
|`[3]`|`uneqmsk`| uneq mask| `RW`| `0x0`| `0x0`|
|`[2]`|`plmmsk`| plm mask| `RW`| `0x0`| `0x0`|
|`[1]`|`aismsk`| ais mask| `RW`| `0x0`| `0x0`|
|`[0]`|`lopmsk`| lop mask| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Alarm Status Report STS

* **Description**           

This register is used to get POH alarm status report.


* **RTL Instant Name**    : `alm_stahi`

* **Address**             : `0x0F_4800`

* **Formula**             : `0x0F_4800 + $STS*$SINTOFF + $OCID`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-8) : Line ID`

    * `$SINTOFF(16-16): sts interrupt offset`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[14]`|`rdista`| rdi  status| `RO`| `0x0`| `0x0`|
|`[13:12]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[11]`|`bersdsta`| bersd  status| `RO`| `0x0`| `0x0`|
|`[10]`|`bersfsta`| bersf  status| `RO`| `0x0`| `0x0`|
|`[9]`|`erdista`| erdis  status| `RO`| `0x0`| `0x0`|
|`[8]`|`bertcasta`| bertca status| `RO`| `0x0`| `0x0`|
|`[7]`|`erdicsta`| erdic status| `RO`| `0x0`| `0x0`|
|`[6]`|`erdipsta`| erdip stable status| `RO`| `0x0`| `0x0`|
|`[5]`|`rfista`| rfi/lom status| `RO`| `0x0`| `0x0`|
|`[4]`|`timsta`| tim status| `RO`| `0x0`| `0x0`|
|`[3]`|`uneqsta`| uneq status| `RO`| `0x0`| `0x0`|
|`[2]`|`plmsta`| plm status| `RO`| `0x0`| `0x0`|
|`[1]`|`aissta`| ais status| `RO`| `0x0`| `0x0`|
|`[0]`|`lopsta`| lop status| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Status Report STS

* **Description**           

This register is used to get POH alarm change status report.


* **RTL Instant Name**    : `alm_chghi`

* **Address**             : `0x0F_4400`

* **Formula**             : `0x0F_4400 + $STS*$SINTOFF + $OCID`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-8) : Line ID`

    * `$SINTOFF(16-16): sts interrupt offset`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[14]`|`rdistachg`| rdi  status change| `W1C`| `0x0`| `0x0`|
|`[13]`|`jnstachg`| jn message change| `W1C`| `0x0`| `0x0`|
|`[12]`|`pslstachg`| psl byte change| `W1C`| `0x0`| `0x0`|
|`[11]`|`bersdstachg`| bersd stable status change| `W1C`| `0x0`| `0x0`|
|`[10]`|`bersfstachg`| bersf stable status change| `W1C`| `0x0`| `0x0`|
|`[9]`|`erdistachg`| erdis status change| `W1C`| `0x0`| `0x0`|
|`[8]`|`bertcastachg`| bertca status change| `W1C`| `0x0`| `0x0`|
|`[7]`|`erdicstachg`| erdic status change| `W1C`| `0x0`| `0x0`|
|`[6]`|`erdipstachg`| erdip status change| `W1C`| `0x0`| `0x0`|
|`[5]`|`rfistachg`| rfi/lom status change| `W1C`| `0x0`| `0x0`|
|`[4]`|`timstachg`| tim status change| `W1C`| `0x0`| `0x0`|
|`[3]`|`uneqstachg`| uneq status change| `W1C`| `0x0`| `0x0`|
|`[2]`|`plmstachg`| plm status change| `W1C`| `0x0`| `0x0`|
|`[1]`|`aisstachg`| ais status change| `W1C`| `0x0`| `0x0`|
|`[0]`|`lopstachg`| lop status change| `W1C`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Or Status Report STS

* **Description**           

This register is used to get POH alarm or changing status report.


* **RTL Instant Name**    : `alm_orstahi`

* **Address**             : `0x0F_4C00`

* **Formula**             : `0x0F_4C00 + $STS `

* **Where**               : 

    * `$STS(0-47)  : STS`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:9]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[8:0]`|`orstachg`| or changing status  bit, each bit is represent for each Line changing status line8,...line1,line0. Line 8 for debug at Framer level| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Status Report STS

* **Description**           

This register is used to get POH alarm global change status report.


* **RTL Instant Name**    : `alm_glbchghi`

* **Address**             : `0x0F_4FFE`

* **Formula**             : `0x0F_4FFE + $GRPID`

* **Where**               : 

    * `$GRPID(0-1) : Group ID, each group contain 32 STS divided from 48 STS`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`glbstachg`| global status change bit for 32 STS STS31,...STS1,STS0| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Mask Report STS

* **Description**           

This register is used to get POH alarm global mask report.


* **RTL Instant Name**    : `alm_glbmskhi`

* **Address**             : `0x0F_4FFC`

* **Formula**             : `0x0F_4FFC + $GRPID`

* **Where**               : 

    * `$GRPID(0-1) : Group ID, each group contain 32 STS divided from 48 STS`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`glbmsk`| global mask for 32 STS STS31,...STS1,STS0| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Alarm Status Mask Report VT/TU3

* **Description**           

This register is used to get POH alarm mask report.


* **RTL Instant Name**    : `alm_msklo`

* **Address**             : `0x0E_0000`

* **Formula**             : `0x0E_0000 + $STS*32 + $OCID*$VINTOFF + $VTID`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-7) : Line ID`

    * `$VTID(0-27)  : VT ID`

    * `$VINTOFF(8192-8192): VT interrupt offset`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[14]`|`rdimsk`| rdi mask| `RW`| `0x0`| `0x0`|
|`[13]`|`jnstachgmsk`| pslstachg mask| `RW`| `0x0`| `0x0`|
|`[12]`|`pslstachgmsk`| jnstachg mask| `RW`| `0x0`| `0x0`|
|`[11]`|`bersdmsk`| bersd mask| `RW`| `0x0`| `0x0`|
|`[10]`|`bersfmsk`| bersf  mask| `RW`| `0x0`| `0x0`|
|`[9]`|`erdimsk`| erdis mask| `RW`| `0x0`| `0x0`|
|`[8]`|`bertcamsk`| bertca mask| `RW`| `0x0`| `0x0`|
|`[7]`|`erdicmsk`| erdic  mask| `RW`| `0x0`| `0x0`|
|`[6]`|`erdipmsk`| erdip  mask| `RW`| `0x0`| `0x0`|
|`[5]`|`rfimsk`| rfi mask| `RW`| `0x0`| `0x0`|
|`[4]`|`timmsk`| tim mask| `RW`| `0x0`| `0x0`|
|`[3]`|`uneqmsk`| uneq mask| `RW`| `0x0`| `0x0`|
|`[2]`|`plmmsk`| plm mask| `RW`| `0x0`| `0x0`|
|`[1]`|`aismsk`| ais mask| `RW`| `0x0`| `0x0`|
|`[0]`|`lopmsk`| lop mask| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Alarm Status Report VT/TU3

* **Description**           

This register is used to get POH alarm status.


* **RTL Instant Name**    : `alm_stalo`

* **Address**             : `0x0E_1000`

* **Formula**             : `0x0E_1000 + $STS*32 + $OCID*$VINTOFF + $VTID`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-7) : Line ID`

    * `$VTID(0-27)  : VT ID`

    * `$VINTOFF(8192-8192): VT interrupt offset`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[14]`|`rdista`| rdi  status| `RO`| `0x0`| `0x0`|
|`[13:12]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[11]`|`bersdsta`| bersd status| `RW`| `0x0`| `0x0`|
|`[10]`|`bersfsta`| bersf  status| `RW`| `0x0`| `0x0`|
|`[9]`|`erdista`| erdis  status| `RO`| `0x0`| `0x0`|
|`[8]`|`bertcasta`| bertca status| `RO`| `0x0`| `0x0`|
|`[7]`|`erdicsta`| erdic status| `RO`| `0x0`| `0x0`|
|`[6]`|`erdipsta`| erdip status| `RO`| `0x0`| `0x0`|
|`[5]`|`rfista`| rfi status| `RO`| `0x0`| `0x0`|
|`[4]`|`timsta`| tim status| `RO`| `0x0`| `0x0`|
|`[3]`|`uneqsta`| uneq status| `RO`| `0x0`| `0x0`|
|`[2]`|`plmsta`| plm status| `RO`| `0x0`| `0x0`|
|`[1]`|`aissta`| ais status| `RO`| `0x0`| `0x0`|
|`[0]`|`lopsta`| lop status| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Status Report VT/TU3

* **Description**           

This register is used to get POH alarm change status report.


* **RTL Instant Name**    : `alm_chglo`

* **Address**             : `0x0E_0800`

* **Formula**             : `0x0E_0800 + $STS*32 + $OCID*$VINTOFF + $VTID`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-7) : Line ID`

    * `$VTID(0-27)  : VT ID`

    * `$VINTOFF(8192-8192): VT interrupt offset`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[14]`|`rdistachg`| rdi  status change| `W1C`| `0x0`| `0x0`|
|`[13]`|`jnstachg`| jn message change| `W1C`| `0x0`| `0x0`|
|`[12]`|`pslstachg`| psl byte change| `W1C`| `0x0`| `0x0`|
|`[11]`|`bersdstachg`| bersd stable status change| `W1C`| `0x0`| `0x0`|
|`[10]`|`bersfstachg`| bersf stable status change| `W1C`| `0x0`| `0x0`|
|`[9]`|`erdistachg`| erdis status change| `W1C`| `0x0`| `0x0`|
|`[8]`|`bertcastachg`| bertca status change| `W1C`| `0x0`| `0x0`|
|`[7]`|`erdicstachg`| erdic status change| `W1C`| `0x0`| `0x0`|
|`[6]`|`erdipstachg`| erdip status change| `W1C`| `0x0`| `0x0`|
|`[5]`|`rfistachg`| rfi status change| `W1C`| `0x0`| `0x0`|
|`[4]`|`timstachg`| tim status change| `W1C`| `0x0`| `0x0`|
|`[3]`|`uneqstachg`| uneq status change| `W1C`| `0x0`| `0x0`|
|`[2]`|`plmstachg`| plm status change| `W1C`| `0x0`| `0x0`|
|`[1]`|`aisstachg`| ais status change| `W1C`| `0x0`| `0x0`|
|`[0]`|`lopstachg`| lop status change| `W1C`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Or Status Report VT/TU3

* **Description**           

This register is used to get POH alarm or status change status report.


* **RTL Instant Name**    : `alm_orstalo`

* **Address**             : `0x0E_1800`

* **Formula**             : `0x0E_1800 + $STS + $OCID*64`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-7) : Line ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:0]`|`orstachg`| or status change bit| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Status Report VT/TU3

* **Description**           

This register is used to get POH alarm global change status report.


* **RTL Instant Name**    : `alm_glbchglo`

* **Address**             : `0x0E_FFF0`

* **Formula**             : `0x0E_FFF0 + $GRPID`

* **Where**               : 

    * `$GRPID(0-11) : Group ID, each group contain 32 STS divided from 384 STS of 8 Line`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`glbstachg`| global status change bit for 32 STS STS31,...STS1,STS0| `RO`| `0x0`| `0x0 // End: ###################################################################################### Begin:`|

###POH Interrupt Global Mask Report VT/TU3

* **Description**           

This register is used to get POH alarm global mask report.


* **RTL Instant Name**    : `alm_glbmsklo`

* **Address**             : `0x0E_FFE0`

* **Formula**             : `0x0E_FFE0 + $GRPID`

* **Where**               : 

    * `$GRPID(0-11) : Group ID, each group contain 32 STS divided from 384 STS of 8 Line`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`glbmsk`| global status change bit for 32 STS STS31,...STS1,STS0| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Mask Report

* **Description**           

This register is used to get POH,PDH alarm global mask report for high order.


* **RTL Instant Name**    : `alm_glbmskhislice`

* **Address**             : `0x00_0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[3:0]`|`glbmskhi`| global mask change bit for high order group - pdhgrp1,pdhgrp0,pohgrp1,pohgrp0| `RW`| `0x0`| `0x0 // End: ###################################################################################### Begin:`|

###POH Interrupt Global Mask Report

* **Description**           

This register is used to get POH,PDH alarm global mask report for low order.


* **RTL Instant Name**    : `alm_glbmsk1oslice`

* **Address**             : `0x00_0024`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[23:0]`|`glbmsklo`| Global mask bit for low order group - pdhgrp11...,pdhgrp1,pdhgrp0,pohgrp11...pohgrp1,pohgrp0| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Status Report

* **Description**           

This register is used to get POH,PDH alarm global change status report for high order.


* **RTL Instant Name**    : `alm_glbchghislice`

* **Address**             : `0x00_0005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[3:0]`|`glbstachghi`| global status change bit for high order group - pdhgrp1,pdhgrp0,pohgrp1,pohgrp0| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Status Report

* **Description**           

This register is used to get POH,PDH alarm global change status report for low order.


* **RTL Instant Name**    : `alm_glbchgloslice`

* **Address**             : `0x00_0025`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[23:0]`|`glbstachglo`| global status change bit for low order group - pdhgrp11...pdhgrp1,pdhgrp0,grp11...grp1,grp0| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt  Global Status Out Report

* **Description**           

This register is used to get POH,PDH alarm global change status report for high,low order after ANDED with mask.


* **RTL Instant Name**    : `alm_glbchgo`

* **Address**             : `0x00_0006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`glbstachglo`| global status change bit for high order group - pdhgrp11...pdhgrp1,pdhgrp0,grp11...grp1,grp0| `RO`| `0x0`| `0x0`|
|`[7:4]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[3:0]`|`glbstachghi`| global status change bit for high order group , pdhgrp1,pdhgrp0,pohgrp1,pohgrp0| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH BER Measured STS/VT Channel Control 1

* **Description**           

This register is used to config measured channel .


* **RTL Instant Name**    : `ramberstscontrl`

* **Address**             : `0x06_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[14:0]`|`chncontrl`| Channel control in STS*2 + OCID*128 + TU3TYPE + 10752| `RW`| `0x0`| `0x0 STS: 0-47 for OCID = 0-7, or LineID for OCID = 8 OCID: 0-7 Path level, 8 Line level TU3TYPE: 1: TU3, 0: STS If channel is VT , Channel control will be in STS*28 + OCID*1344 + VT End: Begin:`|

###POH BER Measured STS/VT Channel Control 2

* **Description**           

This register is used to config measured channel .


* **RTL Instant Name**    : `ramberstscontrl2`

* **Address**             : `0x06_0013`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[14:0]`|`chncontrl`| Channel control in STS*2 + OCID*128 + TU3TYPE + 10752| `RW`| `0x0`| `0x0 STS: 0-47 for OCID = 0-7, or LineID for OCID = 8 OCID: 0-7 Path level, 8 Line level TU3TYPE: 1: TU3, 0: STS If channel is VT , Channel control will be in STS*28 + OCID*1344 + VT End: Begin:`|

###PDH Alarm Status Mask Report DE3

* **Description**           

This register is used to get PDH alarm mask report.


* **RTL Instant Name**    : `dealm_mskhi`

* **Address**             : `0x06_6000`

* **Formula**             : `0x06_6000 + $DE3*$SINTOFF + $OCID`

* **Where**               : 

    * `$DE3(0-47)  : DE3`

    * `$OCID(0-7) : Line ID`

    * `$SINTOFF(16-16): DE3 interrupt offset`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[2]`|`bertcamsk`| bertca mask| `RW`| `0x0`| `0x0`|
|`[1]`|`bersdmsk`| bersd mask| `RW`| `0x0`| `0x0`|
|`[0]`|`bersfmsk`| bersf  mask| `RW`| `0x0`| `0x0 // End: Begin:`|

###PDH Alarm Status Report DE3

* **Description**           

This register is used to get PDH alarm status report.


* **RTL Instant Name**    : `dealm_stahi`

* **Address**             : `0x06_6800`

* **Formula**             : `0x06_6800 + $DE3*$SINTOFF + $OCID`

* **Where**               : 

    * `$DE3(0-47)  : DE3`

    * `$OCID(0-7) : Line ID`

    * `$SINTOFF(16-16): DE3 interrupt offset`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[2]`|`bertcasta`| bertca status| `RO`| `0x0`| `0x0`|
|`[1]`|`bersdsta`| bersd  status| `RO`| `0x0`| `0x0`|
|`[0]`|`bersfsta`| bersf  status| `RO`| `0x0`| `0x0 // End: Begin:`|

###PDH Interrupt Status Report DE3

* **Description**           

This register is used to get PDH alarm change status report.


* **RTL Instant Name**    : `dealm_chghi`

* **Address**             : `0x06_6400`

* **Formula**             : `0x06_6400 + $DE3*$SINTOFF + $OCID`

* **Where**               : 

    * `$DE3(0-47)  : DE3`

    * `$OCID(0-7) : Line ID`

    * `$SINTOFF(16-16): DE3 interrupt offset`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[2]`|`bertcastachg`| bertca status change| `W1C`| `0x0`| `0x0`|
|`[1]`|`bersdstachg`| bersd stable status change| `W1C`| `0x0`| `0x0`|
|`[0]`|`bersfstachg`| bersf stable status change| `W1C`| `0x0`| `0x0 // End: Begin:`|

###PDH Interrupt Or Status Report DE3

* **Description**           

This register is used to get PDH alarm or changing status report.


* **RTL Instant Name**    : `dealm_orstahi`

* **Address**             : `0x06_6C00`

* **Formula**             : `0x06_6C00 + $DE3 `

* **Where**               : 

    * `$DE3(0-47)  : DE3`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[7:0]`|`orstachg`| or changing status  bit, each bit is represent for each Line changing status line7,...line1,line0.| `RO`| `0x0`| `0x0 End: Begin:`|

###PDH Interrupt Global Status Report DE3

* **Description**           

This register is used to get PDH alarm global change status report.


* **RTL Instant Name**    : `dealm_glbchghi`

* **Address**             : `0x06_6FFE`

* **Formula**             : `0x06_6FFE + $GRPID`

* **Where**               : 

    * `$GRPID(0-1) : Group ID, each group contain 32 DE3 divided from 48 DE3`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`glbstachg`| global status change bit for 32 DE3 DE331,...DE31,DE30| `RO`| `0x0`| `0x0 // End: Begin:`|

###PDH Interrupt Global Mask Report DE3

* **Description**           

This register is used to get PDH alarm global mask report.


* **RTL Instant Name**    : `dealm_glbmskhi`

* **Address**             : `0x06_6FFC`

* **Formula**             : `0x06_6FFC + $GRPID`

* **Where**               : 

    * `$GRPID(0-1) : Group ID, each group contain 32 DE3 divided from 48 DE3`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`glbmsk`| global mask for 32 DE3 DE331,...DE31,DE30| `RW`| `0x0`| `0x0 // End: Begin:`|

###PDH Alarm Status Mask Report DE1

* **Description**           

This register is used to get PDH alarm mask report.


* **RTL Instant Name**    : `dealm_msklo`

* **Address**             : `0x07_0000`

* **Formula**             : `0x07_0000 + $DE3*32 + $OCID*$VINTOFF + $DE1ID`

* **Where**               : 

    * `$DE3(0-47)  : DE3`

    * `$OCID(0-7) : Line ID`

    * `$DE1ID(0-27)  : DE1 ID`

    * `$VINTOFF(8192-8192): DE1 interrupt offset`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[2]`|`bertcamsk`| bertca mask| `RW`| `0x0`| `0x0`|
|`[1]`|`bersdmsk`| bersd mask| `RW`| `0x0`| `0x0`|
|`[0]`|`bersfmsk`| bersf  mask| `RW`| `0x0`| `0x0 // End: Begin:`|

###PDH Alarm Status Report DE1

* **Description**           

This register is used to get PDH alarm status.


* **RTL Instant Name**    : `dealm_stalo`

* **Address**             : `0x07_1000`

* **Formula**             : `0x07_1000 + $DE3*32 + $OCID*$VINTOFF + $DE1ID`

* **Where**               : 

    * `$DE3(0-47)  : DE3`

    * `$OCID(0-7) : Line ID`

    * `$DE1ID(0-27)  : DE1 ID`

    * `$VINTOFF(8192-8192): DE1 interrupt offset`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[2]`|`bertcasta`| bertca status| `RO`| `0x0`| `0x0`|
|`[1]`|`bersdsta`| bersd status| `RW`| `0x0`| `0x0`|
|`[0]`|`bersfsta`| bersf  status| `RW`| `0x0`| `0x0 // End: Begin:`|

###PDH Interrupt Status Report DE1

* **Description**           

This register is used to get PDH alarm change status report.


* **RTL Instant Name**    : `dealm_chglo`

* **Address**             : `0x07_0800`

* **Formula**             : `0x07_0800 + $DE3*32 + $OCID*$VINTOFF + $DE1ID`

* **Where**               : 

    * `$DE3(0-47)  : DE3`

    * `$OCID(0-7) : Line ID`

    * `$DE1ID(0-27)  : DE1 ID`

    * `$VINTOFF(8192-8192): DE1 interrupt offset`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[2]`|`bertcastachg`| bertca status change| `W1C`| `0x0`| `0x0`|
|`[1]`|`bersdstachg`| bersd stable status change| `W1C`| `0x0`| `0x0`|
|`[0]`|`bersfstachg`| bersf stable status change| `W1C`| `0x0`| `0x0 // End: Begin:`|

###PDH Interrupt Or Status Report DE1

* **Description**           

This register is used to get PDH alarm or status change status report.


* **RTL Instant Name**    : `dealm_orstalo`

* **Address**             : `0x07_1800`

* **Formula**             : `0x07_1800 + $DE3 + $OCID*64`

* **Where**               : 

    * `$DE3(0-47)  : DE3`

    * `$OCID(0-7) : Line ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:0]`|`orstachg`| or status change bit| `RO`| `0x0`| `0x0 // End: Begin:`|

###PDH Interrupt Global Status Report DE1

* **Description**           

This register is used to get PDH alarm global change status report.


* **RTL Instant Name**    : `dealm_glbchglo`

* **Address**             : `0x07_FFF0`

* **Formula**             : `0x07_FFF0 + $GRPID`

* **Where**               : 

    * `$GRPID(0-11) : Group ID, each group contain 32 DE3 divided from 384 DE3 of 8 Line`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`glbstachg`| global status change bit for 32 DE3 DE331,...DE31,DE30| `RO`| `0x0`| `0x0 // End: ###################################################################################### Begin:`|

###PDH Interrupt Global Mask Report DE1

* **Description**           

This register is used to get PDH alarm global mask report.


* **RTL Instant Name**    : `dealm_glbmsklo`

* **Address**             : `0x07_FFE0`

* **Formula**             : `0x07_FFE0 + $GRPID`

* **Where**               : 

    * `$GRPID(0-11) : Group ID, each group contain 32 DE3 divided from 384 DE3 of 8 Line`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`glbmsk`| global status change bit for 32 DE3 DE331,...DE31,DE30| `RW`| `0x0`| `0x0 // End:`|

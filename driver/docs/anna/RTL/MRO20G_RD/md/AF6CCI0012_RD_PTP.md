## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CCI0012_RD_PTP
####Register Table

|Name|Address|
|-----|-----|
|`Hold Register 1`|`0x00_0000`|
|`Hold Register 2`|`0x00_0001`|
|`Hold Register 3`|`0x00_0002`|
|`PTP enable`|`0x00_0003`|
|`PTP enable`|`0x00_0004`|
|`PTP enable`|`0x00_0005`|
|`PTP Device`|`0x00_0006`|
|`Config Master SPID`|`0x00_0007`|
|`Config Slaver ReqPID`|`0x00_0008`|
|`Config chsv4 bypass`|`0x00_0009`|
|`PTP Ready`|`0x00_0015`|
|`Counter TX FACE PTP`|`0x00_0400(R2C)`|
|`Counter TX FACE PTP`|`0x00_0410(RO)`|
|`Counter PTP Packet`|`0x00_0460(R2C)`|
|`Counter PTP Packet`|`0x00_0470(RO)`|
|`Counter Sync Received`|`0x00_480(R2C)`|
|`Counter Sync Received`|`0x00_4C0(RO)`|
|`Counter Folu Received`|`0x00_490(R2C)`|
|`Counter Folu Received`|`0x00_4D0(RO)`|
|`Counter dreq Received`|`0x00_4A0(R2C)`|
|`Counter dreq Received`|`0x00_4E0(RO)`|
|`Counter dres Received`|`0x00_4B0(R2C)`|
|`Counter dres Received`|`0x00_4F0(RO)`|
|`Counter Rx Sync Extract Err`|`0x00_500(R2C)`|
|`Counter Rx Sync Extract Err`|`0x00_0540(RO)`|
|`Counter Rx Folu Extract Err`|`0x00_0510(R2C)`|
|`Counter Rx Folu Extract Err`|`0x00_0550(RO)`|
|`Counter Rx Dreq Extract Err`|`0x00_0520(R2C)`|
|`Counter Rx Dreq Extract Err`|`0x00_0560(RO)`|
|`Counter Rx Dres Extract Err`|`0x00_0530(R2C)`|
|`Counter Rx Dres Extract Err`|`0x00_0570(RO)`|
|`Counter Rx Sync Chsum Err`|`0x00_0580(R2C)`|
|`Counter Rx Sync Chsum Err`|`0x00_05C0(RO)`|
|`Counter Rx Folu Chsum Err`|`0x00_0590(R2C)`|
|`Counter Rx Folu Chsum Err`|`0x00_05D0(RO)`|
|`Counter Rx Dreq Chsum Err`|`0x00_05A0(R2C)`|
|`Counter Rx Dreq Chsum Err`|`0x00_5E0(RO)`|
|`Counter Rx Dres Chsum Err`|`0x00_05B0(R2C)`|
|`Counter Rx Dres Chsum Err`|`0x00_05F0(RO)`|
|`Counter Sync Transmit`|`0x00_0600(R2C)`|
|`Counter Sync Transmit`|`0x00_0640(RO)`|
|`Counter Folu Transmit`|`0x00_0610(R2C)`|
|`Counter Folu Transmit`|`0x00_0650(RO)`|
|`Counter dreq transmit`|`0x00_0620(R2C)`|
|`Counter dreq transmit`|`0x00_0660(RO)`|
|`Counter dres transmit`|`0x00_0630(R2C)`|
|`Counter dres transmit`|`0x00_0670(RO)`|
|`classify sticky`|`0x00_0330`|
|`PTP check sticky`|`0x00_0331`|
|`ptp getinfo sticky`|`0x00_0332`|
|`trans sticky`|`0x00_0333`|
|`cpu queue sticky`|`0x00_0334`|
|`Config t1t3mode`|`0x00_0017`|
|`Config cpu flush`|`0x00_0018`|
|`Read CPU Queue`|`0x00_0216`|
|`Enb CPU Interupt`|`0x00_0200`|
|`Interupt CPU Queue`|`0x00_0201`|
|`Interupt CPU Queue`|`0x00_0202`|
|`config unicast MAC address global`|`0x00_4003`|
|`config multicast MAC address global 1`|`0x00_4004`|
|`config multicast MAC address global 2`|`0x00_4005`|
|`config any MAC enable`|`0x00_4006`|
|`config MAC global matching enable`|`0x00_4007`|
|`Config mul ipv4 global 1`|`0x00_400A`|
|`Config mul ipv4 global 2`|`0x00_400B`|
|`Config mul ipv6 global 1`|`0x00_400C`|
|`Config mul ipv6 global 2`|`0x00_400D`|
|`config any IP enable`|`0x00_400E`|
|`CLA Ready`|`0x00_4011`|
|`PTP layer force value`|`0x00_4016`|
|`MRU Packet Size`|`0x00_4017`|
|`cfg uni/mul mac`|`0x00_5100-0x00_511F`|
|`Config GE unicast/multicast IPv4/IPv6 address 1`|`0x00_5120-0x00_513F`|
|`Config GE unicast/multicast IPv4/IPv6 address 2`|`0x00_5140-0x00_515F`|
|`Config GE unicast/multicast IPv4/IPv6 address 3`|`0x00_5160-0x00_517F`|
|`Config GE unicast/multicast IPv4/IPv6 address 4`|`0x00_5180-0x00_519F`|
|`cfg ptp protocol`|`0x00_51E0-0x00_51EF`|
|`Config GE port ipv4/ipv6 matching enable`|`0x00_51F0-0x00_51FF`|
|`eth2cla sticky`|`0x00_4100`|
|`cla2epa sticky`|`0x00_4101`|
|`cla2ptp sticky`|`0x00_4102`|
|`parser sticky 0`|`0x00_4104`|
|`parser sticky 1`|`0x00_4105`|
|`lookup sticky`|`0x00_4106`|
|`datashift sticky`|`0x00_4107`|
|`allc sticky`|`0x00_410A`|
|`allc_blkfre_cnt`|`0x00_4200`|
|`allc_blkiss_cnt`|`0x00_4201`|
|`todinit_cfg`|`0x2000`|
|`tod_current`|`0x2001`|
|`todmode_cfg`|`0x2002`|
|`bp_latency_cfg`|`0x2020 - 0x2021`|
|`face_latency_cfg`|`0x2040 - 0x204F`|
|`Hold Register 40G 1`|`0x00_B020`|
|`Hold Register 40G 2`|`0x00_B021`|
|`Hold Register 40G 3`|`0x00_B022`|
|`PTP enable`|`0x00_8004`|
|`PTP enable`|`0x00_8005`|
|`PTP Device`|`0x00_8006`|
|``|`0x00_B000`|
|``|`0x00_B001`|
|``|`0x00_B002`|
|``|`0x00_B003`|
|``|`0x00_B004`|
|``|`0x00_B005`|
|``|`0x00_B006`|
|``|`0x00_B007`|
|``|`0x00_B008`|
|``|`0x00_B009`|
|``|`0x00_B00A`|
|``|`0x00_B00B`|
|``|`0x00_B00C`|
|``|`0x00_B00D`|
|``|`0x00_B00E`|
|``|`0x00_B00F`|
|``|`0x00_B010`|
|``|`0x00_B011`|
|``|`0x00_B012`|
|``|`0x00_8460(R2C)`|
|``|`0x008470(RO)`|
|``|`0x00_8420(R2C)`|
|``|`0x008430(RO)`|
|``|`0x00_8400(R2C)`|
|``|`0x008410(RO)`|
|`Counter Sync Received 40G PORT`|`0x00_8480(R2C)`|
|`Counter Sync Received 40G PORT`|`0x00_84C0(RO)`|
|`Counter Folu Received 40G PORT`|`0x00_8490(R2C)`|
|`Counter Folu Received 40G PORT`|`0x00_84D0(RO)`|
|`Counter dreq Received 40G PORT`|`0x00_84A0(R2C)`|
|`Counter dreq Received 40G PORT`|`0x00_84E0(RO)`|
|`Counter dres Received 40G PORT`|`0x00_84B0(R2C)`|
|`Counter dres Received 40G PORT`|`0x00_84F0(RO)`|
|`Counter Rx Sync Extract Err 40G PORT`|`0x00_8500(R2C)`|
|`Counter Rx Sync Extract Err 40G PORT`|`0x00_8540(RO)`|
|`Counter Rx Folu Extract Err 40G PORT`|`0x00_8510(R2C)`|
|`Counter Rx Folu Extract Err 40G PORT`|`0x00_8550(RO)`|
|`Counter Rx Dreq Extract Err 40G PORT`|`0x00_8520(R2C)`|
|`Counter Rx Dreq Extract Err 40G PORT`|`0x00_8560(RO)`|
|`Counter Rx Dres Extract Err 40G PORT`|`0x00_8530(R2C)`|
|`Counter Rx Dres Extract Err 40G PORT`|`0x00_8570(RO)`|
|`Counter Rx Sync Chsum Err 40G PORT`|`0x00_8580(R2C)`|
|`Counter Rx Sync Chsum Err 40G PORT`|`0x00_85C0(RO)`|
|`Counter Rx Folu Chsum Err 40G PORT`|`0x00_8590(R2C)`|
|`Counter Rx Folu Chsum Err 40G PORT`|`0x00_85D0(RO)`|
|`Counter Rx Dreq Chsum Err 40G PORT`|`0x00_85A0(R2C)`|
|`Counter Rx Dreq Chsum Err 40G PORT`|`0x00_85E0(RO)`|
|`Counter Rx Dres Chsum Err 40G PORT`|`0x00_85B0(R2C)`|
|`Counter Rx Dres Chsum Err 40G PORT`|`0x00_85F0(RO)`|
|`Counter Sync Transmit 40G PORT`|`0x00_8600(R2C)`|
|`Counter Sync Transmit 40G PORT`|`0x00_8640(RO)`|
|`Counter Folu Transmit 40G PORT`|`0x00_8610(R2C)`|
|`Counter Folu Transmit 40G PORT`|`0x00_8650(RO)`|
|`Counter dreq transmit`|`0x00_8620(R2C)`|
|`Counter dreq transmit`|`0x00_8660(RO)`|
|`Counter dres transmit`|`0x00_8630(R2C)`|
|`Counter dres transmit`|`0x00_8670(RO)`|
|`Read CPU Queue`|`0x00_8216`|
|`Enb CPU Interupt`|`0x00_8200`|
|`Interupt CPU Queue`|`0x00_8201`|
|`Interupt CPU Queue`|`0x00_8202`|
|`Config t1t3mode`|`0x00_8017`|
|`PTP interupt status`|`0x00_80FE`|
|`config vlan id global`|`0x00_4021-0x00_4024`|
|`config vlan id global enable`|`0x00_4020`|
|`config vlan id per port`|`0x00_51A0-0x00_51AF`|
|`config vlan40 id global`|`0x00_B040-0x00_B043`|
|`config vlan40 id perport`|`0x0_A000-0x0_A00F`|
|`PTP Face Dis Timestaping`|`0x00_0020`|
|`PTP BP Dis Timestamping`|`0x00_8020`|


###Hold Register 1

* **Description**           

This register hold value 1


* **RTL Instant Name**    : `upen_hold1`

* **Address**             : `0x00_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cfg_hold1`| hold value 1| `RW`| `0x0`| `0x0 End: Begin:`|

###Hold Register 2

* **Description**           

This register hold value 2


* **RTL Instant Name**    : `upen_hold2`

* **Address**             : `0x00_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cfg_hold2`| hold value 2| `RW`| `0x0`| `0x0 End: Begin:`|

###Hold Register 3

* **Description**           

This register hold value


* **RTL Instant Name**    : `upen_hold3`

* **Address**             : `0x00_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cfg_hold3`| hold value 3| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP enable

* **Description**           

used to enable ptp for 16 port ingress


* **RTL Instant Name**    : `upen_ptp_en`

* **Address**             : `0x00_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_ptp_en`| '1' : enable, '0': disable| `RW`| `0xFFFF`| `0xFFFF End: Begin:`|

###PTP enable

* **Description**           

used to indicate 1-step/2-step mode for each port


* **RTL Instant Name**    : `upen_ptp_stmode`

* **Address**             : `0x00_0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_ptp_stmode`| '1' : 2-step, '0': 1-step| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP enable

* **Description**           

used to indicate master/slaver mode for each port


* **RTL Instant Name**    : `upen_ptp_ms`

* **Address**             : `0x00_0005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_ptp_ms`| '1' : master, '0': slaver| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP Device

* **Description**           

This register is used to config PTP device


* **RTL Instant Name**    : `upen_ptp_dev`

* **Address**             : `0x00_0006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[01:00]`|`device_mode`| '00': BC mode, '01': reserved '10': TC Separate mode, '11': TC General mode| `RW`| `0x2`| `0x2 End: Begin:`|

###Config Master SPID

* **Description**           

Used to checking sourcePortIdentity of Sync, Follow-up and Delay Response packet


* **RTL Instant Name**    : `upen_ms_srcpid`

* **Address**             : `0x00_0007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `96`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[95:80]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[79:00]`|`cfg_ms_srcpid`| sourcePortIdentity value of Master| `RW`| `0x0`| `0x0 End: Begin:`|

###Config Slaver ReqPID

* **Description**           

Used to checking requestingPortIdentity of Delay Response packet


* **RTL Instant Name**    : `upen_sl_reqpid`

* **Address**             : `0x00_0008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `96`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[95:80]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[79:00]`|`cfg_sl_reqpid`| RequestingPortIdentity value of Slaver| `RW`| `0x0`| `0x0 End: Begin:`|

###Config chsv4 bypass

* **Description**           

Used to indicate bypass ipv4 header checksum error


* **RTL Instant Name**    : `upen_chsv4_bypass`

* **Address**             : `0x00_0009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`cfg_chsv4_bypass`| '1': enable, '0': disable| `RW`| `0x1`| `0x1 End: [15:0] : en/dis uni mac matching for port 0 to port 15`|

###PTP Ready

* **Description**           

This register is used to indicate config ptp done


* **RTL Instant Name**    : `upen_ptp_rdy`

* **Address**             : `0x00_0015`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`ptp_rdy`| '1' : ready| `RW`| `0x00`| `0x00 End: '000': L2 '001': ipv4 '010': ipv6 '011': ipv4 vpn '100': ipv6 vpn`|

###Counter TX FACE PTP

* **Description**           

Total tx face packet


* **RTL Instant Name**    : `upen_tx_ptp_cnt_r2c`

* **Address**             : `0x00_0400(R2C)`

* **Formula**             : `0x00_0400 +  $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tx_face_cnt`| total tx face packet| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter TX FACE PTP

* **Description**           

Total tx face packet


* **RTL Instant Name**    : `upen_tx_ptp_cnt_ro`

* **Address**             : `0x00_0410(RO)`

* **Formula**             : `0x00_0410 +  $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tx_face_cnt`| total tx face packet| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter PTP Packet

* **Description**           

Total rx face packet


* **RTL Instant Name**    : `upen_rx_ptp_cnt_r2c`

* **Address**             : `0x00_0460(R2C)`

* **Formula**             : `0x00_0460 +  $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_ptp_cnt`| Total rx ptp packet| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter PTP Packet

* **Description**           

Total rx face packet


* **RTL Instant Name**    : `upen_rx_ptp_cnt_ro`

* **Address**             : `0x00_0470(RO)`

* **Formula**             : `0x00_0470 +  $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_ptp_cnt`| Total rx ptp packet| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Sync Received

* **Description**           

Number of Sync message received


* **RTL Instant Name**    : `upen_rx_sync_cnt_r2c`

* **Address**             : `0x00_480(R2C)`

* **Formula**             : `0x00_480+ $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`syncrx_cnt`| number sync packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Sync Received

* **Description**           

Number of Sync message received


* **RTL Instant Name**    : `upen_rx_sync_cnt_ro`

* **Address**             : `0x00_4C0(RO)`

* **Formula**             : `0x00_4C0+ $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`syncrx_cnt`| number sync packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Folu Received

* **Description**           

Number of folu message received


* **RTL Instant Name**    : `upen_rx_folu_cnt_r2c`

* **Address**             : `0x00_490(R2C)`

* **Formula**             : `0x00_490 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`folurx_cnt`| number folu packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Folu Received

* **Description**           

Number of folu message received


* **RTL Instant Name**    : `upen_rx_folu_cnt_ro`

* **Address**             : `0x00_4D0(RO)`

* **Formula**             : `0x00_4D0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`folurx_cnt`| number folu packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dreq Received

* **Description**           

Number of dreq message received


* **RTL Instant Name**    : `upen_rx_dreq_cnt_r2c`

* **Address**             : `0x00_4A0(R2C)`

* **Formula**             : `0x00_4A0+$pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dreqrx_cnt`| number dreq packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dreq Received

* **Description**           

Number of dreq message received


* **RTL Instant Name**    : `upen_rx_dreq_cnt_ro`

* **Address**             : `0x00_4E0(RO)`

* **Formula**             : `0x00_4E0+$pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dreqrx_cnt`| number dreq packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dres Received

* **Description**           

Number of dres message received


* **RTL Instant Name**    : `upen_rx_dres_cnt_r2c`

* **Address**             : `0x00_4B0(R2C)`

* **Formula**             : `0x00_4B0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dresrx_cnt`| number dres packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dres Received

* **Description**           

Number of dres message received


* **RTL Instant Name**    : `upen_rx_dres_cnt_ro`

* **Address**             : `0x00_4F0(RO)`

* **Formula**             : `0x00_4F0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dresrx_cnt`| number dres packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Sync Extract Err

* **Description**           

Number of extr err sync mess


* **RTL Instant Name**    : `upen_rx_extr_err_syncnt_r2c`

* **Address**             : `0x00_500(R2C)`

* **Formula**             : `0x00_500 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_synccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Sync Extract Err

* **Description**           

Number of extr err sync mess


* **RTL Instant Name**    : `upen_rx_extr_err_syncnt_ro`

* **Address**             : `0x00_0540(RO)`

* **Formula**             : `0x00_0540 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_synccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Folu Extract Err

* **Description**           

Number of extr err folu mess


* **RTL Instant Name**    : `upen_rx_extr_err_folucnt_r2c`

* **Address**             : `0x00_0510(R2C)`

* **Formula**             : `0x00_0510 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_foluccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Folu Extract Err

* **Description**           

Number of extr err folu mess


* **RTL Instant Name**    : `upen_rx_extr_err_folucnt_ro`

* **Address**             : `0x00_0550(RO)`

* **Formula**             : `0x00_0550 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_foluccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dreq Extract Err

* **Description**           

Number of extr err dreq mess


* **RTL Instant Name**    : `upen_rx_extr_err_dreqcnt_r2c`

* **Address**             : `0x00_0520(R2C)`

* **Formula**             : `0x00_0520 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_dreqcnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dreq Extract Err

* **Description**           

Number of extr err dreq mess


* **RTL Instant Name**    : `upen_rx_extr_err_dreqcnt_ro`

* **Address**             : `0x00_0560(RO)`

* **Formula**             : `0x00_0560 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_dreqcnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dres Extract Err

* **Description**           

Number of extr err dres mess


* **RTL Instant Name**    : `upen_rx_extr_err_drescnt_r2c`

* **Address**             : `0x00_0530(R2C)`

* **Formula**             : `0x00_0530 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_drescnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dres Extract Err

* **Description**           

Number of extr err dres mess


* **RTL Instant Name**    : `upen_rx_extr_err_drescnt_ro`

* **Address**             : `0x00_0570(RO)`

* **Formula**             : `0x00_0570 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_drescnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Sync Chsum Err

* **Description**           

Number of Chs err sync mess


* **RTL Instant Name**    : `upen_rx_chs_err_syncnt_r2c`

* **Address**             : `0x00_0580(R2C)`

* **Formula**             : `0x00_0580 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_synccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Sync Chsum Err

* **Description**           

Number of Chs err sync mess


* **RTL Instant Name**    : `upen_rx_chs_err_syncnt_ro`

* **Address**             : `0x00_05C0(RO)`

* **Formula**             : `0x00_05C0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_synccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Folu Chsum Err

* **Description**           

Number of Chs err folu mess


* **RTL Instant Name**    : `upen_rx_chs_err_folucnt_r2c`

* **Address**             : `0x00_0590(R2C)`

* **Formula**             : `0x00_0590 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_folucnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Folu Chsum Err

* **Description**           

Number of Chs err folu mess


* **RTL Instant Name**    : `upen_rx_chs_err_folucnt_ro`

* **Address**             : `0x00_05D0(RO)`

* **Formula**             : `0x00_05D0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_folucnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dreq Chsum Err

* **Description**           

Number of Chs err dreq mess


* **RTL Instant Name**    : `upen_rx_chs_err_dreqcnt_r2c`

* **Address**             : `0x00_05A0(R2C)`

* **Formula**             : `0x00_05A0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_dreqcnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dreq Chsum Err

* **Description**           

Number of Chs err dreq mess


* **RTL Instant Name**    : `upen_rx_chs_err_dreqcnt_ro`

* **Address**             : `0x00_5E0(RO)`

* **Formula**             : `0x00_5E0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_dreqcnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dres Chsum Err

* **Description**           

Number of Chs err dres mess


* **RTL Instant Name**    : `upen_rx_chs_err_drescnt_r2c`

* **Address**             : `0x00_05B0(R2C)`

* **Formula**             : `0x00_05B0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_drescnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dres Chsum Err

* **Description**           

Number of Chs err dres mess


* **RTL Instant Name**    : `upen_rx_chs_err_drescnt_ro`

* **Address**             : `0x00_05F0(RO)`

* **Formula**             : `0x00_05F0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_drescnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Sync Transmit

* **Description**           

Number of Sync message transmit


* **RTL Instant Name**    : `upen_tx_sync_cnt_r2c`

* **Address**             : `0x00_0600(R2C)`

* **Formula**             : `0x00_0600 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`synctx_cnt`| number sync packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Sync Transmit

* **Description**           

Number of Sync message transmit


* **RTL Instant Name**    : `upen_tx_sync_cnt_ro`

* **Address**             : `0x00_0640(RO)`

* **Formula**             : `0x00_0640 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`synctx_cnt`| number sync packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Folu Transmit

* **Description**           

Number of folu message transmit


* **RTL Instant Name**    : `upen_tx_folu_cnt_r2c`

* **Address**             : `0x00_0610(R2C)`

* **Formula**             : `0x00_0610 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`folutx_cnt`| number folu packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Folu Transmit

* **Description**           

Number of folu message transmit


* **RTL Instant Name**    : `upen_tx_folu_cnt_ro`

* **Address**             : `0x00_0650(RO)`

* **Formula**             : `0x00_0650 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`folutx_cnt`| number folu packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dreq transmit

* **Description**           

Number of dreq message transmit


* **RTL Instant Name**    : `upen_tx_dreq_cnt_r2c`

* **Address**             : `0x00_0620(R2C)`

* **Formula**             : `0x00_0620 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dreqtx_cnt`| number dreq packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dreq transmit

* **Description**           

Number of dreq message transmit


* **RTL Instant Name**    : `upen_tx_dreq_cnt_ro`

* **Address**             : `0x00_0660(RO)`

* **Formula**             : `0x00_0660 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dreqtx_cnt`| number dreq packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dres transmit

* **Description**           

Number of dres message transmit


* **RTL Instant Name**    : `upen_tx_dres_cnt_r2c`

* **Address**             : `0x00_0630(R2C)`

* **Formula**             : `0x00_0630 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`drestx_cnt`| number dres packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dres transmit

* **Description**           

Number of dres message transmit


* **RTL Instant Name**    : `upen_tx_dres_cnt_ro`

* **Address**             : `0x00_0670(RO)`

* **Formula**             : `0x00_0670 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`drestx_cnt`| number dres packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###classify sticky

* **Description**           

sticky of classify


* **RTL Instant Name**    : `upen_cla_stk`

* **Address**             : `0x00_0330`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cla_stk`| sticky ge_classify| `W1C`| `0x0`| `0x0 End: Begin:`|

###PTP check sticky

* **Description**           

sticky of ptp check


* **RTL Instant Name**    : `upen_chk_stk`

* **Address**             : `0x00_0331`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`chk_stk`| sticky ptp check| `W1C`| `0x0`| `0x0 End: Begin:`|

###ptp getinfo sticky

* **Description**           

sticky of ptp get info


* **RTL Instant Name**    : `upen_getinf_stk`

* **Address**             : `0x00_0332`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`getinf_stk`| sticky ptp get info| `W1C`| `0x0`| `0x0 End: Begin:`|

###trans sticky

* **Description**           

sticky of ptp trans


* **RTL Instant Name**    : `upen_trans_stk`

* **Address**             : `0x00_0333`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`trans_stk`| sticky ptp trans| `W1C`| `0x0`| `0x0 End: Begin:`|

###cpu queue sticky

* **Description**           

sticky of cpu que


* **RTL Instant Name**    : `upen_cpuque_stk`

* **Address**             : `0x00_0334`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cpuque_stk`| sticky of cpu queue| `W1C`| `0x0`| `0x0 End: Begin:`|

###Config t1t3mode

* **Description**           

Used to config t1t3mode


* **RTL Instant Name**    : `upen_t1t3mode`

* **Address**             : `0x00_0017`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`cfg_t1t3mode_enable`| 1: enable this mode| `RW`| `0x0`| `0x0`|
|`[0]`|`cfg_t1t3mode`| 0: send timestamp to cpu queue 1: send back ptp packet mode| `RW`| `0x0`| `0x0 End: Begin:`|

###Config cpu flush

* **Description**           

Used to config cpu flush


* **RTL Instant Name**    : `upen_cpu_flush`

* **Address**             : `0x00_0018`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`cfg_cpu_flush`| cpu flush| `RW`| `0x0`| `0x0 End: Begin:`|

###Read CPU Queue

* **Description**           

Used to read CPU queue


* **RTL Instant Name**    : `upen_cpuque`

* **Address**             : `0x00_0216`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:102]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[101:100]`|`typ`| typ of ptp packet| `RO`| `0x0`| `0x0`|
|`[99:84]`|`seqid`| sequence of packet from sequenceId field| `RO`| `0x0`| `0x0`|
|`[83:80]`|`egr_pid`| egress port id| `RO`| `0x0`| `0x0`|
|`[79:00]`|`egr_tmr_tod`| egress timer tod| `RO`| `0x0`| `0x0 End: Begin:`|

###Enb CPU Interupt

* **Description**           

Used to read CPU queue


* **RTL Instant Name**    : `upen_int_en`

* **Address**             : `0x00_0200`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`int_en`| set "1" ,interupt enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Interupt CPU Queue

* **Description**           

Used to read CPU queue


* **RTL Instant Name**    : `upen_int`

* **Address**             : `0x00_0201`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`int_en`| set"1" queue info ready ,write 1 to clear| `RW`| `0x0`| `0x0 End: Begin:`|

###Interupt CPU Queue

* **Description**           

Used to read CPU queue


* **RTL Instant Name**    : `upen_sta`

* **Address**             : `0x00_0202`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[16:00]`|`upen_sta`| current state of CPU queue, bit[17] "1" queue FULL, bit[16] "1" NOT empty, ready for cpu read, bit[15:0] queue lengh| `RW`| `0x0`| `0x0 End: Begin:`|

###config unicast MAC address global

* **Description**           

config unicast MAC address global for all port


* **RTL Instant Name**    : `upen_umac_glb`

* **Address**             : `0x00_4003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`cfg_umac_glb`| uni MAC global| `RW`| `0x0`| `0x0 End: Begin:`|

###config multicast MAC address global 1

* **Description**           

config multicast MAC address global 1 for all port


* **RTL Instant Name**    : `upen_mmac1_glb`

* **Address**             : `0x00_4004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`cfg_mmac1_glb`| mul MAC global 1| `RW`| `0x0`| `0x0 End: Begin:`|

###config multicast MAC address global 2

* **Description**           

config multicast MAC address global 2 for all port


* **RTL Instant Name**    : `upen_mmac2_glb`

* **Address**             : `0x00_4005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`cfg_mmac2_glb`| mul MAC global 2| `RW`| `0x0`| `0x0 End: Begin:`|

###config any MAC enable

* **Description**           

is used to enable any mac for each port


* **RTL Instant Name**    : `upen_anymac_en`

* **Address**             : `0x00_4006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_anymac_en`| '1': enable , '0': disable| `RW`| `0x0`| `0x0 End: Begin:`|

###config MAC global matching enable

* **Description**           

is used to indicate MAC global matching enable


* **RTL Instant Name**    : `upen_macglb_en`

* **Address**             : `0x00_4007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[02:00]`|`cfg_anymac_en`| [0] : en/dis unicast mac global matching [1] : en/dis multicast mac global 1 matching [2] : en/dis multicast mac global 2 matching| `RW`| `0x0`| `0x0 End: Begin:`|

###Config mul ipv4 global 1

* **Description**           

config multicast ipv4 address global 1


* **RTL Instant Name**    : `upen_v4mip1_glb`

* **Address**             : `0x00_400A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_v4mip1_glb`| used to check multicast ip| `RW`| `0x0`| `0x0 End: Begin:`|

###Config mul ipv4 global 2

* **Description**           

config multicast ipv4 address global 2


* **RTL Instant Name**    : `upen_v4mip2_glb`

* **Address**             : `0x00_400B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_v4mip2_glb`| used to check multicast ip| `RW`| `0x0`| `0x0 End: Begin:`|

###Config mul ipv6 global 1

* **Description**           

config multicast ipv6 address global 1


* **RTL Instant Name**    : `upen_v6mip1_glb`

* **Address**             : `0x00_400C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_v6mip1_glb`| used to check multicast ip| `RW`| `0x0`| `0x0 End: Begin:`|

###Config mul ipv6 global 2

* **Description**           

config multicast ipv6 address global 2


* **RTL Instant Name**    : `upen_v6mip2_glb`

* **Address**             : `0x00_400D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_v6mip2_glb`| used to check multicast ip| `RW`| `0x0`| `0x0 End: Begin:`|

###config any IP enable

* **Description**           

is used to enable any ip for each port


* **RTL Instant Name**    : `upen_anyip_en`

* **Address**             : `0x00_400E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_anyip_en`| '1': enable , '0': disable| `RW`| `0x0`| `0x0 End: Begin:`|

###CLA Ready

* **Description**           

This register is used to indicate config ptp done


* **RTL Instant Name**    : `upen_cla_rdy`

* **Address**             : `0x00_4011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`cfg_cla_rdy`| '1' : ready| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP layer force value

* **Description**           

This register is used to config ptp layer force value


* **RTL Instant Name**    : `upen_force_ptplayer`

* **Address**             : `0x00_4016`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[03:01]`|`ptp_layer`| 3'b000: PTP L2 3'b001: IPv4 3'b010: IPv6 3'b011: IPv4 MPLS unicast 3'b100: IPv6 MPLS unicast| `RW`| `0x0`| `0x0`|
|`[00:00]`|`ptp_ind`| ptp indicate: 0: disable 1: enable| `RW`| `0x0`| `0x0 End: Begin:`|

###MRU Packet Size

* **Description**           

This register is used to config MRU Value


* **RTL Instant Name**    : `upen_mru_val`

* **Address**             : `0x00_4017`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14:14]`|`cfg_mru_en`| MRU enable 1: enable 0: disable| `RW`| `0x0`| `0x0`|
|`[13:00]`|`cfg_mru_val`| MRU value(Byte)| `RW`| `0x0`| `0x0 End: Begin:`|

###cfg uni/mul mac

* **Description**           

config uni/mul MAC for each port

transtype = 0 : unicast

transtype = 1 : multicast

HDL_PATH     : inscorepi.insmac.membuf.ram.ram[$transtype*16 + $pid]


* **RTL Instant Name**    : `upen_mac`

* **Address**             : `0x00_5100-0x00_511F`

* **Formula**             : `0x00_5100 + $transtype*16 + $pid`

* **Where**               : 

    * `$transtype(0-1)`

    * `$pid(0-15)`

* **Width**               : `64`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:49]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[48:48]`|`macena`| mac matching enable 0: disable 1: enable| `RW`| `0x0`| `0x0`|
|`[47:00]`|`cfg_mac`| unicast/multicast mac for each port| `RW`| `0x0`| `0x0 End: Begin:`|

###Config GE unicast/multicast IPv4/IPv6 address 1

* **Description**           

config GE uni/mul IP 1 for each port

transtype = 0 : unicast

transtype = 1 : multicast

HDL_PATH     : inscorepi.insgeip1.membuf.ram.ram[$transtype*16 + $pid]


* **RTL Instant Name**    : `upen_geip1`

* **Address**             : `0x00_5120-0x00_513F`

* **Formula**             : `0x00_5120 + $transtype*16 + $pid`

* **Where**               : 

    * `$transtype(0-1)`

    * `$pid(0-15)`

* **Width**               : `128`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_geip1`| uni/mul ip 1 for each port ipv4:<br>{96'b0, ipv4[31:0]} ipv6: ipv6[127:0]| `RW`| `0x0`| `0x0 End: Begin:`|

###Config GE unicast/multicast IPv4/IPv6 address 2

* **Description**           

config GE uni/mul IP 2 for each port

transtype = 0 : unicast

transtype = 1 : multicast

HDL_PATH     : inscorepi.insgeip2.membuf.ram.ram[$transtype*16 + $pid]


* **RTL Instant Name**    : `upen_geip2`

* **Address**             : `0x00_5140-0x00_515F`

* **Formula**             : `0x00_5140 + $transtype*16 + $pid`

* **Where**               : 

    * `$transtype(0-1)`

    * `$pid(0-15)`

* **Width**               : `128`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_geip2`| uni/mul ip 2 for each port ipv4:<br>{96'b0, ipv4[31:0]} ipv6: ipv6[127:0]| `RW`| `0x0`| `0x0 End: Begin:`|

###Config GE unicast/multicast IPv4/IPv6 address 3

* **Description**           

config GE uni/mul IP 3 for each port

transtype = 0 : unicast

transtype = 1 : multicast

HDL_PATH     : inscorepi.insgeip3.membuf.ram.ram[$transtype*16 + $pid]


* **RTL Instant Name**    : `upen_geip3`

* **Address**             : `0x00_5160-0x00_517F`

* **Formula**             : `0x00_5160 + $transtype*16 + $pid`

* **Where**               : 

    * `$transtype(0-1)`

    * `$pid(0-15)`

* **Width**               : `128`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_geip3`| uni/mul ip 3 for each port ipv4:<br>{96'b0, ipv4[31:0]} ipv6: ipv6[127:0]| `RW`| `0x0`| `0x0 End: Begin:`|

###Config GE unicast/multicast IPv4/IPv6 address 4

* **Description**           

config GE uni/mul IP 4 for each port

transtype = 0 : unicast

transtype = 1 : multicast

HDL_PATH     : inscorepi.insgeip4.membuf.ram.ram[$transtype*16 + $pid]


* **RTL Instant Name**    : `upen_geip4`

* **Address**             : `0x00_5180-0x00_519F`

* **Formula**             : `0x00_5180 + $transtype*16 + $pid`

* **Where**               : 

    * `$transtype(0-1)`

    * `$pid(0-15)`

* **Width**               : `128`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_geip4`| uni/mul ip 4 for each port ipv4:<br>{96'b0, ipv4[31:0]} ipv6: ipv6[127:0]| `RW`| `0x0`| `0x0 End: Begin:`|

###cfg ptp protocol

* **Description**           

config protocol for each port at BC mode in PTP service


* **RTL Instant Name**    : `upen_ptl`

* **Address**             : `0x00_51E0-0x00_51EF`

* **Formula**             : `0x00_51E0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32 HDL_PATH     : inscorepi.insptl.membuf.ram.ram[$pid]`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[02:00]`|`cfg_ptl`| protocol value: '000': L2 '001': ipv4 '010': ipv6 '011': ipv4 vpn '100': ipv6 vpn| `RW`| `0x0`| `0x0 End: Begin:`|

###Config GE port ipv4/ipv6 matching enable

* **Description**           

config IPv4/IPv6 matching for each GE port

HDL_PATH     : inscorepi.insptl.membuf.ram.ram[$pid]


* **RTL Instant Name**    : `upen_geipen`

* **Address**             : `0x00_51F0-0x00_51FF`

* **Formula**             : `0x00_51F0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:07]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[06:00]`|`cfg_geipen`| config ip matching for each GE port [6]: ena/dis mul ip global 2 matching [5]: ena/dis mul ip global 1 matching [4]: ena/dis mul ip 1 matching [3]: ena/dis uni ip 4 matching [2]: ena/dis uni ip 3 matching [1]: ena/dis uni ip 2 matching [0]: ena/dis uni ip 1 matching| `RW`| `0x0`| `0x0 End: Begin:`|

###eth2cla sticky

* **Description**           

sticky of eth2cla interface


* **RTL Instant Name**    : `upen_eth2cla_stk`

* **Address**             : `0x00_4100`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `64`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:48]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[47:40]`|`ethn_ptch`| ETH #n ptch sticky| `W1C`| `0x0`| `0x0`|
|`[39:37]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[36:36]`|`eth0_ptch`| ETH_0 PTCH : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[35:04]`|`ethn_stk`| ETH_n valid, sop, eop, err sticky| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`eth0_err`| ETH_0 ERR  : 0: clear  1: set| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`eth0_eop`| ETH_0 EOP  : 0: clear  1: set| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`eth0_sop`| ETH_0 SOP  : 0: clear  1: set| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`eth0_vld`| ETH_0 VALID: 0: clear  1: set| `W1C`| `0x0`| `0x0 End: Begin:`|

###cla2epa sticky

* **Description**           

sticky of cla2epa interface


* **RTL Instant Name**    : `upen_cla2epa_stk`

* **Address**             : `0x00_4101`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `96`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[95:72]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[71:00]`|`cla2epa_stk`| sticky cla to epa| `W1C`| `0x0`| `0x0 End: Begin:`|

###cla2ptp sticky

* **Description**           

sticky of cla2ptp interface


* **RTL Instant Name**    : `upen_cla2ptp_stk`

* **Address**             : `0x00_4102`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `96`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[95:72]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[71:00]`|`cla2ptp_stk`| sticky cla to ptp| `W1C`| `0x0`| `0x0 End: Begin:`|

###parser sticky 0

* **Description**           

sticky 0  of parser


* **RTL Instant Name**    : `upen_parser_stk0`

* **Address**             : `0x00_4104`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `96`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[95:72]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[71:06]`|`parser_stk0`| sticky parser| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`par0_free`| PARSER_0 FREE BLK : 1: clear , 0: set| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`par0_eoseg`| PARSER_0 EOSEG    : 1: clear , 0: set| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`par0_blkemp`| PARSER_0 BLK EMPTY: 1: clear , 0: set| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`par0_errmru`| PARSER_0 MRU ERROR: 1: clear , 0: set| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`par0_err`| PARSER_0 PKT SHORT: 1: clear , 0: set| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`par0_done`| PARSER_0 DONE:      1: clear , 0: set| `W1C`| `0x0`| `0x0 End: Begin:`|

###parser sticky 1

* **Description**           

sticky 1  of parser


* **RTL Instant Name**    : `upen_parser_stk1`

* **Address**             : `0x00_4105`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:108]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[107:12]`|`parser_stk1`| sticky parser| `W1C`| `0x0`| `0x0`|
|`[11:11]`|`par0_udpprt`| PARSER_0 UDP PRT  : 0: clear, 1: set| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`par0_ipda`| PARSER_0 IP DA    : 0: clear, 1: set| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`par0_ipsa`| PARSER_0 IP SA    : 0: clear, 1: set| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`par0_udpptl`| PARSER_0 UDP PTL  : 0: clear, 1: set| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`par0_ipver`| PARSER_0 IPVER  : 0: clear, 1: set| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`par0_mac`| PARSER_0 MAC    : 0: clear, 1: set| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`par0_mefecid`| PARSER_0 MEFECID: 0: clear, 1: set| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`par0_pwlabel`| PARSER_0 PWLABEL: 0: clear, 1: set| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`par0_ethtyp`| PARSER_0 ETHTYPE: 0: clear, 1: set| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`par0_sndvlan`| PARSER_0 SNDVLAN: 0: clear, 1: set| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`par0_fstvlan`| PARSER_0 FSTVLAN: 0: clear, 1: set| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`par0_ptch`| PARSER_0 PTCH   : 0: clear, 1: set| `W1C`| `0x0`| `0x0 End: Begin:`|

###lookup sticky

* **Description**           

sticky of lookup


* **RTL Instant Name**    : `upen_lkup_stk`

* **Address**             : `0x00_4106`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `96`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[95:88]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[87:87]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[86:86]`|`porttab_outen`| PORT    TAB OUT_EN: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[85:85]`|`ptchtab_outen`| PTCH    TAB OUT_EN: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[84:84]`|`fvlntab_outen`| FSTVLAN TAB OUT_EN: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[83:83]`|`svlntab_outen`| SNDVLAN TAB OUT_EN: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[82:82]`|`pwhash_outen`| PWHASH  TAB OUT_EN: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[81:81]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[80:80]`|`rulechk_outen`| RULECHK TAB OUT_EN: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[79:79]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[78:78]`|`porttab_win`| PORT    TABLE WIN: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[77:77]`|`ptchtab_win`| PTCH    TABLE WIN: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[76:76]`|`fvlntab_win`| FSTVLAN TABLE WIN: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[75:75]`|`svlntab_win`| SNDVLAN TABLE WIN: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[74:74]`|`pwhash_win`| PWHASH  TABLE WIN: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[73:73]`|`etypcam_win`| ETHTYPE TABLE WIN: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[72:72]`|`rulechk_win`| RULECHK TABLE WIN: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[71:08]`|`lkup0_stk1`| LOOKUP_0 sticky 1| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`lkup0_ptpind`| LOOKUP_0 PTP IND   : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`lkup0_udpind`| LOOKUP_0 UDP IND   : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`lkup0_eoseg`| LOOKUP_0 EOSEG     : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`lkup0_blkemp`| LOOKUP_0 BLK EMPTY : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`lkup0_errmru`| LOOKUP_0 MRU ERR: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`lkup0_fail`| LOOKUP_0 FAIL   : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`lkup0_stk0`| LOOKUP_0 sticky 0| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`lkup0_done`| LOOKUP_0 DONE   : 0: clear 1: set| `W1C`| `0x0`| `0x0 End: Begin:`|

###datashift sticky

* **Description**           

sticky of datashift


* **RTL Instant Name**    : `upen_datashidt_stk`

* **Address**             : `0x00_4107`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `64`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:44]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[43:43]`|`datsh0_ssercpu`| DATSHF_0 SSER CPU : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[42:42]`|`datsh0_sseroam`| DATSHF_0 SSER OAM : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[41:41]`|`datsh0_sserdata`| DATSHF_0 SSER DATA: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[40:40]`|`datsh0_sserpw`| DATSHF_0 SSER PW  : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[39:38]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[37:37]`|`datsh0_serptp`| DATSHF_0 SER PTP   : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[36:36]`|`datsh0_serepass`| DATSHF_0 SER EPASS : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[35:35]`|`datsh0_serdcc`| DATSHF_0 SER DCC   : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[34:34]`|`datsh0_serimsg`| DATSHF_0 SER IMSG  : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[33:33]`|`datsh0_sercem`| DATSHF_0 SER CEM   : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[32:32]`|`datsh0_serdrop`| DATSHF_0 SER DROP  : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[31:30]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[29:29]`|`datsh0_mef`| DATSHF_0 MEF      : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[28:28]`|`datsh0_oam`| DATSHF_0 OAM      : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[27:27]`|`datsh0_mmpls`| DATSHF_0 MPLS MUL : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[26:26]`|`datsh0_umpls`| DATSHF_0 MPLS UNI : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[25:25]`|`datsh0_ipv6`| DATSHF_0 IPv6     : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[24:24]`|`datsh0_ipv4`| DATSHF_0 IPv4     : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[23:21]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[20:20]`|`datsh0_4label`| DATSHF_0 4LABEL : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[19:19]`|`datsh0_3label`| DATSHF_0 3LABEL : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[18:18]`|`datsh0_2label`| DATSHF_0 2LABEL : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[17:17]`|`datsh0_1label`| DATSHF_0 1LABEL : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[16:16]`|`datsh0_0label`| DATSHF_0 0LABEL : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[15:15]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[14:14]`|`datsh0_2vlan`| DATSHF_0 2VLAN : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[13:13]`|`datsh0_1vlan`| DATSHF_0 1VLAN : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[12:12]`|`datsh0_0vlan`| DATSHF_0 0VLAN : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[11:11]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`datsh0_ptpind`| DATSHF_0 PTP  IND : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`datsh0_udpind`| DATSHF_0 UDP  IND : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`datsh0_lkwin`| DATSHF_0 LKUP WIN : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`datsh0_err4`| DATSHF_0 MRU ERROR: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`datsh0_err3`| DATSHF_0 BLK EMPTY: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`datsh0_err2`| DATSHF_0 LKUP FAIL: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`datsh0_err0`| DATSHF_0 ETH ERROR: 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`datsh0_eop`| DATSHF_0 EOP      : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`datsh0_sop`| DATSHF_0 SOP      : 0: clear 1: set| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`datsh0_vld`| DATSHF_0 VALID    : 0: clear 1: set| `W1C`| `0x0`| `0x0 End: Begin:`|

###allc sticky

* **Description**           

sticky of egress header


* **RTL Instant Name**    : `upen_allc_stk`

* **Address**             : `0x00_410A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `64`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:20]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[19:19]`|`ffge11_full`| FIFO GE11 FULL: 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[18:18]`|`ffge10_full`| FIFO GE10 FULL: 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[17:17]`|`ffge9_full`| FIFO GE9  FULL: 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[16:16]`|`ffge8_full`| FIFO GE8  FULL: 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[15:15]`|`ffge7_full`| FIFO GE7  FULL: 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[14:14]`|`ffge6_full`| FIFO GE6  FULL: 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[13:13]`|`ffge5_full`| FIFO GE5  FULL: 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[12:12]`|`ffge4_full`| FIFO GE4  FULL: 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[11:09]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`ffxfi_full`| FIFO XFI FULL: 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[07:05]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`blkreq`| BLK REQUEST: 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`buffre`| BUFFER FREE: 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`parfre`| PARSER FREE: 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`blkemp`| BLK EMPTY  : 0: clear 1:set| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`samefree`| SAME FREE  : 0: clear 1:set| `W1C`| `0x0`| `0x0 End: Begin:`|

###allc_blkfre_cnt

* **Description**           

allc block free counter


* **RTL Instant Name**    : `upen_allc_blkfre_cnt`

* **Address**             : `0x00_4200`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`allc_blkfre_cnt`| block free counter| `RO`| `0x0`| `0x0 End: Begin:`|

###allc_blkiss_cnt

* **Description**           

allc block issue counter


* **RTL Instant Name**    : `upen_allc_blkiss_cnt`

* **Address**             : `0x00_4201`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`allc_blkiss_cnt`| block issue counter| `RO`| `0x0`| `0x0 End: Begin:`|

###todinit_cfg

* **Description**           

init value tod


* **RTL Instant Name**    : `upen_todinit_cfg`

* **Address**             : `0x2000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:48]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[47:00]`|`upen_todsecinit`| cfg init sec| `RW`| `0x0`| `0x0 End: Begin:`|

###tod_current

* **Description**           

current timer


* **RTL Instant Name**    : `upen_tod_cur`

* **Address**             : `0x2001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:48]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[47:00]`|`cur_todsec`| current second| `RO`| `0x0`| `0x0 End: Begin:`|

###todmode_cfg

* **Description**           

operaation ToD


* **RTL Instant Name**    : `upen_tod_mode`

* **Address**             : `0x2002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10:03]`|`compensate_route_1pps`| The ability to offset their internal time counter with respect the 1PPS signal they receive. This is to compensate for the propagation delay of the 1PPS signal in our system.| `RW`| `0x0`| `0x0`|
|`[02:00]`|`upen_todmode`| select output tod "0" free-run, "1" external pps| `RW`| `0x0`| `0x0 End: Begin:`|

###bp_latency_cfg

* **Description**           

This register configure latency adjust for tx/rx backplan serdes


* **RTL Instant Name**    : `upen_bp_latency_mode`

* **Address**             : `0x2020 - 0x2021`

* **Formula**             : `0x2020 + $BackplanID`

* **Where**               : 

    * `$BackplanID: 0-1`

* **Width**               : `22`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[21:11]`|`tx_latency_adj_bp`| latency adjust from tx backplan MAC to serdes pin in nanosecond| `RW`| `0x297`| `0x297`|
|`[10:00]`|`rx_latency_adj_bp`| latency adjust from rx backplan serdes pin to MAC in nanosecond| `RW`| `0xF2`| `0xF2 End: Begin:`|

###face_latency_cfg

* **Description**           

This register configure latency adjust for tx/rx face plate serdes


* **RTL Instant Name**    : `upen_face_latency_mode`

* **Address**             : `0x2040 - 0x204F`

* **Formula**             : `0x2040 + $FacePlateID`

* **Where**               : 

    * `$FacePlateID: 0-15`

* **Width**               : `22`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[21:11]`|`tx_latency_adj_face`| latency adjust from tx face plate MAC to serdes pin in nanosecond calibrated value of TENGE is 0x82, calibrated value of GE is 0x198, calibrated value of FX is 0x490| `RW`| `0x490`| `0x490`|
|`[10:00]`|`rx_latency_adj_face`| latency adjust from rx face plate serdes pin to MAC in nanosecond calibrated value of TENGE is 0x82, calibrated value of GE is 0x138, calibrated value of FX is 0x581| `RW`| `0x581`| `0x581 End: Begin:`|

###Hold Register 40G 1

* **Description**           

This register hold value 1


* **RTL Instant Name**    : `upen_hold_40g1`

* **Address**             : `0x00_B020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cfg_hold_40g1`| hold 40g value 1| `RW`| `0x0`| `0x0 End: Begin:`|

###Hold Register 40G 2

* **Description**           

This register hold value 2


* **RTL Instant Name**    : `upen_hold_40g2`

* **Address**             : `0x00_B021`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cfg_hold_40g2`| hold 40g value 2| `RW`| `0x0`| `0x0 End: Begin:`|

###Hold Register 40G 3

* **Description**           

This register hold value 3


* **RTL Instant Name**    : `upen_hold_40g3`

* **Address**             : `0x00_B022`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cfg_hold_40g3`| hold 40g value 3| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP enable

* **Description**           

used to indicate 1-step/2-step mode for each port


* **RTL Instant Name**    : `upen_ptp40_stmode`

* **Address**             : `0x00_8004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_ptp_stmode`| '1' : 2-step, '0': 1-step| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP enable

* **Description**           

used to indicate master/slaver mode for each port


* **RTL Instant Name**    : `upen_ptp40_ms`

* **Address**             : `0x00_8005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_ptp_ms`| '1' : master, '0': slaver| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP Device

* **Description**           

This register is used to config PTP device


* **RTL Instant Name**    : `upen_ptp40_dev`

* **Address**             : `0x00_8006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[01:00]`|`device_mode`| '00': BC mode, '01': reserved '10': TC Separate mode, '11': TC General mode| `RW`| `0x2`| `0x2 End: Begin:`|

###

* **Description**           

enable for any MAC/IPv4/IPv6


* **RTL Instant Name**    : `upen_cfg_ptp_bypass`

* **Address**             : `0x00_B000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[05:00]`|`cfg_ptp_bypass`| bit[0] l2_sa bit[1] l2_da bit[2] ip4_sa bit[3] ip4_da bit[4] ip6_sa bit[5] ip6_da| `RW`| `0x0`| `0x0  End: Begin:`|

###

* **Description**           

cfg ptp mac sa


* **RTL Instant Name**    : `upen_cfg_mac_sa`

* **Address**             : `0x00_B001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:48]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[47:00]`|`cfg_mac_sa`| MAC SA| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp mac da


* **RTL Instant Name**    : `upen_cfg_mac_da`

* **Address**             : `0x00_B002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:48]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[47:00]`|`cfg_mac_da`| MAC DA| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv4 sa1


* **RTL Instant Name**    : `upen_cfg_ip4_sa1`

* **Address**             : `0x00_B003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ipv4_sa1`| IPv4 SA1| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv4 sa2


* **RTL Instant Name**    : `upen_cfg_ip4_sa2`

* **Address**             : `0x00_B004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ipv4_sa2`| IPv4 SA2| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv4 sa3


* **RTL Instant Name**    : `upen_cfg_ip4_sa3`

* **Address**             : `0x00_B005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ipv4_sa3`| IPv4 SA3| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv4 sa4


* **RTL Instant Name**    : `upen_cfg_ip4_sa4`

* **Address**             : `0x00_B006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ipv4_sa4`| IPv4 SA4| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ip4 da1


* **RTL Instant Name**    : `upen_cfg_ip4_da1`

* **Address**             : `0x00_B007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ip4_da1`| MAC DA1| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ip4 da2


* **RTL Instant Name**    : `upen_cfg_ip4_da2`

* **Address**             : `0x00_B008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ip4_da2`| MAC DA2| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ip4 da3


* **RTL Instant Name**    : `upen_cfg_ip4_da3`

* **Address**             : `0x00_B009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ip4_da3`| MAC DA3| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ip4 da4


* **RTL Instant Name**    : `upen_cfg_ip4_da4`

* **Address**             : `0x00_B00A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ip4_da4`| MAC DA4| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 sa1


* **RTL Instant Name**    : `upen_cfg_ip6_sa1`

* **Address**             : `0x00_B00B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_sa1`| IPv6 SA1| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 sa2


* **RTL Instant Name**    : `upen_cfg_ip6_sa2`

* **Address**             : `0x00_B00C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_sa2`| IPv6 SA2| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 sa3


* **RTL Instant Name**    : `upen_cfg_ip6_sa3`

* **Address**             : `0x00_B00D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_sa3`| IPv6 SA3| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 sa4


* **RTL Instant Name**    : `upen_cfg_ip6_sa4`

* **Address**             : `0x00_B00E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv4_sa4`| IPv4 SA4| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 da1


* **RTL Instant Name**    : `upen_cfg_ip6_da1`

* **Address**             : `0x00_B00F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_da1`| IPv6 DA1| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 da2


* **RTL Instant Name**    : `upen_cfg_ip6_da2`

* **Address**             : `0x00_B010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_da2`| IPv6 DA2| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 da3


* **RTL Instant Name**    : `upen_cfg_ip6_da3`

* **Address**             : `0x00_B011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_da3`| IPv6 DA3| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 da4


* **RTL Instant Name**    : `upen_cfg_ip6_da4`

* **Address**             : `0x00_B012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_da4`| IPv6 DA4| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

counter iport


* **RTL Instant Name**    : `upen_cnt_pkt_in_r2c`

* **Address**             : `0x00_8460(R2C)`

* **Formula**             : `0x00_8460 +  $ipid`

* **Where**               : 

    * `$ipid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_input_port`| Total RX counter input| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

counter iport


* **RTL Instant Name**    : `upen_cnt_pkt_in_ro`

* **Address**             : `0x008470(RO)`

* **Formula**             : `0x008470 +  $ipid`

* **Where**               : 

    * `$ipid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_input_port`| Total RX counter input| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

Total Rx counter ptp 


* **RTL Instant Name**    : `upen_cnt_ptppkt_rx_r2c`

* **Address**             : `0x00_8420(R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxcnt_ptp`| Total counter RX 40G port| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

Total Rx counter ptp 


* **RTL Instant Name**    : `upen_cnt_ptppkt_rx_ro`

* **Address**             : `0x008430(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxcnt_ptp`| Total counter RX 40G port| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

Total Tx counter


* **RTL Instant Name**    : `upen_cnt_txpkt_out_r2c`

* **Address**             : `0x00_8400(R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`tx40cnt_ptp`| tx counter 40G output| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

Total Tx counter


* **RTL Instant Name**    : `upen_cnt_txpkt_out_ro`

* **Address**             : `0x008410(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`tx40cnt_ptp`| tx counter 40G output| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter Sync Received 40G PORT

* **Description**           

Number of Sync message received


* **RTL Instant Name**    : `upen_rx_sync_cnt40_r2c`

* **Address**             : `0x00_8480(R2C)`

* **Formula**             : `0x00_8480+ $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`syncrx_cnt40`| number sync packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Sync Received 40G PORT

* **Description**           

Number of Sync message received


* **RTL Instant Name**    : `upen_rx_sync_cnt40_ro`

* **Address**             : `0x00_84C0(RO)`

* **Formula**             : `0x00_84C0+ $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`syncrx_cnt40`| number sync packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Folu Received 40G PORT

* **Description**           

Number of folu message received


* **RTL Instant Name**    : `upen_rx_folu_cnt40_r2c`

* **Address**             : `0x00_8490(R2C)`

* **Formula**             : `0x00_8490 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`folurx_cnt40`| number folu packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Folu Received 40G PORT

* **Description**           

Number of folu message received


* **RTL Instant Name**    : `upen_rx_folu_cnt40_ro`

* **Address**             : `0x00_84D0(RO)`

* **Formula**             : `0x00_84D0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`folurx_cnt40`| number folu packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dreq Received 40G PORT

* **Description**           

Number of dreq message received


* **RTL Instant Name**    : `upen_rx_dreq_cnt40_r2c`

* **Address**             : `0x00_84A0(R2C)`

* **Formula**             : `0x00_84A0+$pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dreqrx_cnt40`| number dreq packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dreq Received 40G PORT

* **Description**           

Number of dreq message received


* **RTL Instant Name**    : `upen_rx_dreq_cnt40_ro`

* **Address**             : `0x00_84E0(RO)`

* **Formula**             : `0x00_84E0+$pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dreqrx_cnt40`| number dreq packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dres Received 40G PORT

* **Description**           

Number of dres message received


* **RTL Instant Name**    : `upen_rx_dres_cnt40_r2c`

* **Address**             : `0x00_84B0(R2C)`

* **Formula**             : `0x00_84B0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dresrx_cnt40`| number dres packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dres Received 40G PORT

* **Description**           

Number of dres message received


* **RTL Instant Name**    : `upen_rx_dres_cnt40_ro`

* **Address**             : `0x00_84F0(RO)`

* **Formula**             : `0x00_84F0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dresrx_cnt40`| number dres packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Sync Extract Err 40G PORT

* **Description**           

Number of extr err sync mess


* **RTL Instant Name**    : `upen_rx_extr_err_syncnt40_r2c`

* **Address**             : `0x00_8500(R2C)`

* **Formula**             : `0x00_8500 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_synccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Sync Extract Err 40G PORT

* **Description**           

Number of extr err sync mess


* **RTL Instant Name**    : `upen_rx_extr_err_syncnt40_ro`

* **Address**             : `0x00_8540(RO)`

* **Formula**             : `0x00_8540 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_synccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Folu Extract Err 40G PORT

* **Description**           

Number of extr err folu mess


* **RTL Instant Name**    : `upen_rx_extr_err_folucnt40_r2c`

* **Address**             : `0x00_8510(R2C)`

* **Formula**             : `0x00_8510 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_foluccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Folu Extract Err 40G PORT

* **Description**           

Number of extr err folu mess


* **RTL Instant Name**    : `upen_rx_extr_err_folucnt40_ro`

* **Address**             : `0x00_8550(RO)`

* **Formula**             : `0x00_8550 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_foluccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dreq Extract Err 40G PORT

* **Description**           

Number of extr err dreq mess


* **RTL Instant Name**    : `upen_rx_extr_err_dreqcnt40_r2c`

* **Address**             : `0x00_8520(R2C)`

* **Formula**             : `0x00_8520 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_dreqcnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dreq Extract Err 40G PORT

* **Description**           

Number of extr err dreq mess


* **RTL Instant Name**    : `upen_rx_extr_err_dreqcnt40_ro`

* **Address**             : `0x00_8560(RO)`

* **Formula**             : `0x00_8560 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_dreqcnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dres Extract Err 40G PORT

* **Description**           

Number of extr err dres mess


* **RTL Instant Name**    : `upen_rx_extr_err_drescnt40_r2c`

* **Address**             : `0x00_8530(R2C)`

* **Formula**             : `0x00_8530 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_drescnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dres Extract Err 40G PORT

* **Description**           

Number of extr err dres mess


* **RTL Instant Name**    : `upen_rx_extr_err_drescnt40_ro`

* **Address**             : `0x00_8570(RO)`

* **Formula**             : `0x00_8570 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_drescnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Sync Chsum Err 40G PORT

* **Description**           

Number of Chs err sync mess


* **RTL Instant Name**    : `upen_rx_chs_err_syncnt40_r2c`

* **Address**             : `0x00_8580(R2C)`

* **Formula**             : `0x00_8580 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_synccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Sync Chsum Err 40G PORT

* **Description**           

Number of Chs err sync mess


* **RTL Instant Name**    : `upen_rx_chs_err_syncnt40_ro`

* **Address**             : `0x00_85C0(RO)`

* **Formula**             : `0x00_85C0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_synccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Folu Chsum Err 40G PORT

* **Description**           

Number of Chs err folu mess


* **RTL Instant Name**    : `upen_rx_chs_err_folucnt40_r2c`

* **Address**             : `0x00_8590(R2C)`

* **Formula**             : `0x00_8590 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_folucnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Folu Chsum Err 40G PORT

* **Description**           

Number of Chs err folu mess


* **RTL Instant Name**    : `upen_rx_chs_err_folucnt40_ro`

* **Address**             : `0x00_85D0(RO)`

* **Formula**             : `0x00_85D0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_folucnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dreq Chsum Err 40G PORT

* **Description**           

Number of Chs err dreq mess


* **RTL Instant Name**    : `upen_rx_chs_err_dreqcnt40_r2c`

* **Address**             : `0x00_85A0(R2C)`

* **Formula**             : `0x00_85A0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_dreqcnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dreq Chsum Err 40G PORT

* **Description**           

Number of Chs err dreq mess


* **RTL Instant Name**    : `upen_rx_chs_err_dreqcnt40_ro`

* **Address**             : `0x00_85E0(RO)`

* **Formula**             : `0x00_85E0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_dreqcnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dres Chsum Err 40G PORT

* **Description**           

Number of Chs err dres mess


* **RTL Instant Name**    : `upen_rx_chs_err_drescnt40_r2c`

* **Address**             : `0x00_85B0(R2C)`

* **Formula**             : `0x00_85B0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_drescnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dres Chsum Err 40G PORT

* **Description**           

Number of Chs err dres mess


* **RTL Instant Name**    : `upen_rx_chs_err_drescnt40_ro`

* **Address**             : `0x00_85F0(RO)`

* **Formula**             : `0x00_85F0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_drescnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Sync Transmit 40G PORT

* **Description**           

Number of Sync message transmit


* **RTL Instant Name**    : `upen_tx_sync_cnt40_r2c`

* **Address**             : `0x00_8600(R2C)`

* **Formula**             : `0x00_8600 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`synctx_cnt40`| number sync packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Sync Transmit 40G PORT

* **Description**           

Number of Sync message transmit


* **RTL Instant Name**    : `upen_tx_sync_cnt40_ro`

* **Address**             : `0x00_8640(RO)`

* **Formula**             : `0x00_8640 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`synctx_cnt40`| number sync packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Folu Transmit 40G PORT

* **Description**           

Number of folu message transmit


* **RTL Instant Name**    : `upen_tx_folu_cnt40_r2c`

* **Address**             : `0x00_8610(R2C)`

* **Formula**             : `0x00_8610 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`folutx_cnt40`| number folu packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Folu Transmit 40G PORT

* **Description**           

Number of folu message transmit


* **RTL Instant Name**    : `upen_tx_folu_cnt40_ro`

* **Address**             : `0x00_8650(RO)`

* **Formula**             : `0x00_8650 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`folutx_cnt40`| number folu packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dreq transmit

* **Description**           

Number of dreq message transmit


* **RTL Instant Name**    : `upen_tx_dreq_cnt40_r2c`

* **Address**             : `0x00_8620(R2C)`

* **Formula**             : `0x00_8620 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dreqtx_cnt40`| number dreq packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dreq transmit

* **Description**           

Number of dreq message transmit


* **RTL Instant Name**    : `upen_tx_dreq_cnt40_ro`

* **Address**             : `0x00_8660(RO)`

* **Formula**             : `0x00_8660 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dreqtx_cnt40`| number dreq packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dres transmit

* **Description**           

Number of dres message transmit


* **RTL Instant Name**    : `upen_tx_dres_cnt40_r2c`

* **Address**             : `0x00_8630(R2C)`

* **Formula**             : `0x00_8630 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`drestx_cnt40`| number dres packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dres transmit

* **Description**           

Number of dres message transmit


* **RTL Instant Name**    : `upen_tx_dres_cnt40_ro`

* **Address**             : `0x00_8670(RO)`

* **Formula**             : `0x00_8670 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`drestx_cnt40`| number dres packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Read CPU Queue

* **Description**           

Used to read CPU queue


* **RTL Instant Name**    : `upen_cpuque40`

* **Address**             : `0x00_8216`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:102]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[101:100]`|`typ`| typ of ptp packet| `RO`| `0x0`| `0x0`|
|`[99:84]`|`seqid`| sequence of packet from sequenceId field| `RO`| `0x0`| `0x0`|
|`[83:80]`|`egr_pid`| egress port id| `RO`| `0x0`| `0x0`|
|`[79:00]`|`egr_tmr_tod`| egress timer tod| `RO`| `0x0`| `0x0 End: Begin:`|

###Enb CPU Interupt

* **Description**           

Used to read CPU queue


* **RTL Instant Name**    : `upen_int_en40`

* **Address**             : `0x00_8200`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`int_en`| set "1" ,interupt enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Interupt CPU Queue

* **Description**           

Used to read CPU queue


* **RTL Instant Name**    : `upen_int40`

* **Address**             : `0x00_8201`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`int_en`| set"1" queue info ready ,write 1 to clear| `RW`| `0x0`| `0x0 End: Begin:`|

###Interupt CPU Queue

* **Description**           

Used to read CPU queue


* **RTL Instant Name**    : `upen_sta40`

* **Address**             : `0x00_8202`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[16:00]`|`upen_sta`| current state of CPU queue, bit[17] "1" queue FULL, bit[16] "1" NOT empty, ready for cpu read, bit[15:0] queue lengh| `RW`| `0x0`| `0x0 End: Begin:`|

###Config t1t3mode

* **Description**           

Used to config t1t3mode


* **RTL Instant Name**    : `upen_t1t3mode40g`

* **Address**             : `0x00_8017`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`cfg_t1t3mode40g_enable`| 1: enable this mode| `RW`| `0x0`| `0x0`|
|`[0]`|`cfg_t1t3mode40g`| 0: send timestamp to cpu queue 1: send back ptp packet mode| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP interupt status

* **Description**           

Used to detect interupt event


* **RTL Instant Name**    : `upen_int_sta`

* **Address**             : `0x00_80FE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`interupt_of_40g_bp_port`| 1: active, Read CPU Queue interupt 40G BP| `RW`| `0x0`| `0x0`|
|`[0]`|`interupt_of_face___port`| 1: active, Read CPU Queue interupt FACE| `RW`| `0x0`| `0x0 End: Begin:`|

###config vlan id global

* **Description**           

config vlan id global  for all port FACE


* **RTL Instant Name**    : `upen_face_vid_glb`

* **Address**             : `0x00_4021-0x00_4024`

* **Formula**             : `0x00_4021 + $num`

* **Where**               : 

    * `$num(0-3)`

* **Width**               : `12`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11:00]`|`cfg_face_vidglb`| global vlan id for FACE| `RW`| `0x0`| `0x0 End: Begin:`|

###config vlan id global enable

* **Description**           

config vlan id global enable matching for all port FACE


* **RTL Instant Name**    : `upen_face_vid_glb_en`

* **Address**             : `0x00_4020`

* **Formula**             : `0x00_4020`

* **Where**               : 

* **Width**               : `20`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:01]`|`reserve`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`cfg_face_vidglb_ena`| "1" enable checking global vlanid| `RW`| `0x0`| `0x0 End: Begin:`|

###config vlan id per port

* **Description**           

config vlan id per port FACE


* **RTL Instant Name**    : `upen_face_vid_perport`

* **Address**             : `0x00_51A0-0x00_51AF`

* **Formula**             : `0x00_51A0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `26`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[25]`|`ena_vlanid2`| set "1" enable checking vlan id2| `RW`| `0x0`| `0x0`|
|`[24]`|`ena_vlanid1`| set "1" enable checking vlan id1| `RW`| `0x0`| `0x0`|
|`[23:12]`|`vlanid2`| perport vlan id1 for FACE| `RW`| `0x0`| `0x0`|
|`[11:00]`|`vlanid1`| perport vlan id2 for FACE| `RW`| `0x0`| `0x0 End: Begin:`|

###config vlan40 id global

* **Description**           

config vlan id global  for all port 40G BACKPLAN


* **RTL Instant Name**    : `upen_bp_vid_glb`

* **Address**             : `0x00_B040-0x00_B043`

* **Formula**             : `0x00_B040 + $num`

* **Where**               : 

    * `$num(0-3)`

* **Width**               : `13`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12]`|`cfg_bp_vidglb_enable`| set "1" ( in case $num = 0, $num!= 0 dont care) enable  checking  bp global vlanid| `RW`| `0x0`| `0x0`|
|`[11:00]`|`cfg_bp_vidglb`| global vlan id for 40G port| `RW`| `0x0`| `0x0 End: Begin:`|

###config vlan40 id perport

* **Description**           

config vlan id per port 40G BACKPLAN


* **RTL Instant Name**    : `upen_bp_vid_perport`

* **Address**             : `0x0_A000-0x0_A00F`

* **Formula**             : `0x00_A000 + $opid`

* **Where**               : 

    * `$opid(0-15)`

* **Width**               : `26`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[25]`|`ena_bpvlanid2`| set "1" enable checking vlan id2| `RW`| `0x0`| `0x0`|
|`[24]`|`ena_bpvlanid1`| set "1" enable checking vlan id1| `RW`| `0x0`| `0x0`|
|`[23:12]`|`cfg_bp_vid2`| global vlan id2 for 40G port| `RW`| `0x0`| `0x0`|
|`[11:00]`|`cfg_bp_vid1`| global vlan id1 for 40G port| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP Face Dis Timestaping

* **Description**           

used to disable ptp for TC update CF face 2 BP


* **RTL Instant Name**    : `upen_ge_ptp_dis_timestamping`

* **Address**             : `0x00_0020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`cfg_timstap_dis_face_2_bp`| '1' : disable update CF, '0': enable| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP BP Dis Timestamping

* **Description**           

used to disable ptp for TC update CF face 2 BP


* **RTL Instant Name**    : `upen_bp_ptp_dis_timestamping`

* **Address**             : `0x00_8020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`cfg_timstap_dis_bp_2_face`| '1' : disable update CF, '0': enable| `RW`| `0x0`| `0x0 End:`|

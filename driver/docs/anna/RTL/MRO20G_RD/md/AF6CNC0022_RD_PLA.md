## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_PLA
####Register Table

|Name|Address|
|-----|-----|
|`Payload Assembler Low-Order Payload Control`|`0x0_1000-0x1_B7FF`|
|`Payload Assembler Add/Remove Pseudowire Protocol Control`|`0x0_1800-0x1_BFFF`|
|`Payload Assembler Pseudowire Header Control`|`0x2_0000-0x2_29FF`|
|`Payload Assembler Pseudowire PSN Control`|`0x4_8000-0x4_A9FF`|
|`Payload Assembler Psedowire Protection Control`|`0x5_0000-0x5_29FF`|
|`Payload Assembler Output UPSR Control`|`0x5_8000-0x5_A9FF`|
|`Payload Assembler Output HSPW Control`|`0x6_0000-0x6_029F`|
|`Payload Assembler Output PSN Process Control`|`0x4_0000`|
|`Payload Assembler Output PSN Buffer Control`|`0x4_0010-0x4_0015`|
|`Payload Assembler Hold Register Control`|`0x7_0000-0x7_0002`|
|`Read HA Address Bit3_0 Control`|`0x70100 - 0x7010F`|
|`Read HA Address Bit7_4 Control`|`0x70110 - 0x7011F`|
|`Read HA Address Bit11_8 Control`|`0x70120 - 0x7012F`|
|`Read HA Address Bit15_12 Control`|`0x070130 - 0x7013F`|
|`Read HA Address Bit19_16 Control`|`0x70140 - 0x7014F`|
|`Read HA Address Bit23_20 Control`|`0x70150 - 0x7015F`|
|`Read HA Address Bit24 and Data Control`|`0x70160 - 0x70161`|
|`Read HA Hold Data63_32`|`0x70170`|
|`Read HA Hold Data95_64`|`0x70171`|
|`Read HA Hold Data127_96`|`0x70172`|
|`Payload Assembler Output PSN Process Control`|`0x4_0001`|
|`Payload Assembler Force Parity Error Control`|`0x42040`|
|`Payload Assembler Disable Parity Control`|`0x42041`|
|`Payload Assembler Parity Error Sticky`|`0x42042`|
|`Payload Assembler Force CRC Error Control`|`0x42030`|
|`PLA CRC Error Sticky`|`0x42031`|
|`PLA CRC Error Counter`|`0x42032`|
|`Payload Assembler Low-Order Payload Control`|`0x2_8000-0x2_C000`|
|`Payload Assembler OC192c Add/Remove Pseudowire Protocol Control`|`0x2_8001-0x2_C001`|
|`BERT GEN CONTROL`|`0x4_4000 - 0x4_5000`|
|`good counter tdm  r2c`|`0x4_4001 - 0x4_5001`|
|`good counter tdm  ro`|`0x4_4002 - 0x4_5002`|
|`error counter tdm  r2c`|`0x4_4003 - 0x4_5003`|
|`error counter tdm  ro`|`0x4_4004 - 0x4_5004`|
|`lost counter tdm  r2c`|`0x4_4005 - 0x4_5005`|
|`lost counter tdm  ro`|`0x4_4006 - 0x4_5006`|
|`lost counter tdm  ro`|`0x4_4007 - 0x4_5007`|
|`lost counter tdm  ro`|`0x4_4008 - 0x4_5008`|


###Payload Assembler Low-Order Payload Control

* **Description**           

This register is used to configure payload in each Pseudowire channels per slice

HDL_PATH  : ipwcore.ipwslcore.isl48core[$Oc96Slice*2 + $Oc48Slice].irtlpla.cfgpwctrl.membuf.ram.ram[$Pwid]


* **RTL Instant Name**    : `pla_pld_ctrl`

* **Address**             : `0x0_1000-0x1_B7FF`

* **Formula**             : `0x0_1000 + $Oc96Slice*32768 + $Oc48Slice*8192 + $Pwid`

* **Where**               : 

    * `$Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G`

    * `$Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice`

    * `$Pwid(0-2047): pseudowire channel for each OC-48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`plalkpwidctrl`| Lookup to a output psedowire| `RW`| `0x0`| `0x0`|
|`[17]`|`plalopldtssrcctrl`| Timestamp Source 0: PRC global 1: TS from CDR engine| `RW`| `0x0`| `0x0`|
|`[16:15]`|`plalopldtypectrl`| Payload Type 0: satop 1: ces without cas 2: cep| `RW`| `0x0`| `0x0`|
|`[14:0]`|`plalopldsizectrl`| Payload Size satop/cep mode: bit[13:0] payload size (ex: value as 0x100 is payload size 256 bytes) ces mode: bit[5:0] number of DS0 timeslot bit[14:6] number of NxDS0 frame| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Add/Remove Pseudowire Protocol Control

* **Description**           

This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each OC-48 slice


* **RTL Instant Name**    : `pla_add_rmv_pw_ctrl`

* **Address**             : `0x0_1800-0x1_BFFF`

* **Formula**             : `0x0_1800 + $Oc96Slice*32768 + $Oc48Slice*8192 + $Pwid`

* **Where**               : 

    * `$Oc96Slice(0-3): OC-96 slices, there are total 4xOC-96 slices for 20G`

    * `$Oc48Slice(0-1): OC-48 slices, there are total 2xOC-48 slices per OC-96 slice`

    * `$Pwid(0-2047): pseudowire channel for each OC-48 slice`

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`plalostaaddrmvctrl`| protocol state to add/remove pw Step1: Add intitial or pw idle, the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1" value for the protocol state to start adding pw. The protocol state value is "1". Step3: CPU enables pw at demap to finish the adding pw process. HW will automatically change the protocol state value to "2" to run the pw, and keep this state. Step4: For removing the pw, CPU will write "3" value for the protocol state to start removing pw. The protocol state value is "3". Step5: Poll the protocol state until return value "0" value, after that CPU disables pw at demap to finish the removing pw process| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Pseudowire Header Control

* **Description**           

This register is used to config Control Word for the psedowire channels

HDL_PATH  : ipwcore.ipwcfg.membuf.ram.ram[$pwid]


* **RTL Instant Name**    : `pla_pw_hdr_ctrl`

* **Address**             : `0x2_0000-0x2_29FF`

* **Formula**             : `0x2_0000 + $pwid`

* **Where**               : 

    * `$pwid(0-10751): pseudowire channel`

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9]`|`plapwhiratectrl`| set 1 for high rate path (12c/48c/192c), other is set 0| `RW`| `0x0`| `0x0`|
|`[8]`|`plapwsuprctrl`| Suppresion Enable 1: enable 0: disable| `RW`| `0x0`| `0x0`|
|`[7]`|`plapwrbitdisctrl`| R bit disable 1: disable R bit in the Control Word (assign zero in the packet) 0: normal| `RW`| `0x0`| `0x0`|
|`[6]`|`plapwmbitdisctrl`| M or NP bits disable 1: disable M or NP bits in the Control Word (assign zero in the packet) 0: normal| `RW`| `0x0`| `0x0`|
|`[5]`|`plapwlbitdisctrl`| L bit disable 1: disable L bit in the Control Word (assign zero in the packet) 0: normal| `RW`| `0x0`| `0x0`|
|`[4]`|`plapwrbitcpuctrl`| R bit value from CPU (low priority than PlaPwRbitDisCtrl)| `RW`| `0x0`| `0x0`|
|`[3:2]`|`plapwmbitcpuctrl`| M or NP bits value from CPU (low priority than PlaLoPwMbitDisCtrl)| `RW`| `0x0`| `0x0`|
|`[1]`|`plapwlbitcpuctrl`| L bit value from CPU (low priority than PlaLoPwLbitDisCtrl)| `RW`| `0x0`| `0x0`|
|`[0]`|`plapwenctrl`| Pseudowire enable 1: enable 0: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Pseudowire PSN Control

* **Description**           

This register is used to config psedowire PSN and APS at output

HDL_PATH  : irdport.iflowcfg.membuf.ram.ram[$pwid]


* **RTL Instant Name**    : `pla_pw_psn_ctrl`

* **Address**             : `0x4_8000-0x4_A9FF`

* **Formula**             : `0x4_8000 + $pwid`

* **Where**               : 

    * `$pwid(0-10751): pseudowire channel`

* **Width**               : `7`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6:0]`|`plaoutpsnlenctrl`| PSN length| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Psedowire Protection Control

* **Description**           

This register is used to config UPSR/HSPW for protect psedowire

HDL_PATH  : irdport.iapslkcfg.membuf.ram.ram[$pwid]


* **RTL Instant Name**    : `pla_pw_pro_ctrl`

* **Address**             : `0x5_0000-0x5_29FF`

* **Formula**             : `0x5_0000 + $pwid`

* **Where**               : 

    * `$pwid(0-10751): flow channel`

* **Width**               : `30`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[29:16]`|`plaoutupsrgrpctr`| UPRS group| `RW`| `0x0`| `0x0`|
|`[15:2]`|`plaouthspwgrpctr`| HSPW group| `RW`| `0x0`| `0x0`|
|`[1]`|`plaflowupsrusedctrl`| Flow UPSR is used or not 0:not used, all configurations for UPSR will be not effected 1:used| `RW`| `0x0`| `0x0`|
|`[0]`|`plaflowhspwusedctrl`| Flow HSPW is used or not 0:not used, all configurations for HSPW will be not effected 1:used| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Output UPSR Control

* **Description**           

This register is used to config UPSR group

HDL_PATH  : irdport.iupsrcfg.membuf.ram.ram[$upsrgrp]


* **RTL Instant Name**    : `pla_out_upsr_ctrl`

* **Address**             : `0x5_8000-0x5_A9FF`

* **Formula**             : `0x5_8000 + $upsrgrp`

* **Where**               : 

    * `$upsrgrp(0-671): UPSR group address`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`plaoutupsrenctr`| Total is 10752 upsrID, divide into 672 group address for configuration, 16-bit is correlative with 16 upsrID each group. Bit#0 is for upsrID#0, bit#1 is for upsrID#1... 1: enable 0: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Output HSPW Control

* **Description**           

This register is used to config HSPW group

HDL_PATH  : irdport.ihspwcfg.membuf.ram.ram[$hspwgrp]


* **RTL Instant Name**    : `pla_out_hspw_ctrl`

* **Address**             : `0x6_0000-0x6_029F`

* **Formula**             : `0x6_0000 + $hspwgrp`

* **Where**               : 

    * `$hspwgrp(0-671): HSPW group address`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`plaouthspwctr`| Total is 10752 hspwID, divide into 672 group address for configuration, 16-bit is correlative with 16 hspwID each group. Bit#0 is for hspwID#0, bit#1 is for hspwID#1...| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Output PSN Process Control

* **Description**           

This register is used to control PSN configuration


* **RTL Instant Name**    : `pla_out_psnpro_ctrl`

* **Address**             : `0x4_0000`

* **Formula**             : `0x4_0000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`plaoutpsnproreqctr`| Request to process PSN 1: request to write/read PSN header buffer. The CPU need to prepare a complete PSN header into PSN buffer before request write OR read out a complete PSN header in PSN buffer after request read done 0: HW will automatically set to 0 when a request done| `RW`| `0x0`| `0x0`|
|`[30]`|`plaoutpsnprornwctr`| read or write PSN 1: read PSN 0: write PSN| `RW`| `0x0`| `0x0`|
|`[29:23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22:16]`|`plaoutpsnprolenctr`| Length of a complete PSN need to read/write| `RW`| `0x0`| `0x0`|
|`[15]`|`plaoutpsnpropagectr`| there is 2 pages PSN location per PWID, depend on the configuration of "PlaOutHspwPsnCtr", the engine will choice which the page to encapsulate into the packet.| `RW`| `0x0`| `0x0`|
|`[14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:0]`|`plaoutpsnproflowctr`| Flow channel| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Output PSN Buffer Control

* **Description**           

This register is used to store PSN data which is before CPU request write or after CPU request read %%

The header format from entry#0 to entry#4 is as follow: %%

{DA(6-byte),SA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 96 bytes)} %%

Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%



PSN header is MEF-8: %%

EthType:  0x88D8 %%

4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for 	remote 	receive side. %%

PSN header is MPLS:  %%

EthType:  0x8847 %%

4/8/12-byte PSN Header with format: {OuterLabel2[31:0](optional), OuterLabel1[31:0](optional),  InnerLabel[31:0]}   %%

Where InnerLabel[31:12] is pseodowire identification for remote receive side. %%

Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit =  for InnerLabel %%

PSN header is UDP/Ipv4:  %%

EthType:  0x0800 %%

28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %%

{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %%

{IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %%

{IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %%

{IP_SrcAdr[31:0]} %%

{IP_DesAdr[31:0]} %%

{UDP_SrcPort[15:0], UDP_DesPort[15:0]} %%

{UDP_Sum[31:0] (optional)} %%

Case: %%

IP_Protocol[7:0]: 0x11 to signify UDP  %%

IP_Protocol[7:0]: 0x11 to signify UDP  %%

IP_Header_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields %%

{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +%%

IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +%%

{IP_TTL[7:0], IP_Protocol[7:0]} + %%

IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +%%

IP_DesAdr[31:16] + IP_DesAdr[15:0]%%

UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%

UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%

UDP_Sum[31:0] (optional): CPU calculate these fields as a temporarily for HW before plus rest fields%%

IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%%

IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%%

IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%%

IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%%

IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%%

IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%%

IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%%

IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%

{8-bit zeros, IP_Protocol[7:0]} +%%

UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%

PSN header is UDP/Ipv6:  %%

EthType:  0x86DD%%

48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:%%

{IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}%%

{16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]} %%

{IP_SrcAdr[127:96]}%%

{IP_SrcAdr[95:64]}%%

{IP_SrcAdr[63:32]}%%

{IP_SrcAdr[31:0]}%%

{IP_DesAdr[127:96]}%%

{IP_DesAdr[95:64]}%%

{IP_DesAdr[63:32]}%%

{IP_DesAdr[31:0]}%%

{UDP_SrcPort[15:0], UDP_DesPort[15:0]}%%

{UDP_Sum[31:0]}%%

Case:%%

IP_Next_Header[7:0]: 0x11 to signify UDP %%

UDP_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields%%

IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%%

IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%%

IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%%

IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%%

IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%%

IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%%

IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%%

IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%

{8-bit zeros, IP_Next_Header[7:0]} +%%

UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%

UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%

UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%

User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field. %%

PSN header is MPLS over Ipv4:%%

IPv4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS%%

After IPv4 header is MPLS labels where inner label is used for PW identification%%

PSN header is MPLS over Ipv6:%%

IPv6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS%%

After IPv6 header is MPLS labels where inner label is used for PW identification%%

PSN header (all modes) with RTP enable: RTP used for TDM PW (define in RFC3550), is in the first 8-byte of this buffer (bit[127:64] of segid == 0), following is PSN header (start from bit[63:0] of segid = 0)%%

Case:%%

RTPSSRC[31:0] : This is the SSRC value of RTP header%%

RtpPtValue[6:0]: This is the PT value of RTP header, define in http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml


* **RTL Instant Name**    : `pla_out_psnbuf_ctrl`

* **Address**             : `0x4_0010-0x4_0015`

* **Formula**             : `0x4_0010 + $segid`

* **Where**               : 

    * `$segid(0-5): segment 16-byte PSN, total is 6 segments for maximum 96 bytes PSN  %`

* **Width**               : `128`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:0]`|`plaoutpsnproreqctr`| PSN buffer| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Hold Register Control

* **Description**           

This register is used to control hold register.


* **RTL Instant Name**    : `pla_hold_reg_ctrl`

* **Address**             : `0x7_0000-0x7_0002`

* **Formula**             : `0x7_0000 + $holdreg`

* **Where**               : 

    * `$holdreg(0-2): hold register with (holdreg=0) for bit63_32, (holdreg=1) for bit95_64, (holdreg=2) for bit127_96`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`plaholdregctr`| hold register value| `RW`| `0x0`| `0x0 End: Begin:`|

###Read HA Address Bit3_0 Control

* **Description**           

This register is used to send HA read address bit3_0 to HA engine


* **RTL Instant Name**    : `rdha3_0_control`

* **Address**             : `0x70100 - 0x7010F`

* **Formula**             : `0x70100 + HaAddr3_0`

* **Where**               : 

    * `$HaAddr3_0(0-15): HA Address Bit3_0`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr3_0`| Read value will be 0x8C100 plus HaAddr3_0| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit7_4 Control

* **Description**           

This register is used to send HA read address bit7_4 to HA engine


* **RTL Instant Name**    : `rdha7_4_control`

* **Address**             : `0x70110 - 0x7011F`

* **Formula**             : `0x70110 + HaAddr7_4`

* **Where**               : 

    * `$HaAddr7_4(0-15): HA Address Bit7_4`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr7_4`| Read value will be 0x8C100 plus HaAddr7_4| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit11_8 Control

* **Description**           

This register is used to send HA read address bit11_8 to HA engine


* **RTL Instant Name**    : `rdha11_8_control`

* **Address**             : `0x70120 - 0x7012F`

* **Formula**             : `0x70120 + HaAddr11_8`

* **Where**               : 

    * `$HaAddr11_8(0-15): HA Address Bit11_8`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr11_8`| Read value will be 0x8C100 plus HaAddr11_8| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit15_12 Control

* **Description**           

This register is used to send HA read address bit15_12 to HA engine


* **RTL Instant Name**    : `rdha15_12_control`

* **Address**             : `0x070130 - 0x7013F`

* **Formula**             : `0x070130 + HaAddr15_12`

* **Where**               : 

    * `$HaAddr15_12(0-15): HA Address Bit15_12`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr15_12`| Read value will be 0x8C100 plus HaAddr15_12| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit19_16 Control

* **Description**           

This register is used to send HA read address bit19_16 to HA engine


* **RTL Instant Name**    : `rdha19_16_control`

* **Address**             : `0x70140 - 0x7014F`

* **Formula**             : `0x70140 + HaAddr19_16`

* **Where**               : 

    * `$HaAddr19_16(0-15): HA Address Bit19_16`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr19_16`| Read value will be 0x8C100 plus HaAddr19_16| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit23_20 Control

* **Description**           

This register is used to send HA read address bit23_20 to HA engine


* **RTL Instant Name**    : `rdha23_20_control`

* **Address**             : `0x70150 - 0x7015F`

* **Formula**             : `0x70150 + HaAddr23_20`

* **Where**               : 

    * `$HaAddr23_20(0-15): HA Address Bit23_20`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr23_20`| Read value will be 0x8C100 plus HaAddr23_20| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit24 and Data Control

* **Description**           

This register is used to send HA read address bit24 to HA engine to read data


* **RTL Instant Name**    : `rdha24data_control`

* **Address**             : `0x70160 - 0x70161`

* **Formula**             : `0x70160 + HaAddr24`

* **Where**               : 

    * `$HaAddr24(0-1): HA Address Bit24`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata31_0`| HA read data bit31_0| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data63_32

* **Description**           

This register is used to read HA dword2 of data.


* **RTL Instant Name**    : `rdha_hold63_32`

* **Address**             : `0x70170`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata63_32`| HA read data bit63_32| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data95_64

* **Description**           

This register is used to read HA dword3 of data.


* **RTL Instant Name**    : `rdindr_hold95_64`

* **Address**             : `0x70171`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata95_64`| HA read data bit95_64| `RO`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data127_96

* **Description**           

This register is used to read HA dword4 of data.


* **RTL Instant Name**    : `rdindr_hold127_96`

* **Address**             : `0x70172`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata127_96`| HA read data bit127_96| `RO`| `0xx`| `0xx End: Begin:`|

###Payload Assembler Output PSN Process Control

* **Description**           

This register is used to control PSN configuration


* **RTL Instant Name**    : `pla_out_psnpro_ha_ctrl`

* **Address**             : `0x4_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`plaoutpsnproreqctr`| Request to process PSN 1: request to write/read PSN header buffer. The CPU need to prepare a complete PSN header into PSN buffer before request write OR read out a complete PSN header in PSN buffer after request read done 0: HW will automatically set to 0 when a request done| `RW`| `0x0`| `0x0`|
|`[30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22:16]`|`plaoutpsnprolenctr`| Length of a complete PSN need to read/write| `RW`| `0x0`| `0x0`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`plaoutpsnpropagectr`| there is 2 pages PSN location per PWID, depend on the configuration of "PlaOutHspwPsnCtr", the engine will choice which the page to encapsulate into the packet.| `RW`| `0x0`| `0x0`|
|`[12:0]`|`plaoutpsnpropwidctr`| Pseudowire channel| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Force Parity Error Control

* **Description**           

This register is used to force parity error


* **RTL Instant Name**    : `pla_force_par_err_control`

* **Address**             : `0x42040`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `21`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[20]`|`forceparerroutupsrctrreg`| Force Parity Error Ouput UPSR Control Register| `RW`| `0x0`| `0x0`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`forceparerrpwproctrreg`| Force Parity Error PW Protection Control Register| `RW`| `0x0`| `0x0`|
|`[17]`|`forceparerrpwpsnctrreg`| Force Parity Error PW PSNr Control Register| `RW`| `0x0`| `0x0`|
|`[16]`|`forceparerrpwhdrctrreg`| Force Parity Error PW Header Control Register| `RW`| `0x0`| `0x0`|
|`[15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`forceparerrlkpwctrregoc96_3`| Force Parity Error Lookup PW  Control Register OC96#3| `RW`| `0x0`| `0x0`|
|`[13]`|`forceparerrpldctrregoc96_3_oc48_1`| Force Parity Error Payload Control Register OC48#1 of OC96#3| `RW`| `0x0`| `0x0`|
|`[12]`|`forceparerrpldctrregoc96_3_oc48_0`| Force Parity Error Payload Control Register OC48#0 of OC96#3| `RW`| `0x0`| `0x0`|
|`[11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10]`|`forceparerrlkpwctrregoc96_2`| Force Parity Error Lookup PW  Control Register OC96#2| `RW`| `0x0`| `0x0`|
|`[9]`|`forceparerrpldctrregoc96_2_oc48_1`| Force Parity Error Payload Control Register OC48#1 of OC96#2| `RW`| `0x0`| `0x0`|
|`[8]`|`forceparerrpldctrregoc96_2_oc48_0`| Force Parity Error Payload Control Register OC48#0 of OC96#2| `RW`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`forceparerrlkpwctrregoc96_1`| Force Parity Error Lookup PW  Control Register OC96#1| `RW`| `0x0`| `0x0`|
|`[5]`|`forceparerrpldctrregoc96_1_oc48_1`| Force Parity Error Payload Control Register OC48#1 of OC96#1| `RW`| `0x0`| `0x0`|
|`[4]`|`forceparerrpldctrregoc96_1_oc48_0`| Force Parity Error Payload Control Register OC48#0 of OC96#1| `RW`| `0x0`| `0x0`|
|`[3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`forceparerrlkpwctrregoc96_0`| Force Parity Error Lookup PW  Control Register OC96#0| `RW`| `0x0`| `0x0`|
|`[1]`|`forceparerrpldctrregoc96_0_oc48_1`| Force Parity Error Payload Control Register OC48#1 of OC96#0| `RW`| `0x0`| `0x0`|
|`[0]`|`forceparerrpldctrregoc96_0_oc48_0`| Force Parity Error Payload Control Register OC48#0 of OC96#0| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Disable Parity Control

* **Description**           

This register is used to disable parity check


* **RTL Instant Name**    : `pla_dis_par_control`

* **Address**             : `0x42041`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `21`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[20]`|`disparerroutupsrctrreg`| Dis Parity Error Ouput UPSR Control Register| `RW`| `0x0`| `0x0`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`disparerrpwproctrreg`| Dis Parity Error PW Protection Control Register| `RW`| `0x0`| `0x0`|
|`[17]`|`disparerrpwpsnctrreg`| Dis Parity Error PW PSNr Control Register| `RW`| `0x0`| `0x0`|
|`[16]`|`disparerrpwhdrctrreg`| Dis Parity Error PW Header Control Register| `RW`| `0x0`| `0x0`|
|`[15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`disparerrlkpwctrregoc96_3`| Dis Parity Error Lookup PW  Control Register OC96#3| `RW`| `0x0`| `0x0`|
|`[13]`|`disparerrpldctrregoc96_3_oc48_1`| Dis Parity Error Payload Control Register OC48#1 of OC96#3| `RW`| `0x0`| `0x0`|
|`[12]`|`disparerrpldctrregoc96_3_oc48_0`| Dis Parity Error Payload Control Register OC48#0 of OC96#3| `RW`| `0x0`| `0x0`|
|`[11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10]`|`disparerrlkpwctrregoc96_2`| Dis Parity Error Lookup PW  Control Register OC96#2| `RW`| `0x0`| `0x0`|
|`[9]`|`disparerrpldctrregoc96_2_oc48_1`| Dis Parity Error Payload Control Register OC48#1 of OC96#2| `RW`| `0x0`| `0x0`|
|`[8]`|`disparerrpldctrregoc96_2_oc48_0`| Dis Parity Error Payload Control Register OC48#0 of OC96#2| `RW`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`disparerrlkpwctrregoc96_1`| Dis Parity Error Lookup PW  Control Register OC96#1| `RW`| `0x0`| `0x0`|
|`[5]`|`disparerrpldctrregoc96_1_oc48_1`| Dis Parity Error Payload Control Register OC48#1 of OC96#1| `RW`| `0x0`| `0x0`|
|`[4]`|`disparerrpldctrregoc96_1_oc48_0`| Dis Parity Error Payload Control Register OC48#0 of OC96#1| `RW`| `0x0`| `0x0`|
|`[3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`disparerrlkpwctrregoc96_0`| Dis Parity Error Lookup PW  Control Register OC96#0| `RW`| `0x0`| `0x0`|
|`[1]`|`disparerrpldctrregoc96_0_oc48_1`| Dis Parity Error Payload Control Register OC48#1 of OC96#0| `RW`| `0x0`| `0x0`|
|`[0]`|`disparerrpldctrregoc96_0_oc48_0`| Dis Parity Error Payload Control Register OC48#0 of OC96#0| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Parity Error Sticky

* **Description**           

This register is used to sticky parity check


* **RTL Instant Name**    : `pla_par_err_stk`

* **Address**             : `0x42042`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `21`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[20]`|`parerroutupsrctrstk`| Dis Parity Error Ouput UPSR Control Register| `W1C`| `0x0`| `0x0`|
|`[19]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[18]`|`parerrpwproctrstk`| Dis Parity Error PW Protection Control Register| `W1C`| `0x0`| `0x0`|
|`[17]`|`parerrpwpsnctrstk`| Dis Parity Error PW PSNr Control Register| `W1C`| `0x0`| `0x0`|
|`[16]`|`parerrpwhdrctrstk`| Dis Parity Error PW Header Control Register| `W1C`| `0x0`| `0x0`|
|`[15]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[14]`|`parerrlkpwctrregoc96_3stk`| Dis Parity Error Lookup PW  Control Register OC96#3| `W1C`| `0x0`| `0x0`|
|`[13]`|`parerrpldctrregoc96_3_oc48_1stk`| Dis Parity Error Payload Control Register OC48#1 of OC96#3| `W1C`| `0x0`| `0x0`|
|`[12]`|`parerrpldctrregoc96_3_oc48_0stk`| Dis Parity Error Payload Control Register OC48#0 of OC96#3| `W1C`| `0x0`| `0x0`|
|`[11]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[10]`|`parerrlkpwctrregoc96_2stk`| Dis Parity Error Lookup PW  Control Register OC96#2| `W1C`| `0x0`| `0x0`|
|`[9]`|`parerrpldctrregoc96_2_oc48_1stk`| Dis Parity Error Payload Control Register OC48#1 of OC96#2| `W1C`| `0x0`| `0x0`|
|`[8]`|`parerrpldctrregoc96_2_oc48_0stk`| Dis Parity Error Payload Control Register OC48#0 of OC96#2| `W1C`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[6]`|`parerrlkpwctrregoc96_1stk`| Dis Parity Error Lookup PW  Control Register OC96#1| `W1C`| `0x0`| `0x0`|
|`[5]`|`parerrpldctrregoc96_1_oc48_1stk`| Dis Parity Error Payload Control Register OC48#1 of OC96#1| `W1C`| `0x0`| `0x0`|
|`[4]`|`parerrpldctrregoc96_1_oc48_0stk`| Dis Parity Error Payload Control Register OC48#0 of OC96#1| `W1C`| `0x0`| `0x0`|
|`[3]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[2]`|`parerrlkpwctrregoc96_0stk`| Dis Parity Error Lookup PW  Control Register OC96#0| `W1C`| `0x0`| `0x0`|
|`[1]`|`parerrpldctrregoc96_0_oc48_1stk`| Dis Parity Error Payload Control Register OC48#1 of OC96#0| `W1C`| `0x0`| `0x0`|
|`[0]`|`parerrpldctrregoc96_0_oc48_0stk`| Dis Parity Error Payload Control Register OC48#0 of OC96#0| `W1C`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Force CRC Error Control

* **Description**           

This register is used to force CRC error


* **RTL Instant Name**    : `pla_force_crc_err_control`

* **Address**             : `0x42030`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `29`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28]`|`placrcerrforevercfg`| Force crc error mode  1: forever when field PLACrcErrNumberCfg differ zero 0: burst| `RW`| `0x0`| `0x0`|
|`[27:0]`|`placrcerrnumbercfg`| number of CRC error inserted to PLA DDR| `RW`| `0x0`| `0x0 End: Begin:`|

###PLA CRC Error Sticky

* **Description**           

This register report sticky of PLA CRC error.


* **RTL Instant Name**    : `pla_crc_sticky`

* **Address**             : `0x42031`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`placrcerror`| PLA CRC error| `WC`| `0x0`| `0x0 End: Begin:`|

###PLA CRC Error Counter

* **Description**           

This register counts PLA CRC error.


* **RTL Instant Name**    : `PLA_crc_counter`

* **Address**             : `0x42032`

* **Formula**             : `0x42032 + R2C`

* **Where**               : 

    * `$R2C(0-1): value zero for read only,value 1 for read to clear`

* **Width**               : `28`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`placrcerrorcounter`| PLA Crc error counter| `RO`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Low-Order Payload Control

* **Description**           

This register is used to configure payload in each Pseudowire channels per slice


* **RTL Instant Name**    : `pla_192c_pld_ctrl`

* **Address**             : `0x2_8000-0x2_C000`

* **Formula**             : `0x2_8000 + $Oc192Slice*16384`

* **Where**               : 

    * `$Oc192Slice(0-1): OC-192 slices, there are total 2xOC-192 slices for 20G`

* **Width**               : `30`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[29:16]`|`pla192pwidctrl`| lookup pwid| `RW`| `0x0`| `0x0`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:0]`|`pla192pldctrl`| Payload Size| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler OC192c Add/Remove Pseudowire Protocol Control

* **Description**           

This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each OC-192 slice


* **RTL Instant Name**    : `pla_oc192c_add_rmv_pw_ctrl`

* **Address**             : `0x2_8001-0x2_C001`

* **Formula**             : `0x2_8001 + $Oc192Slice*16384`

* **Where**               : 

    * `$Oc192Slice(0-1): OC-192 slices, there are total 2xOC-192 slices for 20G`

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`plalostaaddrmvctrl`| protocol state to add/remove pw Step1: Add intitial or pw idle, the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1" value for the protocol state to start adding pw. The protocol state value is "1". Step3: CPU enables pw at demap to finish the adding pw process. HW will automatically change the protocol state value to "2" to run the pw, and keep this state. Step4: For removing the pw, CPU will write "3" value for the protocol state to start removing pw. The protocol state value is "3". Step5: Poll the protocol state until return value "0" value, after that CPU disables pw at demap to finish the removing pw process| `RW`| `0x0`| `0x0 End: Begin:`|

###BERT GEN CONTROL

* **Description**           

This register config mode for bert


* **RTL Instant Name**    : `bert_montdm_config`

* **Address**             : `0x4_4000 - 0x4_5000`

* **Formula**             : `0x4_4000 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10]`|`bert_inv`| Set 1 to enbale invert mode| `RW`| `0x1`| `0x1`|
|`[9:7]`|`thr_err`| Thrhold declare lost syn sta| `RW`| `0x3`| `0x3`|
|`[6:4]`|`thr_syn`| Thrhold declare syn sta| `RW`| `0x3`| `0x3`|
|`[3]`|`bertena`| Set 1 to enable BERT| `RW`| `0x0`| `0x0`|
|`[2:0]`|`bertmode`| "0" prbs15, "1" prbs23, "2" prbs31,"3" prbs20, "4" prbs20r, "5" all1,"6" all0| `RW`| `0x0`| `0x0 End: Begin:`|

###good counter tdm  r2c

* **Description**           

Counter


* **RTL Instant Name**    : `goodbit_pen_tdm_mon_r2c`

* **Address**             : `0x4_4001 - 0x4_5001`

* **Formula**             : `0x4_4001 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `34`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[33:00]`|`goodmonr2c`| mon goodbit mode read 2clear| `RC`| `0x0`| `0x0 End: Begin:`|

###good counter tdm  ro

* **Description**           

Counter


* **RTL Instant Name**    : `goodbit_pen_tdm_mon_ro`

* **Address**             : `0x4_4002 - 0x4_5002`

* **Formula**             : `0x4_4002 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `34`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[33:00]`|`goodmonro`| mon goodbit mode read only| `RO`| `0x0`| `0x0 End: Begin:`|

###error counter tdm  r2c

* **Description**           

Counter


* **RTL Instant Name**    : `errorbit_pen_tdm_mon_r2c`

* **Address**             : `0x4_4003 - 0x4_5003`

* **Formula**             : `0x4_4003 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`errormonr2c`| mon errorbit mode read 2clear| `RC`| `0x0`| `0x0 End: Begin:`|

###error counter tdm  ro

* **Description**           

Counter


* **RTL Instant Name**    : `errorbit_pen_tdm_mon_ro`

* **Address**             : `0x4_4004 - 0x4_5004`

* **Formula**             : `0x4_4004 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`errormonro`| mon errorbit mode read only| `RO`| `0x0`| `0x0 End: Begin:`|

###lost counter tdm  r2c

* **Description**           

Counter


* **RTL Instant Name**    : `lostbit_pen_tdm_mon_r2c`

* **Address**             : `0x4_4005 - 0x4_5005`

* **Formula**             : `0x4_4005 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`lostmonr2c`| mon lostbit mode read 2clear| `RC`| `0x0`| `0x0 End: Begin:`|

###lost counter tdm  ro

* **Description**           

Counter


* **RTL Instant Name**    : `lostbit_pen_tdm_mon_ro`

* **Address**             : `0x4_4006 - 0x4_5006`

* **Formula**             : `0x4_4006 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`lostmonro`| mon lostbit mode read only| `RO`| `0x0`| `0x0 End: Begin:`|

###lost counter tdm  ro

* **Description**           

Status


* **RTL Instant Name**    : `lostbit_pen_tdm_mon_ro`

* **Address**             : `0x4_4007 - 0x4_5007`

* **Formula**             : `0x4_4007 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`staprbs`| "3" SYNC , other LOST| `R0`| `0x0`| `0x0 End: Begin:`|

###lost counter tdm  ro

* **Description**           

Sticky


* **RTL Instant Name**    : `lostbit_pen_tdm_mon_ro`

* **Address**             : `0x4_4008 - 0x4_5008`

* **Formula**             : `0x4_4008 + $bid * 0x1000`

* **Where**               : 

    * `$bid(0-1): BERT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0:0]`|`stickyprbs`| "1" LOSTSYN| `W1C`| `0x0`| `0x1 End:`|

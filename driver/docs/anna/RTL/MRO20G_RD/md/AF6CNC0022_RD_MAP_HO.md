## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_MAP_HO
####Register Table

|Name|Address|
|-----|-----|
|`Demap Channel Control`|`0x0000-0x007FF`|
|`Map Line Control`|`0x4000-0x47FF`|
|`map_loopback_ctrl`|`0x4_0FF`|
|`Sel Ho Bert Gen`|`0x8_200 - 0xB_A00`|
|`Sel Mode Bert Gen`|`0x8_300 - 0xB_B00`|
|`Inser Error`|`0x8_320 - 0xB_B20`|
|`Counter num of bit gen`|`0x8_380 - 0xB_B80`|
|`Sel Ho Bert TDM Mon`|`0x8_400 - 0xB_C00`|
|`Sel Ho Bert PW Mon`|`0x8_600 - 0xB_E00`|
|`Sel Mode Bert TDM mon`|`0x8_510 - 0xB_D10`|
|`Sel Mode Bert PW mon`|`0x8_710 - 0xB_F10`|
|`TDM loss sync`|`0x8_502 - 0xB_D02`|
|`PW loss sync`|`0x8_702 - 0xB_F02`|
|`counter good bit TDM mon`|`0x8_580 - 0xB_D80`|
|`counter error bit in TDM mon`|`0x8_560 - 0xB_D60`|
|`Counter loss bit in TDM mon`|`0x8_5A0 - 0xB_DA0`|
|`counter good bit TDM mon`|`0x8_780 - 0xB_F80`|
|`counter error bit in TDM mon`|`0x8_760 - 0xB_F60`|
|`Counter loss bit in TDM mon`|`0x8_7A0 - 0xB_FA0`|
|`RAM Map Parity Force Control`|`0x4808`|
|`RAM Map Parity Disable Control`|`0x4809`|
|`RAM Map parity Error Sticky`|`0x480a`|
|`RAM DeMap Parity Force Control`|`0x0808`|
|`RAM DeMap Parity Disable Control`|`0x0809`|
|`RAM DeMap parity Error Sticky`|`0x080a`|


###Demap Channel Control

* **Description**           

The registers are used by the hardware to configure PW channel


* **RTL Instant Name**    : `demap_channel_ctrl`

* **Address**             : `0x0000-0x007FF`

* **Formula**             : `0x0000 + 256*slice + stsid`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

    * `$stsid(0-47): is STS in OC48`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:5]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[4:3]`|`demapchtype`| 0:CEP,  1:HDLC/PPP/ML, 2:VCAT/LCAS| `RW`| `0x0`| `0x0`|
|`[2]`|`demapsr_vc3n3c`| 0:slave of VC3_N3c  1:master of VC3-N3c| `RW`| `0x0`| `0x0`|
|`[1]`|`demapsrctype`| 0:VC3 1:VC3-3c| `RW`| `0x0`| `0x0`|
|`[0]`|`demapchen`| PW/Channels Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Map Line Control

* **Description**           

The registers provide the per line configurations for STS


* **RTL Instant Name**    : `map_line_ctrl`

* **Address**             : `0x4000-0x47FF`

* **Formula**             : `0x4000 + 256*slice + stsid`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

    * `$stsid(0-47): is STS in OC48`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:10]`|`mapchtype`| 0:CEP,  1:HDLC/PPP/ML, 2:VCAT/LCAS| `RW`| `0x0`| `0x0`|
|`[9]`|`mapsrc_vc3n3c`| 0:slave of VC3_N3c  1:master of VC3-N3c| `RW`| `0x0`| `0x0`|
|`[8]`|`mapsrc_type`| 0:VC3 1:VC3-3c| `RW`| `0x0`| `0x0`|
|`[7]`|`mapchen`| PW/Channels Enable| `RW`| `0x0`| `0x0`|
|`[6]`|`maptimesrcmaster`| This bit is used to indicate the master timing or the master VC3 in VC4/VC4-Xc| `RW`| `0x0`| `0x0`|
|`[5:0]`|`maptimesrcid`| The reference line ID used for timing reference or the master VC3 ID in VC4/VC4-Xc| `RW`| `0x0`| `0x0 End: Begin:`|

###map_loopback_ctrl

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `map_loopback_ctrl`

* **Address**             : `0x4_0FF`

* **Formula**             : `0x4_0FF + 256*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8]`|`map_loopout`| Map loop out full Slice 48| `RW`| `0x0`| `0x0`|
|`[7:0]`|`idlecode`| IDLE Code| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert Gen

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_gen`

* **Address**             : `0x8_200 - 0xB_A00`

* **Formula**             : `0x8_200 + 2048*engid`

* **Where**               : 

    * `$engid(0-1): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`gen_en`| set "1" to enable bert gen| `RW`| `0x0`| `0x0`|
|`[8:6]`|`line_id`| line id OC48| `RW`| `0x0`| `0x0`|
|`[5:0]`|`stsid`| STS ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Mode Bert Gen

* **Description**           

The registers select mode bert gen


* **RTL Instant Name**    : `ctrl_pen_gen`

* **Address**             : `0x8_300 - 0xB_B00`

* **Formula**             : `0x8_300 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[09]`|`swapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[08]`|`invmode`| invert data| `RW`| `0x0`| `0x0`|
|`[07:00]`|`patt_mode`| sel pattern gen # 0x01 : all0 # 0x02 : prbs15 # 0x04 : prbs20r # 0x08 : prbs20 # 0x10 : prbs23 # 0x20 : prbs31 # other: all1| `RW`| `0x0`| `0x0 End: Begin:`|

###Inser Error

* **Description**           

The registers select rate inser error


* **RTL Instant Name**    : `ctrl_ber_pen`

* **Address**             : `0x8_320 - 0xB_B20`

* **Formula**             : `0x8_320 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ber_rate`| TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to Pattern Generator [31:0] == 32'd0          :disable  BER_level_val	BER_level 1000:        	BER 10e-3 10_000:      	BER 10e-4 100_000:     	BER 10e-5 1_000_000:   	BER 10e-6 10_000_000:  	BER 10e-7 100_000_000: 	BER 10e-8 1_000_000_000 : BER 10e-9| `RW`| `0x0`| `0x0 Begin:`|

###Counter num of bit gen

* **Description**           

The registers counter bit genertaie in tdm side


* **RTL Instant Name**    : `goodbit_ber_pen`

* **Address**             : `0x8_380 - 0xB_B80`

* **Formula**             : `0x8_380 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`goodbit`| counter goodbit| `R2C`| `0x0`| `0x0 Begin:`|

###Sel Ho Bert TDM Mon

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_tdm_mon`

* **Address**             : `0x8_400 - 0xB_C00`

* **Formula**             : `0x8_400 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`montdmen`| set "1" to enable bert tdm mon| `RW`| `0x0`| `0x0`|
|`[12:10]`|`tdmslide`| slide id| `RW`| `0x0`| `0x0`|
|`[9:0]`|`masterid`| chanel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert PW Mon

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_pw_mon`

* **Address**             : `0x8_600 - 0xB_E00`

* **Formula**             : `0x8_600 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`monpwen`| set "1" to enable bert pw mon| `RW`| `0x0`| `0x0`|
|`[12:10]`|`monpwslide`| pw slide id| `RW`| `0x0`| `0x0`|
|`[9:0]`|`masterid`| chanel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Mode Bert TDM mon

* **Description**           

The registers select mode bert mon in tdm side


* **RTL Instant Name**    : `ctrl_pen_tdm_mon`

* **Address**             : `0x8_510 - 0xB_D10`

* **Formula**             : `0x8_510 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[19:16]`|`thrhold_pattern_sync`| minimum number of byte sync to go sync state| `RW`| `0x0`| `0xA`|
|`[15:12]`|`thrhold_error`| maximum err in state sync, if more than thrhold go loss sync| `RW`| `0x0`| `0x2`|
|`[11:10]`|`reservee`| reserve| `RW`| `0x0`| `0x0`|
|`[09]`|`swapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[08]`|`invmode`| invert data| `RW`| `0x0`| `0x0`|
|`[07:00]`|`patt_mode`| sel pattern gen # 0x01 : all1 # 0x02 : all0 # 0x04 : prbs15 # 0x08 : prbs20r # 0x10 : prbs20 # 0x20 : prss23 # 0x40 : prbs31| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Mode Bert PW mon

* **Description**           

The registers select mode bert mon in pw side


* **RTL Instant Name**    : `ctrl_pen_pw_mon`

* **Address**             : `0x8_710 - 0xB_F10`

* **Formula**             : `0x8_710 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[19:16]`|`thrhold_pattern_sync`| minimum number of byte sync to go sync state| `RW`| `0x0`| `0xA`|
|`[15:12]`|`thrhold_error`| maximum err in state sync, if more than thrhold go loss sync| `RW`| `0x0`| `0x2`|
|`[11:10]`|`pw_reserve`| pw reserve| `RW`| `0x0`| `0x0`|
|`[09]`|`swapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[08]`|`invmode`| invert data| `RW`| `0x0`| `0x0`|
|`[07:00]`|`patt_mode`| sel pattern gen # 0x01 : all1 # 0x02 : all0 # 0x04 : prbs15 # 0x08 : prbs20r # 0x10 : prbs20 # 0x20 : prss23 # 0x40 : prbs31| `RW`| `0x0`| `0x0 End: Begin:`|

###TDM loss sync

* **Description**           

The registers indicate bert mon loss sync in tdm side


* **RTL Instant Name**    : `loss_tdm_mon`

* **Address**             : `0x8_502 - 0xB_D02`

* **Formula**             : `0x8_502 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`sticky_err`| "1" indicate loss sync| `WC`| `0x0`| `0x0 End: Begin:`|

###PW loss sync

* **Description**           

The registers indicate bert mon loss sync in pw side


* **RTL Instant Name**    : `loss_pw_mon`

* **Address**             : `0x8_702 - 0xB_F02`

* **Formula**             : `0x8_702 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`sticky_err`| "1" indicate loss sync| `WC`| `0x0`| `0x0 End: Begin:`|

###counter good bit TDM mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `goodbit_pen_tdm_mon`

* **Address**             : `0x8_580 - 0xB_D80`

* **Formula**             : `0x8_580 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_goodbit`| counter goodbit| `R2C`| `0x0`| `0x0 End: Begin:`|

###counter error bit in TDM mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `err_pen_tdm_mon`

* **Address**             : `0x8_560 - 0xB_D60`

* **Formula**             : `0x8_560 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_errbit`| counter err bit| `R2C`| `0x0`| `0x0 End: Begin:`|

###Counter loss bit in TDM mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `lossbit_pen_tdm_mon`

* **Address**             : `0x8_5A0 - 0xB_DA0`

* **Formula**             : `0x8_5A0 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_errbit`| counter err bit| `R2C`| `0x0`| `0x0 End: Begin:`|

###counter good bit TDM mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `goodbit_pen_pw_mon`

* **Address**             : `0x8_780 - 0xB_F80`

* **Formula**             : `0x8_780 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_pwgoodbit`| counter goodbit| `R2C`| `0x0`| `0x0 End: Begin:`|

###counter error bit in TDM mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `errbit_pen_pw_mon`

* **Address**             : `0x8_760 - 0xB_F60`

* **Formula**             : `0x8_760 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_pwerrbit`| counter err bit| `R2C`| `0x0`| `0x0 End: Begin:`|

###Counter loss bit in TDM mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `lossbit_pen_pw_mon`

* **Address**             : `0x8_7A0 - 0xB_FA0`

* **Formula**             : `0x8_7A0 + 2048*slice`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_pwlossbi`| counter loss bit| `R2C`| `0x0`| `0x0 End: Begin:`|

###RAM Map Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_Map_Parity_Force_Control`

* **Address**             : `0x4808`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`mapslc7ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice7"| `RW`| `0x0`| `0x0`|
|`[6]`|`mapslc6ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice6"| `RW`| `0x0`| `0x0`|
|`[5]`|`mapslc5ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice5"| `RW`| `0x0`| `0x0`|
|`[4]`|`mapslc4ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice4"| `RW`| `0x0`| `0x0`|
|`[3]`|`mapslc3ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice3"| `RW`| `0x0`| `0x0`|
|`[2]`|`mapslc2ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice2"| `RW`| `0x0`| `0x0`|
|`[1]`|`mapslc1ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice1"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapslc0ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice0"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM Map Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_Map_Parity_Disable_Control`

* **Address**             : `0x4809`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`mapslc7ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice7"| `RW`| `0x0`| `0x0`|
|`[6]`|`mapslc6ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice6"| `RW`| `0x0`| `0x0`|
|`[5]`|`mapslc5ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice5"| `RW`| `0x0`| `0x0`|
|`[4]`|`mapslc4ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice4"| `RW`| `0x0`| `0x0`|
|`[3]`|`mapslc3ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice3"| `RW`| `0x0`| `0x0`|
|`[2]`|`mapslc2ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice2"| `RW`| `0x0`| `0x0`|
|`[1]`|`mapslc1ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice1"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapslc0ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice0"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM Map parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_Map_Parity_Error_Sticky`

* **Address**             : `0x480a`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7]`|`mapslc7ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice7"| `RW`| `0x0`| `0x0`|
|`[6]`|`mapslc6ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice6"| `RW`| `0x0`| `0x0`|
|`[5]`|`mapslc5ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice5"| `RW`| `0x0`| `0x0`|
|`[4]`|`mapslc4ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice4"| `RW`| `0x0`| `0x0`|
|`[3]`|`mapslc3ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice3"| `RW`| `0x0`| `0x0`|
|`[2]`|`mapslc2ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice2"| `RW`| `0x0`| `0x0`|
|`[1]`|`mapslc1ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice1"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapslc0ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice0"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM DeMap Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_DeMap_Parity_Force_Control`

* **Address**             : `0x0808`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`mapslc7ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice7"| `RW`| `0x0`| `0x0`|
|`[6]`|`mapslc6ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice6"| `RW`| `0x0`| `0x0`|
|`[5]`|`mapslc5ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice5"| `RW`| `0x0`| `0x0`|
|`[4]`|`mapslc4ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice4"| `RW`| `0x0`| `0x0`|
|`[3]`|`mapslc3ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice3"| `RW`| `0x0`| `0x0`|
|`[2]`|`mapslc2ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice2"| `RW`| `0x0`| `0x0`|
|`[1]`|`mapslc1ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice1"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapslc0ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice0"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM DeMap Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_DeMap_Parity_Disable_Control`

* **Address**             : `0x0809`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`mapslc7ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice7"| `RW`| `0x0`| `0x0`|
|`[6]`|`mapslc6ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice6"| `RW`| `0x0`| `0x0`|
|`[5]`|`mapslc5ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice5"| `RW`| `0x0`| `0x0`|
|`[4]`|`mapslc4ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice4"| `RW`| `0x0`| `0x0`|
|`[3]`|`mapslc3ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice3"| `RW`| `0x0`| `0x0`|
|`[2]`|`mapslc2ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice2"| `RW`| `0x0`| `0x0`|
|`[1]`|`mapslc1ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice1"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapslc0ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice0"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM DeMap parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_DeMap_Parity_Error_Sticky`

* **Address**             : `0x080a`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7]`|`mapslc7ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice7"| `RW`| `0x0`| `0x0`|
|`[6]`|`mapslc6ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice6"| `RW`| `0x0`| `0x0`|
|`[5]`|`mapslc5ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice5"| `RW`| `0x0`| `0x0`|
|`[4]`|`mapslc4ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice4"| `RW`| `0x0`| `0x0`|
|`[3]`|`mapslc3ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice3"| `RW`| `0x0`| `0x0`|
|`[2]`|`mapslc2ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice2"| `RW`| `0x0`| `0x0`|
|`[1]`|`mapslc1ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice1"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapslc0ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice0"| `RW`| `0x0`| `0x0 End:`|

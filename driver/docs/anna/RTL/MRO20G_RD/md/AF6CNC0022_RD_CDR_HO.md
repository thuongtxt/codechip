## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_CDR_HO
####Register Table

|Name|Address|
|-----|-----|
|`CDR CPU  Reg Hold Control`|`0x030000-0x030002`|
|`CDR Timing External reference control`|`0x0000003`|
|`CDR STS Timing control`|`0x0000100-0x000011F`|
|`CDR ACR Engine Timing control`|`0x0020800-0x0020BFF`|
|`CDR Adjust State status`|`0x0021000-0x00213FF`|
|`CDR Adjust Holdover value status`|`0x0021800-0x0021BFF`|
|`DCR TX Engine Active Control`|`0x010000`|
|`DCR PRC Source Select Configuration`|`0x0010001`|
|`DCR PRC Frequency Configuration`|`0x0010002`|
|`DCR PRC Frequency Configuration`|`0x001000b`|
|`DCR RTP Frequency Configuration`|`0x001000C`|
|`RAM TimingGen Parity Force Control`|`0x188`|
|`RAM TimingGen Parity Disable Control`|`0x189`|
|`RAM TimingGen parity Error Sticky`|`0x18a`|
|`RAM ACR Parity Force Control`|`0x2000c`|
|`RAM ACR Parity Disable Control`|`0x2000d`|
|`RAM ACR parity Error Sticky`|`0x2000e`|
|`Read HA Address Bit3_0 Control`|`0x30200`|
|`Read HA Address Bit7_4 Control`|`0x30210`|
|`Read HA Address Bit11_8 Control`|`0x30220`|
|`Read HA Address Bit15_12 Control`|`0x30230`|
|`Read HA Address Bit19_16 Control`|`0x30240`|
|`Read HA Address Bit23_20 Control`|`0x30250`|
|`Read HA Address Bit24 and Data Control`|`0x30260`|
|`Read HA Hold Data63_32`|`0x30270`|
|`Read HA Hold Data95_64`|`0x30271`|
|`Read HA Hold Data127_96`|`0x30272`|
|`CDR per Channel Interrupt Enable Control`|`0x00025000-0x000251FF`|
|`CDR per Channel Interrupt Status`|`0x00025200-0x000253FF`|
|`CDR per Channel Current Status`|`0x00025400-0x000255FF`|
|`CDR per Channel Interrupt OR Status`|`0x00025600-0x0002560F`|
|`CDR per STS/VC Interrupt OR Status`|`0x000257FF`|
|`CDR per STS/VC Interrupt Enable Control`|`0x000257FE`|
|`CDR InCome Packet Information Capture Trigger`|`0x0023007`|
|`CDR InCome Packet Information Capture Trigger`|`0x0023100`|


###CDR CPU  Reg Hold Control

* **Description**           

The register provides hold register for three word 32-bits MSB when CPU access to engine.


* **RTL Instant Name**    : `cdr_cpu_hold_ctrl`

* **Address**             : `0x030000-0x030002`

* **Formula**             : `0x030000 + HoldId`

* **Where**               : 

    * `$HoldId(0-2): Hold register`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdreg0`| Hold 32 bits| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Timing External reference control

* **Description**           

This register is used to configure for the 2Khz coefficient of two external reference signal in order to generate timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)


* **RTL Instant Name**    : `cdr_timing_extref_ctrl`

* **Address**             : `0x0000003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:32]`|`ext3n2k`| The 2Khz coefficient of the third external reference signal| `RW`| `0x0`| `0x0`|
|`[31:16]`|`ext2n2k`| The 2Khz coefficient of the second external reference signal| `RW`| `0x0`| `0x0`|
|`[15:0]`|`ext1n2k`| The 2Khz coefficient of the first external reference signal| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR STS Timing control

* **Description**           

This register is used to configure timing mode for per STS


* **RTL Instant Name**    : `cdr_sts_timing_ctrl`

* **Address**             : `0x0000100-0x000011F`

* **Formula**             : `0x0000100+STS`

* **Where**               : 

    * `$STS(0-47):`

* **Width**               : `40`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[39]`|`slc7vc3eparen`| VC3 EPAR mode 0 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[38]`|`slc6vc3eparen`| VC3 EPAR mode 0 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[37]`|`slc5vc3eparen`| VC3 EPAR mode 0 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[36]`|`slc4vc3eparen`| VC3 EPAR mode 0 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[35]`|`slc3vc3eparen`| VC3 EPAR mode 0 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[34]`|`slc2vc3eparen`| VC3 EPAR mode 0 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[33]`|`slc1vc3eparen`| VC3 EPAR mode 0 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[32]`|`slc0vc3eparen`| VC3 EPAR mode 0 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[31:28]`|`slc7vc3timemode`| VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode| `RW`| `0x0`| `0x0 10: Line OCN#5 timing mode 11: Line OCN#6 timing mode 12: Line OCN#7 timing mode 13: Line OCN#8 timing mode`|
|`[27:24]`|`slc6vc3timemode`| VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode| `RW`| `0x0`| `0x0 10: Line OCN#5 timing mode 11: Line OCN#6 timing mode 12: Line OCN#7 timing mode 13: Line OCN#8 timing mode`|
|`[23:20]`|`slc5vc3timemode`| VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode| `RW`| `0x0`| `0x0 10: Line OCN#5 timing mode 11: Line OCN#6 timing mode 12: Line OCN#7 timing mode 13: Line OCN#8 timing mode`|
|`[19:16]`|`slc4vc3timemode`| VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode| `RW`| `0x0`| `0x0 10: Line OCN#5 timing mode 11: Line OCN#6 timing mode 12: Line OCN#7 timing mode 13: Line OCN#8 timing mode`|
|`[15:12]`|`slc3vc3timemode`| VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode| `RW`| `0x0`| `0x0 10: Line OCN#5 timing mode 11: Line OCN#6 timing mode 12: Line OCN#7 timing mode 13: Line OCN#8 timing mode`|
|`[11:8]`|`slc2vc3timemode`| VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode| `RW`| `0x0`| `0x0 10: Line OCN#5 timing mode 11: Line OCN#6 timing mode 12: Line OCN#7 timing mode 13: Line OCN#8 timing mode`|
|`[7:4]`|`slc1vc3timemode`| VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode| `RW`| `0x0`| `0x0 10: Line OCN#5 timing mode 11: Line OCN#6 timing mode 12: Line OCN#7 timing mode 13: Line OCN#8 timing mode`|
|`[3:0]`|`slc0vc3timemode`| VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: CDR timing mode for CEP mode (ACR/DCR) 9: Line EXT#3 timing mode| `RW`| `0x0`| `0x0 10: Line OCN#5 timing mode 11: Line OCN#6 timing mode 12: Line OCN#7 timing mode 13: Line OCN#8 timing mode End: Begin:`|

###CDR ACR Engine Timing control

* **Description**           

This register is used to configure timing mode for per STS


* **RTL Instant Name**    : `cdr_acr_eng_timing_ctrl`

* **Address**             : `0x0020800-0x0020BFF`

* **Formula**             : `0x0020800 + 64*slice + stsid`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

    * `$stsid(0-47): is STS in OC48`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[26]`|`vc4_4c`| VC4_4c/TU3 mode 0: STS1/VC4 1: TU3/VC4-4c| `RW`| `0x0`| `0x0`|
|`[25]`|`pktlen_ind`| The payload packet  length for jumbo frame| `RW`| `0x0`| `0x0`|
|`[24]`|`holdvalmode`| Hold value mode of NCO, default value 0 0: Hardware calculated and auto update 1: Software calculated and update, hardware is disabled| `RW`| `0x0`| `0x0`|
|`[23]`|`seqmode`| Sequence mode mode, default value 0 0: Wrap zero 1: Skip zero| `RW`| `0x0`| `0x0`|
|`[22:20]`|`linetype`| Line type mode 0-5: unused 6: STS1/TU3 7: VC4/VC4-4c| `RW`| `0x0`| `0x0`|
|`[19:4]`|`pktlen`| The payload packet  length parameter to create a packet. SAToP mode: The number payload of bit. CESoPSN mode: The number of bit which converted to full DS1/E1 rate mode. In CESoPSN mode, the payload is assembled by NxDS0 with M frame, the value configured to this register is Mx256 bits for E1 mode, Mx193 bits for DS1 mode.| `RW`| `0x0`| `0x0`|
|`[3:0]`|`cdrtimemode`| CDR time mode 0-7: unused 8: ACR timing mode 9: unused 10: DCR timing mode 11: unused| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Adjust State status

* **Description**           

This register is used to store status or configure  some parameter of per CDR engine


* **RTL Instant Name**    : `cdr_adj_state_stat`

* **Address**             : `0x0021000-0x00213FF`

* **Formula**             : `0x0021000 + 64*slice + stsid`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

    * `$stsid(0-47): is STS in OC48`

* **Width**               : `8`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2:0]`|`adjstate`| Adjust state 0: Load state 1: Wait state 2: Initialization state 3: Learn State 4: Rapid State 5: Lock State 6: Freeze State 7: Holdover State| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Adjust Holdover value status

* **Description**           

This register is used to store status or configure  some parameter of per CDR engine


* **RTL Instant Name**    : `cdr_adj_holdover_value_stat`

* **Address**             : `0x0021800-0x0021BFF`

* **Formula**             : `0x0021800 + 64*slice + stsid`

* **Where**               : 

    * `$slice(0-1): is OC48 slice`

    * `$stsid(0-47): is STS in OC48`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdval`| NCO Holdover value| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR TX Engine Active Control

* **Description**           

This register is used to activate the DCR TX Engine. Active high.


* **RTL Instant Name**    : `dcr_tx_eng_active_ctrl`

* **Address**             : `0x010000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`data`| DCR TX Engine Active Control| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR PRC Source Select Configuration

* **Description**           

This register is used to configure to select PRC source for PRC timer. The PRC clock selected must be less than 19.44 Mhz.


* **RTL Instant Name**    : `dcr_prc_src_sel_cfg`

* **Address**             : `0x0010001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3]`|`dcrprcdirectmode`| 0: Select RTP frequency lower than 65535 and use value DCRRTPFreq as RTP frequency 1: Select RTP frequency value 155.52Mhz divide by DcrRtpFreqDivide. In this mode, below DcrPrcSourceSel[2:0] must set to 1| `RW`| `0x0`| `0x0`|
|`[2:0]`|`dcrprcsourcesel`| PRC source selection. 0: PRC Reference Clock 1: System Clock 19Mhz 2: External Reference Clock 1. 3: External Reference Clock 2. 4: Ocn Line Clock Port 1 5: Ocn Line Clock Port 2 6: Ocn Line Clock Port 3 7: Ocn Line Clock Port 4| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR PRC Frequency Configuration

* **Description**           

This register is used to select RTP frequency 77.76Mhz or 155.52Mhz


* **RTL Instant Name**    : `dcr_prd_freq_cfg`

* **Address**             : `0x0010002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`dcrrtpfreqdivide`| Dcr RTP Frequency = 155.52Mhz/ DcrRtpFreqDivide| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR PRC Frequency Configuration

* **Description**           

This register is used to configure the frequency of PRC clock.


* **RTL Instant Name**    : `dcr_prc_freq_cfg`

* **Address**             : `0x001000b`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`dcrprcfrequency`| Frequency of PRC clock. This is used to configure the frequency of PRC clock in Khz. Unit is Khz. Exp: The PRC clock is 19.44Mhz. This register will be configured to 19440.| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR RTP Frequency Configuration

* **Description**           

This register is used to configure the frequency of PRC clock.


* **RTL Instant Name**    : `dcr_rtp_freq_cfg`

* **Address**             : `0x001000C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`dcrrtpfreq`| Frequency of RTP clock. This is used to configure the frequency of RTP clock in Khz. Unit is Khz. Exp: The RTP clock is 19.44Mhz. This register will be configured to 19440.| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM TimingGen Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_TimingGen_Parity_Force_Control`

* **Address**             : `0x188`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:1]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`cdrststimingctrl_parerrfrc`| Force parity For RAM Control "CDR STS Timing control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM TimingGen Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_TimingGen_Parity_Diable_Control`

* **Address**             : `0x189`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:1]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`cdrststimingctrl_parerrdis`| Disable parity For RAM Control "CDR STS Timing control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM TimingGen parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_TimingGen_Parity_Error_Sticky`

* **Address**             : `0x18a`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`cdrststimingctrl_parerrstk`| Error parity For RAM Control "CDR STS Timing control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM ACR Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_ACR_Parity_Force_Control`

* **Address**             : `0x2000c`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`cdracrtimingctrlslc7_parerrfrc`| Force parity For RAM Control "CDR ACR Engine Timing control" Slice7| `RW`| `0x0`| `0x0`|
|`[6]`|`cdracrtimingctrlslc6_parerrfrc`| Force parity For RAM Control "CDR ACR Engine Timing control" Slice6| `RW`| `0x0`| `0x0`|
|`[5]`|`cdracrtimingctrlslc5_parerrfrc`| Force parity For RAM Control "CDR ACR Engine Timing control" Slice5| `RW`| `0x0`| `0x0`|
|`[4]`|`cdracrtimingctrlslc4_parerrfrc`| Force parity For RAM Control "CDR ACR Engine Timing control" Slice4| `RW`| `0x0`| `0x0`|
|`[3]`|`cdracrtimingctrlslc3_parerrfrc`| Force parity For RAM Control "CDR ACR Engine Timing control" Slice3| `RW`| `0x0`| `0x0`|
|`[2]`|`cdracrtimingctrlslc2_parerrfrc`| Force parity For RAM Control "CDR ACR Engine Timing control" Slice2| `RW`| `0x0`| `0x0`|
|`[1]`|`cdracrtimingctrlslc1_parerrfrc`| Force parity For RAM Control "CDR ACR Engine Timing control" Slice1| `RW`| `0x0`| `0x0`|
|`[0]`|`cdracrtimingctrlslc0_parerrfrc`| Force parity For RAM Control "CDR ACR Engine Timing control" Slice0| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM ACR Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_ACR_Parity_Disable_Control`

* **Address**             : `0x2000d`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`cdracrtimingctrlslc7_parerrdis`| Disable parity For RAM Control "CDR ACR Engine Timing control" Slice7| `RW`| `0x0`| `0x0`|
|`[6]`|`cdracrtimingctrlslc6_parerrdis`| Disable parity For RAM Control "CDR ACR Engine Timing control" Slice6| `RW`| `0x0`| `0x0`|
|`[5]`|`cdracrtimingctrlslc5_parerrdis`| Disable parity For RAM Control "CDR ACR Engine Timing control" Slice5| `RW`| `0x0`| `0x0`|
|`[4]`|`cdracrtimingctrlslc4_parerrdis`| Disable parity For RAM Control "CDR ACR Engine Timing control" Slice4| `RW`| `0x0`| `0x0`|
|`[3]`|`cdracrtimingctrlslc3_parerrdis`| Disable parity For RAM Control "CDR ACR Engine Timing control" Slice3| `RW`| `0x0`| `0x0`|
|`[2]`|`cdracrtimingctrlslc2_parerrdis`| Disable parity For RAM Control "CDR ACR Engine Timing control" Slice2| `RW`| `0x0`| `0x0`|
|`[1]`|`cdracrtimingctrlslc1_parerrdis`| Disable parity For RAM Control "CDR ACR Engine Timing control" Slice1| `RW`| `0x0`| `0x0`|
|`[0]`|`cdracrtimingctrlslc0_parerrdis`| Disable parity For RAM Control "CDR ACR Engine Timing control" Slice0| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM ACR parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_ACR_Parity_Error_Sticky`

* **Address**             : `0x2000e`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7]`|`cdracrtimingctrlslc7_parerrstk`| Error parity For RAM Control "CDR ACR Engine Timing control" Slice7| `RW`| `0x0`| `0x0`|
|`[6]`|`cdracrtimingctrlslc6_parerrstk`| Error parity For RAM Control "CDR ACR Engine Timing control" Slice6| `RW`| `0x0`| `0x0`|
|`[5]`|`cdracrtimingctrlslc5_parerrstk`| Error parity For RAM Control "CDR ACR Engine Timing control" Slice5| `RW`| `0x0`| `0x0`|
|`[4]`|`cdracrtimingctrlslc4_parerrstk`| Error parity For RAM Control "CDR ACR Engine Timing control" Slice4| `RW`| `0x0`| `0x0`|
|`[3]`|`cdracrtimingctrlslc3_parerrstk`| Error parity For RAM Control "CDR ACR Engine Timing control" Slice3| `RW`| `0x0`| `0x0`|
|`[2]`|`cdracrtimingctrlslc2_parerrstk`| Error parity For RAM Control "CDR ACR Engine Timing control" Slice2| `RW`| `0x0`| `0x0`|
|`[1]`|`cdracrtimingctrlslc1_parerrstk`| Error parity For RAM Control "CDR ACR Engine Timing control" Slice1| `RW`| `0x0`| `0x0`|
|`[0]`|`cdracrtimingctrlslc0_parerrstk`| Error parity For RAM Control "CDR ACR Engine Timing control" Slice0| `RW`| `0x0`| `0x0 End: Begin:`|

###Read HA Address Bit3_0 Control

* **Description**           

This register is used to send HA read address bit3_0 to HA engine


* **RTL Instant Name**    : `rdha3_0_control`

* **Address**             : `0x30200`

* **Formula**             : `0x30200 + HaAddr3_0`

* **Where**               : 

    * `$HaAddr3_0(0-15): HA Address Bit3_0`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr3_0`| Read value will be 0x01000 plus HaAddr3_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit7_4 Control

* **Description**           

This register is used to send HA read address bit7_4 to HA engine


* **RTL Instant Name**    : `rdha7_4_control`

* **Address**             : `0x30210`

* **Formula**             : `0x30210 + HaAddr7_4`

* **Where**               : 

    * `$HaAddr7_4(0-15): HA Address Bit7_4`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr7_4`| Read value will be 0x01000 plus HaAddr7_4| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit11_8 Control

* **Description**           

This register is used to send HA read address bit11_8 to HA engine


* **RTL Instant Name**    : `rdha11_8_control`

* **Address**             : `0x30220`

* **Formula**             : `0x30220 + HaAddr11_8`

* **Where**               : 

    * `$HaAddr11_8(0-15): HA Address Bit11_8`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr11_8`| Read value will be 0x01000 plus HaAddr11_8| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit15_12 Control

* **Description**           

This register is used to send HA read address bit15_12 to HA engine


* **RTL Instant Name**    : `rdha15_12_control`

* **Address**             : `0x30230`

* **Formula**             : `0x30230 + HaAddr15_12`

* **Where**               : 

    * `$HaAddr15_12(0-15): HA Address Bit15_12`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr15_12`| Read value will be 0x01000 plus HaAddr15_12| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit19_16 Control

* **Description**           

This register is used to send HA read address bit19_16 to HA engine


* **RTL Instant Name**    : `rdha19_16_control`

* **Address**             : `0x30240`

* **Formula**             : `0x30240 + HaAddr19_16`

* **Where**               : 

    * `$HaAddr19_16(0-15): HA Address Bit19_16`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr19_16`| Read value will be 0x01000 plus HaAddr19_16| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit23_20 Control

* **Description**           

This register is used to send HA read address bit23_20 to HA engine


* **RTL Instant Name**    : `rdha23_20_control`

* **Address**             : `0x30250`

* **Formula**             : `0x30250 + HaAddr23_20`

* **Where**               : 

    * `$HaAddr23_20(0-15): HA Address Bit23_20`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr23_20`| Read value will be 0x01000 plus HaAddr23_20| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit24 and Data Control

* **Description**           

This register is used to send HA read address bit24 to HA engine to read data


* **RTL Instant Name**    : `rdha24data_control`

* **Address**             : `0x30260`

* **Formula**             : `0x30260 + HaAddr24`

* **Where**               : 

    * `$HaAddr24(0-1): HA Address Bit24`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata31_0`| HA read data bit31_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data63_32

* **Description**           

This register is used to read HA dword2 of data.


* **RTL Instant Name**    : `rdha_hold63_32`

* **Address**             : `0x30270`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata63_32`| HA read data bit63_32| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data95_64

* **Description**           

This register is used to read HA dword3 of data.


* **RTL Instant Name**    : `rdindr_hold95_64`

* **Address**             : `0x30271`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata95_64`| HA read data bit95_64| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data127_96

* **Description**           

This register is used to read HA dword4 of data.


* **RTL Instant Name**    : `rdindr_hold127_96`

* **Address**             : `0x30272`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata127_96`| HA read data bit127_96| `RW`| `0xx`| `0xx End: Begin:`|

###CDR per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable of CDR


* **RTL Instant Name**    : `cdr_per_chn_intr_en_ctrl`

* **Address**             : `0x00025000-0x000251FF`

* **Formula**             : `0x00025000 +  StsID*32 + VtnID`

* **Where**               : 

    * `$StsID(0-15): STS-1/VC-3 ID, 0:ID 0-31 of OC48#0, 1: 32-47 of OC48#0, 2:0-31 of OC48#1, ...`

    * `$VtnID(0-31): VT/TU number ID in the Group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`cdrunlokcedintren`| Set 1 to enable change UnLocked te event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per Channel Interrupt Status

* **Description**           

This is the per Channel interrupt tus of CDR


* **RTL Instant Name**    : `cdr_per_chn_intr_stat`

* **Address**             : `0x00025200-0x000253FF`

* **Formula**             : `0x00025200 +  StsID*32 + VtnID`

* **Where**               : 

    * `$StsID(0-15): STS-1/VC-3 ID, 0:ID 0-31 of OC48#0, 1: 32-47 of OC48#0, 2:0-31 of OC48#1, ...`

    * `$VtnID(0-31): VT/TU number ID in the Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`cdrunlockedintr`| Set 1 if there is a change in UnLocked the event.| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per Channel Current Status

* **Description**           

This is the per Channel Current tus of CDR


* **RTL Instant Name**    : `cdr_per_chn_curr_stat`

* **Address**             : `0x00025400-0x000255FF`

* **Formula**             : `0x00025400 +  StsID*32 + VtnID`

* **Where**               : 

    * `$StsID(0-15): STS-1/VC-3 ID, 0:ID 0-31 of OC48#0, 1: 32-47 of OC48#0, 2:0-31 of OC48#1, ...`

    * `$VtnID(0-31): VT/TU number ID in the Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`cdrunlockedcurrsta`| Current tus of UnLocked event.| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per Channel Interrupt OR Status

* **Description**           

The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the CDR. Each bit is used to store Interrupt OR tus of the related DS1/E1.


* **RTL Instant Name**    : `cdr_per_chn_intr_or_stat`

* **Address**             : `0x00025600-0x0002560F`

* **Formula**             : `0x00025600 +  StsID`

* **Where**               : 

    * `$StsID(0-15): STS-1/VC-3 ID, 0:ID 0-31 of OC48#0, 1: 32-47 of OC48#0, 2:0-31 of OC48#1, ...`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`cdrvtintrorsta`| Set to 1 if any interrupt status bit of corresponding DS1/E1 is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per STS/VC Interrupt OR Status

* **Description**           

The register consists of 16 bits for 16 STS/VCs of the CDR. Each bit is used to store Interrupt OR tus of the related STS/VC.


* **RTL Instant Name**    : `cdr_per_stsvc_intr_or_stat`

* **Address**             : `0x000257FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`cdrstsintrorsta`| Set to 1 if any interrupt status bit of corresponding STS/VC is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per STS/VC Interrupt Enable Control

* **Description**           

The register consists of 16 interrupt enable bits for 16 STS/VCs in the Rx DS1/E1/J1 Framer.


* **RTL Instant Name**    : `cdr_per_stsvc_intr_en_ctrl`

* **Address**             : `0x000257FE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`cdrstsintren`| Set to 1 to enable the related STS/VC to generate interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR InCome Packet Information Capture Trigger

* **Description**           

The register consists of 16 interrupt enable bits for 16 STS/VCs in the Rx DS1/E1/J1 Framer.


* **RTL Instant Name**    : `cdr_income_packet_info_trigger`

* **Address**             : `0x0023007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16]`|`trigger`| Dump trigger, write 1 and write 0.| `RW`| `0x0`| `0x0`|
|`[15:0]`|`cdrid`| Set CDR ID, same value bit[14:0] of Classify Per Pseudowire Identification to CDR Control.| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR InCome Packet Information Capture Trigger

* **Description**           

The register consists of 16 interrupt enable bits for 16 STS/VCs in the Rx DS1/E1/J1 Framer.


* **RTL Instant Name**    : `cdr_income_packet_info_trigger`

* **Address**             : `0x0023100`

* **Formula**             : `0x0023100 +  entry`

* **Where**               : 

    * `$entry(0-255): 256 packet continuous`

* **Width**               : `128`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[65]`|`hoenb`| High Order path enable| `RW`| `0x0`| `0x0`|
|`[64]`|`pkterror`| packet error, set 1 incase of Lbit| `RW`| `0x0`| `0x0`|
|`[63:32]`|`rtptimestamp`| RTP timestamp| `RW`| `0x0`| `0x0`|
|`[31:16]`|`rtpseqnum`| RTP Sequence Number| `RW`| `0x0`| `0x0`|
|`[15:0]`|`cwseqnum`| Control Word Sequence Number| `RW`| `0x0`| `0x0 End:`|

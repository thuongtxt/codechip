## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-11-13|AF6Project|Initial version|




##AF6CNC0022_RD_CLA
####Register Table

|Name|Address|
|-----|-----|
|`Classify Global PSN Control`|`0x00000000 - 0x00000003`|
|`Classify HBCE Hashing Table Control`|`0x00020000 - 0x00023FFF`|
|`Classify Per Group Enable Control`|`0x00070000 - 0x00071FFF`|
|`Classify Per Ethernet port Enable Control`|`0x000D0000`|
|`Receive Ethernet port Count0_64 bytes packet`|`0x000D1000(RO)`|
|`Receive Ethernet port Count0_64 bytes packet`|`0x000D1800(RC)`|
|`Receive Ethernet port Count65_127 bytes packet`|`0x000D1004(RO)`|
|`Receive Ethernet port Count65_127 bytes packet`|`0x000D1804(RC)`|
|`Receive Ethernet port Count128_255 bytes packet`|`0x000D1008(RO)`|
|`Receive Ethernet port Count128_255 bytes packet`|`0x000D1808(RC)`|
|`Receive Ethernet port Count256_511 bytes packet`|`0x000D100C(RO)`|
|`Receive Ethernet port Count256_511 bytes packet`|`0x000D180C(RC)`|
|`Receive Ethernet port Count512_1024 bytes packet`|`0x000D1010(RO)`|
|`Receive Ethernet port Count512_1024 bytes packet`|`0x000D1810(RC)`|
|`Receive Ethernet port Count1025_1528 bytes packet`|`0x000D1014(RO)`|
|`Receive Ethernet port Count1025_1528 bytes packet`|`0x000D1814(RC)`|
|`Receive Ethernet port Count1529_2047 bytes packet`|`0x000D1018(RO)`|
|`Receive Ethernet port Count1529_2047 bytes packet`|`0x000D1818(RC)`|
|`Receive Ethernet port Count Jumbo packet`|`0x000D101C(RO)`|
|`Receive Ethernet port Count Jumbo packet`|`0x000D181C(RC)`|
|`Receive Ethernet port Count Unicast packet`|`0x000D1020(RO)`|
|`Receive Ethernet port Count Unicast packet`|`0x000D1820(RC)`|
|`Receive Ethernet port Count Total packet`|`0x000D1024(RO)`|
|`Receive Ethernet port Count Total packet`|`0x000D1824(RC)`|
|`Receive Ethernet port Count Broadcast packet`|`0x000D1028(RO)`|
|`Receive Ethernet port Count Broadcast packet`|`0x000D1828(RC)`|
|`Receive Ethernet port Count Multicast packet`|`0x000D102C(RO)`|
|`Receive Ethernet port Count Multicast packet`|`0x000D182C(RC)`|
|`Receive Ethernet port Count Under size packet`|`0x000D1030(RO)`|
|`Receive Ethernet port Count Under size packet`|`0x000D1830(RC)`|
|`Receive Ethernet port Count Over size packet`|`0x000D1034(RO)`|
|`Receive Ethernet port Count Over size packet`|`0x000D1834(RC)`|
|`Receive Ethernet port Count FCS error packet`|`0x000D1038(RO)`|
|`Receive Ethernet port Count FCS error packet`|`0x000D1838(RC)`|
|`Receive Ethernet port Count number of bytes of packet`|`0x000D103C(RO)`|
|`Receive Ethernet port Count number of bytes of packet`|`0x000D183C(RC)`|
|`Classify HBCE Looking Up Information Control2`|`0x0040000 - 0x0047FFF`|
|`Classify Per Pseudowire Slice to CDR Control`|`0x00C0000 - 0x00C1FFF`|
|`Classify Hold Register Status`|`0x00000A - 0x00000C`|
|`Read HA Address Bit3_0 Control`|`0xE0000 - 0xE000F`|
|`Read HA Address Bit7_4 Control`|`0xE0010 - 0xE001F`|
|`Read HA Address Bit11_8 Control`|`0xE0020 - 0xE002F`|
|`Read HA Address Bit15_12 Control`|`0xE0030 - 0xE003F`|
|`Read HA Address Bit19_16 Control`|`0xE0040 - 0xE004F`|
|`Read HA Address Bit23_20 Control`|`0xE0050 - 0xE005F`|
|`Read HA Address Bit24 and Data Control`|`0xE0060 - 0xE0061`|
|`Read HA Hold Data63_32`|`0xE0070`|
|`Read HA Hold Data95_64`|`0xE0071`|
|`Read HA Hold Data127_96`|`0xE0072`|
|`Classify Parity Register Control`|`0x000F8000`|
|`Classify Parity Disable register Control`|`0x000F8001`|
|`Classify Parity sticky error`|`0x000F8002`|
|`Receive Ethernet port Count PSN error packet`|`0x00000005(RO)`|
|`Receive Ethernet port Count PSN error packet`|`0x00000006(RC)`|
|`Receive Ethernet port Count PSN error packet`|`0x00000007(RO)`|
|`Receive Ethernet port Count PSN error packet`|`0x00000008(RC)`|


###Classify Global PSN Control

* **Description**           

This register configures identification per Ethernet port


* **RTL Instant Name**    : `cla_glb_psn`

* **Address**             : `0x00000000 - 0x00000003`

* **Formula**             : `0x00000000 +  eth_port`

* **Where**               : 

    * `$eth_port(0-3): Ethernet port ID`

* **Width**               : `80`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[79]`|`rxethchksumen`| Enable checksum checking for IPv4/6| `RW`| `0x0`| `0x0`|
|`[78]`|`rxethtypedis`| Disable check ETH type| `RW`| `0x0`| `0x0`|
|`[77:30]`|`rxpsndaexp`| Mac Address expected at Rx Ethernet port| `RW`| `0x0`| `0x0`|
|`[29]`|`rxsendlbit2cdren`| Enable to send Lbit to CDR engine 1: Enable to send Lbit 0: Disable| `RW`| `0x1`| `0x0`|
|`[28]`|`rxpsnmplsoutlabelchecken`| Enable to check MPLS outer 1: Enable checking 0: Disable checking| `RW`| `0x0`| `0x0`|
|`[27]`|`rxpsncpubfdctlen`| Enable VCCV BFD control packet sending to CPU for processing 1: Enable sending 0: Discard| `RW`| `0x0`| `0x0`|
|`[26]`|`rxmaccheckdis`| Disable to check MAC address at Ethernet port receive direction 1: Disable checking 0: Enable checking| `RW`| `0x0`| `0x0`|
|`[25]`|`rxpsncpuicmpen`| Enable ICMP control packet sending to CPU for processing 1: Enable sending 0: Discard| `RW`| `0x0`| `0x0`|
|`[24]`|`rxpsncpuarpen`| Enable ARP control packet sending to CPU for processing 1: Enable sending 0: Discard| `RW`| `0x0`| `0x0`|
|`[23]`|`pweloopclaen`| Enable Loop back traffic from PW Encapsulation to Classification 1: Enable Loop back mode 0: Normal, not loop back| `RW`| `0x0`| `0x0`|
|`[22]`|`rxpsnipudpmode`| This bit is applicable for Ipv4/Ipv6 packet from Ethernet side 1: Classify engine uses RxPsnIpUdpSel to decide which UDP port (Source or Destination) is used to identify pseudowire packet 0: Classify engine will automatically search for value 0x85E in source or destination UDP port. The remaining UDP port is used to identify pseudowire packet| `RW`| `0x0`| `0x0`|
|`[21]`|`rxpsnipudpsel`| This bit is applicable for Ipv4/Ipv6 using to select Source or Destination to identify pseudowire packet from Ethernet side. It is not use when RxPsnIpUdpMode is zero 1: Classify engine selects source UDP port to identify pseudowire packet 0: Classify engine selects destination UDP port to identify pseudowire packet| `RW`| `0x0`| `0x0`|
|`[20]`|`rxpsnipttlchken`| Enable check TTL field in MPLS/Ipv4 or Hop Limit field in Ipv6 1: Enable checking 0: Disable checking| `RW`| `0x0`| `0x0`|
|`[19:0]`|`rxpsnmplsoutlabel`| Received 2-label MPLS packet from PSN side will be discarded when it's outer label is different than RxPsnMplsOutLabel| `RW`| `0x0`| `0x0 End: Begin:`|

###Classify HBCE Hashing Table Control

* **Description**           

HBCE module uses 14 bits for tab-index. Tab-index is generated by hashing function applied to the original id. %%

# HDL_PATH     :rtlclapro.iaf6ces96rtlcla_hcbe.mem_p0hshtabinf.memram.ram.ram[$HashID]

# Hashing function applies an XOR function to all bits of tab-index. %%

# The indexes to the tab-index are generated by hashing function and therefore collisions may occur. %%

# There are maximum fours (4) entries for every hash to identify flow traffic whether match or not. %%

# If the collisions are more than fours (4), they are handled by pointer to link another memory. %%

# The formula of hash pattern is {label ID(20bits), PSN mode (2bits), eth_port(2bits)}, call HashPattern (24bits)%%

# The HashID formula has two case depend on CLAHbceCodingSelectedMode%%

# CLAHbceCodingSelectedMode = 1: HashID = HashPattern[14:0] XOR {6'd0,HashPattern[23:15]}, CLAHbceStoreID[8:0] = HashPattern[23:15]%%

# CLAHbceCodingSelectedMode = 0: HashID = HashPattern[14:0], CLAHbceStoreID = HashPattern[23:15]

# #CLAHbceCodingSelectedMode = 1: HashID = HashPattern[13:0] XOR {4'd0,HashPattern[23:14]}, CLAHbceStoreID = HashPattern[23:14]%%

# #CLAHbceCodingSelectedMode = 0: HashID = HashPattern[13:0], CLAHbceStoreID = HashPattern[23:14]


* **RTL Instant Name**    : `cla_hbce_hash_table`

* **Address**             : `0x00020000 - 0x00023FFF`

* **Formula**             : `0x00020000 + HashID`

* **Where**               : 

    * `$HashID(0-32768): HashID`

* **Width**               : `13`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[60]`|`rxethpwonflyvc34`| CEP EBM on-fly fractional, length changed| `RW`| `0x0`| `0x0`|
|`[59:46]`|`rxethpwlen`| length of packet to check malform| `RW`| `0x0`| `0x0`|
|`[45]`|`rxethsupen`| Suppress Enable| `RW`| `0x0`| `0x0`|
|`[44]`|`rxethcepmode`| CEP mode working 0,1: CEP basic| `RW`| `0x0`| `0x0`|
|`[43:12]`|`rxethrtpssrcvalue`| This value is used to compare with SSRC value in RTP header of received TDM PW packets| `RW`| `0x0`| `0x0`|
|`[11:5]`|`rxethrtpptvalue`| This value is used to compare with PT value in RTP header of received TDM PW packets| `RW`| `0x0`| `0x0`|
|`[4]`|`rxethrtpen`| Enable RTP 1: Enable RTP 0: Disable RTP| `RW`| `0x0`| `0x0`|
|`[3]`|`rxethrtpssrcchken`| Enable checking SSRC field of RTP header in received TDM PW packet 1: Enable checking 0: Disable checking| `RW`| `0x0`| `0x0`|
|`[2]`|`rxethrtpptchken`| Enable checking PT field of RTP header in received TDM PW packet 1: Enable checking 0: Disable checking| `RW`| `0x0`| `0x0`|
|`[70:51]`|`rxpwmefomplsecidvalue`| ECID value used to compare with ECID in Ethernet MEF packet| `RW`| `0x0`| `0x0`|
|`[50:3]`|`rxpwmefomplsdavalue`| DA value used to compare with DA in Ethernet MEF packet| `RW`| `0x0`| `0x0`|
|`[Bit:Bit]`|`name`| Description| `Type`| `SW_Reset`| `HW_Reset`|
|`[12]`|`clahbcelink`| this is pointer to link extra location for conflict hash more than fours #		1: Link to another memory #		0: not link more| `RW`| `0x0`| `0x0`|
|`[11:0]`|`clahbcememoryextrastartpointer`| this is a MemExtraPtr to read the Classify HBCE Looking Up Information Extra Control in case the number of collisions are more than 4| `RW`| `0x0`| `0x0 #End: Begin:`|

###Classify Per Group Enable Control

* **Description**           

This register configures Group that Flowid (or Pseudowire ID) is enable or not

# HDL_PATH     :rtlclapro.iaf6ces96rtlcla_hcbe.irampwgrp.ram.ram[$Grp_ID]


* **RTL Instant Name**    : `cla_per_grp_enb`

* **Address**             : `0x00070000 - 0x00071FFF`

* **Formula**             : `0x00070000 + Working_ID*0x1000 + Grp_ID`

* **Where**               : 

    * `$Working_ID(0-1): CLAHbceGrpWorking`

    * `$Grp_ID(0-4095): CLAHbceGrpIDFlow`

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[71:58]`|`clahbceflowid2`| This is PW ID identification| `RW`| `0x0`| `0x0`|
|`[57]`|`clahbceflowenb2`| The flow is identified in this table, it mean the traffic is identified 1: Flow identified 0: Flow look up fail| `RW`| `0x0`| `0x0`|
|`[56:48]`|`clahbcestoreid2`| this is saving some additional information to identify a certain lookup address rule within a particular table entry HBCE and also to be able to distinguish those that collide| `RW`| `0x0`| `0x0`|
|`[47:34]`|`clahbceflowid1`| This is PW ID identification| `RW`| `0x0`| `0x0`|
|`[33]`|`clahbceflowenb1`| The flow is identified in this table, it mean the traffic is identified 1: Flow identified 0: Flow look up fail| `RW`| `0x0`| `0x0`|
|`[32:24]`|`clahbcestoreid1`| this is saving some additional information to identify a certain lookup address rule within a particular table entry HBCE and also to be able to distinguish those that collide| `RW`| `0x0`| `0x0`|
|`[23:10]`|`clahbceflowid0`| This is PW ID identification| `RW`| `0x0`| `0x0`|
|`[9]`|`clahbceflowenb0`| The flow is identified in this table, it mean the traffic is identified 1: Flow identified 0: Flow look up fail| `RW`| `0x0`| `0x0`|
|`[Bit:Bit]`|`name`| Description| `Type`| `SW_Reset`| `HW_Reset`|
|`[0]`|`clagrppwen`| This indicate the FlowID (or Pseudowire ID) is enable or not #		1: FlowID enable #		0: FlowID disable| `RW`| `0x0`| `0x0 # End: Begin:`|

###Classify Per Ethernet port Enable Control

* **Description**           

This register configures specific Ethernet port is enable or not


* **RTL Instant Name**    : `cla_per_eth_enb`

* **Address**             : `0x000D0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3]`|`claethport4en`| This indicate Ethernet port 4 is enable or not 1: enable 0: disable| `RW`| `0x0`| `0x0`|
|`[2]`|`claethport3en`| This indicate Ethernet port 3 is enable or not 1: enable 0: disable| `RW`| `0x0`| `0x0`|
|`[1]`|`claethport2en`| This indicate Ethernet port 2 is enable or not 1: enable 0: disable| `RW`| `0x0`| `0x0`|
|`[0]`|`claethport1en`| This indicate Ethernet port 1 is enable or not 1: enable 0: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count0_64 bytes packet

* **Description**           

This register is statistic counter for the packet having 0 to 64 bytes


* **RTL Instant Name**    : `Eth_cnt0_64_ro`

* **Address**             : `0x000D1000(RO)`

* **Formula**             : `0x000D1000 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt0_64`| This is statistic counter for the packet having 0 to 64 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count0_64 bytes packet

* **Description**           

This register is statistic counter for the packet having 0 to 64 bytes


* **RTL Instant Name**    : `Eth_cnt0_64_rc`

* **Address**             : `0x000D1800(RC)`

* **Formula**             : `0x000D1800 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt0_64`| This is statistic counter for the packet having 0 to 64 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count65_127 bytes packet

* **Description**           

This register is statistic counter for the packet having 65 to 127 bytes


* **RTL Instant Name**    : `Eth_cnt65_127_ro`

* **Address**             : `0x000D1004(RO)`

* **Formula**             : `0x000D1004 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt65_127`| This is statistic counter for the packet having 65 to 127 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count65_127 bytes packet

* **Description**           

This register is statistic counter for the packet having 65 to 127 bytes


* **RTL Instant Name**    : `Eth_cnt65_127_rc`

* **Address**             : `0x000D1804(RC)`

* **Formula**             : `0x000D1804 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt65_127`| This is statistic counter for the packet having 65 to 127 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count128_255 bytes packet

* **Description**           

This register is statistic counter for the packet having 128 to 255 bytes


* **RTL Instant Name**    : `Eth_cnt128_255_ro`

* **Address**             : `0x000D1008(RO)`

* **Formula**             : `0x000D1008 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt128_255`| This is statistic counter for the packet having 128 to 255 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count128_255 bytes packet

* **Description**           

This register is statistic counter for the packet having 128 to 255 bytes


* **RTL Instant Name**    : `Eth_cnt128_255_rc`

* **Address**             : `0x000D1808(RC)`

* **Formula**             : `0x000D1808 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt128_255`| This is statistic counter for the packet having 128 to 255 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count256_511 bytes packet

* **Description**           

This register is statistic counter for the packet having 256 to 511 bytes


* **RTL Instant Name**    : `Eth_cnt256_511_ro`

* **Address**             : `0x000D100C(RO)`

* **Formula**             : `0x000D100C + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt256_511`| This is statistic counter for the packet having 256 to 511 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count256_511 bytes packet

* **Description**           

This register is statistic counter for the packet having 256 to 511 bytes


* **RTL Instant Name**    : `Eth_cnt256_511_rc`

* **Address**             : `0x000D180C(RC)`

* **Formula**             : `0x000D180C + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt256_511`| This is statistic counter for the packet having 256 to 511 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count512_1024 bytes packet

* **Description**           

This register is statistic counter for the packet having 512 to 1023 bytes


* **RTL Instant Name**    : `Eth_cnt512_1024_ro`

* **Address**             : `0x000D1010(RO)`

* **Formula**             : `0x000D1010 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt512_1024`| This is statistic counter for the packet having 512 to 1023 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count512_1024 bytes packet

* **Description**           

This register is statistic counter for the packet having 512 to 1023 bytes


* **RTL Instant Name**    : `Eth_cnt512_1024_rc`

* **Address**             : `0x000D1810(RC)`

* **Formula**             : `0x000D1810 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt512_1024`| This is statistic counter for the packet having 512 to 1023 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count1025_1528 bytes packet

* **Description**           

This register is statistic counter for the packet having 1024 to 1518 bytes


* **RTL Instant Name**    : `Eth_cnt1025_1528_ro`

* **Address**             : `0x000D1014(RO)`

* **Formula**             : `0x000D1014 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt1025_1528`| This is statistic counter for the packet having 1024 to 1518 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count1025_1528 bytes packet

* **Description**           

This register is statistic counter for the packet having 1024 to 1518 bytes


* **RTL Instant Name**    : `Eth_cnt1025_1528_rc`

* **Address**             : `0x000D1814(RC)`

* **Formula**             : `0x000D1814 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt1025_1528`| This is statistic counter for the packet having 1024 to 1518 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count1529_2047 bytes packet

* **Description**           

This register is statistic counter for the packet having 1519 to 2047 bytes


* **RTL Instant Name**    : `Eth_cnt1529_2047_ro`

* **Address**             : `0x000D1018(RO)`

* **Formula**             : `0x000D1018 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt1529_2047`| This is statistic counter for the packet having 1519 to 2047 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count1529_2047 bytes packet

* **Description**           

This register is statistic counter for the packet having 1519 to 2047 bytes


* **RTL Instant Name**    : `Eth_cnt1529_2047_rc`

* **Address**             : `0x000D1818(RC)`

* **Formula**             : `0x000D1818 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt1529_2047`| This is statistic counter for the packet having 1519 to 2047 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Jumbo packet

* **Description**           

This register is statistic counter for the packet having more than 2048 bytes (jumbo)


* **RTL Instant Name**    : `Eth_cnt_jumbo_ro`

* **Address**             : `0x000D101C(RO)`

* **Formula**             : `0x000D101C + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntjumbo`| This is statistic counter for the packet more than 2048 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Jumbo packet

* **Description**           

This register is statistic counter for the packet having more than 2048 bytes (jumbo)


* **RTL Instant Name**    : `Eth_cnt_jumbo_rc`

* **Address**             : `0x000D181C(RC)`

* **Formula**             : `0x000D181C + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntjumbo`| This is statistic counter for the packet more than 2048 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Unicast packet

* **Description**           

This register is statistic counter for the unicast packet


* **RTL Instant Name**    : `Eth_cnt_Unicast_ro`

* **Address**             : `0x000D1020(RO)`

* **Formula**             : `0x000D1020 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntunicast`| This is statistic counter for the unicast packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Unicast packet

* **Description**           

This register is statistic counter for the unicast packet


* **RTL Instant Name**    : `Eth_cnt_Unicast_rc`

* **Address**             : `0x000D1820(RC)`

* **Formula**             : `0x000D1820 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntunicast`| This is statistic counter for the unicast packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Total packet

* **Description**           

This register is statistic counter for the total packet at receive side


* **RTL Instant Name**    : `rx_port_pkt_cnt_ro`

* **Address**             : `0x000D1024(RO)`

* **Formula**             : `0x000D1024 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnttotal`| This is statistic counter total packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Total packet

* **Description**           

This register is statistic counter for the total packet at receive side


* **RTL Instant Name**    : `rx_port_pkt_cnt_rc`

* **Address**             : `0x000D1824(RC)`

* **Formula**             : `0x000D1824 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnttotal`| This is statistic counter total packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Broadcast packet

* **Description**           

This register is statistic counter for the Broadcast packet at receive side


* **RTL Instant Name**    : `rx_port_bcast_pkt_cnt_ro`

* **Address**             : `0x000D1028(RO)`

* **Formula**             : `0x000D1028 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntbroadcast`| This is statistic counter broadcast packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Broadcast packet

* **Description**           

This register is statistic counter for the Broadcast packet at receive side


* **RTL Instant Name**    : `rx_port_bcast_pkt_cnt_rc`

* **Address**             : `0x000D1828(RC)`

* **Formula**             : `0x000D1828 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntbroadcast`| This is statistic counter broadcast packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Multicast packet

* **Description**           

This register is statistic counter for the Multicast packet at receive side


* **RTL Instant Name**    : `rx_port_mcast_pkt_cnt_ro`

* **Address**             : `0x000D102C(RO)`

* **Formula**             : `0x000D102C + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntmulticast`| This is statistic counter multicast packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Multicast packet

* **Description**           

This register is statistic counter for the Multicast packet at receive side


* **RTL Instant Name**    : `rx_port_mcast_pkt_cnt_rc`

* **Address**             : `0x000D182C(RC)`

* **Formula**             : `0x000D182C + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntmulticast`| This is statistic counter multicast packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Under size packet

* **Description**           

This register is statistic counter for the under size packet at receive side


* **RTL Instant Name**    : `rx_port_under_size_pkt_cnt_ro`

* **Address**             : `0x000D1030(RO)`

* **Formula**             : `0x000D1030 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntundersize`| This is statistic counter under size packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Under size packet

* **Description**           

This register is statistic counter for the under size packet at receive side


* **RTL Instant Name**    : `rx_port_under_size_pkt_cnt_rc`

* **Address**             : `0x000D1830(RC)`

* **Formula**             : `0x000D1830 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntundersize`| This is statistic counter under size packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Over size packet

* **Description**           

This register is statistic counter for the over size packet at receive side


* **RTL Instant Name**    : `rx_port_over_size_pkt_cnt_ro`

* **Address**             : `0x000D1034(RO)`

* **Formula**             : `0x000D1034 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntoversize`| This is statistic counter over size packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count Over size packet

* **Description**           

This register is statistic counter for the over size packet at receive side


* **RTL Instant Name**    : `rx_port_over_size_pkt_cnt_rc`

* **Address**             : `0x000D1834(RC)`

* **Formula**             : `0x000D1834 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntoversize`| This is statistic counter over size packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count FCS error packet

* **Description**           

This register is statistic count FCS error packet at receive side


* **RTL Instant Name**    : `Eth_cnt_phy_err_ro`

* **Address**             : `0x000D1038(RO)`

* **Formula**             : `0x000D1038 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntphyerr`| This is statistic counter FCS error packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count FCS error packet

* **Description**           

This register is statistic count FCS error packet at receive side


* **RTL Instant Name**    : `Eth_cnt_phy_err_rc`

* **Address**             : `0x000D1838(RC)`

* **Formula**             : `0x000D1838 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntphyerr`| This is statistic counter FCS error packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count number of bytes of packet

* **Description**           

This register is statistic count number of bytes of packet at receive side


* **RTL Instant Name**    : `Eth_cnt_byte_ro`

* **Address**             : `0x000D103C(RO)`

* **Formula**             : `0x000D103C + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntbyte`| This is statistic counter number of bytes of packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count number of bytes of packet

* **Description**           

This register is statistic count number of bytes of packet at receive side


* **RTL Instant Name**    : `Eth_cnt_byte_rc`

* **Address**             : `0x000D183C(RC)`

* **Formula**             : `0x000D183C + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Receive Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntbyte`| This is statistic counter number of bytes of packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Classify HBCE Looking Up Information Control2

* **Description**           

This memory contain maximum 8 entries extra to examine one Pseudowire label whether match or not


* **RTL Instant Name**    : `cla_hbce_lkup_info_extra`

* **Address**             : `0x0040000 - 0x0047FFF`

* **Formula**             : `0x0040000 + CollisHashIDExtra*0x1000  + MemExtraPtr`

* **Where**               : 

    * `$MemExtraPtr(0-4095): MemExtraPtr`

    * `$CollisHashIDExtra (0-7): Collision`

* **Width**               : `38`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[37]`|`claextrahbceflowdirect`| this configure FlowID working in normal mode (not in any group)| `RW`| `0x0`| `0x0`|
|`[36]`|`claextrahbcegrpworking`| this configure group working or protection| `RW`| `0x0`| `0x0`|
|`[35:24]`|`claextrahbcegrpidflow`| this configure a group ID that FlowID following| `RW`| `0x0`| `0x0`|
|`[23:11]`|`claextrahbceflowid`| This is PW ID identification| `RW`| `0x0`| `0x0`|
|`[10]`|`claextrahbceflowenb`| The flow is identified in this table, it mean the traffic is identified 1: Flow identified 0: Flow look up fail| `RW`| `0x0`| `0x0`|
|`[9:0]`|`claextrahbcestoreid`| this is saving some additional information to identify a certain lookup address rule within a particular table entry HBCE and also to be able to distinguish those that collide| `RW`| `0x0`| `0x0 End: Begin:`|

###Classify Per Pseudowire Slice to CDR Control

* **Description**           

This register configures Slice ID to CDR


* **RTL Instant Name**    : `cla_2_cdr_cfg`

* **Address**             : `0x00C0000 - 0x00C1FFF`

* **Formula**             : `0x00C0000 +  PWID`

* **Where**               : 

    * `$PWID(0-10752): Pseudowire ID`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`pwcdren`| Indicate Pseudowire enable| `RW`| `0x0`| `0x0 1: Enable 0: Disable`|
|`[14]`|`reserved`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:11]`|`pwholooc48id`| Indicate 8x Hi order OC48| `RW`| `0x0`| `0x0`|
|`[10:0]`|`pwtdmlineid`| The formular of this TdmLineId is mastersts*32+vtid| `RW`| `0x0`| `0x0 End: Begin:`|

###Classify Hold Register Status

* **Description**           

This register using for hold remain that more than 128bits


* **RTL Instant Name**    : `cla_hold_status`

* **Address**             : `0x00000A - 0x00000C`

* **Formula**             : `0x00000A +  HID`

* **Where**               : 

    * `$HID(0-2): Hold ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pwholdstatus`| Hold 32bits| `RW`| `0x0`| `0x0 End: Begin:`|

###Read HA Address Bit3_0 Control

* **Description**           

This register is used to send HA read address bit3_0 to HA engine


* **RTL Instant Name**    : `rdha3_0_control`

* **Address**             : `0xE0000 - 0xE000F`

* **Formula**             : `0xE0000 + HaAddr3_0`

* **Where**               : 

    * `$HaAddr3_0(0-15): HA Address Bit3_0`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr3_0`| Read value will be 0xE00000 plus HaAddr3_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit7_4 Control

* **Description**           

This register is used to send HA read address bit7_4 to HA engine


* **RTL Instant Name**    : `rdha7_4_control`

* **Address**             : `0xE0010 - 0xE001F`

* **Formula**             : `0xE0010 + HaAddr7_4`

* **Where**               : 

    * `$HaAddr7_4(0-15): HA Address Bit7_4`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr7_4`| Read value will be 0xE00000 plus HaAddr7_4| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit11_8 Control

* **Description**           

This register is used to send HA read address bit11_8 to HA engine


* **RTL Instant Name**    : `rdha11_8_control`

* **Address**             : `0xE0020 - 0xE002F`

* **Formula**             : `0xE0020 + HaAddr11_8`

* **Where**               : 

    * `$HaAddr11_8(0-15): HA Address Bit11_8`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr11_8`| Read value will be 0xE00000 plus HaAddr11_8| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit15_12 Control

* **Description**           

This register is used to send HA read address bit15_12 to HA engine


* **RTL Instant Name**    : `rdha15_12_control`

* **Address**             : `0xE0030 - 0xE003F`

* **Formula**             : `0xE0030 + HaAddr15_12`

* **Where**               : 

    * `$HaAddr15_12(0-15): HA Address Bit15_12`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr15_12`| Read value will be 0xE00000 plus HaAddr15_12| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit19_16 Control

* **Description**           

This register is used to send HA read address bit19_16 to HA engine


* **RTL Instant Name**    : `rdha19_16_control`

* **Address**             : `0xE0040 - 0xE004F`

* **Formula**             : `0xE0040 + HaAddr19_16`

* **Where**               : 

    * `$HaAddr19_16(0-15): HA Address Bit19_16`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr19_16`| Read value will be 0xE00000 plus HaAddr19_16| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit23_20 Control

* **Description**           

This register is used to send HA read address bit23_20 to HA engine


* **RTL Instant Name**    : `rdha23_20_control`

* **Address**             : `0xE0050 - 0xE005F`

* **Formula**             : `0xE0050 + HaAddr23_20`

* **Where**               : 

    * `$HaAddr23_20(0-15): HA Address Bit23_20`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr23_20`| Read value will be 0xE00000 plus HaAddr23_20| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit24 and Data Control

* **Description**           

This register is used to send HA read address bit24 to HA engine to read data


* **RTL Instant Name**    : `rdha24data_control`

* **Address**             : `0xE0060 - 0xE0061`

* **Formula**             : `0xE0060 + HaAddr24`

* **Where**               : 

    * `$HaAddr24(0-1): HA Address Bit24`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata31_0`| HA read data bit31_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data63_32

* **Description**           

This register is used to read HA dword2 of data.


* **RTL Instant Name**    : `rdha_hold63_32`

* **Address**             : `0xE0070`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata63_32`| HA read data bit63_32| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data95_64

* **Description**           

This register is used to read HA dword3 of data.


* **RTL Instant Name**    : `rdindr_hold95_64`

* **Address**             : `0xE0071`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata95_64`| HA read data bit95_64| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data127_96

* **Description**           

This register is used to read HA dword4 of data.


* **RTL Instant Name**    : `rdindr_hold127_96`

* **Address**             : `0xE0072`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata127_96`| HA read data bit127_96| `RW`| `0xx`| `0xx End: Begin:`|

###Classify Parity Register Control

* **Description**           

This register using for Force Parity


* **RTL Instant Name**    : `cla_Parity_control`

* **Address**             : `0x000F8000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `17`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16]`|`claforceerr7`| Enable parity error force for "Classify Per Pseudowire Identification to CDR Control"| `RW`| `0x0`| `0x0`|
|`[15]`|`claforceerr6`| Enable parity error force for "Classify HBCE Hashing Table Control"| `RW`| `0x0`| `0x0`|
|`[14:7]`|`claforceerr5`| Enable parity error force for "Classify HBCE Looking Up Information Control2"| `RW`| `0x0`| `0x0`|
|`[6]`|`claforceerr4`| Enable parity error force for "Classify Per Group Enable Control"| `RW`| `0x0`| `0x0`|
|`[5]`|`claforceerr3`| Enable parity error force for "Classify Per Pseudowire MEF Over MPLS Control"| `RW`| `0x0`| `0x0`|
|`[4]`|`claforceerr2`| Enable parity error force for "Classify Per Pseudowire Type Control"| `RW`| `0x0`| `0x0`|
|`[3:0]`|`claforceerr1`| Enable parity error force for "Classify HBCE Looking Up Information Control"| `RW`| `0x0`| `0x0 1: enable force 0: not force End: Begin:`|

###Classify Parity Disable register Control

* **Description**           

This register using for Disable Parity


* **RTL Instant Name**    : `cla_Parity_Disable_control`

* **Address**             : `0x000F8001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `17`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16]`|`cladischkerr7`| Disable parity error check for "Classify Per Pseudowire Identification to CDR Control"| `RW`| `0x0`| `0x0`|
|`[15]`|`cladischkerr6`| Disable parity error check for "Classify HBCE Hashing Table Control"| `RW`| `0x0`| `0x0`|
|`[14:7]`|`cladischkerr5`| Disable parity error check for "Classify HBCE Looking Up Information Control2"| `RW`| `0x0`| `0x0`|
|`[6]`|`cladischkerr4`| Disable parity error check for "Classify Per Group Enable Control"| `RW`| `0x0`| `0x0`|
|`[5]`|`cladischkerr3`| Disable parity error check for "Classify Per Pseudowire MEF Over MPLS Control"| `RW`| `0x0`| `0x0`|
|`[4]`|`cladischkerr2`| Disable parity error check for "Classify Per Pseudowire Type Control"| `RW`| `0x0`| `0x0`|
|`[3:0]`|`cladischkerr1`| Disable parity error check for "Classify HBCE Looking Up Information Control"| `RW`| `0x0`| `0x0 1: Disable error check 0: Enable error check End: Begin:`|

###Classify Parity sticky error

* **Description**           

This register using for checking sticky error


* **RTL Instant Name**    : `cla_Parity_stk_err`

* **Address**             : `0x000F8002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `17`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16]`|`claparstkerr7`| Parity sticky error check for "Classify Per Pseudowire Identification to CDR Control"| `W1C`| `0x0`| `0x0`|
|`[15]`|`claparstkerr6`| Parity sticky error check for "Classify HBCE Hashing Table Control"| `W1C`| `0x0`| `0x0`|
|`[14:7]`|`claparstkerr5`| Parity sticky error check for "Classify HBCE Looking Up Information Control2"| `W1C`| `0x0`| `0x0`|
|`[6]`|`claparstkerr4`| Parity sticky error check for "Classify Per Group Enable Control"| `W1C`| `0x0`| `0x0`|
|`[5]`|`claparstkerr3`| Parity sticky error check for "Classify Per Pseudowire MEF Over MPLS Control"| `W1C`| `0x0`| `0x0`|
|`[4]`|`claparstkerr2`| Parity sticky error check for "Classify Per Pseudowire Type Control"| `W1C`| `0x0`| `0x0`|
|`[3:0]`|`claparstkerr1`| Parity sticky error check for "Classify HBCE Looking Up Information Control"| `W1C`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count PSN error packet

* **Description**           

This register is statistic count PSN error packet at receive side


* **RTL Instant Name**    : `Eth_cnt_psn_err1_ro`

* **Address**             : `0x00000005(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntpsnerr1`| This is statistic counter PSN error packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count PSN error packet

* **Description**           

This register is statistic count PSN error packet at receive side


* **RTL Instant Name**    : `Eth_cnt_psn_err1_rc`

* **Address**             : `0x00000006(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntpsnerr1`| This is statistic counter PSN error packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Receive Ethernet port Count PSN error packet

* **Description**           

This register is statistic count PSN error packet at receive side


* **RTL Instant Name**    : `Eth_cnt_psn_err2_ro`

* **Address**             : `0x00000007(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntpsnerr2`| This is statistic counter PSN error packet| `RO`| `0x0`| `0x0 End:`|

###Receive Ethernet port Count PSN error packet

* **Description**           

This register is statistic count PSN error packet at receive side


* **RTL Instant Name**    : `Eth_cnt_psn_err2_rc`

* **Address**             : `0x00000008(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntpsnerr2`| This is statistic counter PSN error packet| `RO`| `0x0`| `0x0 End:`|

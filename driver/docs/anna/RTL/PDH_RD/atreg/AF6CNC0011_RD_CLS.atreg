######################################################################################
# LIU port
# 83-00: E1/DE1 Port
# 51-28: E3/DS3/EC1 Port

######################################################################################


######################################################################################
#    *************
#   * 1.1. Ethernet DA & SA  bit47_32 Configuration *
#    *************
// Begin:
// Register Full Name: Ethernet DA bit47_32 Configuration
// RTL Instant Name  : upen_dasa
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x_0000
// Formula      : {N/A}
// Description  : This register  is used to configure the DA bit47_32
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] 	%% Name 			%% Description 						%% Type 			%% Reset 			%% Default
// 	Field: [31:16]  	%% DA_bit47_32   	%% DA bit47_32 		  				%% R/W 				%% 0xCAFE 			%% 0xCAFE  
// 	Field: [15:00]  	%% SA_bit47_32   	%% SA bit47_32 		  				%% R/W 				%% 0xABCD 			%% 0xABCD  
// End:

######################################################################################
#    *************
#   * 1.2. Ethernet DA bit31_00 Configuration *
#    *************
// Begin:
// Register Full Name: Ethernet DA bit31_00 Configuration
// RTL Instant Name  : upen_da31
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x_0001
// Formula      : {N/A}
// Description  : This register  is used to configure the DA bit32_00 
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] 	%% Name 			%% Description 						%% Type 			%% Reset 			%% Default
// 	Field: [31:00]  	%% DA_bit31_00   	%% DA bit31_00 		  				%% R/W 				%% 0xCAFE_CAFE 		%% 0xCAFE_CAFE  
// End:

######################################################################################
#    *************
#   * 1.3. Ethernet SA bit31_00 Configuration *
#    *************
// Begin:
// Register Full Name: Ethernet SA bit31_00 Configuration
// RTL Instant Name  : upen_sa31
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x_0002
// Formula      : {N/A}
// Description  : This register  is used to configure the SA bit31_00 
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] 	%% Name 			%% Description 						%% Type 			%% Reset 			%% Default
// 	Field: [31:00]  	%% SA_bit31_00   	%% SA bit31_00 		  				%% R/W 				%% 0xABCD_ABCD 		%% 0xABCD_ABCD  
// End:

######################################################################################
#    *************
#   * 1.4. Ethernet E-type  Configuration *
#    *************
// Begin:
// Register Full Name: Ethernet Sub_Type of packet control Configuration
// RTL Instant Name  : upen_type
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x_0003
// Formula      : {N/A}
// Description  : This register  is used to configure E-type and Sub_Type
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] 	%% Name 			%% Description 						%% Type 			%% Reset 			%% Default
//  Field: [31:16]   	%% Unused    		%% Unsued 					        %% R/W            	%% 0x0      		%% 0x0
// 	Field: [15:00]  	%% E_type 		   	%% E_type of dat pkt	 			%% R/W 				%% 0x88E4 			%% 0x88E4  
// End:

######################################################################################
#    *************
#   * 1.5. Classified Ethernet field mask & Lengh pkt Configuration *
#    *************
// Begin:
// Register Full Name:  Classified Ethernet field mask & Lengh pkt  Configuration
// RTL Instant Name  : upen_mask
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x_0004
// Formula      : {N/A}
// Description  : This register  is used to mask some field of packet. These mask field are checked to discard or forward pkt 
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] 	%% Name 			%% Description 						%% Type 			%% Reset 			%% Default
// 	Field: [31:16]  	%% Eth_len   		%% Len field , same as pdhmux cfg	%% R/W 				%% 0xFE 			%% 0xFE
// 	Field: [15:14]  	%% cbchk_mode 		%% Count bytes chk compare with length as mode , only use for RTL self-test
												00 		: use length cfg 
											    Others 	: use length calc by RTL	
																				%% R/W 				%% 0x0 				%% 0x0
// 	Field: [13:12]  	%% Seqerr_force		%% only use for RTL self-test 		%% R/W 				%% 0x0 				%% 0x0

// 	Field: [06:06]  	%% default_out		%% Default output in etype check disable case
											   CLS 2.5G : 0 : forward to MAC 1G   , 1 : forward to PDH MUX		
											   CLS   1G : 0 : forward to MAC 2.5G , 1 : forward to xxx (reserved)	
																				%% R/W 				%% 0x0 				%% 0x0
// 	Field: [05:05]  	%% Subtype_mask	%% sub-type check enable 				%% R/W 				%% 0x0 				%% 0x0
// 	Field: [04:04]  	%% Sequence_mask	%% sequence check enable			%% R/W 				%% 0x0 				%% 0x0
// 	Field: [03:03]  	%% Length_mask 		%% length check enable				%% R/W 				%% 0x0 				%% 0x0
// 	Field: [02:02]  	%% Etype_mask 		%% E_type check enable				%% R/W 				%% 0x0 				%% 0x0
// 	Field: [01:01]  	%% SA_mask   		%% SA check enable  				%% R/W 				%% 0x0 				%% 0x0
// 	Field: [00:00]  	%% DA_mask   		%% DA check enable  				%% R/W 				%% 0x0 				%% 0x0
// End:

######################################################################################
#    *************
#   * 2.1. Classified packet counters
#    *************
// Begin:
// Register Full Name: Ethernet Encapsulation EtheType and EtherLength  Configuration
// RTL Instant Name  : upen_count
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x1000 - 0x101F
// Formula      : Address + $ro*16 + $id
// Where        : {$ro(0-1) : ro bit} %% {$id(0-15) : Counter ID}
// Description  : This register is used to read classified packet counters , support both mode r2c and ro
					 Counter ID detail :
						+ 0 	: DS1      packet counter (only count if etype check & subt_ype check anable)
						+ 1 	: DS3      packet counter (only count if etype check & sub_type check anable)
						+ 2 	: Control  packet counter (only count if etype check anable)
						+ 4		: err seq counter		  	
						+ 5		: fcs err counter
						+ 8		: da err packet counter
						+ 9		: sa err  packet counter
						+ 10	: no use
						+ 11	: len err  packet counter
						+ 7		: unused
					 ro			: enable mean read only (not clear)  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] 	%% Name 			%% Description 						%% Type 			%% Reset 			%% Default
// 	Field: [31:0]  		%% Ether_Type   	%% Counter value 		  			%% R/W 				%% 0x0	 			%% 0x0  
// End:

######################################################################################
#    *************
#   * 3.1. Dump packet  Configuration *
#    *************
// Begin:
// Register Full Name:  Dump packet  Configuration
// RTL Instant Name  : upen_dump
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x_0006
// Formula      : {N/A}
// Description  : This register  is used to configure dump packet type , use to dump pkt & debug 
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] 	%% Name 			%% Description 						%% Type 			%% Reset 			%% Default
// 	Field: [31:03]  	%% Unused   		%% Unused			  				%% R/W 				%% 0x0 				%% 0x0
// 	Field: [02:01]  	%% dump_typ 		%% Dump pkt type
												00 : dump pkt input
												01 : dump pkt ds1 output
												10 : dump pkt ds3 output
												11 : dump pkt control output	%% R/W 				%% 0x0 				%% 0x0
// 	Field: [00:00]  	%% dfsop_enb    		%% dump from sop  enable  		%% R/W 				%% 0x0 				%% 0x0
// End:

######################################################################################
#    *************
#   * 3.2. Dump packet data
#    *************
// Begin:
// Register Full Name:  Dump packet data 
// RTL Instant Name  : upen_dbpkt
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x2000 - 0x2FFF
// Formula      : Address + $id
// Where        :  {$id(0-4095) : Counter ID}
// Description  : This register is used to read data of dump packets,use to debug

// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] 	%% Name 			%% Description 						%% Type 			%% Reset 			%% Default
// 	Field: [31:0]  		%% pkt_dat 		  	%% Data of dump packet 	  			%% R/W 				%% 0x0	 			%% 0x0  
// End:

######################################################################################
#    *************
#   * 4.1.Classified Sticky Debug 
#    *************
// Begin:
// Register Full Name: Debug Stick 0 
// RTL Instant Name  : upen_stk
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x0007
// Formula: {N/A} 
// Description  :  This sticky is used to debug. Write 0xFFFF_FFFF to clear
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] 	%% Name 			 %% Description 					%% Type 			%% Reset 			%% Default
//  Field: [31:14]   	%% Unused    		 %% Unsued 					        %% R/W            	%% 0x0      		%% 0x0
// 	Field: [13:13]  	%% Len_err_stk		 %% Length error sticky				%% R/W 				%% 0x0 				%% 0x0  
// 	Field: [12:12]  	%% Etp_err_stk 		 %% E-type error sticky				%% R/W 				%% 0x0 				%% 0x0
  
// 	Field: [11:11]  	%%  Sa_err_stk 		 %% SA Cnt error sticky				%% R/W 				%% 0x0 				%% 0x0  
// 	Field: [10:10]  	%%  Da_err_stk 		 %% DA Cnt error sticky				%% R/W 				%% 0x0 				%% 0x0  
// 	Field: [09:09]  	%% Fcn_err_stk 		 %% Fcs Cnt  error sticky			%% R/W 				%% 0x0 				%% 0x0  
// 	Field: [08:08]  	%% Seq_err_stk 		 %% Sequence error sticky			%% R/W 				%% 0x0 				%% 0x0  

//  Field: [07:06]   	%% Unused    		 %% Unsued 					        %% R/W            	%% 0x0      		%% 0x0
// 	Field: [05:05]  	%% Ctl_pkt_stk 		 %% Control packet output sticky 	%% R/W 				%% 0x0 				%% 0x0  
// 	Field: [04:04]  	%% Ds3_pkt_stk 		 %% Ds3     packet output sticky 	%% R/W 				%% 0x0 				%% 0x0  

// 	Field: [03:03]  	%% Ds1_pkt_stk 		 %% DS1     packet output sticky 	%% R/W 				%% 0x0 				%% 0x0  
// 	Field: [02:02]  	%% Fcs_pkt_stk 		 %% FCS     packet  input sticky 	%% R/W 				%% 0x0 				%% 0x0  
// 	Field: [01:01]  	%% Eop_vld_stk 		 %% Eop     valid   input sticky 	%% R/W 				%% 0x0 				%% 0x0  
// 	Field: [00:00]  	%% Pkt_vld_stk 		 %% Pkt     valid   input sticky 	%% R/W 				%% 0x0 				%% 0x0  
// End:














##################################################################
# Arrive Technologies
#
# Revision 1.0 - 2013.Mar.23

##################################################################
#This section is for guideline 
#File name: filename.atreg (atreg = ATVN register define)
#Comment out a line: Please use (# or //#) character at the beginning of the line
#Delimiter character: (%%)

##################################################################
#Common rules for register:
#Access types:
#R/W : Read / Write
#R/W/C: Read / Write 1 to clear (also called as sticky bit)
#R_O: Read Only, No clear on read
#R2C: Read Only, Read to Clear
#W_O: write only
#W1C: Read / Write 1 to clear (also called as sticky bit)
##################################################################
#********************* 
#1.0 MDIO ACK 
#********************* 
// Begin:
// Register Full Name: Configure Raise mode for Interrupt Pin 
// RTL Instant Name  : ack_bit
// Address: 0x0
// Formula      : Base_Address + Address
// Where        : 
// Description  : ack bit status this bit set to 1 when Read/Write OP done and will be clear when ACK_CLR bit set
// Width        : 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name            %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:01]    %% unused           %% unused %% RW  %% 0x0 %% 0x0  
// Field: [00]   	 %% ack_bit          %% set to 1 when Read/Write OP done and will be clear when ACK_CLR bit set %% RO  %% 0x0 %% 0x0
// End :

##################################################################
#********************* 
#1.1 MDIO Port Select
#********************* 
// Begin:
// Register Full Name: Config PHY address
// RTL Instant Name  : pcfg_ctl1
// Address: 0x10
// Formula      : Base_Address + Address
// Where        : 
// Description  :This is Config PHY address and Port Select
// Width        : 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name            %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:08]    %% unused           %% unused %% RW  %% 0x0 %% 0x0  
// Field: [07:04]   	 %% md_cs      %% Select MDIO control port 1->4 
//											 0 : Un-Select
//											 1 : Select MDIO P1
//											 2 : Select MDIO P2
//											 3 : Select MDIO P3
//											 4 : Select MDIO P4  %% RW  %% 0x0 %% 0x0
// Field: [03:00]   	 %% md_phyadr  %%  MDIO Device Addresses it is fix 0x1 for Xilinx SGMII Serdes  %% RW  %% 0x0 %% 0x0
// End :
##################################################################
#********************* 
#1.2 MDIO Clear ACK
#********************* 
// Begin:
// Register Full Name: Status of Diagnostic Interrupt
// RTL Instant Name  : md_ack_clr
// Address: 0x11
// Formula      : Base_Address + Address
// Where        : 
// Description  :This is Status of Diagnostic Interrupt 1-2
// Width        : 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name            %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:01]    %% unused           %% unused %% RW  %% 0x0 %% 0x0  
// Field: [00:00]    %% ack_clr       	 %% Write to Clear ACK OP
//											{1} : Clear ACK %% W1C  %% 0x0 %% 0x0
// End :
##################################################################
#********************* 
#1.1 MDIO Port Addr + RW OP
#********************* 
// Begin:
// Register Full Name: Config Reg address
// RTL Instant Name  : pcfg_ctl2
// Address: 0x12
// Formula      : Base_Address + Address
// Where        : 
// Description  :This is Config Reg address and RNW
// Width        : 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name            %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:08]    %% unused           %% unused %% RW  %% 0x0 %% 0x0  
// Field: [06:05]   	 %% md_op      %% OP: operation code
//											 2'b00 : Open
//											 2'b10 : Read Operation
//											 2'b01 : Write Operation  %% RW  %% 0x0 %% 0x0
// Field: [04:00]   	 %% md_regadr  %%  MDIO Register  Addresses  %% RW  %% 0x0 %% 0x0
// End :
##################################################################
#********************* 
#1.1 MDIO Port DATA Read/Write
#********************* 
// Begin:
// Register Full Name: Config Reg address
// RTL Instant Name  : pcfg_ctl3
// Address: 0x20
// Formula      : Base_Address + Address
// Where        : 
// Description  :This is Config Reg address and RNW
// Width        : 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name            %% Description        %% Type %% SW_Reset %% HW_Reset
// Field: [31:16]   	 %% md_data_read  %%  MDIO Data  write to PHY chip  %% RW  %% 0x0 %% 0x0
// Field: [15:00]   	 %% md_data_read  %%  MDIO Data read from PHY chip  %% RO  %% 0x0 %% 0x0
// End :
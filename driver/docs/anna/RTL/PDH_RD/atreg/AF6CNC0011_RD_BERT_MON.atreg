######################################################################################
#| Access Policy| Use Case| Description                | Effect of a Write on Current Field Value  | Effect of a Read on Current Field Value | Read-back Value |
#| ------|-----------|---------------------------------| ------------------------------------------|-----------------------------------------|-----------------|
#| RO    | Status    | Read Only                       | No effect                | No effect            | Current Value |
#| RW    | Config    | Read, Write                     | Changed to written value | No effect            | Current value |
#| RC    | Status    | Read Clears All                 | No effect                | Sets all bits to 0   | Current value |
#| RS    | UNKNOW    | Read Sets All                   | No effect                | Sets all bits to 1   | Current value |
#| WRC   | UNKNOW    | Write, Read Clears All          | Changed to written value | Sets all bits to 0   | Current value |
#| WRS   | UNKNOW    | Write, Read Sets All            | Changed to written value | Sets all bits to 1   | Current value |
#| WC    | Status    | Write Clears All                | Sets all bits to 0       | No effect            | Current value |
#| WS    | Status    | Write Sets All                  | Sets all bits to 1       | No effect            | Current value |
#| WSRC  | UNKNOW    | Write Sets All, Read Clears All | Sets all bits to 1       | Sets all bits to 0   | Current value |
#| WCRS  | UNKNOW    | Write Clears All,Read Sets All  | Sets all bits to 0       | Sets all bits to 1   | Current value |
#| W1C   | Interrupt | Write 1 to Clear                | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current Value |
#| W1S   | Interrupt | Write 1 to Set                  | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W1T   | UNKNOW    | Write 1 to Toggle               | If the bit in the wr value is a 1, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W0C   | Interrupt | Write 0 to Clear                | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current value |
#| W0S   | Interrupt | Write 0 to Set                  | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W0T   | UNKNOW    | Write 0 to Toggle               | If the bit in the wr value is a 0, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W1SRC | UNKNOW    | Write 1 to Set, Read Clears All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W1CRS | UNKNOW    | Write 1 to Clear, Read Sets All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| W0SRC | UNKNOW    | Write 0 to Set, Read Clears All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W0CRS | UNKNOW    | Write 0 to Clear, Read Sets All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| WO    | Config    | Write Only                      | Changed to written value | No effect | Undefined |
#| WOC   | UNKNOW    | Write Only Clears All           | Sets all bits to 0     | No effect | Undefined |
#| WOS   | UNKNOW    | Write Only Sets All             | Sets all bits to 1     | No effect | Undefined |
#| W1    | UNKNOW    | Write Once                      | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Current value |
#| WO1   | UNKNOW    | Write Only, Once                | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Undefined |
#=============================================================================
# Sample
#=============================================================================

######################################################################################
#SAMPLE> // Begin:
#SAMPLE> //# Some description
#SAMPLE> // Register Full Name: Device Version ID
#SAMPLE> // RTL Instant Name: VersionID
#SAMPLE> //# {FunctionName,SubFunction_InstantName_Description}
#SAMPLE> //# RTL Instant Name and Full Name MUST be unique within a register description file
#SAMPLE> // Address: 0x00_0000
#SAMPLE> //# Address is relative address, will be sum with Base_Address for each function block
#SAMPLE> // Formula      : Address + $portid 
#SAMPLE> // Where        : {$portid(0-7): port identification} 
#SAMPLE> // Description  : This register indicates the module' name and version. 
#SAMPLE> // Width: 32
#SAMPLE> // Register Type: {Status}
#SAMPLE> //# Field: [Bit:Bit] %%  Name     %% Description            %% Type %% SW_Reset %% HW_Reset
#SAMPLE> // Field: [31:24]    %%  Year     %% 2013                   %% RO   %% 0x0D     %% 0x0D
#SAMPLE> // Field: [23:16]    %%  Week     %% Week Synthesized FPGA  %% RO   %% 0x0C     %% 0x0C
#SAMPLE> // Field: [15:8]     %%  Day      %% Day Synthesized FPGA   %% RO   %% 0x16     %% 0x16
#SAMPLE> // Field: [7:4]      %%  VerID    %% FPGA Version           %% RO   %% 0x0      %% 0x0
#SAMPLE> // Field: [3:0]      %%  NumID    %% Number of FPGA Version %% RO   %% 0x1      %% 0x1
#SAMPLE> // End:
##################################################################
#                                                                #
#       10. Bert ID Mon	                                         #  
#                                                                #      
##################################################################
// Begin:
// Register Full Name: Sel Bert ID Mon
// RTL Instant Name  : upen_id_reg_mon
// Address      : 0x8500 - 0x851F
// Formula      : Address + $mid
// Where        : {$mid(0-31)}
// Description  : Sel 16 chanel id which enable moniter BERT from 12xOC24 channel 
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31:12]  %%  unused     %% unsed      %% RW  %% 0x0 %% 0x0
// Field: [11]     %%  mon_en	  %% bit enable %% RW  %% 0x0 %% 0x0
// Field: [10]     %%  DS3_bus    %% indicate DS3 bus select
//                                        "0" : DS3: bus1 for TDM side
//                                        "1" : DS3: bus2 for PSN Side %% RW  %% 0x0 %% 0x0
// Field: [09:00]  %%  mon_id     %% channel id, TDMID for TDM side, PWID for PSN Side %% RW  %% 0x0 %% 0x0
// End:
##################################################################
#                                                                #
#       11.  Global Mon Register                                 #  
#                                                                #      
##################################################################
// Begin:
// Register Full Name: Bert Gen Global Register 
// RTL Instant Name  : mglb_pen
// Address      : 0x8400
// Formula      : 
// Where        : 
// Description  : global mon config  
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31:10] %% unused %% unused %% RW %% 0x0 %% 0x0
// Field: [09:06] %% lopthrcfg %% Threshold for lost pattern moniter   %% RW %% 0x00 %% 0x01
// Field: [05:02] %% synthrcfg %% Threshold for gosyn state of pattern moniter   %% RW %% 0x00 %% 0x0C
// Field: [01]     %% swapmode  %% enable swap data  			   %% RW %% 0x00 %% 0x0
// Field: [00]     %% invmode   %% enable invert data  			   %% RW %% 0x00 %% 0x0
// End:
##################################################################
#                                                                #
#       12.  Sticky enable                                       #  
#                                                                #      
##################################################################
// Begin:
// Register Full Name: Sticky Enable 
// RTL Instant Name  : stkcfg_pen
// Address      : 0x8401
// Formula      : 
// Where        : 
// Description  : This register is used to configure the test pattern sticky mode
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31:01] %% unused %% unused %% RW %% 0x0 %% 0x0
// Field: [00]     %% stkcfg_reg       %%   			   %% RC %% 0x00 %% 0x0
// End:
##################################################################
#                                                                #
#       13.  Sticky Error                                        #  
#                                                                #      
##################################################################
// Begin:
// Register Full Name: Sticky Error 
// RTL Instant Name  : stk_pen
// Address      : 0x8402
// Formula      : 
// Where        : 
// Description  : Bert sticky error  
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31:16]  %% unused %% unused %% RW %% 0x0 %% 0x0
// Field: [15:0]    %% stkdi  %% indicate error of 16id moniter bert %% RW %% 0x00 %% 0x0
// End:
##################################################################
#                                                                #
#       14.  Error  counter                                      #  
#                                                                #      
##################################################################
// Begin :
// Register Full Name: Counter Error
// RTL Instant Name  : errlat_pen
// Address      : 0x8404
// Formula      : 
// Where        : 
// Description  : Latch error counter  
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31:0]    %% errcnt_lat  %% counter err %% RW %% 0x00 %% 0x0
// End:
##################################################################
#                                                                #
#       15.  Good  counter                                       #  
#                                                                #      
##################################################################
// Begin:
// Register Full Name: Counter Good
// RTL Instant Name  : goodlat_pen
// Address      : 0x8405
// Formula      : 
// Where        : 
// Description  : Latch good counter  
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31:0]    %%  goodcnt_lat  %% counter good %% RW %% 0x00 %% 0x0
// End:
##################################################################
#                                                                #
#       16.  Lost  counter                                       #  
#                                                                #      
##################################################################
// Begin:
// Register Full Name: Counter Lost
// RTL Instant Name  : lostlat_pen
// Address      : 0x8406
// Formula      : 
// Where        : 
// Description  :   
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31:0]    %%  lostcnt_lat  %% counter good %% RW %% 0x00 %% 0x0
// End:
##################################################################
#                                                                #
#       17. Mon Control                                          #  
#                                                                #      
##################################################################
// Begin:
// Register Full Name: Mon Control Bert Generate
// RTL Instant Name  : mon_ctrl_pen
// Address      : 0x8420 - 0x843F
// Formula      : Address + $mctrl_id
// Where        : {$mctrl_id(0-31)}
// Description  : Used to control bert gen mode of 16 id
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field:  [31:12] %% unused      %% unsed                      %% RW  %% 0x0 %% 0x0 
// Field:  [11]     %% mswapmode	   %% swap data   %% RW  %% 0x0 %% 0x0  
// Field:  [10]     %% minvmode	   %% invert data %% RW  %% 0x0 %% 0x0 
// Field:  [09 :05] %% mpatbit      %% number of bit fix patt use %% RW %% 0x0 %% 0x0
// Field:  [04 :00] %% mpatt_mode   %% sel pattern gen 
                                    # 0x0 : Prbs9
									# 0x1 : Prbs11
									# 0x2 : Prbs15
									# 0x3 : Prbs20r
									# 0x4 : Prbs20
									# 0x5 : qrss20
									# 0x6 : Prbs23
									# 0x7 : dds1
                                    # 0x8 : dds2
									# 0x9 : dds3
                                    # 0xA : dds4
									# 0xB : dds5
									# 0xC : daly
									# 0XD : octet55_v2
									# 0xE : octet55_v3
                                    # 0xF : fix3in24
									# 0x10: fix1in8
									# 0x11: fix2in8
									# 0x12: fixpat
                                    # 0x13: sequence
 									# 0x14: ds0prbs
                                    # 0x15: fixnin32
 													%% RW %% 0x0 %% 0x0
//End:
##################################################################
#                                                                #
#       18. Config Fix Pattern Mon                               #  
#                                                                #      
##################################################################
// Begin:
// Register Full Name: Config expected Fix pattern gen
// RTL Instant Name  : thrnfix_pen
// Address      : 0x8440 - 0x845F
// Formula      : Address + $genid
// Where        : {$genid (0-31)}
// Description  : config fix pattern gen
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %% Name %% Description %% Type %% Reset   %% Default
// Field:  [ 31:0]   %% crrthrnfix %% config fix pattern gen  %% RW  %% 0x0 %% 0x0
// End:
##################################################################
#                                                                #
#       19. Status of Mon BERT                                   #
#                                                                #      
##################################################################
// Begin:
// Register Full Name: Status of mon bert
// RTL Instant Name  : mon_status_pen
// Address      : 0x84E0 - 0x84FF
// Formula      : Address + $msttid
// Where        : {$msttid (0-31)}
// Description  : Indicate current status of bert mon   
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31:02]  %% unused     %% unsed %% RW  %% 0x0 %% 0x0 
// Field: [01:00]  %% mon_crr_stt    %% Status [1:0]: Prbs status
//   										LOPSTA = 2'd0;
//                                          SRCSTA = 2'd1;
//                                          VERSTA = 2'd2;
//                                          INFSTA = 2'd3;       %% R_O %% 0x0 %% 0x0 
// End:
##################################################################
#                                                                #
#       20. Moniter Error                                        #
#                                                                #      
##################################################################
// Begin:
// Register Full Name: counter error
// RTL Instant Name  : merrcnt_pen
// Address      : 0x8540 - 0x855F
// Formula      : Address + $errid
// Where        : {$errid(0-31)}
// Description  : Moniter error bert counter of 16 chanel id  
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31 :0]   %%  errcnt_pdo %% value of err counter %% R2C  %% 0x0 %% 0x0 
// End:
##################################################################
#                                                                #
#       21. Moniter Good Bit                                      #
#                                                                #      
##################################################################
// Begin:
// Register Full Name: Moniter Good Bit 
// RTL Instant Name  : mgoodbit_pen
// Address      : 0x8580 - 0x859F
// Formula      : Address + $mgb_id
// Where        : {$mgb_id(0-31)}
// Description  : Moniter good bert counter of 16 chanel id    
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31 :0]   %%  goodbit_pdo %% value of good cnt %% RW  %% 0x0 %% 0x0 
// End:
##################################################################
#                                                                #
#       22. Moniter Lost Bit                                      #
#                                                                #      
##################################################################
// Begin:
// Register Full Name: Moniter Lost Bit 
// RTL Instant Name  : mlostbit_pen
// Address      : 0x85C0 - 0x84DF
// Formula      : Address + $mlos_id
// Where        : {$mlos_id(0-31)}
// Description  :   
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31 :0]   %%  lostbit_pdo %% Value of lost cnt %% RW  %% 0x0 %% 0x0 
// End:
##################################################################
#                                                                #
#       23. Config nxDS0  Moniter                                #
#                                                                #      
##################################################################
// Begin:
// Register Full Name: Config nxDS0 moniter
// RTL Instant Name  : mtsen_pen
// Address      : 0x84C0 - 0x84CF
// Formula      : Address + $mid
// Where        : {$mid(0-31)}
// Description  : enable moniter 32 timeslot E1 or 24timeslot T1  
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name %% Description %% Type %% Reset %% Default
// Field: [31 :0]   %%  mtsen_pdo  %% set "1" to en moniter %% RW  %% 0x0 %% 0x0 
// End:
##################################################################
#                                                                #
#       24.  Moniter counter load id                             #  
#                                                                #      
##################################################################
// Begin:
// Register Full Name : moniter counter load id
// RTL Instant Name  : cntld_strb
// Address      : 0x8403
// Formula      : 
// Where        : 
// Description  : used to load the error/good/loss counter report of bert id which the channel ID is value of register.  
// Width        : 32
// Register Type: {Config }
//#Field: [Bit:Bit] %%  Name  %% Description %% Type %% Reset %% Default
// Field: [31:04]  %% unused %% unused %% RW %% 0x0 %% 0x0
// Field: [03:00]   %% idload %%  %% RW %% 0x00 %% 0x0
// End:
##################################################################
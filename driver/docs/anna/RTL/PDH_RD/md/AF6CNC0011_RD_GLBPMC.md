## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-05-29|AF6Project|Initial version|




##AF6CNC0011_RD_GLBPMC
####Register Table

|Name|Address|
|-----|-----|
|`CPU Reg Hold 0`|`0x3_B319`|
|`Pseudowire Transmit Counter`|`0x0_0000 - 0x0_0BFF(RC)`|
|`Pseudowire Transmit Counter`|`0x0_1000-0x0_1BFF(RO)`|
|`Pseudowire Receiver Counter Info0`|`0x0_8000 - 0x0_8BFF(RC)`|
|`Pseudowire Receiver Counter Info0`|`0x0_9000-0x0_9BFF(RO)`|
|`Pseudowire Receiver Counter Info1`|`0x1_0000 - 0x1_0BFF(RC)`|
|`Pseudowire Receiver Counter Info1`|`0x1_1000-0x1_1BFF(RO)`|
|`Pseudowire Receiver Counter Info2`|`0x1_8000 - 0x1_8BFF(RC)`|
|`Pseudowire Receiver Counter Info2`|`0x1_9000-0x1_9BFF(RO)`|
|`Pseudowire Byte Counter`|`0x6_8000- 0x6_83FF(RC)`|
|`Pseudowire Byte Counter`|`0x6_8400-0x6_87FF(RO)`|
|`PMR Error Counter`|`0x3_B000 - 0x3_B05F(RC)`|
|`PMR Error Counter`|`0x3_B080 - 0x3_B0DF(RO)`|
|`POH path sts ERDI Counter1`|`0x3_D800-0x3_D85F (RO) 0xB_D800-0xB_D85F (RO)`|
|`POH path vt ERDI Counter1`|`0x6_4000 - 0x6_47DF(RO)`|
|`POH path vt ERDI Counter1`|`0xE_4000 - 0xE_47DF(RO)`|
|`POH vt pointer Counter`|`0x5_0000 - 0x5_07DF(RC)`|
|`POH vt pointer Counter`|`0x5_4000-0x5_47DF(RO)`|
|`POH sts pointer Counter`|`0x3_F000 - 0x3_F05F(RC)`|
|`POH sts pointer Counter`|`0x3_F800-0x3_F85F(RO)`|
|`PDH ds1 cntval Counter Bus1`|`0x3_0000 - 0x3_07DF(RC)`|
|`PDH ds1 cntval Counter Bus1`|`0x3_0800-0x3_0FDF(RO)`|
|`PDH ds3 cntval Counter`|`0x3_8000 - 0x3_805F(RC)`|
|`PDH ds3 cntval Counter`|`0x3_8800-0x3_885F(RO)`|
|`PDH ds1 prm0 Counter`|`0x4_0000 - 0x4_07DF(RC)`|
|`PDH ds1 prm0 Counter`|`0x4_4000-0x4_47DF(RO)`|
|`PDH ds1 prm1 Counter`|`0x4_8000 - 0x4_87DF(RC)`|
|`PDH ds1 prm1 Counter`|`0x4_C000-0x4_C7DF(RO)`|
|`CLA Counter00`|`0x3_B320(RC)`|
|`CLA Counter00`|`0x3_B321(RO)`|
|`ETH Counter00`|`0x3_B328(RC)`|
|`ETH Counter00`|`0x3_B329(RO)`|
|`ETH Packet RX Counter`|`0x3_B370(RC)`|
|`ETH Packet RX Counter`|`0x3_B371(RO)`|
|`ETH Byte Counter`|`0x3_B372(RC)`|
|`ETH Byte Counter`|`0x3_B373(RO)`|
|`ETH Counter00`|`0x3_B338(RC)`|
|`ETH Counter00`|`0x3_B339(RO)`|
|`ETH Packet TX Counter`|`0x3_B374(RC)`|
|`ETH Packet TX Counter`|`0x3_B375(RO)`|
|`ETH Byte TX Counter`|`0x3_B376(RC)`|
|`ETH Byte TX Counter`|`0x3_B377(RO)`|
|`ETH TX Packet OAM CNT`|`0x3_B336(RC)`|
|`ETH TX Packet OAM CNT`|`0x3_B337(RO)`|
|`Classifier Bytes Counter vld`|`0x3_B308-0x3_B30B(RC)`|
|`Classifier Bytes Counter vld`|`0x3_B30C-0x3_B30F(RO)`|
|`Classifier output counter pkt0`|`0x3_B348-0x3_B34B(RC)`|
|`Classifier output counter pkt0`|`0x3_B34C-0x3_B34F(RO)`|
|`Classifier output counter pkt1`|`0x3_B350-0x3_B353(RC)`|
|`Classifier output counter pkt1`|`0x3_B354-0x3_B357(RO)`|
|`Classifier input counter pkt`|`0x3_B306(RC)`|
|`Classifier input counter pkt`|`0x3_B307(RO)`|
|`PDH Mux Ethernet Byte Counter`|`0x3_B300(RC)`|
|`PDH Mux Ethernet Byte Counter`|`0x3_B301(RO)`|
|`PDH Mux Ethernet Packet Counter`|`0x3_B340(RC)`|
|`PDH Mux Ethernet Packet Counter`|`0x3_B341(RO)`|
|`DE1 TDM bit counter`|`0x3_B200-0x3_B27F(RC)`|
|`DE1 TDM bit counter`|`0x3_B280-0x3_B2FF(RO)`|
|`DE3 TDM bit counter`|`0x3_B180-0x3_B19F(RC)`|
|`DE3 TDM bit counter`|`0x3_B1A0-0x3_B1BF(RO)`|


###CPU Reg Hold 0

* **Description**           

The register provides hold register from [63:32]


* **RTL Instant Name**    : `hold_reg0`

* **Address**             : `0x3_B319`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`hold0`| Hold from  [63:32]bit| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_rc`

* **Address**             : `0x0_0000 - 0x0_0BFF(RC)`

* **Formula**             : `0x0_0000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`txpwcnt`| in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : Unused in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : txpkt| `RC`| `0x0`| `0x0 Begin:`|

###Pseudowire Transmit Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_ro`

* **Address**             : `0x0_1000-0x0_1BFF(RO)`

* **Formula**             : `0x0_1000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`txpwcnt`| in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : Unused in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : txpkt| `RC`| `0x0`| `0x0 Begin:`|

###Pseudowire Receiver Counter Info0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info0_rc`

* **Address**             : `0x0_8000 - 0x0_8BFF(RC)`

* **Formula**             : `0x0_8000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxlate + offset = 1 : rxpbit + offset = 2 : rxlbit in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxpkt| `RC`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info0_ro`

* **Address**             : `0x0_9000-0x0_9BFF(RO)`

* **Formula**             : `0x0_9000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxlate + offset = 1 : rxpbit + offset = 2 : rxlbit in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxpkt| `RC`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info1_rc`

* **Address**             : `0x1_0000 - 0x1_0BFF(RC)`

* **Formula**             : `0x1_0000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info1`| in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxearly + offset = 1 : rxlops + offset = 2 : Unused in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxlost + offset = 1 : Unused + offset = 2 : Unused| `RC`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info1_ro`

* **Address**             : `0x1_1000-0x1_1BFF(RO)`

* **Formula**             : `0x1_1000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info1`| in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxearly + offset = 1 : rxlops + offset = 2 : Unused in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxlost + offset = 1 : Unused + offset = 2 : Unused| `RC`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info2

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info2_rc`

* **Address**             : `0x1_8000 - 0x1_8BFF(RC)`

* **Formula**             : `0x1_8000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info2`| in case of rxpwcnt_info2_cnt0 side: + offset = 0 : rxoverrun + offset = 1 : rxunderrun + offset = 2 : rxmalform in case of rxpwcnt_info2_cnt1 side: + offset = 0 : rxoam + offset = 1 : rxduplicate + offset = 2 : rxstray| `RC`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info2

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info2_ro`

* **Address**             : `0x1_9000-0x1_9BFF(RO)`

* **Formula**             : `0x1_9000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info2`| in case of rxpwcnt_info2_cnt0 side: + offset = 0 : rxoverrun + offset = 1 : rxunderrun + offset = 2 : rxmalform in case of rxpwcnt_info2_cnt1 side: + offset = 0 : rxoam + offset = 1 : rxduplicate + offset = 2 : rxstray| `RC`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pwcntbyte_rc`

* **Address**             : `0x6_8000- 0x6_83FF(RC)`

* **Formula**             : `0x6_8000 + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`pwcntbyte`| txpwcntbyte (rxpwcntbyte)| `RC`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pwcntbyte_ro`

* **Address**             : `0x6_8400-0x6_87FF(RO)`

* **Formula**             : `0x6_8400 + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`pwcntbyte`| txpwcntbyte (rxpwcntbyte)| `RC`| `0x0`| `0x0 End: Begin:`|

###PMR Error Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_pmr_cnt0_rc`

* **Address**             : `0x3_B000 - 0x3_B05F(RC)`

* **Formula**             : `0x3_B000 + 32*$offset + $stsid`

* **Where**               : 

    * `$offset(0-2)`

    * `$stsid(0-31)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[31:00]`|`pmr_err_cnt`| in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b1 + offset = 2 : Unused in case of pmr_err_cnt1 side: + offset = 0 : b2 + offset = 1 : Unused + offset = 2 : Unused| `RC`| `0x0`| `0x0 End: Begin:`|

###PMR Error Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_pmr_cnt0_ro`

* **Address**             : `0x3_B080 - 0x3_B0DF(RO)`

* **Formula**             : `0x3_B080 + 32*$offset + $stsid`

* **Where**               : 

    * `$offset(0-2)`

    * `$stsid(0-31)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[31:00]`|`pmr_err_cnt`| in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b1 + offset = 2 : Unused in case of pmr_err_cnt1 side: + offset = 0 : b2 + offset = 1 : Unused + offset = 2 : Unused| `RC`| `0x0`| `0x0 End: Begin:`|

###POH path sts ERDI Counter1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pohpmdat_sts_ber_erdi1`

* **Address**             : `0x3_D800-0x3_D85F (RO) 0xB_D800-0xB_D85F (RO)`

* **Formula**             : `0x3_D800 + 32*$offset +  $stsid`

* **Where**               : 

    * `$stsid(0-31)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_ber_erdi`| in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : sts_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : sts_bip(B3) + offset = 1 : Unused + offset = 2 : Unused| `RO`| `0x0`| `0x0 End: Begin:`|

###POH path vt ERDI Counter1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pohpmdat_vt_ber_erdi1_ro`

* **Address**             : `0x6_4000 - 0x6_47DF(RO)`

* **Formula**             : `0x6_4000 + 672*$offset + 28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_ber_erdi`| in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused| `RO`| `0x0`| `0x0 End: Begin:`|

###POH path vt ERDI Counter1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pohpmdat_vt_ber_erdi1_ro`

* **Address**             : `0xE_4000 - 0xE_47DF(RO)`

* **Formula**             : `0xE_4000 + 672*$offset + 28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_ber_erdi`| in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused| `RO`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt_rc`

* **Address**             : `0x5_0000 - 0x5_07DF(RC)`

* **Formula**             : `0x5_0000 + 672*$offset +  28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset = 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset = 0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused| `RC`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt_ro`

* **Address**             : `0x5_4000-0x5_47DF(RO)`

* **Formula**             : `0x5_4000 + 672*$offset +  28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset = 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset = 0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused| `RC`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt_rc`

* **Address**             : `0x3_F000 - 0x3_F05F(RC)`

* **Formula**             : `0x3_F000 + 32*$offset +  $stsid`

* **Where**               : 

    * `$stsid(0-31)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`poh_sts_cnt`| in case of poh_sts_cnt0 side: + offset = 0 :  pohtx_sts_dec + offset = 1 :  pohrx_sts_dec + offset = 2 :  Unused in case of poh_sts_cnt1 side: + offset = 0 :  pohtx_sts_inc + offset = 1 :  pohrx_sts_inc + offset = 2 :  Unused| `RC`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt_ro`

* **Address**             : `0x3_F800-0x3_F85F(RO)`

* **Formula**             : `0x3_F800 + 32*$offset +  $stsid`

* **Where**               : 

    * `$stsid(0-31)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`poh_sts_cnt`| in case of poh_sts_cnt0 side: + offset = 0 :  pohtx_sts_dec + offset = 1 :  pohrx_sts_dec + offset = 2 :  Unused in case of poh_sts_cnt1 side: + offset = 0 :  pohtx_sts_inc + offset = 1 :  pohrx_sts_inc + offset = 2 :  Unused| `RC`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 cntval Counter Bus1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de1cntval30_rc`

* **Address**             : `0x3_0000 - 0x3_07DF(RC)`

* **Formula**             : `0x3_0000 + 672*$offset + 28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`ds1_cntval_bus1_cnt`| in case of ds1_cntval_bus1_cnt0 side: + offset = 0 : fe + offset = 1 : rei + offset = 2 : unused in case of ds1_cntval_bus1_cnt1 side: + offset = 0 : crc + offset = 1 : lcv + offset = 2 : unused| `RC`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 cntval Counter Bus1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de1cntval30_ro`

* **Address**             : `0x3_0800-0x3_0FDF(RO)`

* **Formula**             : `0x3_0800 + 672*$offset + 28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`ds1_cntval_bus1_cnt`| in case of ds1_cntval_bus1_cnt0 side: + offset = 0 : fe + offset = 1 : rei + offset = 2 : unused in case of ds1_cntval_bus1_cnt1 side: + offset = 0 : crc + offset = 1 : lcv + offset = 2 : unused| `RC`| `0x0`| `0x0 End: Begin:`|

###PDH ds3 cntval Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de3cnt0_rc`

* **Address**             : `0x3_8000 - 0x3_805F(RC)`

* **Formula**             : `0x3_8000 + 32*$offset + $stsid`

* **Where**               : 

    * `$stsid(0-31)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`ds3_cntval_cnt`| in case of ds3_cntval_cnt0 side: + offset = 0 : fe + offset = 1 : pb + offset = 2 : lcv in case of ds3_cntval_cnt1 side: + offset = 0 : rei + offset = 1 : cb + offset = 2 : unused| `RC`| `0x0`| `0x0 End: Begin:`|

###PDH ds3 cntval Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de3cnt0_ro`

* **Address**             : `0x3_8800-0x3_885F(RO)`

* **Formula**             : `0x3_8800 + 32*$offset + $stsid`

* **Where**               : 

    * `$stsid(0-31)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`ds3_cntval_cnt`| in case of ds3_cntval_cnt0 side: + offset = 0 : fe + offset = 1 : pb + offset = 2 : lcv in case of ds3_cntval_cnt1 side: + offset = 0 : rei + offset = 1 : cb + offset = 2 : unused| `RC`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 prm0 Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_de1prm_cnt0_rc`

* **Address**             : `0x4_0000 - 0x4_07DF(RC)`

* **Formula**             : `0x4_0000 + 672*$offset + 28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`ds1_prm0_cnt`| in case of ds1_prm0_cnt0 side: + offset = 0 : SL + offset = 1 : SE + offset = 2 : G5 in case of ds1_prm0_cnt1 side: + offset = 0 : LV + offset = 1 : G6 + offset = 2 : G4| `RC`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 prm0 Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_de1prm_cnt0_ro`

* **Address**             : `0x4_4000-0x4_47DF(RO)`

* **Formula**             : `0x4_4000 + 672*$offset + 28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`ds1_prm0_cnt`| in case of ds1_prm0_cnt0 side: + offset = 0 : SL + offset = 1 : SE + offset = 2 : G5 in case of ds1_prm0_cnt1 side: + offset = 0 : LV + offset = 1 : G6 + offset = 2 : G4| `RC`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 prm1 Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_de1prm_cnt1_rc`

* **Address**             : `0x4_8000 - 0x4_87DF(RC)`

* **Formula**             : `0x4_8000 + 672*$offset + 28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`ds1_prm1_cnt`| in case of ds1_prm1_cnt0 side: + offset = 0 : G3 + offset = 1 : G1 + offset = 2 : unused in case of ds1_prm1_cnt1 side: + offset = 0 : G2 + offset = 1 : unused + offset = 2 : unused| `RC`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 prm1 Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_de1prm_cnt1_ro`

* **Address**             : `0x4_C000-0x4_C7DF(RO)`

* **Formula**             : `0x4_C000 + 672*$offset + 28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`ds1_prm1_cnt`| in case of ds1_prm1_cnt0 side: + offset = 0 : G3 + offset = 1 : G1 + offset = 2 : unused in case of ds1_prm1_cnt1 side: + offset = 0 : G2 + offset = 1 : unused + offset = 2 : unused| `RC`| `0x0`| `0x0 End: Begin:`|

###CLA Counter00

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cla0_rc`

* **Address**             : `0x3_B320(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cla_err`| cla_err| `RC`| `0x0`| `0x0 End: Begin:`|

###CLA Counter00

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cla0_ro`

* **Address**             : `0x3_B321(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cla_err`| cla_err| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH Counter00

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxeth00_rc`

* **Address**             : `0x3_B328(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxpkt`| rxpkt| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH Counter00

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxeth00_ro`

* **Address**             : `0x3_B329(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxpkt`| rxpkt| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH Packet RX Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxeth1_pkt_rc`

* **Address**             : `0x3_B370(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntrxpkt`| ETH rx Packet Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH Packet RX Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxeth1_pkt_ro`

* **Address**             : `0x3_B371(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntrxpkt`| ETH rx Packet Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxeth1_byte_rc`

* **Address**             : `0x3_B372(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntrxbyte`| ETH rx Byte Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxeth1_byte_ro`

* **Address**             : `0x3_B373(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntrxbyte`| ETH rx Byte Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH Counter00

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txeth00_rc`

* **Address**             : `0x3_B338(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txpkt`| txpkt| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH Counter00

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txeth00_ro`

* **Address**             : `0x3_B339(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txpkt`| txpkt| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH Packet TX Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txeth1_pkt_rc`

* **Address**             : `0x3_B374(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxpkt`| ETH tx Packet Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH Packet TX Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txeth1_pkt_ro`

* **Address**             : `0x3_B375(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxpkt`| ETH tx Packet Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH Byte TX Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txeth1_byte_rc`

* **Address**             : `0x3_B376(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxbyte`| ETH tx Byte Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH Byte TX Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txeth1_byte_ro`

* **Address**             : `0x3_B377(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxbyte`| ETH tx Byte Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH TX Packet OAM CNT

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cnttxoam_rc`

* **Address**             : `0x3_B336(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxoamt`| TX Packet OAM CNT| `RC`| `0x0`| `0x0 End: Begin:`|

###ETH TX Packet OAM CNT

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cnttxoam_ro`

* **Address**             : `0x3_B337(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxoamt`| TX Packet OAM CNT| `RC`| `0x0`| `0x0 End: Begin:`|

###Classifier Bytes Counter vld

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_bcnt_rc`

* **Address**             : `0x3_B308-0x3_B30B(RC)`

* **Formula**             : `0x3_B308 + $clsid`

* **Where**               : 

    * `$clsid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`bytecnt`| Classifier bytecnt| `RC`| `0x0`| `0x0 End: Begin:`|

###Classifier Bytes Counter vld

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_bcnt_ro`

* **Address**             : `0x3_B30C-0x3_B30F(RO)`

* **Formula**             : `0x3_B30C + $clsid`

* **Where**               : 

    * `$clsid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`bytecnt`| Classifier bytecnt| `RC`| `0x0`| `0x0 End: Begin:`|

###Classifier output counter pkt0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_opcnt0_rc`

* **Address**             : `0x3_B348-0x3_B34B(RC)`

* **Formula**             : `0x3_B348 + $offset`

* **Where**               : 

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`opktcnt0_opktcnt1_`| in case of opktcnt0 + offset = 0 : DS1/E1 good pkt counter + offset = 1 : Control pkt good counter + offset = 2 : FCs err cnt in case of opktcnt1 + offset = 0 : DS3/E3/EC1 good pkt counter + offset = 1 : Sequence err  counter + offset = 2 : len pkt err cnt| `RC`| `0x0`| `0x0 End: Begin:`|

###Classifier output counter pkt0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_opcnt0_ro`

* **Address**             : `0x3_B34C-0x3_B34F(RO)`

* **Formula**             : `0x3_B34C + $offset`

* **Where**               : 

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`opktcnt0_opktcnt1_`| in case of opktcnt0 + offset = 0 : DS1/E1 good pkt counter + offset = 1 : Control pkt good counter + offset = 2 : FCs err cnt in case of opktcnt1 + offset = 0 : DS3/E3/EC1 good pkt counter + offset = 1 : Sequence err  counter + offset = 2 : len pkt err cnt| `RC`| `0x0`| `0x0 End: Begin:`|

###Classifier output counter pkt1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_opcnt1_rc`

* **Address**             : `0x3_B350-0x3_B353(RC)`

* **Formula**             : `0x3_B350 + $offset`

* **Where**               : 

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`opktcnt0_opktcnt1_`| in case of opktcnt0 + offset = 0 : DA error pkt counter + offset = 1 : E-type err pkt counter + offset = 2 : Unused in case of opktcnt1 + offset = 0 : SA error pkt counter + offset = 1 : Length field err counter + offset = 2 : Unused| `RC`| `0x0`| `0x0 End: Begin:`|

###Classifier output counter pkt1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_opcnt1_ro`

* **Address**             : `0x3_B354-0x3_B357(RO)`

* **Formula**             : `0x3_B354 + $offset`

* **Where**               : 

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`opktcnt0_opktcnt1_`| in case of opktcnt0 + offset = 0 : DA error pkt counter + offset = 1 : E-type err pkt counter + offset = 2 : Unused in case of opktcnt1 + offset = 0 : SA error pkt counter + offset = 1 : Length field err counter + offset = 2 : Unused| `RC`| `0x0`| `0x0 End: Begin:`|

###Classifier input counter pkt

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_ipcnt_rc`

* **Address**             : `0x3_B306(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ipktcnt`| input packet cnt| `RC`| `0x0`| `0x0 End: Begin:`|

###Classifier input counter pkt

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_ipcnt_ro`

* **Address**             : `0x3_B307(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ipktcnt`| input packet cnt| `RC`| `0x0`| `0x0 End: Begin:`|

###PDH Mux Ethernet Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_eth_bytecnt_rc`

* **Address**             : `0x3_B300(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`eth_bytecnt`| ETH Byte Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###PDH Mux Ethernet Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_eth_bytecnt_ro`

* **Address**             : `0x3_B301(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`eth_bytecnt`| ETH Byte Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###PDH Mux Ethernet Packet Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_eth_bytecnt_rc`

* **Address**             : `0x3_B340(RC)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`eth_pktcnt`| ETH Packet Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###PDH Mux Ethernet Packet Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_eth_bytecnt_ro`

* **Address**             : `0x3_B341(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`eth_pktcnt`| ETH Packet Counter| `RC`| `0x0`| `0x0 End: Begin:`|

###DE1 TDM bit counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_de1_tdmvld_cnt_rc`

* **Address**             : `0x3_B200-0x3_B27F(RC)`

* **Formula**             : `0x3_B200 + $lid`

* **Where**               : 

    * `$lid(0-127)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`de1_bit_cnt`| bit_cnt| `RC`| `0x0`| `0x0 End: Begin:`|

###DE1 TDM bit counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_de1_tdmvld_cnt_ro`

* **Address**             : `0x3_B280-0x3_B2FF(RO)`

* **Formula**             : `0x3_B280 + $lid`

* **Where**               : 

    * `$lid(0-127)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`de1_bit_cnt`| bit_cnt| `RC`| `0x0`| `0x0 End: Begin:`|

###DE3 TDM bit counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_de3_tdmvld_cnt_rc`

* **Address**             : `0x3_B180-0x3_B19F(RC)`

* **Formula**             : `0x3_B180 + $lid`

* **Where**               : 

    * `$lid(0-31)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`de3_bit_cnt`| bit_cnt| `RC`| `0x0`| `0x0 End:`|

###DE3 TDM bit counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_de3_tdmvld_cnt_ro`

* **Address**             : `0x3_B1A0-0x3_B1BF(RO)`

* **Formula**             : `0x3_B1A0 + $lid`

* **Where**               : 

    * `$lid(0-31)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:00]`|`de3_bit_cnt`| bit_cnt| `RC`| `0x0`| `0x0 End:`|

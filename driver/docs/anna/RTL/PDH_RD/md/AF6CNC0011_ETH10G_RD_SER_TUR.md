## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-03-09|AF6Project|Initial version|




##AF6CNC0011_ETH10G_RD_SER_TUR
####Register Table

|Name|Address|
|-----|-----|
|`ETH 10G DRP`|`0x400-0x7FF`|
|`ETH 10G LoopBack`|`0x0002`|
|`ETH 10G TX Reset`|`0x000C`|
|`ETH 10G RX Reset`|`0x000D`|
|`ETH 10G LPMDFE Mode`|`0x000E`|
|`ETH 10G LPMDFE Reset`|`0x000F`|
|`ETH 10G TXDIFFCTRL`|`0x0010`|
|`ETH 10G TXPOSTCURSOR`|`0x0011`|
|`ETH 10G TXPRECURSOR`|`0x0012`|
|`ETH 10G Ctrl FCS`|`0x0080`|
|`ETH 10G AutoNeg`|`0x0081`|


###ETH 10G DRP

* **Description**           

Read/Write DRP address of SERDES


* **RTL Instant Name**    : `OETH_10G_DRP`

* **Address**             : `0x400-0x7FF`

* **Formula**             : `0x400+$DRP`

* **Where**               : 

    * `$DRP(0-1023) : DRP address, see UG578`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:00]`|`drp_rw`| DRP read/write value| `R/W`| `0x0`| `0x0`|

###ETH 10G LoopBack

* **Description**           

Configurate LoopBack


* **RTL Instant Name**    : `ETH_10G_LoopBack`

* **Address**             : `0x0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`lpback_lane0`| Loopback<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|

###ETH 10G TX Reset

* **Description**           

Reset TX SERDES


* **RTL Instant Name**    : `ETH_10G_TX_Reset`

* **Address**             : `0x000C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`txrst_done`| TX Reset Done<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`txrst_trig`| Trige 0->1 to start reset TX SERDES| `R/W`| `0x0`| `0x0`|

###ETH 10G RX Reset

* **Description**           

Reset RX SERDES


* **RTL Instant Name**    : `ETH_49G_RX_Reset`

* **Address**             : `0x000D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`rxrst_done`| RX Reset Done<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`rxrst_trig`| Trige 0->1 to start reset RX SERDES| `R/W`| `0x0`| `0x0`|

###ETH 10G LPMDFE Mode

* **Description**           

Configure LPM/DFE mode


* **RTL Instant Name**    : `ETH_10G_LPMDFE_Mode`

* **Address**             : `0x000E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`lpmdfe_mode`| LPM/DFE mode<br>{0} : DFE mode <br>{1} : LPM mode| `R/W`| `0x0`| `0x0`|

###ETH 10G LPMDFE Reset

* **Description**           

Reset LPM/DFE


* **RTL Instant Name**    : `ETH_10G_LPMDFE_Reset`

* **Address**             : `0x000F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`lpmdfe_reset`| LPM/DFE reset<br>{1} : reset| `R/W`| `0x0`| `0x0`|

###ETH 10G TXDIFFCTRL

* **Description**           

Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail


* **RTL Instant Name**    : `ETH_10G_TXDIFFCTRL`

* **Address**             : `0x0010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:00]`|`txdiffctrl`| TXDIFFCTRL| `R/W`| `0x18`| `0x18`|

###ETH 10G TXPOSTCURSOR

* **Description**           

Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail


* **RTL Instant Name**    : `ETH_10G_TXPOSTCURSOR`

* **Address**             : `0x0011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:00]`|`txpostcursor`| TXPOSTCURSOR| `R/W`| `0x15`| `0x15`|

###ETH 10G TXPRECURSOR

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail


* **RTL Instant Name**    : `ETH_10G_TXPRECURSOR`

* **Address**             : `0x0012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:00]`|`txprecursor`| TXPRECURSOR| `R/W`| `0x0`| `0x0`|

###ETH 10G Ctrl FCS

* **Description**           

configure FCS mode


* **RTL Instant Name**    : `ETH_10G_Ctrl_FCS`

* **Address**             : `0x0080`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`txfcs_ignore`| TX ignore check FCS when txfcs_ins is low<br>{1} : ignore| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`txfcs_ins`| TX inserts 4bytes FCS<br>{1} : insert| `R/W`| `0x1`| `0x1`|
|`[03:02]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:01]`|`rxfcs_ignore`| RX ignore check FCS<br>{1} : ignore| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`rxfcs_rmv`| RX remove 4bytes FCS<br>{1} : remove| `R/W`| `0x1`| `0x1`|

###ETH 10G AutoNeg

* **Description**           

configure Auto-Neg


* **RTL Instant Name**    : `ETH_10G_AutoNeg`

* **Address**             : `0x0081`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:08]`|`an_lt_sta`| Link Control outputs from the auto-negotiationcontroller for the various Ethernet protocols.<br>{0} : DISABLE; PCS is disconnected <br>{1} : SCAN_FOR_CARRIER; RX is connected to PCS <br>{2} : not used <br>{3} : ENABLE; PCS is connected for mission mode operation| `R/W`| `0x0`| `0x0`|
|`[07:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`lt_restart`| This signal triggers a restart of link training regardless of the current state.<br>{1} : ignore| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`lt_enb`| Enables link training. When link training is disabled, all PCS lanes function in mission mode.<br>{1} : enable| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[02:02]`|`an_restart`| This input is used to trigger a restart of the auto-negotiation, regardless of what state the circuit is currently in.<br>{1} : restart| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`an_bypass`| Input to disable auto-negotiation and bypass the auto-negotiation function. If this input is asserted, auto-negotiation is turned off, but the PCS is connected to the output to allow operation.<br>{1} : bypass| `R/W`| `0x1`| `0x1`|
|`[00:00]`|`an_enb`| Enable signal for auto-negotiation<br>{1} : enable| `R/W`| `0x0`| `0x0`|

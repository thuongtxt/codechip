## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_TOP
####Register Table

|Name|Address|
|-----|-----|
|`XFI drp port select`|`0x00043`|
|`TOP Status 1-d`|`0x00061 - 0x0006d`|
|`Top Interface Alarm Sticky`|`0x00060`|
|`Top Interface Alarm Sticky 0`|`0x00050`|
|`Top Interface Alarm Sticky 1`|`0x00051`|
|`Top Interface Alarm Sticky 2`|`0x00052`|


###XFI drp port select

* **Description**           

This is the global configuration register for the Global Serdes Control


* **RTL Instant Name**    : `o_controlx`

* **Address**             : `0x00043`

* **Formula**             : `Base Addr + 0x00043`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[03:00]`|`xfi_port_select`| XFI port select, 1/2 -> XFI#0/XFI#1| `RW`| `0x0`| `0x0 End : Begin:`|

###TOP Status 1-d

* **Description**           



0x61 : PCIe user clock (62.5 Mhz)

0x62 : DDR3#0 user clock (200 Mhz)

0x63 : DDR3#1 user clock (200 Mhz)

0x64 : DDR3#2 user clock (200 Mhz)

0x65 : XFI#0 user clock (156.25 Mhz)

0x66 : XFI#1 user clock (156.25 Mhz)

0x67 : SGMII2G5#0 user clock (312.5 Mhz)

0x68 : SGMII1G#0 user clock (125 Mhz)

0x69 : SGMII1G#1 user clock (125 Mhz)

0x6a : sysPLL user clock (155.52 Mhz)

0x6b : External reference clock (19.44 Mhz)

0x6c : PRC reference clock (19.44 Mhz)

0x6d : 1PPS reference clock (1 Hz)


* **RTL Instant Name**    : `TOP_Status_1_d`

* **Address**             : `0x00061 - 0x0006d`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`i_statusx`| Clock Value Monitor Change from hex format to DEC format to get Clock Value| `RO`| `0x0`| `0x0 End : Begin:`|

###Top Interface Alarm Sticky

* **Description**           

Top Interface Status Sticky


* **RTL Instant Name**    : `i_sticky0`

* **Address**             : `0x00060`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00]`|`system_pll_status`| System PLL status, 0/1->Locked/Unlocked| `R/W/C`| `0x0`| `0x0`|
|`[03:01]`|`un-used`| Un-used| `R/W/C`| `0x0`| `0x0`|
|`[04]`|`ddr3_0_calib_status`| DDR3#0 calib status, 0/1->OK/Fail| `R_O`| `0x0`| `0x0`|
|`[05]`|`ddr3_1_calib_status`| DDR3#1 calib status, 0/1->OK/Fail| `R_O`| `0x0`| `0x0`|
|`[06]`|`ddr3_2_calib_status`| DDR3#2 calib status, 0/1->OK/Fail| `R_O`| `0x0`| `0x0 End : Begin:`|

###Top Interface Alarm Sticky 0

* **Description**           

Top Interface Alarm Sticky


* **RTL Instant Name**    : `i_sticky0`

* **Address**             : `0x00050`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00]`|`system_pll_status`| System PLL status, 0/1->Locked/Unlocked| `R/W/C`| `0x0`| `0x0`|
|`[03:01]`|`un-used`| Un-used| `R/W/C`| `0x0`| `0x0`|
|`[04]`|`ddr3_0_calib_status`| DDR3#0 calib status, 0/1->OK/Fail| `R/W/C`| `0x0`| `0x0`|
|`[05]`|`ddr3_1_calib_status`| DDR3#1 calib status, 0/1->OK/Fail| `R/W/C`| `0x0`| `0x0`|
|`[06]`|`ddr3_2_calib_status`| DDR3#2 calib status, 0/1->OK/Fail| `R/W/C`| `0x0`| `0x0 End : Begin:`|

###Top Interface Alarm Sticky 1

* **Description**           

Top Interface Alarm Sticky


* **RTL Instant Name**    : `i_sticky1`

* **Address**             : `0x00051`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `R/W/C`| `0x0`| `0x0`|
|`[03]`|`xfi_1_reset_done`| XFI#1 reset done, 0/1->Fail/Done| `R/W/C`| `0x0`| `0x0`|
|`[02]`|`xfi_0_reset_done`| XFI#0 reset done, 0/1->Fail/Done| `R/W/C`| `0x0`| `0x0`|
|`[01]`|`xfi_1_core_status__block_lock_`| XFI#1 core status (block lock), 0/1->Unlocked/Lock| `R/W/C`| `0x0`| `0x0`|
|`[00]`|`xfi_0_core_status__block_lock_`| XFI#0 core status (block lock), 0/1->Unlocked/Lock| `R/W/C`| `0x0`| `0x0 End : Begin:`|

###Top Interface Alarm Sticky 2

* **Description**           

Top Interface Alarm Sticky


* **RTL Instant Name**    : `i_sticky1`

* **Address**             : `0x00052`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R/W/C`| `0x0`| `0x0`|
|`[07]`|`unused`| *n/a*| `R/W/C`| `0x0`| `0x0`|
|`[06]`|`sgmii1g_1_reset_done`| SGMII1G#1 reset done, 0/1->Fail/Done| `R/W/C`| `0x0`| `0x0`|
|`[05]`|`sgmii1g_0_reset_done`| SGMII1G#0 reset done, 0/1->Fail/Done| `R/W/C`| `0x0`| `0x0`|
|`[04]`|`sgmii2g5_0_reset_done`| SGMII2G5#0 reset done, 0/1->Fail/Done| `R/W/C`| `0x0`| `0x0`|
|`[03]`|`unused`| *n/a*| `R/W/C`| `0x0`| `0x0`|
|`[02]`|`sgmii1g_1_link_status`| SGMII1G#1 Link status, 0/1->Down/Up| `R/W/C`| `0x0`| `0x0`|
|`[01]`|`sgmii1g_0_link_status`| SGMII1G#0 Link status, 0/1->Down/Up| `R/W/C`| `0x0`| `0x0`|
|`[00]`|`sgmii2g5_0_link_status`| SGMII2G5#0 Link status, 0/1->Down/Up| `R/W/C`| `0x0`| `0x0 End :`|

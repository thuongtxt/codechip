## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_PLA
####Register Table

|Name|Address|
|-----|-----|
|`Thalassa PDHPW Payload Assemble Signaling Convert 1 to 4 Status`|`0x00008000-0x0000AF77`|
|`Thalassa PDHPW Adding Protocol To Prevent Burst`|`0x00000003`|
|`Thalassa PDHPW Payload Assemble Payload Size Control`|`0x00002000-0x0000229F`|
|`Thalassa PDHPW Payload Assemble uP Address Convert Control`|`0x00000000`|
|`Thalassa PDHPW Payload Assemble Engine Status`|`0x00028000-0x0002829F #The address format for these registers is 0x0028000 + $PWID`|


###Thalassa PDHPW Payload Assemble Signaling Convert 1 to 4 Status

* **Description**           

These registers are used to convert


* **RTL Instant Name**    : `sig_conv_sta`

* **Address**             : `0x00008000-0x0000AF77`

* **Formula**             : `0x00008000  + 1024*$stsid + 128*$vtg + 32*$vtn + $ds0id`

* **Where**               : 

    * `$stsid(0-3)`

    * `$vtg(0-6)`

    * `$vtn(0-3)`

    * `$ds0id(0-31)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2:0]`|`pdhpwplaasmsigconv1to4sta`| convert 1 to 4 signaling bits of timeslots of DS1 mode| `RW`| `0x0`| `0x0 End: Begin:`|

###Thalassa PDHPW Adding Protocol To Prevent Burst

* **Description**           

This register is to add a pseudo-wire that does not cause bottle-neck at transmit PW cause ACR performance bad at receive side


* **RTL Instant Name**    : `add_pro_2_prevent_burst`

* **Address**             : `0x00000003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13:4]`|`addpwid`| PWID to be added| `RW`| `0x0`| `0x0`|
|`[3:0]`|`addpwstate`| State machine to add a PDH PW Step1: Write pseudo-wire ID to be added to AddPwId and set AddPwState to 0x1 to prepare add PW Step2: Do all configuration to add a pseudo-wire such as MAP,DEMAP,PLA,PDA,CLA,PSN.... Step3: Write AddPwState value 0x2 to start add PW protocol Step4: Poll AddPwState until return value of AddPwState equal to 0x0 the finish| `RW`| `0x0`| `0x0 End: Begin:`|

###Thalassa PDHPW Payload Assemble Payload Size Control

* **Description**           

These registers are used to configure payload size in each PW channel


* **RTL Instant Name**    : `pyld_size_ctrl`

* **Address**             : `0x00002000-0x0000229F`

* **Formula**             : `0x00002000 +  $PWID`

* **Where**               : 

    * `$PWID(0-671): Pseudowire ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[18:15]`|`pdhpwplaasmpldtypectrl`| Payload type in the PW channel 0: SAToP 4: CES with CAS 5: CES without CAS 6: CEP| `RW`| `0x0`| `0x0`|
|`[14:0]`|`pdhpwplaasmpldsizectrl`| Payload size in the PW channel SAToP mode: [11:0] payload size in a packet CES mode [5:0] number of DS0 [14:6] number of NxDS0 basic CEP mode: [11:0] payload size in a packetizer fractional CEP mode: [3:0] Because value of payload size may be 3/9, 4/9, 5/9, 6/9, 7/9, 8/9, or 9/9 of VC-3 or VC-4 SPE, depended on fractional VC-3 or fractional VC-4 CEP mode. And a VC-3 or VC-4 //SPE has 9 row. So configured value will be number of rows of a SPE. Example, if value of payload size is 3/9, it means packetizer will assemble payload data of 3 rows of a SPE,  so value of the field //will be 3.| `RW`| `0x0`| `0x0 End: Begin:`|

###Thalassa PDHPW Payload Assemble uP Address Convert Control

* **Description**           

These registers are used to enable converting mode for uP address as accessing into some RAMs which request to convert identification.


* **RTL Instant Name**    : `up_add_conv_ctrl`

* **Address**             : `0x00000000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`pdhpwplaasmcpuadrconvenctrl`| Enable uP address converting 1: enable 0: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Thalassa PDHPW Payload Assemble Engine Status

* **Description**           

These registers are used to save status in the engine.


* **RTL Instant Name**    : `txeth_enable_ctrl`

* **Address**             : `0x00028000-0x0002829F #The address format for these registers is 0x0028000 + $PWID`

* **Formula**             : `0x00028000 +  $PWID`

* **Where**               : 

    * `$PWID(0-672): Pseudowire ID`

* **Width**               : `96`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[95:79]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[78]`|`pdhpwplaasmaal1ptrsetsta`| indicate that the pointer is already set in that AAL1 cycle 1: enable 0: disable| `RW`| `0x0`| `0x0`|
|`[77:76]`|`pdhpwplaasmsignumstartsta`| indicate number of signaling bytes to append into the last payload bytes to become a 4-bytes data valid.| `RW`| `0x0`| `0x0`|
|`[75:71]`|`pdhpwplaasmgensigcntsta`| indicate number of the remaining signaling bytes which need to be append into PW packet.| `RW`| `0x0`| `0x0`|
|`[70:65]`|`pdhpwplaasmpdunumcntsta`| indicate the current number of the PDU counter in AAL1 mode| `RW`| `0x0`| `0x0`|
|`[64:62]`|`pdhpwplaasmpducylcntsta`| indicate the current number of the cycle PDU counter in AAL1 mode| `RW`| `0x0`| `0x0`|
|`[61:58]`|`pdhpwplaasmpdubytecntsta`| indicate the current number of bytes of a PDU in AAL1 mode| `RW`| `0x0`| `0x0`|
|`[57:43]`|`pdhpwplaasmsatopcesbytecntsta`| indicate number of bytes in Satop and CES mode With Satop mode number of bytes of the payload in PW packet With CES mode [57:49]  indicate the current number of NxDS0 counter in CES mode [48:43]  indicate the current number of DS0 byte counter in CES mode With basic CEP mode: number of bytes of the payload in PW packet With fractional CEP mode [57:49] indicate the current number of bytes of actual payload in PW packet [48:43] indicate the current row of payload in PW packet| `RW`| `0x0`| `0x0`|
|`[42:40]`|`pdhpwplaasmdatanumsta`| indicate the current number of data bytes in the data buffer| `RW`| `0x0`| `0x0`|
|`[39:0]`|`pdhpwplaasmdatabufsta`| as data buffer to save data with the maximum 5 bytes| `RW`| `0x0`| `0x0 End:`|

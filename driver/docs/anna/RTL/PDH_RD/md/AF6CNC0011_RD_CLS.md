## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_CLS
####Register Table

|Name|Address|
|-----|-----|
|`Ethernet DA bit47_32 Configuration`|`0x_0000`|
|`Ethernet DA bit31_00 Configuration`|`0x_0001`|
|`Ethernet SA bit31_00 Configuration`|`0x_0002`|
|`Ethernet Sub_Type Configuration`|`0x_0003`|
|`Classified Ethernet field mask  Configuration`|`0x_0004`|
|`Dump packet  Configuration`|`0x_0006`|
|`Ethernet Encapsulation EtheType and EtherLength  Configuration`|`0x1000 - 0x100F`|


###Ethernet DA bit47_32 Configuration

* **Description**           

This register  is used to configure the DA bit47_32


* **RTL Instant Name**    : `upen_dasa`

* **Address**             : `0x_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`da_bit47_32`| DA bit47_32| `R/W`| `0xCAFE`| `0xCAFE`|
|`[15:00]`|`sa_bit47_32`| SA bit47_32| `R/W`| `0xABCD`| `0xABCD End: Begin:`|

###Ethernet DA bit31_00 Configuration

* **Description**           

This register  is used to configure the DA bit32_00


* **RTL Instant Name**    : `upen_da31`

* **Address**             : `0x_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`da_bit31_00`| DA bit31_00| `R/W`| `0xCAFE_CAFE`| `0xCAFE_CAFE End: Begin:`|

###Ethernet SA bit31_00 Configuration

* **Description**           

This register  is used to configure the SA bit31_00


* **RTL Instant Name**    : `upen_sa31`

* **Address**             : `0x_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`sa_bit31_00`| SA bit31_00| `R/W`| `0xABCD_ABCD`| `0xABCD_ABCD End: Begin:`|

###Ethernet Sub_Type Configuration

* **Description**           

This register  is used to configure E-type and Sub_Type


* **RTL Instant Name**    : `upen_type`

* **Address**             : `0x_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`sub_type_mask`| Bit enable to mask a sub-type bit| `R/W`| `0x80`| `0x80`|
|`[23:16]`|`sub_type_value`| Ethernet SubType of control packet 0thers 	: data frame 0x80-0xFF	: control frames| `R/W`| `0x80`| `0x80`|
|`[15:00]`|`e_type`| E_type of pkt| `R/W`| `0xFFFF`| `0xFFFF End: Begin:`|

###Classified Ethernet field mask  Configuration

* **Description**           

This register  is used to mask some field of packet. These mask field are checked to discard or forward pkt


* **RTL Instant Name**    : `upen_mask`

* **Address**             : `0x_0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`e_type_mask`| mask E_type of packet| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`sa_mask`| mask SA of packet| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`da_mask`| mask DA of packet| `R/W`| `0x0`| `0x0 End: Begin:`|

###Dump packet  Configuration

* **Description**           

This register  is used to mask some field of packet. These mask field are checked to discard or forward pkt


* **RTL Instant Name**    : `upen_dump`

* **Address**             : `0x_0006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[02:01]`|`dump_typ`| Dump pkt type 00 : dump pkt input 01 : dump pkt data 10 : dump pkt control 11 : dump pkt data & control| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`dump_enb`| dump pkt enable| `R/W`| `0x0`| `0x0 End: Begin:`|

###Ethernet Encapsulation EtheType and EtherLength  Configuration

* **Description**           

This register is used to read classified packet counters , support both mode r2c and ro

Counter ID detail :

+ 0 	: data    packet counter

+ 1 	: control packet counter

+ 2 	: total  discard packet counter

+ 3		: da err discard packet counter

+ 4		: sa err discard packet counter

+ 5		: etype err discard packet counter

+ 6		: fcs_err discard packet counter

+ 7		: unused

r2c		: enable mean read to clear


* **RTL Instant Name**    : `upen_count`

* **Address**             : `0x1000 - 0x100F`

* **Formula**             : `0x1000 + $r2c*8 + $id`

* **Where**               : 

    * `$r2c(0-1) : r2c bit`

    * `$id(0-7) : Counter ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`ether_type`| Counter value| `R/W`| `0x0`| `0x0 End:`|

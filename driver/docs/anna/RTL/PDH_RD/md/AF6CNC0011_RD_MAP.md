## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_MAP
####Register Table

|Name|Address|
|-----|-----|
|`Demap Channel Control`|`0x000000-0x003FFF`|
|`Map Channel Control`|`0x014000-0x017FFF`|
|`Map Line Control`|`0x010000-0x0103FF`|
|`Map IDLE Code`|`0x018200`|
|`Thalassa Bert Force Single Bit Error`|`0x008500 - 0x008509`|


###Demap Channel Control

* **Description**           

The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number.


* **RTL Instant Name**    : `demap_channel_ctrl`

* **Address**             : `0x000000-0x003FFF`

* **Formula**             : `0x000000 + 672*stsid + 96*vtgid + 32*vtid + slotid`

* **Where**               : 

    * `$stsid(0-23): is STS identification number)`

    * `$vtgid(0-6): is VT Group identification number`

    * `$vtid(0-3): is VT identification number`

    * `$slotid(0-31): is time slot number. It varies from 0 to 23 in DS1 and from 0 to 31 in E1`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14]`|`demapfirstts`| First timeslot indication. Use for CESoP mode only| `RW`| `0x0`| `0x0`|
|`[13:11]`|`demapchanneltype`| Specify which type of mapping for this. Specify which type of mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: ATM over DS1/ E1, SONET/SDH (unused) 3: IMA over DS1/E1, SONET/SDH(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: Reserve| `RW`| `0x0`| `0x0`|
|`[10]`|`demapchen`| PW/Channels Enable| `RW`| `0x0`| `0x0`|
|`[9:0]`|`demappwidfield`| PW ID field that uses for Encapsulation| `RW`| `0x0`| `0x0 End: Begin:`|

###Map Channel Control

* **Description**           

The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number.


* **RTL Instant Name**    : `map_channel_ctrl`

* **Address**             : `0x014000-0x017FFF`

* **Formula**             : `0x014000 + 672*stsid + 96*vtgid + 32*vtid + slotid`

* **Where**               : 

    * `$stsid(0-23): is STS identification number)`

    * `$vtgid(0-6): is VT Group identification number`

    * `$vtid(0-3): is VT identification number`

    * `$slotid(0-31): is time slot number. It varies from 0 to 23 in DS1 and from 0 to 31 in E1`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14]`|`mapfirstts`| First timeslot indication. Use for CESoP mode only| `RW`| `0x0`| `0x0`|
|`[13:11]`|`mapchanneltype`| Specify which type of mapping for this. Specify which type of mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP encapsulation from DS1/E1 2: CESoP with CAS 3: ATM(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7: Reserve| `RW`| `0x0`| `0x0`|
|`[10]`|`mapchen`| PW/Channels Enable| `RW`| `0x0`| `0x0`|
|`[9:0]`|`mappwidfield`| PW ID field that uses for Encapsulation.| `RW`| `0x0`| `0x0 End: Begin:`|

###Map Line Control

* **Description**           

The registers provide the per line configurations for STS/VT/DS1/E1 line.


* **RTL Instant Name**    : `map_line_ctrl`

* **Address**             : `0x010000-0x0103FF`

* **Formula**             : `0x010000 + 32*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$stsid(0-23): is STS identification number`

    * `$vtgid(0-6): is VT Group identification number`

    * `$vtid(0-3): is VT identification number`

* **Width**               : `20`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:17]`|`mapds1lcen`| DS1E1 Loop code insert enable 0: Disable 1: CSU Loop up 2: CSU Loop down 3: FA1 Loop up 4: FA1 Loop down 5: FA2 Loop up 6: FA2 Loop down| `RW`| `0x0`| `0x0`|
|`[16:15]`|`mapframetype`| depend on MapSigType[3:0], the MapFrameType[1:0] bit field can have difference meaning. 0: DS1 SF/E1 BF/E3 G.751(unused) /DS3 framing(unused)/VT1.5,VT2,VT6 normal/STS no POH, no Stuff 1: DS1 ESF/E1 CRC/E3 G.832(unused)/DS1,E1,VT1.5,VT2 fractional 2: CEP DS3/E3/VC3 fractional 3:CEP basic mode or VT with POH/STS with POH, Stuff/DS1,E1,DS3,E3 unframe mode| `RW`| `0x0`| `0x0`|
|`[14:11]`|`mapsigtype`| 0: DS1 1: E1 2: DS3 (unused) 3: E3 (unused) 4: VT 1.5 5: VT 2 6: unused 7: unused 8: VC3 9: VC4 10: STS12c/VC4_4c 12: TU3| `RW`| `0x0`| `0x0`|
|`[10]`|`maptimesrcmaster`| This bit is used to indicate the master timing or the master VC3 in VC4/VC4-Xc| `RW`| `0x0`| `0x0`|
|`[9:0]`|`maptimesrcid`| The reference line ID used for timing reference or the master VC3 ID in VC4/VC4-Xc| `RW`| `0x0`| `0x0 End: Begin:`|

###Map IDLE Code

* **Description**           

The registers provide the idle pattern


* **RTL Instant Name**    : `map_idle_code`

* **Address**             : `0x018200`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`idlecode`| Idle code| `RW`| `0x0`| `0x0 End: Begin:`|

###Thalassa Bert Force Single Bit Error

* **Description**           

The registers provide the threshold  for force bit err.


* **RTL Instant Name**    : `Bert_Force_Single_Bit_Error`

* **Address**             : `0x008500 - 0x008509`

* **Formula**             : `0x008500 + Ber_level`

* **Where**               : 

    * `$Ber_level(3 - 9):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`ber_threshold`| Threshold HW force one bit error.| `RW`| `0x0`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_PDH_MDLPRM
####Register Table

|Name|Address|
|-----|-----|
|`CONFIG ENABLE MONITOR FOR DEBUG BOARD`|`0x14001`|
|`CONFIG ID TO MONITOR FOR DEBUG BOARD`|`0x14002`|
|`CONFIG PRM BIT C/R & STANDARD Tx`|`0x1000 - 0x153F`|
|`CONFIG PRM BIT L/B Tx`|`0x1800 - 0x1D3F`|
|`CONFIG CONTROL STUFF 0`|`0x3000`|
|`COUNTER BYTE MESSAGE TX PRM`|`0x4000 - 0x4D3F`|
|`COUNTER PACKET MESSAGE TX PRM`|`0x5000 - 0x5D3F`|
|`COUNTER VALID INFO TX PRM`|`0x6000 - 0x6D3F`|
|`Buff Message MDL IDLE1`|`0x0000 - 0x012F`|
|`Buff Message MDL IDLE2`|`0x0130 - 0x01C7`|
|`SET TO HAVE PACKET MDL BUFFER 1`|`0x0420 - 0x437`|
|`CONFIG MDL Tx`|`0x0460 - 0x477`|
|`COUNTER BYTE MESSAGE TX MDL IDLE`|`0x0700 - 0x0797`|
|`COUNTER BYTE MESSAGE TX MDL PATH`|`0x0720 - 0x07B7`|
|`COUNTER BYTE MESSAGE TX MDL TEST`|`0x0740 - 0x07D7`|
|`COUNTER VALID MESSAGE TX MDL IDLE`|`0x0600 - 0x0697`|
|`COUNTER VALID MESSAGE TX MDL PATH`|`0x0620 - 0x06B7`|
|`COUNTER VALID MESSAGE TX MDL TEST`|`0x0640 - 0x06D7`|
|`Config Buff Message PRM STANDARD`|`0x08000 - 0x0853F`|
|`CONFIG CONTROL RX PRM MONITOR`|`0xA042`|
|`BUFFER RX PRM MONITOR MESSAGE`|`0x08800 - 0x00887F`|
|`Buff Message MDL with configuration type1`|`0x9800 - 0x09AF3`|
|`Config Buff Message MDL TYPE`|`0x0A000 - 0x0A017`|
|`CONFIG CONTROL RX DESTUFF 0`|`0xA041`|
|`STICKY TO RECEIVE PACKET MDL BUFF CONFIG1`|`0x0A045`|
|`COUNTER GOOD MESSAGE RX PRM`|`0xC000 - 0xCD3F`|
|`COUNTER DROP MESSAGE RX PRM`|`0xD000 - 0xDD3F`|
|`COUNTER MISS MESSAGE RX PRM`|`0xE000 - 0xED3F`|
|`COUNTER BYTE MESSAGE RX PRM`|`0xF000 - 0xFD3F`|
|`COUNTER BYTE MESSAGE RX MDL IDLE`|`0xB400 - 0xB497`|
|`COUNTER BYTE MESSAGE RX MDL PATH`|`0xB420 - 0xB4B7`|
|`COUNTER BYTE MESSAGE RX MDL TEST`|`0xB440 - 0xB4D7`|
|`COUNTER GOOD MESSAGE RX MDL IDLE`|`0xB000 - 0xB117`|
|`COUNTER GOOD MESSAGE RX MDL PATH`|`0xB020 - 0xB137`|
|`COUNTER GOOD MESSAGE RX MDL TEST`|`0xB040 - 0xB157`|
|`COUNTER DROP MESSAGE RX MDL IDLE`|`0xB060 - 0xB177`|
|`COUNTER DROP MESSAGE RX MDL PATH`|`0xB080 - 0xB197`|
|`COUNTER DROP MESSAGE RX MDL TEST`|`0xB0A0 - 0xB1B7`|
|`PRM LB per Channel Interrupt Enable Control`|`0x1A000-0x1A3FF`|
|`PRM LB Interrupt per Channel Interrupt Status`|`0x1A400-0x1A7FF`|
|`PRM LB Interrupt per Channel Current Status`|`0x1A800-0x1ABFF`|
|`PRM LB Interrupt per Channel Interrupt OR Status`|`0x1AC00-0x1AC20`|
|`PRM LB Interrupt OR Status`|`0x1AFFF`|
|`PRM LB Interrupt Enable Control`|`0x1AFFE`|
|`MDL per Channel Interrupt Enable Control`|`0xA100-0xA11F`|
|`MDL Interrupt per Channel Interrupt Status`|`0xA120-0xA13F`|
|`MDL Interrupt per Channel Current Status`|`0xA140-0xA15F`|
|`MDL Interrupt per Channel Interrupt OR Status`|`0xA160-0xA161`|
|`MDL Interrupt OR Status`|`0xA17F`|
|`MDL Interrupt Enable Control`|`0xA17E`|


###CONFIG ENABLE MONITOR FOR DEBUG BOARD

* **Description**           

config enable monitor data before stuff


* **RTL Instant Name**    : `upen_cfg_debug`

* **Address**             : `0x14001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`out_cfg_bar`| reserve| `R/W`| `0x0`| `0x0`|
|`[18:18]`|`out_cfg_selinfo1`| select just 8 bit INFO from PRM FORCE but except 2 bit NmNi, (1) is enable, (0) is disable| `R/W`| `0x0`| `0x0`|
|`[17:17]`|`cfg_loopaback_mdl`| loop back MDL DS3, (1) is enable, (0) is disable| `R/W`| `0x0`| `0x0`|
|`[16:16]`|`cfg_loopaback_prm`| loop back PRM DS1, (1) is enable, (0) is disable| `R/W`| `0x0`| `0x0`|
|`[15:08]`|`out_info_force`| 10 bits info force [15:13] : 3'd1 : to set bit G1 3'd2 :  to set bit G2 3'd3 :  to set bit G3 3'd4 : to set bit G4 3'd5 : to set bit G5 3'd6 :  to set bit G6 [12] : to set bit SE [11] : to set bit FE [10] : to set bit LV [9] : to set bit SL [8] : to set bit LB| `R/W`| `0x0`| `0x0`|
|`[07:01]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`out_cfg_dis`| disable monitor, reset address buffer monitor, (1) is disable, (0) is enable| `R/W`| `0x1`| `0x1 End: Begin:`|

###CONFIG ID TO MONITOR FOR DEBUG BOARD

* **Description**           

config ID to monitor tx message


* **RTL Instant Name**    : `upen_cfgid_mon`

* **Address**             : `0x14002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[09:00]`|`out_cfgid_mon`| ID which is configured to monitor| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG PRM BIT C/R & STANDARD Tx

* **Description**           

Config PRM bit C/R & Standard


* **RTL Instant Name**    : `upen_prm_txcfgcr`

* **Address**             : `0x1000 - 0x153F`

* **Formula**             : `0x1000+ $STSID*32 + *$VTGID*4 + $VTID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[03:03]`|`cfg_prmen_tx`| config enable Tx, (0) is disable (1) is enable| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`cfg_prmfcs_tx`| config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`cfg_prmstd_tx`| config standard Tx, (0) is ANSI, (1) is AT&T| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfg_prm_cr`| config bit command/respond PRM message| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG PRM BIT L/B Tx

* **Description**           

CONFIG PRM BIT L/B Tx


* **RTL Instant Name**    : `upen_prm_txcfglb`

* **Address**             : `0x1800 - 0x1D3F`

* **Formula**             : `0x1800+$STSID*32 + $VTGID*4 + $VTID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `2`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[01:01]`|`cfg_prm_enlb`| config enable CPU config LB bit, (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfg_prm_lb`| config bit Loopback PRM message| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG CONTROL STUFF 0

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_cfg_ctrl0`

* **Address**             : `0x3000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:07]`|`out_txcfg_ctrl0`| reserve| `R/W`| `0x0`| `0x0`|
|`[06:02]`|`reserve1`| reserve1| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fcsinscfg`| (1) enable insert FCS, (0) disable| `R/W`| `0x1`| `0x1`|
|`[00:00]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE TX PRM

* **Description**           

counter byte message PRM


* **RTL Instant Name**    : `upen_txprm_cnt_byte`

* **Address**             : `0x4000 - 0x4D3F`

* **Formula**             : `0x4000 + $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 `

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_byte_prm`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER PACKET MESSAGE TX PRM

* **Description**           

counter packet message PRM


* **RTL Instant Name**    : `upen_txprm_cnt_pkt`

* **Address**             : `0x5000 - 0x5D3F`

* **Formula**             : `0x5000+ $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 `

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `15`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14:00]`|`cnt_pkt_prm`| value counter packet| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER VALID INFO TX PRM

* **Description**           

counter valid info PRM


* **RTL Instant Name**    : `upen_txprm_cnt_info`

* **Address**             : `0x6000 - 0x6D3F`

* **Formula**             : `0x6000 + $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 `

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `15`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14:00]`|`cnt_vld_prm_info`| value counter valid info| `R/W`| `0x0`| `0x0 End: Begin:`|

###Buff Message MDL IDLE1

* **Description**           

config message MDL IDLE Channel ID 0-15


* **RTL Instant Name**    : `upen_mdl_idle1`

* **Address**             : `0x0000 - 0x012F`

* **Formula**             : `0x0000+ $DWORDID*16 + $DE3ID1`

* **Where**               : 

    * `$DWORDID (0-18) : DWORD ID`

    * `$DE3ID1(0-15)  : DE3 ID1`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`idle_byte13`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`ilde_byte12`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`idle_byte11`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`idle_byte10`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###Buff Message MDL IDLE2

* **Description**           

config message MDL IDLE Channel ID 16-23


* **RTL Instant Name**    : `upen_mdl_idle2`

* **Address**             : `0x0130 - 0x01C7`

* **Formula**             : `0x0130+ $DWORDID*8 + $DE3ID2`

* **Where**               : 

    * `$DWORDID (0-18) : DWORD ID`

    * `$DE3ID2 (0-7) : DE3 ID2`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`idle_byte23`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`ilde_byte22`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`idle_byte21`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`idle_byte20`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###SET TO HAVE PACKET MDL BUFFER 1

* **Description**           

SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1


* **RTL Instant Name**    : `upen_sta_idle_alren`

* **Address**             : `0x0420 - 0x437`

* **Formula**             : `0x0420+$DE3ID`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

* **Width**               : `1`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00:00]`|`idle_cfgen`| (0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 0| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG MDL Tx

* **Description**           

Config Tx MDL


* **RTL Instant Name**    : `upen_cfg_mdl`

* **Address**             : `0x0460 - 0x477`

* **Formula**             : `0x0460+$DE3ID`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`cfg_seq_tx`| config enable Tx continous, (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`cfg_entx`| config enable Tx, (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`cfg_fcs_tx`| config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`cfg_mdlstd_tx`| config standard Tx, (0) is ANSI, (1) is AT&T| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfg_mdl_cr`| config bit command/respond MDL message, bit 0 is channel 0 of DS3| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE TX MDL IDLE

* **Description**           

counter byte  IDLE message MDL


* **RTL Instant Name**    : `upen_txmdl_cnt_byteidle`

* **Address**             : `0x0700 - 0x0797`

* **Formula**             : `0x0700+ $DE3ID + $UPRO * 128`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_byte_idle_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE TX MDL PATH

* **Description**           

counter byte  PATH message MDL


* **RTL Instant Name**    : `upen_txmdl_cnt_bytepath`

* **Address**             : `0x0720 - 0x07B7`

* **Formula**             : `0x0720+ $DE3ID + $UPRO * 128`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_byte_path_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE TX MDL TEST

* **Description**           

counter byte  TEST message MDL


* **RTL Instant Name**    : `upen_txmdl_cnt_bytetest`

* **Address**             : `0x0740 - 0x07D7`

* **Formula**             : `0x0740+ $DE3ID + $UPRO * 128`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_byte_test_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER VALID MESSAGE TX MDL IDLE

* **Description**           

counter valid  IDLE message MDL


* **RTL Instant Name**    : `upen_txmdl_cnt_valididle`

* **Address**             : `0x0600 - 0x0697`

* **Formula**             : `0x0600+ $DE3ID + $UPRO * 128`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_valid_idle_mdl`| value counter valid| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER VALID MESSAGE TX MDL PATH

* **Description**           

counter valid PATH message MDL


* **RTL Instant Name**    : `upen_txmdl_cnt_validpath`

* **Address**             : `0x0620 - 0x06B7`

* **Formula**             : `0x0620+ $DE3ID + $UPRO * 128`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_valid_path_mdl`| value counter valid| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER VALID MESSAGE TX MDL TEST

* **Description**           

counter valid  TEST message MDL


* **RTL Instant Name**    : `upen_txmdl_cnt_validtest`

* **Address**             : `0x0640 - 0x06D7`

* **Formula**             : `0x0640+ $DE3ID + $UPRO * 128`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_valid_test_mdl`| value counter valid| `R/W`| `0x0`| `0x0 End: Begin:`|

###Config Buff Message PRM STANDARD

* **Description**           

config standard message PRM


* **RTL Instant Name**    : `upen_rxprm_cfgstd`

* **Address**             : `0x08000 - 0x0853F`

* **Formula**             : `0x08000+ $STSID*32 + *$VTGID*4 + $VTID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[03:03]`|`cfg_prm_rxen`| config enable RX PRM, (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`cfg_prm_cr`| config C/R bit expected| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`cfg_prmfcs_rx`| config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfg_std_prm`| config standard   (0) is ANSI, (1) is AT&T| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG CONTROL RX PRM MONITOR

* **Description**           

config control DeStuff global


* **RTL Instant Name**    : `upen_prm_ctrl`

* **Address**             : `0xA042`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[13:04]`|`prmid_cfg`| PRM ID config monitor,<br>{stsid,vtgid,vtid}| `R/W`| `0x0`| `0x0`|
|`[03:01]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`prm_mon_en`| (1) : enable monitor, (0) disable| `R/W`| `0x0`| `0x0 End: Begin:`|

###BUFFER RX PRM MONITOR MESSAGE

* **Description**           

config standard message PRM


* **RTL Instant Name**    : `upen_rxprm_mon`

* **Address**             : `0x08800 - 0x00887F`

* **Formula**             : `0x08800+ $LOC`

* **Where**               : 

    * `$LOC (0-127) : LOC ID`

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[07:00]`|`val_mes`| value message of PRM| `R/W`| `0x0`| `0x0 End: Begin:`|

###Buff Message MDL with configuration type1

* **Description**           

buffer message MDL which is configured type


* **RTL Instant Name**    : `upen_rxmdl_typebuff1`

* **Address**             : `0x9800 - 0x09AF3`

* **Formula**             : `0x9800 + $STSID* 32 + $RXDWORDID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$RXDWORDID (0-19) : RX Double WORD ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`mdl_tbyte3`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`mdl_tbyte2`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`mdl_tbyte1`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`mdl_tbyte0`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###Config Buff Message MDL TYPE

* **Description**           

config type message MDL


* **RTL Instant Name**    : `upen_rxmdl_cfgtype`

* **Address**             : `0x0A000 - 0x0A017`

* **Formula**             : `0x0A000+$MDLID`

* **Where**               : 

    * `$MDLID(0-23)  : DS3 E3 MDL ID`

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`cfg_fcs_rx`| config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`cfg_mdl_cr`| config C/R bit expected| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`cfg_mdl_std`| (0) is ANSI, (1) is AT&T| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`cfg_mdl_mask_test`| config enable mask to moitor test massage (1): enable, (0): disable| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`cfg_mdl_mask_path`| config enable mask to moitor path massage (1): enable, (0): disable| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfg_mdl_mask_idle`| config enable mask to moitor idle massage (1): enable, (0): disable| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG CONTROL RX DESTUFF 0

* **Description**           

config control DeStuff global


* **RTL Instant Name**    : `upen_destuff_ctrl0`

* **Address**             : `0xA041`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`cfg_crmon`| (0) : disable, (1) : enable| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`reserve1`| reserve| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fcsmon`| (0) : disable monitor, (1) : enable| `R/W`| `0x1`| `0x1`|
|`[02:01]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`headermon_en`| (1) : enable monitor, (0) disable| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF CONFIG1

* **Description**           

sticky row for buffer of type message is configured 0-31


* **RTL Instant Name**    : `upen_mdl_stk_cfg1`

* **Address**             : `0x0A045`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `24`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:00]`|`stk_cfg_alr1`| sticky for buffer of type message is configured 0-23| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER GOOD MESSAGE RX PRM

* **Description**           

counter good message PRM


* **RTL Instant Name**    : `upen_rxprm_gmess`

* **Address**             : `0xC000 - 0xCD3F`

* **Formula**             : `0xC000 + $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 `

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `15`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14:00]`|`cnt_gmess_prm`| value counter packet| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER DROP MESSAGE RX PRM

* **Description**           

counter drop message PRM


* **RTL Instant Name**    : `upen_rxprm_drmess`

* **Address**             : `0xD000 - 0xDD3F`

* **Formula**             : `0xD000 + $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 `

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `15`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14:00]`|`cnt_drmess_prm`| value counter packet| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER MISS MESSAGE RX PRM

* **Description**           

counter miss message PRM


* **RTL Instant Name**    : `upen_rxprm_mmess`

* **Address**             : `0xE000 - 0xED3F`

* **Formula**             : `0xE000 + $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 `

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `15`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14:00]`|`cnt_mmess_prm`| value counter packet| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE RX PRM

* **Description**           

counter byte message PRM


* **RTL Instant Name**    : `upen_rxprm_cnt_byte`

* **Address**             : `0xF000 - 0xFD3F`

* **Formula**             : `0xF000 + $STSID*32 + $VTGID*4 + $VTID + $UPRO * 1024 `

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_byte_rxprm`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE RX MDL IDLE

* **Description**           

counter byte IDLE message MDL


* **RTL Instant Name**    : `upen_rxmdl_cnt_byteidle`

* **Address**             : `0xB400 - 0xB497`

* **Formula**             : `0xB400+ $MDLID + $UPRO * 128`

* **Where**               : 

    * `$MDLID (0-23) : MDL ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_byte_idle_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE RX MDL PATH

* **Description**           

counter byte PATH message MDL


* **RTL Instant Name**    : `upen_rxmdl_cnt_bytepath`

* **Address**             : `0xB420 - 0xB4B7`

* **Formula**             : `0xB420+ $MDLID + $UPRO * 128`

* **Where**               : 

    * `$MDLID (0-23) : MDL ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_byte_path_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE RX MDL TEST

* **Description**           

counter byte TEST message MDL


* **RTL Instant Name**    : `upen_rxmdl_cnt_bytetest`

* **Address**             : `0xB440 - 0xB4D7`

* **Formula**             : `0xB440+ $MDLID + $UPRO * 128`

* **Where**               : 

    * `$MDLID (0-23) : MDL ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_byte_test_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER GOOD MESSAGE RX MDL IDLE

* **Description**           

counter good IDLE message MDL


* **RTL Instant Name**    : `upen_rxmdl_cnt_goodidle`

* **Address**             : `0xB000 - 0xB117`

* **Formula**             : `0xB000+ $MDLID + $UPRO * 256`

* **Where**               : 

    * `$MDLID (0-23) : MDL ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_good_idle_mdl`| value counter good| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER GOOD MESSAGE RX MDL PATH

* **Description**           

counter good PATH message MDL


* **RTL Instant Name**    : `upen_rxmdl_cnt_goodpath`

* **Address**             : `0xB020 - 0xB137`

* **Formula**             : `0xB020+ $MDLID + $UPRO * 256`

* **Where**               : 

    * `$MDLID (0-23) : MDL ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_good_path_mdl`| value counter good| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER GOOD MESSAGE RX MDL TEST

* **Description**           

counter good TEST message MDL


* **RTL Instant Name**    : `upen_rxmdl_cnt_goodtest`

* **Address**             : `0xB040 - 0xB157`

* **Formula**             : `0xB040+ $MDLID + $UPRO * 256`

* **Where**               : 

    * `$MDLID (0-23) : MDL ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_good_test_mdl`| value counter good| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER DROP MESSAGE RX MDL IDLE

* **Description**           

counter drop IDLE message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_dropidle`

* **Address**             : `0xB060 - 0xB177`

* **Formula**             : `0xB060+ $MDLID + $UPRO * 256`

* **Where**               : 

    * `$MDLID (0-23) : MDL ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_drop_idle_mdl`| value counter drop| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER DROP MESSAGE RX MDL PATH

* **Description**           

counter drop PATH message MDL


* **RTL Instant Name**    : `upen_rxmdl_cnt_droppath`

* **Address**             : `0xB080 - 0xB197`

* **Formula**             : `0xB080+ $MDLID + $UPRO * 256`

* **Where**               : 

    * `$MDLID (0-23) : MDL ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_drop_path_mdl`| value counter drop| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER DROP MESSAGE RX MDL TEST

* **Description**           

counter drop TEST message MDL


* **RTL Instant Name**    : `upen_rxmdl_cnt_droptest`

* **Address**             : `0xB0A0 - 0xB1B7`

* **Formula**             : `0xB0A0+ $MDLID + $UPRO * 256`

* **Where**               : 

    * `$MDLID (0-23) : MDL ID`

    * `$UPRO (0-1) : upen read only`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_drop_test_mdl`| value counter drop| `R/W`| `0x0`| `0x0 End: Begin:`|

###PRM LB per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable


* **RTL Instant Name**    : `prm_cfg_lben_int`

* **Address**             : `0x1A000-0x1A3FF`

* **Formula**             : `0x1A000+ $STSID*32 + $VTGID*4 + $VTID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`prm_cfglben_int1`| Set 1 to enable change event to generate an interrupt slice 1| `RW`| `0x0`| `0x0`|
|`[0]`|`prm_cfglben_int0`| Set 1 to enable change event to generate an interrupt slice 0| `RW`| `0x0`| `0x0 End: Begin:`|

###PRM LB Interrupt per Channel Interrupt Status

* **Description**           

This is the per Channel interrupt status.


* **RTL Instant Name**    : `prm_lb_int_sta`

* **Address**             : `0x1A400-0x1A7FF`

* **Formula**             : `0x1A400+ $STSID*32 + $VTGID*4 + $VTID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `2`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`prm_lbint_sta1`| Set 1 if there is a change event slice 1| `RW`| `0x0`| `0x0`|
|`[0]`|`prm_lbint_sta0`| Set 1 if there is a change event slice 0| `RW`| `0x0`| `0x0 End: Begin:`|

###PRM LB Interrupt per Channel Current Status

* **Description**           

This is the per Channel Current status.


* **RTL Instant Name**    : `prm_lb_int_crrsta`

* **Address**             : `0x1A800-0x1ABFF`

* **Formula**             : `0x1A800+ $STSID*32 + $VTGID*4 + $VTID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `2`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`prm_lbint_crrsta1`| Current status of event slice 1| `RW`| `0x0`| `0x0`|
|`[0]`|`prm_lbint_crrsta0`| Current status of event slice 0| `RW`| `0x0`| `0x0 End: Begin:`|

###PRM LB Interrupt per Channel Interrupt OR Status

* **Description**           




* **RTL Instant Name**    : `prm_lb_intsta`

* **Address**             : `0x1AC00-0x1AC20`

* **Formula**             : `0x1AC00 +  $GID`

* **Where**               : 

    * `$GID (0-31) : group ID`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`prm_lbintsta`| Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###PRM LB Interrupt OR Status

* **Description**           

The register consists of 2 bits. Each bit is used to store Interrupt OR status.


* **RTL Instant Name**    : `prm_lb_sta_int`

* **Address**             : `0x1AFFF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`prm_lbsta_int`| Set to 1 if any interrupt status bit is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###PRM LB Interrupt Enable Control

* **Description**           

The register consists of 2 interrupt enable bits .


* **RTL Instant Name**    : `prm_lb_en_int`

* **Address**             : `0x1AFFE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`prm_lben_int`| Set to 1 to enable to generate interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable


* **RTL Instant Name**    : `mdl_cfgen_int`

* **Address**             : `0xA100-0xA11F`

* **Formula**             : `0xA100 +  $MDLDE3ID`

* **Where**               : 

    * `$MDLDE3ID (0-23) : DE3 ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mdl_cfgen_int`| Set 1 to enable change event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt per Channel Interrupt Status

* **Description**           

This is the per Channel interrupt status.


* **RTL Instant Name**    : `mdl_int_sta`

* **Address**             : `0xA120-0xA13F`

* **Formula**             : `0xA120 +  $MDLDE3ID + $LINEID * 32`

* **Where**               : 

    * `$MDLDE3ID (0-23) : DE3 ID`

    * `$LINEID(0-1) : LINE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mdlint_sta`| Set 1 if there is a change event.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt per Channel Current Status

* **Description**           

This is the per Channel Current status.


* **RTL Instant Name**    : `mdl_int_crrsta`

* **Address**             : `0xA140-0xA15F`

* **Formula**             : `0xA140 +  $MDLDE3ID + $LINEID * 32`

* **Where**               : 

    * `$MDLDE3ID (0-23) : DE3 ID`

    * `$LINEID(0-1) : LINE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mdlint_crrsta`| Current status of event.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt per Channel Interrupt OR Status

* **Description**           

The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1.


* **RTL Instant Name**    : `mdl_intsta`

* **Address**             : `0xA160-0xA161`

* **Formula**             : `0xA160 +  $GID`

* **Where**               : 

    * `$GID (0-1) : group ID`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`mdlintsta`| Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt OR Status

* **Description**           

The register consists of 2 bits. Each bit is used to store Interrupt OR status.


* **RTL Instant Name**    : `mdl_sta_int`

* **Address**             : `0xA17F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`mdlsta_int`| Set to 1 if any interrupt status bit is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt Enable Control

* **Description**           

The register consists of 2 interrupt enable bits .


* **RTL Instant Name**    : `mdl_en_int`

* **Address**             : `0xA17E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`mdlen_int`| Set to 1 to enable to generate interrupt.| `RW`| `0x0`| `0x0 End:`|

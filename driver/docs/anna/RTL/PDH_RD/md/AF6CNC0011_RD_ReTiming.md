## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_ReTiming
####Register Table

|Name|Address|
|-----|-----|
|`SSM Engine Status`|`0x00050200`|
|`SSM Engine Status`|`0x00050201`|
|`SSM Engine Status`|`0x00050202`|
|`Tx SSM Framer Control`|`0x00010000 - 0x00010053`|
|`Tx SSM Framer Control`|`0x00010600 - 0x00010653`|
|`Rx SSM Framer Control`|`0x0001C000 - 0x0001C053`|
|`SSM Slip Buffer Data Sticky`|`0x00014001`|
|`SSM Slip Buffer Frame Sticky`|`0x00014002`|
|`SSM Slip Buffer Data Sticky`|`0x00014003`|
|`SSM Slip Buffer Frame Sticky`|`0x00014004`|
|`SSM Slip Buffer Data Sticky`|`0x00014005`|
|`SSM Slip Buffer Frame Sticky`|`0x00014006`|
|`SSM Slip Buffer Frame Sticky`|`0x00014100-0x00014153`|
|`Rx SSM Framer HW Status`|`0x0001C400 - 0x0001C053`|
|`Rx SSM Framer CRC Error Counter`|`0x0001E800 - 0x0001E8FF`|
|`Rx SSM Framer REI Counter`|`0x0001F000 - 0x0001F0FF`|
|`Rx SSM Framer FBE Counter`|`0x0001E000 - 0x0001E0FF`|


###SSM Engine Status

* **Description**           

These registers are used to enable SSM engine for line 31 to 0


* **RTL Instant Name**    : `ssmsta_pen`

* **Address**             : `0x00050200`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ssmengsta`| SSM engine Status - 1: SSM Engine Ready for sending - 0: SSM Engine Busy| `RW`| `0x0`| `0x0 End: Begin:`|

###SSM Engine Status

* **Description**           

These registers are used to enable SSM engine for line 47-32


* **RTL Instant Name**    : `ssmsta_pen`

* **Address**             : `0x00050201`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ssmengsta`| SSM engine Status - 1: SSM Engine Ready for sending - 0: SSM Engine Busy| `RW`| `0x0`| `0x0 End: Begin:`|

###SSM Engine Status

* **Description**           

These registers are used to enable SSM engine for line 83-48


* **RTL Instant Name**    : `ssmsta_pen`

* **Address**             : `0x00050202`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[19:00]`|`ssmengsta`| SSM engine Status - 1: SSM Engine Ready for sending - 0: SSM Engine Busy| `RW`| `0x0`| `0x0 End: Begin:`|

###Tx SSM Framer Control

* **Description**           

This is used to configure the SSM transmitter


* **RTL Instant Name**    : `txctrl00_pen`

* **Address**             : `0x00010000 - 0x00010053`

* **Formula**             : `0x00010000 + LiuID`

* **Where**               : 

    * `$liuid(0-83):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`e1errins`| E1 Error Insert Enable| `RW`| `0x0`| `0x0`|
|`[30:30]`|`sivalcfg`| Si Value default| `RW`| `0x0`| `0x0`|
|`[29:25]`|`savalcfg`| Sa4-Sa8 Value default| `RW`| `0x0`| `0x0`|
|`[24:24]`|`ssmena`| SSM enable 1: Enable SSM 0: Disable SSM| `RW`| `0x0`| `0x0`|
|`[23:16]`|`ssmcount`| SSM Message Counter. These bits are used to store the amount of repetitions the Transmit SSM message will be sent - 0 : the transmit BOC will be set continuously - other: the amount of repetitions the Transmit SSM message will be sent before an all ones| `RW`| `0x0`| `0x0`|
|`[15:10]`|`ssmmess`| SSM Message. These bits are used to store the SSM message to be transmitted out the National bits or Si International bit or FDL - E1: [13:10] for SSM Message - T1: [15:10] for BOC code| `RW`| `0x0`| `0x0`|
|`[09:05]`|`ssmsasel`| Only one of the five Sa bits can be chosen for SSM transmission at a time. Don't care in T1 case - Bit9: for Sa8 - Bit8: for Sa7 - Bit7: for Sa6 - Bit6: for Sa5 - Bit5: for Sa4| `RW`| `0x0`| `0x0`|
|`[04:04]`|`ssmsisasel`| Select Si or Sa to transmit SSM. Don't care in T1 case - 0: Sa National Bits (Only one of the five Sa bits can be chosen for SSM transmission at a time) - 1: Si International Bits| `RW`| `0x0`| `0x0`|
|`[03:00]`|`txde1md`| Transmit DS1/E1 framing mode - 0000: DS1 Unframe (For bypass TX Framer of PDH) - 0001: DS1 SF (D4) - 0010: DS1 ESF - 1000: E1 Unframe (For bypass TX Framer of PDH) - 1001: E1 Basic Frame - 1010: E1 CRC4 Frame - Other: Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###Tx SSM Framer Control

* **Description**           

This is used to configure the SSM transmitter


* **RTL Instant Name**    : `txctrl01_pen`

* **Address**             : `0x00010600 - 0x00010653`

* **Formula**             : `0x00010600 + LiuID`

* **Where**               : 

    * `$liuid(0-83):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:09]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[08:08]`|`retena`| Retiming enalbe 1: enable 0: disalbe| `RW`| `0x0`| `0x0`|
|`[07:07]`|`aisneadis`| AIS near-end disable 1: Disable forwading AIS 0: Enable forwading AIS| `RW`| `0x0`| `0x0`|
|`[06:06]`|`raifardis`| RAI far-end disable 1: Disable forwading RAI 0: Enable forwading RAI| `RW`| `0x0`| `0x0`|
|`[05:05]`|`raineadis`| RAI near-end disable 1: Disable forwading RAI 0: Enable forwading RAI| `RW`| `0x0`| `0x0`|
|`[04:04]`|`sibypass`| Si CRC4 Bypass Enable| `RW`| `0x0`| `0x0`|
|`[03:03]`|`sabypass`| Sa4-Sa8 Bypass Enable| `RW`| `0x0`| `0x0`|
|`[02:02]`|`e2bypass`| E2 of CRC4 Bypass Enable| `RW`| `0x0`| `0x0`|
|`[01:01]`|`e1bypass`| E1 of CRC4 Bypass Enable| `RW`| `0x0`| `0x0`|
|`[00:00]`|`e2errins`| E2 Error Insert Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Rx SSM Framer Control

* **Description**           

This is used to configure the SSM transmitter


* **RTL Instant Name**    : `rxctrl_pen`

* **Address**             : `0x0001C000 - 0x0001C053`

* **Formula**             : `0x0001C000 + LiuID`

* **Where**               : 

    * `$liuid(0-83):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[10:10]`|`rxde1crccheck`| Set 1 to check CRC6| `RW`| `0x0`| `0x0`|
|`[09:06]`|`rxde1lofthres`| Threshold for declare LOF| `RW`| `0x0`| `0x0`|
|`[05:05]`|`rxde1flofmod`| Select mode Slide Window or Continous to declare LOF - 1: Slide Window detection (window 8, threshold RxDE1LofThres) - 0: Continous detection| `RW`| `0x0`| `0x0`|
|`[04:04]`|`rxde1frclof`| Set 1 to force Re-frame (default 0)| `RW`| `0x0`| `0x0`|
|`[03:00]`|`rxde1md`| Receive DS1/E1 framing mode - 0000: DS1 Unframe (For bypass TX Framer of PDH) - 0001: DS1 SF (D4) - 0010: DS1 ESF - 1000: E1 Unframe (For bypass TX Framer of PDH) - 1001: E1 Basic Frame - 1010: E1 CRC4 Frame - Other: Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###SSM Slip Buffer Data Sticky

* **Description**           

These registers are used for 31-0 line


* **RTL Instant Name**    : `stk0_upen`

* **Address**             : `0x00014001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config}                               %% RO       %% 0x0      %% 0x0`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ssmslipdat`| SSM Slip Buffer for Data - 1: SSM Data Slip Enable - 0: SSM Data Slip Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###SSM Slip Buffer Frame Sticky

* **Description**           

These registers are used for 31-0 line


* **RTL Instant Name**    : `stk1_upen`

* **Address**             : `0x00014002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config}                                    %% RO       %% 0x0      %% 0x0`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:00]`|`ssmslipfrm`| SSM Slip Buffer for Frame - 1: SSM Frame Slip Enable - 0: SSM Frame Slip Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###SSM Slip Buffer Data Sticky

* **Description**           

These registers are used for 63-32


* **RTL Instant Name**    : `stk0_upen`

* **Address**             : `0x00014003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config}                                   %% RO       %% 0x0      %% 0x0`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ssmslipdat`| SSM Slip Buffer for Data - 1: SSM Data Slip Enable - 0: SSM Data Slip Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###SSM Slip Buffer Frame Sticky

* **Description**           

These registers are used for 63-32


* **RTL Instant Name**    : `stk1_upen`

* **Address**             : `0x00014004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ssmslipfrm`| SSM Slip Buffer for Frame - 1: SSM Frame Slip Enable - 0: SSM Frame Slip Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###SSM Slip Buffer Data Sticky

* **Description**           

These registers are used for 83-64


* **RTL Instant Name**    : `stk0_upen`

* **Address**             : `0x00014005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[19:00]`|`ssmslipdat`| SSM Slip Buffer for Data - 1: SSM Data Slip Enable - 0: SSM Data Slip Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###SSM Slip Buffer Frame Sticky

* **Description**           

These registers are used for 83-64


* **RTL Instant Name**    : `stk1_upen`

* **Address**             : `0x00014006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[19:00]`|`ssmslipfrm`| SSM Slip Buffer for Frame - 1: SSM Frame Slip Enable - 0: SSM Frame Slip Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###SSM Slip Buffer Frame Sticky

* **Description**           

This is the per channel CRC error counter for DS1/E1/J1 receive framer


* **RTL Instant Name**    : `cnt0_pen`

* **Address**             : `0x00014100-0x00014153`

* **Formula**             : `0x00014100 + 16*Rd2Clr + LiuID`

* **Where**               : 

    * `$Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only`

    * `$LiuID(0 - 15):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`slipcnt`| Slip Buffer Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Rx SSM Framer HW Status

* **Description**           

These registers are used for Hardware status only


* **RTL Instant Name**    : `status_pen`

* **Address**             : `0x0001C400 - 0x0001C053`

* **Formula**             : `0x0001C400 + liuid`

* **Where**               : 

    * `$liuid(0-83):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[02:00]`|`rxde1sta`| RX Framer Status - 5: In Frame - 0: LOF - Other: Searching State| `RO`| `0x0`| `0x0 End: Begin:`|

###Rx SSM Framer CRC Error Counter

* **Description**           

This is the per channel CRC error counter for DS1/E1/J1 receive framer


* **RTL Instant Name**    : `rxfrm_crc_err_cnt`

* **Address**             : `0x0001E800 - 0x0001E8FF`

* **Formula**             : `0x0001E800 + 128*Rd2Clr + LiuID`

* **Where**               : 

    * `$Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only`

    * `$LiuID(0 - 15):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[07:00]`|`de1crcerrcnt`| CRC Error Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Rx SSM Framer REI Counter

* **Description**           

This is the per channel REI counter for DS1/E1/J1 receive framer


* **RTL Instant Name**    : `rxfrm_rei_cnt`

* **Address**             : `0x0001F000 - 0x0001F0FF`

* **Formula**             : `0x0001F000 + 128*Rd2Clr + LiuID`

* **Where**               : 

    * `$Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only`

    * `$liuid(0-83):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[07:00]`|`de1reierrcnt`| REI Error Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Rx SSM Framer FBE Counter

* **Description**           

This is the per channel REI counter for DS1/E1/J1 receive framer


* **RTL Instant Name**    : `rxfrm_fbe_cnt`

* **Address**             : `0x0001E000 - 0x0001E0FF`

* **Formula**             : `0x0001E000 + 16*Rd2Clr + LiuID`

* **Where**               : 

    * `$Rd2Clr(0 - 1): Rd2Clr = 0: Read to clear. Otherwise, Read Only`

    * `$liuid(0-83):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[07:00]`|`de1fbeerrcnt`| CRC Error Counter| `RW`| `0x0`| `0x0 End:`|

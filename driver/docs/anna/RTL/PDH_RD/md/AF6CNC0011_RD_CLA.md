## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_CLA
####Register Table

|Name|Address|
|-----|-----|
|`Classify Global PSN Control`|`0x00000000`|
|`Classify Per Pseudowire Type Control`|`0x0000B000-0x0000B29F #The address format for these registers is 0x0000C000 + PWID`|
|`Classify HBCE Global Control`|`0x00008000`|
|`Classify HBCE Hashing Table Control`|`0x00009000 - 0x00009FFF`|
|`Classify HBCE Looking Up Information Control`|`0x0000A000 - 0x0000AFFF #The address format for these registers is 0x0000E000 +  $CellID`|
|`CDR Pseudowire Look Up Control`|`0x00003000 - 0x0000329F`|
|`Classify Pseudowire MEF over MPLS Control`|`0x0000C000 - 0x0000C29F`|
|`Classify Per Group Enable Control`|`0x0000D000 - 0x0000D29F`|


###Classify Global PSN Control

* **Description**           

This register controls operation modes of PSN interface.


* **RTL Instant Name**    : `glb_psn`

* **Address**             : `0x00000000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[30]`|`rxpsnvlantagmodeen`| 1: Enable (default) VLAN TAG mode which contain PWID 0:  Disable VLAN TAG mode, normal PSN operation| `RW`| `0x0`| `0x0`|
|`[29]`|`rxsendlbit2cdren`| Enable to send Lbit to CDR engine 1: Enable to send Lbit 0: Disable| `RW`| `0x1`| `0x0`|
|`[28]`|`rxpsnmplsoutlabelchecken`| Enable to check MPLS outer 1: Enable checking 0: Disable checking| `RW`| `0x0`| `0x0`|
|`[27]`|`rxpsncpubfdctlen`| Enable VCCV BFD control packet sending to CPU for processing 1: Enable sending 0: Discard| `RW`| `0x0`| `0x0`|
|`[26]`|`rxmaccheckdis`| Disable to check MAC address at Ethernet port receive direction 1: Disable checking 0: Enable checking| `RW`| `0x0`| `0x0`|
|`[25]`|`rxpsncpuicmpen`| Enable ICMP control packet sending to CPU for processing 1: Enable sending 0: Discard| `RW`| `0x0`| `0x0`|
|`[24]`|`rxpsncpuarpen`| Enable ARP control packet sending to CPU for processing 1: Enable sending 0: Discard| `RW`| `0x0`| `0x0`|
|`[23]`|`pweloopclaen`| Enable Loop back traffic from PW Encapsulation to Classification 1: Enable Loop back mode 0: Normal, not loop back| `RW`| `0x0`| `0x0`|
|`[22]`|`rxpsnipudpmode`| This bit is applicable for Ipv4/Ipv6 packet from Ethernet side 1: Classify engine uses RxPsnIpUdpSel to decide which UDP port (Source or Destination) is used to identify pseudowire packet 0: Classify engine will automatically search for value 0x85E in source or destination UDP port. The remaining UDP port is used to identify pseudowire packet| `RW`| `0x0`| `0x0`|
|`[21]`|`rxpsnipudpsel`| This bit is applicable for Ipv4/Ipv6 using to select Source or Destination to identify pseudowire packet from Ethernet side. It is not use when RxPsnIpUdpMode is zero 1: Classify engine selects source UDP port to identify pseudowire packet 0: Classify engine selects destination UDP port to identify pseudowire packet| `RW`| `0x0`| `0x0`|
|`[20]`|`rxpsnipttlchken`| Enable check TTL field in MPLS/Ipv4 or Hop Limit field in Ipv6 1: Enable checking 0: Disable checking| `RW`| `0x0`| `0x0`|
|`[19:0]`|`rxpsnmplsoutlabel`| Received 2-label MPLS packet from PSN side will be discarded when it's outer label is different than RxPsnMplsOutLabel| `RW`| `0x0`| `0x0 End: Begin:`|

###Classify Per Pseudowire Type Control

* **Description**           

This register configures identification types per pseudowire


* **RTL Instant Name**    : `per_pw_ctrl`

* **Address**             : `0x0000B000-0x0000B29F #The address format for these registers is 0x0000C000 + PWID`

* **Formula**             : `0x0000B000 +  $PWID`

* **Where**               : 

    * `$PWID(0-671): Pseudowire ID`

* **Width**               : `62`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[61]`|`rxethpwonflyvc34`| CEP EBM on-fly fractional, length changed| `RW`| `0x0`| `0x0`|
|`[60:47]`|`rxethpwlen`| length of packet to check malform| `RW`| `0x0`| `0x0`|
|`[46:45]`|`rxethcepmode`| CEP mode working 0,1: CEP basic 2: VC3 fractional 3: VC4 fractional| `RW`| `0x0`| `0x0`|
|`[44]`|`rxethpwen`| PW enable working 1: Enable PW 0: Disable PW| `RW`| `0x0`| `0x0`|
|`[43:12]`|`rxethrtpssrcvalue`| This value is used to compare with SSRC value in RTP header of received TDM PW packets| `RW`| `0x0`| `0x0`|
|`[11:5]`|`rxethrtpptvalue`| This value is used to compare with PT value in RTP header of received TDM PW packets| `RW`| `0x0`| `0x0`|
|`[4]`|`rxethrtpen`| Enable RTP 1: Enable RTP 0: Disable RTP| `RW`| `0x0`| `0x0`|
|`[3]`|`rxethrtpssrcchken`| Enable checking SSRC field of RTP header in received TDM PW packet 1: Enable checking 0: Disable checking| `RW`| `0x0`| `0x0`|
|`[2]`|`rxethrtpptchken`| Enable checking PT field of RTP header in received TDM PW packet 1: Enable checking 0: Disable checking| `RW`| `0x0`| `0x0`|
|`[1:0]`|`rxethpwtype`| this is PW type working 0: CES mode without CAS 1: CES mode with CAS 2: CEP mode| `RW`| `0x0`| `0x0 End: Begin:`|

###Classify HBCE Global Control

* **Description**           

This register is used to configure global for HBCE module


* **RTL Instant Name**    : `hbce_glb`

* **Address**             : `0x00008000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `20`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:4]`|`clahbcetimeoutvalue`| this is time out value for HBCE module, packing be discarded| `RW`| `0x00FF`| `0x0`|
|`[3]`|`clahbcehashingtableselectedpage`| this is to select hashing table page to access looking up information buffer 1: PageID 1 0: PageID 0| `RW`| `0x1`| `0x0`|
|`[2:0]`|`clahbcecodingselectedmode`| selects mode to code origin id 4/5/6/7: code level 4/5/6/7 3: code level 3 (xor 4 subgroups origin id together) 2: code level 2 1: code level 1 0: none xor origin id| `RW`| `0x1`| `0x0 End: Begin:`|

###Classify HBCE Hashing Table Control

* **Description**           

HBCE module uses 10 bits for tabindex. tabindex is generated by hasding function applied to the origin id. The collisions due to hashing are handled by pointer to variable size blocks. // // Handling variable  size blocks requires a dynamic memory management scheme. The number of entries in each variable size block is defined by the number of rules that collide within a specific entry of //the tabindex. Hashing function applies an XOR function to all bits of tabindex. The indexes to the tabindex are generated by hashing function and therefore collisions may occur. In order to resolve //these collisions efficiently, HBCE define a complex data structure associated with each entry of the orgin id. Hashing table has two fields, flow number (flownum) (number of labelid mapped to this //particular table entry) and memory start pointer (memsptr) (hold a pointer to the variable size block and the number of  rules that collide). In case, a table entry might be empty  which means that it //is not mapped to any flow address rule, flownum = 0. Moreover, a table entry may be mapped to many flow address rules. In this case, where collisions occur, hashing table have to store a pointer to  //the variable size block and the number of rules that collide. The number of colliding rules also indicates the size of the block. The formating of hashing table page0 in each case is shown as below.%%

Hash ID:%%

PATERN HASH %%

Label ID (20bit)  +  Lookup mode (2bit) + port (1bit)%%

With Lookup Mode: 0/1/2 ~ PSN MEF/UDP/MPLS%%

+ NOXOR       :  Hashid[10:0]  = Patern Hash [10:0], StoreID[11:0] = Patern Hash [22:11]%%

+ ONEXOR     :  Hashid[10:0]  = Patern Hash [10:0] XOR Patern Hash [21:11],%%

StoreID = Patern Hash [22]%%

+ TWOXOR    :   Hashid  = Patern Hash [10:0] XOR Patern Hash [21:11] XOR {7'b0,Patern Hash[22]}%%

StoreID = Patern Hash [22:11]


* **RTL Instant Name**    : `hbce_hash_table`

* **Address**             : `0x00009000 - 0x00009FFF`

* **Formula**             : `0x00009000 + $PageID*0x1000 + $HashID`

* **Where**               : 

    * `$PageID(0-1): Page selection`

    * `$HashID(0-2047): HashID`

* **Width**               : `17`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16:13]`|`clahbceflownumber`| this is to identify HashID is empty or win or collision 0: empty format 1: normal format others: collision format| `RW`| `0x0`| `0x0`|
|`[12:0]`|`clahbcememorystartpointer`| this is a MemPtr to read the Classify HBCE Looking Up Information Control| `RW`| `0x0`| `0x0 End: Begin:`|

###Classify HBCE Looking Up Information Control

* **Description**           

In HBCE module, all operations access this memory in order to examine whether an exact match exists or not. The Memory Pool implements dynamic memory management scheme and supports //variable size blocks. It supports requests for allocation and deallocation of variable size blocks when inserting and deleting lookup address occur. Linking between multiple blocks is implemented by //writing the address of the  next  block  in  the  previous block. In general, HBCE implementation is based on sequential accesses to memory pool and to the dynamically allocated collision nodes. The //formating of memory pool word is shown as below.%%

The format of memory pool word is shown as below. There are two case formats depend on field[23]


* **RTL Instant Name**    : `hbce_lkup_info`

* **Address**             : `0x0000A000 - 0x0000AFFF #The address format for these registers is 0x0000E000 +  $CellID`

* **Formula**             : `0x0000A000 +  $CellID`

* **Where**               : 

    * `$ CellID(0-8192):  Cell Info Identification`

* **Width**               : `37`
* **Register Type**       : `Config #Normal format`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[35]`|`clahbceflowdirect`| this configure FlowID working in normal mode (not in any group)| `RW`| `0x0`| `0x0`|
|`[34]`|`clahbcegrpworking`| this configure group working or protection| `RW`| `0x0`| `0x0`|
|`[33:24]`|`clahbcegrpidflow`| this configure a group ID that FlowID following| `RW`| `0x0`| `0x0`|
|`[23]`|`clahbcememorystatus`| This is zero for "normal format"| `RW`| `0x0`| `0x0`|
|`[22:13]`|`clahbceflowid`| a number identifying the output port of HBCE module  this configure a group ID that FlowID following| `RW`| `0x0`| `0x0`|
|`[12]`|`clahbceflowenb`| The flow is identified in this table, it mean the traffic is identified 1: Flow identified 0: Flow look up fail| `RW`| `0x0`| `0x0`|
|`[11:0]`|`clahbcestoreid`| this is saving some additional information to identify a certain lookup address rule within a particular table entry HBCE and also to be able to distinguish those that collide| `RW`| `0x0`| `0x0 #link format End: Begin:`|

###CDR Pseudowire Look Up Control

* **Description**           

Used for TDM PW. This register is used to select PW for CDR function.


* **RTL Instant Name**    : `cdr_pw_lkup_ctrl`

* **Address**             : `0x00003000 - 0x0000329F`

* **Formula**             : `0x00003000 + PWID`

* **Where**               : 

    * `$PWID(0-671): pwid`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11]`|`pwcdrslice`| 0: Slice 0 corresponding to sts 0,2,4,...,46 1: Slice 1 corresponding to sts 1,3,5,...,47| `RW`| `0x0`| `0x0`|
|`[10]`|`pwcdrdis`| applicable for pseudowire that is enabled CDR function (eg: ACR/DCR) 0: Enable CDR (default) 1: Disable CDR| `RW`| `0x0`| `0x0`|
|`[9:0]`|`pwcdrlineid`| CDR line ID lookup from pseudowire Ids.| `RW`| `0x0`| `0x0 End: Begin:`|

###Classify Pseudowire MEF over MPLS Control

* **Description**           

Used for TDM PW. This register is used to configure MEF over MPLS


* **RTL Instant Name**    : `pw_mef_over_mpls_ctrl`

* **Address**             : `0x0000C000 - 0x0000C29F`

* **Formula**             : `0x0000C000 + $PWID`

* **Where**               : 

    * `$PWID(0-671): pwid`

* **Width**               : `51`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[50:3]`|`rxpwmefomplsdavalue`| DA value used to compare with DA in Ethernet MEF packet| `RW`| `0x0`| `0x0`|
|`[2]`|`rxpwmefomplsdachecken`| 1: Enable check  RxPwMEFoMPLSDaValue with DA in Ethernet MEF packet 0: Disable check  RxPwMEFoMPLSDaValue with DA in Ethernet MEF packet| `RW`| `0x0`| `0x0`|
|`[1]`|`rxpwmefomplsecidchecken`| 1: Enable check  ECID in Ethernet MEF packet with MPLS PW label 0: Disable check  ECID in Ethernet MEF packet with MPLS PW label| `RW`| `0x0`| `0x0`|
|`[0]`|`rxpwmefomplsen`| 1: Enable MEF over MPLS PW mode, in this case, after MPLS PW label is ETH MEF packet 0: Normal case, disable MEF over MPLS PW mode, in this case, after MPLS PW label is PW control word| `RW`| `0x0`| `0x0 End: Begin:`|

###Classify Per Group Enable Control

* **Description**           

This register configures Group that Flowid (or Pseudowire ID) is enable or not


* **RTL Instant Name**    : `cla_per_grp_enb`

* **Address**             : `0x0000D000 - 0x0000D29F`

* **Formula**             : `0x0000D000 + Working_ID*0x400 + Grp_ID`

* **Where**               : 

    * `$Working_ID(0-1): CLAHbceGrpWorking`

    * `$Grp_ID(0-671): CLAHbceGrpIDFlow`

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`clagrppwen`| This indicate the FlowID (or Pseudowire ID) is enable or not 1: FlowID enable 0: FlowID disable| `RW`| `0x0`| `0x0 End:`|

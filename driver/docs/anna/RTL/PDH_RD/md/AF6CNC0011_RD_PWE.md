## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_PWE
####Register Table

|Name|Address|
|-----|-----|
|`Pseudowire Transmit Ethernet Header Value Control`|`0x00008000-0x0000AA00`|
|`Pseudowire Transmit Ethernet Header Length Control`|`0x0001000-0x0000129F #The address format for these registers is 0x0001000 + $PWID`|
|`Pseudowire Transmit Ethernet PAD Control`|`0x0002000`|
|`Pseudowire Transmit Header RTP SSRC Value Control`|`0x0003000-0x0000329F #The address format for these registers is 0x0003000 + $PWID`|
|`Pseudowire Transmit Enable Control`|`0x0021000-0x0002129F #The address format for these registers is 0x0021000 + $PWID`|
|`Pseudowire Transmit R bit Control`|`0x0025000-0x0002529F #The address format for these registers is 0x0025000 + $PWID`|
|`Pseudowire Transmit HSPW Label Control`|`0x005000-0x00529F`|
|`Pseudowire Transmit UPSR and HSPW Control`|`0x006000-0x00629F`|
|`Pseudowire Transmit UPSR Group Control`|`0x007000-0x00729F`|
|`Pseudowire Transmit HSPW Protection Control`|`0x004000-0x00429F`|


###Pseudowire Transmit Ethernet Header Value Control

* **Description**           

This register configures value of transmit Ethernet pseudowire header. Each pseudowire has 16 entries (1 entry contains 32 bits) for header value configuration. The header format from %% //entry#0 to entry#15 is as follow:  %%

{DA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 48 bytes)} %%

Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%

PSN header is MEF-8:  %%

EthType:  0x88D8 %%

4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for 	remote 	receive side. %%

PSN header is MPLS:  %%

EthType:  0x8847 %%

4/8-byte PSN Header with format: {OuterLabel[31:0](optional), InnerLabel[31:0]}  where 	InnerLabel[31:12] is pseodowire identification for remote receive side. %%

Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit = 1 for InnerLabel %%

PSN header is UDP/Ipv4:   %%

EthType:  0x0800 %%

28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %%



{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %%

{IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %%

{IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %%

{IP_SrcAdr[31:0]}%%

{IP_DesAdr[31:0]}%%

{UDP_SrcPort[15:0], UDP_DesPort[15:0]}%%

{32-bit zeros}%%




* **RTL Instant Name**    : `pw_txeth_hdr`

* **Address**             : `0x00008000-0x0000AA00`

* **Formula**             : `0x00008000  + $PwId*16 + $Entry`

* **Where**               : 

    * `IP_Protocol[7:0]: 0x11 to signify UDP`

    * `IP_Header_Sum[31:0]:`

    * `{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +`

    * `IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +`

    * `{IP_TTL[7:0], IP_Protocol[7:0]} +`

    * `IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +`

    * `IP_DesAdr[31:16] + IP_DesAdr[15:0]`

    * `UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused`

    * `UDP_DesPort : used as remote pseudowire identification or 0x85E if unused`

    * `PSN header is UDP/Ipv6:`

    * `EthType:  0x86DD`

    * `48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:`

    * `{IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}`

    * `{16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]}`

    * `{IP_SrcAdr[127:96]}`

    * `{IP_SrcAdr[95:64]}`

    * `{IP_SrcAdr[63:32]}`

    * `{IP_SrcAdr[31:0]}`

    * `{IP_DesAdr[127:96]}`

    * `{IP_DesAdr[95:64]}`

    * `{IP_DesAdr[63:32]}`

    * `{IP_DesAdr[31:0]}`

    * `{UDP_SrcPort[15:0], UDP_DesPort[15:0]}`

    * `{UDP_Sum[31:0]}`

    * `IP_Next_Header[7:0]: 0x11 to signify UDP`

    * `UDP_Sum[31:0]:`

    * `IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] +`

    * `IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] +`

    * `IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] +`

    * `IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] +`

    * `IP_DesAdr[127:112] + 	IP_DesAdr[111:96] +`

    * `IP_DesAdr[95:80]  + IP_DesAdr[79:64] +`

    * `IP_DesAdr[63:48]  + IP_DesAdr[47:32] +`

    * `IP_DesAdr[31:16]  + IP_DesAdr[15:0] +`

    * `{8-bit zeros, IP_Next_Header[7:0]} +`

    * `UDP_SrcPort[15:0] +  UDP_DesPort[15:0]`

    * `UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused`

    * `UDP_DesPort : used as remote pseudowire identification or 0x85E if unused`

    * `User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field.`

    * `PSN header is MPLS over Ipv4:`

    * `IPV4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS`

    * `After IPV4 header is MPLS labels where inner label is used for PW identification`

    * `PSN header is MPLS over Ipv6:`

    * `IPV6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS`

    * `After IPV6 header is MPLS labels where inner label is used for PW identification`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethpwheadvalue`| Transmit Ethernet Pseudowire Header value in each entry| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Ethernet Header Length Control

* **Description**           

This register configures length in number of bytes of AF6F1 transmit Ethernet header.


* **RTL Instant Name**    : `pw_txeth_hdr_len_ctrl`

* **Address**             : `0x0001000-0x0000129F #The address format for these registers is 0x0001000 + $PWID`

* **Formula**             : `0x0001000 +  $PWID`

* **Where**               : 

    * `$PWID(0-671): Pseudowire ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[22:16]`|`txethpwrtpptvalue`| Used for TDM PW, this is the PT value of RTP header, define in http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml| `RW`| `0x0`| `0x0`|
|`[15]`|`txethpwrtpen`| Used for TDM PW 1: Enable RTP field in PSN header (used for TDM PW with DCR timing) 0: Disable RTP field in PSN header (used for ATM PW or TDM PW without DCR timing)| `RW`| `0x0`| `0x0`|
|`[14:12]`|`txethpwpsntype`| PW PSN Type 1: PW PSN header is UDP/IPv4 2: PW PSN header is UDP/IPv6 3: PW PSN header is MPLS and EXP bits of inner label is replaced by ATM cell priority bits (used for ATM PW) 4: PW MPLS no outer label over Ipv4 (total 1 MPLS label) 5: PW MPLS no outer label over Ipv6 (total 1 MPLS label) 6: PW MPLS one outer label over Ipv4 (total 2 MPLS label) 7: PW MPLS one outer label over Ipv6 (total 2 MPLS label) Others: for other PW PSN header type (include MPLS with configuration EXP bits)| `RW`| `0x0`| `0x0`|
|`[11]`|`txethpwcwtype`| 0: Control word 4-byte (used for PDH/ATM N to one pseudowire) 1: Control word 3-byte (used for ATM one to one VCC/VPC pseudowire)| `RW`| `0x0`| `0x0`|
|`[10:9]`|`txethpwnumvlan`| Number of VLANs in Transmit Ethernet Pseudowire Header.| `RW`| `0x0`| `0x0`|
|`[8:7]`|`txethpwnummplsoutlb`| Number of MPLS outer labels in Transmit Ethernet Pseudowire Header. This field is only applicable when the pseudowire selects MPLS as its PSN header, //	//				otherwise, this field must be set to zero value.| `RW`| `0x0`| `0x0`|
|`[6:0]`|`txethpwheadlen`| Length in number of bytes of Transmit Ethernet Pseudowire Header. This length is counted from start of Ethernet packet (DA) to end of pseudowire header (the next of //		pseudowire header is control word and TDM payload). Be noted that this length field includes SA field for counting too (although the below Pseudowire Transmit Ethernet Header Value Control does //	   not contains SA field because AF6FHW0013 already has a global MacAddress for the SA field). In case of VLAN TAG mode contains PWID (DA,SA,VLAN1,VLAN2), header length value equal to 0x14| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Ethernet PAD Control

* **Description**           

This register configures mode to insert number of PAD bytes of AF6FH1 transmit Ethernet


* **RTL Instant Name**    : `pw_txeth_pad_ctrl`

* **Address**             : `0x0002000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1:0]`|`txethpadmode`| Modes to insert PAD for pseudowire packet at Ethernet transmit direction 0: Insert PAD when total Ethernet packet length is less than 64 bytes. Refer to IEEE 802.3 1: Insert PAD when control word plus payload length is less than 64 bytes 2: Insert PAD when total Ethernet packet length minus VLANs and MPLS outer labels(if exist) is less than 64 bytes. The reason is that networking devices often insert or remove VLANs and MPLS outer //labels when packet traverse them| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Header RTP SSRC Value Control

* **Description**           

Used for TDM PW. This register configures RTP SSRC value.


* **RTL Instant Name**    : `pw_txeth_rtp_ssrc_ctrl`

* **Address**             : `0x0003000-0x0000329F #The address format for these registers is 0x0003000 + $PWID`

* **Formula**             : `0x0003000 +  $PWID`

* **Where**               : 

    * `$PWID(0-671): Pseudowire ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethpwrtpssrcvalue`| Used for TDM PW, this is the SSRC value of RTP header, define in RFC3550| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Enable Control

* **Description**           

This register configures pseudowire enable for transmit to Ethernet direction


* **RTL Instant Name**    : `pw_txeth_enable_ctrl`

* **Address**             : `0x0021000-0x0002129F #The address format for these registers is 0x0021000 + $PWID`

* **Formula**             : `0x0021000 +  $PWID`

* **Where**               : 

    * `$PWID(0-671): Pseudowire ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10:8]`|`txpwethportid`| 0: PW will be sent to ETH SGMII port0, ignore in XGMII mode 1: PW will be sent to ETH SGMII port1, ignore in XGMII mode 2: PW will be sent to ETH SGMII port2, ignore in XGMII mode 3: PW will be sent to ETH SGMII port3, ignore in XGMII mode 4: PW will be sent to ETH SGMII port4, ignore in XGMII mode| `RW`| `0x0`| `0x0`|
|`[7]`|`txpwsuppressen`| 0: normal 1: Enable payload suppression when AIS| `RW`| `0x0`| `0x0`|
|`[6]`|`txpwcwmbitdisable`| 0: normal 1: Disable Mbit/NPbit in control word| `RW`| `0x0`| `0x0`|
|`[5]`|`txpwcwlbitdisable`| 0: normal 1: Disable Lbit in control word| `RW`| `0x0`| `0x0`|
|`[4:3]`|`txpwcwmbitcpu`| low priority than TxPwLbitDisable 0: normal other: CPU force Mbit<br>{N,P} bit in control word, the value of Mbit should be 2'b10| `RW`| `0x0`| `0x0`|
|`[2]`|`txpwcwlbitcpu`| low priority than TxPwLbitDisable 0: normal 1: CPU force Lbit in control word| `RW`| `0x0`| `0x0`|
|`[1]`|`txpwcwtype`| 0: Control word 4-byte (used for PDH/ATM N to one pseudowire) 1: Control word 3-byte (used for ATM one to one VCC/VPC pseudowire)| `RW`| `0x0`| `0x0`|
|`[0]`|`txpwen`| 1: Enable 0: Disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit R bit Control

* **Description**           

This register configures pseudowire L/M bit control  for transmit to Ethernet direction


* **RTL Instant Name**    : `pw_txeth_Rbit_ctrl`

* **Address**             : `0x0025000-0x0002529F #The address format for these registers is 0x0025000 + $PWID`

* **Formula**             : `0x0025000 +  $PWID`

* **Where**               : 

    * `$PWID(0-671): Pseudowire ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:5]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[4:3]`|`txpwceptype`| 0: CEP basic PW 1: CEP VC3(STS) fractional PW (only 1 dword VT mask) 2: CEP VC4 fractional PW (3 dword   VT/STS mask)| `RW`| `0x0`| `0x0`|
|`[2]`|`txpwcepen`| 0: CES PW 1: CEP PW| `RW`| `0x0`| `0x0`|
|`[1]`|`txpwrbitcpu`| low priority than TxPwRbitDisable 0: Normal 1: CPU force Rbit at transmit PW direction| `RW`| `0x0`| `0x0`|
|`[0]`|`txpwrbitdisable`| 1: Disable sending Rbit in PW control word 0: Normal| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit HSPW Label Control

* **Description**           

Used for TDM PW. This register configures "label ID" when HSPW working at protection mode value.


* **RTL Instant Name**    : `Pseudowire_Transmit_HSPW_Label_Control`

* **Address**             : `0x005000-0x00529F`

* **Formula**             : `0x005000 + PwId`

* **Where**               : 

    * `$PwId(0 - 671): Pseudowire Identification`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethpwhspwlabelvalue`| Used for TDM PW, this is the HSPW label| `RW`| `0x0`| `0x0 UDP_SrcPort[15:0] +  UDP_DesPort[15:0] Inner Label [31:0] MEF label [31:0] End: Begin:`|

###Pseudowire Transmit UPSR and HSPW Control

* **Description**           

Used for TDM PW. This register configures UPSR and HSPW value.


* **RTL Instant Name**    : `Pseudowire_Transmit_UPSR_and_HSPW_Control`

* **Address**             : `0x006000-0x00629F`

* **Formula**             : `0x006000 + PwId`

* **Where**               : 

    * `$PwId(0 - 671): Pseudowire Identification`

* **Width**               : `22`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[21]`|`txethpwupsrusevalue`| Used for TDM PW, this is the UPSR using or not 0: PW not join any UPSR Group 1: PW join a UPSR Group| `RW`| `0x0`| `0x0`|
|`[20]`|`txethpwhspwusevalue`| Used for TDM PW, this is the HSPW using or not 0: PW not join any HSPW Group 1: PW join a HSPW Group| `RW`| `0x0`| `0x0`|
|`[19:10]`|`txethpwupsrgrpvalue`| Used for TDM PW, this is the UPSR group| `RW`| `0x0`| `0x0`|
|`[9:0]`|`txethpwhspwgrpvalue`| Used for TDM PW, this is the HSPW group| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit UPSR Group Control

* **Description**           

Used for TDM PW. This register configures UPSR Group enable or not


* **RTL Instant Name**    : `Pseudowire_Transmit_UPSR_Group_Control`

* **Address**             : `0x007000-0x00729F`

* **Formula**             : `0x007000 + TxEthPwUpsrGrpValue`

* **Where**               : 

    * `$TxEthPwUpsrGrpValue(0 - 671): UPSR Group Identification`

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`txethpwupsrenvalue`| Used for TDM PW, this is the UPSR Group enable or not 0: UPSR Group disable 1: UPSR Group enable| `RW`| `0x0`| `0x0 End Begin:`|

###Pseudowire Transmit HSPW Protection Control

* **Description**           

Used for TDM PW. This register configures HSPW Group Protection enable


* **RTL Instant Name**    : `Pseudowire_Transmit_HSPW_Protection_Control`

* **Address**             : `0x004000-0x00429F`

* **Formula**             : `0x004000 + TxEthPwHspwGrpValue`

* **Where**               : 

    * `$TxEthPwHspwGrpValue(0 - 671): HSPW Group Identification`

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`txethpwhspwenvalue`| Used for TDM PW, this is the HSPW enable 0: HSPW Group using normal label 1: HSPW Group using "HSPW Label" as lable for packet transmit| `RW`| `0x0`| `0x0 End`|

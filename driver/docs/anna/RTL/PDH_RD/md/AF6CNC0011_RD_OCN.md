## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_OCN
####Register Table

|Name|Address|
|-----|-----|
|`OCN Global Rx Framer Master  Control`|`0x00000`|
|`OCN Global Rx Framer LOS Threshold`|`0x00006`|
|`OCN Global Rx Framer LOF Threshold`|`0x00007`|
|`OCN Global Tx Framer Control`|`0x00001`|
|`OCN Global Tx Line clock 6M selection 1`|`0x0000a`|
|`OCN Global Tx Line clock 6M selection 2`|`0x0000b`|
|`OCN Global Tx Line clock Looptime mode 1`|`0x0000c`|
|`OCN Global Tx Line clock Looptime mode 2`|`0x0000d`|
|`OCN Global Tx Framer RDI-L Insertion Threshold Control`|`0x00009`|
|`OCN Global STS Pointer Interpreter Control`|`0x00002`|
|`OCN Global VTTU Pointer Interpreter Control`|`0x00003`|
|`OCN Global Pointer Generator Control`|`0x00004`|
|`OCN Global Loopback Control`|`0x00005`|
|`OCN Global select path DS3 over SDH config 1`|`0x00010`|
|`OCN Global select path DS3 over SDH config 2`|`0x00011`|
|`OCN Global Rx_TOHBUS K Byte Control`|`0x00014`|
|`OCN Global TOHBUS K_Byte Threshold`|`0x00015`|
|`OCN Global Tx_TOHBUS K Byte Control`|`0x00016`|
|`OCN STS Pointer Interpreter Per Channel Control`|`0x22000 - 0x22e2f`|
|`OCN STS Pointer Generator Per Channel Control`|`0x23000 - 0x23e2f`|
|`OCN RXPP Per STS payload Control`|`0x40000 - 0x5402f`|
|`OCN VTTU Pointer Interpreter Per Channel Control`|`0x40800 - 0x54fff`|
|`OCN TXPP Per STS Multiplexing Control`|`0x60000 - 0x7402f`|
|`OCN VTTU Pointer Generator Per Channel Control`|`0x60800 - 0x74fff`|
|`OCN Rx STS/VC per Alarm Interrupt Status`|`0x22140 - 0x22f6f`|
|`OCN Rx STS/VC per Alarm Current Status`|`0x22180 - 0x22faf`|
|`OCN Rx VT/TU per Alarm Interrupt Status`|`0x42800 - 0x56fff`|
|`OCN Rx VT/TU per Alarm Current Status`|`0x43000 - 0x57fff`|
|`OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter`|`0x22080 - 0x22eaf`|
|`OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter`|`0x41000 - 0x55fff`|
|`OCN TxPg STS per Alarm Interrupt Status`|`0x23140 - 0x23f6f`|
|`OCN TxPg STS pointer adjustment per channel counter`|`0x23080 - 0x23eaf`|
|`OCN TxPg VTTU per Alarm Interrupt Status`|`0x62800 - 0x76fff`|
|`OCN TxPg VTTU pointer adjustment per channel counter`|`0x61000 - 0x75fff`|
|`OCN TOH Global K1 Stable Monitoring Threshold Control`|`0x24000`|
|`OCN TOH Global K2 Stable Monitoring Threshold Control`|`0x24001`|
|`OCN TOH Global S1 Stable Monitoring Threshold Control`|`0x24002`|
|`OCN TOH Global RDI_L Detecting Threshold Control`|`0x24003`|
|`OCN TOH Global AIS_L Detecting Threshold Control`|`0x24004`|
|`OCN TOH Global K1 Sampling Threshold Control`|`0x24005`|
|`OCN TOH Global Error Counter Control`|`0x24006`|
|`OCN TOH Montoring Affect Control`|`0x24007`|
|`OCN TOH Monitoring Per Channel Control`|`0x24100 - 0x2412f`|
|`OCN TOH Monitoring B1 Error Read Only Counter`|`0x24800 - 0x2482f`|
|`OCN TOH Monitoring B1 Error Read to Clear Counter`|`0x24a00 - 0x24a2f`|
|`OCN TOH Monitoring B1 Block Error Read Only Counter`|`0x24c00 - 0x24c2f`|
|`OCN TOH Monitoring B1 Block Error Read to Clear Counter`|`0x24e00 - 0x24e2f`|
|`OCN TOH Monitoring B2 Error Read Only Counter`|`0x24840 - 0x2486f`|
|`OCN TOH Monitoring B2 Error Read to Clear Counter`|`0x24a40 - 0x24a6f`|
|`OCN TOH Monitoring B2 Block Error Read Only Counter`|`0x24c40 - 0x24c6f`|
|`OCN TOH Monitoring B2 Block Error Read to Clear Counter`|`0x24e40 - 0x24e6f`|
|`OCN TOH Monitoring REI Error Read Only Counter`|`0x24940 - 0x2496f`|
|`OCN TOH Monitoring REI Error Read to Clear Counter`|`0x24b40 - 0x24b6f`|
|`OCN TOH Monitoring REI Block Error Read Only Counter`|`0x24d40 - 0x24d6f`|
|`OCN TOH Monitoring REI Block Error Read to Clear Counter`|`0x24f40 - 0x24f6f`|
|`OCN TOH K1 Monitoring Status`|`0x24880 - 0x248af`|
|`OCN TOH K2 Monitoring Status`|`0x248c0 - 0x248ef`|
|`OCN TOH S1 Monitoring Status`|`0x24900 - 0x2492f`|
|`OCN Rx Line per Alarm Interrupt Enable Control`|`0x24200 - 0x2422f`|
|`OCN Rx Line per Alarm Interrupt Status`|`0x24240 - 0x2426f`|
|`OCN Rx Line per Alarm Current Status`|`0x24280 - 0x242af`|
|`OCN Rx Line Per Line Interrupt Enable Control 1`|`0x242f0`|
|`OCN Rx Line Per Line Interrupt Enable Control 2`|`0x242f1`|
|`OCN Rx Line per Line Interrupt OR Status 1`|`0x242f2`|
|`OCN Rx Line per Line Interrupt OR Status 2`|`0x242f3`|
|`OCN Rx Line per Slice24 Interrupt OR Status`|`0x242f4`|
|`OCN Tx Framer Per Channel Control`|`0x21000 - 0x2142f`|
|`OCN Tx J0 Insertion Buffer`|`0x21200 - 0x2162f`|


###OCN Global Rx Framer Master  Control

* **Description**           

This is the global configuration register for Rx Framer


* **RTL Instant Name**    : `glbrfm_reg`

* **Address**             : `0x00000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`rxfrmaislrdilen`| Enable/disable the insertion of RDI-L at TOH insertion when AIS_L condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[15]`|`rxfrmtimrdilen`| Enable/disable the insertion of RDI-L at TOH insertion when TIM condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[14]`|`rxfrmoofrdilen`| Enable/disable the insertion of RDI-L at TOH insertion when OOF condition detected. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[13]`|`rxfrmlofrdilen`| Enable/disable the insertion of RDI-L at TOH insertion when LOF condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[12]`|`rxfrmlosrdilen`| Enable/disable the insertion of RDI-L at TOH insertion when LOS condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[11:9]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[8]`|`rxfrmaislaispen`| Enable/disable the insertion of P-AIS at STS pointer interpreter when AIS_L condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[7]`|`rxfrmtimaispen`| Enable/disable the insertion of P-AIS at STS pointer interpreter when TIM condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[6]`|`rxfrmoofaispen`| Enable/disable the insertion of P-AIS at STS pointer interpreter when OOF condition detected. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[5]`|`rxfrmlofaispen`| Enable/disable the insertion of P-AIS at STS pointer interpreter when LOF condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[4]`|`rxfrmlosaispen`| Enable/disable the insertion of P-AIS at STS pointer interpreter when LOS condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[3]`|`rxfrmlofcfg`| Detected LOF mode. 1: OOF counter is reset in In-frame state. 0: OOF counter is only reset when In-frame state exists continuously more than 3 ms| `RW`| `0x0`| `0x0`|
|`[2]`|`rxfrmlosdetdis`| Enable/Disable detect LOS. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[1]`|`rxfrmloscfg`| Detected LOS mode. 1: the LOS declare when all zero or all one detected 0: the LOS declare when all zero detected| `RW`| `0x1`| `0x1`|
|`[0]`|`rxfrmdescren`| Enable/disable de-scrambling of the Rx coming data stream. 1: Enable 0: Disable| `RW`| `0x1`| `0x1 End : Begin:`|

###OCN Global Rx Framer LOS Threshold

* **Description**           

Configure thresholds for LOS detection at receive SONET/SDH framer,There are three thresholds


* **RTL Instant Name**    : `glblosthr_reg`

* **Address**             : `0x00006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[28]`|`rxfrmnumvalmondis`| Disable to generate LOS to Rx Framer base on detecting number of valid on Rx data valid. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[27:20]`|`rxfrmnumvalmonthr`| Threshold to generate LOS to Rx Framer base on detecting number of valid on Rx data valid.(Threshold = PPM * 1). Default 0x20 ~ 32ppm| `RW`| `0x20`| `0x20`|
|`[19:16]`|`rxfrmlosclr2thr`| Configure the period of time during which there is no period of consecutive data without transition, used for reset no transition counter. The recommended value is 0x8 (~5us).| `RW`| `0x8`| `0x8`|
|`[15:8]`|`rxfrmlossetthr`| Configure the value that define the period of time in which there is no transition detected from incoming data from SERDES. The recommended value is 0x28 (~25us).| `RW`| `0x28`| `0x28`|
|`[7:0]`|`rxfrmlosclr1thr`| Configure the period of time during which there is no period of consecutive data without transition, used for counting at INFRM to change state to LOS The recommended value is 0x51 (~50us).| `RW`| `0x51`| `0x51 End : Begin:`|

###OCN Global Rx Framer LOF Threshold

* **Description**           

Configure thresholds for LOF detection at receive SONET/SDH framer,There are two thresholds


* **RTL Instant Name**    : `glblofthr_reg`

* **Address**             : `0x00007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:8]`|`rxfrmlofsetthr`| Configure the OOF time counter threshold for entering LOF state. Resolution of this threshold is one frame.| `RW`| `0x18`| `0x18`|
|`[7:0]`|`rxfrmlofclrthr`| Configure the In-frame time counter threshold for exiting LOF state. Resolution of this threshold is one frame.| `RW`| `0x18`| `0x18 End : Begin:`|

###OCN Global Tx Framer Control

* **Description**           

This is the global configuration register for the Tx Framer


* **RTL Instant Name**    : `glbtfm_reg`

* **Address**             : `0x00001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`txlineooffrc`| Enable/disable force OOF for tx lines.| `RW`| `0x0`| `0x0`|
|`[1]`|`txlineb1errfrc`| Enable/disable force B1 error for  tx lines.| `RW`| `0x0`| `0x0`|
|`[0]`|`txlinescren`| Enable/disable scrambling of the Tx data stream. 1: Enable 0: Disable| `RW`| `0x1`| `0x1 End : Begin:`|

###OCN Global Tx Line clock 6M selection 1

* **Description**           

This is the global configuration register for selecting clock 6M source to transmit Tx line stream (EC1 Id: 0-23)


* **RTL Instant Name**    : `glbclksel_reg1`

* **Address**             : `0x0000a`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`txlineclk6msel1`| Bit #0 for line #0. 1: Clock 6M is soure2 (External) 0: Clock 6M is soure1 (System).| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Tx Line clock 6M selection 2

* **Description**           

This is the global configuration register for selecting clock 6M source to transmit Tx line stream (EC1 Id: 24-47)


* **RTL Instant Name**    : `glbclksel_reg2`

* **Address**             : `0x0000b`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`txlineclk6msel2`| Bit #0 for line #24. 1: Clock 6M is source2 (External) 0: Clock 6M is source1 (System).| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Tx Line clock Looptime mode 1

* **Description**           

This is the global configuration register for selecting clock source to transmit Tx line stream (EC1 Id: 0-23) is looptime or selected from 2 sources 6M (sysclk & extclk)


* **RTL Instant Name**    : `glbclklooptime_reg1`

* **Address**             : `0x0000c`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`txlineclklooptime1`| Bit #0 for line #0. 1: Looptime Mode 0: Using TxLineClk6mSel1 for transmit Tx line source clock.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Tx Line clock Looptime mode 2

* **Description**           

This is the global configuration register for selecting clock source to transmit Tx line stream (EC1 Id: 24-47) is looptime or selected from 2 sources(sysclk & extclk)


* **RTL Instant Name**    : `glbclklooptime_reg2`

* **Address**             : `0x0000d`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`txlineclklooptime2`| Bit #0 for line #24. 1: Looptime Mode 0: Using TxLineClk6mSel2 for transmit Tx line source clock.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Tx Framer RDI-L Insertion Threshold Control

* **Description**           

Configure the number of frames in which L-RDI will be inserted when an L-RDI event is triggered. The register contains two numbers


* **RTL Instant Name**    : `glbrdiinsthr_reg`

* **Address**             : `0x00009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[12:8]`|`txfrmrdiinsthr2`| Threshold 2 for SDH mode| `RW`| `0x8`| `0x8`|
|`[7:5]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[4:0]`|`txfrmrdiinsthr1`| Threshold 1 for Sonet mode| `RW`| `0x14`| `0x14 End : Begin:`|

###OCN Global STS Pointer Interpreter Control

* **Description**           

This is the global configuration register for the STS Pointer Interpreter


* **RTL Instant Name**    : `glbspi_reg`

* **Address**             : `0x00002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29]`|`stspihobussel2`| Select bus between SPI slice2  and HOBus slice2  for downstream to PDH. 1: Hobus slice2 selected 0: SPI slice2 (normal) selected| `RW`| `0x0`| `0x0`|
|`[28]`|`stspihobussel1`| Select bus between SPI slice1  and HOBus slice1  for downstream to PDH. 1: Hobus slice1 selected 0: SPI slice1 (normal) selected| `RW`| `0x0`| `0x0`|
|`[27:24]`|`rxpgflowthresh`| Overflow/underflow threshold to resynchronize read/write pointer.| `RW`| `0x3`| `0x3`|
|`[23:20]`|`rxpgadjthresh`| Adjustment threshold to make a pointer increment/decrement.| `RW`| `0xc`| `0xc`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`stspiaisaispen`| Enable/Disable forwarding P_AIS when AIS state is detected at STS Pointer interpreter. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[17]`|`stspilopaispen`| Enable/Disable forwarding P_AIS when LOP state is detected at STS Pointer interpreter. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[16]`|`stspimajormode`| Majority mode for detecting increment/decrement at STS pointer Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5| `RW`| `0x1`| `0x1`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:12]`|`stspinorptrthresh`| Threshold of number of normal pointers between two contiguous frames within pointer adjustments.| `RW`| `0x3`| `0x3`|
|`[11:8]`|`stspindfptrthresh`| Threshold of number of contiguous NDF pointers for entering LOP state at FSM.| `RW`| `0x8`| `0x8`|
|`[7:4]`|`stspibadptrthresh`| Threshold of number of contiguous invalid pointers for entering LOP state at FSM.| `RW`| `0x8`| `0x8`|
|`[3:0]`|`stspipohaistype`| Enable/disable STS POH defect types to downstream AIS in case of terminating the related STS such as the STS carries VT/TU. [0]: Enable for TIM defect [1]: Enable for Unequiped defect [2]: Enable for VC-AIS [3]: Enable for PLM defect| `RW`| `0xf`| `0xf End : Begin:`|

###OCN Global VTTU Pointer Interpreter Control

* **Description**           

This is the global configuration register for the VTTU Pointer Interpreter


* **RTL Instant Name**    : `glbvpi_reg`

* **Address**             : `0x00003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29]`|`vtpilomaispen`| Enable/Disable forwarding AIS when LOM is detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[28]`|`vtpilominvlcntmod`| H4 monitoring mode. 1: Expected H4 is current frame in the validated sequence plus one. 0: Expected H4 is the last received value plus one.| `RW`| `0x0`| `0x0`|
|`[27:24]`|`vtpilomgoodthresh`| Threshold of number of contiguous frames with validated sequence	of multi framers in LOM state for condition to entering IM state.| `RW`| `0x3`| `0x3`|
|`[23:20]`|`vtpilominvlthresh`| Threshold of number of contiguous frames with invalidated sequence of multi framers in IM state  for condition to entering LOM state.| `RW`| `0x8`| `0x8`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`vtpiaisaispen`| Enable/Disable forwarding AIS when AIS state is detected at VTTU Pointer interpreter. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[17]`|`vtpilopaispen`| Enable/Disable forwarding AIS when LOP state is detected at VTTU Pointer interpreter. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[16]`|`vtpimajormode`| Majority mode detecting increment/decrement in VTTU pointer Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5| `RW`| `0x1`| `0x1`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:12]`|`vtpinorptrthresh`| Threshold of number of normal pointers between two contiguous frames within pointer adjustments.| `RW`| `0x3`| `0x3`|
|`[11:8]`|`vtpindfptrthresh`| Threshold of number of contiguous NDF pointers for entering LOP state at FSM.| `RW`| `0x8`| `0x8`|
|`[7:4]`|`vtpibadptrthresh`| Threshold of number of contiguous invalid pointers for entering LOP state at FSM.| `RW`| `0x8`| `0x8`|
|`[3:0]`|`vtpipohaistype`| Enable/disable VTTU POH defect types to downstream AIS in case of terminating the related VTTU. [0]: Enable for TIM defect [1]: Enable for Un-equipment defect [2]: Enable for VC-AIS [3]: Enable for PLM defect| `RW`| `0xf`| `0xf End : Begin:`|

###OCN Global Pointer Generator Control

* **Description**           

This is the global configuration register for the Tx Pointer Generator


* **RTL Instant Name**    : `glbtpg_reg`

* **Address**             : `0x00004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9:8]`|`txpgnorptrthresh`| Threshold of number of normal pointers between two contiguous frames to make a condition of pointer adjustments.| `RW`| `0x3`| `0x3`|
|`[7:4]`|`txpgflowthresh`| Overflow/underflow threshold to resynchronize read/write pointer of TxFiFo.| `RW`| `0x3`| `0x3`|
|`[3:0]`|`txpgadjthresh`| Adjustment threshold to make a condition of pointer increment/decrement.| `RW`| `0xc`| `0xc End : Begin:`|

###OCN Global Loopback Control

* **Description**           

This is the global configuration register for loopback modes at OCN block


* **RTL Instant Name**    : `glbloop_reg`

* **Address**             : `0x00005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3]`|`glblbec1remote`| Remote loopback for 48 EC1 lines.| `RW`| `0x0`| `0x0`|
|`[2]`|`glblbec1local`| Local loopback for 48 EC1 lines .| `RW`| `0x0`| `0x0`|
|`[1]`|`glblbholocal`| Local loopback for HoBus.| `RW`| `0x0`| `0x0`|
|`[0]`|`glblblolocal`| Local loopback for LoBus.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global select path DS3 over SDH config 1

* **Description**           

This is the global configuration register for selecting path DS3 over SDH (STS1 Id: 0-23) at SPI


* **RTL Instant Name**    : `glbds3osdh_reg1`

* **Address**             : `0x00010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`spids3oversdhsel1`| Bit #0 for STS1 #0. 1: Path  DS3 over SDH selected. 0: Normal path.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global select path DS3 over SDH config 2

* **Description**           

This is the global configuration register for selecting path DS3 over SDH (STS1 Id: 24-47) at SPI


* **RTL Instant Name**    : `glbds3osdh_reg2`

* **Address**             : `0x00011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`spids3oversdhsel2`| Bit #0 for STS1 #24. 1: Path  DS3 over SDH selected. 0: Normal path.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Rx_TOHBUS K Byte Control

* **Description**           

Configure TOHBUS for K Bytes at RX OCN.


* **RTL Instant Name**    : `glbrxtohbusctl_reg`

* **Address**             : `0x00014`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`rxtohbuskextctl`| K Byte Configuration for 24 Lines at Rx OCN. Each line use 1 bit to enable/disable Masking FF to Rx_OHBUS when Section alarm happen at Rx OCN. 1: Disable. 0: Enable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global TOHBUS K_Byte Threshold

* **Description**           

Configure TOHBUS for K_extention Bytes.


* **RTL Instant Name**    : `glbtohbusthr_reg`

* **Address**             : `0x00015`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:4]`|`rxtohbusk2stbthr`| Stable threshold Configuration for K2 Byte.| `RW`| `0x3`| `0x3`|
|`[3:0]`|`rxtohbusk1stbthr`| Stable threshold Configuration for K1 Byte.| `RW`| `0x3`| `0x3 End : Begin:`|

###OCN Global Tx_TOHBUS K Byte Control

* **Description**           

Configure TOHBUS for K Bytes at TX OCN.


* **RTL Instant Name**    : `glbtxtohbusctl_reg`

* **Address**             : `0x00016`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`txtohbuskextctl`| K Byte Configuration for 24 Lines at Rx OCN. Each line use 1 bit to enable/disable propagation suppression RDI,AIS in K2[2:0] at Tx OCN. Default is enable this function - when detect RDI/AIS at K2[2:0] from OHBUS, we must overwrite 3'b000 into K2[2:0]. 1: Disable. 0: Enable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN STS Pointer Interpreter Per Channel Control

* **Description**           

Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3.

Backdoor		: irxpp_stspp_inst.irxpp_stspp[0].rxpp_stspiramctl.array


* **RTL Instant Name**    : `spiramctl`

* **Address**             : `0x22000 - 0x22e2f`

* **Formula**             : `0x22000 + 512*SliceId + StsId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`stspichklom`| Enable/disable LOM checking. This field will be set to 1 when payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[13:12]`|`stspissdetpatt`| Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction.| `RW`| `0x0`| `0x0`|
|`[11]`|`stspiaisfrc`| Forcing SFM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[10]`|`stspissdeten`| Enable/disable checking SS bits in STSPI state machine. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[9]`|`stspiadjrule`| Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode| `RW`| `0x0`| `0x0`|
|`[8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN STS Pointer Generator Per Channel Control

* **Description**           

Each register is used to configure for STS pointer Generator engines.

Backdoor		: itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array


* **RTL Instant Name**    : `spgramctl`

* **Address**             : `0x23000 - 0x23e2f`

* **Formula**             : `0x23000 + 512*SliceId + StsId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`stspib3biperrfrc`| Forcing B3 Bip error. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[6]`|`stspilopfrc`| Forcing LOP. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[5]`|`stspiueqfrc`| Forcing SFM to UEQ state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[4]`|`stspiaisfrc`| Forcing SFM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[3:2]`|`stspissinspatt`| Configure pattern SS bits that is used to insert to pointer value.| `RW`| `0x0`| `0x0`|
|`[1]`|`stspissinsen`| Enable/disable SS bits insertion. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`stspipohins`| Enable/disable POH Insertion. High to enable insertion of POH.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN RXPP Per STS payload Control

* **Description**           

Each register is used to configure VT payload mode per STS.

Backdoor		: irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_demramctl.array


* **RTL Instant Name**    : `demramctl`

* **Address**             : `0x40000 - 0x5402f`

* **Formula**             : `0x40000 + 16384*SliceId + StsId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`pidemststerm`| Enable to terminate the related STS/VC. It means that STS POH defects related to the STS/VC to generate AIS to downstream. Must be set to 1. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[15:14]`|`pidemspetype`| Configure types of SPE. 0: Disable processing pointers of all VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2: TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.| `RW`| `0x0`| `0x0`|
|`[13:12]`|`pidemtug26type`| Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12| `RW`| `0x0`| `0x0`|
|`[11:10]`|`pidemtug25type`| Configure types of VT/TUs in TUG-2 #5.| `RW`| `0x0`| `0x0`|
|`[9:8]`|`pidemtug24type`| Configure types of VT/TUs in TUG-2 #4.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`pidemtug23type`| Configure types of VT/TUs in TUG-2 #3.| `RW`| `0x0`| `0x0`|
|`[5:4]`|`pidemtug22type`| Configure types of VT/TUs in TUG-2 #2.| `RW`| `0x0`| `0x0`|
|`[3:2]`|`pidemtug21type`| Configure types of VT/TUs in TUG-2 #1.| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pidemtug20type`| Configure types of VT/TUs in TUG-2 #0.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN VTTU Pointer Interpreter Per Channel Control

* **Description**           

Each register is used to configure for VTTU pointer interpreter engines.

Backdoor		: irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_vpiramctl.array


* **RTL Instant Name**    : `vpiramctl`

* **Address**             : `0x40800 - 0x54fff`

* **Formula**             : `0x40800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5]`|`vtpiloterm`| Enable to terminate the related VTTU. It means that VTTU POH defects related to the VTTU to generate AIS to downstream.| `RW`| `0x0`| `0x0`|
|`[4]`|`vtpiaisfrc`| Forcing SFM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[3:2]`|`vtpissdetpatt`| Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction.| `RW`| `0x0`| `0x0`|
|`[1]`|`vtpissdeten`| Enable/disable checking SS bits in PI State Machine. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`vtpiadjrule`| Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN TXPP Per STS Multiplexing Control

* **Description**           

Each register is used to configure VT payload mode per STS at Tx pointer generator.

Backdoor		: itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgdmctl.array


* **RTL Instant Name**    : `pgdemramctl`

* **Address**             : `0x60000 - 0x7402f`

* **Formula**             : `0x60000 + 16384*SliceId + StsId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:14]`|`pgdemspetype`| Configure types of SPE. 0: Disable processing pointers of all VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2: TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.| `RW`| `0x0`| `0x0`|
|`[13:12]`|`pgdemtug26type`| Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12| `RW`| `0x0`| `0x0`|
|`[11:10]`|`pgdemtug25type`| Configure types of VT/TUs in TUG-2 #5.| `RW`| `0x0`| `0x0`|
|`[9:8]`|`pgdemtug24type`| Configure types of VT/TUs in TUG-2 #4.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`pgdemtug23type`| Configure types of VT/TUs in TUG-2 #3.| `RW`| `0x0`| `0x0`|
|`[5:4]`|`pgdemtug22type`| Configure types of VT/TUs in TUG-2 #2.| `RW`| `0x0`| `0x0`|
|`[3:2]`|`pgdemtug21type`| Configure types of VT/TUs in TUG-2 #1.| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pgdemtug20type`| Configure types of VT/TUs in TUG-2 #0.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN VTTU Pointer Generator Per Channel Control

* **Description**           

Each register is used to configure for VTTU pointer Generator engines.

Backdoor		: itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgctl.array


* **RTL Instant Name**    : `vpgramctl`

* **Address**             : `0x60800 - 0x74fff`

* **Formula**             : `0x60800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`vtpgbiperrfrc`| Forcing Bip error. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[6]`|`vtpglopfrc`| Forcing LOP. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[5]`|`vtpgueqfrc`| Forcing SFM to UEQ state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[4]`|`vtpgaisfrc`| Forcing SFM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[3:2]`|`vtpgssinspatt`| Configure pattern SS bits that is used to insert to Pointer value.| `RW`| `0x0`| `0x0`|
|`[1]`|`vtpgssinsen`| Enable/disable SS bits insertion. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`vtpgpohins`| Enable/ disable POH Insertion. High to enable insertion of POH.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx STS/VC per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of STS/VC pointer interpreter.  %%

Each register is used to store 6 sticky bits for 6 alarms in the STS/VC.


* **RTL Instant Name**    : `upstschstkram`

* **Address**             : `0x22140 - 0x22f6f`

* **Formula**             : `0x22140 + 512*SliceId + StsId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:5]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[4]`|`stspistsnewdetintr`| Set to 1 while an New Pointer Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[3]`|`stspistsndfintr`| Set to 1 while an NDF event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[2]`|`stspicepuneqstatepchgintr`| Set to 1  while there is change in CEP Un-equip Path in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in CEP Un-equip Path state or not.| `W1C`| `0x0`| `0x0`|
|`[1]`|`stspistsaisstatechgintr`| Set to 1 while there is change in AIS state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in AIS state or not.| `W1C`| `0x0`| `0x0`|
|`[0]`|`stspistslopstatechgintr`| Set 1 to while there is change in LOP state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in LOP state or not.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx STS/VC per Alarm Current Status

* **Description**           

This is the per Alarm current status of STS/VC pointer interpreter.  %%

Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC.


* **RTL Instant Name**    : `upstschstaram`

* **Address**             : `0x22180 - 0x22faf`

* **Formula**             : `0x22180 + 512*SliceId + StsId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[2]`|`stspistscepuneqpcurstatus`| CEP Un-eqip Path current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  StsPiStsCepUeqPStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.| `RO`| `0x0`| `0x0`|
|`[1]`|`stspistsaiscurstatus`| AIS current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  StsPiStsAISStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.| `RO`| `0x0`| `0x0`|
|`[0]`|`stspistslopcurstatus`| LOP current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  StsPiStsLopStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx VT/TU per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of VT/TU pointer interpreter . Each register is used to store 5 sticky bits for 5 alarms in the VT/TU.


* **RTL Instant Name**    : `upvtchstkram`

* **Address**             : `0x42800 - 0x56fff`

* **Formula**             : `0x42800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:5]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[4]`|`vtpistsnewdetintr`| Set to 1 while an New Pointer Detection event is detected at VT/TU pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[3]`|`vtpistsndfintr`| Set to 1 while an NDF event is detected at VT/TU pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[2]`|`vtpistscepuneqvstatechgintr`| Set 1 to while there is change in Unequip state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in Uneqip state or not.| `W1C`| `0x0`| `0x0`|
|`[1]`|`vtpistsaisstatechgintr`| Set 1 to while there is change in AIS state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in LOP state or not.| `W1C`| `0x0`| `0x0`|
|`[0]`|`vtpistslopstatechgintr`| Set 1 to while there is change in LOP state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in AIS state or not.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx VT/TU per Alarm Current Status

* **Description**           

This is the per Alarm current status of VT/TU pointer interpreter. Each register is used to store 3 bits to store current status of 3 alarms in the VT/TU.


* **RTL Instant Name**    : `upvtchstaram`

* **Address**             : `0x43000 - 0x57fff`

* **Formula**             : `0x43000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[2]`|`vtpistscepuneqcurstatus`| Unequip current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsCepUneqVStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set.| `RO`| `0x0`| `0x0`|
|`[1]`|`vtpistsaiscurstatus`| AIS current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsAISStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set.| `RO`| `0x0`| `0x0`|
|`[0]`|`vtpistslopcurstatus`| LOP current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsLopStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode


* **RTL Instant Name**    : `adjcntperstsram`

* **Address**             : `0x22080 - 0x22eaf`

* **Formula**             : `0x22080 + 512*SliceId + 64*ReadOnlyMode + 32*AdjMode + StsId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$ReadOnlyMode(0-1)`

    * `$AdjMode(0-1)`

    * `$StsId(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`rxppstspiptadjcnt`| The pointer Increment counter or decrease counter. Bit [6] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VT/TU pointer interpreter. These counters are in saturation mode


* **RTL Instant Name**    : `adjcntperstkram`

* **Address**             : `0x41000 - 0x55fff`

* **Formula**             : `0x41000 + 16384*SliceId + 2048*ReadOnlyMode + 1024*AdjMode + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$ReadOnlyMode(0-1)`

    * `$AdjMode(0-1)`

    * `$StsId(0-23):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`rxppstspiptadjcnt`| The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg STS per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms


* **RTL Instant Name**    : `stspgstkram`

* **Address**             : `0x23140 - 0x23f6f`

* **Formula**             : `0x23140 + 512*SliceId + StsId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[3]`|`stspgaisintr`| Set to 1 while an AIS status event (at h2pos) is detected at Tx STS Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[2]`|`stspgfifoovfintr`| Set to 1 while an FIFO Overflowed event is detected at Tx STS Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[1]`|`stspgndfintr`| Set to 1 while an NDF status event (at h2pos) is detected at Tx STS Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[0]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg STS pointer adjustment per channel counter

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode


* **RTL Instant Name**    : `adjcntpgperstsram`

* **Address**             : `0x23080 - 0x23eaf`

* **Formula**             : `0x23080 + 512*SliceId + 64*ReadOnlyMode + 32*AdjMode + StsId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$ReadOnlyMode(0-1)`

    * `$AdjMode(0-1)`

    * `$StsId(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`stspgptadjcnt`| The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg VTTU per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of STS/VT/TU pointer generator . Each register is used to store 3 sticky bits for 3 alarms


* **RTL Instant Name**    : `vtpgstkram`

* **Address**             : `0x62800 - 0x76fff`

* **Formula**             : `0x62800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[3]`|`vtpgaisintr`| Set to 1 while an AIS status event (at h2pos) is detected at Tx VTTU Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[2]`|`vtpgfifoovfintr`| Set to 1 while an FIFO Overflowed event is detected at Tx VTTU Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[1]`|`vtpgndfintr`| Set to 1 while an NDF status event (at h2pos) is detected at Tx VTTU Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[0]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg VTTU pointer adjustment per channel counter

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VTTU pointer generator. These counters are in saturation mode


* **RTL Instant Name**    : `adjcntpgpervtram`

* **Address**             : `0x61000 - 0x75fff`

* **Formula**             : `0x61000 + 16384*SliceId + 2048*ReadOnlyMode + 1024*AdjMode + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$ReadOnlyMode(0-1)`

    * `$AdjMode(0-1)`

    * `$StsId(0-23):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`vtpgptadjcnt`| The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Global K1 Stable Monitoring Threshold Control

* **Description**           

Configure thresholds for monitoring change of K1 state at TOH monitoring. There are two thresholds for each kind of threshold.


* **RTL Instant Name**    : `tohglbk1stbthr_reg`

* **Address**             : `0x24000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10:8]`|`tohk1stbthr2`| The second threshold for detecting stable K1 status.| `RW`| `0x3`| `0x3`|
|`[7:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2:0]`|`tohk1stbthr1`| The first threshold for detecting stable K1 status.| `RW`| `0x4`| `0x4 End : Begin:`|

###OCN TOH Global K2 Stable Monitoring Threshold Control

* **Description**           

Configure thresholds for monitoring change of K2 state at TOH monitoring. There are two thresholds for each kind of threshold.


* **RTL Instant Name**    : `tohglbk2stbthr_reg`

* **Address**             : `0x24001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10:8]`|`tohk2stbthr2`| The second threshold for detecting stable K2 status.| `RW`| `0x3`| `0x3`|
|`[7:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2:0]`|`tohk2stbthr1`| The first threshold for detecting stable K2 status.| `RW`| `0x4`| `0x4 End : Begin:`|

###OCN TOH Global S1 Stable Monitoring Threshold Control

* **Description**           

Configure thresholds for monitoring change of S1 state at TOH monitoring. There are two thresholds for each kind of threshold.


* **RTL Instant Name**    : `tohglbs1stbthr_reg`

* **Address**             : `0x24002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`tohs1stbthr2`| The second threshold for detecting stable S1 status.| `RW`| `0x5`| `0x5`|
|`[7:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3:0]`|`tohs1stbthr1`| The first threshold for detecting stable S1 status.| `RW`| `0x5`| `0x5 End : Begin:`|

###OCN TOH Global RDI_L Detecting Threshold Control

* **Description**           

Configure thresholds for detecting RDI_L at TOH monitoring. There are two thresholds for each kind of threshold.


* **RTL Instant Name**    : `tohglbrdidetthr_reg`

* **Address**             : `0x24003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`tohrdidetthr2`| The second threshold for detecting RDI_L.| `RW`| `0x4`| `0x4`|
|`[7:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3:0]`|`tohrdidetthr1`| The first threshold for detecting RDI_L.| `RW`| `0x9`| `0x9 End : Begin:`|

###OCN TOH Global AIS_L Detecting Threshold Control

* **Description**           

Configure thresholds for detecting AIS_L at TOH monitoring. There are two thresholds for each kind of threshold.


* **RTL Instant Name**    : `tohglbaisdetthr_reg`

* **Address**             : `0x24004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`tohaisdetthr2`| The second threshold for detecting AIS_L.| `RW`| `0x3`| `0x3`|
|`[7:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3:0]`|`tohaisdetthr1`| The first threshold for detecting AIS_L.| `RW`| `0x4`| `0x4 End : Begin:`|

###OCN TOH Global K1 Sampling Threshold Control

* **Description**           

Configure thresholds for sampling K1 bytes to detect APS defect at TOH monitoring. There are two thresholds.


* **RTL Instant Name**    : `tohglbk1smpthr_reg`

* **Address**             : `0x24005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`tohk1smpthr2`| The second threshold for sampling K1 to detect APS defect.| `RW`| `0x7`| `0x7`|
|`[7:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3:0]`|`tohk1smpthr1`| The first threshold for sampling K1 to detect APS defect.| `RW`| `0x7`| `0x7 End : Begin:`|

###OCN TOH Global Error Counter Control

* **Description**           

Configure mode for counters in TOH monitoring.


* **RTL Instant Name**    : `tohglberrcntmod_reg`

* **Address**             : `0x24006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`tohreierrcntmod`| REI counter Mode. 1: Saturation mode 0: Roll over mode| `RW`| `0x1`| `0x1`|
|`[1]`|`tohb2errcntmod`| B2 counter Mode. 1: Saturation mode 0: Roll over mode| `RW`| `0x1`| `0x1`|
|`[0]`|`tohb1errcntmod`| B1 counter Mode. 1: Saturation mode 0: Roll over mode| `RW`| `0x1`| `0x1 End : Begin:`|

###OCN TOH Montoring Affect Control

* **Description**           

Configure affective mode for TOH monitoring.


* **RTL Instant Name**    : `tohglbaffen_reg`

* **Address**             : `0x24007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3]`|`tohaisaffstbmon`| AIS affects to Stable monitoring status of the line at which LOF or LOS is detected 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[2]`|`tohaisaffrdilmon`| AIS affects to RDI-L monitoring status of the line at which LOF or LOS is detected. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[1]`|`tohaisaffaislmon`| AIS affects to AIS-L monitoring  status of the line at which LOF or LOS is detected. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[0]`|`tohaisafferrcnt`| AIS affects to error counters (B1,B2,REI) of the line at which LOF or LOS is detected. 1: Disable 0: Enable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring Per Channel Control

* **Description**           

Each register is used to configure for TOH monitoring engine of the related line.


* **RTL Instant Name**    : `tohramctl`

* **Address**             : `0x24100 - 0x2412f`

* **Formula**             : `0x24100  + Ec1Id`

* **Where**               : 

    * `$Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:9]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[8]`|`b1errcntblkmod`| B1 counter Block Mode for fedding PM. 1: Block count mode 0: Bit count mode| `RW`| `0x0`| `0x0`|
|`[7]`|`b2errcntblkmod`| B2 counter Block Mode for fedding PM. 1: Block count mode 0: Bit count mode| `RW`| `0x0`| `0x0`|
|`[6]`|`reierrcntblkmod`| REI counter Block Mode for fedding PM. 1: Block count mode 0: Bit count mode| `RW`| `0x0`| `0x0`|
|`[5]`|`stbrdiislthressel`| Select the thresholds for detecting RDI_L 1: Threshold 2 is selected 0: Threshold 1 is selected| `RW`| `0x0`| `0x0`|
|`[4]`|`stbaislthressel`| Select the thresholds for detecting AIS_L 1: Threshold 2 is selected 0: Threshold 1 is selected| `RW`| `0x0`| `0x0`|
|`[3]`|`k2stbmd`| Select K2[7:3] or K2[7:0] to detect validated K2 value 1: K2[7:0] value is selected 0: K2[7:3] value is selected| `RW`| `0x0`| `0x0`|
|`[2]`|`stbk1k2thressel`| Select the thresholds for detecting stable or non-stable K1/K2 status 1: Threshold 2 is selected 0: Threshold 1 is selected| `RW`| `0x0`| `0x0`|
|`[1]`|`stbs1thressel`| Select the thresholds for detecting stable or non-stable S1 status 1: Threshold 2 is selected 0: Threshold 1 is selected| `RW`| `0x0`| `0x0`|
|`[0]`|`smpk1thressel`| Select the sample threshold for detecting APS defect. 1: Threshold 2 is selected 0: Threshold 1 is selected| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B1 Error Read Only Counter

* **Description**           

Each register is used to store B1 error read only counter of the related line.


* **RTL Instant Name**    : `tohb1errrocnt`

* **Address**             : `0x24800 - 0x2482f`

* **Formula**             : `0x24800 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[22:0]`|`b1errrocnt`| B1 Error Read Only Counter.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B1 Error Read to Clear Counter

* **Description**           

Each register is used to store B1 error read to clear counter of the related line.


* **RTL Instant Name**    : `tohb1errr2ccnt`

* **Address**             : `0x24a00 - 0x24a2f`

* **Formula**             : `0x24a00 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[22:0]`|`b1errr2ccnt`| B1 Error Read To Clear Counter.| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B1 Block Error Read Only Counter

* **Description**           

Each register is used to store B1 Block error read only counter of the related line.


* **RTL Instant Name**    : `tohb1blkerrrocnt`

* **Address**             : `0x24c00 - 0x24c2f`

* **Formula**             : `0x24c00 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[15:0]`|`b1blkerrrocnt`| B1 Block Error Read Only Counter.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B1 Block Error Read to Clear Counter

* **Description**           

Each register is used to store B1 Block error read to clear counter of the related line.


* **RTL Instant Name**    : `tohb1blkerrr2ccnt`

* **Address**             : `0x24e00 - 0x24e2f`

* **Formula**             : `0x24e00 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[15:0]`|`b1blkerrr2ccnt`| B1 Block Error Read To Clear Counter.| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B2 Error Read Only Counter

* **Description**           

Each register is used to store B2 error read only counter of the related line.


* **RTL Instant Name**    : `tohb2errrocnt`

* **Address**             : `0x24840 - 0x2486f`

* **Formula**             : `0x24840 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[22:0]`|`b2errrocnt`| B2 Error Read Only Counter.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B2 Error Read to Clear Counter

* **Description**           

Each register is used to store B2 error read to clear counter of the related line.


* **RTL Instant Name**    : `tohb2errr2ccnt`

* **Address**             : `0x24a40 - 0x24a6f`

* **Formula**             : `0x24a40 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[22:0]`|`b2errr2ccnt`| B2 Error Read To Clear Counter.| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B2 Block Error Read Only Counter

* **Description**           

Each register is used to store B2 Block error read only counter of the related line.


* **RTL Instant Name**    : `tohb2blkerrrocnt`

* **Address**             : `0x24c40 - 0x24c6f`

* **Formula**             : `0x24c40 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[15:0]`|`b2blkerrrocnt`| B2 Block Error Read Only Counter.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B2 Block Error Read to Clear Counter

* **Description**           

Each register is used to store B2 Block error read to clear counter of the related line.


* **RTL Instant Name**    : `tohb2blkerrr2ccnt`

* **Address**             : `0x24e40 - 0x24e6f`

* **Formula**             : `0x24e40 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[15:0]`|`b2blkerrr2ccnt`| B2 Block Error Read To Clear Counter.| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring REI Error Read Only Counter

* **Description**           

Each register is used to store REI error read only counter of the related line.


* **RTL Instant Name**    : `tohreierrrocnt`

* **Address**             : `0x24940 - 0x2496f`

* **Formula**             : `0x24940 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[22:0]`|`reierrrocnt`| REI Error Read Only Counter.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring REI Error Read to Clear Counter

* **Description**           

Each register is used to store REI error read to clear counter of the related line.


* **RTL Instant Name**    : `tohreierrr2ccnt`

* **Address**             : `0x24b40 - 0x24b6f`

* **Formula**             : `0x24b40 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[22:0]`|`reierrr2ccnt`| REI Error Read To Clear Counter.| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring REI Block Error Read Only Counter

* **Description**           

Each register is used to store REI Block error read only counter of the related line.


* **RTL Instant Name**    : `tohreiblkerrrocnt`

* **Address**             : `0x24d40 - 0x24d6f`

* **Formula**             : `0x24d40 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[15:0]`|`reiblkerrrocnt`| REI Block Error Read Only Counter.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring REI Block Error Read to Clear Counter

* **Description**           

Each register is used to store REI Block error read to clear counter of the related line.


* **RTL Instant Name**    : `tohreiblkerrr2ccnt`

* **Address**             : `0x24f40 - 0x24f6f`

* **Formula**             : `0x24f40 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[15:0]`|`reiblkerrr2ccnt`| REI Block Error Read To Clear Counter.| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH K1 Monitoring Status

* **Description**           

Each register is used to store K1 Monitoring Status of the related line.


* **RTL Instant Name**    : `tohk1monsta`

* **Address**             : `0x24880 - 0x248af`

* **Formula**             : `0x24880 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23]`|`curapsdef`| current APS Defect. 1: APS defect is detected from APS bytes. 0: APS defect is not detected from APS bytes.| `RW`| `0x0`| `0x0`|
|`[22:19]`|`k1smpcnt`| Sampling counter.| `RW`| `0x0`| `0x0`|
|`[18:16]`|`samek1cnt`| The number of same contiguous K1 bytes. It is held at StbK1Thr value when the number of same contiguous K1 bytes is equal to or more than the StbK1Thr value.In this case, K1 bytes are stable.| `RW`| `0x0`| `0x0`|
|`[15:8]`|`k1stbval`| Stable K1 value. It is updated when detecting a new stable value.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`k1curval`| Current K1 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN TOH K2 Monitoring Status

* **Description**           

Each register is used to store K2 Monitoring Status of the related line.


* **RTL Instant Name**    : `tohk2monsta`

* **Address**             : `0x248c0 - 0x248ef`

* **Formula**             : `0x248c0 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[28:21]`|`internal`| Internal.| `RW`| `0x0`| `0x0`|
|`[20]`|`curaisl`| current AIS-L Defect. 1: AIS-L defect is detected from K2 bytes. 0: AIS-L defect is not detected from K2 bytes.| `RW`| `0x0`| `0x0`|
|`[19]`|`currdil`| current RDI-L Defect. 1: RDI-L defect is detected from K2 bytes. 0: RDI-L defect is not detected from K2 bytes.| `RW`| `0x0`| `0x0`|
|`[18:16]`|`samek2cnt`| The number of same contiguous K2 bytes. It is held at StbK2Thr value when the number of same contiguous K2 bytes is equal to or more than the StbK2Thr value.In this case, K2 bytes are stable.| `RW`| `0x0`| `0x0`|
|`[15:8]`|`k2stbval`| Stable K2 value. It is updated when detecting a new stable value.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`k2curval`| Current K2 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN TOH S1 Monitoring Status

* **Description**           

Each register is used to store S1 Monitoring Status of the related line.


* **RTL Instant Name**    : `tohs1monsta`

* **Address**             : `0x24900 - 0x2492f`

* **Formula**             : `0x24900 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[19:16]`|`sames1cnt`| The number of same contiguous S1 bytes. It is held at StbS1Thr value when the number of same contiguous S1 bytes is equal to or more than the StbS1Thr value.In this case, S1 bytes are stable.| `RW`| `0x0`| `0x0`|
|`[15:8]`|`s1stbval`| Stable S1 value. It is updated when detecting a new stable value.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`s1curval`| Current S1 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line per Alarm Interrupt Enable Control

* **Description**           

This is the per Alarm interrupt enable of Rx framer and TOH monitoring. Each register is used to store 9 bits to enable interrupts when the related alarms in related line happen.


* **RTL Instant Name**    : `tohintperalrenbctl`

* **Address**             : `0x24200 - 0x2422f`

* **Formula**             : `0x24200 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[12]`|`tcalstatechgintren`| Set 1 to enable TCA-L state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[11]`|`sdlstatechgintren`| Set 1 to enable SD-L state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[10]`|`sflstatechgintren`| Set 1 to enable SF-L state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[9]`|`timlstatechgintren`| Set 1 to enable TIM-L state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[8]`|`s1stbstatechgintren`| Set 1 to enable S1 Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[7]`|`k1stbstatechgintren`| Set 1 to enable K1 Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[6]`|`apslstatechgintren`| Set 1 to enable APS-L Defect Stable state change event in the related line to generate an interrupt..| `RW`| `0x0`| `0x0`|
|`[5]`|`k2stbstatechgintren`| Set 1 to enable K2 Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[4]`|`rdilstatechgintren`| Set 1 to enable RDI-L Defect Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[3]`|`aislstatechgintren`| Set 1 to enable AIS-L Defect Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[2]`|`oofstatechgintren`| Set 1 to enable OOF Defect Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[1]`|`lofstatechgintren`| Set 1 to enable LOF Defect Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[0]`|`losstatechgintren`| Set 1 to enable LOS Defect Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line.


* **RTL Instant Name**    : `tohintsta`

* **Address**             : `0x24240 - 0x2426f`

* **Formula**             : `0x24240 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[12]`|`tcalstatechgintr`| Set 1 while TCA-L state change detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[11]`|`sdlstatechgintr`| Set 1 while SD-L state change detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[10]`|`sflstatechgintr`| Set 1 while SF-L state change detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[9]`|`timlstatechgintr`| Set 1 while Tim-L state change detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[8]`|`s1stbstatechgintr`| Set 1 one new stable S1 value detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[7]`|`k1stbstatechgintr`| Set 1 one new stable K1 value detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[6]`|`apslstatechgintr`| Set 1 while APS-L Defect state change event happens in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[5]`|`k2stbstatechgintr`| Set 1 one new stable K2 value detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[4]`|`rdilstatechgintr`| Set 1 while RDI-L Defect state change event happens in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[3]`|`aislstatechgintr`| Set 1 while AIS-L Defect state change event happens in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[2]`|`oofstatechgintr`| Set 1 while OOF state change event happens in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[1]`|`lofstatechgintr`| Set 1 while LOF state change event happens in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[0]`|`losstatechgintr`| Set 1 while LOS state change event happens in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line per Alarm Current Status

* **Description**           

This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line.


* **RTL Instant Name**    : `tohcursta`

* **Address**             : `0x24280 - 0x242af`

* **Formula**             : `0x24280 + Ec1Id`

* **Where**               : 

    * `Ec1Id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[12]`|`tcaldefcurstatus`| TCA-L Defect  in the related line. 1: TCA-L defect is set 0: TCA-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[11]`|`sdldefcurstatus`| SD-L Defect  in the related line. 1: SD-L defect is set 0: SD-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[10]`|`sfldefcurstatus`| SF-L Defect  in the related line. 1: SF-L defect is set 0: SF-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[9]`|`timldefcurstatus`| TIM-L Defect  in the related line. 1: TIM-L defect is set 0: TIM-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[8]`|`s1stbstatechgintr`| S1 stable status  in the related line.| `RW`| `0x0`| `0x0`|
|`[7]`|`k1stbcurstatus`| K1 stable status  in the related line.| `RW`| `0x0`| `0x0`|
|`[6]`|`apslcurstatus`| APS-L Defect  in the related line. 1: APS-L defect is set 0: APS-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[5]`|`k2stbcurstatus`| K2 stable status  in the related line.| `RW`| `0x0`| `0x0`|
|`[4]`|`rdildefcurstatus`| RDI-L Defect  in the related line. 1: RDI-L defect is set 0: RDI-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[3]`|`aisldefcurstatus`| AIS-L Defect  in the related line. 1: AIS-L defect is set 0: AIS-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[2]`|`oofcurstatus`| OOF current status in the related line. 1: OOF state 0: Not OOF state.| `RW`| `0x0`| `0x0`|
|`[1]`|`lofcurstatus`| LOF current status in the related line. 1: LOF state 0: Not LOF state.| `RW`| `0x0`| `0x0`|
|`[0]`|`loscurstatus`| LOS current status in the related line. 1: LOS state 0: Not LOS state.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line Per Line Interrupt Enable Control 1

* **Description**           

The register consists of 24 bits for 24 lines (EC1 Id: 0-23) at Rx side to enable interrupts when alarms of related lines happen. It is noted that this register is higher priority than the OCN Rx Line per Alarm Interrupt Enable Control register.


* **RTL Instant Name**    : `tohintperlineenctl1`

* **Address**             : `0x242f0`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`ocnrxllineintren1`| Bit #0 to enable for line #0.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line Per Line Interrupt Enable Control 2

* **Description**           

The register consists of 24 bits for 24 lines (EC1 Id: 24-47) at Rx side to enable interrupts when alarms of related lines happen. It is noted that this register is higher priority than the OCN Rx Line per Alarm Interrupt Enable Control register.


* **RTL Instant Name**    : `tohintperlineenctl2`

* **Address**             : `0x242f1`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`ocnrxllineintren2`| Bit #0 to enable for line #24.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line per Line Interrupt OR Status 1

* **Description**           

The register consists of 24 bits for 24 lines (EC1 Id: 0-23) at Rx side. Each bit is used to store Interrupt OR status of the related line. If there are any bits in this register and they are enabled to raise interrupt, the Rx line level interrupt bit is set.


* **RTL Instant Name**    : `tohintperline1`

* **Address**             : `0x242f2`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`ocnrxllineintr1`| Set to 1 to indicate that there is any interrupt status bit in the OCN Rx Line per Alarm Interrupt Status register of the related STS/VC to be set and they are enabled to raise interrupt. Bit 0 for line #0, respectively.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line per Line Interrupt OR Status 2

* **Description**           

The register consists of 24 bits for 24 lines (EC1 Id: 24-47) at Rx side. Each bit is used to store Interrupt OR status of the related line. If there are any bits in this register and they are enabled to raise interrupt, the Rx line level interrupt bit is set.


* **RTL Instant Name**    : `tohintperline2`

* **Address**             : `0x242f3`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`ocnrxllineintr2`| Set to 1 to indicate that there is any interrupt status bit in the OCN Rx Line per Alarm Interrupt Status register of the related STS/VC to be set and they are enabled to raise interrupt. Bit 0 for line #24, respectively.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line per Slice24 Interrupt OR Status

* **Description**           

The register consists of 2 bits for 2 slice at Rx side. Each bit is used to store Interrupt OR status of the related slice24.


* **RTL Instant Name**    : `tohintperslice`

* **Address**             : `0x242f4`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`ocnrxlsliceintr2`| Set to 1 to indicate that there is any interrupt status bit in the OCN Rx Line per Line Interrupt OR Status 2 register of the related Slice24 #1 to be set and they are enabled to raise interrupt.| `RW`| `0x0`| `0x0`|
|`[0]`|`ocnrxlsliceintr1`| Set to 1 to indicate that there is any interrupt status bit in the OCN Rx Line per Line Interrupt OR Status 1 register of the related Slice24 #1 to be set and they are enabled to raise interrupt.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Tx Framer Per Channel Control

* **Description**           

Each register is used to configure operation modes for Tx framer engine and APS pattern inserted into APS bytes (K1 and K2 bytes) of the SONET/SDH line being relative with the address of the address of the register.


* **RTL Instant Name**    : `tfmramctl`

* **Address**             : `0x21000 - 0x2142f`

* **Formula**             : `0x21000 + 1024*SliceId + StsId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`ocntxs1pat`| Pattern to insert into S1 byte.| `RW`| `0x0`| `0x0`|
|`[23:8]`|`ocntxapspat`| Pattern to insert into APS bytes (K1, K2).| `RW`| `0x0`| `0x0`|
|`[7]`|`ocntxs1ldis`| S1 insertion disable 1: Disable 0: Enable.| `RW`| `0x0`| `0x0`|
|`[6]`|`ocntxapsdis`| Disable to process APS 1: Disable 0: Enable.| `RW`| `0x0`| `0x0`|
|`[5]`|`ocntxreildis`| Auto REI_L insertion disable 1: Disable inserting REI_L automatically. 0: Enable automatically inserting REI_L.| `RW`| `0x0`| `0x0`|
|`[4]`|`ocntxrdildis`| Auto RDI_L insertion disable 1: Disable inserting RDI_L automatically. 0: Enable automatically inserting RDI_L.| `RW`| `0x0`| `0x0`|
|`[3]`|`ocntxautob2dis`| Auto B2 disable 1: Disable inserting calculated B2 values into B2 positions automatically. 0: Enable automatically inserting calculated B2 values into B2 positions.| `RW`| `0x0`| `0x0`|
|`[2]`|`ocntxrdilthressel`| Select the number of frames being inserted RDI-L defects when receive direction requests generating RDI-L at transmit direction. 1: Threshold2 is selected. 0: Threshold1 is selected.| `RW`| `0x0`| `0x0`|
|`[1]`|`ocntxrdilfrc`| RDI-L force. 1: Force RDI-L defect into transmit data. 0: Not force RDI-L| `RW`| `0x0`| `0x0`|
|`[0]`|`ocntxaislfrc`| AIS-L force. 1: Force AIS-L defect into transmit data. 0: Not force AIS-L| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Tx J0 Insertion Buffer

* **Description**           

Each register is used to store one J0 double word of in 64-byte message of the SONET/SDH line being relative with the address of the address of the register inserted into J0 positions.


* **RTL Instant Name**    : `tfmj0insctl`

* **Address**             : `0x21200 - 0x2162f`

* **Formula**             : `0x21200 + 1024*SliceId + 16*StsId + DwId`

* **Where**               : 

    * `$SliceId(0-0): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47`

    * `$StsId(0-23)`

    * `DwId(0-15): Double word (4bytes) ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`ocntxj0`| J0 pattern for insertion.| `RW`| `0x0`| `0x0 End :`|

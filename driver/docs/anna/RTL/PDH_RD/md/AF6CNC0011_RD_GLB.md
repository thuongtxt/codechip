## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_GLB
####Register Table

|Name|Address|
|-----|-----|
|`Device Version ID`|`0x00_0000`|
|`GLB Sub-Core Active`|`0x00_0001`|
|`Chip Interrupt Status`|`0x00_0002`|
|`Chip Interrupt Enable Control`|`0x00_0003`|
|`Chip Interrupt Restore Control`|`0x00_0004`|
|`EPort Interrupt Enable Control`|`0x00_00C0`|
|`EPort Link Event`|`0x00_00C4`|
|`EPort Link Status`|`0x00_00C8`|
|`EPort Link Force`|`0x00_00CC`|
|`Write Hold Data63_32`|`0x00_0011`|
|`Write Hold Data95_64`|`0x00_0012`|
|`Write Hold Data127_96`|`0x00_0013`|
|`Read Hold Data63_32`|`0x00_0021`|
|`Read Hold Data95_64`|`0x00_0022`|
|`Read Hold Data127_96`|`0x00_0023`|
|`Mac Address Bit31_0 Control`|`0x00_0100`|
|`Mac Address Bit47_32 Control`|`0x00_0101`|
|`IP Address Bit31_0 Control`|`0x00_0102`|
|`IP Address Bit63_32 Control`|`0x00_0103`|
|`IP Address Bit95_64 Control`|`0x00_0104`|
|`IP Address Bit127_96 Control`|`0x00_0105`|
|`CDR REF OUT 8KHZ Control`|`0x00_010c`|
|`RAM Parity Force Control`|`0x130`|
|`RAM parity Disable Control`|`0x131`|
|`RAM parity Error Sticky`|`0x112`|
|`Read HA Hold Data127_96`|`0x00121`|
|`Read HA Address Bit3_0 Control`|`0x00200`|
|`Read HA Address Bit7_4 Control`|`0x00210`|
|`Read HA Address Bit11_8 Control`|`0x00220`|
|`Read HA Address Bit15_12 Control`|`0x00230`|
|`Read HA Address Bit19_16 Control`|`0x00240`|
|`Read HA Address Bit23_20 Control`|`0x00250`|
|`Read HA Address Bit24 and Data Control`|`0x00260`|
|`Read HA Hold Data63_32`|`0x00270`|
|`Read HA Hold Data95_64`|`0x00271`|
|`Read HA Hold Data127_96`|`0x00272`|
|`Temperate Event`|`0x00_00D0`|
|`PDA DDR CRC Error Counter`|`0x000142(RO)`|
|`PDA DDR CRC Error Counter`|`0x000141(R2C)`|
|`PDA DDR Force Error Control`|`0x000140`|
|`Rx DDR ECC Correctable Error Counter`|`0x00014a(RO)`|
|`Rx DDR ECC Correctable Error Counter`|`0x000149(R2C)`|
|`Rx DDR ECC UnCorrectable Error Counter`|`0x00014C(RO)`|
|`Rx DDR ECC UnCorrectable Error Counter`|`0x00014B(R2C)`|
|`Rx DDR Force Error Control`|`0x000148`|
|`Tx DDR ECC Correctable Error Counter`|`0x000152(RO)`|
|`Tx DDR ECC Correctable Error Counter`|`0x000151(R2C)`|
|`Tx DDR ECC UnCorrectable Error Counter`|`0x000154(RO)`|
|`Tx DDR ECC UnCorrectable Error Counter`|`0x000153(R2C)`|
|`Tx DDR Force Error Control`|`0x000150`|


###Device Version ID

* **Description**           

This register indicates the module' name and version.


* **RTL Instant Name**    : `VersionID`

* **Address**             : `0x00_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`year`| 2013| `RO`| `0x0D`| `0x0D`|
|`[23:16]`|`week`| Week Synthesized FPGA| `RO`| `0x0C`| `0x0C`|
|`[15:8]`|`day`| Day Synthesized FPGA| `RO`| `0x16`| `0x16`|
|`[7:4]`|`verid`| FPGA Version| `RO`| `0xO`| `0x0`|
|`[3:0]`|`numid`| Number of FPGA Version| `RO`| `0x1`| `0x1 End: Begin:`|

###GLB Sub-Core Active

* **Description**           

This register indicates the active ports.


* **RTL Instant Name**    : `Active`

* **Address**             : `0x00_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9]`|`pmcpact`| PMC Active 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[8]`|`ethpact`| Ethernet Interface Active 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[7]`|`cdrpact`| CDR Active 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[6]`|`clapact`| CLA Active 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[5]`|`pwepact`| PWE Active 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[4]`|`pdapact`| PDA Active 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[3]`|`plapact`| PLA Active 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[2]`|`mappact`| MAP Active 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[1]`|`pdhpact`| PDH Active 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`ocnpact`| OCN Active 1: Enable 0: Disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Chip Interrupt Status

* **Description**           

This register configure interrupt enable.


* **RTL Instant Name**    : `inten_status`

* **Address**             : `0x00_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29]`|`cdr2intenable`| CDR Group1 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[28]`|`cdr1intenable`| CDR Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[27]`|`sysmontempenable`| SysMon temperature Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[26]`|`parintenable`| RAM Parity Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[25]`|`mdlintenable`| MDL DE3 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[24]`|`prmintenable`| PRM DE1 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[23:22]`|`unused`| *n/a*| `RW`| `0x0`| `0x0  // Field: [21]`|
|`[21]`|`pmintenable`| PM Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[20]`|`fmintenable`| FM Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[19]`|`seuintenble`| SEU Interrupt Alarm| `RW`| `0x0`| `0x0`|
|`[18]`|`ethintenable`| ETH Port Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[17:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`pwintenable`| PW Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`de1grp1intenable`| PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[12]`|`de3grp1intenable`| PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[11:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`de1grp0intenable`| PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[8]`|`de3grp0intenable`| PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[7:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`ocnvtintenable`| OCN VT Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[1]`|`ocnstsintenable`| OCN STS Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`ocnlineintenable`| OCN Line Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Chip Interrupt Enable Control

* **Description**           

This register configure interrupt enable.


* **RTL Instant Name**    : `inten_ctrl`

* **Address**             : `0x00_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29]`|`cdr2intenable`| CDR Group1 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[28]`|`cdr1intenable`| CDR Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[27]`|`sysmontempenable`| SysMon temperature Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[26]`|`parintenable`| RAM Parity Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[25]`|`mdlintenable`| MDL DE3 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[24]`|`prmintenable`| PRM DE1 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[23:22]`|`unused`| *n/a*| `RW`| `0x0`| `0x0  // Field: [21]`|
|`[21]`|`pmintenable`| PM Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[20]`|`fmintenable`| FM Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[19]`|`seuintenble`| SEU Interrupt Alarm| `RW`| `0x0`| `0x0`|
|`[18]`|`ethintenable`| ETH Port Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[17:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`pwintenable`| PW Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`de1grp1intenable`| PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[12]`|`de3grp1intenable`| PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[11:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`de1grp0intenable`| PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[8]`|`de3grp0intenable`| PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[7:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`ocnvtintenable`| OCN VT Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[1]`|`ocnstsintenable`| OCN STS Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`ocnlineintenable`| OCN Line Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Chip Interrupt Restore Control

* **Description**           

This register configure interrupt restore enable.


* **RTL Instant Name**    : `inten_restore`

* **Address**             : `0x00_0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29]`|`cdr2intenable`| CDR Group1 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[28]`|`cdr1intenable`| CDR Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[27]`|`sysmontempenable`| SysMon temperature Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[26]`|`parintenable`| RAM Parity Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[25]`|`mdlintenable`| MDL DE3 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[24]`|`prmintenable`| PRM DE1 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[23:22]`|`unused`| *n/a*| `RW`| `0x0`| `0x0  // Field: [21]`|
|`[21]`|`pmintenable`| PM Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[20]`|`fmintenable`| FM Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`ethintenable`| ETH Port Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[17:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`pwintenable`| PW Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`de1grp1intenable`| PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[12]`|`de3grp1intenable`| PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[11:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`de1grp0intenable`| PDH DE1 Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[8]`|`de3grp0intenable`| PDH DE3 Group0 Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[7:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`ocnvtintenable`| OCN VT Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[1]`|`ocnstsintenable`| OCN STS Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`ocnlineintenable`| OCN Line Interrupt Alarm  1: Enable  0: Disable| `RW`| `0x0`| `0x0 End: Begin:`|

###EPort Interrupt Enable Control

* **Description**           

This register configure interrupt enable.


* **RTL Instant Name**    : `inten_Eport`

* **Address**             : `0x00_00C0`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[19:19]`|`xfi_3`| XFI_3    Interrupt Enable  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[18:18]`|`xfi_2`| XFI_2    Interrupt Enable  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[17:17]`|`xfi_1`| XFI_1    Interrupt Enable  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[16:16]`|`xfi_0`| XFI_0    Interrupt Enable  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[15:13]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[12:12]`|`qsgmii_3`| QSGMII_3 Interrupt Enable  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[11:09]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[08:08]`|`qsgmii_2`| QSGMII_2 Interrupt Enable  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[07:05]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[04:04]`|`qsgmii_1`| QSGMII_1 Interrupt Enable  1: Enable  0: Disable| `RW`| `0x0`| `0x0`|
|`[03:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`qsgmii_0`| QSGMII_0 Interrupt Enable  1: Enable  0: Disable| `RW`| `0x0`| `0x0 End: Begin:`|

###EPort Link Event

* **Description**           

This register report state change of link status.


* **RTL Instant Name**    : `linkalm_Eport`

* **Address**             : `0x00_00C4`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `WC`| `0x0`| `0x0`|
|`[19:19]`|`xfi_3`| XFI_3    Link State change Event  1: Changed  0: Normal| `WC`| `0x0`| `0x0`|
|`[18:18]`|`xfi_2`| XFI_2    Link State change Event  1: Changed  0: Normal| `WC`| `0x0`| `0x0`|
|`[17:17]`|`xfi_1`| XFI_1    Link State change Event  1: Changed  0: Normal| `WC`| `0x0`| `0x0`|
|`[16:16]`|`xfi_0`| XFI_0    Link State change Event  1: Changed  0: Normal| `WC`| `0x0`| `0x0`|
|`[15:13]`|`unused`| *n/a*| `WC`| `0x0`| `0x0`|
|`[12:12]`|`qsgmii_3`| QSGMII_3 Link State change Event  1: Changed  0: Normal| `WC`| `0x0`| `0x0`|
|`[11:09]`|`unused`| *n/a*| `WC`| `0x0`| `0x0`|
|`[08:08]`|`qsgmii_2`| QSGMII_2 Link State change Event  1: Changed  0: Normal| `WC`| `0x0`| `0x0`|
|`[07:05]`|`unused`| *n/a*| `WC`| `0x0`| `0x0`|
|`[04:04]`|`qsgmii_1`| QSGMII_1 Link State change Event  1: Changed  0: Normal| `WC`| `0x0`| `0x0`|
|`[03:01]`|`unused`| *n/a*| `WC`| `0x0`| `0x0`|
|`[00:00]`|`qsgmii_0`| QSGMII_0 Link State change Event  1: Changed  0: Normal| `WC`| `0x0`| `0x0 End: Begin:`|

###EPort Link Status

* **Description**           

This register configure interrupt enable.


* **RTL Instant Name**    : `linksta_Eport`

* **Address**             : `0x00_00C8`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[19:19]`|`xfi_3`| XFI_3 Link Status  1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[18:18]`|`xfi_2`| XFI_2 Link Status  1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[17:17]`|`xfi_1`| XFI_1 Link Status  1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[16:16]`|`xfi_0`| XFI_0 Link Status  1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[15:13]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[12:12]`|`qsgmii_3`| QSGMII_3 Link Status 1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[11:09]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[08:08]`|`qsgmii_2`| QSGMII_2 Link Status 1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[07:05]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[04:04]`|`qsgmii_1`| QSGMII_1 Link Status 1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[03:01]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[00:00]`|`qsgmii_0`| QSGMII_0 Link Status 1: Up  0: Down| `RO`| `0x0`| `0x0 End: Begin:`|

###EPort Link Force

* **Description**           

This register configure interrupt enable.


* **RTL Instant Name**    : `linkfrc_Eport`

* **Address**             : `0x00_00CC`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[19:19]`|`xfi_3`| XFI_3 Link Force  1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[18:18]`|`xfi_2`| XFI_2 Link Force  1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[17:17]`|`xfi_1`| XFI_1 Link Force  1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[16:16]`|`xfi_0`| XFI_0 Link Force  1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[15:13]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[12:12]`|`qsgmii_3`| QSGMII_3 Link Force 1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[11:09]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[08:08]`|`qsgmii_2`| QSGMII_2 Link Force 1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[07:05]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[04:04]`|`qsgmii_1`| QSGMII_1 Link Force 1: Up  0: Down| `RO`| `0x0`| `0x0`|
|`[03:01]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[00:00]`|`qsgmii_0`| QSGMII_0 Link Force 1: Up  0: Down| `RO`| `0x0`| `0x0 End: Begin:`|

###Write Hold Data63_32

* **Description**           

This register is used to write dword #2 of data.


* **RTL Instant Name**    : `wr_hold_63_32`

* **Address**             : `0x00_0011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`wrhold63_32`| The write dword #2| `RW`| `0x0`| `0x0 End: Begin:`|

###Write Hold Data95_64

* **Description**           

This register is used to write dword #3 of data.


* **RTL Instant Name**    : `wr_hold_95_64`

* **Address**             : `0x00_0012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`wrhold95_64`| The write dword #3| `RW`| `0x0`| `0x0 End: Begin:`|

###Write Hold Data127_96

* **Description**           

This register is used to write dword #4 of data.


* **RTL Instant Name**    : `wr_hold_127_96`

* **Address**             : `0x00_0013`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`wrhold127_96`| The write dword #4| `RW`| `0x0`| `0x0 End: Begin:`|

###Read Hold Data63_32

* **Description**           

This register is used to Read dword #2 of data.


* **RTL Instant Name**    : `rd_hold_63_32`

* **Address**             : `0x00_0021`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rdhold63_32`| The Read dword #2| `RW`| `0x0`| `0x0 End: Begin:`|

###Read Hold Data95_64

* **Description**           

This register is used to Read dword #3 of data.


* **RTL Instant Name**    : `rd_hold_95_64`

* **Address**             : `0x00_0022`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rdhold95_64`| The Read dword #3| `RW`| `0x0`| `0x0 End: Begin:`|

###Read Hold Data127_96

* **Description**           

This register is used to Read dword #4 of data.


* **RTL Instant Name**    : `rd_hold_127_96`

* **Address**             : `0x00_0023`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rdhold127_96`| The Read dword #4| `RW`| `0x0`| `0x0 End: Begin:`|

###Mac Address Bit31_0 Control

* **Description**           

This register configures bit 31 to 0 of MAC address.


* **RTL Instant Name**    : `mac_add_31_0_ctrl`

* **Address**             : `0x00_0100`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`macaddress31_0`| Bit 31 to 0 of MAC Address. Bit 31 is MSB bit| `RW`| `0x0`| `0x0 End: Begin:`|

###Mac Address Bit47_32 Control

* **Description**           

This register configures bit 47to 32 of MAC address.


* **RTL Instant Name**    : `mac_add_47_32_ctrl`

* **Address**             : `0x00_0101`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`macaddress47_32`| Bit 47 to 32 of MAC Address. Bit 47 is MSB bit| `RW`| `0x0`| `0x0`|
|`[26]`|`gemode`| GE mode 0:XGMII; 1:GMII| `RW`| `0x0`| `0x0`|
|`[27]`|`pdhloopout`| PDH parallel loopback| `RW`| `0x0`| `0x0`|
|`[28]`|`pdhglbmode`| PDH global mode for loopback, 0:DS3; 1:E3| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Address Bit31_0 Control

* **Description**           

This register configures bit 31 to 0 of IP address.


* **RTL Instant Name**    : `ip_add_31_0_ctrl`

* **Address**             : `0x00_0102`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`ipaddress31_0`| Bit 31 to 0 of IP Address. Bit 31 is MSB bit| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Address Bit63_32 Control

* **Description**           

This register configures bit 63to 32 of IP address.


* **RTL Instant Name**    : `ip_add_63_32_ctrl`

* **Address**             : `0x00_0103`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`ipaddress63_32`| Bit 63 to 32 of IP Address. Bit 63 is MSB bit| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Address Bit95_64 Control

* **Description**           

This register configures bit 95 to 64 of IP address.


* **RTL Instant Name**    : `ip_add_95_64_ctrl`

* **Address**             : `0x00_0104`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`ipaddress95_64`| Bit 95 to 64 of IP Address. Bit 95 is MSB bit| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Address Bit127_96 Control

* **Description**           

This register configures bit 127 to 96 of IP address.


* **RTL Instant Name**    : `ip_add_127_96_ctrl`

* **Address**             : `0x00_0105`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`ipaddress127_96`| Bit 127 to 96 of IP Address. Bit 127 is MSB bit| `RW`| `0x0`| `0x0 End:`|

###CDR REF OUT 8KHZ Control

* **Description**           

This register is used to get the status of RX NCO engine


* **RTL Instant Name**    : `cdr_refout_8khz_ctrl`

* **Address**             : `0x00_010c`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`cdr1refmasklosdis`| : 0: enable mask out8k in case of LOS 1: disable mask out8k in case of LOS| `RW`| `0x0`| `0x0`|
|`[30:20]`|`cdr1refid`| CDR engine ID (in CDR mode) or Rx LIU ID (in loop mode, bit[10:5]) that used for 8Khz ref out| `RW`| `0x0`| `0x0`|
|`[19:18]`|`cdr1refds1`| 0: Line ID used for 8KHZ ref out is E3 mode 1: Line ID used for 8KHZ ref out is DS3 mode 2: Line ID used for 8KHZ ref out is EC1 mode| `RW`| `0x0`| `0x0`|
|`[17:16]`|`cdr1refselect`| : 0: System mode, used system clock to generate 8Khz ref out 1: Loop timing mode,  used rx LIU to generate 8Khz ref out 2: CDR timing mode,  used clock from CDR engine to generate 8KHZ out. CDR clock can be ACR/DCR/Loop/System/Ext1/...| `RW`| `0x0`| `0x0`|
|`[15]`|`cdr0refmasklosdis`| : 0: enable mask out8k in case of LOS 1: disable mask out8k in case of LOS| `RW`| `0x0`| `0x0`|
|`[14:4]`|`cdr0refid`| CDR engine ID (in CDR mode) or Rx LIU ID (in loop mode, bit[10:5]) that used for 8Khz ref out| `RW`| `0x0`| `0x0`|
|`[3:2]`|`cdr0refds1`| : 0: Line ID used for 8KHZ ref out is E3 mode 1: Line ID used for 8KHZ ref out is DS3 mode 2: Line ID used for 8KHZ ref out is EC1 mode| `RW`| `0x0`| `0x0`|
|`[1:0]`|`cdr0refselect`| 0: System mode, used system clock to generate 8Khz ref out 1: Loop timing mode,  used rx LIU  to generate 8Khz ref out 2: CDR timing mode,  used clock from CDR engine to generate 8KHZ out. CDR clock can be ACR/DCR/Loop/System/Ext1/...| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_Parity_Force_Control`

* **Address**             : `0x130`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `96`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[86]`|`ddrtxeccnoncorerr_errfrc`| Force ECC non-correctable error for Tx DDR| `RW`| `0x0`| `0x0`|
|`[85]`|`ddrtxecccorerr_errfrc`| Force ECC correctable error for Tx DDR| `RW`| `0x0`| `0x0`|
|`[84]`|`ddrrxeccnoncorerr_errfrc`| Force ECC non-correctable error for Rx DDR| `RW`| `0x0`| `0x0`|
|`[83]`|`ddrrxecccorerr_errfrc`| Force ECC correctable error for Rx DDR| `RW`| `0x0`| `0x0`|
|`[82]`|`clasoftwavectl_parerrfrc`| Force parity For RAM Control "Softwave Control"| `RW`| `0x0`| `0x0`|
|`[81]`|`cdrpwelookupctl_parerrfrc`| Force parity For RAM Control "CDR Pseudowire Look Up Control"| `RW`| `0x0`| `0x0`|
|`[80]`|`clapergrpenbctl_parerrfrc`| Force parity For RAM Control "Classify Per Group Enable Control"| `RW`| `0x0`| `0x0`|
|`[79]`|`clamefovermplsctl_parerrfrc`| Force parity For RAM Control "Classify Pseudowire MEF over MPLS Control"| `RW`| `0x0`| `0x0`|
|`[78]`|`claperpwetypectl_parerrfrc`| Force parity For RAM Control "Classify Per Pseudowire Type Control"| `RW`| `0x0`| `0x0`|
|`[77]`|`clahbcelookupinfctl_parerrfrc`| Force parity For RAM Control "Classify HBCE Looking Up Information Control"| `RW`| `0x0`| `0x0`|
|`[76]`|`clahbcehashtab1ctl_parerrfrc`| Force parity For RAM Control "Classify HBCE Hashing Table Control" PageID 1| `RW`| `0x0`| `0x0`|
|`[75]`|`clahbcehashtab0ctl_parerrfrc`| Force parity For RAM Control "Classify HBCE Hashing Table Control" PageID 0| `RW`| `0x0`| `0x0`|
|`[74]`|`pwetxhspwgrpproctl_parerrfrc`| Force parity For RAM Control "Pseudowire Transmit HSPW Group Protection Control"| `RW`| `0x0`| `0x0`|
|`[73]`|`pwetxupsagrpenbctl_parerrfrc`| Force parity For RAM Control "Pseudowire Transmit UPSR Group Enable Control"| `RW`| `0x0`| `0x0`|
|`[72]`|`pwetxupsahspwmodctl_parerrfrc`| Force parity For RAM Control "Pseudowire Transmit UPSR and HSPW mode Control"| `RW`| `0x0`| `0x0`|
|`[71]`|`pwetxhspwlabelctl_parerrfrc`| Force parity For RAM Control "Pseudowire Transmit HSPW Label Control"| `RW`| `0x0`| `0x0`|
|`[70]`|`pwetxhdrrtpssrcctl_parerrfrc`| Force parity For RAM Control "Pseudowire Transmit Header RTP SSRC Value Control "| `RW`| `0x0`| `0x0`|
|`[69]`|`pwetxethhdrlenctl_parerrfrc`| Force parity For RAM Control "Pseudowire Transmit Ethernet Header Length Control"| `RW`| `0x0`| `0x0`|
|`[68]`|`pwetxethhdrvalctl_parerrfrc`| Force parity For RAM Control "Pseudowire Transmit Ethernet Header Value Control"| `RW`| `0x0`| `0x0`|
|`[67]`|`pwetxenbctl_parerrfrc`| Force parity For RAM Control "Pseudowire Transmit Enable Control"| `RW`| `0x0`| `0x0`|
|`[66]`|`pdaddrcrc_errfrc`| Force check DDR CRC error| `RW`| `0x0`| `0x0`|
|`[65]`|`pdatdmmode_parerrfrc`| Force parity For RAM Control "Pseudowire PDA TDM Mode Control"| `RW`| `0x0`| `0x0`|
|`[64]`|`pdajitbuf_parerrfrc`| Force parity For RAM Control "Pseudowire PDA Jitter Buffer Control"| `RW`| `0x0`| `0x0`|
|`[63]`|`pdareor_parerrfrc`| Force parity For RAM Control "Pseudowire PDA Reorder Control"| `RW`| `0x0`| `0x0`|
|`[62]`|`plapaylsizeslc1_parerrfrc`| Force parity For RAM Control "Thalassa PDHPW Payload Assemble Payload Size Control"SliceId 1| `RW`| `0x0`| `0x0`|
|`[61]`|`plapaylsizeslc0_parerrfrc`| Force parity For RAM Control "Thalassa PDHPW Payload Assemble Payload Size Control"SliceId 0| `RW`| `0x0`| `0x0`|
|`[60]`|`cdracrtimingctrl_parerrfrc_slice1`| Force parity For RAM Control "CDR ACR Engine Timing control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[59]`|`cdrvttimingctrl_parerrfrc_slice1`| Force parity For RAM Control "CDR VT Timing control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[58]`|`cdrststimingctrl_parerrfrc_slice1`| Force parity For RAM Control "CDR STS Timing control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[57]`|`cdracrtimingctrl_parerrfrc_slice0`| Force parity For RAM Control "CDR ACR Engine Timing control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[56]`|`cdrvttimingctrl_parerrfrc_slice0`| Force parity For RAM Control "CDR VT Timing control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[55]`|`cdrststimingctrl_parerrfrc_slice0`| Force parity For RAM Control "CDR STS Timing control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[54]`|`demapchlctrl_parerrfrc_slice1`| Force parity For RAM Control "DEMAP Thalassa Demap Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[53]`|`demapchlctrl_parerrfrc_slice0`| Force parity For RAM Control "DEMAP Thalassa Demap Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[52]`|`maplinectrl_parerrfrc_slice1`| Force parity For RAM Control "MAP Thalassa Map Line Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[51]`|`mapchlctrl_parerrfrc_slice1`| Force parity For RAM Control "MAP Thalassa Map Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[50]`|`maplinectrl_parerrfrc_slice0`| Force parity For RAM Control "MAP Thalassa Map Line Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[49]`|`mapchlctrl_parerrfrc_slice0`| Force parity For RAM Control "MAP Thalassa Map Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[48]`|`pdhvtasyncmapctrl_parerrfrc_slice1`| Force parity For RAM Control "PDH VT Async Map Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[47]`|`pdhstsvtmapctrl_parerrfrc_slice1`| Force parity For RAM Control "PDH STS/VT Map Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[46]`|`pdhtxm23e23trace_parerrfrc_slice1`| Force parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte" SliceId 1| `RW`| `0x0`| `0x0`|
|`[45]`|`pdhtxm23e23ctrl_parerrfrc_slice1`| Force parity For RAM Control "PDH TxM23E23 Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[44]`|`pdhtxm12e12ctrl_parerrfrc_slice1`| Force parity For RAM Control "PDH TxM12E12 Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[43]`|`pdhtxde1sigctrl_parerrfrc_slice1`| Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[42]`|`pdhtxde1frmctrl_parerrfrc_slice1`| Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[41]`|`pdhrxde1frmctrl_parerrfrc_slice1`| Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[40]`|`pdhrxm12e12ctrl_parerrfrc_slice1`| Force parity For RAM Control "PDH RxM12E12 Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[39]`|`pdhrxds3e3ohctrl_parerrfrc_slice1`| Force parity For RAM Control "PDH RxDS3E3 OH Pro Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[38]`|`pdhrxm23e23ctrl_parerrfrc_slice1`| Force parity For RAM Control "PDH RxM23E23 Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[37]`|`pdhstsvtdemapctrl_parerrfrc_slice1`| Force parity For RAM Control "PDH STS/VT Demap Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[36]`|`pdhmuxctrl_parerrfrc_slice1`| Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[35]`|`pdhvtasyncmapctrl_parerrfrc_slice0`| Force parity For RAM Control "PDH VT Async Map Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[34]`|`pdhstsvtmapctrl_parerrfrc_slice0`| Force parity For RAM Control "PDH STS/VT Map Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[33]`|`pdhtxm23e23trace_parerrfrc_slice0`| Force parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte" SliceId 0| `RW`| `0x0`| `0x0`|
|`[32]`|`pdhtxm23e23ctrl_parerrfrc_slice0`| Force parity For RAM Control "PDH TxM23E23 Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[31]`|`pdhtxm12e12ctrl_parerrfrc_slice0`| Force parity For RAM Control "PDH TxM12E12 Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[30]`|`pdhtxde1sigctrl_parerrfrc_slice0`| Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[29]`|`pdhtxde1frmctrl_parerrfrc_slice0`| Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[28]`|`pdhrxde1frmctrl_parerrfrc_slice0`| Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[27]`|`pdhrxm12e12ctrl_parerrfrc_slice0`| Force parity For RAM Control "PDH RxM12E12 Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[26]`|`pdhrxds3e3ohctrl_parerrfrc_slice0`| Force parity For RAM Control "PDH RxDS3E3 OH Pro Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[25]`|`pdhrxm23e23ctrl_parerrfrc_slice0`| Force parity For RAM Control "PDH RxM23E23 Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[24]`|`pdhstsvtdemapctrl_parerrfrc_slice0`| Force parity For RAM Control "PDH STS/VT Demap Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[23]`|`pdhmuxctrl_parerrfrc_slice0`| Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[22]`|`pohwrctrl_parerrfrc`| Force parity For RAM Control "POH BER Control VT/DSN", "POH BER Control STS/TU3"| `RW`| `0x0`| `0x0`|
|`[21]`|`pohwrtrsh_parerrfrc`| Force parity For RAM Control "POH BER Threshold 2"| `RW`| `0x0`| `0x0`|
|`[20]`|`pohterctrllo_parerrfrc`| Force parity For RAM Control "POH Termintate Insert Control VT/TU3"| `RW`| `0x0`| `0x0`|
|`[19]`|`pohterctrlhi_parerrfrc`| Force parity For RAM Control "POH Termintate Insert Control STS"| `RW`| `0x0`| `0x0`|
|`[18]`|`pohcpevtctr_parerrfrc`| Force parity For RAM Control "POH CPE VT Control Register"| `RW`| `0x0`| `0x0`|
|`[17]`|`pohcpestsctr_parerrfrc`| Force parity For RAM Control "POH CPE STS/TU3 Control Register"| `RW`| `0x0`| `0x0`|
|`[16]`|`ocntohctlslc1_parerrfrc`| Force parity For RAM Control "OCN TOH Monitoring Per Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[15]`|`ocnspictlslc1_parerrfrc`| Force parity For RAM Control "OCN STS Pointer Interpreter Per Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[14]`|`ocnspictlslc0_parerrfrc`| Force parity For RAM Control "OCN STS Pointer Interpreter Per Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[13]`|`ocnvpidemslc1_parerrfrc`| Force parity For RAM Control "OCN RXPP Per STS payload Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[12]`|`ocnvpidemslc0_parerrfrc`| Force parity For RAM Control "OCN RXPP Per STS payload Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[11]`|`ocnvpictlslc1_parerrfrc`| Force parity For RAM Control "OCN VTTU Pointer Interpreter Per Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[10]`|`ocnvpictlslc0_parerrfrc`| Force parity For RAM Control "OCN VTTU Pointer Interpreter Per Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[9]`|`ocnvpgdemslc1_parerrfrc`| Force parity For RAM Control "OCN TXPP Per STS Multiplexing Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[8]`|`ocnvpgdemslc0_parerrfrc`| Force parity For RAM Control "OCN TXPP Per STS Multiplexing Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[7]`|`ocnvpgctlslc1_parerrfrc`| Force parity For RAM Control "OCN VTTU Pointer Generator Per Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[6]`|`ocnvpgctlslc0_parerrfrc`| Force parity For RAM Control "OCN VTTU Pointer Generator Per Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[5]`|`ocnspgctlslc1_parerrfrc`| Force parity For RAM Control "OCN STS Pointer Generator Per Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[4]`|`ocnspgctlslc0_parerrfrc`| Force parity For RAM Control "OCN STS Pointer Generator Per Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[3]`|`ocntfmctlslc1_parerrfrc`| Force parity For RAM Control "OCN Tx Framer Per Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[2]`|`ocntfmctlslc0_parerrfrc`| Force parity For RAM Control "OCN Tx Framer Per Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[1]`|`ocntfmj0slc1_parerrfrc`| Force parity For RAM Control "OCN Tx J0 Insertion Buffer" SliceId 1| `RW`| `0x0`| `0x0`|
|`[0]`|`ocntfmj0slc0_parerrfrc`| Force parity For RAM Control "OCN Tx J0 Insertion Buffer" SliceId 0| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM parity Disable Control

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_Parity_Disable_Control`

* **Address**             : `0x131`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `96`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[86]`|`ddrtxeccnoncorerr_errdis`| Disable check ECC non-correctable error for Tx DDR| `RW`| `0x0`| `0x0`|
|`[85]`|`ddrtxecccorerr_errdis`| Disable check ECC correctable error for Tx DDR| `RW`| `0x0`| `0x0`|
|`[84]`|`ddrrxeccnoncorerr_errdis`| Disable check ECC non-correctable error for Rx DDR| `RW`| `0x0`| `0x0`|
|`[83]`|`ddrrxecccorerr_errdis`| Disable check ECC correctable error for Rx DDR| `RW`| `0x0`| `0x0`|
|`[82]`|`clasoftwavectl_pardis`| Disable parity For RAM Control "Softwave Control"| `RW`| `0x0`| `0x0`|
|`[81]`|`cdrpwelookupctl_pardis`| Disable parity For RAM Control "CDR Pseudowire Look Up Control"| `RW`| `0x0`| `0x0`|
|`[80]`|`clapergrpenbctl_pardis`| Disable parity For RAM Control "Classify Per Group Enable Control"| `RW`| `0x0`| `0x0`|
|`[79]`|`clamefovermplsctl_pardis`| Disable parity For RAM Control "Classify Pseudowire MEF over MPLS Control"| `RW`| `0x0`| `0x0`|
|`[78]`|`claperpwetypectl_pardis`| Disable parity For RAM Control "Classify Per Pseudowire Type Control"| `RW`| `0x0`| `0x0`|
|`[77]`|`clahbcelookupinfctl_pardis`| Disable parity For RAM Control "Classify HBCE Looking Up Information Control"| `RW`| `0x0`| `0x0`|
|`[76]`|`clahbcehashtab1ctl_pardis`| Disable parity For RAM Control "Classify HBCE Hashing Table Control" PageID 1| `RW`| `0x0`| `0x0`|
|`[75]`|`clahbcehashtab0ctl_pardis`| Disable parity For RAM Control "Classify HBCE Hashing Table Control" PageID 0| `RW`| `0x0`| `0x0`|
|`[74]`|`pwetxhspwgrpproctl_pardis`| Disable parity For RAM Control "Pseudowire Transmit HSPW Group Protection Control"| `RW`| `0x0`| `0x0`|
|`[73]`|`pwetxupsagrpenbctl_pardis`| Disable parity For RAM Control "Pseudowire Transmit UPSR Group Enable Control"| `RW`| `0x0`| `0x0`|
|`[72]`|`pwetxupsahspwmodctl_pardis`| Disable parity For RAM Control "Pseudowire Transmit UPSR and HSPW mode Control"| `RW`| `0x0`| `0x0`|
|`[71]`|`pwetxhspwlabelctl_pardis`| Disable parity For RAM Control "Pseudowire Transmit HSPW Label Control"| `RW`| `0x0`| `0x0`|
|`[70]`|`pwetxhdrrtpssrcctl_pardis`| Disable parity For RAM Control "Pseudowire Transmit Header RTP SSRC Value Control "| `RW`| `0x0`| `0x0`|
|`[69]`|`pwetxethhdrlenctl_pardis`| Disable parity For RAM Control "Pseudowire Transmit Ethernet Header Length Control"| `RW`| `0x0`| `0x0`|
|`[68]`|`pwetxethhdrvalctl_pardis`| Disable parity For RAM Control "Pseudowire Transmit Ethernet Header Value Control"| `RW`| `0x0`| `0x0`|
|`[67]`|`pwetxenbctl_pardis`| Disable parity For RAM Control "Pseudowire Transmit Enable Control"| `RW`| `0x0`| `0x0`|
|`[66]`|`pdaddrcrc_errdis`| Disable check DDR CRC error| `RW`| `0x0`| `0x0`|
|`[65]`|`pdatdmmode_pardis`| Disable parity For RAM Control "Pseudowire PDA TDM Mode Control"| `RW`| `0x0`| `0x0`|
|`[64]`|`pdajitbuf_pardis`| Disable parity For RAM Control "Pseudowire PDA Jitter Buffer Control"| `RW`| `0x0`| `0x0`|
|`[63]`|`pdareor_pardis`| Disable parity For RAM Control "Pseudowire PDA Reorder Control"| `RW`| `0x0`| `0x0`|
|`[62]`|`plapaylsizeslc1_pardis`| Disable parity For RAM Control "Thalassa PDHPW Payload Assemble Payload Size Control"SliceId 1| `RW`| `0x0`| `0x0`|
|`[61]`|`plapaylsizeslc0_pardis`| Disable parity For RAM Control "Thalassa PDHPW Payload Assemble Payload Size Control"SliceId 0| `RW`| `0x0`| `0x0`|
|`[60]`|`cdracrtimingctrlslc1_pardis`| Disable parity For RAM Control "CDR ACR Engine Timing control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[59]`|`cdrvttimingctrlslc1_pardis`| Disable parity For RAM Control "CDR VT Timing control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[58]`|`cdrststimingctrlslc1_pardis`| Disable parity For RAM Control "CDR STS Timing control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[57]`|`cdracrtimingctrlslc0_pardis`| Disable parity For RAM Control "CDR ACR Engine Timing control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[56]`|`cdrvttimingctrlslc0_pardis`| Disable parity For RAM Control "CDR VT Timing control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[55]`|`cdrststimingctrlslc0_pardis`| Disable parity For RAM Control "CDR STS Timing control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[54]`|`demapchlctrlslc1_pardis`| Disable parity For RAM Control "DEMAP Thalassa Demap Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[53]`|`demapchlctrlslc0_pardis`| Disable parity For RAM Control "DEMAP Thalassa Demap Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[52]`|`maplinectrlslc1_pardis`| Disable parity For RAM Control "MAP Thalassa Map Line Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[51]`|`mapchlctrlslc1_pardis`| Disable parity For RAM Control "MAP Thalassa Map Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[50]`|`maplinectrlslc0_pardis`| Disable parity For RAM Control "MAP Thalassa Map Line Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[49]`|`mapchlctrlslc0_pardis`| Disable parity For RAM Control "MAP Thalassa Map Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[48]`|`pdhvtasyncmapctrlslc1_pardis`| Disable parity For RAM Control "PDH VT Async Map Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[47]`|`pdhstsvtmapctrlslc1_pardis`| Disable parity For RAM Control "PDH STS/VT Map Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[46]`|`pdhtxm23e23traceslc1_pardis`| Disable parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte" SliceId 1| `RW`| `0x0`| `0x0`|
|`[45]`|`pdhtxm23e23ctrlslc1_pardis`| Disable parity For RAM Control "PDH TxM23E23 Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[44]`|`pdhtxm12e12ctrlslc1_pardis`| Disable parity For RAM Control "PDH TxM12E12 Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[43]`|`pdhtxde1sigctrlslc1_pardis`| Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[42]`|`pdhtxde1frmctrlslc1_pardis`| Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[41]`|`pdhrxde1frmctrlslc1_pardis`| Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[40]`|`pdhrxm12e12ctrlslc1_pardis`| Disable parity For RAM Control "PDH RxM12E12 Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[39]`|`pdhrxds3e3ohctrlslc1_pardis`| Disable parity For RAM Control "PDH RxDS3E3 OH Pro Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[38]`|`pdhrxm23e23ctrlslc1_pardis`| Disable parity For RAM Control "PDH RxM23E23 Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[37]`|`pdhstsvtdemapctrlslc1_pardis`| Disable parity For RAM Control "PDH STS/VT Demap Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[36]`|`pdhmuxctrlslc1_pardis`| Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[35]`|`pdhvtasyncmapctrlslc0_pardis`| Disable parity For RAM Control "PDH VT Async Map Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[34]`|`pdhstsvtmapctrlslc0_pardis`| Disable parity For RAM Control "PDH STS/VT Map Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[33]`|`pdhtxm23e23traceslc0_pardis`| Disable parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte" SliceId 0| `RW`| `0x0`| `0x0`|
|`[32]`|`pdhtxm23e23ctrlslc0_pardis`| Disable parity For RAM Control "PDH TxM23E23 Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[31]`|`pdhtxm12e12ctrlslc0_pardis`| Disable parity For RAM Control "PDH TxM12E12 Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[30]`|`pdhtxde1sigctrlslc0_pardis`| Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[29]`|`pdhtxde1frmctrlslc0_pardis`| Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[28]`|`pdhrxde1frmctrlslc0_pardis`| Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[27]`|`pdhrxm12e12ctrlslc0_pardis`| Disable parity For RAM Control "PDH RxM12E12 Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[26]`|`pdhrxds3e3ohctrlslc0_pardis`| Disable parity For RAM Control "PDH RxDS3E3 OH Pro Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[25]`|`pdhrxm23e23ctrlslc0_pardis`| Disable parity For RAM Control "PDH RxM23E23 Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[24]`|`pdhstsvtdemapctrlslc0_pardis`| Disable parity For RAM Control "PDH STS/VT Demap Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[23]`|`pdhmuxctrl_pardis`| Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[22]`|`pohwrctrl_pardis`| Disable parity For RAM Control "POH BER Control VT/DSN", "POH BER Control STS/TU3"| `RW`| `0x0`| `0x0`|
|`[21]`|`pohwrtrsh_pardis`| Disable parity For RAM Control "POH BER Threshold 2"| `RW`| `0x0`| `0x0`|
|`[20]`|`pohterctrllo_pardis`| Disable parity For RAM Control "POH Termintate Insert Control VT/TU3"| `RW`| `0x0`| `0x0`|
|`[19]`|`pohterctrlhi_pardis`| Disable parity For RAM Control "POH Termintate Insert Control STS"| `RW`| `0x0`| `0x0`|
|`[18]`|`pohcpevtctr_pardis`| Disable parity For RAM Control "POH CPE VT Control Register"| `RW`| `0x0`| `0x0`|
|`[17]`|`pohcpestsctr_pardis`| Disable parity For RAM Control "POH CPE STS/TU3 Control Register"| `RW`| `0x0`| `0x0`|
|`[16]`|`ocntohctlslc1_pardis`| Disable parity For RAM Control "OCN TOH Monitoring Per Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[15]`|`ocnspictlslc1_pardis`| Disable parity For RAM Control "OCN STS Pointer Interpreter Per Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[14]`|`ocnspictlslc0_pardis`| Disable parity For RAM Control "OCN STS Pointer Interpreter Per Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[13]`|`ocnvpidemslc1_pardis`| Disable parity For RAM Control "OCN RXPP Per STS payload Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[12]`|`ocnvpidemslc0_pardis`| Disable parity For RAM Control "OCN RXPP Per STS payload Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[11]`|`ocnvpictlslc1_pardis`| Disable parity For RAM Control "OCN VTTU Pointer Interpreter Per Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[10]`|`ocnvpictlslc0_pardis`| Disable parity For RAM Control "OCN VTTU Pointer Interpreter Per Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[9]`|`ocnvpgdemslc1_pardis`| Disable parity For RAM Control "OCN TXPP Per STS Multiplexing Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[8]`|`ocnvpgdemslc0_pardis`| Disable parity For RAM Control "OCN TXPP Per STS Multiplexing Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[7]`|`ocnvpgctlslc1_pardis`| Disable parity For RAM Control "OCN VTTU Pointer Generator Per Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[6]`|`ocnvpgctlslc0_pardis`| Disable parity For RAM Control "OCN VTTU Pointer Generator Per Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[5]`|`ocnspgctlslc1_pardis`| Disable parity For RAM Control "OCN STS Pointer Generator Per Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[4]`|`ocnspgctlslc0_pardis`| Disable parity For RAM Control "OCN STS Pointer Generator Per Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[3]`|`ocntfmctlslc1_pardis`| Disable parity For RAM Control "OCN Tx Framer Per Channel Control" SliceId 1| `RW`| `0x0`| `0x0`|
|`[2]`|`ocntfmctlslc0_pardis`| Disable parity For RAM Control "OCN Tx Framer Per Channel Control" SliceId 0| `RW`| `0x0`| `0x0`|
|`[1]`|`ocntfmj0slc1_pardis`| Disable parity For RAM Control "OCN Tx J0 Insertion Buffer" SliceId 1| `RW`| `0x0`| `0x0`|
|`[0]`|`ocntfmj0slc0_pardis`| Disable parity For RAM Control "OCN Tx J0 Insertion Buffer" SliceId 0| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_Parity_Error_Sticky`

* **Address**             : `0x112`

* **Formula**             : `0x112 + Wrd`

* **Where**               : 

    * `$Wrd(0-2): RAM parity Error Word 0:wrd[31:0]; 1:wrd[63:32];2:wrd[95:64]`

* **Width**               : `96`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[86]`|`ddrtxeccnoncorerr_errstk`| Error Sticky ECC non-correctable error for Tx DDR| `W1C`| `0x0`| `0x0`|
|`[85]`|`ddrtxecccorerr_errstk`| Error Sticky ECC correctable error for Tx DDR| `W1C`| `0x0`| `0x0`|
|`[84]`|`ddrrxeccnoncorerr_errstk`| Error Sticky ECC non-correctable error for Rx DDR| `W1C`| `0x0`| `0x0`|
|`[83]`|`ddrrxecccorerr_errstk`| Error Sticky ECC correctable error for Rx DDR| `W1C`| `0x0`| `0x0`|
|`[82]`|`clasoftwavectl_parerrstk`| Parity Error Sticky For RAM Control "Softwave Control"| `W1C`| `0x0`| `0x0`|
|`[81]`|`cdrpwelookupctl_parerrstk`| Parity Error Sticky For RAM Control "CDR Pseudowire Look Up Control"| `W1C`| `0x0`| `0x0`|
|`[80]`|`clapergrpenbctl_parerrstk`| Parity Error Sticky For RAM Control "Classify Per Group Enable Control"| `W1C`| `0x0`| `0x0`|
|`[79]`|`clamefovermplsctl_parerrstk`| Parity Error Sticky For RAM Control "Classify Pseudowire MEF over MPLS Control"| `W1C`| `0x0`| `0x0`|
|`[78]`|`claperpwetypectl_parerrstk`| Parity Error Sticky For RAM Control "Classify Per Pseudowire Type Control"| `W1C`| `0x0`| `0x0`|
|`[77]`|`clahbcelookupinfctl_parerrstk`| Parity Error Sticky For RAM Control "Classify HBCE Looking Up Information Control"| `W1C`| `0x0`| `0x0`|
|`[76]`|`clahbcehashtab1ctl_parerrstk`| Parity Error Sticky For RAM Control "Classify HBCE Hashing Table Control" PageID 1| `W1C`| `0x0`| `0x0`|
|`[75]`|`clahbcehashtab0ctl_parerrstk`| Parity Error Sticky For RAM Control "Classify HBCE Hashing Table Control" PageID 0| `W1C`| `0x0`| `0x0`|
|`[74]`|`pwetxhspwgrpproctl_parerrstk`| Parity Error Sticky For RAM Control "Pseudowire Transmit HSPW Group Protection Control"| `W1C`| `0x0`| `0x0`|
|`[73]`|`pwetxupsagrpenbctl_parerrstk`| Parity Error Sticky For RAM Control "Pseudowire Transmit UPSR Group Enable Control"| `W1C`| `0x0`| `0x0`|
|`[72]`|`pwetxupsahspwmodctl_parerrstk`| Parity Error Sticky For RAM Control "Pseudowire Transmit UPSR and HSPW mode Control"| `W1C`| `0x0`| `0x0`|
|`[71]`|`pwetxhspwlabelctl_parerrstk`| Parity Error Sticky For RAM Control "Pseudowire Transmit HSPW Label Control"| `W1C`| `0x0`| `0x0`|
|`[70]`|`pwetxhdrrtpssrcctl_parerrstk`| Parity Error Sticky For RAM Control "Pseudowire Transmit Header RTP SSRC Value Control "| `W1C`| `0x0`| `0x0`|
|`[69]`|`pwetxethhdrlenctl_parerrstk`| Parity Error Sticky For RAM Control "Pseudowire Transmit Ethernet Header Length Control"| `W1C`| `0x0`| `0x0`|
|`[68]`|`pwetxethhdrvalctl_parerrstk`| Parity Error Sticky For RAM Control "Pseudowire Transmit Ethernet Header Value Control"| `W1C`| `0x0`| `0x0`|
|`[67]`|`pwetxenbctl_parerrstk`| Parity Error Sticky For RAM Control "Pseudowire Transmit Enable Control"| `W1C`| `0x0`| `0x0`|
|`[66]`|`pdaddrcrc_errstk`| Sticky check DDR CRC error| `W1C`| `0x0`| `0x0`|
|`[65]`|`pdatdmmode_parerrstk`| Parity Error Sticky For RAM Control "Pseudowire PDA TDM Mode Control"| `W1C`| `0x0`| `0x0`|
|`[64]`|`pdajitbuf_parerrstk`| Parity Error Sticky For RAM Control "Pseudowire PDA Jitter Buffer Control"| `W1C`| `0x0`| `0x0`|
|`[63]`|`pdareor_parerrstk`| Parity Error Sticky For RAM Control "Pseudowire PDA Reorder Control"| `W1C`| `0x0`| `0x0`|
|`[62]`|`plapaylsizeslc1_parerrstk`| Parity Error Sticky For RAM Control "Thalassa PDHPW Payload Assemble Payload Size Control"SliceId 1| `W1C`| `0x0`| `0x0`|
|`[61]`|`plapaylsizeslc0_parerrstk`| Parity Error Sticky For RAM Control "Thalassa PDHPW Payload Assemble Payload Size Control"SliceId 0| `W1C`| `0x0`| `0x0`|
|`[60]`|`cdracrtimingctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "CDR ACR Engine Timing control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[59]`|`cdrvttimingctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "CDR VT Timing control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[58]`|`cdrststimingctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "CDR STS Timing control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[57]`|`cdracrtimingctrlslc0_parerrstk`| Parity Error Sticky For RAM Control "CDR ACR Engine Timing control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[56]`|`cdrvttimingctrlslc0_parerrstk`| Parity Error Sticky For RAM Control "CDR VT Timing control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[55]`|`cdrststimingctrlslc0_parerrstk`| Parity Error Sticky For RAM Control "CDR STS Timing control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[54]`|`demapchlctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "DEMAP Thalassa Demap Channel Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[53]`|`demapchlctrlslc0_parerrstk`| Parity Error Sticky For RAM Control "DEMAP Thalassa Demap Channel Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[52]`|`maplinectrlslc1_parerrstk`| Parity Error Sticky For RAM Control "MAP Thalassa Map Line Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[51]`|`mapchlctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "MAP Thalassa Map Channel Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[50]`|`maplinectrlslc0_parerrstk`| Parity Error Sticky For RAM Control "MAP Thalassa Map Line Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[49]`|`mapchlctrlslc0_parerrstk`| Parity Error Sticky For RAM Control "MAP Thalassa Map Channel Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[48]`|`pdhvtasyncmapctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH VT Async Map Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[47]`|`pdhstsvtmapctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH STS/VT Map Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[46]`|`pdhtxm23e23traceslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH TxM23E23 E3g832 Trace Byte" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[45]`|`pdhtxm23e23ctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH TxM23E23 Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[44]`|`pdhtxm12e12ctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH TxM12E12 Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[43]`|`pdhtxde1sigctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[42]`|`pdhtxde1frmctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Tx Framer Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[41]`|`pdhrxde1frmctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Rx Framer Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[40]`|`pdhrxm12e12ctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH RxM12E12 Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[39]`|`pdhrxds3e3ohctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH RxDS3E3 OH Pro Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[38]`|`pdhrxm23e23ctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH RxM23E23 Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[37]`|`pdhstsvtdemapctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH STS/VT Demap Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[36]`|`pdhmuxctrlslc1_parerrstk`| Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Rx Framer Mux Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[35]`|`pdhvtasyncmapctrl_slc0parerrstk`| Parity Error Sticky For RAM Control "PDH VT Async Map Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[34]`|`pdhstsvtmapctrl_slc0parerrstk`| Parity Error Sticky For RAM Control "PDH STS/VT Map Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[33]`|`pdhtxm23e23trace_parerrstk`| Parity Error Sticky For RAM Control "PDH TxM23E23 E3g832 Trace Byte" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[32]`|`pdhtxm23e23ctrl_slc0parerrstk`| Parity Error Sticky For RAM Control "PDH TxM23E23 Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[31]`|`pdhtxm12e12ctrl_slc0parerrstk`| Parity Error Sticky For RAM Control "PDH TxM12E12 Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[30]`|`pdhtxde1sigctrl_slc0parerrstk`| Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[29]`|`pdhtxde1frmctrl_slc0parerrstk`| Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Tx Framer Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[28]`|`pdhrxde1frmctrl_slc0parerrstk`| Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Rx Framer Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[27]`|`pdhrxm12e12ctrl_slc0parerrstk`| Parity Error Sticky For RAM Control "PDH RxM12E12 Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[26]`|`pdhrxds3e3ohctrl_slc0parerrstk`| Parity Error Sticky For RAM Control "PDH RxDS3E3 OH Pro Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[25]`|`pdhrxm23e23ctrl_slc0parerrstk`| Parity Error Sticky For RAM Control "PDH RxM23E23 Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[24]`|`pdhstsvtdemapctrl_slc0parerrstk`| Parity Error Sticky For RAM Control "PDH STS/VT Demap Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[23]`|`pdhmuxctrl_slc0parerrstk`| Parity Error Sticky For RAM Control "PDH DS1/E1/J1 Rx Framer Mux Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[22]`|`pohwrctrl_parerrstk`| Parity Error Sticky For RAM Control "POH BER Control VT/DSN", "POH BER Control STS/TU3"| `W1C`| `0x0`| `0x0`|
|`[21]`|`pohwrtrsh_parerrstk`| Parity Error Sticky For RAM Control "POH BER Threshold 2"| `W1C`| `0x0`| `0x0`|
|`[20]`|`pohterctrllo_parerrstk`| Parity Error Sticky For RAM Control "POH Termintate Insert Control VT/TU3"| `W1C`| `0x0`| `0x0`|
|`[19]`|`pohterctrlhi_parerrstk`| Parity Error Sticky For RAM Control "POH Termintate Insert Control STS"| `W1C`| `0x0`| `0x0`|
|`[18]`|`pohcpevtctr_parerrstk`| Parity Error Sticky For RAM Control "POH CPE VT Control Register"| `W1C`| `0x0`| `0x0`|
|`[17]`|`pohcpestsctr_parerrstk`| Parity Error Sticky For RAM Control "POH CPE STS/TU3 Control Register"| `W1C`| `0x0`| `0x0`|
|`[16]`|`ocntohctlslc1_parerrstk`| Parity Error Sticky For RAM Control "OCN TOH Monitoring Per Channel Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[15]`|`ocnspictlslc1_parerrstk`| Parity Error Sticky For RAM Control "OCN STS Pointer Interpreter Per Channel Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[14]`|`ocnspictlslc0_parerrstk`| Parity Error Sticky For RAM Control "OCN STS Pointer Interpreter Per Channel Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[13]`|`ocnvpidemslc1_parerrstk`| Parity Error Sticky For RAM Control "OCN RXPP Per STS payload Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[12]`|`ocnvpidemslc0_parerrstk`| Parity Error Sticky For RAM Control "OCN RXPP Per STS payload Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[11]`|`ocnvpictlslc1_parerrstk`| Parity Error Sticky For RAM Control "OCN VTTU Pointer Interpreter Per Channel Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[10]`|`ocnvpictlslc0_parerrstk`| Parity Error Sticky For RAM Control "OCN VTTU Pointer Interpreter Per Channel Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[9]`|`ocnvpgdemslc1_parerrstk`| Parity Error Sticky For RAM Control "OCN TXPP Per STS Multiplexing Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[8]`|`ocnvpgdemslc0_parerrstk`| Parity Error Sticky For RAM Control "OCN TXPP Per STS Multiplexing Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[7]`|`ocnvpgctlslc1_parerrstk`| Parity Error Sticky For RAM Control "OCN VTTU Pointer Generator Per Channel Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[6]`|`ocnvpgctlslc0_parerrstk`| Parity Error Sticky For RAM Control "OCN VTTU Pointer Generator Per Channel Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[5]`|`ocnspgctlslc1_parerrstk`| Parity Error Sticky For RAM Control "OCN STS Pointer Generator Per Channel Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[4]`|`ocnspgctlslc0_parerrstk`| Parity Error Sticky For RAM Control "OCN STS Pointer Generator Per Channel Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[3]`|`ocntfmctlslc1_parerrstk`| Parity Error Sticky For RAM Control "OCN Tx Framer Per Channel Control" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[2]`|`ocntfmctlslc0_parerrstk`| Parity Error Sticky For RAM Control "OCN Tx Framer Per Channel Control" SliceId 0| `W1C`| `0x0`| `0x0`|
|`[1]`|`ocntfmj0slc1_parerrstk`| Parity Error Sticky For RAM Control "OCN Tx J0 Insertion Buffer" SliceId 1| `W1C`| `0x0`| `0x0`|
|`[0]`|`ocntfmj0slc0_parerrstk`| Parity Error Sticky For RAM Control "OCN Tx J0 Insertion Buffer" SliceId 0| `W1C`| `0x0`| `0x0 End: Begin:`|

###Read HA Hold Data127_96

* **Description**           

This register is used to read HA dword4 of data.


* **RTL Instant Name**    : `RAM_Parity_Error_Intr_Group`

* **Address**             : `0x00121`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2]`|`wrd2`| Word2| `RW`| `0`| `0`|
|`[1]`|`wrd1`| Word1| `RW`| `0`| `0`|
|`[0]`|`wrd0`| Word0| `RW`| `0`| `0 End: Begin:`|

###Read HA Address Bit3_0 Control

* **Description**           

This register is used to send HA read address bit3_0 to HA engine


* **RTL Instant Name**    : `rdha3_0_control`

* **Address**             : `0x00200`

* **Formula**             : `0x00200 + HaAddr3_0`

* **Where**               : 

    * `$HaAddr3_0(0-15): HA Address Bit3_0`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr3_0`| Read value will be 0x01000 plus HaAddr3_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit7_4 Control

* **Description**           

This register is used to send HA read address bit7_4 to HA engine


* **RTL Instant Name**    : `rdha7_4_control`

* **Address**             : `0x00210`

* **Formula**             : `0x00210 + HaAddr7_4`

* **Where**               : 

    * `$HaAddr7_4(0-15): HA Address Bit7_4`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr7_4`| Read value will be 0x01000 plus HaAddr7_4| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit11_8 Control

* **Description**           

This register is used to send HA read address bit11_8 to HA engine


* **RTL Instant Name**    : `rdha11_8_control`

* **Address**             : `0x00220`

* **Formula**             : `0x00220 + HaAddr11_8`

* **Where**               : 

    * `$HaAddr11_8(0-15): HA Address Bit11_8`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr11_8`| Read value will be 0x01000 plus HaAddr11_8| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit15_12 Control

* **Description**           

This register is used to send HA read address bit15_12 to HA engine


* **RTL Instant Name**    : `rdha15_12_control`

* **Address**             : `0x00230`

* **Formula**             : `0x00230 + HaAddr15_12`

* **Where**               : 

    * `$HaAddr15_12(0-15): HA Address Bit15_12`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr15_12`| Read value will be 0x01000 plus HaAddr15_12| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit19_16 Control

* **Description**           

This register is used to send HA read address bit19_16 to HA engine


* **RTL Instant Name**    : `rdha19_16_control`

* **Address**             : `0x00240`

* **Formula**             : `0x00240 + HaAddr19_16`

* **Where**               : 

    * `$HaAddr19_16(0-15): HA Address Bit19_16`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr19_16`| Read value will be 0x01000 plus HaAddr19_16| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit23_20 Control

* **Description**           

This register is used to send HA read address bit23_20 to HA engine


* **RTL Instant Name**    : `rdha23_20_control`

* **Address**             : `0x00250`

* **Formula**             : `0x00250 + HaAddr23_20`

* **Where**               : 

    * `$HaAddr23_20(0-15): HA Address Bit23_20`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr23_20`| Read value will be 0x01000 plus HaAddr23_20| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit24 and Data Control

* **Description**           

This register is used to send HA read address bit24 to HA engine to read data


* **RTL Instant Name**    : `rdha24data_control`

* **Address**             : `0x00260`

* **Formula**             : `0x00260 + HaAddr24`

* **Where**               : 

    * `$HaAddr24(0-1): HA Address Bit24`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata31_0`| HA read data bit31_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data63_32

* **Description**           

This register is used to read HA dword2 of data.


* **RTL Instant Name**    : `rdha_hold63_32`

* **Address**             : `0x00270`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata63_32`| HA read data bit63_32| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data95_64

* **Description**           

This register is used to read HA dword3 of data.


* **RTL Instant Name**    : `rdindr_hold95_64`

* **Address**             : `0x00271`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata95_64`| HA read data bit95_64| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data127_96

* **Description**           

This register is used to read HA dword4 of data.


* **RTL Instant Name**    : `rdindr_hold127_96`

* **Address**             : `0x00272`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata127_96`| HA read data bit127_96| `RW`| `0xx`| `0xx End: Begin:`|

###Temperate Event

* **Description**           

This register report state change of Temperate status.


* **RTL Instant Name**    : `alm_temp`

* **Address**             : `0x00_00D0`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00:00]`|`temp_alarm`| Temperate change Event  1: Changed  0: Normal| `WC`| `0x0`| `0x0 End: Begin:`|

###PDA DDR CRC Error Counter

* **Description**           

Count number of CRC error detected


* **RTL Instant Name**    : `PDA_DDR_CRC_Error_Counter_ro`

* **Address**             : `0x000142(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pdaddrcrcerror`| This counter count the number of DDR CRC error.| `RW`| `0x0`| `0x0 End: Begin:`|

###PDA DDR CRC Error Counter

* **Description**           

Count number of CRC error detected


* **RTL Instant Name**    : `PDA_DDR_CRC_Error_Counter_r2c`

* **Address**             : `0x000141(R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pdaddrcrcerror`| This counter count the number of DDR CRC error.| `RW`| `0x0`| `0x0 End: Begin:`|

###PDA DDR Force Error Control

* **Description**           




* **RTL Instant Name**    : `PDA_DDR_errins_en_cfg`

* **Address**             : `0x000140`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28]`|`pdaddrerrrate`| 0: One-shot with number of errors; 1:Line rate| `RW`| `0x0`| `0x0`|
|`[27:0]`|`pdaddrerrthr`| Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1| `RW`| `0x0`| `0x0 End: Begin:`|

###Rx DDR ECC Correctable Error Counter

* **Description**           

Count number of Rx DDR ECC Correctable error detected


* **RTL Instant Name**    : `Rx_DDR_ECC_Cor_Error_Counter_ro`

* **Address**             : `0x00014a(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxddrecccorerror`| This counter count the number of RxDDR ECC Correctable error.| `RW`| `0x0`| `0x0 End: Begin:`|

###Rx DDR ECC Correctable Error Counter

* **Description**           

Count number of Rx DDR ECC Correctable error detected


* **RTL Instant Name**    : `Rx_DDR_ECC_Cor_Error_Counter_r2c`

* **Address**             : `0x000149(R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxddrecccorerror`| This counter count the number of RxDDR ECC Correctable error.| `RW`| `0x0`| `0x0 End: Begin:`|

###Rx DDR ECC UnCorrectable Error Counter

* **Description**           

Count number of Rx DDR ECC UnCorrectable error detected


* **RTL Instant Name**    : `Rx_DDR_ECC_UnCor_Error_Counter_ro`

* **Address**             : `0x00014C(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxddreccuncorerror`| This counter count the number of Rx DDR ECC unCorrectable error.| `RW`| `0x0`| `0x0 End: Begin:`|

###Rx DDR ECC UnCorrectable Error Counter

* **Description**           

Count number of Rx DDR ECC UnCorrectable error detected


* **RTL Instant Name**    : `Rx_DDR_ECC_UnCor_Error_Counter_r2c`

* **Address**             : `0x00014B(R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxddreccuncorerror`| This counter count the number of Rx DDR ECC unCorrectable error.| `RW`| `0x0`| `0x0 End: Begin:`|

###Rx DDR Force Error Control

* **Description**           




* **RTL Instant Name**    : `Rx_DDR_errins_en_cfg`

* **Address**             : `0x000148`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[29]`|`rxddrerrmode`| 0: Correctable (1bit); 1:UnCorrectable (2bit)| `RW`| `0x0`| `0x0`|
|`[28]`|`rxddrerrrate`| 0: One-shot with number of errors; 1:Line rate| `RW`| `0x0`| `0x0`|
|`[27:0]`|`rxddrerrthr`| Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1| `RW`| `0x0`| `0x0 End: Begin:`|

###Tx DDR ECC Correctable Error Counter

* **Description**           

Count number of Tx DDR ECC Correctable error detected


* **RTL Instant Name**    : `Tx_DDR_ECC_Cor_Error_Counter_ro`

* **Address**             : `0x000152(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txddrecccorerror`| This counter count the number of TxDDR ECC Correctable error.| `RW`| `0x0`| `0x0 End: Begin:`|

###Tx DDR ECC Correctable Error Counter

* **Description**           

Count number of Tx DDR ECC Correctable error detected


* **RTL Instant Name**    : `Tx_DDR_ECC_Cor_Error_Counter_r2c`

* **Address**             : `0x000151(R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txddrecccorerror`| This counter count the number of TxDDR ECC Correctable error.| `RW`| `0x0`| `0x0 End: Begin:`|

###Tx DDR ECC UnCorrectable Error Counter

* **Description**           

Count number of Rx DDR ECC UnCorrectable error detected


* **RTL Instant Name**    : `Tx_DDR_ECC_UnCor_Error_Counter_ro`

* **Address**             : `0x000154(RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txddreccuncorerror`| This counter count the number of Tx DDR ECC unCorrectable error.| `RW`| `0x0`| `0x0 End: Begin:`|

###Tx DDR ECC UnCorrectable Error Counter

* **Description**           

Count number of Rx DDR ECC UnCorrectable error detected


* **RTL Instant Name**    : `Tx_DDR_ECC_UnCor_Error_Counter_r2c`

* **Address**             : `0x000153(R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txddreccuncorerror`| This counter count the number of Tx DDR ECC unCorrectable error.| `RW`| `0x0`| `0x0 End: Begin:`|

###Tx DDR Force Error Control

* **Description**           




* **RTL Instant Name**    : `Tx_DDR_errins_en_cfg`

* **Address**             : `0x000150`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[29]`|`txddrerrmode`| 0: Correctable (1bit); 1:UnCorrectable (2bit)| `RW`| `0x0`| `0x0`|
|`[28]`|`txddrerrrate`| 0: One-shot with number of errors; 1:Line rate| `RW`| `0x0`| `0x0`|
|`[27:0]`|`txddrerrthr`| Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1| `RW`| `0x0`| `0x0 End:`|

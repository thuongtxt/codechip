## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_PMC
####Register Table

|Name|Address|
|-----|-----|
|`Ethernet Receive FCS Error Packet Counter`|`0x00000000 (RO)`|
|`Ethernet Receive FCS Error Packet Counter`|`0x00000001 (R2C)`|
|`Ethernet Receive Header Error Packet Counter`|`0x00000002 (RO)`|
|`Ethernet Receive Header Error Packet Counter`|`0x00000003 (R2C)`|
|`Ethernet Receive PSN Header Error Packet Counter`|`0x00000004 (RO)`|
|`Ethernet Receive PSN Header Error Packet Counter`|`0x00000005 (R2C)`|
|`Ethernet Receive UDP Port Error Packet Counter`|`0x00000006 (RO)`|
|`Ethernet Receive UDP Port Error Packet Counter`|`0x00000007 (R2C)`|
|`4.1.5. Ethernet Receive OAM Packet Counter`|`0x0000000E (RO)`|
|`4.1.5. Ethernet Receive OAM Packet Counter`|`0x0000000F (R2C)`|
|`Ethernet Transmit OAM Packet Counter`|`0x00000010 (RO)`|
|`Ethernet Transmit OAM Packet Counter`|`0x00000011 (R2C)`|
|`4.1.7. Ethernet Receive Total Byte Counter`|`0x00000012 (RO)`|
|`4.1.7. Ethernet Receive Total Byte Counter`|`0x00000013 (R2C)`|
|`4.1.7. Ethernet Receive Total Packt Counter`|`0x00000014 (RO)`|
|`4.1.7. Ethernet Receive Total Packt Counter`|`0x00000015 (R2C)`|
|`Ethernet Receive Classified Packet Counter`|`0x00000016 (RO)`|
|`Ethernet Receive Classified Packet Counter`|`0x00000017 (R2C)`|
|`Ethernet Transmit Byte Counter`|`0x00000018 (RO)`|
|`Ethernet Transmit Byte Counter`|`0x00000019 (R2C)`|
|`Ethernet Transmit Packet Counter`|`0x0000001A (RO)`|
|`Ethernet Transmit Packet Counter`|`0x0000001B (R2C)`|
|`Ethernet Receive 1to64Byte Packet Counter`|`0x00000020 (RO)`|
|`Ethernet Receive 1to64Byte Packet Counter`|`0x00000021 (R2C)`|
|`Ethernet Transmit 1to64Byte Packet Counter`|`0x00000030 (RO)`|
|`Ethernet Transmit 1to64Byte Packet Counter`|`0x00000031 (R2C)`|
|`Ethernet Receive 65to128Byte Packet Counter`|`0x00000022 (RO)`|
|`Ethernet Receive 65to128Byte Packet Counter`|`0x00000023 (R2C)`|
|`Ethernet Transmit 65to128Byte Packet Counter`|`0x00000032 (RO)`|
|`Ethernet Transmit 65to128Byte Packet Counter`|`0x00000033 (R2C)`|
|`Ethernet Receive 129to256Byte Packet Counter`|`0x00000024 (RO)`|
|`Ethernet Receive 129to256Byte Packet Counter`|`0x00000025 (R2C)`|
|`Ethernet Transmit 129to256Byte Packet Counter`|`0x00000034 (RO)`|
|`Ethernet Transmit 129to256Byte Packet Counter`|`0x00000035 (R2C)`|
|`Ethernet Receive 257to512Byte Packet Counter`|`0x00000026 (RO)`|
|`Ethernet Receive 257to512Byte Packet Counter`|`0x00000027 (R2C)`|
|`Ethernet Transmit 257to512Byte Packet Counter`|`0x00000036 (RO)`|
|`Ethernet Transmit 257to512Byte Packet Counter`|`0x00000037 (R2C)`|
|`Ethernet Receive 513to1024Byte Packet Counter`|`0x00000028 (RO)`|
|`Ethernet Receive 513to1024Byte Packet Counter`|`0x00000029 (R2C)`|
|`Ethernet Transmit 513to1024Byte Packet Counter`|`0x00000038 (RO)`|
|`Ethernet Transmit 513to1024Byte Packet Counter`|`0x00000039 (R2C)`|
|`Ethernet Receive 1025to1528Byte Packet Counter`|`0x0000002A (RO)`|
|`Ethernet Receive 1025to1528Byte Packet Counter`|`0x0000002B (R2C)`|
|`Ethernet Transmit 1025to1528Byte Packet Counter`|`0x0000003A (RO)`|
|`Ethernet Transmit 1025to1528Byte Packet Counter`|`0x0000003B (R2C)`|
|`Ethernet Receive jumboByte Packet Counter`|`0x0000002C (RO)`|
|`Ethernet Receive jumboByte Packet Counter`|`0x0000002D (R2C)`|
|`Ethernet Transmit jumboByte Packet Counter`|`0x0000003C (RO)`|
|`Ethernet Transmit jumboByte Packet Counter`|`0x0000003D (R2C)`|
|`Pseudowire Transmit Good Packet Counter`|`0x010800 - 0x010a9F(RO)`|
|`Pseudowire Transmit Good Packet Counter`|`0x010000 - 0x01029F(RC)`|
|`Pseudowire Receive Payload Octet Counter`|`0x011800 - 0x011A9F(RO)`|
|`Pseudowire Receive Payload Octet Counter`|`0x011000 - 0x01129F(RC)`|
|`Pseudowire Transmit LBit Packet Counter`|`0x012800 - 0x012A9F(RO)`|
|`Pseudowire Transmit LBit Packet Counter`|`0x012000 - 0x01229F(RC)`|
|`Pseudowire Transmit RBit Packet Counter`|`0x013800 - 0x013A9F(RO)`|
|`Pseudowire Transmit RBit Packet Counter`|`0x013000 - 0x01329F(RC)`|
|`Pseudowire Transmit MBit_NBit Packet Counter`|`0x014800 - 0x014A9F(RO)`|
|`Pseudowire Transmit MBit_NBit Packet Counter`|`0x014000 - 0x01429F(RC)`|
|`Pseudowire Transmit PBit Packet Counter`|`0x015800 - 0x015A9F(RO)`|
|`Pseudowire Transmit PBit Packet Counter`|`0x015000 - 0x01529F(RC)`|
|`Pseudowire Receive Good Packet Counter`|`0x020800 - 0x020A9F(RO)`|
|`Pseudowire Receive Good Packet Counter`|`0x020000 - 0x02029F(RC)`|
|`Pseudowire Receive Good Byte Counter`|`0x021800 - 0x021A9F(RO)`|
|`Pseudowire Receive Good Byte Counter`|`0x021000 - 0x02129F(RC)`|
|`Pseudowire Receive LOFS Counter`|`0x022800 - 0x022A9F(RO)`|
|`Pseudowire Receive LOFS Counter`|`0x022000 - 0x02229F(RC)`|
|`Pseudowire Receive LBit Packet Counter`|`0x023800 - 0x023A9F(RO)`|
|`Pseudowire Receive LBit Packet Counter`|`0x023000 - 0x02329F(RC)`|
|`Pseudowire Receive M_NBit Packet Counter`|`0x024800 - 0x024A9F(RO)`|
|`Pseudowire Receive M_NBit Packet Counter`|`0x024000 - 0x02429F(RC)`|
|`Pseudowire Receive PBit Packet Counter`|`0x025800 - 0x025A9F(RO)`|
|`Pseudowire Receive PBit Packet Counter`|`0x025000 - 0x02529F(RC)`|
|`Pseudowire Receive RBit Packet Counter`|`0x026800 - 0x026A9F(RO)`|
|`Pseudowire Receive RBit Packet Counter`|`0x026000 - 0x02629F(RC)`|
|`Pseudowire Receive Reorder Drop Packet Counter`|`0x027800 - 0x027A9F(RO)`|
|`Pseudowire Receive Reorder Drop Packet Counter`|`0x027000 - 0x02729F(RC)`|
|`Pseudowire Receive Reorder Out Of Sequence Packet Counter`|`0x028800 - 0x028A9F(RO)`|
|`Pseudowire Receive Reorder Out Of Sequence Packet Counter`|`0x028000 - 0x02829F(RC)`|
|`Pseudowire Receive Reorder Lost Packet Counter`|`0x029800 - 0x029A9F(RO)`|
|`Pseudowire Receive Reorder Lost Packet Counter`|`0x029000 - 0x02929F(RC)`|
|`Pseudowire Receive Jitter Buffer OverRun Counter`|`0x02A800 - 0x02AA9F(RO)`|
|`Pseudowire Receive Jitter Buffer OverRun Counter`|`0x02A000 - 0x02A29F(RC)`|
|`Pseudowire Receive Jitter Buffer UnderRun Counter`|`0x02B800 - 0x02BA9F(RO)`|
|`Pseudowire Receive Jitter Buffer UnderRun Counter`|`0x02B000 - 0x02B29F(RC)`|
|`Pseudowire Receive Malform packet Counter`|`0x02C800 - 0x02CA9F(RO)`|
|`Pseudowire Receive Malform packet Counter`|`0x02C000 - 0x02C29F(RC)`|
|`Pseudowire Receive Stray packet Counter`|`0x02D800 - 0x02DA9F(RO)`|
|`Pseudowire Receive Stray packet Counter`|`0x02D000 - 0x02D29F(RC)`|
|`Pseudowire Receive Duplicate packet Counter`|`0x02E800 - 0x02EA9F(RO)`|
|`Pseudowire Receive Duplicate packet Counter`|`0x02E000 - 0x02E29F(RC)`|
|`Counter Per Alarm Interrupt Enable Control`|`0x040000`|
|`Counter Per Alarm Interrupt Status`|`0x040800`|
|`Counter Per Alarm Current Status`|`0x041000`|
|`Counter Interrupt OR Status`|`0x041800`|
|`Counter per Group Interrupt OR Status`|`0x041BFF`|
|`Counter per Group Interrupt Enable Control`|`0x041BFE`|


###Ethernet Receive FCS Error Packet Counter

* **Description**           

TCount number of FCS error packets detected


* **RTL Instant Name**    : `eth_rx_fcs_err_cnt_ro`

* **Address**             : `0x00000000 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxfcserrorpk`| This counter count the number of FCS error Ethernet PHY packets received.| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive FCS Error Packet Counter

* **Description**           

TCount number of FCS error packets detected


* **RTL Instant Name**    : `eth_rx_fcs_err_cnt_r2c`

* **Address**             : `0x00000001 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxfcserrorpk`| This counter count the number of FCS error Ethernet PHY packets received.| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive Header Error Packet Counter

* **Description**           

Count number of Ethernet MAC header error packets detected


* **RTL Instant Name**    : `eth_rx_header_err_cnt_ro`

* **Address**             : `0x00000002 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxetherrorpk`| This counter count the number of packets with MAC Header Error.| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive Header Error Packet Counter

* **Description**           

Count number of Ethernet MAC header error packets detected


* **RTL Instant Name**    : `eth_rx_header_err_cnt_r2c`

* **Address**             : `0x00000003 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxetherrorpk`| This counter count the number of packets with MAC Header Error.| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive PSN Header Error Packet Counter

* **Description**           

Count number of PSN header error packets detected


* **RTL Instant Name**    : `eth_rx_psnheader_err_cnt_ro`

* **Address**             : `0x00000004 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxpsnerrorpk`| This counter count the number of packets with PSN Header error.| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive PSN Header Error Packet Counter

* **Description**           

Count number of PSN header error packets detected


* **RTL Instant Name**    : `eth_rx_psnheader_err_cnt_r2c`

* **Address**             : `0x00000005 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxpsnerrorpk`| This counter count the number of packets with PSN Header error.| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive UDP Port Error Packet Counter

* **Description**           

Count number of UDP port error packets detected


* **RTL Instant Name**    : `eth_rx_udpport_err_cnt_ro`

* **Address**             : `0x00000006 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxudperrorpk`| This counter count the number of packets with UDP port error.| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive UDP Port Error Packet Counter

* **Description**           

Count number of UDP port error packets detected


* **RTL Instant Name**    : `eth_rx_udpport_err_cnt_r2c`

* **Address**             : `0x00000007 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxudperrorpk`| This counter count the number of packets with UDP port error.| `RO`| `0x0`| `0x0 End: Begin:`|

###4.1.5. Ethernet Receive OAM Packet Counter

* **Description**           

Count number of classified OAM packets (ARP, ICMP,...) received from Ethernet port


* **RTL Instant Name**    : `eth_rx_oam_pkt_cnt_ro`

* **Address**             : `0x0000000E (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxoampkt`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###4.1.5. Ethernet Receive OAM Packet Counter

* **Description**           

Count number of classified OAM packets (ARP, ICMP,...) received from Ethernet port


* **RTL Instant Name**    : `eth_rx_oam_pkt_cnt_r2c`

* **Address**             : `0x0000000F (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxoampkt`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit OAM Packet Counter

* **Description**           

Count number of transmitted OAM packets (ARP, ICMP,...) to Ethernet port


* **RTL Instant Name**    : `eth_tx_oam_pkt_cnt_ro`

* **Address**             : `0x00000010 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`txoampkt`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit OAM Packet Counter

* **Description**           

Count number of transmitted OAM packets (ARP, ICMP,...) to Ethernet port


* **RTL Instant Name**    : `eth_tx_oam_pkt_cnt_r2c`

* **Address**             : `0x00000011 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`txoampkt`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###4.1.7. Ethernet Receive Total Byte Counter

* **Description**           

Count total number of bytes received from Ethernet port


* **RTL Instant Name**    : `eth_rx_total_byte_cnt_ro`

* **Address**             : `0x00000012 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxbyte`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###4.1.7. Ethernet Receive Total Byte Counter

* **Description**           

Count total number of bytes received from Ethernet port


* **RTL Instant Name**    : `eth_rx_total_byte_cnt_r2c`

* **Address**             : `0x00000013 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxbyte`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###4.1.7. Ethernet Receive Total Packt Counter

* **Description**           

Count total number of packets received from Ethernet port


* **RTL Instant Name**    : `eth_rx_total_pkt_cnt_ro`

* **Address**             : `0x00000014 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpacket`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###4.1.7. Ethernet Receive Total Packt Counter

* **Description**           

Count total number of packets received from Ethernet port


* **RTL Instant Name**    : `eth_rx_total_pkt_cnt_r2c`

* **Address**             : `0x00000015 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpacket`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive Classified Packet Counter

* **Description**           

Count number of classified packets (PW packets plus OAM packets) from Ethernet port


* **RTL Instant Name**    : `eth_rx_cla_pkt_cnt_ro`

* **Address**             : `0x00000016 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxclassifypk`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive Classified Packet Counter

* **Description**           

Count number of classified packets (PW packets plus OAM packets) from Ethernet port


* **RTL Instant Name**    : `eth_rx_cla_pkt_cnt_r2c`

* **Address**             : `0x00000017 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxclassifypk`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit Byte Counter

* **Description**           

Count total number of  bytes transmitted to Ethernet port


* **RTL Instant Name**    : `eth_tx_byte_cnt_ro`

* **Address**             : `0x00000018 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txbyte`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit Byte Counter

* **Description**           

Count total number of  bytes transmitted to Ethernet port


* **RTL Instant Name**    : `eth_tx_byte_cnt_r2c`

* **Address**             : `0x00000019 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txbyte`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit Packet Counter

* **Description**           

Count total number of  packets transmitted to Ethernet port


* **RTL Instant Name**    : `eth_tx_pkt_cnt_ro`

* **Address**             : `0x0000001A (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txpacket`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit Packet Counter

* **Description**           

Count total number of  packets transmitted to Ethernet port


* **RTL Instant Name**    : `eth_tx_pkt_cnt_r2c`

* **Address**             : `0x0000001B (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txpacket`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive 1to64Byte Packet Counter

* **Description**           

Count total number of  received packets length from 1 to 64 bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_1to64byte_pkt_cnt_ro`

* **Address**             : `0x00000020 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive 1to64Byte Packet Counter

* **Description**           

Count total number of  received packets length from 1 to 64 bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_1to64byte_pkt_cnt_r2c`

* **Address**             : `0x00000021 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit 1to64Byte Packet Counter

* **Description**           

Count total number of  transmitted packets length from 1 to 64 bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_1to64byte_pkt_cnt_ro`

* **Address**             : `0x00000030 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit 1to64Byte Packet Counter

* **Description**           

Count total number of  transmitted packets length from 1 to 64 bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_1to64byte_pkt_cnt_r2c`

* **Address**             : `0x00000031 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive 65to128Byte Packet Counter

* **Description**           

Count total number of  received packets length from 65 to 128 bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_65to128byte_pkt_cnt_ro`

* **Address**             : `0x00000022 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive 65to128Byte Packet Counter

* **Description**           

Count total number of  received packets length from 65 to 128 bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_65to128byte_pkt_cnt_r2c`

* **Address**             : `0x00000023 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit 65to128Byte Packet Counter

* **Description**           

Count total number of  transmitted packets length from 65 to 128 bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_65to128byte_pkt_cnt_ro`

* **Address**             : `0x00000032 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit 65to128Byte Packet Counter

* **Description**           

Count total number of  transmitted packets length from 65 to 128 bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_65to128byte_pkt_cnt_r2c`

* **Address**             : `0x00000033 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive 129to256Byte Packet Counter

* **Description**           

Count total number of  received packets length from 129  to 256 bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_129to256byte_pkt_cnt_ro`

* **Address**             : `0x00000024 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive 129to256Byte Packet Counter

* **Description**           

Count total number of  received packets length from 129  to 256 bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_129to256byte_pkt_cnt_r2c`

* **Address**             : `0x00000025 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit 129to256Byte Packet Counter

* **Description**           

Count total number of  transmitted packets length from 129  to 256 bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_129to256byte_pkt_cnt_ro`

* **Address**             : `0x00000034 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit 129to256Byte Packet Counter

* **Description**           

Count total number of  transmitted packets length from 129  to 256 bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_129to256byte_pkt_cnt_r2c`

* **Address**             : `0x00000035 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive 257to512Byte Packet Counter

* **Description**           

Count total number of  received packets length from 257  to  512 bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_257to512byte_pkt_cnt_ro`

* **Address**             : `0x00000026 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive 257to512Byte Packet Counter

* **Description**           

Count total number of  received packets length from 257  to  512 bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_257to512byte_pkt_cnt_r2c`

* **Address**             : `0x00000027 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit 257to512Byte Packet Counter

* **Description**           

Count total number of  transmitted packets length from 257  to  512 bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_257to512byte_pkt_cnt_ro`

* **Address**             : `0x00000036 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit 257to512Byte Packet Counter

* **Description**           

Count total number of  transmitted packets length from 257  to  512 bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_257to512byte_pkt_cnt_r2c`

* **Address**             : `0x00000037 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive 513to1024Byte Packet Counter

* **Description**           

Count total number of  received packets length from 513 to 1024 bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_513to1024byte_pkt_cnt_ro`

* **Address**             : `0x00000028 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive 513to1024Byte Packet Counter

* **Description**           

Count total number of  received packets length from 513 to 1024 bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_513to1024byte_pkt_cnt_r2c`

* **Address**             : `0x00000029 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit 513to1024Byte Packet Counter

* **Description**           

Count total number of  transmitted packets length from 513 to 1024 bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_513to1024byte_pkt_cnt_ro`

* **Address**             : `0x00000038 (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit 513to1024Byte Packet Counter

* **Description**           

Count total number of  transmitted packets length from 513 to 1024 bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_513to1024byte_pkt_cnt_r2c`

* **Address**             : `0x00000039 (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive 1025to1528Byte Packet Counter

* **Description**           

Count total number of  received packets length from 1025 to 1528 bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_1025to1528byte_pkt_cnt_ro`

* **Address**             : `0x0000002A (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive 1025to1528Byte Packet Counter

* **Description**           

Count total number of  received packets length from 1025 to 1528 bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_1025to1528byte_pkt_cnt_r2c`

* **Address**             : `0x0000002B (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit 1025to1528Byte Packet Counter

* **Description**           

Count total number of  transmitted packets length from 1025 to 1528 bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_1025to1528byte_pkt_cnt_ro`

* **Address**             : `0x0000003A (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit 1025to1528Byte Packet Counter

* **Description**           

Count total number of  transmitted packets length from 1025 to 1528 bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_1025to1528byte_pkt_cnt_r2c`

* **Address**             : `0x0000003B (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive jumboByte Packet Counter

* **Description**           

Count total number of  received packets length from jumbo bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_jumbobyte_pkt_cnt_ro`

* **Address**             : `0x0000002C (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Receive jumboByte Packet Counter

* **Description**           

Count total number of  received packets length from jumbo bytes from Ethernet port


* **RTL Instant Name**    : `eth_rx_jumbobyte_pkt_cnt_r2c`

* **Address**             : `0x0000002D (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit jumboByte Packet Counter

* **Description**           

Count total number of  transmitted packets length from jumbo bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_jumbobyte_pkt_cnt_ro`

* **Address**             : `0x0000003C (RO)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Ethernet Transmit jumboByte Packet Counter

* **Description**           

Count total number of  transmitted packets length from jumbo bytes from Ethernet port


* **RTL Instant Name**    : `eth_tx_jumbobyte_pkt_cnt_r2c`

* **Address**             : `0x0000003D (R2C)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`packetnum`| Counter value| `RO`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Good Packet Counter

* **Description**           

Count number of CESoETH packets transmitted to Ethernet side.


* **RTL Instant Name**    : `Pseudowire_Transmit_Good_Packet_Counter_ro`

* **Address**             : `0x010800 - 0x010a9F(RO)`

* **Formula**             : `0x010800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDsu`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txpwgoodpk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Good Packet Counter

* **Description**           

Count number of CESoETH packets transmitted to Ethernet side.


* **RTL Instant Name**    : `Pseudowire_Transmit_Good_Packet_Counter_rc`

* **Address**             : `0x010000 - 0x01029F(RC)`

* **Formula**             : `0x010000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDsu`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txpwgoodpk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Payload Octet Counter

* **Description**           

Count number of pseudowire payload octet received


* **RTL Instant Name**    : `Pseudowire_Receive_Payload_Octet_Counter_ro`

* **Address**             : `0x011800 - 0x011A9F(RO)`

* **Formula**             : `0x011800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwpayoct`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Payload Octet Counter

* **Description**           

Count number of pseudowire payload octet received


* **RTL Instant Name**    : `Pseudowire_Receive_Payload_Octet_Counter_rc`

* **Address**             : `0x011000 - 0x01129F(RC)`

* **Formula**             : `0x011000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwpayoct`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit LBit Packet Counter

* **Description**           

Count number of CESoETH packets transmitted to Ethernet side.


* **RTL Instant Name**    : `Pseudowire_Transmit_LBit_Packet_Counter_ro`

* **Address**             : `0x012800 - 0x012A9F(RO)`

* **Formula**             : `0x012800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDsu`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txpwlbitpk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit LBit Packet Counter

* **Description**           

Count number of CESoETH packets transmitted to Ethernet side.


* **RTL Instant Name**    : `Pseudowire_Transmit_LBit_Packet_Counter_rc`

* **Address**             : `0x012000 - 0x01229F(RC)`

* **Formula**             : `0x012000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDsu`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txpwlbitpk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit RBit Packet Counter

* **Description**           

Count number of CESoETH packets transmitted to Ethernet side.


* **RTL Instant Name**    : `Pseudowire_Transmit_RBit_Packet_Counter_ro`

* **Address**             : `0x013800 - 0x013A9F(RO)`

* **Formula**             : `0x013800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDsu`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txpwrbitpk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit RBit Packet Counter

* **Description**           

Count number of CESoETH packets transmitted to Ethernet side.


* **RTL Instant Name**    : `Pseudowire_Transmit_RBit_Packet_Counter_rc`

* **Address**             : `0x013000 - 0x01329F(RC)`

* **Formula**             : `0x013000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDsu`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txpwrbitpk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit MBit_NBit Packet Counter

* **Description**           

Count number of CESoETH packets transmitted to Ethernet side.


* **RTL Instant Name**    : `Pseudowire_Transmit_MBit_NBit_Packet_Counter_ro`

* **Address**             : `0x014800 - 0x014A9F(RO)`

* **Formula**             : `0x014800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDsu`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txpwmbit_nbitpk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit MBit_NBit Packet Counter

* **Description**           

Count number of CESoETH packets transmitted to Ethernet side.


* **RTL Instant Name**    : `Pseudowire_Transmit_MBit_NBit_Packet_Counter_rc`

* **Address**             : `0x014000 - 0x01429F(RC)`

* **Formula**             : `0x014000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDsu`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txpwmbit_nbitpk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit PBit Packet Counter

* **Description**           

Count number of CESoETH packets transmitted to Ethernet side.


* **RTL Instant Name**    : `Pseudowire_Transmit_PBit_Packet_Counter_ro`

* **Address**             : `0x015800 - 0x015A9F(RO)`

* **Formula**             : `0x015800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDsu`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txpwpbitpk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit PBit Packet Counter

* **Description**           

Count number of CESoETH packets transmitted to Ethernet side.


* **RTL Instant Name**    : `Pseudowire_Transmit_PBit_Packet_Counter_rc`

* **Address**             : `0x015000 - 0x01529F(RC)`

* **Formula**             : `0x015000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDsu`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txpwpbitpk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Good Packet Counter

* **Description**           

Count number of CESoETH packets received


* **RTL Instant Name**    : `Pseudowire_Receive_Good_Packet_Counter_ro`

* **Address**             : `0x020800 - 0x020A9F(RO)`

* **Formula**             : `0x020800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwgoodpk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Good Packet Counter

* **Description**           

Count number of CESoETH packets received


* **RTL Instant Name**    : `Pseudowire_Receive_Good_Packet_Counter_rc`

* **Address**             : `0x020000 - 0x02029F(RC)`

* **Formula**             : `0x020000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwgoodpk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Good Byte Counter

* **Description**           

Count number of CESoETH packets received


* **RTL Instant Name**    : `Pseudowire_Receive_Good_Byte_Counter_ro`

* **Address**             : `0x021800 - 0x021A9F(RO)`

* **Formula**             : `0x021800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwbytepk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Good Byte Counter

* **Description**           

Count number of CESoETH packets received


* **RTL Instant Name**    : `Pseudowire_Receive_Good_Byte_Counter_rc`

* **Address**             : `0x021000 - 0x02129F(RC)`

* **Formula**             : `0x021000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwbytepk`| Counter value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive LOFS Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_LOFS_Counter_ro`

* **Address**             : `0x022800 - 0x022A9F(RO)`

* **Formula**             : `0x022800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwlofsevent`| This counter count the number of transitions from normal sate to loss of frame state| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive LOFS Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_LOFS_Counter_rc`

* **Address**             : `0x022000 - 0x02229F(RC)`

* **Formula**             : `0x022000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwlofsevent`| This counter count the number of transitions from normal sate to loss of frame state| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive LBit Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_LBit_Packet_Counter_ro`

* **Address**             : `0x023800 - 0x023A9F(RO)`

* **Formula**             : `0x023800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwlbitevent`| This counter count the number of Receive LBit Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive LBit Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_LBit_Packet_Counter_rc`

* **Address**             : `0x023000 - 0x02329F(RC)`

* **Formula**             : `0x023000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwlbitevent`| This counter count the number of Receive LBit Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive M_NBit Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_M_NBit_Packet_Counter_ro`

* **Address**             : `0x024800 - 0x024A9F(RO)`

* **Formula**             : `0x024800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwm_nbitevent`| This counter count the number of Receive M_NBit Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive M_NBit Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_M_NBit_Packet_Counter_rc`

* **Address**             : `0x024000 - 0x02429F(RC)`

* **Formula**             : `0x024000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwm_nbitevent`| This counter count the number of Receive M_NBit Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive PBit Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_PBit_Packet_Counter_ro`

* **Address**             : `0x025800 - 0x025A9F(RO)`

* **Formula**             : `0x025800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwpbitevent`| This counter count the number of Receive PBit Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive PBit Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_PBit_Packet_Counter_rc`

* **Address**             : `0x025000 - 0x02529F(RC)`

* **Formula**             : `0x025000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwpbitevent`| This counter count the number of Receive PBit Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive RBit Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_RBit_Packet_Counter_ro`

* **Address**             : `0x026800 - 0x026A9F(RO)`

* **Formula**             : `0x026800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwrbitevent`| This counter count the number of Receive RBit Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive RBit Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_RBit_Packet_Counter_rc`

* **Address**             : `0x026000 - 0x02629F(RC)`

* **Formula**             : `0x026000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwrbitevent`| This counter count the number of Receive RBit Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Reorder Drop Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Reorder_Drop_Packet_Counter_ro`

* **Address**             : `0x027800 - 0x027A9F(RO)`

* **Formula**             : `0x027800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwreordropevent`| This counter count the number of Receive Reorder Drop Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Reorder Drop Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Reorder_Drop_Packet_Counter_rc`

* **Address**             : `0x027000 - 0x02729F(RC)`

* **Formula**             : `0x027000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwreordropevent`| This counter count the number of Receive Reorder Drop Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Reorder Out Of Sequence Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_ro`

* **Address**             : `0x028800 - 0x028A9F(RO)`

* **Formula**             : `0x028800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwreoroseqevent`| This counter count the number of Receive Reorder Out Of Sequence Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Reorder Out Of Sequence Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Reorder_Out_Of_Sequence_Packet_Counter_rc`

* **Address**             : `0x028000 - 0x02829F(RC)`

* **Formula**             : `0x028000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwreoroseqevent`| This counter count the number of Receive Reorder Out Of Sequence Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Reorder Lost Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Reorder_Lost_Packet_Counter_ro`

* **Address**             : `0x029800 - 0x029A9F(RO)`

* **Formula**             : `0x029800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwreorlostevent`| This counter count the number of Receive Reorder Lost Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Reorder Lost Packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Reorder_Lost_Packet_Counter_rc`

* **Address**             : `0x029000 - 0x02929F(RC)`

* **Formula**             : `0x029000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwreorlostevent`| This counter count the number of Receive Reorder Lost Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Jitter Buffer OverRun Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_ro`

* **Address**             : `0x02A800 - 0x02AA9F(RO)`

* **Formula**             : `0x02A800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwoverrunevent`| This counter count the number of Receive Jitter Buffer Overrun| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Jitter Buffer OverRun Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Jitter_Buffer_OverRun_Counter_rc`

* **Address**             : `0x02A000 - 0x02A29F(RC)`

* **Formula**             : `0x02A000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwoverrunevent`| This counter count the number of Receive Jitter Buffer Overrun| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Jitter Buffer UnderRun Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_ro`

* **Address**             : `0x02B800 - 0x02BA9F(RO)`

* **Formula**             : `0x02B800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwunderrunevent`| This counter count the number of Receive Jitter Buffer UnderRun| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Jitter Buffer UnderRun Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Jitter_Buffer_UnderRun_Counter_rc`

* **Address**             : `0x02B000 - 0x02B29F(RC)`

* **Formula**             : `0x02B000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwunderrunevent`| This counter count the number of Receive Jitter Buffer UnderRun| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Malform packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Malform_packet_Counter_ro`

* **Address**             : `0x02C800 - 0x02CA9F(RO)`

* **Formula**             : `0x02C800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwmalformevent`| This counter count the number of Receive Malform packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Malform packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Malform_packet_Counter_rc`

* **Address**             : `0x02C000 - 0x02C29F(RC)`

* **Formula**             : `0x02C000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwmalformevent`| This counter count the number of Receive Malform packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Stray packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Stray_packet_Counter_ro`

* **Address**             : `0x02D800 - 0x02DA9F(RO)`

* **Formula**             : `0x02D800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwstrayevent`| This counter count the number of Receive Stray packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Stray packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Stray_packet_Counter_rc`

* **Address**             : `0x02D000 - 0x02D29F(RC)`

* **Formula**             : `0x02D000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwstrayevent`| This counter count the number of Receive Stray packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Duplicate packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Duplicate_packet_Counter_ro`

* **Address**             : `0x02E800 - 0x02EA9F(RO)`

* **Formula**             : `0x02E800 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwdupliateevent`| This counter count the number of Receive Duplicate packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receive Duplicate packet Counter

* **Description**           




* **RTL Instant Name**    : `Pseudowire_Receive_Duplicate_packet_Counter_rc`

* **Address**             : `0x02E000 - 0x02E29F(RC)`

* **Formula**             : `0x02E000 + PwId`

* **Where**               : 

    * `$PwId (0 - 671): Pseudowire IDs`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxpwdupliateevent`| This counter count the number of Receive Duplicate packet| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter Per Alarm Interrupt Enable Control

* **Description**           

This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.


* **RTL Instant Name**    : `Counter_Per_Alarm_Interrupt_Enable_Control`

* **Address**             : `0x040000`

* **Formula**             : `0x040000 +  GrpID*32 + BitID`

* **Where**               : 

    * `$GrpID(0-31):  Pseudowire ID bits[10:5]`

    * `$BitID(0-31): Pseudowire ID bits [4:0]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8]`|`straystatechgintren`| Set 1 to enable Stray packet event to generate an interrupt| `RW`| `0x0`| `0x0`|
|`[7]`|`malformstatechgintren`| Set 1 to enable Malform packet event to generate an interrupt| `RW`| `0x0`| `0x0`|
|`[6]`|`mbitstatechgintren`| Set 1 to enable Mbit packet event to generate an interrupt| `RW`| `0x0`| `0x0`|
|`[5]`|`rbitstatechgintren`| Set 1 to enable Rbit packet event to generate an interrupt| `RW`| `0x0`| `0x0`|
|`[4]`|`lofssyncstatechgintren`| Set 1 to enable Lost of frame Sync event to generate an interrupt| `RW`| `0x0`| `0x0`|
|`[3]`|`underrunstatechgintren`| Set 1 to enable change jitter buffer state event from normal to underrun and vice versa in the related pseudowire to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[2]`|`overrunstatechgintren`| Set 1 to enable change jitter buffer state event from normal to overrun and vice versa in the related pseudowire to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[1]`|`lofsstatechgintren`| Set 1 to enable change lost of frame state(LOFS) event from normal to LOFS and vice versa in the related pseudowire to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[0]`|`lbitstatechgintren`| Set 1 to enable Lbit packet event to generate an interrupt| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter Per Alarm Interrupt Status

* **Description**           

This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.


* **RTL Instant Name**    : `Counter_Per_Alarm_Interrupt_Status`

* **Address**             : `0x040800`

* **Formula**             : `0x040800 +  GrpID*32 + BitID`

* **Where**               : 

    * `$GrpID(0-31):  Pseudowire ID bits[10:5]`

    * `$BitID(0-31): Pseudowire ID bits [4:0]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8]`|`straystatechgintrsta`| Set 1 when Stray packet event is detected in the pseudowire| `RW`| `0x0`| `0x0`|
|`[7]`|`malformstatechgintrsta`| Set 1 when Malform packet event is detected in the pseudowire| `RW`| `0x0`| `0x0`|
|`[6]`|`mbitstatechgintrsta`| Set 1 when a Mbit packet event is detected in the pseudowire| `RW`| `0x0`| `0x0`|
|`[5]`|`rbitstatechgintrsta`| Set 1 when a Rbit packet event is detected in the pseudowire| `RW`| `0x0`| `0x0`|
|`[4]`|`lofssyncstatechgintrsta`| Set 1 when a lost of frame Sync event is detected in the pseudowire| `RW`| `0x0`| `0x0`|
|`[3]`|`underrunstatechgintrsta`| Set 1 when there is a change in jitter buffer underrun state  in the related pseudowire| `RW`| `0x0`| `0x0`|
|`[2]`|`overrunstatechgintrsta`| Set 1 when there is a change in jitter buffer overrun state in the related pseudowire| `RW`| `0x0`| `0x0`|
|`[1]`|`lofsstatechgintrsta`| Set 1 when there is a change in lost of frame state(LOFS) in the related pseudowire| `RW`| `0x0`| `0x0`|
|`[0]`|`lbitstatechgintrsta`| Set 1 when a Lbit packet event is detected in the pseudowire| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter Per Alarm Current Status

* **Description**           

This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.


* **RTL Instant Name**    : `Counter_Per_Alarm_Current_Status`

* **Address**             : `0x041000`

* **Formula**             : `0x041000 +  GrpID*32 + BitID`

* **Where**               : 

    * `$GrpID(0-31):  Pseudowire ID bits[10:5]`

    * `$BitID(0-31): Pseudowire ID bits [4:0]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8]`|`straycurstatus`| Stray packet state current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[7]`|`malformcurstatus`| Malform packet state current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[6]`|`mbitcurstatus`| Mbit packet state current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[5]`|`rbitcurstatus`| Rbit packet state current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[4]`|`lofssynccurstatus`| Lost of frame Sync state current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[3]`|`underruncurstatus`| Jitter buffer underrun current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[2]`|`overruncurstatus`| Jitter buffer overrun current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[1]`|`lofsstatecurstatus`| Lost of frame state current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[0]`|`lbitstatecurstatus`| a Lbit packet state current status in the related pseudowire| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter Interrupt OR Status

* **Description**           

This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.


* **RTL Instant Name**    : `Counter_Interrupt_OR_Status`

* **Address**             : `0x041800`

* **Formula**             : `0x041800 +  Slice*1024 + GrpID`

* **Where**               : 

    * `$Slice(0-0):  Pseudowire ID bits[10]`

    * `$GrpID(0-31):  Pseudowire ID bits[9:5]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`introrstatus`| Set to 1 to indicate that there is any interrupt status bit in the Counter per Alarm Interrupt Status register of the related pseudowires to be set and they are enabled to raise interrupt. Bit 0 of GrpID#0 for pseudowire 0, bit 31 of GrpID#0 for pseudowire 31, bit 0 of GrpID#1 for pseudowire 32, respectively.| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter per Group Interrupt OR Status

* **Description**           

The register consists of 8 bits for 8 Group of the PW Counter. Each bit is used to store Interrupt OR status of the related Group.


* **RTL Instant Name**    : `counter_per_group_intr_or_stat`

* **Address**             : `0x041BFF`

* **Formula**             : `0x041BFF +  Slice*1024`

* **Where**               : 

    * `$Slice(0-0):  Pseudowire ID bits[10]`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`groupintrorsta`| Set to 1 if any interrupt bit of corresponding Group is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter per Group Interrupt Enable Control

* **Description**           

The register consists of 8 interrupt enable bits for 8 group in the PW counter.


* **RTL Instant Name**    : `counter_per_group_intr_en_ctrl`

* **Address**             : `0x041BFE`

* **Formula**             : `0x041BFE +  Slice*1024`

* **Where**               : 

    * `$Slice(0-0):  Pseudowire ID bits[10]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`groupintren`| Set to 1 to enable the related Group to generate interrupt.| `RW`| `0x0`| `0x0 End:`|

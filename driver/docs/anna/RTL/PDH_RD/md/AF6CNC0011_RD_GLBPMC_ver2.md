## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_GLBPMC_ver2
####Register Table

|Name|Address|
|-----|-----|
|`Pseudowire Transmit Counter`|`0x0_0000-0x0_0BFF(RW)`|
|`Pseudowire Transmit Counter`|`0x0_8000-0x0_8BFF(RW)`|
|`Pseudowire TX Byte Counter`|`0x8_0000-0x8_03FF (RW)`|
|`Pseudowire Receiver Counter Info0`|`0x1_0000-0x1_03FF (RW)`|
|`Pseudowire Receiver Counter Info0`|`0x1_4000-0x1_43FF (RW)`|
|`Pseudowire Receiver Counter Info1`|`0x2_0000-0x2_0BFF(RW)`|
|`Pseudowire Receiver Counter Info1`|`0x2_8000-0x2_8BFF(RW)`|
|`Pseudowire Receiver Counter Info2`|`0x3_0000-0x3_0BFF(RW)`|
|`Pseudowire Receiver Counter Info2`|`0x3_8000-0x3_8BFF(RW)`|
|`PMR Error Counter`|`0x9_1800-0x9_182F(RW)`|
|`PMR Error Counter`|`0x9_1840-0x9_186F(RW)`|
|`POH Path STS Counter`|`0x9_2000-0x9_21FF (RW)`|
|`POH Path STS Counter`|`0x9_2200-0x9_23FF (RW)`|
|`POH Path VT Counter`|`0x7_0000 - 0x7_3FFF(RW)`|
|`POH Path VT Counter`|`0x7_4000 - 0x7_7FFF(RW)`|
|`POH vt pointer Counter 0`|`0x5_0000-0x5_3FFF(RW)`|
|`POH vt pointer Counter 0`|`0x5_4000-0x5_7FFF(RW)`|
|`POH vt pointer Counter 1`|`0x6_0000-0x6_3FFF(RW)`|
|`POH vt pointer Counter 1`|`0x6_4000-0x6_7FFF(RW)`|
|`POH sts pointer Counter`|`0x9_0800-0x9_09FF(RW)`|
|`POH sts pointer Counter`|`0x9_0A00-0x9_0BFF(RW)`|
|`POH sts pointer Counter`|`0x9_1000-0x9_11FF(RW)`|
|`POH sts pointer Counter`|`0x9_1200-0x9_13FF(RW)`|
|`PDH ds1 cntval Counter`|`0x4_0000-0x4_3FFF(RW)`|
|`PDH ds1 cntval Counter`|`0x4_4000-0x4_7FFF(RW)`|
|`PDH ds3 cntval Counter`|`0x9_0000-0x9_01FF(RW)`|
|`PDH ds3 cntval Counter`|`0x9_0200-0x9_03FF(RW)`|
|`CLA Counter00`|`0x3_B321(RW)`|
|`CLA Counter00`|`0x3_B323(RW)`|
|`ETH TX Packet OAM CNT`|`0x9_3134(RW)`|
|`Classifier Bytes Counter vld`|`0x9_3140-0x9_3143(RW)`|
|`Classifier Bytes Counter vld`|`0x9_3160-0x9_3163(RW)`|
|`Classifier output counter pkt0`|`0x3_B34C-0x3_B34F(RW)`|
|`Classifier output counter pkt0`|`0xB_B34C-0xB_B34F(RW)`|
|`Classifier output counter pkt1`|`0x3_B354-0x3_B357(RW)`|
|`Classifier output counter pkt1`|`0xB_B354-0xB_B357(RW)`|
|`Classifier input counter pkt`|`0x9_3136(RW)`|
|`Classifier input counter pkt`|`0x9_3138(RW)`|
|`PDH Mux Ethernet Byte Counter`|`0x9_3120(RW)`|
|`PDH Mux Ethernet Byte Counter`|`0x9_3122(RW)`|
|`PDH Mux Ethernet Packet Counter`|`0x9_3126(RW)`|
|`PDH Mux Ethernet Packet Counter`|`0x9_3128(RW)`|
|`DE1 TDM bit counter`|`0x9_3000-0x9_307F(RW)`|
|`DE3 TDM bit counter`|`0x9_3100-0x9_311F(RW)`|
|`MACGE tx type packet`|`0x9_3080(RW)`|
|`MACGE tx type packet`|`0x9_3081(RW)`|
|`MACGE tx type packet`|`0x9_3087(RW)`|
|`MACGE tx type packet`|`0x9_3088(RW)`|
|`MACGE rx type packet`|`0x9_3090(RW)`|
|`MACGE rx type packet`|`0x9_3091(RW)`|
|`MACGE rx type packet`|`0x9_3097(RW)`|
|`MACGE rx type packet`|`0x9_3098(RW)`|
|`MACTGE tx type packet`|`0x9_30A0(RW)`|
|`MACTGE tx type packet`|`0x9_30A1(RW)`|
|`MACTGE tx type packet`|`0x9_30A7(RW)`|
|`MACTGE tx type packet`|`0x9_30A8(RW)`|
|`MACTGE rx type packet`|`0x9_30B0(RW)`|
|`MACTGE rx type packet`|`0x9_30B1(RW)`|
|`MACTGE rx type packet`|`0x9_30B7(RW)`|
|`MACTGE rx type packet`|`0x9_30B8(RW)`|
|`MACDIM tx type packet`|`0x9_30C0(RW)`|
|`MACDIM tx type packet`|`0x9_30C1(RW)`|
|`MACDIM tx type packet`|`0x9_30C7(RW)`|
|`MACDIM tx type packet`|`0x9_30C8(RW)`|
|`MACDIM rx type packet`|`0x9_30D0(RW)`|
|`MACDIM rx type packet`|`0x9_30D1(RW)`|
|`MACDIM rx type packet`|`0x9_30D7(RW)`|
|`MACDIM rx type packet`|`0x9_30D8(RW)`|
|`MAC2_5 tx type packet`|`0x9_30E0(RW)`|
|`MAC2_5 tx type packet`|`0x9_30E1(RW)`|
|`MAC2_5 tx type packet`|`0x9_30E7(RW)`|
|`MAC2_5 tx type packet`|`0x9_30E8(RW)`|
|`MAC2_5 rx type packet`|`0x9_30F0(RW)`|
|`MAC2_5 rx type packet`|`0x9_30F1(RW)`|
|`MAC2_5 rx type packet`|`0x9_30F7(RW)`|
|`MAC2_5 rx type packet`|`0x9_30F8(RW)`|


###Pseudowire Transmit Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_info1_rw`

* **Address**             : `0x0_0000-0x0_0BFF(RW)`

* **Formula**             : `0x0_0000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`txpwcnt`| in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : txpkt in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : unused| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_info1_rw`

* **Address**             : `0x0_8000-0x0_8BFF(RW)`

* **Formula**             : `0x0_8000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`txpwcnt`| in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : txpkt in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : unused| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire TX Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_info0`

* **Address**             : `0x8_0000-0x8_03FF (RW)`

* **Formula**             : `0x8_0000 + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:00]`|`pwcntbyte`| txpwcntbyte| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info0_rw`

* **Address**             : `0x1_0000-0x1_03FF (RW)`

* **Formula**             : `0x1_0000 + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of rxpwcnt_info0_cnt1 side: + rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info0_rw`

* **Address**             : `0x1_4000-0x1_43FF (RW)`

* **Formula**             : `0x1_4000 + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of rxpwcnt_info0_cnt1 side: + rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info1_rw`

* **Address**             : `0x2_0000-0x2_0BFF(RW)`

* **Formula**             : `0x2_0000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxstray in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxpbit + offset = 1 : rxlbit + offset = 2 : rxmalform| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info1_rw`

* **Address**             : `0x2_8000-0x2_8BFF(RW)`

* **Formula**             : `0x2_8000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxstray in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxpbit + offset = 1 : rxlbit + offset = 2 : rxmalform| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info2

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info2_rw`

* **Address**             : `0x3_0000-0x3_0BFF(RW)`

* **Formula**             : `0x3_0000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info1`| in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxunderrun + offset = 1 : rxlops + offset = 2 : rxearly in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxoverrun + offset = 1 : rxlost + offset = 2 : rxlate| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info2

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info2_rw`

* **Address**             : `0x3_8000-0x3_8BFF(RW)`

* **Formula**             : `0x3_8000 + 1024*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info1`| in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxunderrun + offset = 1 : rxlops + offset = 2 : rxearly in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxoverrun + offset = 1 : rxlost + offset = 2 : rxlate| `RW`| `0x0`| `0x0 End: Begin:`|

###PMR Error Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_pmr_cnt0_rw`

* **Address**             : `0x9_1800-0x9_182F(RW)`

* **Formula**             : `0x9_1800 + 16*$offset + $lineid`

* **Where**               : 

    * `$offset(0-2)`

    * `$lineid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[31:00]`|`pmr_err_cnt`| in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 : rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err| `RW`| `0x0`| `0x0 End: Begin:`|

###PMR Error Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_pmr_cnt0_rw`

* **Address**             : `0x9_1840-0x9_186F(RW)`

* **Formula**             : `0x9_1840 + 16*$offset + $lineid`

* **Where**               : 

    * `$offset(0-2)`

    * `$lineid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[31:00]`|`pmr_err_cnt`| in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 : rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Path STS Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_sts_pohpm_rw`

* **Address**             : `0x9_2000-0x9_21FF (RW)`

* **Formula**             : `0x9_2000 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_vt_cnt`| in case of pohpath_cnt0 side: + sts_rei in case of pohpath_cnt1 side: + sts_bip(B3)| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Path STS Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_sts_pohpm_rw`

* **Address**             : `0x9_2200-0x9_23FF (RW)`

* **Formula**             : `0x9_2200 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_vt_cnt`| in case of pohpath_cnt0 side: + sts_rei in case of pohpath_cnt1 side: + sts_bip(B3)| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Path VT Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_vt_pohpm_rw`

* **Address**             : `0x7_0000 - 0x7_3FFF(RW)`

* **Formula**             : `0x7_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_vt_cnt`| in case of pohpath_cnt0 side: + vt_rei in case of pohpath_cnt1 side: + vt_bip| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Path VT Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_vt_pohpm_rw`

* **Address**             : `0x7_4000 - 0x7_7FFF(RW)`

* **Formula**             : `0x7_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_vt_cnt`| in case of pohpath_cnt0 side: + vt_rei in case of pohpath_cnt1 side: + vt_bip| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter 0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt0_rw`

* **Address**             : `0x5_0000-0x5_3FFF(RW)`

* **Formula**             : `0x5_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + pohtx_vt_dec in case of poh_vt_cnt1 side: + pohtx_vt_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter 0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt0_rw`

* **Address**             : `0x5_4000-0x5_7FFF(RW)`

* **Formula**             : `0x5_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + pohtx_vt_dec in case of poh_vt_cnt1 side: + pohtx_vt_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter 1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt1_rw`

* **Address**             : `0x6_0000-0x6_3FFF(RW)`

* **Formula**             : `0x6_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + pohrx_vt_dec in case of poh_vt_cnt1 side: + pohrx_vt_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter 1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt1_rw`

* **Address**             : `0x6_4000-0x6_7FFF(RW)`

* **Formula**             : `0x6_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + pohrx_vt_dec in case of poh_vt_cnt1 side: + pohrx_vt_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt0_rw`

* **Address**             : `0x9_0800-0x9_09FF(RW)`

* **Formula**             : `0x9_0800 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_sts_cnt`| in case of poh_vt_cnt0 side: + pohtx_sts_dec in case of poh_vt_cnt1 side: + pohtx_sts_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt0_rw`

* **Address**             : `0x9_0A00-0x9_0BFF(RW)`

* **Formula**             : `0x9_0A00 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_sts_cnt`| in case of poh_vt_cnt0 side: + pohtx_sts_dec in case of poh_vt_cnt1 side: + pohtx_sts_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt1_rw`

* **Address**             : `0x9_1000-0x9_11FF(RW)`

* **Formula**             : `0x9_1000 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_sts_cnt`| in case of poh_vt_cnt0 side: + pohrx_sts_dec in case of poh_vt_cnt1 side: + pohrx_sts_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt1_rw`

* **Address**             : `0x9_1200-0x9_13FF(RW)`

* **Formula**             : `0x9_1200 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_sts_cnt`| in case of poh_vt_cnt0 side: + pohrx_sts_dec in case of poh_vt_cnt1 side: + pohrx_sts_inc| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 cntval Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de1cntval30_rw`

* **Address**             : `0x4_0000-0x4_3FFF(RW)`

* **Formula**             : `0x4_0000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`ds1_cntval_bus1_cnt`| (rei/fe/crc)| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 cntval Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de1cntval30_rw`

* **Address**             : `0x4_4000-0x4_7FFF(RW)`

* **Formula**             : `0x4_4000 + 256*$stsid + 32*$vtgid + 8*vtid + slcid`

* **Where**               : 

    * `$stsid(0-48)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`ds1_cntval_bus1_cnt`| (rei/fe/crc)| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds3 cntval Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de3cnt0_rw`

* **Address**             : `0x9_0000-0x9_01FF(RW)`

* **Formula**             : `0x9_0000 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`ds3_cntval_cnt`| (cb/pb/rei/fe)| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds3 cntval Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de3cnt0_rw`

* **Address**             : `0x9_0200-0x9_03FF(RW)`

* **Formula**             : `0x9_0200 + 8*$stsid + $slcid`

* **Where**               : 

    * `$stsid(0-47)`

    * `$slcid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`ds3_cntval_cnt`| (cb/pb/rei/fe)| `RW`| `0x0`| `0x0 End: Begin:`|

###CLA Counter00

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cla0_rw`

* **Address**             : `0x3_B321(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cla_err`| cla_err| `RW`| `0x0`| `0x0 End: Begin:`|

###CLA Counter00

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cla0_rw`

* **Address**             : `0x3_B323(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cla_err`| cla_err| `RW`| `0x0`| `0x0 End: Begin:`|

###ETH TX Packet OAM CNT

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cnttxoam`

* **Address**             : `0x9_3134(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxoamt`| TX Packet OAM CNT| `RW`| `0x0`| `0x0 End: Begin:`|

###Classifier Bytes Counter vld

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_bcnt_rw`

* **Address**             : `0x9_3140-0x9_3143(RW)`

* **Formula**             : `0x9_3140 + $clsid`

* **Where**               : 

    * `$clsid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`bytecnt`| Classifier bytecnt| `RW`| `0x0`| `0x0 End: Begin:`|

###Classifier Bytes Counter vld

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_bcnt_rw`

* **Address**             : `0x9_3160-0x9_3163(RW)`

* **Formula**             : `0x9_3160 + $clsid`

* **Where**               : 

    * `$clsid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`bytecnt`| Classifier bytecnt| `RW`| `0x0`| `0x0 End: Begin:`|

###Classifier output counter pkt0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_opcnt0_rw`

* **Address**             : `0x3_B34C-0x3_B34F(RW)`

* **Formula**             : `0x3_B34C + $offset`

* **Where**               : 

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`opktcnt0_opktcnt1_`| in case of opktcnt0 + offset = 0 : DS1/E1 good pkt counter + offset = 1 : Control pkt good counter + offset = 2 : FCs err cnt in case of opktcnt1 + offset = 0 : DS3/E3/EC1 good pkt counter + offset = 1 : Sequence err  counter + offset = 2 : len pkt err cnt| `RW`| `0x0`| `0x0 End: Begin:`|

###Classifier output counter pkt0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_opcnt0_rw`

* **Address**             : `0xB_B34C-0xB_B34F(RW)`

* **Formula**             : `0xB_B34C + $offset`

* **Where**               : 

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`opktcnt0_opktcnt1_`| in case of opktcnt0 + offset = 0 : DS1/E1 good pkt counter + offset = 1 : Control pkt good counter + offset = 2 : FCs err cnt in case of opktcnt1 + offset = 0 : DS3/E3/EC1 good pkt counter + offset = 1 : Sequence err  counter + offset = 2 : len pkt err cnt| `RW`| `0x0`| `0x0 End: Begin:`|

###Classifier output counter pkt1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_opcnt1_rw`

* **Address**             : `0x3_B354-0x3_B357(RW)`

* **Formula**             : `0x3_B354 + $offset`

* **Where**               : 

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`opktcnt0_opktcnt1_`| in case of opktcnt0 + offset = 0 : DA error pkt counter + offset = 1 : E-type err pkt counter + offset = 2 : Unused in case of opktcnt1 + offset = 0 : SA error pkt counter + offset = 1 : Length field err counter + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###Classifier output counter pkt1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_opcnt1_rw`

* **Address**             : `0xB_B354-0xB_B357(RW)`

* **Formula**             : `0xB_B354 + $offset`

* **Where**               : 

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`opktcnt0_opktcnt1_`| in case of opktcnt0 + offset = 0 : DA error pkt counter + offset = 1 : E-type err pkt counter + offset = 2 : Unused in case of opktcnt1 + offset = 0 : SA error pkt counter + offset = 1 : Length field err counter + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###Classifier input counter pkt

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_ipcnt_rw`

* **Address**             : `0x9_3136(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ipktcnt`| input packet cnt| `RW`| `0x0`| `0x0 End: Begin:`|

###Classifier input counter pkt

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_cls_ipcnt_rw`

* **Address**             : `0x9_3138(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ipktcnt`| input packet cnt| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH Mux Ethernet Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_eth_bytecnt_rw`

* **Address**             : `0x9_3120(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`eth_bytecnt`| ETH Byte Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH Mux Ethernet Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_eth_bytecnt_rw`

* **Address**             : `0x9_3122(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`eth_bytecnt`| ETH Byte Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH Mux Ethernet Packet Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_eth_bytecnt_rw`

* **Address**             : `0x9_3126(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`eth_pktcnt`| ETH Packet Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH Mux Ethernet Packet Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_eth_bytecnt_rw`

* **Address**             : `0x9_3128(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`eth_pktcnt`| ETH Packet Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###DE1 TDM bit counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_de1_tdmvld_cnt`

* **Address**             : `0x9_3000-0x9_307F(RW)`

* **Formula**             : `0x9_3000 + $lid`

* **Where**               : 

    * `$lid(0-127)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`de1_bit_cnt`| bit_cnt| `RW`| `0x0`| `0x0 End: Begin:`|

###DE3 TDM bit counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_de3_tdmvld_cnt`

* **Address**             : `0x9_3100-0x9_311F(RW)`

* **Formula**             : `0x9_3100 + $lid`

* **Where**               : 

    * `$lid(0-31)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`de3_bit_cnt`| bit_cnt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACGE tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge00lltx_typepkt_rw`

* **Address**             : `0x9_3080(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txpkt`| txpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACGE tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge00lltx_typepkt_rw`

* **Address**             : `0x9_3081(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txpkt`| txpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACGE tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge00lltx_pkt`

* **Address**             : `0x9_3087(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxpkt`| tx Packet Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MACGE tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge00lltx_byte`

* **Address**             : `0x9_3088(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxbyte`| tx Byte Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MACGE rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge00llrx_typepkt_rw`

* **Address**             : `0x9_3090(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxpkt`| rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACGE rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge00llrx_typepkt_rw`

* **Address**             : `0x9_3091(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxpkt`| rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACGE rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge00llrx_pkt`

* **Address**             : `0x9_3097(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntrxpkt`| rx Packet Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MACGE rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge00llrx_byte`

* **Address**             : `0x9_3098(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntrxbyte`| rx Byte Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MACTGE tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge10lltx_typepkt_rw`

* **Address**             : `0x9_30A0(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txpkt`| txpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACTGE tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge10lltx_typepkt_rw`

* **Address**             : `0x9_30A1(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txpkt`| txpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACTGE tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge10lltx_pkt`

* **Address**             : `0x9_30A7(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxpkt`| tx Packet Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MACTGE tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge10lltx_byte`

* **Address**             : `0x9_30A8(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxbyte`| tx Byte Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MACTGE rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge10llrx_typepkt_rw`

* **Address**             : `0x9_30B0(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxpkt`| rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACTGE rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge10llrx_typepkt_rw`

* **Address**             : `0x9_30B1(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxpkt`| rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACTGE rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge10llrx_pkt`

* **Address**             : `0x9_30B7(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntrxpkt`| rx Packet Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MACTGE rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge10llrx_byte`

* **Address**             : `0x9_30B8(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntrxbyte`| rx Byte Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MACDIM tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_dimtx_typepkt_rw`

* **Address**             : `0x9_30C0(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txpkt`| txpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACDIM tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_dimtx_typepkt_rw`

* **Address**             : `0x9_30C1(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txpkt`| txpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACDIM tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_dimtx_pkt`

* **Address**             : `0x9_30C7(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxpkt`| tx Packet Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MACDIM tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_dimtx_byte`

* **Address**             : `0x9_30C8(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxbyte`| tx Byte Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MACDIM rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_dimrx_typepkt_rw`

* **Address**             : `0x9_30D0(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxpkt`| rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACDIM rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_dimrx_typepkt_rw`

* **Address**             : `0x9_30D1(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxpkt`| rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MACDIM rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_dimrx_pkt`

* **Address**             : `0x9_30D7(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntrxpkt`| rx Packet Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MACDIM rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_dimrx_byte`

* **Address**             : `0x9_30D8(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntrxbyte`| rx Byte Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MAC2_5 tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_mac2_5tx_typepkt_rw`

* **Address**             : `0x9_30E0(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txpkt`| txpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MAC2_5 tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_mac2_5tx_typepkt_rw`

* **Address**             : `0x9_30E1(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txpkt`| txpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MAC2_5 tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_mac2_5tx_pkt`

* **Address**             : `0x9_30E7(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxpkt`| tx Packet Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MAC2_5 tx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_mac2_5tx_byte`

* **Address**             : `0x9_30E8(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnttxbyte`| tx Byte Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MAC2_5 rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_mac2_5rx_typepkt_rw`

* **Address**             : `0x9_30F0(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxpkt`| rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MAC2_5 rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_mac2_5rx_typepkt_rw`

* **Address**             : `0x9_30F1(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxpkt`| rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###MAC2_5 rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_ge2_5rx_pkt`

* **Address**             : `0x9_30F7(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntrxpkt`| rx Packet Counter| `RW`| `0x0`| `0x0 End: Begin:`|

###MAC2_5 rx type packet

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_mac2_5rx_byte`

* **Address**             : `0x9_30F8(RW)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntrxbyte`| rx Byte Counter| `RW`| `0x0`| `0x0 End:`|

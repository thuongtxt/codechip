## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_GLBPMC_ver3
####Register Table

|Name|Address|
|-----|-----|
|`Pseudowire Transmit Counter`|`0x0_0000-0x0_03FF(RW)`|
|`Pseudowire Transmit Counter`|`0x0_0800-0x0_0FFF(RW)`|
|`Pseudowire TX Byte Counter`|`0x8_0000-0x8_03FF (RW)`|
|`Pseudowire Receiver Counter Info0`|`0x1_0000-0x1_03FF (RW)`|
|`Pseudowire Receiver Counter Info0`|`0x1_0800-0x1_BFF (RW)`|


###Pseudowire Transmit Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_info1_rw`

* **Address**             : `0x0_0000-0x0_03FF(RW)`

* **Formula**             : `0x0_0000 + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`txpwcnt`| txpwcnt| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_info1_rw`

* **Address**             : `0x0_0800-0x0_0FFF(RW)`

* **Formula**             : `0x0_0800 + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`txpwcnt`| txpwcnt| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire TX Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_info0`

* **Address**             : `0x8_0000-0x8_03FF (RW)`

* **Formula**             : `0x8_0000 + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:00]`|`pwcntbyte`| txpwcntbyte| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info0_rw`

* **Address**             : `0x1_0000-0x1_03FF (RW)`

* **Formula**             : `0x1_0000 + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of rxpwcnt_info0_cnt1 side: + rxpkt| `RW`| `0x0`| `0x0 End:`|

###Pseudowire Receiver Counter Info0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info0_rw`

* **Address**             : `0x1_0800-0x1_BFF (RW)`

* **Formula**             : `0x1_0800 + $pwid`

* **Where**               : 

    * `$pwid(0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + rxcntbyte in case of rxpwcnt_info0_cnt1 side: + rxpkt| `RW`| `0x0`| `0x0 End:`|

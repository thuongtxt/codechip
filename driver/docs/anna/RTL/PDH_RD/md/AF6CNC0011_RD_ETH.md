## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_ETH
####Register Table

|Name|Address|
|-----|-----|
|`IP Ethernet Triple Speed Global Version`|`0x00`|
|`IP Ethernet Triple Speed Global Control`|`0x01`|
|`IP Ethernet Triple Speed Global Interrupt Type Request`|`0x02`|
|`IP Ethernet Triple Speed Global Interrupt Status`|`0x03`|
|`IP Ethernet Triple Speed Global Interface Control`|`0x04`|
|`IP Ethernet Triple Speed Global User Request Status`|`0x08`|
|`IP Ethernet Triple Speed PCS Control`|`0x20`|
|`IP Ethernet Triple Speed PCS Status`|`0x21`|
|`IP Ethernet Triple Speed PCS PHY Identifier 0`|`0x22`|
|`IP Ethernet Triple Speed PCS PHY Identifier 1`|`0x23`|
|`IP Ethernet Triple Speed PCS Advertisement`|`0x24`|
|`IP Ethernet Triple Speed PCS Link Partner Ability Base`|`0x25`|
|`IP Ethernet Triple Speed PCS An Expansion`|`0x26`|
|`IP Ethernet Triple Speed PCS An Next Page Transmitter`|`0x27`|
|`IP Ethernet Triple Speed PCS An Next Page Receiver`|`0x28`|
|`IP Ethernet Triple Speed PCS Extended Status`|`0x2F`|
|`IP Ethernet Triple Speed PCS Option Control`|`0x32`|
|`IP Ethernet Triple Speed PCS Option Status`|`0x33`|
|`IP Ethernet Triple Speed PCS Option FSM Status 0`|`0x34`|
|`IP Ethernet Triple Speed PCS Option FSM Status 1`|`0x35`|
|`IP Ethernet Triple Speed PCS Option Link Timer`|`0x38`|
|`IP Ethernet Triple Speed PCS Option Sync Timer`|`0x39`|
|`IP Ethernet Triple Speed PCS Option PRBS Test Control`|`0x3C`|
|`IP Ethernet Triple Speed PCS Option PRBS Test Status`|`0x3D`|
|`IP Ethernet Triple Speed Simple MAC Active Control`|`0x40`|
|`IP Ethernet Triple Speed Simple MAC Receiver Control`|`0x42`|
|`IP Ethernet Triple Speed Simple MAC Transmitter Control`|`0x42 0x43`|
|`IP Ethernet Triple Speed Simple MAC Data Inter Frame`|`0x45`|
|`IP Ethernet Triple Speed Simple MAC flow Control Time Interval`|`0x46`|
|`IP Ethernet Triple Speed Simple MAC flow Control Quanta`|`0x47`|
|`IP Ethernet Triple Speed Simple MAC Interrupt Status`|`0x48`|
|`IP Ethernet Triple Speed Simple MAC Latch Status`|`0x49`|
|`ETH FCS Force Error Control`|`0x0001007`|


###IP Ethernet Triple Speed Global Version

* **Description**           

This register is used to indicate version


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Global_Version`

* **Address**             : `0x00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:8]`|`ipethtripglbvendor`| | `RO`| `0xAE`| `0xAE`|
|`[7:0]`|`ipethtripglbverionid`| | `RO`| `0x10`| `0x10 End: Begin:`|

###IP Ethernet Triple Speed Global Control

* **Description**           

This register is global Control


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Global_Control`

* **Address**             : `0x01`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[10:8]`|`ipethtripglbctlelfifodepth`| Elastic FIFO Depth, Only used to NONE PCS| `RW`| `0x0`| `0x0`|
|`[6]`|`ipethtripglbctlspstable`| Software must write 0x1| `RW`| `0x0`| `0x0`|
|`[5]`|`ipethtripglbctlspisphy`| 1: Enable SMAC auto update speed operation by PHY device (or PCS) 0: Disable| `RW`| `0x0`| `0x0`|
|`[4]`|`ipethtripcfgctlselect`| 1: Enable engine used con-fig interface by register IP Ethernet Triple Speed Global Interface Control 0: Disable| `RW`| `0x0`| `0x0`|
|`[2]`|`ipethtripglbctlflush`| Software reset 1: Software Reset 0: Normal| `RW`| `0x0`| `0x0`|
|`[1]`|`ipethtripglbctlinten`| Interrupt Enable 1: Enable Interrupt 0: Disable Interrupt| `RW`| `0x0`| `0x0`|
|`[0]`|`ipethtripglbctlactive`| Port Active 1: Active 0: No Active| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed Global Interrupt Type Request

* **Description**           

This register is global interrupt type request


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Global_Interrupt_Type_Request`

* **Address**             : `0x02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3]`|`ipethtripglbinttypmdio`| MDIO interrupt request 1: SMAC interrupt request 0: SMAC no request| `RW`| `0x0`| `0x0`|
|`[2]`|`ipethtripglbinttypsmac`| Simple MAC interrupt request 1: SMAC interrupt request 0: SMAC no request| `RW`| `0x0`| `0x0`|
|`[1]`|`ipethtripglbinttypglb`| Global interrupt request 1: Global interrupt request 0: Global no request| `RW`| `0x0`| `0x0`|
|`[0]`|`ipethtripglbinttyppcs`| PCS interrupt request 1: PCS interrupt request 0: PCS no request| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed Global Interrupt Status

* **Description**           

This register is global status


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Global_Interrupt_Status`

* **Address**             : `0x03`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`ipethtripglbstatxifer`| Transmitter Interface check error occur| `RW`| `0x0`| `0x0`|
|`[5]`|`ipethtripglbstarxifover`| Receiver Interface Rate match FIFO overflow occur| `RW`| `0x0`| `0x0`|
|`[4]`|`ipethtripglbstarxifunder`| Receiver Interface Rate match FIFO underflow occur| `RW`| `0x0`| `0x0`|
|`[3]`|`ipethtripglbstarxffrder`| Receiver User FIFO convert clock domain read errror occur| `RW`| `0x0`| `0x0`|
|`[2]`|`ipethtripglbstatxffwrer`| Transmitter User FIFO convert clock domain write error occur| `RW`| `0x0`| `0x0`|
|`[1]`|`ipethtripglbstatxshrder`| Transmitter User shift engine read error assert| `RW`| `0x0`| `0x0`|
|`[0]`|`ipethtripglbstatxshwrer`| Transmitter User shift engine write error assert| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed Global Interface Control

* **Description**           

This register is Interface Control %%

Ethernet interface will description below %%

TBI RGMII SGMII Functional %%

1 0 0 - TBI ( GMII) <--> PCS 1000BaseX <--> SMAC <---> USER speed 1000Mbps - rxclk (125Mhz), gtxclk (125Mhz) %%

1 0 1 - TBI ( GMII) <--> PCS SGMII <--> SMAC <---> USER speed 10/100/1000Mbps - rxclk (125Mhz), gtxclk (125Mhz) %%

1 1 0 - TBI (RGMII) <--> PCS 1000BaseX <--> SMAC <---> USER speed 1000Mbps - rxclk (125Mhz), gtxclk (125Mhz) %%

1 1 1 - TBI (RGMII) <--> PCS SGMII <--> SMAC <---> USER speed 10/100/1000Mbps - rxclk (125Mhz), gtxclk (125Mhz) %%

0 0 X - GMII/MII <------------------> SMAC <---> USER speed 10/100/1000Mbps - rxclk (2.5/25/125Mhz), gtxclk (2.5/25/125Mhz) %%

0 1 X - RGMII <------------------> SMAC <---> USER speed 10/100/1000Mbps - rxclk (2.5/25/125Mhz), gtxclk (2.5/25/125Mhz)


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Global_Interface_Control`

* **Address**             : `0x04`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14]`|`ipethtripglbstargmii`| RGMII interface Status 0x1: RGMII Active 0x0: GMII/MII Active| `RW`| `0x0`| `0x0`|
|`[13]`|`ipethtripglbstasgmii`| SGMII interface status 0x1: PCS-SGMII 0x0: PCS-1000Base-X| `RW`| `0x0`| `0x0`|
|`[12]`|`ipethtripglbstatbi`| TBI interface status 0x1:  TBI interface Acitve (include PCS 1000Base-X or SGMII) 0x0:  None PCS| `RW`| `0x0`| `0x0`|
|`[9:8]`|`ipethtripglbstaspeed`| Speed operation status 0x0: 10Mbps 0x1: 100Mbps 0x2: 1000Mbps 0x3: Unused| `RW`| `0x0`| `0x0`|
|`[6]`|`ipethtripglbcfgrgmii`| RGMII interface Active 0x1: RGMII Active 0x0: GMII Active| `RW`| `0x0`| `0x0`|
|`[5]`|`ipethtripglbcfgsgmii`| SGMII interface Active 0x1: PCS-SGMII 0x0: PCS-1000Base-X| `RW`| `0x0`| `0x0`|
|`[4]`|`ipethtripglbcfgtbi`| TBI interface 0x1:  TBI interface Acitve (include PCS 1000Base-X or SGMII) 0x0:  None PCS| `RW`| `0x0`| `0x0`|
|`[1:0]`|`ipethtripglbcfgspeed`| Speed MAC operation 0x0: 10Mbps 0x1: 100Mbps 0x2: 1000Mbps 0x3: Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed Global User Request Status

* **Description**           

This register is User status, Used to HW debug


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Global_User_Request_Status`

* **Address**             : `0x08`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5]`|`ipethtripglbstausrrxvl`| | `RW`| `0x0`| `0x0`|
|`[4]`|`ipethtripglbstausrtxvl`| | `RW`| `0x0`| `0x0`|
|`[3:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`ipethtripglbstapaugen`| | `RW`| `0x0`| `0x0`|
|`[0]`|`ipethtripglbstausrtxenb`| | `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS Control

* **Description**           

This register is PCS Control


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Control`

* **Address**             : `0x20`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`ipethtrippcsreset`| 1: Core Reset request 0: Normal Operation| `RW`| `0x0`| `0x0`|
|`[14]`|`ipethtrippcsloopback`| 1: Enable Line Loop in (LVDS) 0: No loop| `RW`| `0x0`| `0x0`|
|`[13]`|`ipethtrippcsspeedlsb`| Speed Selection (LSB)| `RW`| `0x0`| `0x0`|
|`[12]`|`ipethtrippcsanen`| 1: Enable Auto-Negotiation process 0: Disable Auto-Negotiation process| `RW`| `0x0`| `0x0`|
|`[11]`|`ipethtrippcspowerdown`| 1: Power down, Force Lost of Signal 0: Normal Operation| `RW`| `0x0`| `0x0`|
|`[10]`|`ipethtrippcsisolate`| 1: Electrically Isolate PCS from GMII 0: Normal Operation| `RW`| `0x0`| `0x0`|
|`[9]`|`ipethtrippcsrestartan`| 1: Restart Auto-Negotiation Process 0: Normal Operation| `RW`| `0x0`| `0x0`|
|`[8]`|`ipethtrippcsfullduplex`| 1: Full-Duplex Mode 0: Haft-duplex Mode| `RW`| `0x0`| `0x0`|
|`[7]`|`ipethtrippcscollisiontest`| 1: Enable Collision test 0: Disable Collision test| `RW`| `0x0`| `0x0`|
|`[6]`|`ipethtrippcsspeedmsb`| Speed Selection (MSB) Case:<br>{IpEthTripPcsSpeedMsb[0], IpEthTripPcsSpeedLsb[0]} 0: Speed 10Mbps 1: Speed 100Mbps 2: Speed 1000Mbps 3: Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS Status

* **Description**           

This register is PCS Status


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Status`

* **Address**             : `0x21`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[5]`|`ipethtrippcsstaautonegcomplete`| 1: Auto-Negotiation process completed 0: Auto-Negotiation process not completed| `RW`| `0x0`| `0x0`|
|`[4]`|`ipethtrippcsstaremotefault`| 1: Remote fault condition detected 0: No remote fault condition detected| `RW`| `0x0`| `0x0`|
|`[2]`|`ipethtrippcsstalinkstatus`| 1: Link is up 0: Link is down| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS PHY Identifier 0

* **Description**           

This register is PHY Identifier 0


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_PHY_Identifier_0`

* **Address**             : `0x22`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`value`| | `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS PHY Identifier 1

* **Description**           

This register is PHY Identifier 1


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_PHY_Identifier_1`

* **Address**             : `0x23`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`ipethtrippcsvendor21_7`| | `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS Advertisement

* **Description**           

This register is Auto-Negotiation Advertisement


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Advertisement`

* **Address**             : `0x24`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`ipethtrippcsanadvnextpage`| 1 : Next Page functionality is supported 0 : Next Page functionality is not supported| `RW`| `0x0`| `0x0`|
|`[13:12]`|`ipethtrippcsanadvremotefault`| Remote Fault, self clearing to 0x0 after auto-negotiation 0 : No Error 1 : Offline 2 : Link Failure 3 : Auto-Negotiation Error| `RW`| `0x0`| `0x0`|
|`[8:7]`|`ipethtrippcsanadvpause`| 0 : No PAUSE 1 : Symmetric PAUSE 2 : Asymmetric PAUSE towards link partner 3 : Both Symmetric PAUSE and Asymmetric PAUSE supported| `RW`| `0x0`| `0x0`|
|`[6]`|`ipethtrippcsanadvhalfduplex`| 1 : Half Duplex Mode is supported 0 : Half Duplex Mode is not supported| `RW`| `0x0`| `0x0`|
|`[5]`|`ipethtrippcsanadvfullduplex`| 1 : Full Duplex Mode is supported 0 : Full Duplex Mode is not supported| `RW`| `0x0`| `0x0001 End: Begin:`|

###IP Ethernet Triple Speed PCS Link Partner Ability Base

* **Description**           

This register is Auto-Negotiation Link Partner Ability Base


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Link_Partner_Ability_Base`

* **Address**             : `0x25`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`ipethtrippcsanlpanextpage`| 1 : Next Page functionality is supported 0 : Next Page functionality is not supported| `RW`| `0x0`| `0x0`|
|`[14]`|`ipethtrippcsanlpaacknowledge`| Used by Auto-Negotiation function to indicate reception of a link partners base or next page| `RW`| `0x0`| `0x0`|
|`[13:9]`|`ipethtrippcsanlparemotefault`| Remote Fault 0 : No Error 1 : Offline 2 : Link Failure 3 : Auto-Negotiation Error| `RW`| `0x0`| `0x0`|
|`[8:7]`|`ipethtrippcsanlpapause`| 0 : No PAUSE 1 : Symmetric PAUSE 2 : Asymmetric PAUSE towards link partner 3 : Both Symmetric PAUSE and Asymmetric PAUSE supported| `RW`| `0x0`| `0x0`|
|`[6]`|`ipethtrippcsanlpahalfduplex`| 1 : Half Duplex Mode is supported 0 : Half Duplex Mode is not supported| `RW`| `0x0`| `0x0`|
|`[5]`|`ipethtrippcsanlpafullduplex`| 1 : Full Duplex Mode is supported 0 : Full Duplex Mode is not supported| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS An Expansion

* **Description**           

This register is Auto-Negotiation Expansion Register


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_An_Expansion`

* **Address**             : `0x26`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2]`|`ipethtrippcsanexpnextpageable`| Next Page Able| `RW`| `0x0`| `0x0`|
|`[1]`|`ipethtrippcsanexppagereceived`| Page Received 1 : A new page has been received 0 : A new page has not been received| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS An Next Page Transmitter

* **Description**           

This register is Auto-Negotiation Next Page Transmitter


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Transmitter`

* **Address**             : `0x27`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`ipethtrippcsannptxnextpage`| 1: Additional Next Page(s) will follow 0: Last page| `RW`| `0x0`| `0x0`|
|`[13]`|`ipethtrippcsannptxmessagepage`| Message Page 1: Message Page 0: Unformatted Page| `RW`| `0x0`| `0x0`|
|`[12]`|`ipethtrippcsannptxack`| Acknowledge 1: Comply with message 0: Cannot comply with message| `RW`| `0x0`| `0x0`|
|`[11]`|`ipethtrippcsannptxtoggle`| Value toggles between subsequent Next Page| `RW`| `0x0`| `0x0`|
|`[10:0]`|`ipethtrippcaannptxmeassge`| Message Code Field or Unformatted Page, (00000000001 is Null Message Code)| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS An Next Page Receiver

* **Description**           

This register is Auto-Negotiation Next Page Receiver


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_An_Next_Page_Receiver`

* **Address**             : `0x28`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`ipethtrippcsannprxnextpage`| 1: Additional Next Page(s) will follow 0: Last page| `RW`| `0x0`| `0x0`|
|`[14]`|`ipethtrippcsannprxack`| Acknowledge, Used by Auto-Negotiation function to indicate reception of a link partners base or next page| `RW`| `0x0`| `0x0`|
|`[13]`|`ipethtrippcsannprxmessagepage`| Message Page 1: Message Page 0: Unformatted Page| `RW`| `0x0`| `0x0`|
|`[12]`|`ipethtrippcsannprxack2`| Acknowledge2 1: Comply with message 0: Cannot comply with message| `RW`| `0x0`| `0x0`|
|`[11]`|`ipethtrippcsannprxtoggle`| Value toggles between subsequent Next Page| `RW`| `0x0`| `0x0`|
|`[10:0]`|`ipethtrippcaannprxmeassge`| Message Code Field or Unformatted Page| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS Extended Status

* **Description**           

This register is Extended Status


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Extended_Status`

* **Address**             : `0x2F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`ipethtrippcsex1000bxfdupx`| Support 1000Base_X full duplex 1: Supported 0: No Supported| `RW`| `0x0`| `0x0`|
|`[14]`|`ipethtrippcsex1000bxhdupx`| Support 1000Base_X halfl duplex 1: Supported 0: No Supported| `RW`| `0x0`| `0x0`|
|`[13]`|`ipethtrippcsex1000btfdupx`| Support 1000Base_T full duplex 1: Supported 0: No Supported| `RW`| `0x0`| `0x0`|
|`[12]`|`ipethtrippcsex1000bthdupx`| Support 1000Base_T halfl duplex 1: Supported 0: No Supported| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS Option Control

* **Description**           

This register is PCS Option Control


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Option_Control`

* **Address**             : `0x32`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`ipethtrippcsctlfifoadjc2`| Enable rate match FIFO used adjust C2 code word when Auto-Negotiation enable 0 : Disable 1 : Enable| `RW`| `0x0`| `0x0`|
|`[14:12]`|`ipethtrippcsctlfifodepth`| Rate match FIFO depth, support support up to SGMII speed 10Mbps jumbo frame 12K bytes 0 : +/- 4 words 1 : +/- 8 words 2 : +/- 16 words 3 : +/- 32 words 4 : +/- 64 words 5 : +/- 128 words 6 : +/- 256 words 7 : Unused| `RW`| `0x0`| `0x0`|
|`[11]`|`ipethtrippcsctlloopin`| Diagnostic, loop in enable 1 : Loop back in enable 0 : Normal Operations| `RW`| `0x0`| `0x0`|
|`[10]`|`ipethtrippcsctlloopout`| Diagnostic, loop out enable 1 : Loop back out enable 0 : Normal Operations| `RW`| `0x0`| `0x0`|
|`[9]`|`ipethtrippcsctltestdisp`| Value Disparity Test| `RW`| `0x0`| `0x0`|
|`[8]`|`ipethtrippcsctltestfdisp`| 1 : Enable Disparity Test 0 : Normal Operations| `RW`| `0x0`| `0x0`|
|`[7]`|`ipethtrippcsctltxswap`| Enable Swap output TBI data 1 :  Enable 0 :  Disable| `RW`| `0x0`| `0x0`|
|`[6]`|`ipethtrippcsctltxpolar`| Enable Invert output TBI data 1 :  Enable 0 :  Disable| `RW`| `0x0`| `0x0`|
|`[5]`|`ipethtrippcsctlrxswap`| Enable Swap input  TBI data 1 :  Enable 0 :  Disable| `RW`| `0x0`| `0x0`|
|`[4]`|`ipethtrippcsctlrxpolar`| Enable Invert input TBI data 1 :  Enable 0 :  Disable| `RW`| `0x0`| `0x0`|
|`[3]`|`ipethtrippcsctlsgmiisel`| Enable select SGMII mode when IpEthTripPcsCtlModSel assert 1 :  SGMII mode 0 :  1000BASE-X| `RW`| `0x0`| `0x0`|
|`[2]`|`ipethtrippcsctlmodsel`| Mode Interface select 1 :  Enable select sgmii/1000base_x depend on  IpEthTripPcsCtlSgmiiSel bit 0 :  Disable| `RW`| `0x0`| `0x0`|
|`[1]`|`ipethtrippcsintlinken`| Enable interrupt Link status 1 :  Enable 0 :  Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`ipethtrippcsintanen`| Enable interrupt Auto-Negotiation 1 :  Enable 0 :  Disable| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS Option Status

* **Description**           

This register is PCS Option Status


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Option_Status`

* **Address**             : `0x33`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7]`|`ipethtrippcsstafifounderflow`| Under flow elastic FIFO status 1: Under flow elastic FIFO occurred,  resolve by increase  Rate match FIFO depth at  IpEthTripPcsCtlFifoDepth[2:0] at IP Ethernet Triple Speed PCS Option Control register 0: Under flow elastic FIFO occurred| `RW`| `0x0`| `0x0`|
|`[6]`|`ipethtrippcsstafifooverflow`| Over flow elastic FIFO status 1: Over flow elastic FIFO occurred, resolve by increase  Rate match FIFO depth at  IpEthTripPcsCtlFifoDepth[2:0] at IP Ethernet Triple Speed PCS Option Control register 0: No over flow elastic FIFO occurred| `RW`| `0x0`| `0x0`|
|`[5]`|`ipethtrippcsstadisper`| Disparity violation 1: Disparity violation 0: Disparity no violation| `RW`| `0x0`| `0x0`|
|`[4]`|`ipethtrippcsstacodeer`| Code word invalid detection 1: Detect an code word invalid 0: No Code word invalid detection| `RW`| `0x0`| `0x0`|
|`[3]`|`ipethtrippcsstalinkfail`| Link fail status 1: Link down 0: Link up| `RW`| `0x0`| `0x0`|
|`[2]`|`ipethtrippcsstaancomplete`| Auto-Negotiation status 1: Auto-Neg complete 0: Auto-Neg Fail| `RW`| `0x0`| `0x0`|
|`[1]`|`ipethtrippcsintlinkreq`| Link interrupt request 1: Interrupt request 0: No Interrupt request| `RW`| `0x0`| `0x0`|
|`[0]`|`ipethtrippcsintanreq`| Auto-Negotiation interrupt request 1: Interrupt request 0: No Interrupt request| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS Option FSM Status 0

* **Description**           

This register is PCS FSM Current Status 0, used to HW debug


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_0`

* **Address**             : `0x34`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9:8]`|`ipethtrippcscurxmit`| Xmit current status (define at 802.3 section 3) 0x0 : XMIT_IDL 0x1 : XMIT_CFG 0x2 : XMIT_DAT| `RW`| `0x0`| `0x0`|
|`[7:4]`|`ipethtrippcscuranfsm`| Auto-Negotiation Current FSM 0x0 : AN_ENABLE 0x1 : AN_RESTART 0x2 : AN_DIS_LINK_OK 0x3 : ABILITY_DET 0x4 : ACK_DET 0x5 : NEXT_PAGE_WAIT 0x6 : COMPLETE_ACK 0x7 : DLE_DET 0x8 : LINK_OK| `RW`| `0x0`| `0x0`|
|`[3:0]`|`ipethtrippcscursyncfsm`| Synchronization current FSM 0x0 : LOSYNC 0x1 : COM_DET1 0x2 : ACQ_SYNC1 0x3 : COM_DET2 0x4 : ACQ_SYNC2 0x5 : ACQ_SYNC2 0x6 : SYNC_ACQ1 0x7 : SYNC_ACQ2 0x8 : SYNC_ACQ3 0x9 : SYNC_ACQ4 0xa : SYNC_ACQ2A 0xb : SYNC_ACQ3A 0xc : SYNC_ACQ3A| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS Option FSM Status 1

* **Description**           

This register is PCS FSM Current Status 1, only used to HW debug


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Option_FSM_Status_1`

* **Address**             : `0x35`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9:5]`|`ipethtrippcscurrxfsm`| Receiver Engine current FSM 0x00 :  LINK_FAIL 0x01 :  WAIT_FOR_K 0x02 :  RX_K 0x03 :  RX_CB 0x04 :  RX_CC 0x05 :  RX_CD 0x06 :  IDLE 0x07 :  CARRIER_DET 0x08 :  FALSE_CARR 0x09 :  RX_INVALID 0x0A :  START_OF_PACKET 0x0B :  RECEIVE 0x0C :  EARLY_END 0x0D :  TRI_RRI 0x0E :  TRR_EXTEND 0x0F :  RX_DATA_ERR 0x10 :   RX_DATA 0x11 :   EARLY_END_EXT 0x12 :   EPD2_CHECK_END 0x13 :   EXTEND_ERR| `RW`| `0x0`| `0x0`|
|`[4:0]`|`ipethtrippcscurtxfsm`| Transmitter Engine current FSM 0x00 :   CFG_C1A 0x01 :   CFG_C1B 0x02 :   CFG_C1C 0x03 :   CFG_C1D 0x04 :   CFG_C2A 0x05 :   CFG_C2B 0x06 :   CFG_C2C 0x07 :   CFG_C2D 0x08 :   IDLE_DISP 0x09 :   IDLE_I1B 0x0A :  IDLE_I2B 0x0B :  START_ERROR 0x0C :  TX_DATA_ERROR 0x0D :  START_OF_PACKET 0x0E :  TX_DATA 0x0F :   EOP_NOEXT 0x10 :   EOP_EXT 0x11 :   EPD2_NOEXT 0x12 :   EPD3 0x13 :   EXTEND_BY_1 0x14 :   CARR_EXT| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS Option Link Timer

* **Description**           

This register is PCS Link Timer, unit = 65535 ns ~ 65us


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Option_Link_Timer`

* **Address**             : `0x38`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:8]`|`ipethtrippcscfglinktimersgmii`| Auto-Negotiation cycle by the Link timer with SGMII standard. Default (0x19h * 65us ~ 1.6ms)| `RW`| `0x0`| `0x0`|
|`[7:0]`|`ipethtrippcscfglinktimerbasex`| Auto-Negotiation cycle by the Link timer with 1000 BASE-XI standard, Default  (0x99h * 65us ~ 10ms)| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS Option Sync Timer

* **Description**           

This register is PCS Sync Timer, unit = 65535 ns ~ 65us


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Option_Sync_Timer`

* **Address**             : `0x39`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `17`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16]`|`ipethtrippcscfgsyncdis`| Disable monitor sync| `RW`| `0x0`| `0x0`|
|`[15:8]`|`ipethtrippcscfgsynctimer`| standard. Default (0x7Fh * 65us ~ 8.25ms)| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS Option PRBS Test Control

* **Description**           

This register is PRBS test status


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Control`

* **Address**             : `0x3C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`ipethtrippcsctltestprbsen`| Enable PRBS test 1 : Enable 0 : Normal Operations| `RW`| `0x0`| `0x0`|
|`[14]`|`ipethtrippcsctltestalignen`| Enable PRBS monitor request align to SERDES 1 : Enable 0 : Disable| `RW`| `0x0`| `0x0`|
|`[13:4]`|`ipethtrippcstestfixdat`| Fix Data generation| `RW`| `0x0`| `0x0`|
|`[3:0]`|`ipethtrippcstestmode`| Test mode [0]: Set 1 PRBS15 Test, else Fix Pattern Test [3:2]: Force Error| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed PCS Option PRBS Test Status

* **Description**           

This register is PRBS test status


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_PCS_Option_PRBS_Test_Status`

* **Address**             : `0x3D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`ipethtrippcstestrxalign`| 0x0| `RW`| `0x0`| `0x0`|
|`[14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:4]`|`ipethtrippcstestcurrrxdata`| | `RW`| `0x0`| `0x0`|
|`[3]`|`ipethtrippcstestcurrprbs15er`| | `RW`| `0x0`| `0x0`|
|`[2]`|`ipethtrippcstestcurrfixdater`| | `RW`| `0x0`| `0x0`|
|`[1]`|`ipethtrippcstestlatchprbs15er`| 0x0| `RW`| `0x0`| `0x0`|
|`[0]`|`ipethtrippcstestlatchfixdater`| 0x0| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed Simple MAC Active Control

* **Description**           

This register is Active Control


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Simple_MAC_Active_Control`

* **Address**             : `0x40`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[5]`|`ipethtripsmacloopout`| Loop out Enable (TX ? RX) 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[4]`|`ipethtripsmactxactive`| Active transmitter side 1: Active 0: No Active| `RW`| `0x1`| `0x1`|
|`[1]`|`ipethtripsmacloopin`| Loop In Enable (TX ? RX) 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`ipethtripsmacrxactive`| Active receiver side 1: Active 0: No Active| `RW`| `0x1`| `0x1 End: Begin:`|

###IP Ethernet Triple Speed Simple MAC Receiver Control

* **Description**           

This register is Simple MAC Receiver Control


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Simple_MAC_Receiver_Control`

* **Address**             : `0x42`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:12]`|`ipethtripsmacrxmask`| Mask Error forward to user [3]: Ipg_Error: set 1 to Enable, else is disable [2]: Rx_Error: set 1 to Enable, else is disable [1]: Prm_Error: set 1 to Enable, else is disable [0]: Fcs_Error: set 1 to Enable, else is disable| `RW`| `0x0`| `0x0`|
|`[11:8]`|`ipethtripsmacrxipg`| Number of Inter packet Gap can detected for Receiver sides| `RW`| `0x0`| `0x0`|
|`[7:4]`|`ipethtripsmacrxprm`| Number of preamble bytes that receiver side can be received, this is maximum preamble bytes need to detect and one SFD byte| `RW`| `0x0`| `0x0`|
|`[3]`|`ipethtripsamcrxpausachk`| Enable Pause  frame terminate SA check 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[2]`|`ipethtripsamcrxpaudachk`| Enable Pause  frame terminate DA check 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[1]`|`ipethtripsmacrxpaudichk`| Disable Pause frame termination 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[0]`|`ipethtripsmacrxfcspass`| Enable pass 4 byte FCS 1: Enable 0: Disable| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed Simple MAC Transmitter Control

* **Description**           

This register is Simple MAC Transmitter Control


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Simple_MAC_Transmitter_Control`

* **Address**             : `0x42 0x43`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12:8]`|`ipethtripsmactxipg`| Number of Inter packet Gap generated for Transmitter sides. Value range of parameter is from 4 to 31| `RW`| `0x0`| `0x0`|
|`[7:4]`|`ipethtripsmactxprm`| Number of preamble bytes (not include SFD byte) that Transmitter generator.| `RW`| `0x0`| `0x0`|
|`[3]`|`ipethtripsmactxer`| Force TX_ER| `RW`| `0x0`| `0x0`|
|`[2]`|`ipethtripsamctxpaddis`| Enable PAD insertion 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[1]`|`ipethtripsmactxpaudis`| Enable Pause  frame generator 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[0]`|`ipethtripsmactxfcsins`| Enable FCS Insertion 1: Enable 0: Disable| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed Simple MAC Data Inter Frame

* **Description**           

This register is Simple MAC Data Inter Frame Control


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Simple_MAC_Data_Inter_Frame`

* **Address**             : `0x45`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:8]`|`ipethtripsmacidlerxdata`| Data Inter Frame Receive from PHY or PCS| `RW`| `0x0`| `0x0`|
|`[7:0]`|`ipethtripsmacidletxdata`| Data Inter Frame Transmit to PHY or PCS| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed Simple MAC flow Control Time Interval

* **Description**           

This register is Simple MAC the Time Interval, it used for engine flow control.


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Simple_MAC_flow_Control_Time_Interval`

* **Address**             : `0x46`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`ipethtripsmactimeinterval`| In an time interval, a pause frame with quanta value that is configured bel0, will be transmitted if received packet FIFO is full, unit value is 512 bits time or 64 bytes time| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed Simple MAC flow Control Quanta

* **Description**           

This register is Simple MAC the Quanta, it used for engine flow control.


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Simple_MAC_flow_Control_Quanta`

* **Address**             : `0x47`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`ipethtripsmacquanta`| Quanta value of pause ON frame, unit is 512 bits time or 64 bytes time| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed Simple MAC Interrupt Status

* **Description**           

This register is Interrupt status.


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Simple_MAC_Interrupt_Status`

* **Address**             : `0x48`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2]`|`ipethtripsmacviolinfo`| Information violation between SMAC and USER at TxSide| `RW`| `0x0`| `0x0`|
|`[1]`|`ipethtripsmaclostsop`| Lost SOP detection| `RW`| `0x0`| `0x0`|
|`[0]`|`ipethtripsmaclosteop`| Lost EOP detection or TX FIFO empty when packet transmitting| `RW`| `0x0`| `0x0 End: Begin:`|

###IP Ethernet Triple Speed Simple MAC Latch Status

* **Description**           

This register is latch status, Used to HW debug


* **RTL Instant Name**    : `IP_Ethernet_Triple_Speed_Simple_MAC_Latch_Status`

* **Address**             : `0x49`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10]`|`ipethtripsmaclatpaugen`| 0x0| `RW`| `0x0`| `0x0`|
|`[9]`|`ipethtripsmaclatusrrxvl`| 0x0| `RW`| `0x0`| `0x0`|
|`[8]`|`ipethtripsmaclatusrtxvl`| 0x0| `RW`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`ipethtripsmaclatsmactxrder`| 0x0| `RW`| `0x0`| `0x0`|
|`[5]`|`ipethtripsmaclatsmactxreq`| 0x0| `RW`| `0x0`| `0x0`|
|`[4]`|`ipethtripsmaclatsmactxvl`| 0x0| `RW`| `0x0`| `0x0`|
|`[3]`|`ipethtripsmaclatsmacrxer`| 0x0| `RW`| `0x0`| `0x0`|
|`[2]`|`ipethtripsmaclatsmacrxdv`| 0x0| `RW`| `0x0`| `0x0`|
|`[1]`|`ipethtripsmaclatsmactxer`| 0x0| `RW`| `0x0`| `0x0`|
|`[0]`|`ipethtripsmaclatsmactxen`| 0x0| `RW`| `0x0`| `0x0 End: Begin:`|

###ETH FCS Force Error Control

* **Description**           




* **RTL Instant Name**    : `ETH_FCS_errins_en_cfg`

* **Address**             : `0x0001007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[30:29]`|`ethfcserrmode`| 0: FCS (MAC); 1:Tx interface MAC2PCS; 2: Rx interface PCS2MAC| `RW`| `0x0`| `0x0`|
|`[28]`|`ethfcserrrate`| 0: One-shot with number of errors; 1:Line rate| `RW`| `0x0`| `0x0`|
|`[27:0]`|`ethfcserrthr`| Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1| `RW`| `0x0`| `0x0 End:`|

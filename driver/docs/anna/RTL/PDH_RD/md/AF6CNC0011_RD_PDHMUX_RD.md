## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_PDHMUX_RD
####Register Table

|Name|Address|
|-----|-----|
|`Active Regiter`|`0x0000`|
|`Version ID`|`0x0001`|
|`PDH to LIU Look-up Table`|`0x1000 - 0x13FF`|
|`LIU to PDH Look-up Table`|`0x1400 - 0x1453`|
|`NCO mode LIU to PDH`|`0x1500 - 0x1553`|
|`Ethernet Encapsulation DA bit47_32 Configuration`|`0x0002`|
|`Ethernet Encapsulation DA bit32_00 Configuration`|`0x0003`|
|`Ethernet Encapsulation SA bit47_32 Configuration`|`0x0004`|
|`Ethernet Encapsulation SA bit32_00 Configuration`|`0x0005`|
|`Ethernet Encapsulation EtheType and EtherLength  Configuration`|`0x0006`|
|`TX LIU FIFO Overrun Counter`|`0x0200 - 0x0253`|
|`TX LIU FIFO Underrun Counter`|`0x0280 - 0x02D3`|
|`RX PDH FIFO Overrun`|`0x0010`|
|`AXI4-Stream (PDH to MAC) Packet Counter`|`0x0013`|
|`AXI4-Stream (MAC to PDH) Byte Counter`|`0x0014`|
|`AXI4-Stream (PDH to MAC) Packet Counter`|`0x0015`|
|`AXI4-Stream (PDH to MAC) Byte Counter`|`0x0016`|
|`DA/SA error packet counter`|`0x0016`|
|`Debug Stick 0`|`0x0017`|


###Active Regiter

* **Description**           

This register configures Active for all Arrive modules


* **RTL Instant Name**    : `upen0`

* **Address**             : `0x0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`axi4_stream_loopout`| AXI4 Stream loop_out| `R/W`| `0x0`| `0x1`|
|`[01:01]`|`axi4_stream_loopin`| AXI4 Stream loop_in| `R/W`| `0x0`| `0x1`|
|`[00:00]`|`active`| Module Active - Set 1: Active all modules - Set 0: De-active all modules + Flush all FIFOs + Reset all Status and Control Register| `RW`| `0x1`| `0x1 End: Begin:`|

###Version ID

* **Description**           

This register shows Version ID


* **RTL Instant Name**    : `upen1`

* **Address**             : `0x0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`verion_year`| Version Year| `RO`| `0x0`| `0x0`|
|`[23:16]`|`verion_month`| Version Month| `RO`| `0x0`| `0x0`|
|`[15:08]`|`verion_date`| Version Date| `RO`| `0x0`| `0x0`|
|`[07:00]`|`verion_number`| Version Number| `RO`| `0x0`| `0x0 End: Begin:`|

###PDH to LIU Look-up Table

* **Description**           

This configure is used to convert ID PDH to LIU ID


* **RTL Instant Name**    : `upen_c_0`

* **Address**             : `0x1000 - 0x13FF`

* **Formula**             : `0x1000 + stsid*32 + vtgid*4 + vtid`

* **Where**               : 

    * `$stsid(0-23): stsid`

    * `$vtgid(0-6): stsid`

    * `$vtid(0-2): $vtid`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`liuenb`| LIU Enable| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`liumod`| LIU Mode - 0: For DE1 - 1: For DE3/EC1| `R/W`| `0x0`| `0x0`|
|`[06:00]`|`liuid`| Liu ID - 0-83:For DE1 - 0-23:For DE3/EC1| `R/W`| `0x0`| `0x0 End: Begin:`|

###LIU to PDH Look-up Table

* **Description**           

This configure is used to convert ID PDH to LIU ID


* **RTL Instant Name**    : `upen_c_1`

* **Address**             : `0x1400 - 0x1453`

* **Formula**             : `0x1400 + liuid`

* **Where**               : 

    * `$liuid(0-83): liuid`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`liuenb`| LIU Enable| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[09:00]`|`pdhid`| PDH ID (STS,VTG,VT)| `R/W`| `0x0`| `0x0 End: Begin:`|

###NCO mode LIU to PDH

* **Description**           

This configure is used to convert ID PDH to LIU ID


* **RTL Instant Name**    : `upen_c_2`

* **Address**             : `0x1500 - 0x1553`

* **Formula**             : `0x1500 + liuid`

* **Where**               : 

    * `$liuid(0-83): liuid`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`txncomd`| tx_nco_mode - Set 4: EC1. - Set 3: E3. - Set 2: DS3. - Set 1: E1. - Set 0: DS1| `R/W`| `0x0`| `0x0 End: Begin:`|

###Ethernet Encapsulation DA bit47_32 Configuration

* **Description**           

This register  is used to configure the DA bit47_32


* **RTL Instant Name**    : `upen2`

* **Address**             : `0x0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:00]`|`da_bit47_32`| DA bit47_32| `R/W`| `0xCAFE`| `0xCAFE End: Begin:`|

###Ethernet Encapsulation DA bit32_00 Configuration

* **Description**           

This register  is used to configure the DA bit32_00


* **RTL Instant Name**    : `upen3`

* **Address**             : `0x0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`da_bit31_00`| DA bit31_00| `R/W`| `0xCAFE_CAFE`| `0xCAFE_CAFE End: Begin:`|

###Ethernet Encapsulation SA bit47_32 Configuration

* **Description**           

This register  is used to configure the SA bit47_32 and Sub_Type


* **RTL Instant Name**    : `upen4`

* **Address**             : `0x0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`ether_subtype`| Ethernet SubType - 0x0		: DS1/E1 Frames - 0x1-0xF 	: reserved - 0x10		: DS3/E3/EC-1 Frames - 0x11-0x7F	: reserved - 0x80-0xFF	: control frames| `R/W`| `0x0`| `0x0`|
|`[15:00]`|`sa_bit47_32`| SA bit47_32| `R/W`| `0xABCD`| `0xABCD End: Begin:`|

###Ethernet Encapsulation SA bit32_00 Configuration

* **Description**           

This register  is used to configure the SA bit32_00


* **RTL Instant Name**    : `upen5`

* **Address**             : `0x0005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`sa_bit31_00`| SA bit31_00| `R/W`| `0xABCD_ABCD`| `0xABCD_ABCD End: Begin:`|

###Ethernet Encapsulation EtheType and EtherLength  Configuration

* **Description**           

This register is used to configure the EtheType and EtherLength


* **RTL Instant Name**    : `upen6`

* **Address**             : `0x0006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`ether_type`| Ethernet Type| `R/W`| `0x8800`| `0x8800`|
|`[15:00]`|`ether_len`| Ethernet Length - DS1/E1: number of DS1/E1 * 2 bytes + 2 (seq num) - DS3/E3/EC-1: number of DS3/E3/EC-1 * 6 bytes + 2 - Control frames: length of control frame payload| `R/W`| `0xAA`| `0xAA End: Begin:`|

###TX LIU FIFO Overrun Counter

* **Description**           

TX LIU overrun counter. Write 0x0 to clear


* **RTL Instant Name**    : `upen7`

* **Address**             : `0x0200 - 0x0253`

* **Formula**             : `0x0200 + liuid`

* **Where**               : 

    * `$liuid(0-83):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txliuoverrun_cnt`| Tx_LUI_overrun_counter| `R/W`| `0x0`| `0x0 End: Begin:`|

###TX LIU FIFO Underrun Counter

* **Description**           

TX LIU Underrun counter. Write 0x0 to clear


* **RTL Instant Name**    : `upen8`

* **Address**             : `0x0280 - 0x02D3`

* **Formula**             : `0x0280 + liuid`

* **Where**               : 

    * `$liuid(0-83):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`txliuunderrun_cnt`| Tx_LUI_underrun_counter| `R/W`| `0x0`| `0x0 End: Begin:`|

###RX PDH FIFO Overrun

* **Description**           

RX LIU FIFO is full. Write 0xFFFF_FFFF to clear


* **RTL Instant Name**    : `upen9`

* **Address**             : `0x0010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rxliufull`| Rx_Liu_Fifo_Full - Bit#0 for LIU ID#0 - Bit#1 for LIU ID#1 - Bit#31 for LIU ID#31 - Set 1: shown FIFO Full| `R/W`| `0x0`| `0x0 End: Begin:`|

###AXI4-Stream (PDH to MAC) Packet Counter

* **Description**           

AXI4-Stream Packet Counter. Write 0x0 to clear


* **RTL Instant Name**    : `upen12`

* **Address**             : `0x0013`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`axispktcnt`| axis_packect_counter| `R/W`| `0x0`| `0x0 End: Begin:`|

###AXI4-Stream (MAC to PDH) Byte Counter

* **Description**           

AXI4-Stream Byte Counter. Write 0x0 to clear


* **RTL Instant Name**    : `upen13`

* **Address**             : `0x0014`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`axisbytecnt`| axis_byte_counter| `R/W`| `0x0`| `0x0 End: Begin:`|

###AXI4-Stream (PDH to MAC) Packet Counter

* **Description**           

AXI4-Stream Packet Counter. Write 0x0 to clear


* **RTL Instant Name**    : `upen14`

* **Address**             : `0x0015`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`axispktcnt`| axis_packect_counter| `R/W`| `0x0`| `0x0 End: Begin:`|

###AXI4-Stream (PDH to MAC) Byte Counter

* **Description**           

AXI4-Stream Byte Counter. Write 0x0 to clear


* **RTL Instant Name**    : `upen15`

* **Address**             : `0x0016`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`axisbytecnt`| axis_byte_counter| `R/W`| `0x0`| `0x0 End: Begin:`|

###DA/SA error packet counter

* **Description**           

Counter DA/SA error packet of Decapsulation. Write 0x0 to clear


* **RTL Instant Name**    : `upen16`

* **Address**             : `0x0016`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`pktcnt`| axis_byte_counter| `R/W`| `0x0`| `0x0 End: Begin:`|

###Debug Stick 0

* **Description**           

This sticky is used to debug. Write 0xFFFF_FFFF to clear


* **RTL Instant Name**    : `upen17`

* **Address**             : `0x0017`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`dec_seq_err`| Dec detect Sequence Error| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`dec_eop_err`| Dec detect EOP Error| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`dec_ethlen_err`| Dec detect Ethernet Lenght Error| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`dec_ethtyp_err`| Dec detect Ethernet Type Error| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`dec_subtyp_err`| Dec detect Sub Type Error| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`dec_da_err`| Dec detect DA error| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`dec_sa_err`| Dec detect SA error| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`decbuf_read_err`| Dec buffer read when buffer is empty| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`decbuf_write_err`| Dec buffer write when buffer is full| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`encbuf_read_err`| Enc buffer read when buffer is empty| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`encbuf_write_err`| Enc buffer write when buffer is full| `R/W`| `0x0`| `0x0 End:`|

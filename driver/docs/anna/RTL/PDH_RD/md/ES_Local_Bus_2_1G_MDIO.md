## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-03-09|Project|Initial version|




##ES_Local_Bus_2_1G_MDIO
####Register Table

|Name|Address|
|-----|-----|
|`Configure Raise mode for Interrupt Pin`|`0x0`|
|`Config PHY address`|`0x10`|
|`Status of Diagnostic Interrupt`|`0x11`|
|`Config Reg address`|`0x12`|
|`Config Reg address`|`0x20`|


###Configure Raise mode for Interrupt Pin

* **Description**           

ack bit status this bit set to 1 when Read/Write OP done and will be clear when ACK_CLR bit set


* **RTL Instant Name**    : `ack_bit`

* **Address**             : `0x0`

* **Formula**             : `Base_0x0 + 0x0`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00]`|`ack_bit`| set to 1 when Read/Write OP done and will be clear when ACK_CLR bit set| `RO`| `0x0`| `0x0 End : Begin:`|

###Config PHY address

* **Description**           

This is Config PHY address and Port Select


* **RTL Instant Name**    : `pcfg_ctl1`

* **Address**             : `0x10`

* **Formula**             : `Base_0x10 + 0x10`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[07:04]`|`md_cs`| Select MDIO control port 1->4 0 : Un-Select 1 : Select MDIO P1 2 : Select MDIO P2 3 : Select MDIO P3 4 : Select MDIO P4| `RW`| `0x0`| `0x0`|
|`[03:00]`|`md_phyadr`| MDIO Device Addresses it is fix 0x1 for Xilinx SGMII Serdes| `RW`| `0x0`| `0x0 End : Begin:`|

###Status of Diagnostic Interrupt

* **Description**           

This is Status of Diagnostic Interrupt 1-2


* **RTL Instant Name**    : `md_ack_clr`

* **Address**             : `0x11`

* **Formula**             : `Base_0x11 + 0x11`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`ack_clr`| Write to Clear ACK OP<br>{1} : Clear ACK| `W1C`| `0x0`| `0x0 End : Begin:`|

###Config Reg address

* **Description**           

This is Config Reg address and RNW


* **RTL Instant Name**    : `pcfg_ctl2`

* **Address**             : `0x12`

* **Formula**             : `Base_0x12 + 0x12`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[06:05]`|`md_op`| OP: operation code 2'b00 : Open 2'b10 : Read Operation 2'b01 : Write Operation| `RW`| `0x0`| `0x0`|
|`[04:00]`|`md_regadr`| MDIO Register  Addresses| `RW`| `0x0`| `0x0 End : Begin:`|

###Config Reg address

* **Description**           

This is Config Reg address and RNW


* **RTL Instant Name**    : `pcfg_ctl3`

* **Address**             : `0x20`

* **Formula**             : `Base_0x20 + 0x20`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`md_data_read`| MDIO Data  write to PHY chip| `RW`| `0x0`| `0x0`|
|`[15:00]`|`md_data_read`| MDIO Data read from PHY chip| `RO`| `0x0`| `0x0 End :`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_POH_BER
####Register Table

|Name|Address|
|-----|-----|
|`POH Threshold Global Control`|`0x00_0003`|
|`POH Hi-order Path Over Head Grabber`|`0x02_4000`|
|`POH Lo-order VT/TU3 Over Head Grabber`|`0x02_6000`|
|`POH CPE STS/TU3 Control Register`|`0x02_A000`|
|`POH CPE VT Control Register`|`0x02_8000`|
|`POH CPE STS Status Register`|`0x02_C2C0`|
|`POH CPE VT/TU3 Status Register`|`0x02_C000`|
|`POH CPE J1 STS Expected Message buffer`|`0x0B_0000`|
|`POH CPE J1 STS Current Message buffer`|`0x0B_1000`|
|`POH CPE J2 Expected Message buffer`|`0x08_0000`|
|`POH CPE J2 Current Message buffer`|`0x09_0000`|
|`POH CPE J1 Insert Message buffer`|`0x0B_2000`|
|`POH CPE J2 Insert Message buffer`|`0x0A_0000`|
|`POH Termintate Insert Control STS`|`0x04_0400`|
|`POH Termintate Insert Control VT/TU3`|`0x04_4000`|
|`POH Termintate Insert Buffer STS`|`0x01_0800`|
|`POH Termintate Insert Buffer TU3/VT`|`0x01_8000`|
|`POH BER Global Control`|`0x06_0000`|
|`POH BER Error Code Select Control 0`|`0x06_0004`|
|`POH BER Error Code Select Control 0`|`0x06_0005`|
|`POH BER Error Sticky`|`0x06_0001`|
|`POH BER Threshold 1`|`0x06_2300`|
|`POH BER Threshold 2`|`0x06_0400`|
|`POH BER Control VT/DSN`|`0x06_2000`|
|`POH BER Control STS/TU3`|`0x06_2007`|
|`POH BER Report VT/DSN`|`0x06_8000`|
|`POH BER Report STS/TU3`|`0x06_8B00`|
|`POH BIP Counter Report STS`|`0x0C_82C0`|
|`POH BIP Counter Report TU3/VT`|`0x0C_8000`|
|`POH REI Counter Report STS`|`0x0C_C2C0`|
|`POH REI Counter Report TU3/VT`|`0x0C_C000`|
|`POH Alarm Status Mask Report STS`|`0x0D_0000`|
|`POH Alarm Status Report STS`|`0x0D_0040`|
|`POH Interrupt Status Report STS`|`0x0D_0020`|
|`POH Interrupt Global Status Report STS`|`0x0D_007F`|
|`POH Interrupt Global Mask Report STS`|`0x0D_007E`|
|`POH Alarm Status Mask Report VT/TU3`|`0x0E_0000`|
|`POH Alarm Status Report VT/TU3`|`0x0E_0800`|
|`POH Interrupt Status Report STS`|`0x0E_0400`|
|`POH Interrupt Or Status Report VT/TU3`|`0x0E_0C00`|
|`POH Interrupt Global Status Report VT/TU3`|`0x0E_0FFF`|
|`POH Interrupt Global Mask Report VT/TU3`|`0x0E_0FFE`|
|`POH Interrupt Global Mask Report`|`0x00_0004`|
|`POH Interrupt Global Status Report`|`0x00_0005`|
|`POH Interrupt  Global Status Out Report`|`0x00_0006`|


###POH Threshold Global Control

* **Description**           

This register is used to set Threshold for stable detection.


* **RTL Instant Name**    : `pcfg_trshglbctr`

* **Address**             : `0x00_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`jnmsgdebound`| Debound Threshold for Jn 16/64byte| `RW`| `0x5`| `0x5`|
|`[27:24]`|`v5rfistbtrsh`| V5 RDI Stable Thershold| `RW`| `0x1`| `0x1`|
|`[23:20]`|`v5rdistbtrsh`| V5 RDI Stable Thershold| `RW`| `0x1`| `0x1`|
|`[19:16]`|`v5slbstbtrsh`| V5 Signal Lable Stable Thershold| `RW`| `0x1`| `0x1`|
|`[15:12]`|`g1rdistbtrsh`| G1 RDI Path Stable Thershold| `RW`| `0x1`| `0x1`|
|`[11:8]`|`c2plmstbtrsh`| C2 Path Signal Lable Stable Thershold| `RW`| `0x1`| `0x1`|
|`[7:4]`|`jnstbtrsh`| J1/J2 Message Stable Threshold| `RW`| `0x1`| `0x1`|
|`[3:0]`|`debound`| Debound Threshold| `RW`| `0x1`| `0x1 End: Begin:`|

###POH Hi-order Path Over Head Grabber

* **Description**           

This register is used to grabber Hi-Order Path Overhead


* **RTL Instant Name**    : `pohstspohgrb`

* **Address**             : `0x02_4000`

* **Formula**             : `0x02_4000 + $stsid * 8 + $sliceid`

* **Where**               : 

    * `$sliceid(0-1): Slice Identification`

    * `$stsid(0-23): STS Identification`

* **Width**               : `68`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[67]`|`hlais`| High-level AIS from OCN| `RO`| `0x0`| `0x0`|
|`[66]`|`lom`| LOM  from OCN| `RO`| `0x0`| `0x0`|
|`[65]`|`lop`| LOP from OCN| `RO`| `0x0`| `0x0`|
|`[64]`|`ais`| AIS from OCN| `RO`| `0x0`| `0x0`|
|`[63:56]`|`k3`| K3 byte| `RO`| `0x0`| `0x0`|
|`[55:48]`|`f3`| F3 byte| `RO`| `0x0`| `0x0`|
|`[47:40]`|`h4`| H4 byte| `RO`| `0x0`| `0x0`|
|`[39:32]`|`f2`| F2 byte| `RO`| `0x0`| `0x0`|
|`[31:24]`|`g1`| G1 byte| `RO`| `0x0`| `0x0`|
|`[23:16]`|`c2`| C2 byte| `RO`| `0x0`| `0x0`|
|`[15:8]`|`n1`| N1 byte| `RO`| `0x0`| `0x0`|
|`[7:0]`|`j1`| J1 byte| `RO`| `0x0`| `0x0 End: Begin:`|

###POH Lo-order VT/TU3 Over Head Grabber

* **Description**           

This register is used to grabber Lo-Order Path Overhead. Incase the TU3 mode, the $vtid = 0, using for Tu3 POH grabber.

Incase VT mode, the $vtid = 0-27, using for VT POH grabber.


* **RTL Instant Name**    : `pohvtpohgrb`

* **Address**             : `0x02_6000`

* **Formula**             : `0x02_6000 + $sliceid*672 + $stsid*28 + $vtid`

* **Where**               : 

    * `$sliceid(0-0): Slice Identification`

    * `$stsid(0-23): STS Identification`

    * `$vtid(0-27): VT Identification`

* **Width**               : `36`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[35]`|`hlais`| High-level AIS from OCN| `RO`| `0x0`| `0x0`|
|`[34]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[33]`|`lop`| LOP from OCN| `RO`| `0x0`| `0x0`|
|`[32]`|`ais`| AIS from OCN| `RO`| `0x0`| `0x0`|
|`[31:24]`|`byte3`| G1 byte or K3 byte or K4 byte| `RO`| `0x0`| `0x0`|
|`[23:16]`|`byte2`| C2 byte or F3 byte or N2 byte| `RO`| `0x0`| `0x0`|
|`[15:8]`|`byte1`| N1 byte or H4 byte or J2 byte| `RO`| `0x0`| `0x0`|
|`[7:0]`|`byte0`| J1 byte or F2 byte or V5 byte| `RO`| `0x0`| `0x0 End: Begin:`|

###POH CPE STS/TU3 Control Register

* **Description**           

This register is used to configure the POH Hi-order Path Monitoring.


* **RTL Instant Name**    : `pohcpestsctr`

* **Address**             : `0x02_A000`

* **Formula**             : `0x02_A000 + $sliceid*48 + $stsid*2 + $tu3en`

* **Where**               : 

    * `$sliceid(0-1): Slice Identification`

    * `$stsid(0-23): STS Identification`

    * `$tu3en(0-1): Tu3enable, 0: STS, 1:Tu3`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[19]`|`plmenb`| PLM enable| `RW`| `0x0`| `0x0`|
|`[18]`|`vcaisdstren`| VcaisDstren| `RW`| `0x0`| `0x0`|
|`[17]`|`plmdstren`| PlmDstren| `RW`| `0x0`| `0x0`|
|`[16]`|`uneqdstren`| UneqDstren| `RW`| `0x0`| `0x0`|
|`[15]`|`timdstren`| TimDstren. For slice id 1, this will enable/disable send TIM alarm to OCN block for generating TIM alarm.| `RW`| `0x1`| `0x0`|
|`[14]`|`sdhmode`| SDH mode| `RW`| `0x0`| `0x0`|
|`[13]`|`blkmden`| Block mode BIP| `RW`| `0x0`| `0x0`|
|`[12]`|`erdienb`| Enable E-RDI| `RW`| `0x0`| `0x0`|
|`[11:4]`|`pslexp`| C2 Expected Path Signal Lable Value| `RW`| `0x0`| `0x0`|
|`[3]`|`timenb`| Enable Monitor TIM| `RW`| `0x0`| `0x0`|
|`[2]`|`reiblkmden`| Block mode REI| `RW`| `0x0`| `0x0`|
|`[1:0]`|`j1mode`| 0: 1Byte 1:16Byte 2:64byte 3:Floating| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE VT Control Register

* **Description**           

This register is used to configure the POH Lo-order Path Monitoring.


* **RTL Instant Name**    : `pohcpevtctr`

* **Address**             : `0x02_8000`

* **Formula**             : `0x02_8000 + $sliceid*672 + $stsid*28 +$vtid`

* **Where**               : 

    * `$sliceid(0-0): Slice Identification`

    * `$stsid(0-23): STS Identification`

    * `$vtid(0-27):Vt Identification`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[14]`|`plmenb`| VcaisDstren| `RW`| `0x0`| `0x0`|
|`[13]`|`vcaisdstren`| VcaisDstren| `RW`| `0x0`| `0x0`|
|`[12]`|`plmdstren`| PlmDstren| `RW`| `0x0`| `0x0`|
|`[11]`|`uneqdstren`| UneqDstren| `RW`| `0x0`| `0x0`|
|`[10]`|`timdstren`| TimDstren| `RW`| `0x0`| `0x0`|
|`[9]`|`vsdhmode`| SDH mode| `RW`| `0x0`| `0x0`|
|`[8]`|`vblkmden`| Block mode BIP| `RW`| `0x0`| `0x0`|
|`[7]`|`erdienb`| Enable E-RDI| `RW`| `0x0`| `0x0`|
|`[6:4]`|`vslexp`| V5 Expected Path Signal Lable Value| `RW`| `0x0`| `0x0`|
|`[3]`|`timenb`| Enable Monitor TIM| `RW`| `0x0`| `0x0`|
|`[2]`|`reiblkmden`| Block mode REI| `RW`| `0x0`| `0x0`|
|`[1:0]`|`j2mode`| 0: 1Byte 1:16Byte 2:64byte 3:Floating| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE STS Status Register

* **Description**           

This register is used to get POH Hi-order status of monitoring.


* **RTL Instant Name**    : `pohcpestssta`

* **Address**             : `0x02_C2C0`

* **Formula**             : `0x02_C2C0 + $sliceid*32 + $stsid`

* **Where**               : 

    * `$sliceid(0-1): Slice Identification`

    * `$stsid(0-23): STS Identification`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[12]`|`c2stbsta`| C2 stable status| `RO`| `0x0`| `0x0`|
|`[11:8]`|`c2stbcnt`| C2 stable counter| `RO`| `0x0`| `0x0`|
|`[7:0]`|`c2acpt`| C2 accept byte| `RO`| `0x0`| `0x0 End: Begin:`|

###POH CPE VT/TU3 Status Register

* **Description**           

This register is used to get POH Lo-order status of monitoring.


* **RTL Instant Name**    : `pohcpevtsta`

* **Address**             : `0x02_C000`

* **Formula**             : `0x02_C000 + $sliceid*672 + $stsid*28 +$vtid`

* **Where**               : 

    * `$sliceid(0-0): Slice Identification`

    * `$stsid(0-23): STS Identification`

    * `$vtid(0-27):Vt Identification`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[13]`|`vslstbsta`| VSL stable status| `RO`| `0x0`| `0x0`|
|`[12:9]`|`vslstbcnt`| VSL stable counter| `RO`| `0x0`| `0x0`|
|`[8:6]`|`vslacpt`| VSL accept byte| `RO`| `0x0`| `0x0`|
|`[5:0]`|`rfistatus`| RFI status| `RO`| `0x0`| `0x0 End: Begin:`|

###POH CPE J1 STS Expected Message buffer

* **Description**           

The J1 Expected Message Buffer.


* **RTL Instant Name**    : `pohmsgstsexp`

* **Address**             : `0x0B_0000`

* **Formula**             : `0x0B_0000 + $sliceid*256 + $stsid*8 + $msgid`

* **Where**               : 

    * `$sliceid(0-1): Slice Identification`

    * `$stsid(0-23): STS Identification`

    * `$msgid(0-7): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j1expmsg`| J1 Expected Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J1 STS Current Message buffer

* **Description**           

The J1 Current Message Buffer.


* **RTL Instant Name**    : `pohmsgstscur`

* **Address**             : `0x0B_1000`

* **Formula**             : `0x0B_1000 + $sliceid*256 + $stsid*8 + $msgid`

* **Where**               : 

    * `$sliceid(0-1): Slice Identification`

    * `$stsid(0-23): STS Identification`

    * `$msgid(0-7): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j1curmsg`| J1 Current Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J2 Expected Message buffer

* **Description**           

The J2 Expected Message Buffer.


* **RTL Instant Name**    : `pohmsgvtexp`

* **Address**             : `0x08_0000`

* **Formula**             : `0x08_0000 + $sliceid*5376 + $stsid*224 + $vtid*8 + $msgid`

* **Where**               : 

    * `$sliceid(0-0): Slice Identification`

    * `$stsid(0-23): STS Identification`

    * `$vtid(0-27): VT Identification`

    * `$msgid(0-7): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j2expmsg`| J2 Expected Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J2 Current Message buffer

* **Description**           

The J2 Current Message Buffer.


* **RTL Instant Name**    : `pohmsgvtcur`

* **Address**             : `0x09_0000`

* **Formula**             : `0x09_0000 + $sliceid*5376 + $stsid*224 + $vtid*8 + $msgid`

* **Where**               : 

    * `$sliceid(0-0): Slice Identification`

    * `$stsid(0-23): STS Identification`

    * `$vtid(0-27): VT Identification`

    * `$msgid(0-7): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j2curmsg`| J2 Current Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J1 Insert Message buffer

* **Description**           

The J1 Current Message Buffer.


* **RTL Instant Name**    : `pohmsgstsins`

* **Address**             : `0x0B_2000`

* **Formula**             : `0x0B_2000 + $sliceid*256 + $stsid*8 + $msgid`

* **Where**               : 

    * `$sliceid(0-0): Slice Identification`

    * `$stsid(0-23): STS Identification`

    * `$msgid(0-7): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j1insmsg`| J1 Insert Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J2 Insert Message buffer

* **Description**           

The J2 Insert Message Buffer.


* **RTL Instant Name**    : `pohmsgvtins`

* **Address**             : `0x0A_0000`

* **Formula**             : `0x0A_0000 + $sliceid*5376 + $stsid*224 + $vtid*8 + $msgid`

* **Where**               : 

    * `$sliceid(0-0): Slice Identification`

    * `$stsid(0-23): STS Identification`

    * `$vtid(0-27): VT Identification`

    * `$msgid(0-7): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j2insmsg`| J2 Insert Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Termintate Insert Control STS

* **Description**           

This register is used to control STS POH insert .


* **RTL Instant Name**    : `ter_ctrlhi`

* **Address**             : `0x04_0400`

* **Formula**             : `0x04_0400 + $STS + $OCID*32`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-0)  : Line ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:7]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[6]`|`g1spare`| G1 spare value| `RW`| `0x0`| `0x0`|
|`[5]`|`plm`| 0 : Enable, 1: Disable send ERDI if PLM detected| `RW`| `0x0`| `0x0`|
|`[4]`|`uneq`| 0 : Enable, 1: Disable send ERDI if UNEQ detected| `RW`| `0x0`| `0x0`|
|`[3]`|`timmsk`| 0 : Enable, 1: Disable send ERDI if TIM detected| `RW`| `0x0`| `0x0`|
|`[2]`|`aislopmsk`| 0 : Enable, 1: Disable send ERDI if AIS,LOP detected| `RW`| `0x0`| `0x0`|
|`[1:0]`|`jnfrmd`| 0:1 byte, 1: 16/64byte| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Termintate Insert Control VT/TU3

* **Description**           

This register is used to control STS POH insert. TU3 is at VT ID = 0. Fields must be the same as ter_ctrlhi


* **RTL Instant Name**    : `ter_ctrllo`

* **Address**             : `0x04_4000`

* **Formula**             : `0x04_4000 + $STS*28 + $OCID*672 + $VT`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-0)  : Line ID`

    * `$VT(0-27)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[14:13]`|`k4b0b1`| K4b0b1 value| `RW`| `0x0`| `0x0`|
|`[12:11]`|`k4aps`| K4aps value| `RW`| `0x0`| `0x0`|
|`[10]`|`k4spare`| K4spare value| `RW`| `0x0`| `0x0`|
|`[9]`|`rfival`| RFI value| `RW`| `0x0`| `0x0`|
|`[8:6]`|`vslval`| VT signal label value| `RW`| `0x0`| `0x0`|
|`[5]`|`plm`| 0 : Enable, 1: Disable send ERDI if PLM detected| `RW`| `0x0`| `0x0`|
|`[4]`|`uneq`| 0 : Enable, 1: Disable send ERDI if UNEQ detected| `RW`| `0x0`| `0x0`|
|`[3]`|`timmsk`| 0 : Enable, 1: Disable send ERDI if TIM detected| `RW`| `0x0`| `0x0`|
|`[2]`|`aislopmsk`| 0 : Enable, 1: Disable send ERDI if AIS,LOP detected| `RW`| `0x0`| `0x0`|
|`[1:0]`|`jnfrmd`| 0:1 byte, 1: 16/64byte| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Termintate Insert Buffer STS

* **Description**           

This register is used for storing POH BYTEs inserted to Sonet/SDH. %%

BGRP = 0 : G1,J1  %%

BGRP = 1 : N1,C2  %%

BGRP = 2 : H4,F2  %%

BGRP = 3 : K3,F3


* **RTL Instant Name**    : `rtlpohccterbufhi`

* **Address**             : `0x01_0800`

* **Formula**             : `0x01_0800 + $OCID*256 + $STS*4 + $BGRP`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-0)  : Line ID`

    * `$BGRP(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[17]`|`byte1msk`| Enable/Disable (1/0)write to buffer| `WO`| `0x0`| `0x0`|
|`[16:9]`|`byte1`| Byte1 (G1/N1/H4/K3)| `RW`| `0x0`| `0x0`|
|`[8]`|`byte0msk`| Enable/Disable (1/0) write to buffer| `WO`| `0x0`| `0x0`|
|`[7:0]`|`byte0`| Byte0 (J1/C2/F2/F3)| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Termintate Insert Buffer TU3/VT

* **Description**           

This register is used for storing POH BYTEs inserted to Sonet/SDH. TU3 is at VT ID = 0,1 %%

For VT %%

BGRP = 0 : J2,V5 %%

BGRP = 1 : K4,N2 %%

For TU3 %%

VT = 0, BGRP = 0 : G1,J1 %%

VT = 0, BGRP = 1 : N1,C2 %%

VT = 1, BGRP = 0 : H4,F2 %%

VT = 1, BGRP = 1 : K3,F3


* **RTL Instant Name**    : `rtlpohccterbuflo`

* **Address**             : `0x01_8000`

* **Formula**             : `0x01_8000 + $OCID*4096 + $STS*64 + $VT*2 + $BGRP`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-0)  : Line ID`

    * `$VT(0-27)`

    * `$BGRP(0-1)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[17]`|`byte1msk`| Enable/Disable (1/0)write to buffer| `WO`| `0x0`| `0x0`|
|`[16:9]`|`byte1`| Byte1 (J2/K4)| `RW`| `0x0`| `0x0`|
|`[8]`|`byte0msk`| Enable/Disable (1/0) write to buffer| `WO`| `0x0`| `0x0`|
|`[7:0]`|`byte0`| Byte0 (V5/N2)| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Global Control

* **Description**           

This register is used to enable STS,VT,DSN globally.


* **RTL Instant Name**    : `pcfg_glbenb`

* **Address**             : `0x06_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[3]`|`timerenb`| Enable timer| `RW`| `0x1`| `0x0`|
|`[2]`|`stsenb`| Enable STS/TU3 channel| `RW`| `0x1`| `0x0`|
|`[1]`|`vtenb`| Enable STS/TU3 channel| `RW`| `0x1`| `0x0`|
|`[0]`|`dsnsenb`| Enable STS/TU3 channel| `RW`| `0x1`| `0x0 End: Begin:`|

###POH BER Error Code Select Control 0

* **Description**           

This register is used to select error to monitor .


* **RTL Instant Name**    : `pcfg_errsel0`

* **Address**             : `0x06_0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[23:0]`|`linecodeen`| 1: Line code violation , 0: Parity code. For<br>{STS23,...STS0}| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Error Code Select Control 0

* **Description**           

This register is used to select error to monitor .


* **RTL Instant Name**    : `pcfg_errsel1`

* **Address**             : `0x06_0005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[23:0]`|`linecodeen`| 1: Line code violation , 0: Parity code. For<br>{STS47,...STS24}| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Error Sticky

* **Description**           

This register is used to check error in BER engine.


* **RTL Instant Name**    : `stkalarm`

* **Address**             : `0x06_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[1]`|`stserr`| STS error| `W1C`| `0x0`| `0x0`|
|`[0]`|`vterr`| VT error| `W1C`| `0x0`| `0x0 End: Begin:`|

###POH BER Threshold 1

* **Description**           

This register is used to configure threshold of BER level 3.


* **RTL Instant Name**    : `imemrwptrsh1`

* **Address**             : `0x06_2300`

* **Formula**             : `0x06_2300 + $Rate`

* **Where**               : 

    * `$Rate(0-127): STS Rate for rate from STS1,STS3,STS6...STS48,(0-16)...VT1.5,VT2,DS1,E1(65,67,69,71)....`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[18:9]`|`setthres`| SetThreshold| `RW`| `0x0`| `0x0`|
|`[8:0]`|`winthres`| WindowThreshold| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Threshold 2

* **Description**           

This register is used to configure threshold of BER level 4 to level 8.


* **RTL Instant Name**    : `imemrwptrsh2`

* **Address**             : `0x06_0400`

* **Formula**             : `0x06_0400 + $Rate*8 + $Thresloc`

* **Where**               : 

    * `$Rate(0-63): STS Rate for rate from STS1,STS3,STS6...STS48,....VT1.5,VT2,DS1,E1....`

    * `$Thresloc(0-7): Set/Clear/Window threshold for BER level from 4 to 8`

* **Width**               : `34`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[33:17]`|`scwthres1`| Set/Clear/Window Threshold| `RW`| `0x0`| `0x0`|
|`[16:0]`|`scwthres2`| Set/Clear/Window Threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Control VT/DSN

* **Description**           

This register is used to enable and set threshold SD SF .


* **RTL Instant Name**    : `imemrwpctrl1`

* **Address**             : `0x06_2000`

* **Formula**             : `0x06_2000 + $STS*32 + $OCID*8+ $VTG`

* **Where**               : 

    * `$STS(0-23): STS`

    * `$OCID(0-3)  : Line ID, 0-1: VT, 2-3: DE1`

    * `$VTG(0-6)   : VT group`

* **Width**               : `48`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:46]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[45:43]`|`tcatrsh4`| TCA threshold raise channel 4| `RW`| `0x0`| `0x0`|
|`[42:40]`|`tcatrsh3`| TCA threshold raise channel 3| `RW`| `0x0`| `0x0`|
|`[39:37]`|`tcatrsh2`| TCA threshold raise channel 2| `RW`| `0x0`| `0x0`|
|`[36:34]`|`tcatrsh1`| TCA threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[33:32]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[31]`|`etype4`| 0: DS1/VT1.5 1: E1/VT2 channel 3| `RW`| `0x0`| `0x0`|
|`[30:28]`|`sftrsh4`| SF threshold raise channel 3| `RW`| `0x0`| `0x0`|
|`[27:25]`|`sdtrsh4`| SD threshold raise channel 3| `RW`| `0x0`| `0x0`|
|`[24]`|`ena4`| Enable channel 3| `RW`| `0x0`| `0x0`|
|`[23]`|`etype3`| 0: DS1/VT1.5 1: E1/VT2 channel 2| `RW`| `0x0`| `0x0`|
|`[22:20]`|`sftrsh3`| SF threshold raise channel 2| `RW`| `0x0`| `0x0`|
|`[19:17]`|`sdtrsh3`| SD threshold raise channel 2| `RW`| `0x0`| `0x0`|
|`[16]`|`ena3`| Enable channel 2| `RW`| `0x0`| `0x0`|
|`[15]`|`etype2`| 0: DS1/VT1.5 1: E1/VT2 channel 1| `RW`| `0x0`| `0x0`|
|`[14:12]`|`sftrsh2`| SF threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[11:9]`|`sdtrsh2`| SD threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[8]`|`ena2`| Enable channel 1| `RW`| `0x0`| `0x0`|
|`[7]`|`etype1`| 0: DS1/VT1.5 1: E1/VT2 channel 0| `RW`| `0x0`| `0x0`|
|`[6:4]`|`sftrsh1`| SF threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[3:1]`|`sdtrsh1`| SD threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[0]`|`ena1`| Enable channel 0| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Control STS/TU3

* **Description**           

This register is used to enable and set threshold SD SF.


* **RTL Instant Name**    : `imemrwpctrl2`

* **Address**             : `0x06_2007`

* **Formula**             : `0x06_2007 + $STS*32 + $OCID*8`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-3)  : Line ID, Line ID 0-1: STS, Line ID 2-3: Line EC1 channel 1, DE3 channel 2`

* **Width**               : `48`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:40]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[39:37]`|`tcatrsh2`| TCA threshold raise channel 2| `RW`| `0x0`| `0x0`|
|`[36:34]`|`tcatrsh1`| TCA threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[33:32]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:21]`|`rate2`| STS Rate 0-63 type| `RW`| `0x0`| `0x0`|
|`[20:18]`|`sftrsh2`| SF threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[17:15]`|`sdtrsh2`| SD threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[14]`|`ena2`| Enable channel 1| `RW`| `0x0`| `0x0`|
|`[13:7]`|`rate1`| STS Rate 0-63 type| `RW`| `0x0`| `0x0`|
|`[6:4]`|`sftrsh1`| SF threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[3:1]`|`sdtrsh1`| SD threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[0]`|`ena1`| Enable channel 0| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Report VT/DSN

* **Description**           

This register is used to get current BER rate .


* **RTL Instant Name**    : `ramberratevtds`

* **Address**             : `0x06_8000`

* **Formula**             : `0x06_8000 + $STS*112 + $OCID*28 + $VT`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-3)     : Line ID, VT:0-1,DSN:2-3`

    * `$VT(0-27)   : VT/DS1 ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[3]`|`hwsta`| Hardware status| `RW`| `0x0`| `0x0`|
|`[2:0]`|`rate`| BER rate| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Report STS/TU3

* **Description**           

This register is used to get current BER rate . BER DE3 used with OCID 2-3, TU3TYPE = 1. BER EC1 used with OCID 2-3, TU3TYPE = 0.


* **RTL Instant Name**    : `ramberrateststu3`

* **Address**             : `0x06_8B00`

* **Formula**             : `0x06_8B00 + $STS*8 + $OCID*2 + $TU3TYPE`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-3)     : Line ID,  Line ID 0-1: STS, Line ID 2-3: Line EC1/DE3`

    * `$TU3TYPE(0-1)  : Type TU3:1, STS:0`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[3]`|`hwsta`| Hardware status| `RW`| `0x0`| `0x0`|
|`[2:0]`|`rate`| BER rate| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BIP Counter Report STS

* **Description**           

This register is used to get POH BIP Counter. OCID 1 is used for B2 counter with 24 B2 each OCID.


* **RTL Instant Name**    : `ipm_bipcnthi`

* **Address**             : `0x0C_82C0`

* **Formula**             : `0x0C_82C0 + $STS + $OCID*32`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-1)     : Line ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[15:0]`|`bipcnt`| BIP counter| `W0C`| `0x0`| `0x0 End: Begin:`|

###POH BIP Counter Report TU3/VT

* **Description**           

This register is used to get POH BIP Counter


* **RTL Instant Name**    : `ipm_bipcntlo`

* **Address**             : `0x0C_8000`

* **Formula**             : `0x0C_8000 + $STS*28 + $OCID*672 + $VT`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-0)   : Line ID`

    * `$VT(0-27)   : VT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[15:0]`|`bipcnt`| BIP counter| `W0C`| `0x0`| `0x0 End: Begin:`|

###POH REI Counter Report STS

* **Description**           

This register is used to get POH REI Counter. OCID 1 is used for B2 counter with 24 B2 each OCID.


* **RTL Instant Name**    : `ipm_reicnthi`

* **Address**             : `0x0C_C2C0`

* **Formula**             : `0x0C_C2C0 + $STS + $OCID*32`

* **Where**               : 

    * `$STS(0-24)  : STS`

    * `$OCID(0-1)     : Line ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[15:0]`|`reicnt`| REI counter| `W0C`| `0x0`| `0x0 End: Begin:`|

###POH REI Counter Report TU3/VT

* **Description**           

This register is used to get POH REI Counter


* **RTL Instant Name**    : `ipm_reicntlo`

* **Address**             : `0x0C_C000`

* **Formula**             : `0x0C_C000 + $STS*28 + $OCID*672 + $VT`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-0)   : Line ID`

    * `$VT(0-27)   : VT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[15:0]`|`reicnt`| REI counter| `W0C`| `0x0`| `0x0 End: Begin:`|

###POH Alarm Status Mask Report STS

* **Description**           

This register is used to get POH alarm mask report.


* **RTL Instant Name**    : `alm_mskhi`

* **Address**             : `0x0D_0000`

* **Formula**             : `0x0D_0000 + $STS + $OCID*128`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-3) : Line ID, ID 0,1 for path STS 0-23, ID 2,3 EC1 Line 0-23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[11]`|`bersdmsk`| bersd mask| `RW`| `0x0`| `0x0`|
|`[10]`|`bersfmsk`| bersf  mask| `RW`| `0x0`| `0x0`|
|`[9]`|`erdimsk`| erdi mask| `RW`| `0x0`| `0x0`|
|`[8]`|`bertcamsk`| bertca mask| `RW`| `0x0`| `0x0`|
|`[7]`|`jnstbmsk`| jn stable mask| `RW`| `0x0`| `0x0`|
|`[6]`|`pslstbmsk`| psl stable mask| `RW`| `0x0`| `0x0`|
|`[5]`|`rfimsk`| rfi/lom mask| `RW`| `0x0`| `0x0`|
|`[4]`|`timmsk`| tim mask| `RW`| `0x0`| `0x0`|
|`[3]`|`uneqmsk`| uneq mask| `RW`| `0x0`| `0x0`|
|`[2]`|`plmmsk`| plm mask| `RW`| `0x0`| `0x0`|
|`[1]`|`aismsk`| ais mask| `RW`| `0x0`| `0x0`|
|`[0]`|`lopmsk`| lop mask| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Alarm Status Report STS

* **Description**           

This register is used to get POH alarm status report.


* **RTL Instant Name**    : `alm_stahi`

* **Address**             : `0x0D_0040`

* **Formula**             : `0x0D_0040 + $STS + $OCID*128`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-3) : Line ID, ID 0,1 for path STS 0-23, ID 2,3 EC1 Line 0-23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[11]`|`bersdsta`| bersd  status| `W1C`| `0x0`| `0x0`|
|`[10]`|`bersfsta`| bersf  status| `W1C`| `0x0`| `0x0`|
|`[9]`|`erdista`| erdi status status| `RO`| `0x0`| `0x0`|
|`[8]`|`bertcasta`| bertca status| `RO`| `0x0`| `0x0`|
|`[7]`|`jnstbsta`| jn stable status| `RO`| `0x0`| `0x0`|
|`[6]`|`pslstbsta`| psl stable status| `RO`| `0x0`| `0x0`|
|`[5]`|`rfista`| rfi/lom status| `RO`| `0x0`| `0x0`|
|`[4]`|`timsta`| tim status| `RO`| `0x0`| `0x0`|
|`[3]`|`uneqsta`| uneq status| `RO`| `0x0`| `0x0`|
|`[2]`|`plmsta`| plm status| `RO`| `0x0`| `0x0`|
|`[1]`|`aissta`| ais status| `RO`| `0x0`| `0x0`|
|`[0]`|`lopsta`| lop status| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Status Report STS

* **Description**           

This register is used to get POH alarm change status report.


* **RTL Instant Name**    : `alm_chghi`

* **Address**             : `0x0D_0020`

* **Formula**             : `0x0D_0020 + $STS + $OCID*128`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-3) : Line ID, ID 0,1 for path STS 0-23, ID 2,3 EC1 Line 0-23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[11]`|`bersdstachg`| bersd stable status change| `W1C`| `0x0`| `0x0`|
|`[10]`|`bersfstachg`| bersf stable status change| `W1C`| `0x0`| `0x0`|
|`[9]`|`erdistachg`| erdi status change| `W1C`| `0x0`| `0x0`|
|`[8]`|`bertcastachg`| bertca status change| `W1C`| `0x0`| `0x0`|
|`[7]`|`jnstbstachg`| jn stable status change| `W1C`| `0x0`| `0x0`|
|`[6]`|`pslstbstachg`| psl stable status change| `W1C`| `0x0`| `0x0`|
|`[5]`|`rfistachg`| rfi/lom status change| `W1C`| `0x0`| `0x0`|
|`[4]`|`timstachg`| tim status change| `W1C`| `0x0`| `0x0`|
|`[3]`|`uneqstachg`| uneq status change| `W1C`| `0x0`| `0x0`|
|`[2]`|`plmstachg`| plm status change| `W1C`| `0x0`| `0x0`|
|`[1]`|`aisstachg`| ais status change| `W1C`| `0x0`| `0x0`|
|`[0]`|`lopstachg`| lop status change| `W1C`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Status Report STS

* **Description**           

This register is used to get POH alarm global change status report.


* **RTL Instant Name**    : `alm_glbchghi`

* **Address**             : `0x0D_007F`

* **Formula**             : `0x0D_007F + $OCID*128`

* **Where**               : 

    * `$OCID(0-3) : Line ID, ID 0,1 for path STS 0-23, ID 2,3 EC1 Line 0-23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[23:0]`|`glbstachg`| global status change bit| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Mask Report STS

* **Description**           

This register is used to get POH alarm global mask report.


* **RTL Instant Name**    : `alm_glbmskhi`

* **Address**             : `0x0D_007E`

* **Formula**             : `0x0D_007E + $OCID*128`

* **Where**               : 

    * `$OCID(0-3) : Line ID, ID 0,1 for path STS 0-23, ID 2,3 EC1 Line 0-23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[23:0]`|`glbmsk`| global mask| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Alarm Status Mask Report VT/TU3

* **Description**           

This register is used to get POH alarm mask report.


* **RTL Instant Name**    : `alm_msklo`

* **Address**             : `0x0E_0000`

* **Formula**             : `0x0E_0000 + $STS*32 + $OCID*4096 + $VTID`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-1) : Line ID, ID 0,1 for path STS 0-23`

    * `$VTID(0-27)  : VT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[11]`|`bersdmsk`| bersd mask| `RW`| `0x0`| `0x0`|
|`[10]`|`bersfmsk`| bersf  mask| `RW`| `0x0`| `0x0`|
|`[9]`|`erdimsk`| erdi mask| `RW`| `0x0`| `0x0`|
|`[8]`|`bertcamsk`| bertca mask| `RW`| `0x0`| `0x0`|
|`[7]`|`jnstbmsk`| jn stable mask| `RW`| `0x0`| `0x0`|
|`[6]`|`pslstbmsk`| psl stable mask| `RW`| `0x0`| `0x0`|
|`[5]`|`rfimsk`| rfi mask| `RW`| `0x0`| `0x0`|
|`[4]`|`timmsk`| tim mask| `RW`| `0x0`| `0x0`|
|`[3]`|`uneqmsk`| uneq mask| `RW`| `0x0`| `0x0`|
|`[2]`|`plmmsk`| plm mask| `RW`| `0x0`| `0x0`|
|`[1]`|`aismsk`| ais mask| `RW`| `0x0`| `0x0`|
|`[0]`|`lopmsk`| lop mask| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Alarm Status Report VT/TU3

* **Description**           

This register is used to get POH alarm status.


* **RTL Instant Name**    : `alm_stalo`

* **Address**             : `0x0E_0800`

* **Formula**             : `0x0E_0800 + $STS*32 + $OCID*4096 + $VTID`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-1) : Line ID, ID 0,1 for path STS 0-23`

    * `$VTID(0-27)  : VT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[11]`|`bersdsta`| bersd status| `RW`| `0x0`| `0x0`|
|`[10]`|`bersfsta`| bersf  status| `RW`| `0x0`| `0x0`|
|`[9]`|`erdista`| erdi status status| `RO`| `0x0`| `0x0`|
|`[8]`|`bertcasta`| bertca status| `RO`| `0x0`| `0x0`|
|`[7]`|`jnstbsta`| jn stable status| `RO`| `0x0`| `0x0`|
|`[6]`|`pslstbsta`| psl stable status| `RO`| `0x0`| `0x0`|
|`[5]`|`rfista`| rfi status| `RO`| `0x0`| `0x0`|
|`[4]`|`timsta`| tim status| `RO`| `0x0`| `0x0`|
|`[3]`|`uneqsta`| uneq status| `RO`| `0x0`| `0x0`|
|`[2]`|`plmsta`| plm status| `RO`| `0x0`| `0x0`|
|`[1]`|`aissta`| ais status| `RO`| `0x0`| `0x0`|
|`[0]`|`lopsta`| lop status| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Status Report STS

* **Description**           

This register is used to get POH alarm change status report.


* **RTL Instant Name**    : `alm_chglo`

* **Address**             : `0x0E_0400`

* **Formula**             : `0x0E_0400 + $STS*32 + $OCID*4096 + $VTID`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-1) : Line ID, ID 0,1 for path STS 0-23`

    * `$VTID(0-27)  : VT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[11]`|`bersdstachg`| bersd stable status change| `W1C`| `0x0`| `0x0`|
|`[10]`|`bersfstachg`| bersf stable status change| `W1C`| `0x0`| `0x0`|
|`[9]`|`erdistachg`| erdi status change| `W1C`| `0x0`| `0x0`|
|`[8]`|`bertcastachg`| bertca status change| `W1C`| `0x0`| `0x0`|
|`[7]`|`jnstbstachg`| jn stable status change| `W1C`| `0x0`| `0x0`|
|`[6]`|`pslstbstachg`| psl stable status change| `W1C`| `0x0`| `0x0`|
|`[5]`|`rfistachg`| rfi status change| `W1C`| `0x0`| `0x0`|
|`[4]`|`timstachg`| tim status change| `W1C`| `0x0`| `0x0`|
|`[3]`|`uneqstachg`| uneq status change| `W1C`| `0x0`| `0x0`|
|`[2]`|`plmstachg`| plm status change| `W1C`| `0x0`| `0x0`|
|`[1]`|`aisstachg`| ais status change| `W1C`| `0x0`| `0x0`|
|`[0]`|`lopstachg`| lop status change| `W1C`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Or Status Report VT/TU3

* **Description**           

This register is used to get POH alarm or status change status report.


* **RTL Instant Name**    : `alm_orstalo`

* **Address**             : `0x0E_0C00`

* **Formula**             : `0x0E_0C00 + $STS + $OCID*4096`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-1) : Line ID, ID 0,1 for path STS 0-23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:0]`|`orstachg`| or status change bit| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Status Report VT/TU3

* **Description**           

This register is used to get POH alarm global change status report.


* **RTL Instant Name**    : `alm_glbchglo`

* **Address**             : `0x0E_0FFF`

* **Formula**             : `0x0E_0FFF + $OCID*4096`

* **Where**               : 

    * `$OCID(0-1) : Line ID, ID 0,1 for path STS 0-23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[23:0]`|`glbstachg`| global status change bit| `RO`| `0x0`| `0x0 // End: ###################################################################################### Begin:`|

###POH Interrupt Global Mask Report VT/TU3

* **Description**           

This register is used to get POH alarm global mask report.


* **RTL Instant Name**    : `alm_glbmsklo`

* **Address**             : `0x0E_0FFE`

* **Formula**             : `0x0E_0FFE + $OCID*4096`

* **Where**               : 

    * `$OCID(0-1) : Line ID, ID 0,1 for path STS 0-23`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[23:0]`|`glbmsk`| global status change bit| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Mask Report

* **Description**           

This register is used to get POH alarm global mask report for high,low order.


* **RTL Instant Name**    : `alm_glbmsk`

* **Address**             : `0x00_0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:16]`|`glbmsklo`| global mask bit for low order slice - ocid1,ocid0| `RW`| `0x0`| `0x0`|
|`[15:0]`|`glbmskhi`| global mask change bit for high order slice - ocid3,ocid1,ocid2,ocid0| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Status Report

* **Description**           

This register is used to get POH alarm global change status report for high,low order.


* **RTL Instant Name**    : `alm_glbchg`

* **Address**             : `0x00_0005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:16]`|`glbstachglo`| global status change bit for low order slice - ocid1,ocid0| `RO`| `0x0`| `0x0`|
|`[15:0]`|`glbstachghi`| global status change bit for high order slice - ocid3,ocid1,ocid2,ocid0| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt  Global Status Out Report

* **Description**           

This register is used to get POH alarm global change status report for high,low order after ANDED with mask.


* **RTL Instant Name**    : `alm_glbchgo`

* **Address**             : `0x00_0006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:16]`|`glbstachglo`| global status change bit for low order slice - ocid1,ocid0| `RO`| `0x0`| `0x0`|
|`[15:0]`|`glbstachghi`| global status change bit for high order slice - ocid3,ocid1,ocid2,ocid0| `RO`| `0x0`| `0x0 // End:`|

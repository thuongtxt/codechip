## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-05-11|AF6Project|Initial version|




##AF6CNC0011_RD_TOP
####Register Table

|Name|Address|
|-----|-----|
|`Device Product ID`|`0xF0_0000`|
|`Device Year Month Day Version ID`|`0xF0_0001`|
|`Device Internal ID`|`0xF0_0003`|
|`general purpose inputs for TX Uart`|`0x40`|
|`Flow Control Mechanism for Ethernet pass-through Diagnostic Value`|`0x41`|
|`o_control2`|`0x42`|
|`Diagnostic Enable Control`|`0x43`|
|`Serdes Turning Bus Select for XFI`|`0x44`|
|`Configure Clock monitor output`|`0x49`|
|`general purpose outputs from RX Uart`|`0x60`|
|`unused`|`0x61`|
|`Top Status`|`0x62`|
|`Top Interface Alarm Sticky`|`0x50`|
|`Clock Monitoring Status`|`0x46000 - 0x4601F`|
|`Clock Monitoring Status`|`0x47000 - 0x4701f`|


###Device Product ID

* **Description**           

This register indicates Product ID.


* **RTL Instant Name**    : `ProductID`

* **Address**             : `0xF0_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`productid`| ProductId| `RO`| `0x60290011`| `0x60290011 End: Begin:`|

###Device Year Month Day Version ID

* **Description**           

This register indicates Year Month Day and main version ID.


* **RTL Instant Name**    : `YYMMDD_VerID`

* **Address**             : `0xF0_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`year`| Year| `RO`| `0x16`| `0x16`|
|`[23:16]`|`month`| Month| `RO`| `0x09`| `0x09`|
|`[15:8]`|`day`| Day| `RO`| `0x17`| `0x17`|
|`[7:0]`|`version`| Version| `RO`| `0x01`| `0x01 End: Begin:`|

###Device Internal ID

* **Description**           

This register indicates internal ID.


* **RTL Instant Name**    : `InternalID`

* **Address**             : `0xF0_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`internalid`| InternalID| `RO`| `0x1`| `0x1 End: Begin:`|

###general purpose inputs for TX Uart

* **Description**           

This is the global configuration register for the Global Serdes Control


* **RTL Instant Name**    : `o_control0`

* **Address**             : `0x40`

* **Formula**             : `0x40`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:09]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[08:08]`|`tx_trig_send`| Uart TX trig start send out byte| `RW`| `0x0`| `0x0`|
|`[07:00]`|`tx_data_out`| Uart TX 8-bit data out| `RW`| `0x0`| `0x0 End : Begin:`|

###Flow Control Mechanism for Ethernet pass-through Diagnostic Value

* **Description**           

This is the global configuration register for Flow Control Mechanism for Ethernet pass-through Diagnostic


* **RTL Instant Name**    : `o_control1`

* **Address**             : `0x41`

* **Formula**             : `0x41`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End : Begin:`|

###o_control2

* **Description**           

This is the global configuration Error register for Flow Control Mechanism for Ethernet pass-through Diagnostic


* **RTL Instant Name**    : `o_control2`

* **Address**             : `0x42`

* **Formula**             : `0x42`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End : Begin:`|

###Diagnostic Enable Control

* **Description**           

This is the global configuration register for the MIG (DDR/QDR) Diagnostic Control


* **RTL Instant Name**    : `o_control3`

* **Address**             : `0x43`

* **Formula**             : `0x43`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`sgmii_sp_diagen`| Enable Daignostic for SGMII_SP 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[8]`|`sgmii_dcc_diagen`| Enable Daignostic for SGMII_DCC 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[7]`|`sgmii_dim_diagen`| Enable Daignostic for SGMII_DIM 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[6]`|`basex2500_diagen`| Enable Daignostic for Basex2500 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[5]`|`stby_xfi_diagen`| Enable Daignostic for ACT_STBY 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[4]`|`act_xfi_diagen`| Enable Daignostic for ACT_XFI 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`ddr3_diagen`| Enable Daignostic for DDR#2 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[1]`|`ddr2_diagen`| Enable Daignostic for DDR#1 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[0]`|`ddr1_diagen`| Enable Daignostic for DDR#1 0: Disable 1: Enable| `RW`| `0x0`| `0x0 End : Begin:`|

###Serdes Turning Bus Select for XFI

* **Description**           

This is the global configuration register for the Global Serdes Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag port 1 to 8


* **RTL Instant Name**    : `o_control4`

* **Address**             : `0x44`

* **Formula**             : `0x44`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`xfi1_drp_en`| Select cpu control for serdes DRP or Global control of XFI_STBY 0: Global control 1: DRP access| `RW`| `0x0`| `0x0`|
|`[0]`|`xfi0_drp_en`| Select CPU control for serdes DRP or Global control of XFI_ACT 0: Global control 1: DRP access| `RW`| `0x0`| `0x0 End : Begin:`|

###Configure Clock monitor output

* **Description**           

This is the TOP global configuration register


* **RTL Instant Name**    : `o_control9`

* **Address**             : `0x49`

* **Formula**             : `0x49`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[04:00]`|`cfgrefpid`| Configure Source Clock monitor output for clock_mon 0x0 : pcie_refclk_div2 0x1 : System PLL 155.52Mhz output 0x2 : refin_prc 0x3 : ext_ref_timing 0x4 : xfi_156p25_refclk#0_div2 0x5 : xfi_156p25_refclk#1_div2 0x6 : spare_serdes_refclk_div2 0x7 : basex2500_ref_div2 0x8 : ddr3_refclk#1 0x9 : ddr3_refclk#2 0xa : ddr3_refclk#3| `RW`| `0x0`| `0x0 End : Begin:`|

###general purpose outputs from RX Uart

* **Description**           

This is value output from RX Uart


* **RTL Instant Name**    : `c_uart`

* **Address**             : `0x60`

* **Formula**             : `0x60`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[07:00]`|`i_status0`| Uart RX 8-bit data in| `RO`| `0x0`| `0x0 End : Begin:`|

###unused

* **Description**           




* **RTL Instant Name**    : `unused`

* **Address**             : `0x61`

* **Formula**             : `0x61`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End : Begin:`|

###Top Status

* **Description**           

Top Interface Alarm Status


* **RTL Instant Name**    : `status_top`

* **Address**             : `0x62`

* **Formula**             : `0x62`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:26]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[25]`|`ot_out`| Current Status  Over-Temperature alarm .| `RO`| `0x0`| `0x0`|
|`[24]`|`vbram_alarm_out`| Current Status  VCCBRAM-sensor alarm .| `RO`| `0x0`| `0x0`|
|`[23]`|`vccaux_alarm_out`| Current Status  VCCAUX-sensor alarm .| `RO`| `0x0`| `0x0`|
|`[22]`|`vccint_alarm_out`| Current Status  VCCINT-sensor alarm .| `RO`| `0x0`| `0x0`|
|`[21]`|`user_temp_alarm_out`| Current Status  Temperature-sensor alarm .| `RO`| `0x0`| `0x0`|
|`[20]`|`alarm_out`| Current Status  SysMon Error .| `RO`| `0x0`| `0x0`|
|`[19:17]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[16]`|`systempll`| Current Status System PLL not locked. 0: Locked 1: Unlocked| `RO`| `0x0`| `0x0`|
|`[15:11]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[10]`|`ddr33urst`| Current Status DDR#3 user Reset. 0: OutReseset 1: InReset| `RO`| `0x0`| `0x0`|
|`[9]`|`ddr32urst`| Current Status DDR#2 user Reset. 0: OutReseset 1: InReset| `RO`| `0x0`| `0x0`|
|`[8]`|`ddr31urst`| Current Status DDR#1 user Reset. 0: OutReseset 1: InReset| `RO`| `0x0`| `0x0`|
|`[7:3]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[2]`|`ddr33calib`| Current Status DDR#3 Calib . 0: Calib PASS 1: Calib FAIL| `RO`| `0x0`| `0x0`|
|`[1]`|`ddr32calib`| Current Status DDR#2 Calib . 0: Calib PASS 1: Calib FAIL| `RO`| `0x0`| `0x0`|
|`[0]`|`ddr31calib`| Current Status DDR#1 Calib . 0: Calib PASS 1: Calib FAIL| `RO`| `0x0`| `0x0 End : Begin:`|

###Top Interface Alarm Sticky

* **Description**           

Top Interface Alarm Sticky


* **RTL Instant Name**    : `i_sticky0`

* **Address**             : `0x50`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:26]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[25]`|`ot_out`| Set 1 When Over-Temperature alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[24]`|`vbram_alarm_out`| Set 1 When VCCBRAM-sensor alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[23]`|`vccaux_alarm_out`| Set 1 When VCCAUX-sensor alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[22]`|`vccint_alarm_out`| Set 1 When VCCINT-sensor alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[21]`|`user_temp_alarm_out`| Set 1 When Temperature-sensor alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[20]`|`alarm_out`| Set 1 When SysMon Error Happens.| `W1C`| `0x0`| `0x0`|
|`[19:17]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[16]`|`systempll`| Set 1 while PLL Locked state change event happens when System PLL not locked.| `W1C`| `0x0`| `0x0`|
|`[15:11]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[10]`|`ddr33urst`| Set 1 while user Reset State Change Happens When DDR#3 Reset User.| `W1C`| `0x0`| `0x0`|
|`[9]`|`ddr32urst`| Set 1 while user Reset State Change Happens When DDR#2 Reset User.| `W1C`| `0x0`| `0x0`|
|`[8]`|`ddr31urst`| Set 1 while user Reset State Change Happens When DDR#1 Reset User.| `W1C`| `0x0`| `0x0`|
|`[7:3]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[2]`|`ddr33calib`| Set 1 while Calib State Change Event Happens When DDR#3 Calib Fail.| `W1C`| `0x0`| `0x0`|
|`[1]`|`ddr32calib`| Set 1 while Calib State Change Event Happens When DDR#2 Calib Fail.| `W1C`| `0x0`| `0x0`|
|`[0]`|`ddr31calib`| Set 1 while Calib State Change Event Happens When DDR#1 Calib Fail.| `W1C`| `0x0`| `0x0 End : Begin:`|

###Clock Monitoring Status

* **Description**           



0x0e : System clk 		     (155.52 Mhz)

0x0d : prc_ref clock        (155.52 Mhz)

0x0c : ext_ref clock        (155.52 Mhz)

0x0b : spgmii_user_clk      (125 Mhz)

0x0a : dccgmii_user_clk     (125 Mhz)

0x09 : dimgmii_user_clk     (125 Mhz)

0x08 : basex2500_user_clk	 (312.5 Mhz)

0x07 : 10g_stby_rxclk     	 (156.25 Mhz)

0x06 : 10g_act_rxclk     	 (156.25 Mhz)

0x05 : 10g_stby_txclk     	 (156.25 Mhz)

0x04 : 10g_act_txclk     	 (156.25 Mhz)

0x03 : DDR3#3 user clock    (200 Mhz)

0x02 : DDR3#2 user clock    (200 Mhz)

0x01 : DDR3#1 user clock    (200 Mhz)

0x00 : pcie_upclk clock     (62.5 Mhz)


* **RTL Instant Name**    : `clock_mon_high`

* **Address**             : `0x46000 - 0x4601F`

* **Formula**             : `0x46000 + port_id`

* **Where**               : 

    * `$port_id(0-31`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`i_statusx`| Clock Value Monitor Change from hex format to DEC format to get Clock Value| `RO`| `0x0`| `0x0 End : Begin:`|

###Clock Monitoring Status

* **Description**           



0x1f : unused (0 Mhz)

0x1e : unused (0 Mhz)

0x1d : unused (0 Mhz)

0x1c : pm_tick              (1 Hz) for get value frequency SW must use : F = 155520000/register value

0x1b : unused (0 Mhz)

0x1a : unused (0 Mhz)

0x19 : unused (0 Mhz)

0x18 : unused (0 Mhz)

0x17 : unused (0 Mhz)

0x16 : unused (0 Mhz)

0x15 : unused (0 Mhz)

0x14 : unused (0 Mhz)

0x13 : unused (0 Mhz)

0x12 : unused (0 Mhz)

0x11 : unused (0 Mhz)

0x10 : unused (0 Mhz)

0x0f : unused (0 Mhz)

0x0e : unused (0 Mhz)

0x0d : unused (0 Mhz)

0x0c : fsm_pcp_2   		(1 Mhz or 2 Mhz  frequency clk Diagnostic)

0x0b : fsm_pcp_1   		(1 Mhz or 2 Mhz  frequency clk Diagnostic)

0x0a : fsm_pcp_0   		(1 Mhz or 2 Mhz  frequency clk Diagnostic)

0x09 : spare_gpio_7        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x08 : spare_gpio_6        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x07 : spare_gpio_5        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x06 : spare_gpio_4        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x05 : spare_gpio_3        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x04 : spare_gpio_2        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x03 : spare_gpio_1        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x02 : spare_gpio_0        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x01 : spare_clk_1         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x00 : spare_clk_0         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)


* **RTL Instant Name**    : `clock_mon_slow`

* **Address**             : `0x47000 - 0x4701f`

* **Formula**             : `0x47000 + port_id`

* **Where**               : 

    * `$port_id(0-31`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`i_statusx`| Clock Value Monitor Change from hex format to DEC format to get Clock Value| `RO`| `0x0`| `0x0 End :`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_PDA
####Register Table

|Name|Address|
|-----|-----|
|`Pseudowire PDA Jitter Buffer Control`|`0x00003000 - 0x0000329F`|
|`Pseudowire Transmit Ethernet PAD Control`|`0x0001000`|
|`Pseudowire Transmit Ethernet PAD Control`|`0x0001001`|
|`4.1.4. Pseudowire PDA Reorder Control`|`0x0004000-0x0000429F #The address format for these registers is 0x0004000 + $PWID`|
|`Pseudowire PDA TDM Mode Control`|`0x0005000-0x0000529F #The address format for these registers is 0x0005000 + $PWID`|
|`Pseudowire PDA Idle Code Control`|`0x00005C00`|
|`Receive OAM CPU Segment Information Status`|`0x00002000`|
|`Pseudowire PDA PW PRBS Generation Control`|`0x007000`|
|`Pseudowire PDA PW PRBS Monitor Control`|`0x007800`|
|`Pseudowire PDA EPAR Control`|`0x00009000 - 0x0000929F #The address format for these registers is 0x00009000 + $PWID`|


###Pseudowire PDA Jitter Buffer Control

* **Description**           

This register sets jitter buffer parameters


* **RTL Instant Name**    : `jitbuf_ctrl`

* **Address**             : `0x00003000 - 0x0000329F`

* **Formula**             : `0x00003000  + $PwId`

* **Where**               : 

    * `$PwId(0-671) Pseudowire Identification`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[56:48]`|`pwlopspk`| Number of consecutive empty packets to declare LOPS (lost of packet synchronization) follow rfc4842| `RW`| `0x0`| `0x0`|
|`[47:34]`|`pwpayloadlen`| Payload length of receive pseudowire packet, set 0 for ATM PW| `RW`| `0x0`| `0x0`|
|`[33:29]`|`pwsetlofs`| Number of consecutive lost packets to enter lost of frame state(LOFS), set 0 for ATM PW| `RW`| `0x0`| `0x0`|
|`[28:24]`|`pwclearlofs`| Number of consecutive good packets to exit lost of frame state(LOFS), set 0 for ATM PW| `RW`| `0x0`| `0x0`|
|`[23:12]`|`pdvsizepk`| Set 0 for ATM PW. This parameter is to prevent packet delay variation(PDV) from PSN. PdvSizePk is packet delay variation measured in packet unit. The formula is as follow: PdvSizePk = (Pw64kbpsSpeed * PDVus)/(125*PwPayloadLen) + ((Pw64kbpsSpeed * PDVus)%(125*PwPayloadLen) !=0) Case: Pseudowire speed =   Pw64kbpsSpeed * 64kbps PDVus is packet delay variation in microsecond unit. This value should be multiple of 125us for more accurate| `RW`| `0x0`| `0x0`|
|`[11:0]`|`jitbufsizepk`| Set default value for ATM PW. Jitter buffer size measured in packet unit. The formula is as follow In common: JitBufSizePk = 2 * PdvSizePk When user want to configure PDV and jitter buffer as independent parameters, the formular is: JitBufSizePk = (Pw64kbpsSpeed * JITBUFus)/(125*PwPayloadLen) + ((Pw64kbpsSpeed * JITBUFus)%(125*PwPayloadLen) !=0) where: Pseudowire speed =   Pw64kbpsSpeed * 64kbps JITBUFus is jitter buffer size measured in microsecond unit. This value should be multiple of 125us for more accurate| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Ethernet PAD Control

* **Description**           




* **RTL Instant Name**    : `jitbuf_hot_cfg_ctrl1`

* **Address**             : `0x0001000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:1]`|`pwid`| pwid| `RW`| `0x0`| `0x0`|
|`[0]`|`enable`| enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Ethernet PAD Control

* **Description**           




* **RTL Instant Name**    : `jitbuf_hot_cfg_ctrl2`

* **Address**             : `0x0001001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:1]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`ready`| ready| `RW`| `0x0`| `0x0 End: Begin:`|

###4.1.4. Pseudowire PDA Reorder Control

* **Description**           

Used for TDM PW. This register configures RTP SSRC value.


* **RTL Instant Name**    : `reorder_ctrl`

* **Address**             : `0x0004000-0x0000429F #The address format for these registers is 0x0004000 + $PWID`

* **Formula**             : `0x0004000 +  $PWID`

* **Where**               : 

    * `$PWID(0-671): Pseudowire ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:12]`|`pwreorpdv`| This value is exact the same as above PdvSizePkt parameter, in case PDVSizePkt > 32 packets, set PwReorPdv to 32 because reorder only support maximum 32 packets| `// RW`| `0x0`| `0x0`|
|`[11]`|`pwreoren`| 0: Enable reorder pseudowire packets from Ethernet side before sending to jitter buffer 1: Disable reorder function| `RW`| `0x0`| `0x0`|
|`[10:0]`|`pwreortimeout`| Timer to detect lost of packet sequence. The formula to calculate PwReorTimeOut value from PdvSizePk (defined in Pseudowire PDA Jitter Buffer Control register) is as follow: PwReorTimeout = (Min(32,PdvSizePk) * PwPayloadLen)/ Pw64kbpsSpeed where  Pseudowire speed =   Pw64kbpsSpeed * 64kbps| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA TDM Mode Control

* **Description**           

The register configures modes of TDM Payload De-Assembler function


* **RTL Instant Name**    : `tdm_mode_ctrl`

* **Address**             : `0x0005000-0x0000529F #The address format for these registers is 0x0005000 + $PWID`

* **Formula**             : `0x0005000 +  $PWID`

* **Where**               : 

    * `$PWID(0-671): Pseudowire ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[27:22]`|`pdanxds0`| Number of DS0 allocated for CESoP PW| `RW`| `0x0`| `0x0`|
|`[21:14]`|`pdaidlecode`| Idle pattern to replace data in case of Lost/Lbit packet| `RW`| `0x0`| `0x0`|
|`[13]`|`pdaaisuneqoff`| 0: Enable sending AIS/Uneq alarms from PW to TDM (default) 1: Disable sending AIS/Uneq alarms from PW to TDM| `RW`| `0x0`| `0x0`|
|`[12:11]`|`pdarepmode`| Mode to replace lost packets 0: Replace by replaying previous good packet (often for voice or video application) 1: Replace by AIS (default) 2: Replace by a configuration idle code| `RW`| `0x0`| `0x0`|
|`[10]`|`pdaslice`| 0: Slice 0, sts 0-23 (Only for DE3 card mapping port 0-23 into 0-23 of engine#0) 1: Slice 1, sts 24-47 (Only for DE3 card mapping port 24-47 into 0-23 of engine#1)| `RW`| `0x0`| `0x0`|
|`[9:5]`|`pdafirststsid`| In case of TOH PW  & CES PW: don't care In case of DE3 SAToP PW: this is the STS ID itself In case of  CEP basic , VC4-4c basic: don't care In case of  VC3  fractional (similar STS-1) : this is the STS ID itself In case of  VC4  fractional (similar STS-3) : must be 0(group sts 0,8,16) or 1(group sts 1,9,17) or 2(group sts 2,10,18) or 3(group sts 3,11,19)    to group 7 (total 8 group)| `RW`| `0x0`| `0x0`|
|`[4:1]`|`pdamode`| TDM Payload De-Assembler modes 0: SAToP PW 1: TOH PW (similar to SAToP) 2: CESoP without CAS PW 3: CESoP with CAS PW 8: VC3/VC4/VC4-4c basic 9: VC3 fractional VC11/VC12 10: VC3 asynchronous T3/E3 11: VC4 fractional VC11/VC12/VC3 12: VC11/VC12 basic 15: DE3 SAToP Others: Reserved| `RW`| `0x0`| `0x0`|
|`[0]`|`pdaebmen`| In case of VC3/VC4 fractional, we have option EBM enable or not, EBM is field at CEP control word to indicated which VT is unequipped 0: Disable 1: Enable (default)| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA Idle Code Control

* **Description**           

The register configures idle code for TDM Payload De-Assembler replay function


* **RTL Instant Name**    : `idle_code_ctrl`

* **Address**             : `0x00005C00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`pdaidlecode`| Idle code value| `RW`| `0x0`| `0x0 End: Begin:`|

###Receive OAM CPU Segment Information Status

* **Description**           

This register stores information of OAM packet that will be read and processed by CPU. CPU would serve this address by regularly polling or by scanning OAM interrupt. PDA divides an //received OAM packet into 32-byte segments. CPU has to serve this address to know whether a segment is ready to be read. When a segment (not EOP segment) has just been fullly read by CPU, the maximum //time needed for PDA to prepare next segment is 25 microseconds. CPU can depend on this parameter for polling state machine


* **RTL Instant Name**    : `oam_cpu_seg_info_sta`

* **Address**             : `0x00002000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:3]`|`rxcpuoamseglen`| Length in byte of OAM segment. Value 0 indicates a 1-byte segment, value 31 indicate a 32-byte segment and so on. This field is useful in case of EOP segment| `//        RW`| `0x0`| `0x0`|
|`[2]`|`rxcpuoamsegsop`| Set 1 to indicate this is an SOP segment| `RW`| `0x0`| `0x0`|
|`[1]`|`rxcpuoamsegeop`| Set 1 to indicate this is an EOP segment.| `RW`| `0x0`| `0x0`|
|`[0]`|`rxcpuoamsegrdy`| Set 1 to indicate a segment data is ready to be read by CPU| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA PW PRBS Generation Control

* **Description**           

The register configures modes of PW PRBS Generation function


* **RTL Instant Name**    : `pw_pda_pw_prbs_gen_ctrl`

* **Address**             : `0x007000`

* **Formula**             : `0x007000 + 32768*slice + PwId`

* **Where**               : 

    * `$slice(0-0): is OC24 slice`

    * `$PwId (0 - 671): Pseudowire Identification`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`prbsdata`| Prbs data random| `RW`| `0x0`| `0x0`|
|`[2]`|`prbserror`| Force PRBS Error| `RW`| `0x0`| `0x0`|
|`[1]`|`prbsmode`| Prbs modes 0: Prbs15 (x15 + x14 + 1) 1: Prbs31 (x31 + x28 + 1)| `RW`| `0x0`| `0x0`|
|`[0]`|`prbsenb`| 0: Disable 1: Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA PW PRBS Monitor Control

* **Description**           

The register configures modes of PW PRBS Generation function


* **RTL Instant Name**    : `pw_pda_pw_prbs_mon_ctrl`

* **Address**             : `0x007800`

* **Formula**             : `0x007800 + 32768*slice + PwId`

* **Where**               : 

    * `$slice(0-0): is OC24 slice`

    * `$PwId (0 - 671): Pseudowire Identification`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17]`|`prbserror`| Force PRBS Error Sticky, Read to clear| `RW`| `0x0`| `0x0`|
|`[16]`|`prbssync`| 0: Prbs Loss Status 1: Prbs Sync Status| `RW`| `0x0`| `0x0`|
|`[15:1]`|`prbsdata`| Prbs data random| `RW`| `0x0`| `0x0`|
|`[0]`|`prbsmode`| Prbs modes 0: Prbs15 (x15 + x14 + 1) 1: Prbs31 (x31 + x28 + 1)| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA EPAR Control

* **Description**           

This register show jitter buffer status


* **RTL Instant Name**    : `epar_ctrl`

* **Address**             : `0x00009000 - 0x0000929F #The address format for these registers is 0x00009000 + $PWID`

* **Formula**             : `0x00009000 +  $PWID`

* **Where**               : 

    * `$PWID(0-671): Pseudowire ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11]`|`oeparslice`| OC24 Slice| `RW`| `0x0`| `0x0`|
|`[10:1]`|`oeparlid`| OC24 TDM ID| `RW`| `0x0`| `0x0`|
|`[0]`|`oeparen`| EPAR enable| `RW`| `0x0`| `0x0 End:`|

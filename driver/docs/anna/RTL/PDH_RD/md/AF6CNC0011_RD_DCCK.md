## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_DCCK
####Register Table

|Name|Address|
|-----|-----|
|`Global Interrupt Status`|`0x02001`|
|`DCC ETH2OCN Direction Interupt Status Reg0`|`0x02004`|
|`DCC ETH2OCN Direction Interupt Status Reg1`|`0x02005`|
|`DCC ETH2OCN Direction Interupt Status Reg1`|`0x02006`|
|`DCC OCN2ETH Direction Interupt Buffer Full Status`|`0x02008`|
|`DCC OCN2ETH Direction Interupt CRC Error Status`|`0x02009`|
|`DCC OCN2ETH Direction Interupt Oversize Length Status`|`0x0200D`|
|`DCC OCN2ETH Direction Interupt Status`|`0x0200E`|
|`Loopback Enable Configuration`|`0x00001`|
|`Global Interrupt Enable`|`0x00002`|
|`DCC ETH2OCN Direction Interupt Enable Reg0`|`0x00004`|
|`DCC ETH2OCN Direction Interupt Enable Reg1`|`0x00005`|
|`DCC ETH2OCN Direction Interupt Enable Reg2`|`0x00006`|
|`DCC OCN2ETH Direction Interupt Enable Reg0`|`0x00008`|
|`DCC OCN2ETH Direction Interupt Enable Reg0`|`0x00009`|
|`DCC OCN2ETH Direction Interupt Enable Reg2`|`0x0000D`|
|`DCC OCN2ETH Direction Interupt Enable Reg0`|`0x0000E`|
|`Dcc_Fcs_Rem_Mode`|`0x10003`|
|`DDC_TX_Header_Per_Channel`|`0x11400 - 0x117FF`|
|`DDC_Channel_Enable`|`0x11000`|
|`DDC_RX_Global_ProvisionedHeader_Configuration`|`0x12001`|
|`DDC_RX_MAC_Check_Enable_Configuration`|`0x12002`|
|`DDC_RX_CVLAN_Check_Enable_Configuration`|`0x12003`|
|`DDC_RX_Channel_Mapping`|`0x12400 - 0x1242F`|
|`DCC_OCN2ETH_Pkt_Length_Alarm_Sticky`|`0x11004`|
|`DCC_OCN2ETH_Pkt_Error_And_Buffer_Full_Alarm_Sticky`|`0x11005`|
|`DCC_ETH2OCN_Alarm_Sticky`|`0x12008`|
|`DCC_OCN2ETH_Min_Pkt_Length_Alarm_Status`|`0x11008`|
|`DCC_OCN2ETH_Max_Pkt_Length_Alarm_Status`|`0x11009`|
|`DCC_OCN2ETH_Pkt_CRC_Error_Alarm_Status`|`0x1100A`|
|`DCC_OCN2ETH_Buffer_Full_Alarm_Status`|`0x1100B`|
|`DCC_ETH2OCN_Alarm_Status`|`0x1200A`|
|`Counter_Rx_Eop_From_SGMII_Port1`|`0x12021`|
|`Counter_Rx_Err_From_SGMII_Port1`|`0x12022`|
|`Counter_Rx_Byte_From_SGMII_Port1`|`0x12023`|
|`Counter_Rx_Unknow_From_SGMII_Port1`|`0x12024`|
|`Counter_Tx_Eop_To_SGMII_Port1`|`0x11011`|
|`Counter_Tx_Err_To_SGMII_Port1`|`0x11012`|
|`Counter_Tx_Byte_To_SGMII_Port1`|`0x11013`|
|`Counter_Packet_And_Byte_Per_Channel`|`0x13000 - 0x13FFF`|
|`CONFIG HDLC LO DEC`|`0x04000 - 0x04037`|
|`CONFIG HDLC GLOBAL LO DEC`|`0x04404`|
|`HDLC Encode Master Control`|`0x30003`|
|`HDLC Encode Control Reg 1`|`0x38000 - 0x38037`|
|`HDLC Encode Control Reg 2`|`0x39000 - 0x39037`|
|`Enable Packet Capture`|`0x0A000`|
|`Captured Packet Data`|`0x08000 - 0x080FF`|
|`DDC_Test_Sdh_Req_Interval_Configuration`|`0x00003`|
|`DDC_Config_Test_Gen_Header_Per_Channel`|`0x0C100 - 0x0C120`|
|`DDC_Config_Test_Gen_Mode_Per_Channel`|`0x0C200 - 0x0C220`|
|`DDC_Config_Test_Gen_Global_Gen_Enable_Channel`|`0x0C000`|
|`DDC_Config_Test_Gen_Global_Gen_Mode`|`0x0C001`|
|`DDC_Config_Test_Gen_Global_Packet_Length`|`0x0C002`|
|`DDC_Config_Test_Gen_Global_Gen_Interval`|`0x0C003`|
|`DDC_Test_Mon_Good_Packet_Counter`|`0x0E100 - 0x0E120`|
|`DDC_Test_Mon_Error_Data_Packet_Counter`|`0x0E200 - 0x0E220`|
|`DDC_Test_Mon_Error_VCG_Packet_Counter`|`0x0E300 - 0x0E320`|
|`DDC_Test_Mon_Error_SEQ_Packet_Counter`|`0x0E400 - 0x0E420`|
|`DDC_Test_Mon_Error_FCS_Packet_Counter`|`0x0E500 - 0x0E520`|
|`DDC_Test_Mon_Abort_VCG_Packet_Counter`|`0x0E600 - 0x0E620`|


###Global Interrupt Status

* **Description**           

The register provides global interrupt status


* **RTL Instant Name**    : `upen_glbint_stt`

* **Address**             : `0x02001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:01]`|`kbyte_interrupt`| interrupt from KByte	event| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`dcc_interrupt`| interrupt from DCC event| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC ETH2OCN Direction Interupt Status Reg0

* **Description**           

The register provides interrupt status of DCC event ETH2OCN direction


* **RTL Instant Name**    : `upen_int_rxdcc0_stt`

* **Address**             : `0x02004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`dcc_channel_disable`| DCC Local Channel Identifier mapping is disable Bit[00] -> Bit[47] indicate channel 0 -> 47.| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC ETH2OCN Direction Interupt Status Reg1

* **Description**           

The register provides interrupt status of DCC event ETH2OCN direction


* **RTL Instant Name**    : `upen_int_rxdcc1_stt`

* **Address**             : `0x02005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:03]`|`dcc_sgm_ovrsize_len`| Received DCC packet's length from SGMII port over maximum allowed length	(1318 bytes)| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`dcc_sgm_crc_error`| Received packet from SGMII port has FCS error| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`dcc_cvlid_mismat`| Received 12b CVLAN ID value of DCC frame different from global provisioned CVID| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`dcc_macda_mismat`| Received 43b MAC DA value of DCC frame different from global provisioned DA| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC ETH2OCN Direction Interupt Status Reg1

* **Description**           

The register provides interrupt status of DCC event ETH2OCN direction


* **RTL Instant Name**    : `upen_int_rxdcc2_stt`

* **Address**             : `0x02006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`dcc_eth2ocn_blk_ept`| DCC packet buffer for ETH2OCN direction was fulled, some packets will be lost. Bit[00] -> Bit[47] indicate channel 0 -> 47.| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC OCN2ETH Direction Interupt Buffer Full Status

* **Description**           

The register provides interrupt status of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_int_txdcc0_stt`

* **Address**             : `0x02008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`dcc_ocn2eth_blk_ept`| DCC packet buffer for OCN2ETH direction was fulled, some packets will be lost Bit[00] -> Bit[47] indicate channel 0 -> 47.| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC OCN2ETH Direction Interupt CRC Error Status

* **Description**           

The register provides interrupt status of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_int_txdcc1_stt`

* **Address**             : `0x02009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`dcc_hdlc_crc_error`| Received HDLC frame from OCN has FCS error| `W1C`| `0x0`| `0x0 Bit[00] -> Bit[47] indicate channel 0 -> 47.`|

###DCC OCN2ETH Direction Interupt Oversize Length Status

* **Description**           

The register provides interrupt status of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_int_txdcc2_stt`

* **Address**             : `0x0200D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`dcc_hdlc_ovrsize_len`| Received HDLC packet's length from OCN overed maximum allowed length	(1536 bytes)| `W1C`| `0x0`| `0x0 Bit[00] -> Bit[47] indicate channel 0 -> 47.`|

###DCC OCN2ETH Direction Interupt Status

* **Description**           

The register provides interrupt status of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_int_txdcc3_stt`

* **Address**             : `0x0200E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`dcc_hdlc_undsize_len`| Received HDLC packet's length from OCN undered minimum allowed length (2 bytes)| `W1C`| `0x0`| `0x0 Bit[00] -> Bit[47] indicate channel 0 -> 47.`|

###Loopback Enable Configuration

* **Description**           

The register provides loopback enable configuration


* **RTL Instant Name**    : `upen_loopen`

* **Address**             : `0x00001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`dcc_buffer_loop_en`| Enable loopback of RX-BUFFER to TX-BUFFER      .<br>{1}: enable. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[04:04]`|`genmon_loop_en`| Enable loopback of data from DCC generator to RX-DCC  .<br>{1}: enable. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[03:03]`|`ksdh_loop_en`| Enable loopback of Kbyte information           .<br>{1}: enable. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[02:02]`|`ksgm_loop_en`| Enable SGMII loopback of Kbyte Port.<br>{1}: enable. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[01:01]`|`hdlc_loop_en`| Enable loopback from HDLC Encap to HDLC DEcap of DCC byte.<br>{1}: enable. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[00:00]`|`dsgm_loop_en`| Enable SGMII loopback of DCC Port.<br>{1}: enable. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Global Interrupt Enable

* **Description**           

The register provides configuration to enable global interrupt


* **RTL Instant Name**    : `upen_glb_intenb`

* **Address**             : `0x00002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[01:01]`|`kbyte_interrupt`| Enable interrupt for KByte events| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`dcc_interrupt`| Enable interrupt for DCC event| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC ETH2OCN Direction Interupt Enable Reg0

* **Description**           

The register provides configuration to enable interrupt of DCC events ETH2OCN direction


* **RTL Instant Name**    : `upen_rxdcc_intenb_r0`

* **Address**             : `0x00004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`enb_int_dcc_chan_dis`| Enable Interrupt of DCC Local Channel Identifier mapping per channel Bit[00] -> Bit[47] indicate channel 0 -> 47.<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC ETH2OCN Direction Interupt Enable Reg1

* **Description**           

The register provides configuration to enable interrupt of DCC events ETH2OCN direction


* **RTL Instant Name**    : `upen_rxdcc_intenb_r1`

* **Address**             : `0x00005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[03:03]`|`enb_int_dcc_sgm_ovrsize_len`| Enable Interrupt of "Received DCC packet's length from SGMII port over maximum allowed length (1318 bytes)"| `RW`| `0x0`| `0x0`|
|`[02:02]`|`enb_int_dcc_sgm_crc_error`| Enable Interrupt of "Received packet from SGMII port has FCS error"| `RW`| `0x0`| `0x0`|
|`[01:01]`|`enb_int_dcc_cvlid_mismat`| Enable Interrupt of "Received 12b CVLAN ID value of DCC frame different from global provisioned CVID "<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[00:00]`|`enb_int_dcc_macda_mismat`| Enable Interrupt of "Received 43b MAC DA value of DCC frame different from global provisioned DA"<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC ETH2OCN Direction Interupt Enable Reg2

* **Description**           

The register provides configuration to enable interrupt of DCC events ETH2OCN direction


* **RTL Instant Name**    : `upen_rxdcc_intenb_r2`

* **Address**             : `0x00006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`enb_int_dcc_eth2ocn_blk_ept`| Enable Interrupt of "DCC packet buffer for ETH2OCN direction was fulled, some packets will be lost "	per channel Bit[00] -> Bit[47] indicate channel 0 -> 47.<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC OCN2ETH Direction Interupt Enable Reg0

* **Description**           

The register provides configuration to enable interrupt of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_txdcc_intenb_ept`

* **Address**             : `0x00008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`enb_int_dcc_ocn2eth_blk_ept`| Enable Interrupt of "DCC packet buffer for OCN2ETH direction was fulled, some packets will be lost" per channel Bit[00] -> Bit[47] indicate channel 0 -> 47.<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC OCN2ETH Direction Interupt Enable Reg0

* **Description**           

The register provides configuration to enable interrupt of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_txdcc_intenb_crc`

* **Address**             : `0x00009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`enb_int_dcc_hdlc_crc_error`| Enable Interrupt of "Received HDLC frame from OCN has FCS error	" per channel Bit[00] -> Bit[47] indicate channel 0 -> 47.<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC OCN2ETH Direction Interupt Enable Reg2

* **Description**           

The register provides configuration to enable interrupt of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_txdcc_intenb_ovr`

* **Address**             : `0x0000D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`enb_int_dcc_hdlc_ovrsize_len`| Enable Interrupt of "Received HDLC packet's length from OCN overed maximum allowed length (1536 bytes)	" per channel Bit[00] -> Bit[47] indicate channel 0 -> 47.<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC OCN2ETH Direction Interupt Enable Reg0

* **Description**           

The register provides configuration to enable interrupt of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_txdcc_intenb_undsize_len`

* **Address**             : `0x0000E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`enb_int_dcc_hdlc_undsize_len`| Enable Interrupt of "Received HDLC packet's length from OCN undered minimum allowed length (2 bytes) " per channel Bit[00] -> Bit[47] indicate channel 0 -> 47.<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: End: Begin:`|

###Dcc_Fcs_Rem_Mode

* **Description**           

The register provides configuration to remove FCS32 or FCS16


* **RTL Instant Name**    : `upen_dcctx_fcsrem`

* **Address**             : `0x10003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`fcs_remove_mode`| Remove 4 bytes FCS32 or 2byte FCS16 This configuration depend on the FCS checking mode at HDLC DEC (which is configed at reg address : 0x04000 - 0x04037)<br>{1}: Remove 4bytes FCS32 <br>{0}: Remove 2bytes FCS16| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_TX_Header_Per_Channel

* **Description**           

The register provides data for configuration of 22bytes Header of each channel ID, in which, only 16byte is configurable

the configuration postion of header byte is as follow; DA(6byte) + SA(6byte) + {PCP,DEI,VID}(2byte) + VERSION(1byte) + TYPE(1byte)

hdrpos = 0 is position of DA[47:40] and hdrpos = 15 is position of TYPE field


* **RTL Instant Name**    : `upen_dcchdr`

* **Address**             : `0x11400 - 0x117FF`

* **Formula**             : `0x11400 + $channelid*16 + $hdrpos`

* **Where**               : 

    * `$channelid(0-47): Channel ID`

    * `$hdrpos(0-15):header's byte position`

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`header_pos`| 8b value of header per Channel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Channel_Enable

* **Description**           

The register provides configuration for enable transmission DDC packet per channel


* **RTL Instant Name**    : `upen_dcctx_enacid`

* **Address**             : `0x11000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`channel_enable`| Enable transmitting of DDC packet per channel. Bit[47:00] represent for channel 47->00<br>{1}: enable channel to transmit  DDC byte <br>{0}: disable channel to transmit DDC byte| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_RX_Global_ProvisionedHeader_Configuration

* **Description**           

The register provides data for configuration of 22bytes Header of each channel ID


* **RTL Instant Name**    : `upen_dccrxhdr`

* **Address**             : `0x12001`

* **Formula**             : `0x12001`

* **Where**               : 

* **Width**               : `70`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[69:54]`|`eth_typ`| 16b value of Provisioned ETHERTYPE of DCC| `RW`| `0x0`| `0x880B`|
|`[53:42]`|`cvid`| 12b value of Provisioned C-VLAN ID. This value is used to compared with received CVLAN ID value.| `RW`| `0x0`| `0x0`|
|`[41:00]`|`mac_da`| 42b MSB of Provisioned MAC DA value. This value is used to compared with received MAC_DA[47:06] value. If a match is confirmed, MAC_DA[05:00] is used to represent channelID value before mapping.| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_RX_MAC_Check_Enable_Configuration

* **Description**           

The register provides configuration that enable base MAC DA check per channel


* **RTL Instant Name**    : `upen_dccrxmacenb`

* **Address**             : `0x12002`

* **Formula**             : `0x12002`

* **Where**               : 

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`mac_check_enable`| Enable channel checking of received MAC DA compare to globally provisioned Base MAC.On mismatch, frame is discarded.<br>{1}:enable, <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_RX_CVLAN_Check_Enable_Configuration

* **Description**           

The register provides configuration that enable base CVLAN Check


* **RTL Instant Name**    : `upen_dccrxcvlenb`

* **Address**             : `0x12003`

* **Formula**             : `0x12003`

* **Where**               : 

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`cvl_check_enable`| Enable channel checking of received CVLAN ID compare to globally provisioned CVLAN ID.On mismatch, frame is discarded.<br>{1}:enable, <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_RX_Channel_Mapping

* **Description**           

The register provides channel mapping from received MAC DA to internal channelID


* **RTL Instant Name**    : `upen_dccdec`

* **Address**             : `0x12400 - 0x1242F`

* **Formula**             : `0x12400 + $channelid`

* **Where**               : 

    * `$channelid(0-47): Channel ID`

* **Width**               : `7`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[06:06]`|`channel_enable`| Enable Local Channel Identifier. Rx packet is discarded if enable is not set<br>{0}: disable. <br>{1}: Enable| `RW`| `0x0`| `0x0`|
|`[05:00]`|`mapping_channelid`| Local ChannelID that is mapped from received bit[5:0] of MAC DA| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC_OCN2ETH_Pkt_Length_Alarm_Sticky

* **Description**           

The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)


* **RTL Instant Name**    : `upen_stkerr_pktlen`

* **Address**             : `0x11004`

* **Formula**             : `0x11004`

* **Where**               : 

* **Width**               : `96`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[95:48]`|`dcc_overize_err`| HDLC Length Oversize Error. Alarm per channel Received HDLC Frame from OCN has frame length over maximum allowed length (1536bytes) Bit[95] -> Bit[48]: channel ID 47 ->0| `W1C`| `0x0`| `0x0`|
|`[47:00]`|`dcc_undsize_err`| HDLC Length Undersize Error. Alarm per channel Received HDLC Frame from OCN has frame length below minimum allowed length (2bytes) Bit[47] -> Bit[0]: channel ID 47 ->0| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC_OCN2ETH_Pkt_Error_And_Buffer_Full_Alarm_Sticky

* **Description**           

The register provides Alarm Related to HDLC CRC Error and Buffer Full (OCN2ETH Direction)


* **RTL Instant Name**    : `upen_stkerr_crcbuf`

* **Address**             : `0x11005`

* **Formula**             : `0x11005`

* **Where**               : 

* **Width**               : `96`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[95:48]`|`dcc_crc_err`| HDLC Packet has CRC error (OCN to ETH direction) . Alarm per channel Bit[95] -> Bit[48]: channel ID 47 ->0| `W1C`| `0x0`| `0x0`|
|`[47:00]`|`dcc_buffull_err`| HDLC Packet buffer full (OCN to ETH direction) . Alarm per channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[47] -> Bit[0]: channel ID 47 ->0| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC_ETH2OCN_Alarm_Sticky

* **Description**           

The register provides Alarms of ETH2OCN Direction


* **RTL Instant Name**    : `upen_stkerr_rx_eth2ocn`

* **Address**             : `0x12008`

* **Formula**             : `0x12008`

* **Where**               : 

* **Width**               : `102`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[101:53]`|`dcc_eth2ocn_buffull_err`| Packet buffer full (ETH to OCN direction) . Alarm per channel Buffer of Ethernet Frame has been full. Some frames will be dropped Bit[101] -> Bit[53]: channel ID 47 ->0| `W1C`| `0x0`| `0x0`|
|`[52:52]`|`dcc_rxeth_maxlenerr`| Received DCC packet from SGMII port has violated maximum packet's length error| `W1C`| `0x0`| `0x0`|
|`[51:51]`|`dcc_rxeth_crcerr`| Received DCC packet from SGMII port has CRC error| `W1C`| `0x0`| `0x0`|
|`[50:03]`|`dcc_channel_disable`| DCC Local Channel Identifier mapping is disable Bit[50] -> Bit[03] indicate channel 47-> 00.| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`dcc_cvlid_mismat`| Received 12b CVLAN ID value of DCC frame different from global provisioned CVID| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`dcc_macda_mismat`| Received 43b MAC DA value of DCC frame different from global provisioned DA| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`dcc_ethtp_mismat`| Received Ethernet Type of DCC frame different from global provisioned value| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC_OCN2ETH_Min_Pkt_Length_Alarm_Status

* **Description**           

The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)


* **RTL Instant Name**    : `upen_curerr_pktlen1`

* **Address**             : `0x11008`

* **Formula**             : `0x11008`

* **Where**               : 

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`dcc_undsize_err`| HDLC Length Undersize Error. Status per channel Received HDLC Frame from OCN has frame length below minimum allowed length (2bytes) Bit[47] -> Bit[0]: channel ID 47 ->0| `RO`| `0x0`| `0x0 End: Begin:`|

###DCC_OCN2ETH_Max_Pkt_Length_Alarm_Status

* **Description**           

The register provides Alarm Status Related to HDLC Length Error (OCN2ETH Direction)


* **RTL Instant Name**    : `upen_curerr_pktlen2`

* **Address**             : `0x11009`

* **Formula**             : `0x11009`

* **Where**               : 

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`dcc_overize_err`| HDLC Length Oversize Error. Status per channel Received HDLC Frame from OCN has frame length over maximum allowed length (1536bytes) Bit[47] -> Bit[0]: channel ID 47 ->0| `RO`| `0x0`| `0x0 End: Begin:`|

###DCC_OCN2ETH_Pkt_CRC_Error_Alarm_Status

* **Description**           

The register provides Alarm Status Related to HDLC CRC Error (OCN2ETH Direction)


* **RTL Instant Name**    : `upen_curerr_crcbuf`

* **Address**             : `0x1100A`

* **Formula**             : `0x1100A`

* **Where**               : 

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`dcc_crc_err`| HDLC Packet has CRC error (OCN to ETH direction) . Alarm per channel Bit[47] -> Bit[0]: channel ID 47 ->0| `RO`| `0x0`| `0x0 End: Begin:`|

###DCC_OCN2ETH_Buffer_Full_Alarm_Status

* **Description**           

The register provides Alarm Related to HDLC Buffer Full (OCN2ETH Direction)


* **RTL Instant Name**    : `upen_curerr_bufept`

* **Address**             : `0x1100B`

* **Formula**             : `0x1100B`

* **Where**               : 

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`dcc_buffull_err`| HDLC Packet buffer full (OCN to ETH direction) . Alarm per channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[47] -> Bit[0]: channel ID 47 ->0| `RO`| `0x0`| `0x0 End: Begin:`|

###DCC_ETH2OCN_Alarm_Status

* **Description**           

The register provides Alarms of ETH2OCN Direction


* **RTL Instant Name**    : `upen_dcc_rx_glb_sta`

* **Address**             : `0x1200A`

* **Formula**             : `0x1200A`

* **Where**               : 

* **Width**               : `52`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[51:51]`|`sta_dcc_rxeth_maxlenerr`| "Received DCC packet from SGMII port has violated maximum packet's length error" status| `RO`| `0x0`| `0x0`|
|`[50:03]`|`sta_dcc_channel_disable`| "DCC Local Channel Identifier mapping is disable" status Bit[50] -> Bit[03] indicate channel 47-> 00.| `RO`| `0x0`| `0x0`|
|`[02:02]`|`sta_dcc_cvlid_mismat`| Received 12b CVLAN ID value of DCC frame different from global provisioned CVID| `RO`| `0x0`| `0x0`|
|`[01:01]`|`sta_dcc_macda_mismat`| Received 43b MAC DA value of DCC frame different from global provisioned DA| `RO`| `0x0`| `0x0`|
|`[00:00]`|`sta_dcc_ethtp_mismat`| Received Ethernet Type of DCC frame different from global provisioned value| `RO`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Eop_From_SGMII_Port1

* **Description**           

Counter of number of EOP receive from SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmrxeop`

* **Address**             : `0x12021`

* **Formula**             : `0x12021`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`eop_counter`| Counter of EOP receive from SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Err_From_SGMII_Port1

* **Description**           

Counter of number of ERR receive from SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmrxerr`

* **Address**             : `0x12022`

* **Formula**             : `0x12022`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`err_counter`| Counter of ERR receive from SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Byte_From_SGMII_Port1

* **Description**           

Counter of number of BYTE receive from SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmrxbyt`

* **Address**             : `0x12023`

* **Formula**             : `0x12023`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`byte_counter`| Counter of BYTE receive from SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Unknow_From_SGMII_Port1

* **Description**           

Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmrxfail`

* **Address**             : `0x12024`

* **Formula**             : `0x12024`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`unk_counter`| Counter of unknow receive packet from SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_Eop_To_SGMII_Port1

* **Description**           

Counter of number of EOP transmit to SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmtxeop`

* **Address**             : `0x11011`

* **Formula**             : `0x11011`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sgm_txglb_eop_counter`| Counter of EOP transmit to SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_Err_To_SGMII_Port1

* **Description**           

Counter of number of ERR transmit to SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmtxerr`

* **Address**             : `0x11012`

* **Formula**             : `0x11012`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sgm_txglb_err_counter`| Counter of ERR  transmit to SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_Byte_To_SGMII_Port1

* **Description**           

Counter of number of BYTE transmit to SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmtxbyt`

* **Address**             : `0x11013`

* **Formula**             : `0x11013`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sgm_txglb_byte_counter`| Counter of TX BYTE to SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Packet_And_Byte_Per_Channel

* **Description**           

Counter of DCC at different point represent by cnt_type value:

{1} : counter RX bytes from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction

{2} : counter RX good packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction

{3} : counter RX error packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction

{4} : counter RX lost packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction

{5} : counter TX bytes from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction

{6} : counter TX packets from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction

{7} : counter TX error packet from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction

{8} : counter RX bytes from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction

{9} : counter RX good packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction

{10}: counter RX error packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction

{11}: counter RX lost packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction

{12}: counter TX bytes from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction

{13}: counter TX good packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction

{14}: counter TX error packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction


* **RTL Instant Name**    : `upen_dcc_cnt`

* **Address**             : `0x13000 - 0x13FFF`

* **Formula**             : `0x13000 + $cnt_type*64 + $channelID`

* **Where**               : 

    * `$cnt_type(1-14): counter type`

    * `$channelID(0-47): channel value`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_counter`| Counter of DCC packet and byte| `WC`| `0x0`| `0x0 End: Begin:`|

###CONFIG HDLC LO DEC

* **Description**           

config HDLC ID 0-31

HDL_PATH: iaf6cci0012_lodec_core.ihdlc_cfg.imem113x.ram.ram[$CID]


* **RTL Instant Name**    : `upen_hdlc_locfg`

* **Address**             : `0x04000 - 0x04037`

* **Formula**             : `0x04000+ $CID`

* **Where**               : 

    * `$CID (0-47) : Channel ID`

* **Width**               : `5`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[4]`|`cfg_scren`| config to enable scramble, (1) is enable, (0) is disable| `R/W`| `0x0`| `0x0`|
|`[3]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[2]`|`cfg_bitstuff`| config to select bit stuff or byte sutff, (1) is bit stuff, (0) is byte stuff| `R/W`| `0x0`| `0x0`|
|`[1]`|`cfg_fcsmsb`| config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB| `R/W`| `0x0`| `0x0`|
|`[0]`|`cfg_fcsmode`| config to calculate FCS 32 or FCS 16, (1) is FCS 32, (0) is FCS 16| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG HDLC GLOBAL LO DEC

* **Description**           

config HDLC global


* **RTL Instant Name**    : `upen_hdlc_loglbcfg`

* **Address**             : `0x04404`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3]`|`cfg_lsbfirst`| config to receive LSB first, (1) is LSB first, (0) is default MSB| `R/W`| `0x0`| `0x0`|
|`[2:0]`|`unused`| *n/a*| *n/a*| *n/a*| `End: Begin:`|

###HDLC Encode Master Control

* **Description**           

config HDLC Encode Master


* **RTL Instant Name**    : `upen_hdlc_enc_master_ctrl`

* **Address**             : `0x30003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`encap_lsbfirst`| config to transmit LSB first, (1) is LSB first, (0) is default MSB| `R/W`| `0x0`| `0x0 End: Begin:`|

###HDLC Encode Control Reg 1

* **Description**           

config HDLC Encode Control Register 1


* **RTL Instant Name**    : `upen_hdlc_enc_ctrl_reg1`

* **Address**             : `0x38000 - 0x38037`

* **Formula**             : `0x38000+ $CID`

* **Where**               : 

    * `$CID (0-47) : Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[30:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:09]`|`encap_screnb`| Enable Scamble (1) Enable      , (0) disable| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`reserved`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`encap_idlemod`| This bit is only used in Bit Stuffing mode Used to configured IDLE Mode When it is active, the ENC engine will insert '1' pattern when the ENC is idle. Otherwise the ENC will insert FLAG '7E' pattern (1) Enable            , (0) Disabe| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`encap_sabimod`| Sabi/Protocol Field Mode (1) Field has 2 bytes , (0) field has 1 byte| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`encap_sabiins`| Sabi/Protocol Field Insert Enable (1) Enable Insert     , (0) Disable Insert| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`encap_ctrlins`| Address/Control Field Insert Enable (1) Enable Insert     , (0) Disable Insert| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`encap_fcsmod`| FCS Select Mode (1) 32b FCS           , (0) 16b FCS| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`encap_fcsins`| FCS Insert Enable (1) Enable Insert     , (0) Disable Insert| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`encap_flgmod`| Flag Mode (1) Minimum 2 Flag    , (0) Minimum 1 Flag| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`encap_stfmod`| Stuffing Mode (1) Bit Stuffing      , (0) Byte Stuffing| `R/W`| `0x0`| `0x0 End: Begin:`|

###HDLC Encode Control Reg 2

* **Description**           

config HDLC Encode Control Register 2 Byte Stuff Mode


* **RTL Instant Name**    : `upen_hdlc_enc_ctrl_reg2`

* **Address**             : `0x39000 - 0x39037`

* **Formula**             : `0x39000+ $CID`

* **Where**               : 

    * `$CID (0-47) : Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`encap_addrval`| Address Field| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`encap_ctlrval`| Control Field| `R/W`| `0x0`| `0x0`|
|`[15:08]`|`encap_sapival0`| SAPI/PROTOCOL Field 1st byte| `R/W`| `0x0`| `0x0`|
|`[07:00]`|`encap_sapival1`| SAPI/PROTOCOL Field 2nd byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###Enable Packet Capture

* **Description**           

config enable capture ethernet packet.


* **RTL Instant Name**    : `upen_trig_encap`

* **Address**             : `0x0A000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[03:03]`|`ram_trig_fls`| Trigger flush packets captured RAM Write '0' first, then write '1' to trigger flush RAM process| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`enb_cap_dec`| Enable capture packets from HDLC Decapsulation. Only one type of packet (DCC/APS/HDLC) is captured at a time Write '0' first, then write '1' to trigger capture process (1) Enable            , (0) Disable| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`enb_cap_aps`| Enable capture APS packets from SGMII interface. Only one type of packet (DCC/APS/HDLC) is captured at a time Write '0' first, then write '1' to trigger capture process (1) Enable            , (0) Disable| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`enb_cap_dcc`| Enable capture DCC packets from SGMII interface. Only one type of packet (DCC/APS/HDLC) is captured at a time Write '0' first, then write '1' to trigger capture process (1) Enable            , (0) Disable| `R/W`| `0x0`| `0x0 End: Begin:`|

###Captured Packet Data

* **Description**           

Captured packet data


* **RTL Instant Name**    : `upen_pktcap`

* **Address**             : `0x08000 - 0x080FF`

* **Formula**             : `0x08000 + $loc`

* **Where**               : 

    * `$loc(0-255) : data location`

* **Width**               : `70`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[69:69]`|`cap_sop`| Start of packet| `RO`| `0x0`| `0x0`|
|`[68:68]`|`cap_eop`| End   of packet| `RO`| `0x0`| `0x0`|
|`[67:67]`|`cap_err`| Error of packet| `RO`| `0x0`| `0x0`|
|`[66:64]`|`cap_nob`| Number of valid bytes in 8-bytes data captured| `RO`| `0x0`| `0x0`|
|`[63:00]`|`cap_dat`| packet data captured| `RO`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Sdh_Req_Interval_Configuration

* **Description**           

The register is used to configure time interval to generate fake SDH request signal


* **RTL Instant Name**    : `upen_genmon_reqint`

* **Address**             : `0x00003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `29`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28:28]`|`timer_enable`| Enable generation of fake request SDH signal| `RW`| `0x0`| `0x0`|
|`[27:00]`|`timer_value`| Counter of number of clk 155MHz between fake request SDH signal. This counter is used make a delay interval between 2 consecutive SDH requests generated by DCC GENERATOR. For example: to create a delay of 125us between SDH request signals, with a clock 155Mz, the value of counter to be configed to this field is (125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Config_Test_Gen_Header_Per_Channel

* **Description**           

The register provides data for configuration of VID and MAC DA Header of each channel ID


* **RTL Instant Name**    : `upen_dcc_cfg_testgen_hdr`

* **Address**             : `0x0C100 - 0x0C120`

* **Formula**             : `0x0C100 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `20`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:08]`|`hdr_vid`| 12b of VID value in header of Generated Packet| `RW`| `0x0`| `0x0`|
|`[07:00]`|`mac_da`| Bit [7:0] of MAC DA value [47:0] of generated Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Config_Test_Gen_Mode_Per_Channel

* **Description**           

The register provides data for configuration of generation mode of each channel ID


* **RTL Instant Name**    : `upen_dcc_cfg_testgen_mod`

* **Address**             : `0x0C200 - 0x0C220`

* **Formula**             : `0x0C200 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`gen_payload_mod`| Modes of generated packet payload:<br>{0}: payload will increase <br>{1}: payload is PRBS <br>{2}: payload is fill with 8b pattern configed in gen_fix_pattern field of this register| `RW`| `0x0`| `0x0`|
|`[29:28]`|`gen_len_mode`| Modes of generated packet length :<br>{0}: fix length <br>{1}: increase length <br>{2}: random length| `RW`| `0x0`| `0x0`|
|`[27:20]`|`gen_fix_pattern`| 8b fix pattern payload if mode "fix payload" is used| `RW`| `0x0`| `0x0`|
|`[19:00]`|`gen_number_of_packet`| Number of packets the GEN generates for each channelID in gen burst mode| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Config_Test_Gen_Global_Gen_Enable_Channel

* **Description**           

The register provides configuration to enable Channel to generate DCC packets


* **RTL Instant Name**    : `upen_dcc_cfg_testgen_enacid`

* **Address**             : `0x0C000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`channel_enable`| Enable Channel to generate DCC packets. Bit[31:0] <-> Channel 31->0| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Config_Test_Gen_Global_Gen_Mode

* **Description**           

The register provides global configuration of GEN mode


* **RTL Instant Name**    : `upen_dcc_cfg_testgen_glb_gen_mode`

* **Address**             : `0x0C001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[06:06]`|`gen_force_err_len`| Create wrong packet's length fiedl generated packets| `RW`| `0x0`| `0x0`|
|`[05:05]`|`gen_force_err_fcs`| Create wrong FCS field in generated packets| `RW`| `0x0`| `0x0`|
|`[04:04]`|`gen_force_err_dat`| Create wrong data value in generated packets| `RW`| `0x0`| `0x0`|
|`[03:03]`|`gen_force_err_seq`| Create wrong Sequence field in generated packets| `RW`| `0x0`| `0x0`|
|`[02:02]`|`gen_force_err_vcg`| Create wrong VCG field in generated packets| `RW`| `0x0`| `0x0`|
|`[01:01]`|`gen_burst_mod`| Gen each Channel a number of packet as configured in "Gen mode per channel" resgister| `RW`| `0x0`| `0x0`|
|`[00:00]`|`gen_conti_mod`| Gen packet forever without stopping| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Config_Test_Gen_Global_Packet_Length

* **Description**           

The register provides configuration min length max length of generated Packets


* **RTL Instant Name**    : `upen_dcc_cfg_testgen_glb_length`

* **Address**             : `0x0C002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`gen_max_length`| Maximum length of generated packet| `RW`| `0x0`| `0x0`|
|`[15:00]`|`gen_min_length`| Minimum length of generated packet, also used for fix length packets in case fix length mode is used| `RW`| `0x0`| `0x0 Begin:`|

###DDC_Config_Test_Gen_Global_Gen_Interval

* **Description**           

The register provides configuration for packet generating interval


* **RTL Instant Name**    : `upen_dcc_cfg_testgen_glb_gen_interval`

* **Address**             : `0x0C003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `29`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28:28]`|`gen_intv_enable`| Enable using this interval value| `RW`| `0x0`| `0x0`|
|`[27:00]`|`gen_intv_value`| Counter of number of clk 155MHz between packet generation. This counter is used make a delay interval between 2 consecutive packet generation. For example: to create a delay of 125us between 2 packet generating, with a clock 155Mz, the value of counter to be configed to this field is (125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Mon_Good_Packet_Counter

* **Description**           

Counter of receive good packet


* **RTL Instant Name**    : `upen_mon_godpkt`

* **Address**             : `0x0E100 - 0x0E120`

* **Formula**             : `0x0E100 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_test_mon_good_cnt`| Counter of Receive Good Packet from TEST GEN| `WC`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Mon_Error_Data_Packet_Counter

* **Description**           

Counter of received packets has error data


* **RTL Instant Name**    : `upen_mon_errpkt`

* **Address**             : `0x0E200 - 0x0E220`

* **Formula**             : `0x0E200 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_test_mon_errdat_cnt`| Counter of Receive Error Data Packet from TEST GEN| `WC`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Mon_Error_VCG_Packet_Counter

* **Description**           

Counter of received packet has wrong VCG value


* **RTL Instant Name**    : `upen_mon_errvcg`

* **Address**             : `0x0E300 - 0x0E320`

* **Formula**             : `0x0E300 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_test_mon_errvcg_cnt`| Counter of Receive Error VCG Packet from TEST GEN| `WC`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Mon_Error_SEQ_Packet_Counter

* **Description**           

Counter of received packet has wrong Sequence value


* **RTL Instant Name**    : `upen_mon_errseq`

* **Address**             : `0x0E400 - 0x0E420`

* **Formula**             : `0x0E400 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_test_mon_errseq_cnt`| Counter of Receive Error Sequence Packet from TEST GEN| `WC`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Mon_Error_FCS_Packet_Counter

* **Description**           

Counter of received packet has wrong FCS value


* **RTL Instant Name**    : `upen_mon_errfcs`

* **Address**             : `0x0E500 - 0x0E520`

* **Formula**             : `0x0E500 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_test_mon_errfcs_cnt`| Counter of Receive Error FCS Packet from TEST GEN| `WC`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Mon_Abort_VCG_Packet_Counter

* **Description**           

Counter of received packet has been abbort due to wrong length information


* **RTL Instant Name**    : `upen_mon_abrpkt`

* **Address**             : `0x0E600 - 0x0E620`

* **Formula**             : `0x0E600 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_test_mon_abrpkt_cnt`| Counter of Abbort Packet from TEST GEN| `WC`| `0x0`| `0x0 End:`|

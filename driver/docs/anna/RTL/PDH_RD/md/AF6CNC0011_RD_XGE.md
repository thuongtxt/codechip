## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-01-02|AF6Project|Initial version|




##AF6CNC0011_RD_XGE
####Register Table

|Name|Address|
|-----|-----|
|`FPGA version`|`0xF00000`|
|`FPGA name`|`0xF00001`|
|`FPGA soft reset`|`0xF00004`|
|`FPGA system alarm`|`0xF00052`|
|`XFI PRBS Pattern Generate Configure`|`0x0 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Pattern Packet Number Configure`|`0x1 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Pattern Packet lengthg Configure`|`0x2 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Pattern Packet Data Configure`|`0x3 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Pattern Packet generate Counter RO`|`0x10 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Pattern Packet generate Counter R2C`|`0x20 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Pattern Packet analyzer PRBS Sticky Error`|`0xB00 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Pattern Packet analyzer PRBS Status Error`|`0xB01 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Pattern Packet analyzer Data Configure`|`0xA01 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkttotal`|`0x800 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pktgood`|`0x801 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pktfcserr`|`0x802 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length err`|`0x803 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt PRBS Data err`|`0x807 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length 0-64`|`0x80A Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length 65-128`|`0x80B Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length 129-256`|`0x80C Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length 257-512`|`0x80D Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length 513-1024`|`0x80E Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length 1025-2048`|`0x80F Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkttotal`|`0x900 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pktgood`|`0x901 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pktfcserr`|`0x902 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length err`|`0x903 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt PRBS Data err`|`0x907 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length 0-64`|`0x90A Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length 65-128`|`0x90B Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length 129-256`|`0x90C Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length 257-512`|`0x90D Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length 513-1024`|`0x90E Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|
|`XFI PRBS Packet analyzer Counter pkt length 1025-2048`|`0x90F Base address : 0xF50000 - 0xF57000 <Port : 1-8>`|


###FPGA version

* **Description**           

Register to test CPU


* **RTL Instant Name**    : `version_pen`

* **Address**             : `0xF00000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Version`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`year`| Year| `R_O`| `0x0`| `0x15`|
|`[23:16]`|`month`| Month| `R_O`| `0x0`| `0x17`|
|`[15:8]`|`day`| Day| `R_O`| `0x0`| `0x14`|
|`[7:0]`|`version`| Version| `R_O`| `0x0`| `0x10`|

###FPGA name

* **Description**           

Register to test CPU


* **RTL Instant Name**    : `version_pen`

* **Address**             : `0xF00001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Version`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`product`| Product code| `R_O`| `0x0`| `0x60210051`|

###FPGA soft reset

* **Description**           

Register to test CPU


* **RTL Instant Name**    : `Master_pen`

* **Address**             : `0xF00004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : ` Write 1 to reset, write 0 to unreset`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`soft_reset`| FPGA soft reset internal| `R/W`| `0x0`| `0x0`|

###FPGA system alarm

* **Description**           

Register to test FPGA


* **RTL Instant Name**    : `Card_Status`

* **Address**             : `0xF00052`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`reserved`| *n/a*| `R/W/C`| `0x0`| `0x0`|
|`[15]`|`xfi_pcs_block_lock`| PCS Block status locked for port 8 <1 = RX is synchronized  / 0 = RX is not synchronized>| `R/W/C`| `0x0`| `0x0`|
|`[14]`|`xfi_pcs_block_lock`| PCS Block status locked for port 7 <1 = RX is synchronized  / 0 = RX is not synchronized>| `R/W/C`| `0x0`| `0x0`|
|`[13]`|`xfi_pcs_block_lock`| PCS Block status locked for port 6 <1 = RX is synchronized  / 0 = RX is not synchronized>| `R/W/C`| `0x0`| `0x0`|
|`[12]`|`xfi_pcs_block_lock`| PCS Block status locked for port 5 <1 = RX is synchronized  / 0 = RX is not synchronized>| `R/W/C`| `0x0`| `0x0`|
|`[11]`|`xfi_pcs_block_lock`| PCS Block status locked for port 4 <1 = RX is synchronized  / 0 = RX is not synchronized>| `R/W/C`| `0x0`| `0x0`|
|`[10]`|`xfi_pcs_block_lock`| PCS Block status locked for port 3 <1 = RX is synchronized  / 0 = RX is not synchronized>| `R/W/C`| `0x0`| `0x0`|
|`[9]`|`xfi_pcs_block_lock`| PCS Block status locked for port 2 <1 = RX is synchronized  / 0 = RX is not synchronized>| `R/W/C`| `0x0`| `0x0`|
|`[8]`|`xfi_pcs_block_lock`| PCS Block status locked for port 1 <1 = RX is synchronized  / 0 = RX is not synchronized>| `R/W/C`| `0x0`| `0x0`|
|`[7:5]`|`reserved`| *n/a*| `R/W/C`| `0x0`| `0x0`|
|`[4]`|`ser_init_done`| OCN Serdes init done| `R/W/C`| `0x0`| `0x0`|
|`[3]`|`mig1_2x64_init_calib_complete`| DDR4#1 calib_complete| `R/W/C`| `0x0`| `0x0`|
|`[2]`|`mig2_2x64_init_calib_complete`| DDR4#2 calib_complete| `R/W/C`| `0x0`| `0x0`|
|`[1]`|`mig2_2x36_init_calib_complete`| QDRII calib_complete| `R/W/C`| `0x0`| `0x0`|
|`[0]`|`syspll_locked`| System PLL locked status| `R/W/C`| `0x0`| `0x0`|

###XFI PRBS Pattern Generate Configure

* **Description**           

XFI PRBS Pattern Generate Configure


* **RTL Instant Name**    : `Configure_register`

* **Address**             : `0x0 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x0 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`cfgdainc`| Configure DA Address mode 1 : INC / 0: fixed| `R/W`| `0x0`| `0x0`|
|`[1]`|`cfglengthinc`| Configure length mode 1 : INC / 0: fixed| `R/W`| `0x0`| `0x0`|
|`[2]`|`cfgnumall`| Configure Packet number mode 1 : continuity   / 0: fixed| `R/W`| `0x0`| `0x1`|
|`[3]`|`cfgins_err`| Configure Packet Insert Error 1 : Insert / 0: Un-insert| `R/W`| `0x0`| `0x0`|
|`[4]`|`cfgforcsop`| Configure Force Start of packet 1 : Force / 0: Un-Force| `R/W`| `0x0`| `0x0`|
|`[5]`|`cfgforceop`| Configure Force End of packet 1 : Force / 0: Un-Force| `R/W`| `0x0`| `0x0`|
|`[15:6]`|`reserved`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[16]`|`cfggenen`| Configure PRBS packet test enable 1 : Enable / 0: Disnable| `R/W`| `0x0`| `0x0`|
|`[17]`|`flush`| Configure flush PRBS packet gen  1 : flush / 0: Un-flush| `R/W`| `0x0`| `0x0`|

###XFI PRBS Pattern Packet Number Configure

* **Description**           

XFI PRBS Pattern Packet Number Configure


* **RTL Instant Name**    : `Configure_register`

* **Address**             : `0x1 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x1 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cfgnumpkt`| Configure number of packet generate| `R/W`| `0x14`| `0x14`|

###XFI PRBS Pattern Packet lengthg Configure

* **Description**           

XFI PRBS Pattern Packet lengthg Configure


* **RTL Instant Name**    : `Configure_register`

* **Address**             : `0x2 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x2 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`cfglengthmin`| Configure length minimum of packet generate| `R/W`| `0x40`| `0x40`|
|`[31:16]`|`cfglengthmax`| Configure length maximum of packet generate| `R/W`| `0x5DC`| `0x5DC`|

###XFI PRBS Pattern Packet Data Configure

* **Description**           

XFI PRBS Pattern Packet Data Configure


* **RTL Instant Name**    : `Configure_register`

* **Address**             : `0x3 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x3 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`cfgdatfix`| Configure Fixed byte Data generate| `R/W`| `0x84`| `0x84`|
|`[9:8]`|`cfgdatmod`| Configure data mode generate  0x1 fixed data / 0x2 : PRBS31 data| `R/W`| `0x2`| `0x2`|

###XFI PRBS Pattern Packet generate Counter RO

* **Description**           

XFI PRBS Pattern Packet generate Counter RO


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x10 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x10 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`penr_opkttotal`| TX Packet Counterer| `R/O`| `0x0`| `0x0`|

###XFI PRBS Pattern Packet generate Counter R2C

* **Description**           

XFI PRBS Pattern Packet generate Counter R2C


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x20 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x20 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`penr2cpkttotal`| TX Packet Counterer| `R2C`| `0x0`| `0x0`|

###XFI PRBS Pattern Packet analyzer PRBS Sticky Error

* **Description**           

XFI PRBS Pattern Packet analyzer PRBS Sticky Error


* **RTL Instant Name**    : `Configure_register`

* **Address**             : `0xB00 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0xB00 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`err_stk`| Packet analyzer PRBS Sticky Error <0 = RX is synchronized /1 = RX is not synchronized>| `R/W/C`| `0x0`| `0x0`|

###XFI PRBS Pattern Packet analyzer PRBS Status Error

* **Description**           

XFI PRBS Pattern Packet analyzer PRBS Status Error


* **RTL Instant Name**    : `Configure_register`

* **Address**             : `0xB01 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0xB01 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`err_sta`| Packet analyzer PRBS Status Error <0 = RX is synchronized /1 = RX is not synchronized>| `RO`| `0x0`| `0x0`|

###XFI PRBS Pattern Packet analyzer Data Configure

* **Description**           

XFI PRBS Pattern Packet analyzer Data Configure


* **RTL Instant Name**    : `Configure_register`

* **Address**             : `0xA01 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0xA01 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28]`|`cfgfcsdrop`| Configure FCS Drop| `R/W`| `0x0`| `0x1`|
|`[11:8]`|`cfgdatmod`| Configure data mode analyzer  0x1 fixed data / 0x2 : PRBS31 data| `R/W`| `0x2`| `0x2`|

###XFI PRBS Packet analyzer Counter pkttotal

* **Description**           

XFI PRBS Packet analyzer Counter pkttotal


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x800 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x800 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pkttotal`| RX Packet total| `R/O`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pktgood

* **Description**           

XFI PRBS Packet analyzer Counter pktgood


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x801 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x801 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktgood`| RX Packet good| `R/O`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pktfcserr

* **Description**           

XFI PRBS Packet analyzer Counter pktfcserr


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x802 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x802 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktfcserr`| RX Packet FCS error| `R/O`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length err

* **Description**           

XFI PRBS Packet analyzer Counter pkt length err


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x803 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x803 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlengtherr`| RX Packet lengthgth error| `R/O`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt PRBS Data err

* **Description**           

XFI PRBS Packet analyzer Counter pkt PRBS Data err


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x807 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x807 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktdaterr`| RX Packet PRBS data error| `R/O`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 0-64

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 0-64


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x80A Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x80A Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen64`| RX Counter pkt length 0-64| `R/O`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 65-128

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 65-128


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x80B Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x80B Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen128`| RX Counter pkt length 65-128| `R/O`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 129-256

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 129-256


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x80C Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x80C Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen256`| RX Counter pkt length 129-256| `R/O`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 257-512

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 257-512


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x80D Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x80D Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen512`| RX Counter pkt length 257-215| `R/O`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 513-1024

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 513-1024


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x80E Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x80E Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen1024`| RX Counter pkt length 513-1024| `R/O`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 1025-2048

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 1025-2048


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x80F Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x80F Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen2048`| RX Counter pkt length 1025-2048| `R/O`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkttotal

* **Description**           

XFI PRBS Packet analyzer Counter pkttotal


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x900 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x900 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pkttotal`| RX Packet total| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pktgood

* **Description**           

XFI PRBS Packet analyzer Counter pktgood


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x901 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x901 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktgood`| RX Packet good| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pktfcserr

* **Description**           

XFI PRBS Packet analyzer Counter pktfcserr


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x902 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x902 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktfcserr`| RX Packet FCS error| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length err

* **Description**           

XFI PRBS Packet analyzer Counter pkt length err


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x903 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x903 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlengtherr`| RX Packet lengthgth error| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt PRBS Data err

* **Description**           

XFI PRBS Packet analyzer Counter pkt PRBS Data err


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x907 Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x907 Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktdaterr`| RX Packet PRBS data error| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 0-64

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 0-64


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x90A Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x90A Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen64`| RX Counter pkt length 0-64| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 65-128

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 65-128


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x90B Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x90B Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen128`| RX Counter pkt length 65-128| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 129-256

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 129-256


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x90C Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x90C Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen256`| RX Counter pkt length 129-256| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 257-512

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 257-512


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x90D Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x90D Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen512`| RX Counter pkt length 257-215| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 513-1024

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 513-1024


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x90E Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x90E Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen1024`| RX Counter pkt length 513-1024| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 1025-2048

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 1025-2048


* **RTL Instant Name**    : `Status_register`

* **Address**             : `0x90F Base address : 0xF50000 - 0xF57000 <Port : 1-8>`

* **Formula**             : `Base address + 0x90F Base address : 0xF50000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen2048`| RX Counter pkt length 1025-2048| `R2C`| `0x0`| `0x0`|

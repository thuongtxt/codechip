## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-03-09|AF6Project|Initial version|




##AF6CNC0021_GMII_RD_DIAG
####Register Table

|Name|Address|
|-----|-----|
|`MII Test Enable.`|`0x302`|
|`MII Channels Enable.`|`0x00 - 0x30`|
|`MII Burst test enable gen packet.`|`0x0f - 0x3f`|
|`MII Channels Sticky alarm.`|`0x01 - 0x31`|
|`MII Channels Status alarm.`|`0x0E - 0x3E`|
|`MII Channels Test mode control.`|`0x02 - 0x32`|
|`MII Channels Configure Mac DA MSB.`|`0x03 - 0x33`|
|`MII Channels Configure Mac DA LSB.`|`0x04 - 0x34`|
|`MII Channels Configure Mac SA MSB.`|`0x05 - 0x35`|
|`MII Channels Configure Mac SA LSB.`|`0x06 - 0x36`|
|`MII Channels Configure Mac Type.`|`0x07 - 0x37`|
|`MII Tx counter R2C.`|`0x08 - 0x38`|
|`MII Rx counter R2C.`|`0x09 - 0x39`|
|`MII FCS error counter R2C.`|`0x0A - 0x3A`|
|`MII PRBS error counter R2C.`|`0x401 - 0x431`|
|`MII Tx counter RO.`|`0x0B - 0x3B`|
|`MII Rx counter RO.`|`0x0C - 0x3C`|
|`MII FCS error counter RO.`|`0x0D - 0x3D`|
|`MII PRBS error counter RO.`|`0x400 - 0x430`|


###MII Test Enable.

* **Description**           

Register to enable test MII interface


* **RTL Instant Name**    : `control_pen9`

* **Address**             : `0x302`

* **Formula**             : `0x302`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `4`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`channel0_testen`| Set 1 to enable test MII channel 0| `R/W`| `0x0`| `0x0`|
|`[1]`|`channel1_testen`| Set 1 to enable test MII channel 1| `R/W`| `0x0`| `0x0`|
|`[2]`|`channel2_testen`| Set 1 to enable test MII channel 2| `R/W`| `0x0`| `0x0`|
|`[3]`|`channel3_testen`| Set 1 to enable test MII channel 3| `R/W`| `0x0`| `0x0 End : Begin:`|

###MII Channels Enable.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `control_pen1`

* **Address**             : `0x00 - 0x30`

* **Formula**             : `0x00 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`test_enable`| Set 1 to enable Tx test generator| `R/W`| `0x0`| `0x0`|
|`[1]`|`start_gatetime_diag`| Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration| `RW`| `0x0`| `0x0`|
|`[2]`|`status_gatetime_diag`| Status Gatetime diagnostic 1:Running 0:Done-Ready| `RO`| `0x0`| `0x0`|
|`[14:2]`|`unsed`| Unsed| `R/W`| `0x0`| `0x0`|
|`[31:15]`|`time_cfg`| Gatetime Configuration 1-86400 second| `RW`| `0x0`| `0x0 End : Begin:`|

###MII Burst test enable gen packet.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `control_pen2`

* **Address**             : `0x0f - 0x3f`

* **Formula**             : `0x0f + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`burst_gen_packet_number`| Burst gen packet number, must configure burst number > 0 to generator packet, hardware will clear number packet configure when finish.| `R/W`| `0x0`| `0x0 End : Begin:`|

###MII Channels Sticky alarm.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `Alarm_sticky`

* **Address**             : `0x01 - 0x31`

* **Formula**             : `0x01 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Sticky`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mon_packet_error`| Set 1 when monitor packet error.| `R/W/C`| `0x0`| `0x0`|
|`[1]`|`mon_packet_len_error`| Set 1 when monitor packet len error.| `R/W/C`| `0x0`| `0x0`|
|`[2]`|`mon_fcs_error`| Set 1 when monitor FCS error.| `R/W/C`| `0x0`| `0x0`|
|`[3]`|`mon_data_error`| Set 1 when monitor data error.| `R/W/C`| `0x0`| `0x0`|
|`[4]`|`mon_mac_header_error`| Set 1 when monitor MAC header error.| `R/W/C`| `0x0`| `0x0`|
|`[5]`|`mon_data_sync`| Set 1 when monitor Data sync.| `R/W/C`| `0x0`| `0x0`|
|`[31:6]`|`unused`| *n/a*| `R/W/C`| `0x0`| `0x0 End : Begin:`|

###MII Channels Status alarm.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `Alarm_status`

* **Address**             : `0x0E - 0x3E`

* **Formula**             : `0x0E + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mon_packet_error`| Set 1 when monitor packet error.| `RO`| `0x0`| `0x0`|
|`[1]`|`mon_packet_len_error`| Set 1 when monitor packet len error.| `RO`| `0x0`| `0x0`|
|`[2]`|`mon_fcs_error`| Set 1 when monitor FCS error.| `RO`| `0x0`| `0x0`|
|`[3]`|`mon_data_error`| Set 1 when monitor data error.| `RO`| `0x0`| `0x0`|
|`[4]`|`mon_mac_header_error`| Set 1 when monitor MAC header error.| `RO`| `0x0`| `0x0`|
|`[5]`|`mon_data_sync`| Set 1 when monitor Data sync.| `RO`| `0x0`| `0x0`|
|`[6]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[23:7]`|`currert_gatetime_diag`| Current running time of Gatetime diagnostic| `RO`| `0x0`| `0x0`|
|`[31:24]`|`unused`| *n/a*| `RO`| `0x0`| `0x0 End : Begin:`|

###MII Channels Test mode control.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `control_pen3`

* **Address**             : `0x02 - 0x32`

* **Formula**             : `0x02 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`loopout_enable`| FPGA loopout enable: 0/1 -> Dis/En.| `R/W`| `0x0`| `0x0`|
|`[1]`|`mac_header_check_enable`| Mac header check enable: 0/1 -> Dis/En.| `R/W`| `0x0`| `0x0`|
|`[2]`|`configure_test_mode`| Configure test mode: 0/1 -> Continues/Burst.| `R/W`| `0x0`| `0x0`|
|`[3]`|`invert_fcs_enable`| Invert FCS enable: 0/1 -> Dis/En.| `R/W`| `0x1`| `0x1`|
|`[5:4]`|`force_error`| Force error: 0/1/2/3 -> Normal/Data/Force FCS/Force Framer.| `R/W`| `0x0`| `0x0`|
|`[7:6]`|`band_width`| Bandwidth: 0/1/2/3 -> 100%/90%/80%/70%.| `R/W`| `0x0`| `0x0`|
|`[10:8]`|`data_mode`| Data mode: 0:PRBS7 1:PRBS15 2:PRBS23 3:PRBS31 4:FIX| `R/W`| `0x0`| `0x0`|
|`[19:12]`|`fix_data_value`| Fix data value.| `R/W`| `0xAA`| `0xAA`|
|`[31:20]`|`payload_size`| Payload size.| `R/W`| `0x40`| `0x40 End : Begin:`|

###MII Channels Configure Mac DA MSB.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `control_pen4`

* **Address**             : `0x03 - 0x33`

* **Formula**             : `0x03 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`mac_da_configure_msb`| Mac DA MSB configure value| `R/W`| `0x01010101`| `0x01010101 End : Begin:`|

###MII Channels Configure Mac DA LSB.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `control_pen5`

* **Address**             : `0x04 - 0x34`

* **Formula**             : `0x04 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `16`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`mac_da_configure_lsb`| Mac DA LSB configure value| `R/W`| `0xC0CA`| `0xC0CA End : Begin:`|

###MII Channels Configure Mac SA MSB.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `control_pen6`

* **Address**             : `0x05 - 0x35`

* **Formula**             : `0x05 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`mac_sa_configure_msb`| Mac SA MSB configure value| `R/W`| `0x01010101`| `0x01010101 End : Begin:`|

###MII Channels Configure Mac SA LSB.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `control_pen7`

* **Address**             : `0x06 - 0x36`

* **Formula**             : `0x06 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `16`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`mac_sa_configure_lsb`| Mac SA LSB configure value| `R/W`| `0xC0CA`| `0xC0CA End : Begin:`|

###MII Channels Configure Mac Type.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `control_pen8`

* **Address**             : `0x07 - 0x37`

* **Formula**             : `0x07 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `16`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`mac_type_configure`| Mac Type configure value| `R/W`| `0x0800`| `0x0800 End : Begin:`|

###MII Tx counter R2C.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `counter1`

* **Address**             : `0x08 - 0x38`

* **Formula**             : `0x08 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tx_counter_r2c`| Tx counter R2C| `R2C`| `0x0`| `0x0 End : Begin:`|

###MII Rx counter R2C.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `counter2`

* **Address**             : `0x09 - 0x39`

* **Formula**             : `0x09 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_counter_r2c`| Rx counter R2C| `R2C`| `0x0`| `0x0 End : Begin:`|

###MII FCS error counter R2C.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `counter3`

* **Address**             : `0x0A - 0x3A`

* **Formula**             : `0x0A + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`fcs_error_counter_r2c`| FCS error counter R2C| `R2C`| `0x0`| `0x0 End : Begin:`|

###MII PRBS error counter R2C.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `counter8`

* **Address**             : `0x401 - 0x431`

* **Formula**             : `0x401 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`prbs_error_counter_r2c`| PRBS error counter R2C| `R2C`| `0x0`| `0x0 End : Begin:`|

###MII Tx counter RO.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `counter4`

* **Address**             : `0x0B - 0x3B`

* **Formula**             : `0x0B + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tx_counter_ro`| Tx counter RO| `RO`| `0x0`| `0x0 End : Begin:`|

###MII Rx counter RO.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `counter5`

* **Address**             : `0x0C - 0x3C`

* **Formula**             : `0x0C + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_counter_ro`| Rx counter RO| `RO`| `0x0`| `0x0 End : Begin:`|

###MII FCS error counter RO.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `counter6`

* **Address**             : `0x0D - 0x3D`

* **Formula**             : `0x0D + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`fcs_error_counter_ro`| FCS error counter RO| `RO`| `0x0`| `0x0 End : Begin:`|

###MII PRBS error counter RO.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `counter7`

* **Address**             : `0x400 - 0x430`

* **Formula**             : `0x400 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`prbs_error_counter_ro`| PRBS error counter RO| `RO`| `0x0`| `0x0 End :`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_BACKDOOR
####Register Table

|Name|Address|
|-----|-----|
|`Pseudowire PDA Jitter Buffer Control`|`0x00010000 - 0x00011FFF #The address format for these registers is 0x00010000 + PWID`|
|`Pseudowire PDA Reorder Control`|`0x00020000 - 0x00021FFF #The address format for these registers is 0x00020000 + PWID`|
|`Pseudowire PDA Low Order TDM mode Control`|`0x00030000 - 0x0003FFFF #The address format for low order path is 0x00030000 +  LoOc48ID*0x800 + LoOrderoc24Slice*0x400 + TdmOc24Pwid`|
|`Pseudowire PDA Low Order TDM Look Up Control`|`0x00040000 - 0x00043FFF #The address format for low order path is 0x00040000 +  LoOc48ID*0x800 + LoOc24Slice*0x400 + TdmOc24Pwid`|
|`Pseudowire PDA High Order TDM Look Up Control`|`0x00044000 - 0x000441FF #The address format for high order path is 0x00044000 +  HoOc48ID*0x40 + HoMasSTS`|
|`Pseudowire PDA High Order TDM mode Control`|`0x00050000 - 0x000501FF #The address format is 0x00050000 +  HoOc48ID*0x100 + HoMasSTS`|
|``|``|
|``|``|
|``|``|
|``|``|


###Pseudowire PDA Jitter Buffer Control

* **Description**           


* **RTL Instant Name**    : `rtljitbuf.ramjitbufcfg.ram.ram[]`

* **Address**             : `0x00010000 - 0x00011FFF #The address format for these registers is 0x00010000 + PWID`

* **Formula**             : `0x00010000 +  PWID`

* **Where**               : 

    * `$PWID(0-6143): Pseudowire ID`

* **Width**               : `32`
* **Register Type**       : `n/a`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[Thisregisterconfiguresjitterbufferparametersperpseudo-wireBegin:]`|``| | *n/a*| *n/a*| *n/a*|

###Pseudowire PDA Reorder Control

* **Description**           


* **RTL Instant Name**    : `ramreorcfg.ram.ram[]`

* **Address**             : `0x00020000 - 0x00021FFF #The address format for these registers is 0x00020000 + PWID`

* **Formula**             : `0x00020000 +  PWID`

* **Where**               : 

    * `$PWID(0-6143): Pseudowire ID`

* **Width**               : `32`
* **Register Type**       : `n/a`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[Thisregisterconfiguresreorderparametersperpseudo-wireBegin:]`|``| | *n/a*| *n/a*| *n/a*|

###Pseudowire PDA Low Order TDM mode Control

* **Description**           


* **RTL Instant Name**    : `rtlpdatdm.rtl20glotdm.rtllodeas.ramdeascfg0.ram.ram[],_this_is_for_slice48_1_4_rtlpdatdm.rtl20glotdm.rtllodeas.ramdeascfg1.ram.ram[],_this_is_for_slice48_5_6`

* **Address**             : `0x00030000 - 0x0003FFFF #The address format for low order path is 0x00030000 +  LoOc48ID*0x800 + LoOrderoc24Slice*0x400 + TdmOc24Pwid`

* **Formula**             : `0x00030000 + $LoOc48ID*0x800 + $LoOrderoc24Slice*0x400 + $TdmOc24Pwid`

* **Where**               : 

    * `$LoOc48ID(0-3): LoOC48 ID`

    * `$LoOrderoc24Slice(0-1): Lo-order slice oc24`

    * `$TdmOc24Pwid(0-1023): TDM OC24 slice PWID`

* **Width**               : `32`
* **Register Type**       : `n/a`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[ThisregisterconfigureTDMmodeforinterworkingbetweenPseudowireandLoTDMBegin:]`|``| | *n/a*| *n/a*| *n/a*|

###Pseudowire PDA Low Order TDM Look Up Control

* **Description**           


* **RTL Instant Name**    : `rtlpdatdm.rampwlkcfglo.ram.ram[]`

* **Address**             : `0x00040000 - 0x00043FFF #The address format for low order path is 0x00040000 +  LoOc48ID*0x800 + LoOc24Slice*0x400 + TdmOc24Pwid`

* **Formula**             : `0x00040000 + LoOc48ID*0x800 + LoOc24Slice*0x400 + TdmOc24PwID`

* **Where**               : 

    * `$LoOc48ID(0-3): LoOC48 ID`

    * `$LoOc24Slice(0-1): Lo-order slice oc24`

    * `TdmOc24PwID(0-1023): TDM OC24 slice PWID`

* **Width**               : `32`
* **Register Type**       : `n/a`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[ThisregisterconfigurelookupfromTDMPWIDtoglobal8192PWIDBegin:]`|``| | *n/a*| *n/a*| *n/a*|

###Pseudowire PDA High Order TDM Look Up Control

* **Description**           


* **RTL Instant Name**    : `rtlpdatdm.rampwlkcfgho.ram.ram[]`

* **Address**             : `0x00044000 - 0x000441FF #The address format for high order path is 0x00044000 +  HoOc48ID*0x40 + HoMasSTS`

* **Formula**             : `0x00044000 + HoOc48ID*0x40 + HoMasSTS`

* **Where**               : 

    * `$HoOc48ID(0-3): HoOC48 ID`

    * `$HoMasSTS(0-47): HO TDM per OC48 master STS ID`

* **Width**               : `32`
* **Register Type**       : `n/a`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[ThisregisterconfigurelookupfromHOTDMmasterSTSIStoglobal8192PWIDBegin:]`|``| | *n/a*| *n/a*| *n/a*|

###Pseudowire PDA High Order TDM mode Control

* **Description**           

This register configure TDM mode for interworking between Pseudowire and Ho TDM


* **RTL Instant Name**    : `rtlpdahotdm.rtl48hodeas[].ramdeascfg.ram.ram[],_where_rtl48hodeas[]_is_module_instant`

* **Address**             : `0x00050000 - 0x000501FF #The address format is 0x00050000 +  HoOc48ID*0x100 + HoMasSTS`

* **Formula**             : `0x00050000 + HoOc48ID*0x100 + HoMasSTS`

* **Where**               : 

    * `$HoOc48ID(0-3): HoOC48 ID`

    * `$HoMasSTS(0-47): HO TDM per OC48 master STS ID`

* **Width**               : `32`
* **Register Type**       : `n/a`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[rtlclapro.iaf6ces96rtlcla_hcbe.irampwmod.array.ramtesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtldemap1.demap_pw_cfg_ram.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlmap1.map_pw_cfg_ram.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlmap1.map_src_cfg_ram.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlcdr1.engidcnt.timgen.ram1.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlcdr1.engidcnt.timgen.ram2.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlcdr1.engcdr.slc0_cdrnco_engacr.ramcfg.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlpdh1.pdhrxmux.vtcnt.stsctrlbuf.ramtesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlpdh1.stsvtdemap.ctrlbuf.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlpdh1.rxm13e13.rxm23e23.ctrlbuf.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlpdh1.rxm13e13.rxm12e12.ctrlbuf.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlpdh1.rxm13e13.rxm12e12.vtcnt.stsctrlbuf.ramtesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlpdh1.ds1e1rxfrm.ctrlbuf.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlpdh1.stsvtmap.ctrlbuf.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlpdh1.de1txfrm.ctrlbuf.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlpdh1.txm13e13.txm23e23.ctrlbuf.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtllotdmcore0.rtlpdh1.txm13e13.txm12e12.ctrlbuf.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.cpei.ramstctri.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.cpei.ramvctri.array]`|``| | *n/a*| *n/a*| *n/a*|

###

* **Description**           


* **RTL Instant Name**    : ``

* **Address**             : ``

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `n/a`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[POHTermintateInsertControlSTStesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccter.rtlpohccterpro.rtlpohccterctrl.imemrwpxctrlhi.ram]`|``| | *n/a*| *n/a*| *n/a*|

###

* **Description**           


* **RTL Instant Name**    : ``

* **Address**             : ``

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `n/a`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[POHTermintateInsertControlVT/TU3testtop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccter.rtlpohccterpro.rtlpohccterctrl.iarray113x_ctrllo.ram]`|``| | *n/a*| *n/a*| *n/a*|

###

* **Description**           


* **RTL Instant Name**    : ``

* **Address**             : ``

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `n/a`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[POHTermintateInsertBufferSTStesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccter.rtlpohccterbuflchi0.iarray112xpohbufb0testtop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccter.rtlpohccterbuflchi0.iarray112xpohbufb1]`|``| | *n/a*| *n/a*| *n/a*|

###

* **Description**           


* **RTL Instant Name**    : ``

* **Address**             : ``

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `n/a`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[POHTermintateInsertBufferTU3/VTtesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccter.rtlpohccterbuflo0.iarray112xpohbufb0testtop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccter.rtlpohccterbuflo0.iarray112xpohbufb1testtop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohbercore.imemrwpctrl.ramtesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohbercore.imemrwptrsh.ramtesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohbercore.imemrwpctrl.ramtesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohbercore.imemrwpctrl.ramtesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohbercore.iarray112xrep.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohbercore.iarray112xrep.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccintdef.rtlpohccinthigrp.rtlpohccinthi0.at6rtlnosegintxwrap0.array112x.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccintdef.rtlpohccinthigrp.rtlpohccinthi1.at6rtlnosegintxwrap0.array112x.array....testtop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccintdef.rtlpohccinthigrp.rtlpohccinthi0.at6rtlnosegintxwrap3.array112x.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccintdef.rtlpohccinthigrp.rtlpohccinthi1.at6rtlnosegintxwrap3.array112x.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccintdef.rtlpohccintlogrp.rtlpohccintlo0.at6rtlintxwrap0.array112x.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccintdef.rtlpohccintlogrp.rtlpohccintlo1.at6rtlintxwrap0.array112x.array....testtop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccintdef.rtlpohccintlogrp.rtlpohccintlo0.at6rtlintxwrap3.array112x.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccintdef.rtlpohccintlogrp.rtlpohccintlo1.at6rtlintxwrap3.array112x.arraytesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccpm.rtlpohbercnt224bip.iarray113x_cntdat.ramtesttop.af6top_0i.iaf6cci0011rtlcore.rtlpoh.rtlpohccpm.rtlpohbercnt224rei.iarray113x_cntdat.ramtesttop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl1pla1cfgpw_ram.array.ramaddr=data17:0-->OC48#1,OC24#1(addr=0-1023)-->pwidperslice24testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl1pla2cfgpw_ram.array.ramaddr=data17:0-->OC48#1,OC24#2(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl2pla1cfgpw_ram.array.ramaddr=data17:0-->OC48#2,OC24#1(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl2pla2cfgpw_ram.array.ramaddr=data17:0-->OC48#2,OC24#2(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl3pla1cfgpw_ram.array.ramaddr=data17:0-->OC48#3,OC24#1(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl3pla2cfgpw_ram.array.ramaddr=data17:0-->OC48#3,OC24#2(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl4pla1cfgpw_ram.array.ramaddr=data17:0-->OC48#4,OC24#1(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl4pla2cfgpw_ram.array.ramaddr=data17:0-->OC48#4,OC24#2(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl5pla1cfgpw_ram.array.ramaddr=data17:0-->OC48#5,OC24#1(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl5pla2cfgpw_ram.array.ramaddr=data17:0-->OC48#5,OC24#2(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl6pla1cfgpw_ram.array.ramaddr=data17:0-->OC48#6,OC24#1(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl6pla2cfgpw_ram.array.ramaddr=data17:0-->OC48#6,OC24#2(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl1pwblkcfg_ram.array.ramaddr=data21:0-->OC48#1(addr=0-2047)-->pwidperslice48testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl2pwblkcfg_ram.array.ramaddr=data21:0-->OC48#2(addr=0-2047)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl3pwblkcfg_ram.array.ramaddr=data21:0-->OC48#3(addr=0-2047)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl4pwblkcfg_ram.array.ramaddr=data21:0-->OC48#4(addr=0-2047)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl5pwblkcfg_ram.array.ramaddr=data21:0-->OC48#5(addr=0-2047)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl6pwblkcfg_ram.array.ramaddr=data21:0-->OC48#6(addr=0-2047)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl1pla1stapw_ram.array.ramaddr=data1:0-->OC48#1,OC24#1(addr=0-1023)-->pwidperslice24testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl1pla2stapw_ram.array.ramaddr=data1:0-->OC48#1,OC24#2(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl2pla1stapw_ram.array.ramaddr=data1:0-->OC48#2,OC24#1(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl2pla2stapw_ram.array.ramaddr=data1:0-->OC48#2,OC24#2(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl3pla1stapw_ram.array.ramaddr=data1:0-->OC48#3,OC24#1(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl3pla2stapw_ram.array.ramaddr=data1:0-->OC48#3,OC24#2(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl4pla1stapw_ram.array.ramaddr=data1:0-->OC48#4,OC24#1(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl4pla2stapw_ram.array.ramaddr=data1:0-->OC48#4,OC24#2(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl5pla1stapw_ram.array.ramaddr=data1:0-->OC48#5,OC24#1(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl5pla2stapw_ram.array.ramaddr=data1:0-->OC48#5,OC24#2(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl6pla1stapw_ram.array.ramaddr=data1:0-->OC48#6,OC24#1(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ilosl6pla2stapw_ram.array.ramaddr=data1:0-->OC48#6,OC24#2(addr=0-1023)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ip1pwcfg_ram.array.ramaddr=data-->port#1(addr=0-8191)-->pwidoutputtesttop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ip2pwcfg_ram.array.ramaddr=data-->port#2(addr=0-8191)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ip3pwcfg_ram.array.ramaddr=data-->port#3(addr=0-8191)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ip4pwcfg_ram.array.ramaddr=data-->port#4(addr=0-8191)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ip1upsrcfg_ram.array.ramaddr=data15:0-->port#1(addr=0-511)-->upsrgroupper16upsridtesttop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ip2upsrcfg_ram.array.ramaddr=data15:0-->port#2(addr=0-511)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ip3upsrcfg_ram.array.ramaddr=data15:0-->port#3(addr=0-511)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ip4upsrcfg_ram.array.ramaddr=data15:0-->port#4(addr=0-511)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ip1hspwcfg_ram.array.ramaddr=data1:0-->port#1(addr=0-4095)-->hspwidtesttop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ip2hspwcfg_ram.array.ramaddr=data1:0-->port#2(addr=0-4095)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ip3hspwcfg_ram.array.ramaddr=data1:0-->port#3(addr=0-4095)testtop.af6top_0i.iaf6cci0011rtlcore.rtlpla.ip4hspwcfg_ram.array.ramaddr=data1:0-->port#4(addr=0-4095)PSNtesttop.af6top_0i.mem_qdr1.array.ramaddr=data127:0-->(addr=0-131072)-->{psncnt2:0,psnpage,pwid12:0}withpsncnt2:0=0-5(0:first16-byte,1:second16-byte....)]`|``| | *n/a*| *n/a*| *n/a*|

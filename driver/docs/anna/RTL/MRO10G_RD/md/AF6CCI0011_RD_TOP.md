## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_TOP
####Register Table

|Name|Address|
|-----|-----|
|`Top ETH-Port Control 0`|`0x00_0040`|
|`Top ETH-Port Control 1`|`0x00_0041`|
|`Top ETH-Port Status e`|`0x00_006e`|


###Top ETH-Port Control 0

* **Description**           

This register indicates the active ports.


* **RTL Instant Name**    : `top_o_control0`

* **Address**             : `0x00_0040`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22:20]`|`ecsc_mac3_rxsel`| Ethernet CISCO MAC3 Select Serdes ID 0x0: CISCO_XFI_Serdes#08 0x1: CISCO_XFI_Serdes#09 0x2: CISCO_XFI_Serdes#10 0x3: CISCO_XFI_Serdes#11 0x4: CISCO_XFI_Serdes#12 0x5: CISCO_XFI_Serdes#13 0x6: CISCO_XFI_Serdes#14 0x7: CISCO_XFI_Serdes#15| `RW`| `0x0`| `0x0`|
|`[19:19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18:16]`|`ecsc_mac2_rxsel`| Ethernet CISCO MAC2 Select port ID 0x0: CISCO_XFI_Serdes#08 0x1: CISCO_XFI_Serdes#09 0x2: CISCO_XFI_Serdes#10 0x3: CISCO_XFI_Serdes#11 0x4: CISCO_XFI_Serdes#12 0x5: CISCO_XFI_Serdes#13 0x6: CISCO_XFI_Serdes#14 0x7: CISCO_XFI_Serdes#15| `RW`| `0x0`| `0x0`|
|`[15:08]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[07:07]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[06:04]`|`earr_mac1_rxsel`| Ethernet ARRIVE MAC1 Select XFI serdes ID 0x0: ARRIVE_XFI_Serdes#00 0x1: ARRIVE_XFI_Serdes#01 0x2: ARRIVE_XFI_Serdes#02 0x3: ARRIVE_XFI_Serdes#03 0x4: ARRIVE_XFI_Serdes#04 0x5: ARRIVE_XFI_Serdes#05 0x6: ARRIVE_XFI_Serdes#06 0x7: ARRIVE_XFI_Serdes#07| `RW`| `0x0`| `0x0`|
|`[03:03]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[02:00]`|`earr_mac0_rxsel`| Ethernet ARRIVE MAC0 Select XFI serdes ID 0x0: ARRIVE_XFI_Serdes#00 0x1: ARRIVE_XFI_Serdes#01 0x2: ARRIVE_XFI_Serdes#02 0x3: ARRIVE_XFI_Serdes#03 0x4: ARRIVE_XFI_Serdes#04 0x5: ARRIVE_XFI_Serdes#05 0x6: ARRIVE_XFI_Serdes#06 0x7: ARRIVE_XFI_Serdes#07| `RW`| `0x0`| `0x0 End: Begin:`|

###Top ETH-Port Control 1

* **Description**           

This register indicates the active ports.


* **RTL Instant Name**    : `top_o_control1`

* **Address**             : `0x00_0041`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`ecsc_xfi_txen`| Ethernet CISCO_XFI_Serdes Tx Enable bit_00 correspond for CISCO_XFI_Serdes#08 0x0: Disable 0x1: Enable bit_01 correspond for CISCO_XFI_Serdes#09 bit_02 correspond for CISCO_XFI_Serdes#10 bit_03 correspond for CISCO_XFI_Serdes#11 bit_04 correspond for CISCO_XFI_Serdes#12 bit_05 correspond for CISCO_XFI_Serdes#13 bit_06 correspond for CISCO_XFI_Serdes#14 bit_07 correspond for CISCO_XFI_Serdes#15| `RW`| `0x0`| `0x0`|
|`[23:16]`|`ecsc_xfi_txsel`| Ethernet CISCO_XFI_Serdes Select Data from bit_00 correspond for CISCO_XFI_Serdes#08 0x0: ECSC_MAC2_TX 0x1: ECSC_MAC3_TX bit_01 correspond for CISCO_XFI_Serdes#09 bit_02 correspond for CISCO_XFI_Serdes#10 bit_03 correspond for CISCO_XFI_Serdes#11 bit_04 correspond for CISCO_XFI_Serdes#12 bit_05 correspond for CISCO_XFI_Serdes#13 bit_06 correspond for CISCO_XFI_Serdes#14 bit_07 correspond for CISCO_XFI_Serdes#15| `RW`| `0x0`| `0x0`|
|`[15:08]`|`earr_xfi_txen`| Ethernet ARRIVE_XFI_Serdes Tx Enable bit_00 correspond for ARRIVE_XFI_Serdes#00 0x0: Disable 0x1: Enable bit_01 correspond for ARRIVE_XFI_Serdes#01 bit_02 correspond for ARRIVE_XFI_Serdes#02 bit_03 correspond for ARRIVE_XFI_Serdes#03 bit_04 correspond for ARRIVE_XFI_Serdes#04 bit_05 correspond for ARRIVE_XFI_Serdes#05 bit_06 correspond for ARRIVE_XFI_Serdes#06 bit_07 correspond for ARRIVE_XFI_Serdes#07| `RW`| `0x0`| `0x0`|
|`[07:00]`|`earr_xfi_txsel`| Ethernet ARRIVE_XFI_Serdes Select Data from bit_00 correspond for ARRIVE_XFI_Serdes#00 0x0: EARR_MAC0_TX 0x1: EARR_MAC1_TX bit_01 correspond for ARRIVE_XFI_Serdes#01 bit_02 correspond for ARRIVE_XFI_Serdes#02 bit_03 correspond for ARRIVE_XFI_Serdes#03 bit_04 correspond for ARRIVE_XFI_Serdes#04 bit_05 correspond for ARRIVE_XFI_Serdes#05 bit_06 correspond for ARRIVE_XFI_Serdes#06 bit_07 correspond for ARRIVE_XFI_Serdes#07| `RW`| `0x0`| `0x0 End: Begin:`|

###Top ETH-Port Status e

* **Description**           

This register used to show status of Ethernet Cross-Point


* **RTL Instant Name**    : `top_i_statuse`

* **Address**             : `0x00_006e`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`ecsc_xfi_txen`| Status of Ethernet CISCO_XFI_Serdes Tx Enable bit_00 correspond for CISCO_XFI_Serdes#08 0x0: Disable 0x1: Enable bit_01 correspond for CISCO_XFI_Serdes#09 bit_02 correspond for CISCO_XFI_Serdes#10 bit_03 correspond for CISCO_XFI_Serdes#11 bit_04 correspond for CISCO_XFI_Serdes#12 bit_05 correspond for CISCO_XFI_Serdes#13 bit_06 correspond for CISCO_XFI_Serdes#14 bit_07 correspond for CISCO_XFI_Serdes#15| `RW`| `0x0`| `0x0`|
|`[23:23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22:20]`|`ecsc_mac3_rxsel`| Status Ethernet CISCO MAC1 Select XFI serdes ID 0x0: CISCO_XFI_Serdes#08 0x1: CISCO_XFI_Serdes#09 0x2: CISCO_XFI_Serdes#10 0x3: CISCO_XFI_Serdes#11 0x4: CISCO_XFI_Serdes#12 0x5: CISCO_XFI_Serdes#13 0x6: CISCO_XFI_Serdes#14 0x7: CISCO_XFI_Serdes#15| `RW`| `0x0`| `0x0`|
|`[19:19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18:16]`|`ecsc_mac2_rxsel`| Status Ethernet CISCO MAC0 Select XFI serdes ID 0x0: CISCO_XFI_Serdes#08 0x1: CISCO_XFI_Serdes#09 0x2: CISCO_XFI_Serdes#10 0x3: CISCO_XFI_Serdes#11 0x4: CISCO_XFI_Serdes#12 0x5: CISCO_XFI_Serdes#13 0x6: CISCO_XFI_Serdes#14 0x7: CISCO_XFI_Serdes#15| `RW`| `0x0`| `0x0`|
|`[15:08]`|`earr_xfi_txen`| Status of Ethernet ARRIVE_XFI_Serdes Tx Enable bit_00 correspond for ARRIVE_XFI_Serdes#00 0x0: Disable 0x1: Enable bit_01 correspond for ARRIVE_XFI_Serdes#01 bit_02 correspond for ARRIVE_XFI_Serdes#02 bit_03 correspond for ARRIVE_XFI_Serdes#03 bit_04 correspond for ARRIVE_XFI_Serdes#04 bit_05 correspond for ARRIVE_XFI_Serdes#05 bit_06 correspond for ARRIVE_XFI_Serdes#06 bit_07 correspond for ARRIVE_XFI_Serdes#07| `RW`| `0x0`| `0x0`|
|`[07:07]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[06:04]`|`earr_mac1_rxsel`| Status Ethernet ARRIVE MAC1 Select XFI serdes ID 0x0: ARRIVE_XFI_Serdes#00 0x1: ARRIVE_XFI_Serdes#01 0x2: ARRIVE_XFI_Serdes#02 0x3: ARRIVE_XFI_Serdes#03 0x4: ARRIVE_XFI_Serdes#04 0x5: ARRIVE_XFI_Serdes#05 0x6: ARRIVE_XFI_Serdes#06 0x7: ARRIVE_XFI_Serdes#07| `RW`| `0x0`| `0x0`|
|`[03:03]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[02:00]`|`earr_mac0_rxsel`| Status Ethernet ARRIVE MAC0 Select XFI serdes ID 0x0: ARRIVE_XFI_Serdes#00 0x1: ARRIVE_XFI_Serdes#01 0x2: ARRIVE_XFI_Serdes#02 0x3: ARRIVE_XFI_Serdes#03 0x4: ARRIVE_XFI_Serdes#04 0x5: ARRIVE_XFI_Serdes#05 0x6: ARRIVE_XFI_Serdes#06 0x7: ARRIVE_XFI_Serdes#07| `RW`| `0x0`| `0x0 End:`|

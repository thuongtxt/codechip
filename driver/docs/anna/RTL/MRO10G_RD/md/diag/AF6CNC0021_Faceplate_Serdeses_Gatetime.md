## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-13|AF6Project|Initial version|




##AF6CNC0021_Faceplate_Serdeses_Gatetime
####Register Table

|Name|Address|
|-----|-----|
|`Faceplate serdeses Gatetime for PRBS Raw Diagnostic`|`0x40-0x4F`|
|`Faceplate serdeses Gatetime`|`0x60`|
|`Faceplate serdeses Gatetime Current`|`0x61-0x70`|


###Faceplate serdeses Gatetime for PRBS Raw Diagnostic

* **Description**           

This is Faceplate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 16


* **RTL Instant Name**    : `gatetime_ctr`

* **Address**             : `0x40-0x4F`

* **Formula**             : `Base_0x40 + 0x40 + PortID`

* **Where**               : 

    * `Port ID ( 0 - 15 )`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17]`|`start_diag`| Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration| `RW`| `0x0`| `0x0`|
|`[16:00]`|`time_cfg`| Gatetime Configuration 1-86400 second| `RW`| `0x0`| `0x0 End :`|

###Faceplate serdeses Gatetime

* **Description**           

Faceplate_serdeses_Gatetime_Status


* **RTL Instant Name**    : `Faceplate_serdeses_Gatetime_st`

* **Address**             : `0x60`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unsed`| Unsed| `R/W`| `0x0`| `0x0`|
|`[15:0]`|`status_gatetime_diag`| Status Gatetime diagnostic port 16 to port1 bit per port 1:Running 0:Done-Ready| `RO`| `0x0`| `0x0 End :`|

###Faceplate serdeses Gatetime Current

* **Description**           

Faceplate_serdeses_Gatetime_Status


* **RTL Instant Name**    : `Faceplate_serdeses_Gatetime_current`

* **Address**             : `0x61-0x70`

* **Formula**             : `Baas0x61 +0x60+Port ID`

* **Where**               : 

    * `Port ID ( 1 - 16 )`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unsed`| Unsed| `R/W`| `0x0`| `0x0`|
|`[16:0]`|`currert_gatetime_diag`| Current running time of Gatetime diagnostic| `RO`| `0x0`| `0x0 End :`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-13|AF6Project|Initial version|




##AF6CNC0021_DDR_additional_RD_DIAG
####Register Table

|Name|Address|
|-----|-----|
|`DDR PRBS Force Error`|`0x0 (THIS IS JUST ADDTION BIT FIELD TO ADDRESS #0, NOT FULL REGISTER)`|
|`DDR PRBS Error Counter`|`0x40`|
|`DDR PRBS read command counter`|`0x41`|
|`DDR PRBS Write command counter`|`0x42`|
|`DDR Error Data Latch`|`0x80 - 0x8F`|
|`DDR Expected Data Latch`|`0x90 - 0x9F`|
|`DDR Error Data Bit`|`0xA0 - 0xAF`|


###DDR PRBS Force Error

* **Description**           

PRBS Force Error Enable


* **RTL Instant Name**    : `pcfg_ctl0`

* **Address**             : `0x0 (THIS IS JUST ADDTION BIT FIELD TO ADDRESS #0, NOT FULL REGISTER)`

* **Formula**             : `0x0`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2]`|`force_error_enable`| PRBS Force Error Enable| `RW`| `0x0`| `0x0 End : Begin:`|

###DDR PRBS Error Counter

* **Description**           

PRBS Error counters


* **RTL Instant Name**    : `pcfg_cnt0`

* **Address**             : `0x40`

* **Formula**             : `0x40`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`err_cnt`| DDR PRBS error counter| `RW`| `0x0`| `0x0 End : Begin:`|

###DDR PRBS read command counter

* **Description**           

PRBS read command counters


* **RTL Instant Name**    : `pcfg_cnt1`

* **Address**             : `0x41`

* **Formula**             : `0x41`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rdvl_cnt`| DDR PRBS read counter| `RW`| `0x0`| `0x0 End : Begin:`|

###DDR PRBS Write command counter

* **Description**           

PRBS Write command counters


* **RTL Instant Name**    : `pcfg_cnt2`

* **Address**             : `0x42`

* **Formula**             : `0x42`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`wrvl_cnt`| DDR PRBS Write counter| `RW`| `0x0`| `0x0 End : Begin:`|

###DDR Error Data Latch

* **Description**           

Fist error pattern latch register, includes maximum 16x32bit words for 64b DDR interface. Write to clear the latched value, and start the next latched pattern


* **RTL Instant Name**    : `upen_err_dat`

* **Address**             : `0x80 - 0x8F`

* **Formula**             : `0x80 + word_id`

* **Where**               : 

    * `$word_id(0-15)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`err_data`| DDR Error Data  Latch| `RWC`| `0x0`| `0x0 End : Begin:`|

###DDR Expected Data Latch

* **Description**           

Expected pattern latch register, latched with the first error occur, includes maximumx 16x32bit words for 64b DDR interface. This register is cleared and latched for the next error/expected value, when the Error Data Latch register is cleared.


* **RTL Instant Name**    : `upen_exp_dat`

* **Address**             : `0x90 - 0x9F`

* **Formula**             : `0x90 + word_id`

* **Where**               : 

    * `$word_id(0-15)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`exp_data`| DDR Expected Data Latched| `RW`| `0x0`| `0x0 End : Begin:`|

###DDR Error Data Bit

* **Description**           

maximum 16x32bit words for 64b DDR interface. Used to accumulated indicate the error position. When Error occur, the correlative bit of this register is set when the read data and the expected data is unmatch. Write 0 to clear the whole pattern.


* **RTL Instant Name**    : `upen_err_pat`

* **Address**             : `0xA0 - 0xAF`

* **Formula**             : `0xA0 + word_id`

* **Where**               : 

    * `$word_id(0-15)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`err_pat`| DDR Error Data Bit| `RW`| `0x0`| `0x0 End :`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-13|AF6Project|Initial version|




##AF6CNC0021_ETH40G_RD
####Register Table

|Name|Address|
|-----|-----|
|`ETH 40G DRP`|`0x1000-0x1FFF`|
|`ETH 40G LoopBack`|`0x0002`|
|`ETH 40G QLL Status`|`0x000B`|
|`ETH 40G TX Reset`|`0x000C`|
|`ETH 40G RX Reset`|`0x000D`|
|`ETH 40G LPMDFE Mode`|`0x000E`|
|`ETH 40G LPMDFE Reset`|`0x000F`|
|`ETH 40G TXDIFFCTRL`|`0x0010`|
|`ETH 40G TXPOSTCURSOR`|`0x0011`|
|`ETH 40G TXPRECURSOR`|`0x0012`|
|`ETH 40G Ctrl FCS`|`0x0080`|
|`ETH 40G AutoNeg`|`0x0081`|
|`ETH 40G Diag Ctrl0`|`0x0020`|
|`ETH 40G Diag Ctrl1`|`0x0021`|
|`ETH 40G Diag Ctrl2`|`0x0022`|
|`ETH 40G Diag Ctrl3`|`0x0023`|
|`ETH 40G Diag Ctrl4`|`0x0024`|
|`ETH 40G Diag Ctrl6`|`0x0026`|
|`ETH 40G Diag Ctrl7`|`0x0027`|
|`Gatetime Current`|`0x28`|
|`ETH 40G Diag Sta0`|`0x0040`|
|`ETH 40G Diag TXPKT`|`0x0042`|
|`ETH 40G Diag TXNOB`|`0x0043`|
|`ETH 40G Diag RXPKT`|`0x0044`|
|`ETH 40G Diag RXNOB`|`0x0045`|
|`ETH 40G TX CFG`|`0x2021`|
|`ETH 40G RX CFG`|`0x20C2`|
|`ETH 40G FEC_AN STICKY`|`0x2102`|
|`ETH 40G TX STICKY`|`0x2103`|
|`ETH 40G RX STICKY`|`0x2104`|
|`ETH 40G FEC_AN INTTERUPT ENABLE`|`0x2105`|
|`ETH 40G TX INTTERUPT ENABLE`|`0x2106`|
|`ETH 40G RX INTTERUPT ENABLE`|`0x2107`|
|`ETH 40G FEC_AN ALARM`|`0x2108`|
|`ETH 40G TX ALARM`|`0x2109`|
|`ETH 40G RX ALARM`|`0x210A`|
|`ETH 40G CFG GLBEN`|`0x2100`|
|`ETH 40G CFG TICK REG`|`0x2101`|
|`ETH 40G Statistics TX COUNTER`|`0x2000 - 0x2019`|
|`ETH 40G Statistics RX Counters`|`0x2080 - 0x20AC`|


###ETH 40G DRP

* **Description**           

Read/Write DRP address of SERDES


* **RTL Instant Name**    : `OETH_40G_DRP`

* **Address**             : `0x1000-0x1FFF`

* **Formula**             : `0x1000+$P*0x400+$DRP`

* **Where**               : 

    * `$P(0-3) : Lane ID`

    * `$DRP(0-1023) : DRP address, see UG578`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:00]`|`drp_rw`| DRP read/write value| `R/W`| `0x0`| `0x0`|

###ETH 40G LoopBack

* **Description**           

Configurate LoopBack


* **RTL Instant Name**    : `ETH_40G_LoopBack`

* **Address**             : `0x0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`lpback_lane3`| Loopback lane3<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[11:08]`|`lpback_lane2`| Loopback lane2<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[07:04]`|`lpback_lane1`| Loopback lane1<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`lpback_lane0`| Loopback lane0<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|

###ETH 40G QLL Status

* **Description**           

QPLL status


* **RTL Instant Name**    : `ETH_40G_QLL_Status`

* **Address**             : `0x000B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:29]`|`qpll1_lock_change`| QPLL1 has transition lock/unlock, Group 0-3<br>{1} : QPLL1_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[28:28]`|`qpll0_lock_change`| QPLL0 has transition lock/unlock, Group 0-3<br>{1} : QPLL0_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[27:26]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[25:25]`|`qpll1_lock`| QPLL0 is Locked, Group 0-3<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[24:24]`|`qpll0_lock`| QPLL0 is Locked, Group 0-3<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[23:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###ETH 40G TX Reset

* **Description**           

Reset TX SERDES


* **RTL Instant Name**    : `ETH_40G_TX_Reset`

* **Address**             : `0x000C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`txrst_done`| TX Reset Done<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`txrst_trig`| Trige 0->1 to start reset TX SERDES| `R/W`| `0x0`| `0x0`|

###ETH 40G RX Reset

* **Description**           

Reset RX SERDES


* **RTL Instant Name**    : `ETH_49G_RX_Reset`

* **Address**             : `0x000D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`rxrst_done`| RX Reset Done<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`rxrst_trig`| Trige 0->1 to start reset RX SERDES| `R/W`| `0x0`| `0x0`|

###ETH 40G LPMDFE Mode

* **Description**           

Configure LPM/DFE mode


* **RTL Instant Name**    : `ETH_40G_LPMDFE_Mode`

* **Address**             : `0x000E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`lpmdfe_mode`| LPM/DFE mode<br>{0} : DFE mode <br>{1} : LPM mode| `R/W`| `0x0`| `0x0`|

###ETH 40G LPMDFE Reset

* **Description**           

Reset LPM/DFE


* **RTL Instant Name**    : `ETH_40G_LPMDFE_Reset`

* **Address**             : `0x000F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`lpmdfe_reset`| LPM/DFE reset<br>{1} : reset| `R/W`| `0x0`| `0x0`|

###ETH 40G TXDIFFCTRL

* **Description**           

Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail


* **RTL Instant Name**    : `ETH_40G_TXDIFFCTRL`

* **Address**             : `0x0010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:00]`|`txdiffctrl`| TXDIFFCTRL| `R/W`| `0x18`| `0x18`|

###ETH 40G TXPOSTCURSOR

* **Description**           

Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail


* **RTL Instant Name**    : `ETH_40G_TXPOSTCURSOR`

* **Address**             : `0x0011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:00]`|`txpostcursor`| TXPOSTCURSOR| `R/W`| `0x15`| `0x15`|

###ETH 40G TXPRECURSOR

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail


* **RTL Instant Name**    : `ETH_40G_TXPRECURSOR`

* **Address**             : `0x0012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:00]`|`txprecursor`| TXPRECURSOR| `R/W`| `0x0`| `0x0`|

###ETH 40G Ctrl FCS

* **Description**           

configure FCS mode


* **RTL Instant Name**    : `ETH_40G_Ctrl_FCS`

* **Address**             : `0x0080`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`txfcs_ignore`| TX ignore check FCS when txfcs_ins is low<br>{1} : ignore| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`txfcs_ins`| TX inserts 4bytes FCS<br>{1} : insert| `R/W`| `0x1`| `0x1`|
|`[03:02]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:01]`|`rxfcs_ignore`| RX ignore check FCS<br>{1} : ignore| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`rxfcs_rmv`| RX remove 4bytes FCS<br>{1} : remove| `R/W`| `0x1`| `0x1`|

###ETH 40G AutoNeg

* **Description**           

configure Auto-Neg


* **RTL Instant Name**    : `ETH_40G_AutoNeg`

* **Address**             : `0x0081`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:29]`|`fec_rx_enb`| FEC RX enable<br>{1} : enable| `R/W`| `0x1`| `0x1`|
|`[28:28]`|`fec_tx_enb`| FEC TX enable<br>{1} : enable| `R/W`| `0x1`| `0x1`|
|`[24:24]`|`an_pseudo_sel`| Selects the polynomial generator for the bit 49 random bit generator<br>{0} : x^7+x^3+1 <br>{1} : x^7+x^6+1| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`an_nonce_seed`| 8-bit seed to initialize the nonce field polynomial generator. Non-zero.| `R/W`| `0x5A`| `0x5A`|
|`[15:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:08]`|`an_lt_sta`| Link Control outputs from the auto-negotiationcontroller for the various Ethernet protocols.<br>{0} : DISABLE; PCS is disconnected <br>{1} : SCAN_FOR_CARRIER; RX is connected to PCS <br>{2} : not used <br>{3} : ENABLE; PCS is connected for mission mode operation| `R/W`| `0x0`| `0x0`|
|`[07:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`lt_restart`| This signal triggers a restart of link training regardless of the current state.<br>{1} : ignore| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`lt_enb`| Enables link training. When link training is disabled, all PCS lanes function in mission mode.<br>{1} : enable| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[02:02]`|`an_restart`| This input is used to trigger a restart of the auto-negotiation, regardless of what state the circuit is currently in.<br>{1} : restart| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`an_bypass`| Input to disable auto-negotiation and bypass the auto-negotiation function. If this input is asserted, auto-negotiation is turned off, but the PCS is connected to the output to allow operation.<br>{1} : bypass| `R/W`| `0x1`| `0x1`|
|`[00:00]`|`an_enb`| Enable signal for auto-negotiation<br>{1} : enable| `R/W`| `0x0`| `0x0`|

###ETH 40G Diag Ctrl0

* **Description**           

Diagnostic control 0


* **RTL Instant Name**    : `ETH_40G_Diag_ctrl0`

* **Address**             : `0x0020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:25]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[24:24]`|`diag_err`| Error detection<br>{1} : error| `W1C`| `0x0`| `0x0`|
|`[23:21]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[20:20]`|`diag_ferr`| Enable force error data of diagnostic packet<br>{1} : force error| `R/W`| `0x0`| `0x0`|
|`[19:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:04]`|`diag_datmod`| payload mod of ethernet frame<br>{2}     : PRBS31 <br>{other} : inscrease| `R/W`| `0x0`| `0x0`|
|`[03:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`diag_enb`| enable diagnostic block<br>{1} : enable| `R/W`| `0x0`| `0x0`|

###ETH 40G Diag Ctrl1

* **Description**           

Diagnostic control 1


* **RTL Instant Name**    : `ETH_40G_Diag_ctrl1`

* **Address**             : `0x0021`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`diag_lenmax`| Maximum length of diagnostic packet, count from 0, min value is 63| `R/W`| `0x0`| `0x0`|
|`[15:00]`|`diag_lenmin`| Minimum length of diagnostic packet, count from 0, min value is 63| `R/W`| `0x0`| `0x0`|

###ETH 40G Diag Ctrl2

* **Description**           

Diagnostic control 2


* **RTL Instant Name**    : `ETH_40G_Diag_ctrl2`

* **Address**             : `0x0022`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`diag_dalsb`| 32bit-LSB DA of diagnostic packet| `R/W`| `0x0`| `0x0`|

###ETH 40G Diag Ctrl3

* **Description**           

Diagnostic control 3


* **RTL Instant Name**    : `ETH_40G_Diag_ctrl3`

* **Address**             : `0x0023`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`diag_salsb`| 32bit-LSB SA of diagnostic packet| `R/W`| `0x0`| `0x0`|

###ETH 40G Diag Ctrl4

* **Description**           

Diagnostic control 4


* **RTL Instant Name**    : `ETH_40G_Diag_ctrl4`

* **Address**             : `0x0024`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`diag_damsb`| 16bit-MSB DA of diagnostic packet| `R/W`| `0x0`| `0x0`|
|`[15:00]`|`diag_samsb`| 16bit-MSB SA of diagnostic packet| `R/W`| `0x0`| `0x0`|

###ETH 40G Diag Ctrl6

* **Description**           

Diagnostic control 7


* **RTL Instant Name**    : `ETH_40G_Diag_ctrl6`

* **Address**             : `0x0026`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `17`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17]`|`enb_type`| Config enable insert type from CPU, (1) is enable, (0) is disable| `RW`| `0x0`| `0x0`|
|`[16:0]`|`type_cfg`| value type that is configured| `RW`| `0x0800`| `0x0800`|

###ETH 40G Diag Ctrl7

* **Description**           

Diagnostic control 7


* **RTL Instant Name**    : `ETH_40G_Diag_ctrl7`

* **Address**             : `0x0027`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unsed`| Unsed| `R/W`| `0x0`| `0x0`|
|`[18]`|`status_gatetime_diag`| Status Gatetime diagnostic 1:Running 0:Done-Ready| `RO`| `0x0`| `0x0`|
|`[17]`|`start_gatetime_diag`| Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration| `RW`| `0x0`| `0x0`|
|`[16:0]`|`time_cfg`| Gatetime Configuration 1-86400 second| `RW`| `0x0`| `0x0`|

###Gatetime Current

* **Description**           




* **RTL Instant Name**    : `Gatetime_current`

* **Address**             : `0x28`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unsed`| Unsed| `R/W`| `0x0`| `0x0`|
|`[16:0]`|`currert_gatetime_diag`| Current running time of Gatetime diagnostic| `RO`| `0x0`| `0x0`|

###ETH 40G Diag Sta0

* **Description**           

Diagnostic Sta0


* **RTL Instant Name**    : `ETH_40G_Diag_Sta0`

* **Address**             : `0x0040`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:07]`|`diag_txmis_sop`| Packet miss SOP| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`diag_txmis_eop`| Packet miss EOP| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`diag_txsop_eop`| Short packet, length is less than 16bytes| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`diag_txwff_ful`| TX-Fifo is full| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`diag_rxmis_sop`| Packet miss SOP| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`diag_rxmis_eop`| Packet miss EOP| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`diag_rxsop_eop`| Short packet, length is less than 16bytes  t| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`diag_rxwff_ful`| RX-Fifo is full| `W1C`| `0x0`| `0x0`|

###ETH 40G Diag TXPKT

* **Description**           

Diagnostic TX packet counter


* **RTL Instant Name**    : `ETH_40G_Diag_TXPKT`

* **Address**             : `0x0042`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`diag_txpkt`| TX packet counter| `R2C`| `0x0`| `0x0`|

###ETH 40G Diag TXNOB

* **Description**           

Diagnostic TX number of bytes counter


* **RTL Instant Name**    : `ETH_40G_Diag_TXNOB`

* **Address**             : `0x0043`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`diag_txnob`| TX number of byte counter| `R2C`| `0x0`| `0x0`|

###ETH 40G Diag RXPKT

* **Description**           

Diagnostic RX packet counter


* **RTL Instant Name**    : `ETH_40G_Diag_RXPKT`

* **Address**             : `0x0044`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`diag_rxpkt`| RX packet counter| `R2C`| `0x0`| `0x0`|

###ETH 40G Diag RXNOB

* **Description**           

Diagnostic RX number of bytes counter


* **RTL Instant Name**    : `ETH_40G_Diag_RXNOB`

* **Address**             : `0x0045`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`diag_rxnob`| RX number of byte counter| `R2C`| `0x0`| `0x0`|

###ETH 40G TX CFG

* **Description**           

ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)


* **RTL Instant Name**    : `ETH_40G_TX_CFG`

* **Address**             : `0x2021`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`reserve`| *n/a*| `RW`| `0x0`| `0x0`|
|`[04:04]`|`cfg_txen`| enable transmit side,<br>{1} is enable, <br>{0} is disable| `RW`| `0x1`| `0x1`|
|`[03:00]`|`ipg_cfg`| configure Tx IPG| `RW`| `0x0`| `0x0`|

###ETH 40G RX CFG

* **Description**           

ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)


* **RTL Instant Name**    : `ETH_40G_RX_CFG`

* **Address**             : `0x20C2`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:25]`|`reserve`| *n/a*| `RW`| `0x0`| `0x0`|
|`[24:24]`|`cfg_rxen`| enable receive side,<br>{1} is enable, <br>{0} is disable| `RW`| `0x0`| `0x0`|
|`[23:23]`|`reserve`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22:08]`|`cfg_maxlen`| configure Rx MTU, max len packet receive| `RW`| `0x2580`| `0x2580`|
|`[07:00]`|`cfg_minlen`| | `configure Rx MTU, min len packet receive`| `RW`| `0x40`|

###ETH 40G FEC_AN STICKY

* **Description**           

ETH 40G -pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 44-45)


* **RTL Instant Name**    : `ETH_40G_FEC_AN_STICKY`

* **Address**             : `0x2102`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `14`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13:10]`|`stkfec_inc_cant_correct_count`| stkfec_inc_cant_correct_count, per lane| `W1C`| `0x0`| `0x0`|
|`[09:06]`|`stkfec_inc_correct_count`| stkfec_inc_correct_count , per lane| `W1C`| `0x0`| `0x0`|
|`[05:02]`|`stkfec_lock_error`| stkfec_lock_error, per lane| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`stkan_autoneg_complete`| stkan_autoneg_complete| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`stkan_parallel_detection_fault`| stkan_parallel_detection_fault| `W1C`| `0x0`| `0x0`|

###ETH 40G TX STICKY

* **Description**           

ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)


* **RTL Instant Name**    : `ETH_40G_TX_STICKY`

* **Address**             : `0x2103`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `18`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:17]`|`tx_underflow_err`| not support with xilinx IP- reserve| `W1C`| `0x0`| `0x0`|
|`[16:16]`|`stktx_local_fault`| A value of 1 indicates the transmit encoder state machine is in the TX_INIT state| `W1C`| `0x0`| `0x0`|
|`[15:12]`|`stklt_signal_detect`| This signal indicates when the respective link training state machine has entered the SEND_DATA state, in which normal PCS operation can resume, per lane| `W1C`| `0x0`| `0x0`|
|`[11:08]`|`stklt_training`| This signal indicates when the respective link training state machine is performing link training, per lane| `W1C`| `0x0`| `0x0`|
|`[07:04]`|`stklt_training_fail`| This signal is asserted during link training if the corresponding link training state machine detects a time-out during the training period, per lane| `W1C`| `0x0`| `0x0`|
|`[03:00]`|`stklt_frame_lock`| When link training has begun, these signals are asserted, per lane| `W1C`| `0x0`| `0x0`|

###ETH 40G RX STICKY

* **Description**           

ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)


* **RTL Instant Name**    : `ETH_40G_RX_STICKY`

* **Address**             : `0x2104`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`stkrx_local_fault`| stkrx_local_fault| `W1C`| `0x0`| `0x0`|
|`[30:00]`|`stkrx_remote_fault`| stkrx_remote_fault| `W1C`| `0x0`| `0x0`|
|`[29:29]`|`stkrx_internal_local_fault`| stkrx_internal_local_fault| `W1C`| `0x0`| `0x0`|
|`[28:28]`|`stkrx_received_local_fault`| stkrx_received_local_fault| `W1C`| `0x0`| `0x0`|
|`[27:24]`|`stkrx_framing_err`| stk_framing_err, per lane| `W1C`| `0x0`| `0x0`|
|`[23:20]`|`stkrx_synced_err`| stk_synced_err, per lane| `W1C`| `0x0`| `0x0`|
|`[19:16]`|`stkrx_mf_len_err`| stk_mf_len_err, per lane| `W1C`| `0x0`| `0x0`|
|`[15:12]`|`stkrx_mf_repeat_err`| stk_mf_repeat_err,  per lane| `W1C`| `0x0`| `0x0`|
|`[11:11]`|`stkrx_aligned_err`| stk_aligned_err_| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`stkrx_misaligned`| stk misaligned| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`stkrx_truncated`| stk truncated| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`stkrx_hi_ber`| stk stkrx_hi_ber| `W1C`| `0x0`| `0x0`|
|`[07:04]`|`stkrx_bip_err`| stk these signal are asserted  per lane| `W1C`| `0x0`| `0x0`|
|`[03:00]`|`stkrx_mf_err`| stk these signals are asserted, per lane| `W1C`| `0x0`| `0x0`|

###ETH 40G FEC_AN INTTERUPT ENABLE

* **Description**           

ETH 40G -pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 44-45)


* **RTL Instant Name**    : `ETH_40G_FEC_AN_INTEN`

* **Address**             : `0x2105`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `14`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13:10]`|`int_en_fec_inc_cant_correct_count`| enable interrupt fec_inc_cant_correct_count, per lane,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[09:06]`|`int_en_fec_inc_correct_count`| enable interrupt fec_inc_correct_count , per lane,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[05:02]`|`int_en_fec_lock_error`| enable interrupt fec_lock_error, per lane,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`int_en_an_autoneg_complete`| enable interrupt an_autoneg_complete,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`int_en_an_parallel_detection_fault`| enable interrupt an_parallel_detection_fault,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|

###ETH 40G TX INTTERUPT ENABLE

* **Description**           

ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)


* **RTL Instant Name**    : `ETH_40G_TX_INTEN`

* **Address**             : `0x2106`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `17`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16:16]`|`int_en_tx_local_fault`| enable interrupt TX_INIT,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[15:12]`|`int_en_lt_signal_detect`| enable interrupt link training state machine has entered the SEND_DATA state, in which normal PCS operation can resume, per lane,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[11:08]`|`int_en_lt_training`| enable interrupt link training state machine is performing link training, per lane,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[07:04]`|`int_en_lt_training_fail`| enable interrupt link training state machine detects a time-out during the training period, per lane,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[03:00]`|`int_en_lt_frame_lock`| enable interrupt link training has begun, per lane,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|

###ETH 40G RX INTTERUPT ENABLE

* **Description**           

ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)


* **RTL Instant Name**    : `ETH_40G_RX_INTEN`

* **Address**             : `0x2107`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`int_en_rx_local_fault`| enable interrupt rx_local_fault,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[30:00]`|`int_en_rx_remote_fault`| enable interrupt rx_remote_fault,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[29:29]`|`int_en_rx_internal_local_fault`| enable interrupt rx_internal_local_fault,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[28:28]`|`int_en_rx_received_local_fault`| enable interrupt rx_received_local_fault,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[27:24]`|`int_en_rx_framing_err`| enable interrupt framing_err, per lane,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[23:20]`|`int_en_rx_synced_err`| enable interrupt synced_err, per lane,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[19:16]`|`int_en_rx_mf_len_err`| enable interrupt mf_len_err, per lane ,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[15:12]`|`int_en_rx_mf_repeat_err`| enable interrupt mf_repeat_err,  per lane,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[11:11]`|`int_en_rx_aligned_err`| enable interrupt aligned_err,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`int_en_rx_misaligned`| enable interrupt misaligned,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`int_en_rx_truncated`| enable interrupt truncated,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`int_en_rx_hi_ber`| enable interrupt rx_hi_ber,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[07:04]`|`int_en_rx_bip_err`| enable interrupt rx_bip_err,    per lane,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|
|`[03:00]`|`int_en_rx_mf_err`| enable interrupt rx_mf_err,  per lane,<br>{1} is enable, <br>{0} is disable| `W1C`| `0x0`| `0x0`|

###ETH 40G FEC_AN ALARM

* **Description**           

ETH 40G -pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 44-45)


* **RTL Instant Name**    : `ETH_40G_FEC_AN_ALARM`

* **Address**             : `0x2108`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `14`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13:10]`|`stat_fec_inc_cant_correct_count`| stat_fec_inc_cant_correct_count, per lane| `W1C`| `0x0`| `0x0`|
|`[09:06]`|`stat_fec_inc_correct_count`| stat_fec_inc_correct_count , per lane| `W1C`| `0x0`| `0x0`|
|`[05:02]`|`stat_fec_lock_error`| stat_fec_lock_error, per lane| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`stat_an_autoneg_complete`| stat_an_autoneg_complete| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`stat_an_parallel_detection_fault`| stat_an_parallel_detection_fault| `W1C`| `0x0`| `0x0`|

###ETH 40G TX ALARM

* **Description**           

ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)


* **RTL Instant Name**    : `ETH_40G_TX_ALARM`

* **Address**             : `0x2109`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `18`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:17]`|`tx_underflow_err`| not support with xilinx IP- reserve| `W1C`| `0x0`| `0x0`|
|`[16:16]`|`stat_tx_local_fault`| A value of 1 indicates the transmit encoder state machine is in the TX_INIT state| `W1C`| `0x0`| `0x0`|
|`[15:12]`|`stat_lt_signal_detect`| This signal indicates when the respective link training state machine has entered the SEND_DATA state, in which normal PCS operation can resume, per lane| `W1C`| `0x0`| `0x0`|
|`[11:08]`|`stat_lt_training`| This signal indicates when the respective link training state machine is performing link training, per lane| `W1C`| `0x0`| `0x0`|
|`[07:04]`|`stat_lt_training_fail`| This signal is asserted during link training if the corresponding link training state machine detects a time-out during the training period, per lane| `W1C`| `0x0`| `0x0`|
|`[03:00]`|`stat_lt_frame_lock`| When link training has begun, these signals are asserted, per lane| `W1C`| `0x0`| `0x0`|

###ETH 40G RX ALARM

* **Description**           

ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)


* **RTL Instant Name**    : `ETH_40G_RX_ALARM`

* **Address**             : `0x210A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`stat_rx_local_fault`| stat_rx_local_fault| `W1C`| `0x0`| `0x0`|
|`[30:00]`|`stat_rx_remote_fault`| stat_rx_remote_fault| `W1C`| `0x0`| `0x0`|
|`[29:29]`|`stat_rx_internal_local_fault`| stat_rx_internal_local_fault| `W1C`| `0x0`| `0x0`|
|`[28:28]`|`stat_rx_received_local_fault`| stat_rx_received_local_fault| `W1C`| `0x0`| `0x0`|
|`[27:24]`|`stat_rx_framing_err`| stat_framing_err, per lane| `W1C`| `0x0`| `0x0`|
|`[23:20]`|`stat_rx_synced_err`| synced_err, per lane| `W1C`| `0x0`| `0x0`|
|`[19:16]`|`stat_rx_mf_len_err`| mf_len_err, per lane| `W1C`| `0x0`| `0x0`|
|`[15:12]`|`stat_rx_mf_repeat_err`| mf_repeat_err,  per lane| `W1C`| `0x0`| `0x0`|
|`[11:11]`|`stat_rx_aligned_err`| aligned_err_| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`stat_rx_misaligned`| misaligned| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`stat_rx_truncated`| truncated| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`stat_rx_hi_ber`| stat_rx_hi_ber| `W1C`| `0x0`| `0x0`|
|`[07:04]`|`stat_rx_bip_err`| these signal are asserted  per lane| `W1C`| `0x0`| `0x0`|
|`[03:00]`|`stat_rx_mf_err`| these signals are asserted, per lane| `W1C`| `0x0`| `0x0`|

###ETH 40G CFG GLBEN

* **Description**           




* **RTL Instant Name**    : `ETH_40G_CFG_GLBEN`

* **Address**             : `0x2100`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`reserve`| *n/a*| `RW`| `0x0`| `0x0`|
|`[01:01]`|`cfg_sel_tick`| configure select pm_tick from CPU configure (tick_reg) or from signal pm_tick, (1) from signal pm_tick, (0) from CPU| `RW`| `0x0`| `0x0`|
|`[00:00]`|`cfg_enable_cnt`| configure enbale counter, (1) is enable, (0) is disable| `RW`| `0x1`| `0x1`|

###ETH 40G CFG TICK REG

* **Description**           




* **RTL Instant Name**    : `ETH_40G_CFG_TICK_REG`

* **Address**             : `0x2101`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00:00]`|`tick_reg`| write value "1" for tick, auto low (value "0")| `RW`| `0x0`| `0x0`|

###ETH 40G Statistics TX COUNTER

* **Description**           

ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 69-70/table 2-22)


* **RTL Instant Name**    : `ETH_40G_Statistics_TX_COUNTER`

* **Address**             : `0x2000 - 0x2019`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_tx_val`| value of resgister Statistics Tx Counter| `RW`| `0x0`| `0x0`|

###ETH 40G Statistics RX Counters

* **Description**           

AXI4 Statistics Counters ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 71-75/table 2-22)


* **RTL Instant Name**    : `ETH_40G_Statistics_RX_Counters`

* **Address**             : `0x2080 - 0x20AC`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_rx_val`| value of resgister Statistics Rx Counters| `RW`| `0x0`| `0x0`|

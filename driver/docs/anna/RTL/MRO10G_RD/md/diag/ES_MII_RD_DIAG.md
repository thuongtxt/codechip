## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-13|Project|Initial version|




##ES_MII_RD_DIAG
####Register Table

|Name|Address|
|-----|-----|
|`MiiAlarmGlb`|`0x00`|
|`MiiStatusGlb`|`0x10`|
|`EthPacketConfig`|`0x01`|
|`MiiEthTestControl`|`0x03`|
|`gatetime config value`|`0x04`|
|`Gatetime Current`|`0x05`|
|`MiiTxCounterR2C.`|`0x0C`|
|`MiiRxCounterR2C..`|`0x0D`|
|`MiiTxCounterRO.`|`0x08`|
|`MiiRxCounterRO.`|`0x09`|


###MiiAlarmGlb

* **Description**           

This is status of packet diagnostic


* **RTL Instant Name**    : `MiiAlarmGlb`

* **Address**             : `0x00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`opkterr`| Packet Error<br>{1} : Error <br>{0} : OK| `R1W`| `0x0`| `0x0`|
|`[11:09]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[08:08]`|`olenerr`| Packet Len Error<br>{1} : Error <br>{0} : OK| `R1W`| `0x0`| `0x0`|
|`[07:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:04]`|`odaterr`| PRBS payload Data Error ( Not Syn)<br>{1} : Not Syn <br>{0} : Syn| `R1W`| `0x0`| `0x0`|
|`[03:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`link_sta`| PHY Link up status<br>{1} : Link Down <br>{0} : Link Up| `R1W`| `0x0`| `0x0`|

###MiiStatusGlb

* **Description**           

This is status of packet diagnostic


* **RTL Instant Name**    : `MiiStatusGlb`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`opkterr`| Packet Error<br>{1} : Error <br>{0} : OK| `RO`| `0x0`| `0x0`|
|`[11:09]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[08:08]`|`olenerr`| Packet Len Error<br>{1} : Error <br>{0} : OK| `RO`| `0x0`| `0x0`|
|`[07:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:04]`|`odaterr`| PRBS payload Data Error ( Not Syn)<br>{1} : Not Syn <br>{0} : Syn| `RO`| `0x0`| `0x0`|
|`[03:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`link_sta`| PHY Link up status<br>{1} : Link Down <br>{0} : Link Up| `RO`| `0x0`| `0x0`|

###EthPacketConfig

* **Description**           

Configuration Packet Diagnostic,


* **RTL Instant Name**    : `EthPacketConfig`

* **Address**             : `0x01`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `R/W`| `0x00`| `0x00`|
|`[27:20]`|`icfgpat`| Data fix value for  idatamod = 0x2| `R/W`| `0xBC`| `0xBC`|
|`[19:18]`|`unused`| *n/a*| `R/W`| `0x00`| `0x00`|
|`[17:16]`|`ibandwidth`| Band Width<br>{0} full <br>{1} 50% <br>{2} 75%| `R/W`| `0x00`| `0x00`|
|`[15:15]`|`unused`| *n/a*| `R/W`| `0x00`| `0x00`|
|`[14:12]`|`idatamod`| Payload Data Mode 0:PRBS7 1:PRBS15 2:PRBS23 3:PRBS31| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`ipklen`| Packet len| `R/W`| `0x40`| `0x40`|

###MiiEthTestControl

* **Description**           

Configuration Diagnostic,


* **RTL Instant Name**    : `MiiEthTestControl`

* **Address**             : `0x03`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[2]`|`start_diag`| Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration| `RW`| `0x0`| `0x0`|
|`[01:01]`|`iforceerr`| PRBS Data Error Insert<br>{0} : None <br>{1} : Insert| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`test_en`| PRBS test Enable<br>{0} : Disable <br>{1} : Enable| `R/W`| `0x0`| `0x0`|

###gatetime config value

* **Description**           

This is Mate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 4


* **RTL Instant Name**    : `gatetime_cfg`

* **Address**             : `0x04`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16:00]`|`time_cfg`| Gatetime Configuration 1-86400 second| `RW`| `0x0`| `0x0`|

###Gatetime Current

* **Description**           




* **RTL Instant Name**    : `Gatetime_current`

* **Address**             : `0x05`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17]`|`status_gatetime_diag`| Status Gatetime diagnostic port 16 to port1 bit per port 1:Running 0:Done-Ready| `RO`| `0x0`| `0x0`|
|`[16:0]`|`currert_gatetime_diag`| Current running time of Gatetime diagnostic| `RO`| `0x0`| `0x0 Begin:`|

###MiiTxCounterR2C.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `itxpkr2c`

* **Address**             : `0x0C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tx_counter_r2c`| Tx Packet counter R2C| `R2C`| `0x0`| `0x0 End : Begin:`|

###MiiRxCounterR2C..

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `counter2`

* **Address**             : `0x0D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_counter_r2c`| Rx counter R2C| `R2C`| `0x0`| `0x0 Begin:`|

###MiiTxCounterRO.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `counter4`

* **Address**             : `0x08`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tx_counter_ro`| Tx counter RO| `RO`| `0x0`| `0x0 Begin:`|

###MiiRxCounterRO.

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `counter5`

* **Address**             : `0x09`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_counter_ro`| Rx counter RO| `RO`| `0x0`| `0x0`|

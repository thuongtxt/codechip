## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2016-10-07|AF6Project|Initial version|




##AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG
####Register Table

|Name|Address|
|-----|-----|
|`OCN Global Rx Framer Control`|`0x00000`|
|`OCN Global Rx Framer LOS Detecting Control 1`|`0x00006`|
|`OCN Global Rx Framer LOS Detecting Control 2`|`0x00007`|
|`OCN Global Rx Framer LOF Threshold`|`0x00008`|
|`OCN Global Tx Framer Control`|`0x00001`|
|`OCN Rx Framer Status`|`0x00010 - 0x00040`|
|`OCN Rx Framer Sticky`|`0x00011 - 0x00041`|
|`OCN Rx Framer B1 error counter read only`|`0x00012 - 0x00042`|
|`OCN Rx Framer B1 error counter read to clear`|`0x00013 - 0x00043`|


###OCN Global Rx Framer Control

* **Description**           

This is the global configuration register for the Rx Framer


* **RTL Instant Name**    : `glbrfm_reg`

* **Address**             : `0x00000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14:12]`|`rxfrmbadfrmthresh`| Threshold for A1A2 missing counter, that is used to change state from FRAMED to HUNT.| `RW`| `0x4`| `0x4`|
|`[11:8]`|`rxfrmdescren`| Enable/disable de-scrambling of the Rx coming data stream for 4 lines. 1: Enable 0: Disable| `RW`| `0xf`| `0xf`|
|`[7:0]`|`rxfrmstmocnmode`| STM rate mode for Rx of  4 lines, each line use 2 bits.Bits[1:0] for line 0 0: OC48 1: OC12 2: OC3| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Rx Framer LOS Detecting Control 1

* **Description**           

This is the global configuration register for the Rx Framer LOS detecting 1


* **RTL Instant Name**    : `glbclkmon_reg`

* **Address**             : `0x00006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`rxfrmlosdetmod`| Detected LOS mode. 1: the LOS declare when all zero or all one detected 0: the LOS declare when all zero detected| `RW`| `0x1`| `0x1`|
|`[17]`|`rxfrmlosdetdis`| Disable detect LOS. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[16]`|`rxfrmclkmondis`| Disable to generate LOS to Rx Framer if detecting error on Rx line clock. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[15:0]`|`rxfrmclkmonthr`| Threshold to generate LOS to Rx Framer if detecting error on Rx line clock.(Threshold = PPM * 155.52/8 for STM16 and STM1). Default 0x3cc ~ 50ppm for STM16 and STM1. With STM4 must config value that is this default value divide by 4.| `RW`| `0x3cc`| `0x3cc End : Begin:`|

###OCN Global Rx Framer LOS Detecting Control 2

* **Description**           

This is the global configuration register for the Rx Framer LOS detecting 2


* **RTL Instant Name**    : `glbdetlos_pen`

* **Address**             : `0x00007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`rxfrmlosclr2thr`| Configure the period of time during which there is no period of consecutive data without transition, used for reset no transition counter. The recommended value is 0x30 (~2.5ms) for STM16 and STM1. With STM4 must config value that is this default value divide by 4.| `RW`| `0x30`| `0x30`|
|`[23:12]`|`rxfrmlossetthr`| Configure the value that define the period of time in which there is no transition detected from incoming data from SERDES. The recommended value is 0x3cc (~50ms) for STM16 and STM1. With STM4 must config value that is this default value divide by 4.| `RW`| `0x3cc`| `0x3cc`|
|`[11:0]`|`rxfrmlosclr1thr`| Configure the period of time during which there is no period of consecutive data without transition, used for counting at INFRM to change state to OOF The recommended value is 0x3cc (~50ms) for STM16 and STM1. With STM4 must config value that is this default value divide by 4.| `RW`| `0x3cc`| `0x3cc End : Begin:`|

###OCN Global Rx Framer LOF Threshold

* **Description**           

Configure thresholds for LOF detection at receive SONET/SDH framer,There are two thresholds


* **RTL Instant Name**    : `glblofthr_reg`

* **Address**             : `0x00008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`rxfrmlofdetmod`| Detected LOF mode.Set 1 to clear OOF counter when state into INFRAMED| `RW`| `0x1`| `0x1`|
|`[15:8]`|`rxfrmlofsetthr`| Configure the OOF time counter threshold for entering LOF state. Resolution of this threshold is one frame.| `RW`| `0x18`| `0x18`|
|`[7:0]`|`rxfrmlofclrthr`| Configure the In-frame time counter threshold for exiting LOF state. Resolution of this threshold is one frame.| `RW`| `0x18`| `0x18 End : Begin:`|

###OCN Global Tx Framer Control

* **Description**           

This is the global configuration register for the Tx Framer


* **RTL Instant Name**    : `glbtfm_reg`

* **Address**             : `0x00001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[19:16]`|`txlineooffrc`| Enable/disable force OOF for 4 tx lines. Bit[0] for line 0.| `RW`| `0x0`| `0x0`|
|`[15:12]`|`txlineb1errfrc`| Enable/disable force B1 error for 4 tx lines. Bit[0] for line 0.| `RW`| `0x0`| `0x0`|
|`[11:8]`|`txfrmscren`| Enable/disable scrambling for 4 tx lines. Bit[0] for line 0. 1: Enable 0: Disable| `RW`| `0xf`| `0xf`|
|`[7:0]`|`txfrmstmocnmode`| STM rate mode for Tx of 4 lines, each line use 2 bits.Bits[1:0] for line 0 0: OC48 1: OC12 2: OC3| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer Status

* **Description**           

Rx Framer status


* **RTL Instant Name**    : `rxfrmsta`

* **Address**             : `0x00010 - 0x00040`

* **Formula**             : `0x00010 + 16*LineId`

* **Where**               : 

    * `$LineId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[2]`|`oofcurstatus`| OOF current status in the related line. 1: OOF state 0: Not OOF state.| `RW`| `0x0`| `0x0`|
|`[1]`|`lofcurstatus`| LOF current status in the related line. 1: LOF state 0: Not LOF state.| `RW`| `0x0`| `0x0`|
|`[0]`|`loscurstatus`| LOS current status in the related line. 1: LOS state 0: Not LOS state.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer Sticky

* **Description**           

Rx Framer sticky


* **RTL Instant Name**    : `rxfrmstk`

* **Address**             : `0x00011 - 0x00041`

* **Formula**             : `0x00011 + 16*LineId`

* **Where**               : 

    * `$LineId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:5]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[4]`|`oc3algerrstk`| Set 1 while any error detected at OC3 Aligner engine.| `W1C`| `0x0`| `0x0`|
|`[3]`|`clkmonerrstk`| Set 1 while any error detected at Error clock monitor engine.| `W1C`| `0x0`| `0x0`|
|`[2]`|`oofstatechgstk`| Set 1 while OOF state change event happens in the related line.| `W1C`| `0x0`| `0x0`|
|`[1]`|`lofstatechgstk`| Set 1 while LOF state change event happens in the related line.| `W1C`| `0x0`| `0x0`|
|`[0]`|`losstatechgstk`| Set 1 while LOS state change event happens in the related line.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer B1 error counter read only

* **Description**           

Rx Framer B1 error counter read only


* **RTL Instant Name**    : `rxfrmb1cntro`

* **Address**             : `0x00012 - 0x00042`

* **Formula**             : `0x00012 + 16*LineId`

* **Where**               : 

    * `$LineId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxfrmb1errro`| Number of B1 error - read only| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer B1 error counter read to clear

* **Description**           

Rx Framer B1 error counter read to clear


* **RTL Instant Name**    : `rxfrmb1cntr2c`

* **Address**             : `0x00013 - 0x00043`

* **Formula**             : `0x00013 + 16*LineId`

* **Where**               : 

    * `$LineId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxfrmb1errr2c`| Number of B1 error - read to clear| `RC`| `0x0`| `0x0 End :`|

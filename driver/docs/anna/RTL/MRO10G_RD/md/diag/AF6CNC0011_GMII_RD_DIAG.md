## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2016-10-07|AF6Project|Initial version|




##AF6CNC0011_GMII_RD_DIAG
####Register Table

|Name|Address|
|-----|-----|
|`SGMII/QSGMII Channels Enable.`|`0x00 - 0x30`|
|`SGMII/QSGMII Burst test enable gen packet.`|`0x0f - 0x3f`|
|`SGMII/QSGMII Channels Sticky alarm.`|`0x01 - 0x31`|
|`SGMII/QSGMII Channels Status alarm.`|`0x0E - 0x3E`|
|`SGMII/QSGMII Channels Test mode control.`|`0x02 - 0x32`|
|`SGMII/QSGMII Channels Configure Mac DA MSB.`|`0x03 - 0x33`|
|`SGMII/QSGMII Channels Configure Mac DA LSB.`|`0x04 - 0x34`|
|`SGMII/QSGMII Channels Configure Mac SA MSB.`|`0x05 - 0x35`|
|`SGMII/QSGMII Channels Configure Mac SA LSB.`|`0x06 - 0x36`|
|`SGMII/QSGMII Channels Configure Mac Type.`|`0x07 - 0x37`|
|`SGMII/QSGMII Tx counter R2C.`|`0x08 - 0x38`|
|`SGMII/QSGMII Rx counter R2C.`|`0x09 - 0x39`|
|`SGMII/QSGMII FCS error counter R2C.`|`0x0A - 0x3A`|
|`SGMII/QSGMII PRBS error counter R2C.`|`0x0E - 0x3E`|
|`SGMII/QSGMII Tx counter RO.`|`0x0B - 0x3B`|
|`SGMII/QSGMII Rx counter RO.`|`0x0C - 0x3C`|
|`SGMII/QSGMII FCS error counter RO.`|`0x0D - 0x3D`|
|`SGMII/QSGMII PRBS error counter RO.`|`0x0F - 0x3F`|


###SGMII/QSGMII Channels Enable.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `control_pen1`

* **Address**             : `0x00 - 0x30`

* **Formula**             : `0x00 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`test enable`| Set 1 to enable Tx test generator| `R/W`| `0x0`| `0x0`|
|`[31:1]`|`unsed`| Unsed| `R/W`| `0x0`| `0x0 End : Begin:`|

###SGMII/QSGMII Burst test enable gen packet.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `control_pen2`

* **Address**             : `0x0f - 0x3f`

* **Formula**             : `0x0f + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`burst gen packet number`| Burst gen packet number, must configure burst number > 0 to generator packet, hardware will clear number packet configure when finish.| `R/W`| `0x0`| `0x0 End : Begin:`|

###SGMII/QSGMII Channels Sticky alarm.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `Alarm_sticky`

* **Address**             : `0x01 - 0x31`

* **Formula**             : `0x01 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Sticky`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mon packet error`| Set 1 when monitor packet error.| `R/W/C`| `0x0`| `0x0`|
|`[1]`|`mon packet len error`| Set 1 when monitor packet len error.| `R/W/C`| `0x0`| `0x0`|
|`[2]`|`mon fcs error`| Set 1 when monitor FCS error.| `R/W/C`| `0x0`| `0x0`|
|`[3]`|`mon data error`| Set 1 when monitor data error.| `R/W/C`| `0x0`| `0x0`|
|`[4]`|`mon mac header error`| Set 1 when monitor MAC header error.| `R/W/C`| `0x0`| `0x0`|
|`[5]`|`mon data sync`| Set 1 when monitor Data sync.| `R/W/C`| `0x0`| `0x0`|
|`[31:6]`|`unused`| *n/a*| `R/W/C`| `0x0`| `0x0 End : Begin:`|

###SGMII/QSGMII Channels Status alarm.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `Alarm_status`

* **Address**             : `0x0E - 0x3E`

* **Formula**             : `0x0E + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mon packet error`| Set 1 when monitor packet error.| `RO`| `0x0`| `0x0`|
|`[1]`|`mon packet len error`| Set 1 when monitor packet len error.| `RO`| `0x0`| `0x0`|
|`[2]`|`mon fcs error`| Set 1 when monitor FCS error.| `RO`| `0x0`| `0x0`|
|`[3]`|`mon data error`| Set 1 when monitor data error.| `RO`| `0x0`| `0x0`|
|`[4]`|`mon mac header error`| Set 1 when monitor MAC header error.| `RO`| `0x0`| `0x0`|
|`[5]`|`mon data sync`| Set 1 when monitor Data sync.| `RO`| `0x0`| `0x0`|
|`[31:6]`|`unsed`| Unsed| `RO`| `0x0`| `0x0 End : Begin:`|

###SGMII/QSGMII Channels Test mode control.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `control_pen3`

* **Address**             : `0x02 - 0x32`

* **Formula**             : `0x02 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`loopout enable`| FPGA loopout enable: 0/1 -> Dis/En.| `R/W`| `0x0`| `0x0`|
|`[1]`|`mac header check enable`| Mac header check enable: 0/1 -> Dis/En.| `R/W`| `0x0`| `0x0`|
|`[2]`|`configure test mode`| Configure test mode: 0/1 -> Continues/Burst.| `R/W`| `0x0`| `0x0`|
|`[3]`|`invert fcs enable`| Invert FCS enable: 0/1 -> Dis/En.| `R/W`| `0x1`| `0x1`|
|`[5:4]`|`force error`| Force error: 0/1/2/3 -> Normal/Data/Force FCS/Force Framer.| `R/W`| `0x0`| `0x0`|
|`[7:6]`|`bandwidth`| Bandwidth: 0/1/2/3 -> 100%/90%/80%/70%.| `R/W`| `0x0`| `0x0`|
|`[10:8]`|`data mode`| Data mode: 0:PRBS7 1:PRBS15 2:PRBS23 3:PRBS31 4:FIX 5:SEQ| `R/W`| `0x0`| `0x0`|
|`[19:12]`|`fix data value`| Fix data value.| `R/W`| `0xAA`| `0xAA`|
|`[31:20]`|`payload size`| Payload size.| `R/W`| `0x40`| `0x40 End : Begin:`|

###SGMII/QSGMII Channels Configure Mac DA MSB.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `control_pen4`

* **Address**             : `0x03 - 0x33`

* **Formula**             : `0x03 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`mac da configure msb`| Mac DA MSB configure value| `R/W`| `0x01010101`| `0x01010101 End : Begin:`|

###SGMII/QSGMII Channels Configure Mac DA LSB.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `control_pen5`

* **Address**             : `0x04 - 0x34`

* **Formula**             : `0x04 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `16`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`mac da configure lsb`| Mac DA LSB configure value| `R/W`| `0xC0CA`| `0xC0CA End : Begin:`|

###SGMII/QSGMII Channels Configure Mac SA MSB.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `control_pen6`

* **Address**             : `0x05 - 0x35`

* **Formula**             : `0x05 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`mac sa configure msb`| Mac SA MSB configure value| `R/W`| `0x01010101`| `0x01010101 End : Begin:`|

###SGMII/QSGMII Channels Configure Mac SA LSB.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `control_pen7`

* **Address**             : `0x06 - 0x36`

* **Formula**             : `0x06 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `16`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`mac sa configure lsb`| Mac SA LSB configure value| `R/W`| `0xC0CA`| `0xC0CA End : Begin:`|

###SGMII/QSGMII Channels Configure Mac Type.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `control_pen8`

* **Address**             : `0x07 - 0x37`

* **Formula**             : `0x07 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `16`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`mac type configure`| Mac Type configure value| `R/W`| `0x0800`| `0x0800 End : Begin:`|

###SGMII/QSGMII Tx counter R2C.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `counter1`

* **Address**             : `0x08 - 0x38`

* **Formula**             : `0x08 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tx counter r2c`| Tx counter R2C| `R2C`| `0x0`| `0x0 End : Begin:`|

###SGMII/QSGMII Rx counter R2C.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `counter2`

* **Address**             : `0x09 - 0x39`

* **Formula**             : `0x09 + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx counter r2c`| Rx counter R2C| `R2C`| `0x0`| `0x0 End : Begin:`|

###SGMII/QSGMII FCS error counter R2C.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `counter3`

* **Address**             : `0x0A - 0x3A`

* **Formula**             : `0x0A + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`fcs error counter r2c`| FCS error counter R2C| `R2C`| `0x0`| `0x0 End : Begin:`|

###SGMII/QSGMII PRBS error counter R2C.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `counter3`

* **Address**             : `0x0E - 0x3E`

* **Formula**             : `0x0E + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`prbs error counter r2c`| PRBS error counter R2C| `R2C`| `0x0`| `0x0 End : Begin:`|

###SGMII/QSGMII Tx counter RO.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `counter4`

* **Address**             : `0x0B - 0x3B`

* **Formula**             : `0x0B + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tx counter ro`| Tx counter RO| `RO`| `0x0`| `0x0 End : Begin:`|

###SGMII/QSGMII Rx counter RO.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `counter5`

* **Address**             : `0x0C - 0x3C`

* **Formula**             : `0x0C + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx counter ro`| Rx counter RO| `RO`| `0x0`| `0x0 End : Begin:`|

###SGMII/QSGMII FCS error counter RO.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `counter6`

* **Address**             : `0x0D - 0x3D`

* **Formula**             : `0x0D + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`fcs error counter ro`| FCS error counter RO| `RO`| `0x0`| `0x0 End : Begin:`|

###SGMII/QSGMII PRBS error counter RO.

* **Description**           

Register to test SGMII/QSGMII interface


* **RTL Instant Name**    : `counter6`

* **Address**             : `0x0F - 0x3F`

* **Formula**             : `0x0F + channel_id*16`

* **Where**               : 

    * `$channel_id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`prbs error counter ro`| PRBS error counter RO| `RO`| `0x0`| `0x0 End :`|

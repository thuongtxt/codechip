## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_OCN
####Register Table

|Name|Address|
|-----|-----|
|`OCN Global Rx Framer Control`|`0x00000`|
|`OCN Global Rx Framer LOS Detecting Control 1`|`0x00006`|
|`OCN Global Rx Framer LOS Detecting Control 2`|`0x00007`|
|`OCN Global Tx Framer Control`|`0x00001`|
|`OCN Global STS Pointer Interpreter Control`|`0x00002`|
|`OCN Global VTTU Pointer Interpreter Control`|`0x00003`|
|`OCN Global Pointer Generator Control`|`0x00004`|
|`OCN STS Pointer Interpreter Per Channel Control`|`0x22000 - 0x22e2f`|
|`OCN STS Pointer Generator Per Channel Control`|`0x23000 - 0x23e2f`|
|`OCN Rx SXC Control`|`0x24000 - 0x24d2f`|
|`OCN Tx SXC Control`|`0x25000 - 0x2572f`|
|`OCN RXPP Per STS payload Control`|`0x40000 - 0x5402f`|
|`OCN VTTU Pointer Interpreter Per Channel Control`|`0x40800 - 0x54fff`|
|`OCN TXPP Per STS Multiplexing Control`|`0x60000 - 0x7402f`|
|`OCN VTTU Pointer Generator Per Channel Control`|`0x60800 - 0x74fff`|
|`OCN Rx Framer Status`|`0x20000 - 0x20e00`|
|`OCN Rx Framer Sticky`|`0x20001 - 0x20e01`|
|`OCN Rx Framer B1 error counter read only`|`0x20002 - 0x20e02`|
|`OCN Rx Framer B1 error counter read to clear`|`0x20003 - 0x20e03`|
|`OCN Rx STS/VC per Alarm Interrupt Status`|`0x22140 - 0x22f6f`|
|`OCN Rx STS/VC per Alarm Current Status`|`0x22180 - 0x22faf`|
|`OCN Rx VT/TU per Alarm Interrupt Status`|`0x42800 - 0x56fff`|
|`OCN Rx VT/TU per Alarm Current Status`|`0x43000 - 0x57fff`|
|`OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter`|`0x22080 - 0x22eaf`|
|`OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter`|`0x41000 - 0x55fff`|
|`OCN TxPg STS per Alarm Interrupt Status`|`0x23140 - 0x23f6f`|
|`OCN TxPg STS pointer adjustment per channel counter`|`0x23080 - 0x23eaf`|
|`OCN TxPg VTTU per Alarm Interrupt Status`|`0x62800 - 0x76fff`|
|`OCN TxPg VTTU pointer adjustment per channel counter`|`0x61000 - 0x75fff`|
|`OCN Rx Bridge and Roll SXC Control`|`0x26000 - 0x2672f`|
|`OCN Tx Bridge and Roll SXC Control`|`0x27000 - 0x2772f`|
|`OCN Rx High Order Map concatenate configuration`|`0x28000 - 0x2872f`|
|`OCN Parity Force Config 1 - RxBar+TxBar+SPI+SPG`|`0x00010`|
|`OCN Parity Disable Config 1 - RxBar+TxBar+SPI+SPG`|`0x00011`|
|`OCN Parity Error Sticky 1 - RxBar+TxBar+SPI+SPG`|`0x00012`|
|`OCN Parity Force Config 2 - RxSXC+TxSXC+OHO`|`0x00014`|
|`OCN Parity Disable Config 2 - RxSXC+TxSXC+OHO`|`0x00015`|
|`OCN Parity Error Sticky 2 - RxSXC+TxSXC+OHO`|`0x00016`|
|`OCN Parity Force Config 3 - VPI+VPG`|`0x00018`|
|`OCN Parity Disable Config 3 - VPI+VPG`|`0x00019`|
|`OCN Parity Error Sticky 3 - VPI+VPG`|`0x0001a`|


###OCN Global Rx Framer Control

* **Description**           

This is the global configuration register for the Rx Framer


* **RTL Instant Name**    : `glbrfm_reg`

* **Address**             : `0x00000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:16]`|`rxlinesyncsel`| Select line id for synchronization 8 rx lines. Bit[0] for line 0. Note that must only 1 bit is set| `RW`| `0x1`| `0x1`|
|`[15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`rxfrmlosaisen`| Enable/disable forwarding P_AIS when LOS detected at Rx Framer. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[13]`|`rxfrmoofaisen`| Enable/disable forwarding P_AIS when OOF detected at Rx Framer. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[12]`|`rxfrmb1blockcntmod`| B1 Counter Mode. 1: Block mode 0: Bit-wise mode| `RW`| `0x1`| `0x1`|
|`[11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10:8]`|`rxfrmbadfrmthresh`| Threshold for A1A2 missing counter, that is used to change state from FRAMED to HUNT.| `RW`| `0x3`| `0x3`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6:4]`|`rxfrmb1goodthresh`| Threshold for B1 good counter, that is used to change state from CHECK to FRAMED.| `RW`| `0x4`| `0x4`|
|`[3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`rxfrmdescren`| Enable/disable de-scrambling of the Rx coming data stream. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[1]`|`rxfrmb1chkfrmen`| Enable/disable B1 check option is added to the required framing algorithm. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[0]`|`rxfrmessimodeen`| TFI-5 mode. 1: Enable 0: Disable| `RW`| `0x1`| `0x1 End : Begin:`|

###OCN Global Rx Framer LOS Detecting Control 1

* **Description**           

This is the global configuration register for the Rx Framer LOS detecting 1


* **RTL Instant Name**    : `glbclkmon_reg`

* **Address**             : `0x00006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`rxfrmlosdetmod`| Detected LOS mode. 1: the LOS declare when all zero or all one detected 0: the LOS declare when all zero detected| `RW`| `0x1`| `0x1`|
|`[17]`|`rxfrmlosdetdis`| Disable detect LOS. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[16]`|`rxfrmclkmondis`| Disable to generate LOS to Rx Framer if detecting error on Rx line clock. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[15:0]`|`rxfrmclkmonthr`| Threshold to generate LOS to Rx Framer if detecting error on Rx line clock.(Threshold = PPM * 155.52/8). Default 0x3cc ~ 50ppm| `RW`| `0x3cc`| `0x3cc End : Begin:`|

###OCN Global Rx Framer LOS Detecting Control 2

* **Description**           

This is the global configuration register for the Rx Framer LOS detecting 2


* **RTL Instant Name**    : `glbdetlos_pen`

* **Address**             : `0x00007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`rxfrmlosclr2thr`| Configure the period of time during which there is no period of consecutive data without transition, used for reset no transition counter. The recommended value is 0x30 (~2.5us).| `RW`| `0x30`| `0x30`|
|`[23:12]`|`rxfrmlossetthr`| Configure the value that define the period of time in which there is no transition detected from incoming data from SERDES. The recommended value is 0x3cc (~50us).| `RW`| `0x3cc`| `0x3cc`|
|`[11:0]`|`rxfrmlosclr1thr`| Configure the period of time during which there is no period of consecutive data without transition, used for counting at INFRM to change state to LOS The recommended value is 0x3cc (~50us).| `RW`| `0x3cc`| `0x3cc End : Begin:`|

###OCN Global Tx Framer Control

* **Description**           

This is the global configuration register for the Tx Framer


* **RTL Instant Name**    : `glbtfm_reg`

* **Address**             : `0x00001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`txlineooffrc`| Enable/disable force OOF for 8 tx lines. Bit[0] for line 0.| `RW`| `0x0`| `0x0`|
|`[23:16]`|`txlineb1errfrc`| Enable/disable force B1 error for 8 tx lines. Bit[0] for line 0.| `RW`| `0x0`| `0x0`|
|`[15]`|`txltohpwb2en`| Enable selecting for B2 value from TOHPW| `RW`| `0x0`| `0x0 1: Enable 0: Disable`|
|`[14]`|`txltohpwb1en`| Enable selecting for B1 value from TOHPW| `RW`| `0x0`| `0x0 1: Enable 0: Disable`|
|`[13]`|`txltohpwj0en`| Enable selecting for J0 value from TOHPW| `RW`| `0x0`| `0x0 1: Enable 0: Disable`|
|`[12]`|`txltohpwdis`| Disable function TOHPW insertion| `RW`| `0x0`| `0x0 1: Disable 0: Enable`|
|`[11:4]`|`txlinesyncsel`| Select line id for synchronization 8 tx lines. Bit[0] for line 0. Note that must only 1 bit is set| `RW`| `0x1`| `0x1`|
|`[3:1]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`txfrmscren`| Enable/disable scrambling of the Tx data stream. 1: Enable 0: Disable| `RW`| `0x1`| `0x1 End : Begin:`|

###OCN Global STS Pointer Interpreter Control

* **Description**           

This is the global configuration register for the STS Pointer Interpreter


* **RTL Instant Name**    : `glbspi_reg`

* **Address**             : `0x00002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[27:24]`|`rxpgflowthresh`| Overflow/underflow threshold to resynchronize read/write pointer.| `RW`| `0x3`| `0x3`|
|`[23:20]`|`rxpgadjthresh`| Adjustment threshold to make a pointer increment/decrement.| `RW`| `0xc`| `0xc`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`stspiaisaispen`| Enable/Disable forwarding P_AIS when AIS state is detected at STS Pointer interpreter. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[17]`|`stspilopaispen`| Enable/Disable forwarding P_AIS when LOP state is detected at STS Pointer interpreter. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[16]`|`stspimajormode`| Majority mode for detecting increment/decrement at STS pointer Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5| `RW`| `0x1`| `0x1`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:12]`|`stspinorptrthresh`| Threshold of number of normal pointers between two contiguous frames within pointer adjustments.| `RW`| `0x3`| `0x3`|
|`[11:8]`|`stspindfptrthresh`| Threshold of number of contiguous NDF pointers for entering LOP state at FSM.| `RW`| `0x8`| `0x8`|
|`[7:4]`|`stspibadptrthresh`| Threshold of number of contiguous invalid pointers for entering LOP state at FSM.| `RW`| `0x8`| `0x8`|
|`[3:0]`|`stspipohaistype`| Enable/disable STS POH defect types to downstream AIS in case of terminating the related STS such as the STS carries VT/TU. [0]: Enable for TIM defect [1]: Enable for Unequiped defect [2]: Enable for VC-AIS [3]: Enable for PLM defect| `RW`| `0xf`| `0xf End : Begin:`|

###OCN Global VTTU Pointer Interpreter Control

* **Description**           

This is the global configuration register for the VTTU Pointer Interpreter


* **RTL Instant Name**    : `glbvpi_reg`

* **Address**             : `0x00003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29]`|`vtpilomaispen`| Enable/Disable forwarding AIS when LOM is detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[28]`|`vtpilominvlcntmod`| H4 monitoring mode. 1: Expected H4 is current frame in the validated sequence plus one. 0: Expected H4 is the last received value plus one.| `RW`| `0x0`| `0x0`|
|`[27:24]`|`vtpilomgoodthresh`| Threshold of number of contiguous frames with validated sequence	of multi framers in LOM state for condition to entering IM state.| `RW`| `0x3`| `0x3`|
|`[23:20]`|`vtpilominvlthresh`| Threshold of number of contiguous frames with invalidated sequence of multi framers in IM state  for condition to entering LOM state.| `RW`| `0x8`| `0x8`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`vtpiaisaispen`| Enable/Disable forwarding AIS when AIS state is detected at VTTU Pointer interpreter. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[17]`|`vtpilopaispen`| Enable/Disable forwarding AIS when LOP state is detected at VTTU Pointer interpreter. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[16]`|`vtpimajormode`| Majority mode detecting increment/decrement in VTTU pointer Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5| `RW`| `0x1`| `0x1`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:12]`|`vtpinorptrthresh`| Threshold of number of normal pointers between two contiguous frames within pointer adjustments.| `RW`| `0x3`| `0x3`|
|`[11:8]`|`vtpindfptrthresh`| Threshold of number of contiguous NDF pointers for entering LOP state at FSM.| `RW`| `0x8`| `0x8`|
|`[7:4]`|`vtpibadptrthresh`| Threshold of number of contiguous invalid pointers for entering LOP state at FSM.| `RW`| `0x8`| `0x8`|
|`[3:0]`|`vtpipohaistype`| Enable/disable VTTU POH defect types to downstream AIS in case of terminating the related VTTU. [0]: Enable for TIM defect [1]: Enable for Un-equipment defect [2]: Enable for VC-AIS [3]: Enable for PLM defect| `RW`| `0xf`| `0xf End : Begin:`|

###OCN Global Pointer Generator Control

* **Description**           

This is the global configuration register for the Tx Pointer Generator


* **RTL Instant Name**    : `glbtpg_reg`

* **Address**             : `0x00004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9:8]`|`txpgnorptrthresh`| Threshold of number of normal pointers between two contiguous frames to make a condition of pointer adjustments.| `RW`| `0x3`| `0x3`|
|`[7:4]`|`txpgflowthresh`| Overflow/underflow threshold to resynchronize read/write pointer of TxFiFo.| `RW`| `0x3`| `0x3`|
|`[3:0]`|`txpgadjthresh`| Adjustment threshold to make a condition of pointer increment/decrement.| `RW`| `0xd`| `0xd End : Begin:`|

###OCN STS Pointer Interpreter Per Channel Control

* **Description**           

Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3.

Backdoor		: irxpp_stspp_inst.irxpp_stspp[0].rxpp_stspiramctl.array


* **RTL Instant Name**    : `spiramctl`

* **Address**             : `0x22000 - 0x22e2f`

* **Formula**             : `0x22000 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15]`|`stspiremoteloopback`| Enable/disable Remote Loopback. This bit will be set to 1 send to CPU a STS-AIS alarm 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[14]`|`stspichklom`| Enable/disable LOM checking. This field will be set to 1 when payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[13:12]`|`stspissdetpatt`| Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction.| `RW`| `0x0`| `0x0`|
|`[11]`|`stspiaisfrc`| Forcing SFM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[10]`|`stspissdeten`| Enable/disable checking SS bits in STSPI state machine. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[9]`|`stspiadjrule`| Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode| `RW`| `0x0`| `0x0`|
|`[8]`|`stspistsslvind`| This is used to configure STS is slaver or master. 1: Slaver.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`stspistsmstid`| This is the ID of the master STS-1 in the concatenation that contains this STS-1.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN STS Pointer Generator Per Channel Control

* **Description**           

Each register is used to configure for STS pointer Generator engines.

Backdoor		: itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array


* **RTL Instant Name**    : `spgramctl`

* **Address**             : `0x23000 - 0x23e2f`

* **Formula**             : `0x23000 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`stspgstsslvind`| This is used to configure STS is slaver or master. 1: Slaver.| `RW`| `0x0`| `0x0`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:8]`|`stspgstsmstid`| This is the ID of the master STS-1 in the concatenation that contains this STS-1.| `RW`| `0x0`| `0x0`|
|`[7]`|`stspgb3biperrfrc`| Forcing B3 Bip error. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[6]`|`stspglopfrc`| Forcing LOP. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[5]`|`stspgueqfrc`| Forcing SFM to UEQ state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[4]`|`stspgaisfrc`| Forcing SFM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[3:2]`|`stspgssinspatt`| Configure pattern SS bits that is used to insert to pointer value.| `RW`| `0x0`| `0x0`|
|`[1]`|`stspgssinsen`| Enable/disable SS bits insertion. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`stspgpohins`| Enable/disable POH Insertion. High to enable insertion of POH.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx SXC Control

* **Description**           

Each register is used for each outgoing STS (to PDH) of any line (16 lines - 6 Lo and 8 Ho) can be randomly configured to //connect  to any ingoing STS of any ingoing line (from TFI-5 line - 8 lines).

Backdoor		: isdhsxc_inst.isxc_rxrd[0].rxsxcramctl.array


* **RTL Instant Name**    : `rxsxcramctl`

* **Address**             : `0x24000 - 0x24d2f`

* **Formula**             : `0x24000 + 256*LineId + StsId`

* **Where**               : 

    * `$LineId(0-13)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`rxsxclineid`| Contains the ingoing LineID (0-7) or used to Configure loop back to PDH (If value LineID is 15). Disconnect (output is all one) if value LineID is other value (recommend is 14).| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`rxsxcstsid`| Contains the ingoing STSID (0-47).| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Tx SXC Control

* **Description**           

Each register is used for each outgoing STS (to TFI-5 Line) of any line (8 lines) can be randomly configured to connect to any ingoing STS of any ingoing line (from PDH) (16 lines - 6 Lo and 8 Ho).

Backdoor		: isdhsxc_inst.isxc_txrd[0].txxcramctl.array


* **RTL Instant Name**    : `txsxcramctl`

* **Address**             : `0x25000 - 0x2572f`

* **Formula**             : `0x25000 + 256*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`txsxclineid`| Contains the ingoing LineID (0-13: 0-5 for Lo) or used to Configure loop back to TFI-5 (If value LineID is 15). Disconnect (output is all one) if value LineID is other value (recommend is 14).| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`txsxcstsid`| Contains the ingoing STSID (0-47).| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN RXPP Per STS payload Control

* **Description**           

Each register is used to configure VT payload mode per STS.

Backdoor		: irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_demramctl.array


* **RTL Instant Name**    : `demramctl`

* **Address**             : `0x40000 - 0x5402f`

* **Formula**             : `0x40000 + 16384*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17]`|`pidemaisdownst`| Enable/disable AIS downstream to Lo Bus. (Used for Remote loopback function) 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[16]`|`pidemststerm`| Enable to terminate the related STS/VC. It means that STS POH defects related to the STS/VC to generate AIS to downstream. Must be set to 1. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[15:14]`|`pidemspetype`| Configure types of SPE. 0: Disable processing pointers of all VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2: TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.| `RW`| `0x0`| `0x0`|
|`[13:12]`|`pidemtug26type`| Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12| `RW`| `0x0`| `0x0`|
|`[11:10]`|`pidemtug25type`| Configure types of VT/TUs in TUG-2 #5.| `RW`| `0x0`| `0x0`|
|`[9:8]`|`pidemtug24type`| Configure types of VT/TUs in TUG-2 #4.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`pidemtug23type`| Configure types of VT/TUs in TUG-2 #3.| `RW`| `0x0`| `0x0`|
|`[5:4]`|`pidemtug22type`| Configure types of VT/TUs in TUG-2 #2.| `RW`| `0x0`| `0x0`|
|`[3:2]`|`pidemtug21type`| Configure types of VT/TUs in TUG-2 #1.| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pidemtug20type`| Configure types of VT/TUs in TUG-2 #0.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN VTTU Pointer Interpreter Per Channel Control

* **Description**           

Each register is used to configure for VTTU pointer interpreter engines.

Backdoor		: irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_vpiramctl.array


* **RTL Instant Name**    : `vpiramctl`

* **Address**             : `0x40800 - 0x54fff`

* **Formula**             : `0x40800 + 16384*LineId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`vtpiremoteloopback`| Enable/disable Remote Loopback. This bit will be set to 1 send to CPU a VT-AIS alarm 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[5]`|`vtpiloterm`| Enable to terminate the related VTTU. It means that VTTU POH defects related to the VTTU to generate AIS to downstream.| `RW`| `0x0`| `0x0`|
|`[4]`|`vtpiaisfrc`| Forcing SFM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[3:2]`|`vtpissdetpatt`| Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction.| `RW`| `0x0`| `0x0`|
|`[1]`|`vtpissdeten`| Enable/disable checking SS bits in PI State Machine. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`vtpiadjrule`| Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN TXPP Per STS Multiplexing Control

* **Description**           

Each register is used to configure VT payload mode per STS at Tx pointer generator.

Backdoor		: itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgdmctl.array


* **RTL Instant Name**    : `pgdemramctl`

* **Address**             : `0x60000 - 0x7402f`

* **Formula**             : `0x60000 + 16384*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:14]`|`pgdemspetype`| Configure types of SPE. 0: Disable processing pointers of all VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2: TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.| `RW`| `0x0`| `0x0`|
|`[13:12]`|`pgdemtug26type`| Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12| `RW`| `0x0`| `0x0`|
|`[11:10]`|`pgdemtug25type`| Configure types of VT/TUs in TUG-2 #5.| `RW`| `0x0`| `0x0`|
|`[9:8]`|`pgdemtug24type`| Configure types of VT/TUs in TUG-2 #4.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`pgdemtug23type`| Configure types of VT/TUs in TUG-2 #3.| `RW`| `0x0`| `0x0`|
|`[5:4]`|`pgdemtug22type`| Configure types of VT/TUs in TUG-2 #2.| `RW`| `0x0`| `0x0`|
|`[3:2]`|`pgdemtug21type`| Configure types of VT/TUs in TUG-2 #1.| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pgdemtug20type`| Configure types of VT/TUs in TUG-2 #0.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN VTTU Pointer Generator Per Channel Control

* **Description**           

Each register is used to configure for VTTU pointer Generator engines.

Backdoor		: itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgctl.array


* **RTL Instant Name**    : `vpgramctl`

* **Address**             : `0x60800 - 0x74fff`

* **Formula**             : `0x60800 + 16384*LineId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`vtpgbiperrfrc`| Forcing Bip error. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[6]`|`vtpglopfrc`| Forcing LOP. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[5]`|`vtpgueqfrc`| Forcing SFM to UEQ state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[4]`|`vtpgaisfrc`| Forcing SFM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[3:2]`|`vtpgssinspatt`| Configure pattern SS bits that is used to insert to Pointer value.| `RW`| `0x0`| `0x0`|
|`[1]`|`vtpgssinsen`| Enable/disable SS bits insertion. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`vtpgpohins`| Enable/ disable POH Insertion. High to enable insertion of POH.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer Status

* **Description**           

Rx Framer status


* **RTL Instant Name**    : `rxfrmsta`

* **Address**             : `0x20000 - 0x20e00`

* **Formula**             : `0x20000 + 512*LineId`

* **Where**               : 

    * `$LineId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[2]`|`staffcnvful`| Status fifo convert clock domain is full| `RO`| `0x0`| `0x0`|
|`[1]`|`stalos`| Loss of Signal status| `RO`| `0x0`| `0x0`|
|`[0]`|`staoof`| Out of Frame that is detected at RxFramer| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer Sticky

* **Description**           

Rx Framer sticky


* **RTL Instant Name**    : `rxfrmstk`

* **Address**             : `0x20001 - 0x20e01`

* **Formula**             : `0x20001 + 512*LineId`

* **Where**               : 

    * `$LineId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[2]`|`stkffcnvful`| Sticky fifo convert clock domain is full| `W1C`| `0x0`| `0x0`|
|`[1]`|`stklos`| Loss of Signal  sticky change| `W1C`| `0x0`| `0x0`|
|`[0]`|`stkoof`| Out of Frame sticky  change| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer B1 error counter read only

* **Description**           

Rx Framer B1 error counter read only


* **RTL Instant Name**    : `rxfrmb1cntro`

* **Address**             : `0x20002 - 0x20e02`

* **Formula**             : `0x20002 + 512*LineId`

* **Where**               : 

    * `$LineId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxfrmb1errro`| Number of B1 error - read only| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer B1 error counter read to clear

* **Description**           

Rx Framer B1 error counter read to clear


* **RTL Instant Name**    : `rxfrmb1cntr2c`

* **Address**             : `0x20003 - 0x20e03`

* **Formula**             : `0x20003 + 512*LineId`

* **Where**               : 

    * `$LineId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxfrmb1errr2c`| Number of B1 error - read to clear| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN Rx STS/VC per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of STS/VC pointer interpreter.  %%

Each register is used to store 6 sticky bits for 6 alarms in the STS/VC.


* **RTL Instant Name**    : `upstschstkram`

* **Address**             : `0x22140 - 0x22f6f`

* **Formula**             : `0x22140 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:6]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[5]`|`stspistsconcdetintr`| Set to 1 while an Concatenation Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[4]`|`stspistsnewdetintr`| Set to 1 while an New Pointer Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[3]`|`stspistsndfintr`| Set to 1 while an NDF event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[2]`|`stspicepuneqstatepchgintr`| Set to 1  while there is change in CEP Un-equip Path in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in CEP Un-equip Path state or not.| `W1C`| `0x0`| `0x0`|
|`[1]`|`stspistsaisstatechgintr`| Set to 1 while there is change in AIS state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in AIS state or not.| `W1C`| `0x0`| `0x0`|
|`[0]`|`stspistslopstatechgintr`| Set 1 to while there is change in LOP state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in LOP state or not.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx STS/VC per Alarm Current Status

* **Description**           

This is the per Alarm current status of STS/VC pointer interpreter.  %%

Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC.


* **RTL Instant Name**    : `upstschstaram`

* **Address**             : `0x22180 - 0x22faf`

* **Formula**             : `0x22180 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[2]`|`stspistscepuneqpcurstatus`| CEP Un-eqip Path current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  StsPiStsCepUeqPStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.| `RO`| `0x0`| `0x0`|
|`[1]`|`stspistsaiscurstatus`| AIS current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  StsPiStsAISStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.| `RO`| `0x0`| `0x0`|
|`[0]`|`stspistslopcurstatus`| LOP current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  StsPiStsLopStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx VT/TU per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of VT/TU pointer interpreter . Each register is used to store 5 sticky bits for 5 alarms in the VT/TU.


* **RTL Instant Name**    : `upvtchstkram`

* **Address**             : `0x42800 - 0x56fff`

* **Formula**             : `0x42800 + 16384*LineId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:5]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[4]`|`vtpistsnewdetintr`| Set to 1 while an New Pointer Detection event is detected at VT/TU pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[3]`|`vtpistsndfintr`| Set to 1 while an NDF event is detected at VT/TU pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[2]`|`vtpistscepuneqvstatechgintr`| Set 1 to while there is change in Unequip state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in Uneqip state or not.| `W1C`| `0x0`| `0x0`|
|`[1]`|`vtpistsaisstatechgintr`| Set 1 to while there is change in AIS state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in LOP state or not.| `W1C`| `0x0`| `0x0`|
|`[0]`|`vtpistslopstatechgintr`| Set 1 to while there is change in LOP state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in AIS state or not.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx VT/TU per Alarm Current Status

* **Description**           

This is the per Alarm current status of VT/TU pointer interpreter. Each register is used to store 3 bits to store current status of 3 alarms in the VT/TU.


* **RTL Instant Name**    : `upvtchstaram`

* **Address**             : `0x43000 - 0x57fff`

* **Formula**             : `0x43000 + 16384*LineId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[2]`|`vtpistscepuneqcurstatus`| Unequip current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsCepUneqVStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set.| `RO`| `0x0`| `0x0`|
|`[1]`|`vtpistsaiscurstatus`| AIS current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsAISStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set.| `RO`| `0x0`| `0x0`|
|`[0]`|`vtpistslopcurstatus`| LOP current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsLopStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode


* **RTL Instant Name**    : `adjcntperstsram`

* **Address**             : `0x22080 - 0x22eaf`

* **Formula**             : `0x22080 + 512*LineId + 64*AdjMode + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$AdjMode(0-1)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`rxppstspiptadjcnt`| The pointer Increment counter or decrease counter. Bit [6] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VT/TU pointer interpreter. These counters are in saturation mode


* **RTL Instant Name**    : `adjcntperstkram`

* **Address**             : `0x41000 - 0x55fff`

* **Formula**             : `0x41000 + 16384*LineId + 2048*AdjMode + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$AdjMode(0-1)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`rxppstspiptadjcnt`| The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg STS per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms


* **RTL Instant Name**    : `stspgstkram`

* **Address**             : `0x23140 - 0x23f6f`

* **Formula**             : `0x23140 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[3]`|`stspgaisintr`| Set to 1 while an AIS status event (at h2pos) is detected at Tx STS Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[2]`|`stspgfifoovfintr`| Set to 1 while an FIFO Overflowed event is detected at Tx STS Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[1]`|`stspgndfintr`| Set to 1 while an NDF status event (at h2pos) is detected at Tx STS Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[0]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg STS pointer adjustment per channel counter

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode


* **RTL Instant Name**    : `adjcntpgperstsram`

* **Address**             : `0x23080 - 0x23eaf`

* **Formula**             : `0x23080 + 512*LineId + 64*AdjMode + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$AdjMode(0-1)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`stspgptadjcnt`| The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg VTTU per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of STS/VT/TU pointer generator . Each register is used to store 3 sticky bits for 3 alarms


* **RTL Instant Name**    : `vtpgstkram`

* **Address**             : `0x62800 - 0x76fff`

* **Formula**             : `0x62800 + 16384*LineId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[3]`|`vtpgaisintr`| Set to 1 while an AIS status event (at h2pos) is detected at Tx VTTU Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[2]`|`vtpgfifoovfintr`| Set to 1 while an FIFO Overflowed event is detected at Tx VTTU Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[1]`|`vtpgndfintr`| Set to 1 while an NDF status event (at h2pos) is detected at Tx VTTU Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[0]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg VTTU pointer adjustment per channel counter

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VTTU pointer generator. These counters are in saturation mode


* **RTL Instant Name**    : `adjcntpgpervtram`

* **Address**             : `0x61000 - 0x75fff`

* **Formula**             : `0x61000 + 16384*LineId + 2048*AdjMode + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$AdjMode(0-1)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`vtpgptadjcnt`| The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Bridge and Roll SXC Control

* **Description**           

Each register is used for each outgoing STS (to Rx SPI) of any line (8 lines) can be randomly configured to //connect to any ingoing STS of any ingoing line (from TFI-5 line - 8 lines).

Backdoor		: irxfrm_inst.isdhbar_sxc.isdhbar_sxcrd[0].rxsxcramctl.array


* **RTL Instant Name**    : `rxbarsxcramctl`

* **Address**             : `0x26000 - 0x2672f`

* **Formula**             : `0x26000 + 256*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`rxbarsxclineid`| Contains the ingoing LineID (0-7). Disconnect (output is all one) if value LineID is other value (recommend is 14).| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`rxbarsxcstsid`| Contains the ingoing STSID (0-47).| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Tx Bridge and Roll SXC Control

* **Description**           

Each register is used for each outgoing STS (to TFI-5 Line) of any line (8 lines) can be randomly configured to connect to any ingoing STS of any ingoing line (from Tx SPG) .

Backdoor		: itxpp_stspp_inst.isdhtxbar_sxc.isdhbar_sxcrd[0].rxsxcramctl.array


* **RTL Instant Name**    : `txbarsxcramctl`

* **Address**             : `0x27000 - 0x2772f`

* **Formula**             : `0x27000 + 256*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`txbarsxclineid`| Contains the ingoing LineID (0-7). Disconnect (output is all one) if value LineID is other value (recommend is 14).| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`txsxcstsid`| Contains the ingoing STSID (0-47).| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx High Order Map concatenate configuration

* **Description**           

Each register is used to configure concatenation for each STS to High Order Map path.

Backdoor		: irxpp_outho_inst.irxpp_outputho [0].ohoramctl.array


* **RTL Instant Name**    : `rxhomapramctl`

* **Address**             : `0x28000 - 0x2872f`

* **Formula**             : `0x28000 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`homapaisdowns`| Enable/disable AIS downstream to Ho Bus. (Used for Remote loopback function) 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[8]`|`homapstsslvind`| This is used to configure STS is slaver or master. 1: Slaver.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`homapstsmstid`| This is the ID of the master STS-1 in the concatenation that contains this STS-1.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Force Config 1 - RxBar+TxBar+SPI+SPG

* **Description**           

OCN Parity Force Config 1


* **RTL Instant Name**    : `parfrccfg1`

* **Address**             : `0x00010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`spgparerrfrc`| Force Parity for ram config "OCN STS Pointer Generator Per Channel Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[23:16]`|`spiparerrfrc`| Force Parity for ram config "OCN STS Pointer Interpreter Per Channel Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[15:8]`|`txbarparerrfrc`| Force Parity for ram config "OCN Tx Bridge and Roll SXC Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`rxbarparerrfrc`| Force Parity for ram config "OCN Rx Bridge and Roll SXC Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Disable Config 1 - RxBar+TxBar+SPI+SPG

* **Description**           

OCN Parity Disable Config 1


* **RTL Instant Name**    : `pardiscfg1`

* **Address**             : `0x00011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`spgparerrfrc`| Disable Parity for ram config "OCN STS Pointer Generator Per Channel Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[23:16]`|`spiparerrfrc`| Disable Parity for ram config "OCN STS Pointer Interpreter Per Channel Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[15:8]`|`txbarparerrfrc`| Disable Parity for ram config "OCN Tx Bridge and Roll SXC Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`rxbarparerrfrc`| Disable Parity for ram config "OCN Rx Bridge and Roll SXC Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Error Sticky 1 - RxBar+TxBar+SPI+SPG

* **Description**           

OCN Parity Error Sticky 1


* **RTL Instant Name**    : `parerrstk1`

* **Address**             : `0x00012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`spgparerrfrc`| Error Sticky for ram config "OCN STS Pointer Generator Per Channel Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[23:16]`|`spiparerrfrc`| Error Sticky for ram config "OCN STS Pointer Interpreter Per Channel Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[15:8]`|`txbarparerrfrc`| Error Sticky for ram config "OCN Tx Bridge and Roll SXC Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[7:0]`|`rxbarparerrfrc`| Error Sticky for ram config "OCN Rx Bridge and Roll SXC Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Force Config 2 - RxSXC+TxSXC+OHO

* **Description**           

OCN Parity Force Config 2


* **RTL Instant Name**    : `parfrccfg2`

* **Address**             : `0x00014`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:22]`|`rxhoparerrfrc`| Force Parity for ram config "OCN Rx High Order Map concatenate configuration".Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[21:14]`|`txsxcparerrfrc`| Force Parity for ram config "OCN Tx SXC Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[13:0]`|`rxsxcparerrfrc`| Force Parity for ram config "OCN Rx SXC Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Disable Config 2 - RxSXC+TxSXC+OHO

* **Description**           

OCN Parity Disable Config 2


* **RTL Instant Name**    : `pardiscfg2`

* **Address**             : `0x00015`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:22]`|`rxhoparerrfrc`| Disable Parity for ram config "OCN Rx High Order Map concatenate configuration".Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[21:14]`|`txsxcparerrfrc`| Disable Parity for ram config "OCN Tx SXC Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[13:0]`|`rxsxcparerrfrc`| Disable Parity for ram config "OCN Rx SXC Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Error Sticky 2 - RxSXC+TxSXC+OHO

* **Description**           

OCN Parity Error Sticky 2


* **RTL Instant Name**    : `parerrstk2`

* **Address**             : `0x00016`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[29:22]`|`rxhoparerrfrc`| Error Sticky for ram config "OCN Rx High Order Map concatenate configuration".Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[21:14]`|`txsxcparerrfrc`| Error Sticky for ram config "OCN Tx SXC Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[13:0]`|`rxsxcparerrfrc`| Error Sticky for ram config "OCN Rx SXC Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Force Config 3 - VPI+VPG

* **Description**           

OCN Parity Force Config 3


* **RTL Instant Name**    : `parfrccfg3`

* **Address**             : `0x00018`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:18]`|`vpgctlparerrfrc`| Force Parity for ram config "OCN VTTU Pointer Generator Per Channel Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[17:12]`|`vpgdemparerrfrc`| Force Parity for ram config "OCN TXPP Per STS Multiplexing Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[11:6]`|`vpictlparerrfrc`| Force Parity for ram config "OCN VTTU Pointer Interpreter Per Channel Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[5:0]`|`vpidemparerrfrc`| Force Parity for ram config "OCN RXPP Per STS payload Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Disable Config 3 - VPI+VPG

* **Description**           

OCN Parity Disable Config 3


* **RTL Instant Name**    : `pardiscfg3`

* **Address**             : `0x00019`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:18]`|`vpgctlparerrfrc`| Disable Parity for ram config "OCN VTTU Pointer Generator Per Channel Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[17:12]`|`vpgdemparerrfrc`| Disable Parity for ram config "OCN TXPP Per STS Multiplexing Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[11:6]`|`vpictlparerrfrc`| Disable Parity for ram config "OCN VTTU Pointer Interpreter Per Channel Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[5:0]`|`vpidemparerrfrc`| Disable Parity for ram config "OCN RXPP Per STS payload Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Error Sticky 3 - VPI+VPG

* **Description**           

OCN Parity Error Sticky 3


* **RTL Instant Name**    : `parerrstk3`

* **Address**             : `0x0001a`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[23:18]`|`vpgctlparerrfrc`| Error Sticky for ram config "OCN VTTU Pointer Generator Per Channel Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[17:12]`|`vpgdemparerrfrc`| Error Sticky for ram config "OCN TXPP Per STS Multiplexing Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[11:6]`|`vpictlparerrfrc`| Error Sticky for ram config "OCN VTTU Pointer Interpreter Per Channel Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[5:0]`|`vpidemparerrfrc`| Error Sticky for ram config "OCN RXPP Per STS payload Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0 End :`|

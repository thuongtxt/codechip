## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_PLA
####Register Table

|Name|Address|
|-----|-----|
|`Payload Assembler Low-Order Payload Control`|`0x0_2000-0x3_63FF`|
|`Payload Assembler Low-Order Pseudowire Control`|`0x0_D000-0x3_DBFF`|
|`Payload Assembler Low-Order Add/Remove Pseudowire Protocol Control`|`0x0_2800-0x3_6BFF`|
|`Payload Assembler High-Order Payload Control`|`0x8_0100-0x8_0B2F`|
|`Payload Assembler High-Order Pseudowire Control`|`0x8_0680-0x8_10AF`|
|`Payload Assembler Hig-Order Add/Remove Pseudowire Protocol Control`|`0x8_0140-0x8_0B6F`|
|`Payload Assembler Output Pseudowire Control`|`0x9_0000-0xA_17FF`|
|`Payload Assembler Output UPSR Control`|`0x9_4000-0xC_5FFF`|
|`Payload Assembler Output HSPW Control`|`0x9_8000-0xC_8FFF`|
|`Payload Assembler Output PSN Process Control`|`0x8_4000`|
|`Payload Assembler Output PSN Buffer Control`|`0x8_4010-0x8_4015`|
|`Payload Assembler Hold Register Control`|`0x8_C000-0x8_C002`|
|`Read HA Address Bit3_0 Control`|`0x8C100 - 0x8C10F`|
|`Read HA Address Bit7_4 Control`|`0x8C110 - 0x8C11F`|
|`Read HA Address Bit11_8 Control`|`0x8C120 - 0x8C12F`|
|`Read HA Address Bit15_12 Control`|`0x08C130 - 0x8C13F`|
|`Read HA Address Bit19_16 Control`|`0x8C140 - 0x8C14F`|
|`Read HA Address Bit23_20 Control`|`0x8C150 - 0x8C15F`|
|`Read HA Address Bit24 and Data Control`|`0x8C160 - 0x8C161`|
|`Read HA Hold Data63_32`|`0x8C170`|
|`Read HA Hold Data95_64`|`0x8C171`|
|`Read HA Hold Data127_96`|`0x8C172`|
|`Payload Assembler Output PSN Process Control`|`0x8_4001`|
|`Payload Assembler Force Parity Error Control0`|`0x88040`|
|`Payload Assembler Disable Parity Check Control0`|`0x88041`|
|`Payload Assembler Parity Error Sticky0`|`0x88042`|
|`Payload Assembler Force Parity Error Control1`|`0x88044`|
|`Payload Assembler Disable Parity Check Control1`|`0x88045`|
|`Payload Assembler Parity Error Sticky1`|`0x88046`|
|`Payload Assembler Force CRC Error Control`|`0x88048`|
|`Payload Assembler Disable CRC Check Control`|`0x88049`|
|`Payload Assembler CRC Error Sticky`|`0x8804A`|
|`Pseudowire PLA FSM Control`|`0x88020`|


###Payload Assembler Low-Order Payload Control

* **Description**           

This register is used to configure payload in each LO Pseudowire channels

HDL_PATH: begin:

IF(($LoOc48Slice==0) && ($$LoOc24Slice==0))

ilosl1pla1cfgpw_ram.array.ram[$$LoPwid]

ELSEIF(($LoOc48Slice==0) && ($$LoOc24Slice==1))

ilosl1pla2cfgpw_ram.array.ram[$$LoPwid]

IF(($LoOc48Slice==1) && ($$LoOc24Slice==0))

ilosl2pla1cfgpw_ram.array.ram[$$LoPwid]

IF(($LoOc48Slice==1) && ($$LoOc24Slice==1))

ilosl2pla2cfgpw_ram.array.ram[$$LoPwid]

IF(($LoOc48Slice==2) && ($$LoOc24Slice==0))

ilosl3pla1cfgpw_ram.array.ram[$$LoPwid]

IF(($LoOc48Slice==2) && ($$LoOc24Slice==1))

ilosl3pla2cfgpw_ram.array.ram[$$LoPwid]

IF(($LoOc48Slice==3) && ($$LoOc24Slice==0))

ilosl4pla1cfgpw_ram.array.ram[$$LoPwid]

IF(($LoOc48Slice==3) && ($$LoOc24Slice==1))

ilosl4pla2cfgpw_ram.array.ram[$$LoPwid]

HDL_PATH: end:


* **RTL Instant Name**    : `pla_lo_pld_ctrl`

* **Address**             : `0x0_2000-0x3_63FF`

* **Formula**             : `0x0_2000 + $LoOc48Slice*65536 + $LoOc24Slice*16384 + $LoPwid`

* **Where**               : 

    * `$LoOc48Slice(0-3): LO OC-48 slices, there are total 4xOC-48 slices for LO 10G`

    * `$LoOc24Slice(0-1): LO OC-24 slices, there are total 2xOC-24 slices per LO OC-48 slice`

    * `$LoPwid(0-1343): pseudowire channel for each LO OC-24 slice`

* **Width**               : `18`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17]`|`plalopldtssrcctrl`| Timestamp Source 0: PRC global 1: TS from CDR engine| `RW`| `0x0`| `0x0`|
|`[16:15]`|`plalopldtypectrl`| Payload Type 0: satop 1: ces without cas 2: cep| `RW`| `0x0`| `0x0`|
|`[14:0]`|`plalopldsizectrl`| Payload Size satop/cep mode: bit[13:0] payload size (ex: value as 0x100 is payload size 256 bytes) ces mode: bit[5:0] number of DS0 timeslot bit[14:6] number of NxDS0 frame| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Low-Order Pseudowire Control

* **Description**           

This register is used to configure some pseudowire properties in each LO Pseudowire channels

HDL_PATH: begin:

IF($LoOc48Slice==0)

ilosl1pwblkcfg_ram.array.ram[$LoOc24Slice*1024 + $LoPwid]

IF($LoOc48Slice==1)

ilosl2pwblkcfg_ram.array.ram[$LoOc24Slice*1024 + $LoPwid]

IF($LoOc48Slice==2)

ilosl3pwblkcfg_ram.array.ram[$LoOc24Slice*1024 + $LoPwid]

IF($LoOc48Slice==3)

ilosl4pwblkcfg_ram.array.ram[$LoOc24Slice*1024 + $LoPwid]

HDL_PATH: end:


* **RTL Instant Name**    : `pla_lo_pw_ctrl`

* **Address**             : `0x0_D000-0x3_DBFF`

* **Formula**             : `0x0_D000 + $LoOc48Slice*65536 + $LoOc24Slice*2048 + $LoPwid`

* **Where**               : 

    * `$LoOc48Slice(0-3): LO OC-48 slices, there are total 4xOC-48 slices for LO 10G`

    * `$LoOc24Slice(0-1): LO OC-24 slices, there are total 2xOC-24 slices per LO OC-48 slice`

    * `$LoPwid(0-1343): pseudowire channel for each LO OC-24 slice`

* **Width**               : `22`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[21:20]`|`plalopwpidctrl`| Port ID output| `RW`| `0x0`| `0x0`|
|`[19:07]`|`plalopwpwidctrl`| real PW id lookup| `RW`| `0x0`| `0x0`|
|`[06]`|`plalopwsuprctrl`| Suppresion Enable 1: enable 0: disable| `RW`| `0x0`| `0x0`|
|`[05]`|`plalopwmbitdisctrl`| M or NP bits disable 1: disable M or NP bits in the Control Word (assign zero in the packet) 0: normal| `RW`| `0x0`| `0x0`|
|`[04]`|`plalopwlbitdisctrl`| L bit disable 1: disable L bit in the Control Word (assign zero in the packet) 0: normal| `RW`| `0x0`| `0x0`|
|`[3:2]`|`plalopwmbitcpuctrl`| M or NP bits value from CPU (low priority than PlaLoPwMbitDisCtrl)| `RW`| `0x0`| `0x0`|
|`[1]`|`plalopwlbitcpuctrl`| L bit value from CPU (low priority than PlaLoPwLbitDisCtrl)| `RW`| `0x0`| `0x0`|
|`[0]`|`plalopwenctrl`| LO Pseudowire enable 1: enable 0: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Low-Order Add/Remove Pseudowire Protocol Control

* **Description**           

This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each LO OC-24 slice


* **RTL Instant Name**    : `pla_lo_add_rmv_pw_ctrl`

* **Address**             : `0x0_2800-0x3_6BFF`

* **Formula**             : `0x0_2800 + $LoOc48Slice*65536 + $LoOc24Slice*16384 + $LoPwid`

* **Where**               : 

    * `$LoOc48Slice(0-3): LO OC-48 slices, there are total 4xOC-48 slices for LO 10G`

    * `$LoOc24Slice(0-1): LO OC-24 slices, there are total 2xOC-24 slices per LO OC-48 slice`

    * `$LoPwid(0-1343): pseudowire channel for each LO OC-24 slice`

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`plalostaaddrmvctrl`| protocol state to add/remove pw Step1: Add intitial or pw idle, the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1" value for the protocol state to start adding pw. The protocol state value is "1". Step3: CPU enables pw at demap to finish the adding pw process. HW will automatically change the protocol state value to "2" to run the pw, and keep this state. Step4: For removing the pw, CPU will write "3" value for the protocol state to start removing pw. The protocol state value is "3". Step5: Poll the protocol state until return value "0" value, after that CPU disables pw at demap to finish the removing pw process| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler High-Order Payload Control

* **Description**           

This register is used to configure payload in each HO Pseudowire channels


* **RTL Instant Name**    : `pla_ho_pldd_ctrl`

* **Address**             : `0x8_0100-0x8_0B2F`

* **Formula**             : `0x8_0100 + $HoOc96Slice*2048 + $HoOc48Slice*512 + $HoPwid`

* **Where**               : 

    * `$HoOc96Slice(0-1): HO OC-96 slices, there are total 2xOC-96 slices for HO 10G or 4xOC-96 slices for HO 20G`

    * `$HoOc48Slice(0-1): HO OC-48 slices, there are total 2xOC-48 slices per HO OC-96 slice`

    * `$HoPwid(0-63): pseudowire channel for each HO OC-48 slice, 0-47 for CEP, other is for TOH PW`

* **Width**               : `17`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16:15]`|`plalopldtypectrl`| Payload Type 0: satop 2: cep| `RW`| `0x0`| `0x0`|
|`[14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:0]`|`plalopldsizectrl`| Payload Size| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler High-Order Pseudowire Control

* **Description**           

This register is used to configure some pseudowire properties in each HO Pseudowire channels


* **RTL Instant Name**    : `pla_ho_pw_ctrl`

* **Address**             : `0x8_0680-0x8_10AF`

* **Formula**             : `0x8_0680 + $HoOc96Slice*2048 + $HoOc48Slice*64 + $HoPwid`

* **Where**               : 

    * `$HoOc96Slice(0-1): HO OC-96 slices, there are total 2xOC-96 slices for HO 10G or 4xOC-96 slices for HO 20G`

    * `$HoOc48Slice(0-1): HO OC-48 slices, there are total 2xOC-48 slices per HO OC-96 slice`

    * `$HoPwid(0-63): pseudowire channel for each HO OC-48 slice, 0-47 for CEP, other is for TOH PW`

* **Width**               : `22`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[21:20]`|`plahopwpidctrl`| Port ID output| `RW`| `0x0`| `0x0`|
|`[19:07]`|`plahopwpwidctrl`| real PW id lookup| `RW`| `0x0`| `0x0`|
|`[06]`|`plahopwsuprctrl`| Suppresion Enable 1: enable 0: disable| `RW`| `0x0`| `0x0`|
|`[05]`|`plahopwnpbitdisctrl`| NP bits disable 1: disable NP bits in the Control Word (assign zero in the packet) 0: normal| `RW`| `0x0`| `0x0`|
|`[04]`|`plahopwlbitdisctrl`| L bit disable 1: disable L bit in the Control Word (assign zero in the packet) 0: normal| `RW`| `0x0`| `0x0`|
|`[3:2]`|`plahopwmbitcpuctrl`| NP bits value from CPU (low priority than PlaHoPwNPbitDisCtrl)| `RW`| `0x0`| `0x0`|
|`[1]`|`plahopwlbitcpuctrl`| L bit value from CPU (low priority than PlaHoPwLbitDisCtrl)| `RW`| `0x0`| `0x0`|
|`[0]`|`plahopwenctrl`| HO Pseudowire enable 1: enable 0: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Hig-Order Add/Remove Pseudowire Protocol Control

* **Description**           

This register is used to add/remove pseudowire to prevent burst packets to other packet processing engines in each HO OC-48 slice


* **RTL Instant Name**    : `pla_ho_add_rmv_pw_ctrl`

* **Address**             : `0x8_0140-0x8_0B6F`

* **Formula**             : `0x8_0140 + $HoOc96Slice*2048 + $HoOc48Slice*512 + $HoPwid`

* **Where**               : 

    * `$HoOc96Slice(0-1): HO OC-96 slices, there are total 2xOC-96 slices for HO 10G or 4xOC-96 slices for HO 20G`

    * `$HoOc48Slice(0-1): HO OC-48 slices, there are total 2xOC-48 slices per HO OC-96 slice`

    * `$HoPwid(0-63): pseudowire channel for each HO OC-48 slice, 0-47 for CEP, other is for TOH PW`

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`plalostaaddrmvctrl`| protocol state to add/remove pw Step1: Add intitial or pw idle, the protocol state value is "zero" Step2: For adding the pw, CPU will Write "1" value for the protocol state to start adding pw. The protocol state value is "1". Step3: CPU enables pw at demap to finish the adding pw process. HW will automatically change the protocol state value to "2" to run the pw, and keep this state. Step4: For removing the pw, CPU will write "3" value for the protocol state to start removing pw. The protocol state value is "3". Step5: Poll the protocol state until return value "0" value, after that CPU disables pw at demap to finish the removing pw process| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Output Pseudowire Control

* **Description**           

This register is used to config psedowire at output

HDL_PATH : ip1pwcfg_ram.array.ram[$pwid]


* **RTL Instant Name**    : `pla_out_pw_ctrl`

* **Address**             : `0x9_0000-0xA_17FF`

* **Formula**             : `0x9_0000 + $pid*65536 + $pwid`

* **Where**               : 

    * `$pid(0-1): output port which the PW will be sent out`

    * `$pwid(0-6143): pseudowire channel`

* **Width**               : `37`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[36]`|`plaouthspwusedctr`| HSPW is used or not 0:not used, all configurations for HSPW will be not effected 1:used| `RW`| `0x0`| `0x0`|
|`[35]`|`plaoutupsrusedctr`| UPSR is used or not 0:not used, all configurations for UPSR will be not effected 1:used| `RW`| `0x0`| `0x0`|
|`[34]`|`plaoutcepctr`| PW is in a CEP mode| `RW`| `0x0`| `0x0`|
|`[33:22]`|`plaouthspwgrpctr`| HSPW group| `RW`| `0x0`| `0x0`|
|`[21:9]`|`plaoutupsrgrpctr`| UPRS group| `RW`| `0x0`| `0x0`|
|`[8]`|`plaoutrbitdisctrl`| R bit disable 1: disable R bit in the Control Word (assign zero in the packet) 0: normal| `RW`| `0x0`| `0x0`|
|`[7]`|`plaoutrbitcpuctrl`| R bit value from CPU (low priority than PlaOutRbitDisCtrl)| `RW`| `0x0`| `0x0`|
|`[6:0]`|`plaoutpsnlenctrl`| PSN length| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Output UPSR Control

* **Description**           

This register is used to config UPSR group


* **RTL Instant Name**    : `pla_out_upsr_ctrl`

* **Address**             : `0x9_4000-0xC_5FFF`

* **Formula**             : `0x9_4000 + $upsrgrp`

* **Where**               : 

    * `$upsrgrp(0-511): UPSR group address`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`plaoutupsrenctr`| Total is 8092 upsrID, divide into 512 group address for configuration, 16-bit is correlative with 16 upsrID each group. Bit#0 is for upsrID#0, bit#1 is for upsrID#1... 1: enable 0: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Output HSPW Control

* **Description**           

This register is used to config HSPW group


* **RTL Instant Name**    : `pla_out_hspw_ctrl`

* **Address**             : `0x9_8000-0xC_8FFF`

* **Formula**             : `0x9_8000 + $hspwid`

* **Where**               : 

    * `$hspwid(0-4095): HSPW group`

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`plaouthspwpsnctr`| PSN location for HSPW group| `RW`| `0x0`| `0x0`|
|`[0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Output PSN Process Control

* **Description**           

This register is used to control PSN configuration


* **RTL Instant Name**    : `pla_out_psnpro_ctrl`

* **Address**             : `0x8_4000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`plaoutpsnproreqctr`| Request to process PSN 1: request to write/read PSN header buffer. The CPU need to prepare a complete PSN header into PSN buffer before request write OR read out a complete PSN header in PSN buffer after request read done 0: HW will automatically set to 0 when a request done| `RW`| `0x0`| `0x0`|
|`[30]`|`plaoutpsnprornwctr`| read or write PSN 1: read PSN 0: write PSN| `RW`| `0x0`| `0x0`|
|`[29:23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22:16]`|`plaoutpsnprolenctr`| Length of a complete PSN need to read/write| `RW`| `0x0`| `0x0`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`plaoutpsnpropagectr`| there is 2 pages PSN location per PWID, depend on the configuration of "PlaOutHspwPsnCtr", the engine will choice which the page to encapsulate into the packet.| `RW`| `0x0`| `0x0`|
|`[12:0]`|`plaoutpsnpropwidctr`| Pseudowire channel| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Output PSN Buffer Control

* **Description**           

This register is used to store PSN data which is before CPU request write or after CPU request read %%

The header format from entry#0 to entry#4 is as follow: %%

{DA(6-byte),SA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 96 bytes)} %%

Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%



PSN header is MEF-8: %%

EthType:  0x88D8 %%

4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for 	remote 	receive side. %%

PSN header is MPLS:  %%

EthType:  0x8847 %%

4/8/12-byte PSN Header with format: {OuterLabel2[31:0](optional), OuterLabel1[31:0](optional),  InnerLabel[31:0]}   %%

Where InnerLabel[31:12] is pseodowire identification for remote receive side. %%

Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit =  for InnerLabel %%

PSN header is UDP/Ipv4:  %%

EthType:  0x0800 %%

28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %%

{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %%

{IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %%

{IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %%

{IP_SrcAdr[31:0]} %%

{IP_DesAdr[31:0]} %%

{UDP_SrcPort[15:0], UDP_DesPort[15:0]} %%

{UDP_Sum[31:0] (optional)} %%

Case: %%

IP_Protocol[7:0]: 0x11 to signify UDP  %%

IP_Protocol[7:0]: 0x11 to signify UDP  %%

IP_Header_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields %%

{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +%%

IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +%%

{IP_TTL[7:0], IP_Protocol[7:0]} + %%

IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +%%

IP_DesAdr[31:16] + IP_DesAdr[15:0]%%

UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%

UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%

UDP_Sum[31:0] (optional): CPU calculate these fields as a temporarily for HW before plus rest fields%%

IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%%

IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%%

IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%%

IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%%

IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%%

IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%%

IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%%

IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%

{8-bit zeros, IP_Protocol[7:0]} +%%

UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%

PSN header is UDP/Ipv6:  %%

EthType:  0x86DD%%

48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:%%

{IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}%%

{16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]} %%

{IP_SrcAdr[127:96]}%%

{IP_SrcAdr[95:64]}%%

{IP_SrcAdr[63:32]}%%

{IP_SrcAdr[31:0]}%%

{IP_DesAdr[127:96]}%%

{IP_DesAdr[95:64]}%%

{IP_DesAdr[63:32]}%%

{IP_DesAdr[31:0]}%%

{UDP_SrcPort[15:0], UDP_DesPort[15:0]}%%

{UDP_Sum[31:0]}%%

Case:%%

IP_Next_Header[7:0]: 0x11 to signify UDP %%

UDP_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields%%

IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%%

IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%%

IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%%

IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%%

IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%%

IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%%

IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%%

IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%

{8-bit zeros, IP_Next_Header[7:0]} +%%

UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%

UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%

UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%

User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field. %%

PSN header is MPLS over Ipv4:%%

IPv4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS%%

After IPv4 header is MPLS labels where inner label is used for PW identification%%

PSN header is MPLS over Ipv6:%%

IPv6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS%%

After IPv6 header is MPLS labels where inner label is used for PW identification%%

PSN header (all modes) with RTP enable: RTP used for TDM PW (define in RFC3550), is in the first 8-byte of this buffer (bit[127:64] of segid == 0), following is PSN header (start from bit[63:0] of segid = 0)%%

Case:%%

RTPSSRC[31:0] : This is the SSRC value of RTP header%%

RtpPtValue[6:0]: This is the PT value of RTP header, define in http://www.iana.org/assignments/rtp-parameters/rtp-parameters.xml


* **RTL Instant Name**    : `pla_out_psnbuf_ctrl`

* **Address**             : `0x8_4010-0x8_4015`

* **Formula**             : `0x8_4010 + $segid`

* **Where**               : 

    * `$segid(0-5): segment 16-byte PSN, total is 6 segments for maximum 96 bytes PSN  %`

* **Width**               : `128`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:0]`|`plaoutpsnproreqctr`| PSN buffer| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Hold Register Control

* **Description**           

This register is used to control hold register.


* **RTL Instant Name**    : `pla_hold_reg_ctrl`

* **Address**             : `0x8_C000-0x8_C002`

* **Formula**             : `0x8_C000 + $holdreg`

* **Where**               : 

    * `$holdreg(0-2): hold register with (holdreg=0) for bit63_32, (holdreg=1) for bit95_64, (holdreg=2) for bit127_96`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`plaholdregctr`| hold register value| `RW`| `0x0`| `0x0 End: Begin:`|

###Read HA Address Bit3_0 Control

* **Description**           

This register is used to send HA read address bit3_0 to HA engine


* **RTL Instant Name**    : `rdha3_0_control`

* **Address**             : `0x8C100 - 0x8C10F`

* **Formula**             : `0x8C100 + $HaAddr3_0`

* **Where**               : 

    * `$HaAddr3_0(0-F): HA Address Bit3_0`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr3_0`| Read value will be 0x8C100 plus HaAddr3_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit7_4 Control

* **Description**           

This register is used to send HA read address bit7_4 to HA engine


* **RTL Instant Name**    : `rdha7_4_control`

* **Address**             : `0x8C110 - 0x8C11F`

* **Formula**             : `0x8C110 + $HaAddr7_4`

* **Where**               : 

    * `$HaAddr7_4(0-F): HA Address Bit7_4`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr7_4`| Read value will be 0x8C100 plus HaAddr7_4| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit11_8 Control

* **Description**           

This register is used to send HA read address bit11_8 to HA engine


* **RTL Instant Name**    : `rdha11_8_control`

* **Address**             : `0x8C120 - 0x8C12F`

* **Formula**             : `0x8C120 + $HaAddr11_8`

* **Where**               : 

    * `$HaAddr11_8(0-F): HA Address Bit11_8`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr11_8`| Read value will be 0x8C100 plus HaAddr11_8| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit15_12 Control

* **Description**           

This register is used to send HA read address bit15_12 to HA engine


* **RTL Instant Name**    : `rdha15_12_control`

* **Address**             : `0x08C130 - 0x8C13F`

* **Formula**             : `0x08C130 + $HaAddr15_12`

* **Where**               : 

    * `$HaAddr15_12(0-F): HA Address Bit15_12`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr15_12`| Read value will be 0x8C100 plus HaAddr15_12| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit19_16 Control

* **Description**           

This register is used to send HA read address bit19_16 to HA engine


* **RTL Instant Name**    : `rdha19_16_control`

* **Address**             : `0x8C140 - 0x8C14F`

* **Formula**             : `0x8C140 + $HaAddr19_16`

* **Where**               : 

    * `$HaAddr19_16(0-F): HA Address Bit19_16`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr19_16`| Read value will be 0x8C100 plus HaAddr19_16| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit23_20 Control

* **Description**           

This register is used to send HA read address bit23_20 to HA engine


* **RTL Instant Name**    : `rdha23_20_control`

* **Address**             : `0x8C150 - 0x8C15F`

* **Formula**             : `0x8C150 + $HaAddr23_20`

* **Where**               : 

    * `$HaAddr23_20(0-F): HA Address Bit23_20`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr23_20`| Read value will be 0x8C100 plus HaAddr23_20| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit24 and Data Control

* **Description**           

This register is used to send HA read address bit24 to HA engine to read data


* **RTL Instant Name**    : `rdha24data_control`

* **Address**             : `0x8C160 - 0x8C161`

* **Formula**             : `0x8C160 + $HaAddr24`

* **Where**               : 

    * `$HaAddr24(0-1): HA Address Bit24`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata31_0`| HA read data bit31_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data63_32

* **Description**           

This register is used to read HA dword2 of data.


* **RTL Instant Name**    : `rdha_hold63_32`

* **Address**             : `0x8C170`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata63_32`| HA read data bit63_32| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data95_64

* **Description**           

This register is used to read HA dword3 of data.


* **RTL Instant Name**    : `rdindr_hold95_64`

* **Address**             : `0x8C171`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata95_64`| HA read data bit95_64| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data127_96

* **Description**           

This register is used to read HA dword4 of data.


* **RTL Instant Name**    : `rdindr_hold127_96`

* **Address**             : `0x8C172`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata127_96`| HA read data bit127_96| `RW`| `0xx`| `0xx End: Begin:`|

###Payload Assembler Output PSN Process Control

* **Description**           

This register is used to control PSN configuration


* **RTL Instant Name**    : `pla_out_psnpro_ha_ctrl`

* **Address**             : `0x8_4001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`plaoutpsnproreqctr`| Request to process PSN 1: request to write/read PSN header buffer. The CPU need to prepare a complete PSN header into PSN buffer before request write OR read out a complete PSN header in PSN buffer after request read done 0: HW will automatically set to 0 when a request done| `RW`| `0x0`| `0x0`|
|`[30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29:23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22:16]`|`plaoutpsnprolenctr`| Length of a complete PSN need to read/write| `RW`| `0x0`| `0x0`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`plaoutpsnpropagectr`| there is 2 pages PSN location per PWID, depend on the configuration of "PlaOutHspwPsnCtr", the engine will choice which the page to encapsulate into the packet.| `RW`| `0x0`| `0x0`|
|`[12:0]`|`plaoutpsnpropwidctr`| Pseudowire channel| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Force Parity Error Control0

* **Description**           

This register is used to force parity error


* **RTL Instant Name**    : `pla_force_par_err_control0`

* **Address**             : `0x88040`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[30]`|`forceparerrlopwectrregoc96_1`| Force Parity Error HO PWE Control Register of OC96#1| `RW`| `0x0`| `0x0`|
|`[29]`|`forceparerrlopldctrregoc96_1_oc48_1`| Force Parity Error HO Payload Control Register of OC96#1 OC#48#1| `RW`| `0x0`| `0x0`|
|`[28]`|`forceparerrlopldctrregoc96_1_oc48_0`| Force Parity Error HO Payload Control Register of OC96#1 OC#48#0| `RW`| `0x0`| `0x0`|
|`[27]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[26]`|`forceparerrlopwectrregoc96_0`| Force Parity Error HO PWE Control Register of OC96#0| `RW`| `0x0`| `0x0`|
|`[25]`|`forceparerrlopldctrregoc96_0_oc48_1`| Force Parity Error HO Payload Control Register of OC96#0 OC#48#1| `RW`| `0x0`| `0x0`|
|`[24]`|`forceparerrlopldctrregoc96_0_oc48_0`| Force Parity Error HO Payload Control Register of OC96#0 OC#48#0| `RW`| `0x0`| `0x0`|
|`[23:15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`forceparerrlopwectrregoc48_3`| Force Parity Error LO PWE Control Register of OC48#3| `RW`| `0x0`| `0x0`|
|`[13]`|`forceparerrlopldctrregoc48_3_oc24_1`| Force Parity Error LO Payload Control Register of OC48#3 OC#24#1| `RW`| `0x0`| `0x0`|
|`[12]`|`forceparerrlopldctrregoc48_3_oc24_0`| Force Parity Error LO Payload Control Register of OC48#3 OC#24#0| `RW`| `0x0`| `0x0`|
|`[11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10]`|`forceparerrlopwectrregoc48_2`| Force Parity Error LO PWE Control Register of OC48#2| `RW`| `0x0`| `0x0`|
|`[9]`|`forceparerrlopldctrregoc48_2_oc24_1`| Force Parity Error LO Payload Control Register of OC48#2 OC#24#1| `RW`| `0x0`| `0x0`|
|`[8]`|`forceparerrlopldctrregoc48_2_oc24_0`| Force Parity Error LO Payload Control Register of OC48#2 OC#24#0| `RW`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`forceparerrlopwectrregoc48_1`| Force Parity Error LO PWE Control Register of OC48#1| `RW`| `0x0`| `0x0`|
|`[5]`|`forceparerrlopldctrregoc48_1_oc24_1`| Force Parity Error LO Payload Control Register of OC48#1 OC#24#1| `RW`| `0x0`| `0x0`|
|`[4]`|`forceparerrlopldctrregoc48_1_oc24_0`| Force Parity Error LO Payload Control Register of OC48#1 OC#24#0| `RW`| `0x0`| `0x0`|
|`[3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`forceparerrlopwectrregoc48_0`| Force Parity Error LO PWE Control Register of OC48#0| `RW`| `0x0`| `0x0`|
|`[1]`|`forceparerrlopldctrregoc48_0_oc24_1`| Force Parity Error LO Payload Control Register of OC48#0 OC#24#1| `RW`| `0x0`| `0x0`|
|`[0]`|`forceparerrlopldctrregoc48_0_oc24_0`| Force Parity Error LO Payload Control Register of OC48#0 OC#24#0| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Disable Parity Check Control0

* **Description**           

This register is used to disable parity check


* **RTL Instant Name**    : `pla_dis_par_control0`

* **Address**             : `0x88041`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[30]`|`disparlopwectrregoc96_1`| Disable Parity HO PWE Control Register of OC96#1| `RW`| `0x0`| `0x0`|
|`[29]`|`disparlopldctrregoc96_1_oc48_1`| Disable Parity HO Payload Control Register of OC96#1 OC#48#1| `RW`| `0x0`| `0x0`|
|`[28]`|`disparlopldctrregoc96_1_oc48_0`| Disable Parity Error HO Payload Control Register of OC96#1 OC#48#0| `RW`| `0x0`| `0x0`|
|`[27]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[26]`|`disparlopwectrregoc96_0`| Disable Parity Error HO PWE Control Register of OC96#0| `RW`| `0x0`| `0x0`|
|`[25]`|`disparlopldctrregoc96_0_oc48_1`| Disable Parity Error HO Payload Control Register of OC96#0 OC#48#1| `RW`| `0x0`| `0x0`|
|`[24]`|`disparlopldctrregoc96_0_oc48_0`| Disable Parity Error HO Payload Control Register of OC96#0 OC#48#0| `RW`| `0x0`| `0x0`|
|`[23:15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`disparlopwectrregoc48_3`| Disable Parity Error LO PWE Control Register of OC48#3| `RW`| `0x0`| `0x0`|
|`[13]`|`disparlopldctrregoc48_3_oc24_1`| Disable Parity Error LO Payload Control Register of OC48#3 OC#24#1| `RW`| `0x0`| `0x0`|
|`[12]`|`disparlopldctrregoc48_3_oc24_0`| Disable Parity Error LO Payload Control Register of OC48#3 OC#24#0| `RW`| `0x0`| `0x0`|
|`[11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10]`|`disparlopwectrregoc48_2`| Disable Parity Error LO PWE Control Register of OC48#2| `RW`| `0x0`| `0x0`|
|`[9]`|`disparlopldctrregoc48_2_oc24_1`| Disable Parity Error LO Payload Control Register of OC48#2 OC#24#1| `RW`| `0x0`| `0x0`|
|`[8]`|`disparlopldctrregoc48_2_oc24_0`| Disable Parity Error LO Payload Control Register of OC48#2 OC#24#0| `RW`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`disparlopwectrregoc48_1`| Disable Parity Error LO PWE Control Register of OC48#1| `RW`| `0x0`| `0x0`|
|`[5]`|`disparlopldctrregoc48_1_oc24_1`| Disable Parity Error LO Payload Control Register of OC48#1 OC#24#1| `RW`| `0x0`| `0x0`|
|`[4]`|`disparlopldctrregoc48_1_oc24_0`| Disable Parity Error LO Payload Control Register of OC48#1 OC#24#0| `RW`| `0x0`| `0x0`|
|`[3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`disparlopwectrregoc48_0`| Disable Parity Error LO PWE Control Register of OC48#0| `RW`| `0x0`| `0x0`|
|`[1]`|`disparlopldctrregoc48_0_oc24_1`| Disable Parity Error LO Payload Control Register of OC48#0 OC#24#1| `RW`| `0x0`| `0x0`|
|`[0]`|`disparlopldctrregoc48_0_oc24_0`| Disable Parity Error LO Payload Control Register of OC48#0 OC#24#0| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Parity Error Sticky0

* **Description**           

This register is used to sticky parity check


* **RTL Instant Name**    : `pla_par_stk0`

* **Address**             : `0x88042`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[30]`|`parlopwectrregoc96_1stk`| Sticky Parity HO PWE Control Register of OC96#1| `RW`| `0x0`| `0x0`|
|`[29]`|`parlopldctrregoc96_1_oc48_1stk`| Sticky Parity HO Payload Control Register of OC96#1 OC#48#1| `RW`| `0x0`| `0x0`|
|`[28]`|`parlopldctrregoc96_1_oc48_0stk`| Sticky Parity Error HO Payload Control Register of OC96#1 OC#48#0| `RW`| `0x0`| `0x0`|
|`[27]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[26]`|`parlopwectrregoc96_0stk`| Sticky Parity Error HO PWE Control Register of OC96#0| `RW`| `0x0`| `0x0`|
|`[25]`|`parlopldctrregoc96_0_oc48_1stk`| Sticky Parity Error HO Payload Control Register of OC96#0 OC#48#1| `RW`| `0x0`| `0x0`|
|`[24]`|`parlopldctrregoc96_0_oc48_0stk`| Sticky Parity Error HO Payload Control Register of OC96#0 OC#48#0| `RW`| `0x0`| `0x0`|
|`[23:15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`parlopwectrregoc48_3stk`| Sticky Parity Error LO PWE Control Register of OC48#3| `RW`| `0x0`| `0x0`|
|`[13]`|`parlopldctrregoc48_3_oc24_1stk`| Sticky Parity Error LO Payload Control Register of OC48#3 OC#24#1| `RW`| `0x0`| `0x0`|
|`[12]`|`parlopldctrregoc48_3_oc24_0stk`| Sticky Parity Error LO Payload Control Register of OC48#3 OC#24#0| `RW`| `0x0`| `0x0`|
|`[11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10]`|`parlopwectrregoc48_2stk`| Sticky Parity Error LO PWE Control Register of OC48#2| `RW`| `0x0`| `0x0`|
|`[9]`|`parlopldctrregoc48_2_oc24_1stk`| Sticky Parity Error LO Payload Control Register of OC48#2 OC#24#1| `RW`| `0x0`| `0x0`|
|`[8]`|`parlopldctrregoc48_2_oc24_0stk`| Sticky Parity Error LO Payload Control Register of OC48#2 OC#24#0| `RW`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`parlopwectrregoc48_1stk`| Sticky Parity Error LO PWE Control Register of OC48#1| `RW`| `0x0`| `0x0`|
|`[5]`|`parlopldctrregoc48_1_oc24_1stk`| Sticky Parity Error LO Payload Control Register of OC48#1 OC#24#1| `RW`| `0x0`| `0x0`|
|`[4]`|`parlopldctrregoc48_1_oc24_0stk`| Sticky Parity Error LO Payload Control Register of OC48#1 OC#24#0| `RW`| `0x0`| `0x0`|
|`[3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`parlopwectrregoc48_0stk`| Sticky Parity Error LO PWE Control Register of OC48#0| `RW`| `0x0`| `0x0`|
|`[1]`|`parlopldctrregoc48_0_oc24_1stk`| Sticky Parity Error LO Payload Control Register of OC48#0 OC#24#1| `RW`| `0x0`| `0x0`|
|`[0]`|`parlopldctrregoc48_0_oc24_0stk`| Sticky Parity Error LO Payload Control Register of OC48#0 OC#24#0| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Force Parity Error Control1

* **Description**           

This register is used to force parity error


* **RTL Instant Name**    : `pla_force_par_err_control1`

* **Address**             : `0x88044`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22]`|`forceparerrhspwp2ctrreg`| Force Parity Error HSPW Port2 Control Register| `RW`| `0x0`| `0x0`|
|`[21]`|`forceparerrupsrp2ctrreg`| Force Parity Error UPSR Port2 Control Register| `RW`| `0x0`| `0x0`|
|`[20]`|`forceparerroutpwep2ctrreg`| Force Parity Error Output PWE Port2 Control Register| `RW`| `0x0`| `0x0`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`forceparerrhspwp1ctrreg`| Force Parity Error HSPW Port1 Control Register| `RW`| `0x0`| `0x0`|
|`[17]`|`forceparerrupsrp1ctrreg`| Force Parity Error UPSR Port1 Control Register| `RW`| `0x0`| `0x0`|
|`[16]`|`forceparerroutpwep1ctrreg`| Force Parity Error Output PWE Port1 Control Register| `RW`| `0x0`| `0x0`|
|`[15:0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Disable Parity Check Control1

* **Description**           

This register is used to disable parity check


* **RTL Instant Name**    : `pla_dis_par_control1`

* **Address**             : `0x88045`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22]`|`disparhspwp2ctrreg`| Disable Parity HSPW Port2 Control Register| `RW`| `0x0`| `0x0`|
|`[21]`|`disparupsrp2ctrreg`| Disable Parity UPSR Port2 Control Register| `RW`| `0x0`| `0x0`|
|`[20]`|`disparoutpwep2ctrreg`| Disable Parity Output PWE Port2 Control Register| `RW`| `0x0`| `0x0`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`disparhspwp1ctrreg`| Disable Parity HSPW Port1 Control Register| `RW`| `0x0`| `0x0`|
|`[17]`|`disparupsrp1ctrreg`| Disable Parity UPSR Port1 Control Register| `RW`| `0x0`| `0x0`|
|`[16]`|`disparoutpwep1ctrreg`| Disable Parity Output PWE Port1 Control Register| `RW`| `0x0`| `0x0`|
|`[15:0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Parity Error Sticky1

* **Description**           

This register is used to sticky parity check


* **RTL Instant Name**    : `pla_par_err_stk1`

* **Address**             : `0x88046`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22]`|`parhspwp2ctrregstk`| Sticky Parity HSPW Port2 Control Register| `RW`| `0x0`| `0x0`|
|`[21]`|`parupsrp2ctrregstk`| Sticky Parity UPSR Port2 Control Register| `RW`| `0x0`| `0x0`|
|`[20]`|`paroutpwep2ctrregstk`| Sticky Parity Output PWE Port2 Control Register| `RW`| `0x0`| `0x0`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`parhspwp1ctrregstk`| Sticky Parity HSPW Port1 Control Register| `RW`| `0x0`| `0x0`|
|`[17]`|`parupsrp1ctrregstk`| Sticky Parity UPSR Port1 Control Register| `RW`| `0x0`| `0x0`|
|`[16]`|`paroutpwep1ctrregstk`| Sticky Parity Output PWE Port1 Control Register| `RW`| `0x0`| `0x0`|
|`[15:0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Force CRC Error Control

* **Description**           

This register is used to force CRC error


* **RTL Instant Name**    : `pla_force_crc_err_control`

* **Address**             : `0x88048`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`forcecrcerrctr`| Force CRC Error Control| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Disable CRC Check Control

* **Description**           

This register is used to disable CRC check


* **RTL Instant Name**    : `pla_dis_crc_control`

* **Address**             : `0x88049`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`discrcchkctr`| Disable CRC check Control| `RW`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler CRC Error Sticky

* **Description**           

This register is used to check CRC error sticky


* **RTL Instant Name**    : `pla_crc_err_stk`

* **Address**             : `0x8804A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`crccheckstk`| Sticky CRC check Port1| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PLA FSM Control

* **Description**           

This register is used to control FSM TX PW


* **RTL Instant Name**    : `pwe_pla_fsm_ctr`

* **Address**             : `0x88020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`forcefsm`| Force FSM state for selftest| `RW`| `0x0`| `0x0`|
|`[0]`|`fsmpinen`| Enable FSM from pin outside| `RW`| `0x0`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CNC0021_RD_MACDUMP
####Register Table

|Name|Address|
|-----|-----|
|`Mac 40G dump direction control`|`0x0000804 #The address format for these registers is 0x00010000 + PWID`|
|`Mac 40G dump data status`|`0x000000 - 0x0003FF #The address format for these registers is 0x000000 + entry`|
|`Mac 40G dump Hold Register Status`|`0x000800 - 0x000001`|


###Mac 40G dump direction control

* **Description**           

This register configures direction to dump packet

#HDL_PATH: rtljitbuf.ramjitbufcfg.ram.ram[$PWID]


* **RTL Instant Name**    : `ramdumtypecfg`

* **Address**             : `0x0000804 #The address format for these registers is 0x00010000 + PWID`

* **Formula**             : `0x0000804 #The address format for these registers is 0x00010000 + PWID +  PWID`

* **Where**               : 

    * `$PWID(0-10751): Pseudowire ID`

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`directiondump`| Tx or Rx MAC direction 1: Dump Tx MAC40G 0: Dump Rx MAC40G| `RW`| `0x0`| `0x0 End: Begin:`|

###Mac 40G dump data status

* **Description**           

This register shows data value to be dump, first entry always contain SOP


* **RTL Instant Name**    : `ramdumpdata`

* **Address**             : `0x000000 - 0x0003FF #The address format for these registers is 0x000000 + entry`

* **Formula**             : `0x000000 +  entry`

* **Where**               : 

    * `$entry(0-1023): Data entry`

* **Width**               : `69`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[68]`|`dumpsop`| Start of packet indication| `RO`| `0x0`| `0x0`|
|`[67]`|`dumpeop`| End of packet indication| `RO`| `0x0`| `0x0`|
|`[66:64]`|`dumpnob`| Number of byte indication 0 means 1 byte| `RW`| `0x0`| `0x0`|
|`[63:0]`|`dumpdata`| Data value Bit63_56 is MSB| `RW`| `0x0`| `0x0 End: Begin:`|

###Mac 40G dump Hold Register Status

* **Description**           

This register using for hold remain that more than 32bits


* **RTL Instant Name**    : `dump_hold_status`

* **Address**             : `0x000800 - 0x000001`

* **Formula**             : `0x000800 +  HID`

* **Where**               : 

    * `$HID(0-1): Hold ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dumpholdstatus`| Hold 32bits| `RW`| `0x0`| `0x0 End:`|

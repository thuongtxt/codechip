## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2016-10-04|AF6Project|Initial version|




##AF6CNC0021_RD_EPA_FLOWCTRL
####Register Table

|Name|Address|
|-----|-----|
|`CONFIG THRESHOLD`|`0x00-0x0F`|
|`CONFIG ENABLE FLOW CONTROL`|`0x10`|


###CONFIG THRESHOLD

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_cfgthreshold`

* **Address**             : `0x00-0x0F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:08]`|`val_thres2`| threshold for 3/4 of max buffer contain (256*3/4)kbytes| `R/W`| `0x0`| `0x0`|
|`[07:00]`|`val_thes1`| threshold for 1/4 of max buffer contain (256/4)kbytes| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE FLOW CONTROL

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_enbflwctrl`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `0`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00:00]`|`out_enbflw`| (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0 End:`|

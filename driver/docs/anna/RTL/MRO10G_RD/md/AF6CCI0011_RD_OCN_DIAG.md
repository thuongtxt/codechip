## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_OCN_DIAG
####Register Table

|Name|Address|
|-----|-----|
|`OCN Global Rx Framer Control`|`0xf43000`|
|`OCN Global Tx Framer Control`|`0xf43001`|
|`OCN Rx Framer Status`|`0xf43100 - 0xf43170`|
|`OCN Rx Framer Sticky`|`0xf43101 - 0xf43171`|
|`OCN Rx Framer B1 error counter read only`|`0xf43102 - 0xf43172`|
|`OCN Rx Framer B1 error counter read to clear`|`0xf43103 - 0xf43173`|


###OCN Global Rx Framer Control

* **Description**           

This is the global configuration register for the Rx Framer


* **RTL Instant Name**    : `glbrfm_reg`

* **Address**             : `0xf43000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:16]`|`rxlinesyncsel`| Select line id for synchronization 8 rx lines. Bit[0] for line 0. Note that must only 1 bit is set| `RW`| `0x1`| `0x1`|
|`[15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`rxfrmlosaisen`| Enable/disable forwarding P_AIS when LOS detected at Rx Framer. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[13]`|`rxfrmoofaisen`| Enable/disable forwarding P_AIS when OOF detected at Rx Framer. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[12]`|`rxfrmb1blockcntmod`| B1 Counter Mode. 1: Block mode 0: Bit-wise mode| `RW`| `0x1`| `0x1`|
|`[11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10:8]`|`rxfrmbadfrmthresh`| Threshold for A1A2 missing counter, that is used to change state from FRAMED to HUNT.| `RW`| `0x3`| `0x3`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6:4]`|`rxfrmb1goodthresh`| Threshold for B1 good counter, that is used to change state from CHECK to FRAMED.| `RW`| `0x4`| `0x4`|
|`[3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`rxfrmdescren`| Enable/disable de-scrambling of the Rx coming data stream. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[1]`|`rxfrmb1chkfrmen`| Enable/disable B1 check option is added to the required framing algorithm. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[0]`|`rxfrmessimodeen`| TFI-5 mode. 1: Enable 0: Disable| `RW`| `0x1`| `0x1 End : Begin:`|

###OCN Global Tx Framer Control

* **Description**           

This is the global configuration register for the Tx Framer


* **RTL Instant Name**    : `glbtfm_reg`

* **Address**             : `0xf43001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[31:24]`|`txlineooffrc`| Enable/disble force OOF for 8 tx lines. Bit[0] for line 0.| `RW`| `0x0`| `0x0`|
|`[23:16]`|`txlineb1errfrc`| Enable/disble force B1 error for 8 tx lines. Bit[0] for line 0.| `RW`| `0x0`| `0x0`|
|`[15:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:4]`|`txlinesyncsel`| Select line id for synchronization 8 tx lines. Bit[0] for line 0. Note that must only 1 bit is set| `RW`| `0x1`| `0x1`|
|`[3:1]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`txfrmscren`| Enable/disable scrambling of the Tx data stream. 1: Enable 0: Disable| `RW`| `0x1`| `0x1 End : Begin:`|

###OCN Rx Framer Status

* **Description**           

Rx Framer status


* **RTL Instant Name**    : `rxfrmsta`

* **Address**             : `0xf43100 - 0xf43170`

* **Formula**             : `0xf43100 + 16*LineId`

* **Where**               : 

    * `$LineId(0-7)`

* **Width**               : `128`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:1]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[0]`|`staoof`| Out of Frame that is detected at RxFramer| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer Sticky

* **Description**           

Rx Framer sticky


* **RTL Instant Name**    : `rxfrmstk`

* **Address**             : `0xf43101 - 0xf43171`

* **Formula**             : `0xf43101 + 16*LineId`

* **Where**               : 

    * `$LineId(0-7)`

* **Width**               : `128`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:1]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[0]`|`stkoof`| Out of Frame sticky change| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer B1 error counter read only

* **Description**           

Rx Framer B1 error counter read only


* **RTL Instant Name**    : `rxfrmb1cntro`

* **Address**             : `0xf43102 - 0xf43172`

* **Formula**             : `0xf43102 + 16*LineId`

* **Where**               : 

    * `$LineId(0-7)`

* **Width**               : `128`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:32]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[31:0]`|`rxfrmb1errro`| Number of B1 error - read only| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer B1 error counter read to clear

* **Description**           

Rx Framer B1 error counter read to clear


* **RTL Instant Name**    : `rxfrmb1cntr2c`

* **Address**             : `0xf43103 - 0xf43173`

* **Formula**             : `0xf43103 + 16*LineId`

* **Where**               : 

    * `$LineId(0-7)`

* **Width**               : `128`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:32]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[31:0]`|`rxfrmb1errr2c`| Number of B1 error - read to clear| `RC`| `0x0`| `0x0 End :`|

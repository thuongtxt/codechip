## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0012_RD_PTP
####Register Table

|Name|Address|
|-----|-----|
|`Hold Register 1`|`0x00_0000`|
|`Hold Register 2`|`0x00_0001`|
|`Hold Register 3`|`0x00_0002`|
|`PTP enable`|`0x00_0003`|
|`PTP enable`|`0x00_0004`|
|`PTP enable`|`0x00_0005`|
|`PTP Device`|`0x00_0006`|
|`Config Master SPID`|`0x00_0007`|
|`Config Slaver ReqPID`|`0x00_0008`|
|`Config chsv4 bypass`|`0x00_0009`|
|`config uni MAC global ingress`|`0x00_000A`|
|`config mul MAC global 1 ingress`|`0x00_000B`|
|`config mul MAC global 2 ingress`|`0x00_000C`|
|`config uni MAC matching enable`|`0x00_000D`|
|`config multi MAC matching enable`|`0x00_000E`|
|`config any MAC enable`|`0x00_000F`|
|`Config mul ipv4 global 1`|`0x00_0010`|
|`Config mul ipv4 global 2`|`0x00_0011`|
|`Config mul ipv6 global 1`|`0x00_0012`|
|`Config mul ipv6 global 2`|`0x00_0013`|
|`config any IP enable`|`0x00_0014`|
|`PTP Ready`|`0x00_0015`|
|`cfg protocol`|`0x00_0060-0x00_006F`|
|`cfg tpid`|`0x00_0070-0x00_007F`|
|`cfg uni/mul mac`|`0x00_0080-0x00_009F`|
|`cfg uni ip 1`|`0x00_00A0-0x00_00AF`|
|`cfg uni ip 2`|`0x00_00B0-0x00_00BF`|
|`cfg uni ip 3`|`0x00_00C0-0x00_00CF`|
|`cfg uni ip 4`|`0x00_00D0-0x00_00DF`|
|`cfg mul ip`|`0x00_00E0-0x00_00EF`|
|`cfg ip matching enable`|`0x00_00F0-0x00_00FF`|
|`Counter PTP Classify Err`|`0x00_0200-0x00_020F`|
|`Counter PTP Packet`|`0x00_0210-0x00_021F`|
|`Counter Sync Received`|`0x00_0240-0x00_024F`|
|`Counter Folu Received`|`0x00_0250-0x00_025F`|
|`Counter dreq Received`|`0x00_0260-0x00_026F`|
|`Counter dres Received`|`0x00_0270-0x00_027F`|
|`Counter Rx Sync Extract Err`|`0x00_0280-0x00_028F`|
|`Counter Rx Folu Extract Err`|`0x00_0290-0x00_029F`|
|`Counter Rx Dreq Extract Err`|`0x00_02A0-0x00_02AF`|
|`Counter Rx Dres Extract Err`|`0x00_02B0-0x00_02BF`|
|`Counter Rx Sync Chsum Err`|`0x00_02C0-0x00_02CF`|
|`Counter Rx Folu Chsum Err`|`0x00_02D0-0x00_02DF`|
|`Counter Rx Dreq Chsum Err`|`0x00_02E0-0x00_02EF`|
|`Counter Rx Dres Chsum Err`|`0x00_02F0-0x00_02FF`|
|`Counter Sync Transmit`|`0x00_0300-0x00_030F`|
|`Counter Folu Transmit`|`0x00_0310-0x00_031F`|
|`Counter dreq transmit`|`0x00_0320-0x00_032F`|
|`Counter dres transmit`|`0x00_0330-0x00_033F`|
|`Counter sop eop`|`0x00_0220-0x00_022F`|
|`classify sticky`|`0x00_0230`|
|`PTP check sticky`|`0x00_0231`|
|`ptp getinfo sticky`|`0x00_0232`|
|`trans sticky`|`0x00_0233`|
|``|`0x00_3000`|
|``|`0x00_3001`|
|``|`0x00_3002`|
|``|`0x00_3003`|
|``|`0x00_3004`|
|``|`0x00_3005`|
|``|`0x00_3006`|
|``|`0x00_3007`|
|``|`0x00_3008`|
|``|`0x00_3009`|
|``|`0x00_300A`|
|``|`0x00_300B`|
|``|`0x00_300C`|
|``|`0x00_300D`|
|``|`0x00_300E`|
|``|`0x00_300F`|
|``|`0x00_3010`|
|``|`0x00_3011`|
|``|`0x00_3012`|
|`cpu queue sticky`|`0x00_0234`|
|`Config t1t3mode`|`0x00_0017`|
|`Config cpu flush`|`0x00_0018`|
|`Read CPU Queue`|`0x00_0016`|


###Hold Register 1

* **Description**           

This register hold value 1


* **RTL Instant Name**    : `upen_hold1`

* **Address**             : `0x00_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cfg_hold1`| hold value 1| `RW`| `0x0`| `0x0 End: Begin:`|

###Hold Register 2

* **Description**           

This register hold value 2


* **RTL Instant Name**    : `upen_hold2`

* **Address**             : `0x00_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cfg_hold2`| hold value 2| `RW`| `0x0`| `0x0 End: Begin:`|

###Hold Register 3

* **Description**           

This register hold value


* **RTL Instant Name**    : `upen_hold3`

* **Address**             : `0x00_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cfg_hold3`| hold value 3| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP enable

* **Description**           

used to enable ptp for 16 port ingress


* **RTL Instant Name**    : `upen_ptp_en`

* **Address**             : `0x00_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_ptp_en`| '1' : enable, '0': disable| `RW`| `0xFFFF`| `0xFFFF End: Begin:`|

###PTP enable

* **Description**           

used to indicate 1-step/2-step mode for each port


* **RTL Instant Name**    : `upen_ptp_stmode`

* **Address**             : `0x00_0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_ptp_stmode`| '1' : 2-step, '0': 1-step| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP enable

* **Description**           

used to indicate master/slaver mode for each port


* **RTL Instant Name**    : `upen_ptp_ms`

* **Address**             : `0x00_0005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_ptp_ms`| '1' : master, '0': slaver| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP Device

* **Description**           

This register is used to config PTP device


* **RTL Instant Name**    : `upen_ptp_dev`

* **Address**             : `0x00_0006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[01:00]`|`device_mode`| '00': BC mode, '01': reserved '10': TC Separate mode, '11': TC General mode| `RW`| `0x0`| `0x0 End: Begin:`|

###Config Master SPID

* **Description**           

Used to checking sourcePortIdentity of Sync, Follow-up and Delay Response packet


* **RTL Instant Name**    : `upen_ms_srcpid`

* **Address**             : `0x00_0007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `96`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[95:80]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[79:00]`|`cfg_ms_srcpid`| sourcePortIdentity value of Master| `RW`| `0x0`| `0x0 End: Begin:`|

###Config Slaver ReqPID

* **Description**           

Used to checking requestingPortIdentity of Delay Response packet


* **RTL Instant Name**    : `upen_sl_reqpid`

* **Address**             : `0x00_0008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `96`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[95:80]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[79:00]`|`cfg_sl_reqpid`| RequestingPortIdentity value of Slaver| `RW`| `0x0`| `0x0 End: Begin:`|

###Config chsv4 bypass

* **Description**           

Used to indicate bypass ipv4 header checksum error


* **RTL Instant Name**    : `upen_chsv4_bypass`

* **Address**             : `0x00_0009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`cfg_chsv4_bypass`| '1': enable, '0': disable| `RW`| `0x1`| `0x1 End: Begin:`|

###config uni MAC global ingress

* **Description**           

config uni MAC address global for ingress ports


* **RTL Instant Name**    : `upen_ingunimac_glb`

* **Address**             : `0x00_000A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`cfg_ingunimac_glb`| uni MAC address global for ingress ports| `RW`| `0xFFFF_FFFF_FFFF`| `0xFFFF_FFFF_FFFF End: Begin:`|

###config mul MAC global 1 ingress

* **Description**           

config mul MAC address global 1 for ingress ports


* **RTL Instant Name**    : `upen_ingmulmac1_glb`

* **Address**             : `0x00_000B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`cfg_ingmulmac1_glb`| mul MAC address global 1 for ingress ports| `RW`| `0xFFFF_FFFF_FFFF`| `0xFFFF_FFFF_FFFF End: Begin:`|

###config mul MAC global 2 ingress

* **Description**           

config mul MAC address global 2 for ingress ports


* **RTL Instant Name**    : `upen_ingmulmac2_glb`

* **Address**             : `0x00_000C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`cfg_ingmulmac2_glb`| mul MAC address global 2 for ingress ports| `RW`| `0xFFFF_FFFF_FFFF`| `0xFFFF_FFFF_FFFF End: Begin:`|

###config uni MAC matching enable

* **Description**           

is used to indicate unicast MAC matching enable


* **RTL Instant Name**    : `upen_unimac_en`

* **Address**             : `0x00_000D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16:00]`|`cfg_unimac_en`| [16]   : en/dis uni mac matching global for all port [15:0] : en/dis uni mac matching for port 0 to port 15| `RW`| `0x1_FFFF`| `0x1_FFFF End: Begin:`|

###config multi MAC matching enable

* **Description**           

is used to indicate multicast MAC matching enable


* **RTL Instant Name**    : `upen_mulmac_en`

* **Address**             : `0x00_000E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`cfg_mulmac_en`| [17]   : en/dis mul mac matching global 2 for all port [16]   : en/dis mul mac matching global 1 for all port [15:0] : en/dis mul mac matching for port 0 to port 15| `RW`| `0x3_FFFF`| `0x3_FFFF End: Begin:`|

###config any MAC enable

* **Description**           

is used to enable any mac for each port


* **RTL Instant Name**    : `upen_anymac_en`

* **Address**             : `0x00_000F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_anymac_en`| '1': enable , '0': disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Config mul ipv4 global 1

* **Description**           

config multicast ipv4 address global 1


* **RTL Instant Name**    : `upen_v4mulip1_global`

* **Address**             : `0x00_0010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_v4mulip1_global`| used to check multicast ip| `RW`| `0x0`| `0x0 End: Begin:`|

###Config mul ipv4 global 2

* **Description**           

config multicast ipv4 address global 2


* **RTL Instant Name**    : `upen_v4mulip2_global`

* **Address**             : `0x00_0011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_v4mulip2_global`| used to check multicast ip| `RW`| `0x0`| `0x0 End: Begin:`|

###Config mul ipv6 global 1

* **Description**           

config multicast ipv6 address global 1


* **RTL Instant Name**    : `upen_v6mulip1_global`

* **Address**             : `0x00_0012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_v6mulip1_global`| used to check multicast ip| `RW`| `0x0`| `0x0 End: Begin:`|

###Config mul ipv6 global 2

* **Description**           

config multicast ipv6 address global 2


* **RTL Instant Name**    : `upen_v6mulip2_global`

* **Address**             : `0x00_0013`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_v6mulip2_global`| used to check multicast ip| `RW`| `0x0`| `0x0 End: Begin:`|

###config any IP enable

* **Description**           

is used to enable any ip for each port


* **RTL Instant Name**    : `upen_anyip_en`

* **Address**             : `0x00_0014`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_anyip_en`| '1': enable , '0': disable| `RW`| `0x0`| `0x0 End: Begin:`|

###PTP Ready

* **Description**           

This register is used to indicate config ptp done


* **RTL Instant Name**    : `upen_ptp_rdy`

* **Address**             : `0x00_0015`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`ptp_rdy`| '1' : ready| `RW`| `0x00`| `0x00 End: Begin:`|

###cfg protocol

* **Description**           

config protocol for each port at BC mode


* **RTL Instant Name**    : `upen_ptl`

* **Address**             : `0x00_0060-0x00_006F`

* **Formula**             : `0x00_0060 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[02:00]`|`cfg_ptl`| protocol value: '000': L2 '001': ipv4 '010': ipv6 '011': ipv4 vpn '100': ipv6 vpn| `RW`| `0x0`| `0x0 End: Begin:`|

###cfg tpid

* **Description**           

config tpid for each port


* **RTL Instant Name**    : `upen_tpid`

* **Address**             : `0x00_0070-0x00_007F`

* **Formula**             : `0x00_0070 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `64`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:00]`|`cfg_tpid`| [63:48]: tpid value 4 [47:32]: tpid value 3 [31:16]: tpid value 2 [15:00]: tpid value 1 tpid value: 88a8, 8100, 9100 or 9200| `RW`| `0x0`| `0x0 End: Begin:`|

###cfg uni/mul mac

* **Description**           

config uni/mul MAC for each port


* **RTL Instant Name**    : `upen_mac`

* **Address**             : `0x00_0080-0x00_009F`

* **Formula**             : `0x00_0080 + $transtype*16 + $pid`

* **Where**               : 

    * `$transtype(0-1)`

    * `$pid(0-15)`

* **Width**               : `48`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`cfg_mac`| unicast/multicast mac for each port| `RW`| `0x0`| `0x0 End: Begin:`|

###cfg uni ip 1

* **Description**           

config unicast IP 1 for each port


* **RTL Instant Name**    : `upen_uniip1`

* **Address**             : `0x00_00A0-0x00_00AF`

* **Formula**             : `0x00_00A0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `128`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_uniip1`| unicast ip 1 for each port ipv4:<br>{96'b0, ipv4[31:0]} ipv6: ipv6[127:0]| `RW`| `0x0`| `0x0 End: Begin:`|

###cfg uni ip 2

* **Description**           

config unicast IP 2 for each port


* **RTL Instant Name**    : `upen_uniip2`

* **Address**             : `0x00_00B0-0x00_00BF`

* **Formula**             : `0x00_00B0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `128`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_uniip2`| unicast ip 2 for each port ipv4:<br>{96'b0, ipv4[31:0]} ipv6: ipv6[127:0]| `RW`| `0x0`| `0x0 End: Begin:`|

###cfg uni ip 3

* **Description**           

config unicast IP 3 for each port


* **RTL Instant Name**    : `upen_uniip3`

* **Address**             : `0x00_00C0-0x00_00CF`

* **Formula**             : `0x00_00C0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `128`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_uniip3`| unicast ip 3 for each port ipv4:<br>{96'b0, ipv4[31:0]} ipv6: ipv6[127:0]| `RW`| `0x0`| `0x0 End: Begin:`|

###cfg uni ip 4

* **Description**           

config unicast IP 4 for each port


* **RTL Instant Name**    : `upen_uniip4`

* **Address**             : `0x00_00D0-0x00_00DF`

* **Formula**             : `0x00_00D0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `128`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_uniip4`| unicast ip 4 for each port ipv4:<br>{96'b0, ipv4[31:0]} ipv6: ipv6[127:0]| `RW`| `0x0`| `0x0 End: Begin:`|

###cfg mul ip

* **Description**           

config multicast IP for each port


* **RTL Instant Name**    : `upen_mulip`

* **Address**             : `0x00_00E0-0x00_00EF`

* **Formula**             : `0x00_00E0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `128`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_mulip`| multicast ip for each port ipv4:<br>{96'b0, ipv4[31:0]} ipv6: ipv6[127:0]| `RW`| `0x0`| `0x0 End: Begin:`|

###cfg ip matching enable

* **Description**           

config IPv4/IPv6 matching for each port


* **RTL Instant Name**    : `upen_ipen`

* **Address**             : `0x00_00F0-0x00_00FF`

* **Formula**             : `0x00_00F0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:07]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[06:00]`|`cfg_ipen`| config ip matching for each port [6]: ena/dis mul ip global 2 matching [5]: ena/dis mul ip global 1 matching [4]: ena/dis mul ip matching [3]: ena/dis uni ip 4 matching [2]: ena/dis uni ip 3 matching [1]: ena/dis uni ip 2 matching [0]: ena/dis uni ip 1 matching| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter PTP Classify Err

* **Description**           

Number of classify err packet in classify module


* **RTL Instant Name**    : `upen_rx_claerr_cnt`

* **Address**             : `0x00_0200-0x00_020F`

* **Formula**             : `0x00_0200 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_claerr_cnt`| Number classify error packet| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter PTP Packet

* **Description**           

Number of ptp packet in classify module


* **RTL Instant Name**    : `upen_rx_ptp_cnt`

* **Address**             : `0x00_0210-0x00_021F`

* **Formula**             : `0x00_0210 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_ptp_cnt`| Number ptp packet| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Sync Received

* **Description**           

Number of Sync message received


* **RTL Instant Name**    : `upen_rx_sync_cnt`

* **Address**             : `0x00_0240-0x00_024F`

* **Formula**             : `0x00_0240 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`syncrx_cnt`| number sync packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Folu Received

* **Description**           

Number of folu message received


* **RTL Instant Name**    : `upen_rx_folu_cnt`

* **Address**             : `0x00_0250-0x00_025F`

* **Formula**             : `0x00_0250 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`folurx_cnt`| number folu packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dreq Received

* **Description**           

Number of dreq message received


* **RTL Instant Name**    : `upen_rx_dreq_cnt`

* **Address**             : `0x00_0260-0x00_026F`

* **Formula**             : `0x00_0260 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dreqrx_cnt`| number dreq packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dres Received

* **Description**           

Number of dres message received


* **RTL Instant Name**    : `upen_rx_dres_cnt`

* **Address**             : `0x00_0270-0x00_027F`

* **Formula**             : `0x00_0270 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dresrx_cnt`| number dres packet at Rx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Sync Extract Err

* **Description**           

Number of extr err sync mess


* **RTL Instant Name**    : `upen_rx_extr_err_syncnt`

* **Address**             : `0x00_0280-0x00_028F`

* **Formula**             : `0x00_0280 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_synccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Folu Extract Err

* **Description**           

Number of extr err folu mess


* **RTL Instant Name**    : `upen_rx_extr_err_folucnt`

* **Address**             : `0x00_0290-0x00_029F`

* **Formula**             : `0x00_0290 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_foluccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dreq Extract Err

* **Description**           

Number of extr err dreq mess


* **RTL Instant Name**    : `upen_rx_extr_err_dreqcnt`

* **Address**             : `0x00_02A0-0x00_02AF`

* **Formula**             : `0x00_02A0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_dreqcnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dres Extract Err

* **Description**           

Number of extr err dres mess


* **RTL Instant Name**    : `upen_rx_extr_err_drescnt`

* **Address**             : `0x00_02B0-0x00_02BF`

* **Formula**             : `0x00_02B0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_extrerr_drescnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Sync Chsum Err

* **Description**           

Number of Chs err sync mess


* **RTL Instant Name**    : `upen_rx_chs_err_syncnt`

* **Address**             : `0x00_02C0-0x00_02CF`

* **Formula**             : `0x00_02C0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_synccnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Folu Chsum Err

* **Description**           

Number of Chs err folu mess


* **RTL Instant Name**    : `upen_rx_chs_err_folucnt`

* **Address**             : `0x00_02D0-0x00_02DF`

* **Formula**             : `0x00_02D0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_folucnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dreq Chsum Err

* **Description**           

Number of Chs err dreq mess


* **RTL Instant Name**    : `upen_rx_chs_err_dreqcnt`

* **Address**             : `0x00_02E0-0x00_02EF`

* **Formula**             : `0x00_02E0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_dreqcnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Rx Dres Chsum Err

* **Description**           

Number of Chs err dres mess


* **RTL Instant Name**    : `upen_rx_chs_err_drescnt`

* **Address**             : `0x00_02F0-0x00_02FF`

* **Formula**             : `0x00_02F0 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_cherr_drescnt`| | `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Sync Transmit

* **Description**           

Number of Sync message transmit


* **RTL Instant Name**    : `upen_tx_sync_cnt`

* **Address**             : `0x00_0300-0x00_030F`

* **Formula**             : `0x00_0300 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`synctx_cnt`| number sync packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter Folu Transmit

* **Description**           

Number of folu message transmit


* **RTL Instant Name**    : `upen_tx_folu_cnt`

* **Address**             : `0x00_0310-0x00_031F`

* **Formula**             : `0x00_0310 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`folutx_cnt`| number folu packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dreq transmit

* **Description**           

Number of dreq message transmit


* **RTL Instant Name**    : `upen_tx_dreq_cnt`

* **Address**             : `0x00_0320-0x00_032F`

* **Formula**             : `0x00_0320 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dreqtx_cnt`| number dreq packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter dres transmit

* **Description**           

Number of dres message transmit


* **RTL Instant Name**    : `upen_tx_dres_cnt`

* **Address**             : `0x00_0330-0x00_033F`

* **Formula**             : `0x00_0330 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`drestx_cnt`| number dres packet at Tx| `RC`| `0x0`| `0x0 End: Begin:`|

###Counter sop eop

* **Description**           

number of mac_sop and mac_eop for each port


* **RTL Instant Name**    : `upen_staend_num`

* **Address**             : `0x00_0220-0x00_022F`

* **Formula**             : `0x00_0220 + $pid`

* **Where**               : 

    * `$pid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`num_sta_end`| Number sop eop of packet| `RC`| `0x0`| `0x0 End: Begin:`|

###classify sticky

* **Description**           

sticky of classify


* **RTL Instant Name**    : `upen_cla_stk`

* **Address**             : `0x00_0230`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cla_stk`| sticky ge_classify| `W1C`| `0x0`| `0x0 End: Begin:`|

###PTP check sticky

* **Description**           

sticky of ptp check


* **RTL Instant Name**    : `upen_chk_stk`

* **Address**             : `0x00_0231`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`chk_stk`| sticky ptp check| `W1C`| `0x0`| `0x0 End: Begin:`|

###ptp getinfo sticky

* **Description**           

sticky of ptp get info


* **RTL Instant Name**    : `upen_getinf_stk`

* **Address**             : `0x00_0232`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`getinf_stk`| sticky ptp get info| `W1C`| `0x0`| `0x0 End: Begin:`|

###trans sticky

* **Description**           

sticky of ptp trans


* **RTL Instant Name**    : `upen_trans_stk`

* **Address**             : `0x00_0233`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`trans_stk`| sticky ptp trans| `W1C`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

enable for any MAC/IPv4/IPv6


* **RTL Instant Name**    : `upen_cfg_ptp_bypass`

* **Address**             : `0x00_3000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[05:00]`|`cfg_ptp_bypass`| bit[0] l2_sa bit[1] l2_da bit[2] ip4_sa bit[3] ip4_da bit[4] ip6_sa bit[5] ip6_da| `RW`| `0x0`| `0x0  End: Begin:`|

###

* **Description**           

cfg ptp mac sa


* **RTL Instant Name**    : `upen_cfg_mac_sa`

* **Address**             : `0x00_3001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:48]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[47:00]`|`cfg_mac_sa`| MAC SA| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp mac da


* **RTL Instant Name**    : `upen_cfg_mac_da`

* **Address**             : `0x00_3002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:48]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[47:00]`|`cfg_mac_da`| MAC DA| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv4 sa1


* **RTL Instant Name**    : `upen_cfg_ip4_sa1`

* **Address**             : `0x00_3003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ipv4_sa1`| IPv4 SA1| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv4 sa2


* **RTL Instant Name**    : `upen_cfg_ip4_sa2`

* **Address**             : `0x00_3004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ipv4_sa2`| IPv4 SA2| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv4 sa3


* **RTL Instant Name**    : `upen_cfg_ip4_sa3`

* **Address**             : `0x00_3005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ipv4_sa3`| IPv4 SA3| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv4 sa4


* **RTL Instant Name**    : `upen_cfg_ip4_sa4`

* **Address**             : `0x00_3006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ipv4_sa4`| IPv4 SA4| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ip4 da1


* **RTL Instant Name**    : `upen_cfg_ip4_da1`

* **Address**             : `0x00_3007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ip4_da1`| MAC DA1| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ip4 da2


* **RTL Instant Name**    : `upen_cfg_ip4_da2`

* **Address**             : `0x00_3008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ip4_da2`| MAC DA2| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ip4 da3


* **RTL Instant Name**    : `upen_cfg_ip4_da3`

* **Address**             : `0x00_3009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ip4_da3`| MAC DA3| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ip4 da4


* **RTL Instant Name**    : `upen_cfg_ip4_da4`

* **Address**             : `0x00_300A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cfg_ip4_da4`| MAC DA4| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 sa1


* **RTL Instant Name**    : `upen_cfg_ip6_sa1`

* **Address**             : `0x00_300B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_sa1`| IPv6 SA1| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 sa2


* **RTL Instant Name**    : `upen_cfg_ip6_sa2`

* **Address**             : `0x00_300C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_sa2`| IPv6 SA2| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 sa3


* **RTL Instant Name**    : `upen_cfg_ip6_sa3`

* **Address**             : `0x00_300D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_sa3`| IPv6 SA3| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 sa4


* **RTL Instant Name**    : `upen_cfg_ip6_sa4`

* **Address**             : `0x00_300E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv4_sa4`| IPv4 SA4| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 da1


* **RTL Instant Name**    : `upen_cfg_ip6_da1`

* **Address**             : `0x00_300F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_da1`| IPv6 DA1| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 da2


* **RTL Instant Name**    : `upen_cfg_ip6_da2`

* **Address**             : `0x00_3010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_da2`| IPv6 DA2| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 da3


* **RTL Instant Name**    : `upen_cfg_ip6_da3`

* **Address**             : `0x00_3011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_da3`| IPv6 DA3| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

cfg ptp ipv6 da4


* **RTL Instant Name**    : `upen_cfg_ip6_da4`

* **Address**             : `0x00_3012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:00]`|`cfg_ipv6_da4`| IPv6 DA4| `RW`| `0x0`| `0x0 End: Begin:`|

###cpu queue sticky

* **Description**           

sticky of cpu que


* **RTL Instant Name**    : `upen_cpuque_stk`

* **Address**             : `0x00_0234`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cpuque_stk`| sticky of cpu queue| `W1C`| `0x0`| `0x0 End: Begin:`|

###Config t1t3mode

* **Description**           

Used to config t1t3mode


* **RTL Instant Name**    : `upen_t1t3mode`

* **Address**             : `0x00_0017`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`cfg_t1t3mode`| 0: send timestamp to cpu queue 1: send back ptp packet mode| `RW`| `0x0`| `0x0 End: Begin:`|

###Config cpu flush

* **Description**           

Used to config cpu flush


* **RTL Instant Name**    : `upen_cpu_flush`

* **Address**             : `0x00_0018`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00:00]`|`cfg_cpu_flush`| cpu flush| `RW`| `0x0`| `0x0 End: Begin:`|

###Read CPU Queue

* **Description**           

Used to read CPU queue


* **RTL Instant Name**    : `upen_cpuque`

* **Address**             : `0x00_0016`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:102]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[101:100]`|`typ`| typ of ptp packet| `RO`| `0x0`| `0x0`|
|`[99:84]`|`seqid`| sequence of packet from sequenceId field| `RO`| `0x0`| `0x0`|
|`[83:80]`|`egr_pid`| egress port id| `RO`| `0x0`| `0x0`|
|`[79:00]`|`egr_tmr_tod`| egress timer tod| `RO`| `0x0`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_PDH
####Register Table

|Name|Address|
|-----|-----|
|`PDH Global Registers`|`0x00000000 - 0x00000000`|
|`STS/VT Demap Control`|`0x00024000 - 0x000243FF`|
|`STS/VT Demap HW Status`|`0x00024400 - 0x000247FF`|
|`STS/VT Demap Output Debug PRBS`|`0x00020110-0x00020117`|
|`STS/VT Demap Output Debug Test ID Configuration`|`0x00020000-0x00020000`|
|`STS/VT Demap Output Debug Test Report Sticky`|`0x00020001- 0x00020001`|
|`STS/VT Demap Output Debug Test Pattern Configuration`|`0x00020002-0x00020002`|
|`STS/VT Demap Concatenation Control`|`0x00025000 - 0x0002501F`|
|`STS/VT Demap CEP STS HW Status`|`0x00025080-0x0002509F`|
|`STS/VT Demap CEP VC4 fractional VC3 Control`|`0x000251FF`|
|`DS1/E1/J1 Rx Framer Mux Control`|`0x00030000 - 0x0003001F`|
|`PDH Rx Per STS/VC Payload Control`|`0x00030020 - 0x0003003F`|
|`RxM23E23 Control`|`0x00040000 - 0x0004001F`|
|`RxM23E23 HW Status`|`0x00040080-0x0004009F`|
|`RxDS3E3 OH Pro Control`|`0x040420-0x04043F`|
|`RxDS3E3 OH Pro Threshold`|`0x040401`|
|`RxM23E23 Framer REI Threshold Control`|`0x00040403`|
|`RxM23E23 Framer FBE Threshold Control`|`0x00040404`|
|`RxM23E23 Framer Parity Threshold Control`|`0x00040405`|
|`RxM23E23 Framer Cbit Parity Threshold Control`|`0x00040406`|
|`RxM23E23 Framer REI Counter`|`0x00040440 - 0x0004047F`|
|`RxM23E23 Framer FBE Counter`|`0x00040480 - 0x000404BF`|
|`RxM23E23 Framer Parity Counter`|`0x000404C0 - 0x000404FF`|
|`RxM23E23 Framer Cbit Parity Counter`|`0x00040500 - 0x0004053F`|
|`RxDS3E3 FEAC Buffer`|`0x040560-0x04057F`|
|`RxM23E23 Framer Channel Interrupt Enable Control`|`0x000405FE`|
|`RxM23E23 Framer Channel Interrupt Status`|`0x000405FF`|
|`RxM23E23 Framer per Channel Interrupt Enable Control`|`0x00040580-0x0004059F`|
|`RxM23E23 Framer per Channel Interrupt Change Status`|`0x000405A0-0x000405BF`|
|`RxM23E23 Framer per Channel Current Status`|`0x000405C0-0x000405DF`|
|`RxDS3E3 OH Pro DLK Swap Bit Enable`|`0x04040B`|
|`DS3/E3 Test Pattern Monitoring Global Control`|`0x040700`|
|`DS3/E3 Test Pattern Monitoring Sticky Enable`|`0x040701`|
|`DS3/E3 Test Pattern Monitoring Error Counter`|`0x040702`|
|`DS3/E3 Test Pattern Monitoring Good Bit Counter`|`0x040704`|
|`DS3/E3 Test Pattern Monitoring Lost Bit Counter`|`0x040706`|
|`DS3/E3 Test Pattern Monitoring Counter Loading`|`0x040708`|
|`DS3/E3 Test Pattern Monitoring Error-Bit Counter Loading`|`0x040709`|
|`DS3/E3 Test Pattern Monitoring Good-Bit Counter Loading`|`0x04070A`|
|`DS3/E3 Test Pattern Monitoring Lost-Bit Counter Loading`|`0x04070B`|
|`RxM12E12 Control`|`0x00041000 - 0x000410FF`|
|`PDH RxM12E12 Per STS/VC Payload Control`|`0x00041300 - 0x0004131F`|
|`RxM12E12 HW Status`|`0x00041200 - 0x000412FE`|
|`RxM12E12 Framer Channel Interrupt Enable Control`|`0x000413FE`|
|`RxM12E12 Framer Channel Interrupt Status`|`0x000413FF`|
|`RxM12E12 Framer per Channel Interrupt Enable Control`|`0x00041380-0x0004139F`|
|`RxM12E12 Framer per Channel Interrupt Change Status`|`0x000413A0-0x000413BF`|
|`RxM12E12 Framer per Channel Current Status`|`0x000413C0-0x000413DF`|
|`RxM12E12 Global Control`|`0x00041340`|
|`DS1/E1/J1 Rx Framer FBE Threshold Control`|`0x00050003 - 0x00050003`|
|`DS1/E1/J1 Rx Framer CRC Threshold Control`|`0x00050004 - 0x00050004`|
|`DS1/E1/J1 Rx Framer REI Threshold Control`|`0x00050005 - 0x00050005`|
|`DS1/E1/J1 Rx Framer Control`|`0x00054000 - 0x000543FF`|
|`DS1/E1/J1 Rx Framer HW Status`|`0x00054400-0x000547FF`|
|`DS1/E1/J1 Rx Framer CRC Error Counter`|`0x00056800 - 0x00056FFF`|
|`DS1/E1/J1 Rx Framer REI Counter`|`0x00057000 - 0x000577FF`|
|`DS1/E1/J1 Rx Framer FBE Counter`|`0x00056000-0x000567FF`|
|`DS1/E1/J1 Rx Framer per Channel Interrupt Enable Control`|`0x00055000-0x000553FF`|
|`DS1/E1/J1 Rx Framer per Channel Interrupt Status`|`0x00055400-0x000557FF`|
|`DS1/E1/J1 Rx Framer per Channel Current Status`|`0x00055800-0x00055BFF`|
|`DS1/E1/J1 Rx Framer per Channel Interrupt OR Status`|`0x00055C00-0x00055C1F`|
|`DS1/E1/J1 Rx Framer per STS/VC Interrupt OR Status`|`0x00055FFF`|
|`DS1/E1/J1 Rx Framer per STS/VC Interrupt Enable Control`|`0x00055FFE`|
|`DS1/E1/J1 Rx Framer DLK Control`|`0x000D8000 -  0x000D82F7`|
|`DS1/E1/J1 Rx Framer DLK Status`|`0x000D9000 -  0x000D92F7`|
|`DS1/E1/J1 Rx Framer DLK Flush Message`|`0x000D0011`|
|`DS1/E1/J1 Rx Framer DLK Access DDR Control`|`0x000D0010`|
|`DS1/E1/J1 Rx Framer DLK Message0`|`0x000D0016`|
|`DS1/E1/J1 Rx Framer DLK Message1`|`0x000D0017`|
|`DS1/E1/J1 Rx Framer DLK Message2`|`0x000D0018`|
|`DS1/E1/J1 Rx Framer DLK Message3`|`0x000D0019`|
|`DS1/E1/J1 Rx Framer DLK Interrupt Enable`|`0x000DE000`|
|`DS1/E1/J1 Rx Framer DLK Interrupt Sticky`|`0x000DE400`|
|`DS1/E1/J1 Test Pattern Monitoring Global Control`|`0x054900 - 0x054900`|
|`DS1/E1/J1 Test Pattern Monitoring Sticky Enable`|`0x054901 - 0x054901`|
|`DS1/E1/J1 Test Pattern Monitoring Sticky`|`0x054902 - 0x054902`|
|`DS1/E1/J1 Test Pattern Monitoring Mode`|`0x054910 - 0x05491F`|
|`DS1/E1/J1 Test Pattern Monitoring Fixed Pattern`|`0x054920 - 0x05492F`|
|`DS1/E1/J1 Test Pattern Monitoring Selected Channel`|`0x054800 - 0x05480F`|
|`DS1/E1/J1 Test Pattern Monitoring nxDS0 Control`|`0x054810 - 0x05481F`|
|`DS1/E1/J1 Test Pattern Monitoring nxDS0 Concatenation Control`|`0x054820 - 0x05482F`|
|`DS1/E1/J1 Test Pattern Monitoring Error Counter`|`0x054960 - 0x05497F`|
|`DS1/E1/J1 Test Pattern Monitoring Good Bit Counter`|`0x054980 - 0x05499F`|
|`DS1/E1/J1 Test Pattern Monitoring Loss Bit Counter`|`0x0549A0 - 0x0549BF`|
|`DS1/E1/J1 Test Pattern Monitoring Counter Load ID`|`0x054903 - 0x054903`|
|`DS1/E1/J1 Test Pattern Monitoring Error-Bit Counter Loading`|`0x054904 - 0x649B04`|
|`DS1/E1/J1 Test Pattern Monitoring Good-Bit Counter Loading`|`0x054905 - 0x649B05`|
|`DS1/E1/J1 Test Pattern Monitoring Lost-Bit Counter Loading`|`0x054906 - 0x649B06`|
|`DS1/E1/J1 Test Pattern Monitoring Timing Status`|`0x054830 - 0x05483F`|
|`DS1/E1/J1 Test Pattern Monitoring Status`|`0x054940 - 0x05495F`|
|`SPE/VT Map Fifo Length Water Mark`|`0x00070002 - 0x00070002`|
|`STS/VT Map Control`|`0x00076000 - 0x000763FF`|
|`VT Async Map Control`|`0x00076800 - 0x00076BFF`|
|`STS/VT Map FIFO Center Sticky`|`0x00070020 - 0x0007003B`|
|`STS/VT Map HW Status`|`0x00076400-0x000767FF`|
|`TS/VT Map Jitter Configuration`|`0x00070080`|
|`DS1/E1/J1 Tx Framer Control`|`0x00094000 - 0x000943FF`|
|`DS1/E1/J1 Tx Framer Status`|`0x00094400 - 0x000947FF`|
|`DS1/E1/J1 Tx Framer Signaling Insertion Control`|`0x00094800 - 0x00094BFF`|
|`DS1/E1/J1 Tx Framer Signaling ID Conversion Control`|`0x000C2000 - 0x000C23FF`|
|`DS1/E1/J1 Tx Framer Signaling CPU Insert Enable Control`|`0x000CFF01`|
|`DS1/E1/J1 Tx Framer Signaling CPU DS1/E1 signaling Write Control`|`0x000CFF02`|
|`DS1/E1/J1 Tx Framer Signaling Buffer`|`0x000C8000 - 0x000CFF00`|
|`DS1/E1/J1 Test Pattern Generation Control`|`0x0950FF - 0x0950FF`|
|`DS1/E1/J1 Test Pattern Generation Single Bit Error Insertion`|`0x0950FE - 0x0950FE`|
|`DS1/E1/J1 Test Pattern Generation Mode`|`0x095080 - 0x09508F`|
|`DS1/E1/J1 Test Pattern Generation Selected Channel`|`0x095000 - 0x09500F`|
|`DS1/E1/J1 Test Pattern Generation nxDS0 Control`|`0x095010 - 0x09501F`|
|`DS1/E1/J1 Test Pattern Generation nxDS0 Concatenation Control`|`0x095020 - 0x09502F`|
|`DS1/E1/J1 Test Pattern Generation Fixed Pattern Control`|`0x095090 - 0x09509F`|
|`DS1/E1/J1 Test Pattern Generation Error Rate Insertion`|`0x0950A0 - 0x0950AF`|
|`DS1/E1/J1 Test Pattern Generation Timing Status`|`0x615030 - 0x65503F`|
|`DS1/E1/J1 Test Pattern Generation Status`|`0x0950C0 - 0x0950EF`|
|`DS1/E1/J1 Tx Framer DLK Indirect Access Control`|`0x00090008`|
|`DS1/E1/J1 Tx Framer DLK Indirect Access Write Data Control`|`0x00090009`|
|`DS1/E1/J1 Tx Framer DLK Indirect Access Read Data Status`|`0x0009000A`|
|`DS1/E1/J1 Tx Framer DLK Insertion Control`|`0x096020-0x09602F`|
|`DS1/E1/J1 Tx Framer DLK Insertion Enable`|`0x00096010`|
|`DS1/E1/J1 Tx Framer DLK Insertion New Message Start Enable`|`0x00096011`|
|`DS1/E1/J1 Tx Framer DLK Insertion Engine Status`|`0x00096012`|
|`DS1/E1/J1 Tx Framer DLK Insertion Message Memory`|`0x096800-0x096FFF`|
|`DS1/E1/J1 Tx Framer DLK Insertion BOM Control`|`0x097000-0x0973FF`|
|`TxM23E23 Control`|`0x00080000 - 0x0008001F`|
|`TxM23E23 HW Status`|`0x00080080-0x0008009F`|
|`TxM23E23 E3g832 Trace Byte`|`0x00080200-0x0803FF`|
|`DS3/E3 Test Pattern Generation Global Control`|`0x0800D0-7800D3`|
|`TxM12E12 Control`|`0x00081000 - 0x000810FF`|
|`TxM12E12 HW Status`|`0x00081200 - 0x000812FF`|
|`Tx M12E12 Jitter Configuration`|`0x00081380`|
|`LC test reg1`|`0xE_0000`|
|`LC test reg2`|`0xE_0001`|
|`Config Limit error`|`0xE_0002`|
|`Config limit match`|`0xE_0003`|
|`Config thresh fdl parttern`|`0xE_0004`|
|`Config thersh error`|`0xE_0005`|
|`Sticky pattern detected`|`0xE_20FF`|
|`Number of pattern`|`0xE_20FE`|
|`Current Inband Code`|`0xE_2100 - 0xE_24FF`|
|`Sticky FDL message detected`|`0xE_08FF`|
|`Config User programmble Pattern User Codes`|`0xE_08F8`|
|`Number of message`|`0xE_08FE`|
|`Currenr Message FDL Detected`|`0xE_1800 - 0xE_1BFF`|
|`Config Theshold Pattern Report`|`0xE_0006`|
|`Sticky Loopcode detected`|`0xE_2800 - 0xE_281F`|
|`Sticky Loopcode change`|`0xE_3000 - 0xE_371f`|
|`Sticky Outband LoopCode`|`0xE_0900 - 0xE_091F`|
|`Sticky Outband LoopCode change`|`0xE_1000 - 0x101f`|
|`RAM Parity Force Control`|`0x10`|
|`RAM Parity Disable Control`|`0x11`|
|`RAM parity Error Sticky`|`0x12`|
|`DS1/E1/J1 Payload Error Insert Enable Configuration`|`0x00090002`|
|`DS1/E1/J1 Payload Error Insert threhold Configuration`|`0x00090003`|
|`DS1/E1/J1 Rx Error Insert Enable Configuration`|`0x00050017`|
|`DS1/E1/J1 Payload Error Insert threhold Configuration`|`0x00050018`|
|`DS3/E3 Payload Error Insert Enable Configuration`|`0x000800C0`|
|`DS3/E3 Payload Error Insert threhold Configuration`|`0x000800C1`|
|`DS3/E3 Rx Error Insert Enable Configuration`|`0x000400C0`|
|`DS3/E3 Payload Error Insert threhold Configuration`|`0x000400C1`|


###PDH Global Registers

* **Description**           

This is the global configuration register for the PDH


* **RTL Instant Name**    : `glb`

* **Address**             : `0x00000000 - 0x00000000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12]`|`pdhde3masklof4out8k`| Set 1 to ignore DE3 LOF to mask out8k| `RW`| `0x0`| `0x0`|
|`[11]`|`pdhde3maskds3aissignal4out8k`| Set 1 to ignore DS3 AIS signal to mask out8k| `RW`| `0x0`| `0x0`|
|`[10]`|`pdhde3masklosais4out8k`| Set 1 to ignore DS3 LOS all0s and AIS all1s to mask out8k| `RW`| `0x0`| `0x0`|
|`[9]`|`pdhe13idconv`| Set 1 to enable ID conversion of the E1 in an E3 as following ID in an E3 (bit 4:0) ID at Data Map (bit 4:0) 5'd0                  5'd0 5'd1                  5'd1 5'd2                  5'd2 5'd3                  5'd4 5'd4                  5'd5 5'd5                  5'd6 5'd6                  5'd8 5'd7                  5'd9 5'd8                  5'd10 5'd9                  5'd12 5'd10                 5'd13 5'd11                 5'd14 5'd12                 5'd16 5'd13                 5'd17 5'd14                 5'd18 5'd15                 5'd20| `RW`| `0x0`| `0x0`|
|`[8]`|`pdhfullsonetsdh`| Set 1 to enable full SONET/SDH mode (no traffic on PDH interface)| `RW`| `0x0`| `0x0`|
|`[7]`|`pdhfulllineloop`| Set 1 to enable full Line Loop Back at Data map side| `RW`| `0x0`| `0x0`|
|`[6]`|`pdhfullpayloop`| Set 1 to enable full Payload Loop back at Data map side| `RW`| `0x0`| `0x0`|
|`[5]`|`pdhfullpdhbus`| Set 1 to enable Full traffic on PDH Bus| `RW`| `0x0`| `0x0`|
|`[4]`|`pdhparallelpdhmd`| Set 1 to enable Parallel PDH Bus mode| `RW`| `0x0`| `0x0`|
|`[3]`|`pdhsaturation`| Set 1 to enable Saturation mode of all PDH counters. Set 0 for Roll-Over mode| `RW`| `0x0`| `0x0`|
|`[2]`|`pdhreservedbit`| Transmit Reserved Bit| `RW`| `0x0`| `0x0`|
|`[1]`|`pdhstufftbit`| Transmit Stuffing Bit| `RW`| `0x0`| `0x0`|
|`[0]`|`pdhnoidconv`| Set 1 to enable no ID convert at PDH Block| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Demap Control

* **Description**           

The STS/VT Demap Control is use to configure for per channel STS/VT demap operation.


* **RTL Instant Name**    : `stsvt_demap_ctrl`

* **Address**             : `0x00024000 - 0x000243FF`

* **Formula**             : `0x00024000 +  32*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$stsid(0-23):`

    * `$vtgid(0-6):`

    * `$vtid(0-3)`

* **Width**               : `9`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8]`|`stsvtdemapfrcais`| This bit is set to 1 to force the AIS to downstream data. 1: force AIS. 0: not force AIS.| `RW`| `0x0`| `0x0`|
|`[7]`|`stsvtdemapautoais`| This bit is set to 1 to automatically generate AIS to downstream data whenever the high-order STS/VT is in AIS. 1: auto AIS generation. 0: not auto AIS generation| `RW`| `0x0`| `0x0`|
|`[6]`|`stsvtdemapcepmode`| STS/VC CEP fractional mode| `RW`| `0x0`| `0x0`|
|`[5:4]`|`stsvtdemapfrmtype`| STS/VT Frmtype configure| `RW`| `0x0`| `0x0`|
|`[3:0]`|`stsvtdemapsigtype`| STS/VT Sigtype configure Sigtype[3:0] Frmtype[1:0] CepMode Operation Mode 0 x x VT/TU to DS1 Demap 1 x x VT/TU to E1 Demap 2 x 0 STS1/VC3 to DS3 Demap 2 x 1 TUG3/TU3 to DS3 Demap 3 x 0 STS1/VC3 to E3 Demap 3 x 1 TUG3/TU3 to E3 Demap 4 0 x Packet over VT1.5/TU11 4 1 x Reserved 4 2 x Reserved 4 3 x VT1.5/TU11 Basic CEP 5 0 x Packet over VT2/TU12 5 1 x Reserved 5 2 x Reserved 5 3 x VT2/TU12 Basic CEP 6 x x Reserved 7 x x Reserved 8 0 x Packet over STS1/VC3 8 1 0 STS1/VC3 Fractional CEP carrying VT2/TU12 8 1 1 STS1/VC3 Fractional CEP carrying VT15/TU11 8 2 0 STS1/VC3 Fractional CEP carrying E3 8 2 1 STS1/VC3 Fractional CEP carrying DS3 8 3 x STS1/VC3 Basic CEP 9 0 x Packet over STS3c/VC4 9 1 0 STS3c/VC4 Fractional CEP carrying VT2/TU12 9 1 1 STS3c/VC4 Fractional CEP carrying VT15/TU11 9 2 0 STS3c/VC4 Fractional CEP carrying E3 9 2 1 STS3c/VC4 Fractional CEP carrying DS3 9 3 x STS3c/VC4 Basic CEP 10 3 x STS12c/VC4_4c Basic CEP 11 x x Reserved 12 x x Packet over TU3 13 x x Reserved 14 x x Reserved 15 x x Disable STS/VC/VT/TU/DS1/E1/DS3/E3| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Demap HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `stsvt_demap_hw_stat`

* **Address**             : `0x00024400 - 0x000247FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `27`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[26:0]`|`stsvtdemapstatus`| for HW debug only| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Demap Output Debug PRBS

* **Description**           

for HW debug only


* **RTL Instant Name**    : `stsvt_demap_debug_prbs`

* **Address**             : `0x00020110-0x00020117`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `10`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9:0]`|`outputdebugprbs`| for HW debug only| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Demap Output Debug Test ID Configuration

* **Description**           

for HW debug only


* **RTL Instant Name**    : `stsvt_demap_dbg_testid_cfg`

* **Address**             : `0x00020000-0x00020000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9:0]`|`testidcfg`| for HW debug only| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Demap Output Debug Test Report Sticky

* **Description**           

for HW debug only


* **RTL Instant Name**    : `stsvt_demap_dbg_testrep_sticky`

* **Address**             : `0x00020001- 0x00020001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`testrptstk`| for HW debug only| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Demap Output Debug Test Pattern Configuration

* **Description**           

for HW debug only


* **RTL Instant Name**    : `stsvt_demap_dbg_testpat_cfg`

* **Address**             : `0x00020002-0x00020002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`testpatterncfg`| for HW debug only| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Demap Concatenation Control

* **Description**           

The STS/VT Demap Concatenation Control is use to configure for per STS1/VC3 Concatenation operation.


* **RTL Instant Name**    : `stsvt_demap_conc_ctrl`

* **Address**             : `0x00025000 - 0x0002501F`

* **Formula**             : `0x00025000 +  stsid`

* **Where**               : 

    * `$stsid(0-23):`

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[5]`|`stsvtdemapccaten`| This bit is set to 1 to configure the STS to Concatenation mode| `RW`| `0x0`| `0x0`|
|`[4:0]`|`stsvtdemapccatmstid`| Master ID of the concatenated STS1/VC3. Write the same STS1/VC3 ID to indicate that STS1/VC3 is the master STS1/VC3 of a concatenated group| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Demap CEP STS HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `stsvt_demap_cep_sts_hw_stat`

* **Address**             : `0x00025080-0x0002509F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `127`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[118:105]`|`ebmvtg`| | `RW`| `0x0`| `0x0`|
|`[104:77]`|`ebmbit`| | `RW`| `0x0`| `0x0`|
|`[76:49]`|`stspos`| | `RW`| `0x0`| `0x0`|
|`[48:21]`|`stsneg`| | `RW`| `0x0`| `0x0`|
|`[20:13]`|`b3calc`| | `RW`| `0x0`| `0x0`|
|`[12:5]`|`b3lat`| | `RW`| `0x0`| `0x0`|
|`[4:2]`|`vtgid`| | `RW`| `0x0`| `0x0`|
|`[1:0]`|`vtid`| | `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Demap CEP VC4 fractional VC3 Control

* **Description**           

The STS/VT Demap CEP VC4 fractional VC3 Control is use to configure for per STS1/VC3 Concatenation operation.


* **RTL Instant Name**    : `stsvt_demap_cep_vc4_frac_vc3_ctrl`

* **Address**             : `0x000251FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:0]`|`vc4fracvc3`| Each bit for each STS. Set 1 is VC4 fractional VC3 mode. Set 0 for other mode| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer Mux Control

* **Description**           

These registers are used to configure for the source data of Ds1/E1 which may be get from Serial PDH interface or from SONET/SDH.


* **RTL Instant Name**    : `dej1_rxfrm_mux_ctrl`

* **Address**             : `0x00030000 - 0x0003001F`

* **Formula**             : `0x00030000 +  stsid`

* **Where**               : 

    * `$stsid(0-23):`

* **Width**               : `28`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`pdhrxmuxsel`| 28 selector bit for 28 DS1/E1/J1 in a STS/VC 1: source selected from PDH Interface 0 (default): source selected from SONET/SDH Interface| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH Rx Per STS/VC Payload Control

* **Description**           

The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1.


* **RTL Instant Name**    : `rx_stsvc_payload_ctrl`

* **Address**             : `0x00030020 - 0x0003003F`

* **Formula**             : `0x00030020 +  stsid`

* **Where**               : 

    * `$stsid(0-23):`

* **Width**               : `14`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13:12]`|`pdhvtrxmaptug26type`| Define types of VT/TUs in TUG-2-6. 0: TU11/VT1.5 type 1: TU12/VT2 type 2: reserved 3: VC/STS/DS3/E3| `RW`| `0x0`| `0x0`|
|`[11:10]`|`pdhvtrxmaptug25type`| Define types of VT/TUs in TUG-2-5| `RW`| `0x0`| `0x0`|
|`[9:8]`|`pdhvtrxmaptug24type`| Define types of VT/TUs in TUG-2-4| `RW`| `0x0`| `0x0`|
|`[7:6]`|`pdhvtrxmaptug23type`| Define types of VT/TUs in TUG-2-3| `RW`| `0x0`| `0x0`|
|`[5:4]`|`pdhvtrxmaptug22type`| Define types of VT/TUs in TUG-2-2| `RW`| `0x0`| `0x0`|
|`[3:2]`|`pdhvtrxmaptug21type`| Define types of VT/TUs in TUG-2-1| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pdhvtrxmaptug20type`| Define types of VT/TUs in TUG-2-0| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Control

* **Description**           

The RxM23E23 Control is use to configure for per channel Rx DS3/E3 operation.


* **RTL Instant Name**    : `rxm23e23_ctrl`

* **Address**             : `0x00040000 - 0x0004001F`

* **Formula**             : `0x00040000 +  de3id`

* **Where**               : 

    * `$de3id(0-23):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[21]`|`ds3loffrwdis`| Forward DS3 LOF to generate L-bit to PSN in case of MonOnly Disable| `RW`| `0x0`| `0x0`|
|`[20]`|`ds3aissigfrwenb`| Forward DS3 AIS signal to generate L-bit to PSN in case of MonOnly Enable| `RW`| `0x0`| `0x0`|
|`[19]`|`rxds3forceaislbit`| Force DS3 AIS, L-bit to PSN, data all one to PSN for Payload/Line Remote Loopback| `RW`| `0x0`| `0x0`|
|`[18]`|`rxde3mononly`| Framer monitor only| `RW`| `0x0`| `0x0`|
|`[17]`|`rxds3e3chenb`| | `RW`| `0x0`| `0x0`|
|`[16:14]`|`rxds3e3losthres`| Threshold to detect LOS all zero, Set 0 to disable| `RW`| `0x0`| `0x0`|
|`[13:11]`|`rxds3e3aisthres`| Threshold to detect AIS all one, Set 0 to disable| `RW`| `0x0`| `0x0`|
|`[10:8]`|`rxds3e3lofthres`| | `RW`| `0x0`| `0x0`|
|`[7]`|`rxds3e3payallone`| This bit is set to 1 to force the DS3/E3 framer payload AIS downstream| `RW`| `0x0`| `0x0`|
|`[6]`|`rxds3e3lineallone`| This bit is set to 1 to force the DS3/E3 framer line AIS downstream| `RW`| `0x0`| `0x0`|
|`[5]`|`rxds3e3lineloop`| | `RW`| `0x0`| `0x0`|
|`[4]`|`rxds3e3frclof`| This bit is set to 1 to force the Rx DS3/E3 framer into LOF status. 1: force LOF. 0: not force LOF.| `RW`| `0x0`| `0x0`|
|`[3:0]`|`rxds3e3md`| Rx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3 C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111: E3 G.751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `rxm23e23_hw_stat`

* **Address**             : `0x00040080-0x0004009F`

* **Formula**             : `0x00040080 +  de3id`

* **Where**               : 

    * `$de3id(0-23):`

* **Width**               : `7`
* **Register Type**       : `Stat`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`rxds3_crraic`| Current AIC Bit value| `RW`| `0x0`| `0x0`|
|`[5]`|`rxds3_crraisdownstr`| AIS Down Stream due to HI_Level AIS of DS3E3| `RW`| `0x0`| `0x0`|
|`[4]`|`rxds3_crrlosallzero`| LOS All zero of DS3E3| `RW`| `0x0`| `0x0`|
|`[3]`|`rxds3_crraisallone`| AIS All one of DS3E3| `RW`| `0x0`| `0x0`|
|`[2]`|`rxds3_crraissignal`| AIS signal of DS3| `RW`| `0x0`| `0x0`|
|`[1:0]`|`rxds3e3state`| 0:LOF, 1:Searching, 2: Verify, 3:InFrame| `RW`| `0x0`| `0x0 End: Begin:`|

###RxDS3E3 OH Pro Control

* **Description**           




* **RTL Instant Name**    : `rx_de3_oh_ctrl`

* **Address**             : `0x040420-0x04043F`

* **Formula**             : `0x040420 +  de3id`

* **Where**               : 

    * `$de3id(0-23):`

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[5]`|`dlen`| DLEn 1: Data Link send to HDLC 0: Data Link not send to HDLC| `RW`| `0x0`| `0x0`|
|`[4:3]`|`ds3e3dlsl`| 00: DL 01: UDL 10: FEAC 11: NA| `RW`| `0x0`| `0x0`|
|`[2]`|`ds3feacen`| Enable detection FEAC state change| `RW`| `0x0`| `0x0`|
|`[1]`|`fbefbitcnten`| Enable FBE counts F-Bit| `RW`| `0x0`| `0x0`|
|`[0]`|`fbembitcnten`| Enable FBE counts M-Bit| `RW`| `0x0`| `0x0 End: Begin:`|

###RxDS3E3 OH Pro Threshold

* **Description**           




* **RTL Instant Name**    : `rxde3_oh_thrsh_cfg`

* **Address**             : `0x040401`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:21]`|`idlethresds3`| | `RW`| `0x0`| `0x0`|
|`[20:18]`|`aisthresds3`| | `RW`| `0x0`| `0x0`|
|`[17:15]`|`timingmakere3g832`| | `RW`| `0x0`| `0x0`|
|`[14:12]`|`newpaytypee3g832`| | `RW`| `0x0`| `0x0`|
|`[11:9]`|`aicthres`| | `RW`| `0x0`| `0x0`|
|`[8:6]`|`rdie3g832thres`| | `RW`| `0x0`| `0x0`|
|`[5:3]`|`rdie3g751thres`| | `RW`| `0x0`| `0x0`|
|`[2:0]`|`rdids3thres`| | `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer REI Threshold Control

* **Description**           

This is the REI threshold configuration register for the PDH DS3/E3 Rx framer


* **RTL Instant Name**    : `rxm23e23_rei_thrsh_cfg`

* **Address**             : `0x00040403`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23reithres`| Threshold to generate interrupt if the REI counter exceed this threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer FBE Threshold Control

* **Description**           

This is the REI threshold configuration register for the PDH DS3/E3 Rx framer


* **RTL Instant Name**    : `rxm23e23_fbe_thrsh_cfg`

* **Address**             : `0x00040404`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23fbethres`| Threshold to generate interrupt if the FBE counter exceed this threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer Parity Threshold Control

* **Description**           

This is the REI threshold configuration register for the PDH DS3/E3 Rx framer


* **RTL Instant Name**    : `rxm23e23_parity_thrsh_cfg`

* **Address**             : `0x00040405`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23parthres`| Threshold to generate interrupt if the PAR counter exceed this threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer Cbit Parity Threshold Control

* **Description**           

This is the REI threshold configuration register for the PDH DS3/E3 Rx framer


* **RTL Instant Name**    : `rxm23e23_cbit_parity_thrsh_cfg`

* **Address**             : `0x00040406`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23cparthres`| Threshold to generate interrupt if the Cbit Parity counter exceed this threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer REI Counter

* **Description**           

This is the per channel REI counter for DS3/E3 receive framer


* **RTL Instant Name**    : `rxm23e23_rei_cnt`

* **Address**             : `0x00040440 - 0x0004047F`

* **Formula**             : `0x00040440 +  32*Rd2Clr + de3id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only`

    * `$de3id(0-23):)`

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23reicnt`| DS3/E3 REI error accumulator| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer FBE Counter

* **Description**           

This is the per channel FBE counter for DS3/E3 receive framer


* **RTL Instant Name**    : `rxm23e23_fbe_cnt`

* **Address**             : `0x00040480 - 0x000404BF`

* **Formula**             : `0x00040480 +  32*Rd2Clr + de3id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.`

    * `$de3id(0-23):`

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`count`| frame FBE counter| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer Parity Counter

* **Description**           

This is the per channel Parity counter for DS3/E3 receive framer


* **RTL Instant Name**    : `rxm23e23_parity_cnt`

* **Address**             : `0x000404C0 - 0x000404FF`

* **Formula**             : `0x000404C0 +  32*Rd2Clr + de3id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.`

    * `$de3id(0-23):`

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23parcnt`| DS3/E3 Parity error accumulator| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer Cbit Parity Counter

* **Description**           

This is the per channel Cbit Parity counter for DS3/E3 receive framer


* **RTL Instant Name**    : `rxm23e23_cbit_parity_cnt`

* **Address**             : `0x00040500 - 0x0004053F`

* **Formula**             : `0x00040500 +  32*Rd2Clr + de3id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only.`

    * `$de3id(0-23):`

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxm23e23cparcnt`| DS3/E3 Cbit Parity error accumulator| `RW`| `0x0`| `0x0 End: Begin:`|

###RxDS3E3 FEAC Buffer

* **Description**           




* **RTL Instant Name**    : `rx_de3_feac_buffer`

* **Address**             : `0x040560-0x04057F`

* **Formula**             : `0x040560 +  de3id`

* **Where**               : 

    * `$de3id(0-23):`

* **Width**               : `9`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8:6]`|`feac_state`| 1xx: idle codeword 001: active codeword 010: de-active codeword 011: alarm/status codeword| `RW`| `0x0`| `0x0`|
|`[5:0]`|`feac_codeword`| 6 x-bits cw in format 111111110xxxxxx0| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer Channel Interrupt Enable Control

* **Description**           

This is the Channel interrupt enable of DS3/E3 Rx Framer.


* **RTL Instant Name**    : `rxm23e23_chn_intr_cfg`

* **Address**             : `0x000405FE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:0]`|`rxm23e23intren`| Each bit correspond with each channel DS3/E3 0: Disable 1: Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer Channel Interrupt Status

* **Description**           

This is the Channel interrupt tus of DS3/E3 Rx Framer.


* **RTL Instant Name**    : `rxm23e23_chn_intr_stat`

* **Address**             : `0x000405FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:0]`|`rxm23e23intrsta`| Each bit correspond with each channel DS3/E3 0: No Interrupt 1: Have Interrupt| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer.


* **RTL Instant Name**    : `rxm23e23_per_chn_intr_cfg`

* **Address**             : `0x00040580-0x0004059F`

* **Formula**             : `0x00040580 +  de3id`

* **Where**               : 

    * `$de3id(0-23):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`de3bertca_en`| Set 1 to enable change DE3 BER TCA event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[14]`|`de3bersd_en`| Set 1 to enable change DE3 BER SD event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[13]`|`de3bersf_en`| Set 1 to enable change DE3 BER SF event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[12]`|`de3lcvcnt_en`| Set 1 to enable change DE3 LCV counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[11]`|`ds3cp_e3tr_en`| Set 1 to enable change DS3 CP counter or E3G831 Trace event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[10]`|`de3parcnt_en`| Set 1 to enable change DE3 Parity counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[9]`|`de3fbecnt_en`| Set 1 to enable change DE3 FBE counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[8]`|`de3reicnt_en`| Set 1 to enable change DE3 REI counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[7]`|`ds3feac_e3ssm_en`| Set 1 to enable change DS3 FEAC or E3G832 SSM event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[6]`|`ds3aic_e3tm_en`| Set 1 to enable change DS3 AIC or E3G832 TM event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[5]`|`ds3idlesig_e3pt_en`| Set 1 to enable change DS3 IDLE signal or E3G832 PayloadType event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[4]`|`de3aisalloneintren`| Set 1 to enable change AIS All One event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[3]`|`de3losallzerointren`| Set 1 to enable change LOS All Zero event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[2]`|`ds3aissigintren`| Set 1 to enable change AIS Signal event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[1]`|`de3rdiintren`| Set 1 to enable change RDI event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[0]`|`de3lofintren`| Set 1 to enable change LOF event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer per Channel Interrupt Change Status

* **Description**           

This is the per Channel interrupt tus of DS3/E3 Rx Framer.


* **RTL Instant Name**    : `rxm23e23_per_chn_intr_stat`

* **Address**             : `0x000405A0-0x000405BF`

* **Formula**             : `0x000405A0 +  de3id`

* **Where**               : 

    * `$de3id(0-23):`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`de3bertca_intr`| Set 1 if there is change DE3 BER TCA event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[14]`|`de3bersd_intr`| Set 1 if there is change DE3 BER SD event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[13]`|`de3bersf_intr`| Set 1 if there is change DE3 BER SF event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[12]`|`de3lcvcnt_intr`| Set 1 if there is change DE3 LCV counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[11]`|`ds3cp_e3tr_intr`| Set 1 if there is change DS3 CP counter or E3G831 Trace event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[10]`|`de3parcnt_intr`| Set 1 if there is change DE3 Parity counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[9]`|`de3fbecnt_intr`| Set 1 if there is change DE3 FBE counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[8]`|`de3reicnt_intr`| Set 1 to enable change DE3 REI counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[7]`|`ds3feac_e3ssm_intr`| Set 1 if there is a change DS3 FEAC or E3G832 SSM event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[6]`|`ds3aic_e3tm_intr`| Set 1 if there is a change DS3 AIC or E3G832 TM event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[5]`|`ds3idlesig_e3pt_intr`| Set 1 if there is a change DS3 IDLE signal or E3G832 PayloadType event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[4]`|`de3aisallone_intr`| Set 1 if there is a change AIS All One event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[3]`|`de3losallzero_intr`| Set 1 if there is a change LOS All Zero event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[2]`|`ds3aissig_intr`| Set 1 if there is a change AIS Signal event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[1]`|`de3rdi_intr`| Set 1 if there is a  change RDI event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[0]`|`de3lof_intr`| Set 1 if there is a  change LOF event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM23E23 Framer per Channel Current Status

* **Description**           

This is the per Channel Current tus of DS3/E3 Rx Framer.


* **RTL Instant Name**    : `rxm23e23_per_chn_curr_stat`

* **Address**             : `0x000405C0-0x000405DF`

* **Formula**             : `0x000405C0 +  de3id`

* **Where**               : 

    * `$de3id(0-23):`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`de3bertca_currsta`| Current status DE3 BER TCA event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[14]`|`de3bersd_currsta`| Current status DE3 BER SD event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[13]`|`de3bersf_currsta`| Current status DE3 BER SF event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[12]`|`de3lcvcnt_currsta`| Current status DE3 LCV counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[11]`|`ds3cp_e3tr_currsta`| Current status DS3 CP counter or E3G831 Trace event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[10]`|`de3parcnt_currsta`| Current status DE3 Parity counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[9]`|`de3fbecnt_currsta`| Current status DE3 FBE counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[8]`|`de3reicnt_currsta`| Current status DE3 REI counter event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[7]`|`ds3feac_e3ssm_currsta`| Current status of DS3 FEAC or E3G832 SSM event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[6]`|`ds3aic_e3tm_currsta`| Current status of DS3 AIC or E3G832 TM event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[5]`|`ds3idlesig_e3pt_currsta`| Current status of DS3 IDLE signal or E3G832 PayloadType event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[4]`|`de3aisallonecurrsta`| Current status of AIS All One event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[3]`|`de3losallzerocurrsta`| Current status of LOS All Zero event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[2]`|`ds3aissigcurrsta`| Current status of AIS Signal event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[1]`|`de3rdicurrsta`| Current status of RDI event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[0]`|`de3lofcurrsta`| Current status of LOF event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###RxDS3E3 OH Pro DLK Swap Bit Enable

* **Description**           




* **RTL Instant Name**    : `rxde3_oh_dlk_swap_bit_cfg`

* **Address**             : `0x04040B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `3`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2]`|`dlkswapbitds3en`| | `RW`| `0x0`| `0x0`|
|`[1]`|`dlkswapbite3g751en`| | `RW`| `0x0`| `0x0`|
|`[0]`|`dlkswapbite3g823en`| | `RW`| `0x0`| `0x0 End: Begin:`|

###DS3/E3 Test Pattern Monitoring Global Control

* **Description**           




* **RTL Instant Name**    : `de3_testpat_mon_glb_cfg`

* **Address**             : `0x040700`

* **Formula**             : `0x040700 + id`

* **Where**               : 

    * `$id(0-3):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[22]`|`rxde3ptmen`| Bit Enable Test Pattern Monitoring| `RW`| `0x0`| `0x0`|
|`[21:18]`|`rxde3ptmid`| To configure which DS3/E3 line is selected  to monitor the Test Pattern| `RW`| `0x0`| `0x0`|
|`[17]`|`rxde3ptminten`| Interrupt enable to output upint| `RW`| `0x0`| `0x0`|
|`[16]`|`rxde3ptmsatura`| Counter BERT mode 1: PTM Counter Saturation Mode 0: PTM Counter Roll-Over Mode| `RW`| `0x0`| `0x0`|
|`[15]`|`rxde3ptmstkcfg`| BERT error sticky| `RW`| `0x0`| `0x0`|
|`[14:11]`|`rxde3ptmlopthr`| Threshold for Loss Of Pattern detection. Status will go LOP if the number of 		error patterns (8 bits) in 16 consecutive patterns is more than or equal the RxDe3PtmLopThr| `RW`| `0x0`| `0x0`|
|`[10:7]`|`rxde3ptmsynthr`| Threshold for Sync detection. Status will go SYN if the number of corrected 		consecutive patterns (8 bits) is more than or equal the RxDe3PtmSynThr.| `RW`| `0x0`| `0x0`|
|`[6]`|`rxde3ptmswap`| PTM swap Bit. If it is set to 1, pattern is transmitted LSB first, then MSB. 		Normal is set to 0.| `RW`| `0x0`| `0x0`|
|`[5]`|`rxde3ptminv`| PTM inversion Bit. Normal is set to 0.| `RW`| `0x0`| `0x0`|
|`[4]`|`rxde3ptmresync`| Bit force Ptm engine n from a certain te to loss sync state. Engine will 		re-search pattern test. Note: if this bit is hold the value 1, engine is always in loss sync state.| `RW`| `0x0`| `0x0`|
|`[3:0]`|`rxde3ptmmod`| Monitor DS3/E3 BERT mode 0000: Prbs9 0001: Prbs11 0010: Prbs15 0011: Prbs20 0100: Prbs20 0101: Qrss20 0110: Prbs23 0111: Prbs31 1000: Sequence 1001: AllOne 1010: AllZero 1011: AA55| `RW`| `0x0`| `0x0 End: Begin:`|

###DS3/E3 Test Pattern Monitoring Sticky Enable

* **Description**           




* **RTL Instant Name**    : `de3_testpat_mon_sticky_en`

* **Address**             : `0x040701`

* **Formula**             : `0x040701 + 16*id`

* **Where**               : 

    * `$id(0-3):`

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`rxde3ptmstken`| Enable PTM Sticky| `RW`| `0x0`| `0x0 End: Begin:`|

###DS3/E3 Test Pattern Monitoring Error Counter

* **Description**           

These registers are used to be error counter report of PTM while the monitor is in frame


* **RTL Instant Name**    : `de3_testpat_mon_err_cnt`

* **Address**             : `0x040702`

* **Formula**             : `0x040702 +  16*id +  R2Clr`

* **Where**               : 

    * `$id(0-3):`

    * `$R2Clr(0-1): 0: R2CLR, 1: RO`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxde3ptmerrcnt`| Error counter for BERT monitoring.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS3/E3 Test Pattern Monitoring Good Bit Counter

* **Description**           

These registers are used to be good bit counter report of PTM while the monitor is in frame


* **RTL Instant Name**    : `de3_testpat_mon_goodbit_cnt`

* **Address**             : `0x040704`

* **Formula**             : `0x040704 +  16*id +  R2Clr`

* **Where**               : 

    * `$id(0-3):`

    * `$R2Clr(0-1): 0: R2CLR, 1: RO`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxde3ptmgoodcnt`| Good bit counter for BERT monitoring.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS3/E3 Test Pattern Monitoring Lost Bit Counter

* **Description**           

These registers are used to be bit counter report of PTM while the monitor is in loss of frame


* **RTL Instant Name**    : `de3_testpat_mon_lostbit_cnt`

* **Address**             : `0x040706`

* **Formula**             : `0x040706 +  16*id +  R2Clr`

* **Where**               : 

    * `$id(0-3):`

    * `$R2Clr(0-1): 0: R2CLR, 1: RO`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxde3ptmlostcnt`| Counter for BERT monitoring.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS3/E3 Test Pattern Monitoring Counter Loading

* **Description**           

This register is used to configure the enable to load all the test pattern monitor to the loading registers at the same time.


* **RTL Instant Name**    : `de3_testpat_mon_cnt_cfg`

* **Address**             : `0x040708`

* **Formula**             : `0x040708 + 16*id`

* **Where**               : 

    * `$id(0-3):`

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`rxde3ptmcntloaden`| DS3/E3 Pattern Monitoring Counter Load Enable.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS3/E3 Test Pattern Monitoring Error-Bit Counter Loading

* **Description**           

This register stores the value of Error-Bit counter after counter loading action


* **RTL Instant Name**    : `de3_testpat_mon_errbit_cnt`

* **Address**             : `0x040709`

* **Formula**             : `0x040709 + 16*id`

* **Where**               : 

    * `$id(0-3):`

* **Width**               : `32`
* **Register Type**       : `status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxde3ptmloaderrcnt`| Load Error counter.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS3/E3 Test Pattern Monitoring Good-Bit Counter Loading

* **Description**           

This register stores the value of Good-Bit counter after counter loading action


* **RTL Instant Name**    : `de3_testpat_mon_goodbit_cnt_loading`

* **Address**             : `0x04070A`

* **Formula**             : `0x04070A +  16*id`

* **Where**               : 

    * `$id(0-3):`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxde3ptmgdcntload`| Load Good counter.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS3/E3 Test Pattern Monitoring Lost-Bit Counter Loading

* **Description**           

This register stores the value of Lost-Bit counter after counter loading action


* **RTL Instant Name**    : `de3_testpat_mon_lostbit_cnt_loading`

* **Address**             : `0x04070B`

* **Formula**             : `0x04070B +  16*id`

* **Where**               : 

    * `$id(0-3):`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxde3ptmlosscntload`| Load Loss counter.| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Control

* **Description**           

The RxM12E12 Control is use to configure for per channel Rx DS2/E2 operation.


* **RTL Instant Name**    : `rxm12e12_ctrl`

* **Address**             : `0x00041000 - 0x000410FF`

* **Formula**             : `0x00041000 +  8*de3id + de2id`

* **Where**               : 

    * `$de3id(0-23)`

    * `$de2id(0-6):`

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3]`|`rxds2e2aismask`| This bit is set to 1 to disable AIS downstream masking to RxDE1 into LOF status. 1: UnMask. 0: Mask.| `RW`| `0x0`| `0x0`|
|`[2]`|`rxds2e2frclof`| This bit is set to 1 to force the Rx DS3/E3 framer into LOF tus. 1: force LOF. 0: not force LOF.| `RW`| `0x0`| `0x0`|
|`[1:0]`|`rxds2e2md`| Rx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2 G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH RxM12E12 Per STS/VC Payload Control

* **Description**           

The STS configuration registers are used to configure kinds of STS-1s and the kinds of VT/TUs in each STS-1.


* **RTL Instant Name**    : `rxm12e12_stsvc_payload_ctrl`

* **Address**             : `0x00041300 - 0x0004131F`

* **Formula**             : `0x00041300 +  stsid`

* **Where**               : 

    * `$stsid(0-23):`

* **Width**               : `14`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13:12]`|`pdhvtrxmaptug26type`| Define types of VT/TUs in TUG-2-6. 0: TU11/VT1.5 type 1: TU12/VT2 type 2: reserved 3: reserved| `RW`| `0x0`| `0x0`|
|`[11:10]`|`pdhvtrxmaptug25type`| Define types of VT/TUs in TUG-2-5| `RW`| `0x0`| `0x0`|
|`[9:8]`|`pdhvtrxmaptug24type`| Define types of VT/TUs in TUG-2-4| `RW`| `0x0`| `0x0`|
|`[7:6]`|`pdhvtrxmaptug23type`| Define types of VT/TUs in TUG-2-3| `RW`| `0x0`| `0x0`|
|`[5:4]`|`pdhvtrxmaptug22type`| Define types of VT/TUs in TUG-2-2| `RW`| `0x0`| `0x0`|
|`[3:2]`|`pdhvtrxmaptug21type`| Define types of VT/TUs in TUG-2-1| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pdhvtrxmaptug20type`| Define types of VT/TUs in TUG-2-0| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `rxm12e12_hw_stat`

* **Address**             : `0x00041200 - 0x000412FE`

* **Formula**             : `0x00041200 +  8*de3id + de2id`

* **Where**               : 

    * `$de3id(0-23)`

    * `$de2id(0-6):`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`rxm12e12status`| for HW debug only| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Framer Channel Interrupt Enable Control

* **Description**           

This is the Channel interrupt enable of DS2/E2 Rx Framer.


* **RTL Instant Name**    : `RxM12E12_chn_intr_cfg`

* **Address**             : `0x000413FE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `12`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6:0]`|`rxm12e12intren`| Each bit correspond with each channel DS2/E2 in DS3/E3 0: Disable 1: Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Framer Channel Interrupt Status

* **Description**           

This is the Channel interrupt tus of DS2/E2 Rx Framer.


* **RTL Instant Name**    : `RxM12E12_chn_intr_stat`

* **Address**             : `0x000413FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `7`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6:0]`|`rxm12e12intrsta`| Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Framer per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable of DS2/E2 Rx Framer.


* **RTL Instant Name**    : `RxM12E12_per_chn_intr_cfg`

* **Address**             : `0x00041380-0x0004139F`

* **Formula**             : `0x00041380 +  de3id`

* **Where**               : 

    * `$de3id(0-23):`

* **Width**               : `7`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6:0]`|`de2lofintren`| Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Framer per Channel Interrupt Change Status

* **Description**           

This is the per Channel interrupt status of DS2/E2 Rx Framer.


* **RTL Instant Name**    : `RxM12E12_per_chn_intr_stat`

* **Address**             : `0x000413A0-0x000413BF`

* **Formula**             : `0x000413A0 +  de3id`

* **Where**               : 

    * `$de3id(0-23):`

* **Width**               : `7`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6:0]`|`de2lofintr`| Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Framer per Channel Current Status

* **Description**           

This is the per Channel Current status of DS2/E2 Rx Framer.


* **RTL Instant Name**    : `RxM12E12_per_chn_curr_stat`

* **Address**             : `0x000413C0-0x000413DF`

* **Formula**             : `0x000413C0 +  de3id`

* **Where**               : 

    * `$de3id(0-23):`

* **Width**               : `7`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6:0]`|`de2lofcurrsta`| Each bit correspond with each channel DS2/E2 in DS3/E3 0: No Interrupt 1: Have Interrupt| `RW`| `0x0`| `0x0 End: Begin:`|

###RxM12E12 Global Control

* **Description**           

This is the Channel interrupt enable of DS2/E2 Rx Framer.


* **RTL Instant Name**    : `RxM12E12_glb_cfg`

* **Address**             : `0x00041340`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `12`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`rxm12e12nobmode`| Number of bit mode for DCR Tx Shapper 0: Bit mode 1: Byte mode| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer FBE Threshold Control

* **Description**           

This is the FBE threshold configuration register for the PDH DS1/E1/J1 Rx framer


* **RTL Instant Name**    : `dej1_fbe_thrsh_cfg`

* **Address**             : `0x00050003 - 0x00050003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`rxde1fbethres`| Threshold to generate interrupt if the FBE counter exceed this threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer CRC Threshold Control

* **Description**           

This is the CRC threshold configuration register for the PDH DS1/E1/J1 Rx framer


* **RTL Instant Name**    : `dej1_rx_crc_thrsh_cfg`

* **Address**             : `0x00050004 - 0x00050004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`rxde1crcthres`| Threshold to generate interrupt if the CRC counter exceed this threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer REI Threshold Control

* **Description**           

This is the REI threshold configuration register for the PDH DS1/E1/J1 Rx framer


* **RTL Instant Name**    : `dej1_rei_thrsh_cfg`

* **Address**             : `0x00050005 - 0x00050005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`rxde1reithres`| Threshold to generate interrupt if the FBE counter exceed this threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer Control

* **Description**           

DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer.


* **RTL Instant Name**    : `dej1_rx_framer_ctrl`

* **Address**             : `0x00054000 - 0x000543FF`

* **Formula**             : `0x00054000 +  32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$de3id(0-23)`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`rxde1loffrwdis`| Forward DE1 LOF to generate L-bit to PSN in case of MonOnly, set 1 to disable| `RW`| `0x0`| `0x0`|
|`[30]`|`rxde1frcaislbit`| Force AIS alarm here,L-bit, data all one to PSN when Line/Payload Remote Loopback| `RW`| `0x0`| `0x0`|
|`[29]`|`rxde1frcaislinedwn`| Set 1 to force AIS Line DownStream| `RW`| `0x0`| `0x0`|
|`[28]`|`rxde1frcaisplddwn`| Set 1 to force AIS payload DownStream| `RW`| `0x0`| `0x0`|
|`[27]`|`rxde1frclos`| Set 1 to force LOS| `RW`| `0x0`| `0x0`|
|`[26]`|`rxde1lomfdstren`| Set 1 to generate AIS forwarding dowstream in case E1CRC LOMF defect is present.| `RW`| `0x0`| `0x0`|
|`[25:23]`|`rxde1loscntthres`| Threshold to detect LOS all one, Set 0 to disable.| `RW`| `0x0`| `0x0`|
|`[22]`|`rxde1mononly`| Set 1 to enable the Monitor Only mode at the RXDS1/E1 framer| `RW`| `0x0`| `0x0`|
|`[21]`|`rxde1locallineloop`| Set 1 to enable Local Line loop back| `RW`| `0x0`| `0x0`|
|`[20]`|`rxde1localpayloop`| Set 1 to enable Local Payload loop back. Sharing for VT Loopback in case of CEP path| `RW`| `0x0`| `0x0`|
|`[19:17]`|`rxde1e1sacfg`| Incase E1 Sa used for FDL configuration 0x0: No Sa bit used 0x3: Sa4 is used for SSM 0x4: Sa5 is used for SSM 0x5: Sa6 is used for SSM 0x6: Sa7 is used for SSM 0x7: Sa8 is used for SSM Incase DS1 bit[19:18] dont care bit [17]   set " 1" to enable Rx DLK| `RW`| `0x0`| `0x0`|
|`[16:13]`|`rxde1lofthres`| Threshold for FAS error to declare LOF in DS1/E1/J1 mode| `RW`| `0x0`| `0x0`|
|`[12:10]`|`rxde1aiscntthres`| Threshold to detect AIS all one, Set 0 to disable| `RW`| `0x0`| `0x0`|
|`[9:7]`|`rxde1raicntthres`| Threshold of RAI monitoring block| `RW`| `0x0`| `0x0`|
|`[6]`|`rxde1aismasken`| Set 1 to enable output data of the DS1/E1/J1 framer to be set to all one (1) during LOF| `RW`| `0x0`| `0x0`|
|`[5]`|`rxde1en`| Set 1 to enable the DS1/E1/J1 framer.| `RW`| `0x0`| `0x0`|
|`[4]`|`rxde1frclof`| Set 1 to force Re-frame| `RW`| `0x0`| `0x0`|
|`[3:0]`|`rxds1md`| Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF (D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1 Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer HW Status

* **Description**           

These registers are used for Hardware tus only


* **RTL Instant Name**    : `dej1_rx_framer_hw_stat`

* **Address**             : `0x00054400-0x000547FF`

* **Formula**             : `0x00054400 +  32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$de3id(0-23)`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `7`
* **Register Type**       : `Stat`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`rxds1e1_crraisdownstr`| AIS Down Stream due to HI_Level AIS of DS1E1| `RW`| `0x0`| `0x0`|
|`[5]`|`rxds1e1_crrlosallzero`| LOS All zero of DS1E1| `RW`| `0x0`| `0x0`|
|`[4]`|`rxds1e1_crraisallone`| AIS All one of DS1E1| `RW`| `0x0`| `0x0`|
|`[3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2:0]`|`rxde1sta`| RxDE1Sta 000: Search 001: Shift 010: Wait 011: Verify 100: Fs search 101: Inframe| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer CRC Error Counter

* **Description**           

This is the per channel CRC error counter for DS1/E1/J1 receive framer


* **RTL Instant Name**    : `dej1_rx_framer_crcerr_cnt`

* **Address**             : `0x00056800 - 0x00056FFF`

* **Formula**             : `0x00056800 +  1024*Rd2Clr + 32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only`

    * `$de3id(0-23):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `8`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`de1crcerrcnt`| DS1/E1/J1 CRC Error Accumulator| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer REI Counter

* **Description**           

This is the per channel REI counter for DS1/E1/J1 receive framer


* **RTL Instant Name**    : `dej1_rx_framer_rei_cnt`

* **Address**             : `0x00057000 - 0x000577FF`

* **Formula**             : `0x00057000 +  1024*Rd2Clr + 32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only`

    * `$de3id(0-23):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `8`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`de1reicnt`| DS1/E1/J1 REI error accumulator| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer FBE Counter

* **Description**           

This is the per channel REI counter for DS1/E1/J1 receive framer


* **RTL Instant Name**    : `dej1_rx_framer_fbe_cnt`

* **Address**             : `0x00056000-0x000567FF`

* **Formula**             : `0x00056000 +  1024*Rd2Clr + 32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$Rd2Clr(0-1): Rd2Clr = 0: Read to clear. Otherwise, Read Only`

    * `$de3id(0-23):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `8`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`de1fbecnt`| DS1/E1/J1 F-bit error accumulator| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable of DS1/E1/J1 Rx Framer.


* **RTL Instant Name**    : `dej1_rx_framer_per_chn_intr_en_ctrl`

* **Address**             : `0x00055000-0x000553FF`

* **Formula**             : `0x00055000 +  StsID*32 + Tug2ID*4 + VtnID`

* **Where**               : 

    * `$StsID(0-23): STS-1/VC-3 ID`

    * `$Tug2ID(0-6): TUG-2/VTG ID`

    * `$VtnID(0-3): VT/TU number ID in the TUG-2/VTG`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13]`|`de1bersdintren`| Set 1 to enable the BER-SD interrupt.| `RW`| `0x0`| `0x0`|
|`[12]`|`de1bersfintren`| Set 1 to enable the BER-SF interrupt.| `RW`| `0x0`| `0x0`|
|`[11]`|`de1oblcintren`| Set 1 to enable the new Outband LoopCode detected to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[10]`|`de1iblcintren`| Set 1 to enable the new Inband LoopCode detected to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[9]`|`de1sigraiintren`| Set 1 to enable Signalling RAI event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[8]`|`de1siglofintren`| Set 1 to enable Signalling LOF event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[7]`|`de1fbeintren`| Set 1 to enable change FBE te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[6]`|`de1crcintren`| Set 1 to enable change CRC te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[5]`|`de1reiintren`| Set 1 to enable change REI te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[4]`|`de1lomfintren`| Set 1 to enable change LOMF te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[3]`|`de1losintren`| Set 1 to enable change LOS te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[2]`|`de1aisintren`| Set 1 to enable change AIS te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[1]`|`de1raiintren`| Set 1 to enable change RAI te event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[0]`|`de1lofintren`| Set 1 to enable change LOF te event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer per Channel Interrupt Status

* **Description**           

This is the per Channel interrupt tus of DS1/E1/J1 Rx Framer.


* **RTL Instant Name**    : `dej1_rx_framer_per_chn_intr_stat`

* **Address**             : `0x00055400-0x000557FF`

* **Formula**             : `0x00055400 +  StsID*32 + Tug2ID*4 + VtnID`

* **Where**               : 

    * `$StsID(0-23): STS-1/VC-3 ID`

    * `$Tug2ID(0-6): TUG-2/VTG ID`

    * `$VtnID(0-3): VT/TU number ID in the TUG-2/VTG`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13]`|`de1bersdintr`| Set 1 if there is a change of BER-SD.| `RW`| `0x0`| `0x0`|
|`[12]`|`de1bersfintr`| Set 1 if there is a change of BER-SF.| `RW`| `0x0`| `0x0`|
|`[11]`|`de1oblcintr`| Set 1 if there is a change the new Outband LoopCode detected to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[10]`|`de1iblcintr`| Set 1 if there is a change the new Inband LoopCode detected to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[9]`|`de1sigraiintr`| Set 1 if there is a change Signalling RAI event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[8]`|`de1siglofintr`| Set 1 if there is a change Signalling LOF event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[7]`|`de1fbeintr`| Set 1 if there is a change in FBE exceed threshold te event.| `RW`| `0x0`| `0x0`|
|`[6]`|`de1crcintr`| Set 1 if there is a change in CRC exceed threshold te event.| `RW`| `0x0`| `0x0`|
|`[5]`|`de1reiintr`| Set 1 if there is a change in REI exceed threshold te event.| `RW`| `0x0`| `0x0`|
|`[4]`|`de1lomfintr`| Set 1 if there is a change in LOMF te event.| `RW`| `0x0`| `0x0`|
|`[3]`|`de1losintr`| Set 1 if there is a change in LOS te event.| `RW`| `0x0`| `0x0`|
|`[2]`|`de1aisintr`| Set 1 if there is a change in AIS te event.| `RW`| `0x0`| `0x0`|
|`[1]`|`de1raiintr`| Set 1 if there is a change in RAI te event.| `RW`| `0x0`| `0x0`|
|`[0]`|`de1lofintr`| Set 1 if there is a change in LOF te event.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer per Channel Current Status

* **Description**           

This is the per Channel Current tus of DS1/E1/J1 Rx Framer.


* **RTL Instant Name**    : `dej1_rx_framer_per_chn_curr_stat`

* **Address**             : `0x00055800-0x00055BFF`

* **Formula**             : `0x00055800 +  StsID*32 + Tug2ID*4 + VtnID`

* **Where**               : 

    * `$StsID(0-23): STS-1/VC-3 ID`

    * `$Tug2ID(0-6): TUG-2/VTG ID`

    * `$VtnID(0-3): VT/TU number ID in the TUG-2/VTG`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13]`|`de1bersdintr`| Set 1 if there is a change of BER-SD.| `RW`| `0x0`| `0x0`|
|`[12]`|`de1bersfintr`| Set 1 if there is a change of BER-SF.| `RW`| `0x0`| `0x0`|
|`[11]`|`de1oblcintrsta`| Current status the new Outband LoopCode detected to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[10]`|`de1iblcintrsta`| Current status the new Inband LoopCode detected to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[9]`|`de1sigraiintrsta`| Current status Signalling RAI event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[8]`|`de1siglofintrsta`| Current status Signalling LOF event to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[7]`|`de1fbecurrsta`| Current tus of FBE exceed threshold event.| `RW`| `0x0`| `0x0`|
|`[6]`|`de1crccurrsta`| Current tus of CRC exceed threshold event.| `RW`| `0x0`| `0x0`|
|`[5]`|`de1reicurrsta`| Current tus of REI exceed threshold event.| `RW`| `0x0`| `0x0`|
|`[4]`|`de1lomfcurrsta`| Current tus of LOMF event.| `RW`| `0x0`| `0x0`|
|`[3]`|`de1loscurrsta`| Current tus of LOS event.| `RW`| `0x0`| `0x0`|
|`[2]`|`de1aiscurrsta`| Current tus of AIS event.| `RW`| `0x0`| `0x0`|
|`[1]`|`de1raicurrsta`| Current tus of RAI event.| `RW`| `0x0`| `0x0`|
|`[0]`|`de1lofcurrsta`| Current tus of LOF event.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer per Channel Interrupt OR Status

* **Description**           

The register consists of 28 bits for 28 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1.


* **RTL Instant Name**    : `dej1_rx_framer_per_chn_intr_or_stat`

* **Address**             : `0x00055C00-0x00055C1F`

* **Formula**             : `0x00055C00 +  StsID`

* **Where**               : 

    * `$StsID(0-23): STS-1/VC-3 ID`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`rxde1vtintrorsta`| Set to 1 if any interrupt tus bit of corresponding DS1/E1/J1 is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer per STS/VC Interrupt OR Status

* **Description**           

The register consists of 12 bits for 12 STS/VCs of the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related STS/VC.


* **RTL Instant Name**    : `dej1_rx_framer_per_stsvc_intr_or_stat`

* **Address**             : `0x00055FFF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `24`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:0]`|`rxde1stsintrorsta`| Set to 1 if any interrupt tus bit of corresponding STS/VC is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer per STS/VC Interrupt Enable Control

* **Description**           

The register consists of 12 interrupt enable bits for 12 STS/VCs in the Rx DS1/E1/J1 Framer.


* **RTL Instant Name**    : `dej1_rx_framer_per_stsvc_intr_en_ctrl`

* **Address**             : `0x00055FFE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:0]`|`rxde1stsintren`| Set to 1 to enable the related STS/VC to generate interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer DLK Control

* **Description**           

The register is used to configuration for  Rx DS1/E1/J1 Framer DLK.


* **RTL Instant Name**    : `dej1_rx_framer_dlk_ctrl`

* **Address**             : `0x000D8000 -  0x000D82F7`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16:11]`|`rxde1dlkstkthres`| | `RW`| `0x0`| `0x0`|
|`[10:6]`|`rxde1dlkbomthres`| Threshold to generate interrupt if the BOM counter exceed this threshold| `RW`| `0x0`| `0x0`|
|`[5]`|`rxde1dlkfcsinven`| Set 1 to enable FCS invert.| `RW`| `0x0`| `0x0`|
|`[4]`|`rxde1dlkpayinven`| Set 1 to enable payload invert.| `RW`| `0x0`| `0x0`|
|`[3]`|`rxde1dlkfcsen`| reserved| `RW`| `0x0`| `0x0`|
|`[2]`|`rxde1dlkbytestuff`| reserved| `RW`| `0x0`| `0x0`|
|`[1]`|`rxde1dlkmsblsb`| set 1 to enable MSB and LSB convert| `RW`| `0x0`| `0x0`|
|`[0]`|`rxde1dlken`| Set 1 to enable monitor DS1/E1/J1 Rx DLK| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer DLK Status

* **Description**           

The register is used to configuration for  Rx DS1/E1/J1 Framer DLK. For HW use only


* **RTL Instant Name**    : `dej1_rx_framer_dlk_stat`

* **Address**             : `0x000D9000 -  0x000D92F7`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17]`|`rxde1dlknewbomcnt`| | `RW`| `0x0`| `0x0`|
|`[16]`|`rxde1dlknewcachebytecnt`| | `RW`| `0x0`| `0x0`|
|`[15]`|`rxde1dlknewdatastoreen`| | `RW`| `0x0`| `0x0`|
|`[14]`|`rxde1dlknewdatastore`| | `RW`| `0x0`| `0x0`|
|`[13]`|`rxde1dlknewcachefill`| | `RW`| `0x0`| `0x0`|
|`[12]`|`rxde1dlknewmsgcnt`| | `RW`| `0x0`| `0x0`|
|`[11]`|`rxde1dlknewrdadd`| | `RW`| `0x0`| `0x0`|
|`[10]`|`rxde1dlknewwradd`| | `RW`| `0x0`| `0x0`|
|`[9]`|`rxde1dlknewfcs`| | `RW`| `0x0`| `0x0`|
|`[8]`|`rxde1dlknewmsgshift`| | `RW`| `0x0`| `0x0`|
|`[7]`|`rxde1dlknewbitcnt`| | `RW`| `0x0`| `0x0`|
|`[6]`|`rxde1dlknewstuffcnt`| | `RW`| `0x0`| `0x0`|
|`[5]`|`rxde1dlknewflagoff`| | `RW`| `0x0`| `0x0`|
|`[4]`|`rxde1dlknewbyte3store`| | `RW`| `0x0`| `0x0`|
|`[3]`|`rxde1dlknewbyte2store`| | `RW`| `0x0`| `0x0`|
|`[2]`|`rxde1dlknewbyte1store`| | `RW`| `0x0`| `0x0`|
|`[1]`|`rxde1dlknewbyteshf`| | `RW`| `0x0`| `0x0`|
|`[0]`|`rxde1dlknewstate`| | `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer DLK Flush Message

* **Description**           

This register is used to flush all messages of a channel.


* **RTL Instant Name**    : `dej1_rx_framer_dlk_flush_msg_ctrl`

* **Address**             : `0x000D0011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `12`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[10]`|`rxde1dlkflushen`| Set 1 to enable flush buffer.| `RW`| `0x0`| `0x0`|
|`[9:0]`|`rxde1dlkchid`| Channel DLK which is flushed.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer DLK Access DDR Control

* **Description**           

.


* **RTL Instant Name**    : `dej1_framer_clk_acc_ddr_ctrl`

* **Address**             : `0x000D0010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16]`|`rxde1dlkddrready`| | `RW`| `0x0`| `0x0`|
|`[15:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9:0]`|`rxde1dlkchid`| Channel DLK which is read.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer DLK Message0

* **Description**           

.


* **RTL Instant Name**    : `dej1_rx_framer_clk_msg0`

* **Address**             : `0x000D0016`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[26:24]`|`rxde1dlkcache1`| | `RW`| `0x0`| `0x0`|
|`[23:16]`|`rxde1dlkbyte1`| | `RW`| `0x0`| `0x0`|
|`[10:8]`|`rxde1dlkcache0`| | `RW`| `0x0`| `0x0`|
|`[7:0]`|`rxde1dlkbyte0`| | `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer DLK Message1

* **Description**           

.


* **RTL Instant Name**    : `dej1_rx_framer_msg1`

* **Address**             : `0x000D0017`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[26:24]`|`rxde1dlkcache3`| | `RW`| `0x0`| `0x0`|
|`[23:16]`|`rxde1dlkbyte3`| | `RW`| `0x0`| `0x0`|
|`[10:8]`|`rxde1dlkcache2`| | `RW`| `0x0`| `0x0`|
|`[7:0]`|`rxde1dlkbyte2`| | `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer DLK Message2

* **Description**           

.


* **RTL Instant Name**    : `dej1_rx_framer_dkl_msg2`

* **Address**             : `0x000D0018`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[26:24]`|`rxde1dlkcache5`| | `RW`| `0x0`| `0x0`|
|`[23:16]`|`rxde1dlkbyte5`| | `RW`| `0x0`| `0x0`|
|`[10:8]`|`rxde1dlkcache4`| | `RW`| `0x0`| `0x0`|
|`[7:0]`|`rxde1dlkbyte4`| | `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer DLK Message3

* **Description**           

.


* **RTL Instant Name**    : `dej1_rx_framer_dlk_msg3`

* **Address**             : `0x000D0019`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[26:24]`|`rxde1dlkcache7`| | `RW`| `0x0`| `0x0`|
|`[23:16]`|`rxde1dlkbyte7`| | `RW`| `0x0`| `0x0`|
|`[10:8]`|`rxde1dlkcache6`| | `RW`| `0x0`| `0x0`|
|`[7:0]`|`rxde1dlkbyte6`| | `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer DLK Interrupt Enable

* **Description**           

.


* **RTL Instant Name**    : `dej1_rx_framer_dlk_intr_en_ctrl`

* **Address**             : `0x000DE000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `5`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[4]`|`rxde1dlkbomen`| Set 1 if there is a the new BOM detected| `RW`| `0x0`| `0x0`|
|`[3]`|`rxde1dlkcacheerren`| | `RW`| `0x0`| `0x0`|
|`[2]`|`rxde1dlkaborten`| | `RW`| `0x0`| `0x0`|
|`[1]`|`rxde1dlkoverthresen`| | `RW`| `0x0`| `0x0`|
|`[0]`|`rxde1dlknewmsgen`| | `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Rx Framer DLK Interrupt Sticky

* **Description**           

.


* **RTL Instant Name**    : `dej1_rx_framer_dlk_intr_sticky`

* **Address**             : `0x000DE400`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `5`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[4]`|`rxde1dlkbomintr`| Set 1 if there is a the new BOM detected| `RW`| `0x0`| `0x0`|
|`[3]`|`rxde1dlkcacheerrintr`| Set 1 if there is a the new Cache Error detected| `RW`| `0x0`| `0x0`|
|`[2]`|`rxde1dlkabortintr`| Set 1 if there is a the new Abort detected| `RW`| `0x0`| `0x0`|
|`[1]`|`rxde1dlkoverthresintr`| Set 1 if there is a the new Over Threshold detected| `RW`| `0x0`| `0x0`|
|`[0]`|`rxde1dlknewmsgintr`| Set 1 if there is a the new message detected| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Global Control

* **Description**           

This register is used to global configuration for PTM block.


* **RTL Instant Name**    : `dej1_testpat_mon_glb_ctrl`

* **Address**             : `0x054900 - 0x054900`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `12`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11:8]`|`rxde3ptmlosthr`| Threshold for Loss Of Sync detection. Status will go LOS if the number of error patterns (8 bits) in 16 consecutive patterns is more than or equal the RxDE3PtmLosThr.| `RW`| `0x0`| `0x0`|
|`[7:4]`|`rxde3ptmsynthr`| Threshold for Sync detection. Status will go SYN if the number of corrected consecutive patterns (8 bits) is more than or equal the RxDE3PtmLosThr.| `RW`| `0x0`| `0x0`|
|`[1]`|`rxde1ptmswap`| PTM swap Bit. If it is set to 1, pattern is shifted LSB first, then MSB. Normal is set to 0.| `RW`| `0x0`| `0x0`|
|`[0]`|`rxde1ptminv`| PTM inversion Bit.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Sticky Enable

* **Description**           

This register is used to configure the test pattern sticky mode


* **RTL Instant Name**    : `dej1_testpat_mon_sticky_en`

* **Address**             : `0x054901 - 0x054901`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxde1ptmstken`| Test Pattern Error Sticky Control| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Sticky

* **Description**           

This register is used to be tus sticky report of 4 PTM engines.


* **RTL Instant Name**    : `dej1_testpat_mon_sticky`

* **Address**             : `0x054902 - 0x054902`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxde1ptmstk`| BERT Error Sticky| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Mode

* **Description**           

This register is used to data mode for PTM block.


* **RTL Instant Name**    : `dej1_testpat_mon_mode`

* **Address**             : `0x054910 - 0x05491F`

* **Formula**             : `0x054910 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9:5]`|`rxde1ptmpatbit`| The number of bit pattern is used.| `RW`| `0x0`| `0x0`|
|`[4:0]`|`rxde1ptmmode`| Receive DS1/E1/J1 BERT mode 00000: Prbs9 00001: Prbs11 00010: Prbs15 00011: Prbs20 (X19 + X2 + 1) 00100: Prbs20 (X19 + X16 + 1) 00101: Qrss20 (X19 + X16 + 1) 00110: Prbs23 00111: DDS1 01000: DDS2 01001: DDS3 01010: DDS4 01011: DDS5 01100: Daly55 01101: Fix 3 in 24 01110: Fix 1 in 8 01111: Octet55 10000: Fix pattern n-bit 10001: Sequence 10010: Prbs7| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Fixed Pattern

* **Description**           

This register is used to be expected data in fix pattern mode for engine #0 - 15


* **RTL Instant Name**    : `dej1_testpat_mon_fixed_pattern_cfg`

* **Address**             : `0x054920 - 0x05492F`

* **Formula**             : `0x054920 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxde1fixpat`| Configurable fixed pattern for BERT monitoring.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Selected Channel

* **Description**           

This register is used to control configuration for PTM engine #0 - 15


* **RTL Instant Name**    : `dej1_testpat_mon_sel_chn_cfg`

* **Address**             : `0x054800 - 0x05480F`

* **Formula**             : `0x054800 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9]`|`rxde1ptgen`| PTG Bit Enable.| `RW`| `0x0`| `0x0`|
|`[8:0]`|`rxde1ptmid`| Configure which channel in 336 DS1s/E1s/J1s is monitored.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring nxDS0 Control

* **Description**           

These registers are used to control the mode of nxDS0 in a DS1/E1/J1


* **RTL Instant Name**    : `dej1_testpat_mon_nxds0_ctrl`

* **Address**             : `0x054810 - 0x05481F`

* **Formula**             : `0x054810 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `3`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2]`|`rxde1ptmfbitovr`| F- bit overwrite| `RW`| `0x0`| `0x0`|
|`[1]`|`rxde1ptmds0_6b`| Receive Test Pattern from the 2nd bit to the 8th bit| `RW`| `0x0`| `0x0`|
|`[0]`|`rxde1ptmds0_7b`| Receive Test Pattern from the 2nd bit to the 7h bit. If DS0_7b and DS0_6b are not active, engine will receive test pattern from the 1st bit to the 8th bit| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring nxDS0 Concatenation Control

* **Description**           

This register is used to be active DSO for PTG engine: 0-3. A 32 bits register is dedicated for 32 timeslot in case of frame E1, and 24 bits (bit 24 - 1) is given for 24 timeslot in case of frame DS1/J1. The 32-bits are set 1 to indicate full channel extraction.


* **RTL Instant Name**    : `dej1_testpat_mon_nxds0_conc_ctrl`

* **Address**             : `0x054820 - 0x05482F`

* **Formula**             : `0x054820 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxde1ptgds0conctrl`| Per time slot receive DS1/E1/J1 Concatenation enable.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Error Counter

* **Description**           

This is the error counter report of PTM engine #0 - 15 during monitor period.


* **RTL Instant Name**    : `dej1_testpat_mon_err_cnt`

* **Address**             : `0x054960 - 0x05497F`

* **Formula**             : `0x054960 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxde1ptmerrcnt`| Error counter for BERT monitoring.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Good Bit Counter

* **Description**           

This is the good counter report of PTM engine #0 - 15 during monitor period.


* **RTL Instant Name**    : `dej1_testpat_mon_goodbit_cnt`

* **Address**             : `0x054980 - 0x05499F`

* **Formula**             : `0x054980 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxde1ptmgoodcnt`| Good counter for BERT monitoring.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Loss Bit Counter

* **Description**           

This is the loss counter report of PTM engine #0 - 16 during monitor period.


* **RTL Instant Name**    : `dej1_testpat_mon_lossbit_cnt`

* **Address**             : `0x0549A0 - 0x0549BF`

* **Formula**             : `0x0549A0 +  index`

* **Where**               : 

    * `$index(0-23):`

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxde1ptmlosscnt`| Loss counter for BERT monitoring.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Counter Load ID

* **Description**           

This register is used to load the error/good/loss counter report of PTM engine: 0-15 which the channel ID is value of register.


* **RTL Instant Name**    : `dej1_testpat_mon_cntid_cfg`

* **Address**             : `0x054903 - 0x054903`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3:0]`|`rxde1ptmloadid`| Load ID counter.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Error-Bit Counter Loading

* **Description**           

This is the loaded error counter report of PTM.


* **RTL Instant Name**    : `dej1_testpat_mon_errbit_cnt`

* **Address**             : `0x054904 - 0x649B04`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxde1ptmloaderrcnt`| Load Error counter.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Good-Bit Counter Loading

* **Description**           

This is the loaded good counter report of PTM.


* **RTL Instant Name**    : `dej1_testpat_mon_goodbit_cnt_loading`

* **Address**             : `0x054905 - 0x649B05`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxde1ptmloadgoodcnt`| Load Good counter.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Lost-Bit Counter Loading

* **Description**           

This is the loaded loss counter report of PTM.


* **RTL Instant Name**    : `dej1_testpat_mon_lostbit_cnt`

* **Address**             : `0x054906 - 0x649B06`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rxde1ptmloadlosscnt`| Load Loss counter.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Timing Status

* **Description**           

These registers are used for Hardware tus only


* **RTL Instant Name**    : `dej1_testpat_mon_timing_stat`

* **Address**             : `0x054830 - 0x05483F`

* **Formula**             : `0x054830 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `10`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9:0]`|`status`| This field is used for Hw tus only.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Monitoring Status

* **Description**           

These registers are used for Hardware tus only


* **RTL Instant Name**    : `dej1_testpat_mon_stat`

* **Address**             : `0x054940 - 0x05495F`

* **Formula**             : `0x054940 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `48`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[36:0]`|`status`| This field is used for Hw tus only.| `RW`| `0x0`| `0x0 End: Begin:`|

###SPE/VT Map Fifo Length Water Mark

* **Description**           

This register is used for Hardware tus only.


* **RTL Instant Name**    : `spevt_map_fflen_watermask`

* **Address**             : `0x00070002 - 0x00070002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:0]`|`stsvtfifolenmark`| For Hardware use only| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Map Control

* **Description**           

The STS/VT Map Control is use to configure for per channel STS/VT Map operation.


* **RTL Instant Name**    : `stsvt_map_ctrl`

* **Address**             : `0x00076000 - 0x000763FF`

* **Formula**             : `0x00076000 +  32*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$stsid(0-23):`

    * `$vtgid(0-6):`

    * `$vtid(0-3):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19]`|`stsvtmapaismask`| Set 1 to mask AIS from PDH to OCN| `RW`| `0x0`| `0x0`|
|`[18:15]`|`stsvtmapjitidcfg`| STS/VT Mapping Jitter ID configure, default value is 0| `RW`| `0x0`| `0x0`|
|`[14:10]`|`stsvtmapstuffthres`| STS/VT Mapping Stuff Threshold Configure, default value is 12| `RW`| `0x0`| `0x0`|
|`[9:6]`|`stsvtmapjittercfg`| STS/VT Mapping Jitter configure - Vtmap: StsVtMapJitterCfg: Bit FiFo mode - 1'b1: Bit FiFo stuff mode: Only in case of ACR/DCR timing mode or Slaver mode from  ACR/DCR master timing ID - 1'b0: Byte FiFO stuff mode: default value StsVtMapJitterCfg: Master Enable. Only valid in Bit FiFo stuff_mode StsVtMapJitterCfg: AIS insert enable StsVtMapJitterCfg: Byte FiFo mode In case of Byte FiFo Stuff Mode: + 1'b1: Byte FiFo mode Enable: default value + 1'b0: Byte LoopTime FiFo mode Enable: only in case of Loop timing mode - Spe Map: 0001: old_stuff_mode 0010: holdover_en 0100: aisins_en 1001: fine_stuff_mode| `RW`| `0x0`| `0x0`|
|`[5]`|`stsvtmapcenterfifo`| This bit is set to 1 to force center the mapping FIFO.| `RW`| `0x0`| `0x0`|
|`[4]`|`stsvtmapbypass`| This bit is set to 1 to bypass the STS/VT map| `RW`| `0x0`| `0x0`|
|`[3:0]`|`stsvtmapmd`| STS/VT Mapping mode StsVtMapMd	StsVtMapBypass	Operation Mode 0	          0	E1 to VT2 Map 0	          1	E1 to Bus 1	          0	DS1 to VT1.5 Map 1	          1	DS1 to Bus 2	          0	E3 to VC3 Map 2	          1	E3 to TU3 Map 3	          0	DS3 to VC3 Map 3	          1	DS3 to TU3 Map 4	          x	VT2/TU12 Basic CEP 5	          x	Packet over STS1/VC3 6	          0	STS1/VC3 Fractional CEP carrying VT2/TU12 6	          1	STS1/VC3 Fractional CEP carrying VT15/TU11 7	          0	STS1/VC3 Fractional CEP carrying E3 7	          1	STS1/VC3 Fractional CEP carrying DS3 8	          x	STS1/VC3 Basic CEP 9	          x	Packet over STS3c/VC4 10	        0	STS3c/VC4 Fractional CEP carrying VT2/TU12 10	        1	STS3c/VC4 Fractional CEP carrying VT15/TU11 11	        0	STS3c/VC4 Fractional CEP carrying E3 11	        1	STS3c/VC4 Fractional CEP carrying DS3 12	        x	STS3c/VC4/STS12c/VC4_4c Basic CEP 13	        x	Packet over TU3 14	        0	E3 to Bus 14	        1	DS3 to Bus 15	        x	Disable STS/VC/VT/TU/DS1/E1/DS3/E3| `RW`| `0x0`| `0x0 End: Begin:`|

###VT Async Map Control

* **Description**           

The VT Async Map Control is use to configure for per channel VT Async Map operation.


* **RTL Instant Name**    : `vt_async_map_ctrl`

* **Address**             : `0x00076800 - 0x00076BFF`

* **Formula**             : `0x00076800 +  32*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$stsid(0-23)`

    * `$vtgid(0-6):`

    * `$vtid(0-3):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`bitfifoid`| Bit FiFo ID, related to ACR/DCR ID engine. Valid from 0 to 255| `RW`| `0x0`| `0x0`|
|`[23:0]`|`vtstuffthr`| The number of VT Multiframe (500us) threshold for VT async stuffing. Using for BitFiFo mode and ByteFiFo mode. Default value is 0| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Map FIFO Center Sticky

* **Description**           

These registers are used for Hardware tus only


* **RTL Instant Name**    : `stsvt_map_ff_center_sticky`

* **Address**             : `0x00070020 - 0x0007003B`

* **Formula**             : `0x00070020 +  index`

* **Where**               : 

    * `$index(0-23):`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`stsvtfifocenstk`| This field is used for Hw tus only.| `RW`| `0x0`| `0x0 End: Begin:`|

###STS/VT Map HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `stsvt_map_hw_stat`

* **Address**             : `0x00076400-0x000767FF`

* **Formula**             : `0x00076400 +  32*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$stsid(0-23):`

    * `$vtgid(0-6):`

    * `$vtid(0-3):`

* **Width**               : `48`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[35:0]`|`stsvtstatus`| for HW debug only| `RW`| `0x0`| `0x0 End: Begin:`|

###TS/VT Map Jitter Configuration

* **Description**           

This is the configuration for STS/VT Mapping.


* **RTL Instant Name**    : `ts_vt_map_jitter_cfg`

* **Address**             : `0x00070080`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `96`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[69:63]`|`swthreshold1`| | `RW`| `0x0`| `0x0`|
|`[62:56]`|`swthreshold2`| | `RW`| `0x0`| `0x0`|
|`[55:49]`|`swthreshold3`| | `RW`| `0x0`| `0x0`|
|`[48:42]`|`swthreshold4`| | `RW`| `0x0`| `0x0`|
|`[41:35]`|`swthreshold5`| | `RW`| `0x0`| `0x0`|
|`[34:28]`|`swcntmax1`| | `RW`| `0x0`| `0x0`|
|`[27:21]`|`swcntmax2`| | `RW`| `0x0`| `0x0`|
|`[20:14]`|`swcntmax3`| | `RW`| `0x0`| `0x0`|
|`[13:7]`|`swcntmax4`| | `RW`| `0x0`| `0x0`|
|`[6:0]`|`swcntmax5`| | `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer Control

* **Description**           

DS1/E1/J1 Rx framer control is used to configure for Frame mode (DS1, E1, J1) at the DS1/E1 framer.


* **RTL Instant Name**    : `dej1_tx_framer_ctrl`

* **Address**             : `0x00094000 - 0x000943FF`

* **Formula**             : `0x00094000 +  32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$de3id(0-23):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23]`|`txde1forceallone`| Force all one to TDM side when Line Local Loopback only side| `RW`| `0x0`| `0x0`|
|`[22]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[21]`|`txde1fbitbypass`| Set 1 to bypass Fbit insertion in Tx DS1/E1 Framer| `RW`| `0x0`| `0x0`|
|`[20]`|`txde1lineaisins`| Set 1 to enable Line AIS Insert.This bit force all one to Line Local Loopback| `RW`| `0x0`| `0x0`|
|`[19]`|`txde1payaisins`| Set 1 to enable Payload AIS Insert| `RW`| `0x0`| `0x0`|
|`[18]`|`txde1rmtlineloop`| Set 1 to enable remote Line Loop back| `RW`| `0x0`| `0x0`|
|`[17]`|`txde1rmtpayloop`| Set 1 to enable remote Payload Loop back. Sharing for VT Loopback in case of CEP path| `RW`| `0x0`| `0x0`|
|`[16]`|`txde1autoais`| Set 1 to enable AIS indication from data map block to automatically transmit all 1s at DS1/E1/J1 Tx framer| `RW`| `0x0`| `0x0`|
|`[15:9]`|`txde1dlcfg`| Incase E1 mode : use for SSM in Sa, bit[15:13] is Sa position 0=disable, 3=Sa4, 4=Sa5, 5=Sa6, 6=Sa7,7=Sa8. Bit[12:9] is SSM value. Incase DS1 mode : bit[12] Set 1 to Enable FDL transmit , dont care other bits| `RW`| `0x0`| `0x0`|
|`[8]`|`txde1autoyel`| Auto Yellow generation enable 1: Yellow alarm detected from Rx Framer will be automatically transmitted in Tx Framer 0: No automatically Yellow alarm transmitted| `RW`| `0x0`| `0x0`|
|`[7]`|`txde1frcyel`| SW force to Tx Yellow alarm| `RW`| `0x0`| `0x0`|
|`[6]`|`txde1autocrcerr`| Auto CRC error enable 1: CRC Error detected from Rx Framer will be automatically transmitted in REI bit of Tx Framer 0: No automatically CRC Error alarm transmitted| `RW`| `0x0`| `0x0`|
|`[5]`|`txde1frccrcerr`| SW force to Tx CRC Error alarm (REI bit)| `RW`| `0x0`| `0x0`|
|`[4]`|`txde1en`| Set 1 to enable the DS1/E1/J1 framer.| `RW`| `0x0`| `0x0`|
|`[3:0]`|`rxde1md`| Receive DS1/J1 framing mode 0000: DS1/J1 Unframe 0001: DS1 SF (D4) 0010: DS1 ESF 0011: DS1 DDS 0100: DS1 SLC 0101: J1 SF 0110: J1 ESF 1000: E1 Unframe 1001: E1 Basic Frame 1010: E1 CRC4 Frame| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer Status

* **Description**           

These registers are used for Hardware tus only


* **RTL Instant Name**    : `dej1_tx_framer_stat`

* **Address**             : `0x00094400 - 0x000947FF`

* **Formula**             : `0x00094400 +  32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$de3id(0-23):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `3`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2:0]`|`rxde1sta`| 000: Search 001: Shift 010: Wait 011: Verify 100: Fs search 101: Inframe| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer Signaling Insertion Control

* **Description**           

DS1/E1/J1 Rx framer signaling insertion control is used to configure for signaling operation modes at the DS1/E1 framer.


* **RTL Instant Name**    : `dej1_tx_framer_sign_insertion_ctrl`

* **Address**             : `0x00094800 - 0x00094BFF`

* **Formula**             : `0x00094800 +  32*de3id + 4*de2id + de1id`

* **Where**               : 

    * `$de3id(0-23):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[30]`|`txde1sigmfrmen`| Set 1 to enable the Tx DS1/E1/J1 framer to sync the signaling multiframe to the incoming data flow. No applicable for DS1| `RW`| `0x0`| `0x0`|
|`[29:0]`|`txde1sigbypass`| 30 signaling bypass bit for 32 DS0 in an E1 frame. In DS1 mode, only 24 LSB bits is used. Set 1 to bypass Signaling insertion in Tx DS1/E1 Framer| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer Signaling ID Conversion Control

* **Description**           

DS1/E1/J1 Rx framer signaling ID conversion control is used to convert the PW ID to DS1/E1/J1 Line ID at the DS1/E1 framer.


* **RTL Instant Name**    : `dej1_tx_framer_sign_id_conv_ctrl`

* **Address**             : `0x000C2000 - 0x000C23FF`

* **Formula**             : `0x000C2000 +  pwid`

* **Where**               : 

    * `$pwid(0-1023):`

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[41]`|`txde1sigds1`| Set 1 if the PW carry DS0 for the DS1/J1, set 0 for E1| `RW`| `0x0`| `0x0`|
|`[40:32]`|`txde1siglineid`| Output DS1/E1/J1 Line ID of the conversion.| `RW`| `0x0`| `0x0`|
|`[31:0]`|`txde1sigenb`| 32 signaling enable bit for 32 DS0 in an E1 frame. In DS1 mode, only 24 LSB bits is used.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer Signaling CPU Insert Enable Control

* **Description**           

This is the CPU signaling insert global enable bit


* **RTL Instant Name**    : `dej1_tx_framer_sign_cpuins_en_ctrl`

* **Address**             : `0x000CFF01`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`txde1sigcpumd`| Set 1 to enable CPU mode (all signaling is from CPU)| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer Signaling CPU DS1/E1 signaling Write Control

* **Description**           

This is the CPU signaling write control


* **RTL Instant Name**    : `dej1_tx_framer_sign_cpu_de1_wr_ctrl`

* **Address**             : `0x000CFF02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`txde1sigcpuwrctrl`| Set 1 if signaling value written to the Signaling buffer in next CPU cylce is for DS1/J1, set 0 for E1| `RW`| `0x0`| `0x0 Begin:`|

###DS1/E1/J1 Tx Framer Signaling Buffer

* **Description**           

DS1/E1/J1 Rx framer signaling ABCD bit store for each DS0 at the DS1/E1 framer.


* **RTL Instant Name**    : `dej1_tx_framer_sign_buffer`

* **Address**             : `0x000C8000 - 0x000CFF00`

* **Formula**             : `0x000C8000 +  1024*de3id + 128*de2id + 32*de1id + tscnt`

* **Where**               : 

    * `$de3id(0-23):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

    * `$tscnt(0-23): (ds1) or 0 - 31 (e1)`

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3:0]`|`txde1sigabcd`| 4-bit ABCD signaling for each DS0 (Note: for DS1/J1, must write the 0x000C8002 value 1 before writing this ABCD, for E1  must write the 0x000C8002 value 0 before writing this ABCD)| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Generation Control

* **Description**           

This register is used to global configuration for PTG engines


* **RTL Instant Name**    : `dej1_testpat_gen_ctrl`

* **Address**             : `0x0950FF - 0x0950FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`txde1ptgswap`| | `RW`| `0x0`| `0x0`|
|`[0]`|`txde1ptginv`| PTG inversion Bit.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Generation Single Bit Error Insertion

* **Description**           

This register is used to insert a single erroneous bit to DS1/E1/J1 Test Pattern generator.


* **RTL Instant Name**    : `dej1_testpat_gen_single_biterr_ins`

* **Address**             : `0x0950FE - 0x0950FE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3:0]`|`txde1ptmerridins`| | `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Generation Mode

* **Description**           

This register is used to data mode for PTG block.


* **RTL Instant Name**    : `dej1_testpat_gen_mode`

* **Address**             : `0x095080 - 0x09508F`

* **Formula**             : `0x095080 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9:5]`|`txde1ptmpatbit`| The number of bit pattern is used.| `RW`| `0x0`| `0x0`|
|`[4:0]`|`txde1ptgmode`| 00000: Prbs9 00001: Prbs11 00010: Prbs15 00011: Prbs20 (X19 + X2 + 1) 00100: Prbs20 (X19 + X16 + 1) 00101: Qrss20 (X19 + X16 + 1) 00110: Prbs23 00111: DDS1 01000: DDS2 01001: DDS3 01010: DDS4 01011: DDS5 01100: Daly55 01101: Fix 3 in 24 01110: Fix 1 in 8 01111: Octet55 10000: Fix pattern n-bit 10001: Sequence 10010: Prbs7| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Generation Selected Channel

* **Description**           

This register is used to global configuration for PTG engine #0 - 15


* **RTL Instant Name**    : `dej1_testpat_gen_sel_chn`

* **Address**             : `0x095000 - 0x09500F`

* **Formula**             : `0x095000 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9]`|`txde1ptgen`| PTG Bit Enable.| `RW`| `0x0`| `0x0`|
|`[8:0]`|`txde1ptgid`| Configure which channel in 336 DS1s/E1s/J1s is inserted a test pattern.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Generation nxDS0 Control

* **Description**           

This register is used to configure the mode of nxDS0 in DS1/E1/J1 test pattern generator.


* **RTL Instant Name**    : `dej1_testpat_gen_nxds0_ctrl`

* **Address**             : `0x095010 - 0x09501F`

* **Formula**             : `0x095010 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`txde1ptgds0_6b`| Transmit Test Pattern from the 2nd bit to the 8th bit| `RW`| `0x0`| `0x0`|
|`[0]`|`txde1ptgds0_7b`| Transmit Test Pattern from the 2nd bit to the 7h bit. If DS0_7b and DS0_6b are not active, engine will transmit Test Pattern from the 1st bit to the 8th bit| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Generation nxDS0 Concatenation Control

* **Description**           

This register is used to be active bit at level DS0 or PTG engine 0. A 32 bits register is dedicated for 32 timeslot in case of frame E1, and 24 bits (bit 24 - 1) is given for 24 timeslot in case of frame DS1. The 32-bits are set 1 to indicate full channel insertion.


* **RTL Instant Name**    : `dej1_testpat_gen_nxds0_conc_ctrl`

* **Address**             : `0x095020 - 0x09502F`

* **Formula**             : `0x095020 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxde1fixpat`| Configurable fixed pattern for BERT monitoring| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Generation Fixed Pattern Control

* **Description**           

This register is used to be transmitted data in fix pattern mode for engine #0 - 15


* **RTL Instant Name**    : `dej1_testpat_gen_fixedpat_ctrl`

* **Address**             : `0x095090 - 0x09509F`

* **Formula**             : `0x095090 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxde1fixpat`| Configurable fixed pattern for BERT monitoring.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Generation Error Rate Insertion

* **Description**           

This register is used to control register for error insertion


* **RTL Instant Name**    : `dej1_testpat_gen_errrate_inst`

* **Address**             : `0x0950A0 - 0x0950AF`

* **Formula**             : `0x0950A0 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3]`|`txde1bererr`| BER enable insertion for engine n. Single bit error can occur during BER insertion.| `RW`| `0x0`| `0x0`|
|`[2:0]`|`txde1bermd`| Bit Error Rate inserted to Pattern Generator 000: BER 10-2 001: BER 10-3 010: BER 10-4 011: BER 10-5 100: BER 10-6 101: BER 10-7 11x: BER 10-7| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Generation Timing Status

* **Description**           

These registers are used for Hardware tus only


* **RTL Instant Name**    : `dej1_testpat_gen_timing_stat`

* **Address**             : `0x615030 - 0x65503F`

* **Formula**             : `0x615030 +  index`

* **Where**               : 

    * `$index(0-15):`

* **Width**               : `4`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3:0]`|`tus`| status| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Test Pattern Generation Status

* **Description**           

These registers are used for Hardware tus only


* **RTL Instant Name**    : `dej1_testpat_gen_stat`

* **Address**             : `0x0950C0 - 0x0950EF`

* **Formula**             : `0x0950C0 + 16*high + index`

* **Where**               : 

    * `$high(0-1):`

    * `$index(0-15):`

* **Width**               : `4`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3:0]`|`tus`| status| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer DLK Indirect Access Control

* **Description**           

The register holds read/write/address command to access registers that are required to read or write indirectly. %%

When writing a value into indirect registers, data needs to be written into the DLK Indirect Access Write Data Control register, address and command are then written into this register with 1 on bit DLKRdWrPend. Until hardware completes the write cycle, bit DLKRdWrPend is clear to 0.  %%

When reading indirect registers, address and command are written into this register with 1 on bit DLKRdWrPend. Until hardware completes the read cycle, bit DLKRdWrPend is clear to 0. Read data is ready on the DLK Indirect Access Read Data Status register.


* **RTL Instant Name**    : `dej1_tx_framer_dlk_indir_acc_ctrl`

* **Address**             : `0x00090008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`dlkrdwrpend`| Read/Write Pending 1: Read/Write has been pending 0: Read/Write is done| `RW`| `0x0`| `0x0`|
|`[12]`|`dlkwnr`| Write not Read. This bit is used to control the hardware to read or write an indirect register. 1: Read an indirect register 0: Write to an indirect register| `RW`| `0x0`| `0x0`|
|`[8:0]`|`dlkregadr`| Register Address| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer DLK Indirect Access Write Data Control

* **Description**           

The registers hold data value in order to write this value to indirect registers. This register is used in conjunction with the DLK Indirect Access Control register.


* **RTL Instant Name**    : `dej1_tx_framer_dlk_indir_acc_wr_data_ctrl`

* **Address**             : `0x00090009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`wrholddat`| Write Access Holding Data| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer DLK Indirect Access Read Data Status

* **Description**           

The registers hold data value after reading indirect registers. This register is used in conjunction with the DLK Indirect Access Control register.


* **RTL Instant Name**    : `dej1_tx_framer_dlk_indir_acc_rd_data_stat`

* **Address**             : `0x0009000A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`rdholddat`| Read Access Holding Data| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer DLK Insertion Control

* **Description**           

These registers are used to control DLK insertion engines.


* **RTL Instant Name**    : `dej1_tx_framer_dlk_ins_ctrl`

* **Address**             : `0x096020-0x09602F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:9]`|`dlkmsglength`| length of message to transmit in byte. The maximum length is 128 in byte.| `RW`| `0x0`| `0x0`|
|`[8:0]`|`dlkde1id`| To configure which DS1/E1 ID is selected to insert DLK. At a time, user can choose 16 DS1/E1s to carry DLK message.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer DLK Insertion Enable

* **Description**           

This register is used to configure enable or disable DLK insertion engines. This configuration is on per engine.


* **RTL Instant Name**    : `dej1_tx_framer_dlk_ins_en`

* **Address**             : `0x00096010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`dlkinsen`| 1: engine is ready to insert DLK 0: engine is disable to insert DLK.| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer DLK Insertion New Message Start Enable

* **Description**           

This register is used to configure DLK insertion engines to transmit a new message. By setting a bit in this register to 1, DLK engine (appropriate to that bit) will get message in the message memory to insert into the configured DS1/E1 frame. When transmitting finishes, engine automatically clears this bit to inform to CPU that the DLK message has been already transmitted.


* **RTL Instant Name**    : `dej1_tx_framer_dlk_ins_newmsg_start_en`

* **Address**             : `0x00096011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`dlknewmsgstarten`| DlkNewMsgStartEn| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer DLK Insertion Engine Status

* **Description**           

This register is used to inform to CPU the tus of DLK insertion engine. Before using an engine to transmit DLK for a DS1/E1 channel, CPU must read this register to get the status of that engin


* **RTL Instant Name**    : `dej1_tx_framer_dlk_ins_eng_stat`

* **Address**             : `0x00096012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`dlkengsta`| 16-bit corresponds to 16 engines 1: Engine is busy. 0: Engine is free| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer DLK Insertion Message Memory

* **Description**           

This memory contains data messages; each DLK insertion engine has a 128-byte message.e whether or not it is being usued by another DS1/E1 channel.


* **RTL Instant Name**    : `dej1_tx_framer_dlk_ins_msg_mem`

* **Address**             : `0x096800-0x096FFF`

* **Formula**             : `0x096800 +  msgid*128 + byteid`

* **Where**               : 

    * `$msgid(0-15):`

    * `$byteid(0-127):`

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`dlkinsmsg`| DlkInsMsg| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Tx Framer DLK Insertion BOM Control

* **Description**           

This memory is used to control transmitting Bit-Oriented Message (BOM). This memory is accessed indirectly via indirect Access registers


* **RTL Instant Name**    : `dej1_tx_framer_dlk_ins_bom_ctrl`

* **Address**             : `0x097000-0x0973FF`

* **Formula**             : `0x097000 +  de3id*32 + de2id*4 + de1id`

* **Where**               : 

    * `$de3id(0-23):`

    * `$de2id(0-6):`

    * `$de1id(0-3):`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[10]`|`dlkbomen`| enable transmitting BOM. By setting this field to 1, DLK engine will rt to insert 6-bit into the BOM pattern 111111110xxxxxx0 with the MSB is transmitted first. When transmitting finishes, engine automatically clears this bit to inform to CPU that the BOM message has been already transmitted.| `RW`| `0x0`| `0x0`|
|`[9:6]`|`dlkbomrpttime`| indicate the number of time that the BOM is transmitted repeatedly, 0xF for send continuos.| `RW`| `0x0`| `0x0`|
|`[5:0]`|`dlkbommsg`| 6-bit BOM message in pattern BOM 111111110xxxxxx0 in which the MSB is transmitted first.| `RW`| `0x0`| `0x0 End: Begin:`|

###TxM23E23 Control

* **Description**           

The TxM23E23 Control is use to configure for per channel Tx DS3/E3 operation.


* **RTL Instant Name**    : `txm23e23_ctrl`

* **Address**             : `0x00080000 - 0x0008001F`

* **Formula**             : `0x00080000 +  de3id`

* **Where**               : 

    * `$de3id(0-23)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[30]`|`txds3unfrmaismode`| Set 1 to send out AIS signal pattern in DS3Unframe, default set 0 to send out AIS all 1's pattern in DS3Unframe| `RW`| `0x0`| `0x0`|
|`[29]`|`txds3e3frcallone`| Force All One to remote only for Line Local Loopback(control bit5 of RX M23 0x40000)| `RW`| `0x0`| `0x0`|
|`[28]`|`txds3e3chenb`| | `RW`| `0x0`| `0x0`|
|`[27:26]`|`txds3e3dlkmode`| 0:DS3DL/E3G751NA/E3G832GC; 1:DS3UDL/E3G832NR; 2: DS3FEAC; 3:DS3NA| `RW`| `0x0`| `0x0`|
|`[25]`|`txds3e3dlkenb`| Set 1 to enable DLK DS3| `RW`| `0x0`| `0x0`|
|`[24:21]`|`txds3e3feacthres`| Number of word repeat, 0xF for send continuos| `RW`| `0x0`| `0x0`|
|`[20]`|`txds3e3lineallone`| | `RW`| `0x0`| `0x0`|
|`[19]`|`txds3e3payloop`| Payload Remote Loopback| `RW`| `0x0`| `0x0`|
|`[18]`|`txds3e3lineloop`| Line Remote Loopback| `RW`| `0x0`| `0x0`|
|`[17]`|`txds3e3frcyel`| | `RW`| `0x0`| `0x0`|
|`[16]`|`txds3e3autoyel`| | `RW`| `0x0`| `0x0`|
|`[15:14]`|`txds3e3loopmd`| DS3 C-bit mode: [15:14]: FE Control: 00: idle code; 01: activate cw; 10: de-activate cw; 11: alrm/status E3G832 mode: [15:14]: DL mode Normal: Loop Mode configuration| `RW`| `0x0`| `0x0`|
|`[13:7]`|`txds3e3loopen`| DS3 C-bit mode: [12:7]: FEAC Control Word E3G832 mode: [13:11]: Payload type [10:7]:  SSM Config Normal: [13:7]: Loop en configuration per channel| `RW`| `0x0`| `0x0`|
|`[6]`|`txds3e3ohbypass`| | `RW`| `0x0`| `0x0`|
|`[5]`|`txds3e3idleset`| | `RW`| `0x0`| `0x0`|
|`[4]`|`txds3e3aisset`| | `RW`| `0x0`| `0x0`|
|`[3:0]`|`txds3e3md`| Tx DS3/E3 framing mode 0000: DS3 Unframed 0001: DS3 C-bit parity carrying 28 DS1s or 21 E1s 0010: DS3 M23 carrying 28 DS1s or 21 E1s 0011: DS3 C-bit map 0100: E3 Unframed 0101: E3 G832 0110: E3 G.751 carrying 16 E1s 0111: E3 G751 Map 1000: Bypass RxM13E13 (DS1/E1 mode)| `RW`| `0x0`| `0x0 End: Begin:`|

###TxM23E23 HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `txm23e23_hw_stat`

* **Address**             : `0x00080080-0x0008009F`

* **Formula**             : `0x00080080 +  de3id`

* **Where**               : 

    * `$de3id(0-23):`

* **Width**               : `48`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[42:0]`|`txm23e23status`| for HW debug only| `RW`| `0x0`| `0x0 End: Begin:`|

###TxM23E23 E3g832 Trace Byte

* **Description**           




* **RTL Instant Name**    : `txm23e23_e3g832_tracebyte`

* **Address**             : `0x00080200-0x0803FF`

* **Formula**             : `0x00080200 +  de3id*16 + bytecnt`

* **Where**               : 

    * `$de3id(0-23:`

    * `$bytecnt(0-15:`

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`txm23e23_e3g832_trace_byte`| TxM23E23 E3g832 Trace Byte| `RW`| `0x0`| `0x0 End: Begin:`|

###DS3/E3 Test Pattern Generation Global Control

* **Description**           




* **RTL Instant Name**    : `de3_testpat_gen_glb_ctrl`

* **Address**             : `0x0800D0-7800D3`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15]`|`txde3ptgberen`| Enable transmit DS3/E3 BER insert| `RW`| `0x0`| `0x0`|
|`[14:12]`|`txde3ptgbermod`| BER mode configuration 000: BER 10-2 001: BER 10-3 010: BER 10-4 011: BER 10-5 100: BER 10-6 1x1: BER 10-7| `RW`| `0x0`| `0x0`|
|`[11]`|`txde3ptgswap`| PTG swap Bit. If it is set to 1, pattern is transmitted LSB first, then MSB. 		Normal is set to 0.| `RW`| `0x0`| `0x0`|
|`[10]`|`txde3ptginv`| PTG inversion Bit. Normal is set to 0.| `RW`| `0x0`| `0x0`|
|`[9]`|`txde3ptgbiterr`| Enable for single bit error insertion.| `RW`| `0x0`| `0x0`|
|`[8:5]`|`txde3ptgmod`| Generation DS3/E3 BERT mode 0000: Prbs9 0001: Prbs11 0010: Prbs15 0011: Prbs20 0100: Prbs20 0101: Qrss20 0110: Prbs23 0111: Prbs31 1000: Sequence 1001: AllOne 1010: AllZero 1011: AA55| `RW`| `0x0`| `0x0`|
|`[4]`|`txde3ptgen`| Bit Enable Test Pattern Generation| `RW`| `0x0`| `0x0`|
|`[3:0]`|`txde3ptgid`| To configure which DS3/E3 line is selected  to test the Pattern GenerationID Configuration| `RW`| `0x0`| `0x0 End: Begin:`|

###TxM12E12 Control

* **Description**           

The TxM12E12 Control is use to configure for per channel Tx DS2/E2 operation.


* **RTL Instant Name**    : `txm12e12_ctrl`

* **Address**             : `0x00081000 - 0x000810FF`

* **Formula**             : `0x00081000 +  8*de3id + de2id`

* **Where**               : 

    * `$de3id(0-23):`

    * `$de2id(0-6):`

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1:0]`|`txds2e2md`| Tx DS2/E2 framing mode 00: DS2 T1.107 carrying 4 DS1s 01: DS2 G.747 carrying 3 E1s 10: E2 G.742 carrying 4 E1s 11: Reserved| `RW`| `0x0`| `0x0 End: Begin:`|

###TxM12E12 HW Status

* **Description**           

for HW debug only


* **RTL Instant Name**    : `txm12e12_hw_stat`

* **Address**             : `0x00081200 - 0x000812FF`

* **Formula**             : `0x00081200 +  8*de3id + de2id`

* **Where**               : 

    * `$de3id(0-23):`

    * `$de2id(0-6):`

* **Width**               : `96`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[80:0]`|`txm12e12status`| for HW debug only| `RW`| `0x0`| `0x0 End: Begin:`|

###Tx M12E12 Jitter Configuration

* **Description**           

This is the configuration for Tx M13/E13


* **RTL Instant Name**    : `txm12e12_jitter_cfg`

* **Address**             : `0x00081380`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `96`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[69:63]`|`swthreshold1`| | `RW`| `0x0`| `0x0`|
|`[62:56]`|`swthreshold2`| | `RW`| `0x0`| `0x0`|
|`[55:49]`|`swthreshold3`| | `RW`| `0x0`| `0x0`|
|`[48:42]`|`swthreshold4`| | `RW`| `0x0`| `0x0`|
|`[41:35]`|`swthreshold5`| | `RW`| `0x0`| `0x0`|
|`[34:28]`|`swcntmax1`| | `RW`| `0x0`| `0x0`|
|`[27:21]`|`swcntmax2`| | `RW`| `0x0`| `0x0`|
|`[20:14]`|`swcntmax3`| | `RW`| `0x0`| `0x0`|
|`[13:7]`|`swcntmax4`| | `RW`| `0x0`| `0x0`|
|`[6:0]`|`swcntmax5`| | `RW`| `0x0`| `0x0 End: Begin:`|

###LC test reg1

* **Description**           

For testing only


* **RTL Instant Name**    : `upen_test1`

* **Address**             : `0xE_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`test_reg1`| value of reg1| `RW`| `0x1004_2015`| `0x1004_2015 End: Begin:`|

###LC test reg2

* **Description**           

For testing only


* **RTL Instant Name**    : `upen_test2`

* **Address**             : `0xE_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`test_reg2`| value of reg2| `RW`| `0x00`| `0x00 End: Begin:`|

###Config Limit error

* **Description**           

Check pattern error in state matching pattern


* **RTL Instant Name**    : `upen_lim_err`

* **Address**             : `0xE_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6:0]`|`cfg_lim_err`| if cnt err more than limit error,search failed| `RW`| `0xF`| `0xF End: Begin:`|

###Config limit match

* **Description**           

Used to check parttern match


* **RTL Instant Name**    : `upen_lim_mat`

* **Address**             : `0xE_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_lim_mat`| if cnt match more than limit mat, serch ok| `RW`| `0xFFE`| `0xFFE End: Begin:`|

###Config thresh fdl parttern

* **Description**           

Check fdl message codes match


* **RTL Instant Name**    : `upen_th_fdl_mat`

* **Address**             : `0xE_0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`cfg_th_fdl_mat`| if message codes repeat more than thresh, search ok,| `RW`| `0x5`| `0x5 End: Begin:`|

###Config thersh error

* **Description**           

Max error for one pattern


* **RTL Instant Name**    : `upen_th_err`

* **Address**             : `0xE_0005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:07]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[06:00]`|`cfg_th_err`| if pattern error more than thresh , change to check another pattern| `RW`| `0xF`| `0xF End: Begin:`|

###Sticky pattern detected

* **Description**           

For HW debug


* **RTL Instant Name**    : `upen_stk`

* **Address**             : `0xE_20FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2:0]`|`updo_stk`| bit[0] = 1 :empty bit[1] = 1 :full, bit[2] = 1 :interrupt| `R2C`| `0x0`| `0x0 End: Begin:`|

###Number of pattern

* **Description**           

For HW debug


* **RTL Instant Name**    : `upen_len`

* **Address**             : `0xE_20FE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10:00]`|`info_len_l`| number patt detected| `RO`| `0x0`| `0x0 End: Begin:`|

###Current Inband Code

* **Description**           

Used to report current status of chid


* **RTL Instant Name**    : `upen_info`

* **Address**             : `0xE_2100 - 0xE_24FF`

* **Formula**             : `0xE_2100 + id`

* **Where**               : 

    * `$id (0-1023):`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[02:00]`|`updo_sta`| bit [2:0] == 0x0  CSU UP bit [2:0] == 0x1  CSU DOWN bit [2:0] == 0x2  FAC1 UP bit [2:0] == 0x3  FAC1 DOWN bit [2:0] == 0x4  FAC2 UP bit [2:0] == 0x5  FAC2 DOWN bit [2:0] == 0x7  Change from LoopCode to nomal| `RO`| `0x0`| `0x0 End: Begin:`|

###Sticky FDL message detected

* **Description**           

For HW debug


* **RTL Instant Name**    : `upen_fdl_stk`

* **Address**             : `0xE_08FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2:0]`|`updo_fdlstk`| bit[0] = 1 :empty, bit[1] = 1 :full, bit[2] = 1 :interrupt| `R2C`| `0x0`| `0x0 End: Begin:`|

###Config User programmble Pattern User Codes

* **Description**           

check fdl message codes


* **RTL Instant Name**    : `upen_fdlpat`

* **Address**             : `0xE_08F8`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `128`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:56]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[55:00]`|`updo_fdl_mess`| mess codes| `RO`| `0x284C_701C`| `0x284C_701C End: Begin:`|

###Number of message

* **Description**           

For HW debug


* **RTL Instant Name**    : `upen_len_mess`

* **Address**             : `0xE_08FE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10:00]`|`info_len_l`| number patt detected| `RO`| `0x0`| `0x0 End: Begin:`|

###Currenr Message FDL Detected

* **Description**           

Info fdl message detected


* **RTL Instant Name**    : `upen_fdl_info`

* **Address**             : `0xE_1800 - 0xE_1BFF`

* **Formula**             : `0xE_1800 + $fdl_len`

* **Where**               : 

    * `$fdl_len (0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`mess_info_l`| message info bit[15:8] :	8bit BOM pattern format 0xxxxxx0 bit[4]    : set 1 indicate BOM message  bit[3:0]== 0x0 11111111_01111110 bit[3:0]== 0x1 line loop up bit[3:0]== 0x2 line loop down bit[3:0]== 0x3 payload loop up bit[3:0]== 0x4 payload loop down bit[3:0]== 0x5 smartjack act bit[3:0]== 0x6 smartjact deact bit[3:0]== 0x7 idle| `RO`| `0x0`| `0x0 End: Begin:`|

###Config Theshold Pattern Report

* **Description**           

Maximum Pattern Report


* **RTL Instant Name**    : `upen_th_stt_int`

* **Address**             : `0xE_0006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`updo_th_stt_int`| if number of pattern detected more than threshold, interrupt enable| `RW`| `0xFF`| `0xFF End: Begin:`|

###Sticky Loopcode detected

* **Description**           

Indicate loopcode is detected


* **RTL Instant Name**    : `upen_ibstk_rs`

* **Address**             : `0xE_2800 - 0xE_281F`

* **Formula**             : `0xE_2800 + $base`

* **Where**               : 

    * `$base (0-31)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`updo_ibstk_rs`| sticky inband loopcode detect base 0 : chid0 - chid31 ... base 31: chid991 - chid1023| `RW`| `0x0`| `0x0 End: Begin:`|

###Sticky Loopcode change

* **Description**           

Indicate loopcode is detected


* **RTL Instant Name**    : `upen_ibstk_fl`

* **Address**             : `0xE_3000 - 0xE_371f`

* **Formula**             : `0xE_3000 + $base`

* **Where**               : 

    * `$base (0-31)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`updo_ibstk_fl`| sticky inband change base 0 : chid0 - chid31 ... base 31: chid991 - chid1023| `RW`| `0x0`| `0x0 End: Begin:`|

###Sticky Outband LoopCode

* **Description**           

Indicate outband loopcode is detect


* **RTL Instant Name**    : `upen_obstk_rs`

* **Address**             : `0xE_0900 - 0xE_091F`

* **Formula**             : `0xE_0900 + $base`

* **Where**               : 

    * `$base (0-31)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`updo_obstk_rs`| Sticky outband detected base 0 : chid0 - chid31 ... base 31: chid991 - chid1023| `RW`| `0x0`| `0x0 End: Begin:`|

###Sticky Outband LoopCode change

* **Description**           

Indicate outband loopcode is changed


* **RTL Instant Name**    : `upen_obstk_fl`

* **Address**             : `0xE_1000 - 0x101f`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`updo_obstk_fl`| Sticky outband change base 0 : chid0 - chid31 ... base 31: chid991 - chid1023| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_Parity_Force_Control`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:13]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[12]`|`pdhvtasyncmapctrl_parerrfrc`| Force parity For RAM Control "PDH VT Async Map Control"| `RW`| `0x0`| `0x0`|
|`[11]`|`pdhstsvtmapctrl_parerrfrc`| Force parity For RAM Control "PDH STS/VT Map Control"| `RW`| `0x0`| `0x0`|
|`[10]`|`pdhtxm23e23trace_parerrfrc`| Force parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"| `RW`| `0x0`| `0x0`|
|`[9]`|`pdhtxm23e23ctrl_parerrfrc`| Force parity For RAM Control "PDH TxM23E23 Control"| `RW`| `0x0`| `0x0`|
|`[8]`|`pdhtxm12e12ctrl_parerrfrc`| Force parity For RAM Control "PDH TxM12E12 Control"| `RW`| `0x0`| `0x0`|
|`[7]`|`pdhtxde1sigctrl_parerrfrc`| Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling Control"| `RW`| `0x0`| `0x0`|
|`[6]`|`pdhtxde1frmctrl_parerrfrc`| Force parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"| `RW`| `0x0`| `0x0`|
|`[5]`|`pdhrxde1frmctrl_parerrfrc`| Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"| `RW`| `0x0`| `0x0`|
|`[4]`|`pdhrxm12e12ctrl_parerrfrc`| Force parity For RAM Control "PDH RxM12E12 Control"| `RW`| `0x0`| `0x0`|
|`[3]`|`pdhrxds3e3ohctrl_parerrfrc`| Force parity For RAM Control "PDH RxDS3E3 OH Pro Control"| `RW`| `0x0`| `0x0`|
|`[2]`|`pdhrxm23e23ctrl_parerrfrc`| Force parity For RAM Control "PDH RxM23E23 Control"| `RW`| `0x0`| `0x0`|
|`[1]`|`pdhstsvtdemapctrl_parerrfrc`| Force parity For RAM Control "PDH STS/VT Demap Control"| `RW`| `0x0`| `0x0`|
|`[0]`|`pdhmuxctrl_parerrfrc`| Force parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux Control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_Parity_Disable_Control`

* **Address**             : `0x11`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:13]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[12]`|`pdhvtasyncmapctrl_parerrdis`| Disable parity For RAM Control "PDH VT Async Map Control"| `RW`| `0x0`| `0x0`|
|`[11]`|`pdhstsvtmapctrl_parerrdis`| Disable parity For RAM Control "PDH STS/VT Map Control"| `RW`| `0x0`| `0x0`|
|`[10]`|`pdhtxm23e23trace_parerrdis`| Disable parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"| `RW`| `0x0`| `0x0`|
|`[9]`|`pdhtxm23e23ctrl_parerrdis`| Disable parity For RAM Control "PDH TxM23E23 Control"| `RW`| `0x0`| `0x0`|
|`[8]`|`pdhtxm12e12ctrl_parerrdis`| Disable parity For RAM Control "PDH TxM12E12 Control"| `RW`| `0x0`| `0x0`|
|`[7]`|`pdhtxde1sigctrl_parerrdis`| Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling Control"| `RW`| `0x0`| `0x0`|
|`[6]`|`pdhtxde1frmctrl_parerrdis`| Disable parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"| `RW`| `0x0`| `0x0`|
|`[5]`|`pdhrxde1frmctrl_parerrdis`| Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"| `RW`| `0x0`| `0x0`|
|`[4]`|`pdhrxm12e12ctrl_parerrdis`| Disable parity For RAM Control "PDH RxM12E12 Control"| `RW`| `0x0`| `0x0`|
|`[3]`|`pdhrxds3e3ohctrl_parerrdis`| Disable parity For RAM Control "PDH RxDS3E3 OH Pro Control"| `RW`| `0x0`| `0x0`|
|`[2]`|`pdhrxm23e23ctrl_parerrdis`| Disable parity For RAM Control "PDH RxM23E23 Control"| `RW`| `0x0`| `0x0`|
|`[1]`|`pdhstsvtdemapctrl_parerrdis`| Disable parity For RAM Control "PDH STS/VT Demap Control"| `RW`| `0x0`| `0x0`|
|`[0]`|`pdhmuxctrl_parerrdis`| Disable parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux Control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_Parity_Error_Sticky`

* **Address**             : `0x12`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12]`|`pdhvtasyncmapctrl_parerrstk`| Error parity For RAM Control "PDH VT Async Map Control"| `RW`| `0x0`| `0x0`|
|`[11]`|`pdhstsvtmapctrl_parerrstk`| Error parity For RAM Control "PDH STS/VT Map Control"| `RW`| `0x0`| `0x0`|
|`[10]`|`pdhtxm23e23trace_parerrstk`| Error parity For RAM Control "PDH TxM23E23 E3g832 Trace Byte"| `RW`| `0x0`| `0x0`|
|`[9]`|`pdhtxm23e23ctrl_parerrstk`| Error parity For RAM Control "PDH TxM23E23 Control"| `RW`| `0x0`| `0x0`|
|`[8]`|`pdhtxm12e12ctrl_parerrstk`| Error parity For RAM Control "PDH TxM12E12 Control"| `RW`| `0x0`| `0x0`|
|`[7]`|`pdhtxde1sigctrl_parerrstk`| Error parity For RAM Control "PDH DS1/E1/J1 Tx Framer Signalling Control"| `RW`| `0x0`| `0x0`|
|`[6]`|`pdhtxde1frmctrl_parerrstk`| Error parity For RAM Control "PDH DS1/E1/J1 Tx Framer Control"| `RW`| `0x0`| `0x0`|
|`[5]`|`pdhrxde1frmctrl_parerrstk`| Error parity For RAM Control "PDH DS1/E1/J1 Rx Framer Control"| `RW`| `0x0`| `0x0`|
|`[4]`|`pdhrxm12e12ctrl_parerrstk`| Error parity For RAM Control "PDH RxM12E12 Control"| `RW`| `0x0`| `0x0`|
|`[3]`|`pdhrxds3e3ohctrl_parerrstk`| Error parity For RAM Control "PDH RxDS3E3 OH Pro Control"| `RW`| `0x0`| `0x0`|
|`[2]`|`pdhrxm23e23ctrl_parerrstk`| Error parity For RAM Control "PDH RxM23E23 Control"| `RW`| `0x0`| `0x0`|
|`[1]`|`pdhstsvtdemapctrl_parerrstk`| Error parity For RAM Control "PDH STS/VT Demap Control"| `RW`| `0x0`| `0x0`|
|`[0]`|`pdhmuxctrl_parerrstk`| Error parity For RAM Control "PDH DS1/E1/J1 Rx Framer Mux Control"| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Payload Error Insert Enable Configuration

* **Description**           




* **RTL Instant Name**    : `dej1_errins_en_cfg`

* **Address**             : `0x00090002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9:0]`|`txde1errinsid`| Force ID channel| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Payload Error Insert threhold Configuration

* **Description**           




* **RTL Instant Name**    : `dej1_errins_thr_cfg`

* **Address**             : `0x00090003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[30:29]`|`txde1errormode`| Force error mode: 3:bit (included Fbit); 2:unused; 1: unused: 0: disable| `RW`| `0x0`| `0x0`|
|`[28]`|`txde1errorsingle_en`| 0: One-shot with number of errors; 1:Line rate| `RW`| `0x0`| `0x0`|
|`[27:0]`|`txde1payerrinsthr`| Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1| `RW`| `0x0`| `0x0	// End: Begin:`|

###DS1/E1/J1 Rx Error Insert Enable Configuration

* **Description**           




* **RTL Instant Name**    : `dej1_rx_errins_en_cfg`

* **Address**             : `0x00050017`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9:0]`|`rxde1errinsid`| Force ID channel| `RW`| `0x0`| `0x0 End: Begin:`|

###DS1/E1/J1 Payload Error Insert threhold Configuration

* **Description**           




* **RTL Instant Name**    : `dej1_rx_errins_thr_cfg`

* **Address**             : `0x00050018`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[30:29]`|`rxde1errormode`| Force error mode: 3:bit (included Fbit); 2:unused; 1: unused: 0: disable| `RW`| `0x0`| `0x0`|
|`[28]`|`rxde1errorsingle_en`| 0: One-shot with number of errors; 1:Line rate| `RW`| `0x0`| `0x0`|
|`[27:0]`|`rxde1payerrinsthr`| Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1| `RW`| `0x0`| `0x0	// End: Begin:`|

###DS3/E3 Payload Error Insert Enable Configuration

* **Description**           




* **RTL Instant Name**    : `de3_errins_en_cfg`

* **Address**             : `0x000800C0`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9:0]`|`txde3errinsid`| Force ID channel| `RW`| `0x0`| `0x0 End: Begin:`|

###DS3/E3 Payload Error Insert threhold Configuration

* **Description**           




* **RTL Instant Name**    : `de3_errins_thr_cfg`

* **Address**             : `0x000800C1`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[30:29]`|`txde3errormode`| Force error mode: 3:bit (included Fbit); 2:unused; 1: unused: 0: disable| `RW`| `0x0`| `0x0`|
|`[28]`|`txde3errorsingle_en`| 0: One-shot with number of errors; 1:Line rate| `RW`| `0x0`| `0x0`|
|`[27:0]`|`txde3payerrinsthr`| Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1| `RW`| `0x0`| `0x0	// End: Begin:`|

###DS3/E3 Rx Error Insert Enable Configuration

* **Description**           




* **RTL Instant Name**    : `de3_rx_errins_en_cfg`

* **Address**             : `0x000400C0`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[9:0]`|`rxde3errinsid`| Force ID channel| `RW`| `0x0`| `0x0 End: Begin:`|

###DS3/E3 Payload Error Insert threhold Configuration

* **Description**           




* **RTL Instant Name**    : `de3_rx_errins_thr_cfg`

* **Address**             : `0x000400C1`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[30:29]`|`rxde3errormode`| Force error mode: 3:bit (included Fbit); 2:unused; 1: unused: 0: disable| `RW`| `0x0`| `0x0`|
|`[28]`|`rxde3errorsingle_en`| 0: One-shot with number of errors; 1:Line rate| `RW`| `0x0`| `0x0`|
|`[27:0]`|`rxde3payerrinsthr`| Error Threshold in bit unit or the number of error in one-shot mode. Valid value from 1| `RW`| `0x0`| `0x0	// End:`|

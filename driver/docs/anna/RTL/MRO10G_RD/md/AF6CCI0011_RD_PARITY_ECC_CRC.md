## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_PARITY_ECC_CRC
####Register Table

|Name|Address|
|-----|-----|
|`QDR ECC Force Error Control`|`0xF26000`|
|`QDR ECC Error Sticky`|`0xF26001`|
|`QDR ECC Correctable Error Counter`|`0xF26002`|
|`QDR ECC NonCorrectable Error Counter`|`0xF26004`|
|`DDRno1 PDA ECC Force Error Control`|`0x500020`|
|`DDRno1 PDA ECC Error Sticky`|`0x500021`|
|`DDRno1 PDA ECC Correctable Error Counter`|`0x500022`|
|`DDRno1 PDA ECC NonCorrectable Error Counter`|`0x500024`|
|`DDRno1 PLA CRC Force Error Control`|`0x488050`|
|`DDRno1 PLA CRC Error Sticky`|`0x488051`|
|`DDRno1 CRC Error Counter`|`0x488052`|
|`DDRno2 PDA CRC Force Error Control`|`0x500030`|
|`DDRno2 PDA CRC Error Sticky`|`0x500031`|
|`DDRno2 CRC Error Counter`|`0x500032`|


###QDR ECC Force Error Control

* **Description**           

This register configures QDR ECC force error.


* **RTL Instant Name**    : `qdr_ecc_force_error_control`

* **Address**             : `0xF26000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `30`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[29]`|`qdreccnoncorrectcfg`| Ecc error mode  1: Non-correctable  0: Correctable| `RW`| `0x0`| `0x0`|
|`[28]`|`qdreccerrforevercfg`| Force ecc error mode  1: forever when field QDREccErrNumber differ zero 0: burst| `RW`| `0x0`| `0x0`|
|`[27:0]`|`qdreccerrnumbercfg`| number of ECC error inserted to QDR| `RW`| `0x0`| `0x0 End: Begin:`|

###QDR ECC Error Sticky

* **Description**           

This register report sticky of QDR ECC error.


* **RTL Instant Name**    : `qdr_ecc_sticky`

* **Address**             : `0xF26001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`qdreccnoncorrecterror`| QDR non-correctable error| `WC`| `0x0`| `0x0`|
|`[0]`|`qdrecccorrecterror`| | `QDR correctable error`| `WC`| `0x0`|

###QDR ECC Correctable Error Counter

* **Description**           

This register counts QDR ECC error.


* **RTL Instant Name**    : `qdr_ecc_correctable_counter`

* **Address**             : `0xF26002`

* **Formula**             : `0xF26002 + R2C`

* **Where**               : 

    * `$R2C(0-1): value zero for read only,value 1 for read to clear`

* **Width**               : `28`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`qdreccerrorcounter`| | `QDR Ecc error counter`| `RO`| `0x0`|

###QDR ECC NonCorrectable Error Counter

* **Description**           

This register counts QDR ECC error.


* **RTL Instant Name**    : `qdr_ecc_noncorrectable_counter`

* **Address**             : `0xF26004`

* **Formula**             : `0xF26004 + R2C`

* **Where**               : 

    * `$R2C(0-1): value zero for read only,value 1 for read to clear`

* **Width**               : `28`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`qdreccerrorcounter`| | `QDR Ecc error counter`| `RO`| `0x0`|

###DDRno1 PDA ECC Force Error Control

* **Description**           

This register configures DDRno1 ECC force error.


* **RTL Instant Name**    : `ddrno1_ecc_force_error_control`

* **Address**             : `0x500020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `30`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[29]`|`ddrno1eccnoncorrectcfg`| Ecc error mode  1: Non-correctable  0: Correctable| `RW`| `0x0`| `0x0`|
|`[28]`|`ddrno1eccerrforevercfg`| Force ecc error mode  1: forever when field DDRno1EccErrNumber differ zero 0: burst| `RW`| `0x0`| `0x0`|
|`[27:0]`|`ddrno1eccerrnumbercfg`| number of ECC error inserted to DDRno1| `RW`| `0x0`| `0x0 End: Begin:`|

###DDRno1 PDA ECC Error Sticky

* **Description**           

This register report sticky of DDRno1 ECC error.


* **RTL Instant Name**    : `ddrno1_ecc_sticky`

* **Address**             : `0x500021`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`ddrno1eccnoncorrecterror`| DDRno1 non-correctable error| `WC`| `0x0`| `0x0`|
|`[0]`|`ddrno1ecccorrecterror`| | `DDRno1 correctable error`| `WC`| `0x0`|

###DDRno1 PDA ECC Correctable Error Counter

* **Description**           

This register counts DDRno1 ECC error.


* **RTL Instant Name**    : `ddrno1_ecc_correctable_counter`

* **Address**             : `0x500022`

* **Formula**             : `0x500022 + R2C`

* **Where**               : 

    * `$R2C(0-1): value zero for read only,value 1 for read to clear`

* **Width**               : `28`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`ddrno1eccerrorcounter`| | `DDRno1 Ecc error counter`| `RO`| `0x0`|

###DDRno1 PDA ECC NonCorrectable Error Counter

* **Description**           

This register counts DDRno1 ECC error.


* **RTL Instant Name**    : `ddrno1_ecc_noncorrectable_counter`

* **Address**             : `0x500024`

* **Formula**             : `0x500024 + R2C`

* **Where**               : 

    * `$R2C(0-1): value zero for read only,value 1 for read to clear`

* **Width**               : `28`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`ddrno1eccerrorcounter`| | `DDRno1 Ecc error counter`| `RO`| `0x0`|

###DDRno1 PLA CRC Force Error Control

* **Description**           

This register configures DDRno1 PLA CRC force error.


* **RTL Instant Name**    : `ddrno1_crc_force_error_control`

* **Address**             : `0x488050`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `29`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28]`|`ddrno1crcerrforevercfg`| Force crc error mode  1: forever when field DDRno1CrcErrNumber differ zero 0: burst| `RW`| `0x0`| `0x0`|
|`[27:0]`|`ddrno1crcerrnumbercfg`| number of CRC error inserted to DDRno1| `RW`| `0x0`| `0x0 End: Begin:`|

###DDRno1 PLA CRC Error Sticky

* **Description**           

This register report sticky of DDRno1 CRC error.


* **RTL Instant Name**    : `ddrno1_crc_sticky`

* **Address**             : `0x488051`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`ddrno1crcerror`| | `DDRno1 CRC error`| `WC`| `0x0`|

###DDRno1 CRC Error Counter

* **Description**           

This register counts DDRno1 CRC error.


* **RTL Instant Name**    : `ddrno1_crc_counter`

* **Address**             : `0x488052`

* **Formula**             : `0x488052 + R2C`

* **Where**               : 

    * `$R2C(0-1): value zero for read only,value 1 for read to clear`

* **Width**               : `28`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`ddrno1crcerrorcounter`| | `DDRno1 Crc error counter`| `RO`| `0x0`|

###DDRno2 PDA CRC Force Error Control

* **Description**           

This register configures DDRno2 PDA CRC force error.


* **RTL Instant Name**    : `ddrno2_crc_force_error_control`

* **Address**             : `0x500030`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `29`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28]`|`ddrno2crcerrforevercfg`| Force crc error mode  1: forever when field DDRno2CrcErrNumber differ zero 0: burst| `RW`| `0x0`| `0x0`|
|`[27:0]`|`ddrno2crcerrnumbercfg`| number of CRC error inserted to DDRno2| `RW`| `0x0`| `0x0 End: Begin:`|

###DDRno2 PDA CRC Error Sticky

* **Description**           

This register report sticky of DDRno2 CRC error.


* **RTL Instant Name**    : `ddrno2_crc_sticky`

* **Address**             : `0x500031`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`ddrno2crcerror`| | `DDRno2 CRC error`| `WC`| `0x0`|

###DDRno2 CRC Error Counter

* **Description**           

This register counts DDRno2 CRC error.


* **RTL Instant Name**    : `ddrno2_crc_counter`

* **Address**             : `0x500032`

* **Formula**             : `0x500032 + R2C`

* **Where**               : 

    * `$R2C(0-1): value zero for read only,value 1 for read to clear`

* **Width**               : `28`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[27:0]`|`ddrno2crcerrorcounter`| | `DDRno2 Crc error counter`| `RO`| `0x0`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CNC0021_RD_SEM
####Register Table

|Name|Address|
|-----|-----|
|`SEU PCI Configuration 0`|`0x000-0x200`|
|`SEU PCI Configuration 1`|`0x001-0x201`|
|`SEU Alarm`|`0x002-0x202`|
|`SEU PCI Status`|`0x003-0x203`|
|`SEU Interrupt enable`|`0x004-0x204`|
|`SEU Essential Counter R2C`|`0x008-0x208`|
|`SEU Essential Counter RO`|`0x009-0x209`|
|`SEU Uncorrectable Counter R2C`|`0x00A-0x20A`|
|`SEU Uncorrectable Counter RO`|`0x00B-0x20B`|
|`SEU Correctable Counter R2C`|`0x00C-0x20C`|
|`SEU Correctable Counter RO`|`0x00D-0x20D`|
|`SEM SLRID`|`0x010-0x210`|
|`SEM Monitor Interface Status FiFo TX Side`|`0x011-0x211`|
|`SEM Monitor Interface Value FiFo TX Side`|`0x012-0x212`|
|`SEM Monitor Interface Control RX Side`|`0x013-0x213`|
|`SEM Monitor Interface Command Value Part0 RX Side`|`0x014-0x214`|
|`SEM Monitor Interface Command Value Part1 RX Side`|`0x015-0x215`|
|`SEM Monitor Interface Command Value Part2 RX Side`|`0x016-0x216`|
|`SEM Monitor Interface Command Value Part3 RX Side`|`0x017-0x217`|
|`SEM Heartbeat Status`|`0x018-0x218`|


###SEU PCI Configuration 0

* **Description**           

Control Command Interface bus of each SLR


* **RTL Instant Name**    : `sem_cmd_ctrl`

* **Address**             : `0x000-0x200`

* **Formula**             : `0x000+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[10:10]`|`cmd_ctrl_done`| Command Interface status<br>{1} : process done| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`cmd_ctrl_busy`| Command Interface status<br>{1} : Command Interface in process state| `R_O`| `0x0`| `0x0`|
|`[08:08]`|`seu_inject_strobe`| Trigger 1->0 to start command| `R/W`| `0x0`| `0x0`|
|`[07:04]`|`op`| Command type, other values are unused<br>{0xA} : Directed State Change to Observation <br>{0xB} : Software Reset <br>{0xC} : Error Injection Using LFA <br>{0xD} : Directed State Change to Diagnostic Scan <br>{0xE} : Directed State Change to Idle <br>{0xF} : Directed State Change to Detect-only| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###SEU PCI Configuration 1

* **Description**           

Command Interface bus value part0 of each SLR


* **RTL Instant Name**    : `sem_cmd_val0`

* **Address**             : `0x001-0x201`

* **Formula**             : `0x001+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`sem_cmd_slr`| Hardware slr number (2-bit),valid range [0:2]| `R/W`| `0x0`| `0x0`|
|`[11:05]`|`sem_cmd_wradr`| Word Address  :  7bit, valid range [0:122]| `R/W`| `0x0`| `0x0`|

###SEU Alarm

* **Description**           

SEM controller sticky state of each SLR


* **RTL Instant Name**    : `sem_ctrl_stk_sta`

* **Address**             : `0x002-0x202`

* **Formula**             : `0x002+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:27]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[26:26]`|`slr2_status_heartbeat`| SLR2 status_heartbeat : Set 1 indicate to SEM IP got heartbeat event| `W1C`| `0x0`| `0x0`|
|`[25:25]`|`slr1_status_heartbeat`| SLR1 status_heartbeat : Set 1 indicate to SEM IP got heartbeat event| `W1C`| `0x0`| `0x0`|
|`[24:24]`|`slr0_status_heartbeat`| SLR0 status_heartbeat : Set 1 indicate to SEM IP got heartbeat event| `W1C`| `0x0`| `0x0`|
|`[23:19]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[18:18]`|`slr2_sem_act`| SLR2 Sem_Act : Set 1 indicate to SEM IP got Active State event| `W1C`| `0x0`| `0x0`|
|`[17:17]`|`slr1_sem_act`| SLR1 Sem_Act : Set 1 indicate to SEM IP got Active State event| `W1C`| `0x0`| `0x0`|
|`[16:16]`|`slr0_sem_act`| SLR0 Sem_Act : Set 1 indicate to SEM IP got Active State event| `W1C`| `0x0`| `0x0`|
|`[11:11]`|`status_uncorrectable`| status_uncorrectable : Set 1 indicate to SEM IP got uncorrectable event| `W1C`| `0x0`| `0x0`|
|`[10:10]`|`status_correctable`| status_correctable : Set 1 indicate to SEM IP got correctable event| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[08:08]`|`status_essential`| status_essential : Set 1 indicate to SEM IP got essential event| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[06:06]`|`status_idle`| status_idle : Set 1 indicate to SEM IP got IDLE State event| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`status_initialization`| status_initialization : Set 1 indicate to SEM IP got initialization State event| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`status_injection`| status_injection : Set 1 indicate to SEM IP got injection State event| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`status_classification`| status_classification : Set 1 indicate to SEM IP got classification State event| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`fatal_sem_err`| Fatal_Sem_Err : Set 1 indicate to SEM IP has fatal error| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`status_observation`| status_observation : Set 1 indicate to SEM IP got observation State event| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`status_correction`| status_correction : Set 1 indicate to SEM IP got Correction State event| `W1C`| `0x0`| `0x0`|

###SEU PCI Status

* **Description**           

SEM controller current state of each SLR


* **RTL Instant Name**    : `sem_ctrl_cur_sta`

* **Address**             : `0x003-0x203`

* **Formula**             : `0x003+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[18:18]`|`slr2_sem_act`| SLR2 Sem_Act: Set 1 indicate  status FSM of SEM IP is active| `R_O`| `0x0`| `0x0`|
|`[17:17]`|`slr1_sem_act`| SLR1 Sem_Act: Set 1 indicate  status FSM of SEM IP is active| `R_O`| `0x0`| `0x0`|
|`[16:16]`|`slr0_sem_act`| SLR0 Sem_Act: Set 1 indicate  status FSM of SEM IP is active| `R_O`| `0x0`| `0x0`|
|`[15:07]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[06:06]`|`status_idle`| status_idle: Set 1 indicate status FSM of SEM IP is IDLE| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`status_initialization`| status_Initialization: Set 1 indicate status FSM of SEM IP is INIITIALIZATION| `R_O`| `0x0`| `0x0`|
|`[04:04]`|`status_injection`| status_injection: Set 1 indicate status FSM of SEM IP is injection| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`status_classification`| status_classification: Set 1 indicate status FSM of SEM IP is Classification| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`fatal_sem_err`| fatal_sem_err: Set 1 indicate status FSM of SEM IP is Falal Error| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`status_observation`| status_observation: Set 1 indicate status FSM of SEM IP is OBSERVATION| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`status_correction`| status_correction: Set 1 indicate status FSM of SEM IP is Correction| `R_O`| `0x0`| `0x0`|

###SEU Interrupt enable

* **Description**           

SEM controller enable interrupt of each SLR


* **RTL Instant Name**    : `sem_int_enb`

* **Address**             : `0x004-0x204`

* **Formula**             : `0x004+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:27]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[26:26]`|`slr2_status_heartbeat`| Set 1 to SLR2 enable heartbeat interrupt| `R/W`| `0x0`| `0x0`|
|`[25:25]`|`slr1_status_heartbeat`| Set 1 to SLR1 enable heartbeat interrupt| `R/W`| `0x0`| `0x0`|
|`[24:24]`|`slr0_status_heartbeat`| Set 1 to SLR0 enable heartbeat interrupt| `R/W`| `0x0`| `0x0`|
|`[23:12]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[11:11]`|`status_uncorrectable`| Set 1 to enable uncorrectable interrupt| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`status_correctable`| Set 1 to enable correctable interrupt| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[08:08]`|`status_essential`| Set 1 to enable essential interrupt| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[06:06]`|`status_idle`| Set 1 to enable idle interrupt| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`status_initialization`| Set 1 to enable initialization interrupt| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`status_injection`| Set 1 to enable injection interrupt| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`status_classification`| Set 1 to enable classification interrupt| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`fatal_sem_err`| Set 1 to enable fatal interrupt| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`status_observation`| Set 1 to enable observation interrupt| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`corst`| Set 1 to enable correction interrupt| `R/W`| `0x0`| `0x0`|

###SEU Essential Counter R2C

* **Description**           

SEU Essential Counter R2C of each SLR


* **RTL Instant Name**    : `sem_essen_cnt_r2c`

* **Address**             : `0x008-0x208`

* **Formula**             : `0x008+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:00]`|`sem_essential_cnt_r2c`| Sem_Essential_Cnt: Counter Essential of SEM IP| `R2C`| `0x0`| `0x0`|

###SEU Essential Counter RO

* **Description**           

SEU Essential Counter RO of each SLR


* **RTL Instant Name**    : `sem_essen_cnt_ro`

* **Address**             : `0x009-0x209`

* **Formula**             : `0x009+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:00]`|`sem_essential_cnt_ro`| Sem_Essential_Cnt: Counter Essential of SEM IP| `RO`| `0x0`| `0x0`|

###SEU Uncorrectable Counter R2C

* **Description**           

SEU Uncorrectable Counter R2C of each SLR


* **RTL Instant Name**    : `sem_uncorr_cnt_r2c`

* **Address**             : `0x00A-0x20A`

* **Formula**             : `0x00A+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:00]`|`sem_uncorrectable_cnt_r2c`| Sem_UnCorrectable_Cnt: Counter UnCorrectable of SEM IP| `R2C`| `0x0`| `0x0`|

###SEU Uncorrectable Counter RO

* **Description**           

SEU Uncorrectable Counter RO of each SLR


* **RTL Instant Name**    : `sem_uncorr_cnt_ro`

* **Address**             : `0x00B-0x20B`

* **Formula**             : `0x00B+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:00]`|`sem_uncorrectable_cnt_ro`| Sem_UnCorrectable_Cnt: Counter UnCorrectable of SEM IP| `RO`| `0x0`| `0x0`|

###SEU Correctable Counter R2C

* **Description**           

SEU Correctable Counter R2C of each SLR


* **RTL Instant Name**    : `sem_corr_cnt_r2c`

* **Address**             : `0x00C-0x20C`

* **Formula**             : `0x00C+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:00]`|`sem_correctable_cnt_r2c`| Sem_Correctable_Cnt: Counter Correctable of SEM IP| `R2C`| `0x0`| `0x0`|

###SEU Correctable Counter RO

* **Description**           

SEU Correctable Counter RO of each SLR


* **RTL Instant Name**    : `sem_corr_cnt_ro`

* **Address**             : `0x00D-0x20D`

* **Formula**             : `0x00D+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:00]`|`sem_correctable_cnt_ro`| Sem_Correctable_Cnt: Counter Correctable of SEM IP| `RO`| `0x0`| `0x0`|

###SEM SLRID

* **Description**           

Read HW SLR of each SLR


* **RTL Instant Name**    : `sem_slrid`

* **Address**             : `0x010-0x210`

* **Formula**             : `0x010+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:00]`|`slrid`| HW SLR ID| `R_O`| `0x0`| `0x0`|

###SEM Monitor Interface Status FiFo TX Side

* **Description**           

Monitor Interface bus at TX side of each SLR


* **RTL Instant Name**    : `sem_mon_sta`

* **Address**             : `0x011-0x211`

* **Formula**             : `0x011+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`mon_tx_ctrl_trig`| Trigger 0->1 to start reseting FiFo| `R/W`| `0x0`| `0x0`|
|`[30:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:23]`|`mon_tx_eful_stk`| Sticky Commnon TX fifo is early full<br>{1} : early full| `W1C`| `0x0`| `0x0`|
|`[22:22]`|`mon_tx_full_stk`| Sticky Common TX fifo is full<br>{1} : full| `W1C`| `0x0`| `0x0`|
|`[21:21]`|`mon_tx_ept_stk`| Sticky Common TX fifo is empty<br>{1} : empty| `W1C`| `0x0`| `0x0`|
|`[20:20]`|`mon_tx_nept_stk`| Sticky Common TX fifo is not empty<br>{1} : not empty| `W1C`| `0x0`| `0x0`|
|`[19:19]`|`mon_tx_eful_cur`| Current Commnon TX fifo is early full<br>{1} : early full| `R_O`| `0x0`| `0x0`|
|`[18:18]`|`mon_tx_full_cur`| Current Common TX fifo is full<br>{1} : full| `R_O`| `0x0`| `0x0`|
|`[17:17]`|`mon_tx_ept_cur`| Current Common TX fifo is empty<br>{1} : empty| `R_O`| `0x0`| `0x0`|
|`[16:16]`|`mon_tx_nept_cur`| Current Common TX fifo is not empty<br>{1} : not empty| `R_O`| `0x0`| `0x0`|
|`[15:12]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[11:00]`|`mon_tx_len_cur`| Current length of Common fifo| `R_O`| `0x0`| `0x0`|

###SEM Monitor Interface Value FiFo TX Side

* **Description**           

Monitor Interface bus at TX side of each SLR


* **RTL Instant Name**    : `sem_mon_val`

* **Address**             : `0x012-0x212`

* **Formula**             : `0x012+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:00]`|`mon_tx_val`| Status of SEM controller, ASCII format, you have to monitor status of FiFo TX to get valid value| `R_O`| `0x0`| `0x0`|

###SEM Monitor Interface Control RX Side

* **Description**           

Monitor Interface bus at RX side of each SLR


* **RTL Instant Name**    : `sem_mon_ctrl`

* **Address**             : `0x013-0x213`

* **Formula**             : `0x013+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:09]`|`mon_rx_ctrl_done`| Trigger is done<br>{1} : done| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`mon_rx_ctrl_trig`| Trigger 1->0 to HW start sending command to SEM controller via Monitor interface| `R/W`| `0x0`| `0x0`|
|`[07:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`mon_rx_ctrl_len`| Command length| `R/W`| `0x0`| `0x0`|

###SEM Monitor Interface Command Value Part0 RX Side

* **Description**           

Monitor Interface bus at RX side of each SLR


* **RTL Instant Name**    : `sem_mon_cmd_part0`

* **Address**             : `0x014-0x214`

* **Formula**             : `0x014+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mon_rx_cmd_val0`| Command value part0, this is MSB, ASCII format| `R/W`| `0x0`| `0x0`|

###SEM Monitor Interface Command Value Part1 RX Side

* **Description**           

Monitor Interface bus at RX side of each SLR


* **RTL Instant Name**    : `sem_mon_cmd_part1`

* **Address**             : `0x015-0x215`

* **Formula**             : `0x015+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mon_rx_cmd_val1`| Command value part1, it is followed Part0, ASCII format| `R/W`| `0x0`| `0x0`|

###SEM Monitor Interface Command Value Part2 RX Side

* **Description**           

Monitor Interface bus at RX side of each SLR


* **RTL Instant Name**    : `sem_mon_cmd_part2`

* **Address**             : `0x016-0x216`

* **Formula**             : `0x016+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mon_rx_cmd_val2`| Command value part2, it is followed Part1, ASCII format| `R/W`| `0x0`| `0x0`|

###SEM Monitor Interface Command Value Part3 RX Side

* **Description**           

Monitor Interface bus at RX side of each SLR


* **RTL Instant Name**    : `sem_mon_cmd_part3`

* **Address**             : `0x017-0x217`

* **Formula**             : `0x017+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mon_rx_cmd_val3`| Command value part3, it is followed Part2, this is LSB part, ASCII format| `R/W`| `0x0`| `0x0`|

###SEM Heartbeat Status

* **Description**           

Heartbeat status of each SLR


* **RTL Instant Name**    : `sem_hrtb_sta`

* **Address**             : `0x018-0x218`

* **Formula**             : `0x018+$slrid*0x100`

* **Where**               : 

    * `$slrid(0) : Hardware SLR`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[10:10]`|`slr2_hrtb_err_obsv`| SLR2 Heartbeat is observation error| `W1C`| `0x0`| `0x0`|
|`[09:09]`|`slr2_hrtb_err_diag`| SLR2 Heartbeat is diagnostic error| `W1C`| `0x0`| `0x0`|
|`[08:08]`|`slr2_hrtb_err_dete`| SLR2 Heartbeat is detect-only error| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[06:06]`|`slr1_hrtb_err_obsv`| SLR1 Heartbeat is observation error| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`slr1_hrtb_err_diag`| SLR1 Heartbeat is diagnostic error| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`slr1_hrtb_err_dete`| SLR1 Heartbeat is detect-only error| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[02:02]`|`slr0_hrtb_err_obsv`| SLR0 Heartbeat is observation error| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`slr0_hrtb_err_diag`| SLR0 Heartbeat is diagnostic error| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`slr0_hrtb_err_dete`| SLR0 Heartbeat is detect-only error| `W1C`| `0x0`| `0x0`|

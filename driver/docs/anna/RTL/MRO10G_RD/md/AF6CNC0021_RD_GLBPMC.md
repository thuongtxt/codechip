## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CNC0021_RD_GLBPMC
####Register Table

|Name|Address|
|-----|-----|
|`CPU Reg Hold 0`|`0x3_B319`|
|`Pseudowire Transmit Counter`|`0x0_4000-0x0_7EFF(RW)`|
|`Pseudowire Transmit Counter`|`0x8_4000-0x8_7EFF(RW)`|
|`Pseudowire Receiver Counter Info0`|`0x0_C000-0x0_FEFF(RW)`|
|`Pseudowire Receiver Counter Info0`|`0x8_C000-0x8_FEFF(RW)`|
|`Pseudowire Receiver Counter Info1`|`0x1_4000-0x1_7EFF(RW)`|
|`Pseudowire Receiver Counter Info1`|`0x9_4000-0x9_7EFF(RW)`|
|`Pseudowire Receiver Counter Info2`|`0x1_C000-0x1_FEFF(RW)`|
|`Pseudowire Receiver Counter Info2`|`0x9_C000-0x9_FEFF(RW)`|
|`Pseudowire Byte Counter`|`0x6_A000-0x6_B500(RW)`|
|`Pseudowire Byte Counter`|`0x7_2000-0x7_3500(RW)`|
|`PMR Error Counter`|`0x3_B040-0x3_B06F(RW)`|
|`PMR Error Counter`|`0xB_B040-0xB_B06F(RW)`|
|`POH path sts ERDI Counter1`|`0x3_D800-0x3_DDFF (RW)`|
|`POH path sts ERDI Counter1`|`0xB_D800-0xB_DDFF (RW)`|
|`POH path vt ERDI Counter0`|`0x6_4000 - 0x6_7FFF(RW)`|
|`POH path vt ERDI Counter0`|`0xE_4000 - 0xE_7FFF(RW)`|
|`POH path vt ERDI Counter1`|`0x4_4000 - 0x4_7FFF(RW)`|
|`POH path vt ERDI Counter1`|`0xC_4000 - 0xC_7FFF(RW)`|
|`POH vt pointer Counter 0`|`0x5_4000-0x5_7EFF(RW)`|
|`POH vt pointer Counter 0`|`0xD_4000-0xD_7EFF(RW)`|
|`POH vt pointer Counter 1`|`0x4_C000-0x4_FEFF(RW)`|
|`POH vt pointer Counter 1`|`0xC_C000-0xC_FEFF(RW)`|
|`POH sts pointer Counter`|`0x3_F800-0x3_FDFF(RW)`|
|`POH sts pointer Counter`|`0xB_F800-0xB_FDFF(RW)`|
|`PDH ds1 cntval Counter Bus1`|`0x3_0800-0x3_0FDF(RW)`|
|`PDH ds1 cntval Counter Bus1`|`0x3_1800-0x3_1FDF(RW)`|
|`PDH ds1 cntval Counter Bus2`|`0x3_4800-0x3_4FDF(RW)`|
|`PDH ds1 cntval Counter Bus2`|`0x3_5800-0x3_5FDF(RW)`|
|`PDH ds3 cntval Counter`|`0x3_8800-0x3_8DFF(RW)`|
|`PDH ds3 cntval Counter`|`0xB_8800-0xB_8DFF(RW)`|
|`Global PMC 2page Interrupt Sticky`|`0x3_BFFB`|


###CPU Reg Hold 0

* **Description**           

The register provides hold register from [63:32]


* **RTL Instant Name**    : `hold_reg0`

* **Address**             : `0x3_B319`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`hold0`| Hold from  [63:32]bit| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_rw`

* **Address**             : `0x0_4000-0x0_7EFF(RW)`

* **Formula**             : `0x0_4000 + 5376*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-5375)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`txpwcnt`| in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : Unused in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : txpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Transmit Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_txpwcnt_rw`

* **Address**             : `0x8_4000-0x8_7EFF(RW)`

* **Formula**             : `0x8_4000 + 5376*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-5375)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`txpwcnt`| in case of txpwcnt0 side: + offset = 0 : txrbit + offset = 1 : txnbit + offset = 2 : Unused in case of txpwcnt1 side: + offset = 0 : txpbit + offset = 1 : txlbit + offset = 2 : txpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info0_rw`

* **Address**             : `0x0_C000-0x0_FEFF(RW)`

* **Formula**             : `0x0_C000 + 5376*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-5375)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxlate + offset = 1 : rxpbit + offset = 2 : rxlbit in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info0_rw`

* **Address**             : `0x8_C000-0x8_FEFF(RW)`

* **Formula**             : `0x8_C000 + 5376*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-5375)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info0`| in case of rxpwcnt_info0_cnt0 side: + offset = 0 : rxlate + offset = 1 : rxpbit + offset = 2 : rxlbit in case of rxpwcnt_info0_cnt1 side: + offset = 0 : rxrbit + offset = 1 : rxnbit + offset = 2 : rxpkt| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info1_rw`

* **Address**             : `0x1_4000-0x1_7EFF(RW)`

* **Formula**             : `0x1_4000 + 5376*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-5375)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info1`| in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxearly + offset = 1 : rxlops + offset = 2 : Unused in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxlost + offset = 1 : Unused + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info1_rw`

* **Address**             : `0x9_4000-0x9_7EFF(RW)`

* **Formula**             : `0x9_4000 + 5376*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-5375)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info1`| in case of rxpwcnt_info1_cnt0 side: + offset = 0 : rxearly + offset = 1 : rxlops + offset = 2 : Unused in case of rxpwcnt_info1_cnt1 side: + offset = 0 : rxlost + offset = 1 : Unused + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info2

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info2_rw`

* **Address**             : `0x1_C000-0x1_FEFF(RW)`

* **Formula**             : `0x1_C000 + 5376*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-5375)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info2`| in case of rxpwcnt_info2_cnt0 side: + offset = 0 : rxoverrun + offset = 1 : rxunderrun + offset = 2 : rxmalform in case of rxpwcnt_info2_cnt1 side: + offset = 0 : Unused + offset = 1 : Unused + offset = 2 : rxstray| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Receiver Counter Info2

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_rxpwcnt_info2_rw`

* **Address**             : `0x9_C000-0x9_FEFF(RW)`

* **Formula**             : `0x9_C000 + 5376*$offset + $pwid`

* **Where**               : 

    * `$pwid(0-5375)`

    * `$offset(0-2)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`rxpwcnt_info2`| in case of rxpwcnt_info2_cnt0 side: + offset = 0 : rxoverrun + offset = 1 : rxunderrun + offset = 2 : rxmalform in case of rxpwcnt_info2_cnt1 side: + offset = 0 : Unused + offset = 1 : Unused + offset = 2 : rxstray| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pwcntbyte_rw`

* **Address**             : `0x6_A000-0x6_B500(RW)`

* **Formula**             : `0x6_A000 + $pwid`

* **Where**               : 

    * `$pwid(0-5375)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`pwcntbyte`| txpwcntbyte (rxpwcntbyte)| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Byte Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pwcntbyte_rw`

* **Address**             : `0x7_2000-0x7_3500(RW)`

* **Formula**             : `0x7_2000 + $pwid`

* **Where**               : 

    * `$pwid(0-5375)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`pwcntbyte`| txpwcntbyte (rxpwcntbyte)| `RW`| `0x0`| `0x0 End: Begin:`|

###PMR Error Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_pmr_cnt0_rw`

* **Address**             : `0x3_B040-0x3_B06F(RW)`

* **Formula**             : `0x3_B040 + 16*$offset + $lineid`

* **Where**               : 

    * `$offset(0-2)`

    * `$lineid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[31:00]`|`pmr_err_cnt`| in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 : rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err| `RW`| `0x0`| `0x0 End: Begin:`|

###PMR Error Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_pmr_cnt0_rw`

* **Address**             : `0xB_B040-0xB_B06F(RW)`

* **Formula**             : `0xB_B040 + 16*$offset + $lineid`

* **Where**               : 

    * `$offset(0-2)`

    * `$lineid(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[31:00]`|`pmr_err_cnt`| in case of pmr_err_cnt0 side: + offset = 0 : rei_l + offset = 1 : b2 + offset = 2 : b1 in case of pmr_err_cnt1 side: + offset = 0 : rei_l_block_err + offset = 1 : b2_block_err + offset = 2 : b1_block_err| `RW`| `0x0`| `0x0 End: Begin:`|

###POH path sts ERDI Counter1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pohpmdat_sts_ber_erdi1_rw`

* **Address**             : `0x3_D800-0x3_DDFF (RW)`

* **Formula**             : `0x3_D800 + 512*$offset +  64*$slcid + $stsid`

* **Where**               : 

    * `$stsid(0-63)`

    * `$offset(0-2)`

    * `$slcid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_ber_erdi`| in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : sts_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : sts_bip(B3) + offset = 1 : Unused + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###POH path sts ERDI Counter1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pohpmdat_sts_ber_erdi1_rw`

* **Address**             : `0xB_D800-0xB_DDFF (RW)`

* **Formula**             : `0xB_D800 + 512*$offset +  64*$slcid + $stsid`

* **Where**               : 

    * `$stsid(0-63)`

    * `$offset(0-2)`

    * `$slcid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_ber_erdi`| in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : sts_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : sts_bip(B3) + offset = 1 : Unused + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###POH path vt ERDI Counter0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pohpmdat_vt_ber_erdi0_rw`

* **Address**             : `0x6_4000 - 0x6_7FFF(RW)`

* **Formula**             : `0x6_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$slcid(0-3)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_ber_erdi`| in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###POH path vt ERDI Counter0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pohpmdat_vt_ber_erdi0_rw`

* **Address**             : `0xE_4000 - 0xE_7FFF(RW)`

* **Formula**             : `0xE_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$slcid(0-3)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_ber_erdi`| in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###POH path vt ERDI Counter1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pohpmdat_vt_ber_erdi1_rw`

* **Address**             : `0x4_4000 - 0x4_7FFF(RW)`

* **Formula**             : `0x4_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$slcid(0-3)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_ber_erdi`| in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###POH path vt ERDI Counter1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pohpmdat_vt_ber_erdi1_rw`

* **Address**             : `0xC_4000 - 0xC_7FFF(RW)`

* **Formula**             : `0xC_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$slcid(0-3)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`pohpath_ber_erdi`| in case of pohpath_ber_erdi_cnt2 side: + offset = 0 : vt_rei + offset = 1 : Unused + offset = 2 : Unused in case of pohpath_ber_erdi_cnt3 side: + offset = 0 : vt_bip + offset = 1 : Unused + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter 0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt0_rw`

* **Address**             : `0x5_4000-0x5_7EFF(RW)`

* **Formula**             : `0x5_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$slcid(0-3)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset = 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset = 0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter 0

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt0_rw`

* **Address**             : `0xD_4000-0xD_7EFF(RW)`

* **Formula**             : `0xD_4000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$slcid(0-3)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset = 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset = 0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter 1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt1_rw`

* **Address**             : `0x4_C000-0x4_FEFF(RW)`

* **Formula**             : `0x4_C000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$slcid(0-3)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset = 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset = 0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###POH vt pointer Counter 1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_vt_cnt1_rw`

* **Address**             : `0xC_C000-0xC_FEFF(RW)`

* **Formula**             : `0xC_C000 + 5376*$offset + 672*$slcid +  28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$offset(0-2)`

    * `$slcid(0-3)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_vt_cnt`| in case of poh_vt_cnt0 side: + offset = 0 : pohtx_vt_dec + offset = 1 : pohrx_vt_dec + offset = 2 : Unused in case of poh_vt_cnt1 side: + offset = 0 : pohtx_vt_inc + offset = 1 : pohrx_vt_inc + offset = 2 : Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt_rw`

* **Address**             : `0x3_F800-0x3_FDFF(RW)`

* **Formula**             : `0x3_F800 + 512*$offset +  64*$slcid + $stsid`

* **Where**               : 

    * `$stsid(0-63)`

    * `$offset(0-2)`

    * `$slcid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_sts_cnt`| in case of poh_sts_cnt0 side: + offset = 0 :  pohtx_sts_dec + offset = 1 :  pohrx_sts_dec + offset = 2 :  Unused in case of poh_sts_cnt1 side: + offset = 0 :  pohtx_sts_inc + offset = 1 :  pohrx_sts_inc + offset = 2 :  Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###POH sts pointer Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_poh_sts_cnt_rw`

* **Address**             : `0xB_F800-0xB_FDFF(RW)`

* **Formula**             : `0xB_F800 + 512*$offset +  64*$slcid + $stsid`

* **Where**               : 

    * `$stsid(0-63)`

    * `$offset(0-2)`

    * `$slcid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`poh_sts_cnt`| in case of poh_sts_cnt0 side: + offset = 0 :  pohtx_sts_dec + offset = 1 :  pohrx_sts_dec + offset = 2 :  Unused in case of poh_sts_cnt1 side: + offset = 0 :  pohtx_sts_inc + offset = 1 :  pohrx_sts_inc + offset = 2 :  Unused| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 cntval Counter Bus1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de1cntval30_rw`

* **Address**             : `0x3_0800-0x3_0FDF(RW)`

* **Formula**             : `0x3_0800 + 4096*$slcid + 672*$offset + 28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$slcid(0-3)`

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`ds1_cntval_bus1_cnt`| in case of ds1_cntval_bus1_cnt0 side: + offset = 0 : fe + offset = 1 : rei + offset = 2 : unused in case of ds1_cntval_bus1_cnt1 side: + offset = 0 : crc + offset = 1 : lcv + offset = 2 : unused| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 cntval Counter Bus1

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de1cntval30_rw`

* **Address**             : `0x3_1800-0x3_1FDF(RW)`

* **Formula**             : `0x3_1800 + 4096*$slcid + 672*$offset + 28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$slcid(0-3)`

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`ds1_cntval_bus1_cnt`| in case of ds1_cntval_bus1_cnt0 side: + offset = 0 : fe + offset = 1 : rei + offset = 2 : unused in case of ds1_cntval_bus1_cnt1 side: + offset = 0 : crc + offset = 1 : lcv + offset = 2 : unused| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 cntval Counter Bus2

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de1cntval74_rw`

* **Address**             : `0x3_4800-0x3_4FDF(RW)`

* **Formula**             : `0x3_4800 + 4096*$slcid + 672*$offset + 28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$slcid(0-3)`

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`ds1_cntval_bus2_cnt`| in case of ds1_cntval_bus2_cnt0 side: + offset = 0 : fe + offset = 1 : rei + offset = 2 : unused in case of ds1_cntval_bus2_cnt1 side: + offset = 0 : crc + offset = 1 : lcv + offset = 2 : unused| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds1 cntval Counter Bus2

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de1cntval74_rw`

* **Address**             : `0x3_5800-0x3_5FDF(RW)`

* **Formula**             : `0x3_5800 + 4096*$slcid + 672*$offset + 28*stsid + 4*vtgid + vtid`

* **Where**               : 

    * `$slcid(0-3)`

    * `$offset(0-2)`

    * `$stsid(0-23)`

    * `$vtgid(0-6)`

    * `$vtid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`ds1_cntval_bus2_cnt`| in case of ds1_cntval_bus2_cnt0 side: + offset = 0 : fe + offset = 1 : rei + offset = 2 : unused in case of ds1_cntval_bus2_cnt1 side: + offset = 0 : crc + offset = 1 : lcv + offset = 2 : unused| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds3 cntval Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de3cnt0_rw`

* **Address**             : `0x3_8800-0x3_8DFF(RW)`

* **Formula**             : `0x3_8800 + 512*$offset +  64*$slcid + $stsid`

* **Where**               : 

    * `$stsid(0-63)`

    * `$offset(0-2)`

    * `$slcid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`ds3_cntval_cnt`| in case of ds3_cntval_cnt0 side: + offset = 0 : fe + offset = 1 : pb + offset = 2 : lcv in case of ds3_cntval_cnt1 side: + offset = 0 : rei + offset = 1 : cb + offset = 2 : unused| `RW`| `0x0`| `0x0 End: Begin:`|

###PDH ds3 cntval Counter

* **Description**           

The register count information as below. Depending on offset it 's the events specify.


* **RTL Instant Name**    : `upen_pdh_de3cnt0_rw`

* **Address**             : `0xB_8800-0xB_8DFF(RW)`

* **Formula**             : `0xB_8800 + 512*$offset +  64*$slcid + $stsid`

* **Where**               : 

    * `$stsid(0-63)`

    * `$offset(0-2)`

    * `$slcid(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17:00]`|`ds3_cntval_cnt`| in case of ds3_cntval_cnt0 side: + offset = 0 : fe + offset = 1 : pb + offset = 2 : lcv in case of ds3_cntval_cnt1 side: + offset = 0 : rei + offset = 1 : cb + offset = 2 : unused| `RW`| `0x0`| `0x0 End: Begin:`|

###Global PMC 2page Interrupt Sticky

* **Description**           

The register provides hold register from [63:32]


* **RTL Instant Name**    : `upen_glbpmcint`

* **Address**             : `0x3_BFFB`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `7`
* **Register Type**       : `R1C`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`pmrintsticky`| Pmr     Interrupt Sticky  1: Set  0: Clear| `R1C`| `0x0`| `0x0`|
|`[5]`|`pohpmvtintsticky`| Pohpmvt Interrupt Sticky  1: Set  0: Clear| `R1C`| `0x0`| `0x0`|
|`[4]`|`pohpmstsintsticky`| Pohpmsts Interrupt Sticky  1: Set  0: Clear| `R1C`| `0x0`| `0x0`|
|`[3]`|`pohintsticky`| Poh    Interrupt Sticky  1: Set  0: Clear| `R1C`| `0x0`| `0x0`|
|`[2]`|`pdhde1intsticky`| Pdhde1 Interrupt Sticky  1: Set  0: Clear| `R1C`| `0x0`| `0x0`|
|`[1]`|`pdhde3intsticky`| Pdhde3 Interrupt Sticky  1: Set  0: Clear| `R1C`| `0x0`| `0x0`|
|`[0]`|`pwcntintsticky`| Pwcnt  Interrupt Sticky  1: Set  0: Clear| `R1C`| `0x0`| `0x0 End:`|

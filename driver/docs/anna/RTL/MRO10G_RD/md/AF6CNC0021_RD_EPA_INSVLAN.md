## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2016-10-04|AF6Project|Initial version|




##AF6CNC0021_RD_EPA_INSVLAN
####Register Table

|Name|Address|
|-----|-----|
|`CONFIG VLAN INSERTED`|`0x00-0x0F`|
|`CONFIG ENABLE INSERT VLAN TPID`|`0x10`|
|`CONFIG ENABLE INSERT VLAN TPID`|`0x11`|
|`CONFIG ENABLE INSERT VLAN TPID`|`0x12`|


###CONFIG VLAN INSERTED

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_cfgvlan`

* **Address**             : `0x00-0x0F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:13]`|`prior`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE INSERT VLAN TPID

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_enbtpid`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`out_enbtpid`| bit corresponding port to config ( bit 0 is port 0),(0)  use config TPID1, (1) use config TPID 2| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE INSERT VLAN TPID

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_enbtpid`

* **Address**             : `0x11`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`out_cfgtpid2`| valud TPID 2| `R/W`| `0x0`| `0x0`|
|`[15:0]`|`out_cfgtpid1`| value TPID 1| `R/W`| `0x8100`| `0x8100 End: Begin:`|

###CONFIG ENABLE INSERT VLAN TPID

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_enbins`

* **Address**             : `0x12`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`out_enbins`| bit corresponding port to config ( bit 0 is port 0), (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0 End:`|

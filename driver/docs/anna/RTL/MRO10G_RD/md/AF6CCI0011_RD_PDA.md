## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_PDA
####Register Table

|Name|Address|
|-----|-----|
|`Pseudowire PDA Jitter Buffer Control`|`0x00010000 - 0x00011FFF #The address format for these registers is 0x00010000 + PWID`|
|`Pseudowire PDA Jitter Buffer Status`|`0x00014000 - 0x00015FFF #The address format for these registers is 0x00014000 + PWID`|
|`Pseudowire PDA Reorder Control`|`0x00020000 - 0x00021FFF #The address format for these registers is 0x00020000 + PWID`|
|`Pseudowire PDA Low Order TDM mode Control`|`0x00030000 - 0x0003FFFF #The address format for low order path is 0x00030000 +  LoOc48ID*0x1000 + LoOrderoc24Slice*0x400 + TdmOc24Pwid`|
|`Pseudowire PDA Low Order TDM Look Up Control`|`0x00040000 - 0x00043FFF #The address format for low order path is 0x00040000 +  LoOc48ID*0x800 + LoOc24Slice*0x400 + TdmOc24Pwid`|
|`Pseudowire PDA High Order TDM Look Up Control`|`0x00048000 - 0x000481FF #The address format for high order path is 0x00048000 +  HoOc48ID*0x40 + HoMasSTS`|
|`Pseudowire PDA High Order TDM mode Control`|`0x00050000 - 0x000501FF #The address format is 0x00050000 +  HoOc48ID*0x100 + HoMasSTS`|
|`PDA Hold Register Status`|`0x000000 - 0x000002`|
|`Pseudowire PDA per OC48 Low Order TDM PRBS J1 V5 Monitor Status`|`0x00081000 - 0x0008FFFF`|
|`Pseudowire PDA per OC48 Low Order TDM PRBS Generator Control`|`0x00080800 - 0x0008FFFF`|
|`Pseudowire PDA ECC CRC Parity Control`|`0x00008`|
|`Pseudowire PDA ECC CRC Parity Disable Control`|`0x00009`|
|`Pseudowire PDA ECC CRC Parity Sticky`|`0x0000A`|
|`Read HA Address Bit3_0 Control`|`0x01000`|
|`Read HA Address Bit7_4 Control`|`0x01010`|
|`Read HA Address Bit11_8 Control`|`0x01020`|
|`Read HA Address Bit15_12 Control`|`0x01030`|
|`Read HA Address Bit19_16 Control`|`0x01040`|
|`Read HA Address Bit23_20 Control`|`0x01050`|
|`Read HA Address Bit24 and Data Control`|`0x01060`|
|`Read HA Hold Data63_32`|`0x01070`|
|`Read HA Hold Data95_64`|`0x01071`|
|`Read HA Hold Data127_96`|`0x01072`|
|`Pseudowire PDA per OC48 Low Order TDM CESoP Small DS0 Control`|`0x00080000 - 0x0008FFFF`|
|`Pseudowire PDA Jitter Buffer Watermark Status`|`0x0001C000 - 0x0001D7FF #The address format for these registers is 0x0001C000 + PWID`|
|`Pseudowire PDA TOHPW Look Up Control`|`0x000481C0 - 0x000481C7`|
|`Pseudowire PDA TOHPW mode Control`|`0x00050700 - 0x00050707`|


###Pseudowire PDA Jitter Buffer Control

* **Description**           

This register configures jitter buffer parameters per pseudo-wire


* **RTL Instant Name**    : `ramjitbufcfg`

* **Address**             : `0x00010000 - 0x00011FFF #The address format for these registers is 0x00010000 + PWID`

* **Formula**             : `0x00010000 +  PWID`

* **Where**               : 

    * `$PWID(0-6143): Pseudowire ID`

* **Width**               : `57`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[56]`|`pwlowds0mode`| Pseudo-wire CES Low DS0 mode| `RW`| `0x0`| `0x0 1: CESoP Pseodo-wire with NxDS0 <= 3 0: Other Pseodo-wire mode`|
|`[55]`|`pwcepmode`| Pseudo-wire CEP mode indication| `RW`| `0x0`| `0x0 1: CEP Pseodo-wire 0: CES Pseodo-wire`|
|`[54:53]`|`pwholooc48id`| Indicate 4x Hi order OC48 or 4x Low order OC48 slice| `RW`| `0x0`| `0x0`|
|`[52]`|`pwloslice24sel`| Pseudo-wire Low order OC24 slice selection, only valid in Lo Pseudo-wire 1: This PW belong to Slice24 that trasnport STS 1,3,5,...,47 within a Lo OC48 0: This PW belong to Slice24 that trasnport STS 0,2,4,...,46 within a Lo OC48| `RW`| `0x0`| `0x0`|
|`[51:42]`|`pwtdmlineid`| Pseudo-wire(PW) corresponding TDM line ID. If the PW belong to Low order path, this is the OC24 TDM line ID that is using in Lo CDR,PDH and MAP. If the PW belong to Hi order CEP path, this is the OC48 master STS ID| `RW`| `0x0`| `0x0`|
|`[41]`|`pweparen`| Pseudo-wire EPAR timing mode enable 1: Enable EPAR timing mode 0: Disable EPAR timing mode| `RW`| `0x0`| `0x0`|
|`[40]`|`pwhilopathind`| Pseudo-wire belong to Hi-order or Lo-order path 1: Pseudo-wire belong to Hi-order path 0: Pseudo-wire belong to Lo-order path| `RW`| `0x0`| `0x0`|
|`[39:26]`|`pwpayloadlen`| TDM Payload of pseudo-wire in byte unit| `RW`| `0x0`| `0x0`|
|`[25:13]`|`pdvsizeinpkunit`| Pdv size in packet unit. This parameter is to prevent packet delay variation(PDV) from PSN. PdvSizePk is packet delay variation measured in packet unit. The formula is as below: PdvSizeInPkUnit = (PwSpeedinKbps * PdvInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps * PdvInUsus)%(8000*PwPayloadLen) !=0)| `RW`| `0x0`| `0x0`|
|`[12:0]`|`jitbufsizeinpkunit`| Jitter buffer size in packet unit. This parameter is mostly double of PdvSizeInPkUnit. The formula is as below: JitBufSizeInPkUnit = (PwSpeedinKbps * JitBufInUsUnit)/(8000*PwPayloadLen) + ((PwSpeedInKbps * JitBufInUsus)%(8000*PwPayloadLen) !=0)| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA Jitter Buffer Status

* **Description**           

This register shows jitter buffer status per pseudo-wire


* **RTL Instant Name**    : `ramjitbufsta`

* **Address**             : `0x00014000 - 0x00015FFF #The address format for these registers is 0x00014000 + PWID`

* **Formula**             : `0x00014000 +  PWID`

* **Where**               : 

    * `$PWID(0-6143): Pseudowire ID`

* **Width**               : `55`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[54:20]`|`jitbufhwcomsta`| Harware debug only status| `RO`| `0x0`| `0x0`|
|`[19:4]`|`jitbufnumpk`| Current number of packet in jitter buffer| `RW`| `0x0`| `0x0`|
|`[3]`|`jitbuffull`| Jitter Buffer full status| `RW`| `0x0`| `0x0`|
|`[2:0]`|`jitbufstate`| Jitter buffer state machine status 0: START 1: FILL 2: READY 3: READ 4: LOST 5: NEAR_EMPTY Others: NOT VALID| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA Reorder Control

* **Description**           

This register configures reorder parameters per pseudo-wire


* **RTL Instant Name**    : `ramreorcfg`

* **Address**             : `0x00020000 - 0x00021FFF #The address format for these registers is 0x00020000 + PWID`

* **Formula**             : `0x00020000 +  PWID`

* **Where**               : 

    * `$PWID(0-6143): Pseudowire ID`

* **Width**               : `26`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[25:17]`|`pwsetlofsinpk`| Number of consecutive lost packet to declare lost of packet state| `RW`| `0x0`| `0x0`|
|`[16:9]`|`pwsetlopsinmsec`| Number of empty time to declare lost of packet synchronization| `RW`| `0x0`| `0x0`|
|`[8]`|`pwreoren`| Set 1 to enable reorder| `RW`| `0x0`| `0x0`|
|`[7:0]`|`pwreortimeout`| Reorder timeout in 512us unit to detect lost packets. The formula is as below: ReorTimeout = ((Min(31,PdvSizeInPk) * PwPayloadLen * 16)/PwSpeedInKbps| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA Low Order TDM mode Control

* **Description**           

This register configure TDM mode for interworking between Pseudowire and Lo TDM


* **RTL Instant Name**    : `ramlotdmmodecfg`

* **Address**             : `0x00030000 - 0x0003FFFF #The address format for low order path is 0x00030000 +  LoOc48ID*0x1000 + LoOrderoc24Slice*0x400 + TdmOc24Pwid`

* **Formula**             : `0x00030000 + $LoOc48ID*4096 + $LoOrderoc24Slice*2048 + $TdmOc24Pwid`

* **Where**               : 

    * `$LoOc48ID(0-3): LoOC48 ID`

    * `$LoOrderoc24Slice(0-1): Lo-order slice oc24`

    * `$TdmOc24Pwid(0-1343): TDM OC24 slice PWID`

* **Width**               : `29`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28]`|`pdardioff`| Set 1 to disable sending RDI from PW to TDM| `RW`| `0x0`| `0x0`|
|`[27:22]`|`pdanxds0`| Number of DS0 allocated for CESoP PW| `RO`| `0x0`| `0x0`|
|`[21:14]`|`pdaidlecode`| Idle pattern to replace data in case of Lost/Lbit packet| `RW`| `0x0`| `0x0`|
|`[13]`|`pdaaisuneqoff`| Set 1 to disable sending AIS/Unequip from PW to TDM| `RW`| `0x0`| `0x0`|
|`[12:11]`|`pdarepmode`| Mode to replace lost and Lbit packet 0: Replace by replaying previous good packet (often for voice or video application) 1: Replace by AIS (default) 2: Replace by a configuration idle code Replace by a configuration idle code| `RW`| `0x0`| `0x0`|
|`[10]`|`reserved`| *n/a*| `RO`| `0x0`| `0x0`|
|`[9:5]`|`pdastsid`| Only used in DS3/E3 SAToP and TU3/VC3 CEP basic, corresponding Slice OC24 STS ID of PW circuit| `RW`| `0x0`| `0x0`|
|`[4:1]`|`pdamode`| TDM Payload De-Assembler modes 0: DS1/E1 SAToP 2: CESoP without CAS 8: VC3/TU3 CEP basic 12: VC11/VC12 CEP basic 15: DS3/E3 SAToP| `RW`| `0x0`| `0x0`|
|`[0]`|`pdafracebmen`| EBM enable in CEP fractional mode 0: EBM disable 1: EBM enable| `RO`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA Low Order TDM Look Up Control

* **Description**           

This register configure lookup from TDM PWID to global 6144 PWID


* **RTL Instant Name**    : `ramlotdmlkupcfg`

* **Address**             : `0x00040000 - 0x00043FFF #The address format for low order path is 0x00040000 +  LoOc48ID*0x800 + LoOc24Slice*0x400 + TdmOc24Pwid`

* **Formula**             : `0x00040000 + LoOc48ID*4096 + LoOc24Slice*2048 + TdmOc24PwID`

* **Where**               : 

    * `$LoOc48ID(0-3): LoOC48 ID`

    * `$LoOc24Slice(0-1): Lo-order slice oc24`

    * `TdmOc24PwID(0-1343): TDM OC24 slice PWID`

* **Width**               : `13`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12:0]`|`pwid`| Flat 8192 PWID| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA High Order TDM Look Up Control

* **Description**           

This register configure lookup from HO TDM master STSIS to global 6144 PWID


* **RTL Instant Name**    : `ramhotdmlkupcfg`

* **Address**             : `0x00048000 - 0x000481FF #The address format for high order path is 0x00048000 +  HoOc48ID*0x40 + HoMasSTS`

* **Formula**             : `0x00048000 + HoOc48ID*0x40 + HoMasSTS`

* **Where**               : 

    * `$HoOc48ID(0-3): HoOC48 ID`

    * `$HoMasSTS(0-47): HO TDM per OC48 master STS ID`

* **Width**               : `13`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12:0]`|`pwid`| Flat 6144 PWID| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA High Order TDM mode Control

* **Description**           

This register configure TDM mode for interworking between Pseudowire and Ho TDM


* **RTL Instant Name**    : `ramhotdmmodecfg`

* **Address**             : `0x00050000 - 0x000501FF #The address format is 0x00050000 +  HoOc48ID*0x100 + HoMasSTS`

* **Formula**             : `0x00050000 + HoOc48ID*0x100 + HoMasSTS`

* **Where**               : 

    * `$HoOc48ID(0-3): HoOC48 ID`

    * `$HoMasSTS(0-47): HO TDM per OC48 master STS ID`

* **Width**               : `13`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12:5]`|`pdahoidlecode`| Idle pattern to replace data in case of Lost/Lbit packet| `RW`| `0x0`| `0x0`|
|`[4]`|`pdahoaisuneqoff`| Set 1 to disable sending AIS/Unequip from PW to Ho TDM| `RW`| `0x0`| `0x0`|
|`[3:2]`|`pdahorepmode`| HO Mode to replace lost and Lbit packet 0: Replace by replaying previous good packet (often for voice or video application) 1: Replace by AIS (default) 2: Replace by a configuration idle code Replace by a configuration idle code| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pdahomode`| TDM HO TDM mode 0: STS1 CEP basic 1: STS3/STS6 CEP basic 2: STS9/STS12/STS15 CEP basic 3: STS18 to STS48 CEP basic| `RW`| `0x0`| `0x0 End: Begin:`|

###PDA Hold Register Status

* **Description**           

This register using for hold remain that more than 128bits


* **RTL Instant Name**    : `pda_hold_status`

* **Address**             : `0x000000 - 0x000002`

* **Formula**             : `0x000000 +  HID`

* **Where**               : 

    * `$HID(0-2): Hold ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pdaholdstatus`| Hold 32bits| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA per OC48 Low Order TDM PRBS J1 V5 Monitor Status

* **Description**           

This register show the PRBS monitor status after PDA


* **RTL Instant Name**    : `ramlotdmprbsmon`

* **Address**             : `0x00081000 - 0x0008FFFF`

* **Formula**             : `0x00081000 + LoOc48ID*16384 + LoOc24Slice*8192 + TdmOc24PwID`

* **Where**               : 

    * `$LoOc48ID(0-3): LoOC48 ID`

    * `$LoOc24Slice(0-1): Lo-order slice oc24`

    * `TdmOc24PwID(0-1343): TDM OC24 slice PWID`

* **Width**               : `18`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17]`|`pdaprbsb3v5err`| PDA PRBS or B3 or V5 monitor error| `RC`| `0x0`| `0x0`|
|`[16]`|`pdaprbssyncsta`| PDA PRBS sync status| `RW`| `0x0`| `0x0`|
|`[15:0]`|`pdaprbsvalue`| PDA PRBS monitor value| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA per OC48 Low Order TDM PRBS Generator Control

* **Description**           

This register show the PRBS monitor status after PDA


* **RTL Instant Name**    : `ramlotdmprbsgen`

* **Address**             : `0x00080800 - 0x0008FFFF`

* **Formula**             : `0x00080800 + LoOc48ID*16384 + LoOc24Slice*0x8192 + TdmOc24PwID`

* **Where**               : 

    * `$LoOc48ID(0-3): LoOC48 ID`

    * `$LoOc24Slice(0-1): Lo-order slice oc24`

    * `TdmOc24PwID(0-1343): TDM OC24 slice PWID`

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`pdaseqmode`| PDA Sequential data mode| `RW`| `0x0`| `0x0`|
|`[0]`|`pdaprbsgenen`| PDA PRBS Generation enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA ECC CRC Parity Control

* **Description**           

This register configures PDA ECC CRC and Parity.


* **RTL Instant Name**    : `pda_config_ecc_crc_parity_control`

* **Address**             : `0x00008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `7`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`pdaforceecccor`| PDA force link list Ecc error correctable   1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[5]`|`pdaforceeccerr`| PDA force link list Ecc error noncorrectable   1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[4]`|`pdaforcecrcerr`| PDA force data CRC error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[3]`|`pdaforcelotdmparerr`| Pseudowire PDA Low Order TDM mode Control force parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[2]`|`pdaforcetdmlkparerr`| Pseudowire PDA Lo and Ho TDM Look Up Control force parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[1]`|`pdaforcejitbufparerr`| Pseudowire PDA Jitter Buffer Control force parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[0]`|`pdaforcereorderparerr`| Pseudowire PDA Reorder Control force parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA ECC CRC Parity Disable Control

* **Description**           

This register configures PDA ECC CRC and Parity Disable.


* **RTL Instant Name**    : `pda_config_ecc_crc_parity_disable_control`

* **Address**             : `0x00009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `7`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`pdadisableecccor`| PDA disable link list Ecc error correctable   1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[5]`|`pdadisableeccerr`| PDA disable link list Ecc error noncorrectable   1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[4]`|`pdadisablecrcerr`| PDA disable data CRC error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[3]`|`pdadisablelotdmparerr`| Pseudowire PDA Low Order TDM mode Control disable parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[2]`|`pdadisabletdmlkparerr`| Pseudowire PDA Lo and Ho TDM Look Up Control disable parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[1]`|`pdadisablejitbufparerr`| Pseudowire PDA Jitter Buffer Control disable parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[0]`|`pdadisablereorderparerr`| Pseudowire PDA Reorder Control disable parity error  1: Set  0: Clear| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA ECC CRC Parity Sticky

* **Description**           

This register configures PDA ECC CRC and Parity.


* **RTL Instant Name**    : `pda_config_ecc_crc_parity_sticky`

* **Address**             : `0x0000A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `7`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`pdastickyecccor`| PDA sticky link list Ecc error correctable   1: Set  0: Clear| `W1C`| `0x0`| `0x0`|
|`[5]`|`pdastickyeccerr`| PDA sticky link list Ecc error noncorrectable   1: Set  0: Clear| `W1C`| `0x0`| `0x0`|
|`[4]`|`pdastickycrcerr`| PDA sticky data CRC error  1: Set  0: Clear| `W1C`| `0x0`| `0x0`|
|`[3]`|`pdastickylotdmparerr`| Pseudowire PDA Low Order TDM mode Control sticky parity error  1: Set  0: Clear| `W1C`| `0x0`| `0x0`|
|`[2]`|`pdastickytdmlkparerr`| Pseudowire PDA Lo and Ho TDM Look Up Control sticky parity error  1: Set  0: Clear| `W1C`| `0x0`| `0x0`|
|`[1]`|`pdastickyjitbufparerr`| Pseudowire PDA Jitter Buffer Control sticky parity error  1: Set  0: Clear| `W1C`| `0x0`| `0x0`|
|`[0]`|`pdastickyreorderparerr`| Pseudowire PDA Reorder Control sticky parity error  1: Set  0: Clear| `W1C`| `0x0`| `0x0 End: Begin:`|

###Read HA Address Bit3_0 Control

* **Description**           

This register is used to send HA read address bit3_0 to HA engine


* **RTL Instant Name**    : `rdha3_0_control`

* **Address**             : `0x01000`

* **Formula**             : `0x01000 + $HaAddr3_0`

* **Where**               : 

    * `$HaAddr3_0(0-15): HA Address Bit3_0`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr3_0`| Read value will be 0x01000 plus HaAddr3_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit7_4 Control

* **Description**           

This register is used to send HA read address bit7_4 to HA engine


* **RTL Instant Name**    : `rdha7_4_control`

* **Address**             : `0x01010`

* **Formula**             : `0x01010 + $HaAddr7_4`

* **Where**               : 

    * `$HaAddr7_4(0-15): HA Address Bit7_4`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr7_4`| Read value will be 0x01000 plus HaAddr7_4| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit11_8 Control

* **Description**           

This register is used to send HA read address bit11_8 to HA engine


* **RTL Instant Name**    : `rdha11_8_control`

* **Address**             : `0x01020`

* **Formula**             : `0x01020 + $HaAddr11_8`

* **Where**               : 

    * `$HaAddr11_8(0-15): HA Address Bit11_8`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr11_8`| Read value will be 0x01000 plus HaAddr11_8| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit15_12 Control

* **Description**           

This register is used to send HA read address bit15_12 to HA engine


* **RTL Instant Name**    : `rdha15_12_control`

* **Address**             : `0x01030`

* **Formula**             : `0x01030 + $HaAddr15_12`

* **Where**               : 

    * `$HaAddr15_12(0-15): HA Address Bit15_12`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr15_12`| Read value will be 0x01000 plus HaAddr15_12| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit19_16 Control

* **Description**           

This register is used to send HA read address bit19_16 to HA engine


* **RTL Instant Name**    : `rdha19_16_control`

* **Address**             : `0x01040`

* **Formula**             : `0x01040 + $HaAddr19_16`

* **Where**               : 

    * `$HaAddr19_16(0-15): HA Address Bit19_16`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr19_16`| Read value will be 0x01000 plus HaAddr19_16| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit23_20 Control

* **Description**           

This register is used to send HA read address bit23_20 to HA engine


* **RTL Instant Name**    : `rdha23_20_control`

* **Address**             : `0x01050`

* **Formula**             : `0x01050 + $HaAddr23_20`

* **Where**               : 

    * `$HaAddr23_20(0-15): HA Address Bit23_20`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr23_20`| Read value will be 0x01000 plus HaAddr23_20| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit24 and Data Control

* **Description**           

This register is used to send HA read address bit24 to HA engine to read data


* **RTL Instant Name**    : `rdha24data_control`

* **Address**             : `0x01060`

* **Formula**             : `0x01060 + $HaAddr24`

* **Where**               : 

    * `$HaAddr24(0-1): HA Address Bit24`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata31_0`| HA read data bit31_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data63_32

* **Description**           

This register is used to read HA dword2 of data.


* **RTL Instant Name**    : `rdha_hold63_32`

* **Address**             : `0x01070`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata63_32`| HA read data bit63_32| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data95_64

* **Description**           

This register is used to read HA dword3 of data.


* **RTL Instant Name**    : `rdindr_hold95_64`

* **Address**             : `0x01071`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata95_64`| HA read data bit95_64| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data127_96

* **Description**           

This register is used to read HA dword4 of data.


* **RTL Instant Name**    : `rdindr_hold127_96`

* **Address**             : `0x01072`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata127_96`| HA read data bit127_96| `RW`| `0xx`| `0xx End: Begin:`|

###Pseudowire PDA per OC48 Low Order TDM CESoP Small DS0 Control

* **Description**           

This register show the PRBS monitor status after PDA


* **RTL Instant Name**    : `ramlotdmsmallds0control`

* **Address**             : `0x00080000 - 0x0008FFFF`

* **Formula**             : `0x00080000 + LoOc48ID*16384 + LoOc24Slice*8192 + TdmOc24PwID`

* **Where**               : 

    * `$LoOc48ID(0-3): LoOC48 ID`

    * `$LoOc24Slice(0-1): Lo-order slice oc24`

    * `TdmOc24PwID(0-1343): TDM OC24 slice PWID`

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`pdasmallds0en`| PDA CESoP small DS0 enable 1: CESoP Pseodo-wire with NxDS0 <= 3 0: Other Pseodo-wire modes| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA Jitter Buffer Watermark Status

* **Description**           

This register show jitter buffer water mark per pseudo-wire, write to this register will reset water mark


* **RTL Instant Name**    : `ramjitbufwater`

* **Address**             : `0x0001C000 - 0x0001D7FF #The address format for these registers is 0x0001C000 + PWID`

* **Formula**             : `0x0001C000 +  PWID`

* **Where**               : 

    * `$PWID(0-6143): Pseudowire ID`

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:12]`|`jitbufmaxpk`| Water mark Maximum number of packet in jitter buffer| `WRC`| `0x0`| `0x0`|
|`[11:0]`|`jitbufminpk`| Water mark Minimum number of packet in jitter buffer| `WRC`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA TOHPW Look Up Control

* **Description**           

This register configure lookup from TOH PWID to global 6144 PWID


* **RTL Instant Name**    : `ramtohpwlkupcfg`

* **Address**             : `0x000481C0 - 0x000481C7`

* **Formula**             : `0x000481C0 + $TohPwid`

* **Where**               : 

    * `$TohPwid(0-7): TOH PW ID`

* **Width**               : `13`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12:0]`|`pwid`| Flat 6144 PWID| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire PDA TOHPW mode Control

* **Description**           

This register configure mode TOHPW


* **RTL Instant Name**    : `ramtohpwmodecfg`

* **Address**             : `0x00050700 - 0x00050707`

* **Formula**             : `0x00050700 + $TohPwid`

* **Where**               : 

    * `$TohPwid(0-7): TOH PW ID`

* **Width**               : `13`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12:5]`|`pdatohidlecode`| Idle pattern to replace data in case of Lost/Lbit packet| `RW`| `0x0`| `0x0`|
|`[4]`|`pdatohaisoff`| Set 1 to disable sending AIS from PW to OCN| `RW`| `0x0`| `0x0`|
|`[3:2]`|`pdatohrepmode`| Mode to replace lost and Lbit packet 0: Replace by replaying previous good packet (often for voice or video application) 1: Replace by AIS (default) 2: Replace by a configuration idle code Replace by a configuration idle code| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pdatohmode`| TOH  mode must set to 0| `RW`| `0x0`| `0x0 End:`|

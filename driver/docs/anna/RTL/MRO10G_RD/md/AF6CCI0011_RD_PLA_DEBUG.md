## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_PLA_DEBUG
####Register Table

|Name|Address|
|-----|-----|
|`Payload Assembler Low-Order clk155 Sticky`|`0x0_8000 + $LoOc48Slice*65536`|
|`Payload Assembler High-Order clk311 Sticky`|`0x8_0400-0x8_2C00`|
|`Payload Assembler High-Order Block Process Sticky`|`0x8_0601-0x8_2E01`|
|`Payload Assembler Low-Order Block Process Sticky`|`0x8_8001`|
|`Payload Assembler Interface Sticky`|`0x8_8002`|


###Payload Assembler Low-Order clk155 Sticky

* **Description**           

This register is used to used to sticky some alarms for debug per OC-48 Lo-order @clk155.52 domain


* **RTL Instant Name**    : `pla_lo_clk155_stk`

* **Address**             : `0x0_8000 + $LoOc48Slice*65536`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `14`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13]`|`plalo155oc24_1converr`| OC24#1 convert 155to233 Error| `W2C`| `0x0`| `0x0`|
|`[12]`|`plalo155oc24_0converr`| OC24#0 convert 155to233 Error| `W2C`| `0x0`| `0x0`|
|`[11]`|`plalo155oc24_1pwvld`| OC24#1 input of expected pwid| `W2C`| `0x0`| `0x0`|
|`[10]`|`plalo155oc24_0pwvld`| OC24#0 input of expected pwid| `W2C`| `0x0`| `0x0`|
|`[9]`|`plalo155oc24_0invld`| OC24#0 input data from demap| `W2C`| `0x0`| `0x0`|
|`[8]`|`plalo155oc24_1invld`| OC24#1 input data from demap| `W2C`| `0x0`| `0x0`|
|`[7]`|`plalo155oc24_0infst`| OC24#0 input first-timeslot| `W2C`| `0x0`| `0x0`|
|`[6]`|`plalo155oc24_1infst`| OC24#1 input first-timeslot| `W2C`| `0x0`| `0x0`|
|`[5]`|`plalo155oc24_0inais`| OC24#0 input AIS| `W2C`| `0x0`| `0x0`|
|`[4]`|`plalo155oc24_1inais`| OC24#1 input AIS| `W2C`| `0x0`| `0x0`|
|`[3]`|`plalo155oc24_0inpos`| OC24#0 input CEP Pos Pointer| `W2C`| `0x0`| `0x0`|
|`[2]`|`plalo155oc24_0inneg`| OC24#0 input CEP Neg Pointer| `W2C`| `0x0`| `0x0`|
|`[1]`|`plalo155oc24_0inpos`| OC24#1 input CEP Pos Pointer| `W2C`| `0x0`| `0x0`|
|`[0]`|`plalo155oc24_0inneg`| OC24#1 input CEP Neg Pointer| `W2C`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler High-Order clk311 Sticky

* **Description**           

This register is used to used to sticky some alarms for debug per OC-96 Hi-order @clk311.04 domain


* **RTL Instant Name**    : `pla_ho_clk311_stk`

* **Address**             : `0x8_0400-0x8_2C00`

* **Formula**             : `0x8_0400 + $HoOc96Slice*2048`

* **Where**               : 

    * `$HoOc96Slice(0-5): HO OC-96 slices, there are total 6xOC-96 slices for HO 30G or 4xOC-96 slices for HO 20G`

* **Width**               : `14`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[13]`|`plalo311oc48_1converr`| OC48#1 convert 311to233 Error| `W2C`| `0x0`| `0x0`|
|`[12]`|`plalo311oc48_0converr`| OC48#0 convert 311to233 Error| `W2C`| `0x0`| `0x0`|
|`[11]`|`plalo311oc48_1pwvld`| OC48#1 input of expected pwid| `W2C`| `0x0`| `0x0`|
|`[10]`|`plalo311oc48_0pwvld`| OC48#0 input of expected pwid| `W2C`| `0x0`| `0x0`|
|`[9]`|`plalo311oc48_0invld`| OC48#0 input data from demap| `W2C`| `0x0`| `0x0`|
|`[8]`|`plalo311oc48_1invld`| OC48#1 input data from demap| `W2C`| `0x0`| `0x0`|
|`[7]`|`plalo311oc48_0infst`| OC48#0 input first-timeslot| `W2C`| `0x0`| `0x0`|
|`[6]`|`plalo311oc48_1infst`| OC48#1 input first-timeslot| `W2C`| `0x0`| `0x0`|
|`[5]`|`plalo311oc48_0inais`| OC48#0 input AIS| `W2C`| `0x0`| `0x0`|
|`[4]`|`plalo311oc48_1inais`| OC48#1 input AIS| `W2C`| `0x0`| `0x0`|
|`[3]`|`plalo311oc48_0inpos`| OC48#0 input CEP Pos Pointer| `W2C`| `0x0`| `0x0`|
|`[2]`|`plalo311oc48_0inneg`| OC48#0 input CEP Neg Pointer| `W2C`| `0x0`| `0x0`|
|`[1]`|`plalo311oc48_0inpos`| OC48#1 input CEP Pos Pointer| `W2C`| `0x0`| `0x0`|
|`[0]`|`plalo311oc48_0inneg`| OC48#1 input CEP Neg Pointer| `W2C`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler High-Order Block Process Sticky

* **Description**           

This register is used to used to sticky some alarms for debug per OC-96 Hi-order block processing


* **RTL Instant Name**    : `pla_ho_block_pro_stk`

* **Address**             : `0x8_0601-0x8_2E01`

* **Formula**             : `0x8_0601 + $HoOc96Slice*2048`

* **Where**               : 

    * `$HoOc96Slice(0-5): HO OC-96 slices, there are total 6xOC-96 slices for HO 30G or 4xOC-96 slices for HO 20G`

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7]`|`plahoblkpkinforeq`| OC96 packet infor request| `W2C`| `0x0`| `0x0`|
|`[6]`|`plahoblkpkinfoack`| OC96 packet infor Ack| `W2C`| `0x0`| `0x0`|
|`[5]`|`plahoblkpkinfovld`| OC96 packet infor Valid| `W2C`| `0x0`| `0x0`|
|`[4]`|`plahoblkmaxlength`| OC96 max length violation| `W2C`| `0x0`| `0x0`|
|`[3]`|`plahopla2converr`| OC96 OC48#2 Convert Error| `W2C`| `0x0`| `0x0`|
|`[2]`|`plahopla1converr`| OC96 OC48#1 Convert Error| `W2C`| `0x0`| `0x0`|
|`[1]`|`plahoblkemp`| OC96 block empty| `W2C`| `0x0`| `0x0`|
|`[0]`|`plahoblksame`| OC96 block same| `W2C`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Low-Order Block Process Sticky

* **Description**           

This register is used to used to sticky some alarms for debug low-order block processing


* **RTL Instant Name**    : `pla_lo_block_pro_stk`

* **Address**             : `0x8_8001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[28]`|`plaloblkallcacheemp`| LO all cache empty| `W2C`| `0x0`| `0x0`|
|`[27]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[26]`|`plaloblkmaxlengtherr`| LO Max Length Error| `W2C`| `0x0`| `0x0`|
|`[25]`|`plaloddrmuxrdfull`| LO DDR Mux Read Full| `W2C`| `0x0`| `0x0`|
|`[24]`|`plaloddrmuxwrfull`| LO DDR Mux Write Full| `W2C`| `0x0`| `0x0`|
|`[23]`|`plaloddrstaerr`| LO DDR Status Error| `W2C`| `0x0`| `0x0`|
|`[22]`|`plaloddrbuf1stcaful`| LO DDR Buffer 1st Cache Full| `W2C`| `0x0`| `0x0`|
|`[21]`|`plaloddrbuf2ndcaful`| LO DDR Buffer 2nd Cache Full| `W2C`| `0x0`| `0x0`|
|`[20]`|`plaloddrmuxackful`| LO DDR Mux Ack Full| `W2C`| `0x0`| `0x0`|
|`[19]`|`plaloddrbak7ful`| LO DDR Buffer Bank7 Full| `W2C`| `0x0`| `0x0`|
|`[18]`|`plaloddrbak6ful`| LO DDR Buffer Bank6 Full| `W2C`| `0x0`| `0x0`|
|`[17]`|`plaloddrbak5ful`| LO DDR Buffer Bank5 Full| `W2C`| `0x0`| `0x0`|
|`[16]`|`plaloddrbak4ful`| LO DDR Buffer Bank4 Full| `W2C`| `0x0`| `0x0`|
|`[15]`|`plaloddrbak3ful`| LO DDR Buffer Bank3 Full| `W2C`| `0x0`| `0x0`|
|`[14]`|`plaloddrbak2ful`| LO DDR Buffer Bank2 Full| `W2C`| `0x0`| `0x0`|
|`[13]`|`plaloddrbak1ful`| LO DDR Buffer Bank1 Full| `W2C`| `0x0`| `0x0`|
|`[12]`|`plaloddrbak0ful`| LO DDR Buffer Bank0 Full| `W2C`| `0x0`| `0x0`|
|`[11]`|`plaloblkoutvld`| LO Block output valid| `W2C`| `0x0`| `0x0`|
|`[10]`|`plaloblkca256same`| LO Block Cache256 same| `W2C`| `0x0`| `0x0`|
|`[9]`|`plaloblkca128same`| LO Block Cache128 same| `W2C`| `0x0`| `0x0`|
|`[8]`|`plaloblkca64same`| LO Block Cache64 same| `W2C`| `0x0`| `0x0`|
|`[7]`|`plaloblkpkinfovld`| LO Block Pkt Info valid| `W2C`| `0x0`| `0x0`|
|`[6]`|`plaloblkca256emp`| LO Block Cache256 Empty| `W2C`| `0x0`| `0x0`|
|`[5]`|`plaloblkca128emp`| LO Block Cache128 Empty| `W2C`| `0x0`| `0x0`|
|`[4]`|`plaloblkca64emp`| LO Block Cache64 Empty| `W2C`| `0x0`| `0x0`|
|`[3]`|`plaloblkhiblkemp`| LO Block High Size Block Empty| `W2C`| `0x0`| `0x0`|
|`[2]`|`plaloblkloblkemp`| LO Block Low Size Block Empty| `W2C`| `0x0`| `0x0`|
|`[1]`|`plaloblkhiblksame`| LO Block High Size Block same| `W2C`| `0x0`| `0x0`|
|`[0]`|`plaloblkloblksame`| LO Block Low Size Block same| `W2C`| `0x0`| `0x0 End: Begin:`|

###Payload Assembler Interface Sticky

* **Description**           

This register is used to used to sticky some alarms for debug low-order block processing


* **RTL Instant Name**    : `pla_interface_stk`

* **Address**             : `0x8_8002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`plaport4querdy`| Port4 output Queue Pkt ready| `W2C`| `0x0`| `0x0`|
|`[30]`|`plaport4queack`| Port4 output Queue Pkt ack| `W2C`| `0x0`| `0x0`|
|`[29]`|`plaport3querdy`| Port3 output Queue Pkt ready| `W2C`| `0x0`| `0x0`|
|`[28]`|`plaport3queack`| Port3 output Queue Pkt ack| `W2C`| `0x0`| `0x0`|
|`[27]`|`plaport2querdy`| Port2 output Queue Pkt ready| `W2C`| `0x0`| `0x0`|
|`[26]`|`plaport2queack`| Port2 output Queue Pkt ack| `W2C`| `0x0`| `0x0`|
|`[25]`|`plaport1querdy`| Port1 output Queue Pkt ready| `W2C`| `0x0`| `0x0`|
|`[24]`|`plaport1queack`| Port1 output Queue Pkt ack| `W2C`| `0x0`| `0x0`|
|`[23]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[22]`|`plapsnreqqdr`| Request PSN to QDR| `W2C`| `0x0`| `0x0`|
|`[21]`|`plapsnackqdr`| Ack for PSN request| `W2C`| `0x0`| `0x0`|
|`[20]`|`plapsnvldqdr`| Valid for PSN request| `W2C`| `0x0`| `0x0`|
|`[19]`|`plapkinfreqqdrlenwrerr`| Request write pkt info len err| `W2C`| `0x0`| `0x0`|
|`[18]`|`plapkinfreqqdr`| Request Pkt info to QDR| `W2C`| `0x0`| `0x0`|
|`[17]`|`plapkinfackqdr`| Ack for pkt info request| `W2C`| `0x0`| `0x0`|
|`[16]`|`plapkinfvldqdr`| Valid for pkt info request| `W2C`| `0x0`| `0x0`|
|`[15]`|`plapkinfreqqdrlenrderr`| Request read pkt info len err| `W2C`| `0x0`| `0x0`|
|`[14]`|`plardreqddr`| Request read data to DDR| `W2C`| `0x0`| `0x0`|
|`[13]`|`plardackddr`| Ack for read data request| `W2C`| `0x0`| `0x0`|
|`[12]`|`plardvldddr`| Valid for read data request| `W2C`| `0x0`| `0x0`|
|`[11]`|`plap1pweeoperr`| PWE interface end packet err| `W2C`| `0x0`| `0x0`|
|`[10]`|`plawrreqddr`| Request write data to DDR| `W2C`| `0x0`| `0x0`|
|`[9]`|`plawrackddr`| Ack for write data request| `W2C`| `0x0`| `0x0`|
|`[8]`|`plawrvldddr`| Valid for write data request| `W2C`| `0x0`| `0x0`|
|`[7]`|`plap4pwereq`| Port4 PLA ready packet for PWE| `W2C`| `0x0`| `0x0`|
|`[6]`|`plap4pweget`| Port4 PWE get packet| `W2C`| `0x0`| `0x0`|
|`[5]`|`plap3pwereq`| Port3 PLA ready packet for PWE| `W2C`| `0x0`| `0x0`|
|`[4]`|`plap3pweget`| Port3 PWE get packet| `W2C`| `0x0`| `0x0`|
|`[3]`|`plap2pwereq`| Port2 PLA ready packet for PWE| `W2C`| `0x0`| `0x0`|
|`[2]`|`plap2pweget`| Port2 PWE get packet| `W2C`| `0x0`| `0x0`|
|`[1]`|`plap1pwereq`| Port1 PLA ready packet for PWE| `W2C`| `0x0`| `0x0`|
|`[0]`|`plap1pweget`| Port1 PWE get packet| `W2C`| `0x0`| `0x0`|

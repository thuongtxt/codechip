## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0051_RD_POH_BER
####Register Table

|Name|Address|
|-----|-----|
|`POH Hold Register`|`0x00_000A`|
|`POH Hold Register`|`0x01_000A`|
|`POH Hold Indirect Control Register`|`0x00_000C`|
|`POH Hold Indirect Data Register`|`0x00_000D`|
|`POH Hold Indirect Control Register`|`0x01_000C`|
|`POH Hold Indirect Data Register`|`0x01_000D`|
|`POH Global Parity Force`|`0x00_0010`|
|`POH Global Parity Disable`|`0x00_0011`|
|`POH Global Parity Alarm`|`0x00_0012`|
|`POH Global Control`|`0x00_0000`|
|`POH CPE Global Control`|`0x01_0002`|
|`POH Global Alarm`|`0x00_0002`|
|`POH Threshold Global Control`|`0x00_0003`|
|`POH Hi-order Path Over Head Grabber`|`0x02_4000`|
|`POH Lo-order VT Over Head Grabber`|`0x02_6000`|
|`POH CPE STS/TU3 Control Register`|`0x02_A000`|
|`POH CPE VT Control Register`|`0x02_8000`|
|`POH CPE STS Status Register`|`0x02_D500`|
|`POH CPE VT/TU3 Status Register`|`0x02_C000`|
|`POH CPE J1 STS Expected Message buffer`|`0x0B_0000`|
|`POH CPE J1 STS Current Message buffer`|`0x0B_1000`|
|`POH CPE J2 Expected Message buffer`|`0x08_0000`|
|`POH CPE J2 Current Message buffer`|`0x09_0000`|
|`POH CPE J1 Insert Message buffer`|`0x0B_2000`|
|`POH CPE J2 Insert Message buffer`|`0x0A_0000`|
|`POH Termintate Insert Control STS`|`0x04_0400`|
|`POH Termintate Insert Control VT/TU3`|`0x04_4000`|
|`POH Termintate Insert Buffer STS`|`0x01_0800`|
|`POH Termintate Insert Buffer TU3/VT`|`0x01_8000`|
|`POH BER Global Control`|`0x06_0000`|
|`POH BER Error Sticky`|`0x06_0001`|
|`POH BER Threshold 1`|`0x06_2047`|
|`POH BER Threshold 2`|`0x06_0400`|
|`POH BER Control VT/DSN`|`0x06_2000`|
|`POH BER Control STS/TU3`|`0x06_2007`|
|`POH BER Report VT/DSN`|`0x06_8000`|
|`POH BER Report STS/TU3`|`0x06_C000`|
|`POH Counter Report STS`|`0x0C_A000`|
|`POH Counter Report TU3/VT`|`0x0C_8000`|
|`POH Alarm Status Mask Report STS`|`0x0D_0000`|
|`POH Alarm Status Report STS`|`0x0D_0040`|
|`POH Interrupt Status Report STS`|`0x0D_0020`|
|`POH Interrupt Global Status Report STS`|`0x0D_007F`|
|`POH Interrupt Global Mask Report STS`|`0x0D_007E`|
|`POH Alarm Status Mask Report VT/TU3`|`0x0E_0000`|
|`POH Alarm Status Report VT/TU3`|`0x0E_0800`|
|`POH Interrupt Status Report STS`|`0x0E_0400`|
|`POH Interrupt Or Status Report VT/TU3`|`0x0E_0C00`|
|`POH Interrupt Global Status Report VT/TU3`|`0x0E_0FFF`|
|`POH Interrupt Global Mask Report VT/TU3`|`0x0E_0FFE`|
|`POH Interrupt Global Mask Report`|`0x00_0004`|
|`POH Interrupt Global Status Report`|`0x00_0005`|
|`POH Interrupt  Global Status Out Report`|`0x00_0006`|
|`AME version`|`0x0F_0000`|
|`AME Control`|`0x0F_0002`|
|`AME SF/SD Alarm Mask`|`0x0F_0004`|
|`AME MASR Interrupt Mask`|`0x0F_0010`|
|`AME SF MASR register`|`0x0F_0011`|
|`AME SD MASR register`|`0x0F_0012`|
|`AME STS Status table`|`0x0F_2000`|
|`AME STS Enable table`|`0x0F_3000`|
|`AME ROW VT Status table`|`0x0F_2100`|
|`AME VT Status table`|`0x0F_2400`|
|`AME VT Enable table`|`0x0F_3400`|
|`AME STS Hold Off timer`|`0x0F_3800`|
|`AME VT Hold Off timer`|`0x0F_4000`|
|`AME LO ID Lookup`|`0x0F_1000`|
|`AME HO ID Lookup`|`0x0F_1800`|
|`Read HA Address Bit3_0 Control`|`0x000100`|
|`Read HA Address Bit7_4 Control 155*`|`0x000110`|
|`Read HA Address Bit11_8 Control 155*`|`0x000120`|
|`Read HA Address Bit15_12 Control 155*`|`0x000130`|
|`Read HA Address Bit19_16 Control 155*`|`0x000140`|
|`Read HA Address Bit23_20 Control 155*`|`0x000150`|
|`Read HA Address Bit24 and Data Control 155*`|`0x000160`|
|`Read HA Hold Data63_32 155*`|`0x000170`|
|`Read HA Hold Data95_64 155*`|`0x000171`|
|`Read HA Address Bit3_0 Control 311*`|`0x010100`|
|`Read HA Address Bit7_4 Control 311*`|`0x010110`|
|`Read HA Address Bit11_8 Control 311*`|`0x010120`|
|`Read HA Address Bit15_12 Control 311*`|`0x010130`|
|`Read HA Address Bit19_16 Control 311*`|`0x010140`|
|`Read HA Address Bit23_20 Control 311*`|`0x010150`|
|`Read HA Address Bit24 and Data Control 311*`|`0x010160`|
|`Read HA Hold Data63_32 311*`|`0x010170`|
|`Read HA Hold Data95_64 311*`|`0x010171`|


###POH Hold Register

* **Description**           

This register is used for access long register of Ber, Message.


* **RTL Instant Name**    : `holdreg`

* **Address**             : `0x00_000A`

* **Formula**             : `0x00_000A + holdnum`

* **Where**               : 

    * `$holdnum(0-1): Hold register number`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdvalue`| Hold value| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Hold Register

* **Description**           

This register is used for access long register of Grabber.


* **RTL Instant Name**    : `holdregclk2`

* **Address**             : `0x01_000A`

* **Formula**             : `0x01_000A + holdnum`

* **Where**               : 

    * `$holdnum(0-1): Hold register number`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdvalue`| Hold value| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Hold Indirect Control Register

* **Description**           

This register is used for access long register of Ber, Message.


* **RTL Instant Name**    : `holdindctlreg`

* **Address**             : `0x00_000C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdvalue`| Hold value| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Hold Indirect Data Register

* **Description**           

This register is used for access long register of Ber, Message.


* **RTL Instant Name**    : `holdinddatreg`

* **Address**             : `0x00_000D`

* **Formula**             : `0x00_000D + holdnum`

* **Where**               : 

    * `$holdnum(0-2): Hold register number`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdvalue`| Hold value| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Hold Indirect Control Register

* **Description**           

This register is used for access long register of Grabber.


* **RTL Instant Name**    : `holdindctlreg2`

* **Address**             : `0x01_000C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdvalue`| Hold value| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Hold Indirect Data Register

* **Description**           

This register is used for access long register of Grabber.


* **RTL Instant Name**    : `holdinddatreg2`

* **Address**             : `0x01_000D`

* **Formula**             : `0x01_000D + holdnum`

* **Where**               : 

    * `$holdnum(0-2): Hold register number`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdvalue`| Hold value| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Global Parity Force

* **Description**           

This register is used to control Parity , Message Jn request.


* **RTL Instant Name**    : `pcfg_glbparfrc`

* **Address**             : `0x00_0010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:6]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[5]`|`berparctrlfrc`| Force parity POH BER Control VT/DSN, POH BER Control STS/TU3| `RW`| `0x0`| `0x0`|
|`[4]`|`berpartrshfrc`| Force parity  POH BER Threshold 2| `RW`| `0x0`| `0x0`|
|`[3]`|`terparvttu3frc`| Force parity POH Termintate Insert Control VT/TU3| `RW`| `0x0`| `0x0`|
|`[2]`|`terparstsfrc`| Force parity POH Termintate Insert Control STS| `RW`| `0x0`| `0x0`|
|`[1]`|`cpeparvtfrc`| Force parity POH CPE VT Control Register| `RW`| `0x0`| `0x0`|
|`[0]`|`cpeparststu3frc`| Force parity POH CPE STS/TU3 Control Register| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Global Parity Disable

* **Description**           

This register is used to control Parity , Message Jn request.


* **RTL Instant Name**    : `pcfg_glbpardis`

* **Address**             : `0x00_0011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:6]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[5]`|`berparctrldis`| Disable parity POH BER Control VT/DSN, POH BER Control STS/TU3| `RW`| `0x0`| `0x0`|
|`[4]`|`berpartrshdis`| Disable parity POH BER Threshold 2| `RW`| `0x0`| `0x0`|
|`[3]`|`terparvttu3dis`| Disable parity POH Termintate Insert Control VT/TU3| `RW`| `0x0`| `0x0`|
|`[2]`|`terparstsdis`| Disable parity POH Termintate Insert Control STS| `RW`| `0x0`| `0x0`|
|`[1]`|`cpeparvtdis`| Disable parity POH CPE VT Control Register| `RW`| `0x0`| `0x0`|
|`[0]`|`cpeparststu3dis`| Disable parity POH CPE STS/TU3 Control Register| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Global Parity Alarm

* **Description**           

This register is used to show alarm of Parity and Debug


* **RTL Instant Name**    : `pcfg_glbparalm`

* **Address**             : `0x00_0012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:6]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[5]`|`berparctrlstk`| Parity sticky POH BER Control VT/DSN, POH BER Control STS/TU3| `RW`| `0x0`| `0x0`|
|`[4]`|`berpartrshstk`| Parity sticky  POH BER Threshold 2| `RW`| `0x0`| `0x0`|
|`[3]`|`terparvttu3stk`| Parity sticky POH Termintate Insert Control VT/TU3| `RW`| `0x0`| `0x0`|
|`[2]`|`terparstsstk`| Parity sticky POH Termintate Insert Control STS| `RW`| `0x0`| `0x0`|
|`[1]`|`cpeparvtstk`| Parity sticky POH CPE VT Control Register| `RW`| `0x0`| `0x0`|
|`[0]`|`cpeparststu3stk`| Parity sticky POH CPE STS/TU3 Control Register| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Global Control

* **Description**           

This register is used to control Parity , Message Jn request.


* **RTL Instant Name**    : `pcfg_glbctr`

* **Address**             : `0x00_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`terjnreqslen`| Enable JN Ter request for 8 Line| `RW`| `0x0`| `0xF`|
|`[23:16]`|`cpejnreqslen`| Enable JN CPE request for 8 Line| `RW`| `0x0`| `0xF`|
|`[15]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[14:12]`|`cpejnreqstsen`| Enable JN CPE request for 3 group STS, 16 each| `RW`| `0x0`| `0x7`|
|`[11:0]`|`unused`| *n/a*| `RW`| `0xX`| `0xX End: Begin:`|

###POH CPE Global Control

* **Description**           

This register is used to control whether receive bytes from OCN block .


* **RTL Instant Name**    : `pcfg_glbcpectr`

* **Address**             : `0x01_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[29]`|`cpevtline5en`| Enable recevive bytes for VTs Line 5| `RW`| `0x0`| `0x1`|
|`[28]`|`cpevtline4en`| Enable recevive bytes for VTs Line 4| `RW`| `0x0`| `0x1`|
|`[27]`|`cpevtline3en`| Enable recevive bytes for VTs Line 3| `RW`| `0x1`| `0x1`|
|`[26]`|`cpevtline2en`| Enable recevive bytes for VTs Line 2| `RW`| `0x1`| `0x1`|
|`[25]`|`cpevtline1en`| Enable recevive bytes for VTs Line 1| `RW`| `0x1`| `0x1`|
|`[24]`|`cpevtline0en`| Enable recevive bytes for VTs Line 0| `RW`| `0x1`| `0x1`|
|`[23]`|`cpestsline7en`| Enable recevive bytes for STSs Line 7| `RW`| `0x0`| `0x1`|
|`[22]`|`cpestsline6en`| Enable recevive bytes for STSs Line 6| `RW`| `0x0`| `0x1`|
|`[21]`|`cpestsline5en`| Enable recevive bytes for STSs Line 5| `RW`| `0x0`| `0x1`|
|`[20]`|`cpestsline4en`| Enable recevive bytes for STSs Line 4| `RW`| `0x0`| `0x1`|
|`[19]`|`cpestsline3en`| Enable recevive bytes for STSs Line 3| `RW`| `0x1`| `0x1`|
|`[18]`|`cpestsline2en`| Enable recevive bytes for STSs Line 2| `RW`| `0x1`| `0x1`|
|`[17]`|`cpestsline1en`| Enable recevive bytes for STSs Line 1| `RW`| `0x1`| `0x1`|
|`[16]`|`cpestsline0en`| Enable recevive bytes for STSs Line 0| `RW`| `0x1`| `0x1`|
|`[15:0]`|`unused`| *n/a*| `RW`| `0xX`| `0xX End: Begin:`|

###POH Global Alarm

* **Description**           

This register is used to show alarm of Parity and Debug


* **RTL Instant Name**    : `pcfg_glbalm`

* **Address**             : `0x00_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:7]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[6]`|`pmstk`| PM monitor sticky| `RW`| `0x0`| `0x0`|
|`[5]`|`berparctrlstk`| Parity sticky POH BER Control VT/DSN, POH BER Control STS/TU3| `RW`| `0x0`| `0x0`|
|`[4]`|`berpartrshstk`| Parity sticky  POH BER Threshold 2| `RW`| `0x0`| `0x0`|
|`[3]`|`terparvttu3stk`| Parity sticky POH Termintate Insert Control VT/TU3| `RW`| `0x0`| `0x0`|
|`[2]`|`terparstsstk`| Parity sticky POH Termintate Insert Control STS| `RW`| `0x0`| `0x0`|
|`[1]`|`cpeparvtstk`| Parity sticky POH CPE VT Control Register| `RW`| `0x0`| `0x0`|
|`[0]`|`cpeparststu3stk`| Parity sticky POH CPE STS/TU3 Control Register| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Threshold Global Control

* **Description**           

This register is used to set Threshold for stable detection.


* **RTL Instant Name**    : `pcfg_trshglbctr`

* **Address**             : `0x00_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:24]`|`v5rfistbtrsh`| V5 RDI Stable Thershold| `RW`| `0x5`| `0x5`|
|`[23:20]`|`v5rdistbtrsh`| V5 RDI Stable Thershold| `RW`| `0x5`| `0x5`|
|`[19:16]`|`v5slbstbtrsh`| V5 Signal Lable Stable Thershold| `RW`| `0x5`| `0x5`|
|`[15:12]`|`g1rdistbtrsh`| G1 RDI Path Stable Thershold| `RW`| `0x5`| `0x5`|
|`[11:8]`|`c2plmstbtrsh`| C2 Path Signal Lable Stable Thershold| `RW`| `0x5`| `0x5`|
|`[7:4]`|`jnstbtrsh`| J1/J2 Message Stable Threshold| `RW`| `0x5`| `0x5`|
|`[3:0]`|`debound`| Debound Threshold| `RW`| `0x5`| `0x5 End: Begin:`|

###POH Hi-order Path Over Head Grabber

* **Description**           

This register is used to grabber Hi-Order Path Overhead


* **RTL Instant Name**    : `pohstspohgrb`

* **Address**             : `0x02_4000`

* **Formula**             : `0x02_4000 + $stsid * 8 + $sliceid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification`

    * `$stsid(0-47): STS Identification`

* **Width**               : `68`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[67]`|`hlais`| High-level AIS from OCN| `RO`| `0x0`| `0x0`|
|`[66]`|`lom`| LOM  from OCN| `RO`| `0x0`| `0x0`|
|`[65]`|`lop`| LOP from OCN| `RO`| `0x0`| `0x0`|
|`[64]`|`ais`| AIS from OCN| `RO`| `0x0`| `0x0`|
|`[63:56]`|`k3`| K3 byte| `RO`| `0x0`| `0x0`|
|`[55:48]`|`f3`| F3 byte| `RO`| `0x0`| `0x0`|
|`[47:40]`|`h4`| H4 byte| `RO`| `0x0`| `0x0`|
|`[39:32]`|`f2`| F2 byte| `RO`| `0x0`| `0x0`|
|`[31:24]`|`g1`| G1 byte| `RO`| `0x0`| `0x0`|
|`[23:16]`|`c2`| C2 byte| `RO`| `0x0`| `0x0`|
|`[15:8]`|`n1`| N1 byte| `RO`| `0x0`| `0x0`|
|`[7:0]`|`j1`| J1 byte| `RO`| `0x0`| `0x0 End: Begin:`|

###POH Lo-order VT Over Head Grabber

* **Description**           

This register is used to grabber Lo-Order Path Overhead. Incase the TU3 mode, the $vtid = 0, using for Tu3 POH grabber.

Incase VT mode, the $vtid = 0-27, using for VT POH grabber.


* **RTL Instant Name**    : `pohvtpohgrb`

* **Address**             : `0x02_6000`

* **Formula**             : `0x02_6000 + $sliceid*1344 + $stsid*28 + $vtid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification`

    * `$stsid(0-47): STS Identification`

    * `$vtid(0-27): VT Identification`

* **Width**               : `36`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[35]`|`hlais`| High-level AIS from OCN| `RO`| `0x0`| `0x0`|
|`[34]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[33]`|`lop`| LOP from OCN| `RO`| `0x0`| `0x0`|
|`[32]`|`ais`| AIS from OCN| `RO`| `0x0`| `0x0`|
|`[31:24]`|`byte3`| G1 byte or K3 byte or K4 byte| `RO`| `0x0`| `0x0`|
|`[23:16]`|`byte2`| C2 byte or F3 byte or N2 byte| `RO`| `0x0`| `0x0`|
|`[15:8]`|`byte1`| N1 byte or H4 byte or J2 byte| `RO`| `0x0`| `0x0`|
|`[7:0]`|`byte0`| J1 byte or F2 byte or V5 byte| `RO`| `0x0`| `0x0 End: Begin:`|

###POH CPE STS/TU3 Control Register

* **Description**           

This register is used to configure the POH Hi-order Path Monitoring.


* **RTL Instant Name**    : `pohcpestsctr`

* **Address**             : `0x02_A000`

* **Formula**             : `0x02_A000 + $sliceid*96 + $stsid*2 + $tu3en`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification`

    * `$stsid(0-47): STS Identification`

    * `$tu3en(0-1): Tu3enable, 0: STS, 1:Tu3`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:21]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[20]`|`monenb`| Alarm monitor enable| `RW`| `0x0`| `0x0`|
|`[19]`|`plmenb`| PLM enable| `RW`| `0x0`| `0x0`|
|`[18]`|`vcaisdstren`| VcaisDstren| `RW`| `0x0`| `0x0`|
|`[17]`|`plmdstren`| PlmDstren| `RW`| `0x0`| `0x0`|
|`[16]`|`uneqdstren`| UneqDstren| `RW`| `0x0`| `0x0`|
|`[15]`|`timdstren`| TimDstren| `RW`| `0x0`| `0x0`|
|`[14]`|`sdhmode`| SDH mode| `RW`| `0x0`| `0x0`|
|`[13]`|`blkmden`| Block mode BIP| `RW`| `0x0`| `0x0`|
|`[12]`|`erdienb`| Enable E-RDI| `RW`| `0x0`| `0x0`|
|`[11:4]`|`pslexp`| C2 Expected Path Signal Lable Value| `RW`| `0x0`| `0x0`|
|`[3]`|`timenb`| Enable Monitor TIM| `RW`| `0x0`| `0x0`|
|`[2]`|`reiblkmden`| Block mode REI| `RW`| `0x0`| `0x0`|
|`[1:0]`|`j1mode`| 0: 1Byte 1:16Byte 2:64byte 3:Floating| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE VT Control Register

* **Description**           

This register is used to configure the POH Lo-order Path Monitoring.


* **RTL Instant Name**    : `pohcpevtctr`

* **Address**             : `0x02_8000`

* **Formula**             : `0x02_8000 + $sliceid*1344 + $stsid*28 +$vtid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification`

    * `$stsid(0-47): STS Identification`

    * `$vtid(0-27):Vt Identification`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[15]`|`monenb`| Alarm monitor enable| `RW`| `0x0`| `0x0`|
|`[14]`|`plmenb`| VcaisDstren| `RW`| `0x0`| `0x0`|
|`[13]`|`vcaisdstren`| VcaisDstren| `RW`| `0x0`| `0x0`|
|`[12]`|`plmdstren`| PlmDstren| `RW`| `0x0`| `0x0`|
|`[11]`|`uneqdstren`| UneqDstren| `RW`| `0x0`| `0x0`|
|`[10]`|`timdstren`| TimDstren| `RW`| `0x0`| `0x0`|
|`[9]`|`vsdhmode`| SDH mode| `RW`| `0x0`| `0x0`|
|`[8]`|`vblkmden`| Block mode BIP| `RW`| `0x0`| `0x0`|
|`[7]`|`erdienb`| Enable E-RDI| `RW`| `0x0`| `0x0`|
|`[6:4]`|`vslexp`| V5 Expected Path Signal Lable Value| `RW`| `0x0`| `0x0`|
|`[3]`|`timenb`| Enable Monitor TIM| `RW`| `0x0`| `0x0`|
|`[2]`|`reiblkmden`| Block mode REI| `RW`| `0x0`| `0x0`|
|`[1:0]`|`j2mode`| 0: 1Byte 1:16Byte 2:64byte 3:Floating| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE STS Status Register

* **Description**           

This register is used to get POH Hi-order status of monitoring.


* **RTL Instant Name**    : `pohcpestssta`

* **Address**             : `0x02_D500`

* **Formula**             : `0x02_D500 + $sliceid*48 + $stsid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification`

    * `$stsid(0-47): STS Identification`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[12]`|`c2stbsta`| C2 stable status| `RO`| `0x0`| `0x0`|
|`[11:8]`|`c2stbcnt`| C2 stable counter| `RO`| `0x0`| `0x0`|
|`[7:0]`|`c2acpt`| C2 accept byte| `RO`| `0x0`| `0x0 End: Begin:`|

###POH CPE VT/TU3 Status Register

* **Description**           

This register is used to get POH Lo-order status of monitoring.


* **RTL Instant Name**    : `pohcpevtsta`

* **Address**             : `0x02_C000`

* **Formula**             : `0x02_C000 + $sliceid*1344 + $stsid*28 +$vtid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification`

    * `$stsid(0-47): STS Identification`

    * `$vtid(0-27):Vt Identification`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[13]`|`vslstbsta`| VSL stable status| `RO`| `0x0`| `0x0`|
|`[12:9]`|`vslstbcnt`| VSL stable counter| `RO`| `0x0`| `0x0`|
|`[8:6]`|`vslacpt`| VSL accept byte| `RO`| `0x0`| `0x0`|
|`[5:0]`|`rfistatus`| RFI status| `RO`| `0x0`| `0x0 End: Begin:`|

###POH CPE J1 STS Expected Message buffer

* **Description**           

The J1 Expected Message Buffer.


* **RTL Instant Name**    : `pohmsgstsexp`

* **Address**             : `0x0B_0000`

* **Formula**             : `0x0B_0000 + $sliceid*384 + $stsid*8 + $msgid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification`

    * `$stsid(0-47): STS Identification`

    * `$msgid(0-7): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j1expmsg`| J1 Expected Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J1 STS Current Message buffer

* **Description**           

The J1 Current Message Buffer.


* **RTL Instant Name**    : `pohmsgstscur`

* **Address**             : `0x0B_1000`

* **Formula**             : `0x0B_1000 + $sliceid*384 + $stsid*8 + $msgid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification`

    * `$stsid(0-47): STS Identification`

    * `$msgid(0-7): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j1curmsg`| J1 Current Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J2 Expected Message buffer

* **Description**           

The J2 Expected Message Buffer.


* **RTL Instant Name**    : `pohmsgvtexp`

* **Address**             : `0x08_0000`

* **Formula**             : `0x08_0000 + $sliceid*10752 + $stsid*224 + $vtid*8 + $msgid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification`

    * `$stsid(0-47): STS Identification`

    * `$vtid(0-27): VT Identification`

    * `$msgid(0-7): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j2expmsg`| J2 Expected Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J2 Current Message buffer

* **Description**           

The J2 Current Message Buffer.


* **RTL Instant Name**    : `pohmsgvtcur`

* **Address**             : `0x09_0000`

* **Formula**             : `0x09_0000 + $sliceid*10752 + $stsid*224 + $vtid*8 + $msgid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification`

    * `$stsid(0-47): STS Identification`

    * `$vtid(0-27): VT Identification`

    * `$msgid(0-7): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j2curmsg`| J2 Current Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J1 Insert Message buffer

* **Description**           

The J1 Current Message Buffer.


* **RTL Instant Name**    : `pohmsgstsins`

* **Address**             : `0x0B_2000`

* **Formula**             : `0x0B_2000 + $sliceid*512 + $stsid*8 + $msgid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification`

    * `$stsid(0-47): STS Identification`

    * `$msgid(0-7): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j1insmsg`| J1 Insert Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH CPE J2 Insert Message buffer

* **Description**           

The J2 Insert Message Buffer.


* **RTL Instant Name**    : `pohmsgvtins`

* **Address**             : `0x0A_0000`

* **Formula**             : `0x0A_0000 + $sliceid*10752 + $stsid*224 + $vtid*8 + $msgid`

* **Where**               : 

    * `$sliceid(0-3): Slice Identification`

    * `$stsid(0-47): STS Identification`

    * `$vtid(0-27): VT Identification`

    * `$msgid(0-7): Message ID`

* **Width**               : `64`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:0]`|`j2insmsg`| J2 Insert Message| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Termintate Insert Control STS

* **Description**           

This register is used to control STS POH insert .


* **RTL Instant Name**    : `ter_ctrlhi`

* **Address**             : `0x04_0400`

* **Formula**             : `0x04_0400 + $STS + $OCID*48`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-3)  : Line ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:7]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[6]`|`g1spare`| G1 spare value| `RW`| `0x0`| `0x0`|
|`[5]`|`plm`| 0 : Enable, 1: Disable send ERDI if PLM detected| `RW`| `0x0`| `0x0`|
|`[4]`|`uneq`| 0 : Enable, 1: Disable send ERDI if UNEQ detected| `RW`| `0x0`| `0x0`|
|`[3]`|`timmsk`| 0 : Enable, 1: Disable send ERDI if TIM detected| `RW`| `0x0`| `0x0`|
|`[2]`|`aislopmsk`| 0 : Enable, 1: Disable send ERDI if AIS,LOP detected| `RW`| `0x0`| `0x0`|
|`[1:0]`|`jnfrmd`| 0:1 byte, 1: 16/64byte| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Termintate Insert Control VT/TU3

* **Description**           

This register is used to control STS POH insert. TU3 is at VT ID = 0. Fields must be the same as ter_ctrlhi


* **RTL Instant Name**    : `ter_ctrllo`

* **Address**             : `0x04_4000`

* **Formula**             : `0x04_4000 + $STS*28 + $OCID*1344 + $VT`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-3)  : Line ID`

    * `$VT(0-27)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[14:13]`|`k4b0b1`| K4b0b1 value| `RW`| `0x0`| `0x0`|
|`[12:11]`|`k4aps`| K4aps value| `RW`| `0x0`| `0x0`|
|`[10]`|`k4spare`| K4spare value| `RW`| `0x0`| `0x0`|
|`[9]`|`rfival`| RFI value| `RW`| `0x0`| `0x0`|
|`[8:6]`|`vslval`| VT signal label value| `RW`| `0x0`| `0x0`|
|`[5]`|`plm`| 0 : Enable, 1: Disable send ERDI if PLM detected| `RW`| `0x0`| `0x0`|
|`[4]`|`uneq`| 0 : Enable, 1: Disable send ERDI if UNEQ detected| `RW`| `0x0`| `0x0`|
|`[3]`|`timmsk`| 0 : Enable, 1: Disable send ERDI if TIM detected| `RW`| `0x0`| `0x0`|
|`[2]`|`aislopmsk`| 0 : Enable, 1: Disable send ERDI if AIS,LOP detected| `RW`| `0x0`| `0x0`|
|`[1:0]`|`jnfrmd`| 0:1 byte, 1: 16/64byte| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Termintate Insert Buffer STS

* **Description**           

This register is used for storing POH BYTEs inserted to Sonet/SDH. %%

BGRP = 0 : G1,J1  %%

BGRP = 1 : N1,C2  %%

BGRP = 2 : H4,F2  %%

BGRP = 3 : K3,F3


* **RTL Instant Name**    : `rtlpohccterbufhi`

* **Address**             : `0x01_0800`

* **Formula**             : `0x01_0800 + $OCID*256 + $STS*4 + $BGRP`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-3)  : Line ID`

    * `$BGRP(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[17]`|`byte1msk`| Enable/Disable (1/0)write to buffer| `WO`| `0x0`| `0x0`|
|`[16:9]`|`byte1`| Byte1 (G1/N1/H4/K3)| `RW`| `0x0`| `0x0`|
|`[8]`|`byte0msk`| Enable/Disable (1/0) write to buffer| `WO`| `0x0`| `0x0`|
|`[7:0]`|`byte0`| Byte0 (J1/C2/F2/F3)| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Termintate Insert Buffer TU3/VT

* **Description**           

This register is used for storing POH BYTEs inserted to Sonet/SDH. TU3 is at VT ID = 0,1 %%

For VT %%

BGRP = 0 : J2,V5 %%

BGRP = 1 : K4,N2 %%

For TU3 %%

VT = 0, BGRP = 0 : G1,J1 %%

VT = 0, BGRP = 1 : N1,C2 %%

VT = 1, BGRP = 0 : H4,F2 %%

VT = 1, BGRP = 1 : K3,F3


* **RTL Instant Name**    : `rtlpohccterbuflo`

* **Address**             : `0x01_8000`

* **Formula**             : `0x01_8000 + $OCID*4096 + $STS*64 + $VT*2 + $BGRP`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-3)  : Line ID`

    * `$VT(0-27)`

    * `$BGRP(0-1)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[17]`|`byte1msk`| Enable/Disable (1/0)write to buffer| `WO`| `0x0`| `0x0`|
|`[16:9]`|`byte1`| Byte1 (J2/K4)| `RW`| `0x0`| `0x0`|
|`[8]`|`byte0msk`| Enable/Disable (1/0) write to buffer| `WO`| `0x0`| `0x0`|
|`[7:0]`|`byte0`| Byte0 (V5/N2)| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Global Control

* **Description**           

This register is used to enable STS,VT,DSN globally.


* **RTL Instant Name**    : `pcfg_glbenb`

* **Address**             : `0x06_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[3]`|`timerenb`| Enable timer| `RW`| `0x1`| `0x0`|
|`[2]`|`stsenb`| Enable STS/TU3 channel| `RW`| `0x1`| `0x0`|
|`[1]`|`vtenb`| Enable STS/TU3 channel| `RW`| `0x1`| `0x0`|
|`[0]`|`dsnsenb`| Enable STS/TU3 channel| `RW`| `0x1`| `0x0 End: Begin:`|

###POH BER Error Sticky

* **Description**           

This register is used to check error in BER engine.


* **RTL Instant Name**    : `stkalarm`

* **Address**             : `0x06_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[1]`|`stserr`| STS error| `W1C`| `0x0`| `0x0`|
|`[0]`|`vterr`| VT error| `W1C`| `0x0`| `0x0 End: Begin:`|

###POH BER Threshold 1

* **Description**           

This register is used to configure threshold of BER level 3.


* **RTL Instant Name**    : `imemrwptrsh1`

* **Address**             : `0x06_2047`

* **Formula**             : `0x06_2047 + $Rate[2:0]*8  + $Rate[6:3]*128`

* **Where**               : 

    * `$Rate(0-127): STS Rate for rate from STS1,STS3,STS6...STS48,(0-16)...VT1.5,VT2,DS1,E1(65,67,69,71)....`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[18:9]`|`setthres`| SetThreshold| `RW`| `0x0`| `0x0`|
|`[8:0]`|`winthres`| WindowThreshold| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Threshold 2

* **Description**           

This register is used to configure threshold of BER level 4 to level 8.


* **RTL Instant Name**    : `imemrwptrsh2`

* **Address**             : `0x06_0400`

* **Formula**             : `0x06_0400 + $Rate*8 + $Thresloc`

* **Where**               : 

    * `$Rate(0-63): STS Rate for rate from STS1,STS3,STS6...STS48,....VT1.5,VT2,DS1,E1....`

    * `$Thresloc(0-7): Set/Clear/Window threshold for BER level from 4 to 8`

* **Width**               : `34`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[33:17]`|`scwthres1`| Set/Clear/Window Threshold| `RW`| `0x0`| `0x0`|
|`[16:0]`|`scwthres2`| Set/Clear/Window Threshold| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Control VT/DSN

* **Description**           

This register is used to enable and set threshold SD SF .


* **RTL Instant Name**    : `imemrwpctrl1`

* **Address**             : `0x06_2000`

* **Formula**             : `0x06_2000 + $STS*128 + $OCID*8+ $VTG`

* **Where**               : 

    * `$STS(0-47): STS`

    * `$OCID(0-11)  : Line ID, 0-5: VT, 6-11: DE1`

    * `$VTG(0-6)   : VT group`

* **Width**               : `48`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:46]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[45:43]`|`tcatrsh4`| TCA threshold raise channel 4| `RW`| `0x0`| `0x0`|
|`[42:40]`|`tcatrsh3`| TCA threshold raise channel 3| `RW`| `0x0`| `0x0`|
|`[39:37]`|`tcatrsh2`| TCA threshold raise channel 2| `RW`| `0x0`| `0x0`|
|`[36:34]`|`tcatrsh1`| TCA threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[33:32]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[31]`|`etype4`| 0: DS1/VT1.5 1: E1/VT2 channel 3| `RW`| `0x0`| `0x0`|
|`[30:28]`|`sftrsh4`| SF threshold raise channel 3| `RW`| `0x0`| `0x0`|
|`[27:25]`|`sdtrsh4`| SD threshold raise channel 3| `RW`| `0x0`| `0x0`|
|`[24]`|`ena4`| Enable channel 3| `RW`| `0x0`| `0x0`|
|`[23]`|`etype3`| 0: DS1/VT1.5 1: E1/VT2 channel 2| `RW`| `0x0`| `0x0`|
|`[22:20]`|`sftrsh3`| SF threshold raise channel 2| `RW`| `0x0`| `0x0`|
|`[19:17]`|`sdtrsh3`| SD threshold raise channel 2| `RW`| `0x0`| `0x0`|
|`[16]`|`ena3`| Enable channel 2| `RW`| `0x0`| `0x0`|
|`[15]`|`etype2`| 0: DS1/VT1.5 1: E1/VT2 channel 1| `RW`| `0x0`| `0x0`|
|`[14:12]`|`sftrsh2`| SF threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[11:9]`|`sdtrsh2`| SD threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[8]`|`ena2`| Enable channel 1| `RW`| `0x0`| `0x0`|
|`[7]`|`etype1`| 0: DS1/VT1.5 1: E1/VT2 channel 0| `RW`| `0x0`| `0x0`|
|`[6:4]`|`sftrsh1`| SF threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[3:1]`|`sdtrsh1`| SD threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[0]`|`ena1`| Enable channel 0| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Control STS/TU3

* **Description**           

This register is used to enable and set threshold SD SF .


* **RTL Instant Name**    : `imemrwpctrl2`

* **Address**             : `0x06_2007`

* **Formula**             : `0x06_2007 + $STS*128 + $OCID*8`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-7)  : Line ID, Line 4-7 channel 2 for DE3`

* **Width**               : `48`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:40]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[39:37]`|`tcatrsh2`| TCA threshold raise channel 2| `RW`| `0x0`| `0x0`|
|`[36:34]`|`tcatrsh1`| TCA threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[33:32]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:21]`|`rate2`| STS Rate 0-63 type| `RW`| `0x0`| `0x0`|
|`[20:18]`|`sftrsh2`| SF threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[17:15]`|`sdtrsh2`| SD threshold raise channel 1| `RW`| `0x0`| `0x0`|
|`[14]`|`ena2`| Enable channel 1| `RW`| `0x0`| `0x0`|
|`[13:7]`|`rate1`| STS Rate 0-63 type| `RW`| `0x0`| `0x0`|
|`[6:4]`|`sftrsh1`| SF threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[3:1]`|`sdtrsh1`| SD threshold raise channel 0| `RW`| `0x0`| `0x0`|
|`[0]`|`ena1`| Enable channel 0| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Report VT/DSN

* **Description**           

This register is used to get current BER rate .


* **RTL Instant Name**    : `ramberratevtds`

* **Address**             : `0x06_8000`

* **Formula**             : `0x06_8000 + $STS*336 + $OCID*28 + $VT`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-11)     : Line ID, VT:0-5,DSN:6-11`

    * `$VT(0-27)   : VT/DS1 ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[3]`|`hwsta`| Hardware status| `RW`| `0x0`| `0x0`|
|`[2:0]`|`rate`| BER rate| `RW`| `0x0`| `0x0 End: Begin:`|

###POH BER Report STS/TU3

* **Description**           

This register is used to get current BER rate . BER DE3 used with OCID 4-7, TU3TYPE = 1.


* **RTL Instant Name**    : `ramberrateststu3`

* **Address**             : `0x06_C000`

* **Formula**             : `0x06_C000 + $STS*16 + $OCID*2 + $TU3TYPE`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-7)     : Line ID`

    * `$TU3TYPE(0-1)  : Type TU3:1, STS:0`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[3]`|`hwsta`| Hardware status| `RW`| `0x0`| `0x0`|
|`[2:0]`|`rate`| BER rate| `RW`| `0x0`| `0x0 End: Begin:`|

###POH Counter Report STS

* **Description**           

This register is used to get POH Counter, Rx SDH pointer increase, decrease counter.


* **RTL Instant Name**    : `ipm_cnthi`

* **Address**             : `0x0C_A000`

* **Formula**             : `0x0C_A000 + $STS + $OCID*48`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-3)     : Line ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`reicnt`| REI counter| `RC`| `0x0`| `0x0`|
|`[15:0]`|`bipcnt`| BIP counter| `RC`| `0x0`| `0x0 End: Begin:`|

###POH Counter Report TU3/VT

* **Description**           

This register is used to get POH Counter, Rx SDH pointer increase, decrease counter.


* **RTL Instant Name**    : `ipm_cntlo`

* **Address**             : `0x0C_8000`

* **Formula**             : `0x0C_8000 + $STS*28 + $OCID*1344 + $VT`

* **Where**               : 

    * `$STS(0-47)  : STS`

    * `$OCID(0-3)   : Line ID`

    * `$VT(0-27)   : VT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`reicnt`| REI counter| `RC`| `0x0`| `0x0`|
|`[15:0]`|`bipcnt`| BIP counter| `RC`| `0x0`| `0x0 End: Begin:`|

###POH Alarm Status Mask Report STS

* **Description**           

This register is used to get POH alarm mask report.


* **RTL Instant Name**    : `alm_mskhi`

* **Address**             : `0x0D_0000`

* **Formula**             : `0x0D_0000 + $STS + $OCID*128`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[13]`|`jnstachgmsk`| jnstachg mask| `RW`| `0x0`| `0x0`|
|`[12]`|`pslstachgmsk`| pslstachg mask| `RW`| `0x0`| `0x0`|
|`[11]`|`bersdmsk`| bersd mask| `RW`| `0x0`| `0x0`|
|`[10]`|`bersfmsk`| bersf  mask| `RW`| `0x0`| `0x0`|
|`[9]`|`erdimsk`| erdis/rdi mask| `RW`| `0x0`| `0x0`|
|`[8]`|`bertcamsk`| bertca mask| `RW`| `0x0`| `0x0`|
|`[7]`|`erdicmsk`| erdicmsk  mask| `RW`| `0x0`| `0x0`|
|`[6]`|`erdipmsk`| erdipmsk  mask| `RW`| `0x0`| `0x0`|
|`[5]`|`rfimsk`| rfi/lom mask| `RW`| `0x0`| `0x0`|
|`[4]`|`timmsk`| tim mask| `RW`| `0x0`| `0x0`|
|`[3]`|`uneqmsk`| uneq mask| `RW`| `0x0`| `0x0`|
|`[2]`|`plmmsk`| plm mask| `RW`| `0x0`| `0x0`|
|`[1]`|`aismsk`| ais mask| `RW`| `0x0`| `0x0`|
|`[0]`|`lopmsk`| lop mask| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Alarm Status Report STS

* **Description**           

This register is used to get POH alarm status report.


* **RTL Instant Name**    : `alm_stahi`

* **Address**             : `0x0D_0040`

* **Formula**             : `0x0D_0040 + $STS + $OCID*128`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[11]`|`bersdsta`| bersd  status| `W1C`| `0x0`| `0x0`|
|`[10]`|`bersfsta`| bersf  status| `W1C`| `0x0`| `0x0`|
|`[9]`|`erdista`| erdis/rdi  status| `RO`| `0x0`| `0x0`|
|`[8]`|`bertcasta`| bertca status| `RO`| `0x0`| `0x0`|
|`[7]`|`erdicsta`| erdic status| `RO`| `0x0`| `0x0`|
|`[6]`|`erdipsta`| erdip stable status| `RO`| `0x0`| `0x0`|
|`[5]`|`rfista`| rfi/lom status| `RO`| `0x0`| `0x0`|
|`[4]`|`timsta`| tim status| `RO`| `0x0`| `0x0`|
|`[3]`|`uneqsta`| uneq status| `RO`| `0x0`| `0x0`|
|`[2]`|`plmsta`| plm status| `RO`| `0x0`| `0x0`|
|`[1]`|`aissta`| ais status| `RO`| `0x0`| `0x0`|
|`[0]`|`lopsta`| lop status| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Status Report STS

* **Description**           

This register is used to get POH alarm change status report.


* **RTL Instant Name**    : `alm_chghi`

* **Address**             : `0x0D_0020`

* **Formula**             : `0x0D_0020 + $STS + $OCID*128`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[13]`|`jnstachg`| jn message change| `RW`| `0x0`| `0x0`|
|`[12]`|`pslstachg`| psl byte change| `RW`| `0x0`| `0x0`|
|`[11]`|`bersdstachg`| bersd stable status change| `W1C`| `0x0`| `0x0`|
|`[10]`|`bersfstachg`| bersf stable status change| `W1C`| `0x0`| `0x0`|
|`[9]`|`erdistachg`| erdis/rdi status change| `W1C`| `0x0`| `0x0`|
|`[8]`|`bertcastachg`| bertca status change| `W1C`| `0x0`| `0x0`|
|`[7]`|`erdicstachg`| erdic status change| `W1C`| `0x0`| `0x0`|
|`[6]`|`erdipstachg`| erdip status change| `W1C`| `0x0`| `0x0`|
|`[5]`|`rfistachg`| rfi/lom status change| `W1C`| `0x0`| `0x0`|
|`[4]`|`timstachg`| tim status change| `W1C`| `0x0`| `0x0`|
|`[3]`|`uneqstachg`| uneq status change| `W1C`| `0x0`| `0x0`|
|`[2]`|`plmstachg`| plm status change| `W1C`| `0x0`| `0x0`|
|`[1]`|`aisstachg`| ais status change| `W1C`| `0x0`| `0x0`|
|`[0]`|`lopstachg`| lop status change| `W1C`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Status Report STS

* **Description**           

This register is used to get POH alarm global change status report.


* **RTL Instant Name**    : `alm_glbchghi`

* **Address**             : `0x0D_007F`

* **Formula**             : `0x0D_007F + $OCID*128`

* **Where**               : 

    * `$OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[23:0]`|`glbstachg`| global status change bit| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Mask Report STS

* **Description**           

This register is used to get POH alarm global mask report.


* **RTL Instant Name**    : `alm_glbmskhi`

* **Address**             : `0x0D_007E`

* **Formula**             : `0x0D_007E + $OCID*128`

* **Where**               : 

    * `$OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[23:0]`|`glbmsk`| global mask| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Alarm Status Mask Report VT/TU3

* **Description**           

This register is used to get POH alarm mask report.


* **RTL Instant Name**    : `alm_msklo`

* **Address**             : `0x0E_0000`

* **Formula**             : `0x0E_0000 + $STS*32 + $OCID*4096 + $VTID`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47`

    * `$VTID(0-27)  : VT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[13]`|`jnstachgmsk`| jnstachg mask| `RW`| `0x0`| `0x0`|
|`[12]`|`pslstachgmsk`| pslstachg mask| `RW`| `0x0`| `0x0`|
|`[11]`|`bersdmsk`| bersd mask| `RW`| `0x0`| `0x0`|
|`[10]`|`bersfmsk`| bersf  mask| `RW`| `0x0`| `0x0`|
|`[9]`|`erdimsk`| erdis/rdi mask| `RW`| `0x0`| `0x0`|
|`[8]`|`bertcamsk`| bertca mask| `RW`| `0x0`| `0x0`|
|`[7]`|`erdicmsk`| erdic  mask| `RW`| `0x0`| `0x0`|
|`[6]`|`erdipmsk`| erdip  mask| `RW`| `0x0`| `0x0`|
|`[5]`|`rfimsk`| rfi mask| `RW`| `0x0`| `0x0`|
|`[4]`|`timmsk`| tim mask| `RW`| `0x0`| `0x0`|
|`[3]`|`uneqmsk`| uneq mask| `RW`| `0x0`| `0x0`|
|`[2]`|`plmmsk`| plm mask| `RW`| `0x0`| `0x0`|
|`[1]`|`aismsk`| ais mask| `RW`| `0x0`| `0x0`|
|`[0]`|`lopmsk`| lop mask| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Alarm Status Report VT/TU3

* **Description**           

This register is used to get POH alarm status.


* **RTL Instant Name**    : `alm_stalo`

* **Address**             : `0x0E_0800`

* **Formula**             : `0x0E_0800 + $STS*32 + $OCID*4096 + $VTID`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47`

    * `$VTID(0-27)  : VT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[11]`|`bersdsta`| bersd status| `RW`| `0x0`| `0x0`|
|`[10]`|`bersfsta`| bersf  status| `RW`| `0x0`| `0x0`|
|`[9]`|`erdista`| erdis/rdi status| `RO`| `0x0`| `0x0`|
|`[8]`|`bertcasta`| bertca status| `RO`| `0x0`| `0x0`|
|`[7]`|`erdicsta`| erdic status| `RO`| `0x0`| `0x0`|
|`[6]`|`erdipsta`| erdip status| `RO`| `0x0`| `0x0`|
|`[5]`|`rfista`| rfi status| `RO`| `0x0`| `0x0`|
|`[4]`|`timsta`| tim status| `RO`| `0x0`| `0x0`|
|`[3]`|`uneqsta`| uneq status| `RO`| `0x0`| `0x0`|
|`[2]`|`plmsta`| plm status| `RO`| `0x0`| `0x0`|
|`[1]`|`aissta`| ais status| `RO`| `0x0`| `0x0`|
|`[0]`|`lopsta`| lop status| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Status Report STS

* **Description**           

This register is used to get POH alarm change status report.


* **RTL Instant Name**    : `alm_chglo`

* **Address**             : `0x0E_0400`

* **Formula**             : `0x0E_0400 + $STS*32 + $OCID*4096 + $VTID`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47`

    * `$VTID(0-27)  : VT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[13]`|`jnstachg`| jn message change| `RW`| `0x0`| `0x0`|
|`[12]`|`pslstachg`| psl byte change| `RW`| `0x0`| `0x0`|
|`[11]`|`bersdstachg`| bersd stable status change| `W1C`| `0x0`| `0x0`|
|`[10]`|`bersfstachg`| bersf stable status change| `W1C`| `0x0`| `0x0`|
|`[9]`|`erdistachg`| erdis/rdi status change| `W1C`| `0x0`| `0x0`|
|`[8]`|`bertcastachg`| bertca status change| `W1C`| `0x0`| `0x0`|
|`[7]`|`erdicstachg`| erdic status change| `W1C`| `0x0`| `0x0`|
|`[6]`|`erdipstachg`| erdip status change| `W1C`| `0x0`| `0x0`|
|`[5]`|`rfistachg`| rfi status change| `W1C`| `0x0`| `0x0`|
|`[4]`|`timstachg`| tim status change| `W1C`| `0x0`| `0x0`|
|`[3]`|`uneqstachg`| uneq status change| `W1C`| `0x0`| `0x0`|
|`[2]`|`plmstachg`| plm status change| `W1C`| `0x0`| `0x0`|
|`[1]`|`aisstachg`| ais status change| `W1C`| `0x0`| `0x0`|
|`[0]`|`lopstachg`| lop status change| `W1C`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Or Status Report VT/TU3

* **Description**           

This register is used to get POH alarm or status change status report.


* **RTL Instant Name**    : `alm_orstalo`

* **Address**             : `0x0E_0C00`

* **Formula**             : `0x0E_0C00 + $STS + $OCID*4096`

* **Where**               : 

    * `$STS(0-23)  : STS`

    * `$OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:0]`|`orstachg`| or status change bit| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Status Report VT/TU3

* **Description**           

This register is used to get POH alarm global change status report.


* **RTL Instant Name**    : `alm_glbchglo`

* **Address**             : `0x0E_0FFF`

* **Formula**             : `0x0E_0FFF + $OCID*4096`

* **Where**               : 

    * `$OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[23:0]`|`glbstachg`| global status change bit| `RO`| `0x0`| `0x0 // End: ###################################################################################### Begin:`|

###POH Interrupt Global Mask Report VT/TU3

* **Description**           

This register is used to get POH alarm global mask report.


* **RTL Instant Name**    : `alm_glbmsklo`

* **Address**             : `0x0E_0FFE`

* **Formula**             : `0x0E_0FFE + $OCID*4096`

* **Where**               : 

    * `$OCID(0-7) : Line ID, ID 0,2,4,6 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[23:0]`|`glbmsk`| global status change bit| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Mask Report

* **Description**           

This register is used to get POH alarm global mask report for high,low order.


* **RTL Instant Name**    : `alm_glbmsk`

* **Address**             : `0x00_0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:16]`|`glbmsklo`| global mask bit for low order slice - ocid11...ocid3,ocid1,ocid10...ocid2,ocid0| `RW`| `0x0`| `0x0`|
|`[15:0]`|`glbmskhi`| global mask change bit for high order slice - ocid15...ocid3,ocid1,ocid14...ocid2,ocid0| `RW`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt Global Status Report

* **Description**           

This register is used to get POH alarm global change status report for high,low order.


* **RTL Instant Name**    : `alm_glbchg`

* **Address**             : `0x00_0005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:16]`|`glbstachglo`| global status change bit for low order slice - ocid11...ocid3,ocid1,ocid10...ocid2,ocid0| `RO`| `0x0`| `0x0`|
|`[15:0]`|`glbstachghi`| global status change bit for high order slice - ocid15...ocid3,ocid1,ocid14...ocid2,ocid0| `RO`| `0x0`| `0x0 // End: Begin:`|

###POH Interrupt  Global Status Out Report

* **Description**           

This register is used to get POH alarm global change status report for high,low order after ANDED with mask.


* **RTL Instant Name**    : `alm_glbchgo`

* **Address**             : `0x00_0006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[27:16]`|`glbstachglo`| global status change bit for low order slice - ocid11...ocid3,ocid1,ocid10...ocid2,ocid0| `RO`| `0x0`| `0x0`|
|`[15:0]`|`glbstachghi`| global status change bit for high order slice - ocid15...ocid3,ocid1,ocid14...ocid2,ocid0| `RO`| `0x0`| `0x0 // End: Begin:`|

###AME version

* **Description**           

This register is used to get Version of Alarm Monitoring Engine


* **RTL Instant Name**    : `upen_amever`

* **Address**             : `0x0F_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`ameengid`| Engine ID| `RO`| `0xAA41`| `0xAA41`|
|`[15:12]`|`ameglbsts`| STS bits| `RO`| `0x9`| `0x9`|
|`[11:08]`|`ameglbvt`| VT bits| `RO`| `0x5`| `0x5`|
|`[07:00]`|`ameglbver`| Version| `RO`| `0x10`| `0x10 End: Begin:`|

###AME Control

* **Description**           

This register is used to control operation of AM engine


* **RTL Instant Name**    : `upen_amectl`

* **Address**             : `0x0F_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| `RO`| `0xX`| `0xX`|
|`[05:04]`|`amectlscale`| Scale timer, devide by 2^AMECtlScale<br>{0} -> :1 <br>{1} -> :2 <br>{2} -> :4 (Testbend only) <br>{3} -> :8 (Testbend only)| `RW`| `0x0`| `0x0`|
|`[03:03]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[02:02]`|`amectlforceint`| Force Interrupt Pin| `RW`| `0x0`| `0x0`|
|`[01:01]`|`amectlclrhoff`| Clear with hold_off_timer| `RW`| `0x1`| `0x1`|
|`[00:00]`|`amectlactive`| Active| `RW`| `0x1`| `0x1 End: Begin:`|

###AME SF/SD Alarm Mask

* **Description**           

This register is used to Select alarm {ais,lop,plm,ber_sd,ber_sf} into SF/SD table


* **RTL Instant Name**    : `upen_amemask`

* **Address**             : `0x0F_0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RO`| `0xX`| `0xX`|
|`[15:08]`|`amesdalmmask`| SD Alarm Mask, set 1 to choose bit_0: choose BER_SF alarm bit_1: choose BER_SD alarm bit_2: choose PLM alarm bit_3: choose LOP alarm bit_4: choose AIS alarm| `RW`| `0x1B`| `0x1B`|
|`[07:00]`|`amesfalmmask`| SF Alarm Mask| `RW`| `0x1B`| `0x1B bit_0: choose BER_SF alarm bit_1: choose BER_SD alarm bit_2: choose PLM alarm bit_3: choose LOP alarm bit_4: choose AIS alarm`|

###AME MASR Interrupt Mask

* **Description**           

This register is used to enable/disable interrupt pin


* **RTL Instant Name**    : `upen_ameintdi`

* **Address**             : `0x0F_0010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| `RO`| `0xX`| `0xX`|
|`[02:02]`|`amesddis`| SD Interrupt disable| `RW`| `0x0`| `0x0`|
|`[01:01]`|`amesfdis`| SF Interrupt disable| `RW`| `0x0`| `0x0`|
|`[00:00]`|`ameintdis`| AME Interrupt disable| `RW`| `0x1`| `0x1 End: Begin:`|

###AME SF MASR register

* **Description**           

This register is used to report SF row STS/VT table


* **RTL Instant Name**    : `upen_sfrowsts`

* **Address**             : `0x0F_0011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RO`| `0xX`| `0xX`|
|`[23:12]`|`amesfrowvt`| SF VT ROW status| `RC`| `0x0`| `0x0 bit_0: STS#0-STS#31 bit_1: STS#32-STS#63 ..`|
|`[11:00]`|`amesfrowsts`| SF STS ROW Status| `RC`| `0x0`| `0x0 bit_0: STS#0-STS#31 bit_1: STS#32-STS#63 ..`|

###AME SD MASR register

* **Description**           

This register is used to report SD row STS/VT table


* **RTL Instant Name**    : `upen_sdrowsts`

* **Address**             : `0x0F_0012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RO`| `0xX`| `0xX`|
|`[23:12]`|`amesdrowvt`| SD VT ROW status bit_0: STS#0-STS#31 bit_1: STS#32-STS#63 ..| `RC`| `0x0`| `0x0`|
|`[11:00]`|`amesdrowsts`| SD STS ROW Status| `RC`| `0x0`| `0x0 bit_0: STS#0-STS#31 bit_1: STS#32-STS#63 ..`|

###AME STS Status table

* **Description**           

This register is used to report STS status


* **RTL Instant Name**    : `upen_xxstasts`

* **Address**             : `0x0F_2000`

* **Formula**             : `0x0F_2000 + $sd_table*0x8000 + $sts_group`

* **Where**               : 

    * `$sd_table(0-1): 1 -> SD table, 0 -> SF table`

    * `$sts_group(0-11) sts group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`amestasts`| STS Table Status bit_0: STS#0 bit_1: STS#1 ...| `RO`| `0x0`| `0x0 End: Begin:`|

###AME STS Enable table

* **Description**           

This register is used to enable STS table


* **RTL Instant Name**    : `upen_xxcfgsts`

* **Address**             : `0x0F_3000`

* **Formula**             : `0x0F_3000 + $sd_table*0x8000 + $sts_group`

* **Where**               : 

    * `$sd_table(0-1): 1 -> SD table, 0 -> SF table`

    * `$sts_group(0-11) sts group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ameenasts`| STS Table Enabl bit_0: STS#0 bit_1: STS#1 ...| `RW`| `0x0`| `0x0 End: Begin:`|

###AME ROW VT Status table

* **Description**           

This register is used to report Row VT status


* **RTL Instant Name**    : `upen_xxrowvt`

* **Address**             : `0x0F_2100`

* **Formula**             : `0x0F_2100 + $sd_table*0x8000 + $sts_group`

* **Where**               : 

    * `$sd_table(0-1): 1 -> SD table, 0 -> SF table`

    * `$sts_group(0-11) sts_group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`amestarowvt`| Row VT Table Status bit_0: STS#0 bit_1: STS#1 ...| `RC`| `0x0`| `0x0 End: Begin:`|

###AME VT Status table

* **Description**           

This register is used to report VT status


* **RTL Instant Name**    : `upen_xxstavt`

* **Address**             : `0x0F_2400`

* **Formula**             : `0x0F_2400 + $sd_table*0x8000 + $sts_id`

* **Where**               : 

    * `$sd_table(0-1): 1 -> SD table, 0 -> SF table`

    * `$sts_id(0-383) sts id`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RO`| `0xX`| `0xX`|
|`[27:00]`|`amestavt`| VT Table Status bit_0: VT#0 bit_1: VT#1 ...| `RO`| `0x0`| `0x0 End: Begin:`|

###AME VT Enable table

* **Description**           

This register is used to enable VT table


* **RTL Instant Name**    : `upen_xxcfgvt`

* **Address**             : `0x0F_3400`

* **Formula**             : `0x0F_3400 + $sd_table*0x8000 + $sts_id`

* **Where**               : 

    * `$sd_table(0-1): 1 -> SD table, 0 -> SF table`

    * `$sts_id(0-383) sts_id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RO`| `0xX`| `0xX`|
|`[27:00]`|`ameenavt`| VT Table Enable bit_0: VT#0 bit_1: VT#1 ...| `RW`| `0x0`| `0x0 End: Begin:`|

###AME STS Hold Off timer

* **Description**           

This register is used to config hold off timer for STS alarm, hold_off_timer = AMETmsMax * AMETmsUnit


* **RTL Instant Name**    : `upen_xxcfgtms`

* **Address**             : `0x0F_3800`

* **Formula**             : `0x0F_3800 + $sts_id`

* **Where**               : 

    * `$sts_id(0-383) sts_id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RO`| `0xX`| `0xX`|
|`[09:03]`|`ametmsmax`| Hold off timer| `RW`| `0xA`| `0x0`|
|`[02:00]`|`ametmsunit`| Timer Unit<br>{0}: 2ms <br>{1}: 10ms <br>{2}: 50ms <br>{3}: 100ms <br>{4}: 200ms <br>{5}: 1000ms <br>{6}: unused <br>{7}: unused| `RW`| `0x1`| `0x0 Begin:`|

###AME VT Hold Off timer

* **Description**           

This register is used to config hold off timer for VT alarm, hold_off_timer = AMETmvMax * AMETmvUnit


* **RTL Instant Name**    : `upen_xxcfgtmv`

* **Address**             : `0x0F_4000`

* **Formula**             : `0x0F_4000 + $sts_id*0x20 + $vt_id`

* **Where**               : 

    * `$sts_id(0-383) vt_id`

    * `$vt_id(0-27)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RO`| `0xX`| `0xX`|
|`[09:03]`|`ametmvmax`| Hold off timer| `RW`| `0x1`| `0x0`|
|`[02:00]`|`ametmvunit`| Timer Unit<br>{0}: 2ms <br>{1}: 10ms <br>{2}: 50ms <br>{3}: 100ms <br>{4}: 200ms <br>{5}: 1000ms <br>{6}: unused <br>{7}: unused| `RW`| `0x3`| `0x0 Begin:`|

###AME LO ID Lookup

* **Description**           

This register is used to loopkup LO {slice_id,sts_id} -> TFI-5 {slice_id,sts_id}


* **RTL Instant Name**    : `upen_xxcfglkv`

* **Address**             : `0x0F_1000`

* **Formula**             : `0x0F_1000 + $slice_id*0x40 + $sts_id`

* **Where**               : 

    * `$slice_id (0-7): slice id`

    * `$sts_id(0-47) sts_id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:09]`|`unused`| *n/a*| `RO`| `0xX`| `0xX`|
|`[08:06]`|`amelkvline`| TFI-5 Line ID| `RW`| `0x0`| `0x0`|
|`[05:00]`|`amelkvsts`| TFI-5 STS ID| `RW`| `0x0`| `0x0 End: Begin:`|

###AME HO ID Lookup

* **Description**           

This register is used to loopkup HO {slice_id,sts_id} -> TFI-5 {slice_id,sts_id}


* **RTL Instant Name**    : `upen_xxcfglks`

* **Address**             : `0x0F_1800`

* **Formula**             : `0x0F_1800 + $slice_id*0x40 + $sts_id`

* **Where**               : 

    * `$slice_id (0-7): slice id  (also line id)`

    * `$sts_id(0-47) sts_id`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:09]`|`unused`| *n/a*| `RO`| `0xX`| `0xX`|
|`[08:06]`|`amelksline`| TFI-5 Line ID| `RW`| `0x0`| `0x0`|
|`[05:00]`|`amelkssts`| TFI-5 STS ID| `RW`| `0x0`| `0x0 End: #******************** Begin:`|

###Read HA Address Bit3_0 Control

* **Description**           

This register is used to send HA read address bit3_0 to HA engine


* **RTL Instant Name**    : `rdha3_0_control`

* **Address**             : `0x000100`

* **Formula**             : `0x000100 + HaAddr3_0`

* **Where**               : 

    * `$HaAddr3_0(0-F): HA Address Bit3_0`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr3_0`| Read value will be 0x00100 plus HaAddr3_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit7_4 Control 155*

* **Description**           

This register is used to send HA read address bit7_4 to HA engine


* **RTL Instant Name**    : `rdha7_4_control`

* **Address**             : `0x000110`

* **Formula**             : `0x000110 + HaAddr7_4`

* **Where**               : 

    * `$HaAddr7_4(0-F): HA Address Bit7_4`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr7_4`| Read value will be 0x00100 plus HaAddr7_4| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit11_8 Control 155*

* **Description**           

This register is used to send HA read address bit11_8 to HA engine


* **RTL Instant Name**    : `rdha11_8_control`

* **Address**             : `0x000120`

* **Formula**             : `0x000120 + HaAddr11_8`

* **Where**               : 

    * `$HaAddr11_8(0-F): HA Address Bit11_8`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr11_8`| Read value will be 0x00100 plus HaAddr11_8| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit15_12 Control 155*

* **Description**           

This register is used to send HA read address bit15_12 to HA engine


* **RTL Instant Name**    : `rdha15_12_control`

* **Address**             : `0x000130`

* **Formula**             : `0x000130 + HaAddr15_12`

* **Where**               : 

    * `$HaAddr15_12(0-F): HA Address Bit15_12`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr15_12`| Read value will be 0x00100 plus HaAddr15_12| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit19_16 Control 155*

* **Description**           

This register is used to send HA read address bit19_16 to HA engine


* **RTL Instant Name**    : `rdha19_16_control`

* **Address**             : `0x000140`

* **Formula**             : `0x000140 + HaAddr19_16`

* **Where**               : 

    * `$HaAddr19_16(0-F): HA Address Bit19_16`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr19_16`| Read value will be 0x00100 plus HaAddr19_16| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit23_20 Control 155*

* **Description**           

This register is used to send HA read address bit23_20 to HA engine


* **RTL Instant Name**    : `rdha23_20_control`

* **Address**             : `0x000150`

* **Formula**             : `0x000150 + HaAddr23_20`

* **Where**               : 

    * `$HaAddr23_20(0-F): HA Address Bit23_20`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr23_20`| Read value will be 0x00100 plus HaAddr23_20| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit24 and Data Control 155*

* **Description**           

This register is used to send HA read address bit24 to HA engine to read data


* **RTL Instant Name**    : `rdha24data_control`

* **Address**             : `0x000160`

* **Formula**             : `0x000160 + HaAddr24`

* **Where**               : 

    * `$HaAddr24(0-1): HA Address Bit24`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata31_0`| HA read data bit31_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data63_32 155*

* **Description**           

This register is used to read HA dword2 of data.


* **RTL Instant Name**    : `rdha_hold63_32`

* **Address**             : `0x000170`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata63_32`| HA read data bit63_32| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data95_64 155*

* **Description**           

This register is used to read HA dword3 of data.


* **RTL Instant Name**    : `rdindr_hold95_64`

* **Address**             : `0x000171`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata95_64`| HA read data bit95_64| `RW`| `0xx`| `0xx End: #******************** Begin:`|

###Read HA Address Bit3_0 Control 311*

* **Description**           

This register is used to send HA read address bit3_0 to HA engine


* **RTL Instant Name**    : `rdha3_0_control311`

* **Address**             : `0x010100`

* **Formula**             : `0x010100 + HaAddr3_0`

* **Where**               : 

    * `$HaAddr3_0(0-F): HA Address Bit3_0`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr3_0`| Read value will be 0x10100 plus HaAddr3_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit7_4 Control 311*

* **Description**           

This register is used to send HA read address bit7_4 to HA engine


* **RTL Instant Name**    : `rdha7_4_control311`

* **Address**             : `0x010110`

* **Formula**             : `0x010110 + HaAddr7_4`

* **Where**               : 

    * `$HaAddr7_4(0-F): HA Address Bit7_4`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr7_4`| Read value will be 0x10100 plus HaAddr7_4| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit11_8 Control 311*

* **Description**           

This register is used to send HA read address bit11_8 to HA engine


* **RTL Instant Name**    : `rdha11_8_control311`

* **Address**             : `0x010120`

* **Formula**             : `0x010120 + HaAddr11_8`

* **Where**               : 

    * `$HaAddr11_8(0-F): HA Address Bit11_8`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr11_8`| Read value will be 0x10100 plus HaAddr11_8| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit15_12 Control 311*

* **Description**           

This register is used to send HA read address bit15_12 to HA engine


* **RTL Instant Name**    : `rdha15_12_control311`

* **Address**             : `0x010130`

* **Formula**             : `0x010130 + HaAddr15_12`

* **Where**               : 

    * `$HaAddr15_12(0-F): HA Address Bit15_12`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr15_12`| Read value will be 0x10100 plus HaAddr15_12| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit19_16 Control 311*

* **Description**           

This register is used to send HA read address bit19_16 to HA engine


* **RTL Instant Name**    : `rdha19_16_control311`

* **Address**             : `0x010140`

* **Formula**             : `0x010140 + HaAddr19_16`

* **Where**               : 

    * `$HaAddr19_16(0-F): HA Address Bit19_16`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr19_16`| Read value will be 0x10100 plus HaAddr19_16| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit23_20 Control 311*

* **Description**           

This register is used to send HA read address bit23_20 to HA engine


* **RTL Instant Name**    : `rdha23_20_control311`

* **Address**             : `0x010150`

* **Formula**             : `0x010150 + HaAddr23_20`

* **Where**               : 

    * `$HaAddr23_20(0-F): HA Address Bit23_20`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr23_20`| Read value will be 0x10100 plus HaAddr23_20| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit24 and Data Control 311*

* **Description**           

This register is used to send HA read address bit24 to HA engine to read data


* **RTL Instant Name**    : `rdha24data_control311`

* **Address**             : `0x010160`

* **Formula**             : `0x010160 + HaAddr24`

* **Where**               : 

    * `$HaAddr24(0-1): HA Address Bit24`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata31_0`| HA read data bit31_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data63_32 311*

* **Description**           

This register is used to read HA dword2 of data.


* **RTL Instant Name**    : `rdha_hold63_32clk311`

* **Address**             : `0x010170`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata63_32`| HA read data bit63_32| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data95_64 311*

* **Description**           

This register is used to read HA dword3 of data.


* **RTL Instant Name**    : `rdindr_hold95_64clk311`

* **Address**             : `0x010171`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata95_64`| HA read data bit95_64| `RW`| `0xx`| `0xx End:`|

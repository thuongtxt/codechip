## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_PDH_PRM
####Register Table

|Name|Address|
|-----|-----|
|`CONFIG CONTROL TX STUFF 0`|`0x0_C000`|
|`TX CONFIG FORCE ERROR PARITY`|`0x0_C010`|
|`TX CONFIG DIS PARITY`|`0x0_C011`|
|`TX STICKY PARITY`|`0x0_C012`|
|`CONFIG PRM BIT C/R & STANDARD Tx`|`0x1_0000 - 0x1_3FFF`|
|`CONFIG PRM BIT L/B Tx`|`0x1_4000 - 0x1_7FFF`|
|`COUNTER BYTE MESSAGE TX PRM`|`0x2_0000 - 0x2_7FFF`|
|`COUNTER PACKET MESSAGE TX PRM`|`0x2_8000 - 0x2_FFFF`|
|`Config Buff Message PRM STANDARD`|`0x4_0000 - 0x4_3FFF`|
|`CONFIG CONTROL DESTUFF 0`|`0x4_F000`|
|`COUNTER GOOD MESSAGE RX PRM`|`0x6_0000 - 0x6_7FFF`|
|`COUNTER DROP MESSAGE RX PRM`|`0x6_8000 - 0x6_FFFF`|
|`COUNTER MISS MESSAGE RX PRM`|`0x7_0000 - 0x7_7FFF`|
|`COUNTER BYTE MESSAGE RX PRM`|`0x7_8000 - 0x7_FFFF`|
|`RX CONFIG FORCE ERROR PARITY`|`0x4_F400`|
|`RX CONFIG DIS PARITY`|`0x4_F401`|
|`RX STICKY PARITY`|`0x4_F402`|
|`PRM LB per Channel Interrupt Enable Control`|`0x4_8000-0x4_87FF`|
|`PRM LB Interrupt per Channel Interrupt Status`|`0x4_8800-0x4_8FFF`|
|`PRM LB Interrupt per Channel Current Status`|`0x4_9000-0x4_97FF`|
|`PRM LB Interrupt per Channel Interrupt OR Status`|`0x4_9800-0x4_9820`|
|`PRM LB Interrupt OR Status`|`0x4_9FFF`|
|`PRM LB Interrupt Enable Control`|`0x4_9FFE`|


###CONFIG CONTROL TX STUFF 0

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_cfg_ctrl0`

* **Address**             : `0x0_C000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`out_txcfg_ctrl0`| reserve| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`fcs_err_ins`| (0): disable,(1): enable insert error| `R/W`| `0x0`| `0x0`|
|`[06:02]`|`reserve1`| reserve1| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`fcsinscfg`| (1) enable insert FCS, (0) disable| `R/W`| `0x1`| `0x1`|
|`[00:00]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0 End: Begin:`|

###TX CONFIG FORCE ERROR PARITY

* **Description**           

config control FORCE ERROR PARITY


* **RTL Instant Name**    : `upen_txcfg_force`

* **Address**             : `0x0_C010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `26`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[25:25]`|`cfgforce_mdlbuff2_eng8`| force error MDL buffer 2, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[24:24]`|`cfgforce_mdlbuff1_eng8`| force error MDL buffer 1, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[23:23]`|`cfgforce_mdl_eng8`| force error MDL config, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[22:22]`|`cfgforce_mdlbuff2_eng7`| force error MDL buffer 2, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[21:21]`|`cfgforce_mdlbuff1_eng7`| force error MDL buffer 1, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[20:20]`|`cfgforce_mdl_eng7`| force error MDL config, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[19:19]`|`cfgforce_mdlbuff2_eng6`| force error MDL buffer 2, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[18:18]`|`cfgforce_mdlbuff1_eng6`| force error MDL buffer 1, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[17:17]`|`cfgforce_mdl_eng6`| force error MDL config, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[16:16]`|`cfgforce_mdlbuff2_eng5`| force error MDL buffer 2, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[15:15]`|`cfgforce_mdlbuff1_eng5`| force error MDL buffer 1, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[14:14]`|`cfgforce_mdl_eng5`| force error MDL config, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[13:13]`|`cfgforce_mdlbuff2_eng4`| force error MDL buffer 2, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfgforce_mdlbuff1_eng4`| force error MDL buffer 1, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`cfgforce_mdl_eng4`| force error MDL config, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`cfgforce_mdlbuff2_eng3`| force error MDL buffer 2, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`cfgforce_mdlbuff1_eng3`| force error MDL buffer 1, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`cfgforce_mdl_eng3`| force error MDL config, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`cfgforce_mdlbuff2_eng2`| force error MDL buffer 2, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`cfgforce_mdlbuff1_eng2`| force error MDL buffer 1, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`cfgforce_mdl_eng2`| force error MDL config, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`cfgforce_mdlbuff2_eng1`| force error MDL buffer 2, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`cfgforce_mdlbuff1_eng1`| force error MDL buffer 1, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`cfgforce_mdl_eng1`| force error MDL config, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`cfgforce_lbprm`| force error PRM LB config, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfgforce_prm`| force error PRM config, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0 End Begin:`|

###TX CONFIG DIS PARITY

* **Description**           

config control DIS PARITY


* **RTL Instant Name**    : `upen_txcfg_dis`

* **Address**             : `0x0_C011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `26`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[25:25]`|`cfgdis_mdlbuff2_eng8`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[24:24]`|`cfgdis_mdlbuff1_eng8`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[23:23]`|`cfgdis_mdl_eng8`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[22:22]`|`cfgdis_mdlbuff2_eng7`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[21:21]`|`cfgdis_mdlbuff1_eng7`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[20:20]`|`cfgdis_mdl_eng7`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[19:19]`|`cfgdis_mdlbuff2_eng6`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[18:18]`|`cfgdis_mdlbuff1_eng6`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[17:17]`|`cfgdis_mdl_eng6`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[16:16]`|`cfgdis_mdlbuff2_eng5`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[15:15]`|`cfgdis_mdlbuff1_eng5`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[14:14]`|`cfgdis_mdl_eng5`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[13:13]`|`cfgdis_mdlbuff2_eng4`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfgdis_mdlbuff1_eng4`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`cfgdis_mdl_eng4`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`cfgdis_mdlbuff2_eng3`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`cfgdis_mdlbuff1_eng3`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`cfgdis_mdl_eng3`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`cfgdis_mdlbuff2_eng2`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`cfgdis_mdlbuff1_eng2`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`cfgdis_mdl_eng2`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`cfgdis_mdlbuff2_eng1`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`cfgdis_mdlbuff1_eng1`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`cfgdis_mdl_eng1`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`cfgdis_lbprm`| dis error PRM LB config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfgdis_prm`| dis error PRM config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0 End Begin:`|

###TX STICKY PARITY

* **Description**           

config control DeStuff global


* **RTL Instant Name**    : `upen_txsticky_par`

* **Address**             : `0x0_C012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `26`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[25:25]`|`stk_mdlbuff2_eng8`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[24:24]`|`stk_mdlbuff1_eng8`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[23:23]`|`stk_mdl_eng8`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[22:22]`|`stk_mdlbuff2_eng7`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[21:21]`|`stk_mdlbuff1_eng7`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[20:20]`|`stk_mdl_eng7`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[19:19]`|`stk_mdlbuff2_eng6`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[18:18]`|`stk_mdlbuff1_eng6`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[17:17]`|`stk_mdl_eng6`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[16:16]`|`stk_mdlbuff2_eng5`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[15:15]`|`stk_mdlbuff1_eng5`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[14:14]`|`stk_mdl_eng5`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[13:13]`|`stk_mdlbuff2_eng4`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`stk_mdlbuff1_eng4`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[11:11]`|`stk_mdl_eng4`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[10:10]`|`stk_mdlbuff2_eng3`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[09:09]`|`stk_mdlbuff1_eng3`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`stk_mdl_eng3`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`stk_mdlbuff2_eng2`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`stk_mdlbuff1_eng2`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`stk_mdl_eng2`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`stk_mdlbuff2_eng1`| dis error MDL buffer 2, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`stk_mdlbuff1_eng1`| dis error MDL buffer 1, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`stk_mdl_eng1`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`stk_lbprm`| dis error PRM LB config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`stk_prm`| dis error PRM config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0 End Begin:`|

###CONFIG PRM BIT C/R & STANDARD Tx

* **Description**           

Config PRM bit C/R & Standard


* **RTL Instant Name**    : `upen_prm_txcfgcr`

* **Address**             : `0x1_0000 - 0x1_3FFF`

* **Formula**             : `0x1_0000+$STSID[4:3]*7 + $VTGID*128 + $SLICEID*32 + $STSID[2:0]*4 + $VTID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$SLICEID (0-3) : SLICE ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[03:03]`|`cfg_prmen_tx`| config enable Tx, (0) is disable (1) is enable| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`cfg_prmfcs_tx`| config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`cfg_prmstd_tx`| config standard Tx, (0) is ANSI, (1) is AT&T| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfg_prm_cr`| config bit command/respond PRM message| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG PRM BIT L/B Tx

* **Description**           

CONFIG PRM BIT L/B Tx


* **RTL Instant Name**    : `upen_prm_txcfglb`

* **Address**             : `0x1_4000 - 0x1_7FFF`

* **Formula**             : `0x1_4000+$SLICEID*1024 + $STSID*32 + $VTGID*4 + $VTID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$SLICEID (0-3) : SLICE ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `2`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[01:01]`|`cfg_prm_enlb`| config enable CPU config LB bit, (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfg_prm_lb`| config bit Loopback PRM message| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE TX PRM

* **Description**           

counter byte message PRM


* **RTL Instant Name**    : `upen_txprm_cnt_byte`

* **Address**             : `0x2_0000 - 0x2_7FFF`

* **Formula**             : `0x2_0000+$STSID[4:3]*7 + $VTGID*128 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 4096`

* **Where**               : 

    * `$UPRO (0-1) : upen read only`

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$SLICEID (0-3) : SLICE ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_byte_prm`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER PACKET MESSAGE TX PRM

* **Description**           

counter packet message PRM


* **RTL Instant Name**    : `upen_txprm_cnt_pkt`

* **Address**             : `0x2_8000 - 0x2_FFFF`

* **Formula**             : `0x2_8000+$STSID[4:3]*7 + $VTGID*128 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 4096`

* **Where**               : 

    * `$UPRO (0-1) : upen read only`

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$SLICEID (0-3) : SLICE ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `15`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14:00]`|`cnt_pkt_prm`| value counter packet| `R/W`| `0x0`| `0x0 End: Begin:`|

###Config Buff Message PRM STANDARD

* **Description**           

config standard message PRM


* **RTL Instant Name**    : `upen_rxprm_cfgstd`

* **Address**             : `0x4_0000 - 0x4_3FFF`

* **Formula**             : `0x4_0000+$STSID[4:3]*7 + $VTGID*128 + $SLICEID*32 + $STSID[2:0]*4 + $VTID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$SLICEID (0-3) : SLICE ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `3`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[02:02]`|`cfg_prm_cr`| config C/R bit expected| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`cfg_prmfcs_rx`| config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfg_std_prm`| config standard   (0) is ANSI, (1) is AT&T| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG CONTROL DESTUFF 0

* **Description**           

config control DeStuff global


* **RTL Instant Name**    : `upen_destuff_ctrl0`

* **Address**             : `0x4_F000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`cfg_crmon`| (0) : disable, (1) : enable| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`reserve1`| reserve| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fcsmon`| (0) : disable monitor, (1) : enable| `R/W`| `0x1`| `0x1`|
|`[02:01]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`headermon_en`| (1) : enable monitor, (0) disable| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER GOOD MESSAGE RX PRM

* **Description**           

counter good message PRM


* **RTL Instant Name**    : `upen_rxprm_gmess`

* **Address**             : `0x6_0000 - 0x6_7FFF`

* **Formula**             : `0x6_0000+$STSID[4:3]*7 + $VTGID*128 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 4096`

* **Where**               : 

    * `$UPRO (0-1) : upen read only`

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-3) : VTG ID`

    * `$SLICEID (0-3) : SLICE ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `15`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14:00]`|`cnt_gmess_prm`| value counter packet| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER DROP MESSAGE RX PRM

* **Description**           

counter drop message PRM


* **RTL Instant Name**    : `upen_rxprm_drmess`

* **Address**             : `0x6_8000 - 0x6_FFFF`

* **Formula**             : `0x6_8000+$STSID[4:3]*7 + $VTGID*128 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 4096`

* **Where**               : 

    * `$UPRO (0-1) : upen read only`

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$SLICEID (0-3) : SLICE ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `15`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14:00]`|`cnt_drmess_prm`| value counter packet| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER MISS MESSAGE RX PRM

* **Description**           

counter miss message PRM


* **RTL Instant Name**    : `upen_rxprm_mmess`

* **Address**             : `0x7_0000 - 0x7_7FFF`

* **Formula**             : `0x7_0000+$STSID[4:3]*7 + $VTGID*128 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 4096`

* **Where**               : 

    * `$UPRO (0-1) : upen read only`

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$SLICEID (0-3) : SLICE ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `15`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[14:00]`|`cnt_mmess_prm`| value counter packet| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE RX PRM

* **Description**           

counter byte message PRM


* **RTL Instant Name**    : `upen_rxprm_cnt_byte`

* **Address**             : `0x7_8000 - 0x7_FFFF`

* **Formula**             : `0x7_8000+$STSID[4:3]*7 + $VTGID*128 + $SLICEID*32 + $STSID[2:0]*4 + $VTID + $UPRO * 4096`

* **Where**               : 

    * `$UPRO (0-1) : upen read only`

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$SLICEID (0-3) : SLICE ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_byte_rxprm`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###RX CONFIG FORCE ERROR PARITY

* **Description**           

config control DeStuff global


* **RTL Instant Name**    : `upen_rxcfg_force`

* **Address**             : `0x4_F400`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[01:01]`|`cfgforce_rxmdl`| force error MDL config, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfgforce_rxprm`| force error PRM config, (0) : disable, (1) : enable force| `R/W`| `0x0`| `0x0 End Begin:`|

###RX CONFIG DIS PARITY

* **Description**           

config control DeStuff global


* **RTL Instant Name**    : `upen_rxcfg_dis`

* **Address**             : `0x4_F401`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[01:01]`|`cfgdis_rxmdl`| dis error MDL config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfgdis_rxprm`| dis error PRM config, (0) : disable, (1) : enable dis| `R/W`| `0x0`| `0x0 End Begin:`|

###RX STICKY PARITY

* **Description**           

config control DeStuff global


* **RTL Instant Name**    : `upen_rxsticky_par`

* **Address**             : `0x4_F402`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[01:01]`|`stk_rxmdl`| sticky for mdl config error parity| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`stk_rxprm`| sticky for prm config error parity| `R/W`| `0x0`| `0x0 End Begin:`|

###PRM LB per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable


* **RTL Instant Name**    : `prm_cfg_lben_int`

* **Address**             : `0x4_8000-0x4_87FF`

* **Formula**             : `0x4_8000+ $STSID*32 + $VTGID*4 + $VTID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3]`|`prm_cfglben_int3`| Set 1 to enable change event to generate an interrupt slice 3| `RW`| `0x0`| `0x0`|
|`[2]`|`prm_cfglben_int2`| Set 1 to enable change event to generate an interrupt slice 2| `RW`| `0x0`| `0x0`|
|`[1]`|`prm_cfglben_int1`| Set 1 to enable change event to generate an interrupt slice 1| `RW`| `0x0`| `0x0`|
|`[0]`|`prm_cfglben_int0`| Set 1 to enable change event to generate an interrupt slice 0| `RW`| `0x0`| `0x0 End: Begin:`|

###PRM LB Interrupt per Channel Interrupt Status

* **Description**           

This is the per Channel interrupt status.


* **RTL Instant Name**    : `prm_lb_int_sta`

* **Address**             : `0x4_8800-0x4_8FFF`

* **Formula**             : `0x4_8800+ $STSID*32 + $VTGID*4 + $VTID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `4`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3]`|`prm_lbint_sta3`| Set 1 if there is a change event slice 3| `RW`| `0x0`| `0x0`|
|`[2]`|`prm_lbint_sta2`| Set 1 if there is a change event slice 2| `RW`| `0x0`| `0x0`|
|`[1]`|`prm_lbint_sta1`| Set 1 if there is a change event slice 1| `RW`| `0x0`| `0x0`|
|`[0]`|`prm_lbint_sta0`| Set 1 if there is a change event slice 0| `RW`| `0x0`| `0x0 End: Begin:`|

###PRM LB Interrupt per Channel Current Status

* **Description**           

This is the per Channel Current status.


* **RTL Instant Name**    : `prm_lb_int_crrsta`

* **Address**             : `0x4_9000-0x4_97FF`

* **Formula**             : `0x4_9000+ $STSID*32 + $VTGID*4 + $VTID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$VTGID (0-6) : VTG ID`

    * `$VTID (0-3) : VT ID`

* **Width**               : `4`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3]`|`prm_lbint_crrsta3`| Current status of event slice 3| `RW`| `0x0`| `0x0`|
|`[2]`|`prm_lbint_crrsta2`| Current status of event slice 2| `RW`| `0x0`| `0x0`|
|`[1]`|`prm_lbint_crrsta1`| Current status of event slice 1| `RW`| `0x0`| `0x0`|
|`[0]`|`prm_lbint_crrsta0`| Current status of event slice 0| `RW`| `0x0`| `0x0 End: Begin:`|

###PRM LB Interrupt per Channel Interrupt OR Status

* **Description**           




* **RTL Instant Name**    : `prm_lb_intsta`

* **Address**             : `0x4_9800-0x4_9820`

* **Formula**             : `0x4_9800 +  $GID`

* **Where**               : 

    * `$GID (0-31) : group ID`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`prm_lbintsta`| Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###PRM LB Interrupt OR Status

* **Description**           

The register consists of 2 bits. Each bit is used to store Interrupt OR status.


* **RTL Instant Name**    : `prm_lb_sta_int`

* **Address**             : `0x4_9FFF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`prm_lbsta_int`| Set to 1 if any interrupt status bit is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###PRM LB Interrupt Enable Control

* **Description**           

The register consists of 2 interrupt enable bits .


* **RTL Instant Name**    : `prm_lb_en_int`

* **Address**             : `0x4_9FFE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`prm_lben_int`| Set to 1 to enable to generate interrupt.| `RW`| `0x0`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|Project|Initial version|




##OC192_MUX_OC48_16ch_wrap_RD
####Register Table

|Name|Address|
|-----|-----|
|`OC192 MUX OC48 Change Mode`|`0x0080-0x6080`|
|`OC192 MUX OC48 DRP`|`0x1000-0x7FFF`|
|`OC192 MUX OC48 LoopBack`|`0x0002-0x6002`|
|`OC192 MUX OC48 PLL Status`|`0x000B-0x600B`|
|`OC192 MUX OC48 TX Reset`|`0x000C-0x600C`|
|`OC192 MUX OC48 TX Reset`|`0x000D-0x600D`|
|`OC192 MUX OC48 LPMDFE Mode`|`0x000E-0x600E`|
|`OC192 MUX OC48 LPMDFE Reset`|`0x000F-0x600F`|
|`OC192 MUX OC48 TXDIFFCTRL`|`0x0010-0x6010`|
|`OC192 MUX OC48 TXPOSTCURSOR`|`0x0011-0x6011`|
|`OC192 MUX OC48 TXPRECURSOR`|`0x0011-0x6011`|


###OC192 MUX OC48 Change Mode

* **Description**           

Configurate Port mode, there is 4 group (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_Change_Mode`

* **Address**             : `0x0080-0x6080`

* **Formula**             : `0x0080+$G*0x2000+0x80`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`chg_done`| Change mode has done<br>{1} : Done| `W1C`| `0x0`| `0x0`|
|`[11:09]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[08:08]`|`chg_trig`| Trigger 0->1 to start changing mode| `R/W`| `0x0`| `0x0`|
|`[07:04]`|`chg_mode`| Port mode<br>{0} : OC192 <br>{1} : OC48 <br>{2} : OC12 <br>{4} : 10Ge <br>{5} : 1Ge <br>{6} : 100Fx| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`chg_port`| Sub Port ID<br>{0-3} : Group0 => Port 0-3 <br>{0-3} : Group1 => Port 4-7 <br>{0-3} : Group2 => Port 8-11 <br>{0-3} : Group3 => Port 12-15| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 DRP

* **Description**           

Read/Write DRP address of SERDES, there is 4 group (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_DRP`

* **Address**             : `0x1000-0x7FFF`

* **Formula**             : `0x1000+$G*0x2000+$P*0x400+$DRP`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

    * `$P(0-3) : Sub Port ID`

    * `$DRP(0-0x3FF) : DRP address, see UG578`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:00]`|`drp_rw`| DRP read/write value| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 LoopBack

* **Description**           

Configurate LoopBack, there is 4 group (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_LoopBack`

* **Address**             : `0x0002-0x6002`

* **Formula**             : `0x0002+$G*0x2000+0x2`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`lpback_subport3`| Loopback subport 3, Group 0 => Port3, Group 1 => Port 7, , Group 2 => Port 11, Group 3 => Port 15<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[11:08]`|`lpback_subport2`| Loopback subport 2, Group 0 => Port2, Group 1 => Port 6, , Group 2 => Port 10, Group 3 => Port 14<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[07:04]`|`lpback_subport1`| Loopback subport 1, Group 0 => Port1, Group 1 => Port 5, , Group 2 => Port 9, Group 3 => Port 13<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`lpback_subport0`| Loopback subport 0, Group 0 => Port0, Group 1 => Port 4, , Group 2 => Port 8, Group 3 => Port 12<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 PLL Status

* **Description**           

QPLL/CPLL status, there is 4 group (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_PLL_Status`

* **Address**             : `0x000B-0x600B`

* **Formula**             : `0x000B+$G*0x2000+0xB`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:29]`|`qpll1_lock_change`| QPLL1 has transition lock/unlock, Group 0-3<br>{1} : QPLL1_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[28:28]`|`qpll0_lock_change`| QPLL0 has transition lock/unlock, Group 0-3<br>{1} : QPLL0_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[27:26]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[25:25]`|`qpll1_lock`| QPLL0 is Locked, Group 0-3<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[24:24]`|`qpll0_lock`| QPLL0 is Locked, Group 0-3<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[23:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`cpll_lock_change`| CPLL has transition lock/unlock, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15<br>{1} : CPLL_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[11:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`cpll_lock`| CPLL is Locked, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15<br>{1} : Locked| `R_O`| `0x0`| `0x0`|

###OC192 MUX OC48 TX Reset

* **Description**           

Reset TX SERDES, there is 4 group (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_TX_Reset`

* **Address**             : `0x000C-0x600C`

* **Formula**             : `0x000C+$G*0x2000+0xC`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:16]`|`txrst_done`| TX Reset Done, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`txrst_trig`| Trige 0->1 to start reset TX SERDES, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 TX Reset

* **Description**           

Reset RX SERDES, there is 4 group (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_TX_Reset`

* **Address**             : `0x000D-0x600D`

* **Formula**             : `0x000D+$G*0x2000+0xD`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:16]`|`rxrst_done`| RX Reset Done, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`rxrst_trig`| Trige 0->1 to start reset RX SERDES, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 LPMDFE Mode

* **Description**           

Configure LPM/DFE mode , there is 4 group (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_LPMDFE_Mode`

* **Address**             : `0x000E-0x600E`

* **Formula**             : `0x000E+$G*0x2000+0xE`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`lpmdfe_mode`| bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15<br>{0} : DFE mode <br>{1} : LPM mode| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 LPMDFE Reset

* **Description**           

Reset LPM/DFE , there is 4 group (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_LPMDFE_Reset`

* **Address**             : `0x000F-0x600F`

* **Formula**             : `0x000F+$G*0x2000+0xF`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`lpmdfe_reset`| bit per sub port, Must be toggled after switching between modes to initialize adaptation, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15<br>{1} : reset| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 TXDIFFCTRL

* **Description**           

Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail, there is 4 group (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_TXDIFFCTRL`

* **Address**             : `0x0010-0x6010`

* **Formula**             : `0x0010+$G*0x2000+0x10`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:15]`|`txdiffctrl_subport3`| Group 3 => Port 12-15| `R/W`| `0x18`| `0x0`|
|`[14:10]`|`txdiffctrl_subport2`| Group 2 => Port 8-11| `R/W`| `0x18`| `0x0`|
|`[09:05]`|`txdiffctrl_subport1`| Group 1 => Port 4-7| `R/W`| `0x18`| `0x0`|
|`[04:00]`|`txdiffctrl_subport0`| Group 0 => Port 0-3| `R/W`| `0x18`| `0x0`|

###OC192 MUX OC48 TXPOSTCURSOR

* **Description**           

Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail, there is 4 group (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_TXPOSTCURSOR`

* **Address**             : `0x0011-0x6011`

* **Formula**             : `0x0011+$G*0x2000+0x11`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:15]`|`txpostcursor_subport3`| Group 3 => Port 12-15| `R/W`| `0x0`| `0x0`|
|`[14:10]`|`txpostcursor_subport2`| Group 2 => Port 8-11| `R/W`| `0x0`| `0x0`|
|`[09:05]`|`txpostcursor_subport1`| Group 1 => Port 4-7| `R/W`| `0x0`| `0x0`|
|`[04:00]`|`txpostcursor_subport0`| Group 0 => Port 0-3| `R/W`| `0x0`| `0x0`|

###OC192 MUX OC48 TXPRECURSOR

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is 4 group (0-3), each group has 4 sub ports


* **RTL Instant Name**    : `OC192_MUX_OC48_TXPRECURSOR`

* **Address**             : `0x0011-0x6011`

* **Formula**             : `0x0011+$G*0x2000+0x11`

* **Where**               : 

    * `$G(0-3) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:15]`|`txprecursor_subport3`| Group 3 => Port 12-15| `R/W`| `0x0`| `0x0`|
|`[14:10]`|`txprecursor_subport2`| Group 2 => Port 8-11| `R/W`| `0x0`| `0x0`|
|`[09:05]`|`txprecursor_subport1`| Group 1 => Port 4-7| `R/W`| `0x0`| `0x0`|
|`[04:00]`|`txprecursor_subport0`| Group 0 => Port 0-3| `R/W`| `0x0`| `0x0`|

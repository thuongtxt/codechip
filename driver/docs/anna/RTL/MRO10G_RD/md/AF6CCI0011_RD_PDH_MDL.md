## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_PDH_MDL
####Register Table

|Name|Address|
|-----|-----|
|`Buff Message MDL BUFFER1`|`0x0000 - 0x012F`|
|`Buff Message MDL BUFFER1_2`|`0x0130 - 0x01C7`|
|`Config Buff Message MDL BUFFER2`|`0x0200 - 0x032F`|
|`Config Buff Message MDL BUFFER2_2`|`0x0330 - 0x03C7`|
|`SET TO HAVE PACKET MDL BUFFER 1`|`0x0420 - 0x437`|
|`SET TO HAVE PACKET MDL BUFFER 2`|`0x0440 - 0x457`|
|`CONFIG MDL Tx`|`0x0460 - 0x477`|
|`COUNTER BYTE MESSAGE TX MDL IDLE`|`0x0700 - 0x0717`|
|`COUNTER BYTE MESSAGE TX MDL PATH`|`0x0720 - 0x0737`|
|`COUNTER BYTE MESSAGE TX MDL TEST`|`0x0740 - 0x0757`|
|`COUNTER VALID MESSAGE TX MDL IDLE`|`0x0600 - 0x0697`|
|`COUNTER VALID MESSAGE TX MDL PATH`|`0x0620 - 0x06B7`|
|`COUNTER VALID MESSAGE TX MDL TEST R2C`|`0x0640 - 0x06D7`|
|`Buff Message MDL with configuration type`|`0x24000 - 0x25FFF`|
|`Config Buff Message MDL TYPE`|`0x23000 - 0x230BF`|
|`CONFIG CONTROL DESTUFF 0`|`0x23200`|
|`STICKY TO RECEIVE PACKET MDL BUFF CONFIG1`|`0x23210`|
|`STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 2`|`0x23211`|
|`STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 3`|`0x23212`|
|`STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 4`|`0x23213`|
|`STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 5`|`0x23214`|
|`STICKY TO RECEIVE PACKET MDL BUFF CONFIG 6`|`0x23215`|
|`COUNTER BYTE MESSAGE RX MDL IDLE`|`0x26000 - 0x264BF`|
|`COUNTER BYTE MESSAGE RX MDL PATH`|`0x26100 - 0x265BF`|
|`COUNTER BYTE MESSAGE RX MDL TEST`|`0x26200 - 0x266BF`|
|`COUNTER GOOD MESSAGE RX MDL IDLE`|`0x27000 - 0x278BF`|
|`COUNTER GOOD MESSAGE RX MDL PATH`|`0x27100 - 0x279BF`|
|`COUNTER GOOD MESSAGE RX MDL TEST`|`0x27200 - 0x27ABF`|
|`COUNTER DROP MESSAGE RX MDL IDLE`|`0x27300 - 0x27BBF`|
|`COUNTER DROP MESSAGE RX MDL PATH`|`0x27400 - 0x27CBF`|
|`COUNTER DROP MESSAGE RX MDL TEST R2C`|`0x27500 - 0x27DBF`|
|`MDL per Channel Interrupt Enable Control`|`0x29000-0x290FF`|
|`MDL Interrupt per Channel Interrupt Status`|`0x29100-0x291FF`|
|`MDL Interrupt per Channel Current Status`|`0x29200-0x292FF`|
|`MDL Interrupt per Channel Interrupt OR Status`|`0x29300-0x29307`|
|`MDL Interrupt OR Status`|`0x293FF`|
|`MDL Interrupt Enable Control`|`0x293FE`|


###Buff Message MDL BUFFER1

* **Description**           

config message MDL BUFFER Channel ID 0-15


* **RTL Instant Name**    : `upen_mdl_buffer1`

* **Address**             : `0x0000 - 0x012F`

* **Formula**             : `0x0000+ $SLICEID*0x800 + $DWORDID*16 + $DE3ID1`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DWORDID (0-18) : DWORD ID`

    * `$DE3ID1(0-15)  : DE3 ID1`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`idle_byte13`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`ilde_byte12`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`idle_byte11`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`idle_byte10`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###Buff Message MDL BUFFER1_2

* **Description**           

config message MDL BUFFER Channel ID 16-23


* **RTL Instant Name**    : `upen_mdl_buffer1_2`

* **Address**             : `0x0130 - 0x01C7`

* **Formula**             : `0x0130+ $SLICEID*0x800 + $DWORDID*8 + $DE3ID2`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DWORDID (0-18) : DWORD ID`

    * `$DE3ID2 (0-7) : DE3 ID2`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`idle_byte23`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`idle_byte22`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`idle_byte21`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`idle_byte20`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###Config Buff Message MDL BUFFER2

* **Description**           

config message MDL BUFFER2 Channel ID 0-15


* **RTL Instant Name**    : `upen_mdl_tp1`

* **Address**             : `0x0200 - 0x032F`

* **Formula**             : `0x0200+$SLICEID*0x800 + $DE3ID1 + $DWORDID*16`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DWORDID (0-18) : DWORD ID`

    * `$DE3ID1(0-15)  : DS3 E3 ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`tp_byte13`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`tp_byte12`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`tp_byte11`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`tp_byte10`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###Config Buff Message MDL BUFFER2_2

* **Description**           

config message MDL BUFFER2 Channel ID 16-23


* **RTL Instant Name**    : `upen_mdl_tp2`

* **Address**             : `0x0330 - 0x03C7`

* **Formula**             : `0x0330+$SLICEID*0x800 + $DWORDID*8+ $DE3ID2`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DWORDID (0-18) : DWORD ID`

    * `$DE3ID2 (0-7) : DE3 ID2`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`tp_byte23`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`tp_byte22`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`tp_byte21`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`tp_byte20`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###SET TO HAVE PACKET MDL BUFFER 1

* **Description**           

SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 1


* **RTL Instant Name**    : `upen_sta_idle_alren`

* **Address**             : `0x0420 - 0x437`

* **Formula**             : `0x0420+$SLICEID*0x800 +$DE3ID`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DE3ID (0-23) : DE3 ID`

* **Width**               : `1`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00:00]`|`idle_cfgen`| (0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 0| `R/W`| `0x0`| `0x0 End: Begin:`|

###SET TO HAVE PACKET MDL BUFFER 2

* **Description**           

SET/CLEAR to ALRM STATUS MESSAGE in BUFFER 2


* **RTL Instant Name**    : `upen_sta_tp_alren`

* **Address**             : `0x0440 - 0x457`

* **Formula**             : `0x0440+$SLICEID*0x800 +$DE3ID`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DE3ID (0-23) : DE3 ID`

* **Width**               : `1`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00:00]`|`tp_cfgen`| (0) : engine clear for indication to have sent, (1) CPU set for indication to have new message which must send for buffer 1| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG MDL Tx

* **Description**           

Config Tx MDL


* **RTL Instant Name**    : `upen_cfg_mdl`

* **Address**             : `0x0460 - 0x477`

* **Formula**             : `0x0460+$SLICEID*0x800 +$DE3ID`

* **Where**               : 

    * `$SLICEID(0-7) : SLICE ID`

    * `$DE3ID (0-23) : DE3 ID`

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`cfg_seq_tx`| config enable Tx continous, (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`cfg_entx`| config enable Tx, (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`cfg_fcs_tx`| config mode transmit FCS, (1) is T.403-MSB, (0) is T.107-LSB| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`cfg_mdlstd_tx`| config standard Tx, (0) is ANSI, (1) is AT&T| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfg_mdl_cr`| config bit command/respond MDL message, bit 0 is channel 0 of DS3| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE TX MDL IDLE

* **Description**           

counter byte read to clear IDLE message MDL


* **RTL Instant Name**    : `upen_txmdl_cntr2c_byteidle1`

* **Address**             : `0x0700 - 0x0717`

* **Formula**             : `0x0700+ $SLICEID*0x800 + $DE3ID + $UPRO * 128`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

    * `$UPRO (0-1) : UP READ ONLY`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_byte_idle_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE TX MDL PATH

* **Description**           

counter byte read to clear PATH message MDL


* **RTL Instant Name**    : `upen_txmdl_cntr2c_bytepath`

* **Address**             : `0x0720 - 0x0737`

* **Formula**             : `0x0720+ $SLICEID*0x800 + $DE3ID + $UPRO * 128`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

    * `$UPRO (0-1) : UP READ ONLY`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_byte_path_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE TX MDL TEST

* **Description**           

counter byte read to clear TEST message MDL


* **RTL Instant Name**    : `upen_txmdl_cntr2c_bytetest`

* **Address**             : `0x0740 - 0x0757`

* **Formula**             : `0x0740+ $SLICEID*0x800 + $DE3ID + $UPRO * 128`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

    * `$UPRO (0-1) : UP READ ONLY`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_byte_test_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER VALID MESSAGE TX MDL IDLE

* **Description**           

counter valid read to clear IDLE message MDL


* **RTL Instant Name**    : `upen_txmdl_cntr2c_valididle`

* **Address**             : `0x0600 - 0x0697`

* **Formula**             : `0x0600+ $SLICEID*0x800 + $DE3ID + $UPRO * 128`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

    * `$UPRO (0-1) : UP READ ONLY`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `18`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:00]`|`cntr2c_valid_idle_mdl`| value counter valid| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER VALID MESSAGE TX MDL PATH

* **Description**           

counter valid read to clear PATH message MDL


* **RTL Instant Name**    : `upen_txmdl_cntr2c_validpath`

* **Address**             : `0x0620 - 0x06B7`

* **Formula**             : `0x0620+ $SLICEID*0x800 + $DE3ID + $UPRO * 128`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

    * `$UPRO (0-1) : UP READ ONLY`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:00]`|`cntr2c_valid_path_mdl`| value counter valid| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER VALID MESSAGE TX MDL TEST R2C

* **Description**           

counter valid read to clear TEST message MDL


* **RTL Instant Name**    : `upen_txmdl_cntr2c_validtest`

* **Address**             : `0x0640 - 0x06D7`

* **Formula**             : `0x0640+ $SLICEID*0x800 + $DE3ID + $UPRO * 128`

* **Where**               : 

    * `$DE3ID (0-23) : DE3 ID`

    * `$UPRO (0-1) : UP READ ONLY`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:00]`|`cntr2c_valid_test_mdl`| value counter valid| `R/W`| `0x0`| `0x0 End: Begin:`|

###Buff Message MDL with configuration type

* **Description**           

buffer message MDL which is configured type


* **RTL Instant Name**    : `upen_rxmdl_typebuff`

* **Address**             : `0x24000 - 0x25FFF`

* **Formula**             : `0x24000 + $STSID* 256 + $SLICEID*32 + $RXDWORDID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$RXDWORDID (0-19) : RX Double WORD ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`mdl_tbyte3`| MS BYTE| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`mdl_tbyte2`| BYTE 2| `R/W`| `0x0`| `0x0`|
|`[15:8]`|`mdl_tbyte1`| BYTE 1| `R/W`| `0x0`| `0x0`|
|`[7:0]`|`mdl_tbyte0`| LS BYTE| `R/W`| `0x0`| `0x0 End: Begin:`|

###Config Buff Message MDL TYPE

* **Description**           

config type message MDL


* **RTL Instant Name**    : `upen_rxmdl_cfgtype`

* **Address**             : `0x23000 - 0x230BF`

* **Formula**             : `0x23000+ $STSID*8 + $SLICEID`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

* **Width**               : `10`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`cfg_fcs_rx`| config mode receive FCS, (1) is T.403-MSB, (0) is T.107-LSB| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`cfg_mdl_cr`| config C/R bit expected| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`cfg_mdl_std`| (0) is ANSI, (1) is AT&T| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`cfg_mdl_mask_test`| config enable mask to moitor test massage (1): enable, (0): disable| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`cfg_mdl_mask_path`| config enable mask to moitor path massage (1): enable, (0): disable| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`cfg_mdl_mask_idle`| config enable mask to moitor idle massage (1): enable, (0): disable| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG CONTROL DESTUFF 0

* **Description**           

config control DeStuff global


* **RTL Instant Name**    : `upen_destuff_ctrl0`

* **Address**             : `0x23200`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`cfg_crmon`| (0) : disable, (1) : enable| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`reserve1`| reserve| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`fcsmon`| (0) : disable monitor, (1) : enable| `R/W`| `0x1`| `0x1`|
|`[02:01]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`headermon_en`| (1) : enable monitor, (0) disable| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF CONFIG1

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg1`

* **Address**             : `0x23210`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 0-31| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 2

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg2`

* **Address**             : `0x23211`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 32 -63| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 3

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg3`

* **Address**             : `0x23212`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 64 -95| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 4

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg4`

* **Address**             : `0x23213`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 96 -127| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF  CONFIG 5

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg5`

* **Address**             : `0x23214`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 128 -159| `R/W`| `0x0`| `0x0 End: Begin:`|

###STICKY TO RECEIVE PACKET MDL BUFF CONFIG 6

* **Description**           

engine set to alarm message event, and CPU clear to be received new messase


* **RTL Instant Name**    : `upen_mdl_stk_cfg6`

* **Address**             : `0x23215`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mdl_cfg_alr`| sticky for new message channel 160 -191| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE RX MDL IDLE

* **Description**           

counter byte read to clear IDLE message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_byteidle`

* **Address**             : `0x26000 - 0x264BF`

* **Formula**             : `0x26000+ $STSID*8 + $SLICEID + $UPRO * 1024`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_byte_idle_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE RX MDL PATH

* **Description**           

counter byte read to clear PATH message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_bytepath`

* **Address**             : `0x26100 - 0x265BF`

* **Formula**             : `0x26100+ $STSID*8 + $SLICEID + $UPRO * 1024`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_byte_path_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER BYTE MESSAGE RX MDL TEST

* **Description**           

counter byte read to clear TEST message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_bytetest`

* **Address**             : `0x26200 - 0x266BF`

* **Formula**             : `0x26200+ $STSID*8 + $SLICEID + $UPRO * 1024`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_byte_test_mdl`| value counter byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER GOOD MESSAGE RX MDL IDLE

* **Description**           

counter good read to clear IDLE message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_goodidle`

* **Address**             : `0x27000 - 0x278BF`

* **Formula**             : `0x27000+ $STSID*8 + $SLICEID + $UPRO * 2048`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_good_idle_mdl`| value counter good| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER GOOD MESSAGE RX MDL PATH

* **Description**           

counter good read to clear PATH message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_goodpath`

* **Address**             : `0x27100 - 0x279BF`

* **Formula**             : `0x27100+ $STSID*8 + $SLICEID + $UPRO * 2048`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_good_path_mdl`| value counter good| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER GOOD MESSAGE RX MDL TEST

* **Description**           

counter good read to clear TEST message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_goodtest`

* **Address**             : `0x27200 - 0x27ABF`

* **Formula**             : `0x27200+ $STSID*8 + $SLICEID + $UPRO * 2048`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_good_test_mdl`| value counter good| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER DROP MESSAGE RX MDL IDLE

* **Description**           

counter drop read to clear IDLE message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_dropidle`

* **Address**             : `0x27300 - 0x27BBF`

* **Formula**             : `0x27300+ $STSID*8 + $SLICEID + $UPRO * 2048`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_drop_idle_mdl`| value counter drop| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER DROP MESSAGE RX MDL PATH

* **Description**           

counter drop read to clear PATH message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_droppath`

* **Address**             : `0x27400 - 0x27CBF`

* **Formula**             : `0x27400+ $STSID*8 + $SLICEID + $UPRO * 2048`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_drop_path_mdl`| value counter drop| `R/W`| `0x0`| `0x0 End: Begin:`|

###COUNTER DROP MESSAGE RX MDL TEST R2C

* **Description**           

counter drop read to clear TEST message MDL


* **RTL Instant Name**    : `upen_rxmdl_cntr2c_droptest`

* **Address**             : `0x27500 - 0x27DBF`

* **Formula**             : `0x27500+ $STSID*8 + $SLICEID + $UPRO * 2048`

* **Where**               : 

    * `$STSID (0-23) : STS ID`

    * `$SLICEID (0-7) : SLICE ID`

    * `$UPRO (0-1) : UP READ ONLY`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cntr2c_drop_test_mdl`| value counter drop| `R/W`| `0x0`| `0x0 End: Begin:`|

###MDL per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable


* **RTL Instant Name**    : `mdl_cfgen_int`

* **Address**             : `0x29000-0x290FF`

* **Formula**             : `0x29000 +  $STSID + $SLICEID * 32`

* **Where**               : 

    * `$STSID (0-23) : DE3 ID`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mdl_cfgen_int`| Set 1 to enable change event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt per Channel Interrupt Status

* **Description**           

This is the per Channel interrupt status.


* **RTL Instant Name**    : `mdl_int_sta`

* **Address**             : `0x29100-0x291FF`

* **Formula**             : `0x29100 +  $STSID + $SLICEID * 32`

* **Where**               : 

    * `$STSID (0-23) : DE3 ID`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mdlint_sta`| Set 1 if there is a change event.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt per Channel Current Status

* **Description**           

This is the per Channel Current status.


* **RTL Instant Name**    : `mdl_int_crrsta`

* **Address**             : `0x29200-0x292FF`

* **Formula**             : `0x29200 +  $STSID + $SLICEID * 32`

* **Where**               : 

    * `$STSID (0-23) : DE3 ID`

    * `$SLICEID(0-7) : SLICE ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`mdlint_crrsta`| Current status of event.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt per Channel Interrupt OR Status

* **Description**           

The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the Rx DS1/E1/J1 Framer. Each bit is used to store Interrupt OR tus of the related DS1/E1/J1.


* **RTL Instant Name**    : `mdl_intsta`

* **Address**             : `0x29300-0x29307`

* **Formula**             : `0x29300 +  $GID`

* **Where**               : 

    * `$GID (0-7) : group ID`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`mdlintsta`| Set to 1 if any interrupt status bit of corresponding channel is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt OR Status

* **Description**           

The register consists of 2 bits. Each bit is used to store Interrupt OR status.


* **RTL Instant Name**    : `mdl_sta_int`

* **Address**             : `0x293FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`mdlsta_int`| Set to 1 if any interrupt status bit is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###MDL Interrupt Enable Control

* **Description**           

The register consists of 2 interrupt enable bits .


* **RTL Instant Name**    : `mdl_en_int`

* **Address**             : `0x293FE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`mdlen_int`| Set to 1 to enable to generate interrupt.| `RW`| `0x0`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_PWE
####Register Table

|Name|Address|
|-----|-----|
|`Pseudowire Transmit Ethernet Header Mode Control`|`0x00000000 - 0x001FFF #The address format for these registers is 0x00000000 + PWID`|
|`Transmit Ethernet port Count0_64 bytes packet`|`0x0004000(RO)`|
|`Transmit Ethernet port Count0_64 bytes packet`|`0x0004800(RC)`|
|`Transmit Ethernet port Count65_127 bytes packet`|`0x0004002(RO)`|
|`Transmit Ethernet port Count65_127 bytes packet`|`0x0004802(RC)`|
|`Transmit Ethernet port Count128_255 bytes packet`|`0x0004004(RO)`|
|`Transmit Ethernet port Count128_255 bytes packet`|`0x0004804(RC)`|
|`Transmit Ethernet port Count256_511 bytes packet`|`0x0004006(RO)`|
|`Transmit Ethernet port Count256_511 bytes packet`|`0x0004806(RC)`|
|`Transmit Ethernet port Count512_1023 bytes packet`|`0x0004008(RO)`|
|`Transmit Ethernet port Count512_1023 bytes packet`|`0x0004808(RC)`|
|`Transmit Ethernet port Count1024_1518 bytes packet`|`0x000400A(RO)`|
|`Transmit Ethernet port Count1024_1518 bytes packet`|`0x000480A(RC)`|
|`Transmit Ethernet port Count1519_2047 bytes packet`|`0x000400C(RO)`|
|`Transmit Ethernet port Count1519_2047 bytes packet`|`0x000480C(RC)`|
|`Transmit Ethernet port Count Jumbo packet`|`0x000400E(RO)`|
|`Transmit Ethernet port Count Jumbo packet`|`0x000480E(RC)`|
|`Transmit Ethernet port Count Unicast packet`|`0x0004010(RO)`|
|`Transmit Ethernet port Count Unicast packet`|`0x0004810(RC)`|
|`Transmit Ethernet port Count Total packet`|`0x0004012(RO)`|
|`Transmit Ethernet port Count Total packet`|`0x0004812(RC)`|
|`Transmit Ethernet port Count Broadcast packet`|`0x0004014(RO)`|
|`Transmit Ethernet port Count Broadcast packet`|`0x0004814(RC)`|
|`Transmit Ethernet port Count Multicast packet`|`0x0004016(RO)`|
|`Transmit Ethernet port Count Multicast packet`|`0x0004816(RC)`|
|`Transmit Ethernet port Count number of bytes of packet`|`0x000401E(RO)`|
|`Transmit Ethernet port Count number of bytes of packet`|`0x000481E(RC)`|
|`Pseudowire Hold Register Status`|`0x00C00A - 0x00C00D`|
|`Pseudowire Parity Register Control`|`0x000F000`|
|`Pseudowire Parity Disable register Control`|`0x000F001`|
|`Pseudowire Parity sticky error`|`0x000F002`|


###Pseudowire Transmit Ethernet Header Mode Control

* **Description**           

This register is used to control read/write Pseudowire Transmit Ethernet Header Value from/to DDR


* **RTL Instant Name**    : `pw_txeth_hdr_mode`

* **Address**             : `0x00000000 - 0x001FFF #The address format for these registers is 0x00000000 + PWID`

* **Formula**             : `0x00000000 +  PWID`

* **Where**               : 

    * `$PWID(0-8191): Pseudowire ID`

* **Width**               : `9`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8:7]`|`txethpadmode`| this is configuration for insertion PAD in short packets 0: Insert PAD when total Ethernet packet length is less than 64 bytes. Refer to IEEE 802.3 1: Insert PAD when control word plus payload length is less than 64 bytes 2: Insert PAD when total Ethernet packet length minus VLANs and MPLS outer labels(if exist) is less than 64 bytes| `RW`| `0x0`| `0x0`|
|`[6]`|`txethpwrtpen`| this is RTP enable for TDM PW packet 1: Enable RTP 0: Disable RTP| `RW`| `0x0`| `0x0`|
|`[5:2]`|`txethpwpsntype`| this is Transmit PSN header mode working 1: PW PSN header is UDP/IPv4 2: PW PSN header is UDP/IPv6 3: PW MPLS no outer label over Ipv4 (total 1 MPLS label) 4: PW MPLS no outer label over Ipv6 (total 1 MPLS label) 5: PW MPLS one outer label over Ipv4 (total 2 MPLS label) 6: PW MPLS one outer label over Ipv6 (total 2 MPLS label) 7: PW MPLS two outer label over Ipv4 (total 3 MPLS label) 8: PW MPLS two outer label over Ipv6 (total 3 MPLS label) Others: for other PW PSN header type| `RW`| `0x0`| `0x0`|
|`[1:0]`|`txethpwnumvlan`| This is number of vlan in Transmit Ethernet packet 0: no vlan 1: 1 vlan 2: 2 vlan| `RW`| `0x0`| `0x0 End:   Begin:`|

###Transmit Ethernet port Count0_64 bytes packet

* **Description**           

This register is statistic counter for the packet having 0 to 64 bytes


* **RTL Instant Name**    : `Eth_cnt0_64_ro`

* **Address**             : `0x0004000(RO)`

* **Formula**             : `0x0004000 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt0_64`| This is statistic counter for the packet having 0 to 64 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count0_64 bytes packet

* **Description**           

This register is statistic counter for the packet having 0 to 64 bytes


* **RTL Instant Name**    : `Eth_cnt0_64_rc`

* **Address**             : `0x0004800(RC)`

* **Formula**             : `0x0004800 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt0_64`| This is statistic counter for the packet having 0 to 64 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count65_127 bytes packet

* **Description**           

This register is statistic counter for the packet having 65 to 127 bytes


* **RTL Instant Name**    : `Eth_cnt65_127_ro`

* **Address**             : `0x0004002(RO)`

* **Formula**             : `0x0004002 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt65_127`| This is statistic counter for the packet having 65 to 127 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count65_127 bytes packet

* **Description**           

This register is statistic counter for the packet having 65 to 127 bytes


* **RTL Instant Name**    : `Eth_cnt65_127_rc`

* **Address**             : `0x0004802(RC)`

* **Formula**             : `0x0004802 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt65_127`| This is statistic counter for the packet having 65 to 127 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count128_255 bytes packet

* **Description**           

This register is statistic counter for the packet having 128 to 255 bytes


* **RTL Instant Name**    : `Eth_cnt128_255_ro`

* **Address**             : `0x0004004(RO)`

* **Formula**             : `0x0004004 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt128_255`| This is statistic counter for the packet having 128 to 255 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count128_255 bytes packet

* **Description**           

This register is statistic counter for the packet having 128 to 255 bytes


* **RTL Instant Name**    : `Eth_cnt128_255_rc`

* **Address**             : `0x0004804(RC)`

* **Formula**             : `0x0004804 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt128_255`| This is statistic counter for the packet having 128 to 255 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count256_511 bytes packet

* **Description**           

This register is statistic counter for the packet having 256 to 511 bytes


* **RTL Instant Name**    : `Eth_cnt256_511_ro`

* **Address**             : `0x0004006(RO)`

* **Formula**             : `0x0004006 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt256_511`| This is statistic counter for the packet having 256 to 511 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count256_511 bytes packet

* **Description**           

This register is statistic counter for the packet having 256 to 511 bytes


* **RTL Instant Name**    : `Eth_cnt256_511_rc`

* **Address**             : `0x0004806(RC)`

* **Formula**             : `0x0004806 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt256_511`| This is statistic counter for the packet having 256 to 511 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count512_1023 bytes packet

* **Description**           

This register is statistic counter for the packet having 512 to 1023 bytes


* **RTL Instant Name**    : `Eth_cnt512_1024_ro`

* **Address**             : `0x0004008(RO)`

* **Formula**             : `0x0004008 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt512_1024`| This is statistic counter for the packet having 512 to 1023 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count512_1023 bytes packet

* **Description**           

This register is statistic counter for the packet having 512 to 1023 bytes


* **RTL Instant Name**    : `Eth_cnt512_1024_rc`

* **Address**             : `0x0004808(RC)`

* **Formula**             : `0x0004808 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt512_1024`| This is statistic counter for the packet having 512 to 1023 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count1024_1518 bytes packet

* **Description**           

This register is statistic counter for the packet having 1024 to 1518 bytes


* **RTL Instant Name**    : `Eth_cnt1025_1528_ro`

* **Address**             : `0x000400A(RO)`

* **Formula**             : `0x000400A + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt1025_1528`| This is statistic counter for the packet having 1024 to 1518 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count1024_1518 bytes packet

* **Description**           

This register is statistic counter for the packet having 1024 to 1518 bytes


* **RTL Instant Name**    : `Eth_cnt1025_1528_rc`

* **Address**             : `0x000480A(RC)`

* **Formula**             : `0x000480A + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt1025_1528`| This is statistic counter for the packet having 1024 to 1518 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count1519_2047 bytes packet

* **Description**           

This register is statistic counter for the packet having 1519 to 2047 bytes


* **RTL Instant Name**    : `Eth_cnt1529_2047_ro`

* **Address**             : `0x000400C(RO)`

* **Formula**             : `0x000400C + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt1529_2047`| This is statistic counter for the packet having 1519 to 2047 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count1519_2047 bytes packet

* **Description**           

This register is statistic counter for the packet having 1519 to 2047 bytes


* **RTL Instant Name**    : `Eth_cnt1529_2047_rc`

* **Address**             : `0x000480C(RC)`

* **Formula**             : `0x000480C + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnt1529_2047`| This is statistic counter for the packet having 1519 to 2047 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Jumbo packet

* **Description**           

This register is statistic counter for the packet having more than 2048 bytes (jumbo)


* **RTL Instant Name**    : `Eth_cnt_jumbo_ro`

* **Address**             : `0x000400E(RO)`

* **Formula**             : `0x000400E + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntjumbo`| This is statistic counter for the packet more than 2048 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Jumbo packet

* **Description**           

This register is statistic counter for the packet having more than 2048 bytes (jumbo)


* **RTL Instant Name**    : `Eth_cnt_jumbo_rc`

* **Address**             : `0x000480E(RC)`

* **Formula**             : `0x000480E + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntjumbo`| This is statistic counter for the packet more than 2048 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Unicast packet

* **Description**           

This register is statistic counter for the unicast packet


* **RTL Instant Name**    : `Eth_cnt_Unicast_ro`

* **Address**             : `0x0004010(RO)`

* **Formula**             : `0x0004010 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntunicast`| This is statistic counter for the unicast packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Unicast packet

* **Description**           

This register is statistic counter for the unicast packet


* **RTL Instant Name**    : `Eth_cnt_Unicast_rc`

* **Address**             : `0x0004810(RC)`

* **Formula**             : `0x0004810 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntunicast`| This is statistic counter for the unicast packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Total packet

* **Description**           

This register is statistic counter for the total packet at Transmit side


* **RTL Instant Name**    : `eth_tx_pkt_cnt_ro`

* **Address**             : `0x0004012(RO)`

* **Formula**             : `0x0004012 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnttotal`| This is statistic counter total packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Total packet

* **Description**           

This register is statistic counter for the total packet at Transmit side


* **RTL Instant Name**    : `eth_tx_pkt_cnt_rc`

* **Address**             : `0x0004812(RC)`

* **Formula**             : `0x0004812 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcnttotal`| This is statistic counter total packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Broadcast packet

* **Description**           

This register is statistic counter for the Broadcast packet at Transmit side


* **RTL Instant Name**    : `eth_tx_bcast_pkt_cnt_ro`

* **Address**             : `0x0004014(RO)`

* **Formula**             : `0x0004014 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntbroadcast`| This is statistic counter broadcast packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Broadcast packet

* **Description**           

This register is statistic counter for the Broadcast packet at Transmit side


* **RTL Instant Name**    : `eth_tx_bcast_pkt_cnt_rc`

* **Address**             : `0x0004814(RC)`

* **Formula**             : `0x0004814 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntbroadcast`| This is statistic counter broadcast packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Multicast packet

* **Description**           

This register is statistic counter for the Multicast packet at Transmit side


* **RTL Instant Name**    : `eth_tx_mcast_pkt_cnt_ro`

* **Address**             : `0x0004016(RO)`

* **Formula**             : `0x0004016 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntmulticast`| This is statistic counter multicast packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Multicast packet

* **Description**           

This register is statistic counter for the Multicast packet at Transmit side


* **RTL Instant Name**    : `eth_tx_mcast_pkt_cnt_rc`

* **Address**             : `0x0004816(RC)`

* **Formula**             : `0x0004816 + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntmulticast`| This is statistic counter multicast packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count number of bytes of packet

* **Description**           

This register is statistic count number of bytes of packet at Transmit side


* **RTL Instant Name**    : `Eth_cnt_byte_ro`

* **Address**             : `0x000401E(RO)`

* **Formula**             : `0x000401E + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntbyte`| This is statistic counter number of bytes of packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count number of bytes of packet

* **Description**           

This register is statistic count number of bytes of packet at Transmit side


* **RTL Instant Name**    : `Eth_cnt_byte_rc`

* **Address**             : `0x000481E(RC)`

* **Formula**             : `0x000481E + eth_port`

* **Where**               : 

    * `$eth_port(0-3): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`claethcntbyte`| This is statistic counter number of bytes of packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Hold Register Status

* **Description**           

This register using for hold remain that more than 128bits


* **RTL Instant Name**    : `pwe_hold_status`

* **Address**             : `0x00C00A - 0x00C00D`

* **Formula**             : `0x00C00A +  HID`

* **Where**               : 

    * `$HID(0-3): Hold ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pwholdstatus`| Hold 32bits| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Parity Register Control

* **Description**           

This register using for Force Parity


* **RTL Instant Name**    : `pwe_Parity_control`

* **Address**             : `0x000F000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`pwforceerr`| Force parity error enable for "Pseudowire Transmit Ethernet Header Mode Control"| `RW`| `0x0`| `0x0 1: enable force 0: not force End: Begin:`|

###Pseudowire Parity Disable register Control

* **Description**           

This register using for Disable Parity


* **RTL Instant Name**    : `pwe_Parity_Disable_control`

* **Address**             : `0x000F001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`pwdischkerr`| Disable parity error check for "Pseudowire Transmit Ethernet Header Mode Control"| `RW`| `0x0`| `0x0 End: Begin:`|

###Pseudowire Parity sticky error

* **Description**           

This register using for checking sticky error


* **RTL Instant Name**    : `pwe_Parity_stk_err`

* **Address**             : `0x000F002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`pwstkerr`| parity error for "Pseudowire Transmit Ethernet Header Mode Control"| `RW`| `0x0`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CNC0021_RD_UPSR
####Register Table

|Name|Address|
|-----|-----|
|`UPSR SF/SD Alarm Mask Registers`|`0x01000 - 0x0172f`|
|`UPSR SF/SD Hold_Off/Hold_On timer Registers`|`0x02000 - 0x02f2f`|
|`UPSR State Machine Engine Registers 1`|`0x03000 - 0x0317f`|
|`UPSR State Machine Engine Registers 2`|`0x03800 - 0x0397f`|
|`UPSR per Alarm Interrupt Enable Control`|`0x04000 - 0x041ff`|
|`UPSR per Alarm Interrupt Status`|`0x04200 - 0x043ff`|
|`UPSR per Alarm Current Status`|`0x04400 - 0x045ff`|
|`UPSR per Alarm per Selector Interrupt_OR Status`|`0x04600 - 0x0460f`|
|`UPSR per Alarm Selector Group Interrupt_OR Status`|`0x047ff - 0x047ff`|
|`UPSR per Alarm Selector Group Interrupt_OR Enable Control`|`0x047fe - 0x047fe`|


###UPSR SF/SD Alarm Mask Registers

* **Description**           

These configuration registers are used to assign these alarms {plm,tim,ber_sd,ber_sf,uneq,lop,ais} into SF/SD events for 8 lines OCn_Path.

If each alarm is not assigned to SD and SF, this means that this alarm is disabled.


* **RTL Instant Name**    : `upsralrmmasksfsdramctl`

* **Address**             : `0x01000 - 0x0172f`

* **Formula**             : `0x01000 + 64*LineId + StsId`

* **Where**               : 

    * `$LineId(0-7)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`upsrplmmasksf`| Assign PLM defect alarm to SF event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[13]`|`upsrtimmasksf`| Assign TIM defect alarm to SF event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[12]`|`upsrbersdmasksf`| Assign Ber_SD to SF event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[11]`|`upsrbersfmasksf`| Assign Ber_SF to SF event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x1`|
|`[10]`|`upsruneqmasksf`| Assign UNEQ defect to SF event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x1`|
|`[09]`|`upsrlopmasksf`| Assign LOP defect to SF event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x1`|
|`[08]`|`upsraismasksf`| Assign AIS defect to SF event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x1`|
|`[07]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[06]`|`upsrplmmasksd`| Assign PLM defect alarm to SD event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[05]`|`upsrtimmasksd`| Assign TIM defect alarm to SD event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[04]`|`upsrbersdmasksd`| Assign Ber_SD to SD event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x1`|
|`[03]`|`upsrbersfmasksd`| Assign Ber_SF to SD event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[02]`|`upsruneqmasksd`| Assign UNEQ defect to SD event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[01]`|`upsrlopmasksd`| Assign LOP defect to SD event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[00]`|`upsraismasksd`| Assign AIS defect to SD event, set 1 to choose. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0 End : Begin:`|

###UPSR SF/SD Hold_Off/Hold_On timer Registers

* **Description**           

These configuration registers are used to configure hold_off/hold_On for 8 lines OCn_Path (LineId:0-7) and 8 lines MATE (LineId:8-15).

Hold_off Timer 			= UpsrHoldOffTmUnit * UpsrHoldOffTmMax

Hold_on  Timer 			= UpsrHoldOnnTmUnit * UpsrHoldOnnTmMax


* **RTL Instant Name**    : `upsrholdwtrtimerramctl`

* **Address**             : `0x02000 - 0x02f2f`

* **Formula**             : `0x02000 + 64*LineId + StsId`

* **Where**               : 

    * `$LineId(0-15)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22:20]`|`upsrholdonntmunit`| Hold_on Timer Unit. (Scale down 5 times for simulation) 7,6: Unused. 5: Timer Unit is 100 ms. 4: Timer Unit is 50  ms. 3: Timer Unit is 10  ms. 2: Timer Unit is 5   ms. 1: Timer Unit is 1 	 ms. 0: Timer Unit is 0.5 ms.| `RW`| `0x0`| `0x0`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18:16]`|`upsrholdofftmunit`| Hold_off Timer Unit. (Scale down 5 times for simulation) 7,6: Unused. 5: Timer Unit is 100 ms. 4: Timer Unit is 50  ms. 3: Timer Unit is 10  ms. 2: Timer Unit is 5   ms. 1: Timer Unit is 1 	 ms. 0: Timer Unit is 0.5 ms.| `RW`| `0x0`| `0x0`|
|`[15:08]`|`upsrholdonntmmax`| Hold_on Unit_Timer Maximum Value.| `RW`| `0x0`| `0xC8`|
|`[07:00]`|`upsrholdofftmmax`| Hold_off Unit_Timer Maximum Value.| `RW`| `0x0`| `0xC8 End : Begin:`|

###UPSR State Machine Engine Registers 1

* **Description**           

These configuration registers are used to configure for UPSR state machine engine, They are configured per Path Selector

Wait_to_Restore Timer 	= UpsrWtrTmUnit 	* UpsrWtrTmMax


* **RTL Instant Name**    : `upsrsmramctl1`

* **Address**             : `0x03000 - 0x0317f`

* **Formula**             : `0x03000 + SelectorId`

* **Where**               : 

    * `$SelectorId(0-383)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29]`|`upsrsmwtrtmunit`| Wait_to_Restore Timer Unit.(Scale down (10ms/5, or 5ms/5 for simulation) 1: Timer Unit is 1 minute. 0: Timer Unit is 0.5 minute.| `RW`| `0x0`| `0x0`|
|`[28:22]`|`upsrsmwtrtmmax`| Wait_to_Restore Unit_Timer Maximum Value.| `RW`| `0x0`| `0x78`|
|`[21]`|`upsrsmenable`| UPSR enable per selector. When any selector is disable, all off status is reset to Reset value (0x0). and no interrupt is sent. 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[20]`|`upsrsmrevertmode`| Configure Path Selector is revertive or nonrevertive mode. Default is nonrevertive mode 1: Revertive Mode. 0: Nonrevertive Mode.| `RW`| `0x0`| `0x0`|
|`[19:10]`|`upsrsmwokpathid`| Working Path Sonet_ID<br>{SliceId[3:0], StsId[5:0]}.| `RW`| `0x0`| `0x0`|
|`[09:00]`|`upsrsmpropathid`| Protection Path Sonet_ID<br>{SliceId[3:0], StsId[5:0]}.| `RW`| `0x0`| `0x0 End : Begin:`|

###UPSR State Machine Engine Registers 2

* **Description**           

These configuration registers are used to configure for UPSR state machine engine, They are configured per Path Selector

Wait_to_Restore Timer 	= UpsrWtrTmUnit 	* UpsrWtrTmMax


* **RTL Instant Name**    : `upsrsmramctl2`

* **Address**             : `0x03800 - 0x0397f`

* **Formula**             : `0x03800 + SelectorId`

* **Where**               : 

    * `$SelectorId(0-383)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[02:00]`|`upsrsmmanualcmd`| Manually Protection Switching Commands. 7,6: Unused. 5: Lockout of Protection. 4: Forced Switch to Working. 3: Forced Switch to Protection. 2: Manual Switch to Working. 1: Manual Switch to Protection. 0: Clear.| `RW`| `0x0`| `0x0 End : Begin:`|

###UPSR per Alarm Interrupt Enable Control

* **Description**           

These are the per Alarm interrupt enable. Each register is used to store bits to enable interrupts when the related alarms/events in UPSR engine happen.


* **RTL Instant Name**    : `upsrsmintenb`

* **Address**             : `0x04000 - 0x041ff`

* **Formula**             : `0x04000 + SelectorId`

* **Where**               : 

    * `$SelectorId(0-383)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[12:00]`|`upsrsmintenb`| Set 1 to enable alarm/event in UPSR engine to generate an interrupt when having any local state or active path is changed. [12] : SDSF to NRQ Condition: Current state changed from SDSF to NRQ. [11] : WTR to NRQ Condition:  Current state changed from WTR to NRQ. [10] : WTR Condition: Current state changed from NRQ to WTR OR from SDSF to WTR. [9]  : Manual Switch Command. [8]  : SD Condition. [7]  : SF Condition. [6]  : Forced Switch Command. [5]  : Lockout of Protection Command. [4]  : Clear command when current state at External Command Switching. [3:1]: Unused. [0]  : Active path change from working to protection or protection to working.| `RW`| `0x0`| `0x0 End : Begin:`|

###UPSR per Alarm Interrupt Status

* **Description**           

These are the per Alarm interrupt status. Each register is used to store sticky bits for each alarms/events in UPSR engine.


* **RTL Instant Name**    : `upsrsmintint`

* **Address**             : `0x04200 - 0x043ff`

* **Formula**             : `0x04200 + SelectorId`

* **Where**               : 

    * `$SelectorId(0-383)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[12:00]`|`upsrsmintint`| Set 1 while alarm/event detected in the related UPSR selector, and it is generated an interrupt if it is enabled. [12] : SDSF to NRQ Condition:Current state changed from SDSF to NRQ. [11] : WTR to NRQ Condition:Current state changed from WTR to NRQ. [10] : WTR Condition: Current state changed from NRQ to WTR OR from SDSF to WTR. [9]  : Manual Switch Command. [8]  : SD Condition. [7]  : SF Condition. [6]  : Forced Switch Command. [5]  : Lockout of Protection Command. [4]  : Clear command when current state at External Command Switching. [3:1]: Unused. [0]  : Active path change from working to protection or protection to working.| `W1C`| `0x0`| `0x0 End : Begin:`|

###UPSR per Alarm Current Status

* **Description**           

These are the per Alarm Current status. Each register include Local state and actived path indication.


* **RTL Instant Name**    : `upsrsmcursta`

* **Address**             : `0x04400 - 0x045ff`

* **Formula**             : `0x04400 + SelectorId`

* **Where**               : 

    * `$SelectorId(0-383)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[03:01]`|`upsrsmstacurstate`| This field indicate Local State of Path Seletor. 7: Unused. 6: Lockout of Protection. 5: Forced Switch. 4: SF. 3: SD. 2: Manual Switch. 1: Wait_to_Restore. 0: No Request.| `RO`| `0x0`| `0x0`|
|`[00]`|`upsrsmstaactpath`| This field indicate Active Path is Working Path or Protection Path. 1: Protection Path. 0: Working Path.| `RO`| `0x0`| `0x0 End : Begin:`|

###UPSR per Alarm per Selector Interrupt_OR Status

* **Description**           

There are 12 groups(0-11) of selector, These group consists of 32 bits for 32 selectors. Group #0 for SelectorId from 0 to 31, respectively. Each bit is used to store Interrupt_OR status of the related Selector.


* **RTL Instant Name**    : `upsrsmselorint`

* **Address**             : `0x04600 - 0x0460f`

* **Formula**             : `0x04600 + GroupSelectorId`

* **Where**               : 

    * `$GroupSelectorId(0-11)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`upsrsmstaintselor`| Set to 1 to indicate that there is any interrupt status bit in the "UPSR per Alarm Interrupt Status" registers of the related Selector to be //										set and they are enabled to raise interrupt. Bit 0 for Selector #0, respectively.| `RO`| `0x0`| `0x0 End : Begin:`|

###UPSR per Alarm Selector Group Interrupt_OR Status

* **Description**           

The register consists of 12 bits for 12 Selector Groups. Each bit is used to store Interrupt_OR status of the related Selector Group.


* **RTL Instant Name**    : `upsrsmgrporint`

* **Address**             : `0x047ff - 0x047ff`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[11:00]`|`upsrsmstaintgrpor`| Set to 1 to indicate that there is any interrupt status bit in the "UPSR per Alarm per Selector Interrupt_OR Status" register of the related //										Selector Group to be set and they are enabled to raise interrupt Bit 0 for Selector Group #0, respectively.| `RO`| `0x0`| `0x0 End : Begin:`|

###UPSR per Alarm Selector Group Interrupt_OR Enable Control

* **Description**           

The register consists of 12 bits for 12 Selector Groups to enable interrupts when alarms in related Selector Group happen.


* **RTL Instant Name**    : `upsrsmgrporintenb`

* **Address**             : `0x047fe - 0x047fe`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:00]`|`upsrsmstaintgrporen`| Set to 1 to enable the related Selector Group to generate interrupt. Bit 0 for Selector Group #0, respectively.| `RW`| `0x0`| `0x0 End :`|

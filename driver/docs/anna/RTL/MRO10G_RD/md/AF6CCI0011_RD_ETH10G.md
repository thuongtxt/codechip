## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_ETH10G
####Register Table

|Name|Address|
|-----|-----|
|`ETH 10G Version Control`|`0x00`|
|`ETH 10G Flush Control`|`0x01`|
|`ETH 10G Protection Control`|`0x02`|
|`ETH 10G Link Fault Interrupt Enable`|`0x1003`|
|`ETH 10G Link Fault Sticky`|`0x100B`|
|`ETH 10G Link Fault Current Status`|`0x100D`|
|`ETH 10G Loopback Control`|`0x40`|
|`ETH 10G MAC Reveive Control`|`0x42`|
|`ETH 10G MAC Transmit Control`|`0x43`|
|`Transmit Ethernet port Count0_64 bytes packet`|`0x0003000(RO)`|
|`Transmit Ethernet port Count0_64 bytes packet`|`0x0003800(RC)`|
|`Transmit Ethernet port Count65_127 bytes packet`|`0x0003002(RO)`|
|`Transmit Ethernet port Count65_127 bytes packet`|`0x0003802(RC)`|
|`Transmit Ethernet port Count128_255 bytes packet`|`0x0003004(RO)`|
|`Transmit Ethernet port Count128_255 bytes packet`|`0x0003804(RC)`|
|`Transmit Ethernet port Count256_511 bytes packet`|`0x0003006(RO)`|
|`Transmit Ethernet port Count256_511 bytes packet`|`0x0003806(RC)`|
|`Transmit Ethernet port Count512_1023 bytes packet`|`0x0003008(RO)`|
|`Transmit Ethernet port Count512_1023 bytes packet`|`0x0003808(RC)`|
|`Transmit Ethernet port Count1024_1518 bytes packet`|`0x000300A(RO)`|
|`Transmit Ethernet port Count1024_1518 bytes packet`|`0x000380A(RC)`|
|`Transmit Ethernet port Count1519_2047 bytes packet`|`0x000300C(RO)`|
|`Transmit Ethernet port Count1519_2047 bytes packet`|`0x000380C(RC)`|
|`Transmit Ethernet port Count Jumbo packet`|`0x000300E(RO)`|
|`Transmit Ethernet port Count Jumbo packet`|`0x000380E(RC)`|
|`Transmit Ethernet port Count Total packet`|`0x0003012(RO)`|
|`Transmit Ethernet port Count Total packet`|`0x0003812(RC)`|
|`Transmit Ethernet port Count number of bytes of packet`|`0x000301E(RO)`|
|`Transmit Ethernet port Count number of bytes of packet`|`0x000381E(RC)`|


###ETH 10G Version Control

* **Description**           

This register checks version ID


* **RTL Instant Name**    : `eth10g_ver_ctr`

* **Address**             : `0x00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:8]`|`eth10gvendor`| Arrive ETH 10G MAC| `RO`| `0xAF`| `0xAF`|
|`[7:0]`|`eth10gver`| Version ID| `RO`| `0x0`| `0x0 End: Begin:`|

###ETH 10G Flush Control

* **Description**           

This register is used to flush engine


* **RTL Instant Name**    : `eth10g_flush_ctr`

* **Address**             : `0x01`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `3`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2]`|`eth10gflushctrl`| Flush engine| `RW`| `0x0`| `0x0`|
|`[1:0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End: Begin:`|

###ETH 10G Protection Control

* **Description**           

This register is used to flush engine


* **RTL Instant Name**    : `eth10g_protect_ctr`

* **Address**             : `0x02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `13`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12]`|`eth10grxportctrl`| Active/Standby port| `RW`| `0x0`| `0x0`|
|`[11:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`eth10grxdicctrl`| enable DIC(deficit idle count) funtion| `RW`| `0x0`| `0x0`|
|`[8:0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End: Begin:`|

###ETH 10G Link Fault Interrupt Enable

* **Description**           

This register is used to control link fault function


* **RTL Instant Name**    : `eth10g_link_fault_int_enb`

* **Address**             : `0x1003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:26]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[25]`|`eth10gforcetxrf`| Force TX Remote fault, TX will transmit remote fault code to remote side<br>{0} : normal operation <br>{1} : force remote fault| `RW`| `0x0`| `0x0`|
|`[24]`|`eth10gforcetxlf`| Force TX Local fault, TX will transmit local fault code to remote side,<br>{0} : normal operation <br>{1} : force local fault| `RW`| `0x0`| `0x0`|
|`[23]`|`eth10gforcerxrf`| Force RX Remote fault, RX will rasie RF interrupt, TX will transmit idle code to remote side<br>{0} : normal operation <br>{1} : force remote fault| `RW`| `0x0`| `0x0`|
|`[22]`|`eth10gforcerxlf`| Force RX Local fault, RX will raise LF interrupt, TX will transmit remote fault code to remote side,<br>{0} : normal operation <br>{1} : force local fault| `RW`| `0x0`| `0x0`|
|`[21]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[20]`|`eth10gdislfault`| Disable Link Fault function<br>{0} : enable link fault <br>{1} : disable link fault| `RW`| `0x0`| `0x0`|
|`[19:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`eth10gswitchayns`| switch data and code words of link fault is asynchronous<br>{0} : synchronous <br>{1} : asynchronous| `RW`| `0x1`| `0x1`|
|`[15:7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`eth10gdataorframesyncenb`| interrupt enable of Loss of Data Sync<br>{0} : disable <br>{1} : enable| `RW`| `0x0`| `0x0`|
|`[5]`|`eth10gclocklossenb`| interrupt enable of Loss of Clock<br>{0} : disable <br>{1} : enable| `RW`| `0x0`| `0x0`|
|`[4]`|`eth10gclockoutrangeenb`| interrupt enable of Frequency Out of Range<br>{0} : disable <br>{1} : enable| `RW`| `0x0`| `0x0`|
|`[3]`|`eth10gexerrorratioenb`| interrupt enable of Excessive Error Ratio<br>{0} : disable <br>{1} : enable| `RW`| `0x0`| `0x0`|
|`[2]`|`eth10glocalfaultenb`| interrupt enable of local fault<br>{0} : disable <br>{1} : enable| `RW`| `0x0`| `0x0`|
|`[1]`|`eth10gremotefaultenb`| interrupt enable of remote fault<br>{0} : disable <br>{1} : enable| `RW`| `0x0`| `0x0`|
|`[0]`|`eth10ginterrptionenb`| interrupt enable of interruption<br>{0} : disable <br>{1} : enable| `RW`| `0x0`| `0x0 End: Begin:`|

###ETH 10G Link Fault Sticky

* **Description**           

This register is used to report sticky of link fault


* **RTL Instant Name**    : `eth10g_link_fault_sticky`

* **Address**             : `0x100B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`eth10gdataorframesyncstk`| sticky state change of Loss of Data Syncsticky state change| `W1C`| `0x0`| `0x0`|
|`[5]`|`eth10gclocklossstk`| sticky state change of Loss of Clocksticky state change| `W1C`| `0x0`| `0x0`|
|`[4]`|`eth10gclockoutrangestk`| sticky state change of Frequency Out of Rangesticky state change| `W1C`| `0x0`| `0x0`|
|`[3]`|`eth10gexerrorratiostk`| sticky state change of Excessive Error Ratiosticky state change| `W1C`| `0x0`| `0x0`|
|`[2]`|`eth10glocalfaultstk`| sticky state change of local fault sticky state change| `W1C`| `0x0`| `0x0`|
|`[1]`|`eth10gremotefaultstk`| sticky state change of remote fault| `W1C`| `0x0`| `0x0`|
|`[0]`|`eth10ginterrptionstk`| sticky state change of interruption| `W1C`| `0x0`| `0x0 End: Begin:`|

###ETH 10G Link Fault Current Status

* **Description**           

This register is used to report current status of link fault


* **RTL Instant Name**    : `eth10g_link_fault_Cur_Sta`

* **Address**             : `0x100D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`eth10gdataorframesynccur`| sticky state change of Loss of Data Syncsticky state change| `RO`| `0x0`| `0x0`|
|`[5]`|`eth10gclocklosscur`| sticky state change of Loss of Clocksticky state change| `RO`| `0x0`| `0x0`|
|`[4]`|`eth10gclockoutrangecur`| sticky state change of Frequency Out of Rangesticky state change| `RO`| `0x0`| `0x0`|
|`[3]`|`eth10gexerrorratiocur`| sticky state change of Excessive Error Ratiosticky state change| `RO`| `0x0`| `0x0`|
|`[2]`|`eth10glocalfaultcur`| current status of local fault| `RO`| `0x0`| `0x0`|
|`[1]`|`eth10gremotefaultcur`| current status of remote fault| `RO`| `0x0`| `0x0`|
|`[0]`|`eth10ginterrptioncur`| current status of interruption| `RO`| `0x0`| `0x0 End: Begin:`|

###ETH 10G Loopback Control

* **Description**           

This register is used to configure loopback funtion


* **RTL Instant Name**    : `eth10g_loop_ctr`

* **Address**             : `0x40`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `7`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`eth10gstandportloopoutctrl`| Standby port loop-out (XGMII rx to XGMII tx)| `RW`| `0x0`| `0x0`|
|`[5]`|`eth10gactportloopoutctrl`| Active  port loop-out (XGMII rx to XGMII tx)| `RW`| `0x0`| `0x0`|
|`[4:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`eth10gstandportloopinctrl`| Standby port loop-in (XGMII tx to XGMII rx)| `RW`| `0x0`| `0x0`|
|`[1]`|`eth10gactportloopinctrl`| Active  port loop-in (XGMII tx to XGMII rx)| `RW`| `0x0`| `0x0`|
|`[0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End: Begin:`|

###ETH 10G MAC Reveive Control

* **Description**           

This register is used to configure MAC at the reveiver direction


* **RTL Instant Name**    : `eth10g_mac_rx_ctr`

* **Address**             : `0x42`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `12`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11:8]`|`eth10gmacrxipgctrl`| RX inter packet gap| `RW`| `0x4`| `0x4`|
|`[7:1]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`eth10gmacrxfcsbypassctrl`| Bypass the received FCS 4-byte| `RW`| `0x1`| `0x1 End: Begin:`|

###ETH 10G MAC Transmit Control

* **Description**           

This register is used to configure MAC at the reveiver direction


* **RTL Instant Name**    : `eth10g_mac_tx_ctr`

* **Address**             : `0x43`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `13`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12:8]`|`eth10gmactxipgctrl`| TX inter packet gap| `RW`| `0x8`| `0x8`|
|`[7:1]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`eth10gmactxfcsinsctrl`| Insert FCS 4-byte| `RW`| `0x1`| `0x1 End: Begin:`|

###Transmit Ethernet port Count0_64 bytes packet

* **Description**           

This register is statistic counter for the packet having 0 to 64 bytes


* **RTL Instant Name**    : `TxEth_cnt0_64_ro`

* **Address**             : `0x0003000(RO)`

* **Formula**             : `0x0003000 + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt0_64`| This is statistic counter for the packet having 0 to 64 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count0_64 bytes packet

* **Description**           

This register is statistic counter for the packet having 0 to 64 bytes


* **RTL Instant Name**    : `TxEth_cnt0_64_rc`

* **Address**             : `0x0003800(RC)`

* **Formula**             : `0x0003800 + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt0_64`| This is statistic counter for the packet having 0 to 64 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count65_127 bytes packet

* **Description**           

This register is statistic counter for the packet having 65 to 127 bytes


* **RTL Instant Name**    : `TxEth_cnt65_127_ro`

* **Address**             : `0x0003002(RO)`

* **Formula**             : `0x0003002 + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt65_127`| This is statistic counter for the packet having 65 to 127 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count65_127 bytes packet

* **Description**           

This register is statistic counter for the packet having 65 to 127 bytes


* **RTL Instant Name**    : `TxEth_cnt65_127_rc`

* **Address**             : `0x0003802(RC)`

* **Formula**             : `0x0003802 + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt65_127`| This is statistic counter for the packet having 65 to 127 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count128_255 bytes packet

* **Description**           

This register is statistic counter for the packet having 128 to 255 bytes


* **RTL Instant Name**    : `TxEth_cnt128_255_ro`

* **Address**             : `0x0003004(RO)`

* **Formula**             : `0x0003004 + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt128_255`| This is statistic counter for the packet having 128 to 255 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count128_255 bytes packet

* **Description**           

This register is statistic counter for the packet having 128 to 255 bytes


* **RTL Instant Name**    : `TxEth_cnt128_255_rc`

* **Address**             : `0x0003804(RC)`

* **Formula**             : `0x0003804 + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt128_255`| This is statistic counter for the packet having 128 to 255 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count256_511 bytes packet

* **Description**           

This register is statistic counter for the packet having 256 to 511 bytes


* **RTL Instant Name**    : `TxEth_cnt256_511_ro`

* **Address**             : `0x0003006(RO)`

* **Formula**             : `0x0003006 + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt256_511`| This is statistic counter for the packet having 256 to 511 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count256_511 bytes packet

* **Description**           

This register is statistic counter for the packet having 256 to 511 bytes


* **RTL Instant Name**    : `TxEth_cnt256_511_rc`

* **Address**             : `0x0003806(RC)`

* **Formula**             : `0x0003806 + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt256_511`| This is statistic counter for the packet having 256 to 511 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count512_1023 bytes packet

* **Description**           

This register is statistic counter for the packet having 512 to 1023 bytes


* **RTL Instant Name**    : `TxEth_cnt512_1024_ro`

* **Address**             : `0x0003008(RO)`

* **Formula**             : `0x0003008 + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt512_1024`| This is statistic counter for the packet having 512 to 1023 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count512_1023 bytes packet

* **Description**           

This register is statistic counter for the packet having 512 to 1023 bytes


* **RTL Instant Name**    : `TxEth_cnt512_1024_rc`

* **Address**             : `0x0003808(RC)`

* **Formula**             : `0x0003808 + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt512_1024`| This is statistic counter for the packet having 512 to 1023 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count1024_1518 bytes packet

* **Description**           

This register is statistic counter for the packet having 1024 to 1518 bytes


* **RTL Instant Name**    : `Eth_cnt1025_1528_ro`

* **Address**             : `0x000300A(RO)`

* **Formula**             : `0x000300A + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt1025_1528`| This is statistic counter for the packet having 1024 to 1518 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count1024_1518 bytes packet

* **Description**           

This register is statistic counter for the packet having 1024 to 1518 bytes


* **RTL Instant Name**    : `Eth_cnt1025_1528_rc`

* **Address**             : `0x000380A(RC)`

* **Formula**             : `0x000380A + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt1025_1528`| This is statistic counter for the packet having 1024 to 1518 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count1519_2047 bytes packet

* **Description**           

This register is statistic counter for the packet having 1519 to 2047 bytes


* **RTL Instant Name**    : `TxEth_cnt1529_2047_ro`

* **Address**             : `0x000300C(RO)`

* **Formula**             : `0x000300C + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt1529_2047`| This is statistic counter for the packet having 1519 to 2047 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count1519_2047 bytes packet

* **Description**           

This register is statistic counter for the packet having 1519 to 2047 bytes


* **RTL Instant Name**    : `TxEth_cnt1529_2047_rc`

* **Address**             : `0x000380C(RC)`

* **Formula**             : `0x000380C + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnt1529_2047`| This is statistic counter for the packet having 1519 to 2047 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Jumbo packet

* **Description**           

This register is statistic counter for the packet having more than 2048 bytes (jumbo)


* **RTL Instant Name**    : `TxEth_cnt_jumbo_ro`

* **Address**             : `0x000300E(RO)`

* **Formula**             : `0x000300E + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcntjumbo`| This is statistic counter for the packet more than 2048 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Jumbo packet

* **Description**           

This register is statistic counter for the packet having more than 2048 bytes (jumbo)


* **RTL Instant Name**    : `TxEth_cnt_jumbo_rc`

* **Address**             : `0x000380E(RC)`

* **Formula**             : `0x000380E + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcntjumbo`| This is statistic counter for the packet more than 2048 bytes| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Total packet

* **Description**           

This register is statistic counter for the total packet at Transmit side


* **RTL Instant Name**    : `eth_tx_pkt_cnt_ro`

* **Address**             : `0x0003012(RO)`

* **Formula**             : `0x0003012 + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnttotal`| This is statistic counter total packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count Total packet

* **Description**           

This register is statistic counter for the total packet at Transmit side


* **RTL Instant Name**    : `eth_tx_pkt_cnt_rc`

* **Address**             : `0x0003812(RC)`

* **Formula**             : `0x0003812 + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcnttotal`| This is statistic counter total packet| `RO`| `0x0`| `0x0 End: Begin:`|

###Transmit Ethernet port Count number of bytes of packet

* **Description**           

This register is statistic count number of bytes of packet at Transmit side


* **RTL Instant Name**    : `TxEth_cnt_byte_ro`

* **Address**             : `0x000301E(RO)`

* **Formula**             : `0x000301E + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcntbyte`| This is statistic counter number of bytes of packet| `RO`| `0x0`| `0x0 End:`|

###Transmit Ethernet port Count number of bytes of packet

* **Description**           

This register is statistic count number of bytes of packet at Transmit side


* **RTL Instant Name**    : `TxEth_cnt_byte_rc`

* **Address**             : `0x000381E(RC)`

* **Formula**             : `0x000381E + eth_port`

* **Where**               : 

    * `$eth_port(0-0): Transmit Ethernet port ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txethcntbyte`| This is statistic counter number of bytes of packet| `RO`| `0x0`| `0x0 End:`|

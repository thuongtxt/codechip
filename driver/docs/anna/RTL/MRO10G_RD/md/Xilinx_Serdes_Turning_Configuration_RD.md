## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2016-09-27|Project|Initial version|




##Xilinx_Serdes_Turning_Configuration_RD
####Register Table

|Name|Address|
|-----|-----|
|`SERDES DRP PORT`|`0x1000-0x7FFF`|
|`SERDES LoopBack`|`0x0002-0x6002`|
|`SERDES POWER DOWN`|`0x0003-0x6003`|
|`SERDES PLL Status`|`0x000B-0x600B`|
|`SERDES TX Reset`|`0x000C-0x600C`|
|`SERDES RX Reset`|`0x000D-0x600D`|
|`SERDES LPMDFE Mode`|`0x000E-0x600E`|
|`SERDES LPMDFE Reset`|`0x000F-0x600F`|
|`SERDES TXDIFFCTRL`|`0x0010-0x6010`|
|`SERDES TXPOSTCURSOR`|`0x0011-0x6011`|
|`SERDES TXPRECURSOR`|`0x0011-0x6011`|


###SERDES DRP PORT

* **Description**           

Read/Write DRP address of SERDES, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_DRP_PORT`

* **Address**             : `0x1000-0x7FFF`

* **Formula**             : `0x1000+$G*0x2000+$P*0x400+$DRP`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

    * `$P(0-1) : Sub Port ID`

    * `$DRP(0-1023) : DRP address, see UG578`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:00]`|`drp_rw`| DRP read/write value| `R/W`| `0x0`| `0x0`|

###SERDES LoopBack

* **Description**           

Configurate LoopBack, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_LoopBack`

* **Address**             : `0x0002-0x6002`

* **Formula**             : `0x0002+$G*0x2000+0x2`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`lpback_subport3`| Loopback subport 3<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[11:08]`|`lpback_subport2`| Loopback subport 2<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[07:04]`|`lpback_subport1`| Loopback subport 1<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`lpback_subport0`| Loopback subport 0<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|

###SERDES POWER DOWN

* **Description**           

Configurate Power Down , there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_POWER DOWN`

* **Address**             : `0x0003-0x6003`

* **Formula**             : `0x0003+$G*0x2000+0x3`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:06]`|`pdown_subport3`| Power Down subport 3<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x0`| `0x0`|
|`[05:04]`|`pdown_subport2`| Power Down subport 2<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x0`| `0x0`|
|`[03:02]`|`pdown_subport1`| Power Down subport 1<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x0`| `0x0`|
|`[01:00]`|`pdown_subport0`| Power Down subport 0<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x0`| `0x0`|

###SERDES PLL Status

* **Description**           

QPLL/CPLL status, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_PLL_Status`

* **Address**             : `0x000B-0x600B`

* **Formula**             : `0x000B+$G*0x2000+0xB`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:29]`|`qpll1_lock_change`| QPLL1 has transition lock/unlock, Group 0-1<br>{1} : QPLL1_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[28:28]`|`qpll0_lock_change`| QPLL0 has transition lock/unlock, Group 0-1<br>{1} : QPLL0_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[27:26]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[25:25]`|`qpll1_lock`| QPLL0 is Locked, Group 0-1<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[24:24]`|`qpll0_lock`| QPLL0 is Locked, Group 0-1<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[23:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`cpll_lock_change`| CPLL has transition lock/unlock, bit per sub port,<br>{1} : CPLL_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[11:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`cpll_lock`| CPLL is Locked, bit per sub port,<br>{1} : Locked| `R_O`| `0x0`| `0x0`|

###SERDES TX Reset

* **Description**           

Reset TX SERDES, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_TX_Reset`

* **Address**             : `0x000C-0x600C`

* **Formula**             : `0x000C+$G*0x2000+0xC`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:16]`|`txrst_done`| TX Reset Done, bit per sub port<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`txrst_trig`| Trige 0->1 to start reset TX SERDES, bit per sub port| `R/W`| `0x0`| `0x0`|

###SERDES RX Reset

* **Description**           

Reset RX SERDES, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_RX_Reset`

* **Address**             : `0x000D-0x600D`

* **Formula**             : `0x000D+$G*0x2000+0xD`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:16]`|`rxrst_done`| RX Reset Done, bit per sub port<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`rxrst_trig`| Trige 0->1 to start reset RX SERDES, bit per sub port| `R/W`| `0x0`| `0x0`|

###SERDES LPMDFE Mode

* **Description**           

Configure LPM/DFE mode , there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_LPMDFE_Mode`

* **Address**             : `0x000E-0x600E`

* **Formula**             : `0x000E+$G*0x2000+0xE`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`lpmdfe_mode`| bit per sub port<br>{0} : DFE mode <br>{1} : LPM mode| `R/W`| `0x0`| `0x0`|

###SERDES LPMDFE Reset

* **Description**           

Reset LPM/DFE , there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_LPMDFE_Reset`

* **Address**             : `0x000F-0x600F`

* **Formula**             : `0x000F+$G*0x2000+0xF`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`lpmdfe_reset`| bit per sub port, Must be toggled after switching between modes to initialize adaptation<br>{1} : reset| `R/W`| `0x0`| `0x0`|

###SERDES TXDIFFCTRL

* **Description**           

Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7


* **RTL Instant Name**    : `SERDES_TXDIFFCTRL`

* **Address**             : `0x0010-0x6010`

* **Formula**             : `0x0010+$G*0x2000+0x10`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:15]`|`txdiffctrl_subport3`| | `R/W`| `0x18`| `0x0`|
|`[14:10]`|`txdiffctrl_subport2`| | `R/W`| `0x18`| `0x0`|
|`[09:05]`|`txdiffctrl_subport1`| | `R/W`| `0x18`| `0x0`|
|`[04:00]`|`txdiffctrl_subport0`| | `R/W`| `0x18`| `0x0`|

###SERDES TXPOSTCURSOR

* **Description**           

Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports


* **RTL Instant Name**    : `SERDES_TXPOSTCURSOR`

* **Address**             : `0x0011-0x6011`

* **Formula**             : `0x0011+$G*0x2000+0x11`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:15]`|`txpostcursor_subport3`| | `R/W`| `0x0`| `0x0`|
|`[14:10]`|`txpostcursor_subport2`| | `R/W`| `0x0`| `0x0`|
|`[09:05]`|`txpostcursor_subport1`| | `R/W`| `0x0`| `0x0`|
|`[04:00]`|`txpostcursor_subport0`| | `R/W`| `0x0`| `0x0`|

###SERDES TXPRECURSOR

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports


* **RTL Instant Name**    : `SERDES_TXPRECURSOR`

* **Address**             : `0x0011-0x6011`

* **Formula**             : `0x0011+$G*0x2000+0x11`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:15]`|`txprecursor_subport3`| | `R/W`| `0x0`| `0x0`|
|`[14:10]`|`txprecursor_subport2`| | `R/W`| `0x0`| `0x0`|
|`[09:05]`|`txprecursor_subport1`| | `R/W`| `0x0`| `0x0`|
|`[04:00]`|`txprecursor_subport0`| | `R/W`| `0x0`| `0x0`|

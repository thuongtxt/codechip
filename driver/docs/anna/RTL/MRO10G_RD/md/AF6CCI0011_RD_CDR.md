## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_CDR
####Register Table

|Name|Address|
|-----|-----|
|`CDR CPU  Reg Hold Control`|`0x030000-0x030002`|
|`CDR line mode control`|`0x0000000- 0x0000001`|
|`CDR Timing External reference control`|`0x0000003`|
|`CDR STS Timing control`|`0x0000800-0x000081F`|
|`CDR VT Timing control`|`0x0000C00-0x0000FFF`|
|`CDR ToP Timing Frame sync 8K control`|`0x0000820`|
|`CDR Reference Sync 8K Master Output control`|`0x0000821`|
|`CDR Reference Sync 8k Slaver Output control`|`0x0000822`|
|`CDR ACR Timing External reference  and PRC control`|`0x0020000`|
|`CDR ACR Engine Timing control`|`0x0020800-0x0020BFF`|
|`CDR Adjust State status`|`0x0021000-0x00213FF`|
|`CDR Adjust Holdover value status`|`0x0021800-0x0021BFF`|
|`DCR TX Engine Active Control`|`0x0010000`|
|`DCR PRC Source Select Configuration`|`0x0010001`|
|`DCR PRC Frequency Configuration`|`0x001000b`|
|`DCR RTP Frequency Configuration`|`0x001000C`|
|`DCR Tx Engine Timing control`|`0x0010400`|
|`DCR TxShapper Global Configuration`|`0x0010800`|
|`RAM TimingGen Parity Force Control`|`0x82c`|
|`RAM TimingGen Parity Disable Control`|`0x82d`|
|`RAM TimingGen parity Error Sticky`|`0x82e`|
|`RAM ACR Parity Force Control`|`0x2000c`|
|`RAM ACR Parity Disable Control`|`0x2000d`|
|`RAM ACR parity Error Sticky`|`0x2000e`|
|`Read HA Address Bit3_0 Control`|`0x30200`|
|`Read HA Address Bit7_4 Control`|`0x30210`|
|`Read HA Address Bit11_8 Control`|`0x30220`|
|`Read HA Address Bit15_12 Control`|`0x30230`|
|`Read HA Address Bit19_16 Control`|`0x30240`|
|`Read HA Address Bit23_20 Control`|`0x30250`|
|`Read HA Address Bit24 and Data Control`|`0x30260`|
|`Read HA Hold Data63_32`|`0x30270`|
|`Read HA Hold Data95_64`|`0x30271`|
|`Read HA Hold Data127_96`|`0x30272`|
|`CDR per Channel Interrupt Enable Control`|`0x00025000-0x000253FF`|
|`CDR per Channel Interrupt Status`|`0x00025400-0x000257FF`|
|`CDR per Channel Current Status`|`0x00025800-0x00025BFF`|
|`CDR per Channel Interrupt OR Status`|`0x00025C00-0x00025C1F`|
|`CDR per STS/VC Interrupt OR Status`|`0x00025FFF`|
|`CDR per STS/VC Interrupt Enable Control`|`0x00025FFE`|


###CDR CPU  Reg Hold Control

* **Description**           

The register provides hold register for three word 32-bits MSB when CPU access to engine.


* **RTL Instant Name**    : `cdr_cpu_hold_ctrl`

* **Address**             : `0x030000-0x030002`

* **Formula**             : `0x030000 + HoldId`

* **Where**               : 

    * `$HoldId(0-2): Hold register`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdreg0`| Hold 32 bits| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR line mode control

* **Description**           

This register is used to configure line mode for CDR engine.


* **RTL Instant Name**    : `cdr_line_mode_ctrl`

* **Address**             : `0x0000000- 0x0000001`

* **Formula**             : `0x0000000 + Slide`

* **Where**               : 

    * `$Slide(0-1): Slide = 0 for STM4_0, Slide = 1 for STM4_1`

* **Width**               : `128`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[123:112]`|`vc3mode`| VC3 or TU3 mode, each bit is used configured for each STS (only valid in TU3 CEP) 1: TU3 CEP 0: Other mode| `RW`| `0x0`| `0x0`|
|`[107:96]`|`de3mode`| DS3 or E3 mode, each bit is used configured for each STS. 1: DS3 mode 0: E3 mod| `RW`| `0x0`| `0x0`|
|`[95]`|`sts12stsmode`| STS mode of STS12 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[94:88]`|`sts12vttype`| VT Type of 7 VTG in STS12, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0`|
|`[87]`|`sts11stsmode`| STS mode of STS11 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[86:80]`|`sts11vttype`| VT Type of 7 VTG in STS11, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0`|
|`[79]`|`sts10stsmode`| STS mode of STS10 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[78:72]`|`sts10vttype`| VT Type of 7 VTG in STS10, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0`|
|`[71]`|`sts9stsmode`| STS mode of STS9 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[70:64]`|`sts9vttype`| VT Type of 7 VTG in STS9, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0`|
|`[63]`|`sts8stsmode`| STS mode of STS8 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[62:56]`|`sts8vttype`| VT Type of 7 VTG in STS8, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0`|
|`[55]`|`sts7stsmode`| STS mode of STS7 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[54:48]`|`sts7vttype`| VT Type of 7 VTG in STS7, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0`|
|`[47]`|`sts6stsmode`| STS mode of STS6 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[46:40]`|`sts6vttype`| VT Type of 7 VTG in STS6, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0`|
|`[39]`|`sts5stsmode`| STS mode of STS5 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[38:32]`|`sts5vttype`| VT Type of 7 VTG in STS5, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0`|
|`[31]`|`sts4stsmode`| STS mode of STS4 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[30:24]`|`sts4vttype`| VT Type of 7 VTG in STS4, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0`|
|`[23]`|`sts3stsmode`| STS mode of STS3 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[22:16]`|`sts3vttype`| VT Type of 7 VTG in STS3, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0`|
|`[15]`|`sts2stsmode`| STS mode of STS2 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[14:8]`|`sts2vttype`| VT Type of 7 VTG in STS2, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0`|
|`[7]`|`sts1stsmode`| STS mode of STS1 1: STS1/DS3/E3 mode 0: DS1/E1/VT15/VT2 mode| `RW`| `0x0`| `0x0`|
|`[6:0]`|`sts1vttype`| VT Type of 7 VTG in STS1, each bit is used configured for each VTG. 1: DS1/VT15 mode 0: E1/VT2 mode| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Timing External reference control

* **Description**           

This register is used to configure for the 2Khz coefficient of two external reference signal in order to generate timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)


* **RTL Instant Name**    : `cdr_timing_extref_ctrl`

* **Address**             : `0x0000003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:32]`|`ext3n2k`| The 2Khz coefficient of the third external reference signal| `RW`| `0x0`| `0x0`|
|`[31:16]`|`ext2n2k`| The 2Khz coefficient of the second external reference signal| `RW`| `0x0`| `0x0`|
|`[15:0]`|`ext1n2k`| The 2Khz coefficient of the first external reference signal| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR STS Timing control

* **Description**           

This register is used to configure timing mode for per STS


* **RTL Instant Name**    : `cdr_sts_timing_ctrl`

* **Address**             : `0x0000800-0x000081F`

* **Formula**             : `0x0000800+STS`

* **Where**               : 

    * `$STS(0-23):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[29:20]`|`cdrchid`| CDR channel ID in CDR mode.| `RW`| `0x0`| `0x0`|
|`[17]`|`vc3eparen`| VC3 EPAR mode 0 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[16]`|`mapstsmode`| Map STS mode 0: Payload STS mode 1: Payload DE3 mode| `RW`| `0x0`| `0x0`|
|`[7:4]`|`de3timemode`| DE3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode 9: OCN System timing mode 10: DCR timing mode| `RW`| `0x0`| `0x0`|
|`[3:0]`|`vc3timemode`| VC3 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP mode| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR VT Timing control

* **Description**           

This register is used to configure timing mode for per VT


* **RTL Instant Name**    : `cdr_vt_timing_ctrl`

* **Address**             : `0x0000C00-0x0000FFF`

* **Formula**             : `0x0000C00 + STS*32 + TUG*4 + VT`

* **Where**               : 

    * `$STS(0-23):`

    * `$TUG(0 -6):`

    * `$VT(0-3): (0-2)in E1 and (0-3) in DS1`

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[21:12]`|`cdrchid`| CDR channel ID in CDR mode| `RW`| `0x0`| `0x0`|
|`[9]`|`vteparen`| VT EPAR mode 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[8]`|`mapvtmode`| Map VT mode 0: Payload VT15/VT2 mode 1: Payload DS1/E1 mode| `RW`| `0x0`| `0x0`|
|`[7:4]`|`de1timemode`| DE1 time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode 9: OCN System timing mode 10: DCR timing mode| `RW`| `0x0`| `0x0`|
|`[3:0]`|`vttimemode`| VT time mode 0: Internal timing mode 1: Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: ACR timing mode for CEP mode 9: OCN System timing mode 10: DCR timing mode for CEP mode| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR ToP Timing Frame sync 8K control

* **Description**           

This register is used to configure timing source to generate the 125us signal frame sync for ToP


* **RTL Instant Name**    : `cdr_top_timing_frmsync_ctrl`

* **Address**             : `0x0000820`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[18:16]`|`toppdhlinetype`| Line type of DS1/E1 loop time or PDH CDR 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1 7: Reserve| `RW`| `0x0`| `0x0`|
|`[13:4]`|`toppdhlineid`| Line ID of DS1/E1 loop time or PDH/CEP CDR| `RW`| `0x0`| `0x0`|
|`[3:0]`|`toptimemode`| Time mode  for ToP 0: System timing mode 1: DS1/E1 Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: PDH CDR timing mode 9: OCN System timing mode| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Reference Sync 8K Master Output control

* **Description**           

This register is used to configure timing source to generate the reference sync master output signal


* **RTL Instant Name**    : `cdr_refsync_master_octrl`

* **Address**             : `0x0000821`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[18:16]`|`refout1pdhlinetype`| Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1 7: Reserve| `RW`| `0x0`| `0x0`|
|`[13:4]`|`refout1pdhlineid`| Line ID of DS1/E1 loop time or PDH CDR| `RW`| `0x0`| `0x0`|
|`[3:0]`|`refout1timemode`| Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: PDH CDR timing mode 9: External Clock sync| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Reference Sync 8k Slaver Output control

* **Description**           

This register is used to configure timing source to generate the reference sync slaver output signal


* **RTL Instant Name**    : `cdr_refsync_slaver_octrl`

* **Address**             : `0x0000822`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[18:16]`|`refout2pdhlinetype`| Line type of DS1/E1 loop time or PDH/CEP CDR 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1 7: Reserve| `RW`| `0x0`| `0x0`|
|`[13:4]`|`refout2pdhlineid`| Line ID of DS1/E1 loop time or PDH CDR| `RW`| `0x0`| `0x0`|
|`[3:0]`|`refout2timemode`| Time mode for RefOut1 0: System timing mode 1: DS1/E1 Loop timing mode 2: Line OCN#1 timing mode 3: Line OCN#2 timing mode 4: Line OCN#3 timing mode 5: Line OCN#4 timing mode 6: Line EXT#1 timing mode 7: Line EXT#2 timing mode 8: PDH CDR timing mode 9: External Clock sync| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR ACR Timing External reference  and PRC control

* **Description**           

This register is used to configure for the 2Khz coefficient of external reference signal, PRC clock in order to generate the sync 125us timing. The mean that, if the reference signal is 8Khz, the  coefficient must be configured 4 (I.e 8Khz  = 4x2Khz)


* **RTL Instant Name**    : `cdr_acr_timing_extref_prc_ctrl`

* **Address**             : `0x0020000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:48]`|`txocnn2k`| The 2Khz coefficient of the Ethernet clock signal, default 125Mhz input clock| `RW`| `0x0`| `0x0`|
|`[47:32]`|`ext2n2k`| The 2Khz coefficient of the second external reference signal, default 8khz input signal| `RW`| `0x0`| `0x0`|
|`[31:16]`|`ext1n2k`| The 2Khz coefficient of the first external reference signal, default 8khz input signal| `RW`| `0x0`| `0x0`|
|`[15:0]`|`ocn1n2k`| The 2Khz coefficient of the SONET interface signal, default 8khz input signal| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR ACR Engine Timing control

* **Description**           

This register is used to configure timing mode for per STS


* **RTL Instant Name**    : `cdr_acr_eng_timing_ctrl`

* **Address**             : `0x0020800-0x0020BFF`

* **Formula**             : `0x0020800 + CHID`

* **Where**               : 

    * `$CHID(0-768):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[26]`|`vc4_4c`| VC4_4c/TU3 mode 0: STS1/VC4 1: TU3/VC4-4c| `RW`| `0x0`| `0x0`|
|`[25]`|`pktlen_ind`| The payload packet  length for jumbo frame| `RW`| `0x0`| `0x0`|
|`[24]`|`holdvalmode`| Hold value mode of NCO, default value 0 0: Hardware calculated and auto update 1: Software calculated and update, hardware is disabled| `RW`| `0x0`| `0x0`|
|`[23]`|`seqmode`| Sequence mode mode, default value 0 0: Wrap zero 1: Skip zero| `RW`| `0x0`| `0x0`|
|`[22:20]`|`linetype`| Line type mode 0: E1 1: DS1 2: VT2 3: VT15 4: E3 5: DS3 6: STS1/TU3 7: VC4/VC4-4c| `RW`| `0x0`| `0x0`|
|`[19:4]`|`pktlen`| The payload packet  length parameter to create a packet. SAToP mode: The number payload of bit. CESoPSN mode: The number of bit which converted to full DS1/E1 rate mode. In CESoPSN mode, the payload is assembled by NxDS0 with M frame, the value configured to this register is Mx256 bits for E1 mode, Mx193 bits for DS1 mode.| `RW`| `0x0`| `0x0`|
|`[3:0]`|`cdrtimemode`| CDR time mode 0: System mode 1: Loop timing mode, transparency service Rx clock to service Tx clock 2: LIU timing mode, using service Rx clock for CDR source to generate service Tx clock 3: Prc timing mode, using Prc clock for CDR source to generate service Tx clock 4: Ext#1 timing mode, using Ext#1 clock for CDR source to generate service Tx clock 5: Ext#2 timing mode, using Ext#2 clock for CDR source to generate service Tx clock 6: Tx SONET timing mode, using Tx Line OCN clock for CDR source to generate service Tx clock 7: Free timing mode, using system clock for CDR source to generate service Tx clock 8: ACR timing mode 9: Reserve 10: DCR timing mode 11: Reserve 12: ACR Fast lock mode| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Adjust State status

* **Description**           

This register is used to store status or configure  some parameter of per CDR engine


* **RTL Instant Name**    : `cdr_adj_state_stat`

* **Address**             : `0x0021000-0x00213FF`

* **Formula**             : `0x0021000 + CHID`

* **Where**               : 

    * `$CHID(0-768):`

* **Width**               : `8`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2:0]`|`adjstate`| Adjust state 0: Load state 1: Wait state 2: Init state 3: Learn State 4: ReLearn State 5: Lock State 6: ReInit State 7: Holdover State| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR Adjust Holdover value status

* **Description**           

This register is used to store status or configure  some parameter of per CDR engine


* **RTL Instant Name**    : `cdr_adj_holdover_value_stat`

* **Address**             : `0x0021800-0x0021BFF`

* **Formula**             : `0x0021800 + CHID`

* **Where**               : 

    * `$CHID(0-768):`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdval`| NCO Holdover value parameter E1RATE   = 32'd3817748707;   Resolution =    0.262 parameter DS1RATE  = 32'd3837632815;   Resolution =    0.260 parameter VT2RATE  = 32'd4175662648;   Resolution =    0.239 parameter VT15RATE = 32'd4135894433;   Resolution =    0.241 parameter E3RATE   = 32'd2912117977;   Resolution =    0.343 parameter DS3RATE  = 32'd3790634015;   Resolution =    0.263 parameter TU3RATE  = 32'd4148547956;   Resolution =    0.239 parameter STS1RATE = 32'd4246160849;   Resolution =    0.239| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR TX Engine Active Control

* **Description**           

This register is used to activate the DCR TX Engine. Active high.


* **RTL Instant Name**    : `dcr_tx_eng_active_ctrl`

* **Address**             : `0x0010000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`data`| DCR TX Engine Active Control| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR PRC Source Select Configuration

* **Description**           

This register is used to configure to select PRC source for PRC timer. The PRC clock selected must be less than 19.44 Mhz.


* **RTL Instant Name**    : `dcr_prc_src_sel_cfg`

* **Address**             : `0x0010001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `8`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3]`|`dcrprcdirectmode`| 0: Select RTP frequency lower than 65535 and use value DCRRTPFreq as RTP frequency 1: Select RTP frequency value 155.52Mhz divide by DcrRtpFreqDivide. In this mode, below DcrPrcSourceSel[2:0] must set to 1| `RW`| `0x0`| `0x0`|
|`[2:0]`|`dcrprcsourcesel`| PRC source selection. 0: PRC Reference Clock 1: System Clock 19Mhz 2: External Reference Clock 1. 3: External Reference Clock 2. 4: Ocn Line Clock Port 1 5: Ocn Line Clock Port 2 6: Ocn Line Clock Port 3 7: Ocn Line Clock Port 4| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR PRC Frequency Configuration

* **Description**           

This register is used to configure the frequency of PRC clock.


* **RTL Instant Name**    : `dcr_prc_freq_cfg`

* **Address**             : `0x001000b`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `18`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:0]`|`dcrprcfrequency`| Frequency of PRC clock. This is used to configure the frequency of PRC clock in Khz. Unit is Khz. Exp: The PRC clock is 19.44Mhz. This register will be configured to 19440.| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR RTP Frequency Configuration

* **Description**           

This register is used to configure the frequency of PRC clock.


* **RTL Instant Name**    : `dcr_rtp_freq_cfg`

* **Address**             : `0x001000C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `18`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:0]`|`dcrrtpfreq`| Frequency of RTP clock. This is used to configure the frequency of RTP clock in Khz. Unit is Khz. Exp: The RTP clock is 19.44Mhz. This register will be configured to 19440.| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR Tx Engine Timing control

* **Description**           

This register is used to configure timing mode for per STS


* **RTL Instant Name**    : `dcr_tx_eng_timing_ctrl`

* **Address**             : `0x0010400`

* **Formula**             : `0x0010400 + CHID`

* **Where**               : 

    * `$CHID(0-768):`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[21:19]`|`pktlen_bit`| The payload packet length BIT, PktLen[2:0]| `RW`| `0x0`| `0x0`|
|`[18:16]`|`linetype`| Line type mode 0: DS1 1: E1 2: DS3 3: E3 4: VT15 5: VT2 6: STS1/TU3 7: Reserve| `RW`| `0x0`| `0x0`|
|`[15:0]`|`pktlen_byte`| The payload packet length Byte, PktLen[17:3]| `RW`| `0x0`| `0x0 End: Begin:`|

###DCR TxShapper Global Configuration

* **Description**           

This register is used to configure the frequency of PRC clock.


* **RTL Instant Name**    : `dcr_txsh_glb_cfg`

* **Address**             : `0x0010800`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[24]`|`dcrtxshdis`| Set 1 to Disable TxShapper engine| `RW`| `0x0`| `0x0`|
|`[23:0]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM TimingGen Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_TimingGen_Parity_Force_Control`

* **Address**             : `0x82c`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`cdrvttimingctrl_parerrfrc`| Force parity For RAM Control "CDR VT Timing control"| `RW`| `0x0`| `0x0`|
|`[0]`|`cdrststimingctrl_parerrfrc`| Force parity For RAM Control "CDR STS Timing control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM TimingGen Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_TimingGen_Parity_Disbale_Control`

* **Address**             : `0x82d`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`cdrvttimingctrl_parerrdis`| Disable parity For RAM Control "CDR VT Timing control"| `RW`| `0x0`| `0x0`|
|`[0]`|`cdrststimingctrl_parerrdis`| Disable parity For RAM Control "CDR STS Timing control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM TimingGen parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_TimingGen_Parity_Error_Sticky`

* **Address**             : `0x82e`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`cdrvttimingctrl_parerrstk`| Error parity For RAM Control "CDR VT Timing control"| `RW`| `0x0`| `0x0`|
|`[0]`|`cdrststimingctrl_parerrstk`| Error parity For RAM Control "CDR STS Timing control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM ACR Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_ACR_Parity_Force_Control`

* **Address**             : `0x2000c`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:1]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`cdracrtimingctrl_parerrfrc`| Force parity For RAM Control "CDR ACR Engine Timing control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM ACR Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_ACR_Parity_Disable_Control`

* **Address**             : `0x2000d`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`cdracrtimingctrl_parerrdis`| Disable parity For RAM Control "CDR ACR Engine Timing control"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM ACR parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_ACR_Parity_Error_Sticky`

* **Address**             : `0x2000e`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`cdracrtimingctrl_parerrstk`| Error parity For RAM Control "CDR ACR Engine Timing control"| `RW`| `0x0`| `0x0 End: Begin:`|

###Read HA Address Bit3_0 Control

* **Description**           

This register is used to send HA read address bit3_0 to HA engine


* **RTL Instant Name**    : `rdha3_0_control`

* **Address**             : `0x30200`

* **Formula**             : `0x30200 + HaAddr3_0`

* **Where**               : 

    * `$HaAddr3_0(0-F): HA Address Bit3_0`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr3_0`| Read value will be 0x01000 plus HaAddr3_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit7_4 Control

* **Description**           

This register is used to send HA read address bit7_4 to HA engine


* **RTL Instant Name**    : `rdha7_4_control`

* **Address**             : `0x30210`

* **Formula**             : `0x30210 + HaAddr7_4`

* **Where**               : 

    * `$HaAddr7_4(0-F): HA Address Bit7_4`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr7_4`| Read value will be 0x01000 plus HaAddr7_4| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit11_8 Control

* **Description**           

This register is used to send HA read address bit11_8 to HA engine


* **RTL Instant Name**    : `rdha11_8_control`

* **Address**             : `0x30220`

* **Formula**             : `0x30220 + HaAddr11_8`

* **Where**               : 

    * `$HaAddr11_8(0-F): HA Address Bit11_8`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr11_8`| Read value will be 0x01000 plus HaAddr11_8| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit15_12 Control

* **Description**           

This register is used to send HA read address bit15_12 to HA engine


* **RTL Instant Name**    : `rdha15_12_control`

* **Address**             : `0x30230`

* **Formula**             : `0x30230 + HaAddr15_12`

* **Where**               : 

    * `$HaAddr15_12(0-F): HA Address Bit15_12`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr15_12`| Read value will be 0x01000 plus HaAddr15_12| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit19_16 Control

* **Description**           

This register is used to send HA read address bit19_16 to HA engine


* **RTL Instant Name**    : `rdha19_16_control`

* **Address**             : `0x30240`

* **Formula**             : `0x30240 + HaAddr19_16`

* **Where**               : 

    * `$HaAddr19_16(0-F): HA Address Bit19_16`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr19_16`| Read value will be 0x01000 plus HaAddr19_16| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit23_20 Control

* **Description**           

This register is used to send HA read address bit23_20 to HA engine


* **RTL Instant Name**    : `rdha23_20_control`

* **Address**             : `0x30250`

* **Formula**             : `0x30250 + HaAddr23_20`

* **Where**               : 

    * `$HaAddr23_20(0-F): HA Address Bit23_20`

* **Width**               : `20`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:0]`|`readaddr23_20`| Read value will be 0x01000 plus HaAddr23_20| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Address Bit24 and Data Control

* **Description**           

This register is used to send HA read address bit24 to HA engine to read data


* **RTL Instant Name**    : `rdha24data_control`

* **Address**             : `0x30260`

* **Formula**             : `0x30260 + HaAddr24`

* **Where**               : 

    * `$HaAddr24(0-1): HA Address Bit24`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata31_0`| HA read data bit31_0| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data63_32

* **Description**           

This register is used to read HA dword2 of data.


* **RTL Instant Name**    : `rdha_hold63_32`

* **Address**             : `0x30270`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata63_32`| HA read data bit63_32| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data95_64

* **Description**           

This register is used to read HA dword3 of data.


* **RTL Instant Name**    : `rdindr_hold95_64`

* **Address**             : `0x30271`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata95_64`| HA read data bit95_64| `RW`| `0xx`| `0xx End: Begin:`|

###Read HA Hold Data127_96

* **Description**           

This register is used to read HA dword4 of data.


* **RTL Instant Name**    : `rdindr_hold127_96`

* **Address**             : `0x30272`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`readhadata127_96`| HA read data bit127_96| `RW`| `0xx`| `0xx End: Begin:`|

###CDR per Channel Interrupt Enable Control

* **Description**           

This is the per Channel interrupt enable of CDR


* **RTL Instant Name**    : `cdr_per_chn_intr_en_ctrl`

* **Address**             : `0x00025000-0x000253FF`

* **Formula**             : `0x00025000 +  StsID*32 + VtnID`

* **Where**               : 

    * `$StsID(0-23): STS-1/VC-3 ID`

    * `$VtnID(0-31): VT/TU number ID in the Group`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`cdrunlokcedintren`| Set 1 to enable change UnLocked te event to generate an interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per Channel Interrupt Status

* **Description**           

This is the per Channel interrupt tus of CDR


* **RTL Instant Name**    : `cdr_per_chn_intr_stat`

* **Address**             : `0x00025400-0x000257FF`

* **Formula**             : `0x00025400 +  StsID*32 + VtnID`

* **Where**               : 

    * `$StsID(0-23): STS-1/VC-3 ID`

    * `$VtnID(0-31): VT/TU number ID in the Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`cdrunlockedintr`| Set 1 if there is a change in UnLocked the event.| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per Channel Current Status

* **Description**           

This is the per Channel Current tus of CDR


* **RTL Instant Name**    : `cdr_per_chn_curr_stat`

* **Address**             : `0x00025800-0x00025BFF`

* **Formula**             : `0x00025800 +  StsID*32 + VtnID`

* **Where**               : 

    * `$StsID(0-23): STS-1/VC-3 ID`

    * `$VtnID(0-31): VT/TU number ID in the Group`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`cdrunlockedcurrsta`| Current tus of UnLocked event.| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per Channel Interrupt OR Status

* **Description**           

The register consists of 32 bits for 32 VT/TUs of the related STS/VC in the CDR. Each bit is used to store Interrupt OR tus of the related DS1/E1.


* **RTL Instant Name**    : `cdr_per_chn_intr_or_stat`

* **Address**             : `0x00025C00-0x00025C1F`

* **Formula**             : `0x00025C00 +  StsID`

* **Where**               : 

    * `$StsID(0-23): STS-1/VC-3 ID`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cdrvtintrorsta`| Set to 1 if any interrupt status bit of corresponding DS1/E1 is set and its interrupt is enabled.| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per STS/VC Interrupt OR Status

* **Description**           

The register consists of 24 bits for 24 STS/VCs of the CDR. Each bit is used to store Interrupt OR tus of the related STS/VC.


* **RTL Instant Name**    : `cdr_per_stsvc_intr_or_stat`

* **Address**             : `0x00025FFF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cdrstsintrorsta`| Set to 1 if any interrupt status bit of corresponding STS/VC is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###CDR per STS/VC Interrupt Enable Control

* **Description**           

The register consists of 24 interrupt enable bits for 24 STS/VCs in the Rx DS1/E1/J1 Framer.


* **RTL Instant Name**    : `cdr_per_stsvc_intr_en_ctrl`

* **Address**             : `0x00025FFE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cdrstsintren`| Set to 1 to enable the related STS/VC to generate interrupt.| `RW`| `0x0`| `0x0 End:`|

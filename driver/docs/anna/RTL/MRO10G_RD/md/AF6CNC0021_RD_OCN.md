## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CNC0021_RD_OCN
####Register Table

|Name|Address|
|-----|-----|
|`OCN Global FSM pin value`|`0xf0000`|
|`OCN Global STS Pointer Interpreter Control`|`0xf0002`|
|`OCN Global VTTU Pointer Interpreter Control`|`0xf0003`|
|`OCN Global Pointer Generator Control`|`0xf0004`|
|`OCN Global Loopback`|`0xf0005`|
|`OCN Global APS Grouping SXC_Page Selection`|`0xf0006`|
|`OCN Global Passthrough Grouping Enable`|`0xf0007`|
|`OCN Global Tx HO/LO Selection Control1`|`0xf0010 - 0xf0013`|
|`OCN Global Tx HO/LO Selection Control2`|`0xf0018 - 0xf001b`|
|`OCN Global Rx Framer Control - TFI5 Side`|`0x0f000`|
|`OCN Global Rx Framer LOS Detecting Control 1 - TFI5 Side`|`0x0f006`|
|`OCN Global Rx Framer LOS Detecting Control 2 - TFI5 Side`|`0x0f007`|
|`OCN Global Tx Framer Control - TFI5 Side`|`0x0f001`|
|`OCN STS Pointer Interpreter Per Channel Control - TFI5 Side`|`0x02000 - 0x02e2f`|
|`OCN STS Pointer Generator Per Channel Control - TFI5 Side`|`0x03000 - 0x03e2f`|
|`OCN Rx Bridge and Roll SXC Control - TFI5 Side`|`0x06000 - 0x0672f`|
|`OCN Tx Bridge and Roll SXC Control - TFI5 Side`|`0x07000 - 0x0772f`|
|`OCN Rx Framer Status - TFI5 Side`|`0x05000 - 0x05e00`|
|`OCN Rx Framer Sticky - TFI5 Side`|`0x05001 - 0x05e01`|
|`OCN Rx Framer B1 error counter read only - TFI5 Side`|`0x05002 - 0x05e02`|
|`OCN Rx Framer B1 error counter read to clear - TFI5 Side`|`0x05003 - 0x05e03`|
|`OCN Rx STS/VC per Alarm Interrupt Status - TFI5 Side`|`0x02140 - 0x02f6f`|
|`OCN Rx STS/VC per Alarm Current Status - TFI5 Side`|`0x02180 - 0x02faf`|
|`OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter - TFI5 Side`|`0x02080 - 0x02eaf`|
|`OCN TxPg STS per Alarm Interrupt Status - TFI5 Side`|`0x03140 - 0x03f6f`|
|`OCN TxPg STS pointer adjustment per channel counter - TFI5 Side`|`0x03080 - 0x03eaf`|
|`OCN Rx STS/VC Automatically Concatenation Detection - TFI5 Side`|`0x02040 - 0x02e6f`|
|`OCN Tx STS/VC Automatically Concatenation Detection - TFI5 Side`|`0x03040 - 0x03e6f`|
|`OCN Global Rx Framer Control`|`0x20000`|
|`OCN Global Rx Framer LOS Detecting Control 1`|`0x20006`|
|`OCN Global Rx Framer LOS Detecting Control 2`|`0x20007`|
|`OCN Global Rx Framer LOF Threshold`|`0x20008`|
|`OCN Global Rx Framer - AIS dowstream enable`|`0x2000d`|
|`OCN Global Rx Framer - RDI Back toward enable`|`0x2000e`|
|`OCN Global Tx Framer Control`|`0x20001`|
|`OCN Global Tx Framer Control`|`0x2000a`|
|`OCN Global Tx Framer RDI-L Insertion Threshold Control`|`0x20009`|
|`OCN Global Rx Framer Located Slice Pointer Selection 1`|`0x20010`|
|`OCN Global Rx Framer Located Slice Pointer Selection 2`|`0x20011`|
|`OCN Global Tx Framer Located Slice Pointer Selection 1`|`0x20012`|
|`OCN Global Tx Framer Located Slice Pointer Selection 2`|`0x20013`|
|`OCN Global Rx_TOHBUS K Byte Control`|`0x20014`|
|`OCN Global TOHBUS K_Byte Threshold`|`0x20015`|
|`OCN Global Tx_TOHBUS K Byte Control`|`0x20016`|
|`OCN STS Pointer Interpreter Per Channel Control`|`0x22000 - 0x22e2f`|
|`OCN STS Pointer Generator Per Channel Control`|`0x23000 - 0x23e2f`|
|`OCN Rx STS/VC per Alarm Interrupt Status`|`0x22140 - 0x22f6f`|
|`OCN Rx STS/VC per Alarm Current Status`|`0x22180 - 0x22faf`|
|`OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter`|`0x22080 - 0x22eaf`|
|`OCN TxPg STS per Alarm Interrupt Status`|`0x23140 - 0x23f6f`|
|`OCN TxPg STS pointer adjustment per channel counter`|`0x23080 - 0x23eaf`|
|`OCN Rx STS/VC Automatically Concatenation Detection`|`0x22040 - 0x22e6f`|
|`OCN Tx STS/VC Automatically Concatenation Detection`|`0x23040 - 0x23e6f`|
|`OCN Tx Framer Per Line Control`|`0x21000 - 0x21700`|
|`OCN Tx Framer Per Line Control 1`|`0x21001 - 0x21701`|
|`OCN Tx Framer Per Line Control 2`|`0x21002 - 0x21702`|
|`OCN Tx J0 Insertion Buffer`|`0x21010 - 0x2171f`|
|`OCN TOH Global K1 Stable Monitoring Threshold Control`|`0x27000`|
|`OCN TOH Global K2 Stable Monitoring Threshold Control`|`0x27001`|
|`OCN TOH Global S1 Stable Monitoring Threshold Control`|`0x27002`|
|`OCN TOH Global RDI_L Detecting Threshold Control`|`0x27003`|
|`OCN TOH Global AIS_L Detecting Threshold Control`|`0x27004`|
|`OCN TOH Global K1 Sampling Threshold Control`|`0x27005`|
|`OCN TOH Global Error Counter Control`|`0x27006`|
|`OCN TOH Montoring Affect Control`|`0x27007`|
|`OCN TOH Monitoring Per Line Control`|`0x27010 - 0x27017`|
|`OCN TOH Monitoring B1 Error Read Only Counter`|`0x27100 - 0x27107`|
|`OCN TOH Monitoring B1 Error Read to Clear Counter`|`0x27140 - 0x27147`|
|`OCN TOH Monitoring B1 Block Error Read Only Counter`|`0x27180 - 0x27187`|
|`OCN TOH Monitoring B1 Block Error Read to Clear Counter`|`0x271c0 - 0x271c7`|
|`OCN TOH Monitoring B2 Error Read Only Counter`|`0x27108 - 0x2710f`|
|`OCN TOH Monitoring B2 Error Read to Clear Counter`|`0x27148 - 0x2714f`|
|`OCN TOH Monitoring B2 Block Error Read Only Counter`|`0x27188 - 0x2718f`|
|`OCN TOH Monitoring B2 Block Error Read to Clear Counter`|`0x271c8 - 0x271cf`|
|`OCN TOH Monitoring REI Error Read Only Counter`|`0x27128 - 0x2712f`|
|`OCN TOH Monitoring REI Error Read to Clear Counter`|`0x27168 - 0x2716f`|
|`OCN TOH Monitoring REI Block Error Read Only Counter`|`0x271a8 - 0x271af`|
|`OCN TOH Monitoring REI Block Error Read to Clear Counter`|`0x271e8 - 0x270ef`|
|`OCN TOH K1 Monitoring Status`|`0x27110 - 0x27117`|
|`OCN TOH K2 Monitoring Status`|`0x27118 - 0x2711f`|
|`OCN TOH S1 Monitoring Status`|`0x27120 - 0x27127`|
|`OCN Rx Line per Alarm Interrupt Enable Control`|`0x27080 - 0x27087`|
|`OCN Rx Line per Alarm Interrupt Status`|`0x27088 - 0x2708f`|
|`OCN Rx Line per Alarm Current Status`|`0x27090 - 0x27097`|
|`OCN Rx Line Per Line Interrupt Enable Control`|`0x2709e`|
|`OCN Rx Line per Line Interrupt OR Status`|`0x2709f`|
|`OCN Rx Framer Z1 Z2 STS Select Control`|`0x21003 - 0x21703`|
|`OCN Rx Line Per Line E1 captured byte`|`0x27200 - 0x27207`|
|`OCN Rx Line Per Line F1 captured byte`|`0x27208 - 0x2720f`|
|`OCN Rx Line Per Line Z1 captured byte`|`0x27210 - 0x27217`|
|`OCN Rx Line Per Line Z2 captured byte`|`0x27218 - 0x2721f`|
|`OCN Rx Line Per Line E2 captured byte`|`0x27220 - 0x27227`|
|`OCN SXC Control 1 - Config Page 0 and Page 1 of SXC`|`0x44000 - 0x44b2f`|
|`OCN SXC Control 2 - Config Page 2 of SXC`|`0x44040 - 0x44b6f`|
|`OCN SXC Control 3 - Config APS`|`0x44080 - 0x44baf`|
|`OCN Rx High Order Map concatenate configuration`|`0x48000 - 0x4872f`|
|`OCN RXPP Per STS payload Control`|`0x60000 - 0x7402f`|
|`OCN VTTU Pointer Interpreter Per Channel Control`|`0x60800 - 0x74fff`|
|`OCN TXPP Per STS Multiplexing Control`|`0x80000 - 0x9402f`|
|`OCN VTTU Pointer Generator Per Channel Control`|`0x80800 - 0x94fff`|
|`OCN Rx VT/TU per Alarm Interrupt Status`|`0x62800 - 0x76fff`|
|`OCN Rx VT/TU per Alarm Current Status`|`0x63000 - 0x77fff`|
|`OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter`|`0x61000 - 0x75fff`|
|`OCN TxPg VTTU per Alarm Interrupt Status`|`0x82800 - 0x96fff`|
|`OCN TxPg VTTU pointer adjustment per channel counter`|`0x81000 - 0x95fff`|
|`OCN Parity Force Config 1 - TFI5 Side`|`0x0f020`|
|`OCN Parity Disable Config 1 - TFI5 Side`|`0x0f021`|
|`OCN Parity Error Sticky 1 - TFI5 Side`|`0x0f022`|
|`OCN Parity Force Config 2 - Line Side`|`0x20020`|
|`OCN Parity Disable Config 2 - Line Side`|`0x20021`|
|`OCN Parity Error Sticky 2 - Line Side`|`0x20022`|
|`OCN Parity Force Config 3 - VPI+VPG+OHO`|`0xf0020`|
|`OCN Parity Disable Config 3 - VPI+VPG+OHO`|`0xf0021`|
|`OCN Parity Error Sticky 3 - VPI+VPG+OHO`|`0xf0022`|
|`OCN Parity Force Config 5 - SXC selector page0+1`|`0xf0024`|
|`OCN Parity Disable Config 5 - SXC selector page0+1`|`0xf0025`|
|`OCN Parity Error Sticky 5 - SXC selector page0+1`|`0xf0026`|
|`OCN Parity Force Config 6 - SXC selector page2`|`0xf0028`|
|`OCN Parity Disable Config 6 - SXC selector page2`|`0xf0029`|
|`OCN Parity Error Sticky 6 - SXC selector page2`|`0xf002a`|
|`OCN Parity Force Config 7 - SXC Config APS`|`0xf002c`|
|`OCN Parity Disable Config 7 - SXC Config APS`|`0xf002d`|
|`OCN Parity Error Sticky 7 - SXC Config APS`|`0xf002e`|


###OCN Global FSM pin value

* **Description**           

This is the register to read FSM pin value


* **RTL Instant Name**    : `glbfsm_reg`

* **Address**             : `0xf0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:1]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[0]`|`fsmvalue`| FSM pin value.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Global STS Pointer Interpreter Control

* **Description**           

This is the global configuration register for the STS Pointer Interpreter


* **RTL Instant Name**    : `glbspi_reg`

* **Address**             : `0xf0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[27:24]`|`rxpgflowthresh`| Overflow/underflow threshold to resynchronize read/write pointer.| `RW`| `0x3`| `0x3`|
|`[23:20]`|`rxpgadjthresh`| Adjustment threshold to make a pointer increment/decrement.| `RW`| `0xc`| `0xc`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`stspiaisaispen`| Enable/Disable forwarding P_AIS when AIS state is detected at STS Pointer interpreter. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[17]`|`stspilopaispen`| Enable/Disable forwarding P_AIS when LOP state is detected at STS Pointer interpreter. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[16]`|`stspimajormode`| Majority mode for detecting increment/decrement at STS pointer Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5| `RW`| `0x1`| `0x1`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:12]`|`stspinorptrthresh`| Threshold of number of normal pointers between two contiguous frames within pointer adjustments.| `RW`| `0x3`| `0x3`|
|`[11:8]`|`stspindfptrthresh`| Threshold of number of contiguous NDF pointers for entering LOP state at FSM.| `RW`| `0x8`| `0x8`|
|`[7:4]`|`stspibadptrthresh`| Threshold of number of contiguous invalid pointers for entering LOP state at FSM.| `RW`| `0x8`| `0x8`|
|`[3:0]`|`stspipohaistype`| Enable/disable STS POH defect types to downstream AIS in case of terminating the related STS such as the STS carries VT/TU. [0]: Enable for TIM defect [1]: Enable for Unequiped defect [2]: Enable for VC-AIS [3]: Enable for PLM defect| `RW`| `0xf`| `0xf End : Begin:`|

###OCN Global VTTU Pointer Interpreter Control

* **Description**           

This is the global configuration register for the VTTU Pointer Interpreter


* **RTL Instant Name**    : `glbvpi_reg`

* **Address**             : `0xf0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[29]`|`vtpilomaispen`| Enable/Disable forwarding AIS when LOM is detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[28]`|`vtpilominvlcntmod`| H4 monitoring mode. 1: Expected H4 is current frame in the validated sequence plus one. 0: Expected H4 is the last received value plus one.| `RW`| `0x0`| `0x0`|
|`[27:24]`|`vtpilomgoodthresh`| Threshold of number of contiguous frames with validated sequence	of multi framers in LOM state for condition to entering IM state.| `RW`| `0x3`| `0x3`|
|`[23:20]`|`vtpilominvlthresh`| Threshold of number of contiguous frames with invalidated sequence of multi framers in IM state  for condition to entering LOM state.| `RW`| `0x8`| `0x8`|
|`[19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`vtpiaisaispen`| Enable/Disable forwarding AIS when AIS state is detected at VTTU Pointer interpreter. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[17]`|`vtpilopaispen`| Enable/Disable forwarding AIS when LOP state is detected at VTTU Pointer interpreter. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[16]`|`vtpimajormode`| Majority mode detecting increment/decrement in VTTU pointer Interpreter. It is used for rule n of 5. 1: n = 3 0: n = 5| `RW`| `0x1`| `0x1`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:12]`|`vtpinorptrthresh`| Threshold of number of normal pointers between two contiguous frames within pointer adjustments.| `RW`| `0x3`| `0x3`|
|`[11:8]`|`vtpindfptrthresh`| Threshold of number of contiguous NDF pointers for entering LOP state at FSM.| `RW`| `0x8`| `0x8`|
|`[7:4]`|`vtpibadptrthresh`| Threshold of number of contiguous invalid pointers for entering LOP state at FSM.| `RW`| `0x8`| `0x8`|
|`[3:0]`|`vtpipohaistype`| Enable/disable VTTU POH defect types to downstream AIS in case of terminating the related VTTU. [0]: Enable for TIM defect [1]: Enable for Un-equipment defect [2]: Enable for VC-AIS [3]: Enable for PLM defect| `RW`| `0xf`| `0xf End : Begin:`|

###OCN Global Pointer Generator Control

* **Description**           

This is the global configuration register for the Tx Pointer Generator


* **RTL Instant Name**    : `glbtpg_reg`

* **Address**             : `0xf0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9:8]`|`txpgnorptrthresh`| Threshold of number of normal pointers between two contiguous frames to make a condition of pointer adjustments.| `RW`| `0x3`| `0x3`|
|`[7:4]`|`txpgflowthresh`| Overflow/underflow threshold to resynchronize read/write pointer of TxFiFo.| `RW`| `0x3`| `0x3`|
|`[3:0]`|`txpgadjthresh`| Adjustment threshold to make a condition of pointer increment/decrement.| `RW`| `0xd`| `0xd End : Begin:`|

###OCN Global Loopback

* **Description**           

This is the global configuration register for internal loopback


* **RTL Instant Name**    : `glbloop_reg`

* **Address**             : `0xf0005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:5]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[4]`|`sxcapsswfsm`| SW-FSM for APS. 1: Page ID 1 is selected 0: Page ID 0 is selected| `RW`| `0x0`| `0x0`|
|`[3]`|`stspohsel`| STS POH Selection. 1: TFI5 is selected to Mon/Insert POH 0: FacePlate is selected to Mon/Insert POH| `RW`| `0x0`| `0x0`|
|`[2:1]`|`internaldebug`| Internal Debug| `RW`| `0x0`| `0x0`|
|`[0]`|`loloopback`| LoBus Loopback. 1: Loopback in 0: Normal (No loopback)| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global APS Grouping SXC_Page Selection

* **Description**           

This register use 16 bits to configure SXC_Page ID for 16 APS Group. Bit 0 for APS Group 0


* **RTL Instant Name**    : `glbapsgrppage_reg`

* **Address**             : `0xf0006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:0]`|`apsgrppageid`| Each bit is used to configure SXC_Page ID for 16 APS Group. Bit 0 for APS Group 0 1: SXC_Page ID 1 is selected 0: SXC_Page ID 0 is selected| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Passthrough Grouping Enable

* **Description**           

This register use 32 bits to configure for 16 Passthrough Groups at Rx direction and 16 Passthrough Groups at Tx direction.


* **RTL Instant Name**    : `glbpasgrpenb_reg`

* **Address**             : `0xf0007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`txspgpassthrgrpenb`| Each bit is used to configure enable/disable Passthrough mode at Tx SPG for 1 Passthrough Group. Bit 16 for Passthrough Group Tx0 1: Enable 0: Disable.| `RW`| `0x0`| `0x0`|
|`[15:0]`|`rxspipassthrgrpenb`| Each bit is used to configure enable/disable Passthrough mode at Rx SPI for 1 Passthrough Group. Bit 0 for Passthrough Group Rx0 1: Enable 0: Disable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Tx HO/LO Selection Control1

* **Description**           

Configure Tx HO/LO Selection for 4 slices HO/LO OC48 for STS from 0-31.


* **RTL Instant Name**    : `glbtxholosel1_reg`

* **Address**             : `0xf0010 - 0xf0013`

* **Formula**             : `0xf0010 + SliceOc48Id`

* **Where**               : 

    * `$SliceOc48Id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txholoselctl1`| Each bit will be config per STS in any slices HO/LO OC48, bit #0 for STS#0. 1: Select from Ho_Bus 0: Select from Lo_Bus| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Tx HO/LO Selection Control2

* **Description**           

Configure Tx HO/LO Selection for 4 slices HO/LO OC48 for STS from 32-47.


* **RTL Instant Name**    : `glbtxholosel2_reg`

* **Address**             : `0xf0018 - 0xf001b`

* **Formula**             : `0xf0018 + SliceOc48Id`

* **Where**               : 

    * `$SliceOc48Id(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`txholoselctl2`| Each bit will be config per STS in any slices HO/LO OC48, bit #0 for STS#32. 1: Select from Ho_Bus 0: Select from Lo_Bus| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Rx Framer Control - TFI5 Side

* **Description**           

This is the global configuration register for the Rx Framer


* **RTL Instant Name**    : `tfi5glbrfm_reg`

* **Address**             : `0x0f000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:16]`|`tfi5rxlinesyncsel`| Select line id for synchronization 8 rx lines. Bit[0] for line 0. Note that must only 1 bit is set| `RW`| `0x1`| `0x1`|
|`[15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`tfi5rxfrmlosaisen`| Enable/disable forwarding P_AIS when LOS detected at Rx Framer. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[13]`|`tfi5rxfrmoofaisen`| Enable/disable forwarding P_AIS when OOF detected at Rx Framer. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[12]`|`tfi5rxfrmb1blockcntmod`| B1 Counter Mode. 1: Block mode 0: Bit-wise mode| `RW`| `0x1`| `0x1`|
|`[11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10:8]`|`tfi5rxfrmbadfrmthresh`| Threshold for A1A2 missing counter, that is used to change state from FRAMED to HUNT.| `RW`| `0x3`| `0x3`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6:4]`|`tfi5rxfrmb1goodthresh`| Threshold for B1 good counter, that is used to change state from CHECK to FRAMED.| `RW`| `0x4`| `0x4`|
|`[3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`tfi5rxfrmdescren`| Enable/disable de-scrambling of the Rx coming data stream. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[1]`|`tfi5rxfrmb1chkfrmen`| Enable/disable B1 check option is added to the required framing algorithm. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[0]`|`tfi5rxfrmmodeen`| TFI-5 mode. 1: Enable 0: Disable| `RW`| `0x1`| `0x1 End : Begin:`|

###OCN Global Rx Framer LOS Detecting Control 1 - TFI5 Side

* **Description**           

This is the global configuration register for the Rx Framer LOS detecting 1


* **RTL Instant Name**    : `tfi5glbclkmon_reg`

* **Address**             : `0x0f006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`tfi5rxfrmlosdetmod`| Detected LOS mode. 1: the LOS declare when all zero or all one detected 0: the LOS declare when all zero detected| `RW`| `0x1`| `0x1`|
|`[17]`|`tfi5rxfrmlosdetdis`| Disable detect LOS. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[16]`|`tfi5rxfrmclkmondis`| Disable to generate LOS to Rx Framer if detecting error on Rx line clock. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[15:0]`|`tfi5rxfrmclkmonthr`| Threshold to generate LOS to Rx Framer if detecting error on Rx line clock.(Threshold = PPM * 155.52/8). Default 0x3cc ~ 50ppm| `RW`| `0x3cc`| `0x3cc End : Begin:`|

###OCN Global Rx Framer LOS Detecting Control 2 - TFI5 Side

* **Description**           

This is the global configuration register for the Rx Framer LOS detecting 2


* **RTL Instant Name**    : `tfi5glbdetlos_pen`

* **Address**             : `0x0f007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`tfi5rxfrmlosclr2thr`| Configure the period of time during which there is no period of consecutive data without transition, used for reset no transition counter. The recommended value is 0x30 (~2.5us).| `RW`| `0x30`| `0x30`|
|`[23:12]`|`tfi5rxfrmlossetthr`| Configure the value that define the period of time in which there is no transition detected from incoming data from SERDES. The recommended value is 0x3cc (~50us).| `RW`| `0x3cc`| `0x3cc`|
|`[11:0]`|`tfi5rxfrmlosclr1thr`| Configure the period of time during which there is no period of consecutive data without transition, used for counting at INFRM to change state to LOS The recommended value is 0x3cc (~50us).| `RW`| `0x3cc`| `0x3cc End : Begin:`|

###OCN Global Tx Framer Control - TFI5 Side

* **Description**           

This is the global configuration register for the Tx Framer


* **RTL Instant Name**    : `tfi5glbtfm_reg`

* **Address**             : `0x0f001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`tfi5txlineooffrc`| Enable/disable force OOF for 8 tx lines. Bit[0] for line 0.| `RW`| `0x0`| `0x0`|
|`[23:16]`|`tfi5txlineb1errfrc`| Enable/disable force B1 error for 8 tx lines. Bit[0] for line 0.| `RW`| `0x0`| `0x0`|
|`[15:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:4]`|`tfi5txlinesyncsel`| Select line id for synchronization 8 tx lines. Bit[0] for line 0. Note that must only 1 bit is set| `RW`| `0x1`| `0x1`|
|`[3:1]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`tfi5txfrmscren`| Enable/disable scrambling of the Tx data stream. 1: Enable 0: Disable| `RW`| `0x1`| `0x1 End : Begin:`|

###OCN STS Pointer Interpreter Per Channel Control - TFI5 Side

* **Description**           

Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3.


* **RTL Instant Name**    : `tfi5spiramctl`

* **Address**             : `0x02000 - 0x02e2f`

* **Formula**             : `0x02000 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:21]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[20]`|`tfi5passthrenb`| Enable/disable Passthrough mode for BLSR at Rx SPI of TFI5 Side 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[19:16]`|`tfi5passthrgrp`| Passthrough Group ID for BLSR at Rx SPI of TFI5 Side.| `RW`| `0x0`| `0x0`|
|`[15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`tfi5stspichklom`| Enable/disable LOM checking. This field will be set to 1 when payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[13:12]`|`tfi5stspissdetpatt`| Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction.| `RW`| `0x0`| `0x0`|
|`[11]`|`tfi5stspiaisfrc`| Forcing FSM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[10]`|`tfi5stspissdeten`| Enable/disable checking SS bits in STSPI state machine. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[9]`|`tfi5stspiadjrule`| Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode| `RW`| `0x0`| `0x0`|
|`[8]`|`tfi5stspistsslvind`| This is used to configure STS is slaver or master. 1: Slaver.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`tfi5stspistsmstid`| This is the ID of the master STS-1 in the concatenation that contains this STS-1.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN STS Pointer Generator Per Channel Control - TFI5 Side

* **Description**           

Each register is used to configure for STS pointer Generator engines.

# HDL_PATH		: itfi5ho.itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array


* **RTL Instant Name**    : `tfi5spgramctl`

* **Address**             : `0x03000 - 0x03e2f`

* **Formula**             : `0x03000 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`tfi5stspgstsslvind`| This is used to configure STS is slaver or master. 1: Slaver.| `RW`| `0x0`| `0x0`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:8]`|`tfi5stspgstsmstid`| This is the ID of the master STS-1 in the concatenation that contains this STS-1.| `RW`| `0x0`| `0x0`|
|`[7]`|`tfi5stspgb3biperrfrc`| Forcing B3 Bip error. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[6]`|`tfi5stspglopfrc`| Forcing LOP. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[5]`|`tfi5stspgueqfrc`| Forcing FSM to UEQ state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[4]`|`tfi5stspgaisfrc`| Forcing FSM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[3:2]`|`tfi5stspgssinspatt`| Configure pattern SS bits that is used to insert to pointer value.| `RW`| `0x0`| `0x0`|
|`[1]`|`tfi5stspgssinsen`| Enable/disable SS bits insertion. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`tfi5stspgpohins`| Enable/disable POH Insertion. High to enable insertion of POH.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Bridge and Roll SXC Control - TFI5 Side

* **Description**           

Each register is used for each outgoing STS (to Rx SPI) of any line (8 lines) can be randomly configured to //connect to any ingoing STS of any ingoing line (from TFI-5 line - 8 lines).

# HDL_PATH		: itfi5ho.irxfrm_inst.isdhbar_sxc.isdhbar_sxcrd[0].rxsxcramctl.array


* **RTL Instant Name**    : `tfi5rxbarsxcramctl`

* **Address**             : `0x06000 - 0x0672f`

* **Formula**             : `0x06000 + 256*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`tfi5rxbarsxclineid`| Contains the ingoing LineID (0-7). Disconnect (output is all one) if value LineID is other value (recommend is 14).| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`tfi5rxbarsxcstsid`| Contains the ingoing STSID (0-47).| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Tx Bridge and Roll SXC Control - TFI5 Side

* **Description**           

Each register is used for each outgoing STS (to TFI-5 Line) of any line (8 lines) can be randomly configured to connect to any ingoing STS of any ingoing line (from Tx SPG) .

# HDL_PATH		: itfi5ho.itxpp_stspp_inst.isdhtxbar_sxc.isdhbar_sxcrd[0].rxsxcramctl.array


* **RTL Instant Name**    : `tfi5txbarsxcramctl`

* **Address**             : `0x07000 - 0x0772f`

* **Formula**             : `0x07000 + 256*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`tfi5txbarsxclineid`| Contains the ingoing LineID (0-7). Disconnect (output is all one) if value LineID is other value (recommend is 14).| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`tfi5txsxcstsid`| Contains the ingoing STSID (0-47).| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer Status - TFI5 Side

* **Description**           

Rx Framer status


* **RTL Instant Name**    : `tfi5rxfrmsta`

* **Address**             : `0x05000 - 0x05e00`

* **Formula**             : `0x05000 + 512*LineId`

* **Where**               : 

    * `$LineId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[2]`|`tfi5staffcnvful`| Status fifo convert clock domain is full| `RO`| `0x0`| `0x0`|
|`[1]`|`tfi5stalos`| Loss of Signal status| `RO`| `0x0`| `0x0`|
|`[0]`|`tfi5staoof`| Out of Frame that is detected at RxFramer| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer Sticky - TFI5 Side

* **Description**           

Rx Framer sticky


* **RTL Instant Name**    : `tfi5rxfrmstk`

* **Address**             : `0x05001 - 0x05e01`

* **Formula**             : `0x05001 + 512*LineId`

* **Where**               : 

    * `$LineId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[2]`|`tfi5stkffcnvful`| Sticky fifo convert clock domain is full| `W1C`| `0x0`| `0x0`|
|`[1]`|`tfi5stklos`| Loss of Signal  sticky change| `W1C`| `0x0`| `0x0`|
|`[0]`|`tfi5stkoof`| Out of Frame sticky  change| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer B1 error counter read only - TFI5 Side

* **Description**           

Rx Framer B1 error counter read only


* **RTL Instant Name**    : `tfi5rxfrmb1cntro`

* **Address**             : `0x05002 - 0x05e02`

* **Formula**             : `0x05002 + 512*LineId`

* **Where**               : 

    * `$LineId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tfi5rxfrmb1errro`| Number of B1 error - read only| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer B1 error counter read to clear - TFI5 Side

* **Description**           

Rx Framer B1 error counter read to clear


* **RTL Instant Name**    : `tfi5rxfrmb1cntr2c`

* **Address**             : `0x05003 - 0x05e03`

* **Formula**             : `0x05003 + 512*LineId`

* **Where**               : 

    * `$LineId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tfi5rxfrmb1errr2c`| Number of B1 error - read to clear| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN Rx STS/VC per Alarm Interrupt Status - TFI5 Side

* **Description**           

This is the per Alarm interrupt status of STS/VC pointer interpreter.  %%

Each register is used to store 6 sticky bits for 6 alarms in the STS/VC.


* **RTL Instant Name**    : `tfi5upstschstkram`

* **Address**             : `0x02140 - 0x02f6f`

* **Formula**             : `0x02140 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:6]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[5]`|`tfi5stspistsconcdetintr`| Set to 1 while an Concatenation Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[4]`|`tfi5stspistsnewdetintr`| Set to 1 while an New Pointer Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[3]`|`tfi5stspistsndfintr`| Set to 1 while an NDF event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[2]`|`tfi5stspicepuneqstatepchgintr`| Set to 1  while there is change in CEP Un-equip Path in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in CEP Un-equip Path state or not.| `W1C`| `0x0`| `0x0`|
|`[1]`|`tfi5stspistsaisstatechgintr`| Set to 1 while there is change in AIS state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in AIS state or not.| `W1C`| `0x0`| `0x0`|
|`[0]`|`tfi5stspistslopstatechgintr`| Set 1 to while there is change in LOP state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in LOP state or not.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx STS/VC per Alarm Current Status - TFI5 Side

* **Description**           

This is the per Alarm current status of STS/VC pointer interpreter.  %%

Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC.


* **RTL Instant Name**    : `tfi5upstschstaram`

* **Address**             : `0x02180 - 0x02faf`

* **Formula**             : `0x02180 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[2]`|`tfi5stspistscepuneqpcurstatus`| CEP Un-eqip Path current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the Tfi5StsPiStsCepUeqPStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.| `RO`| `0x0`| `0x0`|
|`[1]`|`tfi5stspistsaiscurstatus`| AIS current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  Tfi5StsPiStsAISStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.| `RO`| `0x0`| `0x0`|
|`[0]`|`tfi5stspistslopcurstatus`| LOP current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  Tfi5StsPiStsLopStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter - TFI5 Side

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode


* **RTL Instant Name**    : `tfi5adjcntperstsram`

* **Address**             : `0x02080 - 0x02eaf`

* **Formula**             : `0x02080 + 512*LineId + 64*AdjMode + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$AdjMode(0-1)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`tfi5rxppstspiptadjcnt`| The pointer Increment counter or decrease counter. Bit [6] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg STS per Alarm Interrupt Status - TFI5 Side

* **Description**           

This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms


* **RTL Instant Name**    : `tfi5stspgstkram`

* **Address**             : `0x03140 - 0x03f6f`

* **Formula**             : `0x03140 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[3]`|`tfi5stspgaisintr`| Set to 1 while an AIS status event (at h2pos) is detected at Tx STS Pointer Generator.| `W1C`| `0x0`| `0x0										// Field: [2]`|
|`[1]`|`tfi5stspgndfintr`| Set to 1 while an NDF status event (at h2pos) is detected at Tx STS Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[0]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg STS pointer adjustment per channel counter - TFI5 Side

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode


* **RTL Instant Name**    : `tfi5adjcntpgperstsram`

* **Address**             : `0x03080 - 0x03eaf`

* **Formula**             : `0x03080 + 512*LineId + 64*AdjMode + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$AdjMode(0-1)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`tfi5stspgptadjcnt`| The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN Rx STS/VC Automatically Concatenation Detection - TFI5 Side

* **Description**           

This is the Automatically Concatenation Detection Status at Rx Pointer Interpreter


* **RTL Instant Name**    : `tfi5rxstsconcdetreg`

* **Address**             : `0x02040 - 0x02e6f`

* **Formula**             : `0x02040 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[11]`|`tfi5spicondetvc416c`| This is the indicator that this STS is member of 1 VC4_16C, which is automatically detected.| `RO`| `0x0`| `0x0`|
|`[10]`|`tfi5spicondetvc44c`| This is the indicator that this STS is member of 1 VC4_4C, which is automatically detected.| `RO`| `0x0`| `0x0`|
|`[9]`|`tfi5spicondetvc4`| This is the indicator that this STS is member of 1 VC4, which is automatically detected.| `RO`| `0x0`| `0x0`|
|`[8]`|`tfi5spicondetslvind`| This is the indicator that this STS is slaver or master which is automatically detected. 1: Slaver.| `RO`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[5:0]`|`tfi5spicondetmstid`| This is the ID of the master STS-1 in the concatenation that contains this STS-1 which is automatically detected.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Tx STS/VC Automatically Concatenation Detection - TFI5 Side

* **Description**           

This is the Automatically Concatenation Detection Status at Tx Pointer Generator based on Concatenation indicator from SXC.


* **RTL Instant Name**    : `tfi5txstsconcdetreg`

* **Address**             : `0x03040 - 0x03e6f`

* **Formula**             : `0x03040 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `WO`| `0x0`| `0x0`|
|`[11]`|`tfi5spgcondetvc416c`| This is the indicator that this STS is member of 1 VC4_16C, which is automatically detected.| `RO`| `0x0`| `0x0`|
|`[10]`|`tfi5spgcondetvc44c`| This is the indicator that this STS is member of 1 VC4_4C, which is automatically detected.| `RO`| `0x0`| `0x0`|
|`[9]`|`tfi5spgcondetvc4`| This is the indicator that this STS is member of 1 VC4, which is automatically detected.| `RO`| `0x0`| `0x0`|
|`[8]`|`tfi5spgcondetslvind`| This is the indicator that this STS is slaver or master which is automatically detected. 1: Slaver.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[5:0]`|`tfi5spgcondetmstid`| This is the ID of the master STS-1 in the concatenation that contains this STS-1 which is automatically detected.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Global Rx Framer Control

* **Description**           

This is the global configuration register for the Rx Framer


* **RTL Instant Name**    : `glbrfm_reg`

* **Address**             : `0x20000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:25]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[24]`|`rxfrmoc192enb`| Enable mode OC192 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[23:16]`|`rxfrmdescren`| Enable/disable de-scrambling of the Rx coming data stream. 1: Enable 0: Disable| `RW`| `0xff`| `0xff`|
|`[15:0]`|`rxfrmstmocnmode`| STM rate mode for Rx of  8 lines, each line use 2 bits.Bits[1:0] for line 0 0: OC48 1: OC12 2: OC3 3: OC1| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Rx Framer LOS Detecting Control 1

* **Description**           

This is the global configuration register for the Rx Framer LOS detecting 1


* **RTL Instant Name**    : `glbclkmon_reg`

* **Address**             : `0x20006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:19]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[18]`|`rxfrmlosdetmod`| Detected LOS mode. 1: the LOS declare when all zero or all one detected 0: the LOS declare when all zero detected| `RW`| `0x1`| `0x1`|
|`[17]`|`rxfrmlosdetdis`| Disable detect LOS. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[16]`|`rxfrmclkmondis`| Disable to generate LOS to Rx Framer if detecting error on Rx line clock. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[15:0]`|`rxfrmclkmonthr`| Threshold to generate LOS to Rx Framer if detecting error on Rx line clock.(Threshold = PPM * 155.52/8). Default 0x3cc ~ 50ppm| `RW`| `0x3cc`| `0x3cc End : Begin:`|

###OCN Global Rx Framer LOS Detecting Control 2

* **Description**           

This is the global configuration register for the Rx Framer LOS detecting 2


* **RTL Instant Name**    : `glbdetlos_reg`

* **Address**             : `0x20007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`rxfrmlosclr2thr`| Configure the period of time during which there is no period of consecutive data without transition, used for reset no transition counter. The recommended value is 0x18 (~2.5us).| `RW`| `0x18`| `0x18`|
|`[23:12]`|`rxfrmlossetthr`| Configure the value that define the period of time in which there is no transition detected from incoming data from SERDES. The recommended value is 0x1e6 (~50us).| `RW`| `0x3cc`| `0x3cc`|
|`[11:0]`|`rxfrmlosclr1thr`| Configure the period of time during which there is no period of consecutive data without transition, used for counting at INFRM to change state to LOS The recommended value is 0x3cc (~50us).| `RW`| `0x3cc`| `0x3cc End : Begin:`|

###OCN Global Rx Framer LOF Threshold

* **Description**           

Configure thresholds for LOF detection at receive SONET/SDH framer,There are two thresholds


* **RTL Instant Name**    : `glblofthr_reg`

* **Address**             : `0x20008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`rxfrmlofdetmod`| Detected LOF mode.Set 1 to clear OOF counter when state into INFRAMED| `RW`| `0x1`| `0x1`|
|`[15:8]`|`rxfrmlofsetthr`| Configure the OOF time counter threshold for entering LOF state. Resolution of this threshold is one frame.| `RW`| `0x18`| `0x18`|
|`[7:0]`|`rxfrmlofclrthr`| Configure the In-frame time counter threshold for exiting LOF state. Resolution of this threshold is one frame.| `RW`| `0x18`| `0x18 End : Begin:`|

###OCN Global Rx Framer - AIS dowstream enable

* **Description**           

Enable/disable for AIS dowstream at receive SONET/SDH framer of 8 lines, each line used 4bits,Bits[3:0] for line 0


* **RTL Instant Name**    : `glbrfmaisfwd_reg`

* **Address**             : `0x2000d`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`rxfrmaisfwden2_8`| Enable/disable for AIS dowstream at receive SONET/SDH framer of line2-8, each line used 4bits,Bits[3:0] for line 0| `RW`| `0xfffffff`| `0xfffffff`|
|`[3]`|`rxfrmaislaispen1`| Enable/disable the insertion of P-AIS at STS pointer interpreter when AIS_L condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[2]`|`rxfrmtimaispen1`| Enable/disable the insertion of P-AIS at STS pointer interpreter when TIM condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[1]`|`rxfrmlofaispen1`| Enable/disable the insertion of P-AIS at STS pointer interpreter when LOF condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[0]`|`rxfrmlosaispen1`| Enable/disable the insertion of P-AIS at STS pointer interpreter when LOS condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1 End : Begin:`|

###OCN Global Rx Framer - RDI Back toward enable

* **Description**           

Enable/disable for RDI Back toward at transceive SONET/SDH framer of 8 lines, each line used 4bits,Bits[3:0] for line 0


* **RTL Instant Name**    : `glbrfmrdibwd_reg`

* **Address**             : `0x2000e`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`rxfrmaisfwden2_8`| Enable/disable for RDI Back toward at transceive SONET/SDH framer of line2-8, each line used 4bits,Bits[3:0] for line 0| `RW`| `0xfffffff`| `0xfffffff`|
|`[3]`|`rxfrmaislrdilen`| Enable/disable the insertion of RDI-L at TOH insertion when AIS_L condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[2]`|`rxfrmtimrdilen`| Enable/disable the insertion of RDI-L at TOH insertion when TIM condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[1]`|`rxfrmlofrdilen`| Enable/disable the insertion of RDI-L at TOH insertion when LOF condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1`|
|`[0]`|`rxfrmlosrdilen`| Enable/disable the insertion of RDI-L at TOH insertion when LOS condition detected. 1: Enable 0: Disable| `RW`| `0x1`| `0x1 End : Begin:`|

###OCN Global Tx Framer Control

* **Description**           

This is the global configuration register for the Tx Framer


* **RTL Instant Name**    : `glbtfm_reg`

* **Address**             : `0x20001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:25]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[24]`|`txfrmoc192enb`| Enable mode OC192 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[23:16]`|`txfrmscren`| Enable/disable scrambling of the Tx data stream. 1: Enable 0: Disable| `RW`| `0xff`| `0xff`|
|`[15:0]`|`txfrmstmocnmode`| STM rate mode for Tx of  8 lines, each line use 2 bits.Bits[1:0] for line 0 0: OC48 1: OC12 2: OC3 3: OC1| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Tx Framer Control

* **Description**           

This is the global configuration register for error forcing to Tx Framer


* **RTL Instant Name**    : `glbtfmfrc_reg`

* **Address**             : `0x2000a`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:8]`|`txlineooffrc`| Enable/disable force OOF      for 8 lines. Bit[0] for line 0.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`txlineb1errfrc`| Enable/disable force B1 error for 8 lines. Bit[0] for line 0.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Tx Framer RDI-L Insertion Threshold Control

* **Description**           

Configure the number of frames in which L-RDI will be inserted when an L-RDI event is triggered. And Pattern to insert into Z0 bytes. The register contains two numbers


* **RTL Instant Name**    : `glbrdiinsthr_reg`

* **Address**             : `0x20009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[28:24]`|`txfrmrdiinsthr2`| Threshold 2 for SDH mode| `RW`| `0x8`| `0x8`|
|`[23:21]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[20:16]`|`txfrmrdiinsthr1`| Threshold 1 for Sonet mode| `RW`| `0x14`| `0x14`|
|`[15:8]`|`txfrmohbusdccenb`| Set 1 to enable insert DCC bytes from OHBUS per 8 lines. Bit[0] for line 0.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`ocntxz0pat`| Pattern 1 to insert into Z0 bytes.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Rx Framer Located Slice Pointer Selection 1

* **Description**           

Configure location in Slice Pointer for Rx lines. This regester config for line 0-3

Slc12Id#0: STSID(0-47):0,1,2,3,16,17,18,19,32,33,34,35. Slc12Id#1: STSID:4,5,6,7,20,21,22,23,36,37,38,39 ...

Slc3Id#0 : STSID(0-11):0,4,8. Slc3Id#1: STSID:1,5,9 ...


* **RTL Instant Name**    : `glbrxslcppsel1_reg`

* **Address**             : `0x20010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[30]`|`rxfrmslcppenb3`| Write 1 to enable for Line 3.| `RW`| `0x0`| `0x0`|
|`[29:24]`|`rxfrmslcppsel3`| Configure location in Slice Pointer for Rx line 3, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0`|
|`[23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22]`|`rxfrmslcppenb2`| Write 1 to enable for Line 2.| `RW`| `0x0`| `0x0`|
|`[21:16]`|`rxfrmslcppsel2`| Configure location in Slice Pointer for Rx line 2, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0`|
|`[15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`rxfrmslcppenb1`| Write 1 to enable for Line 1.| `RW`| `0x0`| `0x0`|
|`[13:8]`|`rxfrmslcppsel1`| Configure location in Slice Pointer for Rx line 1, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`rxfrmslcppenb0`| Write 1 to enable for Line 0.| `RW`| `0x0`| `0x0`|
|`[5:0]`|`rxfrmslcppsel0`| Configure location in Slice Pointer for Rx line 0, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Rx Framer Located Slice Pointer Selection 2

* **Description**           

Configure location in Slice Pointer for Rx lines. This regester config for line 4-7

Slc12Id#0: STSID(0-47):0,1,2,3,16,17,18,19,32,33,34,35. Slc12Id#1: STSID:4,5,6,7,20,21,22,23,36,37,38,39 ...

Slc3Id#0 : STSID(0-11):0,4,8. Slc3Id#1: STSID:1,5,9 ...


* **RTL Instant Name**    : `glbrxslcppsel2_reg`

* **Address**             : `0x20011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[30]`|`rxfrmslcppenb7`| Write 1 to enable for Line 7.| `RW`| `0x0`| `0x0`|
|`[29:24]`|`rxfrmslcppsel7`| Configure location in Slice Pointer for Rx line 7, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0`|
|`[23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22]`|`rxfrmslcppenb6`| Write 1 to enable for Line 6.| `RW`| `0x0`| `0x0`|
|`[21:16]`|`rxfrmslcppsel6`| Configure location in Slice Pointer for Rx line 6, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0`|
|`[15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`rxfrmslcppenb5`| Write 1 to enable for Line 5.| `RW`| `0x0`| `0x0`|
|`[13:8]`|`rxfrmslcppsel5`| Configure location in Slice Pointer for Rx line 5, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`rxfrmslcppenb4`| Write 1 to enable for Line 4.| `RW`| `0x0`| `0x0`|
|`[5:0]`|`rxfrmslcppsel4`| Configure location in Slice Pointer for Rx line 4, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Tx Framer Located Slice Pointer Selection 1

* **Description**           

Configure location in Slice Pointer for Tx lines. This regester config for line 0-3

Slc12Id#0: STSID(0-47):0,1,2,3,16,17,18,19,32,33,34,35. Slc12Id#1: STSID:4,5,6,7,20,21,22,23,36,37,38,39 ...

Slc3Id#0 : STSID(0-11):0,4,8. Slc3Id#1: STSID:1,5,9 ...


* **RTL Instant Name**    : `glbtxslcppsel1_reg`

* **Address**             : `0x20012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[30]`|`txfrmslcppenb3`| Write 1 to enable for Line 3.| `RW`| `0x0`| `0x0`|
|`[29:24]`|`txfrmslcppsel3`| Configure location in Slice Pointer for Tx line 3, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0`|
|`[23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22]`|`txfrmslcppenb2`| Write 1 to enable for Line 2.| `RW`| `0x0`| `0x0`|
|`[21:16]`|`txfrmslcppsel2`| Configure location in Slice Pointer for Tx line 2, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0`|
|`[15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`txfrmslcppenb1`| Write 1 to enable for Line 1.| `RW`| `0x0`| `0x0`|
|`[13:8]`|`txfrmslcppsel1`| Configure location in Slice Pointer for Tx line 1, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`txfrmslcppenb0`| Write 1 to enable for Line 0.| `RW`| `0x0`| `0x0`|
|`[5:0]`|`txfrmslcppsel0`| Configure location in Slice Pointer for Tx line 0, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Tx Framer Located Slice Pointer Selection 2

* **Description**           

Configure location in Slice Pointer for Tx lines. This regester config for line 4-7

Slc12Id#0: STSID(0-47):0,1,2,3,16,17,18,19,32,33,34,35. Slc12Id#1: STSID:4,5,6,7,20,21,22,23,36,37,38,39 ...

Slc3Id#0 : STSID(0-11):0,4,8. Slc3Id#1: STSID:1,5,9 ...


* **RTL Instant Name**    : `glbtxslcppsel2_reg`

* **Address**             : `0x20013`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[30]`|`txfrmslcppenb7`| Write 1 to enable for Line 7.| `RW`| `0x0`| `0x0`|
|`[29:24]`|`txfrmslcppsel7`| Configure location in Slice Pointer for Tx line 7, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0`|
|`[23]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[22]`|`txfrmslcppenb6`| Write 1 to enable for Line 6.| `RW`| `0x0`| `0x0`|
|`[21:16]`|`txfrmslcppsel6`| Configure location in Slice Pointer for Tx line 6, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0`|
|`[15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`txfrmslcppenb5`| Write 1 to enable for Line 5.| `RW`| `0x0`| `0x0`|
|`[13:8]`|`txfrmslcppsel5`| Configure location in Slice Pointer for Tx line 5, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0`|
|`[7]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[6]`|`txfrmslcppenb4`| Write 1 to enable for Line 4.| `RW`| `0x0`| `0x0`|
|`[5:0]`|`txfrmslcppsel4`| Configure location in Slice Pointer for Tx line 4, It includes<br>{Slc48Id[1:0],Slc12Id[1:0],Slc3Id[1:0]}.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global Rx_TOHBUS K Byte Control

* **Description**           

Configure TOHBUS for K Bytes at RX OCN.


* **RTL Instant Name**    : `glbrxtohbusctl_reg`

* **Address**             : `0x20014`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rxtohbuskextctl`| K Byte Configuration for 8 Lines at Rx OCN. Each line use 4 bits[3:0]. Bit[3] :  Unused Bit[2] :  Disable Masking FF to Rx_OHBUS when Section alarm happen at Rx OCN. Set 1 to disable. Bit[1] :  Enable K_extention Byte  at Rx OCN. Set 1 to enable. Bit[0] :  Select K_extention Byte is D1_4 byte or D1_10 byte at Rx OCN. Set 1 to choose D1_4 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Global TOHBUS K_Byte Threshold

* **Description**           

Configure TOHBUS for K_extention Bytes.


* **RTL Instant Name**    : `glbtohbusthr_reg`

* **Address**             : `0x20015`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`rxtohbuskestbthr`| Stable threshold Configuration for K_extention Byte.| `RW`| `0x3`| `0x3`|
|`[7:4]`|`rxtohbusk2stbthr`| Stable threshold Configuration for K2 Byte.| `RW`| `0x3`| `0x3`|
|`[3:0]`|`rxtohbusk1stbthr`| Stable threshold Configuration for K1 Byte.| `RW`| `0x3`| `0x3 End : Begin:`|

###OCN Global Tx_TOHBUS K Byte Control

* **Description**           

Configure TOHBUS for K Bytes at TX OCN.


* **RTL Instant Name**    : `glbtxtohbusctl_reg`

* **Address**             : `0x20016`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`txtohbuskextctl`| K Byte Configuration for 8 Lines at Tx OCN. Each line use 4 bits[3:0]. Bit[3] :  Unused Bit[2] :  Disable propagation suppression RDI,AIS in K2[2:0] at Tx OCN. Default is enable this function - when detect RDI/AIS at K2[2:0] from OHBUS, we must overwrite 3'b000 into K2[2:0]. Set 1 to disable. Bit[1] :  Enable K_extention Byte  at Tx OCN. Set 1 to enable. Bit[0] :  Select K_extention Byte is D1_4 byte or D1_10 byte at Tx OCN. Set 1 to choose D1_4 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN STS Pointer Interpreter Per Channel Control

* **Description**           

Each register is used to configure for STS pointer interpreter engines of the STS-1/VC-3.

# HDL_PATH		: itfi5ho.irxpp_stspp_inst.irxpp_stspp[0].rxpp_stspiramctl.array


* **RTL Instant Name**    : `spiramctl`

* **Address**             : `0x22000 - 0x22e2f`

* **Formula**             : `0x22000 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:21]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[20]`|`linepassthrenb`| Enable/disable Passthrough mode for BLSR at Rx SPI of FacePlate Side 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[19:16]`|`linepassthrgrp`| Passthrough Group ID for BLSR at Rx SPI of FacePlate Side.| `RW`| `0x0`| `0x0`|
|`[15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`linestspichklom`| Enable/disable LOM checking. This field will be set to 1 when payload type of VC3/VC4 includes any VC11/VC12 1: Enable. 0: Disable.| `RW`| `0x0`| `0x0`|
|`[13:12]`|`linestspissdetpatt`| Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction.| `RW`| `0x0`| `0x0`|
|`[11]`|`linestspiaisfrc`| Forcing FSM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[10]`|`linestspissdeten`| Enable/disable checking SS bits in STSPI state machine. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[9]`|`linestspiadjrule`| Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode| `RW`| `0x0`| `0x0`|
|`[8]`|`linestspistsslvind`| This is used to configure STS is slaver or master. 1: Slaver.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`linestspistsmstid`| This is the ID of the master STS-1 in the concatenation that contains this STS-1.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN STS Pointer Generator Per Channel Control

* **Description**           

Each register is used to configure for STS pointer Generator engines.

# HDL_PATH		: itfi5ho.itxpp_stspp_inst.itxpp_stspp[0].txpg_stspgctl.array


* **RTL Instant Name**    : `spgramctl`

* **Address**             : `0x23000 - 0x23e2f`

* **Formula**             : `0x23000 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`linestspgstsslvind`| This is used to configure STS is slaver or master. 1: Slaver.| `RW`| `0x0`| `0x0`|
|`[15:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:8]`|`linestspgstsmstid`| This is the ID of the master STS-1 in the concatenation that contains this STS-1.| `RW`| `0x0`| `0x0`|
|`[7]`|`linestspgb3biperrfrc`| Forcing B3 Bip error. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[6]`|`linestspglopfrc`| Forcing LOP. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[5]`|`linestspgueqfrc`| Forcing FSM to UEQ state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[4]`|`linestspgaisfrc`| Forcing FSM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[3:2]`|`linestspgssinspatt`| Configure pattern SS bits that is used to insert to pointer value.| `RW`| `0x0`| `0x0`|
|`[1]`|`linestspgssinsen`| Enable/disable SS bits insertion. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`linestspgpohins`| Enable/disable POH Insertion. High to enable insertion of POH.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx STS/VC per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of STS/VC pointer interpreter.  %%

Each register is used to store 6 sticky bits for 6 alarms in the STS/VC.


* **RTL Instant Name**    : `upstschstkram`

* **Address**             : `0x22140 - 0x22f6f`

* **Formula**             : `0x22140 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:6]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[5]`|`linestspistsconcdetintr`| Set to 1 while an Concatenation Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[4]`|`linestspistsnewdetintr`| Set to 1 while an New Pointer Detection event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[3]`|`linestspistsndfintr`| Set to 1 while an NDF event is detected at STS/VC pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[2]`|`linestspicepuneqstatepchgintr`| Set to 1  while there is change in CEP Un-equip Path in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in CEP Un-equip Path state or not.| `W1C`| `0x0`| `0x0`|
|`[1]`|`linestspistsaisstatechgintr`| Set to 1 while there is change in AIS state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in AIS state or not.| `W1C`| `0x0`| `0x0`|
|`[0]`|`linestspistslopstatechgintr`| Set 1 to while there is change in LOP state in the related STS/VC to generate an interrupt. Read the OCN Rx STS/VC per Alarm Current Status register of the related STS/VC to know the STS/VC whether in LOP state or not.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx STS/VC per Alarm Current Status

* **Description**           

This is the per Alarm current status of STS/VC pointer interpreter.  %%

Each register is used to store 3 bits to store current status of 3 alarms in the STS/VC.


* **RTL Instant Name**    : `upstschstaram`

* **Address**             : `0x22180 - 0x22faf`

* **Formula**             : `0x22180 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[2]`|`linestspistscepuneqpcurstatus`| CEP Un-eqip Path current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the LineStsPiStsCepUeqPStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.| `RO`| `0x0`| `0x0`|
|`[1]`|`linestspistsaiscurstatus`| AIS current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  LineStsPiStsAISStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.| `RO`| `0x0`| `0x0`|
|`[0]`|`linestspistslopcurstatus`| LOP current status in the related STS/VC. When it changes for 0 to 1 or vice versa, the  LineStsPiStsLopStateChgIntr bit in the OCN Rx STS/VC per Alarm Interrupt Status register of the related STS/VC is set.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx PP STS/VC Pointer interpreter pointer adjustment per channel counter

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS/VC pointer interpreter. These counters are in saturation mode


* **RTL Instant Name**    : `adjcntperstsram`

* **Address**             : `0x22080 - 0x22eaf`

* **Formula**             : `0x22080 + 512*LineId + 64*AdjMode + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$AdjMode(0-1)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`linerxppstspiptadjcnt`| The pointer Increment counter or decrease counter. Bit [6] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg STS per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of STS pointer generator . Each register is used to store 3 sticky bits for 3 alarms


* **RTL Instant Name**    : `stspgstkram`

* **Address**             : `0x23140 - 0x23f6f`

* **Formula**             : `0x23140 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[3]`|`linestspgaisintr`| Set to 1 while an AIS status event (at h2pos) is detected at Tx STS Pointer Generator.| `W1C`| `0x0`| `0x0										// Field: [2]`|
|`[1]`|`linestspgndfintr`| Set to 1 while an NDF status event (at h2pos) is detected at Tx STS Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[0]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg STS pointer adjustment per channel counter

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in STS pointer generator. These counters are in saturation mode


* **RTL Instant Name**    : `adjcntpgperstsram`

* **Address**             : `0x23080 - 0x23eaf`

* **Formula**             : `0x23080 + 512*LineId + 64*AdjMode + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$AdjMode(0-1)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`linestspgptadjcnt`| The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN Rx STS/VC Automatically Concatenation Detection

* **Description**           

This is the Automatically Concatenation Detection Status at Rx Pointer Interpreter


* **RTL Instant Name**    : `linerxstsconcdetreg`

* **Address**             : `0x22040 - 0x22e6f`

* **Formula**             : `0x22040 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `WO`| `0x0`| `0x0`|
|`[11]`|`linespicondetvc416c`| This is the indicator that this STS is member of 1 VC4_16C, which is automatically detected.| `RO`| `0x0`| `0x0`|
|`[10]`|`linespicondetvc44c`| This is the indicator that this STS is member of 1 VC4_4C, which is automatically detected.| `RO`| `0x0`| `0x0`|
|`[9]`|`linespicondetvc4`| This is the indicator that this STS is member of 1 VC4, which is automatically detected.| `RO`| `0x0`| `0x0`|
|`[8]`|`linespicondetslvind`| This is the indicator that this STS is slaver or master which is automatically detected. 1: Slaver.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[5:0]`|`linespicondetmstid`| This is the ID of the master STS-1 in the concatenation that contains this STS-1 which is automatically detected.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Tx STS/VC Automatically Concatenation Detection

* **Description**           

This is the Automatically Concatenation Detection Status at Tx Pointer Generator based on Concatenation indicator from SXC.


* **RTL Instant Name**    : `linetxstsconcdetreg`

* **Address**             : `0x23040 - 0x23e6f`

* **Formula**             : `0x23040 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `WO`| `0x0`| `0x0`|
|`[11]`|`linespgcondetvc416c`| This is the indicator that this STS is member of 1 VC4_16C, which is automatically detected.| `RO`| `0x0`| `0x0`|
|`[10]`|`linespgcondetvc44c`| This is the indicator that this STS is member of 1 VC4_4C, which is automatically detected.| `RO`| `0x0`| `0x0`|
|`[9]`|`linespgcondetvc4`| This is the indicator that this STS is member of 1 VC4, which is automatically detected.| `RO`| `0x0`| `0x0`|
|`[8]`|`linespgcondetslvind`| This is the indicator that this STS is slaver or master which is automatically detected. 1: Slaver.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[5:0]`|`linespgcondetmstid`| This is the ID of the master STS-1 in the concatenation that contains this STS-1 which is automatically detected.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Tx Framer Per Line Control

* **Description**           

Each register is used to configure operation modes for Tx framer engine and APS pattern inserted into APS bytes (K1 and K2 bytes) of the SONET/SDH line being relative with the address of the address of the register.


* **RTL Instant Name**    : `tfmregctl`

* **Address**             : `0x21000 - 0x21700`

* **Formula**             : `0x21000 + 256*LineId`

* **Where**               : 

    * `$LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`ocntxs1pat`| Pattern to insert into S1 byte.| `RW`| `0x0`| `0x0`|
|`[23:8]`|`ocntxapspat`| Pattern to insert into APS bytes (K1, K2).| `RW`| `0x0`| `0x0`|
|`[7]`|`ocntxs1ldis`| S1 insertion disable 1: Disable 0: Enable.| `RW`| `0x0`| `0x0`|
|`[6]`|`ocntxapsdis`| Disable to process APS 1: Disable 0: Enable.| `RW`| `0x0`| `0x0`|
|`[5]`|`ocntxreildis`| Auto REI_L insertion disable 1: Disable inserting REI_L automatically. 0: Enable automatically inserting REI_L.| `RW`| `0x0`| `0x0`|
|`[4]`|`ocntxrdildis`| Auto RDI_L insertion disable 1: Disable inserting RDI_L automatically. 0: Enable automatically inserting RDI_L.| `RW`| `0x0`| `0x0`|
|`[3]`|`ocntxautob2dis`| Auto B2 disable 1: Disable inserting calculated B2 values into B2 positions automatically. 0: Enable automatically inserting calculated B2 values into B2 positions.| `RW`| `0x0`| `0x0`|
|`[2]`|`ocntxrdilthressel`| Select the number of frames being inserted RDI-L defects when receive direction requests generating RDI-L at transmit direction. 1: Threshold2 is selected. 0: Threshold1 is selected.| `RW`| `0x0`| `0x0`|
|`[1]`|`ocntxrdilfrc`| RDI-L force. 1: Force RDI-L defect into transmit data. 0: Not force RDI-L| `RW`| `0x0`| `0x0`|
|`[0]`|`ocntxaislfrc`| AIS-L force. 1: Force AIS-L defect into transmit data. 0: Not force AIS-L| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Tx Framer Per Line Control 1

* **Description**           

Each register is used to configure some RS-OH bytes.


* **RTL Instant Name**    : `tfmregctl1`

* **Address**             : `0x21001 - 0x21701`

* **Formula**             : `0x21001 + 256*LineId`

* **Where**               : 

    * `$LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`ocntxrsohundefpat`| Pattern to insert into undefined RS-OH bytes.| `RW`| `0x0`| `0x0`|
|`[23:16]`|`ocntxrsohf1pat`| Pattern to insert into F1 byte.| `RW`| `0x0`| `0x0`|
|`[15:8]`|`ocntxrsohe1pat`| Pattern to insert into E1 byte.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`ocntxrsohz0pat`| Pattern to insert into Z0 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Tx Framer Per Line Control 2

* **Description**           

Each register is used to configure some MS-OH bytes.


* **RTL Instant Name**    : `tfmregctl2`

* **Address**             : `0x21002 - 0x21702`

* **Formula**             : `0x21002 + 256*LineId`

* **Where**               : 

    * `$LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`ocntxmsohundefpat`| Pattern to insert into undefined  MS-OH bytes.| `RW`| `0x0`| `0x0`|
|`[23:16]`|`ocntxmsohe2pat`| Pattern to insert into E2 byte.| `RW`| `0x0`| `0x0`|
|`[15:8]`|`ocntxmsohz2pat`| Pattern to insert into Z2 byte.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`ocntxmsohz1pat`| Pattern to insert into Z1 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Tx J0 Insertion Buffer

* **Description**           

Each register is used to store one J0 double word of in 64-byte message of the SONET/SDH line being relative with the address of the address of the register inserted into J0 positions.


* **RTL Instant Name**    : `tfmj0insctl`

* **Address**             : `0x21010 - 0x2171f`

* **Formula**             : `0x21010 + 256*LineId + DwId`

* **Where**               : 

    * `$LineId(0-7)`

    * `DwId(0-15): Double word (4bytes) ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`ocntxj0`| J0 pattern for insertion.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Global K1 Stable Monitoring Threshold Control

* **Description**           

Configure thresholds for monitoring change of K1 state at TOH monitoring. There are two thresholds for each kind of threshold.


* **RTL Instant Name**    : `tohglbk1stbthr_reg`

* **Address**             : `0x27000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10:8]`|`tohk1stbthr2`| The second threshold for detecting stable K1 status.| `RW`| `0x3`| `0x3`|
|`[7:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2:0]`|`tohk1stbthr1`| The first threshold for detecting stable K1 status.| `RW`| `0x4`| `0x4 End : Begin:`|

###OCN TOH Global K2 Stable Monitoring Threshold Control

* **Description**           

Configure thresholds for monitoring change of K2 state at TOH monitoring. There are two thresholds for each kind of threshold.


* **RTL Instant Name**    : `tohglbk2stbthr_reg`

* **Address**             : `0x27001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[10:8]`|`tohk2stbthr2`| The second threshold for detecting stable K2 status.| `RW`| `0x3`| `0x3`|
|`[7:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2:0]`|`tohk2stbthr1`| The first threshold for detecting stable K2 status.| `RW`| `0x4`| `0x4 End : Begin:`|

###OCN TOH Global S1 Stable Monitoring Threshold Control

* **Description**           

Configure thresholds for monitoring change of S1 state at TOH monitoring. There are two thresholds for each kind of threshold.


* **RTL Instant Name**    : `tohglbs1stbthr_reg`

* **Address**             : `0x27002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`tohs1stbthr2`| The second threshold for detecting stable S1 status.| `RW`| `0x5`| `0x5`|
|`[7:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3:0]`|`tohs1stbthr1`| The first threshold for detecting stable S1 status.| `RW`| `0x5`| `0x5 End : Begin:`|

###OCN TOH Global RDI_L Detecting Threshold Control

* **Description**           

Configure thresholds for detecting RDI_L at TOH monitoring. There are two thresholds for each kind of threshold.


* **RTL Instant Name**    : `tohglbrdidetthr_reg`

* **Address**             : `0x27003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`tohrdidetthr2`| The second threshold for detecting RDI_L.| `RW`| `0x4`| `0x4`|
|`[7:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3:0]`|`tohrdidetthr1`| The first threshold for detecting RDI_L.| `RW`| `0x9`| `0x9 End : Begin:`|

###OCN TOH Global AIS_L Detecting Threshold Control

* **Description**           

Configure thresholds for detecting AIS_L at TOH monitoring. There are two thresholds for each kind of threshold.


* **RTL Instant Name**    : `tohglbaisdetthr_reg`

* **Address**             : `0x27004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`tohaisdetthr2`| The second threshold for detecting AIS_L.| `RW`| `0x3`| `0x3`|
|`[7:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3:0]`|`tohaisdetthr1`| The first threshold for detecting AIS_L.| `RW`| `0x4`| `0x4 End : Begin:`|

###OCN TOH Global K1 Sampling Threshold Control

* **Description**           

Configure thresholds for sampling K1 bytes to detect APS defect at TOH monitoring. There are two thresholds.


* **RTL Instant Name**    : `tohglbk1smpthr_reg`

* **Address**             : `0x27005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`tohk1smpthr2`| The second threshold for sampling K1 to detect APS defect.| `RW`| `0x7`| `0x7`|
|`[7:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3:0]`|`tohk1smpthr1`| The first threshold for sampling K1 to detect APS defect.| `RW`| `0x7`| `0x7 End : Begin:`|

###OCN TOH Global Error Counter Control

* **Description**           

Configure mode for counters in TOH monitoring.


* **RTL Instant Name**    : `tohglberrcntmod_reg`

* **Address**             : `0x27006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`tohreierrcntmod`| REI counter Mode. 1: Saturation mode 0: Roll over mode| `RW`| `0x1`| `0x1`|
|`[1]`|`tohb2errcntmod`| B2 counter Mode. 1: Saturation mode 0: Roll over mode| `RW`| `0x1`| `0x1`|
|`[0]`|`tohb1errcntmod`| B1 counter Mode. 1: Saturation mode 0: Roll over mode| `RW`| `0x1`| `0x1 End : Begin:`|

###OCN TOH Montoring Affect Control

* **Description**           

Configure affective mode for TOH monitoring.


* **RTL Instant Name**    : `tohglbaffen_reg`

* **Address**             : `0x27007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3]`|`tohaisaffstbmon`| AIS affects to Stable monitoring status of the line at which LOF or LOS is detected 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[2]`|`tohaisaffrdilmon`| AIS affects to RDI-L monitoring status of the line at which LOF or LOS is detected. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[1]`|`tohaisaffaislmon`| AIS affects to AIS-L monitoring  status of the line at which LOF or LOS is detected. 1: Disable 0: Enable| `RW`| `0x0`| `0x0`|
|`[0]`|`tohaisafferrcnt`| AIS affects to error counters (B1,B2,REI) of the line at which LOF or LOS is detected. 1: Disable 0: Enable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring Per Line Control

* **Description**           

Each register is used to configure for TOH monitoring engine of the related line.


* **RTL Instant Name**    : `tohramctl`

* **Address**             : `0x27010 - 0x27017`

* **Formula**             : `0x27010  + LineId`

* **Where**               : 

    * `$LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`rxaislfrc`| Rx AIS-L force. 1: Force. 0: Not force| `RW`| `0x0`| `0x0`|
|`[8]`|`b1errcntblkmod`| B1 counter Block Mode for fedding PM. 1: Block count mode 0: Bit count mode| `RW`| `0x0`| `0x0`|
|`[7]`|`b2errcntblkmod`| B2 counter Block Mode for fedding PM. 1: Block count mode 0: Bit count mode| `RW`| `0x0`| `0x0`|
|`[6]`|`reierrcntblkmod`| REI counter Block Mode for fedding PM. 1: Block count mode 0: Bit count mode| `RW`| `0x0`| `0x0`|
|`[5]`|`stbrdiislthressel`| Select the thresholds for detecting RDI_L 1: Threshold 2 is selected 0: Threshold 1 is selected| `RW`| `0x0`| `0x0`|
|`[4]`|`stbaislthressel`| Select the thresholds for detecting AIS_L 1: Threshold 2 is selected 0: Threshold 1 is selected| `RW`| `0x0`| `0x0`|
|`[3]`|`k2stbmd`| Select K2[7:3] or K2[7:0] to detect validated K2 value 1: K2[7:0] value is selected 0: K2[7:3] value is selected| `RW`| `0x0`| `0x0`|
|`[2]`|`stbk1k2thressel`| Select the thresholds for detecting stable or non-stable K1/K2 status 1: Threshold 2 is selected 0: Threshold 1 is selected| `RW`| `0x0`| `0x0`|
|`[1]`|`stbs1thressel`| Select the thresholds for detecting stable or non-stable S1 status 1: Threshold 2 is selected 0: Threshold 1 is selected| `RW`| `0x0`| `0x0`|
|`[0]`|`smpk1thressel`| Select the sample threshold for detecting APS defect. 1: Threshold 2 is selected 0: Threshold 1 is selected| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B1 Error Read Only Counter

* **Description**           

Each register is used to store B1 error read only counter of the related line.


* **RTL Instant Name**    : `tohb1errrocnt`

* **Address**             : `0x27100 - 0x27107`

* **Formula**             : `0x27100 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[22:0]`|`b1errrocnt`| B1 Error Read Only Counter.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B1 Error Read to Clear Counter

* **Description**           

Each register is used to store B1 error read to clear counter of the related line.


* **RTL Instant Name**    : `tohb1errr2ccnt`

* **Address**             : `0x27140 - 0x27147`

* **Formula**             : `0x27140 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[22:0]`|`b1errr2ccnt`| B1 Error Read To Clear Counter.| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B1 Block Error Read Only Counter

* **Description**           

Each register is used to store B1 Block error read only counter of the related line.


* **RTL Instant Name**    : `tohb1blkerrrocnt`

* **Address**             : `0x27180 - 0x27187`

* **Formula**             : `0x27180 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[15:0]`|`b1blkerrrocnt`| B1 Block Error Read Only Counter.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B1 Block Error Read to Clear Counter

* **Description**           

Each register is used to store B1 Block error read to clear counter of the related line.


* **RTL Instant Name**    : `tohb1blkerrr2ccnt`

* **Address**             : `0x271c0 - 0x271c7`

* **Formula**             : `0x271c0 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[15:0]`|`b1blkerrr2ccnt`| B1 Block Error Read To Clear Counter.| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B2 Error Read Only Counter

* **Description**           

Each register is used to store B2 error read only counter of the related line.


* **RTL Instant Name**    : `tohb2errrocnt`

* **Address**             : `0x27108 - 0x2710f`

* **Formula**             : `0x27108 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[22:0]`|`b2errrocnt`| B2 Error Read Only Counter.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B2 Error Read to Clear Counter

* **Description**           

Each register is used to store B2 error read to clear counter of the related line.


* **RTL Instant Name**    : `tohb2errr2ccnt`

* **Address**             : `0x27148 - 0x2714f`

* **Formula**             : `0x27148 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[22:0]`|`b2errr2ccnt`| B2 Error Read To Clear Counter.| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B2 Block Error Read Only Counter

* **Description**           

Each register is used to store B2 Block error read only counter of the related line.


* **RTL Instant Name**    : `tohb2blkerrrocnt`

* **Address**             : `0x27188 - 0x2718f`

* **Formula**             : `0x27188 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[15:0]`|`b2blkerrrocnt`| B2 Block Error Read Only Counter.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring B2 Block Error Read to Clear Counter

* **Description**           

Each register is used to store B2 Block error read to clear counter of the related line.


* **RTL Instant Name**    : `tohb2blkerrr2ccnt`

* **Address**             : `0x271c8 - 0x271cf`

* **Formula**             : `0x271c8 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[15:0]`|`b2blkerrr2ccnt`| B2 Block Error Read To Clear Counter.| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring REI Error Read Only Counter

* **Description**           

Each register is used to store REI error read only counter of the related line.


* **RTL Instant Name**    : `tohreierrrocnt`

* **Address**             : `0x27128 - 0x2712f`

* **Formula**             : `0x27128 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[22:0]`|`reierrrocnt`| REI Error Read Only Counter.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring REI Error Read to Clear Counter

* **Description**           

Each register is used to store REI error read to clear counter of the related line.


* **RTL Instant Name**    : `tohreierrr2ccnt`

* **Address**             : `0x27168 - 0x2716f`

* **Formula**             : `0x27168 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:23]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[22:0]`|`reierrr2ccnt`| REI Error Read To Clear Counter.| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring REI Block Error Read Only Counter

* **Description**           

Each register is used to store REI Block error read only counter of the related line.


* **RTL Instant Name**    : `tohreiblkerrrocnt`

* **Address**             : `0x271a8 - 0x271af`

* **Formula**             : `0x271a8 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[15:0]`|`reiblkerrrocnt`| REI Block Error Read Only Counter.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN TOH Monitoring REI Block Error Read to Clear Counter

* **Description**           

Each register is used to store REI Block error read to clear counter of the related line.


* **RTL Instant Name**    : `tohreiblkerrr2ccnt`

* **Address**             : `0x271e8 - 0x270ef`

* **Formula**             : `0x271e8 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[15:0]`|`reiblkerrr2ccnt`| REI Block Error Read To Clear Counter.| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TOH K1 Monitoring Status

* **Description**           

Each register is used to store K1 Monitoring Status of the related line.


* **RTL Instant Name**    : `tohk1monsta`

* **Address**             : `0x27110 - 0x27117`

* **Formula**             : `0x27110 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23]`|`curapsdef`| current APS Defect. 1: APS defect is detected from APS bytes. 0: APS defect is not detected from APS bytes.| `RW`| `0x0`| `0x0`|
|`[22:19]`|`k1smpcnt`| Sampling counter.| `RW`| `0x0`| `0x0`|
|`[18:16]`|`samek1cnt`| The number of same contiguous K1 bytes. It is held at StbK1Thr value when the number of same contiguous K1 bytes is equal to or more than the StbK1Thr value.In this case, K1 bytes are stable.| `RW`| `0x0`| `0x0`|
|`[15:8]`|`k1stbval`| Stable K1 value. It is updated when detecting a new stable value.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`k1curval`| Current K1 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN TOH K2 Monitoring Status

* **Description**           

Each register is used to store K2 Monitoring Status of the related line.


* **RTL Instant Name**    : `tohk2monsta`

* **Address**             : `0x27118 - 0x2711f`

* **Formula**             : `0x27118 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[28:21]`|`internal`| Internal.| `RW`| `0x0`| `0x0`|
|`[20]`|`curaisl`| current AIS-L Defect. 1: AIS-L defect is detected from K2 bytes. 0: AIS-L defect is not detected from K2 bytes.| `RW`| `0x0`| `0x0`|
|`[19]`|`currdil`| current RDI-L Defect. 1: RDI-L defect is detected from K2 bytes. 0: RDI-L defect is not detected from K2 bytes.| `RW`| `0x0`| `0x0`|
|`[18:16]`|`samek2cnt`| The number of same contiguous K2 bytes. It is held at StbK2Thr value when the number of same contiguous K2 bytes is equal to or more than the StbK2Thr value.In this case, K2 bytes are stable.| `RW`| `0x0`| `0x0`|
|`[15:8]`|`k2stbval`| Stable K2 value. It is updated when detecting a new stable value.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`k2curval`| Current K2 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN TOH S1 Monitoring Status

* **Description**           

Each register is used to store S1 Monitoring Status of the related line.


* **RTL Instant Name**    : `tohs1monsta`

* **Address**             : `0x27120 - 0x27127`

* **Formula**             : `0x27120 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[19:16]`|`sames1cnt`| The number of same contiguous S1 bytes. It is held at StbS1Thr value when the number of same contiguous S1 bytes is equal to or more than the StbS1Thr value.In this case, S1 bytes are stable.| `RW`| `0x0`| `0x0`|
|`[15:8]`|`s1stbval`| Stable S1 value. It is updated when detecting a new stable value.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`s1curval`| Current S1 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line per Alarm Interrupt Enable Control

* **Description**           

This is the per Alarm interrupt enable of Rx framer and TOH monitoring. Each register is used to store 9 bits to enable interrupts when the related alarms in related line happen.


* **RTL Instant Name**    : `tohintperalrenbctl`

* **Address**             : `0x27080 - 0x27087`

* **Formula**             : `0x27080 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[12]`|`tcalstatechgintren`| Set 1 to enable TCA-L state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[11]`|`sdlstatechgintren`| Set 1 to enable SD-L state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[10]`|`sflstatechgintren`| Set 1 to enable SF-L state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[9]`|`timlstatechgintren`| Set 1 to enable TIM-L state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[8]`|`s1stbstatechgintren`| Set 1 to enable S1 Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[7]`|`k1stbstatechgintren`| Set 1 to enable K1 Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[6]`|`apslstatechgintren`| Set 1 to enable APS-L Defect Stable state change event in the related line to generate an interrupt..| `RW`| `0x0`| `0x0`|
|`[5]`|`k2stbstatechgintren`| Set 1 to enable K2 Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[4]`|`rdilstatechgintren`| Set 1 to enable RDI-L Defect Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[3]`|`aislstatechgintren`| Set 1 to enable AIS-L Defect Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[2]`|`oofstatechgintren`| Set 1 to enable OOF Defect Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[1]`|`lofstatechgintren`| Set 1 to enable LOF Defect Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[0]`|`losstatechgintren`| Set 1 to enable LOS Defect Stable state change event in the related line to generate an interrupt.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line.


* **RTL Instant Name**    : `tohintsta`

* **Address**             : `0x27088 - 0x2708f`

* **Formula**             : `0x27088 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[12]`|`tcalstatechgintr`| Set 1 while TCA-L state change detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[11]`|`sdlstatechgintr`| Set 1 while SD-L state change detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[10]`|`sflstatechgintr`| Set 1 while SF-L state change detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[9]`|`timlstatechgintr`| Set 1 while Tim-L state change detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[8]`|`s1stbstatechgintr`| Set 1 one new stable S1 value detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[7]`|`k1stbstatechgintr`| Set 1 one new stable K1 value detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[6]`|`apslstatechgintr`| Set 1 while APS-L Defect state change event happens in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[5]`|`k2stbstatechgintr`| Set 1 one new stable K2 value detected in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[4]`|`rdilstatechgintr`| Set 1 while RDI-L Defect state change event happens in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[3]`|`aislstatechgintr`| Set 1 while AIS-L Defect state change event happens in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[2]`|`oofstatechgintr`| Set 1 while OOF state change event happens in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[1]`|`lofstatechgintr`| Set 1 while LOF state change event happens in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0`|
|`[0]`|`losstatechgintr`| Set 1 while LOS state change event happens in the related line, and it is generated an interrupt if it is enabled.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line per Alarm Current Status

* **Description**           

This is the per Alarm interrupt status of Rx framer and TOH monitoring. Each register is used to store 9 sticky bits for 9 alarms in the line.


* **RTL Instant Name**    : `tohcursta`

* **Address**             : `0x27090 - 0x27097`

* **Formula**             : `0x27090 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:13]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[12]`|`tcaldefcurstatus`| TCA-L Defect  in the related line. 1: TCA-L defect is set 0: TCA-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[11]`|`sdldefcurstatus`| SD-L Defect  in the related line. 1: SD-L defect is set 0: SD-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[10]`|`sfldefcurstatus`| SF-L Defect  in the related line. 1: SF-L defect is set 0: SF-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[9]`|`timldefcurstatus`| TIM-L Defect  in the related line. 1: TIM-L defect is set 0: TIM-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[8]`|`s1stbstatechgintr`| S1 stable status  in the related line.| `RW`| `0x0`| `0x0`|
|`[7]`|`k1stbcurstatus`| K1 stable status  in the related line.| `RW`| `0x0`| `0x0`|
|`[6]`|`apslcurstatus`| APS-L Defect  in the related line. 1: APS-L defect is set 0: APS-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[5]`|`k2stbcurstatus`| K2 stable status  in the related line.| `RW`| `0x0`| `0x0`|
|`[4]`|`rdildefcurstatus`| RDI-L Defect  in the related line. 1: RDI-L defect is set 0: RDI-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[3]`|`aisldefcurstatus`| AIS-L Defect  in the related line. 1: AIS-L defect is set 0: AIS-L defect is cleared.| `RW`| `0x0`| `0x0`|
|`[2]`|`oofcurstatus`| OOF current status in the related line. 1: OOF state 0: Not OOF state.| `RW`| `0x0`| `0x0`|
|`[1]`|`lofcurstatus`| LOF current status in the related line. 1: LOF state 0: Not LOF state.| `RW`| `0x0`| `0x0`|
|`[0]`|`loscurstatus`| LOS current status in the related line. 1: LOS state 0: Not LOS state.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line Per Line Interrupt Enable Control

* **Description**           

The register consists of 4 or 8 bits per lines (STM1: 8bits,STM4: 4bits) at Rx side to enable interrupts when alarms of related lines happen. It is noted that this register is higher priority than the OCN Rx Line per Alarm Interrupt Enable Control register.


* **RTL Instant Name**    : `tohintperlineenctl`

* **Address**             : `0x2709e`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`ocnrxllineintren1`| Bit #0 to enable for line #0.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line per Line Interrupt OR Status

* **Description**           

The register consists of 4 or 8 bits per lines (STM1: 8bits,STM4: 4bits) at Rx side. Each bit is used to store Interrupt OR status of the related line. If there are any bits in this register and they are enabled to raise interrupt, the Rx line level interrupt bit is set.


* **RTL Instant Name**    : `tohintperline`

* **Address**             : `0x2709f`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`ocnrxllineintr1`| Set to 1 to indicate that there is any interrupt status bit in the OCN Rx Line per Alarm Interrupt Status register of the related STS/VC to be set and they are enabled to raise interrupt. Bit 0 for line #0, respectively.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Framer Z1 Z2 STS Select Control

* **Description**           

Each register is used to configure some RS-OH bytes.


* **RTL Instant Name**    : `rfmz1z2selctl`

* **Address**             : `0x21003 - 0x21703`

* **Formula**             : `0x21003 + 256*LineId`

* **Where**               : 

    * `$LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:8]`|`ocnrxz1monstssel`| Select only Z1 position to monitor. STS#0-STS47| `RW`| `0x1`| `0x1`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`ocnrxz2monstssel`| Select only Z2 position to monitor. STS#0-STS47| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line Per Line E1 captured byte

* **Description**           

The register consists value of E1 byte


* **RTL Instant Name**    : `tohe1byte`

* **Address**             : `0x27200 - 0x27207`

* **Formula**             : `0x27200 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`ocnrxe1capbyte`| E1 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line Per Line F1 captured byte

* **Description**           

The register consists value of F1 byte


* **RTL Instant Name**    : `tohf1byte`

* **Address**             : `0x27208 - 0x2720f`

* **Formula**             : `0x27208 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`ocnrxf1capbyte`| F1 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line Per Line Z1 captured byte

* **Description**           

The register consists value of Z1 byte


* **RTL Instant Name**    : `tohz1byte`

* **Address**             : `0x27210 - 0x27217`

* **Formula**             : `0x27210 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`ocnrxz1capbyte`| Z1 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line Per Line Z2 captured byte

* **Description**           

The register consists value of Z2 byte


* **RTL Instant Name**    : `tohz2byte`

* **Address**             : `0x27218 - 0x2721f`

* **Formula**             : `0x27218 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`ocnrxz2capbyte`| Z2 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx Line Per Line E2 captured byte

* **Description**           

The register consists value of E2 byte


* **RTL Instant Name**    : `tohe2byte`

* **Address**             : `0x27220 - 0x27227`

* **Formula**             : `0x27220 + LineId`

* **Where**               : 

    * `LineId(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`ocnrxe2capbyte`| E2 byte.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN SXC Control 1 - Config Page 0 and Page 1 of SXC

* **Description**           

Each register is used for each outgoing STS of any line (12 lines: 0-3:TFI5 side, 4-7: Line Side, 8-11: Loho) can be randomly configured to //connect  to any ingoing STS of any ingoing line (12 lines: 0-3:TFI5 side, 4-7: Line Side, 8-11: Loho). These registers are used for config Pape0 and page1 of SXC.

# HDL_PATH		: isdhsxc.isxc_rxrd[0].rxsxcramctl0.array


* **RTL Instant Name**    : `sxcramctl0`

* **Address**             : `0x44000 - 0x44b2f`

* **Formula**             : `0x44000 + 256*LineId + StsId`

* **Where**               : 

    * `$LineId(0-11)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[27:24]`|`sxclineidpage1`| Contains the ingoing LineID Page1 (0-11: 0-3:TFI5 side, 4-7: Line Side, 8-11: Loho). Disconnect (output is all one) if value LineID is other value (recommend is 14).| `RW`| `0x0`| `0x0`|
|`[23:22]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[21:16]`|`sxcstsidpage1`| Contains the ingoing STSID Page1 (0-47).| `RW`| `0x0`| `0x0`|
|`[15:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`sxclineidpage0`| Contains the ingoing LineID Page0 (0-11: 0-3:TFI5 side, 4-7: Line Side, 8-11: Loho). Disconnect (output is all one) if value LineID is other value (recommend is 14).| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`sxcstsidpage0`| Contains the ingoing STSID Page0 (0-47).| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN SXC Control 2 - Config Page 2 of SXC

* **Description**           

Each register is used for each outgoing STS of any line (12 lines: 0-3:TFI5 side, 4-7: Line Side, 8-11: Loho) can be randomly configured to //connect  to any ingoing STS of any ingoing line (12 lines: 0-3:TFI5 side, 4-7: Line Side, 8-11: Loho). These registers are used for config Pape2 of SXC.

# HDL_PATH		: isdhsxc.isxc_rxrd[0].rxsxcramctl1.array


* **RTL Instant Name**    : `sxcramctl1`

* **Address**             : `0x44040 - 0x44b6f`

* **Formula**             : `0x44040 + 256*LineId + StsId`

* **Where**               : 

    * `$LineId(0-11)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:8]`|`sxclineidpage2`| Contains the ingoing LineID Page2 (0-11: 0-3:TFI5 side, 4-7: Line Side, 8-11: Loho). Disconnect (output is all one) if value LineID is other value (recommend is 14).| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`sxcstsidpage2`| Contains the ingoing STSID Page2 (0-47).| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN SXC Control 3 - Config APS

* **Description**           

Each register is used for APS configuration

# HDL_PATH		: isdhsxc.isxc_rxrd[0].apsctl_ram.array


* **RTL Instant Name**    : `apsramctl`

* **Address**             : `0x44080 - 0x44baf`

* **Formula**             : `0x44080 + 256*LineId + StsId`

* **Where**               : 

    * `$LineId(0-11)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13:12]`|`selectortype`| Selector Type. 3: FSM. Choose based FSM input 2: APS Grouping. 1: UPSR/SNCP Grouping. 0: Disable APS/UPSR.| `RW`| `0x0`| `0x0`|
|`[11:9]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[8:0]`|`selectorid`| Contains Selector ID. When Selector Type is: - FSM: Unused this field. - APS: [8]  : Passthrough Enable for BLSR [7:4]: Passthrough Group ID for BLSR [3:0]: APS Group ID - UPSR/SNCP: Select UPSR ID.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx High Order Map concatenate configuration

* **Description**           

Each register is used to configure concatenation for each STS to High Order Map path.

# HDL_PATH		: irxpp_outho_inst.irxpp_outputho [0].ohoramctl.array


* **RTL Instant Name**    : `rxhomapramctl`

* **Address**             : `0x48000 - 0x4872f`

* **Formula**             : `0x48000 + 512*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:9]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[8]`|`homapstsslvind`| This is used to configure STS is slaver or master. 1: Slaver.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:0]`|`homapstsmstid`| This is the ID of the master STS-1 in the concatenation that contains this STS-1.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN RXPP Per STS payload Control

* **Description**           

Each register is used to configure VT payload mode per STS.

# HDL_PATH		: itfi5ho.irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_demramctl.array


* **RTL Instant Name**    : `demramctl`

* **Address**             : `0x60000 - 0x7402f`

* **Formula**             : `0x60000 + 16384*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`pidemststerm`| Enable to terminate the related STS/VC. It means that STS POH defects related to the STS/VC to generate AIS to downstream. Must be set to 1. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[15:14]`|`pidemspetype`| Configure types of SPE. 0: Disable processing pointers of all VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2: TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.| `RW`| `0x0`| `0x0`|
|`[13:12]`|`pidemtug26type`| Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12| `RW`| `0x0`| `0x0`|
|`[11:10]`|`pidemtug25type`| Configure types of VT/TUs in TUG-2 #5.| `RW`| `0x0`| `0x0`|
|`[9:8]`|`pidemtug24type`| Configure types of VT/TUs in TUG-2 #4.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`pidemtug23type`| Configure types of VT/TUs in TUG-2 #3.| `RW`| `0x0`| `0x0`|
|`[5:4]`|`pidemtug22type`| Configure types of VT/TUs in TUG-2 #2.| `RW`| `0x0`| `0x0`|
|`[3:2]`|`pidemtug21type`| Configure types of VT/TUs in TUG-2 #1.| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pidemtug20type`| Configure types of VT/TUs in TUG-2 #0.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN VTTU Pointer Interpreter Per Channel Control

* **Description**           

Each register is used to configure for VTTU pointer interpreter engines.

# HDL_PATH		: itfi5ho.irxpp_vtpp_inst.irxpp_vtpp[0].rxpp_vpiramctl.array


* **RTL Instant Name**    : `vpiramctl`

* **Address**             : `0x60800 - 0x74fff`

* **Formula**             : `0x60800 + 16384*LineId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5]`|`linevtpiloterm`| Enable to terminate the related VTTU. It means that VTTU POH defects related to the VTTU to generate AIS to downstream.| `RW`| `0x0`| `0x0`|
|`[4]`|`linevtpiaisfrc`| Forcing FSM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[3:2]`|`linevtpissdetpatt`| Configure pattern SS bits that is used to compare with the extracted SS bits from receive direction.| `RW`| `0x0`| `0x0`|
|`[1]`|`linevtpissdeten`| Enable/disable checking SS bits in PI State Machine. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`linevtpiadjrule`| Configure the rule for detecting adjustment condition. 1: The n of 5 rule is selected. This mode is applied for SDH mode 0: The 8 of 10 rule is selected. This mode is applied for SONET mode| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN TXPP Per STS Multiplexing Control

* **Description**           

Each register is used to configure VT payload mode per STS at Tx pointer generator.

# HDL_PATH		: itfi5ho.itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgdmctl.array


* **RTL Instant Name**    : `pgdemramctl`

* **Address**             : `0x80000 - 0x9402f`

* **Formula**             : `0x80000 + 16384*LineId + StsId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:14]`|`pgdemspetype`| Configure types of SPE. 0: Disable processing pointers of all VT/TUs in this SPE. 1: VC-3 type or STS SPE containing VT/TU exception TU-3 2: TUG-3 type containing VT/TU exception TU-3 3: TUG-3 type containing TU-3.| `RW`| `0x0`| `0x0`|
|`[13:12]`|`pgdemtug26type`| Configure types of VT/TUs in TUG-2 #6. 0: TU11 1: TU12| `RW`| `0x0`| `0x0`|
|`[11:10]`|`pgdemtug25type`| Configure types of VT/TUs in TUG-2 #5.| `RW`| `0x0`| `0x0`|
|`[9:8]`|`pgdemtug24type`| Configure types of VT/TUs in TUG-2 #4.| `RW`| `0x0`| `0x0`|
|`[7:6]`|`pgdemtug23type`| Configure types of VT/TUs in TUG-2 #3.| `RW`| `0x0`| `0x0`|
|`[5:4]`|`pgdemtug22type`| Configure types of VT/TUs in TUG-2 #2.| `RW`| `0x0`| `0x0`|
|`[3:2]`|`pgdemtug21type`| Configure types of VT/TUs in TUG-2 #1.| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pgdemtug20type`| Configure types of VT/TUs in TUG-2 #0.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN VTTU Pointer Generator Per Channel Control

* **Description**           

Each register is used to configure for VTTU pointer Generator engines.

# HDL_PATH		: itfi5ho.itxpp_vtpp_inst.itxpp_vtpp[0].txpg_pgctl.array


* **RTL Instant Name**    : `vpgramctl`

* **Address**             : `0x80800 - 0x94fff`

* **Formula**             : `0x80800 + 16384*LineId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`vtpgbiperrfrc`| Forcing Bip error. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[6]`|`vtpglopfrc`| Forcing LOP. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[5]`|`vtpgueqfrc`| Forcing FSM to UEQ state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[4]`|`vtpgaisfrc`| Forcing FSM to AIS state. 1: Force 0: Not force| `RW`| `0x0`| `0x0`|
|`[3:2]`|`vtpgssinspatt`| Configure pattern SS bits that is used to insert to Pointer value.| `RW`| `0x0`| `0x0`|
|`[1]`|`vtpgssinsen`| Enable/disable SS bits insertion. 1: Enable 0: Disable| `RW`| `0x0`| `0x0`|
|`[0]`|`vtpgpohins`| Enable/ disable POH Insertion. High to enable insertion of POH.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Rx VT/TU per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of VT/TU pointer interpreter . Each register is used to store 5 sticky bits for 5 alarms in the VT/TU.


* **RTL Instant Name**    : `upvtchstkram`

* **Address**             : `0x62800 - 0x76fff`

* **Formula**             : `0x62800 + 16384*LineId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:5]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[4]`|`vtpistsnewdetintr`| Set to 1 while an New Pointer Detection event is detected at VT/TU pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[3]`|`vtpistsndfintr`| Set to 1 while an NDF event is detected at VT/TU pointer interpreter. This event doesn't raise interrupt.| `W1C`| `0x0`| `0x0`|
|`[2]`|`vtpistscepuneqvstatechgintr`| Set 1 to while there is change in Unequip state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in Uneqip state or not.| `W1C`| `0x0`| `0x0`|
|`[1]`|`vtpistsaisstatechgintr`| Set 1 to while there is change in AIS state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in LOP state or not.| `W1C`| `0x0`| `0x0`|
|`[0]`|`vtpistslopstatechgintr`| Set 1 to while there is change in LOP state in the related VT/TU to generate an interrupt. Read the OCN Rx VT/TU per Alarm Current Status register of the related VT/TU to know the VT/TU whether in AIS state or not.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Rx VT/TU per Alarm Current Status

* **Description**           

This is the per Alarm current status of VT/TU pointer interpreter. Each register is used to store 3 bits to store current status of 3 alarms in the VT/TU.


* **RTL Instant Name**    : `upvtchstaram`

* **Address**             : `0x63000 - 0x77fff`

* **Formula**             : `0x63000 + 16384*LineId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RO`| `0x0`| `0x0`|
|`[2]`|`vtpistscepuneqcurstatus`| Unequip current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsCepUneqVStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set.| `RO`| `0x0`| `0x0`|
|`[1]`|`vtpistsaiscurstatus`| AIS current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsAISStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set.| `RO`| `0x0`| `0x0`|
|`[0]`|`vtpistslopcurstatus`| LOP current status in the related VT/TU. When it changes for 0 to 1 or vice versa, the VtPiStsLopStateChgIntr bit in the OCN Rx VT/TU per Alarm Interrupt Status register of the related VT/TU is set.| `RO`| `0x0`| `0x0 End : Begin:`|

###OCN Rx PP VT/TU Pointer interpreter pointer adjustment per channel counter

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VT/TU pointer interpreter. These counters are in saturation mode


* **RTL Instant Name**    : `adjcntperstkram`

* **Address**             : `0x61000 - 0x75fff`

* **Formula**             : `0x61000 + 16384*LineId + 2048*AdjMode + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$AdjMode(0-1)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`rxppvipiptadjcnt`| The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg VTTU per Alarm Interrupt Status

* **Description**           

This is the per Alarm interrupt status of STS/VT/TU pointer generator . Each register is used to store 3 sticky bits for 3 alarms


* **RTL Instant Name**    : `vtpgstkram`

* **Address**             : `0x82800 - 0x96fff`

* **Formula**             : `0x82800 + 16384*LineId + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[3]`|`vtpgaisintr`| Set to 1 while an AIS status event (at h2pos) is detected at Tx VTTU Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[2]`|`vtpgfifoovfintr`| Set to 1 while an FIFO Overflowed event is detected at Tx VTTU Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[1]`|`vtpgndfintr`| Set to 1 while an NDF status event (at h2pos) is detected at Tx VTTU Pointer Generator.| `W1C`| `0x0`| `0x0`|
|`[0]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN TxPg VTTU pointer adjustment per channel counter

* **Description**           

Each register is used to store 1 increment counter (AdjMode = 0) or decrement counter (AdjMode = 1) for the related channel in VTTU pointer generator. These counters are in saturation mode


* **RTL Instant Name**    : `adjcntpgpervtram`

* **Address**             : `0x81000 - 0x95fff`

* **Formula**             : `0x81000 + 16384*LineId + 2048*AdjMode + 32*StsId + 4*VtgId + VtId`

* **Where**               : 

    * `$LineId(0-3)`

    * `$AdjMode(0-1)`

    * `$StsId(0-47):`

    * `$VtgId(0-6):`

    * `$VtId(0-3)`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[17:0]`|`vtpgptadjcnt`| The pointer Increment counter or decrease counter. Bit [11] of address is used to indicate that the counter is pointer increment or decrement counter. The counter will stop at maximum value (0x3FFFF).| `RC`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Force Config 1 - TFI5 Side

* **Description**           

OCN Parity Force Config 1


* **RTL Instant Name**    : `parfrccfg1`

* **Address**             : `0x0f020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`esspgparerrfrc`| Force Parity for ram config "OCN STS Pointer Generator Per Channel Control - TFI5 Side". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[23:16]`|`esspiparerrfrc`| Force Parity for ram config "OCN STS Pointer Interpreter Per Channel Control - TFI5 Side". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[15:8]`|`txbarparerrfrc`| Force Parity for ram config "OCN Tx Bridge and Roll SXC Control - TFI5 Side". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`rxbarparerrfrc`| Force Parity for ram config "OCN Rx Bridge and Roll SXC Control - TFI5 Side". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Disable Config 1 - TFI5 Side

* **Description**           

OCN Parity Disable Config 1


* **RTL Instant Name**    : `pardiscfg1`

* **Address**             : `0x0f021`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`esspgparerrdis`| Disable Parity for ram config "OCN STS Pointer Generator Per Channel Control - TFI5 Side". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[23:16]`|`esspiparerrdis`| Disable Parity for ram config "OCN STS Pointer Interpreter Per Channel Control - TFI5 Side". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[15:8]`|`txbarparerrdis`| Disable Parity for ram config "OCN Tx Bridge and Roll SXC Control - TFI5 Side". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`rxbarparerrdis`| Disable Parity for ram config "OCN Rx Bridge and Roll SXC Control - TFI5 Side". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Error Sticky 1 - TFI5 Side

* **Description**           

OCN Parity Error Sticky 1


* **RTL Instant Name**    : `parerrstk1`

* **Address**             : `0x0f022`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`esspgparerrstk`| Error Sticky for ram config "OCN STS Pointer Generator Per Channel Control - TFI5 Side". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[23:16]`|`esspiparerrstk`| Error Sticky for ram config "OCN STS Pointer Interpreter Per Channel Control - TFI5 Side". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[15:8]`|`txbarparerrstk`| Error Sticky for ram config "OCN Tx Bridge and Roll SXC Control - TFI5 Side". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[7:0]`|`rxbarparerrstk`| Error Sticky for ram config "OCN Rx Bridge and Roll SXC Control - TFI5 Side". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Force Config 2 - Line Side

* **Description**           

OCN Parity Force Config 2


* **RTL Instant Name**    : `parfrccfg2`

* **Address**             : `0x20020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`tohparerrfrc`| Force Parity for ram config "OCN TOH Monitoring Per Line Control".Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[15:12]`|`spgparerrfrc`| Force Parity for ram config "OCN STS Pointer Generator Per Channel Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[11:8]`|`spiparerrfrc`| Force Parity for ram config "OCN STS Pointer Interpreter Per Channel Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`txfrmparerrfrc`| Force Parity for ram config "OCN Tx J0 Insertion Buffer". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Disable Config 2 - Line Side

* **Description**           

OCN Parity Disable Config 2


* **RTL Instant Name**    : `pardiscfg2`

* **Address**             : `0x20021`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16]`|`tohparerrdis`| Disable Parity for ram config "OCN TOH Monitoring Per Line Control".Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[15:12]`|`spiparerrdis`| Disable Parity for ram config "OCN STS Pointer Interpreter Per Channel Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[11:8]`|`txbarparerrdis`| Disable Parity for ram config "OCN Tx Bridge and Roll SXC Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[7:0]`|`txfrmparerrdis`| Disable Parity for ram config "OCN Tx J0 Insertion Buffer". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Error Sticky 2 - Line Side

* **Description**           

OCN Parity Error Sticky 2


* **RTL Instant Name**    : `parerrstk2`

* **Address**             : `0x20022`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[16]`|`tohparerrstk`| Error Sticky for ram config "OCN TOH Monitoring Per Line Control".Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[15:12]`|`spiparerrstk`| Error Sticky for ram config "OCN STS Pointer Interpreter Per Channel Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[11:8]`|`txbarparerrstk`| Error Sticky for ram config "OCN Tx Bridge and Roll SXC Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[7:0]`|`txfrmparerrstk`| Error Sticky for ram config "OCN Tx J0 Insertion Buffer". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Force Config 3 - VPI+VPG+OHO

* **Description**           

OCN Parity Force Config 3 for 4 lines 0-3


* **RTL Instant Name**    : `parfrccfg3`

* **Address**             : `0xf0020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[19:16]`|`rxhoparerrfrc0`| Force Parity for ram config "OCN Rx High Order Map concatenate configuration".Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[15:12]`|`vpgctlparerrfrc0`| Force Parity for ram config "OCN VTTU Pointer Generator Per Channel Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[11:8]`|`vpgdemparerrfrc0`| Force Parity for ram config "OCN TXPP Per STS Multiplexing Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[7:4]`|`vpictlparerrfrc0`| Force Parity for ram config "OCN VTTU Pointer Interpreter Per Channel Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0`|
|`[3:0]`|`vpidemparerrfrc0`| Force Parity for ram config "OCN RXPP Per STS payload Control". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Disable Config 3 - VPI+VPG+OHO

* **Description**           

OCN Parity Disable Config 3 for 4 lines 0-3


* **RTL Instant Name**    : `pardiscfg3`

* **Address**             : `0xf0021`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[19:16]`|`rxhoparerrdis0`| Disable Parity for ram config "OCN Rx High Order Map concatenate configuration".Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[15:12]`|`vpgctlparerrdis0`| Disable Parity for ram config "OCN VTTU Pointer Generator Per Channel Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[11:8]`|`vpgdemparerrdis0`| Disable Parity for ram config "OCN TXPP Per STS Multiplexing Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[7:4]`|`vpictlparerrdis0`| Disable Parity for ram config "OCN VTTU Pointer Interpreter Per Channel Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0`|
|`[3:0]`|`vpidemparerrdis0`| Disable Parity for ram config "OCN RXPP Per STS payload Control". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Error Sticky 3 - VPI+VPG+OHO

* **Description**           

OCN Parity Error Sticky 3 for 4 lines 0-3


* **RTL Instant Name**    : `parerrstk3`

* **Address**             : `0xf0022`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[19:16]`|`rxhoparerrstk0`| Error Sticky for ram config "OCN Rx High Order Map concatenate configuration".Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[15:12]`|`vpgctlparerrstk0`| Error Sticky for ram config "OCN VTTU Pointer Generator Per Channel Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[11:8]`|`vpgdemparerrstk0`| Error Sticky for ram config "OCN TXPP Per STS Multiplexing Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[7:4]`|`vpictlparerrstk0`| Error Sticky for ram config "OCN VTTU Pointer Interpreter Per Channel Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0`|
|`[3:0]`|`vpidemparerrstk0`| Error Sticky for ram config "OCN RXPP Per STS payload Control". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Force Config 5 - SXC selector page0+1

* **Description**           

OCN Parity Force Config 5


* **RTL Instant Name**    : `parfrccfg5`

* **Address**             : `0xf0024`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:0]`|`sxcparerrfrc0`| Force Parity for ram config "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Disable Config 5 - SXC selector page0+1

* **Description**           

OCN Parity Disable Config 5


* **RTL Instant Name**    : `pardiscfg5`

* **Address**             : `0xf0025`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:0]`|`sxcparerrdis0`| Disable Parity for ram config "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Error Sticky 5 - SXC selector page0+1

* **Description**           

OCN Parity Error Sticky 5


* **RTL Instant Name**    : `parerrstk5`

* **Address**             : `0xf0026`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[11:0]`|`sxcparerrstk0`| Error Sticky for ram config "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Force Config 6 - SXC selector page2

* **Description**           

OCN Parity Force Config 6


* **RTL Instant Name**    : `parfrccfg6`

* **Address**             : `0xf0028`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:0]`|`sxcparerrfrc1`| Force Parity for ram config "OCN SXC Control 2 - Config Page 2 of SXC". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Disable Config 6 - SXC selector page2

* **Description**           

OCN Parity Disable Config 6


* **RTL Instant Name**    : `pardiscfg6`

* **Address**             : `0xf0029`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:0]`|`sxcparerrdis1`| Disable Parity for ram config "OCN SXC Control 2 - Config Page 2 of SXC". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Error Sticky 6 - SXC selector page2

* **Description**           

OCN Parity Error Sticky 6


* **RTL Instant Name**    : `parerrstk6`

* **Address**             : `0xf002a`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[11:0]`|`sxcparerrstk1`| Error Sticky for ram config "OCN SXC Control 2 - Config Page 2 of SXC". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Force Config 7 - SXC Config APS

* **Description**           

OCN Parity Force Config 7


* **RTL Instant Name**    : `parfrccfg7`

* **Address**             : `0xf002c`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:0]`|`sxcparerrfrc2`| Force Parity for ram config "OCN SXC Control 3 - Config APS". Each bit correspond with each LineId. Set 1 to force.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Disable Config 7 - SXC Config APS

* **Description**           

OCN Parity Disable Config 7


* **RTL Instant Name**    : `pardiscfg7`

* **Address**             : `0xf002d`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11:0]`|`sxcparerrdis2`| Disable Parity for ram config "OCN SXC Control 3 - Config APS". Each bit correspond with each LineId. Set 1 to disable.| `RW`| `0x0`| `0x0 End : Begin:`|

###OCN Parity Error Sticky 7 - SXC Config APS

* **Description**           

OCN Parity Error Sticky 7


* **RTL Instant Name**    : `parerrstk7`

* **Address**             : `0xf002e`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[11:0]`|`sxcparerrstk2`| Error Sticky for ram config "OCN SXC Control 3 - Config APS". Each bit correspond with each LineId.| `W1C`| `0x0`| `0x0 End :`|

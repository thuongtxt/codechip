## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_BERT_GEN
####Register Table

|Name|Address|
|-----|-----|
|`Sel Bert ID Gen`|`0x8500 - 0x850F`|
|`Bert Gen Global Register`|`0x837F`|
|`BERT insert error`|`0x837E`|
|`Control Bert Generate`|`0x8300 - 0x830F`|
|`Config Fix pattern gen`|`0x8310 - 0x831F`|
|`Config Insert Ber`|`0x8320 - 0x832F`|
|`Status of bert`|`0x8340 - 0x834F`|
|`Config nxDS0 enable`|`0x8360 - 0x836F`|
|`Moniter Good Bit`|`0x8380 - 0x838F`|


###Sel Bert ID Gen

* **Description**           

Sel 16 chanel id which enable generate BERT from 12xOC24 channel


* **RTL Instant Name**    : `upen_id_reg_gen`

* **Address**             : `0x8500 - 0x850F`

* **Formula**             : `0x8500 + $id`

* **Where**               : 

    * `$id(0-15)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`gen_en`| bit enable| `RW`| `0x0`| `0x0`|
|`[13:11]`|`slide_id`| OC48 slide| `RW`| `0x0`| `0x0`|
|`[10:00]`|`gen_id`| channel id| `RW`| `0x0`| `0x0 End: Begin:`|

###Bert Gen Global Register

* **Description**           

global config


* **RTL Instant Name**    : `glb_pen`

* **Address**             : `0x837F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[01:00]`|`global`| value of global| `RW`| `0x00`| `0x00 End: Begin:`|

###BERT insert error

* **Description**           

Insert single bit error


* **RTL Instant Name**    : `errins_pen`

* **Address**             : `0x837E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3:0]`|`errins_lat`| enable id insert error| `RW`| `0x0`| `0x0 End: Begin:`|

###Control Bert Generate

* **Description**           

Used to select mode operation of bert


* **RTL Instant Name**    : `ctrl_pen`

* **Address**             : `0x8300 - 0x830F`

* **Formula**             : `0x8300 + $ctrl_id`

* **Where**               : 

    * `$ctrl_id(0-15)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11]`|`swapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[10]`|`invmode`| invert data| `RW`| `0x0`| `0x0`|
|`[09:05]`|`patbit`| number of bit fix patt use| `RW`| `0x0`| `0x0`|
|`[04:00]`|`patt_mode`| sel pattern gen # 0x0 : prbs9 # 0x1 : prbs11 # 0x2 : prbs15 # 0x3 : prbs20r # 0x4 : prbs20 # 0x5 : qrss20 # 0x6 : prbs23 # 0x7 : dds1 # 0x8 : dds2 # 0x9 : dds3 # 0xA : dds4 # 0xB : dds5 # 0xC : daly # 0XD : octet55_v2 # 0xE : octet55_v3 # 0xF : fix3in24 # 0x10: fix1in8 # 0x11: fix2in8 # 0x12: fixpat # 0x13: sequence # 0x14: ds0prbs # 0x15: fixnin32| `RW`| `0x0`| `0x0 End: Begin:`|

###Config Fix pattern gen

* **Description**           

config fix pattern gen


* **RTL Instant Name**    : `thrnfix_pen`

* **Address**             : `0x8310 - 0x831F`

* **Formula**             : `0x8310 + $genid`

* **Where**               : 

    * `$genid (0-15)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`crrthrnfix`| config fix pattern gen| `RW`| `0x0`| `0x0 End: Begin:`|

###Config Insert Ber

* **Description**           

Mode of insert error rate


* **RTL Instant Name**    : `ber_pen`

* **Address**             : `0x8320 - 0x832F`

* **Formula**             : `0x8320 + $berid`

* **Where**               : 

    * `$berid (0-15)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End: Begin:`|

###Status of bert

* **Description**           

Indicate status of bert gen


* **RTL Instant Name**    : `status_pen`

* **Address**             : `0x8340 - 0x834F`

* **Formula**             : `0x8340 + $sttid`

* **Where**               : 

    * `$sttid (0-15)`

* **Width**               : `128`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[127:45]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[44:0]`|`crr_stt`| status value| `RO`| `0x0`| `0x0 End: Begin:`|

###Config nxDS0 enable

* **Description**           

enable bert for 32 timeslot E1 or 24timeslot T1


* **RTL Instant Name**    : `tsen_pen`

* **Address**             : `0x8360 - 0x836F`

* **Formula**             : `0x8360 + $nid`

* **Where**               : 

    * `$nid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tsen_pdo`| set "1" to enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Moniter Good Bit

* **Description**           

counter good bit


* **RTL Instant Name**    : `goodbit_pen`

* **Address**             : `0x8380 - 0x838F`

* **Formula**             : `0x8380 + $gb_id`

* **Where**               : 

    * `$gb_id(0-15)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[23:0]`|`goodbit_pdo`| counter goodbit| `RW`| `0x0`| `0x0 End:`|

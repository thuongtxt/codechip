## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_PDH_LPC
####Register Table

|Name|Address|
|-----|-----|
|`LC test reg1`|`0x0000`|
|`LC test reg2`|`0x0001`|
|`Config Limit error`|`0x0002`|
|`Config limit match`|`0x0003`|
|`Config thresh fdl parttern`|`0x0004`|
|`Config thersh error`|`0x0005`|
|`Sticky pattern detected`|`0x13FF`|
|`Config User programmble Pattern User Codes`|`0x13F0`|
|`Number of pattern`|`0x13FE`|
|`Pattern Inband Detected`|`0x0C00 - 0x0FFF`|
|`Sticky FDL message detected`|`0x0BFF`|
|`Config User programmble Pattern User Codes`|`0x0BFD`|
|`Number of message`|`0x0BFE`|
|`Message FDL Detected`|`0x0400 - 0x_07FF`|
|`Config Theshold Pattern Report`|`0x0006`|


###LC test reg1

* **Description**           

For testing only


* **RTL Instant Name**    : `upen_test1`

* **Address**             : `0x0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`test_reg1`| value of reg1| `RW`| `0x1004_2015`| `0x1004_2015 End Begin:`|

###LC test reg2

* **Description**           

For testing only


* **RTL Instant Name**    : `upen_test2`

* **Address**             : `0x0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`test_reg2`| value of reg2| `RW`| `0x00`| `0x00 End; Begin:`|

###Config Limit error

* **Description**           

Check pattern error in state matching pattern


* **RTL Instant Name**    : `upen_lim_err`

* **Address**             : `0x0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:4]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[3:0]`|`cfg_lim_err`| if cnt err more than limit error,search failed| `R/W`| `0xF`| `0xF End: Begin:`|

###Config limit match

* **Description**           

Used to check parttern match


* **RTL Instant Name**    : `upen_lim_mat`

* **Address**             : `0x0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[15:00]`|`cfg_lim_mat`| if cnt match more than limit mat, serch ok| `R/W`| `0xFFE`| `0xFFE End: Begin:`|

###Config thresh fdl parttern

* **Description**           

Check fdl message codes match


* **RTL Instant Name**    : `upen_th_fdl_mat`

* **Address**             : `0x0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`cfg_th_fdl_mat`| if message codes repeat more than thresh, search ok,| `R/W`| `0x5`| `0x5 End: Begin:`|

###Config thersh error

* **Description**           

Max error for one pattern


* **RTL Instant Name**    : `upen_th_err`

* **Address**             : `0x0005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:07]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[06:00]`|`cfg_th_err`| if pattern error more than thresh , change to check another pattern| `R/W`| `0xF`| `0xF End: Begin:`|

###Sticky pattern detected

* **Description**           

Indicate status of fifo inband partern empty or full


* **RTL Instant Name**    : `upen_stk`

* **Address**             : `0x13FF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[2:0]`|`updo_stk`| bit[0] = 1 :empty, bit[1] = 1 :full, bit[2] = 1 :interrupt| `R2C`| `0x0`| `0x0 End: Begin:`|

###Config User programmble Pattern User Codes

* **Description**           

config patter search


* **RTL Instant Name**    : `upen_pat`

* **Address**             : `0x13F0`

* **Formula**             : `0x13F0 + $pat`

* **Where**               : 

    * `$pat(0-7)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[10:0]`|`updo_pat`| patt codes| `R/W`| `0x0`| `0x0 End: Begin:`|

###Number of pattern

* **Description**           

Indicate number of pattern is detected


* **RTL Instant Name**    : `upen_len`

* **Address**             : `0x13FE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[10:00]`|`info_len_l`| number patt detected| `RO`| `0x0`| `0x0 End: Begin:`|

###Pattern Inband Detected

* **Description**           

Used to report pattern detected to cpu


* **RTL Instant Name**    : `upen_info`

* **Address**             : `0x0C00 - 0x0FFF`

* **Formula**             : `0x0C00 + len`

* **Where**               : 

    * `$len (0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[15:00]`|`pat_info_l`| bit [15:5] channel id, bit [4:0] == 0x8  CSU UP bit [4:0] == 0x9  CSU DOWN bit [4:0] == 0xA  FAC1 UP bit [4:0] == 0xB  FAC1 DOWN bit [4:0] == 0xC  FAC2 UP bit [4:0] == 0xD  FAC2 DOWN| `RO`| `0x0`| `0x0 End: Begin:`|

###Sticky FDL message detected

* **Description**           

Indicate status of fifo fdl message empty or full


* **RTL Instant Name**    : `upen_fdl_stk`

* **Address**             : `0x0BFF`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[2:0]`|`updo_fdlstk`| bit[0] = 1 :empty, bit[1] = 1 :full, bit[2] = 1 :interrupt| `R2C`| `0x0`| `0x0 End: Begin:`|

###Config User programmble Pattern User Codes

* **Description**           

check fdl message codes


* **RTL Instant Name**    : `upen_fdlpat`

* **Address**             : `0x0BFD`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `64`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:56]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[55:00]`|`updo_fdl_mess`| mess codes| `RO`| `0x284C_701C`| `0x284C_701C End: Begin:`|

###Number of message

* **Description**           

Indicate number of message codes is detected


* **RTL Instant Name**    : `upen_len_mess`

* **Address**             : `0x0BFE`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:11]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[10:00]`|`info_len_l`| number patt detected| `RO`| `0x0`| `0x0 End: Begin:`|

###Message FDL Detected

* **Description**           

Info fdl message detected


* **RTL Instant Name**    : `upen_fdl_info`

* **Address**             : `0x0400 - 0x_07FF`

* **Formula**             : `0x0400 + $fdl_len`

* **Where**               : 

    * `$fdl_len (0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[14:00]`|`mess_info_l`| message info bit [14:4] chanel id, bit[3:0]== 0x0 11111111_01111110 bit[3:0]== 0x1 line loop up bit[3:0]== 0x2 line loop down bit[3:0]== 0x3 payload loop up bit[3:0]== 0x4 payload loop down bit[3:0]== 0x5 network loop up bit[3:0]== 0x6 network loop down| `RO`| `0x0`| `0x0 End: Begin:`|

###Config Theshold Pattern Report

* **Description**           

Maximum Pattern Report


* **RTL Instant Name**    : `upen_th_stt_int`

* **Address**             : `0x0006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`updo_th_stt_int`| if number of pattern detected more than threshold, interrupt enable| `R/W`| `0xFF`| `0xFF End:`|

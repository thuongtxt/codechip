## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CNC0021_RD_ETHPASS
####Register Table

|Name|Address|
|-----|-----|
|`CONFIG VLAN REMOVED port 1-2`|`0x00`|
|`CONFIG VLAN REMOVED port 3-4`|`0x01`|
|`CONFIG VLAN REMOVED port 5-6`|`0x02`|
|`CONFIG VLAN REMOVED port 7-8`|`0x03`|
|`CONFIG VLAN REMOVED  port 9-10`|`0x04`|
|`CONFIG VLAN REMOVED  port 11-12`|`0x05`|
|`CONFIG VLAN REMOVED port 13-14`|`0x06`|
|`CONFIG VLAN REMOVED port 15-16`|`0x07`|
|`CONFIG VLAN REMOVED PW`|`0x08`|
|`CONFIG ENABLE REMOVE VLAN TPID`|`0x10`|
|`CONFIG ENABLE REMOVE VLAN TPID`|`0x11`|
|`CONFIG ENABLE REMOVE VLAN TPID`|`0x12`|
|`CONFIG VLAN INSERTED`|`0x100-0x107`|
|`CONFIG ENABLE INSERT VLAN TPID`|`0x110`|
|`CONFIG ENABLE INSERT VLAN TPID`|`0x111`|
|`CONFIG ENABLE INSERT VLAN TPID`|`0x112`|
|`CONFIG THRESHOLD`|`0x200-0x20F`|
|`CONFIG ENABLE FLOW CONTROL`|`0x210`|
|`Ethernet Pass Through Hold Register Status`|`0x80`|
|`Ethernet Pass Through Group1 Counter`|`0xA00 - 0xA2F(RO)`|
|`Ethernet Pass Through Group2 Counter`|`0xA80 - 0xAAF(RO)`|
|`Ethernet Pass Through Group3 Counter`|`0xB00 - 0xB2F(RC)`|
|`Ethernet Pass Through Group1 Counter`|`0xE00 - 0xE2F(RO)`|
|`Ethernet Pass Through Group2 Counter`|`0xE80 - 0xEAF(RO)`|
|`Ethernet Pass Through Group3 Counter`|`0xF00 - 0xF2F(RO)`|
|`Ethernet Pass Through Group4 Counter`|`0xF80 - 0xFBF(RO)`|
|`CONFIG VLAN1 INSERTED PW`|`0x0A0`|
|`CONFIG VLAN2 INSERTED PW`|`0x0A1`|
|`CONFIG VLAN INSERTED PW ENABLE`|`0x0A2`|
|`Ethernet Pass Through MAC Address Control`|`0xC30 - 0xC37 #The address format for these registers is 0xC30 + ethpid`|
|`Ethernet Pass Through Port Enable and MTU Control`|`0xC40 - 0xC47 #The address format for these registers is 0xC40 + ethpid`|


###CONFIG VLAN REMOVED port 1-2

* **Description**           




* **RTL Instant Name**    : `upen_cfgvlan1`

* **Address**             : `0x00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED port 3-4

* **Description**           




* **RTL Instant Name**    : `upen_cfgvlan2`

* **Address**             : `0x01`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED port 5-6

* **Description**           




* **RTL Instant Name**    : `upen_cfgvlan3`

* **Address**             : `0x02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED port 7-8

* **Description**           




* **RTL Instant Name**    : `upen_cfgvlan4`

* **Address**             : `0x03`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED  port 9-10

* **Description**           




* **RTL Instant Name**    : `upen_cfgvlan5`

* **Address**             : `0x04`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED  port 11-12

* **Description**           




* **RTL Instant Name**    : `upen_cfgvlan6`

* **Address**             : `0x05`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED port 13-14

* **Description**           




* **RTL Instant Name**    : `upen_cfgvlan7`

* **Address**             : `0x06`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED port 15-16

* **Description**           




* **RTL Instant Name**    : `upen_cfgvlan8`

* **Address**             : `0x07`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED PW

* **Description**           




* **RTL Instant Name**    : `upen_cfg_pwvlan`

* **Address**             : `0x08`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE REMOVE VLAN TPID

* **Description**           




* **RTL Instant Name**    : `upen_enbtpid`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `18`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17:17]`|`out_enbtpid_pw`| enable compare PW VLAN,<br>{1} is enable, <br>{0} is disable| `R/W`| `0x0`| `0x0`|
|`[16:16]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[15:0]`|`out_cfg_tpid`| bit corresponding port to config ( bit 0 is port 0),<br>{0}  use config TPID1, <br>{1} use config TPID 2| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE REMOVE VLAN TPID

* **Description**           




* **RTL Instant Name**    : `upen_cfgrmtpid`

* **Address**             : `0x11`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`out_cfgtpid2`| value TPID 2| `R/W`| `0x0`| `0x0`|
|`[15:0]`|`out_cfgtpid1`| value TPID 1| `R/W`| `0x8100`| `0x8100 End: Begin:`|

###CONFIG ENABLE REMOVE VLAN TPID

* **Description**           




* **RTL Instant Name**    : `upen_cfgrmtpid_pw`

* **Address**             : `0x12`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`out_cfgtpid_pw2`| value TPID PW 2| `R/W`| `0x0`| `0x0`|
|`[15:0]`|`out_cfgtpid_pw1`| value TPID PW 1| `R/W`| `0x8100`| `0x8100 End: Begin:`|

###CONFIG VLAN INSERTED

* **Description**           




* **RTL Instant Name**    : `upen_cfginsvlan`

* **Address**             : `0x100-0x107`

* **Formula**             : `0x100+ $PID`

* **Where**               : 

    * `$PID(0-15) : Port ID`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:13]`|`prior`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE INSERT VLAN TPID

* **Description**           




* **RTL Instant Name**    : `upen_enbinstpid`

* **Address**             : `0x110`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`out_enbtpid`| bit corresponding port to config ( bit 0 is port 0),(0)  use config TPID1, (1) use config TPID 2| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE INSERT VLAN TPID

* **Description**           




* **RTL Instant Name**    : `upen_cfginstpid`

* **Address**             : `0x111`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`out_cfgtpid2`| valud TPID 2| `R/W`| `0x0`| `0x0`|
|`[15:0]`|`out_cfgtpid1`| value TPID 1| `R/W`| `0x8100`| `0x8100 End: Begin:`|

###CONFIG ENABLE INSERT VLAN TPID

* **Description**           




* **RTL Instant Name**    : `upen_enbins`

* **Address**             : `0x112`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`out_enbins`| bit corresponding port to config ( bit 0 is port 0), (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG THRESHOLD

* **Description**           




* **RTL Instant Name**    : `upen_cfgthreshold`

* **Address**             : `0x200-0x20F`

* **Formula**             : `0x200+ $PID`

* **Where**               : 

    * `$PID(0-15) : Port ID`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:08]`|`val_thres2`| threshold for 3/4 of max buffer contain (256*3/4)kbytes| `R/W`| `0x0`| `0x0`|
|`[07:00]`|`val_thes1`| threshold for 1/4 of max buffer contain (256/4)kbytes| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE FLOW CONTROL

* **Description**           




* **RTL Instant Name**    : `upen_enbflwctrl`

* **Address**             : `0x210`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00:00]`|`out_enbflw`| (0) is disable, (1) is enable| `R/W`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Hold Register Status

* **Description**           

This register using for hold remain that more than 128bits


* **RTL Instant Name**    : `epa_hold_status`

* **Address**             : `0x80`

* **Formula**             : `0x80 +  HID`

* **Where**               : 

    * `$HID(0-0): Hold ID`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`epaholdstatus`| Hold 32bits| `RW`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group1 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_txepagrp1cnt`

* **Address**             : `0xA00 - 0xA2F(RO)`

* **Formula**             : `0xA00 + 16*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-15)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:48]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[47:24]`|`txethgrp1cnt1`| cntoffset = 0 : txpkt255byte cntoffset = 1 : txpkt1023byte cntoffset = 2 : txpktjumbo| `RC`| `0x0`| `0x0`|
|`[23:00]`|`txethgrp1cnt0`| cntoffset = 0 : txpkt127byte cntoffset = 1 : txpkt511byte cntoffset = 2 : txpkt1518byte| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group2 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_txepagrp2cnt`

* **Address**             : `0xA80 - 0xAAF(RO)`

* **Formula**             : `0xA80 + 16*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-15)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:32]`|`txethgrp2cnt1`| cntoffset = 0 : txpktgood cntoffset = 1 : unused cntoffset = 2 : txoversize| `RC`| `0x0`| `0x0`|
|`[31:00]`|`txethgrp2cnt0`| cntoffset = 0 : txbytecnt cntoffset = 1 : txpkttotaldis cntoffset = 2 : txundersize| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group3 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_txepagrp3cnt`

* **Address**             : `0xB00 - 0xB2F(RC)`

* **Formula**             : `0xB00 + 16*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-15)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:48]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[47:24]`|`txethgrp1cnt1`| cntoffset = 0 : txpktoverrun cntoffset = 1 : txpktmulticast cntoffset = 2 : unused| `RC`| `0x0`| `0x0`|
|`[23:00]`|`txethgrp1cnt0`| cntoffset = 0 : txpktpausefrm cntoffset = 1 : txpktbroadcast cntoffset = 2 : txpkt64byte| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group1 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_rxepagrp1cnt`

* **Address**             : `0xE00 - 0xE2F(RO)`

* **Formula**             : `0xE00 + 16*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-15)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:48]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[47:24]`|`rxethgrp1cnt1`| cntoffset = 0 : rxpkt255byte cntoffset = 1 : rxpkt1023byte cntoffset = 2 : rxpktjumbo| `RC`| `0x0`| `0x0`|
|`[23:00]`|`rxethgrp1cnt0`| cntoffset = 0 : rxpkt127byte cntoffset = 1 : rxpkt511byte cntoffset = 2 : rxpkt1518byte| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group2 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_rxepagrp2cnt`

* **Address**             : `0xE80 - 0xEAF(RO)`

* **Formula**             : `0xE80 + 16*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-15)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:40]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[63:32]`|`rxethgrp2cnt1`| cntoffset = 0 : rxpkttotal cntoffset = 1 : unused cntoffset = 2 : rxpktdaloop| `RC`| `0x0`| `0x0`|
|`[31:00]`|`rxethgrp2cnt0`| cntoffset = 0 : rxbytecnt cntoffset = 1 : rxpktfcserr cntoffset = 2 : rxpkttotaldis| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group3 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_rxepagrp3cnt`

* **Address**             : `0xF00 - 0xF2F(RO)`

* **Formula**             : `0xF00 + 16*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-15)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:48]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[47:24]`|`rxethgrp2cnt1`| cntoffset = 0 : rxpktjabber cntoffset = 1 : rxpktpausefrm cntoffset = 2 : rxpktoversize| `RC`| `0x0`| `0x0`|
|`[23:00]`|`rxethgrp2cnt0`| cntoffset = 0 : rxpktpcserr cntoffset = 1 : rxpktfragment cntoffset = 2 : rxpktundersize| `RC`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Group4 Counter

* **Description**           

The register count information as below. Depending on cntoffset it 's the events specify.


* **RTL Instant Name**    : `upen_rxepagrp4cnt`

* **Address**             : `0xF80 - 0xFBF(RO)`

* **Formula**             : `0xF80 + 16*$cntoffset + $ethpid`

* **Where**               : 

    * `$ethpid(0-15)`

    * `$cntoffset(0-2)`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:48]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[47:24]`|`rxethgrp2cnt1`| cntoffset = 0 : rxpktbroadcast cntoffset = 1 : rxpkt64byte cntoffset = 2 : unused| `RC`| `0x0`| `0x0`|
|`[23:00]`|`rxethgrp2cnt0`| cntoffset = 0 : rxpktoverrun cntoffset = 1 : rxpktmulticast cntoffset = 2 : unused| `RC`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN1 INSERTED PW

* **Description**           




* **RTL Instant Name**    : `upen_cfgpwvlan1`

* **Address**             : `0x0A0`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pwsubvlan1`| Pw subport VLAN1 value| `R/W`| `0x8100_0000`| `0x81000000 End: Begin:`|

###CONFIG VLAN2 INSERTED PW

* **Description**           




* **RTL Instant Name**    : `upen_cfgpwvlan2`

* **Address**             : `0x0A1`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pwsubvlan2`| Pw subport VLAN2 value| `R/W`| `0x81000000`| `0x81000000 End: Begin:`|

###CONFIG VLAN INSERTED PW ENABLE

* **Description**           




* **RTL Instant Name**    : `upen_cfgpwvlaninsen`

* **Address**             : `0x0A2`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[1]`|`pwvlansel`| Select VLAN to insert 0: select VLAN1 1: select VLAN2| `R/W`| `0x0`| `0x0`|
|`[0]`|`pwvlaninsen`| Enable insert VLAN 0: disable insert 1: enable insert| `R/W`| `0x1`| `0x1 End: Begin:`|

###Ethernet Pass Through MAC Address Control

* **Description**           

This register configures parameters per ethernet pass through port


* **RTL Instant Name**    : `ramrxepacfg0`

* **Address**             : `0xC30 - 0xC37 #The address format for these registers is 0xC30 + ethpid`

* **Formula**             : `0xC30 +  ethpid`

* **Where**               : 

    * `$ethpid(0-15): Ethernet port ID`

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:0]`|`ethpassmacadd`| Ethernet pass through MAC address used for Loop DA counter| `RW`| `0x0`| `0x0 End: Begin:`|

###Ethernet Pass Through Port Enable and MTU Control

* **Description**           

This register configures parameters per ethernet pass through port


* **RTL Instant Name**    : `ramrxepacfg1`

* **Address**             : `0xC40 - 0xC47 #The address format for these registers is 0xC40 + ethpid`

* **Formula**             : `0xC40 +  ethpid`

* **Where**               : 

    * `$ethpid(0-15): Ethernet port ID`

* **Width**               : `21`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[20]`|`ethpassporten`| Ethernet pass port enable| `RW`| `0x0`| `0x0 1: Enable 0: Disable`|
|`[19]`|`ethpasspcserrdropen`| Ethernet PCS error drop enable| `RW`| `0x0`| `0x0 1: Enable drop PCS error packet 0: Disable drop PCS error packet`|
|`[18]`|`ethpassfcserrdropen`| Ethernet FCS error drop enable| `RW`| `0x0`| `0x0 1: Enable drop FCS error packet 0: Disable drop FCS error packet`|
|`[17]`|`ethpassundersizedropen`| Ethernet Undersize drop enable| `RW`| `0x0`| `0x0 1: Enable drop undersize packet 0: Disable drop undersize packet`|
|`[16]`|`ethpassoversizedropen`| Ethernet Oversize drop enable| `RW`| `0x0`| `0x0 1: Enable drop oversize packet 0: Disable drop oversize packet`|
|`[15]`|`ethpasspausefrmdropen`| Ethernet Pausefrm drop enable| `RW`| `0x0`| `0x0 1: Enable drop pause frame 0: Disable drop pause frame`|
|`[14]`|`ethpassdaloopdropen`| Ethernet Daloop drop enable| `RW`| `0x0`| `0x0 1: Enable drop DA loop packet 0: Disable drop DA loop packet`|
|`[13:0]`|`ethpassmtusize`| Ethernet pass through MTU size| `RW`| `0x2588`| `0x0 End:`|

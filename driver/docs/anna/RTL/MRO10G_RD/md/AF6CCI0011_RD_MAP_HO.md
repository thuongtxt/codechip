## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_MAP_HO
####Register Table

|Name|Address|
|-----|-----|
|`Demap Channel Control`|`0x0000-0x007FF`|
|`Map Line Control`|`0x4000-0x47FF`|
|`Map Global Control`|`0x40FF`|
|`Sel Ho Bert Gen0`|`0x8_200 - 0xB_A00`|
|`Sel Ho Bert Gen1`|`0x8_201 - 0xB_A01`|
|`Sel Ho Bert Gen2`|`0x8_202 - 0xB_A02`|
|`Sel Ho Bert Gen3`|`0x8_203 - 0xB_A03`|
|`Sel Mode Bert Gen`|`0x8_300 - 0xB_B1F`|
|`Inser Error`|`0x8_320 - 0xB_B3F`|
|`Inser Error`|`0x8_3C0 + 2048*slcid`|
|`Counter num of bit gen`|`0x8_380 - 0xB_B80`|
|`Sel Ho Bert TDM Mon0`|`0x8_400 - 0xB_C00`|
|`Sel Ho Bert TDM Mon1`|`0x8_401 - 0xB_C01`|
|`Sel Ho Bert TDM Mon2`|`0x8_402 - 0xB_C02`|
|`Sel Ho Bert TDM Mon3`|`0x8_403 - 0xB_C03`|
|`Sel Ho Bert PW Mon0`|`0x8_600 - 0xB_E00`|
|`Sel Ho Bert PW Mon1`|`0x8_601 - 0xB_E01`|
|`Sel Ho Bert PW Mon2`|`0x8_602 - 0xB_E02`|
|`Sel Ho Bert PW Mon3`|`0x8_603 - 0xB_E03`|
|`Sel Mode Bert TDM mon`|`0x8_520 - 0xB_D3F`|
|`Sel Mode Bert PW mon`|`0x8_720 - 0xB_F3f`|
|`TDM loss sync`|`0x8_51F - 0xB_D1F`|
|`TDM mon status`|`0x8_500 - 0xB_D1B`|
|`PW loss sync`|`0x8_71F - 0xB_F1F`|
|`PW mon stt`|`0x8_700 - 0xB_F1B`|
|`counter good bit TDM mon`|`0x8_580 - 0xB_D9F`|
|`counter error bit in TDM mon`|`0x8_540 - 0xB_D5F`|
|`Counter loss bit in TDM mon`|`0x8_5C0 - 0xB_DDf`|
|`counter good bit TDM mon`|`0x8_780 - 0xB_F9F`|
|`counter error bit in TDM mon`|`0x8_740 - 0xB_F5F`|
|`Counter loss bit in TDM mon`|`0x8_7C0 - 0xB_FDF`|
|`RAM Map Parity Force Control`|`0x4808`|
|`RAM Map Parity Disable Control`|`0x4809`|
|`RAM Map parity Error Sticky`|`0x480a`|
|`RAM DeMap Parity Force Control`|`0x0808`|
|`RAM DeMap Parity Disable Control`|`0x0809`|
|`RAM DeMap parity Error Sticky`|`0x080a`|


###Demap Channel Control

* **Description**           

The registers are used by the hardware to configure PW channel


* **RTL Instant Name**    : `demap_channel_ctrl`

* **Address**             : `0x0000-0x007FF`

* **Formula**             : `0x0000 + 256*slice + stsid`

* **Where**               : 

    * `$slice(0-7): is OC48 slice`

    * `$stsid(0-47): is STS in OC48`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`demapsr_vc3n3c`| 0:slave of VC3_N3c  1:master of VC3-N3c| `RW`| `0x0`| `0x0`|
|`[1]`|`demapsrctype`| 0:VC3 1:VC3-3c| `RW`| `0x0`| `0x0`|
|`[0]`|`demapchen`| PW/Channels Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Map Line Control

* **Description**           

The registers provide the per line configurations for STS


* **RTL Instant Name**    : `map_line_ctrl`

* **Address**             : `0x4000-0x47FF`

* **Formula**             : `0x4000 + 256*slice + stsid`

* **Where**               : 

    * `$slice(0-7): is OC48 slice`

    * `$stsid(0-47): is STS in OC48`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`mapsrc_vc3n3c`| 0:slave of VC3_N3c  1:master of VC3-N3c| `RW`| `0x0`| `0x0`|
|`[8]`|`mapsrc_type`| 0:VC3 1:VC3-3c| `RW`| `0x0`| `0x0`|
|`[7]`|`mapchen`| PW/Channels Enable| `RW`| `0x0`| `0x0`|
|`[6]`|`maptimesrcmaster`| This bit is used to indicate the master timing or the master VC3 in VC4/VC4-Xc| `RW`| `0x0`| `0x0`|
|`[5:0]`|`maptimesrcid`| The reference line ID used for timing reference or the master VC3 ID in VC4/VC4-Xc| `RW`| `0x0`| `0x0 End: Begin:`|

###Map Global Control

* **Description**           

The registers provide the per line configurations for STS


* **RTL Instant Name**    : `map_global_ctrl`

* **Address**             : `0x40FF`

* **Formula**             : `0x40FF`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8]`|`maploopout`| This bit is used to loopout full bus| `RW`| `0x0`| `0x0`|
|`[7:0]`|`mapidlepattern`| Idle pattern| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert Gen0

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_gen0`

* **Address**             : `0x8_200 - 0xB_A00`

* **Formula**             : `0x8_200 + 2048*engid_g0`

* **Where**               : 

    * `$engid_g0 (0-7): id 1-8 of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`gen_en`| set "1" to enable bert gen| `RW`| `0x0`| `0x0`|
|`[8:6]`|`line_id`| line id OC48| `RW`| `0x0`| `0x0`|
|`[5:0]`|`stsid`| STS ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert Gen1

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_gen1`

* **Address**             : `0x8_201 - 0xB_A01`

* **Formula**             : `0x8_201 + 2048*engid_g1`

* **Where**               : 

    * `$engid_g1(0-7): id 9-16 of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`gen_en`| set "1" to enable bert gen| `RW`| `0x0`| `0x0`|
|`[8:6]`|`line_id`| line id OC48| `RW`| `0x0`| `0x0`|
|`[5:0]`|`stsid`| STS ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert Gen2

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_gen2`

* **Address**             : `0x8_202 - 0xB_A02`

* **Formula**             : `0x8_202 + 2048*engid_g2`

* **Where**               : 

    * `$engid_g2(0-7): id 17-24 of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`gen_en`| set "1" to enable bert gen| `RW`| `0x0`| `0x0`|
|`[8:6]`|`line_id`| line id OC48| `RW`| `0x0`| `0x0`|
|`[5:0]`|`stsid`| STS ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert Gen3

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_gen3`

* **Address**             : `0x8_203 - 0xB_A03`

* **Formula**             : `0x8_203 + 2048*engid_g3`

* **Where**               : 

    * `$engid_g3(0-7): id 25-32 of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[9]`|`gen_en`| set "1" to enable bert gen| `RW`| `0x0`| `0x0`|
|`[8:6]`|`line_id`| line id OC48| `RW`| `0x0`| `0x0`|
|`[5:0]`|`stsid`| STS ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Mode Bert Gen

* **Description**           

The registers select mode bert gen


* **RTL Instant Name**    : `ctrl_pen_gen`

* **Address**             : `0x8_300 - 0xB_B1F`

* **Formula**             : `0x8_300 + 2048*slcid + engid`

* **Where**               : 

    * `$slcid(0-7): slice id of HO_BERT`

    * `$engid(0-31) :id of BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[09]`|`swapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[08]`|`invmode`| invert data| `RW`| `0x0`| `0x0`|
|`[07:00]`|`patt_mode`| sel pattern gen # 0x01 : all0 # 0x02 : prbs15 # 0x04 : prbs20r # 0x08 : prbs20 # 0x10 : prbs23 # 0x20 : prbs31 # other: all1| `RW`| `0x0`| `0x0 End: Begin:`|

###Inser Error

* **Description**           

The registers select rate inser error


* **RTL Instant Name**    : `ctrl_ber_pen`

* **Address**             : `0x8_320 - 0xB_B3F`

* **Formula**             : `0x8_320 + 2048*slcid + engid`

* **Where**               : 

    * `$slcid(0-7): id of HO_BERT`

    * `$engid(0-31) :id of BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`ber_rate`| TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to Pattern Generator [31:0] == 32'd0          :disable  BER_level_val	BER_level 100:            BER 10e-2 1000:        	BER 10e-3 10_000:      	BER 10e-4 100_000:     	BER 10e-5 1_000_000:   	BER 10e-6 10_000_000:  	BER 10e-7 100_000_000: 	BER 10e-8 1_000_000_000 : BER 10e-9| `RW`| `0x0`| `0x0 Begin:`|

###Inser Error

* **Description**           

The registers select rate inser error


* **RTL Instant Name**    : `ctrl_ber_pen_single`

* **Address**             : `0x8_3C0 + 2048*slcid`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[04:00]`|`id_force`| ID of BERT   :select id for force error| `RW`| `0x0`| `0x0 Begin:`|

###Counter num of bit gen

* **Description**           

The registers counter bit genertaie in tdm side


* **RTL Instant Name**    : `goodbit_ber_pen`

* **Address**             : `0x8_380 - 0xB_B80`

* **Formula**             : `0x8_380 + 2048*engid`

* **Where**               : 

    * `$engid(0-7): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`goodbit`| counter goodbit| `R2C`| `0x0`| `0x0 Begin:`|

###Sel Ho Bert TDM Mon0

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_tdm_mon0`

* **Address**             : `0x8_400 - 0xB_C00`

* **Formula**             : `0x8_400 + 2048*engid_g0`

* **Where**               : 

    * `$engid_g0(0-7): id1-id8 of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`montdmen`| set "1" to enable bert tdm mon| `RW`| `0x0`| `0x0`|
|`[12:10]`|`tdmlineid`| OC48 line| `RW`| `0x0`| `0x0`|
|`[9:0]`|`masterid`| chanel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert TDM Mon1

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_tdm_mon1`

* **Address**             : `0x8_401 - 0xB_C01`

* **Formula**             : `0x8_401 + 2048*engid_g1`

* **Where**               : 

    * `$engid_g1(0-7): id9-id16  of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`montdmen`| set "1" to enable bert tdm mon| `RW`| `0x0`| `0x0`|
|`[12:10]`|`tdmlineid`| OC48 line| `RW`| `0x0`| `0x0`|
|`[9:0]`|`masterid`| chanel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert TDM Mon2

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_tdm_mon2`

* **Address**             : `0x8_402 - 0xB_C02`

* **Formula**             : `0x8_402 + 2048*engid_g2`

* **Where**               : 

    * `$engid_g2(0-7): id17-id24 of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`montdmen`| set "1" to enable bert tdm mon| `RW`| `0x0`| `0x0`|
|`[12:10]`|`tdmlineid`| OC48 line| `RW`| `0x0`| `0x0`|
|`[9:0]`|`masterid`| chanel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert TDM Mon3

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_tdm_mon3`

* **Address**             : `0x8_403 - 0xB_C03`

* **Formula**             : `0x8_403 + 2048*engid_g3`

* **Where**               : 

    * `$engid_g3(0-7): id25-id32 of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`montdmen`| set "1" to enable bert tdm mon| `RW`| `0x0`| `0x0`|
|`[12:10]`|`tdmlineid`| OC48 line| `RW`| `0x0`| `0x0`|
|`[9:0]`|`masterid`| chanel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert PW Mon0

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_pw_mon_g0`

* **Address**             : `0x8_600 - 0xB_E00`

* **Formula**             : `0x8_600 + 2048*engid_g0`

* **Where**               : 

    * `$engid_g0(0-7): id1-id8 of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`monpwen`| set "1" to enable bert pw mon| `RW`| `0x0`| `0x0`|
|`[12:10]`|`pwlineid`| OC48 line| `RW`| `0x0`| `0x0`|
|`[9:0]`|`masterid`| chanel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert PW Mon1

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_pw_mon_g1`

* **Address**             : `0x8_601 - 0xB_E01`

* **Formula**             : `0x8_601 + 2048*engid_g1`

* **Where**               : 

    * `$engid_g1(0-7): id9-id16 of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`monpwen`| set "1" to enable bert pw mon| `RW`| `0x0`| `0x0`|
|`[12:10]`|`pwlineid`| OC48 line| `RW`| `0x0`| `0x0`|
|`[9:0]`|`masterid`| chanel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert PW Mon2

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_pw_mon_g2`

* **Address**             : `0x8_602 - 0xB_E02`

* **Formula**             : `0x8_602 + 2048*engid_g2`

* **Where**               : 

    * `$engid_g2(0-7): id17-id24 of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`monpwen`| set "1" to enable bert pw mon| `RW`| `0x0`| `0x0`|
|`[12:10]`|`pwlineid`| OC48 line| `RW`| `0x0`| `0x0`|
|`[9:0]`|`masterid`| chanel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Ho Bert PW Mon3

* **Description**           

The registers select 1id in line to gen bert data


* **RTL Instant Name**    : `sel_ho_bert_pw_mon_g3`

* **Address**             : `0x8_603 - 0xB_E03`

* **Formula**             : `0x8_603 + 2048*engid_g3`

* **Where**               : 

    * `$engid_g3(0-7): id25-id32 of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:14]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[13]`|`monpwen`| set "1" to enable bert pw mon| `RW`| `0x0`| `0x0`|
|`[12:10]`|`pwlineid`| OC48 line| `RW`| `0x0`| `0x0`|
|`[9:0]`|`masterid`| chanel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Mode Bert TDM mon

* **Description**           

The registers select mode bert mon in tdm side


* **RTL Instant Name**    : `ctrl_pen_tdm_mon`

* **Address**             : `0x8_520 - 0xB_D3F`

* **Formula**             : `0x8_520 + 2048*slcid + engid`

* **Where**               : 

    * `$slcid(0-7) slice id`

    * `$engid(0-31): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[19:16]`|`thrhold_pattern_sync`| minimum number of byte sync to go sync state| `RW`| `0x0`| `0xA`|
|`[15:12]`|`thrhold_error`| maximum err in state sync, if more than thrhold go loss sync| `RW`| `0x0`| `0x2`|
|`[11:10]`|`reservee`| reserve| `RW`| `0x0`| `0x0`|
|`[09]`|`swapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[08]`|`invmode`| invert data| `RW`| `0x0`| `0x0`|
|`[07:00]`|`patt_mode`| sel pattern gen # 0x01 : all1 # 0x02 : all0 # 0x04 : prbs15 # 0x08 : prbs20r # 0x10 : prbs20 # 0x20 : prss23 # 0x40 : prbs31| `RW`| `0x0`| `0x0 End: Begin:`|

###Sel Mode Bert PW mon

* **Description**           

The registers select mode bert mon in pw side


* **RTL Instant Name**    : `ctrl_pen_pw_mon`

* **Address**             : `0x8_720 - 0xB_F3f`

* **Formula**             : `0x8_720 + 2048*slcid + engid`

* **Where**               : 

    * `$slcid(0-7) slice id`

    * `$engid(0-31): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[19:16]`|`thrhold_pattern_sync`| minimum number of byte sync to go sync state| `RW`| `0x0`| `0xA`|
|`[15:12]`|`thrhold_error`| maximum err in state sync, if more than thrhold go loss sync| `RW`| `0x0`| `0x2`|
|`[11:10]`|`pw_reserve`| pw reserve| `RW`| `0x0`| `0x0`|
|`[09]`|`swapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[08]`|`invmode`| invert data| `RW`| `0x0`| `0x0`|
|`[07:00]`|`patt_mode`| sel pattern gen # 0x01 : all1 # 0x02 : all0 # 0x04 : prbs15 # 0x08 : prbs20r # 0x10 : prbs20 # 0x20 : prss23 # 0x40 : prbs31| `RW`| `0x0`| `0x0 End: Begin:`|

###TDM loss sync

* **Description**           

The registers indicate bert mon loss sync in tdm side


* **RTL Instant Name**    : `loss_tdm_mon`

* **Address**             : `0x8_51F - 0xB_D1F`

* **Formula**             : `0x8_51F + 2048*slc`

* **Where**               : 

    * `$slc(0-7): slice id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sticky_err`| "1" indicate loss sync| `WC`| `0x0`| `0x0 End: Begin:`|

###TDM mon status

* **Description**           

The registers indicate status of bert mon  in tdm side


* **RTL Instant Name**    : `stt_tdm_mon`

* **Address**             : `0x8_500 - 0xB_D1B`

* **Formula**             : `0x8_500 + 2048*slcid + engid`

* **Where**               : 

    * `$slcid(0-7) slice id`

    * `$engid(0-27): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1:0]`|`tdmstate`| Status [1:0]: Prbs status LOPSTA = 2'd0; SRCSTA = 2'd1; VERSTA = 2'd2; INFSTA = 2'd3;| `RO`| `0x0`| `0x0 End: Begin:`|

###PW loss sync

* **Description**           

The registers indicate bert mon loss sync in pw side


* **RTL Instant Name**    : `loss_pw_mon`

* **Address**             : `0x8_71F - 0xB_F1F`

* **Formula**             : `0x8_71F + 2048*engid`

* **Where**               : 

    * `$engid(0-7): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0:0]`|`sticky_err`| "1" indicate loss sync| `WC`| `0x0`| `0x0 End: Begin:`|

###PW mon stt

* **Description**           

The registers indicate bert mon state in pw side


* **RTL Instant Name**    : `stt_pw_mon`

* **Address**             : `0x8_700 - 0xB_F1B`

* **Formula**             : `0x8_700 + 2048*engid`

* **Where**               : 

    * `$engid(0-7): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1:0]`|`pwstate`| Status [1:0]: Prbs status LOPSTA = 2'd0; SRCSTA = 2'd1; VERSTA = 2'd2; INFSTA = 2'd3;| `RO`| `0x0`| `0x0 End: Begin:`|

###counter good bit TDM mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `goodbit_pen_tdm_mon`

* **Address**             : `0x8_580 - 0xB_D9F`

* **Formula**             : `0x8_580 + 2048*slcid + engid`

* **Where**               : 

    * `$slcid(0-7) slice id`

    * `$engid(0-27): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_goodbit`| counter goodbit| `R2C`| `0x0`| `0x0 End: Begin:`|

###counter error bit in TDM mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `err_pen_tdm_mon`

* **Address**             : `0x8_540 - 0xB_D5F`

* **Formula**             : `0x8_540 + 2048*slcid + engid`

* **Where**               : 

    * `$slcid(0-7) slice id`

    * `$engid(0-27): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_errbit`| counter err bit| `R2C`| `0x0`| `0x0 End: Begin:`|

###Counter loss bit in TDM mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `lossbit_pen_tdm_mon`

* **Address**             : `0x8_5C0 - 0xB_DDf`

* **Formula**             : `0x8_5C0 + 2048*slcid + engid`

* **Where**               : 

    * `$slcid(0-7) slice id`

    * `$engid(0-27): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_errbit`| counter err bit| `R2C`| `0x0`| `0x0 End: Begin:`|

###counter good bit TDM mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `goodbit_pen_pw_mon`

* **Address**             : `0x8_780 - 0xB_F9F`

* **Formula**             : `0x8_780 + 2048*slcid + engid`

* **Where**               : 

    * `$slcid(0-7) slice id`

    * `$engid(0-27): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_pwgoodbit`| counter goodbit| `R2C`| `0x0`| `0x0 End: Begin:`|

###counter error bit in TDM mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `errbit_pen_pw_mon`

* **Address**             : `0x8_740 - 0xB_F5F`

* **Formula**             : `0x8_740 + 2048*slcid + engid`

* **Where**               : 

    * `$slcid(0-7) slice id`

    * `$engid(0-27): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_pwerrbit`| counter err bit| `R2C`| `0x0`| `0x0 End: Begin:`|

###Counter loss bit in TDM mon

* **Description**           

The registers count goodbit in  mon tdm side


* **RTL Instant Name**    : `lossbit_pen_pw_mon`

* **Address**             : `0x8_7C0 - 0xB_FDF`

* **Formula**             : `0x8_7C0 + 2048*slcid + engid`

* **Where**               : 

    * `$slcid(0-7) slice id`

    * `$engid(0-27): id of HO_BERT`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cnt_pwlossbi`| counter loss bit| `R2C`| `0x0`| `0x0 End: Begin:`|

###RAM Map Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_Map_Parity_Force_Control`

* **Address**             : `0x4808`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`mapslc7ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice7"| `RW`| `0x0`| `0x0`|
|`[6]`|`mapslc6ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice6"| `RW`| `0x0`| `0x0`|
|`[5]`|`mapslc5ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice5"| `RW`| `0x0`| `0x0`|
|`[4]`|`mapslc4ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice4"| `RW`| `0x0`| `0x0`|
|`[3]`|`mapslc3ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice3"| `RW`| `0x0`| `0x0`|
|`[2]`|`mapslc2ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice2"| `RW`| `0x0`| `0x0`|
|`[1]`|`mapslc1ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice1"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapslc0ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice0"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM Map Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_Map_Parity_Disable_Control`

* **Address**             : `0x4809`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`mapslc7ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice7"| `RW`| `0x0`| `0x0`|
|`[6]`|`mapslc6ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice6"| `RW`| `0x0`| `0x0`|
|`[5]`|`mapslc5ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice5"| `RW`| `0x0`| `0x0`|
|`[4]`|`mapslc4ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice4"| `RW`| `0x0`| `0x0`|
|`[3]`|`mapslc3ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice3"| `RW`| `0x0`| `0x0`|
|`[2]`|`mapslc2ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice2"| `RW`| `0x0`| `0x0`|
|`[1]`|`mapslc1ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice1"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapslc0ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice0"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM Map parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_Map_Parity_Error_Sticky`

* **Address**             : `0x480a`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7]`|`mapslc7ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice7"| `RW`| `0x0`| `0x0`|
|`[6]`|`mapslc6ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice6"| `RW`| `0x0`| `0x0`|
|`[5]`|`mapslc5ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice5"| `RW`| `0x0`| `0x0`|
|`[4]`|`mapslc4ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice4"| `RW`| `0x0`| `0x0`|
|`[3]`|`mapslc3ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice3"| `RW`| `0x0`| `0x0`|
|`[2]`|`mapslc2ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice2"| `RW`| `0x0`| `0x0`|
|`[1]`|`mapslc1ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice1"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapslc0ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice0"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM DeMap Parity Force Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_DeMap_Parity_Force_Control`

* **Address**             : `0x0808`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`mapslc7ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice7"| `RW`| `0x0`| `0x0`|
|`[6]`|`mapslc6ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice6"| `RW`| `0x0`| `0x0`|
|`[5]`|`mapslc5ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice5"| `RW`| `0x0`| `0x0`|
|`[4]`|`mapslc4ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice4"| `RW`| `0x0`| `0x0`|
|`[3]`|`mapslc3ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice3"| `RW`| `0x0`| `0x0`|
|`[2]`|`mapslc2ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice2"| `RW`| `0x0`| `0x0`|
|`[1]`|`mapslc1ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice1"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapslc0ctrl_parerrfrc`| Force parity For RAM Control "Thalassa Map Line Control Slice0"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM DeMap Parity Disable Control

* **Description**           

This register configures force parity for internal RAM


* **RTL Instant Name**    : `RAM_DeMap_Parity_Disable_Control`

* **Address**             : `0x0809`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7]`|`mapslc7ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice7"| `RW`| `0x0`| `0x0`|
|`[6]`|`mapslc6ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice6"| `RW`| `0x0`| `0x0`|
|`[5]`|`mapslc5ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice5"| `RW`| `0x0`| `0x0`|
|`[4]`|`mapslc4ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice4"| `RW`| `0x0`| `0x0`|
|`[3]`|`mapslc3ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice3"| `RW`| `0x0`| `0x0`|
|`[2]`|`mapslc2ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice2"| `RW`| `0x0`| `0x0`|
|`[1]`|`mapslc1ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice1"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapslc0ctrl_parerrdis`| Disable parity For RAM Control "Thalassa Map Line Control Slice0"| `RW`| `0x0`| `0x0 End: Begin:`|

###RAM DeMap parity Error Sticky

* **Description**           

This register configures disable parity for internal RAM


* **RTL Instant Name**    : `RAM_DeMap_Parity_Error_Sticky`

* **Address**             : `0x080a`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7]`|`mapslc7ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice7"| `RW`| `0x0`| `0x0`|
|`[6]`|`mapslc6ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice6"| `RW`| `0x0`| `0x0`|
|`[5]`|`mapslc5ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice5"| `RW`| `0x0`| `0x0`|
|`[4]`|`mapslc4ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice4"| `RW`| `0x0`| `0x0`|
|`[3]`|`mapslc3ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice3"| `RW`| `0x0`| `0x0`|
|`[2]`|`mapslc2ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice2"| `RW`| `0x0`| `0x0`|
|`[1]`|`mapslc1ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice1"| `RW`| `0x0`| `0x0`|
|`[0]`|`mapslc0ctrl_parerrstk`| Error parity For RAM Control "Thalassa Map Line Control Slice0"| `RW`| `0x0`| `0x0 End:`|

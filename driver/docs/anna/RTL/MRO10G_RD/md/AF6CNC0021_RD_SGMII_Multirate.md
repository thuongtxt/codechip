## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CNC0021_RD_SGMII_Multirate
####Register Table

|Name|Address|
|-----|-----|
|`Pedit Block Version`|`0x0800`|
|`Auto-neg-group0 mode`|`0x0801`|
|`Auto-neg-group0 reset`|`0x0802`|
|`Auto-neg-group0 enable`|`0x0803`|
|`Config_speed-group0`|`0x0804`|
|`Auto-neg-group00 status`|`0x0805`|
|`Link-status-group0`|`0x0808`|
|`Auto-neg RX00-RX01 SGMII ability`|`0x0A00`|
|`Auto-neg RX02-RX03 SGMII ability`|`0x0A01`|
|`Auto-neg RX04-RX05 SGMII ability`|`0x0A02`|
|`Auto-neg RX06-RX07 SGMII ability`|`0x0A03`|
|`Auto-neg RX00-RX01 1000Basex ability`|`0x0A00`|
|`Auto-neg RX02-RX03 1000Basex ability`|`0x0A01`|
|`Auto-neg RX04-RX05 1000Basex ability`|`0x0A02`|
|`Auto-neg RX06-RX07 1000Basex ability`|`0x0A03`|
|`GE Loss Of Synchronization Sticky`|`0x080B`|
|`GE Loss Of Synchronization Status`|`0x080A`|
|`GE Loss Of Synchronization Interrupt Enb`|`0x0818`|
|`GE Auto-neg Sticky`|`0x0807`|
|`GE Auto-neg Interrupt Enb`|`0x0817`|
|`GE Remote Fault Sticky`|`0x0819`|
|`GE Remote Fault Interrupt Enb`|`0x081A`|
|`GE Excessive Error Ratio Status`|`0x081C`|
|`GE Excessive Error Ratio Sticky`|`0x081D`|
|`GE Excessive Error Ratio Enble`|`0x081E`|
|`GE Interrupt OR`|`0x0830`|
|`GE Loss Of Sync AND MASK`|`0x0831`|
|`GE Auto_neg State Change AND MASK`|`0x0832`|
|`GE Remote Fault AND MASK`|`0x0833`|
|`GE Excessive Error Ratio AND MASK`|`0x0834`|
|`GE 1000basex Force K30_7`|`0x0822`|
|`GE_TenG Excessive Error Timer`|`0x0837`|
|`GE_TenG Excessive Error Threshold Group0`|`0x0835`|
|`GE_TenG Excessive Error Threshold Group1`|`0x0838`|
|`ENABLE TX 100base FX`|`0x0A08`|
|`Sticky Rx PATTERN DETECT`|`0x0A09`|
|`Sticky Rx CODE ERROR DETECT`|`0x0A0A`|


###Pedit Block Version

* **Description**           

Pedit Block Version


* **RTL Instant Name**    : `version_pen`

* **Address**             : `0x0800`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`day`| day| `R_O`| `0x0E`| `0x0E`|
|`[23:16]`|`month`| month| `R_O`| `0x0C`| `0x0C`|
|`[15:08]`|`year`| year| `R_O`| `0x0D`| `0x0D`|
|`[07:00]`|`number`| number| `R_O`| `0x01`| `0x01`|

###Auto-neg-group0 mode

* **Description**           

Select Auto-neg mode for 1000Basex or SGMII


* **RTL Instant Name**    : `an_mod_pen0`

* **Address**             : `0x0801`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`tx_enb0`| Enable TX side bit per port, bit[16] port 0, bit[31] port 15<br>{1} : enable <br>{0} : disable| `R/W`| `0x0`| `0x0`|
|`[15:00]`|`an_mod0`| Auto-neg mode, bit per port, bit[0] port 0, bit[15] port 15<br>{1} : SGMII <br>{0} : 1000basex| `R/W`| `0x0`| `0x0`|

###Auto-neg-group0 reset

* **Description**           

Restart Auto-neg process of group0 from port 0 to port 15


* **RTL Instant Name**    : `an_rst_pen0`

* **Address**             : `0x0802`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`an_rst0`| Auto-neg reset, sw write 1 then write 0 to restart auto-neg bit per port, bit[0] port 0, bit[15] port 15<br>{1} : restart on <br>{0} : restart off| `R/W`| `0x0`| `0x0`|

###Auto-neg-group0 enable

* **Description**           

enable/disbale Auto-neg of group 0 from port 0 to port 15


* **RTL Instant Name**    : `an_enb_pen0`

* **Address**             : `0x0803`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:00]`|`an_enb0`| Auto-neg enb , bit per port, bit[0] port 0, bit[15] port 15<br>{1} : enable Auto-neg <br>{0} : disable Auto-neg| `R/W`| `0xFFFF`| `0xFFFF`|

###Config_speed-group0

* **Description**           

configure speed when disable Auto-neg of group 0 from port 0 to 15


* **RTL Instant Name**    : `an_spd_pen0`

* **Address**             : `0x0804`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`cfg_spd15`| Speed port 15<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[29:28]`|`cfg_spd14`| Speed port 14<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[27:26]`|`cfg_spd13`| Speed port 13<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[25:24]`|`cfg_spd12`| Speed port 12<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[23:22]`|`cfg_spd11`| Speed port 11<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[21:20]`|`cfg_spd10`| Speed port 10<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[19:18]`|`cfg_spd09`| Speed port 9<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[17:16]`|`cfg_spd08`| Speed port 8<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[15:14]`|`cfg_spd07`| Speed port 7<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[13:12]`|`cfg_spd06`| Speed port 6<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[11:10]`|`cfg_spd05`| Speed port 5<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[09:08]`|`cfg_spd04`| Speed port 4<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[07:06]`|`cfg_spd03`| Speed port 3<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[05:04]`|`cfg_spd02`| Speed port 2<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[03:02]`|`cfg_spd01`| Speed port 1<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|
|`[01:00]`|`cfg_spd00`| Speed port 0<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R/W`| `0x2`| `0x2`|

###Auto-neg-group00 status

* **Description**           

status of Auto-neg of group0 from port 0 to port 7


* **RTL Instant Name**    : `an_sta_pen00`

* **Address**             : `0x0805`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:28]`|`an_sta07`| Speed port 7<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[27:24]`|`an_sta06`| Speed port 6<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[23:20]`|`an_sta05`| Speed port 5<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[19:16]`|`an_sta04`| Speed port 4<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[15:12]`|`an_sta03`| Speed port 3<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[11:08]`|`an_sta02`| Speed port 2<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[07:04]`|`an_sta01`| Speed port 1<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|
|`[03:00]`|`an_sta00`| Speed port 0<br>{15-8} : unused <br>{8} : LINK_OK <br>{7} : IDLE_DETECT <br>{6} : NEXT_PAGE_WAIT <br>{5} : COMPLETE_ACKNOWLEDGE <br>{4} : ACKNOWLEDGE_DETECT <br>{3} : ABILITY_DETECT <br>{2} : AN_DISABLE_LINK_OK <br>{1} : AN_RESTART <br>{0} : AN_ENABLE| `R_O`| `0x0`| `0x0`|

###Link-status-group0

* **Description**           

Link status of group 0 from port 0  to port 15


* **RTL Instant Name**    : `lnk_sync_pen0`

* **Address**             : `0x0808`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:00]`|`lnk_sta0`| Link status, bit per port, bit[0] port  0, bit[7] port 7<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|

###Auto-neg RX00-RX01 SGMII ability

* **Description**           

RX ability of RX-Port00-01 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen00`

* **Address**             : `0x0A00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`link01`| Link status of port1<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[30:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`duplex01`| duplex mode of port1<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[27:26]`|`speed01`| speed of SGMII of port1<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[25:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`link00`| Link status of port0<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[14:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`duplex00`| duplex mode of port0<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[11:10]`|`speed00`| speed of SGMII of port0<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[09:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX02-RX03 SGMII ability

* **Description**           

RX ability of RX-Port02-03 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen01`

* **Address**             : `0x0A01`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`link03`| Link status of port3<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[30:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`duplex03`| duplex mode of port3<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[27:26]`|`speed03`| speed of SGMII of port3<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[25:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`link02`| Link status of port2<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[14:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`duplex02`| duplex mode of port2<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[11:10]`|`speed02`| speed of SGMII of port2<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[09:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX04-RX05 SGMII ability

* **Description**           

RX ability of RX-Port04-05 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen02`

* **Address**             : `0x0A02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`link05`| Link status of port5<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[30:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`duplex05`| duplex mode of port5<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[27:26]`|`speed05`| speed of SGMII of port5<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[25:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`link04`| Link status of port4<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[14:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`duplex04`| duplex mode of port4<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[11:10]`|`speed04`| speed of SGMII of port4<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[09:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX06-RX07 SGMII ability

* **Description**           

RX ability of RX-Port06-07 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen03`

* **Address**             : `0x0A03`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`link07`| Link status of port7<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[30:29]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[28:28]`|`duplex07`| duplex mode of port7<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[27:26]`|`speed07`| speed of SGMII of port7<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[25:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`link06`| Link status of port6<br>{1} : link up <br>{0} : link down| `R_O`| `0x0`| `0x0`|
|`[14:13]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[12:12]`|`duplex06`| duplex mode of port6<br>{1} : full duplex <br>{0} : half duplex| `R_O`| `0x0`| `0x0`|
|`[11:10]`|`speed06`| speed of SGMII of port6<br>{3} : unused <br>{2} : 1000 Mbps <br>{1} : 100  Mbps <br>{0} : 10   Mbps| `R_O`| `0x0`| `0x0`|
|`[09:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX00-RX01 1000Basex ability

* **Description**           

RX ability of RX-Port00-01 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen00`

* **Address**             : `0x0A00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`np01`| Next page of port1| `R_O`| `0x0`| `0x0`|
|`[30:30]`|`ack01`| Acknowledge of port1| `R_O`| `0x0`| `0x0`|
|`[29:28]`|`refault01`| Remote Fault of port1<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[27:23]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`hduplex01`| Half duplex of port1| `R_O`| *n/a*| *n/a*|
|`[21:21]`|`fduplex01`| Full duplex of port1| `R_O`| *n/a*| *n/a*|
|`[20:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`np00`| Next page of port0| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`ack00`| Acknowledge of port0| `R_O`| `0x0`| `0x0`|
|`[13:12]`|`refault00`| Remote Fault of port0<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[11:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`hduplex00`| Half duplex of port0| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fduplex00`| Full duplex of port0| `R_O`| `0x0`| `0x0`|
|`[04:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX02-RX03 1000Basex ability

* **Description**           

RX ability of RX-Port02-03 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen01`

* **Address**             : `0x0A01`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`np03`| Next page  of port3| `R_O`| `0x0`| `0x0`|
|`[30:30]`|`ack03`| Acknowledge of port3| `R_O`| `0x0`| `0x0`|
|`[29:28]`|`refault03`| Remote Fault of port3<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[27:23]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`hduplex03`| Half duplex of port3| `R_O`| *n/a*| *n/a*|
|`[21:21]`|`fduplex03`| Full duplex of port3| `R_O`| *n/a*| *n/a*|
|`[20:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`np02`| Next page of port2| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`ack02`| Acknowledge of port2| `R_O`| `0x0`| `0x0`|
|`[13:12]`|`refault02`| Remote Fault of port2<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[11:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`hduplex02`| Half duplex of port2| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fduplex02`| Full duplex of port2| `R_O`| `0x0`| `0x0`|
|`[04:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX04-RX05 1000Basex ability

* **Description**           

RX ability of RX-Port04-05 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen02`

* **Address**             : `0x0A02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`np05`| Next page  of port5| `R_O`| `0x0`| `0x0`|
|`[30:30]`|`ack05`| Acknowledge of port5| `R_O`| `0x0`| `0x0`|
|`[29:28]`|`refault05`| Remote Fault of port5<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[27:23]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`hduplex05`| Half duplex of port5| `R_O`| *n/a*| *n/a*|
|`[21:21]`|`fduplex05`| Full duplex of port5| `R_O`| *n/a*| *n/a*|
|`[20:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`np04`| Next page of port4| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`ack04`| Acknowledge of port4| `R_O`| `0x0`| `0x0`|
|`[13:12]`|`refault04`| Remote Fault of port4<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[11:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`hduplex04`| Half duplex of port4| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fduplex04`| Full duplex of port4| `R_O`| `0x0`| `0x0`|
|`[04:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###Auto-neg RX06-RX07 1000Basex ability

* **Description**           

RX ability of RX-Port06-07 after Auto-neg succeed


* **RTL Instant Name**    : `an_rxab_pen03`

* **Address**             : `0x0A03`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`np07`| Next page  of port7| `R_O`| `0x0`| `0x0`|
|`[30:30]`|`ack07`| Acknowledge of port7| `R_O`| `0x0`| `0x0`|
|`[29:28]`|`refault07`| Remote Fault of port7<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[27:23]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[22:22]`|`hduplex07`| Half duplex of port7| `R_O`| *n/a*| *n/a*|
|`[21:21]`|`fduplex07`| Full duplex of port7| `R_O`| *n/a*| *n/a*|
|`[20:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:15]`|`np06`| Next page of port6| `R_O`| `0x0`| `0x0`|
|`[14:14]`|`ack06`| Acknowledge of port6| `R_O`| `0x0`| `0x0`|
|`[13:12]`|`refault06`| Remote Fault of port6<br>{0} : No error, Link Ok <br>{1} : Link Failure <br>{2} : Offline <br>{3} : Auto-neg error| `R_O`| `0x0`| `0x0`|
|`[11:07]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[06:06]`|`hduplex06`| Half duplex of port6| `R_O`| `0x0`| `0x0`|
|`[05:05]`|`fduplex06`| Full duplex of port6| `R_O`| `0x0`| `0x0`|
|`[04:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###GE Loss Of Synchronization Sticky

* **Description**           

Sticky state change Loss of synchronization


* **RTL Instant Name**    : `los_sync_stk_pen`

* **Address**             : `0x080B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`gelossofsync_stk`| Sticky state change of Loss Of Synchronization, bit per port, bit0 is port0, bit7 is port7| `W1C`| `0x0`| `0x0`|

###GE Loss Of Synchronization Status

* **Description**           

Current status of Loss of synchronization


* **RTL Instant Name**    : `los_sync_pen`

* **Address**             : `0x080A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`gelossofsync_cur`| Current status of Loss Of Synchronization, bit per port, bit0 is port0, bit7 is port7| `R_O`| `0x0`| `0x0`|

###GE Loss Of Synchronization Interrupt Enb

* **Description**           

Interrupt enable of Loss of synchronization


* **RTL Instant Name**    : `los_sync_int_enb_pen`

* **Address**             : `0x0818`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`gelossofsync_enb`| Interrupt enable of Loss of synchronization, bit per port, bit0 is port0, bit7 is port7| `R/W`| `0x0`| `0x0`|

###GE Auto-neg Sticky

* **Description**           

Sticky state change Auto-neg


* **RTL Instant Name**    : `an_sta_stk_pen`

* **Address**             : `0x0807`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`geauto_neg_stk`| Sticky state change of Auto-neg , bit per port, bit0 is port0, bit7 is port7| `W1C`| `0x0`| `0x0`|

###GE Auto-neg Interrupt Enb

* **Description**           

Interrupt enable of Loss of synchronization


* **RTL Instant Name**    : `an_int_enb_pen`

* **Address**             : `0x0817`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`geauto_neg_enb`| Interrupt enable of Auto-neg, bit per port, bit0 is port0, bit7 is port7| `R/W`| `0x0`| `0x0`|

###GE Remote Fault Sticky

* **Description**           

Sticky state change Remote Fault


* **RTL Instant Name**    : `an_rfi_stk_pen`

* **Address**             : `0x0819`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`geremote_fault_stk`| Sticky state change of Remote Fault , bit per port, bit0 is port0, bit7 is port7| `W1C`| `0x0`| `0x0`|

###GE Remote Fault Interrupt Enb

* **Description**           

Interrupt enable of Remote Fault


* **RTL Instant Name**    : `an_rfi_int_enb_pen`

* **Address**             : `0x081A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`geremote_fault_enb`| Interrupt enable of Remote Fault, bit per port, bit0 is port0, bit7 is port7| `R/W`| `0x0`| `0x0`|

###GE Excessive Error Ratio Status

* **Description**           

Interrupt Excessive Error Ratio Status


* **RTL Instant Name**    : `rxexcer_sta_pen`

* **Address**             : `0x081C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`rxexcer_sta`| Interrupt status of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7| `RO`| `0x0`| `0x0`|

###GE Excessive Error Ratio Sticky

* **Description**           

Interrupt Excessive Error Ratio Sticky


* **RTL Instant Name**    : `rxexcer_stk_pen`

* **Address**             : `0x081D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`rxexcer_stk`| Interrupt sticky of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7| `W1C`| `0x0`| `0x0`|

###GE Excessive Error Ratio Enble

* **Description**           

Interrupt Excessive Error Ratio Enable


* **RTL Instant Name**    : `rxexcer_enb_pen`

* **Address**             : `0x081E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`rxexcer_enb`| Interrupt Enable of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7| `R/W`| `0x0`| `0x0`|

###GE Interrupt OR

* **Description**           

Interrupt OR of all port per interrupt type


* **RTL Instant Name**    : `GE_Interrupt_OR`

* **Address**             : `0x0830`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[03:03]`|`rxexcer_int_or`| Interrupt OR of Excessive Error Ratio| `R_O`| `0x0`| `0x0`|
|`[02:02]`|`an_rfi_int_or`| Interrupt OR of Remote fault| `R_O`| `0x0`| `0x0`|
|`[01:01]`|`ant_int_or`| Interrupt OR of Auto-neg state change| `R_O`| `0x0`| `0x0`|
|`[00:00]`|`los_sync_int_or`| Interrupt OR of Loss of sync| `R_O`| `0x0`| `0x0`|

###GE Loss Of Sync AND MASK

* **Description**           

Interrupt Loss of sync current status AND MASK


* **RTL Instant Name**    : `GE_Loss_Of_Sync_AND_MASK`

* **Address**             : `0x0831`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`gelossofsync_and_mask`| Interrupt Loss of sync, bit per port, bit0 is port0, bit7 is port7| `R_O`| `0x0`| `0x0`|

###GE Auto_neg State Change AND MASK

* **Description**           

Interrupt Auto-neg state change current status AND MASK


* **RTL Instant Name**    : `GE_Auto_neg_State_Change_AND_MASK`

* **Address**             : `0x0832`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`geauto_neg_and_mask`| Interrupt Auto-neg state change, bit per port, bit0 is port0, bit7 is port7| `R_O`| `0x0`| `0x0`|

###GE Remote Fault AND MASK

* **Description**           

Interrupt Remote Fault current status AND MASK


* **RTL Instant Name**    : `GE_Remote_Fault_AND_MASK`

* **Address**             : `0x0833`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`geremote_fault_and_mask`| Interrupt Enable of Remote Fault, bit per port, bit0 is port0, bit7 is port7| `R_O`| `0x0`| `0x0`|

###GE Excessive Error Ratio AND MASK

* **Description**           

Interrupt Excessive Error Ratio current status AND MASK


* **RTL Instant Name**    : `GE_Excessive_Error_Ratio_AND_MASK`

* **Address**             : `0x0834`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`rxexcer_and_mask`| Interrupt Enable of Excessive Error Ratio, bit per port, bit0 is port0, bit7 is port7| `R_O`| `0x0`| `0x0`|

###GE 1000basex Force K30_7

* **Description**           

Forcing countinuous K30.7 for 1000basex mode


* **RTL Instant Name**    : `GE_1000basex_Force_K30_7`

* **Address**             : `0x0822`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `R_O`| `0x0`| `0x0`|
|`[07:00]`|`tx_fk30_7`| Force K30.7 code, bit per port, bit0 is port0, bit7 is port7<br>{1} : enable force K30.7| `R/W`| `0x0`| `0x0`|

###GE_TenG Excessive Error Timer

* **Description**           

Configurate excessive error timer, this reg should be controlled by HW


* **RTL Instant Name**    : `GE_TenG_Excessive_Error_Timer`

* **Address**             : `0x0837`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`rxexcer_timer_grp1`| Timer for group 1 , port9 to port16, this group is use for MRO20G project the default value is 0xF422, it will be 1ms for 1GE mode, and 400us for 10Ge mode| `R/W`| `0xF422`| `0xF422`|
|`[15:00]`|`rxexcer_timer_grp0`| Timer for group 0 , port1 to port8, the default value is 0xF422, it will be 1ms for 1GE mode, and 400us for 10Ge mode| `R/W`| `0xF422`| `0xF422`|

###GE_TenG Excessive Error Threshold Group0

* **Description**           

GE_TenG Excessive Error Threshold Group0


* **RTL Instant Name**    : `GE_TenG_Excessive_Error_Threshold_Group0`

* **Address**             : `0x0835`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`rxexcer_timer_uthres0`| Upper threshold for group 0 , port1 to port8, when error counter is greater than this threshold then current status of excessive error will be set. sw should config 0x11 for 1Ge mode, and 0x7 for 10Ge mode| `R/W`| `0x11`| `0x11`|
|`[15:00]`|`rxexcer_timer_lthres0`| Lower threshold for group 0 , port1 to port8, when error counter is less than this threshold then current status of excessive error will be clear. sw should config 0x1 for 1Ge mode, and 0x1 for 10Ge mode| `R/W`| `0x1`| `0x1`|

###GE_TenG Excessive Error Threshold Group1

* **Description**           

GE_TenG Excessive Error Threshold Group1


* **RTL Instant Name**    : `GE_TenG_Excessive_Error_Threshold_Group1`

* **Address**             : `0x0838`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`rxexcer_timer_uthres1`| Upper threshold for group 1 , port9 to port16, when error counter is greater than this threshold then current status of excessive error will be set. sw should config 0x11 for 1Ge mode, and 0x7 for 10Ge mode| `R/W`| `0x11`| `0x11`|
|`[15:00]`|`rxexcer_timer_lthres1`| Lower threshold for group 1 , port9 to port16, when error counter is less than this threshold then current status of excessive error will be clear. sw should config 0x1 for 1Ge mode, and 0x1 for 10Ge mode| `R/W`| `0x1`| `0x1`|

###ENABLE TX 100base FX

* **Description**           

Interrupt enable of Remote Fault


* **RTL Instant Name**    : `fx_txenb`

* **Address**             : `0x0A08`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:00]`|`fx_entx`| (1) is enable, (0) is disable,  bit0 is port0, bit15 is port15| `R/W`| `0x0`| `0x0`|

###Sticky Rx PATTERN DETECT

* **Description**           

Interrupt enable of Remote Fault


* **RTL Instant Name**    : `fx_patstken`

* **Address**             : `0x0A09`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:00]`|`fx_patstk`| sticky pattern detect| `R/W`| `0x0`| `0x0`|

###Sticky Rx CODE ERROR DETECT

* **Description**           

Interrupt enable of Remote Fault


* **RTL Instant Name**    : `fx_codestken`

* **Address**             : `0x0A0A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:00]`|`fx_codeerrstk`| sticky code error detect| `R/W`| `0x0`| `0x0`|

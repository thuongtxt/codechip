## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2016-09-27|Project|Initial version|




##ETH40G_RD
####Register Table

|Name|Address|
|-----|-----|
|`ETH 40G DRP`|`0x1000-0x1FFF`|
|`ETH 40G LoopBack`|`0x0002`|
|`ETH 40G QLL Status`|`0x000B`|
|`ETH 40G TX Reset`|`0x000C`|
|`ETH 40G RX Reset`|`0x000D`|
|`ETH 40G LPMDFE Mode`|`0x000E`|
|`ETH 40G LPMDFE Reset`|`0x000F`|
|`ETH 40G TXDIFFCTRL`|`0x0010`|
|`ETH 40G TXPOSTCURSOR`|`0x0011`|
|`ETH 40G TXPRECURSOR`|`0x0012`|
|`ETH 40G Ctrl FCS`|`0x0040`|
|`ETH 40G AutoNeg`|`0x0041`|
|`ETH 40G Diag Ctrl0`|`0x0014`|
|`ETH 40G Diag Ctrl1`|`0x0015`|
|`ETH 40G Diag Ctrl2`|`0x0016`|
|`ETH 40G Diag Ctrl3`|`0x0017`|
|`ETH 40G Diag Ctrl4`|`0x0018`|
|`ETH 40G Diag Sta0`|`0x001A`|
|`ETH 40G Diag TXPKT`|`0x001B`|
|`ETH 40G Diag TXNOB`|`0x001C`|
|`ETH 40G Diag RXPKT`|`0x001D`|
|`ETH 40G Diag RXNOB`|`0x001E`|


###ETH 40G DRP

* **Description**           

Read/Write DRP address of SERDES


* **RTL Instant Name**    : `OETH_40G_DRP`

* **Address**             : `0x1000-0x1FFF`

* **Formula**             : `0x1000+$P*0x400+$DRP`

* **Where**               : 

    * `$P(0-3) : Lane ID`

    * `$DRP(0-1023) : DRP address, see UG578`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:00]`|`drp_rw`| DRP read/write value| `R/W`| `0x0`| `0x0`|

###ETH 40G LoopBack

* **Description**           

Configurate LoopBack


* **RTL Instant Name**    : `ETH_40G_LoopBack`

* **Address**             : `0x0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`lpback_lane3`| Loopback lane3<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[11:08]`|`lpback_lane2`| Loopback lane2<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[07:04]`|`lpback_lane1`| Loopback lane1<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`lpback_lane0`| Loopback lane0<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|

###ETH 40G QLL Status

* **Description**           

QPLL status


* **RTL Instant Name**    : `ETH_40G_QLL_Status`

* **Address**             : `0x000B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:29]`|`qpll1_lock_change`| QPLL1 has transition lock/unlock, Group 0-3<br>{1} : QPLL1_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[28:28]`|`qpll0_lock_change`| QPLL0 has transition lock/unlock, Group 0-3<br>{1} : QPLL0_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[27:26]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[25:25]`|`qpll1_lock`| QPLL0 is Locked, Group 0-3<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[24:24]`|`qpll0_lock`| QPLL0 is Locked, Group 0-3<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[23:00]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|

###ETH 40G TX Reset

* **Description**           

Reset TX SERDES


* **RTL Instant Name**    : `ETH_40G_TX_Reset`

* **Address**             : `0x000C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`txrst_done`| TX Reset Done<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`txrst_trig`| Trige 0->1 to start reset TX SERDES| `R/W`| `0x0`| `0x0`|

###ETH 40G RX Reset

* **Description**           

Reset RX SERDES


* **RTL Instant Name**    : `ETH_49G_RX_Reset`

* **Address**             : `0x000D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[16:16]`|`rxrst_done`| RX Reset Done<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`rxrst_trig`| Trige 0->1 to start reset RX SERDES| `R/W`| `0x0`| `0x0`|

###ETH 40G LPMDFE Mode

* **Description**           

Configure LPM/DFE mode


* **RTL Instant Name**    : `ETH_40G_LPMDFE_Mode`

* **Address**             : `0x000E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`lpmdfe_mode`| LPM/DFE mode<br>{0} : DFE mode <br>{1} : LPM mode| `R/W`| `0x0`| `0x0`|

###ETH 40G LPMDFE Reset

* **Description**           

Reset LPM/DFE


* **RTL Instant Name**    : `ETH_40G_LPMDFE_Reset`

* **Address**             : `0x000F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`lpmdfe_reset`| LPM/DFE reset<br>{1} : reset| `R/W`| `0x0`| `0x0`|

###ETH 40G TXDIFFCTRL

* **Description**           

Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail


* **RTL Instant Name**    : `ETH_40G_TXDIFFCTRL`

* **Address**             : `0x0010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:00]`|`txdiffctrl`| TXDIFFCTRL| `R/W`| `0x18`| `0x18`|

###ETH 40G TXPOSTCURSOR

* **Description**           

Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail


* **RTL Instant Name**    : `ETH_40G_TXPOSTCURSOR`

* **Address**             : `0x0011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:00]`|`txpostcursor`| TXPOSTCURSOR| `R/W`| `0x15`| `0x15`|

###ETH 40G TXPRECURSOR

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail


* **RTL Instant Name**    : `ETH_40G_TXPRECURSOR`

* **Address**             : `0x0012`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:00]`|`txprecursor`| TXPRECURSOR| `R/W`| `0x0`| `0x0`|

###ETH 40G Ctrl FCS

* **Description**           

configure FCS mode


* **RTL Instant Name**    : `ETH_40G_Ctrl_FCS`

* **Address**             : `0x0040`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`txfcs_ignore`| TX ignore check FCS when txfcs_ins is low<br>{1} : ignore| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`txfcs_ins`| TX inserts 4bytes FCS<br>{1} : insert| `R/W`| `0x1`| `0x1`|
|`[03:02]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:01]`|`rxfcs_ignore`| RX ignore check FCS<br>{1} : ignore| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`rxfcs_rmv`| RX remove 4bytes FCS<br>{1} : remove| `R/W`| `0x1`| `0x1`|

###ETH 40G AutoNeg

* **Description**           

configure Auto-Neg


* **RTL Instant Name**    : `ETH_40G_AutoNeg`

* **Address**             : `0x0041`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:08]`|`an_lt_sta`| Link Control outputs from the auto-negotiationcontroller for the various Ethernet protocols.<br>{0} : DISABLE; PCS is disconnected <br>{1} : SCAN_FOR_CARRIER; RX is connected to PCS <br>{2} : not used <br>{3} : ENABLE; PCS is connected for mission mode operation| `R/W`| `0x0`| `0x0`|
|`[07:06]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05:05]`|`lt_restart`| This signal triggers a restart of link training regardless of the current state.<br>{1} : ignore| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`lt_enb`| Enables link training. When link training is disabled, all PCS lanes function in mission mode.<br>{1} : enable| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[02:02]`|`an_restart`| This input is used to trigger a restart of the auto-negotiation, regardless of what state the circuit is currently in.<br>{1} : restart| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`an_bypass`| Input to disable auto-negotiation and bypass the auto-negotiation function. If this input is asserted, auto-negotiation is turned off, but the PCS is connected to the output to allow operation.<br>{1} : bypass| `R/W`| `0x1`| `0x1`|
|`[00:00]`|`an_enb`| Enable signal for auto-negotiation<br>{1} : enable| `R/W`| `0x0`| `0x0`|

###ETH 40G Diag Ctrl0

* **Description**           

Diagnostic control 0


* **RTL Instant Name**    : `ETH_40G_Diag_ctrl0`

* **Address**             : `0x0014`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:25]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[24:24]`|`diag_err`| Error detection<br>{1} : error| `W1C`| `0x0`| `0x0`|
|`[23:21]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[20:20]`|`diag_ferr`| Enable force error data of diagnostic packet<br>{1} : force error| `R/W`| `0x0`| `0x0`|
|`[19:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`diag_enb`| enable diagnostic block<br>{1} : enable| `R/W`| `0x0`| `0x0`|

###ETH 40G Diag Ctrl1

* **Description**           

Diagnostic control 1


* **RTL Instant Name**    : `ETH_40G_Diag_ctrl1`

* **Address**             : `0x0015`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`diag_lenmax`| Maximum length of diagnostic packet| `R/W`| `0x0`| `0x0`|
|`[15:00]`|`diag_lenmin`| Minimum length of diagnostic packet| `R/W`| `0x0`| `0x0`|

###ETH 40G Diag Ctrl2

* **Description**           

Diagnostic control 2


* **RTL Instant Name**    : `ETH_40G_Diag_ctrl2`

* **Address**             : `0x0016`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`diag_dalsb`| 32bit-LSB DA of diagnostic packet| `R/W`| `0x0`| `0x0`|

###ETH 40G Diag Ctrl3

* **Description**           

Diagnostic control 3


* **RTL Instant Name**    : `ETH_40G_Diag_ctrl3`

* **Address**             : `0x0017`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`diag_salsb`| 32bit-LSB SA of diagnostic packet| `R/W`| `0x0`| `0x0`|

###ETH 40G Diag Ctrl4

* **Description**           

Diagnostic control 4


* **RTL Instant Name**    : `ETH_40G_Diag_ctrl4`

* **Address**             : `0x0018`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`diag_damsb`| 16bit-MSB DA of diagnostic packet| `R/W`| `0x0`| `0x0`|
|`[15:00]`|`diag_samsb`| 16bit-MSB SA of diagnostic packet| `R/W`| `0x0`| `0x0`|

###ETH 40G Diag Sta0

* **Description**           

Diagnostic Sta0


* **RTL Instant Name**    : `ETH_40G_Diag_Sta0`

* **Address**             : `0x001A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:07]`|`diag_txmis_sop`| Packet miss SOP| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`diag_txmis_eop`| Packet miss EOP| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`diag_txsop_eop`| Short packet, length is less than 16bytes| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`diag_txwff_ful`| TX-Fifo is full| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`diag_rxmis_sop`| Packet miss SOP| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`diag_rxmis_eop`| Packet miss EOP| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`diag_rxsop_eop`| Short packet, length is less than 16bytes  t| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`diag_rxwff_ful`| RX-Fifo is full| `W1C`| `0x0`| `0x0`|

###ETH 40G Diag TXPKT

* **Description**           

Diagnostic TX packet counter


* **RTL Instant Name**    : `ETH_40G_Diag_TXPKT`

* **Address**             : `0x001B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`diag_txpkt`| TX packet counter| `R2C`| `0x0`| `0x0`|

###ETH 40G Diag TXNOB

* **Description**           

Diagnostic TX number of bytes counter


* **RTL Instant Name**    : `ETH_40G_Diag_TXNOB`

* **Address**             : `0x001C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`diag_txnob`| TX number of byte counter| `R2C`| `0x0`| `0x0`|

###ETH 40G Diag RXPKT

* **Description**           

Diagnostic RX packet counter


* **RTL Instant Name**    : `ETH_40G_Diag_RXPKT`

* **Address**             : `0x001D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`diag_rxpkt`| RX packet counter| `R2C`| `0x0`| `0x0`|

###ETH 40G Diag RXNOB

* **Description**           

Diagnostic RX number of bytes counter


* **RTL Instant Name**    : `ETH_40G_Diag_RXNOB`

* **Address**             : `0x001E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`diag_rxnob`| RX number of byte counter| `R2C`| `0x0`| `0x0`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_INTR
####Register Table

|Name|Address|
|-----|-----|
|`Global Interrupt Status`|`0x00_0002`|
|`Global Interrupt Mask Enable`|`0x00_0003`|
|`Global PDH Interrupt Status`|`0x00_0004`|
|`Global Interrupt Pin Disable`|`0x00_0005`|
|`Global Interrupt Mask Restore`|`0x00_0006`|
|`Global  Configuration RAM Parity Interrupt Mask Enable`|`0x00_0007`|
|`Global  Configuration RAM Parity Interrupt Status`|`0x00_0008`|
|`Global LoCDR Interrupt Status`|`0x00_0009`|
|`Global PMC 2page Interrupt Status`|`0x00_000A`|


###Global Interrupt Status

* **Description**           

This register indicate global interrupt status.


* **RTL Instant Name**    : `global_interrupt_status`

* **Address**             : `0x00_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`extpinintstatus`| External Pin Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[30]`|`extorintstatus`| External OR Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[29:19]`|`reserveintstatus`| Reserve Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[18:17]`|`mac40gintstatus`| Two Mac40G Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[16]`|`upsrintstatus`| UPSR Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[15]`|`tengeintstatus`| Chip SEM Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[14]`|`geintstatus`| Chip SEM Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[13]`|`globalpmc`| Global PMC 2 Page Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[12]`|`semintstatus`| Chip SEM Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[11]`|`hocdrintstatus`| High Order CDR Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[10]`|`locdrintstatus`| Low Order CDR Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[9]`|`mdlintstatus`| MDL Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[8]`|`prmintstatus`| PRM Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[7]`|`pmintstatus`| PM Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[6]`|`fmintstatus`| FM Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[5]`|`tfi5intstatus`| TFI5 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[4]`|`stsintstatus`| STS Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[3]`|`vtintstatus`| VT Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[2]`|`de3intstatus`| DS3/E3 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[1]`|`de1intstatus`| DS1/E1 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[0]`|`pweintstatus`| PWE Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0 End: Begin:`|

###Global Interrupt Mask Enable

* **Description**           

This register configures global interrupt mask enable.


* **RTL Instant Name**    : `global_interrupt_mask_enable`

* **Address**             : `0x00_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `19`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[18:17]`|`mac40gintenable`| Two MAC40G Interrupt Enable  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[16]`|`upsrintenable`| UPSR Interrupt Enable  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[15]`|`tengeintenable`| Ten Ge Interrupt Enable  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[14]`|`geintenable`| Ge Interrupt Enable  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[13]`|`glbpmcintenable`| GLB PMC Interrupt Enable  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[12]`|`semintenable`| Chip SEM Interrupt Enable  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[11]`|`hocdrintenable`| High Order CDR Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[10]`|`locdrintenable`| Low Order CDR Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[9]`|`mdlintenable`| MDL Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[8]`|`prmintenable`| PRM Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[7]`|`pmintenable`| PM Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[6]`|`fmintenable`| FM Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[5]`|`tfi5intenable`| TFI5 Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[4]`|`stsintenable`| STS Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[3]`|`vtintenable`| VT Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[2]`|`de3intenable`| DS3/E3 Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[1]`|`de1intenable`| DS1/E1 Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[0]`|`pweintenable`| PWE Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0 End: Begin:`|

###Global PDH Interrupt Status

* **Description**           

This register indicate global pdh interrupt status.


* **RTL Instant Name**    : `global_pdh_interrupt_status`

* **Address**             : `0x00_0004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23]`|`de3slice12intstatus`| DE3 Slice12 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[22]`|`de3slice11intstatus`| DE3 Slice11 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[21]`|`de3slice10intstatus`| DE3 Slice10 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[20]`|`de3slice9intstatus`| DE3 Slice9 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[19]`|`de3slice8intstatus`| DE3 Slice8 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[18]`|`de3slice7intstatus`| DE3 Slice7 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[17]`|`de3slice6intstatus`| DE3 Slice6 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[16]`|`de3slice5intstatus`| DE3 Slice5 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[15]`|`de3slice4intstatus`| DE3 Slice4 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[14]`|`de3slice3intstatus`| DE3 Slice3 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[13]`|`de3slice2intstatus`| DE3 Slice2 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[12]`|`de3slice1intstatus`| DE3 Slice1 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[11]`|`de1slice12intstatus`| DE1 Slice12 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[10]`|`de1slice11intstatus`| DE1 Slice11 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[9]`|`de1slice10intstatus`| DE1 Slice10 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[8]`|`de1slice9intstatus`| DE1 Slice9 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[7]`|`de1slice8intstatus`| DE1 Slice8 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[6]`|`de1slice7intstatus`| DE1 Slice7 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[5]`|`de1slice6intstatus`| DE1 Slice6 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[4]`|`de1slice5intstatus`| DE1 Slice5 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[3]`|`de1slice4intstatus`| DE1 Slice4 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[2]`|`de1slice3intstatus`| DE1 Slice3 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[1]`|`de1slice2intstatus`| DE1 Slice2 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[0]`|`de1slice1intstatus`| DE1 Slice1 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0 End: Begin:`|

###Global Interrupt Pin Disable

* **Description**           

This register configures global interrupt pin disable


* **RTL Instant Name**    : `global_interrupt_pin_disable`

* **Address**             : `0x00_0005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`glbpinintdisable`| Global Interrupt Pin Disable  1: Disable  0: Enable| `RW`| `0x0`| `0x0 End: Begin:`|

###Global Interrupt Mask Restore

* **Description**           

This register configures global interrupt mask enable.


* **RTL Instant Name**    : `global_interrupt_mask_restore`

* **Address**             : `0x00_0006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `13`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[12]`|`upsrintrestore`| UPSR Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[11]`|`hocdrintrestore`| HoCDR Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[10]`|`locdrintrestore`| LoCDR Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[9]`|`mdlintrestore`| MDL Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[8]`|`prmintrestore`| PRM Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[7]`|`pmintrestore`| PM Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[6]`|`fmintrestore`| FM Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[5]`|`tfi5intrestore`| TFI5 Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[4]`|`stsintrestore`| STS Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[3]`|`vtintrestore`| VT Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[2]`|`de3intrestore`| DS3/E3 Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[1]`|`de1intrestore`| DS1/E1 Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[0]`|`pweintrestore`| PWE Interrupt Restore  1: Set  0: Clear| `RW`| `0x0`| `0x0 End: Begin:`|

###Global  Configuration RAM Parity Interrupt Mask Enable

* **Description**           

This register configures parity global interrupt mask enable.


* **RTL Instant Name**    : `global_parity_interrupt_mask_enable`

* **Address**             : `0x00_0007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Enable`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`prmparintenable`| PRM Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[30]`|`ameparintenable`| AME Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[29]`|`pmcparintenable`| PMC Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[28]`|`claparintenable`| CLA Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[27]`|`pwe3parintenable`| PWE ETH Port3 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[26]`|`pwe2parintenable`| PWE ETH Port2 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[25]`|`pwe1parintenable`| PWE ETH Port1 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[24]`|`pwe0parintenable`| PWE ETH Port0 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[23]`|`pdaparintenable`| PDA Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[22]`|`plaparintenable`| PLA Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[21]`|`cdr6parintenable`| CDR Slice6 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[20]`|`map6parintenable`| MAP Slice6 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[19]`|`pdh6parintenable`| PDH Slice6 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[18]`|`cdr5parintenable`| CDR Slice5 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[17]`|`map5parintenable`| MAP Slice5 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[16]`|`pdh5parintenable`| PDH Slice5 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[15]`|`cdr4parintenable`| CDR Slice4 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[14]`|`map4parintenable`| MAP Slice4 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[13]`|`pdh4parintenable`| PDH Slice4 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[12]`|`cdr3parintenable`| CDR Slice3 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[11]`|`map3parintenable`| MAP Slice3 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[10]`|`pdh3parintenable`| PDH Slice3 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[9]`|`cdr2parintenable`| CDR Slice2 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[8]`|`map2parintenable`| MAP Slice2 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[7]`|`pdh2parintenable`| PDH Slice2 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[6]`|`cdr1parintenable`| CDR Slice1 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[5]`|`map1parintenable`| MAP Slice1 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[4]`|`pdh1parintenable`| PDH Slice1 Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[3]`|`cdrhoparintenable`| CDRHO Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[2]`|`maphoparintenable`| MAPHO Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[1]`|`pohparintenable`| POH Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0`|
|`[0]`|`ocnparintenable`| OCN Parity Interrupt Enable  1: Set  0: Clear| `RW`| `0x0`| `0x0 Begin:`|

###Global  Configuration RAM Parity Interrupt Status

* **Description**           

This register show global parity interrupt status.


* **RTL Instant Name**    : `global_parity_interrupt_status`

* **Address**             : `0x00_0008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`prmparintstatus`| PRM Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[30]`|`ameparintstatus`| AME Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[29]`|`pmcparintstatus`| PMC Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[28]`|`claparintstatus`| CLA Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[27]`|`pwe3parintstatus`| PWE ETH Port3 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[26]`|`pwe2parintstatus`| PWE ETH Port2 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[25]`|`pwe1parintstatus`| PWE ETH Port1 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[24]`|`pwe0parintstatus`| PWE ETH Port0 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[23]`|`pdaparintstatus`| PDA Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[22]`|`plaparintstatus`| PLA Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[21]`|`cdr6parintstatus`| CDR Slice6 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[20]`|`map6parintstatus`| MAP Slice6 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[19]`|`pdh6parintstatus`| PDH Slice6 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[18]`|`cdr5parintstatus`| CDR Slice5 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[17]`|`map5parintstatus`| MAP Slice5 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[16]`|`pdh5parintstatus`| PDH Slice5 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[15]`|`cdr4parintstatus`| CDR Slice4 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[14]`|`map4parintstatus`| MAP Slice4 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[13]`|`pdh4parintstatus`| PDH Slice4 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[12]`|`cdr3parintstatus`| CDR Slice3 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[11]`|`map3parintstatus`| MAP Slice3 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[10]`|`pdh3parintstatus`| PDH Slice3 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[9]`|`cdr2parintstatus`| CDR Slice2 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[8]`|`map2parintstatus`| MAP Slice2 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[7]`|`pdh2parintstatus`| PDH Slice2 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[6]`|`cdr1parintstatus`| CDR Slice1 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[5]`|`map1parintstatus`| MAP Slice1 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[4]`|`pdh1parintstatus`| PDH Slice1 Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[3]`|`cdrhoparintstatus`| CDRHO Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[2]`|`maphoparintstatus`| MAPHO Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[1]`|`pohparintstatus`| POH Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[0]`|`ocnparintstatus`| OCN Parity Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0 End: Begin:`|

###Global LoCDR Interrupt Status

* **Description**           

This register indicate global low order CDR interrupt status.


* **RTL Instant Name**    : `global_locdr_interrupt_status`

* **Address**             : `0x00_0009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `12`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11]`|`locdrslice12intstatus`| LoCDR Slice12 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[10]`|`locdrslice11intstatus`| LoCDR Slice11 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[9]`|`locdrslice10intstatus`| LoCDR Slice10 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[8]`|`locdrslice9intstatus`| LoCDR Slice9 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[7]`|`locdrslice8intstatus`| LoCDR Slice8 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[6]`|`locdrslice7intstatus`| LoCDR Slice7 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[5]`|`locdrslice6intstatus`| LoCDR Slice6 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[4]`|`locdrslice5intstatus`| LoCDR Slice5 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[3]`|`locdrslice4intstatus`| LoCDR Slice4 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[2]`|`locdrslice3intstatus`| LoCDR Slice3 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[1]`|`locdrslice2intstatus`| LoCDR Slice2 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[0]`|`locdrslice1intstatus`| LoCDR Slice1 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0 End: Begin:`|

###Global PMC 2page Interrupt Status

* **Description**           

This register indicate global low order CDR interrupt status.


* **RTL Instant Name**    : `global_pmc_2page_interrupt_status`

* **Address**             : `0x00_000A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `7`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`pmrintstatus`| Pmr     Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[5]`|`pohpmvtintstatus`| Pohpmvt Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[4]`|`pohpmstsintstatus`| Pohpmsts Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[3]`|`pohintstatus`| Poh    Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[2]`|`pdhde1intstatus`| Pdhde1 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[1]`|`pdhde3intstatus`| Pdhde3 Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0`|
|`[0]`|`pwcntintstatus`| Pwcnt  Interrupt Status  1: Set  0: Clear| `RO`| `0x0`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2016-10-04|AF6Project|Initial version|




##AF6CNC0021_RD_EPA_RMVLAN
####Register Table

|Name|Address|
|-----|-----|
|`CONFIG VLAN REMOVED port 1-2`|`0x00`|
|`CONFIG VLAN REMOVED port 3-4`|`0x01`|
|`CONFIG VLAN REMOVED port 5-6`|`0x02`|
|`CONFIG VLAN REMOVED port 7-8`|`0x03`|
|`CONFIG VLAN REMOVED  port 9-10`|`0x04`|
|`CONFIG VLAN REMOVED  port 11-12`|`0x05`|
|`CONFIG VLAN REMOVED port 13-14`|`0x06`|
|`CONFIG VLAN REMOVED port 15-16`|`0x07`|
|`CONFIG ENABLE REMOVE VLAN TPID`|`0x10`|
|`CONFIG ENABLE REMOVE VLAN TPID`|`0x11`|


###CONFIG VLAN REMOVED port 1-2

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_cfgvlan1`

* **Address**             : `0x00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED port 3-4

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_cfgvlan2`

* **Address**             : `0x01`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED port 5-6

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_cfgvlan3`

* **Address**             : `0x02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED port 7-8

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_cfgvlan4`

* **Address**             : `0x03`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED  port 9-10

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_cfgvlan5`

* **Address**             : `0x04`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED  port 11-12

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_cfgvlan6`

* **Address**             : `0x05`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED port 13-14

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_cfgvlan7`

* **Address**             : `0x06`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG VLAN REMOVED port 15-16

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_cfgvlan8`

* **Address**             : `0x07`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`prior_p2`| priority| `R/W`| `0x0`| `0x0`|
|`[28:28]`|`cfi_p2`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[27:16]`|`vlanid_p2`| VLan ID| `R/W`| `0x0`| `0x0`|
|`[15:13]`|`prior_p1`| priority| `R/W`| `0x0`| `0x0`|
|`[12:12]`|`cfi_p1`| CFI bit| `R/W`| `0x1`| `0x1`|
|`[11:00]`|`vlanid_p1`| VLan ID| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE REMOVE VLAN TPID

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_enbtpid`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`out_enbtpid`| bit corresponding port to config ( bit 0 is port 0), (0)  use config TPID1, (1) use config TPID 2| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG ENABLE REMOVE VLAN TPID

* **Description**           

config control Stuff global


* **RTL Instant Name**    : `upen_enbtpid`

* **Address**             : `0x11`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`out_cfgtpid2`| valud TPID 2| `R/W`| `0x0`| `0x0`|
|`[15:0]`|`out_cfgtpid1`| value TPID 1| `R/W`| `0x8100`| `0x8100 End:`|

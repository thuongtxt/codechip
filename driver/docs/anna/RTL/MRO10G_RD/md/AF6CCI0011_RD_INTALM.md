## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_INTALM
####Register Table

|Name|Address|
|-----|-----|
|`Counter Per Alarm Interrupt Enable Control`|`0x060000`|
|`Counter Per Alarm Interrupt Status`|`0x060800`|
|`Counter Per Alarm Current Status`|`0x061000`|
|`Counter Interrupt OR Status`|`0x061800`|
|`Counter per Group Interrupt OR Status`|`0x061BFF`|
|`Counter per Group Interrupt Enable Control`|`0x061BFE`|
|`Counter per Slice Interrupt OR Status`|`0x068000`|


###Counter Per Alarm Interrupt Enable Control

* **Description**           

This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.


* **RTL Instant Name**    : `Counter_Per_Alarm_Interrupt_Enable_Control`

* **Address**             : `0x060000`

* **Formula**             : `0x060000 + Grp2048ID*8192 + GrpID*32 + BitID`

* **Where**               : 

    * `$Grp2048ID(0-2):  Pseudowire ID bits[12:11]`

    * `$GrpID(0-63):  Pseudowire ID bits[10:5]`

    * `$BitID(0-31): Pseudowire ID bits [4:0]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8]`|`straystatechgintren`| Set 1 to enable Stray packet event to generate an interrupt| `RW`| `0x0`| `0x0`|
|`[7]`|`malformstatechgintren`| Set 1 to enable Malform packet event to generate an interrupt| `RW`| `0x0`| `0x0`|
|`[6]`|`mbitstatechgintren`| Set 1 to enable Mbit packet event to generate an interrupt| `RW`| `0x0`| `0x0`|
|`[5]`|`rbitstatechgintren`| Set 1 to enable Rbit packet event to generate an interrupt| `RW`| `0x0`| `0x0`|
|`[4]`|`lofssyncstatechgintren`| Set 1 to enable Lost of frame Sync event to generate an interrupt| `RW`| `0x0`| `0x0`|
|`[3]`|`underrunstatechgintren`| Set 1 to enable change jitter buffer state event from normal to underrun and vice versa in the related pseudowire to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[2]`|`overrunstatechgintren`| Set 1 to enable change jitter buffer state event from normal to overrun and vice versa in the related pseudowire to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[1]`|`lofsstatechgintren`| Set 1 to enable change lost of frame state(LOFS) event from normal to LOFS and vice versa in the related pseudowire to generate an interrupt.| `RW`| `0x0`| `0x0`|
|`[0]`|`lbitstatechgintren`| Set 1 to enable Lbit packet event to generate an interrupt| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter Per Alarm Interrupt Status

* **Description**           

This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.


* **RTL Instant Name**    : `Counter_Per_Alarm_Interrupt_Status`

* **Address**             : `0x060800`

* **Formula**             : `0x060800 + Grp2048ID*8192 +  GrpID*32 + BitID`

* **Where**               : 

    * `$Grp2048ID(0-2):  Pseudowire ID bits[12:11]`

    * `$GrpID(0-63):  Pseudowire ID bits[10:5]`

    * `$BitID(0-31): Pseudowire ID bits [4:0]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8]`|`straystatechgintrsta`| Set 1 when Stray packet event is detected in the pseudowire| `RW`| `0x0`| `0x0`|
|`[7]`|`malformstatechgintrsta`| Set 1 when Malform packet event is detected in the pseudowire| `RW`| `0x0`| `0x0`|
|`[6]`|`mbitstatechgintrsta`| Set 1 when a Mbit packet event is detected in the pseudowire| `RW`| `0x0`| `0x0`|
|`[5]`|`rbitstatechgintrsta`| Set 1 when a Rbit packet event is detected in the pseudowire| `RW`| `0x0`| `0x0`|
|`[4]`|`lofssyncstatechgintrsta`| Set 1 when a lost of frame Sync event is detected in the pseudowire| `RW`| `0x0`| `0x0`|
|`[3]`|`underrunstatechgintrsta`| Set 1 when there is a change in jitter buffer underrun state  in the related pseudowire| `RW`| `0x0`| `0x0`|
|`[2]`|`overrunstatechgintrsta`| Set 1 when there is a change in jitter buffer overrun state in the related pseudowire| `RW`| `0x0`| `0x0`|
|`[1]`|`lofsstatechgintrsta`| Set 1 when there is a change in lost of frame state(LOFS) in the related pseudowire| `RW`| `0x0`| `0x0`|
|`[0]`|`lbitstatechgintrsta`| Set 1 when a Lbit packet event is detected in the pseudowire| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter Per Alarm Current Status

* **Description**           

This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.


* **RTL Instant Name**    : `Counter_Per_Alarm_Current_Status`

* **Address**             : `0x061000`

* **Formula**             : `0x061000 + Grp2048ID*8192 +  GrpID*32 + BitID`

* **Where**               : 

    * `$Grp2048ID(0-2):  Pseudowire ID bits[12:11]`

    * `$GrpID(0-63):  Pseudowire ID bits[10:5]`

    * `$BitID(0-31): Pseudowire ID bits [4:0]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[8]`|`straycurstatus`| Stray packet state current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[7]`|`malformcurstatus`| Malform packet state current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[6]`|`mbitcurstatus`| Mbit packet state current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[5]`|`rbitcurstatus`| Rbit packet state current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[4]`|`lofssynccurstatus`| Lost of frame Sync state current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[3]`|`underruncurstatus`| Jitter buffer underrun current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[2]`|`overruncurstatus`| Jitter buffer overrun current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[1]`|`lofsstatecurstatus`| Lost of frame state current status in the related pseudowire.| `RW`| `0x0`| `0x0`|
|`[0]`|`lbitstatecurstatus`| a Lbit packet state current status in the related pseudowire| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter Interrupt OR Status

* **Description**           

This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen.


* **RTL Instant Name**    : `Counter_Interrupt_OR_Status`

* **Address**             : `0x061800`

* **Formula**             : `0x061800 + Grp2048ID*8192 +  Slice*1024 + GrpID`

* **Where**               : 

    * `$Grp2048ID(0-2):  Pseudowire ID bits[12:11]`

    * `$Slice(0-1):  Pseudowire ID bits[10]`

    * `$GrpID(0-31):  Pseudowire ID bits[9:5]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`introrstatus`| Set to 1 to indicate that there is any interrupt status bit in the Counter per Alarm Interrupt Status register of the related pseudowires to be set and they are enabled to raise interrupt. Bit 0 of GrpID#0 for pseudowire 0, bit 31 of GrpID#0 for pseudowire 31, bit 0 of GrpID#1 for pseudowire 32, respectively.| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter per Group Interrupt OR Status

* **Description**           

The register consists of 8 bits for 8 Group of the PW Counter. Each bit is used to store Interrupt OR status of the related Group.


* **RTL Instant Name**    : `counter_per_group_intr_or_stat`

* **Address**             : `0x061BFF`

* **Formula**             : `0x061BFF + Grp2048ID*8192 +  Slice*1024`

* **Where**               : 

    * `$Grp2048ID(0-2):  Pseudowire ID bits[12:11]`

    * `$Slice(0-1):  Pseudowire ID bits[10]`

* **Width**               : `32`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`groupintrorsta`| Set to 1 if any interrupt bit of corresponding Group is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter per Group Interrupt Enable Control

* **Description**           

The register consists of 8 interrupt enable bits for 8 group in the PW counter.


* **RTL Instant Name**    : `counter_per_group_intr_en_ctrl`

* **Address**             : `0x061BFE`

* **Formula**             : `0x061BFE + Grp2048ID*8192 +  Slice*1024`

* **Where**               : 

    * `$Grp2048ID(0-2):  Pseudowire ID bits[12:11]`

    * `$Slice(0-1):  Pseudowire ID bits[10]`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`groupintren`| Set to 1 to enable the related Group to generate interrupt.| `RW`| `0x0`| `0x0 End: Begin:`|

###Counter per Slice Interrupt OR Status

* **Description**           

The register consists of 6 bits for 6 Slice of the PW Counter. Each bit is used to store Interrupt OR status of the related 1024 PWs Group.


* **RTL Instant Name**    : `counter_per_slice_intr_or_stat`

* **Address**             : `0x068000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `6`
* **Register Type**       : `Interrupt`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[5:0]`|`groupintrorsta`| Set to 1 if any interrupt bit of corresponding Slice is set and its interrupt is enabled| `RW`| `0x0`| `0x0 End:`|

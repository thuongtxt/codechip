## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CNC0021_RD_BERT_MON_PW
####Register Table

|Name|Address|
|-----|-----|
|`Sel Bert ID Mon`|`0x8500 - 0x851F`|
|`Bert Gen Global Register`|`0x8400`|
|`Sticky Enable`|`0x8401`|
|`Sticky Error`|`0x8402`|
|`Sticky Error`|`0x8412`|
|`Counter Error`|`0x8404`|
|`Counter Good`|`0x8405`|
|`Counter Lost`|`0x8406`|
|`Mon Control Bert Generate`|`0x8420 - 0x843F`|
|`Config expected Fix pattern gen`|`0x8440 - 0x845F`|
|`Status of mon bert`|`0x84E0 - 0x84FF`|
|`counter error`|`0x8540 - 0x855F`|
|`Moniter Good Bit`|`0x8580 - 0x859F`|
|`Moniter Lost Bit`|`0x85C0 - 0x85DF`|
|`Config nxDS0 moniter`|`0x84C0 - 0x84CF`|
|`moniter counter load id`|`0x8403`|


###Sel Bert ID Mon

* **Description**           

Sel 16 chanel id which enable moniter BERT from 12xOC24 channel


* **RTL Instant Name**    : `upen_id_reg_mon`

* **Address**             : `0x8500 - 0x851F`

* **Formula**             : `0x8500 + $mid`

* **Where**               : 

    * `$mid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:15]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[14]`|`mon_en`| bit enable| `RW`| `0x0`| `0x0`|
|`[13:11]`|`looc48id`| OC48 slide (0-5)| `RW`| `0x0`| `0x0`|
|`[10]`|`looc24slice`| OC24 slide  (0,1)| `RW`| `0x0`| `0x0`|
|`[09:00]`|`tdmoc24pwid`| channel id| `RW`| `0x0`| `0x0 End: Begin:`|

###Bert Gen Global Register

* **Description**           

global mon config


* **RTL Instant Name**    : `mglb_pen`

* **Address**             : `0x8400`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[09:06]`|`lopthrcfg`| Threshold for lost pattern moniter| `RW`| `0x00`| `0x01`|
|`[05:02]`|`synthrcfg`| Threshold for gosyn state of pattern moniter| `RW`| `0x00`| `0x0C`|
|`[01]`|`swapmode`| enable swap data| `RW`| `0x00`| `0x0`|
|`[00]`|`invmode`| enable invert data| `RW`| `0x00`| `0x0 End: Begin:`|

###Sticky Enable

* **Description**           

This register is used to configure the test pattern sticky mode


* **RTL Instant Name**    : `stkcfg_pen`

* **Address**             : `0x8401`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[00]`|`stkcfg_reg`| | `RC`| `0x00`| `0x0 End: Begin:`|

###Sticky Error

* **Description**           

Bert sticky error


* **RTL Instant Name**    : `stk_pen`

* **Address**             : `0x8402`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`stkdi1`| bitmap [15:0] indicate error of id 1-16 moniter bert| `RW`| `0x00`| `0x0 End: Begin:`|

###Sticky Error

* **Description**           

Bert sticky error


* **RTL Instant Name**    : `stk_pen`

* **Address**             : `0x8412`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`stkd2i`| bitmap[31:16] indicate error of id 17-32 moniter bert| `RW`| `0x00`| `0x0 End: Begin :`|

###Counter Error

* **Description**           

Latch error counter


* **RTL Instant Name**    : `errlat_pen`

* **Address**             : `0x8404`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`errcnt_lat`| counter err| `RW`| `0x00`| `0x0 End: Begin:`|

###Counter Good

* **Description**           

Latch good counter


* **RTL Instant Name**    : `goodlat_pen`

* **Address**             : `0x8405`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`goodcnt_lat`| counter good| `RW`| `0x00`| `0x0 End: Begin:`|

###Counter Lost

* **Description**           




* **RTL Instant Name**    : `lostlat_pen`

* **Address**             : `0x8406`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`lostcnt_lat`| counter good| `RW`| `0x00`| `0x0 End: Begin:`|

###Mon Control Bert Generate

* **Description**           

Used to control bert gen mode of 16 id


* **RTL Instant Name**    : `mon_ctrl_pen`

* **Address**             : `0x8420 - 0x843F`

* **Formula**             : `0x8420 + $mctrl_id`

* **Where**               : 

    * `$mctrl_id(0-31)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:12]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[11]`|`mswapmode`| swap data| `RW`| `0x0`| `0x0`|
|`[10]`|`minvmode`| invert data| `RW`| `0x0`| `0x0`|
|`[09:05]`|`mpatbit`| number of bit fix patt use| `RW`| `0x0`| `0x0`|
|`[04:00]`|`mpatt_mode`| sel pattern gen # 0x0 : Prbs9 # 0x1 : Prbs11 # 0x2 : Prbs15 # 0x3 : Prbs20r # 0x4 : Prbs20 # 0x5 : qrss20 # 0x6 : Prbs23 # 0x7 : dds1 # 0x8 : dds2 # 0x9 : dds3 # 0xA : dds4 # 0xB : dds5 # 0xC : daly # 0XD : octet55_v2 # 0xE : octet55_v3 # 0xF : fix3in24 # 0x10: fix1in8 # 0x11: fix2in8 # 0x12: fixpat # 0x13: sequence # 0x14: ds0prbs # 0x15: fixnin32| `RW`| `0x0`| `0x0 End: Begin:`|

###Config expected Fix pattern gen

* **Description**           

config fix pattern gen


* **RTL Instant Name**    : `thrnfix_pen`

* **Address**             : `0x8440 - 0x845F`

* **Formula**             : `0x8440 + $genid`

* **Where**               : 

    * `$genid (0-31)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`crrthrnfix`| config fix pattern gen| `RW`| `0x0`| `0x0 End: Begin:`|

###Status of mon bert

* **Description**           

Indicate current status of bert mon


* **RTL Instant Name**    : `mon_status_pen`

* **Address**             : `0x84E0 - 0x84FF`

* **Formula**             : `0x84E0 + $msttid`

* **Where**               : 

    * `$msttid (0-31)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[01:00]`|`mon_crr_stt`| Status [1:0]: Prbs status LOPSTA = 2'd0; SRCSTA = 2'd1; VERSTA = 2'd2; INFSTA = 2'd3;| `R_O`| `0x0`| `0x0 End: Begin:`|

###counter error

* **Description**           

Moniter error bert counter of 16 chanel id


* **RTL Instant Name**    : `merrcnt_pen`

* **Address**             : `0x8540 - 0x855F`

* **Formula**             : `0x8540 + $errid`

* **Where**               : 

    * `$errid(0-31)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`errcnt_pdo`| value of err counter| `R2C`| `0x0`| `0x0 End: Begin:`|

###Moniter Good Bit

* **Description**           

Moniter good bert counter of 16 chanel id


* **RTL Instant Name**    : `mgoodbit_pen`

* **Address**             : `0x8580 - 0x859F`

* **Formula**             : `0x8580 + $mgb_id`

* **Where**               : 

    * `$mgb_id(0-15)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`goodbit_pdo`| value of good cnt| `RW`| `0x0`| `0x0 End: Begin:`|

###Moniter Lost Bit

* **Description**           




* **RTL Instant Name**    : `mlostbit_pen`

* **Address**             : `0x85C0 - 0x85DF`

* **Formula**             : `0x85C0 + $mlos_id`

* **Where**               : 

    * `$mlos_id(0-31)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`lostbit_pdo`| Value of lost cnt| `RW`| `0x0`| `0x0 End: Begin:`|

###Config nxDS0 moniter

* **Description**           

enable moniter 32 timeslot E1 or 24timeslot T1


* **RTL Instant Name**    : `mtsen_pen`

* **Address**             : `0x84C0 - 0x84CF`

* **Formula**             : `0x84C0 + $mid`

* **Where**               : 

    * `$mid(0-15)`

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`mtsen_pdo`| set "1" to en moniter| `RW`| `0x0`| `0x0 End: Begin:`|

###moniter counter load id

* **Description**           

used to load the error/good/loss counter report of bert id which the channel ID is value of register.


* **RTL Instant Name**    : `cntld_strb`

* **Address**             : `0x8403`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config `
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[03:00]`|`idload`| | `RW`| `0x00`| `0x0 End:`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CNC0022_RD_DCCK
####Register Table

|Name|Address|
|-----|-----|
|`DCCK CPU Reg Hold Control`|`0x0000A-0x0000B`|
|`Global Interrupt Status`|`0x02001`|
|`DCC ETH2OCN Direction Interupt Status Reg0`|`0x02004`|
|`DCC ETH2OCN Direction Interupt Status Reg1`|`0x02005`|
|`DCC ETH2OCN Direction Interupt Status Reg1`|`0x02006`|
|`DCC OCN2ETH Direction Interupt Buffer Full Status`|`0x02008`|
|`DCC OCN2ETH Direction Interupt CRC Error Status`|`0x02009`|
|`DCC OCN2ETH Direction Interupt Oversize Length Status`|`0x0200D`|
|`DCC OCN2ETH Direction Interupt Status`|`0x0200E`|
|`KByte Interupt Status`|`0x0200F`|
|`Loopback Enable Configuration`|`0x00001`|
|`Global Interrupt Enable`|`0x00002`|
|`DCC ETH2OCN Direction Interupt Enable Reg0`|`0x00004`|
|`DCC ETH2OCN Direction Interupt Enable Reg1`|`0x00005`|
|`DCC ETH2OCN Direction Interupt Enable Reg2`|`0x00006`|
|`DCC OCN2ETH Direction Interupt Enable Reg0`|`0x00008`|
|`DCC OCN2ETH Direction Interupt Enable Reg0`|`0x00009`|
|`DCC OCN2ETH Direction Interupt Enable Reg2`|`0x0000D`|
|`DCC OCN2ETH Direction Interupt Enable Reg0`|`0x0000E`|
|`KByte Interupt Enable`|`0x0000F`|
|`DDC_Buffer_Linklist_Init`|`0x10001`|
|`DDC_TX_Header_Per_Channel_reg0`|`0x11200 - 0x1127C`|
|`DDC_TX_Header_Per_Channel_Reg1`|`0x11201 - 0x1127D`|
|`DDC_Channel_Enable`|`0x11000`|
|`DDC_RX_Global_ProvisionedHeader_Configuration`|`0x12001`|
|`DDC_RX_MAC_Check_Enable_Configuration`|`0x12002`|
|`DDC_RX_CVLAN_Check_Enable_Configuration`|`0x12003`|
|`DDC_RX_Channel_Mapping`|`0x12200 - 0x1221F`|
|`Provisioned_APS_KByte_Value_Per_Channel`|`0x21200  - 0x2120F`|
|`Enable Transmit Validated KByte Change Per ChannelID Configuration`|`0x21001`|
|`Timer_TX_Provisioned_APS_Packet`|`0x21002`|
|`Trigger_TX_Provisioned_APS_Packet`|`0x21003`|
|`Enable KByte Overwrite Per ChannelID Configuration`|`0x21004`|
|`APS_TX_Header_Mac_DA`|`0x21006`|
|`APS_TX_Header_Mac_SA`|`0x21007`|
|`APS_TX_Header_VTL`|`0x21008`|
|``|`0x21100  - 0x2110F`|
|``|`0x21009`|
|`APS_RX_Header_MAC_DA_Configuration`|`0x22001`|
|`APS_RX_Header_MAC_SA_Configuration`|`0x22002`|
|`APS_RX_Header_EVT_Configuration`|`0x22003`|
|`APS_RX_ChannelID_Configuration`|`0x22100 - 0x2210F`|
|`APS_RX_ChannelID_Mapping_Configuration`|`0x22400 - 0x2240F`|
|`APS_RX_Alarm_Sticky`|`0x22010`|
|`APS_RX_WatchDog_Timer`|`0x22008`|
|`DCC_OCN2ETH_Pkt_Length_Alarm_Sticky`|`0x11004`|
|`DCC_OCN2ETH_Pkt_Error_And_Buffer_Full_Alarm_Sticky`|`0x11005`|
|`DCC_ETH2OCN_Alarm_Sticky`|`0x12008`|
|`APS_RX_Alarm_Status`|`0x22012`|
|`DCC_OCN2ETH_Min_Pkt_Length_Alarm_Status`|`0x11008`|
|`DCC_OCN2ETH_Max_Pkt_Length_Alarm_Status`|`0x11009`|
|`DCC_OCN2ETH_Pkt_CRC_Error_Alarm_Status`|`0x1100A`|
|`DCC_OCN2ETH_Buffer_Full_Alarm_Status`|`0x1100B`|
|`DCC_ETH2OCN_Alarm_Status`|`0x1200A`|
|`APS_RX_Trig_En_Cap`|`0x22006`|
|`APS_RX_Packet_Header_Cap_Reg0`|`0x22200 - 0x2220C`|
|`APS_RX_Packet_Header_Cap_Reg1`|`0x22201 - 0x2220D`|
|`APS_RX_Packet_Header_Cap_Reg2`|`0x22202 - 0x2220E`|
|`Counter_Rx_Unknow_From_SGMII_Port0`|`0x22024`|
|`Counter_Total_Rx_APS_From_SGMII_Port0`|`0x22025`|
|`Counter_Rx_APS_From_SGMII_Port0_Per_Chan`|`0x22300 - 0x2230F`|
|`Counter_Rx_APS_BYTE_From_RxOcn_Per_Chan`|`0x21330 - 0x2133F`|
|`Counter_Total_Rx_APS_Packet_From_RxOcn`|`0x21360`|
|`Counter_Rx_APS_BYTE_From_RxOcn_Per_Chan`|`0x21361`|
|`Counter_Tx_APS_SOP_To_SGMII`|`0x21350`|
|`Counter_Tx_APS_EOP_To_SGMII`|`0x21351`|
|`Counter_Tx_APS_BYTE_To_SGMII`|`0x21352`|
|`Counter_Rx_Sop_From_SGMII_Port`|`0x12020`|
|`Counter_Rx_Eop_From_SGMII_Port`|`0x12021`|
|`Counter_Rx_Err_From_SGMII_Port`|`0x12022`|
|`Counter_Rx_Byte_From_SGMII_Port`|`0x12023`|
|`Counter_Rx_Unknow_From_SGMII_Port`|`0x12024`|
|`Counter_Rx_DCC_Pkt_From_SGMII_Port1_Per_Chan`|`0x12100 - 0x1211F`|
|`Counter_Rx_DCC_Byte_From_SGMII_Port1_Per_Chan`|`0x121C0 - 0x121DF`|
|`Counter_Rx_DCC_SOP_From_RxOcn_Per_Chan`|`0x11400 - 0x1141F`|
|`Counter_Rx_DCC_EOP_From_RxOcn_Per_Chan`|`0x11420 - 0x1143F`|
|`Counter_Rx_DCC_ERR_From_RxOcn_Per_Chan`|`0x11440 - 0x1145F`|
|`Counter_Rx_DCC_BYTE_From_RxOcn_Per_Chan`|`0x11460 - 0x1147F`|
|`Counter_Rx_DCC_Drp_From_RxOcn_Per_Chan`|`0x11480	  - 0x1149F`|
|`Counter_Rx_Sop_From_OCN`|`0x11504`|
|`Counter_Rx_Eop_From_OCN`|`0x11505`|
|`Counter_Rx_Err_From_OCN`|`0x11506`|
|`Counter_Rx_Byte_From_OCN`|`0x11507`|
|`Counter_Tx_DCC_SOP_To_SGMII_Per_Chan`|`0x114A0 - 0x114BF`|
|`Counter_Tx_DCC_EOP_To_SGMII_Per_Chan`|`0x114C0 - 0x114DF`|
|`Counter_Tx_DCC_BYTE_To_SGMII_Per_Chan`|`0x114E0 - 0x114FF`|
|`Counter_Tx_Sop_To_SGMII_Port1`|`0x11500`|
|`Counter_Tx_Eop_To_SGMII_Port1`|`0x11501`|
|`Counter_Tx_Err_To_SGMII_Port1`|`0x11502`|
|`Counter_Tx_Byte_To_SGMII_Port1`|`0x11503`|
|`Counter_DCC_SOP_to_RxOcn_Per_Chan`|`0x12120 - 0x1213f`|
|`Counter_DCC_EOP_to_RxOcn_Per_Chan`|`0x12140 - 0x1215F`|
|`Counter_DCC_ERR_to_RxOcn_Per_Chan`|`0x12160 - 0x1217F`|
|`Counter_DCC_EPT_to_RxOcn_Per_Chan`|`0x12180 - 0x1219F`|
|`Counter_DCC_BYTE_to_RxOcn_Per_Chan`|`0x121A0 - 0x121BF`|
|`Counter_DCC_Drop_to_RxOcn_Per_Chan`|`0x121E0 - 0x121FF`|
|`Counter_Total_Tx_Sop_To_OCN`|`0x12025`|
|`Counter_Total_Tx_Eop_To_OCN`|`0x12026`|
|`Counter_Total_Tx_Err_To_OCN`|`0x12027`|
|`Counter_Total_Tx_Byte_To_OCN`|`0x12028`|
|`Counter_Total_Drop_To_RxOcn_Pkt`|`0x12029`|
|`CONFIG HDLC LO DEC`|`0x04000 - 0x0401F`|
|`CONFIG HDLC GLOBAL LO DEC`|`0x04404`|
|`HDLC Encode Master Control`|`0x30003`|
|`HDLC Encode Control Reg 1`|`0x38000 - 0x3801F`|
|`HDLC Encode Control Reg 2`|`0x39000 - 0x3901F`|
|`Enable Packet Capture`|`0x0A000`|
|`Captured Packet Data`|`0x08000 - 0x080FF`|
|`APS_Acc_Received_Kbyte_Value_Per_Channel`|`0x22800 - 0x228FF`|
|`DDC_Test_Sdh_Req_Interval_Configuration`|`0x00003`|
|`DDC_Config_Test_Gen_Header_Per_Channel`|`0x0C100 - 0x0C120`|
|`DDC_Config_Test_Gen_Mode_Per_Channel`|`0x0C200 - 0x0C220`|
|`DDC_Config_Test_Gen_Global_Gen_Enable_Channel`|`0x0C000`|
|`DDC_Config_Test_Gen_Global_Gen_Mode`|`0x0C001`|
|`DDC_Config_Test_Gen_Global_Packet_Length`|`0x0C002`|
|`DDC_Config_Test_Gen_Global_Gen_Interval`|`0x0C003`|
|`DDC_Test_Mon_Good_Packet_Counter`|`0x0E100 - 0x0E120`|
|`DDC_Test_Mon_Error_Data_Packet_Counter`|`0x0E200 - 0x0E220`|
|`DDC_Test_Mon_Error_VCG_Packet_Counter`|`0x0E300 - 0x0E320`|
|`DDC_Test_Mon_Error_SEQ_Packet_Counter`|`0x0E400 - 0x0E420`|
|`DDC_Test_Mon_Error_FCS_Packet_Counter`|`0x0E500 - 0x0E520`|
|`DDC_Test_Mon_Abort_VCG_Packet_Counter`|`0x0E600 - 0x0E620`|


###DCCK CPU Reg Hold Control

* **Description**           

The register provides hold register for two word 32-bits MSB when CPU access to engine.


* **RTL Instant Name**    : `dcck_cpu_hold`

* **Address**             : `0x0000A-0x0000B`

* **Formula**             : `0x0000A + $HoldId`

* **Where**               : 

    * `$HoldId(0-1): Hold register`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`holdreg`| Hold 32 bits| `RW`| `0x0`| `0x0 End: Begin:`|

###Global Interrupt Status

* **Description**           

The register provides global interrupt status


* **RTL Instant Name**    : `upen_glbint_stt`

* **Address**             : `0x02001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[01:01]`|`kbyte_interrupt`| interrupt from KByte	event| `RO`| `0x0`| `0x0`|
|`[00:00]`|`dcc_interrupt`| interrupt from DCC event| `RO`| `0x0`| `0x0 End: Begin:`|

###DCC ETH2OCN Direction Interupt Status Reg0

* **Description**           

The register provides interrupt status of DCC event ETH2OCN direction


* **RTL Instant Name**    : `upen_int_rxdcc0_stt`

* **Address**             : `0x02004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`dcc_channel_disable`| DCC Local Channel Identifier mapping is disable Bit[00] -> Bit[31] indicate channel 0 -> 31.| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC ETH2OCN Direction Interupt Status Reg1

* **Description**           

The register provides interrupt status of DCC event ETH2OCN direction


* **RTL Instant Name**    : `upen_int_rxdcc1_stt`

* **Address**             : `0x02005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `5`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:03]`|`dcc_sgm_ovrsize_len`| Received DCC packet's length from SGMII port over maximum allowed length	(1318 bytes)| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`dcc_sgm_crc_error`| Received packet from SGMII port has FCS error| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`dcc_cvlid_mismat`| Received 12b CVLAN ID value of DCC frame different from global provisioned CVID| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`dcc_macda_mismat`| Received 43b MAC DA value of DCC frame different from global provisioned DA| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC ETH2OCN Direction Interupt Status Reg1

* **Description**           

The register provides interrupt status of DCC event ETH2OCN direction


* **RTL Instant Name**    : `upen_int_rxdcc2_stt`

* **Address**             : `0x02006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`dcc_eth2ocn_blk_ept`| DCC packet buffer for ETH2OCN direction was fulled, some packets will be lost. Bit[00] -> Bit[31] indicate channel 0 -> 31.| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC OCN2ETH Direction Interupt Buffer Full Status

* **Description**           

The register provides interrupt status of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_int_txdcc0_stt`

* **Address**             : `0x02008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`dcc_ocn2eth_blk_ept`| DCC packet buffer for OCN2ETH direction was fulled, some packets will be lost Bit[00] -> Bit[31] indicate channel 0 -> 31.| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC OCN2ETH Direction Interupt CRC Error Status

* **Description**           

The register provides interrupt status of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_int_txdcc1_stt`

* **Address**             : `0x02009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`dcc_hdlc_crc_error`| Received HDLC frame from OCN has FCS error| `W1C`| `0x0`| `0x0 Bit[00] -> Bit[31] indicate channel 0 -> 31.`|

###DCC OCN2ETH Direction Interupt Oversize Length Status

* **Description**           

The register provides interrupt status of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_int_txdcc2_stt`

* **Address**             : `0x0200D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`dcc_hdlc_ovrsize_len`| Received HDLC packet's length from OCN overed maximum allowed length	(1536 bytes)| `W1C`| `0x0`| `0x0 Bit[00] -> Bit[31] indicate channel 0 -> 31.`|

###DCC OCN2ETH Direction Interupt Status

* **Description**           

The register provides interrupt status of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_int_txdcc3_stt`

* **Address**             : `0x0200E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`dcc_hdlc_undsize_len`| Received HDLC packet's length from OCN undered minimum allowed length (2 bytes)| `W1C`| `0x0`| `0x0 Bit[00] -> Bit[31] indicate channel 0 -> 31.`|

###KByte Interupt Status

* **Description**           

The register provides interrupt status of Kbyte event


* **RTL Instant Name**    : `upen_int_rxk12_stt`

* **Address**             : `0x0200F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:08]`|`aps_per_channel_mismat`| Received CHANNEL ID value of APS frame different from configured value. This is a per channel status. Bit 23 -> 08 represent for channel 15 -> 0 respectively| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`aps_watdog_alarm`| No packets received in the period defined in watchdog timer register| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`aps_glb_channel_mismat`| There is one or more mismatch between received CHANNELID value of APS frame and configuration value of that channelID. This bit is set whenever a mismatch happen in one or more of 16 channels| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`aps_lencount_mismat`| Received PACKET BYTE COUNTER value of APS frame different from configuration| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`aps_lenfield_mismat`| Received LENGTH FIELD value of APS frame different from configuration| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`aps_ver_mismat`| Received VERSION value of APS frame different from configuration| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`aps_apstp_mismat`| Received TYPE value of APS frame different from configuration| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`aps_ethtp_mismat`| Received ETHERNET TYPE value of APS frame different from configuration| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`aps_macda_mismat`| Received DA value of APS frame different from configed DA| `W1C`| `0x0`| `0x0 End: Begin:`|

###Loopback Enable Configuration

* **Description**           

The register provides loopback enable configuration


* **RTL Instant Name**    : `upen_loopen`

* **Address**             : `0x00001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`dcc_buffer_loop_en`| Enable loopback of RX-BUFFER to TX-BUFFER      .<br>{1}: enable. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[04:04]`|`genmon_loop_en`| Enable loopback of data from DCC generator to RX-DCC  .<br>{1}: enable. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[03:03]`|`ksdh_loop_en`| Enable loopback of Kbyte information           .<br>{1}: enable. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[02:02]`|`ksgm_loop_en`| Enable SGMII loopback of Kbyte Port.<br>{1}: enable. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[01:01]`|`hdlc_loop_en`| Enable loopback from HDLC Encap to HDLC DEcap of DCC byte.<br>{1}: enable. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[00:00]`|`dsgm_loop_en`| Enable SGMII loopback of DCC Port.<br>{1}: enable. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###Global Interrupt Enable

* **Description**           

The register provides configuration to enable global interrupt


* **RTL Instant Name**    : `upen_glb_intenb`

* **Address**             : `0x00002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `2`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[01:01]`|`kbyte_interrupt`| Enable interrupt for KByte events| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`dcc_interrupt`| Enable interrupt for DCC event| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC ETH2OCN Direction Interupt Enable Reg0

* **Description**           

The register provides configuration to enable interrupt of DCC events ETH2OCN direction


* **RTL Instant Name**    : `upen_rxdcc_intenb_r0`

* **Address**             : `0x00004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`enb_int_dcc_chan_dis`| Enable Interrupt of DCC Local Channel Identifier mapping per channel Bit[00] -> Bit[31] indicate channel 0 -> 31.<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC ETH2OCN Direction Interupt Enable Reg1

* **Description**           

The register provides configuration to enable interrupt of DCC events ETH2OCN direction


* **RTL Instant Name**    : `upen_rxdcc_intenb_r1`

* **Address**             : `0x00005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[03:03]`|`enb_int_dcc_sgm_ovrsize_len`| Enable Interrupt of "Received DCC packet's length from SGMII port over maximum allowed length (1318 bytes)"| `RW`| `0x0`| `0x0`|
|`[02:02]`|`enb_int_dcc_sgm_crc_error`| Enable Interrupt of "Received packet from SGMII port has FCS error"| `RW`| `0x0`| `0x0`|
|`[01:01]`|`enb_int_dcc_cvlid_mismat`| Enable Interrupt of "Received 12b CVLAN ID value of DCC frame different from global provisioned CVID "<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[00:00]`|`enb_int_dcc_macda_mismat`| Enable Interrupt of "Received 43b MAC DA value of DCC frame different from global provisioned DA"<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC ETH2OCN Direction Interupt Enable Reg2

* **Description**           

The register provides configuration to enable interrupt of DCC events ETH2OCN direction


* **RTL Instant Name**    : `upen_rxdcc_intenb_r2`

* **Address**             : `0x00006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`enb_int_dcc_eth2ocn_blk_ept`| Enable Interrupt of "DCC packet buffer for ETH2OCN direction was fulled, some packets will be lost "	per channel Bit[00] -> Bit[31] indicate channel 0 -> 31.<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC OCN2ETH Direction Interupt Enable Reg0

* **Description**           

The register provides configuration to enable interrupt of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_txdcc_intenb_ept`

* **Address**             : `0x00008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`enb_int_dcc_ocn2eth_blk_ept`| Enable Interrupt of "DCC packet buffer for OCN2ETH direction was fulled, some packets will be lost" per channel Bit[00] -> Bit[31] indicate channel 0 -> 31.<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC OCN2ETH Direction Interupt Enable Reg0

* **Description**           

The register provides configuration to enable interrupt of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_txdcc_intenb_crc`

* **Address**             : `0x00009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`enb_int_dcc_hdlc_crc_error`| Enable Interrupt of "Received HDLC frame from OCN has FCS error	" per channel Bit[00] -> Bit[31] indicate channel 0 -> 31.<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC OCN2ETH Direction Interupt Enable Reg2

* **Description**           

The register provides configuration to enable interrupt of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_txdcc_intenb_ovr`

* **Address**             : `0x0000D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`enb_int_dcc_hdlc_ovrsize_len`| Enable Interrupt of "Received HDLC packet's length from OCN overed maximum allowed length (1536 bytes)	" per channel Bit[00] -> Bit[31] indicate channel 0 -> 31.<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC OCN2ETH Direction Interupt Enable Reg0

* **Description**           

The register provides configuration to enable interrupt of DCC event OCN2ETH direction


* **RTL Instant Name**    : `upen_txdcc_intenb_crc`

* **Address**             : `0x0000E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`enb_int_dcc_hdlc_undsize_len`| Enable Interrupt of "Received HDLC packet's length from OCN undered minimum allowed length (2 bytes) " per channel Bit[00] -> Bit[31] indicate channel 0 -> 31.<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###KByte Interupt Enable

* **Description**           

The register provides configuration to enable interrupt of Kbyte events


* **RTL Instant Name**    : `upen_rxk12_intenb`

* **Address**             : `0x0000F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:08]`|`enb_int_aps_per_channel_mismat`| Enable Interrupt of "Received CHANNEL ID value of APS frame different from configuration per channel ". Bit 23->08 represent for mismatch of channelID 15->0 respectively<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[07:07]`|`enb_int_aps_watdog_alarm`| Enable Interrupt of "No packets received in the period defined in watchdog timer register"<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[06:06]`|`enb_int_aps_glb_channel_mismat`| Enable Interrupt of "Received CHANNEL ID value of APS frame different from configuration ". This bit is set when there is one or more mismatching channelID happen<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[05:05]`|`enb_int_aps_lencount_mismat`| Enable Interrupt of "Received PACKET BYTE COUNTER value of APS frame different from configuration "<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[04:04]`|`enb_int_aps_lenfield_mismat`| Enable Interrupt of "Received LENGTH FIELD value of APS frame different from configuration "<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[03:03]`|`enb_int_aps_ver_mismat`| Enable Interrupt of "Received VERSION value of APS frame different from configuration "<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[02:02]`|`enb_int_aps_apstp_mismat`| Enable Interrupt of "Received TYPE value of APS frame different from configuration "<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[01:01]`|`enb_int_aps_ethtp_mismat`| Enable Interrupt of "Received ETHERNET TYPE value of APS frame different from configuration "<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0`|
|`[00:00]`|`enb_int_aps_macda_mismat`| Enable Interrupt of "Received DA value of APS frame different from configed DA "<br>{1}:enable interrupt. <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Buffer_Linklist_Init

* **Description**           

The register use to trigger Link-List initialization


* **RTL Instant Name**    : `upen_cfg_llst`

* **Address**             : `0x10001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[03:03]`|`tx_ini_done`| Indicate TX Link-list initialization successed| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`rx_ini_done`| Indicate RX Link-list initialization successed| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`tx_ll_init`| Use to trigger Linklist of DCC TX packet buffer. Write 0 first, then write 1 to trigger| `RW`| `0x0`| `0x0`|
|`[00:00]`|`rx_ll_init`| Use to trigger Linklist of DCC RX packet buffer. Write 0 first, then write 1 to trigger| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_TX_Header_Per_Channel_reg0

* **Description**           

The register provides data for configuration of 22bytes Header of each channel ID


* **RTL Instant Name**    : `upen_dcctxhdr_reg0`

* **Address**             : `0x11200 - 0x1127C`

* **Formula**             : `0x11200 + $channelid*4`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:16]`|`mac_da`| MAC DA value of Channel ID| `RW`| `0x0`| `0x0`|
|`[15:00]`|`mac_sa`| 16b MSB MAC SA value of Channel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_TX_Header_Per_Channel_Reg1

* **Description**           

The register provides data for configuration of 22bytes Header of each channel ID


* **RTL Instant Name**    : `upen_dcctxhdr_reg1`

* **Address**             : `0x11201 - 0x1127D`

* **Formula**             : `0x11201 + $channelid*4`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:32]`|`mac_sa`| 48b LSB MAC SA value of Channel ID| `RW`| `0x0`| `0x0`|
|`[31:16]`|`vlan`| 16b<br>{PCP,DEI,VID} value of VLAN field| `RW`| `0x0`| `0x0`|
|`[15:08]`|`version`| 8b Type field| `RW`| `0x0`| `0x0`|
|`[07:00]`|`type`| 8b Version Field| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Channel_Enable

* **Description**           

The register provides configuration for enable transmission DDC packet per channel


* **RTL Instant Name**    : `upen_dcctx_enacid`

* **Address**             : `0x11000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`channel_enable`| Enable transmitting of DDC packet per channel. Bit[31:0] represent for channel 31->0<br>{1}: enable channel to transmit  DDC byte <br>{0}: disable channel to transmit DDC byte| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_RX_Global_ProvisionedHeader_Configuration

* **Description**           

The register provides data for configuration of 22bytes Header of each channel ID


* **RTL Instant Name**    : `upen_dccrxhdr`

* **Address**             : `0x12001`

* **Formula**             : `0x12001`

* **Where**               : 

* **Width**               : `71`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[70:55]`|`eth_typ`| 16b value of Provisioned ETHERTYPE of DCC| `RW`| `0x0`| `0x880B`|
|`[54:43]`|`cvid`| 12b value of Provisioned C-VLAN ID. This value is used to compared with received CVLAN ID value.| `RW`| `0x0`| `0x0`|
|`[42:00]`|`mac_da`| 43b MSB of Provisioned MAC DA value. This value is used to compared with received MAC_DA[47:05] value. If a match is confirmed, MAC_DA[04:00] is used to represent channelID value before mapping.| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_RX_MAC_Check_Enable_Configuration

* **Description**           

The register provides configuration that enable base MAC DA check


* **RTL Instant Name**    : `upen_dccrxmacenb`

* **Address**             : `0x12002`

* **Formula**             : `0x12002`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`mac_check_enable`| Enable checking of received MAC DA compare to globally provisioned Base MAC.On mismatch, frame is discarded.<br>{1}:enable, <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_RX_CVLAN_Check_Enable_Configuration

* **Description**           

The register provides configuration that enable base CVLAN Check


* **RTL Instant Name**    : `upen_dccrxcvlenb`

* **Address**             : `0x12003`

* **Formula**             : `0x12003`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cvl_check_enable`| Enable checking of received CVLAN ID compare to globally provisioned CVLAN ID.On mismatch, frame is discarded.<br>{1}:enable, <br>{0}: disable| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_RX_Channel_Mapping

* **Description**           

The register provides channel mapping from received MAC DA to internal channelID


* **RTL Instant Name**    : `upen_dccdec`

* **Address**             : `0x12200 - 0x1221F`

* **Formula**             : `0x12200 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `6`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[05:05]`|`channel_enable`| Enable Local Channel Identifier. Rx packet is discarded if enable is not set<br>{0}: disable. <br>{1}: Enable| `RW`| `0x0`| `0x0`|
|`[04:00]`|`mapping_channelid`| Local ChannelID that is mapped from received bit[4:0] of MAC DA| `RW`| `0x0`| `0x0 End: Begin:`|

###Provisioned_APS_KByte_Value_Per_Channel

* **Description**           

The register provides configuration for Kbyte value to overwrite


* **RTL Instant Name**    : `upen_k12pro`

* **Address**             : `0x21200  - 0x2120F`

* **Formula**             : `0x21200 + $channelid`

* **Where**               : 

    * `$channelid(0-15): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`channelid_aps`| 32b of ChannelID APS value to overwrite This Kbyte value is used to overwrite Kbyte value get from RX-OCN when enable [31:24]: K1 byte value [23:16]: K2 byte value [15:08]: D1(EK1&EK2) (extend K) byte value [07:00]: set to 0x0| `RW`| `0x0`| `0x0 End: Begin:`|

###Enable Transmit Validated KByte Change Per ChannelID Configuration

* **Description**           

The register configures channelID for provisioned APS packet


* **RTL Instant Name**    : `upen_cfgenacid`

* **Address**             : `0x21001`

* **Formula**             : `0x21001`

* **Where**               : 

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`channelid_enable`| Enable provisioned ChannelID. Bit[15:0] represents channelID 15->0 This channelID bitmap is used to allow transmission of newly validated K byte change.<br>{1}: enable channelID <br>{0}: disable channelID| `RW`| `0x0`| `0x0 End: Begin:`|

###Timer_TX_Provisioned_APS_Packet

* **Description**           

The register configures timer for transmiting provisioned APS packets of configured channelID


* **RTL Instant Name**    : `upen_cfgtim`

* **Address**             : `0x21002`

* **Formula**             : `0x21002`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`timer_ena`| Enable Timer for TX provisoned APS packet<br>{1}: enable timer <br>{0}: disable timer| `RW`| `0x0`| `0x0`|
|`[30:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:0]`|`timer_val`| Timer value for TX provisoned APS packet. This value is the number of clock counter represent the time interval For ex: to set an interval 256us with the clock 155Mz The clock counter is: 256x10^3/(10^3/155) = 256*155 = 39680 Timer range from 125us to 8ms. This timer is used only when bit 31 (Timer Ena) is set Each time the timer reach configurated value, an APS packet will be transmit| `RW`| `0x0`| `0x0 End: Begin:`|

###Trigger_TX_Provisioned_APS_Packet

* **Description**           

The register configures channelID for provisioned APS packet


* **RTL Instant Name**    : `upen_cfgcid`

* **Address**             : `0x21003`

* **Formula**             : `0x21003`

* **Where**               : 

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0:0]`|`sw_trig`| SW trigger transmission of Provisioned APS packet Write 0, then write 1 to trigger| `RW`| `0x0`| `0x0 End: Begin:`|

###Enable KByte Overwrite Per ChannelID Configuration

* **Description**           

The register configures channelID for provisioned APS packet


* **RTL Instant Name**    : `upen_upen_cfgovwcid`

* **Address**             : `0x21004`

* **Formula**             : `0x21004`

* **Where**               : 

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`channelid_enable`| Enable overwrite Kbyte value of channel Bit[15:0] represents channelID 15->0 This channelID bitmap is used to allow overwrite of channel's Kbyte. When this bit is set. Kbyte will get value from configurated Provisioned Kbyte instead of Kbyte received from RX-OCN<br>{1}: enable overwrite <br>{0}: disable overwrite| `RW`| `0x0`| `0x0 End: Begin:`|

###APS_TX_Header_Mac_DA

* **Description**           

The register provides configuration of TX APS Packet


* **RTL Instant Name**    : `upen_cfg_hdrmda`

* **Address**             : `0x21006`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`mac_da`| MAC DA value of TX APS Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###APS_TX_Header_Mac_SA

* **Description**           

The register provides configuration of TX APS Packet


* **RTL Instant Name**    : `upen_cfg_hdrmsa`

* **Address**             : `0x21007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`mac_sa`| MAC SA value of TX APS Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###APS_TX_Header_VTL

* **Description**           

The register provides configuration of 18bytes Header of each channel ID receive from RX-OCN


* **RTL Instant Name**    : `upen_cfg_hdrvtl`

* **Address**             : `0x21008`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:32]`|`vlan`| 16b<br>{PCP,DEI,VID} value of VLAN field| `RW`| `0x0`| `0x0`|
|`[31:16]`|`ethtype`| 16b Ethernet Type field| `RW`| `0x0`| `0x88B7`|
|`[15:08]`|`apstype`| 8b Type field| `RW`| `0x0`| `0x0`|
|`[07:00]`|`version`| 8b Version Field| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

The register provides channel mapping from portID to ChannelID configuraton


* **RTL Instant Name**    : `upen_cid2pid`

* **Address**             : `0x21100  - 0x2110F`

* **Formula**             : `0x21100 + $portID`

* **Where**               : 

    * `$portID(0-15): Port ID`

* **Width**               : `16`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:00]`|`channelid_val`| 16b value of ChannelID of PortID| `RW`| `0x0`| `0x0 End: Begin:`|

###

* **Description**           

The register provides the initialization Kbyte value of each channelID

To init the content of each channelID, write bit31 = 0 first, then

write bit[31] = 1 and the channelId as well of Kbyte value of that

channelID. For example: to Init data FF for channel 0.

Step1: wr 0x21009 0

Step2: wr 0x21009 800000FF


* **RTL Instant Name**    : `upen_cfg_gencid`

* **Address**             : `0x21009`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`init_enable`| Enable ChannelID Data Initializtion| `RW`| `0x0`| `0x0`|
|`[30:24]`|`init_channelid`| ChannelID value| `RW`| `0x0`| `0x0`|
|`[23:00]`|`init_kbyteval`| Default 24b value of Kbyte| `RW`| `0x0`| `0x0 End: Begin:`|

###APS_RX_Header_MAC_DA_Configuration

* **Description**           

The register provides MAC DA value of provisioned APS port


* **RTL Instant Name**    : `upen_k12rx_macda`

* **Address**             : `0x22001`

* **Formula**             : `0x22001`

* **Where**               : 

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`mac_da`| Provisioned MAC DA value| `RW`| `0x0`| `0x0 End: Begin:`|

###APS_RX_Header_MAC_SA_Configuration

* **Description**           

The register provides MAC SA value of provisioned APS port


* **RTL Instant Name**    : `upen_k12rx_macsa`

* **Address**             : `0x22002`

* **Formula**             : `0x22002`

* **Where**               : 

* **Width**               : `48`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[47:00]`|`mac_sa`| Provisioned MAC SA value| `RW`| `0x0`| `0x0 End: Begin:`|

###APS_RX_Header_EVT_Configuration

* **Description**           

The register provides EVT(Ethernet Type, Version, APS Type) value of provisioned APS port


* **RTL Instant Name**    : `upen_k12rx_evt`

* **Address**             : `0x22003`

* **Formula**             : `0x22003`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`ethtyp`| Provisioned Ethernet Type| `RW`| `0x0`| `0x0`|
|`[15:08]`|`ver`| Provisioned Version Number| `RW`| `0x0`| `0x0`|
|`[07:00]`|`type`| Provisioned APS Type| `RW`| `0x0`| `0x0 End: Begin:`|

###APS_RX_ChannelID_Configuration

* **Description**           

The register provides ChannelID configuration


* **RTL Instant Name**    : `upen_k12rx_chcfg`

* **Address**             : `0x22100 - 0x2210F`

* **Formula**             : `0x22100 + $PortID`

* **Where**               : 

    * `$PortID(0-15): PortID value`

* **Width**               : `20`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11:00]`|`channelid`| 12b represent for Channel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###APS_RX_ChannelID_Mapping_Configuration

* **Description**           

The register provides ChannelID configuration


* **RTL Instant Name**    : `upen_k12rx_chmap`

* **Address**             : `0x22400 - 0x2240F`

* **Formula**             : `0x22400 + $ChannelID`

* **Where**               : 

    * `$ChannelID(0-15): ChannelID value`

* **Width**               : `20`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[03:00]`|`portid`| PortID value map from received Channel ID| `RW`| `0x0`| `0x0 End: Begin:`|

###APS_RX_Alarm_Sticky

* **Description**           

The register provides Receive APS Frame Alarm sticky


* **RTL Instant Name**    : `upen_k12rx_alarm`

* **Address**             : `0x22010`

* **Formula**             : `0x22010`

* **Where**               : 

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:08]`|`per_channel_mismat`| Received CHANNEL ID value of APS frame different from configured value. This is a per channel status. Bit 23 -> 08 represent for channel 15 -> 0 respectively| `W1C`| `0x0`| `0x0`|
|`[07:07]`|`frame_miss`| APS FRAME missed When watchdog timer is enable. This bit is set if no frame is received in pre-defined interval| `W1C`| `0x0`| `0x0`|
|`[06:06]`|`channel_miss`| CHANNEL mismatch. Received CHANNELID value different from configed| `W1C`| `0x0`| `0x0`|
|`[05:05]`|`real_len_miss`| ACTUAL LENGH mismatch. acket LENGTH value different 0x60| `W1C`| `0x0`| `0x0`|
|`[04:04]`|`len_miss`| LENGH mismatch. Received LENGTH value different 0x60| `W1C`| `0x0`| `0x0`|
|`[03:03]`|`ver_miss`| VERSION  mismatch. Received VERSION value different from configed VER| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`type_miss`| TYPE mismatch. Received TYPE value different from configed TYPE| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`ethtype_miss`| ETHERNET TYPE mismatch. Received ETHTYPE value different from configed E| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`mac_da_miss`| DA mismatch. Received DA value different from configed DA| `W1C`| `0x0`| `0x0 End: Begin:`|

###APS_RX_WatchDog_Timer

* **Description**           

The register provides WatchDog Timer configuration


* **RTL Instant Name**    : `upen_k12rx_watdog`

* **Address**             : `0x22008`

* **Formula**             : `0x22008`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:31]`|`enable`| Enable Watchdog Timer| `RW`| `0x0`| `0x0`|
|`[30:24]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[23:00]`|`timer_val`| Watch Dog Timer value. This value is the number of clock counter represent the expected time interval For ex: to set an interval 256us with the clock 155Mz The clock counter is: 256x10^3/(10^3/155) = 256*155 = 39680 This value will be write to this field. Timer range from 256us to 16ms. If in this timer window, no frames received, alarm will be set| `RW`| `0x0`| `0x0 End: Begin:`|

###DCC_OCN2ETH_Pkt_Length_Alarm_Sticky

* **Description**           

The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)


* **RTL Instant Name**    : `upen_stkerr_pktlen`

* **Address**             : `0x11004`

* **Formula**             : `0x11004`

* **Where**               : 

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:32]`|`dcc_overize_err`| HDLC Length Oversize Error. Alarm per channel Received HDLC Frame from OCN has frame length over maximum allowed length (1536bytes) Bit[63] -> Bit[32]: channel ID 31 ->0| `W1C`| `0x0`| `0x0`|
|`[31:00]`|`dcc_undsize_err`| HDLC Length Undersize Error. Alarm per channel Received HDLC Frame from OCN has frame length below minimum allowed length (2bytes) Bit[31] -> Bit[0]: channel ID 31 ->0| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC_OCN2ETH_Pkt_Error_And_Buffer_Full_Alarm_Sticky

* **Description**           

The register provides Alarm Related to HDLC CRC Error and Buffer Full (OCN2ETH Direction)


* **RTL Instant Name**    : `upen_stkerr_crcbuf`

* **Address**             : `0x11005`

* **Formula**             : `0x11005`

* **Where**               : 

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:32]`|`dcc_crc_err`| HDLC Packet has CRC error (OCN to ETH direction) . Alarm per channel Bit[63] -> Bit[32]: channel ID 31 ->0| `W1C`| `0x0`| `0x0`|
|`[31:00]`|`dcc_buffull_err`| HDLC Packet buffer full (OCN to ETH direction) . Alarm per channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[31] -> Bit[0]: channel ID 31 ->0| `W1C`| `0x0`| `0x0 End: Begin:`|

###DCC_ETH2OCN_Alarm_Sticky

* **Description**           

The register provides Alarms of ETH2OCN Direction


* **RTL Instant Name**    : `upen_stkerr_rx_eth2ocn`

* **Address**             : `0x12008`

* **Formula**             : `0x12008`

* **Where**               : 

* **Width**               : `69`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[68:37]`|`dcc_eth2ocn_buffull_err`| Packet buffer full (ETH to OCN direction) . Alarm per channel Buffer of Ethernet Frame has been full. Some frames will be dropped Bit[68] -> Bit[37]: channel ID 31 ->0| `W1C`| `0x0`| `0x0`|
|`[36:36]`|`dcc_rxeth_maxlenerr`| Received DCC packet from SGMII port has violated maximum packet's length error| `W1C`| `0x0`| `0x0`|
|`[35:35]`|`dcc_rxeth_crcerr`| Received DCC packet from SGMII port has CRC error| `W1C`| `0x0`| `0x0`|
|`[34:03]`|`dcc_channel_disable`| DCC Local Channel Identifier mapping is disable Bit[34] -> Bit[03] indicate channel 31-> 00.| `W1C`| `0x0`| `0x0`|
|`[02:02]`|`dcc_cvlid_mismat`| Received 12b CVLAN ID value of DCC frame different from global provisioned CVID| `W1C`| `0x0`| `0x0`|
|`[01:01]`|`dcc_macda_mismat`| Received 43b MAC DA value of DCC frame different from global provisioned DA| `W1C`| `0x0`| `0x0`|
|`[00:00]`|`dcc_ethtp_mismat`| Received Ethernet Type of DCC frame different from global provisioned value| `W1C`| `0x0`| `0x0 End: Begin:`|

###APS_RX_Alarm_Status

* **Description**           

The register provides current status of APS alarm


* **RTL Instant Name**    : `upen_k12rx_cur_err`

* **Address**             : `0x22012`

* **Formula**             : `0x22012`

* **Where**               : 

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:08]`|`per_channel_mismat`| Received CHANNEL ID value of APS frame different from configured value. This is a per channel status. Bit 23 -> 08 represent for channel 15 -> 0 respectively| `R_O`| `0x0`| `0x0`|
|`[07:07]`|`frame_miss`| Current status of APS FRAME missed alarm. When watchdog timer is enable. This bit is set if no frame is received in pre-defined interval| `RO`| `0x0`| `0x0`|
|`[06:06]`|`channel_miss`| Current status of global CHANNEL mismatch alarm. Received CHANNELID value different from configed| `RO`| `0x0`| `0x0`|
|`[05:05]`|`real_len_miss`| Current status of ACTUAL LENGH mismatch alarm. acket LENGTH value different 0x60| `RO`| `0x0`| `0x0`|
|`[04:04]`|`len_miss`| Current status of LENGH mismatch alarm. Received LENGTH value different 0x60| `RO`| `0x0`| `0x0`|
|`[03:03]`|`ver_miss`| Current status of VERSION  mismatch alarm. Received VERSION value different from configed VER| `RO`| `0x0`| `0x0`|
|`[02:02]`|`type_miss`| Current status of TYPE mismatch alarm. Received TYPE value different from configed TYPE| `RO`| `0x0`| `0x0`|
|`[01:01]`|`ethtype_miss`| Current status of ETHERNET TYPE mismatch alarm. Received ETHTYPE value different from configed value| `RO`| `0x0`| `0x0`|
|`[00:00]`|`mac_da_miss`| DA mismatch. Received DA value different from configed DA| `RO`| `0x0`| `0x0 End: Begin:`|

###DCC_OCN2ETH_Min_Pkt_Length_Alarm_Status

* **Description**           

The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)


* **RTL Instant Name**    : `upen_curerr_pktlen1`

* **Address**             : `0x11008`

* **Formula**             : `0x11008`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`dcc_undsize_err`| HDLC Length Undersize Error. Status per channel Received HDLC Frame from OCN has frame length below minimum allowed length (2bytes) Bit[31] -> Bit[0]: channel ID 31 ->0| `RO`| `0x0`| `0x0 End: Begin:`|

###DCC_OCN2ETH_Max_Pkt_Length_Alarm_Status

* **Description**           

The register provides Alarm Status Related to HDLC Length Error (OCN2ETH Direction)


* **RTL Instant Name**    : `upen_curerr_pktlen2`

* **Address**             : `0x11009`

* **Formula**             : `0x11009`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`dcc_overize_err`| HDLC Length Oversize Error. Status per channel Received HDLC Frame from OCN has frame length over maximum allowed length (1536bytes) Bit[31] -> Bit[0]: channel ID 31 ->0| `RO`| `0x0`| `0x0 End: Begin:`|

###DCC_OCN2ETH_Pkt_CRC_Error_Alarm_Status

* **Description**           

The register provides Alarm Status Related to HDLC CRC Error (OCN2ETH Direction)


* **RTL Instant Name**    : `upen_curerr_crcbuf`

* **Address**             : `0x1100A`

* **Formula**             : `0x1100A`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`dcc_crc_err`| HDLC Packet has CRC error (OCN to ETH direction) . Alarm per channel Bit[31] -> Bit[0]: channel ID 31 ->0| `RO`| `0x0`| `0x0 End: Begin:`|

###DCC_OCN2ETH_Buffer_Full_Alarm_Status

* **Description**           

The register provides Alarm Related to HDLC Buffer Full (OCN2ETH Direction)


* **RTL Instant Name**    : `upen_curerr_bufept`

* **Address**             : `0x1100B`

* **Formula**             : `0x1100B`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`dcc_buffull_err`| HDLC Packet buffer full (OCN to ETH direction) . Alarm per channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[31] -> Bit[0]: channel ID 31 ->0| `RO`| `0x0`| `0x0 End: Begin:`|

###DCC_ETH2OCN_Alarm_Status

* **Description**           

The register provides Alarms of ETH2OCN Direction


* **RTL Instant Name**    : `upen_dcc_rx_glb_sta`

* **Address**             : `0x1200A`

* **Formula**             : `0x1200A`

* **Where**               : 

* **Width**               : `68`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[67:36]`|`sta_dcc_eth2ocn_buffull_err`| Packet buffer full (ETH to OCN direction) . Status per channel Buffer of Ethernet Frame has been full. Some frames will be dropped Bit[67] -> Bit[36]: channel ID 31 ->0| `RO`| `0x0`| `0x0`|
|`[35:35]`|`sta_dcc_rxeth_maxlenerr`| "Received DCC packet from SGMII port has violated maximum packet's length error" status| `RO`| `0x0`| `0x0`|
|`[34:03]`|`sta_dcc_channel_disable`| "DCC Local Channel Identifier mapping is disable" status Bit[34] -> Bit[03] indicate channel 31-> 00.| `RO`| `0x0`| `0x0`|
|`[02:02]`|`sta_dcc_cvlid_mismat`| Received 12b CVLAN ID value of DCC frame different from global provisioned CVID| `RO`| `0x0`| `0x0`|
|`[01:01]`|`sta_dcc_macda_mismat`| Received 43b MAC DA value of DCC frame different from global provisioned DA| `RO`| `0x0`| `0x0`|
|`[00:00]`|`sta_dcc_ethtp_mismat`| Received Ethernet Type of DCC frame different from global provisioned value| `RO`| `0x0`| `0x0 End: Begin:`|

###APS_RX_Trig_En_Cap

* **Description**           

The register provides WatchDog Timer configuration


* **RTL Instant Name**    : `upen_k12rx_trig_encap`

* **Address**             : `0x22006`

* **Formula**             : `0x22006`

* **Where**               : 

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[00:00]`|`enable`| Trigger Enable Capturing Ethernet Header - Write '0' first, then write '1' to enable| `RW`| `0x0`| `0x0 End: Begin:`|

###APS_RX_Packet_Header_Cap_Reg0

* **Description**           

The register provides captured header data


* **RTL Instant Name**    : `upen_k12rx_cap_reg0`

* **Address**             : `0x22200 - 0x2220C`

* **Formula**             : `0x22200 + 3*$pktnum`

* **Where**               : 

    * `$pktnum(0-4): Number of packet capture`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:16]`|`mac_da`| Captured MAC DA value| `RO`| `0x0`| `0x0`|
|`[15:00]`|`mac_sa`| 16b MSB of Captured MAC SA value| `RO`| `0x0`| `0x0 End: Begin:`|

###APS_RX_Packet_Header_Cap_Reg1

* **Description**           

The register provides captured header data


* **RTL Instant Name**    : `upen_k12rx_cap_reg1`

* **Address**             : `0x22201 - 0x2220D`

* **Formula**             : `0x22201 + 3*$pktnum`

* **Where**               : 

    * `$pktnum(0-4): Number of packet capture`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:32]`|`mac_sa`| 32b MSB of Captured MAC SA value| `RO`| `0x0`| `0x0`|
|`[31:00]`|`vlan`| Captured VLAN value:<br>{TPID,PCP,DEI,VID}| `RO`| `0x0`| `0x0 End: Begin:`|

###APS_RX_Packet_Header_Cap_Reg2

* **Description**           

The register provides captured header data


* **RTL Instant Name**    : `upen_k12rx_cap_reg2`

* **Address**             : `0x22202 - 0x2220E`

* **Formula**             : `0x22202 + 3*$pktnum`

* **Where**               : 

    * `$pktnum(0-4): Number of packet capture`

* **Width**               : `64`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[63:48]`|`eth_typ`| 16b value of Ethernet Type| `RO`| `0x0`| `0x0`|
|`[47:40]`|`ver`| 8b Version| `RO`| `0x0`| `0x0`|
|`[39:32]`|`typ`| 8b APS Type| `RO`| `0x0`| `0x0`|
|`[31:16]`|`length`| 16b packet length| `RO`| `0x0`| `0x0`|
|`[15:00]`|`unused`| *n/a*| *n/a*| *n/a*| `End: Begin:`|

###Counter_Rx_Unknow_From_SGMII_Port0

* **Description**           

Counter of number of Unknow Packet (not APS packet) receive from SGMII port 0


* **RTL Instant Name**    : `upen_k12_glbcnt_sfail`

* **Address**             : `0x22024`

* **Formula**             : `0x22024`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`glb_sgm2ocn_unk_counter`| Counter of unknow receive packet from SGMII port 0| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Total_Rx_APS_From_SGMII_Port0

* **Description**           

Counter of total number of detected APS Packet receive from SGMII port 0


* **RTL Instant Name**    : `upen_k12_glbcnt_sok`

* **Address**             : `0x22025`

* **Formula**             : `0x22025`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`glb_sgm2ocn_sok_counter`| Counter of detected APS packet from SGMII port 0| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_APS_From_SGMII_Port0_Per_Chan

* **Description**           

Counter of number of detected APS Packet per channel receive from SGMII port 0 per channelID


* **RTL Instant Name**    : `upen_k12_sokpkt_pcid`

* **Address**             : `0x22300 - 0x2230F`

* **Formula**             : `0x22300 + $channelID`

* **Where**               : 

    * `$channelID(0-15): APS channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pcid_sgm2ocn_sok_counter`| Counter of APS packets receive from SGMII port 0 per channelID| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_APS_BYTE_From_RxOcn_Per_Chan

* **Description**           

Counter number of BYTE of APS packet receive from RX-OCN Per Channel


* **RTL Instant Name**    : `upen_byt2k12_pcid`

* **Address**             : `0x21330 - 0x2133F`

* **Formula**             : `0x21330 + $channelID`

* **Where**               : 

    * `$channelID(0-15): APS channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`byte_counter`| Counter number of BYTE of APS packets receive from RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Total_Rx_APS_Packet_From_RxOcn

* **Description**           

Counter Total number of RX APS packet receive from RX-OCN


* **RTL Instant Name**    : `upen_glbsop2k12`

* **Address**             : `0x21360`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_aps_pkt_cnt`| Counter number of APS packets receive from RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_APS_BYTE_From_RxOcn_Per_Chan

* **Description**           

Counter number of Total BYTE of APS packet receive from RX-OCN


* **RTL Instant Name**    : `upen_glbbyt2k12`

* **Address**             : `0x21361`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_aps_byte_counter`| Counter number of BYTE of APS packets receive from RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_APS_SOP_To_SGMII

* **Description**           

Counter number of SOP of TX APS packet to SGMII


* **RTL Instant Name**    : `upen_k12sop2sgm_pcid`

* **Address**             : `0x21350`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sop_counter`| Counter number of SOP of TX APS packets to SGMII| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_APS_EOP_To_SGMII

* **Description**           

Counter number of EOP of TX APS packet to SGMII


* **RTL Instant Name**    : `upen_k12eop2sgm_pcid`

* **Address**             : `0x21351`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`eop_counter`| Counter number of EOP of TX APS packets to SGMII| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_APS_BYTE_To_SGMII

* **Description**           

Counter number of BYTE of TX APS packet to SGMII


* **RTL Instant Name**    : `upen_k12byt2sgm_pcid`

* **Address**             : `0x21352`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`byte_counter`| Counter number of BYTE of TX APS packets to SGMII| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Sop_From_SGMII_Port

* **Description**           

Counter of number of SOP receive from SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmrxsop`

* **Address**             : `0x12020`

* **Formula**             : `0x12020`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sop_counter`| Counter of SOP receive from SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Eop_From_SGMII_Port

* **Description**           

Counter of number of EOP receive from SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmrxeop`

* **Address**             : `0x12021`

* **Formula**             : `0x12021`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`eop_counter`| Counter of EOP receive from SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Err_From_SGMII_Port

* **Description**           

Counter of number of ERR receive from SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmrxerr`

* **Address**             : `0x12022`

* **Formula**             : `0x12022`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`err_counter`| Counter of ERR receive from SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Byte_From_SGMII_Port

* **Description**           

Counter of number of BYTE receive from SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmrxbyt`

* **Address**             : `0x12023`

* **Formula**             : `0x12023`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`byte_counter`| Counter of BYTE receive from SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Unknow_From_SGMII_Port

* **Description**           

Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmrxfail`

* **Address**             : `0x12024`

* **Formula**             : `0x12024`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`unk_counter`| Counter of unknow receive packet from SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_DCC_Pkt_From_SGMII_Port1_Per_Chan

* **Description**           

Counter of number of DCC Packet From SGMII Port Per Channel


* **RTL Instant Name**    : `upen_dcc_sokpkt_pcid`

* **Address**             : `0x12100 - 0x1211F`

* **Formula**             : `0x12100 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_pkt_cnt`| Counter of DCC packets receive from SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_DCC_Byte_From_SGMII_Port1_Per_Chan

* **Description**           

Counter of number of DCC Bytes From SGMII Port Per Channel


* **RTL Instant Name**    : `upen_dcc_sokbyt_pcid`

* **Address**             : `0x121C0 - 0x121DF`

* **Formula**             : `0x121C0 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_byte_cnt`| Counter of DCC Bytes receive from SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_DCC_SOP_From_RxOcn_Per_Chan

* **Description**           

Counter number of SOP of DCC packet receive from RX-OCN


* **RTL Instant Name**    : `upen_sop2dcc_pcid`

* **Address**             : `0x11400 - 0x1141F`

* **Formula**             : `0x11400 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sop_ocn2sgm_cnt_pcid`| Counter number of SOP of DCC packets receive from RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_DCC_EOP_From_RxOcn_Per_Chan

* **Description**           

Counter number of EOP of DCC packet receive from RX-OCN


* **RTL Instant Name**    : `upen_eop2dcc_pcid`

* **Address**             : `0x11420 - 0x1143F`

* **Formula**             : `0x11420 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`eop_ocn2sgm_cnt_pcid`| Counter number of EOP of DCC packets receive from RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_DCC_ERR_From_RxOcn_Per_Chan

* **Description**           

Counter number of ERR of DCC packet receive from RX-OCN


* **RTL Instant Name**    : `upen_err2dcc_pcid`

* **Address**             : `0x11440 - 0x1145F`

* **Formula**             : `0x11440 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`err_ocn2sgm_cnt_pcid`| Counter number of ERR of DCC packets receive from RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_DCC_BYTE_From_RxOcn_Per_Chan

* **Description**           

Counter number of BYTE of DCC packet receive from RX-OCN


* **RTL Instant Name**    : `upen_byt2dcc_pcid`

* **Address**             : `0x11460 - 0x1147F`

* **Formula**             : `0x11460 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`byt_ocn2sgm_cnt_pcid`| Counter number of BYTE of DCC packets receive from RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_DCC_Drp_From_RxOcn_Per_Chan

* **Description**           

Counter number of Drop of DCC packet (from OCN to ETH direction) due to Buffer full


* **RTL Instant Name**    : `upen_errfull_pcid`

* **Address**             : `0x11480	  - 0x1149F`

* **Formula**             : `0x11480 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`errfull_ocn2sgm_cnt_pcid`| Counter number of Dropped DCC packets receive from OCN due to buffer full| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Sop_From_OCN

* **Description**           

Counter of number of SOP receive from RX-OCN


* **RTL Instant Name**    : `upen_dcc_glbcnt_ocnrxsop`

* **Address**             : `0x11504`

* **Formula**             : `0x11504`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sop_ocn2sgm_cnt_glb`| Counter of Total SOP receive from RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Eop_From_OCN

* **Description**           

Counter of number of EOP receive from RX-OCN


* **RTL Instant Name**    : `upen_dcc_glbcnt_ocnrxeop`

* **Address**             : `0x11505`

* **Formula**             : `0x11505`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`eop_ocn2sgm_cnt_glb`| Counter of Total EOP receive from RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Err_From_OCN

* **Description**           

Counter of number of ERR receive from RX-OCN


* **RTL Instant Name**    : `upen_dcc_glbcnt_ocnrxerr`

* **Address**             : `0x11506`

* **Formula**             : `0x11506`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`err_ocn2sgm_cnt_glb`| Counter of Total ERR  receive from RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Rx_Byte_From_OCN

* **Description**           

Counter of number of BYTE receive from RX-OCN


* **RTL Instant Name**    : `upen_dcc_glbcnt_ocnrxbyt`

* **Address**             : `0x11507`

* **Formula**             : `0x11507`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`byt_ocn2sgm_cnt_glb`| Counter of Total RX BYTE from RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_DCC_SOP_To_SGMII_Per_Chan

* **Description**           

Counter number of SOP of TX DCC packet to SGMII


* **RTL Instant Name**    : `upen_dccsop2sgm_pcid`

* **Address**             : `0x114A0 - 0x114BF`

* **Formula**             : `0x114A0 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sop_counter`| Counter number of SOP of TX DCC packets to SGMII| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_DCC_EOP_To_SGMII_Per_Chan

* **Description**           

Counter number of EOP of TX DCC packet to SGMII


* **RTL Instant Name**    : `upen_dcceop2sgm_pcid`

* **Address**             : `0x114C0 - 0x114DF`

* **Formula**             : `0x114C0 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`eop_counter`| Counter number of EOP of TX DCC packets to SGMII| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_DCC_BYTE_To_SGMII_Per_Chan

* **Description**           

Counter number of BYTE of TX DCC packet to SGMII


* **RTL Instant Name**    : `upen_dccbyt2sgm_pcid`

* **Address**             : `0x114E0 - 0x114FF`

* **Formula**             : `0x114E0 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`byte_counter`| Counter number of BYTE of TX DCC packets to SGMII| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_Sop_To_SGMII_Port1

* **Description**           

Counter of number of SOP transmit to SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmtxsop`

* **Address**             : `0x11500`

* **Formula**             : `0x11500`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sgm_txglb_sop_counter`| Counter of SOP transmit to SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_Eop_To_SGMII_Port1

* **Description**           

Counter of number of EOP transmit to SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmtxeop`

* **Address**             : `0x11501`

* **Formula**             : `0x11501`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sgm_txglb_eop_counter`| Counter of EOP transmit to SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_Err_To_SGMII_Port1

* **Description**           

Counter of number of ERR transmit to SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmtxerr`

* **Address**             : `0x11502`

* **Formula**             : `0x11502`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sgm_txglb_err_counter`| Counter of ERR  transmit to SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Tx_Byte_To_SGMII_Port1

* **Description**           

Counter of number of BYTE transmit to SGMII port 1


* **RTL Instant Name**    : `upen_dcc_glbcnt_sgmtxbyt`

* **Address**             : `0x11503`

* **Formula**             : `0x11503`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sgm_txglb_byte_counter`| Counter of TX BYTE to SGMII port 1| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_DCC_SOP_to_RxOcn_Per_Chan

* **Description**           

Counter number of SOP of DCC packet transmit to RX-OCN Per ChannelID


* **RTL Instant Name**    : `upen_sop2ocn_pcid`

* **Address**             : `0x12120 - 0x1213f`

* **Formula**             : `0x12120 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sop_sgm2ocn_counter`| Counter number of SOP of DCC packets transmit to RX-OCN	Per ChannelID| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_DCC_EOP_to_RxOcn_Per_Chan

* **Description**           

Counter number of EOP of DCC packet transmit to RX-OCN Per ChannelID


* **RTL Instant Name**    : `upen_eop2ocn_pcid`

* **Address**             : `0x12140 - 0x1215F`

* **Formula**             : `0x12140 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`eop_sgm2ocn_counter`| Counter number of EOP of DCC packets transmit to RX-OCN Per ChannelID| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_DCC_ERR_to_RxOcn_Per_Chan

* **Description**           

Counter number of ERR of DCC packet transmit to RX-OCN Per ChannelID


* **RTL Instant Name**    : `upen_err2ocn_pcid`

* **Address**             : `0x12160 - 0x1217F`

* **Formula**             : `0x12160 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`err_sgm2ocn_counter`| Counter number of ERR of DCC packets transmit to RX-OCN Per ChannelID| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_DCC_EPT_to_RxOcn_Per_Chan

* **Description**           

Counter number of EPT of DCC packet transmit to RX-OCN Per ChannelID


* **RTL Instant Name**    : `upen_ept2ocn_pcid`

* **Address**             : `0x12180 - 0x1219F`

* **Formula**             : `0x12180 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`ept_sgm2ocn_counter`| Counter number of EPT signal respond to request from RX-OCN Per ChannelID. An empty indicate data is not available when OCN request DCC data| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_DCC_BYTE_to_RxOcn_Per_Chan

* **Description**           

Counter number of BYTE of DCC packet transmit to RX-OCN Per ChannelID


* **RTL Instant Name**    : `upen_byt2ocn_pcid`

* **Address**             : `0x121A0 - 0x121BF`

* **Formula**             : `0x121A0 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`byt_sgm2ocn_counter`| Counter number of BYTE of DCC packets transmit to RX-OCN	Per ChannelID| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_DCC_Drop_to_RxOcn_Per_Chan

* **Description**           

Counter number of Drop DCC packet transmit to RX-OCN Due to Buffer Full Per ChannelID


* **RTL Instant Name**    : `upen_ful2ocn_pcid`

* **Address**             : `0x121E0 - 0x121FF`

* **Formula**             : `0x121E0 + $channelID`

* **Where**               : 

    * `$channelID(0-31): DCC channelID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`drp_sgm2ocn_counter`| Counter number of Drop DCC packets transmit to RX-OCN due to Buffer Full Per ChannelID| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Total_Tx_Sop_To_OCN

* **Description**           

Counter Total number of SOP transmit to RX-OCN


* **RTL Instant Name**    : `upen_dcc_glbcnt_ocntxsop`

* **Address**             : `0x12025`

* **Formula**             : `0x12025`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`sop_sgm2ocn_cnt_glb`| Counter of Total SOP transmit to RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Total_Tx_Eop_To_OCN

* **Description**           

Counter Total number of EOP transmit to RX-OCN


* **RTL Instant Name**    : `upen_dcc_glbcnt_ocntxeop`

* **Address**             : `0x12026`

* **Formula**             : `0x12026`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`eop_sgm2ocn_cnt_glb`| Counter of Total EOP transmit to RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Total_Tx_Err_To_OCN

* **Description**           

Counter Total number of ERR transmit to RX-OCN


* **RTL Instant Name**    : `upen_dcc_glbcnt_ocntxerr`

* **Address**             : `0x12027`

* **Formula**             : `0x12027`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`err_sgm2ocn_cnt_glb`| Counter of Total ERR transmit to RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Total_Tx_Byte_To_OCN

* **Description**           

Counter Total number of BYTE transmit to RX-OCN


* **RTL Instant Name**    : `upen_dcc_glbcnt_ocntxbyt`

* **Address**             : `0x12028`

* **Formula**             : `0x12028`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`byt_sgm2ocn_cnt_glb`| Counter of Total BYTE transmit to RX-OCN| `WC`| `0x0`| `0x0 End: Begin:`|

###Counter_Total_Drop_To_RxOcn_Pkt

* **Description**           

Counter Total number Packet Drop due to buffer full (ETH2OCN Direction)


* **RTL Instant Name**    : `upen_dcc_glbcnt_ful2ocn`

* **Address**             : `0x12029`

* **Formula**             : `0x12029`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`drp_pkt_sgm2ocn_cnt_glb`| Counter of Total Packet Dropped Due to Buffer full (ETH to RX-OCN)| `WC`| `0x0`| `0x0 End: Begin:`|

###CONFIG HDLC LO DEC

* **Description**           

config HDLC ID 0-31

HDL_PATH: iaf6cci0012_lodec_core.ihdlc_cfg.imem113x.ram.ram[$CID]


* **RTL Instant Name**    : `upen_hdlc_locfg`

* **Address**             : `0x04000 - 0x0401F`

* **Formula**             : `0x04000+ $CID`

* **Where**               : 

    * `$CID (0-31) : Channel ID`

* **Width**               : `5`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[4]`|`cfg_scren`| config to enable scramble, (1) is enable, (0) is disable| `R/W`| `0x0`| `0x0`|
|`[3]`|`reserve`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[2]`|`cfg_bitstuff`| config to select bit stuff or byte sutff, (1) is bit stuff, (0) is byte stuff| `R/W`| `0x0`| `0x0`|
|`[1]`|`cfg_fcsmsb`| config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB| `R/W`| `0x0`| `0x0`|
|`[0]`|`cfg_fcsmode`| config to calculate FCS 32 or FCS 16, (1) is FCS 32, (0) is FCS 16| `R/W`| `0x0`| `0x0 End: Begin:`|

###CONFIG HDLC GLOBAL LO DEC

* **Description**           

config HDLC global


* **RTL Instant Name**    : `upen_hdlc_loglbcfg`

* **Address**             : `0x04404`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[3]`|`cfg_lsbfirst`| config to receive LSB first, (1) is LSB first, (0) is default MSB| `R/W`| `0x0`| `0x0`|
|`[2:0]`|`unused`| *n/a*| *n/a*| *n/a*| `End: Begin:`|

###HDLC Encode Master Control

* **Description**           

config HDLC Encode Master


* **RTL Instant Name**    : `upen_hdlc_enc_master_ctrl`

* **Address**             : `0x30003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `1`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`encap_lsbfirst`| config to transmit LSB first, (1) is LSB first, (0) is default MSB| `R/W`| `0x0`| `0x0 End: Begin:`|

###HDLC Encode Control Reg 1

* **Description**           

config HDLC Encode Control Register 1


* **RTL Instant Name**    : `upen_hdlc_enc_ctrl_reg1`

* **Address**             : `0x38000 - 0x3801F`

* **Formula**             : `0x38000+ $CID`

* **Where**               : 

    * `$CID (0-31) : Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[30:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:09]`|`encap_screnb`| Enable Scamble (1) Enable      , (0) disable| `R/W`| `0x0`| `0x0`|
|`[08:08]`|`reserved`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[07:07]`|`encap_idlemod`| This bit is only used in Bit Stuffing mode Used to configured IDLE Mode When it is active, the ENC engine will insert '1' pattern when the ENC is idle. Otherwise the ENC will insert FLAG '7E' pattern (1) Enable            , (0) Disabe| `R/W`| `0x0`| `0x0`|
|`[06:06]`|`encap_sabimod`| Sabi/Protocol Field Mode (1) Field has 2 bytes , (0) field has 1 byte| `R/W`| `0x0`| `0x0`|
|`[05:05]`|`encap_sabiins`| Sabi/Protocol Field Insert Enable (1) Enable Insert     , (0) Disable Insert| `R/W`| `0x0`| `0x0`|
|`[04:04]`|`encap_ctrlins`| Address/Control Field Insert Enable (1) Enable Insert     , (0) Disable Insert| `R/W`| `0x0`| `0x0`|
|`[03:03]`|`encap_fcsmod`| FCS Select Mode (1) 32b FCS           , (0) 16b FCS| `R/W`| `0x0`| `0x0`|
|`[02:02]`|`encap_fcsins`| FCS Insert Enable (1) Enable Insert     , (0) Disable Insert| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`encap_flgmod`| Flag Mode (1) Minimum 2 Flag    , (0) Minimum 1 Flag| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`encap_stfmod`| Stuffing Mode (1) Bit Stuffing      , (0) Byte Stuffing| `R/W`| `0x0`| `0x0 End: Begin:`|

###HDLC Encode Control Reg 2

* **Description**           

config HDLC Encode Control Register 2 Byte Stuff Mode


* **RTL Instant Name**    : `upen_hdlc_enc_ctrl_reg2`

* **Address**             : `0x39000 - 0x3901F`

* **Formula**             : `0x39000+ $CID`

* **Where**               : 

    * `$CID (0-31) : Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`encap_addrval`| Address Field| `R/W`| `0x0`| `0x0`|
|`[23:16]`|`encap_ctlrval`| Control Field| `R/W`| `0x0`| `0x0`|
|`[15:08]`|`encap_sapival0`| SAPI/PROTOCOL Field 1st byte| `R/W`| `0x0`| `0x0`|
|`[07:00]`|`encap_sapival1`| SAPI/PROTOCOL Field 2nd byte| `R/W`| `0x0`| `0x0 End: Begin:`|

###Enable Packet Capture

* **Description**           

config enable capture ethernet packet.


* **RTL Instant Name**    : `upen_trig_encap`

* **Address**             : `0x0A000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `4`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[03:03]`|`ram_trig_fls`| Trigger flush packets captured RAM Write '0' first, then write '1' to trigger flush RAM process| *n/a*| *n/a*| *n/a*|
|`[02:02]`|`enb_cap_dec`| Enable capture packets from HDLC Decapsulation. Only one type of packet (DCC/APS/HDLC) is captured at a time Write '0' first, then write '1' to trigger capture process (1) Enable            , (0) Disable| `R/W`| `0x0`| `0x0`|
|`[01:01]`|`enb_cap_aps`| Enable capture APS packets from SGMII interface. Only one type of packet (DCC/APS/HDLC) is captured at a time Write '0' first, then write '1' to trigger capture process (1) Enable            , (0) Disable| `R/W`| `0x0`| `0x0`|
|`[00:00]`|`enb_cap_dcc`| Enable capture DCC packets from SGMII interface. Only one type of packet (DCC/APS/HDLC) is captured at a time Write '0' first, then write '1' to trigger capture process (1) Enable            , (0) Disable| `R/W`| `0x0`| `0x0 End: Begin:`|

###Captured Packet Data

* **Description**           

Captured packet data


* **RTL Instant Name**    : `upen_pktcap`

* **Address**             : `0x08000 - 0x080FF`

* **Formula**             : `0x08000 + $loc`

* **Where**               : 

    * `$loc(0-255) : data location`

* **Width**               : `70`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[69:69]`|`cap_sop`| Start of packet| `RO`| `0x0`| `0x0`|
|`[68:68]`|`cap_eop`| End   of packet| `RO`| `0x0`| `0x0`|
|`[67:67]`|`cap_err`| Error of packet| `RO`| `0x0`| `0x0`|
|`[66:64]`|`cap_nob`| Number of valid bytes in 8-bytes data captured| `RO`| `0x0`| `0x0`|
|`[63:00]`|`cap_dat`| packet data captured| `RO`| `0x0`| `0x0 End: Begin:`|

###APS_Acc_Received_Kbyte_Value_Per_Channel

* **Description**           

The register provides received Kbyte of each channel ID


* **RTL Instant Name**    : `upen_k12acc`

* **Address**             : `0x22800 - 0x228FF`

* **Formula**             : `0x22800 + $channelid*16 + $loc`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

    * `$loc(0-15): addresses reserved per channel`

* **Width**               : `24`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[23:00]`|`kbyte_value`| Received Kbyte value of each channel [23:16]: K1 Byte [15:08]: K2 Byte [07:00]: Extend Kbyte| `RO`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Sdh_Req_Interval_Configuration

* **Description**           

The register is used to configure time interval to generate fake SDH request signal


* **RTL Instant Name**    : `upen_genmon_reqint`

* **Address**             : `0x00003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `29`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28:28]`|`timer_enable`| Enable generation of fake request SDH signal| `RW`| `0x0`| `0x0`|
|`[27:00]`|`timer_value`| Counter of number of clk 155MHz between fake request SDH signal. This counter is used make a delay interval between 2 consecutive SDH requests generated by DCC GENERATOR. For example: to create a delay of 125us between SDH request signals, with a clock 155Mz, the value of counter to be configed to this field is (125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Config_Test_Gen_Header_Per_Channel

* **Description**           

The register provides data for configuration of VID and MAC DA Header of each channel ID


* **RTL Instant Name**    : `upen_dcc_cfg_testgen_hdr`

* **Address**             : `0x0C100 - 0x0C120`

* **Formula**             : `0x0C100 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `20`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[19:08]`|`hdr_vid`| 12b of VID value in header of Generated Packet| `RW`| `0x0`| `0x0`|
|`[07:00]`|`mac_da`| Bit [7:0] of MAC DA value [47:0] of generated Packet| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Config_Test_Gen_Mode_Per_Channel

* **Description**           

The register provides data for configuration of generation mode of each channel ID


* **RTL Instant Name**    : `upen_dcc_cfg_testgen_mod`

* **Address**             : `0x0C200 - 0x0C220`

* **Formula**             : `0x0C200 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`gen_payload_mod`| Modes of generated packet payload:<br>{0}: payload will increase <br>{1}: payload is PRBS <br>{2}: payload is fill with 8b pattern configed in gen_fix_pattern field of this register| `RW`| `0x0`| `0x0`|
|`[29:28]`|`gen_len_mode`| Modes of generated packet length :<br>{0}: fix length <br>{1}: increase length <br>{2}: random length| `RW`| `0x0`| `0x0`|
|`[27:20]`|`gen_fix_pattern`| 8b fix pattern payload if mode "fix payload" is used| `RW`| `0x0`| `0x0`|
|`[19:00]`|`gen_number_of_packet`| Number of packets the GEN generates for each channelID in gen burst mode| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Config_Test_Gen_Global_Gen_Enable_Channel

* **Description**           

The register provides configuration to enable Channel to generate DCC packets


* **RTL Instant Name**    : `upen_dcc_cfg_testgen_enacid`

* **Address**             : `0x0C000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`channel_enable`| Enable Channel to generate DCC packets. Bit[31:0] <-> Channel 31->0| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Config_Test_Gen_Global_Gen_Mode

* **Description**           

The register provides global configuration of GEN mode


* **RTL Instant Name**    : `upen_dcc_cfg_testgen_glb_gen_mode`

* **Address**             : `0x0C001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[06:06]`|`gen_force_err_len`| Create wrong packet's length fiedl generated packets| `RW`| `0x0`| `0x0`|
|`[05:05]`|`gen_force_err_fcs`| Create wrong FCS field in generated packets| `RW`| `0x0`| `0x0`|
|`[04:04]`|`gen_force_err_dat`| Create wrong data value in generated packets| `RW`| `0x0`| `0x0`|
|`[03:03]`|`gen_force_err_seq`| Create wrong Sequence field in generated packets| `RW`| `0x0`| `0x0`|
|`[02:02]`|`gen_force_err_vcg`| Create wrong VCG field in generated packets| `RW`| `0x0`| `0x0`|
|`[01:01]`|`gen_burst_mod`| Gen each Channel a number of packet as configured in "Gen mode per channel" resgister| `RW`| `0x0`| `0x0`|
|`[00:00]`|`gen_conti_mod`| Gen packet forever without stopping| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Config_Test_Gen_Global_Packet_Length

* **Description**           

The register provides configuration min length max length of generated Packets


* **RTL Instant Name**    : `upen_dcc_cfg_testgen_glb_length`

* **Address**             : `0x0C002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`gen_max_length`| Maximum length of generated packet| `RW`| `0x0`| `0x0`|
|`[15:00]`|`gen_min_length`| Minimum length of generated packet, also used for fix length packets in case fix length mode is used| `RW`| `0x0`| `0x0 Begin:`|

###DDC_Config_Test_Gen_Global_Gen_Interval

* **Description**           

The register provides configuration for packet generating interval


* **RTL Instant Name**    : `upen_dcc_cfg_testgen_glb_gen_interval`

* **Address**             : `0x0C003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `29`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28:28]`|`gen_intv_enable`| Enable using this interval value| `RW`| `0x0`| `0x0`|
|`[27:00]`|`gen_intv_value`| Counter of number of clk 155MHz between packet generation. This counter is used make a delay interval between 2 consecutive packet generation. For example: to create a delay of 125us between 2 packet generating, with a clock 155Mz, the value of counter to be configed to this field is (125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF| `RW`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Mon_Good_Packet_Counter

* **Description**           

Counter of receive good packet


* **RTL Instant Name**    : `upen_mon_godpkt`

* **Address**             : `0x0E100 - 0x0E120`

* **Formula**             : `0x0E100 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_test_mon_good_cnt`| Counter of Receive Good Packet from TEST GEN| `WC`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Mon_Error_Data_Packet_Counter

* **Description**           

Counter of received packets has error data


* **RTL Instant Name**    : `upen_mon_errpkt`

* **Address**             : `0x0E200 - 0x0E220`

* **Formula**             : `0x0E200 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_test_mon_errdat_cnt`| Counter of Receive Error Data Packet from TEST GEN| `WC`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Mon_Error_VCG_Packet_Counter

* **Description**           

Counter of received packet has wrong VCG value


* **RTL Instant Name**    : `upen_mon_errvcg`

* **Address**             : `0x0E300 - 0x0E320`

* **Formula**             : `0x0E300 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_test_mon_errvcg_cnt`| Counter of Receive Error VCG Packet from TEST GEN| `WC`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Mon_Error_SEQ_Packet_Counter

* **Description**           

Counter of received packet has wrong Sequence value


* **RTL Instant Name**    : `upen_mon_errseq`

* **Address**             : `0x0E400 - 0x0E420`

* **Formula**             : `0x0E400 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_test_mon_errseq_cnt`| Counter of Receive Error Sequence Packet from TEST GEN| `WC`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Mon_Error_FCS_Packet_Counter

* **Description**           

Counter of received packet has wrong FCS value


* **RTL Instant Name**    : `upen_mon_errfcs`

* **Address**             : `0x0E500 - 0x0E520`

* **Formula**             : `0x0E500 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_test_mon_errfcs_cnt`| Counter of Receive Error FCS Packet from TEST GEN| `WC`| `0x0`| `0x0 End: Begin:`|

###DDC_Test_Mon_Abort_VCG_Packet_Counter

* **Description**           

Counter of received packet has been abbort due to wrong length information


* **RTL Instant Name**    : `upen_mon_abrpkt`

* **Address**             : `0x0E600 - 0x0E620`

* **Formula**             : `0x0E600 + $channelid`

* **Where**               : 

    * `$channelid(0-31): Channel ID`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`dcc_test_mon_abrpkt_cnt`| Counter of Abbort Packet from TEST GEN| `WC`| `0x0`| `0x0 End:`|

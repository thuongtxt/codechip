## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CCI0011_RD_GLB
####Register Table

|Name|Address|
|-----|-----|
|`Device Product ID`|`0x00_0000`|
|`Device Year Month Day Version ID`|`0x00_0002`|
|`Device Internal ID`|`0x00_0003`|
|`GLB Sub-Core Active`|`0x00_0001`|
|`GLB External 8Khz Clock Selection`|`0x00_0011`|
|`GLB Debug PW Control`|`0x00_0010`|
|`GLB Debug PW Control`|`0x00_0024`|
|`GLB Debug PW PLA HO Speed`|`0x00_0030`|
|`GLB Debug PW PDA HO Speed`|`0x00_0031`|
|`GLB Debug PW PLA LO Speed`|`0x00_0032`|
|`GLB Debug PW PDA LO Speed`|`0x00_0033`|
|`GLB Debug PW PWE Speed`|`0x00_0034`|
|`GLB Debug PW CLA Speed`|`0x00_0035`|
|`Chip Temperature Sticky`|`0x00_0020`|


###Device Product ID

* **Description**           

This register indicates Product ID.


* **RTL Instant Name**    : `ProductID`

* **Address**             : `0x00_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`productid`| ProductId| `RO`| `0x60210051`| `0x60210051 End: Begin:`|

###Device Year Month Day Version ID

* **Description**           

This register indicates Year Month Day and main version ID.


* **RTL Instant Name**    : `YYMMDD_VerID`

* **Address**             : `0x00_0002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`year`| Year| `RO`| `0x16`| `0x16`|
|`[23:16]`|`month`| Month| `RO`| `0x01`| `0x01`|
|`[15:8]`|`day`| Day| `RO`| `0x30`| `0x30`|
|`[7:0]`|`version`| Version| `RO`| `0x40`| `0x40 End: Begin:`|

###Device Internal ID

* **Description**           

This register indicates internal ID.


* **RTL Instant Name**    : `InternalID`

* **Address**             : `0x00_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`internalid`| InternalID| `RO`| `0x0`| `0x0 End: Begin:`|

###GLB Sub-Core Active

* **Description**           

This register indicates the active ports.


* **RTL Instant Name**    : `Active`

* **Address**             : `0x00_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`subcoreen`| Enable per sub-core<br>{1}: Enable <br>{0}: Disable| `RW`| `0x00000000`| `0x00000000 End: Begin:`|

###GLB External 8Khz Clock Selection

* **Description**           

This register configures 8Khz clock selection


* **RTL Instant Name**    : `Timeout`

* **Address**             : `0x00_0011`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0xX`| `0xX`|
|`[7]`|`secondref8khzdis`| SecondRef8Khz Disable| `RW`| `0x0`| `0x0`|
|`[6:4]`|`secondref8khzocnid`| SecondRef8Khz OCN port ID| `RW`| `0x0`| `0x0`|
|`[3]`|`firstref8khzdis`| FirstRef8Khz Disable| `RW`| `0x0`| `0x0`|
|`[2:0]`|`firstref8khzocnid`| FirstRef8Khz OCN port ID| `RW`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW Control

* **Description**           

This register is used to configure debug PW parameters


* **RTL Instant Name**    : `DebugPwControl`

* **Address**             : `0x00_0010`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `30`
* **Register Type**       : `Config|Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[29:20]`|`lotdmpwid`| Lo TDM PW ID or HO Master STS ID corresponding to global PW ID| `RW`| `0x0`| `0x0`|
|`[19:7]`|`globalpwid`| Global PW ID need to debug| `RW`| `0x0`| `0x0`|
|`[6:4]`|`oc48id`| OC48 ID| `RW`| `0x0`| `0x0`|
|`[3]`|`looc24slice`| Low Order OC24 slice if the PW belong to low order path| `RW`| `0x0`| `0x0`|
|`[2:0]`|`hopwtype`| High Order PW type if the PW belong to high order path 0: STS1/VC3 1: STS3/VC4 2: STS12/VC4-4C Others: STS48/VC4-16C| `RW`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW Control

* **Description**           

This register is used to debug PW modules


* **RTL Instant Name**    : `DebugPwSticky`

* **Address**             : `0x00_0024`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `29`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28]`|`claprbserr`| CLA output PRBS error| `WC`| `0x0`| `0x0`|
|`[27:20]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[19]`|`pweprbserr`| PWE input PRBS error| `RC`| `0x0`| `0x0`|
|`[18]`|`plaloprbserr`| Low Order PLA input PRBS error| `RC`| `0x0`| `0x0`|
|`[17]`|`pdahoprbserr`| High Order PDA output PRBS error| `RC`| `0x0`| `0x0`|
|`[16]`|`plahoprbserr`| High Order PLA input PRBS error| `RC`| `0x0`| `0x0`|
|`[15:13]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[12]`|`claprbssyn`| CLA output PRBS sync| `WC`| `0x0`| `0x0`|
|`[11:4]`|`unused`| *n/a*| `RC`| `0x0`| `0x0`|
|`[3]`|`pweprbssyn`| PWE input PRBS sync| `RC`| `0x0`| `0x0`|
|`[2]`|`plaloprbssyn`| Low Order PLA input PRBS sync| `RC`| `0x0`| `0x0`|
|`[1]`|`pdahoprbssyn`| High Order PDA output PRBS sync| `RC`| `0x0`| `0x0`|
|`[0]`|`plahoprbssyn`| High Order PLA input PRBS sync| `RC`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW PLA HO Speed

* **Description**           

This register is used to show PLA HO PW speed


* **RTL Instant Name**    : `DebugPwPlaHoSpeed`

* **Address**             : `0x00_0030`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`plahopwspeed`| High order PLA input PW speed| `RO`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW PDA HO Speed

* **Description**           

This register is used to show PDA HO PW speed


* **RTL Instant Name**    : `DebugPwPdaHoSpeed`

* **Address**             : `0x00_0031`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pdahopwspeed`| High order PDA output PW speed| `RO`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW PLA LO Speed

* **Description**           

This register is used to show PLA LO PW speed


* **RTL Instant Name**    : `DebugPwPlaLoSpeed`

* **Address**             : `0x00_0032`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`plalopwspeed`| Low order PLA input PW speed| `RO`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW PDA LO Speed

* **Description**           

This register is used to show PDA LO PW speed


* **RTL Instant Name**    : `DebugPwPdaLoSpeed`

* **Address**             : `0x00_0033`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pdalopwspeed`| Low order PDA output PW speed| `RO`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW PWE Speed

* **Description**           

This register is used to show PWE PW speed


* **RTL Instant Name**    : `DebugPwPweSpeed`

* **Address**             : `0x00_0034`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pwepwspeed`| PWE input PW speed| `RO`| `0x0`| `0x0 End: Begin:`|

###GLB Debug PW CLA Speed

* **Description**           

This register is used to show PWE PW speed


* **RTL Instant Name**    : `DebugPwClaLoSpeed`

* **Address**             : `0x00_0035`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`clapwspeed`| CLA output PW speed| `RO`| `0x0`| `0x0 End: Begin:`|

###Chip Temperature Sticky

* **Description**           

This register is used to sticky temperature


* **RTL Instant Name**    : `ChipTempSticky`

* **Address**             : `0x00_0020`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `3`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[2]`|`chipslr2tempstk`| Chip SLR2 temperature| `WC`| `0x0`| `0x0`|
|`[1]`|`chipslr1tempstk`| Chip SLR1 temperature| `WC`| `0x0`| `0x0`|
|`[0]`|`chipslr0tempstk`| Chip SLR0 temperature| `WC`| `0x0`| `0x0 End:`|

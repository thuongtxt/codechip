## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2017-10-18|AF6Project|Initial version|




##AF6CNC0021_RD_PRBS
####Register Table

|Name|Address|
|-----|-----|
|`PRBS_LOMAP Generator Control`|`0x50_0000 - 0x50_5fff`|
|`PRBS_LOMAP Monitor`|`0x50_0800 - 0x50_ffff`|
|`PRBS_HOMAP Generator Control`|`0x06_8000 - 0x06_F03F`|
|`PRBS_HOMAP Monitor`|`0x06_8040 - 0x06_F07F`|


###PRBS_LOMAP Generator Control

* **Description**           

Each register is used to configure PRBS generator at MAP.


* **RTL Instant Name**    : `prbslogenctl`

* **Address**             : `0x50_0000 - 0x50_5fff`

* **Formula**             : `0x50_0000 + 65536*2*Slice48Id + 1024*Slice24Id + PwId`

* **Where**               : 

    * `$Slice48Id(0-3):`

    * `$Slice24Id(0-1):`

    * `$PwId(0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`prbslogenctlseqmod`| Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode| `RW`| `0x0`| `0x0`|
|`[0]`|`prbslogenctlenb`| Enable PRBS generator. 1: Enable 0: Disable| `RW`| `0x0`| `0x0 End : Begin:`|

###PRBS_LOMAP Monitor

* **Description**           

Each register is used for PRBS monitor at DEMAP.


* **RTL Instant Name**    : `prbslomon`

* **Address**             : `0x50_0800 - 0x50_ffff`

* **Formula**             : `0x50_0800 + 65536*2*Slice48Id + 1024*Slice24Id + PwId`

* **Where**               : 

    * `$Slice48Id(0-3):`

    * `$Slice24Id(0-1):`

    * `$PwId(0-1023)`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17]`|`prbslomonerr`| PRBS error. 1: Error 0: Not error| `RC`| `0x0`| `0x0`|
|`[16]`|`prbslomonsync`| PRBS monitor sync. Only used for PRBS mode 1: Data PRBS sync (Not error) 0: Not sync| `RO`| `0x0`| `0x0`|
|`[15:1]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`prbslomonctlseqmod`| Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode| `RW`| `0x0`| `0x0 End : Begin:`|

###PRBS_HOMAP Generator Control

* **Description**           

Each register is used to configure PRBS generator at MAP.


* **RTL Instant Name**    : `prbshogenctl`

* **Address**             : `0x06_8000 - 0x06_F03F`

* **Formula**             : `0x06_8000 + 2048*Slice48Id + stsId`

* **Where**               : 

    * `$Slice48Id(0-7):`

    * `$stsId(0-47) :`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:2]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[1]`|`prbshogenctlseqmod`| Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode| `RW`| `0x0`| `0x0`|
|`[0]`|`prbshogenctlenb`| Enable PRBS generator. 1: Enable 0: Disable| `RW`| `0x0`| `0x0 End : Begin:`|

###PRBS_HOMAP Monitor

* **Description**           

Each register is used for PRBS monitor at DEMAP.


* **RTL Instant Name**    : `prbshomon`

* **Address**             : `0x06_8040 - 0x06_F07F`

* **Formula**             : `0x06_8040 + 2048*Slice48Id + stsId`

* **Where**               : 

    * `$Slice48Id(0-7):`

    * `$stsId(0-47) :`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[17]`|`prbshomonerr`| PRBS error. 1: Error 0: Not error| `RC`| `0x0`| `0x0`|
|`[16]`|`prbshomonsync`| PRBS monitor sync. Only used for PRBS mode 1: Data PRBS sync (Not error) 0: Not sync| `RO`| `0x0`| `0x0`|
|`[15:1]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`prbshomonctlseqmod`| Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode| `RW`| `0x0`| `0x0 End :`|

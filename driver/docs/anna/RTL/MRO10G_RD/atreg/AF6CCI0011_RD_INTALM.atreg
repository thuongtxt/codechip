######################################################################################
# Arrive Technologies
# Register Description Document
# Revision 1.0 - 2015.Feb.03

######################################################################################
# This section is for guideline
# Refer to \\Hcmcserv3\Projects\internal_tools\atvn_vlibs\bin\rgm_gen\docs\REGISTER_DESCRIPTION_GUIDELINE.pdf
# File name: filename.atreg (atreg = ATVN register define)
# Comment out a line: Please use (# or //#) character at the beginning of the line
# Delimiter character: (%%)

######################################################################################
#| Access Policy| Use Case| Description                | Effect of a Write on Current Field Value  | Effect of a Read on Current Field Value | Read-back Value |
#| ------|-----------|---------------------------------| ------------------------------------------|-----------------------------------------|-----------------|
#| RO    | Status    | Read Only                       | No effect                | No effect            | Current Value |
#| RW    | Config    | Read, Write                     | Changed to written value | No effect            | Current value |
#| RC    | Status    | Read Clears All                 | No effect                | Sets all bits to 0   | Current value |
#| RS    | UNKNOW    | Read Sets All                   | No effect                | Sets all bits to 1   | Current value |
#| WRC   | UNKNOW    | Write, Read Clears All          | Changed to written value | Sets all bits to 0   | Current value |
#| WRS   | UNKNOW    | Write, Read Sets All            | Changed to written value | Sets all bits to 1   | Current value |
#| WC    | Status    | Write Clears All                | Sets all bits to 0       | No effect            | Current value |
#| WS    | Status    | Write Sets All                  | Sets all bits to 1       | No effect            | Current value |
#| WSRC  | UNKNOW    | Write Sets All, Read Clears All | Sets all bits to 1       | Sets all bits to 0   | Current value |
#| WCRS  | UNKNOW    | Write Clears All,Read Sets All  | Sets all bits to 0       | Sets all bits to 1   | Current value |
#| W1C   | Interrupt | Write 1 to Clear                | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current Value |
#| W1S   | Interrupt | Write 1 to Set                  | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W1T   | UNKNOW    | Write 1 to Toggle               | If the bit in the wr value is a 1, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W0C   | Interrupt | Write 0 to Clear                | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current value |
#| W0S   | Interrupt | Write 0 to Set                  | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W0T   | UNKNOW    | Write 0 to Toggle               | If the bit in the wr value is a 0, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W1SRC | UNKNOW    | Write 1 to Set, Read Clears All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W1CRS | UNKNOW    | Write 1 to Clear, Read Sets All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| W0SRC | UNKNOW    | Write 0 to Set, Read Clears All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W0CRS | UNKNOW    | Write 0 to Clear, Read Sets All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| WO    | Config    | Write Only                      | Changed to written value | No effect | Undefined |
#| WOC   | UNKNOW    | Write Only Clears All           | Sets all bits to 0     | No effect | Undefined |
#| WOS   | UNKNOW    | Write Only Sets All             | Sets all bits to 1     | No effect | Undefined |
#| W1    | UNKNOW    | Write Once                      | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Current value |
#| WO1   | UNKNOW    | Write Only, Once                | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Undefined |
#=============================================================================
# Sample
#=============================================================================

######################################################################################
#SAMPLE> // Begin:
#SAMPLE> //# Some description
#SAMPLE> // Register Full Name: Device Version ID
#SAMPLE> // RTL Instant Name: VersionID
#SAMPLE> //# {FunctionName,SubFunction_InstantName_Description}
#SAMPLE> //# RTL Instant Name and Full Name MUST be unique within a register description file
#SAMPLE> // Address: 0x00_0000
#SAMPLE> //# Address is relative address, will be sum with Base_Address for each function block
#SAMPLE> // Formula      : Address + $portid 
#SAMPLE> // Where        : {$portid(0-7): port identification} 
#SAMPLE> // Description  : This register indicates the module' name and version. 
#SAMPLE> // Width: 32
#SAMPLE> // Register Type: {Status}
#SAMPLE> //# Field: [Bit:Bit] %%  Name     %% Description            %% Type %% SW_Reset %% HW_Reset
#SAMPLE> // Field: [31:24]    %%  Year     %% 2013                   %% RO   %% 0x0D     %% 0x0D
#SAMPLE> // Field: [23:16]    %%  Week     %% Week Synthesized FPGA  %% RO   %% 0x0C     %% 0x0C
#SAMPLE> // Field: [15:8]     %%  Day      %% Day Synthesized FPGA   %% RO   %% 0x16     %% 0x16
#SAMPLE> // Field: [7:4]      %%  VerID    %% FPGA Version           %% RO   %% 0x0      %% 0x0
#SAMPLE> // Field: [3:0]      %%  NumID    %% Number of FPGA Version %% RO   %% 0x1      %% 0x1
#SAMPLE> // End:
######################################################################################
#Interrupt
######################################################################################

#********************
#4.3.1.  Counter Per Alarm Interrupt Enable Control 
#********************
// Begin: 
// Register Full Name: Counter Per Alarm Interrupt Enable Control 
// RTL Instant Name  : Counter Per Alarm Interrupt Enable Control 
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x060000
// Formula: Address + Grp2048ID*8192 + GrpID*32 + BitID 
// Where: {$Grp2048ID(0-2):  Pseudowire ID bits[12:11]} %% {$GrpID(0-63):  Pseudowire ID bits[10:5]} %% {$BitID(0-31): Pseudowire ID bits [4:0]}
// Description: This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen. 
// Width:32
// Register Type: {Config}
// Field: [8] %%	StrayStateChgIntrEn %%  Set 1 to enable Stray packet event to generate an interrupt %% RW %% 0x0 %% 0x0
// Field: [7] %%	MalformStateChgIntrEn %%  Set 1 to enable Malform packet event to generate an interrupt %% RW %% 0x0 %% 0x0
// Field: [6] %%	MbitStateChgIntrEn %%  Set 1 to enable Mbit packet event to generate an interrupt %% RW %% 0x0 %% 0x0
// Field: [5] %%	RbitStateChgIntrEn %%  Set 1 to enable Rbit packet event to generate an interrupt %% RW %% 0x0 %% 0x0
// Field: [4] %%	LofsSyncStateChgIntrEn %%  Set 1 to enable Lost of frame Sync event to generate an interrupt %% RW %% 0x0 %% 0x0
// Field: [3] %%	UnderrunStateChgIntrEn %%  Set 1 to enable change jitter buffer state event from normal to underrun and vice versa in the related pseudowire to generate an interrupt. %% RW %% 0x0 %% 0x0
// Field: [2] %%	OverrunStateChgIntrEn %%  Set 1 to enable change jitter buffer state event from normal to overrun and vice versa in the related pseudowire to generate an interrupt. %% RW %% 0x0 %% 0x0
// Field: [1] %%	LofsStateChgIntrEn %%  Set 1 to enable change lost of frame state(LOFS) event from normal to LOFS and vice versa in the related pseudowire to generate an interrupt. %% RW %% 0x0 %% 0x0
// Field: [0] %%	LbitStateChgIntrEn %%  Set 1 to enable Lbit packet event to generate an interrupt %% RW %% 0x0 %% 0x0
// End: 
#********************
#4.1.34.  Counter Per Alarm Interrupt Status 
#********************
// Begin: 
// Register Full Name: Counter Per Alarm Interrupt Status 
// RTL Instant Name  : Counter Per Alarm Interrupt Status 
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x060800
// Formula: Address + Grp2048ID*8192 +  GrpID*32 + BitID 
// Where: {$Grp2048ID(0-2):  Pseudowire ID bits[12:11]} %% {$GrpID(0-63):  Pseudowire ID bits[10:5]} %% {$BitID(0-31): Pseudowire ID bits [4:0]}
// Description: This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen. 
// Width:32
// Register Type: {Config}
// Field: [8] %%	StrayStateChgIntrSta %%  Set 1 when Stray packet event is detected in the pseudowire %% RW %% 0x0 %% 0x0
// Field: [7] %%	MalformStateChgIntrSta %%  Set 1 when Malform packet event is detected in the pseudowire %% RW %% 0x0 %% 0x0
// Field: [6] %%	MbitStateChgIntrSta %%  Set 1 when a Mbit packet event is detected in the pseudowire %% RW %% 0x0 %% 0x0
// Field: [5] %%	RbitStateChgIntrSta %%  Set 1 when a Rbit packet event is detected in the pseudowire %% RW %% 0x0 %% 0x0
// Field: [4] %%	LofsSyncStateChgIntrSta %%  Set 1 when a lost of frame Sync event is detected in the pseudowire %% RW %% 0x0 %% 0x0
// Field: [3] %%	UnderrunStateChgIntrSta %%  Set 1 when there is a change in jitter buffer underrun state  in the related pseudowire %% RW %% 0x0 %% 0x0
// Field: [2] %%	OverrunStateChgIntrSta %%  Set 1 when there is a change in jitter buffer overrun state in the related pseudowire %% RW %% 0x0 %% 0x0
// Field: [1] %%	LofsStateChgIntrSta %%  Set 1 when there is a change in lost of frame state(LOFS) in the related pseudowire %% RW %% 0x0 %% 0x0
// Field: [0] %%	LbitStateChgIntrSta %%  Set 1 when a Lbit packet event is detected in the pseudowire %% RW %% 0x0 %% 0x0
// End: 
#********************
#4.1.35.  Counter Per Alarm Current Status 
#********************
// Begin: 
// Register Full Name: Counter Per Alarm Current Status 
// RTL Instant Name  : Counter Per Alarm Current Status 
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x061000
// Formula: Address + Grp2048ID*8192 +  GrpID*32 + BitID 
// Where: {$Grp2048ID(0-2):  Pseudowire ID bits[12:11]} %% {$GrpID(0-63):  Pseudowire ID bits[10:5]} %% {$BitID(0-31): Pseudowire ID bits [4:0]}
// Description: This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen. 
// Width:32
// Register Type: {Config}
// Field: [8] %%	StrayCurStatus %%  Stray packet state current status in the related pseudowire. %% RW %% 0x0 %% 0x0
// Field: [7] %%	MalformCurStatus %%  Malform packet state current status in the related pseudowire. %% RW %% 0x0 %% 0x0
// Field: [6] %%	MbitCurStatus %%  Mbit packet state current status in the related pseudowire. %% RW %% 0x0 %% 0x0
// Field: [5] %%	RbitCurStatus %%  Rbit packet state current status in the related pseudowire. %% RW %% 0x0 %% 0x0
// Field: [4] %%	LofsSyncCurStatus %%  Lost of frame Sync state current status in the related pseudowire. %% RW %% 0x0 %% 0x0
// Field: [3] %%	UnderrunCurStatus %%  Jitter buffer underrun current status in the related pseudowire. %% RW %% 0x0 %% 0x0
// Field: [2] %%	OverrunCurStatus %%  Jitter buffer overrun current status in the related pseudowire. %% RW %% 0x0 %% 0x0
// Field: [1] %%	LofsStateCurStatus %%  Lost of frame state current status in the related pseudowire. %% RW %% 0x0 %% 0x0
// Field: [0] %%	LbitStateCurStatus %%  a Lbit packet state current status in the related pseudowire %% RW %% 0x0 %% 0x0
// End: 

#********************
#4.1.36. Counter Interrupt OR Status
#********************
// Begin: 
// Register Full Name: Counter Interrupt OR Status
// RTL Instant Name  : Counter Interrupt OR Status
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x061800
// Formula: Address + Grp2048ID*8192 +  Slice*1024 + GrpID
// Where: {$Grp2048ID(0-2):  Pseudowire ID bits[12:11]} %% {$Slice(0-1):  Pseudowire ID bits[10]} %% {$GrpID(0-31):  Pseudowire ID bits[9:5]}
// Description: This is per Alarm interrupt enable of pseudowires. Each register is used to store 4 bits to enable interrupts when the related alarms in pseudowires happen. 
// Width:32
// Register Type: {Config}
// Field: [31:0] %%	IntrORStatus %%  Set to 1 to indicate that there is any interrupt status bit in the Counter per Alarm Interrupt Status register of the related pseudowires to be set and they are enabled to raise interrupt. Bit 0 of GrpID#0 for pseudowire 0, bit 31 of GrpID#0 for pseudowire 31, bit 0 of GrpID#1 for pseudowire 32, respectively. %% RW %% 0x0 %% 0x0
// End: 

#********************* 
#4.1.36. Counter per Group Interrupt OR Status
#********************* 
// Begin: 
// Register Full Name: Counter per Group Interrupt OR Status
// RTL Instant Name  : counter_per_group_intr_or_stat
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x061BFF
// Formula: Address + Grp2048ID*8192 +  Slice*1024
//# The address format for these registers is 0x041BFF +  Slice*1024
// Where: {$Grp2048ID(0-2):  Pseudowire ID bits[12:11]} %% {$Slice(0-1):  Pseudowire ID bits[10]}
// Description: The register consists of 8 bits for 8 Group of the PW Counter. Each bit is used to store Interrupt OR status of the related Group. 
// Width: 32
// Register Type: {Interrupt} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0] %%  GroupIntrOrSta %%  Set to 1 if any interrupt bit of corresponding Group is set and its interrupt is enabled %% RW %% 0x0 %% 0x0
// End:

#********************* 
#4.1.36. Counter per Group Interrupt Enable Control 
#********************* 
// Begin: 
// Register Full Name: Counter per Group Interrupt Enable Control 
// RTL Instant Name  : counter_per_group_intr_en_ctrl 
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x061BFE
// Formula: Address + Grp2048ID*8192 +  Slice*1024
//# The address format for these registers is 0x041BFF +  Slice*1024
// Where: {$Grp2048ID(0-2):  Pseudowire ID bits[12:11]} %% {$Slice(0-1):  Pseudowire ID bits[10]}
// Description: The register consists of 8 interrupt enable bits for 8 group in the PW counter. 
// Width: 32
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0] %%  GroupIntrEn %% Set to 1 to enable the related Group to generate interrupt. %% RW %% 0x0 %% 0x0
// End:
#********************* 
#4.1.36. Counter per Slice Interrupt OR Status
#********************* 
// Begin: 
// Register Full Name: Counter per Slice Interrupt OR Status
// RTL Instant Name  : counter_per_slice_intr_or_stat
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x068000
// Description: The register consists of 6 bits for 6 Slice of the PW Counter. Each bit is used to store Interrupt OR status of the related 1024 PWs Group. 
// Width: 6
// Register Type: {Interrupt} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [5:0] %%  GroupIntrOrSta %%  Set to 1 if any interrupt bit of corresponding Slice is set and its interrupt is enabled %% RW %% 0x0 %% 0x0
// End:


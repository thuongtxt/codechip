#********************* 
#1.1.1. OCN Global Rx Framer Control
#********************* 
// Begin:
// Register Full Name: OCN Global Rx Framer Control
// RTL Instant Name	: glbrfm_reg
// Address	: 0xf43000
// Formula  : 
// Where    : 
// Description: This is the global configuration register for the Rx Framer
// Width: 128
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [127:24] 	%% unused 			%% unused %% RW  %% 0x0 %% 0x0  
// Field: [23:16] 	%% RxLineSyncSel	%% Select line id for synchronization 8 rx lines. Bit[0] for line 0. Note that must only 1 bit is set %% RW  %% 0x1 %% 0x1  
// Field: [15] 	%% unused 				%% unused %% RW  %% 0x0 %% 0x0  
// Field: [14]  %% RxFrmLosAisEn		%% Enable/disable forwarding P_AIS when LOS detected at Rx Framer.
//											1: Enable
//											0: Disable  %% RW %% 0x1 %% 0x1
// Field: [13]  %% RxFrmOofAisEn 		%% Enable/disable forwarding P_AIS when OOF detected at Rx Framer.
//											1: Enable
//											0: Disable  %% RW %% 0x1 %% 0x1
// Field: [12]  %% RxFrmB1BlockCntMod	%% B1 Counter Mode.
//											1: Block mode
//											0: Bit-wise mode %% RW %% 0x1 %% 0x1
// Field: [11] %% unused 				%% unused %% RW  %% 0x0 %% 0x0  
// Field: [10:8] %% RxFrmBadFrmThresh 	%% Threshold for A1A2 missing counter, that is used to change state from FRAMED to HUNT.    %% RW  %% 0x3 %% 0x3
// Field: [7] 	%% unused 				%% unused %% RW  %% 0x0 %% 0x0  
// Field: [6:4] %% RxFrmB1GoodThresh  	%% Threshold for B1 good counter, that is used to change state from CHECK to FRAMED.  %% RW  %% 0x4 %% 0x4
// Field: [3] 	%% unused 				%% unused %% RW  %% 0x0 %% 0x0
// Field: [2] 	%% RxFrmDescrEn  		%% Enable/disable de-scrambling of the Rx coming data stream.
//											1: Enable
//											0: Disable  %% RW %% 0x1 %% 0x1
// Field: [1] 	%% RxFrmB1ChkFrmEn		%% Enable/disable B1 check option is added to the required framing algorithm.
//											1: Enable
//											0: Disable  %% RW %% 0x1 %% 0x1
// Field: [0] 	%% RxFrmEssiModeEn   	%% TFI-5 mode.
//											1: Enable
//											0: Disable  %% RW %% 0x1 %% 0x1
// End :

#********************* 
#1.1.2. OCN Global Tx Framer Control
#********************* 
// Begin:
// Register Full Name: OCN Global Tx Framer Control
// RTL Instant Name  : glbtfm_reg
// Address: 0xf43001
// Formula      : 
// Where        : 
// Description: This is the global configuration register for the Tx Framer
// Width: 128
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [127:12] 	%% unused 			%% unused %% RW  %% 0x0 %% 0x0  
// Field: [31:24] 	%% TxLineOofFrc		%% Enable/disble force OOF for 8 tx lines. Bit[0] for line 0. %% RW  %% 0x0 %% 0x0  
// Field: [23:16] 	%% TxLineB1ErrFrc	%% Enable/disble force B1 error for 8 tx lines. Bit[0] for line 0. %% RW  %% 0x0 %% 0x0  
// Field: [15:12] 	%% unused 			%% unused %% RW  %% 0x0 %% 0x0 
// Field: [11:4] 	%% TxLineSyncSel	%% Select line id for synchronization 8 tx lines. Bit[0] for line 0. Note that must only 1 bit is set %% RW  %% 0x1 %% 0x1  
// Field: [3:1] 	%% unused 			%% unused %% RW  %% 0x0 %% 0x0 
// Field: [0] 		%% TxFrmScrEn     	%% Enable/disable scrambling of the Tx data stream.
//											1: Enable
//											0: Disable  %% RW %% 0x1 %% 0x1
// End :

#********************* 
#1.1.3. OCN Rx Framer Status
#********************* 
// Begin:
// Register Full Name: OCN Rx Framer Status
// RTL Instant Name  : rxfrmsta
// Address: 0xf43100 - 0xf43170
// Formula      : Address + 16*LineId
// Where        : {$LineId(0-7)}
// Description	: Rx Framer status
// Width		: 128
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [127:1] %% unused 			%% unused %% RO  %% 0x0 %% 0x0
// Field: [0] 	%% StaOof 				%% Out of Frame that is detected at RxFramer	%% RO  %% 0x0 %% 0x0
// End :

#********************* 
#1.1.4. OCN Rx Framer Sticky
#********************* 
// Begin:
// Register Full Name: OCN Rx Framer Sticky
// RTL Instant Name  : rxfrmstk
// Address: 0xf43101 - 0xf43171
// Formula      : Address + 16*LineId
// Where        : {$LineId(0-7)}
// Description	: Rx Framer sticky
// Width		: 128
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [127:1] %% unused 			%% unused %% W1C  %% 0x0 %% 0x0
// Field: [0] %% StkOof 				%% Out of Frame sticky change				%% W1C  %% 0x0 %% 0x0
// End :

#********************* 
#1.1.5. OCN Rx Framer B1 error counter read only
#********************* 
// Begin:
// Register Full Name: OCN Rx Framer B1 error counter read only
// RTL Instant Name  : rxfrmb1cntro
// Address: 0xf43102 - 0xf43172
// Formula      : Address + 16*LineId
// Where        : {$LineId(0-7)}
// Description	: Rx Framer B1 error counter read only
// Width		: 128
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [127:32] %% unused 			%% unused %% RO  %% 0x0 %% 0x0
// Field: [31:0] %% RxFrmB1ErrRo		%% Number of B1 error - read only	%% RO  %% 0x0 %% 0x0
// End :


#********************* 
#1.1.6. OCN Rx Framer B1 error counter read to clear
#********************* 
// Begin:
// Register Full Name: OCN Rx Framer B1 error counter read to clear
// RTL Instant Name  : rxfrmb1cntr2c
// Address: 0xf43103 - 0xf43173
// Formula      : Address + 16*LineId
// Where        : {$LineId(0-7)}
// Description	: Rx Framer B1 error counter read to clear
// Width		: 128
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description            %% Type %% SW_Reset %% HW_Reset
// Field: [127:32] %% unused 			%% unused %% RC  %% 0x0 %% 0x0
// Field: [31:0] %% RxFrmB1ErrR2C		%% Number of B1 error - read to clear	%% RC  %% 0x0 %% 0x0
// End :


	
	

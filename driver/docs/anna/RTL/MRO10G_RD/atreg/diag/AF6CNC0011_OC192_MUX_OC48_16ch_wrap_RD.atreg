##################################################################
# Arrive Technologies
#
# Revision 1.0 - 2013.Mar.23

##################################################################
#This section is for guideline 
#File name: filename.atreg (atreg = ATVN register define)
#Comment out a line: Please use (# or //#) character at the beginning of the line
#Delimiter character: (%%)

##################################################################
#Common rules for register:
#Access types:
#R/W : Read / Write
#R/W/C: Read / Write 1 to clear (also called as sticky bit)
#R_O: Read Only, No clear on read
#R2C: Read Only, Read to Clear
#W_O: write only
#W1C: Read / Write 1 to clear (also called as sticky bit)

##################################################################
// Register Full Name: OC192 MUX OC48 Change Mode
// RTL Instant Name  : OC192_MUX_OC48_Change_Mode
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0080-0x6080
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : Address+($G*0x2000)
// Where        : {$G(0-3) : Group of Ports}
// Description  : Configurate Port mode, there is 4 group (0-3), each group has 4 sub ports
// Width        : 32
// Register Type: {Config}
//# Field : [Bit:Bit] %% Name        %% Description                                 %% Type %% Reset %% Default
//  Field : [31:13]   %% Unused      %%                                             %%      %%       %%    
//  Fieldx: [12:12]   %% chg_done    %% Change mode has done
//                                      {1} : Done                                  %% W1C  %% 0x0   %% 0x0
//  Field : [11:09]   %% Unused      %%                                             %%      %%       %%    
//  Fieldx: [08:08]   %% chg_trig    %% Trigger 0->1 to start changing mode         %% R/W  %% 0x0   %% 0x0
//  Fieldx: [07:04]   %% chg_mode    %% Port mode     
//                                      {0} : OC192
//                                      {1} : OC48 
//                                      {2} : OC12 
//                                      {4} : 10Ge 
//                                      {5} : 1Ge  
//                                      {6} : 100Fx                                 %% R/W  %% 0x0   %% 0x0
//  Fieldx: [03:00]   %% chg_port    %% Sub Port ID 
//                                      {0-3} : Group0 => Port 0-3
//                                      {0-3} : Group1 => Port 4-7
//                                      {0-3} : Group2 => Port 8-11
//                                      {0-3} : Group3 => Port 12-15                %% R/W  %% 0x0   %% 0x0

##################################################################
// Register Full Name: OC192 MUX OC48 Current Mode
// RTL Instant Name  : OC192_MUX_OC48_Current_Mode
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0090-0x6090
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : Address+($G*0x2000)
// Where        : {$G(0-3) : Group of Ports}
// Description  : Current Port mode, there is 4 group (0-3), each group has 4 sub ports
// Width        : 32
// Register Type: {Status}
//# Field : [Bit:Bit] %% Name               %% Description                                 %% Type %% Reset %% Default
//  Field : [31:16]   %% Unused             %%                                             %%      %%       %%    
//  Fieldx: [15:12]   %% cur_mode_subport3  %% Current mode subport3, Group 0 => Port3, Group 1 => Port 7, Group 2 => Port 11, Group 3 => Port 15
//                                          {0} : OC192
//                                          {1} : OC48 
//                                          {2} : OC12 
//                                          {4} : 10Ge 
//                                          {5} : 1Ge  
//                                          {6} : 100Fx                                     %% R_O %% 0x0   %% 0x0
//  Fieldx: [11:08]   %% cur_mode_subport2  %% Current mode subport2, Group 0 => Port2, Group 1 => Port 6, Group 2 => Port 10, Group 3 => Port 14
//                                          {0} : OC192
//                                          {1} : OC48 
//                                          {2} : OC12 
//                                          {4} : 10Ge 
//                                          {5} : 1Ge  
//                                          {6} : 100Fx                                     %% R_O %% 0x0   %% 0x0
//  Fieldx: [07:04]   %% cur_mode_subport1  %% Current mode subport1, Group 0 => Port1, Group 1 => Port 5, Group 2 => Port 9, Group 3 => Port 13
//                                          {0} : OC192
//                                          {1} : OC48 
//                                          {2} : OC12 
//                                          {4} : 10Ge 
//                                          {5} : 1Ge  
//                                          {6} : 100Fx                                     %% R_O %% 0x0   %% 0x0
//  Fieldx: [03:00]   %% cur_mode_subport0  %% Current mode subport0, Group 0 => Port0, Group 1 => Port 4, Group 2 => Port 8, Group 3 => Port 12
//                                          {0} : OC192
//                                          {1} : OC48 
//                                          {2} : OC12 
//                                          {4} : 10Ge 
//                                          {5} : 1Ge  
//                                          {6} : 100Fx                                     %% R_O %% 0x0   %% 0x0

##################################################################
// Register Full Name: OC192 MUX OC48 DRP
// RTL Instant Name  : OC192_MUX_OC48_DRP
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x1000-0x7FFF
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : Address+($G*0x2000)+($P*0x400)+$DRP
// Where        : {$G(0-3) : Group of Ports} %% {$P(0-3) : Sub Port ID} %% {$DRP(0-1023) : DRP address, see UG578}
// Description  : Read/Write DRP address of SERDES, there is 4 group (0-3), each group has 4 sub ports
// Width        : 32
// Register Type: {Config}
//# Field : [Bit:Bit] %% Name        %% Description                                 %% Type %% Reset %% Default
//  Field : [31:10]   %% Unused      %%                                             %%      %%       %%    
//  Fieldx: [09:00]   %% drp_rw      %% DRP read/write value                        %% R/W  %% 0x0   %% 0x0

##################################################################
// Register Full Name: OC192 MUX OC48 LoopBack
// RTL Instant Name  : OC192_MUX_OC48_LoopBack
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0002-0x6002
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : Address+($G*0x2000)
// Where        : {$G(0-3) : Group of Ports}
// Description  : Configurate LoopBack, there is 4 group (0-3), each group has 4 sub ports
// Width        : 32
// Register Type: {Config}
//# Field : [Bit:Bit] %% Name        %% Description                                 %% Type %% Reset %% Default
//  Field : [31:16]   %% Unused      %%                                             %%      %%       %%    
//  Fieldx: [15:12]   %% lpback_subport3 %% Loopback subport 3, Group 0 => Port3, Group 1 => Port 7, Group 2 => Port 11, Group 3 => Port 15
//                                      {0} : Nomoral 
//                                      {2} : Loop-In
//                                      {4} : Loop-Out                              %% R/W  %% 0x0   %% 0x0
//  Fieldx: [11:08]   %% lpback_subport2 %% Loopback subport 2, Group 0 => Port2, Group 1 => Port 6, Group 2 => Port 10, Group 3 => Port 14
//                                      {0} : Nomoral 
//                                      {2} : Loop-In
//                                      {4} : Loop-Out                              %% R/W  %% 0x0   %% 0x0
//  Fieldx: [07:04]   %% lpback_subport1 %% Loopback subport 1, Group 0 => Port1, Group 1 => Port 5, Group 2 => Port 9, Group 3 => Port 13
//                                      {0} : Nomoral 
//                                      {2} : Loop-In
//                                      {4} : Loop-Out                              %% R/W  %% 0x0   %% 0x0
//  Fieldx: [03:00]   %% lpback_subport0 %% Loopback subport 0, Group 0 => Port0, Group 1 => Port 4, Group 2 => Port 8, Group 3 => Port 12
//                                      {0} : Nomoral 
//                                      {2} : Loop-In
//                                      {4} : Loop-Out                              %% R/W  %% 0x0   %% 0x0

##################################################################
// Register Full Name: OC192 MUX OC48 PLL Status
// RTL Instant Name  : OC192_MUX_OC48_PLL_Status
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x000B-0x600B
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : Address+($G*0x2000)
// Where        : {$G(0-3) : Group of Ports}
// Description  : QPLL/CPLL status, there is 4 group (0-3), each group has 4 sub ports
// Width        : 32
// Register Type: {Status}
//# Field : [Bit:Bit] %% Name        %% Description                                 %% Type %% Reset %% Default
//  Field : [31:30]   %% Unused      %%                                             %%      %%       %%    
//  Fieldx: [29:29]   %% QPLL1_Lock_change%% QPLL1 has transition lock/unlock, Group 0-3
//                                          {1} : QPLL1_Lock has changed            %% W1C  %% 0x0   %% 0x0
//  Fieldx: [28:28]   %% QPLL0_Lock_change%% QPLL0 has transition lock/unlock, Group 0-3
//                                          {1} : QPLL0_Lock has changed            %% W1C  %% 0x0   %% 0x0
//  Fieldx: [27:26]   %% Unused          %%                                         %%      %%       %%    
//  Fieldx: [25:25]   %% QPLL1_Lock      %% QPLL0 is Locked, Group 0-3
//                                          {1} : Locked                            %% R_O  %% 0x0   %% 0x0
//  Fieldx: [24:24]   %% QPLL0_Lock      %% QPLL0 is Locked, Group 0-3
//                                          {1} : Locked                            %% R_O  %% 0x0   %% 0x0
//  Fieldx: [23:16]   %% Unused          %%                                         %%      %%       %%    
//  Fieldx: [15:12]   %% CPLL_Lock_Change%% CPLL has transition lock/unlock, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
//                                          {1} : CPLL_Lock has changed             %% W1C  %% 0x0   %% 0x0
//  Fieldx: [11:04]   %% Unused          %%                                         %%      %%       %%    
//  Fieldx: [03:00]   %% CPLL_Lock       %% CPLL is Locked, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
//                                          {1} : Locked                            %% R_O  %% 0x0   %% 0x0

##################################################################
// Register Full Name: OC192 MUX OC48 TX Reset
// RTL Instant Name  : OC192_MUX_OC48_TX_Reset
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x000C-0x600C
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : Address+($G*0x2000)
// Where        : {$G(0-3) : Group of Ports}
// Description  : Reset TX SERDES, there is 4 group (0-3), each group has 4 sub ports
// Width        : 32
// Register Type: {Status}
//# Field : [Bit:Bit] %% Name        %% Description                                 %% Type %% Reset %% Default
//  Field : [31:20]   %% Unused      %%                                             %%      %%       %%    
//  Fieldx: [19:16]   %% txrst_done      %% TX Reset Done, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
//                                          {1} : reset done                        %% W1C  %% 0x0   %% 0x0
//  Fieldx: [15:04]   %% Unused          %%                                         %%      %%       %%    
//  Fieldx: [03:00]   %% txrst_trig      %% Trige 0->1 to start reset TX SERDES, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
//                                                                                  %% R/W  %% 0x0   %% 0x0

##################################################################
// Register Full Name: OC192 MUX OC48 RX Reset
// RTL Instant Name  : OC192_MUX_OC48_RX_Reset
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x000D-0x600D
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : Address+($G*0x2000)
// Where        : {$G(0-3) : Group of Ports}
// Description  : Reset RX SERDES, there is 4 group (0-3), each group has 4 sub ports
// Width        : 32
// Register Type: {Status}
//# Field : [Bit:Bit] %% Name        %% Description                                 %% Type %% Reset %% Default
//  Field : [31:20]   %% Unused      %%                                             %%      %%       %%    
//  Fieldx: [19:16]   %% rxrst_done      %% RX Reset Done, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
//                                          {1} : reset done                        %% W1C  %% 0x0   %% 0x0
//  Fieldx: [15:04]   %% Unused          %%                                         %%      %%       %%    
//  Fieldx: [03:00]   %% rxrst_trig      %% Trige 0->1 to start reset RX SERDES, bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
//                                                                                  %% R/W  %% 0x0   %% 0x0


##################################################################
// Register Full Name: OC192 MUX OC48 LPMDFE Mode
// RTL Instant Name  : OC192_MUX_OC48_LPMDFE_Mode
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x000E-0x600E
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : Address+($G*0x2000)
// Where        : {$G(0-3) : Group of Ports}
// Description  : Configure LPM/DFE mode , there is 4 group (0-3), each group has 4 sub ports
// Width        : 32
// Register Type: {Configure}
//# Field : [Bit:Bit] %% Name        %% Description                                 %% Type %% Reset %% Default
//  Field : [31:04]   %% Unused      %%                                             %%      %%       %%    
//  Fieldx: [03:00]   %% lpmdfe_mode %% bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15
//                                      {0} : DFE mode
//                                      {1} : LPM mode
//                                                                                  %% R/W  %% 0x0   %% 0x0

##################################################################
// Register Full Name: OC192 MUX OC48 LPMDFE Reset
// RTL Instant Name  : OC192_MUX_OC48_LPMDFE_Reset
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x000F-0x600F
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : Address+($G*0x2000)
// Where        : {$G(0-3) : Group of Ports}
// Description  : Reset LPM/DFE , there is 4 group (0-3), each group has 4 sub ports
// Width        : 32
// Register Type: {Configure}
//# Field : [Bit:Bit] %% Name        %% Description                                 %% Type %% Reset %% Default
//  Field : [31:04]   %% Unused      %%                                             %%      %%       %%    
//  Fieldx: [03:00]   %% lpmdfe_reset%% bit per sub port, Must be toggled after switching between modes to initialize adaptation, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 => Port 8-11, Group 3 => Port 12-15
//                                      {1} : reset                                 %% R/W  %% 0x0   %% 0x0

##################################################################
// Register Full Name: OC192 MUX OC48 TXDIFFCTRL
// RTL Instant Name  : OC192_MUX_OC48_TXDIFFCTRL
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0010-0x6010
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : Address+($G*0x2000)
// Where        : {$G(0-3) : Group of Ports}
// Description  : Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail, there is 4 group (0-3), each group has 4 sub ports
// Width        : 32
// Register Type: {Configure}
//# Field : [Bit:Bit] %% Name        %% Description                                 %% Type %% Reset %% Default
//  Field : [31:20]   %% Unused      %%                                             %%      %%       %%    
//  Fieldx: [19:15]   %% TXDIFFCTRL_subport3 %% Group 3 => Port 12-15               %% R/W  %% 0x18  %% 0x0
//  Fieldx: [14:10]   %% TXDIFFCTRL_subport2 %% Group 2 => Port 8-11                %% R/W  %% 0x18  %% 0x0
//  Fieldx: [09:05]   %% TXDIFFCTRL_subport1 %% Group 1 => Port 4-7                 %% R/W  %% 0x18  %% 0x0
//  Fieldx: [04:00]   %% TXDIFFCTRL_subport0 %% Group 0 => Port 0-3                 %% R/W  %% 0x18  %% 0x0

##################################################################
// Register Full Name: OC192 MUX OC48 TXPOSTCURSOR
// RTL Instant Name  : OC192_MUX_OC48_TXPOSTCURSOR
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0011-0x6011
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : Address+($G*0x2000)
// Where        : {$G(0-3) : Group of Ports}
// Description  : Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail, there is 4 group (0-3), each group has 4 sub ports
// Width        : 32
// Register Type: {Configure}
//# Field : [Bit:Bit] %% Name        %% Description                                 %% Type %% Reset %% Default
//  Field : [31:20]   %% Unused      %%                                             %%      %%       %%    
//  Fieldx: [19:15]   %% TXPOSTCURSOR_subport3 %% Group 3 => Port 12-15             %% R/W  %% 0x0   %% 0x0
//  Fieldx: [14:10]   %% TXPOSTCURSOR_subport2 %% Group 2 => Port 8-11              %% R/W  %% 0x0   %% 0x0
//  Fieldx: [09:05]   %% TXPOSTCURSOR_subport1 %% Group 1 => Port 4-7               %% R/W  %% 0x0   %% 0x0
//  Fieldx: [04:00]   %% TXPOSTCURSOR_subport0 %% Group 0 => Port 0-3               %% R/W  %% 0x0   %% 0x0

##################################################################
// Register Full Name: OC192 MUX OC48 TXPRECURSOR
// RTL Instant Name  : OC192_MUX_OC48_TXPRECURSOR
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0011-0x6011
//# Address is relative address, will be sum with Base_Address for each function block
// Formula      : Address+($G*0x2000)
// Where        : {$G(0-3) : Group of Ports}
// Description  : Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is 4 group (0-3), each group has 4 sub ports
// Width        : 32
// Register Type: {Configure}
//# Field : [Bit:Bit] %% Name        %% Description                                 %% Type %% Reset %% Default
//  Field : [31:20]   %% Unused      %%                                             %%      %%       %%    
//  Fieldx: [19:15]   %% TXPRECURSOR_subport3 %% Group 3 => Port 12-15             %% R/W  %% 0x0   %% 0x0
//  Fieldx: [14:10]   %% TXPRECURSOR_subport2 %% Group 2 => Port 8-11              %% R/W  %% 0x0   %% 0x0
//  Fieldx: [09:05]   %% TXPRECURSOR_subport1 %% Group 1 => Port 4-7               %% R/W  %% 0x0   %% 0x0
//  Fieldx: [04:00]   %% TXPRECURSOR_subport0 %% Group 0 => Port 0-3               %% R/W  %% 0x0   %% 0x0



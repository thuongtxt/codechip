######################################################################################
# Arrive Technologies
# Register Description Document
# Revision 1.0 - 2015.Apr.06

######################################################################################
# This section is for guideline
# Refer to \\Hcmcserv3\Projects\internal_tools\atvn_vlibs\bin\rgm_gen\docs\REGISTER_DESCRIPTION_GUIDELINE.pdf
# File name: filename.atreg (atreg = ATVN register define)
# Comment out a line: Please use (# or //#) character at the beginning of the line
# Delimiter character: (%%)

######################################################################################
#| Access Policy| Use Case| Description                | Effect of a Write on Current Field Value  | Effect of a Read on Current Field Value | Read-back Value |
#| ------|-----------|---------------------------------| ------------------------------------------|-----------------------------------------|-----------------|
#| RO    | Status    | Read Only                       | No effect                | No effect            | Current Value |
#| RW    | Config    | Read, Write                     | Changed to written value | No effect            | Current value |
#| RC    | Status    | Read Clears All                 | No effect                | Sets all bits to 0   | Current value |
#| RS    | UNKNOW    | Read Sets All                   | No effect                | Sets all bits to 1   | Current value |
#| WRC   | UNKNOW    | Write, Read Clears All          | Changed to written value | Sets all bits to 0   | Current value |
#| WRS   | UNKNOW    | Write, Read Sets All            | Changed to written value | Sets all bits to 1   | Current value |
#| WC    | Status    | Write Clears All                | Sets all bits to 0       | No effect            | Current value |
#| WS    | Status    | Write Sets All                  | Sets all bits to 1       | No effect            | Current value |
#| WSRC  | UNKNOW    | Write Sets All, Read Clears All | Sets all bits to 1       | Sets all bits to 0   | Current value |
#| WCRS  | UNKNOW    | Write Clears All,Read Sets All  | Sets all bits to 0       | Sets all bits to 1   | Current value |
#| W1C   | Interrupt | Write 1 to Clear                | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current Value |
#| W1S   | Interrupt | Write 1 to Set                  | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W1T   | UNKNOW    | Write 1 to Toggle               | If the bit in the wr value is a 1, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W0C   | Interrupt | Write 0 to Clear                | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current value |
#| W0S   | Interrupt | Write 0 to Set                  | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W0T   | UNKNOW    | Write 0 to Toggle               | If the bit in the wr value is a 0, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W1SRC | UNKNOW    | Write 1 to Set, Read Clears All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W1CRS | UNKNOW    | Write 1 to Clear, Read Sets All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| W0SRC | UNKNOW    | Write 0 to Set, Read Clears All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W0CRS | UNKNOW    | Write 0 to Clear, Read Sets All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| WO    | Config    | Write Only                      | Changed to written value | No effect | Undefined |
#| WOC   | UNKNOW    | Write Only Clears All           | Sets all bits to 0     | No effect | Undefined |
#| WOS   | UNKNOW    | Write Only Sets All             | Sets all bits to 1     | No effect | Undefined |
#| W1    | UNKNOW    | Write Once                      | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Current value |
#| WO1   | UNKNOW    | Write Only, Once                | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Undefined |
#=============================================================================
# Sample
#=============================================================================

######################################################################################
#SAMPLE> // Begin:
#SAMPLE> //# Some description
#SAMPLE> // Register Full Name: Device Version ID
#SAMPLE> // RTL Instant Name: VersionID
#SAMPLE> //# {FunctionName,SubFunction_InstantName_Description}
#SAMPLE> //# RTL Instant Name and Full Name MUST be unique within a register description file
#SAMPLE> // Address: 0x00_0000
#SAMPLE> //# Address is relative address, will be sum with Base_Address for each function block
#SAMPLE> // Formula      : Address + $portid 
#SAMPLE> // Where        : {$portid(0-7): port identification} 
#SAMPLE> // Description  : This register indicates the module' name and version. 
#SAMPLE> // Width: 32
#SAMPLE> // Register Type: {Status}
#SAMPLE> //# Field: [Bit:Bit] %%  Name     %% Description            %% Type %% SW_Reset %% HW_Reset
#SAMPLE> // Field: [31:24]    %%  Year     %% 2013                   %% RO   %% 0x0D     %% 0x0D
#SAMPLE> // Field: [23:16]    %%  Week     %% Week Synthesized FPGA  %% RO   %% 0x0C     %% 0x0C
#SAMPLE> // Field: [15:8]     %%  Day      %% Day Synthesized FPGA   %% RO   %% 0x16     %% 0x16
#SAMPLE> // Field: [7:4]      %%  VerID    %% FPGA Version           %% RO   %% 0x0      %% 0x0
#SAMPLE> // Field: [3:0]      %%  NumID    %% Number of FPGA Version %% RO   %% 0x1      %% 0x1
#SAMPLE> // End:
######################################################################################
#********************* 
#4.1.1.ETH 10G Version Control 
#********************* 
// Begin: 
// Register Full Name: ETH 10G Version Control
// RTL Instant Name  : eth10g_ver_ctr
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x00
// Formula: {N/A}
// Description: This register checks version ID
// Width: 16
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name     %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [15:8]  %% Eth10gVendor %% Arrive ETH 10G MAC      %% RO   %% 0xAF	 %% 0xAF
// Field: [7:0]]  %% Eth10GVer    %% Version ID              %% RO   %% 0x0      %% 0x0
// End:

#********************* 
#4.1.2.ETH 10G Flush Control 
#********************* 
// Begin: 
// Register Full Name: ETH 10G Flush Control
// RTL Instant Name  : eth10g_flush_ctr
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x01
// Formula: {N/A}
// Description: This register is used to flush engine
// Width: 3
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [2]        %% Eth10gFlushCtrl %% Flush engine            %% RW   %% 0x0      %% 0x0
// Field: [1:0]]     %% Unused          %% Unused                  %% RW   %% 0x0      %% 0x0
// End:

#********************* 
#4.1.3.ETH 10G Protection Control 
#********************* 
// Begin: 
// Register Full Name: ETH 10G Protection Control
// RTL Instant Name  : eth10g_protect_ctr
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x02
// Formula: {N/A}
// Description: This register is used to flush engine
// Width: 13
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description                            %% Type %% SW_Reset %% HW_Reset
// Field: [12]       %% Eth10gRxPortCtrl%% Active/Standby port                    %% RW   %% 0x0      %% 0x0
// Field: [11:10]    %% Unused          %% Unused                                 %% RW   %% 0x0      %% 0x0
// Field: [9]        %% Eth10gRxDICCtrl%% enable DIC(deficit idle count) funtion  %% RW   %% 0x0      %% 0x0
// Field: [8:0]      %% Unused          %% Unused                                 %% RW   %% 0x0      %% 0x0
// End:

#********************* 
#4.1.3.ETH 10G Link Fault Interrupt Enable
#********************* 
// Begin: 
// Register Full Name: ETH 10G Link Fault Interrupt Enable
// RTL Instant Name  : eth10g_link_fault_int_enb
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x1003
// Formula: {N/A}
// Description: This register is used to control link fault function
// Width: 32
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description                            %% Type %% SW_Reset %% HW_Reset
// Field: [31:26]    %% Unused          %% Unused                                 %% RW   %% 0x0      %% 0x0
// Field: [25]       %% Eth10gForceTXRF %% Force TX Remote fault, TX will transmit remote fault code to remote side
//											{0} : normal operation
//											{1} : force remote fault     		  %% RW   %% 0x0      %% 0x0
// Field: [24]       %% Eth10gForceTXLF %% Force TX Local fault, TX will transmit local fault code to remote side,
//											{0} : normal operation
//											{1} : force local fault     		  %% RW   %% 0x0      %% 0x0
// Field: [23]       %% Eth10gForceRXRF %% Force RX Remote fault, RX will rasie RF interrupt, TX will transmit idle code to remote side
//											{0} : normal operation
//											{1} : force remote fault     		  %% RW   %% 0x0      %% 0x0
// Field: [22]       %% Eth10gForceRXLF %% Force RX Local fault, RX will raise LF interrupt, TX will transmit remote fault code to remote side,
//											{0} : normal operation
//											{1} : force local fault     		  %% RW   %% 0x0      %% 0x0
// Field: [21]       %% Unused          %% Reserved, SW has to keep this value    %% RW   %% 0x0      %% 0x0
// Field: [20]       %% Eth10gDisLFault %% Disable Link Fault function  		
//											{0} : enable link fault
//											{1} : disable link fault			  %% RW   %% 0x0      %% 0x0
// Field: [19:17]    %% Unused          %% Unused                                 %% RW   %% 0x0      %% 0x0
// Field: [16]       %% Eth10gSwitchAyns%% switch data and code words of link fault is asynchronous
//											{0} : synchronous				      
//											{1} : asynchronous					  %% RW   %% 0x1      %% 0x1
// Field: [15:7]     %% Unused              %% Unused                                 %% RW   %% 0x0      %% 0x0
// Field: [6]        %% Eth10gDataOrFrameSyncEnb %% interrupt enable of Loss of Data Sync
//											{0} : disable 
//											{1} : enable 							%% RW   %% 0x0      %% 0x0
// Field: [5]        %% Eth10gClockLossEnb 	%% interrupt enable of Loss of Clock
//											{0} : disable 
//											{1} : enable 							%% RW   %% 0x0      %% 0x0
// Field: [4]        %% Eth10gClockOutRangeEnb %% interrupt enable of Frequency Out of Range
//											{0} : disable 
//											{1} : enable 							%% RW   %% 0x0      %% 0x0
// Field: [3]        %% Eth10gExErrorRatioEnb	%% interrupt enable of Excessive Error Ratio
//											{0} : disable 
//											{1} : enable 							%% RW   %% 0x0      %% 0x0
// Field: [2]        %% Eth10gLocalFaultEnb %% interrupt enable of local fault 
//											{0} : disable 
//											{1} : enable 							%% RW   %% 0x0      %% 0x0
// Field: [1]        %% Eth10gRemoteFaultEnb%% interrupt enable of remote fault 
//											{0} : disable 
//											{1} : enable 							%% RW   %% 0x0      %% 0x0
// Field: [0]        %% Eth10gInterrptionEnb%% interrupt enable of interruption
//											{0} : disable 
//											{1} : enable 							%% RW   %% 0x0      %% 0x0
// End:

#********************* 
#4.1.3.ETH 10G Link Fault Sticky
#********************* 
// Begin: 
// Register Full Name: ETH 10G Link Fault Sticky
// RTL Instant Name  : eth10g_link_fault_sticky
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x100B
// Formula: {N/A}
// Description: This register is used to report sticky of link fault
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description                            %% Type %% SW_Reset %% HW_Reset
// Field: [31:7]    %% Unused          %% Unused                                 %% RW   %% 0x0      %% 0x0
// Field: [6]        %% Eth10gDataOrFrameSyncStk %% sticky state change of Loss of Data Syncsticky state change%% W1C  %% 0x0      %% 0x0
// Field: [5]        %% Eth10gClockLossStk 	%% sticky state change of Loss of Clocksticky state change%% W1C  %% 0x0      %% 0x0
// Field: [4]        %% Eth10gClockOutRangeStk %% sticky state change of Frequency Out of Rangesticky state change%% W1C  %% 0x0      %% 0x0
// Field: [3]        %% Eth10gExErrorRatioStk	%% sticky state change of Excessive Error Ratiosticky state change%% W1C  %% 0x0      %% 0x0
// Field: [2]        %% Eth10gLocalFaultStk %% sticky state change of local fault sticky state change%% W1C  %% 0x0      %% 0x0
// Field: [1]        %% Eth10gRemoteFaultStk%% sticky state change of remote fault%% W1C  %% 0x0      %% 0x0
// Field: [0]        %% Eth10gInterrptionStk%% sticky state change of interruption%% W1C  %% 0x0      %% 0x0
// End:

#********************* 
#4.1.3.ETH 10G Link Fault Current Status
#********************* 
// Begin: 
// Register Full Name: ETH 10G Link Fault Current Status
// RTL Instant Name  : eth10g_link_fault_Cur_Sta
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x100D
// Formula: {N/A}
// Description: This register is used to report current status of link fault
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description                            %% Type %% SW_Reset %% HW_Reset
// Field: [31:7]    %% Unused          %% Unused                                 %% RW   %% 0x0      %% 0x0
// Field: [6]        %% Eth10gDataOrFrameSyncCur %% sticky state change of Loss of Data Syncsticky state change%% RO   %% 0x0      %% 0x0
// Field: [5]        %% Eth10gClockLossCur 	%% sticky state change of Loss of Clocksticky state change%% RO   %% 0x0      %% 0x0
// Field: [4]        %% Eth10gClockOutRangeCur %% sticky state change of Frequency Out of Rangesticky state change%% RO   %% 0x0      %% 0x0
// Field: [3]        %% Eth10gExErrorRatioCur	%% sticky state change of Excessive Error Ratiosticky state change%% RO   %% 0x0      %% 0x0
// Field: [2]        %% Eth10gLocalFaultCur %% current status of local fault      %% RO   %% 0x0      %% 0x0
// Field: [1]        %% Eth10gRemoteFaultCur%% current status of remote fault     %% RO   %% 0x0      %% 0x0
// Field: [0]        %% Eth10gInterrptionCur%% current status of interruption     %% RO   %% 0x0      %% 0x0
// End:

#********************* 
#4.1.4.ETH 10G Loopback Control 
#********************* 
// Begin: 
// Register Full Name: ETH 10G Loopback Control
// RTL Instant Name  : eth10g_loop_ctr
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x40
// Formula: {N/A}
// Description: This register is used to configure loopback funtion
// Width: 7
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name                     %% Description                                  %% Type %% SW_Reset %% HW_Reset
// Field: [6]        %% Eth10gStandPortLoopoutCtrl%% Standby port loop-out (XGMII rx to XGMII tx) %% RW   %% 0x0      %% 0x0
// Field: [5]        %% Eth10gActPortLoopoutCtrl  %% Active  port loop-out (XGMII rx to XGMII tx) %% RW   %% 0x0      %% 0x0
// Field: [4:3]      %% Unused                    %% Unused                                       %% RW   %% 0x0      %% 0x0
// Field: [2]        %% Eth10gStandPortLoopinCtrl %% Standby port loop-in (XGMII tx to XGMII rx)  %% RW   %% 0x0      %% 0x0
// Field: [1]        %% Eth10gActPortLoopinCtrl   %% Active  port loop-in (XGMII tx to XGMII rx)  %% RW   %% 0x0      %% 0x0
// Field: [0]        %% Unused                    %% Unused                                       %% RW   %% 0x0      %% 0x0
// End:

#********************* 
#4.1.5.ETH 10G MAC Receive Control 
#********************* 
// Begin: 
// Register Full Name: ETH 10G MAC Reveive Control
// RTL Instant Name  : eth10g_mac_rx_ctr
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x42
// Formula: {N/A}
// Description: This register is used to configure MAC at the reveiver direction
// Width: 12	
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name                     %% Description                    %% Type %% SW_Reset %% HW_Reset
// Field: [11:8]     %% Eth10gMACRxIpgCtrl        %% RX inter packet gap            %% RW   %% 0x4      %% 0x4
// Field: [7:1]      %% Unused                    %% Unused                         %% RW   %% 0x0      %% 0x0
// Field: [0]        %% Eth10gMACRxFCSBypassCtrl  %% Bypass the received FCS 4-byte %% RW   %% 0x1      %% 0x1
// End:

#********************* 
#4.1.6.ETH 10G MAC Transmit Control 
#********************* 
// Begin: 
// Register Full Name: ETH 10G MAC Transmit Control
// RTL Instant Name  : eth10g_mac_tx_ctr
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x43
// Formula: {N/A}
// Description: This register is used to configure MAC at the reveiver direction
// Width: 13	
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name                     %% Description          %% Type %% SW_Reset %% HW_Reset
// Field: [12:8]     %% Eth10gMACTxIpgCtrl        %% TX inter packet gap  %% RW   %% 0x8      %% 0x8
// Field: [7:1]      %% Unused                    %% Unused               %% RW   %% 0x0      %% 0x0
// Field: [0]        %% Eth10gMACTxFCSInsCtrl     %% Insert FCS 4-byte    %% RW   %% 0x1      %% 0x1
// End:


#******************************************************************************************* 
#ETH eXAUI TX MAC Statictics Counter
#******************************************************************************************* 

#********************* 
#4.1.7.1 Transmit Ethernet port Count0-64 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count0_64 bytes packet 
// RTL Instant Name  : TxEth_cnt0_64
// Address: 0x0003000(RO) %% 0x0003800(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-0): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 0 to 64 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% TxEthCnt0_64 %% This is statistic counter for the packet having 0 to 64 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.7.2 Transmit Ethernet port Count65_127 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count65_127 bytes packet 
// RTL Instant Name  : TxEth_cnt65_127
// Address: 0x0003002(RO) %% 0x0003802(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-0): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 65 to 127 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% TxEthCnt65_127 %% This is statistic counter for the packet having 65 to 127 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.7.3 Transmit Ethernet port Count128_255 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count128_255 bytes packet 
// RTL Instant Name  : TxEth_cnt128_255
// Address: 0x0003004(RO) %% 0x0003804(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-0): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 128 to 255 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% TxEthCnt128_255 %% This is statistic counter for the packet having 128 to 255 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.7.4 Transmit Ethernet port Count256_511 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count256_511 bytes packet 
// RTL Instant Name  : TxEth_cnt256_511
// Address: 0x0003006(RO) %% 0x0003806(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-0): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 256 to 511 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% TxEthCnt256_511 %% This is statistic counter for the packet having 256 to 511 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.7.5 Transmit Ethernet port Count512_1023 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count512_1023 bytes packet 
// RTL Instant Name  : TxEth_cnt512_1024
// Address: 0x0003008(RO) %% 0x0003808(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-0): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 512 to 1023 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% TxEthCnt512_1024 %% This is statistic counter for the packet having 512 to 1023 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.7.6 Transmit Ethernet port Count1025_1528 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count1024_1518 bytes packet 
// RTL Instant Name  : Eth_cnt1025_1528
// Address: 0x000300A(RO) %% 0x000380A(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-0): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 1024 to 1518 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% TxEthCnt1025_1528 %% This is statistic counter for the packet having 1024 to 1518 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.7.7 Transmit Ethernet port Count1529_2047 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count1519_2047 bytes packet 
// RTL Instant Name  : TxEth_cnt1529_2047
// Address: 0x000300C(RO) %% 0x000380C(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-0): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 1519 to 2047 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% TxEthCnt1529_2047 %% This is statistic counter for the packet having 1519 to 2047 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.7.8 Transmit Ethernet port Count Jumbo packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count Jumbo packet 
// RTL Instant Name  : TxEth_cnt_jumbo
// Address: 0x000300E(RO) %% 0x000380E(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-0): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having more than 2048 bytes (jumbo)
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% TxEthCntJumbo %% This is statistic counter for the packet more than 2048 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.7.9 Transmit Ethernet port Count Total packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count Total packet 
// RTL Instant Name  : eth_tx_pkt_cnt
// Address: 0x0003012(RO) %% 0x0003812(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-0): Transmit Ethernet port ID}
// Description: This register is statistic counter for the total packet at Transmit side 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% TxEthCntTotal %% This is statistic counter total packet %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.7.10 Transmit Ethernet port Count number of bytes of packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count number of bytes of packet 
// RTL Instant Name  : TxEth_cnt_byte
// Address: 0x000301E(RO) %% 0x000381E(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-0): Transmit Ethernet port ID}
// Description: This register is statistic count number of bytes of packet at Transmit side 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% TxEthCntByte %% This is statistic counter number of bytes of packet %% RO %% 0x0 %% 0x0
// End:

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-10-01|Project|Initial version|




##Xilinx_Serdes_Turning_1x
####Register Table

|Name|Address|
|-----|-----|
|`SERDES DRP PORT`|`XFI (0x400-0x7FF) - SGMII&Other (0x200-x3FF)`|
|`SERDES LoopBack`|`0x02`|
|`SERDES POWER DOWN`|`0x03`|
|`SERDES TX Reset`|`0x0C`|
|`SERDES RX Reset`|`0x0D`|
|`SERDES LPMDFE Mode`|`0x0E`|
|`SERDES LPMDFE Reset`|`0x0F`|
|`SERDES TXDIFFCTRL`|`0x10`|
|`SERDES TXPOSTCURSOR`|`0x11`|
|`SERDES TXPRECURSOR`|`0x12`|


###SERDES DRP PORT

* **Description**           

Read/Write DRP address of SERDES,


* **RTL Instant Name**    : `SERDES_DRP_PORT`

* **Address**             : `XFI (0x400-0x7FF) - SGMII&Other (0x200-x3FF)`

* **Formula**             : `XFI + $DRP`

* **Where**               : 

    * `For XFI must control port enable by TOP register control`

    * `$DRP(0-1023) : DRP address, see UG578`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:00]`|`drp_rw`| DRP read/write value| `R/W`| `0x0`| `0x0`|

###SERDES LoopBack

* **Description**           

Configurate LoopBack,


* **RTL Instant Name**    : `SERDES_LoopBack`

* **Address**             : `0x02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`lpback_serdes`| Loop back mode<br>{0} : Normal <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|

###SERDES POWER DOWN

* **Description**           

Configurate Power Down ,


* **RTL Instant Name**    : `SERDES_POWER_DOWN`

* **Address**             : `0x03`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04]`|`txelecidle`| TXELECIDLE must be strapped to TXPD[1] and TXPD[0] of Port<br>{1} : Power Down <br>{0} : Normal| `R/W`| `0x1`| `0x1`|
|`[03:02]`|`txpd`| TX Power Down<br>{0} : Normal <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|
|`[01:00]`|`rxpd`| RX Power Down<br>{0} : Normal <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|

###SERDES TX Reset

* **Description**           

Reset TX SERDES,


* **RTL Instant Name**    : `SERDES_TX_Reset`

* **Address**             : `0x0C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[17:16]`|`txrst_done`| TX Reset Done, bit per sub port<br>{1} : reset done| `RO`| `0x0`| `0x0`|
|`[15:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:00]`|`txrst_en`| Should reset TX_PMA SERDES about 300-500 ns<br>{1} : Reset <br>{0} : Un-Reset| `R/W`| `0x0`| `0x0`|

###SERDES RX Reset

* **Description**           

Reset RX SERDES,


* **RTL Instant Name**    : `SERDES_RX_Reset`

* **Address**             : `0x0D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[17:16]`|`rxrst_done`| RX Reset Done<br>{1} : reset done| `RO`| `0x0`| `0x0`|
|`[15:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:00]`|`rxrst_en`| Should reset reset RX_PMA SERDES about 300-500 ns<br>{1} : Reset <br>{0} : Un-Reset| `R/W`| `0x0`| `0x0`|

###SERDES LPMDFE Mode

* **Description**           

Configure LPM/DFE mode ,


* **RTL Instant Name**    : `SERDES_LPMDFE_Mode`

* **Address**             : `0x0E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`lpmdfe_mode`| bit per sub port<br>{0} : DFE mode <br>{1} : LPM mode| `R/W`| `0x0`| `0x0`|

###SERDES LPMDFE Reset

* **Description**           

Reset LPM/DFE ,


* **RTL Instant Name**    : `SERDES_LPMDFE_Reset`

* **Address**             : `0x0F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:01]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[00:00]`|`lpmdfe_reset`| bit per sub port, Must be toggled after switching between modes to initialize adaptation<br>{1} : reset| `R/W`| `0x0`| `0x0`|

###SERDES TXDIFFCTRL

* **Description**           

Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail, there is  2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7


* **RTL Instant Name**    : `SERDES_TXDIFFCTRL`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:00]`|`txdiffctrl`| | `R/W`| `0x18`| `0x0`|

###SERDES TXPOSTCURSOR

* **Description**           

Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail, there is  2 sub ports


* **RTL Instant Name**    : `SERDES_TXPOSTCURSOR`

* **Address**             : `0x11`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:00]`|`txpostcursor`| | `R/W`| `0x0`| `0x0`|

###SERDES TXPRECURSOR

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is  2 sub ports


* **RTL Instant Name**    : `SERDES_TXPRECURSOR`

* **Address**             : `0x12`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:05]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[04:00]`|`txprecursor`| | `R/W`| `0x0`| `0x0`|

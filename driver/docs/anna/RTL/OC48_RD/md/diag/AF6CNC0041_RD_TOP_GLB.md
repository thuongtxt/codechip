## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-10-01|AF6Project|Initial version|




##AF6CNC0041_RD_TOP_GLB
####Register Table

|Name|Address|
|-----|-----|
|`Device Product ID`|`0xF0_0000`|
|`Device Year Month Day Version ID`|`0xF0_0001`|
|`Device Internal ID`|`0xF0_0003`|
|`general purpose inputs for TX Uart`|`0x5`|
|`general purpose inputs for TX Uart`|`0x40`|
|`10G control`|`0x41`|
|`Diagnostic Enable Control`|`0x43`|
|`general purpose outputs from RX Uart`|`0x60`|
|`Top Interface Alarm Sticky`|`0x00052`|
|`Clock Monitoring Status`|`0x46000 - 0x4601F`|
|`Clock Monitoring Status`|`0x47000 - 0x4701f`|


###Device Product ID

* **Description**           

This register indicates Product ID.


* **RTL Instant Name**    : `ProductID`

* **Address**             : `0xF0_0000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`productid`| ProductId| `RO`| `0x60290021`| `0x60290021 End: Begin:`|

###Device Year Month Day Version ID

* **Description**           

This register indicates Year Month Day and main version ID.


* **RTL Instant Name**    : `YYMMDD_VerID`

* **Address**             : `0xF0_0001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`year`| Year| `RO`| `0x16`| `0x16`|
|`[23:16]`|`month`| Month| `RO`| `0x09`| `0x09`|
|`[15:8]`|`day`| Day| `RO`| `0x17`| `0x17`|
|`[7:0]`|`version`| Version| `RO`| `0x01`| `0x01 End: Begin:`|

###Device Internal ID

* **Description**           

This register indicates internal ID.


* **RTL Instant Name**    : `InternalID`

* **Address**             : `0xF0_0003`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `16`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`internalid`| InternalID| `RO`| `0x1`| `0x1 End: Begin:`|

###general purpose inputs for TX Uart

* **Description**           

This is the global configuration register for the MIG Reset Control


* **RTL Instant Name**    : `o_reset_ctr`

* **Address**             : `0x5`

* **Formula**             : `0x5`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:3]`|`unused`| *n/a*| `RW`| `0x7`| `0x7`|
|`[2]`|`ddr3_reset`| Reset Control for DDR#3 0: Disable 1: Enable| `RW`| `0x1`| `0x1`|
|`[1]`|`ddr2_reset`| Reset Control for DDR#2 0: Disable 1: Enable| `RW`| `0x1`| `0x1`|
|`[0]`|`ddr1_reset`| Reset Control for DDR#1 0: Disable 1: Enable| `RW`| `0x1`| `0x1 End : Begin:`|

###general purpose inputs for TX Uart

* **Description**           

This is the global configuration register for the Global Serdes Control


* **RTL Instant Name**    : `o_control0`

* **Address**             : `0x40`

* **Formula**             : `0x40`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:29]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[28:28]`|`fsm_card_pw_tx_gsel`| global selection between external FSM-CARD and inernal selection<br>{0} : internal card selection <br>{1} : external FSM card selection| `RW`| `0x0`| `0x0`|
|`[27:27]`|`fsm_card_pw_tx_isel`| internal selection<br>{0} : internal card enable tx PW <br>{1} : internal card disable tx PW| `RW`| `0x0`| `0x0`|
|`[26:17]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[16:16]`|`multirate_sel`| select 2 two RX group of Multirate serdes to CORE<br>{0} : select group 0 (0-7) <br>{1} : select group 1 (8-15)| `RW`| `0x0`| `0x0`|
|`[15:08]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[07:00]`|`o_control0`| Uart TX 8-bit data out| `RW`| `0x0`| `0x0 End : Begin:`|

###10G control

* **Description**           

This is the global configuration register for the Global Serdes Control


* **RTL Instant Name**    : `o_control1`

* **Address**             : `0x41`

* **Formula**             : `0x41`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31]`|`dup_tx_10g`| dupplicate 10G port| `RW`| `0x0`| `0x0`|
|`[30]`|`enrxfsmpin`| Enable RX FSM pin1 from outsite for 10G port select| `RW`| `0x0`| `0x0`|
|`[29]`|`sel10gport`| Select RX 10G port inside| `RW`| `0x0`| `0x0`|
|`[28]`|`sel_fsm_card`| (0) : select CPu, (1) select FSM card| `RW`| `0x0`| `0x0`|
|`[27]`|`cpu_sel_card`| (0) : active, (1) standby| `RW`| `0x0`| `0x0`|
|`[26:00]`|`unused`| *n/a*| `RW`| `0x0`| `0x0 End : Begin:`|

###Diagnostic Enable Control

* **Description**           

This is the global configuration register for the MIG (DDR/QDR) Diagnostic Control


* **RTL Instant Name**    : `o_control3`

* **Address**             : `0x43`

* **Formula**             : `0x43`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:9]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[8:8]`|`tfi5_diagen`| Enable Daignostic for TFI5 port) 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[7:6]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[5:5]`|`xfi_stby_diagen`| Enable Daignostic for XFI STBY port) 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[4:4]`|`xfi_act_diagen`| Enable Daignostic for XFI ACTICE port) 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[3:3]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[2]`|`ddr3_diagen`| Enable Daignostic for DDR#1 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[1]`|`ddr2_diagen`| Enable Daignostic for DDR#2 0: Disable 1: Enable| `RW`| `0x0`| `0x0`|
|`[0]`|`ddr1_diagen`| Enable Daignostic for DDR#1 0: Disable 1: Enable| `RW`| `0x0`| `0x0 End : Begin:`|

###general purpose outputs from RX Uart

* **Description**           

This is value output from RX Uart


* **RTL Instant Name**    : `c_uart`

* **Address**             : `0x60`

* **Formula**             : `0x60`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:08]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[07:00]`|`i_status0`| Uart RX 8-bit data in| `RO`| `0x0`| `0x0 End : Begin:`|

###Top Interface Alarm Sticky

* **Description**           

Top Interface Alarm Sticky


* **RTL Instant Name**    : `i_sticky2`

* **Address**             : `0x00052`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:26]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[25]`|`ot_out`| Set 1 When Over-Temperature alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[24]`|`vbram_alarm_out`| Set 1 When VCCBRAM-sensor alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[23]`|`vccaux_alarm_out`| Set 1 When VCCAUX-sensor alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[22]`|`vccint_alarm_out`| Set 1 When VCCINT-sensor alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[21]`|`user_temp_alarm_out`| Set 1 When Temperature-sensor alarm Happens.| `W1C`| `0x0`| `0x0`|
|`[20]`|`alarm_out`| Set 1 When SysMon Error Happens.| `W1C`| `0x0`| `0x0`|
|`[19:11]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[10]`|`ddr43urst`| Set 1 while user Reset State Change Happens When DDR#3 Reset User.| `W1C`| `0x0`| `0x0`|
|`[9]`|`ddr42urst`| Set 1 while user Reset State Change Happens When DDR#2 Reset User.| `W1C`| `0x0`| `0x0`|
|`[8]`|`ddr41urst`| Set 1 while user Reset State Change Happens When DDR#1 Reset User.| `W1C`| `0x0`| `0x0`|
|`[7:4]`|`unused`| *n/a*| `W1C`| `0x0`| `0x0`|
|`[3]`|`ddr43calib`| Set 1 while Calib state change event happens when DDR Calib Fail.| `W1C`| `0x0`| `0x0`|
|`[2]`|`ddr42calib`| Set 1 while Calib state change event happens when DDR Calib Fail.| `W1C`| `0x0`| `0x0`|
|`[1]`|`ddr41calib`| Set 1 while Calib state change event happens when DDR Calib Fail.| `W1C`| `0x0`| `0x0`|
|`[0]`|`systempll`| Set 1 while PLL Locked state change event happens when System PLL not locked.| `W1C`| `0x0`| `0x0 End : Begin:`|

###Clock Monitoring Status

* **Description**           



0x18 : tfi5_txclk           (155.52 Mhz)

0x17 : unused               (0 Mhz)

0x16 : unused               (0 Mhz)

0x15 : unused               (0 Mhz)

0x14 : unused               (0 Mhz)

0x13 : unused               (0 Mhz)

0x12 : unused               (0 Mhz)

0x11 : unused               (0 Mhz)

0x10 : tfi5_rxclk	         (155.52 Mhz)

0x0f : unused               (0 Mhz)

0x0e : ext_ref clock        (19.44 Mhz)

0x0d : prc_ref clock        (19.44 Mhz)

0x0c : ddr_ref3			 (100 Mhz)

0x0b : ddr_ref2			 (100 Mhz)

0x0a : dccgmii_clk          (125 Mhz)

0x09 : eth10g_1_txclk       (156.25 Mhz)

0x08 : eth10g_1_rxclk       (156.25 Mhz)

0x07 : eth10g_0_txclk       (156.25 Mhz)

0x06 : eth10g_0_rxclk       (156.25 Mhz)

0x05 :	ddr_ref1			 (100 Mhz)

0x04 : DDR4#3 user clock    (300 Mhz)

0x03 : DDR4#2 user clock    (300 Mhz)

0x02 : DDR4#1 user clock    (300 Mhz)

0x01 : unused               (0 Mhz)

0x00 : pcie_upclk clock     (62.5 Mhz)


* **RTL Instant Name**    : `clock_mon_high`

* **Address**             : `0x46000 - 0x4601F`

* **Formula**             : `0x46000 + port_id`

* **Where**               : 

    * `$port_id(0-31`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`i_statusx`| Clock Value Monitor Change from hex format to DEC format to get Clock Value| `RO`| `0x0`| `0x0 End : Begin:`|

###Clock Monitoring Status

* **Description**           



0x1f : unused (0 Mhz)

0x1e : unused (0 Mhz)

0x1d : pm_tick              (1 Hz) for get value frequency SW must use : F = 155520000/register value

0x1c : one_pps              (1 Hz) for get value frequency SW must use : F = 155520000/register value

0x1b : unused (0 Mhz)

0x1a : unused (0 Mhz)

0x19 : unused (0 Mhz)

0x18 : unused (0 Mhz)

0x17 : unused (0 Mhz)

0x16 : unused (0 Mhz)

0x15 : unused (0 Mhz)

0x14 : unused (0 Mhz)

0x13 : fsm_pcp[2]			 (1 Mhz or 2 Mhz  frequency clk Diagnostic)

0x12 : fsm_pcp[1]			 (1 Mhz or 2 Mhz  frequency clk Diagnostic)

0x11 : fsm_pcp[0]			 (1 Mhz or 2 Mhz  frequency clk Diagnostic)

0x10 : unused (0 Mhz)

0x0f : unused (0 Mhz)

0x0e : unused (0 Mhz)

0x0d : unused (0 Mhz)

0x0c : unused (0 Mhz)

0x0b : unused (0 Mhz)

0x0a : spare_gpio[7]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x09 : spare_gpio[6]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x08 : spare_gpio[5]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x07 : spare_gpio[4]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x06 : spare_gpio[3]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x05 : spare_gpio[2]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x04 : spare_gpio[1]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x03 : spare_gpio[0]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x02 : unused (0 Mhz)

0x01 : spare_clk[1]         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

0x00 : spare_clk[0]      (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)


* **RTL Instant Name**    : `clock_mon_slow`

* **Address**             : `0x47000 - 0x4701f`

* **Formula**             : `0x47000 + port_id`

* **Where**               : 

    * `$port_id(0-31`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`i_statusx`| Clock Value Monitor Change from hex format to DEC format to get Clock Value| `RO`| `0x0`| `0x0 End :`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-10-01|AF6Project|Initial version|




##AF6CNC0041_Xilinx_Serdes_Turning_2xSGMII
####Register Table

|Name|Address|
|-----|-----|
|`SERDES DRP PORT`|`0x800-0xFFF`|
|`SERDES LoopBack`|`0x02`|
|`SERDES POWER DOWN`|`0x03`|
|`SERDES PLL Status`|`0x0B`|
|`SERDES TX Reset`|`0x0C`|
|`SERDES RX Reset`|`0x0D`|
|`SERDES LPMDFE Mode`|`0x0E`|
|`SERDES LPMDFE Reset`|`0x0F`|
|`SERDES TXDIFFCTRL`|`0x10`|
|`SERDES TXPOSTCURSOR`|`0x11`|
|`SERDES TXPRECURSOR`|`0x12`|
|`SERDES PRBS generator test pattern control`|`0x20`|
|`SERDES prbsctr_pen`|`0x21`|
|`SERDES PRBS Diag Status`|`0x22`|
|`SERDES PRBS RX_PRBS_ERR_CNT`|`0x25E - 0x25F (DRP Address)`|


###SERDES DRP PORT

* **Description**           

Read/Write DRP address of SERDES,


* **RTL Instant Name**    : `SERDES_DRP_PORT`

* **Address**             : `0x800-0xFFF`

* **Formula**             : `0x800+$P*0x400+$DRP`

* **Where**               : 

    * `$P(0-1) : Sub Port ID`

    * `$DRP(0-1023) : DRP address, see UG578`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:00]`|`drp_rw`| DRP read/write value| `R/W`| `0x0`| `0x0`|

###SERDES LoopBack

* **Description**           

Configurate LoopBack,


* **RTL Instant Name**    : `SERDES_LoopBack`

* **Address**             : `0x02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:04]`|`lpback_subport1`| Loopback subport 1<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`lpback_subport0`| Loopback subport 0<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|

###SERDES POWER DOWN

* **Description**           

Configurate Power Down ,


* **RTL Instant Name**    : `SERDES_POWER_DOWN`

* **Address**             : `0x03`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[11:10]`|`txpd1`| Power Down subport 1<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|
|`[09:08]`|`txpd0`| Power Down subport 0<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|
|`[03:02]`|`rxpd1`| Power Down subport 1<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|
|`[01:00]`|`rxpd0`| Power Down subport 0<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|

###SERDES PLL Status

* **Description**           

QPLL/CPLL status,


* **RTL Instant Name**    : `SERDES_PLL_Status`

* **Address**             : `0x0B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:29]`|`qpll1_lock_change`| QPLL1 has transition lock/unlock, Group 0-1<br>{1} : QPLL1_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[28:28]`|`qpll0_lock_change`| QPLL0 has transition lock/unlock, Group 0-1<br>{1} : QPLL0_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[27:26]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[25:25]`|`qpll1_lock`| QPLL0 is Locked, Group 0-1<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[24:24]`|`qpll0_lock`| QPLL0 is Locked, Group 0-1<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[23:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`cpll_lock_change`| CPLL has transition lock/unlock, bit per sub port,<br>{1} : CPLL_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[11:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`cpll_lock`| CPLL is Locked, bit per sub port,<br>{1} : Locked| `R_O`| `0x0`| `0x0`|

###SERDES TX Reset

* **Description**           

Reset TX SERDES,


* **RTL Instant Name**    : `SERDES_TX_Reset`

* **Address**             : `0x0C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[17:16]`|`txrst_done`| TX Reset Done, bit per sub port<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:00]`|`txrst_trig`| Should reset TX_PMA SERDES about 300-500 ns , bit per sub port<br>{1} : Reset <br>{0} : Un-Reset| `R/W`| `0x0`| `0x0`|

###SERDES RX Reset

* **Description**           

Reset RX SERDES,


* **RTL Instant Name**    : `SERDES_RX_Reset`

* **Address**             : `0x0D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[17:16]`|`rxrst_done`| RX Reset Done, bit per sub port<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:00]`|`rxrst_trig`| Should reset reset RX_PMA SERDES about 300-500 ns , bit per sub port<br>{1} : Reset <br>{0} : Un-Reset| `R/W`| `0x0`| `0x0`|

###SERDES LPMDFE Mode

* **Description**           

Configure LPM/DFE mode ,


* **RTL Instant Name**    : `SERDES_LPMDFE_Mode`

* **Address**             : `0x0E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:00]`|`lpmdfe_mode`| bit per sub port<br>{0} : DFE mode <br>{1} : LPM mode| `R/W`| `0x0`| `0x0`|

###SERDES LPMDFE Reset

* **Description**           

Reset LPM/DFE ,


* **RTL Instant Name**    : `SERDES_LPMDFE_Reset`

* **Address**             : `0x0F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[01:00]`|`lpmdfe_reset`| bit per sub port, Must be toggled after switching between modes to initialize adaptation<br>{1} : reset| `R/W`| `0x0`| `0x0`|

###SERDES TXDIFFCTRL

* **Description**           

Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail, there is  2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7


* **RTL Instant Name**    : `SERDES_TXDIFFCTRL`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:05]`|`txdiffctrl_subport1`| | `R/W`| `0x18`| `0x0`|
|`[04:00]`|`txdiffctrl_subport0`| | `R/W`| `0x18`| `0x0`|

###SERDES TXPOSTCURSOR

* **Description**           

Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail, there is  2 sub ports


* **RTL Instant Name**    : `SERDES_TXPOSTCURSOR`

* **Address**             : `0x11`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:05]`|`txpostcursor_subport1`| | `R/W`| `0x0`| `0x0`|
|`[04:00]`|`txpostcursor_subport0`| | `R/W`| `0x0`| `0x0`|

###SERDES TXPRECURSOR

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is  2 sub ports


* **RTL Instant Name**    : `SERDES_TXPRECURSOR`

* **Address**             : `0x12`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:05]`|`txprecursor_subport1`| | `R/W`| `0x0`| `0x0`|
|`[04:00]`|`txprecursor_subport0`| | `R/W`| `0x0`| `0x0`|

###SERDES PRBS generator test pattern control

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is  2 sub ports


* **RTL Instant Name**    : `prbsctr_pen`

* **Address**             : `0x20`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07:04]`|`prbssel_subport1`| Transmitter PRBS generator and Receiver PRBS checker test pattern control.After changing patterns, perform a reset of the RX (GTRXRESET, RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the RX pattern checker can attempt to reestablish the link acquired. No checking is done for non-PRBS patterns.<br>{0}: Standard operation mode.(PRBS generator/check is off) <br>{1}: PRBS-7 <br>{2}: PRBS-9 <br>{3}: PRBS-15 <br>{4}: PRBS-23 <br>{5}: PRBS-31| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`prbssel_subport0`| Transmitter PRBS generator and Receiver PRBS checker test pattern control.After changing patterns, perform a reset of the RX (GTRXRESET, RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the RX pattern checker can attempt to reestablish the link acquired. No checking is done for non-PRBS patterns.<br>{0}: Standard operation mode.(PRBS generator/check is off) <br>{1}: PRBS-7 <br>{2}: PRBS-9 <br>{3}: PRBS-15 <br>{4}: PRBS-23 <br>{5}: PRBS-31| `R/W`| `0x0`| `0x0`|

###SERDES prbsctr_pen

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is  2 sub ports


* **RTL Instant Name**    : `SERDES_prbsctr_pen`

* **Address**             : `0x21`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[05]`|`rxprbscntreset_subport1`| Resets the PRBS error counter.| `R/W`| `0x0`| `0x0`|
|`[04]`|`rxprbscntreset_subport0`| Resets the PRBS error counter.| `R/W`| `0x0`| `0x0`|
|`[01]`|`txprbsforceerr_subport1`| When this port is driven High, errors are forced in the PRBS transmitter. While this port is asserted, the output data pattern contains errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA| `R/W`| `0x0`| `0x0`|
|`[00]`|`txprbsforceerr_subport0`| When this port is driven High, errors are forced in the PRBS transmitter. While this port is asserted, the output data pattern contains errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA| `R/W`| `0x0`| `0x0`|

###SERDES PRBS Diag Status

* **Description**           

QPLL/CPLL status,


* **RTL Instant Name**    : `SERDES_PRBS_Status`

* **Address**             : `0x22`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[11:10]`|`rxprbslocked`| Output to indicate that the RX PRBS checker has been error free after reset. Once asserted High, RXPRBSLOCKED does not deassert until reset of the RX pattern checker via a reset of the RX (GTRXRESET, RXPMARESET, or RXPCSRESET in sequential mode) or a reset of the PRBS error counter (RXPRBSCNTRESET), bit per sub port,<br>{1} : RX PRBS is LOCKED| `R_O`| `0x0`| `0x0`|
|`[09:08]`|`rxprbserr`| This non-sticky status output indicates that PRBS errors have occurred. , bit per sub port,<br>{1} : RX PRBS is Error| `R_O`| `0x0`| `0x0`|
|`[03:02]`|`rxprbslocked_change`| This sticky status output indicates that PRBS not locked, bit per sub port,<br>{1} : RX PRBS not locked has rate| `W1C`| `0x0`| `0x0`|
|`[01:00]`|`rxprbserr_change`| This sticky status output indicates that PRBS errors have occurred. , bit per sub port,<br>{1} : PRBS errors has rate| `W1C`| `0x0`| `0x0`|

###SERDES PRBS RX_PRBS_ERR_CNT

* **Description**           




* **RTL Instant Name**    : `SERDES_PRBS_RX_PRBS_ERR_CNT`

* **Address**             : `0x25E - 0x25F (DRP Address)`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rx_prbs_err_cnt`| PRBS error counter. This counter can be reset by asserting RXPRBSCNTRESET. When a single bit error in incoming data occurs, this counter increments by 1. Single bit errors are counted thus when multiple bit errors occur in incoming data. The counter increments by the actual number of bit errors. Counting begins after config mode and prbs syn . The counter saturates at 32'hFFFFFFFF. This error counter can only be accessed via the DRP interface. Because the DRP only outputs 16 bits of data per operation, two DRP transactions must be completed to read out the complete 32-bit value. To properly read out the error counter, read out the lower 16 bits at address 0x25E first, followed by the upper 16 bits at address 0x25F| `R/W`| `0x0`| `0x0`|

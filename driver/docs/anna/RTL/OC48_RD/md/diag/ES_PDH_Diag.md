## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-10-01|Project|Initial version|




##ES_PDH_Diag
####Register Table

|Name|Address|
|-----|-----|
|`LIU Global Control`|`0x000`|
|`LIU Loopout`|`0x001`|
|`LIU PRBS En`|`0x002`|
|`LIU PRBS Insert Error`|`0x004`|
|`LIU PRBS sticky`|`0x005`|
|`LIU PRBS Invert`|`0x007`|


###LIU Global Control

* **Description**           

This is the global configuration register for the LIU mode


* **RTL Instant Name**    : `liu_ctl_glb_reg`

* **Address**             : `0x000`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[01:01]`|`ds1_clock`| Set 1 to select T1/DS1 Tx Clock from LIU, Set 0 to select E1 Clock from FPGA| `RW`| `0x0`| `0x0`|
|`[00:00]`|`ds1_mode`| Set 1 to select T1/DS1 mode, Set 0 to select E1 mode| `RW`| `0x0`| `0x0 End : Begin:`|

###LIU Loopout

* **Description**           

This is the global configuration register for the LIU loopout


* **RTL Instant Name**    : `liu_loopout_reg`

* **Address**             : `0x001`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`liu_loopout`| LIU loopback out control, per bit per ports| `RW`| `0x0`| `0x0 End : Begin:`|

###LIU PRBS En

* **Description**           

This is the global configuration register for the LIU Generator PRBS data


* **RTL Instant Name**    : `liu_PRBS_En_reg`

* **Address**             : `0x002`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`liu_prbs_en`| LIU PRBS enable, per bit per ports| `RW`| `0x1`| `0x1 End : Begin:`|

###LIU PRBS Insert Error

* **Description**           

This is the global configuration register for the LIU Insert PRBS Error


* **RTL Instant Name**    : `liu_PRBS_Insert_Err_reg`

* **Address**             : `0x004`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`liu_prbs_insert_err`| LIU PRBS Insert Error, per bit per ports| `RW`| `0x0`| `0x0 End : Begin:`|

###LIU PRBS sticky

* **Description**           

This is the global configuration register for the LIU Insert PRBS Error


* **RTL Instant Name**    : `liu_PRBS_Sticky_reg`

* **Address**             : `0x005`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`liu_prbs_stk_err`| LIU PRBS Sticky PRBS error monitor, per bit per ports| `RWC`| `0x0`| `0x0 End : Begin:`|

###LIU PRBS Invert

* **Description**           

This is the global configuration register for the LIU Insert PRBS Error


* **RTL Instant Name**    : `liu_PRBS_Invert_reg`

* **Address**             : `0x007`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`liu_prbs_invert`| Set 1 to enable LIU PRBS Invert, per bit per ports| `RW`| `0x0`| `0x0 End :`|

## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-10-01|Project|Initial version|




##ES_Xilinx_XGMII_RD_DIAG
####Register Table

|Name|Address|
|-----|-----|
|`MiiAlarmGlb`|`0x00`|
|`MiiStatusGlb`|`0x1`|
|`MiiEthTestControl`|`0x02`|
|`gatetime config value`|`0x03`|
|`Gatetime Current`|`0x04`|
|`MiiTxPkgCounter [31:0].`|`0x08`|
|`MiiTxPkgCounter [47:32].`|`0x09`|
|`MiiTxByteCounter [31:0].`|`0x0A`|
|`MiiTxByteCounter [63:32].`|`0x0B`|
|`MiiRXPkgCounter [31:0].`|`0x0C`|
|`MiiRXPkgCounter [47:32].`|`0x0D`|
|`MiiRXByteCounter [31:0].`|`0x0E`|
|`MiiRXbyteCounter [63:32].`|`0x0F`|
|`MiiRXPkgCounterError`|`0x10`|
|`MiiRXDataCounterError`|`0x11`|


###MiiAlarmGlb

* **Description**           

This is status of packet diagnostic


* **RTL Instant Name**    : `MiiAlarmGlb`

* **Address**             : `0x00`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:7]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[6]`|`rx_data_err`| rx data packet error Indicates current status of the rx data Set 1 while error state change event happens when rxdata error<br>{1} : Error <br>{0} : OK| `R1W`| `0x0`| `0x0`|
|`[5:2]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[1]`|`rx_protocol_error`| rx_protocol_error  Indicates current status of the rx_protocol_error Set 1 while rx_protocol_error state change event happens when rx_protocol_error<br>{1} : protocol_error <br>{0} : protocol_OK| `R1W`| `0x0`| `0x0`|
|`[0]`|`link_sta`| PHY Link up status Indicates current status of the link Set 1 while link down state change event happens when 10G link down<br>{1} : Link Down <br>{0} : Link Up| `R1W`| `0x0`| `0x0`|

###MiiStatusGlb

* **Description**           

This is status of packet diagnostic


* **RTL Instant Name**    : `MiiStatusGlb`

* **Address**             : `0x1`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[6]`|`rx_data_err`| rx data packet error<br>{1} : Error <br>{0} : OK| `RO`| `0x0`| `0x0`|
|`[5:2]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[1]`|`rx_protocol_error`| rx_protocol_error<br>{1} : protocol_error <br>{0} : protocol_OK| `RO`| `0x0`| `0x0`|
|`[0]`|`link_sta`| PHY Link up status Indicates current status of the link Set 1 while link down state change event happens when 10G link down<br>{0} : Link Down <br>{1} : Link Up| `RO`| `0x0`| `0x0`|

###MiiEthTestControl

* **Description**           

Configuration Diagnostic,


* **RTL Instant Name**    : `MiiEthTestControl`

* **Address**             : `0x02`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:03]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[13]`|`iinsert_crc`| PRBS test iinsert_crc for tx side<br>{0} : Disable <br>{1} : Enable| `R/W`| `0x0`| `0x0`|
|`[8]`|`iclear_count`| PRBS test clear all count write 0 -> 1-> 0 for clear all count diagnostic<br>{0} : Disable <br>{1} : Enable| `R/W`| `0x0`| `0x0`|
|`[4]`|`irestart_diag`| PRBS test restart write 0 -> 1-> 0 for restart diagnostic<br>{0} : Disable <br>{1} : Enable| `R/W`| `0x0`| `0x0`|
|`[2]`|`start_diag_auto`| Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration| `RW`| `0x0`| `0x0`|
|`[0]`|`test_en`| PRBS test Enable<br>{0} : Disable <br>{1} : Enable| `R/W`| `0x0`| `0x0`|

###gatetime config value

* **Description**           

This is Mate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 4


* **RTL Instant Name**    : `gatetime_cfg`

* **Address**             : `0x03`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16:00]`|`time_cfg`| Gatetime Configuration 1-86400 second| `RW`| `0x0`| `0x0`|

###Gatetime Current

* **Description**           




* **RTL Instant Name**    : `Gatetime_current`

* **Address**             : `0x04`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17]`|`status_gatetime_diag`| Status Gatetime diagnostic port 16 to port1 bit per port 1:Running 0:Done-Ready| `RO`| `0x0`| `0x0`|
|`[16:0]`|`currert_gatetime_diag`| Current running time of Gatetime diagnostic| `RO`| `0x0`| `0x0 Begin:`|

###MiiTxPkgCounter [31:0].

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `itxpklow`

* **Address**             : `0x08`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tx_counterl`| Tx Packet counter[31:0]| `R0`| `0x0`| `0x0 End : Begin:`|

###MiiTxPkgCounter [47:32].

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `itxpkhigh`

* **Address**             : `0x09`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tx_counterh`| Tx Packet counter[47:32]| `R0`| `0x0`| `0x0 End : Begin:`|

###MiiTxByteCounter [31:0].

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `itxbyteclow`

* **Address**             : `0x0A`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tx_byte_counterl`| Tx Byte counter[31:0]| `R0`| `0x0`| `0x0 End : Begin:`|

###MiiTxByteCounter [63:32].

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `itxbytehigh`

* **Address**             : `0x0B`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`tx_byte_counterh`| Tx Byte counter[63:32]| `R0`| `0x0`| `0x0 End : Begin:`|

###MiiRXPkgCounter [31:0].

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `iRXpklow`

* **Address**             : `0x0C`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_counterl`| RX Packet counter[31:0]| `R0`| `0x0`| `0x0 End : Begin:`|

###MiiRXPkgCounter [47:32].

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `iRXpkhigh`

* **Address**             : `0x0D`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_counterh`| RX Packet counter[47:32]| `R0`| `0x0`| `0x0 End : Begin:`|

###MiiRXByteCounter [31:0].

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `iRXbyteclow`

* **Address**             : `0x0E`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_byte_counterl`| RX Byte counter[31:0]| `R0`| `0x0`| `0x0 End : Begin:`|

###MiiRXbyteCounter [63:32].

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `iRXbytehigh`

* **Address**             : `0x0F`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_byte_counterh`| RX Byte counter[63:32]| `R0`| `0x0`| `0x0 End : Begin:`|

###MiiRXPkgCounterError

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `iRX_Pkg_Error`

* **Address**             : `0x10`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_pkg_cnterror`| RX Pkg Error counter| `R0`| `0x0`| `0x0 End : Begin:`|

###MiiRXDataCounterError

* **Description**           

Register to test MII interface


* **RTL Instant Name**    : `iRX_Data_Error`

* **Address**             : `0x11`

* **Formula**             : *n/a*

* **Where**               : *n/a*

* **Width**               : `32`
* **Register Type**       : `Counter`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`rx_data_cnterror`| RX Data Error counter| `R0`| `0x0`| `0x0 End :`|

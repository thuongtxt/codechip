## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-10-01|AF6Project|Initial version|




##AF6CNC0021_XFI_RD_DIAG
####Register Table

|Name|Address|
|-----|-----|
|`XFI PRBS Pattern Generate Configure`|`0x0`|
|`XFI PRBS Pattern Packet Number Configure`|`0x1`|
|`XFI PRBS Pattern Packet lengthg Configure`|`0x2`|
|`XFI PRBS Pattern Packet Data Configure`|`0x3`|
|`XFI PRBS Diagnostic Gatetime Configuration`|`0x8`|
|`XFI PRBS Diagnostic Gatetime Current`|`0x9`|
|`XFI PRBS Pattern Packet generate Counter RO`|`0x10`|
|`XFI PRBS Pattern Packet generate Counter R2C`|`0x20`|
|`XFI PRBS Pattern Packet analyzer PRBS Sticky Error`|`0x700`|
|`XFI PRBS Pattern Packet analyzer PRBS Status Error`|`0x701`|
|`XFI PRBS Pattern Packet analyzer Data Configure`|`0x601`|
|`XFI PRBS Packet analyzer Counter pkttotal`|`0x400`|
|`XFI PRBS Packet analyzer Counter pktgood`|`0x401`|
|`XFI PRBS Packet analyzer Counter pktfcserr`|`0x402`|
|`XFI PRBS Packet analyzer Counter pkt length err`|`0x403`|
|`XFI PRBS Packet analyzer Counter pkt PRBS Data err`|`0x407`|
|`XFI PRBS Packet analyzer Counter pkt length 0-64`|`0x40A`|
|`XFI PRBS Packet analyzer Counter pkt length 65-128`|`0x40B`|
|`XFI PRBS Packet analyzer Counter pkt length 129-256`|`0x40C`|
|`XFI PRBS Packet analyzer Counter pkt length 257-512`|`0x40D`|
|`XFI PRBS Packet analyzer Counter pkt length 513-1024`|`0x40E`|
|`XFI PRBS Packet analyzer Counter pkt length 1025-2048`|`0x40F`|
|`XFI PRBS Packet analyzer Counter pkttotal`|`0x500`|
|`XFI PRBS Packet analyzer Counter pktgood`|`0x501`|
|`XFI PRBS Packet analyzer Counter pktfcserr`|`0x502`|
|`XFI PRBS Packet analyzer Counter pkt length err`|`0x503`|
|`XFI PRBS Packet analyzer Counter pkt PRBS Data err`|`0x507`|
|`XFI PRBS Packet analyzer Counter pkt length 0-64`|`0x50A`|
|`XFI PRBS Packet analyzer Counter pkt length 65-128`|`0x50B`|
|`XFI PRBS Packet analyzer Counter pkt length 129-256`|`0x50C`|
|`XFI PRBS Packet analyzer Counter pkt length 257-512`|`0x50D`|
|`XFI PRBS Packet analyzer Counter pkt length 513-1024`|`0x50E`|
|`XFI PRBS Packet analyzer Counter pkt length 1025-2048`|`0x50F`|


###XFI PRBS Pattern Generate Configure

* **Description**           

XFI PRBS Pattern Generate Configure


* **RTL Instant Name**    : `Configure_register_1`

* **Address**             : `0x0`

* **Formula**             : `0x0`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`cfgdainc`| Configure DA Address mode 1 : INC / 0: fixed| `R/W`| `0x0`| `0x0`|
|`[1]`|`cfglengthinc`| Configure length mode 1 : INC / 0: fixed| `R/W`| `0x0`| `0x0`|
|`[2]`|`cfgnumall`| Configure Packet number mode 1 : continuity   / 0: fixed| `R/W`| `0x0`| `0x1`|
|`[3]`|`cfgins_err`| Configure Packet Insert Error 1 : Insert / 0: Un-insert| `R/W`| `0x0`| `0x0`|
|`[4]`|`cfgforcsop`| Configure Force Start of packet 1 : Force / 0: Un-Force| `R/W`| `0x0`| `0x0`|
|`[5]`|`cfgforceop`| Configure Force End of packet 1 : Force / 0: Un-Force| `R/W`| `0x0`| `0x0`|
|`[15:6]`|`reserved`| *n/a*| `R/W`| `0x0`| `0x0`|
|`[16]`|`cfggenen`| Configure PRBS packet test enable 1 : Enable / 0: Disnable| `R/W`| `0x0`| `0x0`|
|`[17]`|`flush`| Configure flush PRBS packet gen  1 : flush / 0: Un-flush| `R/W`| `0x0`| `0x0`|

###XFI PRBS Pattern Packet Number Configure

* **Description**           

XFI PRBS Pattern Packet Number Configure


* **RTL Instant Name**    : `Configure_register_2`

* **Address**             : `0x1`

* **Formula**             : `0x1`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`cfgnumpkt`| Configure number of packet generate| `R/W`| `0x14`| `0x14`|

###XFI PRBS Pattern Packet lengthg Configure

* **Description**           

XFI PRBS Pattern Packet lengthg Configure


* **RTL Instant Name**    : `Configure_register_3`

* **Address**             : `0x2`

* **Formula**             : `0x2`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:0]`|`cfglengthmin`| Configure length minimum of packet generate| `R/W`| `0x40`| `0x40`|
|`[31:16]`|`cfglengthmax`| Configure length maximum of packet generate| `R/W`| `0x5DC`| `0x5DC`|

###XFI PRBS Pattern Packet Data Configure

* **Description**           

XFI PRBS Pattern Packet Data Configure


* **RTL Instant Name**    : `Configure_register_4`

* **Address**             : `0x3`

* **Formula**             : `0x3`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[7:0]`|`cfgdatfix`| Configure Fixed byte Data generate| `R/W`| `0x84`| `0x84`|
|`[11:8]`|`cfgdatmod`| Configure data mode generate 0x0 : Unused 0x1 : Fixed Data 0x2 : PRBS31 data 0x3 : PRBS15 data 0x4 : PRBS7  data| `R/W`| `0x2`| `0x2`|

###XFI PRBS Diagnostic Gatetime Configuration

* **Description**           

XFI PRBS Diagnostic Gatetime Configuration


* **RTL Instant Name**    : `icfgtime`

* **Address**             : `0x8`

* **Formula**             : `0x8`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control and Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[25]`|`status_gatetime_diag`| Status Gatetime diagnostic 1:Running 0:Done-Ready| `RO`| `0x0`| `0x0`|
|`[24]`|`start_gatetime_diag`| Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration| `RW`| `0x0`| `0x0`|
|`[23:17]`|`unsed`| Unsed| `R/W`| `0x0`| `0x0`|
|`[16:0]`|`time_cfg`| Gatetime Configuration 1-86400 second| `RW`| `0x0`| `0x0`|

###XFI PRBS Diagnostic Gatetime Current

* **Description**           

XFI PRBS Diagnostic Gatetime_Status


* **RTL Instant Name**    : `istatuscfgtime`

* **Address**             : `0x9`

* **Formula**             : `0x9`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:17]`|`unsed`| Unsed| `R/W`| `0x0`| `0x0`|
|`[16:0]`|`currert_gatetime_diag`| Current running time of Gatetime diagnostic| `RO`| `0x0`| `0x0 End :`|

###XFI PRBS Pattern Packet generate Counter RO

* **Description**           

XFI PRBS Pattern Packet generate Counter RO


* **RTL Instant Name**    : `Status_register_1`

* **Address**             : `0x10`

* **Formula**             : `0x10`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`penr_opkttotal`| TX Packet Counterer| `RO`| `0x0`| `0x0`|

###XFI PRBS Pattern Packet generate Counter R2C

* **Description**           

XFI PRBS Pattern Packet generate Counter R2C


* **RTL Instant Name**    : `Status_register_2`

* **Address**             : `0x20`

* **Formula**             : `0x20`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`penr2cpkttotal`| TX Packet Counterer| `R2C`| `0x0`| `0x0`|

###XFI PRBS Pattern Packet analyzer PRBS Sticky Error

* **Description**           

XFI PRBS Pattern Packet analyzer PRBS Sticky Error


* **RTL Instant Name**    : `Configure_register_5`

* **Address**             : `0x700`

* **Formula**             : `0x700`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`err_stk`| Packet analyzer PRBS Sticky Error <0 = RX is synchronized /1 = RX is not synchronized>| `R/W/C`| `0x0`| `0x0`|

###XFI PRBS Pattern Packet analyzer PRBS Status Error

* **Description**           

XFI PRBS Pattern Packet analyzer PRBS Status Error


* **RTL Instant Name**    : `Configure_register_6`

* **Address**             : `0x701`

* **Formula**             : `0x701`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[0]`|`err_sta`| Packet analyzer PRBS Status Error <0 = RX is synchronized /1 = RX is not synchronized>| `RO`| `0x0`| `0x0`|

###XFI PRBS Pattern Packet analyzer Data Configure

* **Description**           

XFI PRBS Pattern Packet analyzer Data Configure


* **RTL Instant Name**    : `Configure_register_7`

* **Address**             : `0x601`

* **Formula**             : `0x601`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[28]`|`cfgfcsdrop`| Configure FCS Drop| `R/W`| `0x0`| `0x1`|
|`[11:8]`|`cfgdatmod`| Configure data mode generate 0x0 : Unused 0x1 : Fixed Data 0x2 : PRBS31 data 0x3 : PRBS15 data 0x4 : PRBS7  data| `R/W`| `0x2`| `0x2`|

###XFI PRBS Packet analyzer Counter pkttotal

* **Description**           

XFI PRBS Packet analyzer Counter pkttotal


* **RTL Instant Name**    : `Status_register_3`

* **Address**             : `0x400`

* **Formula**             : `0x400`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pkttotal`| RX Packet total| `RO`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pktgood

* **Description**           

XFI PRBS Packet analyzer Counter pktgood


* **RTL Instant Name**    : `Status_register_4`

* **Address**             : `0x401`

* **Formula**             : `0x401`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktgood`| RX Packet good| `RO`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pktfcserr

* **Description**           

XFI PRBS Packet analyzer Counter pktfcserr


* **RTL Instant Name**    : `Status_register_5`

* **Address**             : `0x402`

* **Formula**             : `0x402`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktfcserr`| RX Packet FCS error| `RO`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length err

* **Description**           

XFI PRBS Packet analyzer Counter pkt length err


* **RTL Instant Name**    : `Status_register_6`

* **Address**             : `0x403`

* **Formula**             : `0x403`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlengtherr`| RX Packet lengthgth error| `RO`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt PRBS Data err

* **Description**           

XFI PRBS Packet analyzer Counter pkt PRBS Data err


* **RTL Instant Name**    : `Status_register_7`

* **Address**             : `0x407`

* **Formula**             : `0x407`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktdaterr`| RX Packet PRBS data error| `RO`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 0-64

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 0-64


* **RTL Instant Name**    : `Status_register_8`

* **Address**             : `0x40A`

* **Formula**             : `0x40A`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen64`| RX Counter pkt length 0-64| `RO`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 65-128

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 65-128


* **RTL Instant Name**    : `Status_register_9`

* **Address**             : `0x40B`

* **Formula**             : `0x40B`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen128`| RX Counter pkt length 65-128| `RO`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 129-256

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 129-256


* **RTL Instant Name**    : `Status_register_10`

* **Address**             : `0x40C`

* **Formula**             : `0x40C`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen256`| RX Counter pkt length 129-256| `RO`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 257-512

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 257-512


* **RTL Instant Name**    : `Status_register_11`

* **Address**             : `0x40D`

* **Formula**             : `0x40D`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen512`| RX Counter pkt length 257-215| `RO`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 513-1024

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 513-1024


* **RTL Instant Name**    : `Status_register_12`

* **Address**             : `0x40E`

* **Formula**             : `0x40E`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen1024`| RX Counter pkt length 513-1024| `RO`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 1025-2048

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 1025-2048


* **RTL Instant Name**    : `Status_register_13`

* **Address**             : `0x40F`

* **Formula**             : `0x40F`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen2048`| RX Counter pkt length 1025-2048| `RO`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkttotal

* **Description**           

XFI PRBS Packet analyzer Counter pkttotal


* **RTL Instant Name**    : `Status_register_14`

* **Address**             : `0x500`

* **Formula**             : `0x500`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pkttotal`| RX Packet total| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pktgood

* **Description**           

XFI PRBS Packet analyzer Counter pktgood


* **RTL Instant Name**    : `Status_register_15`

* **Address**             : `0x501`

* **Formula**             : `0x501`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktgood`| RX Packet good| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pktfcserr

* **Description**           

XFI PRBS Packet analyzer Counter pktfcserr


* **RTL Instant Name**    : `Status_register_16`

* **Address**             : `0x502`

* **Formula**             : `0x502`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktfcserr`| RX Packet FCS error| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length err

* **Description**           

XFI PRBS Packet analyzer Counter pkt length err


* **RTL Instant Name**    : `Status_register_17`

* **Address**             : `0x503`

* **Formula**             : `0x503`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlengtherr`| RX Packet lengthgth error| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt PRBS Data err

* **Description**           

XFI PRBS Packet analyzer Counter pkt PRBS Data err


* **RTL Instant Name**    : `Status_register_18`

* **Address**             : `0x507`

* **Formula**             : `0x507`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktdaterr`| RX Packet PRBS data error| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 0-64

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 0-64


* **RTL Instant Name**    : `Status_register_19`

* **Address**             : `0x50A`

* **Formula**             : `0x50A`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen64`| RX Counter pkt length 0-64| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 65-128

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 65-128


* **RTL Instant Name**    : `Status_register_20`

* **Address**             : `0x50B`

* **Formula**             : `0x50B`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen128`| RX Counter pkt length 65-128| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 129-256

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 129-256


* **RTL Instant Name**    : `Status_register_21`

* **Address**             : `0x50C`

* **Formula**             : `0x50C`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen256`| RX Counter pkt length 129-256| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 257-512

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 257-512


* **RTL Instant Name**    : `Status_register_22`

* **Address**             : `0x50D`

* **Formula**             : `0x50D`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen512`| RX Counter pkt length 257-215| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 513-1024

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 513-1024


* **RTL Instant Name**    : `Status_register_23`

* **Address**             : `0x50E`

* **Formula**             : `0x50E`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen1024`| RX Counter pkt length 513-1024| `R2C`| `0x0`| `0x0`|

###XFI PRBS Packet analyzer Counter pkt length 1025-2048

* **Description**           

XFI PRBS Packet analyzer Counter pkt length 1025-2048


* **RTL Instant Name**    : `Status_register_24`

* **Address**             : `0x50F`

* **Formula**             : `0x50F`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Control`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:0]`|`pktlen2048`| RX Counter pkt length 1025-2048| `R2C`| `0x0`| `0x0`|

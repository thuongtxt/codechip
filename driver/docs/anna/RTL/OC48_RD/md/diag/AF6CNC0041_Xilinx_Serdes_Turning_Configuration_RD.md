## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-10-01|AF6Project|Initial version|




##AF6CNC0041_Xilinx_Serdes_Turning_Configuration_RD
####Register Table

|Name|Address|
|-----|-----|
|`SERDES DRP PORT`|`0x1000-0x7FFF`|
|`SERDES LoopBack`|`0x0002-0x6002`|
|`SERDES POWER DOWN`|`0x0003-0x6003`|
|`SERDES PLL Status`|`0x000B-0x600B`|
|`SERDES TX Reset`|`0x000C-0x600C`|
|`SERDES RX Reset`|`0x000D-0x600D`|
|`SERDES LPMDFE Mode`|`0x000E-0x600E`|
|`SERDES LPMDFE Reset`|`0x000F-0x600F`|
|`SERDES TXDIFFCTRL`|`0x0010-0x6010`|
|`SERDES TXPOSTCURSOR`|`0x0011-0x6011`|
|`SERDES TXPRECURSOR`|`0x0012-0x6012`|
|`SERDES PRBS generator test pattern control`|`0x0020-0x6020`|
|`SERDES prbsctr_pen`|`0x0021-0x6021`|
|`SERDES PRBS Diag Status`|`0x0022-0x6022`|
|`SERDES PRBS RX_PRBS_ERR_CNT RO`|`0x23 - 0x26`|
|`SERDES PRBS RX_PRBS_ERR_CNT R2C`|`0x33 - 0x36`|
|`Mate serdeses Gatetime for PRBS Raw Diagnostic`|`0x37`|
|`gatetime config value`|`0x38 - 0x3b`|
|`Gatetime Current`|`0x3c - 0x3f`|


###SERDES DRP PORT

* **Description**           

Read/Write DRP address of SERDES, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_DRP_PORT`

* **Address**             : `0x1000-0x7FFF`

* **Formula**             : `0x1000+$G*0x2000+$P*0x400+$DRP`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

    * `$P(0-1) : Sub Port ID`

    * `$DRP(0-1023) : DRP address, see UG578`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:10]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[09:00]`|`drp_rw`| DRP read/write value| `R/W`| `0x0`| `0x0`|

###SERDES LoopBack

* **Description**           

Configurate LoopBack, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_LoopBack`

* **Address**             : `0x0002-0x6002`

* **Formula**             : `0x0002+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`lpback_subport3`| Loopback subport 3<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[11:08]`|`lpback_subport2`| Loopback subport 2<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[07:04]`|`lpback_subport1`| Loopback subport 1<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`lpback_subport0`| Loopback subport 0<br>{0} : Nomoral <br>{2} : Loop-In <br>{4} : Loop-Out| `R/W`| `0x0`| `0x0`|

###SERDES POWER DOWN

* **Description**           

Configurate Power Down , there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_POWER_DOWN`

* **Address**             : `0x0003-0x6003`

* **Formula**             : `0x0003+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[15:14]`|`txpd3`| Power Down subport 3<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|
|`[13:12]`|`txpd2`| Power Down subport 2<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|
|`[11:10]`|`txpd1`| Power Down subport 1<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|
|`[09:08]`|`txpd0`| Power Down subport 0<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|
|`[07:06]`|`rxpd3`| Power Down subport 3<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|
|`[05:04]`|`rxpd2`| Power Down subport 2<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|
|`[03:02]`|`rxpd1`| Power Down subport 1<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|
|`[01:00]`|`rxpd0`| Power Down subport 0<br>{0} : Nomoral <br>{1/2}: Unused <br>{3} : Power-down mode| `R/W`| `0x3`| `0x3`|

###SERDES PLL Status

* **Description**           

QPLL/CPLL status, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_PLL_Status`

* **Address**             : `0x000B-0x600B`

* **Formula**             : `0x000B+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:30]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[29:29]`|`qpll1_lock_change`| QPLL1 has transition lock/unlock, Group 0-1<br>{1} : QPLL1_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[28:28]`|`qpll0_lock_change`| QPLL0 has transition lock/unlock, Group 0-1<br>{1} : QPLL0_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[27:26]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[25:25]`|`qpll1_lock`| QPLL0 is Locked, Group 0-1<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[24:24]`|`qpll0_lock`| QPLL0 is Locked, Group 0-1<br>{1} : Locked| `R_O`| `0x0`| `0x0`|
|`[23:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`cpll_lock_change`| CPLL has transition lock/unlock, bit per sub port,<br>{1} : CPLL_Lock has changed| `W1C`| `0x0`| `0x0`|
|`[11:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`cpll_lock`| CPLL is Locked, bit per sub port,<br>{1} : Locked| `R_O`| `0x0`| `0x0`|

###SERDES TX Reset

* **Description**           

Reset TX SERDES, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_TX_Reset`

* **Address**             : `0x000C-0x600C`

* **Formula**             : `0x000C+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:16]`|`txrst_done`| TX Reset Done, bit per sub port<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`txrst_trig`| Should reset TX_PMA SERDES about 300-500 ns , bit per sub port<br>{1} : Reset <br>{0} : Un-Reset| `R/W`| `0x0`| `0x0`|

###SERDES RX Reset

* **Description**           

Reset RX SERDES, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_RX_Reset`

* **Address**             : `0x000D-0x600D`

* **Formula**             : `0x000D+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:16]`|`rxrst_done`| RX Reset Done, bit per sub port<br>{1} : reset done| `W1C`| `0x0`| `0x0`|
|`[15:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`rxrst_trig`| Should reset reset RX_PMA SERDES about 300-500 ns , bit per sub port<br>{1} : Reset <br>{0} : Un-Reset| `R/W`| `0x0`| `0x0`|

###SERDES LPMDFE Mode

* **Description**           

Configure LPM/DFE mode , there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_LPMDFE_Mode`

* **Address**             : `0x000E-0x600E`

* **Formula**             : `0x000E+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`lpmdfe_mode`| bit per sub port<br>{0} : DFE mode <br>{1} : LPM mode| `R/W`| `0x0`| `0x0`|

###SERDES LPMDFE Reset

* **Description**           

Reset LPM/DFE , there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_LPMDFE_Reset`

* **Address**             : `0x000F-0x600F`

* **Formula**             : `0x000F+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:04]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[03:00]`|`lpmdfe_reset`| bit per sub port, Must be toggled after switching between modes to initialize adaptation<br>{1} : reset| `R/W`| `0x0`| `0x0`|

###SERDES TXDIFFCTRL

* **Description**           

Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7


* **RTL Instant Name**    : `SERDES_TXDIFFCTRL`

* **Address**             : `0x0010-0x6010`

* **Formula**             : `0x0010+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:15]`|`txdiffctrl_subport3`| | `R/W`| `0x18`| `0x0`|
|`[14:10]`|`txdiffctrl_subport2`| | `R/W`| `0x18`| `0x0`|
|`[09:05]`|`txdiffctrl_subport1`| | `R/W`| `0x18`| `0x0`|
|`[04:00]`|`txdiffctrl_subport0`| | `R/W`| `0x18`| `0x0`|

###SERDES TXPOSTCURSOR

* **Description**           

Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports


* **RTL Instant Name**    : `SERDES_TXPOSTCURSOR`

* **Address**             : `0x0011-0x6011`

* **Formula**             : `0x0011+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:15]`|`txpostcursor_subport3`| | `R/W`| `0x0`| `0x0`|
|`[14:10]`|`txpostcursor_subport2`| | `R/W`| `0x0`| `0x0`|
|`[09:05]`|`txpostcursor_subport1`| | `R/W`| `0x0`| `0x0`|
|`[04:00]`|`txpostcursor_subport0`| | `R/W`| `0x0`| `0x0`|

###SERDES TXPRECURSOR

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports


* **RTL Instant Name**    : `SERDES_TXPRECURSOR`

* **Address**             : `0x0012-0x6012`

* **Formula**             : `0x0012+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[19:15]`|`txprecursor_subport3`| | `R/W`| `0x0`| `0x0`|
|`[14:10]`|`txprecursor_subport2`| | `R/W`| `0x0`| `0x0`|
|`[09:05]`|`txprecursor_subport1`| | `R/W`| `0x0`| `0x0`|
|`[04:00]`|`txprecursor_subport0`| | `R/W`| `0x0`| `0x0`|

###SERDES PRBS generator test pattern control

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports


* **RTL Instant Name**    : `prbsctr_pen`

* **Address**             : `0x0020-0x6020`

* **Formula**             : `0x0020+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`prbssel_subport3`| Transmitter PRBS generator and Receiver PRBS checker test pattern control.After changing patterns, perform a reset of the RX (GTRXRESET, RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the RX pattern checker can attempt to reestablish the link acquired. No checking is done for non-PRBS patterns.<br>{0}: Standard operation mode.(PRBS generator/check is off) <br>{1}: PRBS-7 <br>{2}: PRBS-9 <br>{3}: PRBS-15 <br>{4}: PRBS-23 <br>{5}: PRBS-31| `R/W`| `0x0`| `0x0`|
|`[11:08]`|`prbssel_subport2`| Transmitter PRBS generator and Receiver PRBS checker test pattern control.After changing patterns, perform a reset of the RX (GTRXRESET, RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the RX pattern checker can attempt to reestablish the link acquired. No checking is done for non-PRBS patterns.<br>{0}: Standard operation mode.(PRBS generator/check is off) <br>{1}: PRBS-7 <br>{2}: PRBS-9 <br>{3}: PRBS-15 <br>{4}: PRBS-23 <br>{5}: PRBS-31| `R/W`| `0x0`| `0x0`|
|`[07:04]`|`prbssel_subport1`| Transmitter PRBS generator and Receiver PRBS checker test pattern control.After changing patterns, perform a reset of the RX (GTRXRESET, RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the RX pattern checker can attempt to reestablish the link acquired. No checking is done for non-PRBS patterns.<br>{0}: Standard operation mode.(PRBS generator/check is off) <br>{1}: PRBS-7 <br>{2}: PRBS-9 <br>{3}: PRBS-15 <br>{4}: PRBS-23 <br>{5}: PRBS-31| `R/W`| `0x0`| `0x0`|
|`[03:00]`|`prbssel_subport0`| Transmitter PRBS generator and Receiver PRBS checker test pattern control.After changing patterns, perform a reset of the RX (GTRXRESET, RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the RX pattern checker can attempt to reestablish the link acquired. No checking is done for non-PRBS patterns.<br>{0}: Standard operation mode.(PRBS generator/check is off) <br>{1}: PRBS-7 <br>{2}: PRBS-9 <br>{3}: PRBS-15 <br>{4}: PRBS-23 <br>{5}: PRBS-31| `R/W`| `0x0`| `0x0`|

###SERDES prbsctr_pen

* **Description**           

Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports


* **RTL Instant Name**    : `SERDES_prbsctr_pen`

* **Address**             : `0x0021-0x6021`

* **Formula**             : `0x0021+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:20]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[07]`|`rxprbscntreset_subport3`| Resets the PRBS error counter.| `R/W`| `0x0`| `0x0`|
|`[06]`|`rxprbscntreset_subport2`| Resets the PRBS error counter.| `R/W`| `0x0`| `0x0`|
|`[05]`|`rxprbscntreset_subport1`| Resets the PRBS error counter.| `R/W`| `0x0`| `0x0`|
|`[04]`|`rxprbscntreset_subport0`| Resets the PRBS error counter.| `R/W`| `0x0`| `0x0`|
|`[03]`|`txprbsforceerr_subport3`| When this port is driven High, errors are forced in the PRBS transmitter. While this port is asserted, the output data pattern contains errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA| `R/W`| `0x0`| `0x0`|
|`[02]`|`txprbsforceerr_subport2`| When this port is driven High, errors are forced in the PRBS transmitter. While this port is asserted, the output data pattern contains errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA| `R/W`| `0x0`| `0x0`|
|`[01]`|`txprbsforceerr_subport1`| When this port is driven High, errors are forced in the PRBS transmitter. While this port is asserted, the output data pattern contains errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA| `R/W`| `0x0`| `0x0`|
|`[00]`|`txprbsforceerr_subport0`| When this port is driven High, errors are forced in the PRBS transmitter. While this port is asserted, the output data pattern contains errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA| `R/W`| `0x0`| `0x0`|

###SERDES PRBS Diag Status

* **Description**           

QPLL/CPLL status, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,


* **RTL Instant Name**    : `SERDES_PRBS_Status`

* **Address**             : `0x0022-0x6022`

* **Formula**             : `0x0022+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:16]`|`unused`| *n/a*| *n/a*| *n/a*| *n/a*|
|`[15:12]`|`rxprbslocked`| Output to indicate that the RX PRBS checker has been error free after reset. Once asserted High, RXPRBSLOCKED does not deassert until reset of the RX pattern checker via a reset of the RX (GTRXRESET, RXPMARESET, or RXPCSRESET in sequential mode) or a reset of the PRBS error counter (RXPRBSCNTRESET), bit per sub port,<br>{1} : RX PRBS is LOCKED| `R_O`| `0x0`| `0x0`|
|`[11:08]`|`rxprbserr`| This non-sticky status output indicates that PRBS errors have occurred. , bit per sub port,<br>{1} : RX PRBS is Error| `R_O`| `0x0`| `0x0`|
|`[07:04]`|`rxprbslocked_change`| This sticky status output indicates that PRBS not locked, bit per sub port,<br>{1} : RX PRBS not locked has rate| `W1C`| `0x0`| `0x0`|
|`[03:00]`|`rxprbserr_change`| This sticky status output indicates that PRBS errors have occurred. , bit per sub port,<br>{1} : PRBS errors has rate| `W1C`| `0x0`| `0x0`|

###SERDES PRBS RX_PRBS_ERR_CNT RO

* **Description**           

This is counter RX PRBS error RO for Port 1-4 ( 0x23 :Port 1 ....0x26 Port 4)


* **RTL Instant Name**    : `SERDES_PRBS_RX_PRBS_ERR_CNT_RO`

* **Address**             : `0x23 - 0x26`

* **Formula**             : `0x23+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rx_prbs_err_cnt_ro`| PRBS error counter.| `RO`| `0x0`| `0x0`|

###SERDES PRBS RX_PRBS_ERR_CNT R2C

* **Description**           

This is counter RX PRBS error R2C for Port 1-4 ( 0x23 :Port 1 ....0x26 Port 4)


* **RTL Instant Name**    : `SERDES_PRBS_RX_PRBS_ERR_CNT_R2C`

* **Address**             : `0x33 - 0x36`

* **Formula**             : `0x33+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`rx_prbs_err_cnt_r2c`| PRBS error counter.| `R2C`| `0x0`| `0x0 Begin:`|

###Mate serdeses Gatetime for PRBS Raw Diagnostic

* **Description**           

This is Mate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 4


* **RTL Instant Name**    : `gatetime_ctr`

* **Address**             : `0x37`

* **Formula**             : `0x37+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:18]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[0]`|`start_diag4`| Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration| `RW`| `0x0`| `0x0`|
|`[0]`|`start_diag3`| Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration| `RW`| `0x0`| `0x0`|
|`[0]`|`start_diag2`| Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration| `RW`| `0x0`| `0x0`|
|`[0]`|`start_diag1`| Config start Diagnostic trigger 0 to 1 for Start auto run with Gatetime Configuration| `RW`| `0x0`| `0x0`|

###gatetime config value

* **Description**           

This is Mate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 4


* **RTL Instant Name**    : `gatetime_cfg`

* **Address**             : `0x38 - 0x3b`

* **Formula**             : `0x38+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[16:00]`|`time_cfg`| Gatetime Configuration 1-86400 second| `RW`| `0x0`| `0x0`|

###Gatetime Current

* **Description**           




* **RTL Instant Name**    : `Gatetime_current`

* **Address**             : `0x3c - 0x3f`

* **Formula**             : `0x3c+$G*0x2000`

* **Where**               : 

    * `$G(0-1) : Group of Ports`

* **Width**               : `32`
* **Register Type**       : `Configure`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[17]`|`status_gatetime_diag`| Status Gatetime diagnostic port 16 to port1 bit per port 1:Running 0:Done-Ready| `RO`| `0x0`| `0x0`|
|`[16:0]`|`currert_gatetime_diag`| Current running time of Gatetime diagnostic| `RO`| `0x0`| `0x0`|

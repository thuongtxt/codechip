## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-10-01|AF6Project|Initial version|




##AF6CNC0021_Interrupt_Diag
####Register Table

|Name|Address|
|-----|-----|
|`Configure Raise mode for Interrupt Pin`|`0x0`|
|`Enable Diagnostic Interrupt`|`0x1`|
|`Status of Diagnostic Interrupt`|`0x2`|


###Configure Raise mode for Interrupt Pin

* **Description**           

This is Configure Raise mode for Interrupt Pin  1- 2


* **RTL Instant Name**    : `pcfg_mode`

* **Address**             : `0x0`

* **Formula**             : `Base_0x0 + 0x0`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[01]`|`out_mode_1_`| Diagnostic Mode for Interrupt 2 1: Interrupt Level ( Raise when enable signal high ) 0: State change (Raise on pos-edge/neg-edge of config enable signal)| `RW`| `0x0`| `0x0`|
|`[00]`|`out_mode_0_`| Diagnostic Mode for Interrupt 1 1: Interrupt Level ( Raise when enable signal high ) 0: State change  (Raise on pos-edge/neg-edge of config enable signal)| `RW`| `0x0`| `0x0 End : Begin:`|

###Enable Diagnostic Interrupt

* **Description**           

This is Enable Diagnostic Interrupt 1-2


* **RTL Instant Name**    : `pcfg_glb`

* **Address**             : `0x1`

* **Formula**             : `Base_0x1 + 0x1`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[01]`|`out_glb_1_`| Trige 0->1 to start Enable Diagnostic for Interrupt 2 0->1 : Active Interrupt 1->0 : De-active Interrupt| `RW`| `0x0`| `0x0`|
|`[00]`|`out_glb_0_`| Trige 0->1 to start Diagnostic for Interrupt 1 0->1 : Active Interrupt 1->0 : De-active Interrupt| `RW`| `0x0`| `0x0 End : Begin:`|

###Status of Diagnostic Interrupt

* **Description**           

This is Status of Diagnostic Interrupt 1-2


* **RTL Instant Name**    : `stickyx_int`

* **Address**             : `0x2`

* **Formula**             : `Base_0x2 + 0x2`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:02]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[01:00]`|`oint_out`| Interrupt has transition Enabled/ Disable , bit per sub port,<br>{1} : Interrupt Enabled| `W1C`| `0x0`| `0x0 End :`|

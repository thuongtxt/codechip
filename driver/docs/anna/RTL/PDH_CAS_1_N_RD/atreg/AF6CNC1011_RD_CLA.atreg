######################################################################################
# Arrive Technologies
# Register Description Document
# Revision 1.0 - 2015.Apr.06

######################################################################################
# This section is for guideline
# Refer to \\Hcmcserv3\Projects\internal_tools\atvn_vlibs\bin\rgm_gen\docs\REGISTER_DESCRIPTION_GUIDELINE.pdf
# File name: filename.atreg (atreg = ATVN register define)
# Comment out a line: Please use (# or //#) character at the beginning of the line
# Delimiter character: (%%)

######################################################################################
#| Access Policy| Use Case| Description                | Effect of a Write on Current Field Value  | Effect of a Read on Current Field Value | Read-back Value |
#| ------|-----------|---------------------------------| ------------------------------------------|-----------------------------------------|-----------------|
#| RO    | Status    | Read Only                       | No effect                | No effect            | Current Value |
#| RW    | Config    | Read, Write                     | Changed to written value | No effect            | Current value |
#| RC    | Status    | Read Clears All                 | No effect                | Sets all bits to 0   | Current value |
#| RS    | UNKNOW    | Read Sets All                   | No effect                | Sets all bits to 1   | Current value |
#| WRC   | UNKNOW    | Write, Read Clears All          | Changed to written value | Sets all bits to 0   | Current value |
#| WRS   | UNKNOW    | Write, Read Sets All            | Changed to written value | Sets all bits to 1   | Current value |
#| WC    | Status    | Write Clears All                | Sets all bits to 0       | No effect            | Current value |
#| WS    | Status    | Write Sets All                  | Sets all bits to 1       | No effect            | Current value |
#| WSRC  | UNKNOW    | Write Sets All, Read Clears All | Sets all bits to 1       | Sets all bits to 0   | Current value |
#| WCRS  | UNKNOW    | Write Clears All,Read Sets All  | Sets all bits to 0       | Sets all bits to 1   | Current value |
#| W1C   | Interrupt | Write 1 to Clear                | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current Value |
#| W1S   | Interrupt | Write 1 to Set                  | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W1T   | UNKNOW    | Write 1 to Toggle               | If the bit in the wr value is a 1, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W0C   | Interrupt | Write 0 to Clear                | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current value |
#| W0S   | Interrupt | Write 0 to Set                  | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W0T   | UNKNOW    | Write 0 to Toggle               | If the bit in the wr value is a 0, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W1SRC | UNKNOW    | Write 1 to Set, Read Clears All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W1CRS | UNKNOW    | Write 1 to Clear, Read Sets All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| W0SRC | UNKNOW    | Write 0 to Set, Read Clears All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W0CRS | UNKNOW    | Write 0 to Clear, Read Sets All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| WO    | Config    | Write Only                      | Changed to written value | No effect | Undefined |
#| WOC   | UNKNOW    | Write Only Clears All           | Sets all bits to 0     | No effect | Undefined |
#| WOS   | UNKNOW    | Write Only Sets All             | Sets all bits to 1     | No effect | Undefined |
#| W1    | UNKNOW    | Write Once                      | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Current value |
#| WO1   | UNKNOW    | Write Only, Once                | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Undefined |
#=============================================================================
# Sample
#=============================================================================

######################################################################################
#SAMPLE> // Begin:
#SAMPLE> //# Some description
#SAMPLE> // Register Full Name: Device Version ID
#SAMPLE> // RTL Instant Name: VersionID
#SAMPLE> //# {FunctionName,SubFunction_InstantName_Description}
#SAMPLE> //# RTL Instant Name and Full Name MUST be unique within a register description file
#SAMPLE> // Address: 0x00_0000
#SAMPLE> //# Address is relative address, will be sum with Base_Address for each function block
#SAMPLE> // Formula      : Address + $portid 
#SAMPLE> // Where        : {$portid(0-7): port identification} 
#SAMPLE> // Description  : This register indicates the module' name and version. 
#SAMPLE> // Width: 32
#SAMPLE> // Register Type: {Status}
#SAMPLE> //# Field: [Bit:Bit] %%  Name     %% Description            %% Type %% SW_Reset %% HW_Reset
#SAMPLE> // Field: [31:24]    %%  Year     %% 2013                   %% RO   %% 0x0D     %% 0x0D
#SAMPLE> // Field: [23:16]    %%  Week     %% Week Synthesized FPGA  %% RO   %% 0x0C     %% 0x0C
#SAMPLE> // Field: [15:8]     %%  Day      %% Day Synthesized FPGA   %% RO   %% 0x16     %% 0x16
#SAMPLE> // Field: [7:4]      %%  VerID    %% FPGA Version           %% RO   %% 0x0      %% 0x0
#SAMPLE> // Field: [3:0]      %%  NumID    %% Number of FPGA Version %% RO   %% 0x1      %% 0x1
#SAMPLE> // End:
######################################################################################
#********************* 
#4.1.1.Classify Global PSN Control 
#********************* 
// Begin: 
// Register Full Name: Classify Global PSN Control  
// RTL Instant Name  :glb_psn
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x00000000
// Description: This register controls operation modes of PSN interface.
// Width: 32
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31] %% SubPortVlanEn         %% Enable SubPort Vlan     %% RW %% 0x0 %% 0x0
// Field: [30]  %% RxPsnVlanTagModeEn %%
//      1: Enable (default) VLAN TAG mode which contain PWID
//      0:  Disable VLAN TAG mode, normal PSN operation     %% RW %% 0x0 %% 0x0
// Field: [29] %% RxSendLbit2CdrEn %% Enable to send Lbit to CDR engine
//      1: Enable to send Lbit
//      0: Disable %% RW %% 0x1 %% 0x0
// Field: [28] %% RxPsnMplsOutLabelCheckEn %% Enable to check MPLS outer
//      1: Enable checking
//      0: Disable checking %% RW %% 0x0 %% 0x0
// Field: [27] %% RxPsnCpuBfdCtlEn %% Enable VCCV BFD control packet sending to CPU for processing
//      1: Enable sending
//      0: Discard %% RW %% 0x0 %% 0x0
// Field: [26] %% RxMacCheckDis %% Disable to check MAC address at Ethernet port receive direction
//      1: Disable checking
//      0: Enable checking %% RW %% 0x0 %% 0x0
// Field: [25] %% RxPsnCpuIcmpEn %% Enable ICMP control packet sending to CPU for processing
//      1: Enable sending
//      0: Discard %% RW %% 0x0 %% 0x0
// Field: [24] %% RxPsnCpuArpEn %% Enable ARP control packet sending to CPU for processing
//      1: Enable sending
//      0: Discard %% RW %% 0x0 %% 0x0
// Field: [23] %% PweLoopClaEn %% Enable Loop back traffic from PW Encapsulation to Classification 
//      1: Enable Loop back mode
//      0: Normal, not loop back %% RW %% 0x0 %% 0x0
// Field: [22] %% RxPsnIpUdpMode %% This bit is applicable for Ipv4/Ipv6 packet from Ethernet side
//      1: Classify engine uses RxPsnIpUdpSel to decide which UDP port (Source or Destination) is used to identify pseudowire packet
//      0: Classify engine will automatically search for value 0x85E in source or destination UDP port. The remaining UDP port is used to identify pseudowire packet %% RW %% 0x0 %% 0x0
// Field: [21] %% RxPsnIpUdpSel %% This bit is applicable for Ipv4/Ipv6 using to select Source or Destination to identify pseudowire packet from Ethernet side. It is not use when RxPsnIpUdpMode is zero
//      1: Classify engine selects source UDP port to identify pseudowire packet
//      0: Classify engine selects destination UDP port to identify pseudowire packet %% RW %% 0x0 %% 0x0
// Field: [20] %% RxPsnIpTtlChkEn %% Enable check TTL field in MPLS/Ipv4 or Hop Limit field in Ipv6
//      1: Enable checking
//      0: Disable checking %% RW %% 0x0 %% 0x0
// Field: [19:0] %% RxPsnMplsOutLabel %% Received 2-label MPLS packet from PSN side will be discarded when it's outer label is different than RxPsnMplsOutLabel %% RW %% 0x0 %% 0x0
// End:
######################################################################################
#********************* 
#4.1.2 Classify Per Pseudowire Type Control 
#********************* 
// Begin: 
// Register Full Name: Classify Per Pseudowire Type Control  
// RTL Instant Name  :per_pw_ctrl  
// #Address: 0x0000B000-0x0000B53F
// Address: 0x0000C000-0x0000C53F
// #The address format for these registers is 0x0000C000 + PWID 
// Formula: Address +  $PWID
// Where: {$PWID(0-1343): Pseudowire ID}
// Description: This register configures identification types per pseudowire
// Width: 71
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [70:62]  %% RxCASbyte 		%% number byte of signal in case of cesoPSN with CAS  %% RW %% 0x0 %% 0x0
// Field: [61]  %% RxEthPwOnflyVC34 %% CEP EBM on-fly fractional, length changed %% RW %% 0x0 %% 0x0
// Field: [60:47]  %% RxEthPwLen %% length of packet to check malform %% RW %% 0x0 %% 0x0
// Field: [46:45]  %% RxEthCepMode %% CEP mode working
//      0,1: CEP basic
//      2: VC3 fractional
//      3: VC4 fractional  %% RW %% 0x0 %% 0x0
// Field: [44]  %% RxEthPwEn %% PW enable working
//      1: Enable PW 
//      0: Disable PW %% RW %% 0x0 %% 0x0
// Field: [43:12]  %% RxEthRtpSsrcValue %% This value is used to compare with SSRC value in RTP header of received TDM PW packets %% RW %% 0x0 %% 0x0
// Field: [11:5]  %% RxEthRtpPtValue %% This value is used to compare with PT value in RTP header of received TDM PW packets %% RW %% 0x0 %% 0x0
// Field: [4]  %% RxEthRtpEn %% Enable RTP
//      1: Enable RTP 
//      0: Disable RTP  %% RW %% 0x0 %% 0x0
// Field: [3]  %% RxEthRtpSsrcChkEn %% Enable checking SSRC field of RTP header in received TDM PW packet
//      1: Enable checking 
//      0: Disable checking %% RW %% 0x0 %% 0x0
// Field: [2]  %% RxEthRtpPtChkEn %% Enable checking PT field of RTP header in received TDM PW packet
//      1: Enable checking
//      0: Disable checking %% RW %% 0x0 %% 0x0
// Field: [1:0]  %% RxEthPwType %% this is PW type working 
//      0: CES mode without CAS
//      1: CES mode with CAS 
//      2: CEP mode  %% RW %% 0x0 %% 0x0
//End:
######################################################################################
#********************* 
#4.1.3. Classify HBCE Global Control
#********************* 
// Begin: 
// Register Full Name: Classify HBCE Global Control
// RTL Instant Name  :hbce_glb
// Address: 0x00008000
// Description: This register is used to configure global for HBCE module
// Width: 20
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [19:4]  %% CLAHbceTimeoutValue %% this is time out value for HBCE module, packing be discarded %% RW %% 0x00FF %% 0x0
// Field: [3]  %% CLAHbceHashingTableSelectedPage %% this is to select hashing table page to access looking up information buffer
//      1: PageID 1
//      0: PageID 0 %% RW %% 0x1 %% 0x0 
// Field: [2:0]  %% CLAHbceCodingSelectedMode %% selects mode to code origin id
//      4/5/6/7: code level 4/5/6/7
//      3: code level 3 (xor 4 subgroups origin id together)
//      2: code level 2
//      1: code level 1
//      0: none xor origin id %% RW %% 0x1 %% 0x0 
//End:
######################################################################################
#********************* 
#4.1.4. Classify HBCE Hashing Table Control
#********************* 
// Begin: 
// Register Full Name: Classify HBCE Hashing Table Control
// RTL Instant Name  :hbce_hash_table
// #Address: 0x00009000 - 0x00009FFF
// Address: 0x0000A000 - 0x0000B7FF
// Formula: Address + $PageID*0x1000 + $HashID 
// Where: {$PageID(0-1): Page selection} %%  {$HashID(0-2047): HashID}  
// Description:HBCE module uses 10 bits for tabindex. tabindex is generated by hasding function applied to the origin id. The collisions due to hashing are handled by pointer to variable size blocks. // // Handling variable  size blocks requires a dynamic memory management scheme. The number of entries in each variable size block is defined by the number of rules that collide within a specific entry of //the tabindex. Hashing function applies an XOR function to all bits of tabindex. The indexes to the tabindex are generated by hashing function and therefore collisions may occur. In order to resolve //these collisions efficiently, HBCE define a complex data structure associated with each entry of the orgin id. Hashing table has two fields, flow number (flownum) (number of labelid mapped to this //particular table entry) and memory start pointer (memsptr) (hold a pointer to the variable size block and the number of  rules that collide). In case, a table entry might be empty  which means that it //is not mapped to any flow address rule, flownum = 0. Moreover, a table entry may be mapped to many flow address rules. In this case, where collisions occur, hashing table have to store a pointer to  //the variable size block and the number of rules that collide. The number of colliding rules also indicates the size of the block. The formating of hashing table page0 in each case is shown as below.%% 
//Hash ID:%% 
//PATERN HASH %% 
// Label ID (20bit)  +  Lookup mode (2bit) + port (1bit)%% 
//With Lookup Mode: 0/1/2 ~ PSN MEF/UDP/MPLS%% 
//+ NOXOR       :  Hashid[10:0]  = Patern Hash [10:0], StoreID[11:0] = Patern Hash [22:11]%% 
//+ ONEXOR     :  Hashid[10:0]  = Patern Hash [10:0] XOR Patern Hash [21:11],%% 
//                           StoreID = Patern Hash [22]%% 
//+ TWOXOR    :   Hashid  = Patern Hash [10:0] XOR Patern Hash [21:11] XOR {7'b0,Patern Hash[22]}%% 
//                           StoreID = Patern Hash [22:11]
//#
//Width: 17
//Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [16:13]  %% CLAHbceFlowNumber %% this is to identify HashID is empty or win or collision 
//      0: empty format
//      1: normal format
//      others: collision format  %% RW %% 0x0 %% 0x0 
// Field: [12:0]  %% CLAHbceMemoryStartPointer %% this is a MemPtr to read the Classify HBCE Looking Up Information Control %% RW %% 0x0 %% 0x0 
//End:
######################################################################################
#********************* 
#4.1.5. Classify HBCE Looking Up Information Control
#********************* 
// Begin: 
// Register Full Name: Classify HBCE Looking Up Information Control
// RTL Instant Name  :hbce_lkup_info
// #Address: 0x0000A000 - 0x0000AFFF
// Address: 0x0000E000 - 0x0000FFFF
// #The address format for these registers is 0x0000E000 +  $CellID
// Formula: Address +  $CellID
//# Where: {$ CellID(0-4095):  Cell Info Identification}
// Where: {$ CellID(0-8192):  Cell Info Identification}
// Description: In HBCE module, all operations access this memory in order to examine whether an exact match exists or not. The Memory Pool implements dynamic memory management scheme and supports //variable size blocks. It supports requests for allocation and deallocation of variable size blocks when inserting and deleting lookup address occur. Linking between multiple blocks is implemented by //writing the address of the  next  block  in  the  previous block. In general, HBCE implementation is based on sequential accesses to memory pool and to the dynamically allocated collision nodes. The //formating of memory pool word is shown as below.%%
// The format of memory pool word is shown as below. There are two case formats depend on field[23]
// Width: 37
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// #Normal format
// Field: [36]  %% CLAHbceFlowDirect %% this configure FlowID working in normal mode (not in any group) %% RW %% 0x0 %% 0x0
// Field: [35]  %% CLAHbceGrpWorking %% this configure group working or protection  %% RW %% 0x0 %% 0x0
// Field: [34:25]  %% CLAHbceGrpIDFlow %% this configure a group ID that FlowID following  %% RW %% 0x0 %% 0x0
// Field: [24]  %% CLAHbceMemoryStatus %% This is zero for "normal format" %% RW %% 0x0 %% 0x0
// Field: [23:13] %%    CLAHbceFlowID %%  a number identifying the output port of HBCE module  this configure a group ID that FlowID following  %% RW %% 0x0 %% 0x0
// Field: [12]  %% CLAHbceFlowEnb %% The flow is identified in this table, it mean the traffic is identified
//      1: Flow identified
//      0: Flow look up fail  %% RW %% 0x0 %% 0x0
// Field: [11:0]  %% CLAHbceStoreID %% this is saving some additional information to identify a certain lookup address rule within a particular table entry HBCE and also to be able to distinguish those that collide %% RW %% 0x0 %% 0x0
// #link format
//# Field: [24]  %% CLAHbceMemoryStatus %% This is one for "link format" %% RW %% 0x1 %% 0x0
//# Field: [22:16] %% Unused %% Unused  %% RW %% 0x0 %% 0x0
//# Field: [15:0]  %% CLAHbceNextPointer %% Next pointer ( CellID) to jump from this block to another if space that block is not enough space %% RW %% 0x0 %% 0x0
//End:
######################################################################################
#********************* 
#4.1.7. CDR Pseudowire Look Up Control  
#********************* 
// Begin: 
// Register Full Name: CDR Pseudowire Look Up Control  
// RTL Instant Name  :cdr_pw_lkup_ctrl
// Address: 0x00003000 - 0x0000353F
// Formula: Address + PWID
// Where:{$PWID(0-1343): pwid} 
// Description: Used for TDM PW. This register is used to select PW for CDR function.
// Width: 32
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [11] %%   PwCdrSlice %%
//      0: Slice 0 corresponding to sts 0,2,4,...,46
//      1: Slice 1 corresponding to sts 1,3,5,...,47     %% RW %% 0x0 %% 0x0
// Field: [10]  %% PwCdrDis %%  applicable for pseudowire that is enabled CDR function (eg: ACR/DCR)
//      0: Enable CDR (default)
//      1: Disable CDR    %% RW %% 0x0 %% 0x0
// Field: [9:0]   %% PwCdrLineId %% CDR line ID lookup from pseudowire Ids.  %% RW %% 0x0 %% 0x0
// End:
######################################################################################
#********************* 
#4.1.8. Classify Pseudowire MEF over MPLS Control 
#********************* 
// Begin: 
// Register Full Name: Classify Pseudowire MEF over MPLS Control 
// RTL Instant Name  :pw_mef_over_mpls_ctrl
// #Address: 0x0000C000 - 0x0000C53F
// Address: 0x0000D000 - 0x0000D53F
// Formula: Address + $PWID
// Where:{$PWID(0-1344): pwid} 
// Description: Used for TDM PW. This register is used to configure MEF over MPLS
// Width: 51
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [50:3]    %% RxPwMEFoMPLSDaValue %%  DA value used to compare with DA in Ethernet MEF packet   %% RW %% 0x0 %% 0x0
// Field: [2]   %% RxPwMEFoMPLSDaCheckEn %% 
//  1: Enable check  RxPwMEFoMPLSDaValue with DA in Ethernet MEF packet 
//  0: Disable check  RxPwMEFoMPLSDaValue with DA in Ethernet MEF packet  %% RW %% 0x0 %% 0x0
// Field: [1]   %% RxPwMEFoMPLSEcidCheckEn %% 
//  1: Enable check  ECID in Ethernet MEF packet with MPLS PW label
//  0: Disable check  ECID in Ethernet MEF packet with MPLS PW label  %% RW %% 0x0 %% 0x0
// Field: [0]   %%  RxPwMEFoMPLSEn %% 
//  1: Enable MEF over MPLS PW mode, in this case, after MPLS PW label is ETH MEF packet
//  0: Normal case, disable MEF over MPLS PW mode, in this case, after MPLS PW label is PW control word  %% RW %% 0x0 %% 0x0
//End:
#********************* 
#4.1.9. Classify Per Group Enable Control 
#********************* 
// Begin: 
// Register Full Name: Classify Per Group Enable Control 
// RTL Instant Name  : cla_per_grp_enb
// #Address: 0x0000D000 - 0x0000D3FF
// Address: 0x00009000 - 0x000093FF
// Formula: Address + Working_ID*0x400 + Grp_ID
// Where:{$Working_ID(0-1): CLAHbceGrpWorking} %% {$Grp_ID(0-1023): CLAHbceGrpIDFlow}
// Description: This register configures Group that Flowid (or Pseudowire ID) is enable or not
// Width: 1
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [0]  %% CLAGrpPWEn %% This indicate the FlowID (or Pseudowire ID) is enable or not
//		1: FlowID enable
//		0: FlowID disable %% RW %% 0x0 %% 0x0
// End:
######################################################################################

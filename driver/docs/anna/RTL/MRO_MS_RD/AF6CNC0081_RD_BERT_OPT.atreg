##################################################################
# Arrive Technologies
#
# Revision 1.0 - 2013.Mar.23

##################################################################
#This section is for guideline 
#File name: filename.atreg (atreg = ATVN register define)
#Comment out a line: Please use (# or //#) character at the beginning of the line
#Delimiter character: (%%)

######################################################################################
#| Access Policy| Use Case| Description                | Effect of a Write on Current Field Value  | Effect of a Read on Current Field Value | Read-back Value |
#| ------|-----------|---------------------------------| ------------------------------------------|-----------------------------------------|-----------------|
#| RO    | Status    | Read Only                       | No effect                | No effect            | Current Value |
#| RW    | Config    | Read, Write                     | Changed to written value | No effect            | Current value |
#| RC    | Status    | Read Clears All                 | No effect                | Sets all bits to 0   | Current value |
#| RS    | UNKNOW    | Read Sets All                   | No effect                | Sets all bits to 1   | Current value |
#| WRC   | UNKNOW    | Write, Read Clears All          | Changed to written value | Sets all bits to 0   | Current value |
#| WRS   | UNKNOW    | Write, Read Sets All            | Changed to written value | Sets all bits to 1   | Current value |
#| WC    | Status    | Write Clears All                | Sets all bits to 0       | No effect            | Current value |
#| WS    | Status    | Write Sets All                  | Sets all bits to 1       | No effect            | Current value |
#| WSRC  | UNKNOW    | Write Sets All, Read Clears All | Sets all bits to 1       | Sets all bits to 0   | Current value |
#| WCRS  | UNKNOW    | Write Clears All,Read Sets All  | Sets all bits to 0       | Sets all bits to 1   | Current value |
#| W1C   | Interrupt | Write 1 to Clear                | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current Value |
#| W1S   | Interrupt | Write 1 to Set                  | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W1T   | UNKNOW    | Write 1 to Toggle               | If the bit in the wr value is a 1, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W0C   | Interrupt | Write 0 to Clear                | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current value |
#| W0S   | Interrupt | Write 0 to Set                  | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W0T   | UNKNOW    | Write 0 to Toggle               | If the bit in the wr value is a 0, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W1SRC | UNKNOW    | Write 1 to Set, Read Clears All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W1CRS | UNKNOW    | Write 1 to Clear, Read Sets All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| W0SRC | UNKNOW    | Write 0 to Set, Read Clears All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W0CRS | UNKNOW    | Write 0 to Clear, Read Sets All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| WO    | Config    | Write Only                      | Changed to written value | No effect | Undefined |
#| WOC   | UNKNOW    | Write Only Clears All           | Sets all bits to 0     | No effect | Undefined |
#| WOS   | UNKNOW    | Write Only Sets All             | Sets all bits to 1     | No effect | Undefined |
#| W1    | UNKNOW    | Write Once                      | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Current value |
#| WO1   | UNKNOW    | Write Only, Once                | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Undefined |
#=============================================================================
# Sample
#=============================================================================

######################################################################################
#SAMPLE> // Begin:
#SAMPLE> //# Some description
#SAMPLE> // Register Full Name: Device Version ID
#SAMPLE> // RTL Instant Name: VersionID
#SAMPLE> //# {FunctionName,SubFunction_InstantName_Description}
#SAMPLE> //# RTL Instant Name and Full Name MUST be unique within a register description file
#SAMPLE> // Address: 0x00_0000
#SAMPLE> //# Address is relative address, will be sum with Base_Address for each function block
#SAMPLE> // Formula      : Address + $portid 
#SAMPLE> // Where        : {$portid(0-7): port identification} 
#SAMPLE> // Description  : This register indicates the module' name and version. 
#SAMPLE> // Width: 32
#SAMPLE> // Register Type: {Status}
#SAMPLE> //# Field: [Bit:Bit] %%  Name     %% Description            %% Type %% SW_Reset %% HW_Reset
#SAMPLE> // Field: [31:24]    %%  Year     %% 2013                   %% RO   %% 0x0D     %% 0x0D
#SAMPLE> // Field: [23:16]    %%  Week     %% Week Synthesized FPGA  %% RO   %% 0x0C     %% 0x0C
#SAMPLE> // Field: [15:8]     %%  Day      %% Day Synthesized FPGA   %% RO   %% 0x16     %% 0x16
#SAMPLE> // Field: [7:4]      %%  VerID    %% FPGA Version           %% RO   %% 0x0      %% 0x0
#SAMPLE> // Field: [3:0]      %%  NumID    %% Number of FPGA Version %% RO   %% 0x1      %% 0x1
#SAMPLE> // End:

# ******************
# 1.1 Thalassa SEL BERT TS GEN 
# ******************
// Begin:
// Register Full Name: Sel Timeslot Gen
// RTL Instant Name  : sel_ts_gen
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x4102 - 0x7702
// Formula: Address + 512*engid  
// Where: 	{$engid(0-27): id of HO_BERT}  
// Description: The registers select 1id in line to gen bert data
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name     %% Description                   %% Type %% SW_Reset %% HW_Reset
// Field:  [31:0]    %%  gen_tsen %% bit map indicase 32 timeslot,"1" : enable    %% RW %% 0x0 %% 0x0
// End:

# ******************
# 1.2 Thalassa SEL BERT TS MON TDM 
# ******************
// Begin:
// Register Full Name: Sel Timeslot Mon tdm
// RTL Instant Name  : sel_mon_tstdm
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x8102 - 0xB102 
// Formula: Address + 512*engid 
// Where: 	{$engid(0-27): id of HO_BERT} 
// Description: The registers select 1id in line to gen bert data
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name     %% Description                   %% Type %% SW_Reset %% HW_Reset
// Field:  [31:0]    %%  tdm_tsen     %% bit map indicase 32 timeslot,"1" : enable    %% RW %% 0x0 %% 0x0
// End:


# ******************
# 2.1 Thalassa SEL ID BERT GEN 
# ******************
// Begin:
// Register Full Name: Sel Ho Bert Gen
// RTL Instant Name  : sel_ho_bert_gen
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x0_200 - 0x0_21B 
// Formula: Address + engid
// Where:   {$engid(0-27): id of HO_BERT}	
// Description: The registers select id in line to gen bert data
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name     %% Description                   %% Type %% SW_Reset %% HW_Reset
// Field:  [31:15]   %%  unused   %% unsed                         %% RW  %% 0x0 %% 0x0
//Field:   [14] 	 %% gen_en    %% set "1" to enable bert gen    %% RW %% 0x0 %% 0x0 
// Field:  [13:11]   %% lineid    %% line id OC48                  %% RW %% 0x0 %% 0x0
// Field:  [10:0]    %% id        %% TDM_ID   %% RW %% 0x0 %% 0x0
// End:

# ******************
# 2.2 Thalassa SEL ID BERT MON TDM 
# ******************
// Begin:
// Register Full Name: Sel TDM Bert mon
// RTL Instant Name  : sel_bert_montdm
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x0_000 - 0x0_01b 
// Formula: Address + engid
// Where: {$engid(0-27): id of HO_BERT}	
// Description: The registers select 1id in line to gen bert data
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name     %% Description                   %% Type %% SW_Reset %% HW_Reset
// Field:  [31:16]   %%  unused   %% unsed                         %% RW  %% 0x0 %% 0x0
// Field:   [15] 	 %% tdmmon_en    %% set "1" to enable bert gen    %% RW %% 0x0 %% 0x0 
// Field:   [14] 	 %% mon_side     %% "0" tdm side, "1" psn side moniter    %% RW %% 0x0 %% 0x0 
// Field:  [13:11]   %% tdm_lineid   %% line id OC48                  %% RW %% 0x0 %% 0x0
// Field:  [10:0]    %% id           %% TDM_ID for TDM Side or MAP_PWID                %% RW %% 0x0 %% 0x0
// End:


# ******************
# 3.1 Thalassa SEL BERT GEN MODE 
# ******************
// Begin:
// Register Full Name: Sel Mode Bert Gen
// RTL Instant Name  : ctrl_pen_gen
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x4100 - 0x7700 
// Formula: Address + 512*engid
// Where:  {$engid(0-27): bert id}   
// Description: The registers select mode bert gen
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name      %% Description                %% Type %% SW_Reset %% HW_Reset
// Field:  [31:06]  %% unused      %% unsed                      %% RW  %% 0x0 %% 0x0 
// Field:  [05]     %% swapmode	   %% swap data   %% RW  %% 0x0 %% 0x0  
// Field:  [04]     %% invmode	   %% invert data %% RW  %% 0x0 %% 0x0 
// Field:  [03 :00] %% patt_mode   %% sel pattern gen 
//                                    # 0x01 : all0
//									  # 0x02 : prbs15
//									  # 0x03 : prbs23
//									  # 0x05 : prbs31
//                                    # 0x7  : all1
//									  # 0x8  : SEQ(SIM)/prbs20(SYN)
//									  # 0x4  : prbs20r	
//									  # 0x06 : fixpattern 4byte 				
// 												   %% RW %% 0x4 %% 0x4
// End:

# ******************
# 3.1 Thalassa SEL TX FIXPATTERN 4BYTE
# ******************
// Begin:
// Register Full Name: Sel Value of fix pattern
// RTL Instant Name  : txfix_gen
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x4104 - 0xB704 
// Formula: Address + 512*engid
// Where:  {$engid(0-27): bert id}   
// Description: The registers select mode bert gen
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset 
// Field:  [31 :00]  %%txpatten_val      %% value of fix paatern    %% RW %% 0x0 %% 0x0
// End:

# ******************
# 3.2 Thalassa SEL MON TDM  MODE 
# ******************
// Begin:
// Register Full Name: Sel Mode Bert Mon TDM
// RTL Instant Name  : ctrl_pen_montdm
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x8100 - 0xB700 
// Formula: Address + 512*engid
// Where:   {$engid(0-27): bert id}
// Description: The registers select mode bert gen
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name      %% Description             %% Type %% SW_Reset %% HW_Reset
// Field:  [31:06]  %% unused      %% unsed                   %% RW  %% 0x0 %% 0x0 
// Field:  [05]     %% swapmode	   %% swap data   %% RW  %% 0x0 %% 0x0  
// Field:  [04]     %% invmode	   %% invert data %% RW  %% 0x0 %% 0x0 
// Field:  [03 :00] %% patt_mode   %% sel pattern gen 
//                                    # 0x01 : fixpattern
//                                    # 0x02 : all0
//									  # 0x04 : prbs15
//									  # 0x03 : prbs23
//									  # 0x05 : prbs31
//                                    # 0x00 : all1 / SEQ(SIM)
//                                    # 0x06 : prbs20(SYN)
//                                    # 0x08  :prbs20r 	
// 												   %% RW %% 0x4 %% 0x4
// End:

# ******************
# 3.1 Thalassa SEL RX FIXPATTERN 4BYTE
# ******************
// Begin:
// Register Full Name: Sel Value of fix pattern
// RTL Instant Name  : rxfix_gen
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x8104 - 0x8704 
// Formula: Address + 512*engid
// Where:  {$engid(0-27): bert id}   
// Description: The registers select mode bert gen
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset 
// Field:  [31 :00]  %% rxpatten_val      %% value of fix paatern    %% RW %% 0x0 %% 0x0
// End:

# ******************
# 4. Thalassa SEL HO_BERT BER insert 
# ******************
// Begin:
// Register Full Name: Inser Error
// RTL Instant Name  : ctrl_ber_pen
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x4101 - 0xB_701 
// Formula: Address + 512*engid
// Where: 	{$engid(0-27): bert id}
// Description: The registers select rate inser error
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field:  [31:00]   %%  ber_rate   	%% TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to Pattern Generator                         
//                                         [31:0] == 32'd0          :disable
//														
//                                        Incase N DS0 mode :
//                            			 		n        = num timeslot use, BER_level_val recalculate by CFG_DS1,CFG_E1
//								 		 		CFG_DS1  = ( 8n  x  BER_level_val) / 193
//                                       		CFG_E1   = ( 8n  x  BER_level_val) / 256
// 	                                      Other : 
//	 											BER_level_val	BER_level
//	   											1000:        	BER 10e-3
//	   											10_000:      	BER 10e-4
//	   											100_000:     	BER 10e-5
//	   											1_000_000:   	BER 10e-6
//	   											10_000_000:  	BER 10e-7
//																			 %% RW %% 0x0 %% 0x0


# ******************
# 5.1 Thalassa BERT TDM GEN CNT 
# ******************
// Begin:
// Register Full Name: good counter tdm gen
// RTL Instant Name  : goodbit_pen_tdm_gen
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x0600 - 0x063F 
// Formula: Address + id
// Where: 	{$id(0-31): bert id}
// Description: The registers select 1id in line to gen bert data
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name    %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:00]    %%  bertgen %% gen goodbit             %% RC  %% 0x0      %% 0x0 
// End:

# ******************
# 5.1 Thalassa BERT TDM CNT GOOD 
# ******************
// Begin:
// Register Full Name: good counter tdm
// RTL Instant Name  : goodbit_pen_tdm_mon
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x8600 - 0x863F 
// Formula: Address + id
// Where: 	{$id(0-31): bert id}
// Description: The registers select 1id in line to gen bert data
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name    %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:00]    %%  mongood %% moniter goodbit         %% RC  %% 0x0      %% 0x0 
// End:



# ******************
# 5.2 Thalassa  HO_BERT TDM CNT ERR
# ******************
// Begin:
// Register Full Name: TDM err sync
// RTL Instant Name  : err_tdm_mon
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x8640 - 0x867f 
// Formula: Address + id
// Where: 	{$id(0-31): bert id}
// Description: The registers indicate bert mon err in tdm side
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
//  Field:  [31:00]  %%  cnt_errbit     %% counter err bit         %% RC  %% 0x0 %% 0x0  
// End:

# ******************
# 5.3 Thalassa  HO_BERT TDM STICKY LOSS BIT 
# ******************
// Begin:
// Register Full Name: Counter loss bit in TDM mon 
// RTL Instant Name  : lossyn_pen_tdm_mon
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x8105-0xB705 
// Formula: Address + engid*512 
// Where:   {$engid(0-27): slice id of HO_BERT}
// Description: The registers count lossbit in  mon tdm side
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
//  Field:  [31:00]  %%  stk_losssyn    %% sticky loss sync        %% W1C  %% 0x0 %% 0x0  
// End:

# ******************
# 5.3 Thalassa  HO_BERT TDM COUNTER LOSS BIT 
# ******************
// Begin:
// Register Full Name: Counter loss bit in TDM mon 
// RTL Instant Name  : lossbit_pen_tdm_mon
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x8680 - 0x86BF 
// Formula: Address + id
// Where:   {$id(0-31): bert id}
// Description: The registers count lossbit in  mon tdm side
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
//  Field:  [31:00]  %%  cnt_lossbit    %% counter lossbit         %% RC  %% 0x0 %% 0x0  
// End:

# ******************
# 4. Thalassa SEL HO_BERT BER signle insert 
# ******************
// Begin:
// Register Full Name: Single Inser Error
// RTL Instant Name  : singe_ber_pen
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x4105-0x7105  
// Formula: Address + 512*engid
// Where: 	{$engid(0-27): bert id} 
// Description: The registers select rate inser error
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field:  [31:01]  %% unused      %% unsed                        %% RW   %% 0x0 %% 0x0 
// Field:  [0]      %% ena   	   %% sw write "1" to force, hw auto clear  %% RW  %% 0x0 %% 0x0  
// End: 

# ******************
# 4.1.6. Thalassa  HO_BERT TDM MON Status 
# ******************
// Begin:
// Register Full Name: tdm mon stt
// RTL Instant Name  : stt_tdm_mon
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address: 0x8103 - 0xB703 
// Formula: Address + 512*engid
// Where: 	{$engid(0-7): id of HO_BERT} 
// Description: The registers indicate bert mon state in pw side
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field:  [31:03]   %%  unused         %% unsed                   %% RW  %% 0x0 %% 0x0  
//Field:   [1:0]     %%  tdmstate        %% Status [1:0]: Prbs status
//											   LOPSTA = 2'd0;
//											   SRCSTA = 2'd1;
//											   VERSTA = 2'd2;
//											   INFSTA = 2'd3;  %% RO %% 0x0 %% 0x0  
// End:


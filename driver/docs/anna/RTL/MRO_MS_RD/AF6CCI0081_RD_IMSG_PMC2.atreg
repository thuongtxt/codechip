######################################################################################
# Arrive Technologies
# Register Description Document
# Revision 1.0 - 2015.Feb.03

######################################################################################
# This section is for guideline
# Refer to \\Hcmcserv3\Projects\internal_tools\atvn_vlibs\bin\rgm_gen\docs\REGISTER_DESCRIPTION_GUIDELINE.pdf
# File name: filename.atreg (atreg = ATVN register define)
# Comment out a line: Please use (# or //#) character at the beginning of the line
# Delimiter character: (%%)

######################################################################################
#| Access Policy| Use Case| Description                | Effect of a Write on Current Field Value  | Effect of a Read on Current Field Value | Read-back Value |
#| ------|-----------|---------------------------------| ------------------------------------------|-----------------------------------------|-----------------|
#| RO    | Status    | Read Only                       | No effect                | No effect            | Current Value |
#| RW    | Config    | Read, Write                     | Changed to written value | No effect            | Current value |
#| RC    | Status    | Read Clears All                 | No effect                | Sets all bits to 0   | Current value |
#| RS    | UNKNOW    | Read Sets All                   | No effect                | Sets all bits to 1   | Current value |
#| WRC   | UNKNOW    | Write, Read Clears All          | Changed to written value | Sets all bits to 0   | Current value |
#| WRS   | UNKNOW    | Write, Read Sets All            | Changed to written value | Sets all bits to 1   | Current value |
#| WC    | Status    | Write Clears All                | Sets all bits to 0       | No effect            | Current value |
#| WS    | Status    | Write Sets All                  | Sets all bits to 1       | No effect            | Current value |
#| WSRC  | UNKNOW    | Write Sets All, Read Clears All | Sets all bits to 1       | Sets all bits to 0   | Current value |
#| WCRS  | UNKNOW    | Write Clears All,Read Sets All  | Sets all bits to 0       | Sets all bits to 1   | Current value |
#| W1C   | Interrupt | Write 1 to Clear                | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current Value |
#| W1S   | Interrupt | Write 1 to Set                  | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W1T   | UNKNOW    | Write 1 to Toggle               | If the bit in the wr value is a 1, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W0C   | Interrupt | Write 0 to Clear                | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current value |
#| W0S   | Interrupt | Write 0 to Set                  | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W0T   | UNKNOW    | Write 0 to Toggle               | If the bit in the wr value is a 0, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W1SRC | UNKNOW    | Write 1 to Set, Read Clears All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W1CRS | UNKNOW    | Write 1 to Clear, Read Sets All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| W0SRC | UNKNOW    | Write 0 to Set, Read Clears All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W0CRS | UNKNOW    | Write 0 to Clear, Read Sets All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| WO    | Config    | Write Only                      | Changed to written value | No effect | Undefined |
#| WOC   | UNKNOW    | Write Only Clears All           | Sets all bits to 0     | No effect | Undefined |
#| WOS   | UNKNOW    | Write Only Sets All             | Sets all bits to 1     | No effect | Undefined |
#| W1    | UNKNOW    | Write Once                      | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Current value |
#| WO1   | UNKNOW    | Write Only, Once                | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Undefined |
#=============================================================================
# Sample
#=============================================================================

######################################################################################
#SAMPLE> // Begin:
#SAMPLE> //# Some description
#SAMPLE> // Register Full Name: Device Version ID
#SAMPLE> // RTL Instant Name: VersionID
#SAMPLE> //# {FunctionName,SubFunction_InstantName_Description}
#SAMPLE> //# RTL Instant Name and Full Name MUST be unique within a register description file
#SAMPLE> // Address: 0x00_0000
#SAMPLE> //# Address is relative address, will be sum with Base_Address for each function block
#SAMPLE> // Formula      : Address + $portid 
#SAMPLE> // Where        : {$portid(0-7): port identification} 
#SAMPLE> // Description  : This register indicates the module' name and version. 
#SAMPLE> // Width: 32
#SAMPLE> // Register Type: {Status}
#SAMPLE> //# Field: [Bit:Bit] %%  Name     %% Description            %% Type %% SW_Reset %% HW_Reset
#SAMPLE> // Field: [31:24]    %%  Year     %% 2013                   %% RO   %% 0x0D     %% 0x0D
#SAMPLE> // Field: [23:16]    %%  Week     %% Week Synthesized FPGA  %% RO   %% 0x0C     %% 0x0C
#SAMPLE> // Field: [15:8]     %%  Day      %% Day Synthesized FPGA   %% RO   %% 0x16     %% 0x16
#SAMPLE> // Field: [7:4]      %%  VerID    %% FPGA Version           %% RO   %% 0x0      %% 0x0
#SAMPLE> // Field: [3:0]      %%  NumID    %% Number of FPGA Version %% RO   %% 0x1      %% 0x1
#SAMPLE> // End:
######################################################################################

#********************** 
# MPEG Lookup id      *
#**********************
// Begin:
// Register Full Name: MPEG Lookup id
// RTL Instant Name  : upen_mpeglk
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0C_2000
// Formula      : Address + $rxflow_id
// Where        : {$rxflow_id(0-5375) : Counter ID}
// Description  : This register is used to convert rxflow id to slice id and enc id for 4 intance enc (only use for oam ans plain at enc counter)
// Width: 13
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [13:13]      %% enc lookup en    %% 1 : lk enable                    %% R/W              %% 0x0              %% 0x0  
//  Field: [12:11]      %% enc slice        %% enc slice                        %% R/W              %% 0x0              %% 0x0  
//  Field: [10:0]       %% enc id           %% enc id                           %% R/W              %% 0x0              %% 0x0  
// End:
######################################################################################

#********************** 	
#PMC GLB INF Counter enc *
#**********************
// Begin:
// Register Full Name: PMC GLB INF Counter enc
// RTL Instant Name  : upen_enc
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x00_0000
// Formula      : Address +  $idx_id*0x1_0000 + $slc_id*0x4000 +  $pos_id*0x1000 + $r2c*0x800 + $enc_id
// Where        : { $idx_id(0-1) %% $slc_id (0-0) %% $pos_id(0-3) : Position ID} %% $r2c(0-1) : r2c bit} %% {$enc_id(0-335) : Counter ID}
// Description  : This register is used to read some packet counters , support  both mode r2c and ro
                    - in sace of idx_id = 0
                      + pos_id = 0 : encabort
                      + pos_id = 1 : encoam
                      + pos_id = 2 : encplain
                      + pos_id = 3 : encfrg

                    - in sace of idx_id = 1  
                      + pos_id = 0 : encbytegood 
                      + pos_id = 1 : encpktgood 
                      + pos_id = 2 : enctotalbyte
                      + pos_id = 3 : enctotalpkt

                    - r2c       : enable mean read to clear  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [31:0]       %% cnt_enc          %% Counter value                    %% R/W              %% 0x0              %% 0x0  
// End:
######################################################################################

#********************** 	
#PMC GLB INF Counter enc1 *
#**********************
// Begin:
// Register Full Name: PMC GLB INF Counter enc1
// RTL Instant Name  : upen_enc1
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0E_0000
// Formula      : Address +  $idx_id*0x1_0000 + $slc_id*0x4000 +  $pos_id*0x1000 + $r2c*0x800 + $enc_id
// Where        : { $idx_id(0-0) %% $slc_id (0-0) %% $pos_id(0-3) : Position ID} %% $r2c(0-1) : r2c bit} %% {$enc_id(0-335) : Counter ID}
// Description  : This register is used to read some packet counters , support  both mode r2c and ro
                    - in sace of idx_id = 0
                      + pos_id = 0 : encidle
                      + pos_id = 1 : encmtu
                      + pos_id = 2 : unused
                      + pos_id = 3 : unused
                   
                    - r2c       : enable mean read to clear  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [31:0]       %% cnt_enc1         %% Counter value                    %% R/W              %% 0x0              %% 0x0  
// End:
######################################################################################




#********************** 
#PMC GLB INF Counter dec *
#**********************
// Begin:
// Register Full Name: PMC GLB INF Counter dec
// RTL Instant Name  : upen_dec
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x02_0000
// Formula      : Address + $idx_id*0x1_0000 +  $pos_id*0x4000 + $r2c*0x2000 + $dec_id
// Where        : {$idx_id(0-2) %% $pos_id(0-3) : Position ID} %% $r2c(0-1) : r2c bit} %% {$dec_id(0-335) : Counter ID}
// Description  : This register is used to read some packet counters , support  both mode r2c and ro
                    - in sace of idx_id = 0  
                      + pos_id = 0 : decmru
                      + pos_id = 1 : decdispkt 
                      + pos_id = 2 : decfcserr
                      + pos_id = 3 : decidle

	                - in sace of idx_id = 1
                      + pos_id = 0 : decabort
                      + pos_id = 1 : decoam
                      + pos_id = 2 : decplain
                      + pos_id = 3 : decfrg

                    - in sace of idx_id = 2
                      + pos_id = 0 : decpktgood
                      + pos_id = 1 : dectotalbyte
                      + pos_id = 2 : dectotalpkt
                      + pos_id = 3 : decbytegood

                    - r2c       : enable mean read to clear  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [31:0]       %% cnt_dec          %% Counter value                    %% R/W              %% 0x0              %% 0x0  
// End:
######################################################################################

#********************** 
#PMC GLB INF Counter rxflow *
#**********************
// Begin:
// Register Full Name: PMC GLB INF Counter rxflow
// RTL Instant Name  : upen_rxflow
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x05_0000
// Formula      : Address + $idx_id*0x1_0000 +  $pos_id*0x4000 + $r2c*0x2000 + $rxflow_id
// Where        : {$idx_id(0-2) %% $pos_id(0-3) : Position ID} %% $r2c(0-1) : r2c bit} %% {$rxflow_id(0-5375) : Counter ID}
// Description  : This register is used to read some packet counters , support  both mode r2c and ro
                    - in sace of idx_id = 0
                      + pos_id = 0 : rxbytecnt
                      + pos_id = 1 : rxpktcnt
                      + pos_id = 2 : rxdisbyte
                      + pos_id = 3 : rxdispkt

                    - in sace of idx_id = 1  
                      + pos_id = 0 : rxfrg   
                      + pos_id = 1 : rxnullfrg
                      + pos_id = 2 : rxBECN
                      + pos_id = 3 : rxFECN
                    
                    - in sace of idx_id = 2
                      + pos_id = 0 : rxDE
                      + pos_id = 1 : unused
                      + pos_id = 2 : unused
                      + pos_id = 3 : unused

                    - r2c       : enable mean read to clear  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [31:0]       %% cnt_rxflow       %% Counter value                    %% R/W              %% 0x0              %% 0x0  
// End:
######################################################################################

#********************** 
#PMC GLB INF Counter txflow *
#**********************
// Begin:
// Register Full Name: PMC GLB INF Counter txflow
// RTL Instant Name  : upen_txflow
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x08_0000
// Formula      : Address + $idx_id*0x1_0000 +  $pos_id*0x4000 + $r2c*0x2000 + $txflow_id
// Where        : {$idx_id(0-2) %% $pos_id(0-3) : Position ID} %% $r2c(0-1) : r2c bit} %% {$txflow_id(0-5375) : Counter ID}
// Description  : This register is used to read some packet counters , support  both mode r2c and ro
                    - in sace of idx_id = 0
                      + pos_id = 0 : txbytecnt
                      + pos_id = 1 : txpktcnt
                      + pos_id = 2 : txdisbyte 
                      + pos_id = 3 : txdispkt 

                    - in sace of idx_id = 1  
                      + pos_id = 0 : txfrg
                      + pos_id = 1 : txnullfrg 
                      + pos_id = 2 : txBECN
                      + pos_id = 3 : txFECN
                    
                    - in sace of idx_id = 2
                      + pos_id = 0 : txDE
                      + pos_id = 1 : unused
                      + pos_id = 2 : unused
                      + pos_id = 3 : unused

                    - r2c       : enable mean read to clear  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [31:0]       %% cnt_txflow       %% Counter value                    %% R/W              %% 0x0              %% 0x0  
// End:
######################################################################################

#********************** 
#PMC GLB INF Counter txbundle *
#**********************
// Begin:
// Register Full Name: PMC GLB INF Counter txbundle
// RTL Instant Name  : upen_txbundle
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0B_0000
// Formula      : Address + $idx_id*0x1000 +  $pos_id*0x400 + $r2c*0x200 + $txbundle_id
// Where        : {$idx_id(0-1) %% $pos_id(0-3) : Position ID} %% $r2c(0-1) : r2c bit} %% {$txbundle_id(0-511) : Counter ID}
// Description  : This register is used to read some packet counters , support  both mode r2c and ro
                    - in sace of idx_id = 0
                      + pos_id = 0 : buntxfrg
                      + pos_id = 1 : buntxpktdiscard
                      + pos_id = 2 : buntxpktgood
                      + pos_id = 3 : unused

                    - in sace of idx_id = 1  
                      + pos_id = 0 : buntxbyte
                      + pos_id = 1 : unused
                      + pos_id = 2 : unused
                      + pos_id = 3 : unused

                    - r2c       : enable mean read to clear  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [31:0]       %% cnt_txbundle          %% Counter value                    %% R/W              %% 0x0              %% 0x0  
// End:
######################################################################################

#********************** 
#PMC GLB INF Counter rxbundle *
#**********************
// Begin:
// Register Full Name: PMC GLB INF Counter rxbundle
// RTL Instant Name  : upen_rxbundle
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0B_2000
// Formula      : Address + $idx_id*0x1000 +  $pos_id*0x400 + $r2c*0x200 + $rxbundle_id
// Where        : {$idx_id(0-1) %% $pos_id(0-3) : Position ID} %% $r2c(0-1) : r2c bit} %% {$rxbundle_id(0-511) : Counter ID}
// Description  : This register is used to read some packet counters , support  both mode r2c and ro
                    - in sace of idx_id = 0
                      + pos_id = 0 : bunrxfrg
                      + pos_id = 1 : bunrxpktdiscard
                      + pos_id = 2 : bunrxpktgood
                      + pos_id = 3 : unused

                    - in sace of idx_id = 1  
                      + pos_id = 0 : bunrxbyte
                      + pos_id = 1 : unused
                      + pos_id = 2 : unused
                      + pos_id = 3 : unused

                    - r2c       : enable mean read to clear  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [31:0]       %% cnt_rxbundle          %% Counter value                    %% R/W              %% 0x0              %% 0x0  
// End:
######################################################################################

#********************** 
#PMC GLB INF Counter GFP ENC HO *
#**********************
// Begin:
// Register Full Name: PMC GLB INF Counter GFP ENC HO
// RTL Instant Name  : upen_enc_ho
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0B_4000
// Formula      : Address +  $idx_id*0x400 + $pos_id*0x100 + $r2c*0x80 + $encho_id
// Where        :  {$idx_id(0-1) : index} %% {$pos_id(0-3) : Position ID} %% {$r2c(0-1) : r2c bit} %% {$encho_id(0-127) : Counter ID}
// Description  : This register is used to read some packet counters , support  both mode r2c and ro
                   - in sace of idx_id = 0
                      + pos_id = 0 : byte
                      + pos_id = 1 : pkt
                      + pos_id = 2 : idle
                      + pos_id = 3 : unused
                    - in sace of idx_id = 1
                      + pos_id = 0 : GFPF-SO-DATA-FRM-CNT
                      + pos_id = 1 : GFPF-SO-CMF-CSF-FRM-CNT
                      + pos_id = 2 : GFPF-SO-CMF-FRM-CNT
                      + pos_id = 3 : GFPF-SO-MCF-FRM-CNT


                    - r2c       : enable mean read to clear  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [31:0]       %% cnt_encho        %% Counter enc high order            %% R/W              %% 0x0              %% 0x0  
// End:
######################################################################################


#********************** 
#PMC GLB INF Counter GFP DEC HO *
#**********************
// Begin:
// Register Full Name: PMC GLB INF Counter GFP DEC HO
// RTL Instant Name  : upen_dec_ho
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0B_5000
// Formula      : Address +  $idx_id*0x400 + $pos_id*0x100 + $r2c*0x80 + $decho_id
// Where        : {$idx_id(0-5) %% {$pos_id(0-3) : Position ID} %% {$r2c(0-1) : r2c bit} %% {$decho_id(0-127) : Counter ID}
// Description  : This register is used to read some packet counters , support  both mode r2c and ro
                   - in sace of idx_id = 0
                      + pos_id = 0 : err
                      + pos_id = 1 : pkt
                      + pos_id = 2 : idle
                      + pos_id = 3 : byte
                   - in sace of idx_id = 1
                      + pos_id = 0 : GFPF-SK-DATA-FRM-CNT
                      + pos_id = 1 : GFPF-SK-CMF-FRM-CNT
                      + pos_id = 2 : GFPF-SK-THEC-HIT-CNT
                      + pos_id = 3 : GFPF-SK-THEC-ERR-CNT
                   - in sace of idx_id = 2
                      + pos_id = 0 : GFPF-SK-THEC-TYPE-CORR-CNT
                      + pos_id = 1 : GFPF-SK-THEC-CRC-COR-CNT
                      + pos_id = 2 : GFPF-SK-EHEC-HIT-CNT
                      + pos_id = 3 : GFPF-SK-EHEC-ERR-CNT
				   - in sace of idx_id = 3
                      + pos_id = 0 : GFPF-SK-CHEC-HIT-CNT
                      + pos_id = 1 : GFPF-SK-CHEC-ERR-CNT
                      + pos_id = 2 : GFPF-SK-CHEC-PLI-COR-CNT
                      + pos_id = 3 : GFPF-SK-CHEC-CRC-COR-CNT
                   - in sace of idx_id = 4
                      + pos_id = 0 : GFPF-SK-PFCS-HIT-CNT
                      + pos_id = 1 : GFPF-SK-PFCS-ERR-CNT
                      + pos_id = 2 : GFPF-SK-MAX-LEN-ERR-CNT
                      + pos_id = 3 : GFPF-SK-MIN-LEN-ERR-CNT
                  - in sace of idx_id = 5
                      + pos_id = 0 : GFPF-SK-PTI-UPI-DISCARDED-CNT
                      + pos_id = 1 : unused
                      + pos_id = 2 : unused
                      + pos_id = 3 : unused
 




                   - r2c       : enable mean read to clear  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [31:0]       %% cnt_decho        %% Counter dec high order            %% R/W              %% 0x0              %% 0x0  
// End:
######################################################################################

#********************** 
#PMC GLB INF Counter GFP ENC LO *
#**********************
// Begin:
// Register Full Name: PMC GLB INF Counter GFP ENC LO
// RTL Instant Name  : upen_enc_lo
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0B_7000
// Formula      : Address +  $idx_id*0x400 + $pos_id*0x100 + $r2c*0x80 + $encho_id
// Where        :  {$idx_id(0-1) : index} %% {$pos_id(0-3) : Position ID} %% {$r2c(0-1) : r2c bit} %% {$encho_id(0-127) : Counter ID}
// Description  : This register is used to read some packet counters , support  both mode r2c and ro
                   - in sace of idx_id = 0
                      + pos_id = 0 : byte
                      + pos_id = 1 : pkt
                      + pos_id = 2 : idle
                      + pos_id = 3 : unused
                    - in sace of idx_id = 1
                      + pos_id = 0 : GFPF-SO-DATA-FRM-CNT
                      + pos_id = 1 : GFPF-SO-CMF-CSF-FRM-CNT
                      + pos_id = 2 : GFPF-SO-CMF-FRM-CNT
                      + pos_id = 3 : GFPF-SO-MCF-FRM-CNT


                    - r2c       : enable mean read to clear  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [31:0]       %% cnt_enclo        %% Counter enc low order            %% R/W              %% 0x0              %% 0x0  
// End:
######################################################################################


#********************** 
#PMC GLB INF Counter GFP DEC LO *
#**********************
// Begin:
// Register Full Name: PMC GLB INF Counter GFP DEC LO
// RTL Instant Name  : upen_dec_lo
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0B_8000
// Formula      : Address +  $idx_id*0x400 + $pos_id*0x100 + $r2c*0x80 + $decho_id
// Where        : {$idx_id(0-5) %% {$pos_id(0-3) : Position ID} %% {$r2c(0-1) : r2c bit} %% {$decho_id(0-127) : Counter ID}
// Description  : This register is used to read some packet counters , support  both mode r2c and ro
                   - in sace of idx_id = 0
                      + pos_id = 0 : err
                      + pos_id = 1 : pkt
                      + pos_id = 2 : idle
                      + pos_id = 3 : byte
                   - in sace of idx_id = 1
                      + pos_id = 0 : GFPF-SK-DATA-FRM-CNT
                      + pos_id = 1 : GFPF-SK-CMF-FRM-CNT
                      + pos_id = 2 : GFPF-SK-THEC-HIT-CNT
                      + pos_id = 3 : GFPF-SK-THEC-ERR-CNT
                   - in sace of idx_id = 2
                      + pos_id = 0 : GFPF-SK-THEC-TYPE-CORR-CNT
                      + pos_id = 1 : GFPF-SK-THEC-CRC-COR-CNT
                      + pos_id = 2 : GFPF-SK-EHEC-HIT-CNT
                      + pos_id = 3 : GFPF-SK-EHEC-ERR-CNT
				   - in sace of idx_id = 3
                      + pos_id = 0 : GFPF-SK-CHEC-HIT-CNT
                      + pos_id = 1 : GFPF-SK-CHEC-ERR-CNT
                      + pos_id = 2 : GFPF-SK-CHEC-PLI-COR-CNT
                      + pos_id = 3 : GFPF-SK-CHEC-CRC-COR-CNT
                   - in sace of idx_id = 4
                      + pos_id = 0 : GFPF-SK-PFCS-HIT-CNT
                      + pos_id = 1 : GFPF-SK-PFCS-ERR-CNT
                      + pos_id = 2 : GFPF-SK-MAX-LEN-ERR-CNT
                      + pos_id = 3 : GFPF-SK-MIN-LEN-ERR-CNT
                  - in sace of idx_id = 5
                      + pos_id = 0 : GFPF-SK-PTI-UPI-DISCARDED-CNT
                      + pos_id = 1 : unused
                      + pos_id = 2 : unused
                      + pos_id = 3 : unused
 




                   - r2c       : enable mean read to clear  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [31:0]       %% cnt_declo        %% Counter dec low order            %% R/W              %% 0x0              %% 0x0  
// End:
######################################################################################

#********************** 
#PMC GLB INF Counters *
#**********************
// Begin:
// Register Full Name: PMC GLB INF Counters
// RTL Instant Name  : upen_glb_inf_count
//# {FunctionName,SubFunction_InstantName_Description}
//# RTL Instant Name and Full Name MUST be unique within a register description file
// Address      : 0x0C_0000
// Formula      : Address + $pos_id*0x10 + $r2c*0x8 + $cnt_id
// Where        : {{$pos_id(0-15) : Position ID} %% $r2c(0-1) : r2c bit} %% {$cnt_id(0-7) : Counter ID}
// Description  : This register is used to read some packet counters , support  both mode r2c and ro
                    - Pos_id : Position counters
                      + 0  : MAC -> CLA port 0
                      + 1  : MAC -> CLA port 1
                      + 2  : CLA -> PDA port 0
                      + 3  : CLA -> PDA port 1
                      + 4  : PDA -> ENC#0
                      + 5  : PDA -> ENC#1
                      + 6  : PDA -> ENC#2
                      + 7  : PDA -> ENC#3
                      + 8  : PLA -> PKA#0
                      + 9  : PLA -> PKA#1
                      + 10 : PKA#0 -> PW#0
                      + 11 : PKA#0 -> PW#1
                      + 12 : PW#0 -> ETH
                      + 13 : PW#1 -> ETH
                      + 14 : PDA -> ETH PASS 1G
                      + 15 : PDA -> ETH PASS 10G
                             
                    - Counter ID details :
                        + 0     : vld_counters
                        + 1     : sop_counters
                        + 2     : eop_counters
                        + 3     : err_counters
                        + 4     : byte_counters
                        + 5-7   : Unused
                    - r2c       : enable mean read to clear  
// Width: 32
// Register Type: {Config}
//# Field: [Bit:Bit]    %% Name             %% Description                      %% Type             %% Reset            %% Default
//  Field: [31:0]       %% cnt_val          %% Counter value                    %% R/W              %% 0x0              %% 0x0  
// End:



######################################################################################
# Arrive Technologies
# Register Description Document
# Revision 1.0 - 2015.Apr.07

######################################################################################
# This section is for guideline
# Refer to \\Hcmcserv3\Projects\internal_tools\atvn_vlibs\bin\rgm_gen\docs\REGISTER_DESCRIPTION_GUIDELINE.pdf
# File name: filename.atreg (atreg = ATVN register define)
# Comment out a line: Please use (# or //#) character at the beginning of the line
# Delimiter character: (%%)

######################################################################################
#| Access Policy| Use Case| Description                | Effect of a Write on Current Field Value  | Effect of a Read on Current Field Value | Read-back Value |
#| ------|-----------|---------------------------------| ------------------------------------------|-----------------------------------------|-----------------|
#| RO    | Status    | Read Only                       | No effect                | No effect            | Current Value |
#| RW    | Config    | Read, Write                     | Changed to written value | No effect            | Current value |
#| RC    | Status    | Read Clears All                 | No effect                | Sets all bits to 0   | Current value |
#| RS    | UNKNOW    | Read Sets All                   | No effect                | Sets all bits to 1   | Current value |
#| WRC   | UNKNOW    | Write, Read Clears All          | Changed to written value | Sets all bits to 0   | Current value |
#| WRS   | UNKNOW    | Write, Read Sets All            | Changed to written value | Sets all bits to 1   | Current value |
#| WC    | Status    | Write Clears All                | Sets all bits to 0       | No effect            | Current value |
#| WS    | Status    | Write Sets All                  | Sets all bits to 1       | No effect            | Current value |
#| WSRC  | UNKNOW    | Write Sets All, Read Clears All | Sets all bits to 1       | Sets all bits to 0   | Current value |
#| WCRS  | UNKNOW    | Write Clears All,Read Sets All  | Sets all bits to 0       | Sets all bits to 1   | Current value |
#| W1C   | Interrupt | Write 1 to Clear                | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current Value |
#| W1S   | Interrupt | Write 1 to Set                  | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W1T   | UNKNOW    | Write 1 to Toggle               | If the bit in the wr value is a 1, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W0C   | Interrupt | Write 0 to Clear                | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | No effect | Current value |
#| W0S   | Interrupt | Write 0 to Set                  | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | No effect | Current value |
#| W0T   | UNKNOW    | Write 0 to Toggle               | If the bit in the wr value is a 0, the corresponding bit in the field is inverted. Otherwise, the field bit is not affected | No effect | Current value |
#| W1SRC | UNKNOW    | Write 1 to Set, Read Clears All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W1CRS | UNKNOW    | Write 1 to Clear, Read Sets All | If the bit in the wr value is a 1, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| W0SRC | UNKNOW    | Write 0 to Set, Read Clears All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 1. Otherwise, the field bit is not affected | Sets all bits to 0 | Current value |
#| W0CRS | UNKNOW    | Write 0 to Clear, Read Sets All | If the bit in the wr value is a 0, the corresponding bit in the field is set to 0. Otherwise, the field bit is not affected | Sets all bits to 1 | Current value |
#| WO    | Config    | Write Only                      | Changed to written value | No effect | Undefined |
#| WOC   | UNKNOW    | Write Only Clears All           | Sets all bits to 0     | No effect | Undefined |
#| WOS   | UNKNOW    | Write Only Sets All             | Sets all bits to 1     | No effect | Undefined |
#| W1    | UNKNOW    | Write Once                      | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Current value |
#| WO1   | UNKNOW    | Write Only, Once                | Changed to written value if this is the first write operation after a hard reset. Otherwise, has no effect | No effect | Undefined |
#=============================================================================
# Sample
#=============================================================================

######################################################################################
#SAMPLE> // Begin:
#SAMPLE> //# Some description
#SAMPLE> // Register Full Name: Device Version ID
#SAMPLE> // RTL Instant Name: VersionID
#SAMPLE> //# {FunctionName,SubFunction_InstantName_Description}
#SAMPLE> //# RTL Instant Name and Full Name MUST be unique within a register description file
#SAMPLE> // Address: 0x00_0000
#SAMPLE> //# Address is relative address, will be sum with Base_Address for each function block
#SAMPLE> // Formula      : Address + $portid 
#SAMPLE> // Where        : {$portid(0-7): port identification} 
#SAMPLE> // Description  : This register indicates the module' name and version. 
#SAMPLE> // Width: 32
#SAMPLE> // Register Type: {Status}
#SAMPLE> //# Field: [Bit:Bit] %%  Name     %% Description            %% Type %% SW_Reset %% HW_Reset
#SAMPLE> // Field: [31:24]    %%  Year     %% 2013                   %% RO   %% 0x0D     %% 0x0D
#SAMPLE> // Field: [23:16]    %%  Week     %% Week Synthesized FPGA  %% RO   %% 0x0C     %% 0x0C
#SAMPLE> // Field: [15:8]     %%  Day      %% Day Synthesized FPGA   %% RO   %% 0x16     %% 0x16
#SAMPLE> // Field: [7:4]      %%  VerID    %% FPGA Version           %% RO   %% 0x0      %% 0x0
#SAMPLE> // Field: [3:0]      %%  NumID    %% Number of FPGA Version %% RO   %% 0x1      %% 0x1
#SAMPLE> // End:
######################################################################################
#********************* 
#4.1.1. Pseudowire Transmit Ethernet Header Value Control
#********************* 
//# Begin: 
//# Register Full Name: Pseudowire Transmit Ethernet Header Value Control
//# RTL Instant Name  : pw_txeth_hdr
//## {FunctionName,SubFunction_InstantName_Description} 
//## RTL Instant Name and Full Name MUST be unique within a register description file 
//# Address: 0x00000000 - 0x00000007
//## The address format for these registers is 0x00000000 + Entry
//# Formula: Address +  Entry
//# Where: {$Entry(0-7): Entries store data as cache}
//# Description: This register is used as a cache for read/write the Transmit Ethernet pseudowire header (call PSN header from DA to CW) %% 
//# Each read/write command serve for specific pseudowire. %%
//# There are 8 entries (1 entry contains 128 bits) common for all pseudowire, the procedure to read/write these entries from/to DDR be described in Pseudowire Transmit Ethernet Header Procedure Control below %%
//# The header format from entry#0 to entry#7 is as follow: %%
//# {DA(6-byte),SA(6-byte),VLAN1(4-byte,optional),VLAN2(4-byte, optional),EthType(2-byte),PSN Header(4 to 96 bytes)} %%
//# Depends on specific PSN selected for each pseudowire, the EthType and PSN Header field may have the following values and formats: %%
//#
//# PSN header is MEF-8: %% 
//#		EthType:  0x88D8 %%
//#		4-byte PSN Header with format: {ECID[19:0], 0x102} where ECID is pseodowire identification for 	remote 	receive side. %%
//##
//# PSN header is MPLS:  %%
//#		EthType:  0x8847 %%
//#		4/8/12-byte PSN Header with format: {OuterLabel2[31:0](optional), OuterLabel1[31:0](optional),  InnerLabel[31:0]}   %%
//# Where InnerLabel[31:12] is pseodowire identification for remote receive side. %%
//#		Label format: {Idenfifier[19:0],Exp[2:0],StackBit,TTL[7:0]} where StackBit =  for InnerLabel %%
//##
//# PSN header is UDP/Ipv4:  %%
//#		EthType:  0x0800 %%
//#		28-byte PSN Header with format {Ipv4 Header(20 bytes), UDP Header(8 byte)} as below: %%
//#	{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0], IP_Header_Sum[31:16]} %%
//#	{IP_Iden[15:0], IP_Flag[2:0], IP_Frag_Offset[12:0]}  %%
//#	{IP_TTL[7:0], IP_Protocol[7:0], IP_Header_Sum[15:0]} %%
//#	{IP_SrcAdr[31:0]} %%
//#	{IP_DesAdr[31:0]} %%
//#	{UDP_SrcPort[15:0], UDP_DesPort[15:0]} %%
//#	{UDP_Sum[31:0] (optional)} %%
//# Case: %%
//#	IP_Protocol[7:0]: 0x11 to signify UDP  %%
//#	IP_Protocol[7:0]: 0x11 to signify UDP  %%
//#	IP_Header_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields %%
//#		{IP_Ver[3:0], IP_IHL[3:0], IP_ToS[7:0]} +%%
//#		IP_Iden[15:0] + {IP_Flag[2:0], IP_Frag_Offset[12:0]}  +%%
//#		{IP_TTL[7:0], IP_Protocol[7:0]} + %%
//#		IP_SrcAdr[31:16] + IP_SrcAdr[15:0] +%%
//#		IP_DesAdr[31:16] + IP_DesAdr[15:0]%%
//#	UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%
//#	UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%
//#	UDP_Sum[31:0] (optional): CPU calculate these fields as a temporarily for HW before plus rest fields%%
//#		IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%%
//#		IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%%
//#		IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%%
//#		IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%%
//#		IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%%
//#		IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%%
//#		IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%%
//#		IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%
//#		{8-bit zeros, IP_Protocol[7:0]} +%%
//#		UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%
//##%%
//# PSN header is UDP/Ipv6:  %%
//#	EthType:  0x86DD%%
//#	48-byte PSN Header with format {Ipv6 Header(40 bytes), UDP Header(8 byte)} as below:%%
//#	{IP_Ver[3:0], IP_Traffic_Class[7:0], IP_Flow_Label[19:0]}%%
//#	{16-bit zeros, IP_Next_Header[7:0], IP_Hop_Limit[7:0]} %%
//#	{IP_SrcAdr[127:96]}%%
//#	{IP_SrcAdr[95:64]}%%
//#	{IP_SrcAdr[63:32]}%%
//#	{IP_SrcAdr[31:0]}%%
//#	{IP_DesAdr[127:96]}%%
//#	{IP_DesAdr[95:64]}%%
//#	{IP_DesAdr[63:32]}%%
//#	{IP_DesAdr[31:0]}%%
//#	{UDP_SrcPort[15:0], UDP_DesPort[15:0]}%%
//#	{UDP_Sum[31:0]}%%
//# Case:%%
//#	IP_Next_Header[7:0]: 0x11 to signify UDP %%
//#	UDP_Sum[31:0]:  CPU calculate these fields as a temporarily for HW before plus rest fields%%
//#		IP_SrcAdr[127:112] + 	IP_SrcAdr[111:96] + 	%%
//#		IP_SrcAdr[95:80]  + IP_SrcAdr[79:64] + 	%%
//#		IP_SrcAdr[63:48]  + IP_SrcAdr[47:32] + 	%%
//#		IP_SrcAdr[31:16]  + IP_SrcAdr[15:0] + 	%%
//#		IP_DesAdr[127:112] + 	IP_DesAdr[111:96] + 	%%
//#		IP_DesAdr[95:80]  + IP_DesAdr[79:64] + 	%%
//#		IP_DesAdr[63:48]  + IP_DesAdr[47:32] + 	%%
//#		IP_DesAdr[31:16]  + IP_DesAdr[15:0] +%%
//#		{8-bit zeros, IP_Next_Header[7:0]} +%%
//#		UDP_SrcPort[15:0] +  UDP_DesPort[15:0]%%
//#	UDP_SrcPort: used as remote pseudowire identification or 0x85E if unused%%
//#	UDP_DesPort : used as remote pseudowire identification or 0x85E if unused%%
//# User can select either source or destination port for pseodowire identification. See IP/UDP standards for more description about other field. %%
//##%%
//# PSN header is MPLS over Ipv4:%%
//#	IPv4 header must have IP_Protocol[7:0] value 0x89 to signify MPLS%%
//#  After IPv4 header is MPLS labels where inner label is used for PW identification%%
//##%%
//#PSN header is MPLS over Ipv6:%%
//#	IPv6 header must have IP_Next_Header[7:0] value 0x89 to signify MPLS%%
//#  After IPv6 header is MPLS labels where inner label is used for PW identification%%
//##%%
//#PSN header (all modes) with RTP enable: RTP used for TDM PW, following PSN header, define in RFC3550%%
//#Case:%%
//#	RTPSSRC[31:0] : This is the SSRC value of RTP header%%
//#	RtpPtValue[6:0]: This is the PT value of RTP header, define in http://#www.iana.org/assignments/rtp-parameters/rtp-parameters.xml
//##
//# Width: 128
//# Register Type: {Config} 
//## Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
//# Field: [127:0]  %% TxEthPwHeadValue %% Transmit Ethernet Pseudowire Header value in each entry %% RW %% 0x0 %% 0x0
//# End:
#********************* 
#4.1.2. Pseudowire Transmit Ethernet Header Procedure Control
#********************* 
//# Begin: 
//# Register Full Name: Pseudowire Transmit Ethernet Header Value Control
//# RTL Instant Name  : pw_txeth_hdr_ctrl
//## {FunctionName,SubFunction_InstantName_Description} 
//## RTL Instant Name and Full Name MUST be unique within a register description file 
//# Address: 0x00010000
//# Description: This register is used to control read/write Pseudowire Transmit Ethernet Header Value from/to DDR
//# Width: 16
//# Register Type: {Config} 
//## Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
//# Field: [15]  %% TxEthPwStaCmd %% This is status of procedure 
//# 		1: CPU send a command to read/write PSN header values from/to DDR and waiting responsibility
//#		0: HW response that the command (read/write) is done  %% RW %% 0x0 %% 0x0
//# Field: [14]  %% TxEthPwRnwCmd %% CPU indicate command is read or write
//# 		1: read 
//#		0: write %% RW %% 0x0 %% 0x0
//# Field: [13:0]  %% TxEthPwPwId %% the Pseudowire ID that CPU working %% RW %% 0x0 %% 0x0
//# End:
#********************* 
#4.1.3. Pseudowire Transmit Ethernet Header Mode Control
#********************* 
// Begin: 
// Register Full Name: Pseudowire Transmit Ethernet Header Mode Control
// RTL Instant Name  : pw_txeth_hdr_mode
//# {FunctionName,SubFunction_InstantName_Description} 
//# RTL Instant Name and Full Name MUST be unique within a register description file 
// Address: 0x00000000 - 0x001FFF
// #The address format for these registers is 0x00000000 + PWID 
// Formula: Address +  PWID
// Where: {$PWID(0-10751): Pseudowire ID}
// Description: This register is used to control read/write Pseudowire Transmit Ethernet Header Value from/to DDR
// Width: 10
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [9]  %% TxEthPwSubVlanSel %% Tx PW Subport VLAN selection 
//		1: Select second global transmit PW subport VLAN
// 		0: Select first global transmit PW subport VLAN  %% RW %% 0x0 %% 0x0
// Field: [8:7]  %% TxEthPadMode %% this is configuration for insertion PAD in short packets
//		0: Insert PAD when total Ethernet packet length is less than 64 bytes. Refer to IEEE 802.3  
//		1: Insert PAD when control word plus payload length is less than 64 bytes
//		2: Insert PAD when total Ethernet packet length minus VLANs and MPLS outer labels(if exist) is less than 64 bytes  %% RW %% 0x0 %% 0x0
//#The reason is that networking devices often insert or remove VLANs and MPLS outer labels when packet traverse them
// Field: [6]  %% TxEthPwRtpEn %% this is RTP enable for TDM PW packet 
//		1: Enable RTP
// 		0: Disable RTP  %% RW %% 0x0 %% 0x0
// Field: [5:2]  %% TxEthPwPsnType %% this is Transmit PSN header mode working 
// 		1: PW PSN header is UDP/IPv4
//		2: PW PSN header is UDP/IPv6
//		3: PW MPLS no outer label over Ipv4 (total 1 MPLS label)
//		4: PW MPLS no outer label over Ipv6 (total 1 MPLS label)
//		5: PW MPLS one outer label over Ipv4 (total 2 MPLS label)	
//		6: PW MPLS one outer label over Ipv6 (total 2 MPLS label)	
//		7: PW MPLS two outer label over Ipv4 (total 3 MPLS label)	
//		8: PW MPLS two outer label over Ipv6 (total 3 MPLS label)	
//		Others: for other PW PSN header type %% RW %% 0x0 %% 0x0
// Field: [1:0]  %% TxEthPwNumVlan %% This is number of vlan in Transmit Ethernet packet
// 		0: no vlan
//		1: 1 vlan
//		2: 2 vlan %% RW %% 0x0 %% 0x0
// End:
//
//
#********************* 
#4.1.4. Pseudowire Per Ethernet Port statistic counters
#********************* 
#********************* 
#4.1.4.1 Transmit Ethernet port Count0-64 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count0_64 bytes packet 
// RTL Instant Name  : Eth_cnt0_64
// Address: 0x0004000(RO) %% 0x0004800(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 0 to 64 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCnt0_64 %% This is statistic counter for the packet having 0 to 64 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.4.2 Transmit Ethernet port Count65_127 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count65_127 bytes packet 
// RTL Instant Name  : Eth_cnt65_127
// Address: 0x0004002(RO) %% 0x0004802(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 65 to 127 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCnt65_127 %% This is statistic counter for the packet having 65 to 127 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.4.3 Transmit Ethernet port Count128_255 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count128_255 bytes packet 
// RTL Instant Name  : Eth_cnt128_255
// Address: 0x0004004(RO) %% 0x0004804(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 128 to 255 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCnt128_255 %% This is statistic counter for the packet having 128 to 255 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.4.4 Transmit Ethernet port Count256_511 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count256_511 bytes packet 
// RTL Instant Name  : Eth_cnt256_511
// Address: 0x0004006(RO) %% 0x0004806(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 256 to 511 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCnt256_511 %% This is statistic counter for the packet having 256 to 511 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.4.5 Transmit Ethernet port Count512_1023 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count512_1023 bytes packet 
// RTL Instant Name  : Eth_cnt512_1024
// Address: 0x0004008(RO) %% 0x0004808(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 512 to 1023 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCnt512_1024 %% This is statistic counter for the packet having 512 to 1023 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.4.6 Transmit Ethernet port Count1025_1528 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count1024_1518 bytes packet 
// RTL Instant Name  : Eth_cnt1025_1528
// Address: 0x000400A(RO) %% 0x000480A(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 1024 to 1518 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCnt1025_1528 %% This is statistic counter for the packet having 1024 to 1518 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.4.7 Transmit Ethernet port Count1529_2047 bytes packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count1519_2047 bytes packet 
// RTL Instant Name  : Eth_cnt1529_2047
// Address: 0x000400C(RO) %% 0x000480C(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having 1519 to 2047 bytes 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCnt1529_2047 %% This is statistic counter for the packet having 1519 to 2047 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.4.8 Transmit Ethernet port Count Jumbo packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count Jumbo packet 
// RTL Instant Name  : Eth_cnt_jumbo
// Address: 0x000400E(RO) %% 0x000480E(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic counter for the packet having more than 2048 bytes (jumbo)
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCntJumbo %% This is statistic counter for the packet more than 2048 bytes %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.4.9 Transmit Ethernet port Count Unicast packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count Unicast packet 
// RTL Instant Name  : Eth_cnt_Unicast
// Address: 0x0004010(RO) %% 0x0004810(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic counter for the unicast packet 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCntUnicast %% This is statistic counter for the unicast packet %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.4.10 Transmit Ethernet port Count Total packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count Total packet 
// RTL Instant Name  : eth_tx_pkt_cnt
// Address: 0x0004012(RO) %% 0x0004812(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic counter for the total packet at Transmit side 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCntTotal %% This is statistic counter total packet %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.4.11 Transmit Ethernet port Count Broadcast packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count Broadcast packet 
// RTL Instant Name  : eth_tx_bcast_pkt_cnt
// Address: 0x0004014(RO) %% 0x0004814(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic counter for the Broadcast packet at Transmit side 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCntBroadcast %% This is statistic counter broadcast packet %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.4.12 Transmit Ethernet port Count Multicast packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count Multicast packet 
// RTL Instant Name  : eth_tx_mcast_pkt_cnt
// Address: 0x0004016(RO) %% 0x0004816(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic counter for the Multicast packet at Transmit side 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCntMulticast %% This is statistic counter multicast packet %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.4.13 Transmit Ethernet port Count number of bytes of packet 
#********************* 
// Begin: 
// Register Full Name: Transmit Ethernet port Count number of bytes of packet 
// RTL Instant Name  : Eth_cnt_byte
// Address: 0x000401E(RO) %% 0x000481E(RC)
// Formula: Address + eth_port
// Where: {$eth_port(0-3): Transmit Ethernet port ID}
// Description: This register is statistic count number of bytes of packet at Transmit side 
// Width: 32
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [31:0]  %% CLAEthCntByte %% This is statistic counter number of bytes of packet %% RO %% 0x0 %% 0x0
// End:
#********************* 
#4.1.5. Pseudowire Hold Register Status
#********************* 
//# Begin: 
//# Register Full Name: Pseudowire Hold Register Status
//# RTL Instant Name  : pwe_hold_status
//# Address:  0x00C00A - 0x00C00D
//# Formula: Address +  HID
//# Where: {$HID(0-3): Hold ID}
//# Description: This register using for hold remain that more than 128bits 
//# Width: 32
//# Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
//# Field: [31:0]  %% PwHoldStatus %% Hold 32bits %% RW %% 0x0 %% 0x0
//#End:
#********************* 
#4.1.6. Pseudowire Parity Control
#********************* 
// Begin: 
// Register Full Name: Pseudowire Parity Register Control
// RTL Instant Name  : pwe_Parity_control
// Address: 0x000F000
// Description: This register using for Force Parity 
// Width: 1
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [0]  %% PwForceErr %% Force parity error enable for "Pseudowire Transmit Ethernet Header Mode Control" %% RW %% 0x0 %% 0x0
//	1: enable force 
//	0: not force
// End:
#********************* 
#4.1.7. Pseudowire Parity Disable Control
#********************* 
// Begin: 
// Register Full Name: Pseudowire Parity Disable register Control
// RTL Instant Name  : pwe_Parity_Disable_control
// Address: 0x000F001
// Description: This register using for Disable Parity 
// Width: 1
// Register Type: {Config} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [0]  %% PwDisChkErr %% Disable parity error check for "Pseudowire Transmit Ethernet Header Mode Control"%% RW %% 0x0 %% 0x0
//End:
#********************* 
#4.1.8. Pseudowire Parity sticky error
#********************* 
// Begin: 
// Register Full Name: Pseudowire Parity sticky error
// RTL Instant Name  : pwe_Parity_stk_err
// Address: 0x000F002
// Description: This register using for checking sticky error
// Width: 1
// Register Type: {Status} 
//# Field: [Bit:Bit] %%  Name           %% Description             %% Type %% SW_Reset %% HW_Reset
// Field: [0]  %% PwStkErr %% parity error for "Pseudowire Transmit Ethernet Header Mode Control" %% W1C %% 0x0 %% 0x0
//End:




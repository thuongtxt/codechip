
# History

|Revision  |Date         |Description   |
|----------|-------------|--------------|
|1.0       |Sep 06, 2016 |First version |
|1.1       |Oct 03, 2016 |- Add diagnostic guideline for interrupt PIN and UART<br>- Add PRBS gating<br>- Remove port number constrains. All of interfaces will be available on 10G image.|

# Overview

This document is to show how CLIs can be used for diagnostic/bringup phase. This will include:

* SERDES diagnostic, this will include STM/OCn/ETH SERDES at both backplane and faceplate sides.
    * Loopback
    * PRBS
    * Eye-scan
* QDR/DDR diagnostic with both hardware and software base testing. 
    * Hardware base testing: PRBS will be generated and monitored by hardware. 
    * Software base testing: March-C algorithm is used to cover test for address bus, data bus and device memory.
* CLock monitoring
* Interrupt PIN
* Sysmon for:
    * Temperature monitoring
    * Power supplies 
* SEU
* UART

Following features are not covered in this document as they are still not clear how software should interface. CLI `rd/wr` can be used to work directly on registers.

* LED

# How to start Arrive CLI prompt

Arrive CLI prompt can be started by following code:

```c 
#include "AtCliModule.h"

static AtTextUI m_textUI = NULL; /* Cache it till to avoid recreating */

static AtTextUI TextUI(void)
    {
    if (m_textUI == NULL)
        m_textUI = AtDefaultTinyTextUINew();
    return m_textUI;
    }

void ArriveTextUIStart(void)
    {
    AtCliStartWithTextUI(TextUI());
    }

void ArriveTextUIFree(void)
    {
    AtTextUIDelete(m_textUI);
    m_textUI = NULL;
    }
```
 
Calling `ArriveTextUIStart()` function from VxWorks prompt will have a the SDK prompt `AT SDK >` where CLI can be input, for example:

```
AT SDK >show driver
Driver Version  : 3.2.1.b000
Hardware Version:
+------+------------+----------------------------------+----------+
|      |            |                                  |          |
|Device|Product code|Version                           |HAL       |
|      |            |                                  |          |
+------+------------+----------------------------------+----------+
|1(*)  |0x60210051  |6.9 (on 2016/07/25), built: 1.0.56|0x49c01000|
+------+------------+----------------------------------+----------+
```

# Move the FPGA to diagnostic mode

By default, the FPGA will work in normal mode where datapath can run. For diagnostic, it would be best practice to always move image to diagnostic mode. For products that do not require this (PDH card, for example), this operation just does nothing.

* Move to diagnostic mode: `device diag enable`
* Move to normal working mode: `device diag disable`

# CLI syntax

## DDR testing

### CLIs

```
# Hardware base testing CLIs
ddr test start <ddrIds>
ddr test stop <ddrIds>
show ddr counters <ddrIds> <ro/r2c>
show ddr test <ddrIds>
ddr test force <ddrIds>
ddr test unforce <ddrIds>

# Software base March-C
ddr test addressbus <ddrIds>
ddr test databus <ddrIds>
ddr test memory <ddrIds>

# Access directly DDR memory
ddr access enable <ddrIds>
ddr rd <ddrId> <address>
ddr wr <ddrId> <address> <0xfirstDword.secondDword>
```

### Example output

Note, the following output is for demo only, running on real target will have different result. 
 
```
AT SDK >ddr test start 1
AT SDK >show ddr counters 1 r2c
+-----+---------+----------+--------+----+-----+-----+
|     |         |          |        |    |     |     |
|RamId|ErrorCorr|ErrorUCorr|ErrorCrc|Read|Write|Error|
|     |         |          |        |    |     |     |
+-----+---------+----------+--------+----+-----+-----+
|1    |N/A      |N/A       |N/A     |0   |0    |0    |
+-----+---------+----------+--------+----+-----+-----+
AT SDK >show ddr test 1
+-----+-------+----+-----+-----+------+
|     |       |    |     |     |      |
|RamId|State  |Read|Write|Error|Status|
|     |       |    |     |     |      |
+-----+-------+----+-----+-----+------+
|1    |Started|0   |0    |0    |good  |
+-----+-------+----+-----+-----+------+
AT SDK >ddr test stop 1
AT SDK >show ddr test 1
+-----+-------+----+-----+-----+------+
|     |       |    |     |     |      |
|RamId|State  |Read|Write|Error|Status|
|     |       |    |     |     |      |
+-----+-------+----+-----+-----+------+
|1    |Stopped|0   |0    |0    |good  |
+-----+-------+----+-----+-----+------+

AT SDK >ddr test addressbus 1-2
+---+--------------------+------+----------+
|   |                    |      |          |
|RAM|InitStatus          |Result|ErrorCount|
|   |                    |      |          |
+---+--------------------+------+----------+
|1  |cAtErrorDdrCalibFail|      |          |
+---+--------------------+------+----------+
|2  |OK                  |PASS  |0         |
+---+--------------------+------+----------+

AT SDK >ddr test databus 1-2
+---+--------------------+------+----------+
|   |                    |      |          |
|RAM|InitStatus          |Result|ErrorCount|
|   |                    |      |          |
+---+--------------------+------+----------+
|1  |cAtErrorDdrCalibFail|      |          |
+---+--------------------+------+----------+
|2  |OK                  |PASS  |0         |
+---+--------------------+------+----------+

AT SDK >ddr test memory 1-2
+---+--------------------+------+----------+--------------------------------------------+
|   |                    |      |          |                                            |
|Ram|InitStatus          |Result|ErrorCount|FirstErrorAddress                           |
|   |                    |      |          |                                            |
+---+--------------------+------+----------+--------------------------------------------+
|1  |cAtErrorDdrCalibFail|      |          |                                            |
+---+--------------------+------+----------+--------------------------------------------+
|2  |OK                  |FAILED|1         |0x1FFFFC1 (Bank: 7, Row: 16383, Column: 193)|
+---+--------------------+------+----------+--------------------------------------------+
```

## QDR testing

### CLIs

```
# Hardware base testing CLIs
qdr test start <qdrIds>
qdr test stop <qdrIds>
show qdr counters <qdrIds> <ro/r2c>
show qdr test <qdrIds>
qdr test force <qdrIds>
qdr test unforce <qdrIds>

# Software base March-C
qdr test aqdressbus <qdrIds>
qdr test databus <qdrIds>
qdr test memory <qdrIds>

# Access directly qdr memory
qdr access enable <qdrIds>
qdr rd <qdrId> <address>
qdr wr <qdrId> <address> <0xfirstDword.secondDword>
```

### Example output

Output format would be the same as DDR testing output.  

## SERDES

### CLIs

```
# SERDES mode
serdes mode <serdesList> <stm0/stm1/stm4/stm16/stm64/10M/100M/2500M/1G/10G>
serdes loopback <serdesList> <local/remote/release>
serdes prbs mode <serdesList> <prbs7/prbs15/prbs23/prbs31/sequence/fixedpattern>

# Force error
serdes prbs force <serdesList>
serdes prbs unforce <serdesList>

# Enabling
serdes prbs enable <serdesList>
serdes prbs disable <serdesList>

# Gating
serdes prbs duration <serdesList> <durationMs>
serdes prbs start <serdesList>
serdes prbs stop <serdesList>

# Show configuration and status
show serdes <serdesList>
show serdes prbs <serdesList>

# Eye-scan
serdes eyescan enable <lanes>
serdes eyescan disable <lanes>
serdes eyescan <serdesList>
```

Regard `<lanes>`, its ID format is `serdesId.laneId`. It can be a list of lanes. Followings are valid lane identifier:

* `1.1`: the first lane of the first SERDES
* `1.1-2.4`: all lanes of the first and second SERDES.

### Example output

`SET` CLIs will just only give success or fail indication. The `show serdes <serdesList>` will display SERDES configuration and its status.

```
AT SDK >show serdes prbs 1
+-------+--------+------+------+------------+------------+------+----------+---------+---------+----------+----------+-----+--------+-------+-------+
|       |        |      |      |            |            |      |          |         |         |          |          |     |        |       |       |
|channel|engineId|txMode|rxMode|txFixPattern|rxFixPattern|invert|forceError|txEnabled|rxEnabled|txBitOrder|rxBitOrder|alarm|sticky  |genSide|monSide|
|       |        |      |      |            |            |      |          |         |         |          |          |     |        |       |       |
+-------+--------+------+------+------------+------------+------+----------+---------+---------+----------+----------+-----+--------+-------+-------+
|eport.1|1       |prbs23|prbs23|N/A         |N/A         |dis   |dis       |en       |en       |N/A       |N/A       |none |lossSync|psn    |psn    |
+-------+--------+------+------+------------+------------+------+----------+---------+---------+----------+----------+-----+--------+-------+-------+

AT SDK >show serdes 1
+-------+--------+----+---+-------+--------------+---------------+--------------------+---------------------+--------+-------+------+----+--------------+-------------+-----+
|       |        |    |   |       |              |               |                    |                     |        |       |      |    |              |             |     |
|Port   |SerdesId|Mode|vod|pre-tap|first-post-tap|second-post-tap|rxEqualizationDcGain|rxEqualizationControl|Loopback|enabled|pll   |link|Interrupt Mask|equalizerMode|Power|
|       |        |    |   |       |              |               |                    |                     |        |       |      |    |              |             |     |
+-------+--------+----+---+-------+--------------+---------------+--------------------+---------------------+--------+-------+------+----+--------------+-------------+-----+
|eport.1|1       |ge  |N/A|N/A    |N/A           |N/A            |N/A                 |N/A                  |release |en     |locked|down|None          |dfe          |on   |
+-------+--------+----+---+-------+--------------+---------------+--------------------+---------------------+--------+-------+------+----+--------------+-------------+-----+

AT SDK >serdes eyescan enable 1.1

================================================================================

( 380 mV)  xxxxxxxxx_________________________________________________xxxxxxx
( 332 mV)  xxxxxxxx__________________________________________________xxxxxxx
( 284 mV)  xxxxxxx____________________________________________________xxxxxx
( 236 mV)  xxxxxx_____________________________________________________xxxxxx
( 188 mV)  xxxxxx______________________________________________________xxxxx
( 140 mV)  xxxxx_______________________________________________________xxxxx
(  92 mV)  xxxxx________________________________________________________xxxx
(  44 mV)  xxxx_________________________________________________________xxxx
(   0 mV)  xxx___________________________________________________________xxx
( -44 mV)  xxxx__________________________________________________________xxx
( -92 mV)  xxxxx________________________________________________________xxxx
(-140 mV)  xxxxx_______________________________________________________xxxxx
(-188 mV)  xxxxxx______________________________________________________xxxxx
(-236 mV)  xxxxxxx____________________________________________________xxxxxx
(-284 mV)  xxxxxxx____________________________________________________xxxxxx
(-332 mV)  xxxxxxxx__________________________________________________xxxxxxx
(-380 mV)  xxxxxxxxx________________________________________________xxxxxxxx

* Measured width: 58(906.2 ps)
* Measured swing: 16 (760 mV)
* Elapsed time  : 1.331 s

================================================================================
```

## Clock monitoring

### CLIs

```
debug device
clock check
```

### Example output

```
AT SDK >debug device
* Reset: Not yet
* Reset takes 0 (ms)
* RAM reset wait time 0 (ms)
FPGA clock status
========================================================
XGMII(port 1-4) Clock 156.25Mhz Value Status      : 156250  KHz | Current PPM:  0       | Result: OK
XGMII(port 5-8) Clock 156.25Mhz Value Status      : 156250  KHz | Current PPM:  0       | Result: OK
OCN Clock 155.52Mhz Value Status                  : 155520  KHz | Current PPM:  0       | Result: OK
Core 1 Clock 155Mhz Value Status                  : 155520  KHz | Current PPM:  0       | Result: OK
DDR4#1 User Clock Value Status                    : 240008  KHz | Current PPM:      N/A | Result: N/A
DDR4#2 User Clock Value Status                    : 240008  KHz | Current PPM:      N/A | Result: N/A
PCIE User Clock 62.5Mhz Value Status              : 62502   KHz | Current PPM:  34      | Result: OK
FPGA QDRNo1 User Clock 250.00Mhz Value Status     : 250008  KHz | Current PPM:  33      | Result: OK

* DDR calib done max time: 0 (ms)
AT SDK >clock check
All clocks are good
```

## Sysmon

### CLIs

```
# Temperature
device sensor thermal threshold alarmset <celsius>
device sensor thermal threshold alarmclear <celsius>
show device sensor thermal
show device sensor thermal alarm
show device sensor thermal history <r2c/ro>

# Power supplies
TBD
```

### Example output

```
AT SDK >show device sensor thermal alarm
+--------+
|        |
|overheat|
|        |
+--------+
|clear   |
+--------+
AT SDK >show device sensor thermal history r2c
+--------+
|        |
|overheat|
|        |
+--------+
|clear   |
+--------+
AT SDK >show device sensor thermal
+-------------+---------------+-------------+------------+-----------------+-----------------+
|             |               |             |            |                 |                 |
|set threshold|clear threshold|interruptMask|current temp|min recorded temp|max recorded temp|
|             |               |             |            |                 |                 |
+-------------+---------------+-------------+------------+-----------------+-----------------+
|85           |60             |None         |55          |48               |57               |
+-------------+---------------+-------------+------------+-----------------+-----------------+
```

## SEU

### CLIs

```
device sem validate <semId>
show device sem <semIdList>
show device sem alarm <semIdList>
show device sem counters <semIdList> <ro/r2c>
```

### Example outputs

```
AT SDK >device sem validate 1
+-----+------+-----------------+
|     |      |                 |
|SemId|Result|Corrected counter|
|     |      |                 |
+-----+------+-----------------+
|1    |cAtOk |1                |
+-----+------+-----------------+
AT SDK >show device sem 1
+-----+------+-----------+-----+-----------+-------------+
|     |      |           |     |           |             |
|SemId|Active|Status     |Alarm|Error count|InterruptMask|
|     |      |           |     |           |             |
+-----+------+-----------+-----+-----------+-------------+
|1    |Yes   |Observation|None |1          |None         |
+-----+------+-----------+-----+-----------+-------------+
AT SDK >show device sem alarm 1
+------+-----+--------------+-----------+---------+--------------+----------+-----------+
|      |     |              |           |         |              |          |           |
|Active|Idle |Initialization|Observation|Injection|Classification|Correction|Fatal-Error|
|      |     |              |           |         |              |          |           |
+------+-----+--------------+-----------+---------+--------------+----------+-----------+
|set   |clear|clear         |set        |clear    |clear         |clear     |clear      |
+------+-----+--------------+-----------+---------+--------------+----------+-----------+

Note: Active and Observation are usually set in normal operation
AT SDK >show device sem interrupt 1 r2c
+------+-----+--------------+-----------+---------+--------------+----------+-----------+
|      |     |              |           |         |              |          |           |
|Active|Idle |Initialization|Observation|Injection|Classification|Correction|Fatal-Error|
|      |     |              |           |         |              |          |           |
+------+-----+--------------+-----------+---------+--------------+----------+-----------+
|clear |clear|clear         |clear      |clear    |clear         |clear     |clear      |
+------+-----+--------------+-----------+---------+--------------+----------+-----------+
```

## Interrupt PINs

```
device interrupt pin trigger mode <pinIds> <edge/level-high/level-low>
device interrupt pin trigger start <pinIds>
device interrupt pin trigger stop <pinIds>
show device interrupt pin <pinIds>
```

## UART

```
device diag uart transmit <message>
device diag uart receive
```

# MRO Card

## Testing DDR

### Hardware base with PRBS generating/monitoring

```
# Start DDR testing and monitor its result
ddr test start 1-4
show ddr counters 1-4 r2c
show ddr test 1-4

# Force error during testing
ddr test force 1-4
show ddr counters 1-4 r2c
show ddr test 1-4

# Unforce error
ddr test unforce 1-4
show ddr counters 1-4 r2c
show ddr test 1-4

# Stop DDR testing
ddr test stop 1-4
```

### Software base with March-C

```
ddr test addressbus 1-4
ddr test databus 1-4
ddr test memory 1-4
```

### Show debug information

```
debug ddr 1-4
```

### Read/write memory cell

```
ddr access enable 1-4
ddr rd 1 0
ddr wr 1 0 0xAAAAAAAA.55555555
```

## Testing QDR

### Hardware base with PRBS generating/monitoring

```
# Start QDR testing and monitor its result
qdr test start 1
show qdr counters 1 r2c
show qdr test 1

# Force error during testing
qdr test force 1
show qdr counters 1 r2c
show qdr test 1

# Unforce error
qdr test unforce 1
show qdr counters 1 r2c
show qdr test 1

# Stop QDR testing
qdr test stop 1
```

### Software base with March-C

```
qdr test addressbus 1
qdr test databus 1
qdr test memory 1
```

### Show debug information

```
debug qdr 1
```

### Read/write QDR memory cell

```
qdr access enable <qdrId>
qdr rd <qdrId> <localAddress>
qdr wr <qdrId> <localAddress> <value>
```

Example:

```
qdr access enable 1
qdr wr 1 0x0 1.2.3.4.5
qdr rd 1 0x0
```

## Faceplate SERDES

There are 16 faceplate SERDES instances and have ID at CLI context from 1-16. Each SERDES can be configured to work at STM or ETH mode. The same commands will be used for all kinds of interface type.

For simple demo purpose, this section will configure all SERDES to work at STM-4 modes. Developers can flexible change to other modes.

### PRBS testing

**Prerequisite:** As PRBS will be generated/monitored from FPGA, so it is necessary to have optical loopback cable

```
# Specify SERDES mode
serdes mode 1-8 stm4 

# Select prbs mode
serdes prbs mode 1-8 prbs15

# Enable PRBS
serdes prbs enable 1-8

# Check PRBS result
show serdes prbs 1-8

# Force PRBS error, then check if error happen
serdes prbs force 1-8
show serdes prbs 1-8

# Unforce PRBS error, then check if error clear
serdes prbs unforce 1-8
show serdes prbs 1-8
```

### Loopback testing

```

# Local loopback
serdes loopback 1-8 local

# Remote loopback
serdes loopback 1-8 remote

# Release loopback
serdes loopback 1-8 release

# Show loopback status
show serdes 1-8
```

### Eye-scan

```
serdes eyescan enable 1.1-8.1
serdes eyescan 1.1-8.1
```

## MATE interfaces

There are 8 MATE SERDES instances and have ID at CLI context from `17-24`. These SERDES just can only work at STM mode.

### PRBS testing

**Prerequisite:** As PRBS will be generated/monitored from FPGA, so it is necessary to have remote loopback at the peer device.

``` 

# Select prbs mode
serdes prbs mode 17-20 prbs15

# Enable PRBS
serdes prbs enable 17-20

# Check PRBS result
show serdes prbs 17-20

# Force PRBS error, then check if error happen
serdes prbs force 17-20
show serdes prbs 17-20

# Unforce PRBS error, then check if error clear
serdes prbs unforce 17-20
show serdes prbs 17-20
```

### Loopback testing

```
# Local loopback
serdes loopback 17-20 local

# Remote loopback
serdes loopback 17-20 remote

# Release loopback
serdes loopback 17-20 release

# Show loopback status
show serdes 17-20
```

### Eye-scan

```
serdes eyescan enable 17.1-20.1
serdes eyescan 17.1-20.1
```

## 40G Backplane SERDES

Backplane have two SERDES with ID from 25 to 26. They all work at 40G mode.

### PRBS testing

**Prerequisite:** As PRBS will be generated/monitored from FPGA, so it is necessary to have remote loopback at the remote end.

```
# Select prbs mode
serdes prbs mode 25-26 prbs15

# Enable PRBS
serdes prbs enable 25-26

# Check PRBS result
show serdes prbs 25-26

# Force PRBS error, then check if error happen
serdes prbs force 25-26
show serdes prbs 25-26

# Unforce PRBS error, then check if error clear
serdes prbs unforce 25-26
show serdes prbs 25-26
```

### Loopback testing

```
# Local loopback
serdes loopback 25-26 local

# Remote loopback
serdes loopback 25-26 remote

# Release loopback
serdes loopback 25-26 release

# Show loopback status
show serdes 25-26
```

### Eye-scan

```
serdes eyescan enable 25.1-26.1
serdes eyescan 25.1-26.1
```

## SGMII for DCC/K-Byte bypassing

There are two SGMII interfaces will have ID 27 and 28. 

### PRBS testing

**Prerequisite:** As PRBS will be generated/monitored from FPGA, so it is necessary to have remote loopback at the remote end.

```
# Select prbs mode
serdes prbs mode 27-28 prbs15

# Enable PRBS
serdes prbs enable 27-28

# Check PRBS result
show serdes prbs 27-28

# Force PRBS error, then check if error happen
serdes prbs force 27-28
show serdes prbs 27-28

# Unforce PRBS error, then check if error clear
serdes prbs unforce 27-28
show serdes prbs 27-28
```

### Loopback testing

```
# Local loopback
serdes loopback 27-28 local

# Remote loopback
serdes loopback 27-28 remote

# Release loopback
serdes loopback 27-28 release

# Show loopback status
show serdes 27-28
```

### Eye-scan

```
serdes eyescan enable 27.1
serdes eyescan 27.1
```

## Soft Error Mitigation (SEM)

```
device sem validate 1
show device sem 1
show device sem alarm 1
show device sem interrupt 1 r2c
```

## Sysmon

```
show device sensor thermal alarm
show device sensor thermal history r2c
```

## Clock Monitoring

```
debug device
clock check
```

## Interrupt PIN

```
device interrupt pin trigger mode 1-2 edge
device interrupt pin trigger start 1-2
show device interrupt pin 1-2
device interrupt pin trigger stop 1-2
show device interrupt pin 1-2
```

# PDH Card

## Testing DDR

### Hardware base with PRBS generating/monitoring

```
# Start DDR testing and monitor its result
ddr test start 1-3
show ddr counters 1-3 r2c
show ddr test 1-3

# Force error during testing
ddr test force 1-3
show ddr counters 1-3 r2c
show ddr test 1-3

# Unforce error
ddr test unforce 1-3
show ddr counters 1-3 r2c
show ddr test 1-3

# Stop DDR testing
ddr test stop 1-3
```

### Software base with March-C

```
ddr test addressbus 1-3
ddr test databus 1-3
ddr test memory 1-3
```

### Show debug information

```
debug ddr 1-3
```

### Read/write memory cell

```
ddr access enable 1-3
ddr rd 1 0
ddr wr 1 0 0xAAAAAAAA.55555555
```

## 10G backplane SERDES

Backplane have two serdes ID from 1 to 2.

### PRBS testing

**Prerequisite:** As PRBS will be generated/monitored from FPGA, so it is necessary to have remote loopback at the remote end.

```
# Select prbs mode
serdes prbs mode 1-2 prbs15

# Enable PRBS
serdes prbs enable 1-2

# Check PRBS result
show serdes prbs 1-2

# Force PRBS error, then check if error happen
serdes prbs force 1-2
show serdes prbs 1-2

# Unforce PRBS error, then check if error clear
serdes prbs unforce 1-2
show serdes prbs 1-2
```

### Loopback testing

```
# Local loopback
serdes loopback 1-2 local

# Remote loopback
serdes loopback 1-2 remote

# Release loopback
serdes loopback 1-2 release

# Show loopback status
show serdes 1-2
```

### Eye-scan

```
serdes eyescan enable 1.1-2.1
serdes eyescan 1.1-2.1
```

## PDH MUX 2.5G SERDES

2.5GE interface (DIM Link) has a serdes ID 3.

### PRBS testing

**Prerequisite:** As PRBS will be generated/monitored from FPGA, so it is necessary to have remote loopback at the remote end.

```
# Select prbs mode
serdes prbs mode 3 prbs15

# Enable PRBS
serdes prbs enable 3

# Check PRBS result
show serdes prbs 3

# Force PRBS error, then check if error happen
serdes prbs force 3
show serdes prbs 3

# Unforce PRBS error, then check if error clear
serdes prbs unforce 3
show serdes prbs 3
```

### Loopback testing

```
# Local loopback
serdes loopback 3 local

# Remote loopback
serdes loopback 3 remote

# Release loopback
serdes loopback 3 release

# Show loopback status
show serdes 3
```

### Eye-scan

```
serdes eyescan enable 3.1
serdes eyescan 3.1
```

## SGMII for DCC/Kbyte interfaces

SGMII for DCC/DIM control has a serdes ID 4.

### PRBS testing

**Prerequisite:** As PRBS will be generated/monitored from FPGA, so it is necessary to have remote loopback at the remote end.

```
# Select prbs mode
serdes prbs mode 4 prbs15

# Enable PRBS
serdes prbs enable 4

# Check PRBS result
show serdes prbs 4

# Force PRBS error, then check if error happen
serdes prbs force 4
show serdes prbs 4

# Unforce PRBS error, then check if error clear
serdes prbs unforce 4
show serdes prbs 4
```

### Loopback testing

```
# Local loopback
serdes loopback 4 local

# Remote loopback
serdes loopback 4 remote

# Release loopback
serdes loopback 4 release

# Show loopback status
show serdes 4
```

### Eye-scan

```
serdes eyescan enable 4.1
serdes eyescan 4.1
```

## Clock Monitoring

```
debug device
clock check
```

## Soft Error Mitigation (SEM)

```
device sem validate 1
show device sem 1
show device sem alarm 1
show device sem interrupt 1 r2c
```

## Sysmon

```
show device sensor thermal alarm
show device sensor thermal history r2c
```

## Clock Monitoring

```
debug device
clock check
```

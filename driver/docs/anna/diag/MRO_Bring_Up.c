 
 /* from windsh to mcemon */
np_mcemon
 
/* from mcemon to winsh */
~ *
 
/* take FPGA out of reset */
~ tw re re 1 6 1
 
/* take FPGA in reset */
~ tw re re 1 6 0
 
/* check pcie link up  0 means up, -1 means down */
pcie_check_link(3)
 
/* pcie enumeration on FPGA */
pcie_config_bus(3)
 
 
/* display FPGA PCIe config space registers */
dbg_pciHeaderShow(3,1,0,0)
 
 
/* modify offset 4 to 1234 */
~ tcs mem wr.l e8000004 12345678
 
/* displkay offset 4 for one register read */
~ tcs mem rd.l e8000000 1
 
/* from windsh to mcemon */
np_mcemon
 
/* from mcemon to winsh */
~ *
 
/* take FPGA out of reset */
~ tw re re 1 6 1
 
/* take FPGA in reset */
~ tw re re 1 6 0
 
/* check pcie link up  0 means up, -1 means down */
pcie_check_link(3)
 
/* pcie enumeration on FPGA */
pcie_config_bus(3)
 
 
/* display FPGA PCIe config space registers */
dbg_pciHeaderShow(3,1,0,0)
 
 
/* modify offset 4 to 1234 */
~ tcs mem wr.l e8000004 12345678
 
/* displkay offset 4 for one register read */
~ tcs mem rd.l e8000004 1


 
 0xF00005  ==> ebc00014

 imig_rst_2x32_3 =>  3;
 imig_rst_2x32_2 =>  2;
 imig_rst_2x64_1 =>  1;
 imig_rst_2x64_0 =>  0;

0xF00050  ===> ebc00140
 {
  4'h0,
  //For SysMon
  2'h0,
  ot_out,
  vbram_alarm_out,
  //For SysMon
  vccaux_alarm_out,
  vccint_alarm_out,
  user_temp_alarm_out,
  alarm_out,
  //Sys PLL
  3'h0,
  !syspll_locked,
  //MIG QDR  User Reset
  3'h0,
  qdr1_rst,
  //MIG DDR User Reset
  ddr4_rst,
  ddr3_rst,
  ddr2_rst,
  ddr1_rst,
  //MIG QDR  Calib
  3'h0,
  !qdr1_done,
  //MIG DDR Calib
  !ddr4_done,
  !ddr3_done,
  !ddr2_done,
  !ddr1_done
  }
  
  
  
  #Serdes test
//Diag enable
~ tcs mem wr.l ebc0010c ffffff1f


AT SDK >serdes mode 1 stm16
//Trigger request
~ tcs mem rd.l ebd80200 1
~ tcs mem wr.l ebd80200 1010
~ tcs mem wr.l ebd80200 1110
//Checking if mode is applied
~ tcs mem rd.l ebd80200 1
~ tcs mem rd.l ebd80240 1
~ tcs mem wr.l ebd80240 1
//Stop PRBS after changing serdes mode
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebc00110 1
~ tcs mem wr.l ebc00110 2
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebc00114 1
~ tcs mem wr.l ebc00114 0
~ tcs mem rd.l ebc00110 1
~ tcs mem wr.l ebc00110 2
//SERDES mode set done

//Serdes mode set to OC-48
~ tcs mem rd.l ebd80200 1
~ tcs mem wr.l ebd80200 1010
~ tcs mem wr.l ebd80200 1110
~ tcs mem rd.l ebd80200 1
~ tcs mem rd.l ebd80240 1
~ tcs mem wr.l ebd80240 1
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebc00110 1
~ tcs mem wr.l ebc00110 2
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebc00114 1
~ tcs mem wr.l ebc00114 0
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebc00114 1
~ tcs mem wr.l ebc00114 0
~ tcs mem rd.l ebc00110 1
~ tcs mem wr.l ebc00110 2
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebc00110 1
~ tcs mem wr.l ebc00110 2
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebc00114 1
~ tcs mem wr.l ebc00114 0
~ tcs mem rd.l ebc00110 1
~ tcs mem wr.l ebc00110 2
//show serdes 1

~ tcs mem rd.l ebd8002c 1
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebd80240 1
~ tcs mem rd.l ebd80008 1
~ tcs mem rd.l ebd80008 1
~ tcs mem rd.l ebd80038 1
~ tcs mem rd.l ebd80050 1


def genRegForSerdes(serdesId):
    groupId = serdesId / 4
    localId = serdesId % 4
    baseAddress = 0x00f60000
    groupOffset = 0x2000 * groupId
    portOffset = localId * 0x400

    def portGroupAddressWithLocalAddress(localAddress):
        return baseAddress + groupOffset + localAddress

    bitRegInfo = {"RXCDRHOLD": 0x0017,
                  "RXLPMEN": 0x000E,
                  "RXLPMHFOVRDEN":0x0018,
                  "RXLPMLFKLOVRDEN":0x0019,
                  "RXOSOVRDEN":0x001A,
                  "RXLPMOSHOLD":0x0030,
                  "RXLPMOSOVRDEN":0x0031,
                  "RXLPMGCHOLD":0x0032,
                  "RXLPMGCOVRDEN":0x0033}

    fieldRegInfo = {"RXLPM_OS_CFG1":0x1038,
                    "RXLPM_GC_CFG":0x1039}

    # Print formula
    for regName, regBase in bitRegInfo.iteritems():
        print ("%s" % regName).ljust(20) + ":" + ("wr 0x%08x <0/1> bit%d" % (portGroupAddressWithLocalAddress(regBase), localId))

    # DRP reg
    drpRegs = {"RX_CM_TRIM":(0x61, "bit5_2", 0xA)}
    for regName, regInfo in drpRegs.iteritems():
        regBase, bitField, _ = regInfo
        localAddress = regBase + portOffset
        print ("%s" % regName).ljust(20) + ":" + ("wr 0x%08x <value> %s" % (portGroupAddressWithLocalAddress(localAddress), bitField))

    for regName, regBase in fieldRegInfo.iteritems():
        localAddress = regBase + portOffset
        print ("%s" % regName).ljust(20) + ":" + "wr 0x%08x <value>" % (portGroupAddressWithLocalAddress(localAddress))

    # Print default setting
    default = {"RXLPMEN": 1, "RXCDRHOLD": 1, "RXLPMHFOVRDEN": 1, "RXLPMLFKLOVRDEN": 1, "RXLPMOSOVRDEN": 1,
               "RXLPMGCOVRDEN": 1}
    print ""
    print "# Example: "
    for regName, defaultValue in default.iteritems():
        regBase = bitRegInfo[regName]
        print "wr 0x%08x %d bit%d" % (portGroupAddressWithLocalAddress(regBase), defaultValue, localId)

    for regName, regInfo in drpRegs.iteritems():
        regBase, bitField, defaultValue = regInfo
        localAddress = regBase + portOffset
        print "wr 0x%08x 0x%X %s" % (portGroupAddressWithLocalAddress(localAddress), defaultValue, bitField)

def Main():
    for serdesId in range(16):
        print "h2. Port %d" % (serdesId + 1)
        print ""
        print "bc.. "
        genRegForSerdes(serdesId)
        print ""

if __name__ == '__main__':
    Main()


1) The card will come up with U-Boot first and the display will be as shown below. To jump into the U-Boot prompt, press any key before the countdown
"Hit any key to stop autoboot:  0" reaches 0.

U-Boot 2011.03 (Sep 07 2016 - 08:48:11)

CPU0:  P2020, Version: 2.1, (0x80e20021)
Core:  E500, Version: 5.1, (0x80211051)
Clock Configuration:
       CPU0:1200 MHz, CPU1:1200 MHz, 
       CCB:600  MHz,
       DDR:400  MHz (800 MT/s data rate) (Asynchronous), LBC:75   MHz
L1:    D-cache 32 kB enabled
       I-cache 32 kB enabled
Board: P2020TCS 0D (36-bit addrmap)   CPLD_VERSION: 0xb90115
Reset_Reason: (0x4d) Remap_Reset_Reason: (0x4d)  LOCAL_HARD_REQUEST
CPLD_BOOT_BANK: 0x1   soft_swap: 0xa       Watchdog enabled
I2C:   ready
DRAM:  DDR: 2 GiB (DDR3, 64-bit, CL=6, ECC on)
Flash: 128 MiB
L2:    512 KB enabled
MMC:  FSL_ESDHC: 0
IDP: parsing mfg data ...
        keep ethaddr=00:23:8A:F2:79:C0
IDP: parsing sw data ...
        keep hostname=NodeName
        keep ipaddr=47.134.5.102
        keep serverip=47.134.67.54
        keep gatewayip=47.134.4.1
        keep netmask=255.255.252.0
        OK -- no changes
In:    serial
Out:   serial
Err:   serial
Net:   eTSEC1: No support for PHY id 600d84a2; assuming generic
eTSEC2: No support for PHY id 600d84a2; assuming generic
eTSEC3: No support for PHY id 600d84a2; assuming generic
eTSEC1, eTSEC2, eTSEC3 [PRIME]
COLD/PowerOn restarts. Put Robo into reset.
Put Robo2 into reset 
9
######################################################################
##########  Power-On Reset POST script [CTL-X to abort]
######################################################################

##########  idp read sw

   TAG         VALUE
  -----  ------------------------
   NN    NodeName
   IP    47.134.5.102
   FTP   47.134.67.54
   GW    47.134.4.1
   NM    255.255.252.0
   CS    543C00BB
  -----  ------------------------


##########  idp read hw

   TAG         VALUE
  -----  ------------------------
   BC    PROTOE
   BP    NTK536BA
   CC    PROTOCLEIE
   CHWR  04
   CPEC  NTK536BE
   CRDP  0181

   CSWR  01
   ER    64
   ETMP1 73500,69500,80500,76500
   ETMP2 75500,71500,82500,78500
   HI    0x00000192
   MP    0x10002003
   NCPT  01
   PI    P12
   RSV1  0x0000
   RSV2  0x0000
   SSRL  00110AA
   SVER  01
   SW    001
   NROBO 2
   HWRN  04
   BS    NNTMRT0C85N5
   EA    00238AF279C0
   MANS  332
   WK    18
   MD    2015
   MN    NTK536BA-810
   MR    006
   MS    NNTMRT0C85MR
   CS    486FE0D0
  -----  ------------------------


##########  mmcinfo
Device: FSL_ESDHC
Manufacturer ID: fe
OEM: 14e
Name: MMC08 
Tran Speed: 25000000
Rd Block Len: 512
MMC version 4.0
High Capacity: Yes
Capacity: 3900702720
Bus Width: 1-bit

##########  mmcrstn
###### The MMC RST_n_FUNCTION has already been permanently enabled. ######

##########  fpga_download
##########  Load FPGA ...
##########  FPGA Download Done.

##########  bootchain show
preferred chain: chain1
   active chain: chain1 [preferred]
           post: post1 (0xefe80000)
           kern: kern1 (0xedc00000) 
           attempt 1 out of 3

##########  version

U-Boot 2011.03 (Sep 07 2016 - 08:48:11)
powerpc-e500-linux-gnuspe-gcc (GCC) 4.5.1
GNU ld (GNU Binutils) 2.20.1.20100303

######################################################################
##########  passed
######################################################################

setting new bootargs:rw  console=ttyS0,38400 mem=1887M
Un-Protected 1 sectors
Un-Protected 1 sectors
Erasing Flash...
. done
Erased 1 sectors
Writing ENVI1 ... 9....8....7....69....8....7....6....5....4....3....2....1....done
Protected 1 sectors
Protected 1 sectors
Hit any key to stop autoboot:  0 
ROBO is taken out of reset!
Robo2 is being taken of of reset 




2) You need to be U-Boot to program the IP address. As shown below, the IP, FTP, GW(gateway), and NM (netmask) is stored in a piece of hardware called the IDP.
To read the idp, enter "idp read sw" in the U-Boot prompt and you will see:

   TAG         VALUE
  -----  ------------------------
   NN    NodeName
   IP    47.134.5.102
   FTP   47.134.67.54
   GW    47.134.4.1
   NM    255.255.252.0
   CS    543C00BB
  -----  ------------------------
To program values, type �idp set <TAG> <value>�
To save those values into the device, enter idp write

3) Make sure the boottype is default. You can check by enter 'printenv' in the U-Boot prompt, and you will see the following:
BADKERN=0
CURRFPGA=fpga1
CURRKERN=kern1
CURRPOST=post1
FULLCOREFILE=1
RESCUE_CM1_GW=172.16.233.1
RESCUE_CM1_IP=172.16.233.214
RESCUE_CM1_NM=255.255.255.255
RESCUE_CM2_GW=172.16.233.1
RESCUE_CM2_IP=172.16.233.216
RESCUE_CM2_NM=255.255.255.255
RESCUE_FTP=0.0.0.0
RESCUE_NB=169.254
baudrate=38400
bootargs=rw  console=ttyS0,38400 mem=1887M
bootcmd=run flashcopycmd;ipdata=$ipaddr:$serverip:$gatewayip:$netmask:$hostname:$netdev:off panic=10 $bootoptions; bootm $loadaddr
bootdelay=3
bootfile=uImage
boottype=default
consoledev=ttyS0
eth1addr=00:E0:0C:02:01:FD
eth2addr=00:E0:0C:02:02:FD
eth3addr=00:E0:0C:02:03:FD
ethact=eTSEC3
ethaddr=00:23:8A:F2:79:C0
ethprime=eTSEC3
fileaddr=10000000
filesize=10D3A94
flashcopycmd=setenv kern_addr 0; bootchain show-kern; if test $CURRKERN = kern0 ; then setenv kern_addr 0xec000000; fi ; if test $CURRKERN = kern1 ; then setenv kern_addr 0xedc00000; fi ; echo Booting from $CURRKERN ($kern_addr), copying kernel to RAM ; cp.b $kern_addr $loadaddr 0x1c00000; 
gatewayip=47.134.4.1
hostname=NodeName
initrd_high=0x20000000
ipaddr=47.134.5.102
lastboot_release=rel_ome6500_12.10.1_bld002
loadaddr=1000000
loads_echo=1
mem=0k
memctl_intlv_ctl=2
netmask=255.255.252.0
nfsboot=setenv bootargs root=/dev/nfs rw nfsroot=$serverip:$rootpath ip=$ipaddr:$serverip:$gatewayip:$netmask:$hostname:$netdev:off console=$consoledev,$baudrate $othbootargs;tftp $loadaddr $bootfile;tftp $fdtaddr $fdtfile;bootm $loadaddr - $fdtaddr
perf_mode=stable
ramboot=setenv bootargs root=/dev/ram rw console=$consoledev,$baudrate $othbootargs;tftp $ramdiskaddr $ramdiskfile;tftp $loadaddr $bootfile;tftp $fdtaddr $fdtfile;bootm $loadaddr $ramdiskaddr $fdtaddr
rootpath=/opt/nfsroot
serverip=47.134.67.54
stderr=serial
stdin=serial
stdout=serial
tftpflash=tftpboot $loadaddr $uboot; protect off 0xeff80040 +$filesize; erase 0xeff80040 +$filesize; cp.b $loadaddr 0xeff80040 $filesize; protect on 0xeff80040 +$filesize; cmp.b $loadaddr 0xeff80040 $filesize
uboot=u-boot.bin

Environment size: 2075/8187 bytes


If the boottype is not default, you can set the boottype by entering 'setenv boottype default'. To save that value into flash, type 'savee'


4) After you have done everything in U-Boot that is required to boot (the steps listed above), you can boot into the Linux kernel on the disk by typing 'boot'.

5) Once you see the dialog ((none) login: ), you can login. The login name is 'root'. The password is disabled, so when you see the password prompt (password: ), simply press 'Enter'. You should see this:
!!! This is a private network. Any unauthorized access or use will lead to prosecution!!!
(none)> 

6) To ping and transfer files from the network that you have set up, run the following command: 'debug_eth.sh'. You should see the following:
[  159.722957] ADDRCONF(NETDEV_UP): eth2: link is not ready
[  161.726579] PHY: mdio@fffe24520:18 - Link is Up - 100/Full
[  161.743968] ADDRCONF(NETDEV_CHANGE): eth2: link becomes ready
eth2      Link encap:Ethernet  HWaddr 00:23:8A:F2:79:C1  
          inet addr:47.134.5.102  Bcast:47.134.7.255  Mask:255.255.252.0
          inet6 addr: fe80::223:8aff:fef2:79c1/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:1 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:0 (0.0 B)  TX bytes:90 (90.0 B)
          Base address:0xc000 

where inet addr, Bcast, and Mask values are specific to your IP, Broadcast and Mask values.

7) To transfer files from a server to the shelf, you may use ftp, scp, rsync. For scp, you can do:
scp <user>@<server_ip>:<location of file on server> <location on card>


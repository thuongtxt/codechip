## Revision History [Revision History]

|Revision|Date|Author|Description|
|--------|----|------|-----------|
|1.0|2018-03-02|Project|Initial version|




##AF_CRYPTRD
####Register Table

|Name|Address|
|-----|-----|
|`Sys_MaxPktSize`|`0x100`|
|`Sys_FreePkt`|`0x120`|
|`Sys_DonePkt`|`0x140`|
|`Flow_CryptConfiguration`|`0x2000`|
|`Flow_CryptKeyWord0`|`0x3000`|
|`Flow_CryptKeyWord1`|`0x3400`|
|`Flow_CryptKeyWord2`|`0x3800`|
|`Flow_CryptKeyWord3`|`0x3c00`|
|`Flow_CryptSalt`|`0x4000`|
|`Flow_MSBSeq`|`0x4400`|
|`Pkt_Trigger`|`0x1000_0000`|
|`Dat_InStream`|`0x1100_0000`|
|`Dat_InStream`|`0x1200_0000`|


###Sys_MaxPktSize

* **Description**           

Maximum IP packet size to be written


* **RTL Instant Name**    : `upen_maxpsize`

* **Address**             : `0x100`

* **Formula**             : `0x100`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`max_pkt`| Maximum IP packet size to be written in per 2Kbyte unit: 0x00 : 0 Kbyte 0x01 : 2 Kbyte 0x02 : 4 Kbyte| `RW`| `0x0`| `0x0 End: Begin:`|

###Sys_FreePkt

* **Description**           

Current Free Space in Engine in Pkt unit


* **RTL Instant Name**    : `upen_sysfree`

* **Address**             : `0x120`

* **Formula**             : `0x120`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`freesize`| Current Free Space in Engine in Pkt unit| `RO`| `0x0`| `0x0 End: Begin:`|

###Sys_DonePkt

* **Description**           

Current Done Pkt number


* **RTL Instant Name**    : `upen_donepkt`

* **Address**             : `0x140`

* **Formula**             : `0x140`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:8]`|`unused`| *n/a*| `RW`| `0x0`| `0x0`|
|`[7:0]`|`donecnt`| Done pkt count| `RO`| `0x0`| `0x0 End: Begin:`|

###Flow_CryptConfiguration

* **Description**           

Configure per flow cryto information


* **RTL Instant Name**    : `upen_flowcryptcfg`

* **Address**             : `0x2000`

* **Formula**             : `0x2000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:24]`|`cfgtaglen`| Configure ICV length mode: 0x00 : 8 byte 0x01 : 12 byte 0x02 : 16 byte Other: Reserved| `RW`| `0x0`| `0x0`|
|`[23:16]`|`cfgseqmode`| Choosing between Extended sequence mode or normal sequence mode: 0x00 : Normal 0x01 : Extended sequence mode (ESN) Other: Reserved| `RW`| `0x0`| `0x0`|
|`[15:8]`|`cfgcryptmode`| Cryto mode : 0x00 : Encr AES CBC 0x01 : Decr AES CBC 0x02 : Encr AES GCM 0x03 : Decr AES GCM Other: Reserved| `RW`| `0x0`| `0x0`|
|`[7:0]`|`cfgkeymode`| Key mode of crypto Engine: 0x00 : 128 bit AES 0x01 : 192 bit AES 0x02 : 256 bit AES Other: Reserved| `RW`| `0x0`| `0x0 End: Begin:`|

###Flow_CryptKeyWord0

* **Description**           

Configure first 32 bit LSB of Crypto Key Value


* **RTL Instant Name**    : `upen_flowkeywr0`

* **Address**             : `0x3000`

* **Formula**             : `0x3000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cryptkeywr0`| first 32 bit LSB of 256 bit Key| `RW`| `0x0`| `0x0 End: Begin:`|

###Flow_CryptKeyWord1

* **Description**           

Configure second 32 bit LSB of Crypto Key Value


* **RTL Instant Name**    : `upen_flowkeywr1`

* **Address**             : `0x3400`

* **Formula**             : `0x3400`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cryptkeywr1`| second 32 bit LSB of 256 bit Key| `RW`| `0x0`| `0x0 End: Begin:`|

###Flow_CryptKeyWord2

* **Description**           

Configure third 32 bit LSB of Crypto Key Value


* **RTL Instant Name**    : `upen_flowkeywr2`

* **Address**             : `0x3800`

* **Formula**             : `0x3800`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cryptkeywr2`| third 32 bit LSB of 256 bit Key| `RW`| `0x0`| `0x0 End: Begin:`|

###Flow_CryptKeyWord3

* **Description**           

Configure 32 bit MSB of Crypto Key Value


* **RTL Instant Name**    : `upen_flowkeywr3`

* **Address**             : `0x3c00`

* **Formula**             : `0x3c00`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cryptkeywr3`| 32 bit MSB of 256 bit Key| `RW`| `0x0`| `0x0 End: Begin:`|

###Flow_CryptSalt

* **Description**           

Configure Salt value


* **RTL Instant Name**    : `upen_cryptsalt`

* **Address**             : `0x4000`

* **Formula**             : `0x4000`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`cryptsalt`| Configure Salt Value| `RW`| `0x0`| `0x0 End: Begin:`|

###Flow_MSBSeq

* **Description**           

Configure Salt value


* **RTL Instant Name**    : `upen_msbseq`

* **Address**             : `0x4400`

* **Formula**             : `0x4400`

* **Where**               : 

* **Width**               : `32`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[31:00]`|`msbseq`| Configure 32 bit MSB of sequence in Extended sequence mode| `RW`| `0x0`| `0x0 End: Begin:`|

###Pkt_Trigger

* **Description**           

Configure Salt value


* **RTL Instant Name**    : `upen_pkttriger`

* **Address**             : `0x1000_0000`

* **Formula**             : `0x1000_0000`

* **Where**               : 

* **Width**               : `256`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[255:48]`|`reserved`| *n/a*| `WO`| `0x0`| `0x0`|
|`[47:32]`|`flowid`| Flow ID of packet| `WO`| `0x0`| `0x0`|
|`[31:16]`|`payloadofs`| offset from start of packet to payload to be encrypted/descrypted| `WO`| `0x0`| `0x0`|
|`[15:00]`|`pktlen`| Packet Length| `WO`| `0x0`| `0x0 End: Begin:`|

###Dat_InStream

* **Description**           

Configure Salt value


* **RTL Instant Name**    : `upen_InStream`

* **Address**             : `0x1100_0000`

* **Formula**             : `0x1100_0000`

* **Where**               : 

* **Width**               : `256`
* **Register Type**       : `Config`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[255:00]`|`indata`| Write Data to engine| `WO`| `0x0`| `0x0 End: Begin:`|

###Dat_InStream

* **Description**           

Configure Salt value


* **RTL Instant Name**    : `upen_InStream`

* **Address**             : `0x1200_0000`

* **Formula**             : `0x1200_0000`

* **Where**               : 

* **Width**               : `256`
* **Register Type**       : `Status`
* **Bit Field Detail**    : 

|Bit Field |Name|Description|Type|Reset|Default|
|----------|----|-----------|----|-----|-------|
|`[255:00]`|`outdata`| Read Data from Engine| `RO`| `0x0`| `0x0 End:`|

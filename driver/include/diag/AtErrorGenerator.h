/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Diagnostic
 * 
 * File        : AtDiagErrorGenerator.h
 * 
 * Created Date: Mar 21, 2016
 *
 * Description : Provide API to force error at serial interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDIAGERRORGENERATOR_H_
#define _ATDIAGERRORGENERATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtObject.h"
#include "AtChannelClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtErrorGenerator
 * @{
 */

/** @brief Error generator */
typedef struct tAtErrorGenerator *AtErrorGenerator;

/** @brief Error generator modes */
typedef enum eAtErrorGeneratorMode
    {
    cAtErrorGeneratorModeInvalid,   /**< Invalid mode */
    cAtErrorGeneratorModeOneshot,   /**< Oneshot mode */
    cAtErrorGeneratorModeContinuous /**< Continuous mode */
    }eAtErrorGeneratorMode;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Error type to generate. Classes that support error generate will define their
 * own error type to generate */
eAtRet AtErrorGeneratorErrorTypeSet(AtErrorGenerator self, uint32 errorType);
uint32 AtErrorGeneratorErrorTypeGet(AtErrorGenerator self);

/* Generating mode */
eAtRet AtErrorGeneratorModeSet(AtErrorGenerator self, eAtErrorGeneratorMode mode);
eAtErrorGeneratorMode AtErrorGeneratorModeGet(AtErrorGenerator self);

/* Number of of errors to generate */
eAtRet AtErrorGeneratorErrorNumSet(AtErrorGenerator self, uint32 errorNum);
uint32 AtErrorGeneratorErrorNumGet(AtErrorGenerator self);

/* Generate error with specified rate */
eAtRet AtErrorGeneratorErrorRateSet(AtErrorGenerator self, eAtBerRate errorRate);
eAtBerRate AtErrorGeneratorErrorRateGet(AtErrorGenerator self);

/* Start/stop generating */
eAtRet AtErrorGeneratorStart(AtErrorGenerator self);
eAtRet AtErrorGeneratorStop(AtErrorGenerator self);
eBool AtErrorGeneratorIsStarted(AtErrorGenerator self);

#ifdef __cplusplus
}
#endif
#endif /* _ATDIAGERRORGENERATOR_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Diagnostic
 * 
 * File        : AtQuerier.h
 * 
 * Created Date: Jun 17, 2017
 *
 * Description : Querier
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATQUERIER_H_
#define _ATQUERIER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtDebugger.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtQuerier * AtQuerier;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void AtQuerierProblemsQuery(AtQuerier self, AtObject object, AtDebugger debugger);

#ifdef __cplusplus
}
#endif
#endif /* _ATQUERIER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtPmParam.h
 * 
 * Created Date: Mar 18, 2015
 *
 * Description : PM parameter
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPMPARAM_H_
#define _ATPMPARAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSur.h"
#include "AtPmRegister.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
const char *AtPmParamName(AtPmParam self);
AtSurEngine AtPmParamEngineGet(AtPmParam self);
uint32 AtPmParamTypeGet(AtPmParam self);

/* Registers */
AtPmRegister AtPmParamCurrentSecondRegister(AtPmParam self);
AtPmRegister AtPmParamCurrentPeriodRegister(AtPmParam self);
AtPmRegister AtPmParamPreviousPeriodRegister(AtPmParam self);
AtPmRegister AtPmParamCurrentDayRegister(AtPmParam self);
AtPmRegister AtPmParamPreviousDayRegister(AtPmParam self);
uint8 AtPmParamNumRecentPeriodRegisters(AtPmParam self);
AtPmRegister AtPmParamRecentRegister(AtPmParam self, uint8 recentPeriod);

/* Thresholds used for products that support software base Surveillance Module. */
eAtRet AtPmParamPeriodThresholdSet(AtPmParam self, uint32 threshold);
uint32 AtPmParamPeriodThresholdGet(AtPmParam self);
eAtRet AtPmParamDayThresholdSet(AtPmParam self, uint32 threshold);
uint32 AtPmParamDayThresholdGet(AtPmParam self);

#endif /* _ATPMPARAM_H_ */


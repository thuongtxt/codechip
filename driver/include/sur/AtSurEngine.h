/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtSurEngine.h
 * 
 * Created Date: Mar 18, 2015
 *
 * Description : Surveillance engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSURENGINE_H_
#define _ATSURENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannelClasses.h"
#include "AtModuleSur.h"
#include "AtPmParam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtSurEngine
 * @{
 */

/**
 * @brief SDH line PM param counters
 */
typedef struct tAtSurEngineSdhLinePmCounters
    {
    int32 sefsS; /**< Section Severely Error Framing Seconds */
    int32 cvS;   /**< Section Coding Violation */
    int32 esS;   /**< Section Error Seconds */
    int32 sesS;  /**< Section Severely Error Seconds */
    int32 cvL;   /**< Near-end Line Code Violation */
    int32 esL;   /**< Near-end Line Error Seconds */
    int32 sesL;  /**< Near-end Line Severely Error Seconds */
    int32 uasL;  /**< Near-end Line Unavailable Seconds */
    int32 fcL;   /**< Near-end Line Failure Counts */
    int32 cvLfe; /**< Far-end Line Code Violation */
    int32 esLfe; /**< Far-end Line Error Seconds */
    int32 sesLfe;/**< Far-end Line Severely Error Seconds */
    int32 uasLfe;/**< Far-end Line Unavailable Seconds */
    int32 fcLfe; /**< Far-end Line Failure Counts */
    } tAtSurEngineSdhLinePmCounters;

/**
 * @brief SDH path PM param counters
 */
typedef struct tAtSurEngineSdhPathPmCounters
    {
    int32 ppjcPdet;    /**< Positive Pointer Justification Count - STS/VT Path Detected */
    int32 npjcPdet;    /**< Negative Pointer Justification Count - STS/VT Path Detected */
    int32 ppjcPgen;    /**< Positive Pointer Justification Count - STS/VT Path Generated */
    int32 npjcPgen;    /**< Negative Pointer Justification Count - STS/VT Path Generated */
    int32 pjcDiff;     /**< Pointer Justification Count Difference */
    int32 pjcsPdet;    /**< Pointer Justification Count Seconds - STS/VT Path Detected */
    int32 pjcsPgen;    /**< Pointer Justification Count Seconds - STS/VT Path Generated */
    int32 cvP;         /**< Near-end STS/VT Path Code Violation */
    int32 esP;         /**< Near-end STS/VT Path Error Seconds */
    int32 sesP;        /**< Near-end STS/VT Path Severely Error Seconds */
    int32 uasP;        /**< Near-end STS/VT Path Unavailable Seconds */
    int32 fcP;         /**< Near End STS/VT Path Failure Counts */
    int32 cvPfe;       /**< Far-end STS/VT Path Coding Violations. Also Far End Block Error (FEBE) */
    int32 esPfe;       /**< Far-end STS/VT Path Error Seconds */
    int32 sesPfe;      /**< Far-end STS/VT Path Severely Error Seconds */
    int32 uasPfe;      /**< Far-end STS/VT Path Unavailable Seconds */
    int32 fcPfe;       /**< Far-end STS/VT Path Failure Counts */
    int32 efsP;        /**< Error-Free Second count. Derived from ES-P.*/
    int32 efsPfe;      /**< Far End STS path Error-Free Second count. Derived from ES-PFE.*/
    int32 asP;         /**< Path Available Second count. Derived from UAS-P.*/
    int32 asPfe;       /**< Far End Path Available Second count. Derived from UAS-PFE.*/
    int32 ebP;         /**< Path Errored Block Count. A block in which one or more bits are in error..*/
    int32 bbeP;        /**< Path Background Block Error Count. An errored block not occurring as part of an SES.*/
    int32 esrP;        /**< Path Errored Second Ratio. The ratio of ES to total seconds in available time during a fixed measurement interval. This parameter is applicable to both paths and connections.*/
    int32 sesrP;       /**< Path Severely Errored Second Ratio. The ratio of SES to total seconds in available time during a fixed measurement interval. This parameter is applicable to both paths and connections.*/
    int32 bberP;       /**< Path Background Block Error Ratio. The ratio of Background Block Errors (BBE) to total blocks in  available  time  during  a  fixed  measurement  interval.  The  count  of  total  blocks excludes all blocks during SESs. This parameter is applicable only to paths.*/
    }tAtSurEngineSdhPathPmCounters;

/**
 * @brief PDH DE3 PM param counters
 */
typedef struct tAtSurEnginePdhDe3PmCounters
    {
    int32 cvL;     /**< Near-end Line Coding Violation */
    int32 esL;     /**< Near-end Line Error Seconds */
    int32 sesL;    /**< Near-end Line Severely Error Seconds */
    int32 lossL;   /**< Near-end Line LOS Seconds */
    int32 cvpP;    /**< Near-end Path Code Violation */
    int32 cvcpP;   /**< Near-end Path C-bit Code Violation */
    int32 espP;    /**< Near-end Path Error Seconds */
    int32 escpP;   /**< Near-end C-bit Error Seconds */
    int32 sespP;   /**< Near-end Severely Error Seconds */
    int32 sescpP;  /**< Near-end C-bit Severely Error Seconds */
    int32 sasP;    /**< Near-end Path Service Affecting Seconds */
    int32 aissP;   /**< Near-end Path AIS Seconds */
    int32 uaspP;   /**< Near-end Path Unavailable Seconds */
    int32 uascpP;  /**< Near-end C-bit Unavailable Seconds */
    int32 cvcpPfe; /**< Far-end C-bit Coding Violation*/
    int32 escpPfe; /**< Far-end C-bit Error Seconds */
    int32 sescpPfe;/**< Far-end C-bit Severely Error Seconds */
    int32 sascpPfe;/**< Far-end C-bit Service Affecting Seconds */
    int32 uascpPfe;/**< Far-end C-bit Unavailable Seconds */
    int32 esbcpPfe;/**< Far-end C-bit Error Seconds Type B,Error Second type B. This parameter is a count of 1-second intervals containing more than one but less than 44 M-frames with the three FEBE bits not all set to one and no far-end SEF/AIS defects. */
    int32 esaL;    /**< Errored Second-Line type A. This parameter is a count of 1-second intervals containing one BPV or EXZ and no LOS defect. */
    int32 esbL;    /**< Errored Second-Line type B. This parameter is a count of 1-second intervals containing more than one but less than 44 BPV plus EXZ and no LOS defect. */
    int32 esapP;   /**< Errored Second type A, P-bit parity. This parameter is the count of 1-second intervals containing exactly one P-bit parity errors and no SEF or AIS-L defect. It is defined for both DS3 application. */
    int32 esacpP;  /**< Errored Second type A, CP-bit parity. This parameter is the count of 1-second intervals containing one CP-bit parity errors and no SEF or AIS-L defect. It is defined for C-bit parity DS3 application. */
    int32 esbpP;   /**< Errored Second type B, P-bit parity. This parameter is the count of 1-second intervals containing more than one but less than 44 P-bit parity errors and no SEF or AIS-L defect. It is defined for both DS3 application. */
    int32 esbcpP;  /**< Errored Second type B, CP-bit parity. This parameter is the count of 1-second intervals containing more than one but less than 44 CVCP-Ps and no SEF or AIS-L defect. It is defined for C-bit parity DS3 application. */
    int32 fcP;     /**< Near-end Failure Count-Path. This parameter is a count of the number of occurrences of near-end path failure events, with the failure event defined as follows: a near-end path failure event begins when either an LOF or AIS failure is declared and ends when both LOF and AIS failures are clear.*/
    int32 esacpPfe;/**< Error Second type A. This parameter is a count of 1-second intervals containing one M-frames with the three FEBE bits not all set to one and no far-end SEF/AIS defects. */
    int32 fccpPfe; /**< Far-end Failure Count-Path. This parameter is a count of the number of occurrences of far-end path failure events, with the far- end failure event defined as follows: a far-end path failure event begins when an RAI failure is declared and ends when the RAI failure is cleared. The FCCP-PFE applies only to the C-Bit Parity application. */
    }tAtSurEnginePdhDe3PmCounters;

/**
 * @brief PDH DE1 PM param counters
 */
typedef struct tAtSurEnginePdhDe1PmCounters
    {
    int32 cvL;     /**< Near-end Line Coding Violation */
    int32 esL;     /**< Near-end Line Error Seconds */
    int32 sesL;    /**< Near-end Line Severely Error Seconds */
    int32 lossL;   /**< Near-end Line LOS Seconds */
    int32 esLfe;   /**< Far-end Line Error Seconds */
    int32 cvP;     /**< Near-end Path Coding Violation */
    int32 esP;     /**< Near-end Path Error Seconds */
    int32 sesP;    /**< Near-end Path Severely Error Seconds */
    int32 aissP;   /**< Near-end Path AIS Seconds */
    int32 sasP;    /**< Near-end Path Service Affecting Seconds */
    int32 cssP;    /**< Near-end Path Controlled Slips Second */
    int32 uasP;    /**< Near-end Path Unavailable Seconds */
    int32 sefsPfe; /**< Far-end Path Error Frame Seconds */
    int32 esPfe;   /**< Far-end Path Error Seconds*/
    int32 sesPfe;  /**< Far-end Path Severely Error Seconds */
    int32 cssPfe;  /**< Far-end Path Controlled Slips Seconds */
    int32 fcPfe;   /**< Far-end Failure Counts */
    int32 uasPfe;  /**< Far-end Path Unavailable Seconds */
    int32 fcP;     /**< Near-end Failure Count-Path */
    }tAtSurEnginePdhDe1PmCounters;

/**
 * @brief Pseudo-wire PM param counters
 */
typedef struct tAtSurEnginePwPmCounters
    {
    int32 es;      /**< Near-end Error Second */
    int32 ses;     /**< Near-end Severely Error Seconds */
    int32 uas;     /**< Near-end Unavailable Seconds */
    int32 esfe;    /**< Far-end Error Second */
    int32 sesfe;   /**< Far-end Severely Error Seconds */
    int32 uasfe;   /**< Far-end Unavailable Seconds */
    int32 fc;      /**< Near-end Failure Count */
    }tAtSurEnginePwPmCounters;

/**
 * @brief Listener
 */
typedef struct tAtSurEngineListener
    {
    void (*FailureNotify)(AtSurEngine surEngine, void *userData, uint32 changedFailures, uint32 currentStatus);/**< Failure Notify */
    void (*TcaNotify)(AtSurEngine surEngine, void *userData, AtPmParam param, AtPmRegister pmRegister); /**< TCA Notify */
    void (*WillDelete)(AtSurEngine surEngine, void *userData);  /**< Will Delete Listener */
    }tAtSurEngineListener;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtSurEngineInit(AtSurEngine self);
AtModuleSur AtSurEngineModuleGet(AtSurEngine self);
AtChannel AtSurEngineChannelGet(AtSurEngine self);

/* Enabling */
eAtRet AtSurEngineEnable(AtSurEngine self, eBool enable);
eBool AtSurEngineIsEnabled(AtSurEngine self);

/* PM enabling */
eAtRet AtSurEnginePmEnable(AtSurEngine self, eBool enable);
eBool AtSurEnginePmIsEnabled(AtSurEngine self);

/* Failures */
uint32 AtSurEngineCurrentFailuresGet(AtSurEngine self);
uint32 AtSurEngineFailureChangedHistoryGet(AtSurEngine self);
uint32 AtSurEngineFailureChangedHistoryClear(AtSurEngine self);

/* TCA */
uint32 AtSurEngineTcaChangedHistoryGet(AtSurEngine self);
uint32 AtSurEngineTcaChangedHistoryClear(AtSurEngine self);

/* PM parameters */
AtPmParam AtSurEnginePmParamGet(AtSurEngine self, uint32 paramType);
eBool AtSurEnginePmParamIsSupported(AtSurEngine self, uint32 paramType);

/* Read all PM counters */
eAtRet AtSurEngineAllCountersGet(AtSurEngine self, eAtPmRegisterType type, void *counters);
eAtRet AtSurEngineAllCountersClear(AtSurEngine self, eAtPmRegisterType type, void *counters);
uint32 AtSurEnginePmInvalidFlagsGet(AtSurEngine self, eAtPmRegisterType type);

/* Notifications */
eAtRet AtSurEngineFailureNotificationMaskSet(AtSurEngine self, uint32 failureMask, uint32 enableMask);
uint32 AtSurEngineFailureNotificationMaskGet(AtSurEngine self);
eAtRet AtSurEngineTcaNotificationMaskSet(AtSurEngine self, uint32 paramMask, uint32 enableMask);
uint32 AtSurEngineTcaNotificationMaskGet(AtSurEngine self);

/* Listeners */
eAtRet AtSurEngineListenerAdd(AtSurEngine self, void *userData, const tAtSurEngineListener *callback);
eAtRet AtSurEngineListenerRemove(AtSurEngine self, void *userData, const tAtSurEngineListener *callback);

/* PM Threshold Profile */
eAtRet AtSurEngineThresholdProfileSet(AtSurEngine self, AtPmThresholdProfile profile);
AtPmThresholdProfile AtSurEngineThresholdProfileGet(AtSurEngine self);

#endif /* _ATSURENGINE_H_ */

#include "AtSurEngineDeprecated.h"

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtPmRegister.h
 * 
 * Created Date: Mar 18, 2015
 *
 * Description : PM parameter
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPMREGISTER_H_
#define _ATPMREGISTER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSur.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPmRegister
 * @{
 */

/**
 * @brief PM Register type number
 */
typedef enum eAtPmRegisterType
    {
    cAtPmCurrentSecondRegister  = 0, /**< The current second register. */
    cAtPmCurrentPeriodRegister  = 1, /**< The current period register. */
    cAtPmPreviousPeriodRegister = 2, /**< The previous period register. */
    cAtPmRecentPeriodRegister   = 3, /**< The top entry of recent period stack.
                                          The next entry of recent periods will
                                          be the number of current recent period
                                          added 1, so on. up to maximum 32 possible entries. */
    cAtPmCurrentDayRegister     = 35,/**< The current day register. */
    cAtPmPreviousDayRegister    = 36,/**< The previous day register. */
    cAtPmUnknownRegister             /**< Unknown register type */
    }eAtPmRegisterType;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
int32 AtPmRegisterReset(AtPmRegister self);
int32 AtPmRegisterValue(AtPmRegister self);
eBool AtPmRegisterIsValid(AtPmRegister self);
eAtPmRegisterType AtPmRegisterTypeGet(AtPmRegister self);

#endif /* _ATPMREGISTER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtPmThresholdProfile.h
 * 
 * Created Date: Sep 11, 2016
 *
 * Description : Threshold profile
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPMTHRESHOLDPROFILE_H_
#define _ATPMTHRESHOLDPROFILE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSur.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtPmThresholdProfileIdGet(AtPmThresholdProfile self);

/* Line thresholds */
eAtRet AtPmThresholdProfileSdhLinePeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEngineSdhLinePmParam param, uint32 threshold);
uint32 AtPmThresholdProfileSdhLinePeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEngineSdhLinePmParam param);

/* HO path thresholds */
eAtRet AtPmThresholdProfileSdhHoPathPeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param, uint32 threshold);
uint32 AtPmThresholdProfileSdhHoPathPeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param);

/* LO path thresholds */
eAtRet AtPmThresholdProfileSdhLoPathPeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param, uint32 threshold);
uint32 AtPmThresholdProfileSdhLoPathPeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEngineSdhPathPmParam param);

/* DS3/E3 thresholds */
eAtRet AtPmThresholdProfilePdhDe3PeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEnginePdhDe3PmParam param, uint32 threshold);
uint32 AtPmThresholdProfilePdhDe3PeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEnginePdhDe3PmParam param);

/* DS1/E1 thresholds */
eAtRet AtPmThresholdProfilePdhDe1PeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEnginePdhDe1PmParam param, uint32 threshold);
uint32 AtPmThresholdProfilePdhDe1PeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEnginePdhDe1PmParam param);

/* PW thresholds */
eAtRet AtPmThresholdProfilePwPeriodTcaThresholdSet(AtPmThresholdProfile self, eAtSurEnginePwPmParam param, uint32 threshold);
uint32 AtPmThresholdProfilePwPeriodTcaThresholdGet(AtPmThresholdProfile self, eAtSurEnginePwPmParam param);

#ifdef __cplusplus
}
#endif
#endif /* _ATPMTHRESHOLDPROFILE_H_ */


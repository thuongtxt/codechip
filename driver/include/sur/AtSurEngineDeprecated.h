/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtSurEngineDeprecated.h
 * 
 * Created Date: Nov 9, 2015
 *
 * Description : Deprecated SUR engine interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSURENGINEDEPRECATED_H_
#define _ATSURENGINEDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Counter latching */
eAtRet AtSurEngineRegistersLatchAndClear(AtSurEngine self, eAtPmRegisterType type, eBool clear);

/* Deprecated. Use: AtSurEngineRegistersLatchAndClear */
eAtRet AtSurEngineAllCountersLatch(AtSurEngine self);
eAtRet AtSurEngineAllCountersLatchAndClear(AtSurEngine self, eBool clear);

#ifdef __cplusplus
}
#endif
#endif /* _ATSURENGINEDEPRECATED_H_ */


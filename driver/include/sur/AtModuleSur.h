/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : AtModuleSur.h
 * 
 * Created Date: Mar 17, 2015
 *
 * Description : Surveillance Monitoring module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULESUR_H_
#define _ATMODULESUR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleSur
 * @{
 */

/** @brief SUR module abstract class */
typedef struct tAtModuleSur * AtModuleSur;

/** @brief PM param abstract class */
typedef struct tAtPmParam *AtPmParam;

/** @brief PM register abstract class */
typedef struct tAtPmRegister *AtPmRegister;

/** @brief Surveilance Engine Abstract class */
typedef struct tAtSurEngine *AtSurEngine;

/** @brief PM Thresold Profile Abstract class */
typedef struct tAtPmThresholdProfile *AtPmThresholdProfile;

/**
 * @brief SDH line PM param type
 */
typedef enum eAtSurEngineSdhLinePmParam
    {
    cAtSurEngineSdhLinePmParamSefsS    = cBit0,  /**< Section Severely Error Framing Seconds */
    cAtSurEngineSdhLinePmParamCvS      = cBit1,  /**< Section Coding Violation */
    cAtSurEngineSdhLinePmParamEsS      = cBit2,  /**< Section Error Seconds */
    cAtSurEngineSdhLinePmParamSesS     = cBit3,  /**< Section Severely Error Seconds */
    cAtSurEngineSdhLinePmParamCvL      = cBit4,  /**< Near-end Line Code Violation */
    cAtSurEngineSdhLinePmParamEsL      = cBit5,  /**< Near-end Line Error Seconds */
    cAtSurEngineSdhLinePmParamSesL     = cBit6,  /**< Near-end Line Severely Error Seconds */
    cAtSurEngineSdhLinePmParamUasL     = cBit7,  /**< Near-end Line Unavailable Seconds */
    cAtSurEngineSdhLinePmParamFcL      = cBit8,  /**< Near-end Line Failure Counts */
    cAtSurEngineSdhLinePmParamCvLfe    = cBit9,  /**< Far-end Line Code Violation */
    cAtSurEngineSdhLinePmParamEsLfe    = cBit10, /**< Far-end Line Error Seconds */
    cAtSurEngineSdhLinePmParamSesLfe   = cBit11, /**< Far-end Line Severely Error Seconds */
    cAtSurEngineSdhLinePmParamUasLfe   = cBit12, /**< Far-end Line Unavailable Seconds */
    cAtSurEngineSdhLinePmParamFcLfe    = cBit13  /**< Far-end Line Failure Counts */
    }eAtSurEngineSdhLinePmParam;

/**
 * @brief SDH path PM param type
 */
typedef enum eAtSurEngineSdhPathPmParam
    {
    cAtSurEngineSdhPathPmParamPpjcPdet   = cBit0,  /**< Positive Pointer Justification Count - STS/VT Path Detected */
    cAtSurEngineSdhPathPmParamNpjcPdet   = cBit1,  /**< Negative Pointer Justification Count - STS/VT Path Detected */
    cAtSurEngineSdhPathPmParamPpjcPgen   = cBit2,  /**< Positive Pointer Justification Count - STS/VT Path Generated */
    cAtSurEngineSdhPathPmParamNpjcPgen   = cBit3,  /**< Negative Pointer Justification Count - STS/VT Path Generated */
    cAtSurEngineSdhPathPmParamPjcDiff    = cBit4,  /**< Pointer Justification Count Difference */
    cAtSurEngineSdhPathPmParamPjcsPdet   = cBit5,  /**< Pointer Justification Count Seconds - STS/VT Path Detected */
    cAtSurEngineSdhPathPmParamPjcsPgen   = cBit6,  /**< Pointer Justification Count Seconds - STS/VT Path Generated */
    cAtSurEngineSdhPathPmParamCvP        = cBit7,  /**< Near-end STS/VT Path Code Violation */
    cAtSurEngineSdhPathPmParamEsP        = cBit8,  /**< Near-end STS/VT Path Error Seconds */
    cAtSurEngineSdhPathPmParamSesP       = cBit9,  /**< Near-end STS/VT Path Severely Error Seconds */
    cAtSurEngineSdhPathPmParamUasP       = cBit10, /**< Near-end STS/VT Path Unavailable Seconds */
    cAtSurEngineSdhPathPmParamFcP        = cBit11, /**< Near End STS/VT Path Failure Counts */
    cAtSurEngineSdhPathPmParamCvPfe      = cBit12, /**< Far-end STS/VT Path Coding Violations. Also Far End Block Error (FEBE) */
    cAtSurEngineSdhPathPmParamEsPfe      = cBit13, /**< Far-end STS/VT Path Error Seconds */
    cAtSurEngineSdhPathPmParamSesPfe     = cBit14, /**< Far-end STS/VT Path Severely Error Seconds */
    cAtSurEngineSdhPathPmParamUasPfe     = cBit15, /**< Far-end STS/VT Path Unavailable Seconds */
    cAtSurEngineSdhPathPmParamFcPfe      = cBit16,  /**< Far-end STS/VT Path Failure Counts */
    cAtSurEngineSdhPathPmParamEfsP       = cBit17, /**< Error-Free Second count. Derived from ES-P.*/
    cAtSurEngineSdhPathPmParamEfsPfe     = cBit18, /**< Far End STS path Error-Free Second count. Derived from ES-PFE.*/
    cAtSurEngineSdhPathPmParamAsP        = cBit19, /**< Path Available Second count. Derived from UAS-P.*/
    cAtSurEngineSdhPathPmParamAsPfe      = cBit20, /**< Far End Path Available Second count. Derived from UAS-PFE.*/
    cAtSurEngineSdhPathPmParamEbP        = cBit21, /**< Path Errored Block Count. A block in which one or more bits are in error..*/
    cAtSurEngineSdhPathPmParamBbeP       = cBit22, /**< Path Background Block Error Count. An errored block not occurring as part of an SES.*/
    cAtSurEngineSdhPathPmParamEsrP       = cBit23, /**< Path Errored Second Ratio. The ratio of ES to total seconds in available time during a fixed measurement interval. This parameter is applicable to both paths and connections.*/
    cAtSurEngineSdhPathPmParamSesrP      = cBit24, /**< Path Severely Errored Second Ratio. The ratio of SES to total seconds in available time during a fixed measurement interval. This parameter is applicable to both paths and connections.*/
    cAtSurEngineSdhPathPmParamBberP      = cBit25  /**< Path Background Block Error Ratio. The ratio of Background Block Errors (BBE) to total blocks in  available  time  during  a  fixed  measurement  interval.  The  count  of  total  blocks excludes all blocks during SESs. This parameter is applicable only to paths.*/
    }eAtSurEngineSdhPathPmParam;

/**
 * @brief PDH DE3 PM param type
 */
typedef enum eAtSurEnginePdhDe3PmParam
    {
    cAtSurEnginePdhDe3PmParamCvL        = cBit0, /**< Near-end Line Coding Violation */
    cAtSurEnginePdhDe3PmParamEsL        = cBit1, /**< Near-end Line Error Seconds */
    cAtSurEnginePdhDe3PmParamSesL       = cBit2, /**< Near-end Line Severely Error Seconds */
    cAtSurEnginePdhDe3PmParamLossL      = cBit3, /**< Near-end Line LOS Seconds */
    cAtSurEnginePdhDe3PmParamCvpP       = cBit4, /**< Near-end Path Code Violation */
    cAtSurEnginePdhDe3PmParamCvcpP      = cBit5, /**< Near-end Path C-bit Code Violation */
    cAtSurEnginePdhDe3PmParamEspP       = cBit6, /**< Near-end Path Error Seconds */
    cAtSurEnginePdhDe3PmParamEscpP      = cBit7, /**< Near-end C-bit Error Seconds */
    cAtSurEnginePdhDe3PmParamSespP      = cBit8, /**< Near-end Severely Error Seconds */
    cAtSurEnginePdhDe3PmParamSescpP     = cBit9, /**< Near-end C-bit Severely Error Seconds */
    cAtSurEnginePdhDe3PmParamSasP       = cBit10,/**< Near-end Path Service Affecting Seconds */
    cAtSurEnginePdhDe3PmParamAissP      = cBit11,/**< Near-end Path AIS Seconds */
    cAtSurEnginePdhDe3PmParamUaspP      = cBit12,/**< Near-end Path Unavailable Seconds */
    cAtSurEnginePdhDe3PmParamUascpP     = cBit13,/**< Near-end C-bit Unavailable Seconds */
    cAtSurEnginePdhDe3PmParamCvcpPfe    = cBit14,/**< Far-end C-bit Coding Violation*/
    cAtSurEnginePdhDe3PmParamEscpPfe    = cBit15,/**< Far-end C-bit Error Seconds */
    cAtSurEnginePdhDe3PmParamSescpPfe   = cBit16,/**< Far-end C-bit Severely Error Seconds */
    cAtSurEnginePdhDe3PmParamSascpPfe   = cBit17,/**< Far-end C-bit Service Affecting Seconds */
    cAtSurEnginePdhDe3PmParamUascpPfe   = cBit18,/**< Far-end C-bit Unavailable Seconds */
    cAtSurEnginePdhDe3PmParamEsbcpPfe   = cBit19, /**< Far-end C-bit Error Seconds Type B,Error Second type B. This parameter is a count of 1-second intervals containing more than one but less than 44 M-frames with the three FEBE bits not all set to one and no far-end SEF/AIS defects. */
    cAtSurEnginePdhDe3PmParamEsaL       = cBit20, /**< Errored Second-Line type A. This parameter is a count of 1-second intervals containing one BPV or EXZ and no LOS defect. */
    cAtSurEnginePdhDe3PmParamEsbL       = cBit21, /**< Errored Second-Line type B. This parameter is a count of 1-second intervals containing more than one but less than 44 BPV plus EXZ and no LOS defect. */
    cAtSurEnginePdhDe3PmParamEsapP      = cBit22, /**< Errored Second type A, P-bit parity. This parameter is the count of 1-second intervals containing exactly one P-bit parity errors and no SEF or AIS-L defect. It is defined for both DS3 application. */
    cAtSurEnginePdhDe3PmParamEsacpP     = cBit23, /**< Errored Second type A, CP-bit parity. This parameter is the count of 1-second intervals containing one CP-bit parity errors and no SEF or AIS-L defect. It is defined for C-bit parity DS3 application. */
    cAtSurEnginePdhDe3PmParamEsbpP      = cBit24, /**< Errored Second type B, P-bit parity. This parameter is the count of 1-second intervals containing more than one but less than 44 P-bit parity errors and no SEF or AIS-L defect. It is defined for both DS3 application. */
    cAtSurEnginePdhDe3PmParamEsbcpP     = cBit25, /**< Errored Second type B, CP-bit parity. This parameter is the count of 1-second intervals containing more than one but less than 44 CVCP-Ps and no SEF or AIS-L defect. It is defined for C-bit parity DS3 application. */
    cAtSurEnginePdhDe3PmParamFcP        = cBit26, /**< Near-end Failure Count-Path. This parameter is a count of the number of occurrences of near-end path failure events, with the failure event defined as follows: a near-end path failure event begins when either an LOF or AIS failure is declared and ends when both LOF and AIS failures are clear.*/
    cAtSurEnginePdhDe3PmParamEsacpPfe   = cBit27, /**< Error Second type A. This parameter is a count of 1-second intervals containing one M-frames with the three FEBE bits not all set to one and no far-end SEF/AIS defects. */
    cAtSurEnginePdhDe3PmParamFccpPfe    = cBit28  /**< Far-end Failure Count-Path. This parameter is a count of the number of occurrences of far-end path failure events, with the far- end failure event defined as follows: a far-end path failure event begins when an RAI failure is declared and ends when the RAI failure is cleared. The FCCP-PFE applies only to the C-Bit Parity application. */
    }eAtSurEnginePdhDe3PmParam;

/**
 * @brief PDH DE1 PM param type
 */
typedef enum eAtSurEnginePdhDe1PmParam
    {
    cAtSurEnginePdhDe1PmParamCvL      = cBit0,  /**< Near-end Line Coding Violation */
    cAtSurEnginePdhDe1PmParamEsL      = cBit1,  /**< Near-end Line Error Seconds */
    cAtSurEnginePdhDe1PmParamSesL     = cBit2,  /**< Near-end Line Severely Error Seconds */
    cAtSurEnginePdhDe1PmParamLossL    = cBit3,  /**< Near-end Line LOS Seconds */
    cAtSurEnginePdhDe1PmParamEsLfe    = cBit4,  /**< Far-end Line Error Seconds */
    cAtSurEnginePdhDe1PmParamCvP      = cBit5,  /**< Near-end Path Coding Violation */
    cAtSurEnginePdhDe1PmParamEsP      = cBit6,  /**< Near-end Path Error Seconds */
    cAtSurEnginePdhDe1PmParamSesP     = cBit7,  /**< Near-end Path Severely Error Seconds */
    cAtSurEnginePdhDe1PmParamAissP    = cBit8,  /**< Near-end Path AIS Seconds */
    cAtSurEnginePdhDe1PmParamSasP     = cBit9,  /**< Near-end Path Service Affecting Seconds */
    cAtSurEnginePdhDe1PmParamCssP     = cBit10, /**< Near-end Path Controlled Slips Second */
    cAtSurEnginePdhDe1PmParamUasP     = cBit11, /**< Near-end Path Unavailable Seconds */
    cAtSurEnginePdhDe1PmParamSefsPfe  = cBit12, /**< Far-end Path Error Frame Seconds */
    cAtSurEnginePdhDe1PmParamEsPfe    = cBit13, /**< Far-end Path Error Seconds*/
    cAtSurEnginePdhDe1PmParamSesPfe   = cBit14, /**< Far-end Path Severely Error Seconds */
    cAtSurEnginePdhDe1PmParamCssPfe   = cBit15, /**< Far-end Path Controlled Slips Seconds */
    cAtSurEnginePdhDe1PmParamFcPfe    = cBit16, /**< Far-end Failure Counts */
    cAtSurEnginePdhDe1PmParamUasPfe   = cBit17, /**< Far-end Path Unavailable Seconds */
    cAtSurEnginePdhDe1PmParamFcP      = cBit18  /**< Near-end Failure Count-Path */
    }eAtSurEnginePdhDe1PmParam;

/**
 * @brief Pseudo-wire PM param type
 */
typedef enum eAtSurEnginePwPmParam
    {
    cAtSurEnginePwPmParamEs    = cBit0, /**< Near-end Error Second */
    cAtSurEnginePwPmParamSes   = cBit1, /**< Near-end Severely Error Seconds */
    cAtSurEnginePwPmParamUas   = cBit2, /**< Near-end Unavailable Seconds */
    cAtSurEnginePwPmParamFeEs  = cBit3, /**< Far-end Error Second */
    cAtSurEnginePwPmParamFeSes = cBit4, /**< Far-end Severely Error Seconds */
    cAtSurEnginePwPmParamFeUas = cBit5, /**< Far-end Unavailable Seconds */
    cAtSurEnginePwPmParamFc    = cBit6  /**< Near-end Failure Count */
    }eAtSurEnginePwPmParam;

/**
 * @brief Period expire method
 */
typedef enum eAtSurPeriodExpireMethod
    {
    cAtSurPeriodExpireMethodUnknown, /**< Unknown method, for error handling */
    cAtSurPeriodExpireMethodManual,  /**< Application will manually trigger period expire */
    cAtSurPeriodExpireMethodAuto     /**< Period expire is triggered automatically by internal implementation */
    }eAtSurPeriodExpireMethod;

/**
 * @brief PM Tick source
 */
typedef enum eAtSurTickSource
    {
    cAtSurTickSourceUnknown,  /**< Unknown source */
    cAtSurTickSourceInternal, /**< PM tick is from internal free-run source */
    cAtSurTickSourceExternal  /**< PM tick is from external source */
    }eAtSurTickSource;

/** @brief Default SUR listener interface */
typedef struct tAtModuleSurEventListener
    {
    void (*ExpireNotify)(AtModuleSur self, void * userData); /**< Called when the current period is expired. */
    }tAtModuleSurEventListener;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Period expire method */
eAtRet AtModuleSurExpireMethodSet(AtModuleSur self, eAtSurPeriodExpireMethod method);
eAtSurPeriodExpireMethod AtModuleSurExpireMethodGet(AtModuleSur self);

/* Period expiration notification */
eAtRet AtModuleSurExpireNotificationEnable(AtModuleSur self, eBool enable);
eBool AtModuleSurExpireNotificationIsEnabled(AtModuleSur self);

/* PM tick source */
eAtRet AtModuleSurTickSourceSet(AtModuleSur self, eAtSurTickSource source);
eAtSurTickSource AtModuleSurTickSourceGet(AtModuleSur self);

/* Handle 15-minutes period expires. When a period expires, application need to
 * call this API. */
eAtRet AtModuleSurPeriodExpire(AtModuleSur self);
eAtRet AtModuleSurPeriodAsyncExpire(AtModuleSur self);

/* Get elapsed time in seconds of the current period. */
uint32 AtModuleSurPeriodElapsedSecondsGet(AtModuleSur self);

/* Threshold Profile */
uint32 AtModuleSurNumThresholdProfiles(AtModuleSur self);
AtPmThresholdProfile AtModuleSurThresholdProfileGet(AtModuleSur self, uint32 profileId);

/* Timers */
eAtRet AtModuleSurFailureHoldOffTimerSet(AtModuleSur self, uint32 timerInMs);
uint32 AtModuleSurFailureHoldOffTimerGet(AtModuleSur self);
eAtRet AtModuleSurFailureHoldOnTimerSet(AtModuleSur self, uint32 timerInMs);
uint32 AtModuleSurFailureHoldOnTimerGet(AtModuleSur self);
eBool AtModuleSurFailureIsSupported(AtModuleSur self);
eBool AtModuleSurTcaIsSupported(AtModuleSur self);
#endif /* _ATMODULESUR_H_ */


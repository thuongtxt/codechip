/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhTug.h
 * 
 * Created Date: Aug 21, 2012
 *
 * Author      : namnn
 * 
 * Description : TUG. Only mapping interfaces are applicable for this class.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHTUG_H_
#define _ATSDHTUG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtSdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtSdhTug
 * @{
 */
/** @brief TUG mapping types */
typedef enum eAtSdhTugMapType
    {
    cAtSdhTugMapTypeTugMapNone,     /**< No mapping */

    /* TUG-3 mapping types */
    cAtSdhTugMapTypeTug3MapVc3,     /**< TUG-3 maps VC-3 */
    cAtSdhTugMapTypeTug3Map7xTug2s, /**< TUG-3 maps 7xTUG-2s */

    /* TUG-2 mapping types */
    cAtSdhTugMapTypeTug2Map4xTu11s, /**< TUG-2 maps 4xTU-11 */
    cAtSdhTugMapTypeTug2Map3xTu12s  /**< TUG-2 maps 3xTU-12 */
    }eAtSdhTugMapType;
/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHTUG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhChannel.h
 * 
 * Created Date: Jul 27, 2012
 *
 * Description : SDH abstract channel. It is to abstract all of channels in SDH
 *               module. This class extends AtChannel so all of interfaces of
 *               AtChannel are applicable for this class.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHCHANNEL_H_
#define _ATSDHCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtChannel.h"
#include "AtModuleBer.h"
#include "AtBerController.h"
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cAtSdhChannelMaxTtiLength 64

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtSdhChannel
 * @{
 */
/**
 * @brief Channel type
 */
typedef enum eAtSdhChannelType
    {
    cAtSdhChannelTypeUnknown, /**< Unknown */
    cAtSdhChannelTypeLine,    /**< Line */

    /* AUG */
    cAtSdhChannelTypeAug64,   /**< AUG-64 */
    cAtSdhChannelTypeAug16,   /**< AUG-16 */
    cAtSdhChannelTypeAug4,    /**< AUG-4 */
    cAtSdhChannelTypeAug1,    /**< AUG-1 */

    /* AU */
    cAtSdhChannelTypeAu4_64c, /**< AU-4-64c */
    cAtSdhChannelTypeAu4_16c, /**< AU-4-16c */
    cAtSdhChannelTypeAu4_4c,  /**< AU-4-4c */
    cAtSdhChannelTypeAu4,     /**< AU-4 */
    cAtSdhChannelTypeAu3,     /**< AU-3 */
    cAtSdhChannelTypeAu4_16nc,/**< N contiguous AU-4-16c */
    cAtSdhChannelTypeAu4_nc,  /**< N contiguous AU-4 */

    /* TUG */
    cAtSdhChannelTypeTug3,    /**< TUG-3 */
    cAtSdhChannelTypeTug2,    /**< TUG-2 */

    /* TU */
    cAtSdhChannelTypeTu3,     /**< TU-3 */
    cAtSdhChannelTypeTu12,    /**< TU-12 */
    cAtSdhChannelTypeTu11,    /**< TU-11 */

    /* VC */
    cAtSdhChannelTypeVc4_64c, /**< VC-4-64c */
    cAtSdhChannelTypeVc4_16c, /**< VC-4-16c */
    cAtSdhChannelTypeVc4_4c,  /**< VC-4-4c */
    cAtSdhChannelTypeVc4,     /**< VC-4 */
    cAtSdhChannelTypeVc3,     /**< VC-3 */
    cAtSdhChannelTypeVc12,    /**< VC-12 */
    cAtSdhChannelTypeVc11,    /**< VC-11 */
    cAtSdhChannelTypeVc4_16nc,/**< N contiguous VC-4-16c */
    cAtSdhChannelTypeVc4_nc   /**< N contiguous VC-4 */
    }eAtSdhChannelType;

/**
 * @brief Channel mode
 */
typedef enum eAtSdhChannelMode
    {
    cAtSdhChannelModeUnknown, /**< Unknown mode */
    cAtSdhChannelModeSdh,     /**< SDH */
    cAtSdhChannelModeSonet    /**< SONET */
    }eAtSdhChannelMode;

/**
 * @brief TTI mode
 */
typedef enum eAtSdhTtiMode
    {
    cAtSdhTtiMode1Byte,  /**< 1-byte */
    cAtSdhTtiMode16Byte, /**< 16-bytes */
    cAtSdhTtiMode64Byte  /**< 64-bytes */
    }eAtSdhTtiMode;

/**
 * @brief TTI message
 */
typedef struct tAtSdhTti
    {
    eAtSdhTtiMode mode;                       /**< Message mode */
    uint8 message[cAtSdhChannelMaxTtiLength]; /**< Message content */
    uint8 crc;                                /**< CRC-7 for 16-byte message */
    }tAtSdhTti;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Channel type */
eAtSdhChannelType AtSdhChannelTypeGet(AtSdhChannel self);

/* Mapping */
eAtModuleSdhRet AtSdhChannelMapTypeSet(AtSdhChannel self, uint8 mapType);
uint8 AtSdhChannelMapTypeGet(AtSdhChannel self);
eBool AtSdhChannelMapTypeIsSupported(AtSdhChannel self, uint8 mapType);
AtChannel AtSdhChannelMapChannelGet(AtSdhChannel self);

/* Mode */
eAtModuleSdhRet AtSdhChannelModeSet(AtSdhChannel self, eAtSdhChannelMode mode);
eAtSdhChannelMode AtSdhChannelModeGet(AtSdhChannel self);
eBool AtSdhChannelCanChangeMode(AtSdhChannel self, eAtSdhChannelMode mode);

/* BER monitoring */
AtBerController AtSdhChannelBerControllerGet(AtSdhChannel self);

/* TTI */
eAtModuleSdhRet AtSdhChannelTxTtiGet(AtSdhChannel self, tAtSdhTti *tti);
eAtModuleSdhRet AtSdhChannelTxTtiSet(AtSdhChannel self, const tAtSdhTti *tti);
eAtModuleSdhRet AtSdhChannelExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti);
eAtModuleSdhRet AtSdhChannelExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti);
eAtModuleSdhRet AtSdhChannelRxTtiGet(AtSdhChannel self, tAtSdhTti *tti);

/* TTI comparing */
eAtModuleSdhRet AtSdhChannelTtiCompareEnable(AtSdhChannel self, eBool enable);
eBool AtSdhChannelTtiCompareIsEnabled(AtSdhChannel self);

/* TIM monitoring */
eAtModuleSdhRet AtSdhChannelTimMonitorEnable(AtSdhChannel self, eBool enable);
eBool AtSdhChannelTimMonitorIsEnabled(AtSdhChannel self);

/* Block error counters */
uint32 AtSdhChannelBlockErrorCounterGet(AtSdhChannel self, uint16 counterType);
uint32 AtSdhChannelBlockErrorCounterClear(AtSdhChannel self, uint16 counterType);

/* Alarm affecting option */
eAtModuleSdhRet AtSdhChannelAlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable);
eBool AtSdhChannelAlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType);

/* Auto RDI per channel, cannot configure per alarm type */
eAtRet AtSdhChannelAutoRdiEnable(AtSdhChannel self, eBool enable);
eBool AtSdhChannelAutoRdiIsEnabled(AtSdhChannel self);

/* Auto RDI per channel and per alarm type. Note, not all of products support this */
eAtModuleSdhRet AtSdhChannelAlarmAutoRdiEnable(AtSdhChannel self, uint32 alarms, eBool enable);
eBool AtSdhChannelAlarmAutoRdiIsEnabled(AtSdhChannel self, uint32 alarms);

/* Auto REI */
eAtRet AtSdhChannelAutoReiEnable(AtSdhChannel self, eBool enable);
eBool AtSdhChannelAutoReiIsEnabled(AtSdhChannel self);

/* Alarm and interrupt retrieving, use for applications that does not use AtDeviceInterruptProcess */
eAtRet AtSdhChannelInterruptAndDefectGet(AtSdhChannel self, uint32 *changedDefects, uint32 *currentStatus, eBool read2Clear);
eAtRet AtSdhChannelInterruptAndAlarmGet(AtSdhChannel self, uint32 *changedAlarms, uint32 *currentStatus, eBool read2Clear);

/* Interfaces to handle objects in mapping tree */
uint8 AtSdhChannelNumberOfSubChannelsGet(AtSdhChannel self);
AtSdhChannel AtSdhChannelSubChannelGet(AtSdhChannel self, uint8 subChannelId);
AtSdhChannel AtSdhChannelParentChannelGet(AtSdhChannel self);

/* APIs to handle internal add/drop traffic */
eAtRet AtSdhChannelRxTrafficSwitch(AtSdhChannel self, AtSdhChannel otherChannel);
eAtRet AtSdhChannelTxTrafficBridge(AtSdhChannel self, AtSdhChannel otherChannel);
eAtRet AtSdhChannelTxTrafficCut(AtSdhChannel self);

/* Access ID fields */
uint8 AtSdhChannelSts1Get(AtSdhChannel self);
uint8 AtSdhChannelLineGet(AtSdhChannel self);
uint8 AtSdhChannelTug2Get(AtSdhChannel self);
uint8 AtSdhChannelTu1xGet(AtSdhChannel self);
uint8 AtSdhChannelNumSts(AtSdhChannel self);

/* Utils */
tAtSdhTti *AtSdhTtiMake(eAtSdhTtiMode mode, const uint8 *message, uint8 len, tAtSdhTti *tti);
uint8 AtSdhTtiLengthForTtiMode(eAtSdhTtiMode ttiMode);
tAtSdhTti *AtSdh16BytesTtiMake(tAtSdhTti *tti, const uint8 *message, uint8 len, uint8 padding);
tAtSdhTti *AtSdh64BytesTtiMake(tAtSdhTti *tti, const uint8 *message, uint8 len, uint8 padding);

/* To control overhead bytes */
eAtModuleSdhRet AtSdhChannelTxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 value);
uint8 AtSdhChannelTxOverheadByteGet(AtSdhChannel self, uint32 overheadByte);
uint8 AtSdhChannelRxOverheadByteGet(AtSdhChannel self, uint32 overheadByte);
eBool AtSdhChannelTxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte);
eBool AtSdhChannelRxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte);

/* Concatenation */
eAtRet AtSdhChannelConcate(AtSdhChannel self, AtSdhChannel *slaveList, uint8 numSlaves);
eAtRet AtSdhChannelDeconcate(AtSdhChannel self);
AtSdhChannel AtSdhChannelSlaveChannelAtIndex(AtSdhChannel self, uint8 slaveIndex);
uint8 AtSdhChannelNumSlaveChannels(AtSdhChannel self);

/* SF/SD Hold-off/Hold-on timer */
eAtRet AtSdhChannelHoldOffTimerSet(AtSdhChannel self, uint32 timerInMs);
uint32 AtSdhChannelHoldOffTimerGet(AtSdhChannel self);
eAtRet AtSdhChannelHoldOnTimerSet(AtSdhChannel self, uint32 timerInMs);
uint32 AtSdhChannelHoldOnTimerGet(AtSdhChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHCHANNEL_H_ */


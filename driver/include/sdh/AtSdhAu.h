/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhAu.h
 * 
 * Created Date: Aug 21, 2012
 *
 * Description : AU Class
 *               - This class extends AtSdhPath so all of interfaces of AtSdhPath
 *                 are applicable for it
 *               - This class is not mappable because one AU always contains
 *                 one corresponding VC
 *
 * Notes       : This class is not mappable
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHAU_H_
#define _ATSDHAU_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtSdhPath.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHAU_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhPath.h
 * 
 * Created Date: Aug 21, 2012
 *
 * Description : Path class. This class will abstract higher-order path and
 *               low-order path. All of interfaces of AtSdhChannel are available
 *               at this class.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHPATH_H_
#define _ATSDHPATH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtModuleAps.h"
#include "AtSdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtSdhPath
 * @{
 */

/** @brief Error types */
typedef enum eAtSdhPathErrorType
    {
    cAtSdhPathErrorNone  = 0,        /**< No error */
    cAtSdhPathErrorBip   = cBit0,    /**< BIP error */
    cAtSdhPathErrorRei   = cBit1,    /**< REI error */
    cAtSdhPathErrorAll   = cBit1_0   /**< All errors */
    }eAtSdhPathErrorType;

/** @brief Counter type */
typedef enum eAtSdhPathCounterType
    {
    cAtSdhPathCounterTypeUnknown,       /**< Unknown error type */
    cAtSdhPathCounterTypeBip    = cBit0,/**< BIP counter, for VC-3/VC-4, it is B3 error
                                             counter, for VC-1x, it is BIP-V error counter */
    cAtSdhPathCounterTypeRei    = cBit1,/**< REI counter */
    cAtSdhPathCounterTypeRxPPJC = cBit2,/**< Detected Positive Pointer Justification Count */
    cAtSdhPathCounterTypeRxNPJC,        /**< Detected Negative Pointer Justification Count */
    cAtSdhPathCounterTypeTxPPJC,        /**< Generated Positive Pointer Justification Count */
    cAtSdhPathCounterTypeTxNPJC         /**< Generated Negative Pointer Justification Count */
    }eAtSdhPathCounterType;

/** @brief Path alarm */
typedef enum eAtSdhPathAlarmType
    {
    cAtSdhPathAlarmAis         = cBit0,  /**< AIS */
    cAtSdhPathAlarmLop         = cBit1,  /**< LOP */
    cAtSdhPathAlarmTim         = cBit2,  /**< TIM */
    cAtSdhPathAlarmUneq        = cBit3,  /**< UNEQ (Signal Label = 0) */
    cAtSdhPathAlarmPlm         = cBit4,  /**< PLM */
    cAtSdhPathAlarmRdi         = cBit5,  /**< RDI */
    cAtSdhPathAlarmErdiS       = cBit6,  /**< ERDI-S */
    cAtSdhPathAlarmErdiP       = cBit7,  /**< ERDI-P */
    cAtSdhPathAlarmErdiC       = cBit8,  /**< ERDI-C */
    cAtSdhPathAlarmBerSd       = cBit9,  /**< BER-SD */
    cAtSdhPathAlarmBerSf       = cBit10, /**< BER-SF */
    cAtSdhPathAlarmLom         = cBit11, /**< LOM (applicable for structured STS) */
    cAtSdhPathAlarmRfi         = cBit12, /**< RFI */
    cAtSdhPathAlarmPayloadUneq = cBit13, /**< Payload UNEQ (Signal Label = 0 and Jn = 0) */
    cAtSdhPathAlarmBerTca      = cBit14, /**< BER-TCA defect mask */
    cAtSdhPathAlarmRfiS        = cBit15, /**< RFI Server */
    cAtSdhPathAlarmRfiC        = cBit16, /**< RFI Connectivity */
    cAtSdhPathAlarmRfiP        = cBit17, /**< RFI Payload */

    /* Extension events. */
    cAtSdhPathAlarmClockStateChange = cBit18,  /**< CDR clock state change. */
    cAtSdhPathAlarmTtiChange   = cBit19, /**< TTI change event */
    cAtSdhPathAlarmPslChange   = cBit20, /**< PSL change event */
    cAtSdhPathAlarmPiNdf       = cBit21, /**< NDF event is detected */
    cAtSdhPathAlarmPgNdf       = cBit22, /**< NDF event is generated */
    cAtSdhPathAlarmNewPointer  = cBit23  /**< New pointer is detected */
    }eAtSdhPathAlarmType;

/**
 * @brief All counters data structure
 */
typedef struct tAtSdhPathCounters
    {
    uint32 bip;    /**< BIP counter */
    uint32 rei;    /**< REI counter */
    uint32 rxPPJC; /**< Detected Positive Pointer Justification Count */
    uint32 rxNPJC; /**< Detected Negative Pointer Justification Count */
    uint32 txPPJC; /**< Generated Positive Pointer Justification Count */
    uint32 txNPJC; /**< Generated Negative Pointer Justification Count */
    }tAtSdhPathCounters;

/** @brief Path Overhead byte */
typedef enum eAtSdhPathOverheadByte
    {
    cAtSdhPathOverheadByteN1 = cBit0,
    cAtSdhPathOverheadByteC2 = cBit1,
    cAtSdhPathOverheadByteG1 = cBit2,
    cAtSdhPathOverheadByteF2 = cBit3,
    cAtSdhPathOverheadByteH4 = cBit4,
    cAtSdhPathOverheadByteF3 = cBit5,
    cAtSdhPathOverheadByteK3 = cBit6,
    cAtSdhPathOverheadByteV5 = cBit7,
    cAtSdhPathOverheadByteN2 = cBit8,
    cAtSdhPathOverheadByteK4 = cBit9
    }eAtSdhPathOverheadByte;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Path Signal Label */
eAtModuleSdhRet AtSdhPathTxPslSet(AtSdhPath self, uint8 psl);
uint8 AtSdhPathTxPslGet(AtSdhPath self);
uint8 AtSdhPathRxPslGet(AtSdhPath self);
eAtModuleSdhRet AtSdhPathExpectedPslSet(AtSdhPath self, uint8 psl);
uint8 AtSdhPathExpectedPslGet(AtSdhPath self);

/* SS bits */
eAtModuleSdhRet AtSdhPathTxSsSet(AtSdhPath self, uint8 value);
uint8 AtSdhPathTxSsGet(AtSdhPath self);
eAtModuleSdhRet AtSdhPathExpectedSsSet(AtSdhPath self, uint8 value);
uint8 AtSdhPathExpectedSsGet(AtSdhPath self);
eAtModuleSdhRet AtSdhPathSsCompareEnable(AtSdhPath self, eBool enable);
eBool AtSdhPathSsCompareIsEnabled(AtSdhPath self);
eAtRet AtSdhPathSsInsertionEnable(AtSdhPath self, eBool enable);
eBool AtSdhPathSsInsertionIsEnabled(AtSdhPath self);

eAtModuleSdhRet AtSdhPathERdiEnable(AtSdhPath self, eBool enable);
eBool AtSdhPathERdiIsEnabled(AtSdhPath self);

eAtModuleSdhRet AtSdhPathPlmMonitorEnable(AtSdhPath self, eBool enable);
eBool AtSdhPathPlmMonitorIsEnabled(AtSdhPath self);

/* Protection */
AtApsEngine AtSdhPathApsEngineGet(AtSdhPath self);
AtList AtSdhPathApsSelectorsGet(AtSdhPath self);
eAtModuleSdhRet AtSdhPathApsSwitchingConditionSet(AtSdhPath self, uint32 defects, eAtApsSwitchingCondition condition);
eAtApsSwitchingCondition AtSdhPathApsSwitchingConditionGet(AtSdhPath self, uint32 defect);

eAtModuleSdhRet AtSdhPathApsForcedSwitchingConditionSet(AtSdhPath self, eAtApsSwitchingCondition condition);
eAtApsSwitchingCondition AtSdhPathApsForcedSwitchingConditionGet(AtSdhPath self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHPATH_H_ */

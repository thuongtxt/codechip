/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhLineDeprecated.h
 * 
 * Created Date: Jun 1, 2016
 *
 * Description : Deprecated SDH line interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHLINEDEPRECATED_H_
#define _ATSDHLINEDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* @brief Line alarms [deprecated] */
typedef enum eAtSdhLineAlarmTypeDeprecated
    {
    cAtSdhLineAlarmKByteChange = cAtSdhLineAlarmK1Change|cAtSdhLineAlarmK2Change,  /* K-byte change event. Stable K-byte is changed */
    cAtSdhLineAlarmKByteFail   = cBit9   /* K-byte fail. There is no stable K-byte */
    }eAtSdhLineAlarmTypeDeprecated;

/*
 * @brief Line mode
 */
typedef enum eAtSdhLineMode
    {
    cAtSdhLineModeUnknown = cAtSdhChannelModeUnknown, /* Unknown mode */
    cAtSdhLineModeSdh     = cAtSdhChannelModeSdh,     /* SDH */
    cAtSdhLineModeSonet   = cAtSdhChannelModeSonet    /* SONET */
    }eAtSdhLineMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Some products have eye scan controller */
AtEyeScanController AtSdhLineEyeScanControllerGet(AtSdhLine self);

/* Mode, deprecated by AtSdhChannelLineModeSet() and AtSdhChannelLineModeGet() */
eAtModuleSdhRet AtSdhLineModeSet(AtSdhLine self, eAtSdhLineMode mode);
eAtSdhLineMode AtSdhLineModeGet(AtSdhLine self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHLINEDEPRECATED_H_ */


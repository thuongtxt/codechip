/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhVc.h
 * 
 * Created Date: Aug 21, 2012
 *
 * Author      : namnn
 * 
 * Description : VC. All of interfaces of AtSdhPath, AtSdhChannel, AtChannel are
 *               applicable for this class.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHVC_H_
#define _ATSDHVC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtSdhPath.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtSdhVc
 * @{
 */
/** @brief VC mapping type. VC-4 and VC-3 are mappable */
typedef enum eAtSdhVcMapType
    {
    cAtSdhVcMapTypeNone,            /**< Corresponding Container is mapped to VC */

    /* VC-4-16c mapping type */
    cAtSdhVcMapTypeVc4_64cMapC4_64c, /**< VC-4-64c maps C4-64c. Only support this mode */

    /* VC-4-16c mapping type */
    cAtSdhVcMapTypeVc4_16cMapC4_16c, /**< VC-4-16c maps C4-16c. Only support this mode */

    /* VC-4-4c mapping type */
    cAtSdhVcMapTypeVc4_4cMapC4_4c, /**< VC-4-4c maps C4-4c. Only support this mode */

    /* VC-4-16nc mapping type */
    cAtSdhVcMapTypeVc4_16ncMapC4_16nc, /**< VC-4-16nc maps C4-16nc. Only support this mode */

    /* VC-4-nc mapping type */
    cAtSdhVcMapTypeVc4_ncMapC4_nc, /**< VC-4-nc maps C4-nc. Only support this mode */

    /* VC-4 mapping type */
    cAtSdhVcMapTypeVc4MapC4,        /**< VC-4 maps C4. This is default mapping of VC-4 */
    cAtSdhVcMapTypeVc4Map3xTug3s,   /**< VC-4 maps 3xTUG-3s */

    /* VC-3 mapping type */
    cAtSdhVcMapTypeVc3MapC3,        /**< VC-3 maps C3. This is default mapping of VC-3 */
    cAtSdhVcMapTypeVc3Map7xTug2s,   /**< VC-3 maps 7xTUG-2s */
    cAtSdhVcMapTypeVc3MapDe3,       /**< VC-3 maps DS3/E3 */

    /* VC-1x mapping type */
    cAtSdhVcMapTypeVc1xMapC1x,      /**< VC-11/VC-12 map C-11/C-12. This is default mapping of VC-11/VC-12 */
    cAtSdhVcMapTypeVc1xMapDe1       /**< VC-11/VC-12 map DS1/E1 */
    }eAtSdhVcMapType;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

eAtRet AtSdhVcStuffEnable(AtSdhVc self, eBool enable);
eBool AtSdhVcStuffIsEnabled(AtSdhVc self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHVC_H_ */


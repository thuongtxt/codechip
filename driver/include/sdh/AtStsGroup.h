/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtStsGroup.h
 * 
 * Created Date: Nov 15, 2016
 *
 * Description : STS pass-through group.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSTSGROUP_H_
#define _ATSTSGROUP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhSts.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule AtStsGroupModuleGet(AtStsGroup self);
uint32 AtStsGroupIdGet(AtStsGroup self);

/* STS-1 management. */
eAtRet AtStsGroupStsAdd(AtStsGroup self, AtSdhSts sts1);
eAtRet AtStsGroupStsDelete(AtStsGroup self, AtSdhSts sts1);
uint32 AtStsGroupNumSts(AtStsGroup self);
AtSdhSts AtStsGroupStsAtIndex(AtStsGroup self, uint32 stsIndex);

/* Group associations */
eAtRet AtStsGroupDestGroupSet(AtStsGroup self, AtStsGroup destGroup);
AtStsGroup AtStsGroupDestGroupGet(AtStsGroup self);

/* Unidirectional pass-through to destination group. */
eAtRet AtStsGroupPassThroughEnable(AtStsGroup self, eBool enable);
eBool AtStsGroupPassThroughIsEnabled(AtStsGroup self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSTSGROUP_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhLine.h
 * 
 * Created Date: Jul 25, 2012
 *
 * Description : Line class. Although it extends AtSdhChannel, mapping methods
 *               are not applicable. One line always contains one corresponding
 *               AUG.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHLINE_H_
#define _ATSDHLINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtSdhChannel.h"
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtSdhLine
 * @{
 */
/**
 * @brief Line rate
 */
typedef enum eAtSdhLineRate
    {
    cAtSdhLineRateInvalid, /**< Invalid rate */
    cAtSdhLineRateStm0,    /**< STM-0 */
    cAtSdhLineRateStm1,    /**< STM-1 */
    cAtSdhLineRateStm4,    /**< STM-4 */
    cAtSdhLineRateStm16,   /**< STM-16 */
    cAtSdhLineRateStm64    /**< STM-64 */
    }eAtSdhLineRate;

/** @brief Error types */
typedef enum eAtSdhLineErrorType
    {
    cAtSdhLineErrorNone  = 0,        /**< No error */
    cAtSdhLineErrorB1    = cBit0,    /**< B1 error */
    cAtSdhLineErrorB2    = cBit1,    /**< B2 error */
    cAtSdhLineErrorRei   = cBit2,    /**< REI-L error */
    cAtSdhLineErrorAll   = cBit2_0   /**< All errors */
    }eAtSdhLineErrorType;

/**
 * @brief Line counters
 */
typedef enum eAtSdhLineCounterType
    {
    cAtSdhLineCounterUnknown,         /**< Unknown error */
    cAtSdhLineCounterTypeB1  = cBit0, /**< B1 counter */
    cAtSdhLineCounterTypeB2  = cBit1, /**< B2 counter */
    cAtSdhLineCounterTypeRei = cBit2  /**< REI-L counter */
    }eAtSdhLineCounterType;

/**
 * @brief All counters data structure
 */
typedef struct tAtSdhLineCounters
    {
    uint32 b1;  /**< B1 counter */
    uint32 b2;  /**< B2 counter */
    uint32 rei; /**< REI counter */
    }tAtSdhLineCounters;

/** @brief Line alarms */
typedef enum eAtSdhLineAlarmType
    {
    cAtSdhLineAlarmLos         = cBit0,  /**< @brief RS-LOS defect mask */
    cAtSdhLineAlarmOof         = cBit1,  /**< @brief RS-OOF defect mask */
    cAtSdhLineAlarmLof         = cBit2,  /**< @brief RS-LOF defect mask */
    cAtSdhLineAlarmTim         = cBit3,  /**< @brief RS-TIM defect mask */
    cAtSdhLineAlarmAis         = cBit4,  /**< @brief MS-AIS defect mask */
    cAtSdhLineAlarmRdi         = cBit5,  /**< @brief MS-RDI defect mask */
    cAtSdhLineAlarmBerSd       = cBit6,  /**< @brief MS-BER-SD defect mask */
    cAtSdhLineAlarmBerSf       = cBit7,  /**< @brief MS-BER-SF defect mask */
    cAtSdhLineAlarmS1Change    = cBit10, /**< @brief S1 byte change event. */
    cAtSdhLineAlarmRsBerSd     = cBit11, /**< @brief RS-BER-SD defect mask */
    cAtSdhLineAlarmRsBerSf     = cBit12, /**< @brief RS-BER-SF defect mask */
    cAtSdhLineAlarmK1Change    = cBit13, /**< @brief K1-byte change event */
    cAtSdhLineAlarmK2Change    = cBit14, /**< @brief K2-byte change event */
    cAtSdhLineAlarmBerTca      = cBit15, /**< @brief BER-TCA defect mask */
    cAtSdhLineAlarmRfi         = cBit16  /**< @brief MS-RFI failure mask */
    }eAtSdhLineAlarmType;

/** @brief Line Overhead byte */
typedef enum eAtSdhLineOverheadByte
    {
    /* RSOH */
    cAtSdhLineRsOverheadByteA1        = cBit0,
    cAtSdhLineRsOverheadByteA2        = cBit1,
    cAtSdhLineRsOverheadByteJ0        = cBit2,
    cAtSdhLineRsOverheadByteZ0        = 3,
    cAtSdhLineRsOverheadByteB1        = cBit3,
    cAtSdhLineRsOverheadByteE1        = cBit4,
    cAtSdhLineRsOverheadByteF1        = cBit5,
    cAtSdhLineRsOverheadByteD1        = cBit6,
    cAtSdhLineRsOverheadByteD2        = cBit7,
    cAtSdhLineRsOverheadByteD3        = cBit8,
    cAtSdhLineRsOverheadByteH1        = cBit9,
    cAtSdhLineRsOverheadByteH2        = cBit10,
    cAtSdhLineRsOverheadByteH3        = cBit11,
    cAtSdhLineRsOverheadByteUndefined = 5,

    /* MSOH */
    cAtSdhLineMsOverheadByteB2        = cBit12,
    cAtSdhLineMsOverheadByteK1        = cBit13,
    cAtSdhLineMsOverheadByteK2        = cBit14,
    cAtSdhLineMsOverheadByteD4        = cBit15,
    cAtSdhLineMsOverheadByteD5        = cBit16,
    cAtSdhLineMsOverheadByteD6        = cBit17,
    cAtSdhLineMsOverheadByteD7        = cBit18,
    cAtSdhLineMsOverheadByteD8        = cBit19,
    cAtSdhLineMsOverheadByteD9        = cBit20,
    cAtSdhLineMsOverheadByteD10       = cBit21,
    cAtSdhLineMsOverheadByteD11       = cBit22,
    cAtSdhLineMsOverheadByteD12       = cBit23,
    cAtSdhLineMsOverheadByteS1        = cBit24,
    cAtSdhLineMsOverheadByteZ1        = 6,
    cAtSdhLineMsOverheadByteZ2        = 7,
    cAtSdhLineMsOverheadByteM0        = 9,
    cAtSdhLineMsOverheadByteM1        = cBit25,
    cAtSdhLineMsOverheadByteE2        = cBit26,
    cAtSdhLineMsOverheadByteUndefined = 10
    }eAtSdhLineOverheadByte;

/**
 * @brief DCC layers
 */
typedef enum eAtSdhLineDccLayer
    {
    cAtSdhLineDccLayerUnknown = 0,      /**< Unknown layer */
    cAtSdhLineDccLayerSection = cBit0,  /**< Section DCC */
    cAtSdhLineDccLayerLine    = cBit1,  /**< Line DCC */
    cAtSdhLineDccLayerAll     = cBit2   /**< DCC composite of Section and Line DCC */
    }eAtSdhLineDccLayer;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Rate */
eAtModuleSdhRet AtSdhLineRateSet(AtSdhLine self, eAtSdhLineRate rate);
eBool AtSdhLineRateIsSupported(AtSdhLine self, eAtSdhLineRate rate);
eAtSdhLineRate AtSdhLineRateGet(AtSdhLine self);

/* Scramble */
eAtModuleSdhRet AtSdhLineScrambleEnable(AtSdhLine self, eBool enable);
eBool AtSdhLineScrambleIsEnabled(AtSdhLine self);

/* APS helper APIs to control switch/bridge */
eAtModuleSdhRet AtSdhLineSwitch(AtSdhLine self, AtSdhLine toLine);
AtSdhLine AtSdhLineSwitchedLineGet(AtSdhLine self);
eAtModuleSdhRet AtSdhLineSwitchRelease(AtSdhLine self);
eAtModuleSdhRet AtSdhLineBridge(AtSdhLine self, AtSdhLine toLine);
AtSdhLine AtSdhLineBridgedLineGet(AtSdhLine self);
eAtModuleSdhRet AtSdhLineBridgeRelease(AtSdhLine self);

/* LED controlling */
eAtModuleSdhRet AtSdhLineLedStateSet(AtSdhLine self, eAtLedState ledState);
eAtLedState AtSdhLineLedStateGet(AtSdhLine self);

/* S1 */
eAtModuleSdhRet AtSdhLineTxS1Set(AtSdhLine self, uint8 value);
uint8 AtSdhLineTxS1Get(AtSdhLine self);
uint8 AtSdhLineRxS1Get(AtSdhLine self);

/* K1 */
eAtModuleSdhRet AtSdhLineTxK1Set(AtSdhLine self, uint8 value);
uint8 AtSdhLineTxK1Get(AtSdhLine self);
uint8 AtSdhLineRxK1Get(AtSdhLine self);

/* K2 */
eAtModuleSdhRet AtSdhLineTxK2Set(AtSdhLine self, uint8 value);
uint8 AtSdhLineTxK2Get(AtSdhLine self);
uint8 AtSdhLineRxK2Get(AtSdhLine self);

/* Enable/disable insert TOH (K1, K2, S1, ...) */
eAtModuleSdhRet AtSdhLineOverheadBytesInsertEnable(AtSdhLine self, eBool enable);
eBool AtSdhLineOverheadBytesInsertIsEnabled(AtSdhLine self);

/* BER monitoring */
AtBerController AtSdhLineRsBerControllerGet(AtSdhLine self);
AtBerController AtSdhLineMsBerControllerGet(AtSdhLine self);

/* Interface to fast access sub channels in a Line */
/* Access AUG */
AtSdhAug AtSdhLineAug64Get(AtSdhLine self, uint8 aug64Id);
AtSdhAug AtSdhLineAug16Get(AtSdhLine self, uint8 aug16Id);
AtSdhAug AtSdhLineAug4Get(AtSdhLine self, uint8 aug4Id);
AtSdhAug AtSdhLineAug1Get(AtSdhLine self, uint8 aug1Id);

/* Access AU */
AtSdhAu AtSdhLineAu4_64cGet(AtSdhLine self, uint8 aug64Id);
AtSdhAu AtSdhLineAu4_16cGet(AtSdhLine self, uint8 aug16Id);
AtSdhAu AtSdhLineAu4_4cGet(AtSdhLine self, uint8 aug4Id);
AtSdhAu AtSdhLineAu4Get(AtSdhLine self, uint8 aug1Id);
AtSdhAu AtSdhLineAu3Get(AtSdhLine self, uint8 aug1Id, uint8 au3Id);
AtSdhAu AtSdhLineAu4_16ncGet(AtSdhLine self, uint8 aug16Id);
AtSdhAu AtSdhLineAu4_ncGet(AtSdhLine self, uint8 aug1Id);

/* Access TUG */
AtSdhTug AtSdhLineTug3Get(AtSdhLine self, uint8 aug1Id, uint8 tug3Id);
AtSdhTug AtSdhLineTug2Get(AtSdhLine self, uint8 aug1Id, uint8 au3Tug3Id, uint8 tug2Id);

/* Access TU */
AtSdhTu AtSdhLineTu3Get(AtSdhLine self, uint8 aug1Id, uint8 tug3Id);
AtSdhTu AtSdhLineTu1xGet(AtSdhLine self, uint8 aug1Id, uint8 au3Tug3Id, uint8 tug2Id, uint8 tuId);

/* Access VC */
AtSdhVc AtSdhLineVc4_64cGet(AtSdhLine self, uint8 aug64Id);
AtSdhVc AtSdhLineVc4_16cGet(AtSdhLine self, uint8 aug16Id);
AtSdhVc AtSdhLineVc4_4cGet(AtSdhLine self, uint8 aug4Id);
AtSdhVc AtSdhLineVc4Get(AtSdhLine self, uint8 aug1Id);
AtSdhVc AtSdhLineVc3Get(AtSdhLine self, uint8 aug1Id, uint8 au3Tug3Id);
AtSdhVc AtSdhLineVc1xGet(AtSdhLine self, uint8 aug1Id, uint8 au3Tug3Id, uint8 tug2Id, uint8 tuId);
AtSdhVc AtSdhLineVc4_16ncGet(AtSdhLine self, uint8 aug16Id);
AtSdhVc AtSdhLineVc4_ncGet(AtSdhLine self, uint8 aug1Id);

/* Access STS-1 */
AtSdhSts AtSdhLineStsGet(AtSdhLine self, uint8 stsId);

/* Control SERDES */
AtSerdesController AtSdhLineSerdesController(AtSdhLine self);

/* Line PRBS */
AtPrbsEngine AtSdhLinePrbsEngineGet(AtSdhLine self);

/* HDLC channels */
AtHdlcChannel AtSdhLineDccChannelCreate(AtSdhLine self, eAtSdhLineDccLayer layers);
eAtRet AtSdhLineDccChannelDelete(AtSdhLine self, AtHdlcChannel dccChannel);
AtHdlcChannel AtSdhLineDccChannelGet(AtSdhLine self, eAtSdhLineDccLayer layers);

/* Z1/Z2 */
eAtRet AtSdhLineZ1MonitorPositionSet(AtSdhLine self, uint8 sts1);
uint8 AtSdhLineZ1MonitorPositionGet(AtSdhLine self);
eBool AtSdhLineZ1CanMonitor(AtSdhLine self);
eAtRet AtSdhLineZ2MonitorPositionSet(AtSdhLine self, uint8 sts1);
uint8 AtSdhLineZ2MonitorPositionGet(AtSdhLine self);
eBool AtSdhLineZ2CanMonitor(AtSdhLine self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHLINE_H_ */

#include "AtSdhLineDeprecated.h"

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhSts.h
 * 
 * Created Date: May 17, 2017
 *
 * Description : This class will abstract STS-1 time-slot. Almost interfaces of
 *               AtSdhChannel are available at this class.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHSTS_H_
#define _ATSDHSTS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtStsGroup AtSdhStsGroupGet(AtSdhSts self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHSTS_H_ */


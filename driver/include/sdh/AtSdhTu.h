/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhTu.h
 * 
 * Created Date: Aug 21, 2012
 *
 * Description : TU class. Almost interfaces of AtSdhChannel are applicable
 *               except mapping interfaces. Only TU-12 can use mapping interfaces
 *               because it can map VC-12 or VC-11. Other kinds of TU always
 *               have one corresponding VC.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHTU_H_
#define _ATSDHTU_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtSdhPath.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtSdhTu
 * @{
 */
/** @brief TU mapping type */
typedef enum eAtSdhTuMapType
    {
    cAtSdhTuMapTypeNone,        /**< Corresponding VC-m is mapped to TU */

    /* Only TU-12 is mappable */
    cAtSdhTuMapTypeTu12MapVc12, /**< TU-12 maps VC-12. This is default mapping */
    cAtSdhTuMapTypeTu12MapVc11  /**< TU-12 maps VC-11 */
    }eAtSdhTuMapType;
/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHTU_H_ */


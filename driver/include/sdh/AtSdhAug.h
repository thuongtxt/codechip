/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtSdhAug.h
 * 
 * Created Date: Aug 21, 2012
 *
 * Description : AUG class. Although this class extends AtSdhChannel, only mapping
 *               methods are applicable for this class. That means it has no
 *               alarm, counter, timing mode, ...
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDHAUG_H_
#define _ATSDHAUG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtSdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtSdhAug
 * @{
 */
/** @brief AUG Mapping type */
typedef enum eAtSdhAugMapType
    {
    cAtSdhAugMapTypeNone,            /**< No mapping */

    /* AUG-64 mapping types */
    cAtSdhAugMapTypeAug64MapVc4_64c,  /**< AUG-64 maps 1xVC-4-64c */
    cAtSdhAugMapTypeAug64Map4xAug16s, /**< AUG-64 maps 4xAUG-16s */

    /* AUG-16 mapping types */
    cAtSdhAugMapTypeAug16MapVc4_16c, /**< AUG-16 maps 1xVC-4-16c */
    cAtSdhAugMapTypeAug16Map4xAug4s, /**< AUG-16 maps 4xAUG-4s */

    /* AUG-4 mapping types */
    cAtSdhAugMapTypeAug4MapVc4_4c,   /**< AUG-4 maps 1xVC-4-4c */
    cAtSdhAugMapTypeAug4Map4xAug1s,  /**< AUG-4 maps 4xAUG-1s */

    /* AUG-1 mapping types */
    cAtSdhAugMapTypeAug1MapVc4,      /**< AUG-1 maps VC-4 */
    cAtSdhAugMapTypeAug1Map3xVc3s    /**< AUG-1 maps 3xVC-3s */
    }eAtSdhAugMapType;
/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATSDHAUG_H_ */


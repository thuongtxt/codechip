/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : AtModuleSdh.h
 * 
 * Created Date: Jul 25, 2012
 *
 * Description : SDH Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULESDH_H_
#define _ATMODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"
#include "../att/AtAttSdhManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleSdh
 * @{
 */
/** @brief SDH module class */
typedef struct tAtModuleSdh *AtModuleSdh;

/** @brief Abstract class of Line and Path */
typedef struct tAtSdhChannel * AtSdhChannel;

/** @brief Line class */
typedef struct tAtSdhLine * AtSdhLine;

/** @brief AtSdhAug class */
typedef struct tAtSdhAug * AtSdhAug;

/** @brief AtSdhAu class */
typedef struct tAtSdhAu * AtSdhAu;

/** @brief AtSdhPath class */
typedef struct tAtSdhPath * AtSdhPath;

/** @brief SDH module return codes */
typedef uint32 eAtModuleSdhRet;

/** @brief AtSdhTu class */
typedef struct tAtSdhTu * AtSdhTu;

/** @brief AtSdhTug class */
typedef struct tAtSdhTug * AtSdhTug;

/** @brief AtSdhVc class */
typedef struct tAtSdhVc * AtSdhVc;

/** @brief STS Group */
typedef struct tAtStsGroup * AtStsGroup;

/** @brief AtSdhSts class */
typedef struct tAtSdhSts * AtSdhSts;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Access lines */
uint8 AtModuleSdhMaxLinesGet(AtModuleSdh self);
AtSdhLine AtModuleSdhLineGet(AtModuleSdh self, uint8 lineId);
uint32 AtModuleSdhMaxLineRate(AtModuleSdh self);

/* TFI-5 lines */
uint8 AtModuleSdhNumTfi5Lines(AtModuleSdh self);
AtSdhLine AtModuleSdhTfi5LineGet(AtModuleSdh self, uint8 tfi5LineId);

/* Clock configuration (deprecated by clock extractor APIs) */
eAtModuleSdhRet AtModuleSdhOutputClockSourceSet(AtModuleSdh self, uint8 refOutId, AtSdhLine source);
AtSdhLine AtModuleSdhOutputClockSourceGet(AtModuleSdh self, uint8 refOutId);

/* EC1 lines control */
uint32 AtModuleSdhStartEc1LineIdGet(AtModuleSdh self);
uint32 AtModuleSdhNumEc1LinesGet(AtModuleSdh self);

/* ATT */
AtObjectAny AtModuleSdhAttManager(AtModuleSdh self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULESDH_H_ */


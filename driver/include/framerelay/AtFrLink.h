/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : FR
 * 
 * File        : AtFrLink.h
 * 
 * Created Date: Aug 3, 2012
 *
 * Description : Frame Relay link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFRLINK_H_
#define _ATFRLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcLink.h" /* Super class */
#include "AtEthFlow.h"
#include "AtIterator.h"
#include "AtModuleFr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleFr
 * @{
 */

/**
 * @brief Frame relay link counters
 */
typedef struct tAtFrLinkCounters
    {
    uint32 txByte;         /**< Transmit byte count per link */
    uint32 rxFcsError;     /**< FCS error counter */
    uint32 rxInvAddrCtrl;  /**< Invalid address and control characters counter */
    uint32 rxIllegalFrame; /**< Illegal fragments/frames counter */
    uint32 rxAbortFrame;   /**< Aborted fragments/frames counter */
    uint32 rxByte;         /**< Receive byte count per link */
    uint32 rxExceedMru;    /**< MRU exceeded counter */
    }tAtFrLinkCounters;

/**
 * @brief Frame relay link encapsulation type
 */
typedef enum eAtFrLinkEncapType
    {
    cAtFrLinkEncapTypeUnknown,         /**< Unknown */
    cAtFrLinkEncapTypeIetfWithSNAP,    /**< IETF encapsulation with SNAP header, OUI = 0x00-00-00 */
    cAtFrLinkEncapTypeIetfWithoutSNAP, /**< IETF encapsulation without SNAP header */
    cAtFrLinkEncapTypeCisco            /**< CISCO encapsulation */
    }eAtFrLinkEncapType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtFrVirtualCircuit AtFrLinkVirtualCircuitCreate(AtFrLink self, uint32 dlci);
AtFrVirtualCircuit AtFrLinkVirtualCircuitGet(AtFrLink self, uint32 dlci);
eAtModuleFrRet AtFrLinkVirtualCircuitDelete(AtFrLink self, uint32 dlci);

/* Iterator to iterate all of created virtual circuits */
AtIterator AtFrLinkVirtualCircuitIteratorGet(AtFrLink self);
uint32 AtFrLinkNumberVirtualCircuitGet(AtFrLink self);

/* Encapsulation type */
eAtModuleFrRet AtFrLinkEncapTypeSet(AtFrLink self, eAtFrLinkEncapType encapType);
eAtFrLinkEncapType AtFrLinkEncapTypeGet(AtFrLink self);

/* Q922 Address field */
eAtModuleFrRet AtFrLinkTxQ922AddressDCSet(AtFrLink self, uint8 value);
uint8 AtFrLinkTxQ922AddressDCGet(AtFrLink self);
eAtModuleFrRet AtFrLinkTxQ922AddressCRSet(AtFrLink self, uint8 value);
uint8 AtFrLinkTxQ922AddressCRGet(AtFrLink self);
eAtModuleFrRet AtFrLinkTxQ922AddressFECNSet(AtFrLink self, uint8 fecn);
uint8 AtFrLinkTxQ922AddressFECNGet(AtFrLink self);
eAtModuleFrRet AtFrLinkTxQ922AddressBECNSet(AtFrLink self, uint8 becn);
uint8 AtFrLinkTxQ922AddressBECNGet(AtFrLink self);
eAtModuleFrRet AtFrLinkTxQ922AddressDESet(AtFrLink self, uint8 de);
uint8 AtFrLinkTxQ922AddressDEGet(AtFrLink self);
eAtModuleFrRet AtFrLinkTxQ922AddressDlCoreSet(AtFrLink self, uint8 de);
uint8 AtFrLinkTxQ922AddressDlCoreGet(AtFrLink self);

#ifdef __cplusplus
}
#endif
#endif /* _ATFRLINK_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : FR
 * 
 * File        : AtMfrBundle.h
 * 
 * Created Date: Aug 9, 2012
 *
 * Description : Frame Relay bundle
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _AtMFRBUNDLE_H_
#define _AtMFRBUNDLE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcBundle.h" /* Super class */
#include "AtEthFlow.h"
#include "AtModuleFr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleFr
 * @{
 */

/** @brief MFR counter type */
typedef enum eAtMfrBundleCounterType
    {
    cAtMfrBundleCounterTypeUnKnown,              /**< Unknown counter type */
    cAtMfrBundleCounterTypeTxGoodPkts,           /**< Transmit Good packet counter */
    cAtMfrBundleCounterTypeTxGoodBytes,          /**< Transmit Good byte counter */
    cAtMfrBundleCounterTypeTxDiscardPkts,        /**< Transmit packet discard counter, drop  packet when buffer full */
    cAtMfrBundleCounterTypeTxFragments,          /**< Transmit fragments  counter */

    cAtMfrBundleCounterTypeRxGoodPkts,           /**< Receive Good packet counter */
    cAtMfrBundleCounterTypeRxGoodBytes,          /**< Receive Good byte counter */
    cAtMfrBundleCounterTypeRxDiscardPkts,        /**< Receive packet discard counter, drop  packet when buffer full */
    cAtMfrBundleCounterTypeRxFragments           /**< Receive fragments  counter */

    }eAtMfrBundleCounterType;

/** @brief MFR Fragmentation format */
typedef enum eAtMfrBundleFragmentFormat
    {
    cAtMfrBundleFragmentFormatUnknown,  /**< Unknown format */
    cAtMfrBundleFragmentFormatEndToEnd, /**< End-to-End fragmentation format */
    cAtMfrBundleFragmentFormatUNINNI    /**< UNI/NNI fragmentation format */
    }eAtMfrBundleFragmentFormat;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Virtual circuit control */
AtFrVirtualCircuit AtMfrBundleVirtualCircuitCreate(AtMfrBundle self, uint32 dlci);
eAtModuleFrRet AtMfrBundleVirtualCircuitDelete(AtMfrBundle self, uint32 dlci);
AtFrVirtualCircuit AtMfrBundleVirtualCircuitGet(AtMfrBundle self, uint32 dlci);
AtIterator AtMfrBundleVirtualCircuitIteratorGet(AtMfrBundle self);
uint32 AtMfrBundleNumberVirtualCircuitGet(AtMfrBundle self);

/* Fragmentation format */
eAtRet AtMfrBundleFragmentFormatSet(AtMfrBundle self, eAtMfrBundleFragmentFormat format);
eAtMfrBundleFragmentFormat AtMfrBundleFragmentFormatGet(AtMfrBundle self);

#ifdef __cplusplus
}
#endif
#endif /* _AtMFRBUNDLE_H_ */


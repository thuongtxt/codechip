/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : FR
 * 
 * File        : AtModuleFr.h
 * 
 * Created Date: Aug 13, 2012
 *
 * Description : Frame Relay module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEFR_H_
#define _ATMODULEFR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h" /* Super class */
#include "AtIterator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleFr
 * @{
 */

/** @brief Module frame relay */
typedef struct tAtModuleFr * AtModuleFr;

/** @brief Frame relay link class */
typedef struct tAtFrLink * AtFrLink;

/** @brief Multiframe relay bundle class */
typedef struct tAtMfrBundle * AtMfrBundle;

/** @brief Frame relay module return codes */
typedef uint32 eAtModuleFrRet;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Multi frame relay bundle. The following APIs are used for products that have
 * their own bundle ID space. For products that share bundle ID space with MLPPP,
 * AtModuleEncap will manage them, see its APIs for more detail */
AtMfrBundle AtModuleFrMfrBundleCreate(AtModuleFr self, uint32 bundleId);
AtMfrBundle AtModuleFrMfrBundleGet(AtModuleFr self, uint32 bundleId);
eAtModuleFrRet AtModuleFrMfrBundleDelete(AtModuleFr self, uint32 bundleId);

/* MFR bundles Iterator */
AtIterator AtModuleFrMfrBundleIteratorCreate(AtModuleFr self);

/* Max supported bundles */
uint32 AtModuleFrMaxBundlesGet(AtModuleFr self);

/* Max links per bundle */
uint32 AtModuleFrMaxLinksPerBundleGet(AtModuleFr self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEFR_H_ */


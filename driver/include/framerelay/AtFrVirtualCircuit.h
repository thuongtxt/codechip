/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      :FR
 * 
 * File        : AtFrVirtualCircuit.h
 * 
 * Created Date: Apr 26, 2016
 *
 * Description : Frame relay virtual circuit
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFRVIRTUALCIRCUIT_H_
#define _ATFRVIRTUALCIRCUIT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h"
#include "AtFrLink.h"
#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleFr
 * @{
 */

/** @brief FR virtual circuit counter type */
typedef enum eAtFrVirtualCircuitCounterType
    {
    cAtFrVirtualCircuitCounterTypeUnKnown,              /**< Unknown counter type */
    cAtFrVirtualCircuitCounterTypeTxGoodPkts,           /**< Transmit Good packet counter */
    cAtFrVirtualCircuitCounterTypeTxGoodBytes,          /**< Transmit Good byte counter */
    cAtFrVirtualCircuitCounterTypeRxGoodPkts,           /**< Receive Good packet counter */
    cAtFrVirtualCircuitCounterTypeRxGoodBytes           /**< Receive Good byte counter */
    }eAtFrVirtualCircuitCounterType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* ETH Flow binding */
eAtModuleFrRet AtFrVirtualCircuitFlowBind(AtFrVirtualCircuit self, AtEthFlow flow);
AtEthFlow AtFrVirtualCircuitBoundFlowGet(AtFrVirtualCircuit self);

/* OAM flow */
AtEthFlow AtFrVirtualCircuitOamFlowGet(AtFrVirtualCircuit self);

/* Association */
AtMfrBundle AtFrVirtualCircuitBundleGet(AtFrVirtualCircuit self);
AtFrLink AtFrVirtualCircuitLinkGet(AtFrVirtualCircuit self);
uint32 AtFrVirtualCircuitDlciGet(AtFrVirtualCircuit self);

#ifdef __cplusplus
}
#endif
#endif /* _ATFRVIRTUALCIRCUIT_H_ */


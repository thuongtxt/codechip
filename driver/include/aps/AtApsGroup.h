/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsGroup.h
 * 
 * Created Date: Aug 2, 2016
 *
 * Description : Generic APS group interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATAPSGROUP_H_
#define _ATAPSGROUP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtApsGroup
 * @{
 */

/**
 * @brief APS Group selection state
 */
typedef enum eAtApsGroupState
    {
    cAtApsGroupStateUnknown,   /**< Invalid state */
    cAtApsGroupStateWorking,   /**< All of registered selectors will select working channel. */
    cAtApsGroupStateProtection /**< All of registered selectors will select protection channel. */
    }eAtApsGroupState;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModuleApsRet AtApsGroupInit(AtApsGroup self);
uint32 AtApsGroupIdGet(AtApsGroup self);

/* Selector management */
eAtModuleApsRet AtApsGroupSelectorAdd(AtApsGroup self, AtApsSelector selector);
eAtModuleApsRet AtApsGroupSelectorRemove(AtApsGroup self, AtApsSelector selector);
uint32 AtApsGroupNumSelectors(AtApsGroup self);
AtApsSelector AtApsGroupSelectorAtIndex(AtApsGroup self, uint32 selectorIndex);

/* State (working/protection) selection */
eAtModuleApsRet AtApsGroupStateSet(AtApsGroup self, eAtApsGroupState state);
eAtApsGroupState AtApsGroupStateGet(AtApsGroup self);

#ifdef __cplusplus
}
#endif
#endif /* _ATAPSGROUP_H_ */


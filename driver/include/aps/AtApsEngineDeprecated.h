/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsEngineDeprecated.h
 * 
 * Created Date: Mar 22, 2018
 *
 * Description : Deprecated APS engine interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATAPSENGINEDEPRECATED_H_
#define _ATAPSENGINEDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/*
 * @brief Switching reasons, deprecated by eAtApsRequestState
 */
typedef enum eAtApsSwitchingReason
    {
    cAtApsSwitchingReasonNone = cAtApsRequestStateNr,   /* None, engine has not done any switching action */
    cAtApsSwitchingReasonWtr  = cAtApsRequestStateWtr,  /* Wait-to-Restore switching action */
    cAtApsSwitchingReasonMs   = cAtApsRequestStateMs,   /* Manual Switch switching action */
    cAtApsSwitchingReasonSd   = cAtApsRequestStateSd,   /* Signal Degrade switching action */
    cAtApsSwitchingReasonSf   = cAtApsRequestStateSf,   /* Signal Failure switching action */
    cAtApsSwitchingReasonFs   = cAtApsRequestStateFs,   /* Force Switch switching action */
    cAtApsSwitchingReasonLp   = cAtApsRequestStateLp,   /* Lockout of Protection switching action */
    cAtApsSwitchingReasonClear   /* Switching action due to external command cleared */
    }eAtApsSwitchingReason;


/*--------------------------- Forward declarations ---------------------------*/

/* Switching reason, deprecated by AtApsEngineRequestStateGet() */
eAtApsSwitchingReason AtApsEngineSwitchingReasonGet(AtApsEngine self);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATAPSENGINEDEPRECATED_H_ */


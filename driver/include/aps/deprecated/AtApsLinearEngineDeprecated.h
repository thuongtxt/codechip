/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsLinearEngineDeprecated.h
 * 
 * Created Date: Jun 23, 2016
 *
 * Description : Linear APS deprecated APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATAPSLINEARENGINEDEPRECATED_H_
#define _ATAPSLINEARENGINEDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* APS Linear Architecture Switching mode */
typedef enum eAtApsLinearSwitchType
    {
    cAtApsLinearSwitchTypeNonRev = cAtApsSwitchTypeNonRev, /**< Non-revertive switching */
    cAtApsLinearSwitchTypeRev    = cAtApsSwitchTypeRev  /**< Revertive switching */
    }eAtApsLinearSwitchType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Switching type */
eAtModuleApsRet AtApsLinearEngineSwitchTypeSet(AtApsLinearEngine self, eAtApsLinearSwitchType swType);
eAtApsLinearSwitchType AtApsLinearEngineSwitchTypeGet(AtApsLinearEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _ATAPSLINEARENGINEDEPRECATED_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsUpsrEngine.h
 * 
 * Created Date: Jun 22, 2016
 *
 * Description : UPSR engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATAPSUPSRENGINE_H_
#define _ATAPSUPSRENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtApsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtApsUpsrEngine
 * @{
 */

/** @brief UPSR engine */
typedef struct tAtApsUpsrEngine * AtApsUpsrEngine;

/**
 * @brief UPSR external commands
 */
typedef enum eAtApsUpsrExtCmd
    {
    cAtApsUpsrExtCmdUnknown, /**< Unknown command */
    cAtApsUpsrExtCmdClear,  /**< Clear */
    cAtApsUpsrExtCmdFs2Wrk, /**< Force switch to working */
    cAtApsUpsrExtCmdFs2Prt, /**< Force switch to protection */
    cAtApsUpsrExtCmdMs2Wrk, /**< Manual switch to working */
    cAtApsUpsrExtCmdMs2Prt, /**< Manual switch to protection */
    cAtApsUpsrExtCmdLp      /**< Lock out of Protection */
    }eAtApsUpsrExtCmd;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModuleApsRet AtApsUpsrEngineExtCmdSet(AtApsUpsrEngine self, eAtApsUpsrExtCmd extCmd);
eAtApsUpsrExtCmd AtApsUpsrEngineExtCmdGet(AtApsUpsrEngine self);

AtSdhPath AtApsUpsrEngineWorkingPathGet(AtApsUpsrEngine self);
AtSdhPath AtApsUpsrEngineProtectionPathGet(AtApsUpsrEngine self);
AtSdhPath AtApsUpsrEngineActivePathGet(AtApsUpsrEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _ATAPSUPSRENGINE_H_ */


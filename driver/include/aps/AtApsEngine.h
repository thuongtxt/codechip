/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsEngine.h
 * 
 * Created Date: Jun 3, 2013
 *
 * Description : APS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATAPSENGINE_H_
#define _ATAPSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h"
#include "AtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtApsEngine
 * @{
 */

/**
 * @brief Engine states
 */
typedef enum eAtApsEngineState
    {
    cAtApsEngineStateUnknown, /**< Engine in unknown state */
    cAtApsEngineStateStop,    /**< Engine stopped */
    cAtApsEngineStateStart    /**< Engine started */
    }eAtApsEngineState;

/** @brief APS Switching mode */
typedef enum eAtApsSwitchType
    {
    cAtApsSwitchTypeUnknown = 0, /**< Unknown switching type */
    cAtApsSwitchTypeNonRev  = 1, /**< Non-revertive switching */
    cAtApsSwitchTypeRev     = 2  /**< Revertive switching */
    }eAtApsSwitchType;

/**
 * @brief APS Request states
 */
typedef enum eAtApsRequestState
    {
    cAtApsRequestStateNr,     /**< No-Request state */
    cAtApsRequestStateWtr,    /**< Wait-to-Restore state */
    cAtApsRequestStateMs,     /**< Manual Switch state */
    cAtApsRequestStateSd,     /**< Signal Degrade state */
    cAtApsRequestStateSf,     /**< Signal Failure state */
    cAtApsRequestStateFs,     /**< Force Switch state */
    cAtApsRequestStateLp,     /**< Lockout of Protection state */
    cAtApsRequestStateUnknown /**< Unknown state */
    }eAtApsRequestState;

/**
 * @brief Events that can be reported via interrupt
 */
typedef enum eAtApsEngineEvent
    {
    cAtApsEngineEventNone        = 0,     /**< No event */
    cAtApsEngineEventSwitch      = cBit0, /**< Switching action has just happened */
    cAtApsEngineEventStateChange = cBit1  /**< APS engine has changed its request state */
    }eAtApsEngineEvent;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModuleApsRet AtApsEngineInit(AtApsEngine self);
uint32 AtApsEngineIdGet(AtApsEngine self);

/* Engine state */
eAtModuleApsRet AtApsEngineStart(AtApsEngine self);
eAtModuleApsRet AtApsEngineStop(AtApsEngine self);
eAtApsEngineState AtApsEngineStateGet(AtApsEngine self);
eBool AtApsEngineCanStart(AtApsEngine self);

/* Switching type */
eAtModuleApsRet AtApsEngineSwitchTypeSet(AtApsEngine self, eAtApsSwitchType swType);
eAtApsSwitchType AtApsEngineSwitchTypeGet(AtApsEngine self);

/* Switching condition */
eAtModuleApsRet AtApsEngineSwitchingConditionSet(AtApsEngine self, uint32 defects, eAtApsSwitchingCondition condition);
eAtApsSwitchingCondition AtApsEngineSwitchingConditionGet(AtApsEngine self, uint32 defect);

/* Current request state */
eAtApsRequestState AtApsEngineRequestStateGet(AtApsEngine self);

/* WTR time */
uint32 AtApsEngineWtrGet(AtApsEngine self);
eAtModuleApsRet AtApsEngineWtrSet(AtApsEngine self, uint32 wtrInSec);

#ifdef __cplusplus
}
#endif
#endif /* _ATAPSENGINE_H_ */

#include "AtApsEngineDeprecated.h"

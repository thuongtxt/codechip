/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsSelector.h
 * 
 * Created Date: Aug 2, 2016
 *
 * Description : Generic 2-to-1 APS selector.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATAPSSELECTOR_H_
#define _ATAPSSELECTOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleAps.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModuleApsRet AtApsSelectorInit(AtApsSelector self);
uint32 AtApsSelectorIdGet(AtApsSelector self);

eAtModuleApsRet AtApsSelectorEnable(AtApsSelector self, eBool enabled);
eBool AtApsSelectorIsEnabled(AtApsSelector self);

eAtModuleApsRet AtApsSelectorChannelSelect(AtApsSelector self, AtChannel selectedChannel);
AtChannel AtApsSelectorSelectedChannel(AtApsSelector self);

AtChannel AtApsSelectorWorkingChannelGet(AtApsSelector self);
AtChannel AtApsSelectorProtectionChannelGet(AtApsSelector self);
AtChannel AtApsSelectorOutgoingChannelGet(AtApsSelector self);

#ifdef __cplusplus
}
#endif
#endif /* _ATAPSSELECTOR_H_ */


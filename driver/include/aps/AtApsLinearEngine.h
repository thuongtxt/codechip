/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtApsLinearEngine.h
 * 
 * Created Date: Jun 19, 2013
 *
 * Description : Linear APS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATAPSLINEARENGINE_H_
#define _ATAPSLINEARENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtApsEngine.h" /* Super */
#include "AtList.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtApsLinearEngine
 * @{
 */

/** @brief APS Linear operation mode */
typedef enum eAtApsLinearOpMode
    {
    cAtApsLinearOpModeUni, /**< Uni-direction operation */
    cAtApsLinearOpModeBid  /**< Bid-direction operation */
    }eAtApsLinearOpMode;

/**
 * @brief External commands
 */
typedef enum eAtApsLinearExtCmd
    {
    cAtApsLinearExtCmdUnknown, /**< Unknown command */
    cAtApsLinearExtCmdClear,   /**< Clear */
    cAtApsLinearExtCmdExer,    /**< Exercise */
    cAtApsLinearExtCmdMs,      /**< Manual switch */
    cAtApsLinearExtCmdFs,      /**< Force switch */
    cAtApsLinearExtCmdLp       /**< Lock out of Protection */
    }eAtApsLinearExtCmd;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Operation mode */
eAtModuleApsRet AtApsLinearEngineOpModeSet(AtApsLinearEngine self, eAtApsLinearOpMode opMd);
eAtApsLinearOpMode AtApsLinearEngineOpModeGet(AtApsLinearEngine self);

/* Architecture */
eAtModuleApsRet AtApsLinearEngineArchSet(AtApsLinearEngine self, eAtApsLinearArch arch);
eAtApsLinearArch AtApsLinearEngineArchGet(AtApsLinearEngine self);

/* External command */
eAtModuleApsRet AtApsLinearEngineExtCmdSet(AtApsLinearEngine engine, eAtApsLinearExtCmd extlCmd, AtSdhLine affectedLine);
eAtApsLinearExtCmd AtApsLinearEngineExtCmdGet(AtApsLinearEngine engine);
AtSdhLine AtApsLinearEngineExtCmdAffectedLineGet(AtApsLinearEngine engine);

/* Switching status */
AtSdhLine AtApsLinearEngineSwitchLineGet(AtApsLinearEngine self);
AtList AtApsLinearEngineLinesGet(AtApsLinearEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _ATAPSLINEARENGINE_H_ */

#include "deprecated/AtApsLinearEngineDeprecated.h"


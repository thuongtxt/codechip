/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : AtModuleAps.h
 * 
 * Created Date: May 30, 2013
 *
 * Description : APS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEAPS_H_
#define _ATMODULEAPS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h" /* Super class */
#include "AtSdhLine.h"
#include "AtIterator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleAps
 * @{
 */

/** @brief APS module abstract class */
typedef struct tAtModuleAps * AtModuleAps;

/** @brief APS engine */
typedef struct tAtApsEngine * AtApsEngine;

/** @brief APS Linear engine */
typedef struct tAtApsLinearEngine * AtApsLinearEngine;

/** @brief APS Selector */
typedef struct tAtApsSelector * AtApsSelector;

/** @brief APS Group */
typedef struct tAtApsGroup * AtApsGroup;

/** @brief APS Linear Architecture */
typedef enum eAtApsLinearArch
    {
    cAtApsLinearArch11, /**< 1+1 architecture */
    cAtApsLinearArch1n  /**< 1:n architecture */
    }eAtApsLinearArch;

/**
 * @brief Automatic switching condition
 */
typedef enum eAtApsSwitchingCondition
    {
    cAtApsSwitchingConditionInvalid, /**< Invalid switching condition */
    cAtApsSwitchingConditionNone,    /**< No switching */
    cAtApsSwitchingConditionSd,      /**< SD condition */
    cAtApsSwitchingConditionSf       /**< SF condition */
    }eAtApsSwitchingCondition;

/** @brief APS module return codes */
typedef uint32 eAtModuleApsRet;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Linear APS engine management */
uint32 AtModuleApsMaxNumLinearEngines(AtModuleAps self);
AtApsEngine AtModuleApsLinearEngineCreate(AtModuleAps self, uint32 engineId, AtSdhLine lines[], uint8 numLines, eAtApsLinearArch arch);
eAtModuleApsRet AtModuleApsLinearEngineDelete(AtModuleAps self, uint32 engineId);
AtApsEngine AtModuleApsLinearEngineGet(AtModuleAps self, uint32 engineId);
AtIterator AtModuleApsLinearEngineIteratorCreate(AtModuleAps self);

/* UPSR engine management */
uint32 AtModuleApsMaxNumUpsrEngines(AtModuleAps self);
AtApsEngine AtModuleApsUpsrEngineCreate(AtModuleAps self, uint32 engineId, AtSdhPath working, AtSdhPath protection);
eAtModuleApsRet AtModuleApsUpsrEngineDelete(AtModuleAps self, uint32 engineId);
AtApsEngine AtModuleApsUpsrEngineGet(AtModuleAps self, uint32 engineId);
AtIterator AtModuleApsUpsrEngineIteratorCreate(AtModuleAps self);

/* APS Selector management. */
uint32 AtModuleApsMaxNumSelectors(AtModuleAps self);
AtApsSelector AtModuleApsSelectorCreate(AtModuleAps self, uint32 selectorId, AtChannel working, AtChannel protection, AtChannel outgoing);
eAtModuleApsRet AtModuleApsSelectorDelete(AtModuleAps self, uint32 selectorId);
AtApsSelector AtModuleApsSelectorGet(AtModuleAps self, uint32 selectorId);
AtIterator AtModuleApsSelectorIteratorCreate(AtModuleAps self);

/* APS Group management. */
uint32 AtModuleApsMaxNumGroups(AtModuleAps self);
AtApsGroup AtModuleApsGroupCreate(AtModuleAps self, uint32 groupId);
eAtRet AtModuleApsGroupDelete(AtModuleAps self, uint32 groupId);
AtApsGroup AtModuleApsGroupGet(AtModuleAps self, uint32 groupId);
AtIterator AtModuleApsGroupIteratorCreate(AtModuleAps self);

/* STS Group management. */
uint32 AtModuleApsMaxNumStsGroups(AtModuleAps self);
AtStsGroup AtModuleApsStsGroupCreate(AtModuleAps self, uint32 groupId);
eAtRet AtModuleApsStsGroupDelete(AtModuleAps self, uint32 groupId);
AtStsGroup AtModuleApsStsGroupGet(AtModuleAps self, uint32 groupId);
AtIterator AtModuleApsStsGroupIteratorCreate(AtModuleAps self);

uint8 AtModuleApsPeriodicProcess(AtModuleAps self, uint32 periodInMs);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEAPS_H_ */


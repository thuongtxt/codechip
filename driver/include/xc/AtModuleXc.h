/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : AtModuleXc.h
 * 
 * Created Date: Apr 28, 2014
 *
 * Description : XC module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEXC_H_
#define _ATMODULEXC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleXc
 * @{
 */
/** @brief XC module class */
typedef struct tAtModuleXc * AtModuleXc;

/** @brief Cross-connect class */
typedef struct tAtCrossConnect * AtCrossConnect;

/** @brief DS0 Cross-connect class */
typedef struct tAtCrossConnectDs0 * AtCrossConnectDs0;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Supported cross-connects */
AtCrossConnect AtModuleXcDe1CrossConnectGet(AtModuleXc self);
AtCrossConnect AtModuleXcDe3CrossConnectGet(AtModuleXc self);
AtCrossConnect AtModuleXcVcCrossConnectGet(AtModuleXc self);
AtCrossConnectDs0 AtModuleXcDs0CrossConnectGet(AtModuleXc self);

#endif /* _ATMODULEXC_H_ */

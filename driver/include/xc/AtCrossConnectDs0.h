/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : AtCrossConnectDs0.h
 * 
 * Created Date: Feb 7, 2015
 *
 * Description : DS0 cross-connect
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCROSSCONNECTDS0_H_
#define _ATCROSSCONNECTDS0_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCrossConnect.h" /* Super class */
#include "AtModuleXc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Connect/disconnect */
eAtModuleXcRet AtCrossConnectDs0Connect(AtCrossConnectDs0 self,
                                        AtPdhDe1 sourceDe1, uint32 sourceTimeslot,
                                        AtPdhDe1 destDe1, uint32 destTimeslot);
eAtModuleXcRet AtCrossConnectDs0Disconnect(AtCrossConnectDs0 self,
                                           AtPdhDe1 sourceDe1, uint32 sourceTimeslot,
                                           AtPdhDe1 destDe1, uint32 destTimeslot);

/* Query source and destinations */
uint32 AtCrossConnectDs0NumDestChannelsGet(AtCrossConnectDs0 self, AtPdhDe1 sourceDe1, uint8 sourceTimeslotId);
AtPdhDe1 AtCrossConnectDs0DestChannelGetByIndex(AtCrossConnectDs0 self,
                                                AtPdhDe1 sourceDe1, uint32 sourceTimeslotId, uint32 destIndex,
                                                uint8 *destTimeslot);
AtPdhDe1 AtCrossConnectDs0SourceChannelGet(AtCrossConnectDs0 self, AtPdhDe1 destDe1, uint8 destTimeslot, uint8 *sourceTimeslot);

#ifdef __cplusplus
}
#endif
#endif /* _ATCROSSCONNECTDS0_H_ */

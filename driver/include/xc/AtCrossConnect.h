/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : AtCrossConnect.h
 * 
 * Created Date: Aug 1, 2014
 *
 * Description : Cross connect
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCROSSCONNECT_H_
#define _ATCROSSCONNECT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtChannel.h"
#include "AtModuleXc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
const char *AtCrossConnectTypeString(AtCrossConnect self);

/* Connect/disconnect */
eAtModuleXcRet AtCrossConnectChannelConnect(AtCrossConnect self, AtChannel source, AtChannel dest);
eAtModuleXcRet AtCrossConnectChannelDisconnect(AtCrossConnect self, AtChannel source, AtChannel dest);

/* Query source and destinations */
uint32 AtCrossConnectNumDestChannelsGet(AtCrossConnect self, AtChannel source);
AtChannel AtCrossConnectDestChannelGetByIndex(AtCrossConnect self, AtChannel source, uint32 destIndex);
AtChannel AtCrossConnectSourceChannelGet(AtCrossConnect self, AtChannel dest);

/* Module */
AtModule AtCrossConnectModuleGet(AtCrossConnect self);

/* Util */
eAtRet AtCrossConnectAllConnectionsDelete(AtCrossConnect self);

#endif /* _ATCROSSCONNECT_H_ */


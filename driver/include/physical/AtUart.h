/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtUart.h
 * 
 * Created Date: Apr 20, 2016
 *
 * Description : AtUart declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATUART_H_
#define _ATUART_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtRet.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtUart
 * @{
 */

/** @brief UART abstract class */
typedef struct tAtUart * AtUart;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Read-only attributes */
uint8 AtUartSuperLogicRegionIdGet(AtUart self);
AtDevice AtUartDeviceGet(AtUart self);

/* Transmission */
eAtRet AtUartTransmit(AtUart self, const char *buffer, uint32 bufferSize);
uint32 AtUartReceive(AtUart self, char *buffer, uint32 bufferSize);
void AtUartReceiveDataWait(AtUart self);

/* Debugging */
void AtUartDebug(AtUart self);

#ifdef __cplusplus
}
#endif
#endif /* _ATUART_H_ */


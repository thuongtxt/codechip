/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : AtPowerSupplySensor.h
 * 
 * Created Date: Oct 3, 2016
 *
 * Description : AtPowerSupplySensor declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPOWERSUPPLYSENSOR_H_
#define _ATPOWERSUPPLYSENSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSensor.h"
#include "AtPhysicalClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPowerSupplySensor
 * @{
 */

/** @brief Power supply sensor types */
typedef enum eAtPowerSupplyVoltage
    {
    cAtPowerSupplyVoltageInt,     /**< VccInt */
    cAtPowerSupplyVoltageAux,     /**< VccAux */
    cAtPowerSupplyVoltageBram,    /**< VccBram */
    cAtPowerSupplyVoltagePsintlp, /**< VccPsintlp */
    cAtPowerSupplyVoltagePsintfp, /**< VccPsintfp */
    cAtPowerSupplyVoltagePsaux,   /**< VccPsaux */
    cAtPowerSupplyVoltageUser0,   /**< Additional power supply */
    cAtPowerSupplyVoltageUser1,   /**< Additional power supply */
    cAtPowerSupplyVoltageUser2,   /**< Additional power supply */
    cAtPowerSupplyVoltageUser3    /**< Additional power supply */
    }eAtPowerSupplyVoltage;

/**
 * @brief Power supply sensor alarm types
 */
typedef enum eAtPowerSupplySensorAlarmType
    {
    cAtPowerSupplySensorAlarmOfNone       = 0,       /**< No alarms */
    cAtPowerSupplySensorAlarmOfVccInt     = cBit0,   /**< Alarm of VccInt is set */
    cAtPowerSupplySensorAlarmOfVccAux     = cBit1,   /**< Alarm of VccAux is set */
    cAtPowerSupplySensorAlarmOfVccBram    = cBit2,   /**< Alarm of VccBram is set */
    cAtPowerSupplySensorAlarmOfVccPsintlp = cBit3,   /**< Alarm of VccPsintlp is set */
    cAtPowerSupplySensorAlarmOfVccPsintfp = cBit4,   /**< Alarm of VccPsintfp is set */
    cAtPowerSupplySensorAlarmOfVccPsaux   = cBit5,   /**< Alarm of VccPsaux is set */
    cAtPowerSupplySensorAlarmOfVccUser0   = cBit6,   /**< Alarm of VccUser0 is set */
    cAtPowerSupplySensorAlarmOfVccUser1   = cBit7,   /**< Alarm of VccUser1 is set */
    cAtPowerSupplySensorAlarmOfVccUser2   = cBit8,   /**< Alarm of VccUser2 is set */
    cAtPowerSupplySensorAlarmOfVccUser3   = cBit9    /**< Alarm of VccUser3 is set */
    }eAtPowerSupplySensorAlarmType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtPowerSupplySensorVoltageGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType);

eAtRet AtPowerSupplySensorAlarmUpperThresholdSet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType, uint32 voltage);
uint32 AtPowerSupplySensorAlarmUpperThresholdGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType);
eAtRet AtPowerSupplySensorAlarmLowerThresholdSet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType, uint32 voltage);
uint32 AtPowerSupplySensorAlarmLowerThresholdGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType);

uint32 AtPowerSupplySensorMinRecordedVoltageGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType);
uint32 AtPowerSupplySensorMaxRecordedVoltageGet(AtPowerSupplySensor self, eAtPowerSupplyVoltage voltageType);

#ifdef __cplusplus
}
#endif
#endif /* _ATPOWERSUPPLYSENSOR_H_ */

#include "deprecated/AtPowerSupplySensorDeprecated.h"

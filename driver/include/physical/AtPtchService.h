/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtPtchService.h
 * 
 * Created Date: May 7, 2016
 *
 * Description : PTCH service
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPTCHSERVICE_H_
#define _ATPTCHSERVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPtchService
 * @{
 */

/** @brief PTCH abstract service */
typedef struct tAtPtchService * AtPtchService;

/** @brief PTCH service counter type */
typedef enum eAtPtchServiceCounterType
    {
    cAtPtchServiceCounterTypeTxBytes,          /**< Number of bytes transmitted to XFI */
    cAtPtchServiceCounterTypeTxFrames,         /**< number of frames transmitted to XFI */
    cAtPtchServiceCounterTypeTxOversizeFrames, /**< Number of frames are dropped because of crossing XFI MTU */
    cAtPtchServiceCounterTypeRxBytes,          /**< number of bytes received from XFI regardless frames are error or good */
    cAtPtchServiceCounterTypeRxFrames,         /**< number of frames received from XFI regardless frames are error or good */
    cAtPtchServiceCounterTypeRxOversizeFrames, /**< number of received frames cross XFI MTU */
    cAtPtchServiceCounterTypeRxErrorFrames,    /**< number of received frames have CRC error */
    cAtPtchServiceCounterTypeRxGoodFrames,     /**< number of received good frames */
    cAtPtchServiceCounterTypeRxDropFrames      /**< number of received frames are dropped because of buffer full */
    }eAtPtchServiceCounterType;

/** @brief PTCH service type */
typedef enum eAtPtchServiceType
    {
    cAtPtchServiceTypeUnknown,  /**< Unknown service type */
    cAtPtchServiceTypeCEM,      /**< For PW (CEP/SAToP/CESoP) */
    cAtPtchServiceTypeIMSG_EoP, /**< For iMSG and EoP */
    cAtPtchServiceTypeEXAUI_0,  /**< For eXAUI#0 */
    cAtPtchServiceTypeEXAUI_1,  /**< For eXAUI#1 */
    cAtPtchServiceTypeGeBypass  /**< For GE bypass */
    }eAtPtchServiceType;

/** @brief PTCH mode */
typedef enum eAtPtchMode
    {
    cAtPtchModeUnknown,
    cAtPtchModeOneByte,
    cAtPtchModeTwoBytes
    }eAtPtchMode;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Read-only attributes */
eAtPtchServiceType AtPtchServiceTypeGet(AtPtchService self);
AtModule AtPtchServiceModuleGet(AtPtchService self);

/* Enabling */
eAtRet AtPtchServiceEnable(AtPtchService self, eBool enabled);
eBool AtPtchServiceIsEnabled(AtPtchService self);

/* Ingress two-bytes PTCH (direction from HyPhy to XFI). Note, this is
 * not applicable for eXAUI#0 */
eAtRet AtPtchServiceIngressPtchSet(AtPtchService self, uint16 ptch);
uint16 AtPtchServiceIngressPtchGet(AtPtchService self);

/* Number byte PTCH insert */
eAtRet AtPtchServiceIngressModeSet(AtPtchService self, eAtPtchMode mode);
eAtPtchMode AtPtchServiceIngressModeGet(AtPtchService self);

/* Egress one-byte PTCH (direction from XFI to HyPhy). */
eAtRet AtPtchServiceEgressPtchSet(AtPtchService self, uint16 ptch);
uint16 AtPtchServiceEgressPtchGet(AtPtchService self);

/* Offset attribute is only applicable for eXAUI#0.
 * - Direction from eXAUI --> XFI:   PTCH = eXAUIChannelId + offset
 * - Direction from XFI   --> eXAUI: eXAUIChannelId = channelId - offset
 * Calling these APIs on other service type will lead to error code or
 * invalid value are returned */
eAtRet AtPtchServiceIngressOffsetSet(AtPtchService self, uint32 offset);
uint32 AtPtchServiceIngressOffsetGet(AtPtchService self);
eAtRet AtPtchServiceEgressOffsetSet(AtPtchService self, uint32 offset);
uint32 AtPtchServiceEgressOffsetGet(AtPtchService self);

/* Counters */
eBool AtPtchServiceCounterIsSupported(AtPtchService self, eAtPtchServiceCounterType counterType);
uint32 AtPtchServiceCounterGet(AtPtchService self, eAtPtchServiceCounterType counterType);
uint32 AtPtchServiceCounterClear(AtPtchService self, eAtPtchServiceCounterType counterType);

#ifdef __cplusplus
}
#endif
#endif /* _ATPTCHSERVICE_H_ */


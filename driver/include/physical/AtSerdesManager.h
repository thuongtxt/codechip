/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtSerdesManager.h
 * 
 * Created Date: Jun 17, 2016
 *
 * Description : Serdes manager
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSERDESMANAGER_H_
#define _ATSERDESMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtClasses.h"
#include "AtPhysicalClasses.h"
#include "AtRet.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtSerdesManagerNumSerdesControllers(AtSerdesManager self);
AtSerdesController AtSerdesManagerSerdesControllerGet(AtSerdesManager self, uint32 serdesId);

AtDevice AtSerdesManagerDeviceGet(AtSerdesManager self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSERDESMANAGER_H_ */


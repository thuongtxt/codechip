/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : AtSemController.h
 * 
 * Created Date: Dec 9, 2015
 *
 * Description : SEM controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSEMCONTROLLER_H_
#define _ATSEMCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtClasses.h"
#include "AtUart.h"
#include "AtPhysicalClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtSemController
 * @{
 */

/** @brief Soft Error Mitigation (SEM) Alarm types */
typedef enum eAtSemControllerAlarmType
    {
    cAtSemControllerAlarmNone           = 0,      /**< No any alarm */
    cAtSemControllerAlarmActive         = cBit0,  /**< The HEARTBEAT signal is active while
                                                        status_observation is TRUE. This output issues a
                                                        single-cycle high pulse at least once every 128 clock
                                                        cycles. This signal can be used to implement an
                                                        external watchdog timer to detect â€œcontroller stopâ€�
                                                        scenarios that can occur if the controller or clock
                                                        distribution is disabled by soft errors. When
                                                        status_observation is FALSE, the behavior of the
                                                        HEARTBEAT signal is unspecified. */
    cAtSemControllerAlarmInitialization = cBit1,  /**< The initialization signal is active during controller
                                                        initialization, which occurs one time after the design
                                                        begins operation. */
    cAtSemControllerAlarmObservation    = cBit2,  /**< The observation signal is active during controller
                                                        observation of error detection signals. This signal
                                                        remains active after an error detection while the
                                                        controller queries the hardware for information. */
    cAtSemControllerAlarmCorrection     = cBit3,  /**< The correction signal is active during controller
                                                        correction of an error or during transition through
                                                        this controller state if correction is disabled. */
    cAtSemControllerAlarmClassification = cBit4,  /**< The classification signal is active during controller
                                                        classification of an error or during transition
                                                        through this controller state if classification is
                                                        disabled. */
    cAtSemControllerAlarmInjection      = cBit5,  /**< The injection signal is active during controller
                                                        injection of an error. When an error injection is
                                                        complete, and the controller is ready to inject
                                                        another error or return to observation, this signal
                                                        returns inactive. */
    cAtSemControllerAlarmIdle           = cBit6,  /**< The idle state (inactive but ready to resume) is present
                                                        when five signals (status_initialization, status_observation,
                                                        status_correction, status_classification, and
                                                        status_injection outputs) are low. */
    cAtSemControllerAlarmFatalError     = cBit7,   /**< The halted state (inactive due to fatal error) is present
                                                        when five signals (status_initialization, status_observation,
                                                        status_correction, status_classification, and
                                                        status_injection outputs) are high. */
    cAtSemControllerAlarmErrorEssential      = cBit8, /** < Alarm for the essential error detection */
    cAtSemControllerAlarmErrorCorrectable    = cBit9, /** < Alarm for the correctable error detection */
    cAtSemControllerAlarmErrorNoncorrectable = cBit10, /** < Alarm for the un-correctable error detection */
    cAtSemControllerAlarmHeartbeat           = cBit11, /**< Alarm for the heart-beat event. For the device which have SEM controller per SLR
                                                            it is reported per sem controller. For the device which have common SEM controller for
                                                            all SLRs */
    cAtSemControllerAlarmHeartbeatSlr1       = cBit12, /**< Alarm for the Heartbeat of the SLR#1, the first slave SLR in the case device have several
                                                            SLRs but just have the common SEM controller */
    cAtSemControllerAlarmHeartbeatSlr2       = cBit13, /**< Alarm for the Heartbeat of the SLR#2, the second slave SLR in the case device have several
                                                            SLRs but just have the common SEM controller */
    cAtSemControllerAlarmActiveSlr1          = cBit14, /**< Alarm for the Active of the SLR#1, the first slave SLR in the case device have several
                                                            SLRs but just have the common SEM controller */
    cAtSemControllerAlarmActiveSlr2          = cBit15 /**< Alarm for the Heartbeat of the SLR#2, the second slave SLR in the case device have several
                                                            SLRs but just have the common SEM controller */
    }eAtSemControllerAlarmType;

/** @brief SEM counter types */
typedef enum eAtSemControllerCounterType
    {
    cAtSemControllerCounterTypeEssential,      /**< The counter of asserted essential signal due to an error
                                                    classification status signal. */
    cAtSemControllerCounterTypeUncorrectable,  /**< The counter of uncorrectable events. */
    cAtSemControllerCounterTypeCorrectable     /**< The counter of correctable events. */
    }eAtSemControllerCounterType;

/** @brief Default sensor listener interface */
typedef struct tAtSemControllerEventListener
    {
    void (*AlarmChangeStateNotify)(AtSemController self, uint32 changedAlarms, uint32 currentAlarms, void *userData); /**< Called when alarms have change in state */
    }tAtSemControllerEventListener;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Read-only attributes */
AtDevice AtSemControllerDeviceGet(AtSemController self);
uint32 AtSemControllerIdGet(AtSemController self);
const char* AtSemControllerDeviceDescription(AtSemController self);
uint8 AtSemControllerNumSlr(AtSemController self);

eAtRet AtSemControllerInit(AtSemController self);
eAtRet AtSemControllerValidate(AtSemController self);

/* Counters */
uint32 AtSemControllerCounterGet(AtSemController self, uint32 counterType);
uint32 AtSemControllerCounterClear(AtSemController self, uint32 counterType);

/* Alarm and status. */
uint32 AtSemControllerAlarmGet(AtSemController self);
uint32 AtSemControllerAlarmHistoryGet(AtSemController self);
uint32 AtSemControllerAlarmHistoryClear(AtSemController self);

/* Interrupt. */
eAtRet AtSemControllerInterruptMaskSet(AtSemController self, uint32 alarmMask, uint32 enableMask);
uint32 AtSemControllerInterruptMaskGet(AtSemController self);
uint32 AtSemControllerSupportedAlarms(AtSemController self);

/* Event listener reported through interrupt. */
eAtRet AtSemControllerEventListenerAdd(AtSemController self, tAtSemControllerEventListener *listener, void *userData);
eAtRet AtSemControllerEventListenerRemove(AtSemController self, tAtSemControllerEventListener *listener);

/* UART */
AtUart AtSemControllerUartGet(AtSemController self);
eAtRet AtSemControllerValidateByUart(AtSemController self);
eAtRet AtSemControllerForceErrorByUart(AtSemController self, eAtSemControllerCounterType errorType);
eAtRet AtSemControllerAsyncForceErrorByUart(AtSemController self, eAtSemControllerCounterType errorType);

/* Frame attributes */
uint32 AtSemControllerMaxLfa(AtSemController self);
uint32 AtSemControllerFrameSize(AtSemController self);

/* EBD lookup */
eAtRet AtSemControllerEbdFilePathSet(AtSemController self, uint8 slrNum, const char* filePath);
const char* AtSemControllerEbdFilePathGet(AtSemController self, uint8 slrNum);
eBool AtSemControllerErrorIsEssential(AtSemController self, const char *uartReport, uint32 reportLength);

#ifdef __cplusplus
}
#endif
#endif /* _ATSEMCONTROLLER_H_ */

#include "AtSemControllerDeprecated.h"

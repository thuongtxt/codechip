/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtEyeScanController.h
 * 
 * Created Date: Dec 24, 2014
 *
 * Description : Eye scan controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATEYESCANCONTROLLER_H_
#define _ATEYESCANCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtPhysicalClasses.h"
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtEyeScanControllerInit(AtEyeScanController self);

/* SERDES controller */
AtSerdesController AtEyeScanControllerSerdesControllerGet(AtEyeScanController self);

/* Access lanes */
uint8 AtEyeScanControllerNumLanes(AtEyeScanController self);
AtEyeScanLane AtEyeScanControllerLaneGet(AtEyeScanController self, uint8 laneId);
uint8 AtEyeScanControllerMaxNumLanes(void);

/* Start eyescan */
eAtRet AtEyeScanControllerScan(AtEyeScanController self);
eBool AtEyeScanControllerFinished(AtEyeScanController self);

/* Debugging */
eAtRet AtEyeScanControllerDebug(AtEyeScanController self);

/* Concrete controller (only for internal used) */
AtEyeScanController AtEyeScanControllerStmNew(AtSerdesController serdesController, uint32 drpBaseAddress);
AtEyeScanController AtEyeScanControllerGtyE3New(AtSerdesController serdesController, uint32 drpBaseAddress);
AtEyeScanController AtEyeScanControllerGtyE4New(AtSerdesController serdesController, uint32 drpBaseAddress);
AtEyeScanController AtEyeScanControllerXauiNew(AtSerdesController serdesController, uint32 drpBaseAddress);
AtEyeScanController AtEyeScanControllerGthE3New(AtSerdesController serdesController, uint32 drpBaseAddress);
AtEyeScanController AtEyeScanControllerGthE4New(AtSerdesController serdesController, uint32 drpBaseAddress);

#endif /* _ATEYESCANCONTROLLER_H_ */

#include "AtEyeScanControllerDeprecated.h"

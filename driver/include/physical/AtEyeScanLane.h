/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtEyeScanLane.h
 * 
 * Created Date: Dec 24, 2014
 *
 * Description : Eye scan lane
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATEYESCANLANE_H_
#define _ATEYESCANLANE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtPhysicalClasses.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtEyeScanLane
 * @{
 */

/**
 * @brief Pixel information
 */
typedef struct tAtEyeScanLanePixel
    {
    uint16 errorCount;  /**< Error count for current pixel/eye location */
    uint16 sampleCount; /**< Sample count for current pixel/eye location */
    uint8 prescale;     /**< Prescale for current pixel */
    int16 horzOffset;   /**< Horizontal offset for current pixel */
    int16 vertOffset;   /**< Vertical offset for current pixel */
    int16 utSign;       /**< UT sign for current pixel */
    }tAtEyeScanLanePixel;

/**
 * @brief Eye scan listener
 */
typedef struct tAtEyeScanLaneListener
    {
    void (*EyeScanWillStart)(AtEyeScanLane lane, void* userData); /**< Called before eye scan start */

    void (*EyeScanWillScanRow)(AtEyeScanLane lane, int32 verticalOffset, void* userData); /**<
        Called when a row at vertical offset will be scanned */
    void (*EyeScanDidScanRow)(AtEyeScanLane lane, int32 verticalOffset, void* userData); /**<
        Called when a row at vertical offset has just been finished scanning */
    void (*EyeScanDidScanPixel)(AtEyeScanLane lane,
                                int32 verticalOffset, int32 horizontalOffset,
                                uint32 numErrors, uint32 maxNumErrors, float ber, void* userData); /**<
                                Called when finish scanning a pixel */

    void (*EyeScanDidFinish)(AtEyeScanLane lane, void* userData); /**< Called when eye scan finish */
    void (*WillDelete)(AtEyeScanLane lane, void* userData); /**< Called before AtEyeScanLane is deleted */
    }tAtEyeScanLaneListener;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Read-only attributes */
uint8 AtEyeScanLaneIdGet(AtEyeScanLane self);
AtEyeScanController AtEyeScanLaneControllerGet(AtEyeScanLane self);
uint16 AtEyeScanLaneDataWidthGet(AtEyeScanLane self);

/* Max horizontal offset */
uint16 AtEyeScanLaneMaxHorizontalOffsetGet(AtEyeScanLane self);

/* Eye scan enabling */
eAtRet AtEyeScanLaneEnable(AtEyeScanLane self, eBool enable);
eBool AtEyeScanLaneIsEnabled(AtEyeScanLane self);

/* Query current status */
const tAtEyeScanLanePixel *AtEyeScanLaneLastScanPixel(AtEyeScanLane self);
eBool AtEyeScanLaneScanFinished(AtEyeScanLane self);

/* Step sizes */
eAtRet AtEyeScanLaneHorizontalStepSizeSet(AtEyeScanLane self, uint8 horizontalStepSize);
uint8 AtEyeScanLaneHorizontalStepSizeGet(AtEyeScanLane self);
eAtRet AtEyeScanLaneVerticalStepSizeSet(AtEyeScanLane self, uint8 verticalStepSize);
uint8 AtEyeScanLaneVerticalStepSizeGet(AtEyeScanLane self);

/* Max prescale */
eAtRet AtEyeScanLaneMaxPrescaleSet(AtEyeScanLane self, uint8 maxPrescale);
uint8 AtEyeScanLaneMaxPrescaleGet(AtEyeScanLane self);

/* Measured info */
uint32 AtEyeScanLaneMeasuredWidth(AtEyeScanLane self);
uint32 AtEyeScanLaneMeasuredSwing(AtEyeScanLane self);
uint32 AtEyeScanLaneMeasuredSwingInMilliVolt(AtEyeScanLane self);
uint32 AtEyeScanLaneNumHorizontalTaps(AtEyeScanLane self);
uint32 AtEyeScanLaneElapsedTimeMs(AtEyeScanLane self);
float AtEyeScanLaneSpeedInGbps(AtEyeScanLane self);
int32 AtEyeScanLaneVerticalOffsetToMillivolt(AtEyeScanLane self, int32 verticalOffset);

/* Listener */
eAtRet AtEyeScanLaneListenerAdd(AtEyeScanLane self, const tAtEyeScanLaneListener *listener, void* userData);
eAtRet AtEyeScanLaneListenerRemove(AtEyeScanLane self, const tAtEyeScanLaneListener *listener, void* userData);

/* DRP access */
uint16 AtEyeScanLaneDrpRead(AtEyeScanLane self, uint16 drpAddress);
void AtEyeScanLaneDrpWrite(AtEyeScanLane self, uint16 drpAddress, uint16 value);

/* For debugging */
eAtRet AtEyeScanLaneDebug(AtEyeScanLane self);
eAtRet AtEyeScanLaneDebugEnable(AtEyeScanLane self, eBool enable);
eBool AtEyeScanLaneDebugIsEnabled(AtEyeScanLane self);

/* Concrete Lanes. Note: application do not use these APIs */
AtEyeScanLane AtEyeScanLaneFastNew(AtEyeScanController controller, uint8 laneId);
AtEyeScanLane AtEyeScanLaneXauiNew(AtEyeScanController controller, uint8 laneId);
AtEyeScanLane AtEyeScanLaneFastV2New(AtEyeScanController controller, uint8 laneId);
AtEyeScanLane AtEyeScanLaneFastGtyNew(AtEyeScanController controller, uint8 laneId);

#endif /* _ATEYESCANLANE_H_ */

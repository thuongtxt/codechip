/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : AtThermalSensor.h
 * 
 * Created Date: Dec 10, 2015
 *
 * Description : AtThermalSensor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTHERMALSENSOR_H_
#define _ATTHERMALSENSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPhysicalClasses.h"
#include "AtSensor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtThermalSensor
 * @{
 */

/** @brief Thermal Sensor alarm types */
typedef enum eAtThermalSensorAlarmType
    {
    cAtThermalSensorAlarmNone       = 0,    /**< No alarms */
    cAtThermalSensorAlarmOverheat   = cBit0 /**< The current temperature is greater than the set threshold. */
    }eAtThermalSensorAlarmType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Temperature */
int32 AtThermalSensorCurrentTemperatureGet(AtThermalSensor self);
int32 AtThermalSensorMinRecordedTemperatureGet(AtThermalSensor self);
int32 AtThermalSensorMaxRecordedTemperatureGet(AtThermalSensor self);

/* Over-heat set thresholds */
eAtRet AtThermalSensorAlarmSetThresholdSet(AtThermalSensor self, int32 celsius);
int32 AtThermalSensorAlarmSetThresholdGet(AtThermalSensor self);
int32 AtThermalSensorAlarmSetThresholdMax(AtThermalSensor self);
int32 AtThermalSensorAlarmSetThresholdMin(AtThermalSensor self);

/* Over-heat clear thresholds */
eAtRet AtThermalSensorAlarmClearThresholdSet(AtThermalSensor self, int32 celsius);
int32 AtThermalSensorAlarmClearThresholdGet(AtThermalSensor self);
int32 AtThermalSensorAlarmClearThresholdMax(AtThermalSensor self);
int32 AtThermalSensorAlarmClearThresholdMin(AtThermalSensor self);

#ifdef __cplusplus
}
#endif
#endif /* _ATTHERMALSENSOR_H_ */


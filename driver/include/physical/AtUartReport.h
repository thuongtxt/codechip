/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtUartReport.h
 * 
 * Created Date: Aug 25, 2016
 *
 * Description : UART report interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATUARTREPORT_H_
#define _ATUARTREPORT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATUARTREPORT_H_ */


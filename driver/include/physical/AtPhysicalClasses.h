/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtPhysicalClasses.h
 * 
 * Created Date: Dec 25, 2014
 *
 * Description : Eye scan classes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPHYSICALCLASSES_H_
#define _ATPHYSICALCLASSES_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtEyeScanController
 * @{
 */

/** @brief Eye-scan controller */
typedef struct tAtEyeScanController * AtEyeScanController;

/**
 * @}
 */

/**
 * @addtogroup AtEyeScanController
 * @{
 */

/** @brief Eye-scan lane */
typedef struct tAtEyeScanLane * AtEyeScanLane;

/**
 * @}
 */

/**
 * @addtogroup AtSerdesController
 * @{
 */

/** @brief SERDES controller */
typedef struct tAtSerdesController * AtSerdesController;

/**
 * @}
 */

/**
 * @addtogroup AtSerdesManager
 * @{
 */

/** @brief Abstract SERDES manager  */
typedef struct tAtSerdesManager * AtSerdesManager;

/**
 * @}
 */

/**
 * @addtogroup AtSemController
 * @{
 */

/** @brief SEM controller */
typedef struct tAtSemController * AtSemController;

/**
 * @}
 */

/**
 * @addtogroup AtThermalSensor
 * @{
 */

/** @brief Thermal Sensor class */
typedef struct tAtThermalSensor * AtThermalSensor;

/**
 * @}
 */

/**
 * @addtogroup AtPowerSupplySensor
 * @{
 */

/** @brief Power Supply Sensor class */
typedef struct tAtPowerSupplySensor * AtPowerSupplySensor;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THAMODULEDDR2_H_ */

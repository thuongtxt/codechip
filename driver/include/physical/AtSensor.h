/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : AtSensor.h
 * 
 * Created Date: Dec 15, 2015
 *
 * Description : Generic physical sensor interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSENSOR_H_
#define _ATSENSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtRet.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/**
 * @addtogroup AtSensor
 * @{
 */

/**
 * @brief AT Physical sensor class
 */
typedef struct tAtSensor * AtSensor;

/** @brief Default sensor listener interface */
typedef struct tAtSensorEventListener
    {
    void (*AlarmChangeStateNotify)(AtSensor self, uint32 changedAlarms, uint32 currentAlarms, void *userData); /**< Called when alarms have change in state */
    }tAtSensorEventListener;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice AtSensorDeviceGet(AtSensor self);
eAtRet AtSensorInit(AtSensor self);

/* Get current and history alarm. */
uint32 AtSensorAlarmGet(AtSensor self);
uint32 AtSensorAlarmHistoryGet(AtSensor self);
uint32 AtSensorAlarmHistoryClear(AtSensor self);

/* Interrupt. */
eAtRet AtSensorInterruptMaskSet(AtSensor self, uint32 alarmMask, uint32 enableMask);
uint32 AtSensorInterruptMaskGet(AtSensor self);
uint32 AtSensorSupportedAlarms(AtSensor self);

/* Event listener reported through interrupt. */
eAtRet AtSensorEventListenerAdd(AtSensor self, tAtSensorEventListener *listener, void *userData);
eAtRet AtSensorEventListenerRemove(AtSensor self, tAtSensorEventListener *listener);

#ifdef __cplusplus
}
#endif
#endif /* _ATSENSOR_H_ */


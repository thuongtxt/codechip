/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtSerdesController.h
 * 
 * Created Date: Aug 16, 2013
 *
 * Description : SERDES controller. To configure physical parameters of STM
 *               port and GE port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSERDESCONTROLLER_H_
#define _ATSERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtChannel.h"
#include "AtMdio.h"
#include "AtDrp.h"
#include "AtPhysicalClasses.h"
#include "AtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtSerdesController
 * @{
 */

/** @brief SERDES timing mode */
typedef enum eAtSerdesTimingMode
    {
    cAtSerdesTimingModeUnknown,    /**< Unknown timing mode */
    cAtSerdesTimingModeAuto,       /**< Auto */
    cAtSerdesTimingModeLockToData, /**< Lock to data */
    cAtSerdesTimingModeLockToRef   /**< Lock to ref */
    }eAtSerdesTimingMode;

/** @brief SERDES alarm types */
typedef enum eAtSerdesAlarmType
    {
    cAtSerdesAlarmTypeNone,        /**< No alarm */

    /* Physical interface. */
    cAtSerdesAlarmTypeLos        = cBit0, /**< LOS */
    cAtSerdesAlarmTypeTxFault    = cBit1, /**< TX Fault */
    cAtSerdesAlarmTypeRxFault    = cBit2, /**< RX Fault */
    cAtSerdesAlarmTypeNotAligned = cBit3, /**< RX is not aligned */
    cAtSerdesAlarmTypeNotSync    = cBit4, /**< RX is not synchronized */

    /* Link */
    cAtSerdesAlarmTypeLinkDown   = cBit5  /**< Link Down */
    }eAtSerdesAlarmType;

/** @brief physical parameters */
typedef enum eAtSerdesParam
    {
    cAtSerdesParamVod                      = 0x0,  /**< VOD */
    cAtSerdesParamPreEmphasisPreTap        = 0x1,  /**< Pre-emphasis pre-tap */
    cAtSerdesParamPreEmphasisFirstPostTap  = 0x2,  /**< Pre-emphasis first-post-tap */
    cAtSerdesParamPreEmphasisSecondPostTap = 0x3,  /**< Pre-emphasis second-post-tap */
    cAtSerdesParamRxEqualizationDcGain     = 0x10, /**< RX equalization DC Gain */
    cAtSerdesParamRxEqualizationControl    = 0x11, /**< RX equalization control */
    cAtSerdesParamTxPreCursor,                     /**< TX Precursor control (BASE-R only) */
    cAtSerdesParamTxPostCursor,                    /**< TX Postcursor control (BASE-R only) */
    cAtSerdesParamTxDiffCtrl,                      /**< TX Differential Drive control (BASE-R only) */

    /* XILINX params */
    cAtSerdesParamRXLPMHFOVRDEN,   /**< RXLPMHFOVRDEN */
    cAtSerdesParamRXLPMLFKLOVRDEN, /**< RXLPMLFKLOVRDEN */
    cAtSerdesParamRXOSOVRDEN,      /**< RXOSOVRDEN */
    cAtSerdesParamRXLPMOSHOLD,     /**< RXLPMOSHOLD */
    cAtSerdesParamRXLPMOSOVRDEN,   /**< RXLPMOSOVRDEN */
    cAtSerdesParamRXLPM_OS_CFG1,   /**< RXLPM_OS_CFG1 */
    cAtSerdesParamRXLPMGCHOLD,     /**< RXLPMGCHOLD */
    cAtSerdesParamRXLPMGCOVRDEN,   /**< RXLPMGCOVRDEN */
    cAtSerdesParamRXLPM_GC_CFG     /**< RXLPM_GC_CFG */
    }eAtSerdesParam;

/** @brief Link status */
typedef enum eAtSerdesLinkStatus
    {
    cAtSerdesLinkStatusDown,        /**< Link is down */
    cAtSerdesLinkStatusUp,          /**< Link is up */
    cAtSerdesLinkStatusUnknown,     /**< Unknown */
    cAtSerdesLinkStatusNotSupported /**< Not support link status */
    }eAtSerdesLinkStatus;

/** @brief Equalizer mode */
typedef enum eAtSerdesEqualizerMode
    {
    cAtSerdesEqualizerModeUnknown, /**< Unknown */
    cAtSerdesEqualizerModeDfe,     /**< DFE */
    cAtSerdesEqualizerModeLpm      /**< LPM */
    }eAtSerdesEqualizerMode;

/** @brief SERDES mode */
typedef enum eAtSerdesMode
    {
    cAtSerdesModeUnknown,  /**< Unknown */

    /* OCN */
    cAtSerdesModeStm0,     /**< OC-1/STM-0 */
    cAtSerdesModeStm1,     /**< OC-3/STM-1 */
    cAtSerdesModeStm4,     /**< OC-12/STM-4 */
    cAtSerdesModeStm16,    /**< OC-48/STM-16 */
    cAtSerdesModeStm64,    /**< OC-192/STM-64 */

    /* ETH */
    cAtSerdesModeEth10M,   /**< 10M ETH */
    cAtSerdesModeEth100M,  /**< 100M ETH */
    cAtSerdesModeEth2500M, /**< 2.5G ETH */
    cAtSerdesModeEth1G,    /**< 1G ETH */
    cAtSerdesModeEth10G,   /**< 10G ETH */
    cAtSerdesModeEth40G,   /**< 40G ETH */

    /* Generic case */
    cAtSerdesModeOcn,      /**< Generic OCN mode */
    cAtSerdesModeGe        /**< Generic GE mode */
    }eAtSerdesMode;

/** @brief Serdes prbs engine type */
typedef enum eAtSerdesPrbsEngineType
    {
    cAtSerdesPrbsEngineTypeFramed, /**< Framed PRBS */
    cAtSerdesPrbsEngineTypeRaw     /**< Raw PRBS */
    }eAtSerdesPrbsEngineType;

/**
 * @brief SERDES Listener
 */
typedef struct tAtSerdesControllerListener
    {
    void (*AlarmNotify)(AtSerdesController self, void *userData, uint32 changedAlarms, uint32 currentStatus);/**< Failure Notify */
    void (*WillDelete)(AtSerdesController self, void *userData);/**< Notify before delete Serdes Controller */
    }tAtSerdesControllerListener;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager AtSerdesControllerManagerGet(AtSerdesController self);
AtDevice AtSerdesControllerDeviceGet(AtSerdesController self);
eBool AtSerdesControllerIsControllable(AtSerdesController self);

eAtRet AtSerdesControllerInit(AtSerdesController self);
eAtRet AtSerdesControllerReset(AtSerdesController self);
eAtRet AtSerdesControllerRxReset(AtSerdesController self);
eAtRet AtSerdesControllerTxReset(AtSerdesController self);
const char *AtSerdesControllerIdString(AtSerdesController self);

AtChannel AtSerdesControllerPhysicalPortGet(AtSerdesController self);
uint32 AtSerdesControllerIdGet(AtSerdesController self);
eBool AtSerdesControllerPllIsLocked(AtSerdesController self);
eAtSerdesLinkStatus AtSerdesControllerLinkStatus(AtSerdesController self);

/* SERDES mode */
eAtRet AtSerdesControllerModeSet(AtSerdesController self, eAtSerdesMode mode);
eAtSerdesMode AtSerdesControllerModeGet(AtSerdesController self);
eBool AtSerdesControllerModeIsSupported(AtSerdesController self, eAtSerdesMode mode);

/* Enabling */
eAtRet AtSerdesControllerEnable(AtSerdesController self, eBool enable);
eBool AtSerdesControllerIsEnabled(AtSerdesController self);

/* Timing */
eAtRet AtSerdesControllerTimingModeSet(AtSerdesController self, eAtSerdesTimingMode timingMode);
eAtSerdesTimingMode AtSerdesControllerTimingModeGet(AtSerdesController self);
eBool AtSerdesControllerCanControlTimingMode(AtSerdesController self);

/* PMA Analog Control */
eAtRet AtSerdesControllerPhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value);
uint32 AtSerdesControllerPhysicalParamGet(AtSerdesController self, eAtSerdesParam param);
eBool AtSerdesControllerPhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param);

/* Loopback */
eAtRet AtSerdesControllerLoopbackEnable(AtSerdesController self, eAtLoopbackMode, eBool enable);
eBool AtSerdesControllerLoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode);
eBool AtSerdesControllerCanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable);

/* Alarms */
uint32 AtSerdesControllerAlarmGet(AtSerdesController self);
uint32 AtSerdesControllerAlarmHistoryGet(AtSerdesController self);
uint32 AtSerdesControllerAlarmHistoryClear(AtSerdesController self);

/* Interrupt mask. */
eAtRet AtSerdesControllerInterruptMaskSet(AtSerdesController self, uint32 alarmMask, uint32 enableMask);
uint32 AtSerdesControllerInterruptMaskGet(AtSerdesController self);
uint32 AtSerdesControllerSupportedInterruptMask(AtSerdesController self);

/* Listeners */
eAtRet AtSerdesControllerListenerAdd(AtSerdesController self, void *userData, const tAtSerdesControllerListener *callback);
eAtRet AtSerdesControllerListenerRemove(AtSerdesController self, void *userData, const tAtSerdesControllerListener *callback);

/* PRBS */
AtPrbsEngine AtSerdesControllerPrbsEngine(AtSerdesController self);
eAtRet AtSerdesControllerPrbsEngineTypeSet(AtSerdesController self, eAtSerdesPrbsEngineType type);
eAtSerdesPrbsEngineType AtSerdesControllerPrbsEngineTypeGet(AtSerdesController self);

/* MDIO */
AtMdio AtSerdesControllerMdio(AtSerdesController self);

/* DRP */
AtDrp AtSerdesControllerDrp(AtSerdesController self);

/* Equalizer */
eAtRet AtSerdesControllerEqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode);
eAtSerdesEqualizerMode AtSerdesControllerEqualizerModeGet(AtSerdesController self);
eBool AtSerdesControllerEqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode);
eBool AtSerdesControllerEqualizerCanControl(AtSerdesController self);

/* Control power */
eAtRet AtSerdesControllerPowerDown(AtSerdesController self, eBool powerDown);
eBool AtSerdesControllerPowerIsDown(AtSerdesController self);
eBool AtSerdesControllerPowerCanControl(AtSerdesController self);

/* Eye scan controller */
AtEyeScanController AtSerdesControllerEyeScanControllerGet(AtSerdesController self);

/* SERDES may have lanes */
AtSerdesController AtSerdesControllerLaneGet(AtSerdesController self, uint32 laneId);
uint32 AtSerdesControllerNumLanes(AtSerdesController self);
AtSerdesController AtSerdesControllerParentGet(AtSerdesController self);

/* PPM to auto reset */
eBool AtSerdesControllerHasAutoResetPpmThreshold(AtSerdesController self);
uint32 AtSerdesControllerAutoResetPpmThresholdGet(AtSerdesController self);
eAtRet AtSerdesControllerAutoResetPpmThresholdSet(AtSerdesController self, uint32 ppmThreshold);
uint32 AtSerdesControllerAutoResetPpmThresholdMax(AtSerdesController self);

/* Debug */
void AtSerdesControllerDebug(AtSerdesController self);

/* Utils */
eBool AtSerdesControllerModeIsValid(eAtSerdesMode mode);
uint32 AtSerdesControllerRead(AtSerdesController self, uint32 address, eAtModule moduleId);
void AtSerdesControllerWrite(AtSerdesController self, uint32 address, uint32 value, eAtModule moduleId);
eAtRet AtSerdesControllerLoopbackSet(AtSerdesController self, eAtLoopbackMode loopback);
eBool AtSerdesControllerLoopbackIsSupported(AtSerdesController self, eAtLoopbackMode loopback);
eAtLoopbackMode AtSerdesControllerLoopbackGet(AtSerdesController self);
const char *AtSerdesControllerDescription(AtSerdesController self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSERDESCONTROLLER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtSpiController.h
 * 
 * Created Date: Aug 11, 2014
 *
 * Description : SPI controller.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSPICONTROLLER_H_
#define _ATSPICONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtSpiController
 * @{
 */

/** @brief SPI controller */
typedef struct tAtSpiController * AtSpiController;

/** @brief SPI mode */
typedef enum eAtSpiControllerSpiMode
    {
    cAtSpiControllerSpiModeUnknown,          /**< Unknown SPI mode */
    cAtSpiControllerSpiModeBurstInterleaved, /**< Burst Interleaved mode */
    cAtSpiControllerSpiModeFullFrame         /**< Full frame mode */
    }eAtSpiControllerSpiMode;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtSpiControllerVersion(AtSpiController self);
eAtRet AtSpiControllerModeSet(AtSpiController self, eAtSpiControllerSpiMode mode);
eAtSpiControllerSpiMode AtSpiControllerModeGet(AtSpiController self);
uint32 AtSpiControllerNumPortsGet(AtSpiController self);
uint32 AtSpiControllerIdGet(AtSpiController self);

/* Calendar port ID */
eAtRet AtSpiControllerTxCalendarPortIdSet(AtSpiController self, uint32 calendarIndex, uint32 portId);
uint32 AtSpiControllerTxCalendarPortIdGet(AtSpiController self, uint32 calendarIndex);
eAtRet AtSpiControllerRxCalendarPortIdSet(AtSpiController self, uint32 calendarIndex, uint32 portId);
uint32 AtSpiControllerRxCalendarPortIdGet(AtSpiController self, uint32 calendarIndex);

/* Calendar max sequence length */
eAtRet AtSpiControllerTxCalendarMaxSequenceLengthSet(AtSpiController self, uint32 length);
uint32 AtSpiControllerTxCalendarMaxSequenceLengthGet(AtSpiController self);
eAtRet AtSpiControllerRxCalendarMaxSequenceLengthSet(AtSpiController self, uint32 length);
uint32 AtSpiControllerRxCalendarMaxSequenceLengthGet(AtSpiController self);

/* Calendar sequence length */
eAtRet AtSpiControllerTxCalendarSequenceLengthSet(AtSpiController self, uint32 length);
uint32 AtSpiControllerTxCalendarSequenceLengthGet(AtSpiController self);
eAtRet AtSpiControllerRxCalendarSequenceLengthSet(AtSpiController self, uint32 length);
uint32 AtSpiControllerRxCalendarSequenceLengthGet(AtSpiController self);

/* Calendar sequence repeat times */
eAtRet AtSpiControllerTxCalendarSequenceRepeatTimesSet(AtSpiController self, uint32 numTimes);
uint32 AtSpiControllerTxCalendarSequenceRepeatTimesGet(AtSpiController self);
eAtRet AtSpiControllerRxCalendarSequenceRepeatTimesSet(AtSpiController self, uint32 numTimes);
uint32 AtSpiControllerRxCalendarSequenceRepeatTimesGet(AtSpiController self);

/* Max burst */
eAtRet AtSpiControllerTxFifoStatusMaxBurst1Set(AtSpiController self, uint32 portId, uint32 numBlocks);
uint32 AtSpiControllerTxFifoStatusMaxBurst1Get(AtSpiController self, uint32 portId);
eAtRet AtSpiControllerTxFifoStatusMaxBurst2Set(AtSpiController self, uint32 portId, uint32 numBlocks);
uint32 AtSpiControllerTxFifoStatusMaxBurst2Get(AtSpiController self, uint32 portId);
eAtRet AtSpiControllerRxFifoStatusMaxBurst1Set(AtSpiController self, uint32 portId, uint32 numBlocks);
uint32 AtSpiControllerRxFifoStatusMaxBurst1Get(AtSpiController self, uint32 portId);
eAtRet AtSpiControllerRxFifoStatusMaxBurst2Set(AtSpiController self, uint32 portId, uint32 numBlocks);
uint32 AtSpiControllerRxFifoStatusMaxBurst2Get(AtSpiController self, uint32 portId);

/* FIFO status training sequence interval */
eAtRet AtSpiControllerTxFifoStatusTrainingSequenceIntervalSet(AtSpiController self, uint32 numCycles);
uint32 AtSpiControllerTxFifoStatusTrainingSequenceIntervalGet(AtSpiController self);
eAtRet AtSpiControllerRxFifoStatusTrainingSequenceIntervalSet(AtSpiController self, uint32 numCycles);
uint32 AtSpiControllerRxFifoStatusTrainingSequenceIntervalGet(AtSpiController self);

/* Data path training sequence repeat times */
eAtRet AtSpiControllerTxDataPathTrainingSequenceRepeatTimesSet(AtSpiController self, uint32 numTimes);
uint32 AtSpiControllerTxDataPathTrainingSequenceRepeatTimesGet(AtSpiController self);
eAtRet AtSpiControllerRxDataPathTrainingSequenceRepeatTimesSet(AtSpiController self, uint32 numTimes);
uint32 AtSpiControllerRxDataPathTrainingSequenceRepeatTimesGet(AtSpiController self);

/* Data path training sequence interval */
eAtRet AtSpiControllerTxDataPathTrainingSequenceIntervalSet(AtSpiController self, uint32 numCycles);
uint32 AtSpiControllerTxDataPathTrainingSequenceIntervalGet(AtSpiController self);
eAtRet AtSpiControllerRxDataPathTrainingSequenceIntervalSet(AtSpiController self, uint32 numCycles);
uint32 AtSpiControllerRxDataPathTrainingSequenceIntervalGet(AtSpiController self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSPICONTROLLER_H_ */


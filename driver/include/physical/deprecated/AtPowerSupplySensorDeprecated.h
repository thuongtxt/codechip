/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : AtPowerSupplySensorDeprecated.h
 * 
 * Created Date: May 9, 2017
 *
 * Description : Power supply deprecated APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPOWERSUPPLYSENSORDEPRECATED_H_
#define _ATPOWERSUPPLYSENSORDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define AtPowerSupplySensorAlarmSetThresholdSet   AtPowerSupplySensorAlarmUpperThresholdSet
#define AtPowerSupplySensorAlarmSetThresholdGet   AtPowerSupplySensorAlarmUpperThresholdGet
#define AtPowerSupplySensorAlarmClearThresholdSet AtPowerSupplySensorAlarmLowerThresholdSet
#define AtPowerSupplySensorAlarmClearThresholdGet AtPowerSupplySensorAlarmLowerThresholdGet

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATPOWERSUPPLYSENSORDEPRECATED_H_ */


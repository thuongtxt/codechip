/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : AtSemControllerDeprecated.h
 * 
 * Created Date: Mar 16, 2016
 *
 * Description : Deprecated SEM controller.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef ATSEMCONTROLLERDEPRECATED_H_
#define ATSEMCONTROLLERDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* @brief Soft Error Mitigation (SEM) Status types */
typedef enum eAtSemControllerStatus
    {
    cAtSemControllerStatusNotSupport     = 0,      /* SEM IP has not been supported */
    cAtSemControllerStatusActive         = cBit0,  /* FSM of SEM IP is active */
    cAtSemControllerStatusIdle           = cBit1,  /* FMS of SEM IP currently in IDLE */
    cAtSemControllerStatusInitialization = cBit2,  /* FSM of SEM IP is INITIALIZATION */
    cAtSemControllerStatusObservation    = cBit3,  /* FSM of SEM IP is OSERVATION */
    cAtSemControllerStatusUncorrectable  = cBit4   /* Error uncorrectable */
    }eAtSemControllerStatus;

/* @brief Soft Error Mitigation (SEM) Alarm types, deprecated by eAtSemControllerAlarmType */
typedef enum eAtSemControllerAlarmType eAtSemControllerAlarm;

/*--------------------------- Forward declarations ---------------------------*/

/* Corrected error counter, deprecated by AtSemControllerCounterGet() */
uint8 AtSemControllerCorrectedErrorCounterGet(AtSemController self);

/* Current status, deprecated by AtSemControllerAlarmGet() */
uint8 AtSemControllerStatusGet(AtSemController self);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* ATSEMCONTROLLERDEPRECATED_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : AtEyeScanControllerDeprecated.h
 * 
 * Created Date: Jun 1, 2016
 *
 * Description : Deprecated Eye Scan controller interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATEYESCANCONTROLLERDEPRECATED_H_
#define _ATEYESCANCONTROLLERDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtEyeScanEqualizerMode
    {
    cAtEyeScanEqualizerModeDfe,    /* DFE */
    cAtEyeScanEqualizerModeLpm,    /* LPM */
    cAtEyeScanEqualizerModeUnknown /* Unknown */
    }eAtEyeScanEqualizerMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Physical port */
AtChannel AtEyeScanControllerPhysicalPort(AtEyeScanController self);

/* Equalizer mode */
eAtEyeScanEqualizerMode AtEyeScanControllerEqualizerModeGet(AtEyeScanController self);
eAtRet AtEyeScanControllerEqualizerModeSet(AtEyeScanController self, eAtEyeScanEqualizerMode mode);

#ifdef __cplusplus
}
#endif
#endif /* _ATEYESCANCONTROLLERDEPRECATED_H_ */


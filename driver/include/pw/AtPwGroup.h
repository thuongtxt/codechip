/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtPwGroup.h
 * 
 * Created Date: Mar 24, 2015
 *
 * Description : PW Group
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWGROUP_H_
#define _ATPWGROUP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPwGroup
 * @{
 */

/** @brief Group label set */
typedef enum eAtPwGroupLabelSet
    {
    cAtPwGroupLabelSetUnknown, /**< Invalid set */
    cAtPwGroupLabelSetPrimary, /**< Primary label set */
    cAtPwGroupLabelSetBackup   /**< Backup label set */
    }eAtPwGroupLabelSet;

/** @brief Group type */
typedef enum eAtPwGroupType
    {
    cAtPwGroupTypeUnknown, /**< Invalid set */
    cAtPwGroupTypeAps,     /**< APS group type */
    cAtPwGroupTypeHs       /**< Hot standby group type */
    }eAtPwGroupType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Read-only attribute */
uint32 AtPwGroupIdGet(AtPwGroup self);
AtModulePw AtPwGroupModuleGet(AtPwGroup self);
eAtPwGroupType AtPwGroupTypeGet(AtPwGroup self);
const char *AtPwGroupIdString(AtPwGroup self);
const char *AtPwGroupTypeString(AtPwGroup self);

/* Enabling */
eAtRet AtPwGroupEnable(AtPwGroup self, eBool enable);
eBool AtPwGroupIsEnabled(AtPwGroup self);

/* PW management */
eAtRet AtPwGroupPwAdd(AtPwGroup self, AtPw pw);
eAtRet AtPwGroupPwRemove(AtPwGroup self, AtPw pw);
eBool AtPwGroupContainsPw(AtPwGroup self, AtPw pw);
uint32 AtPwGroupNumPws(AtPwGroup self);
AtPw AtPwGroupPwAtIndex(AtPwGroup self, uint32 pwIndex);

/* Label switching */
eAtRet AtPwGroupTxLabelSetSelect(AtPwGroup self, eAtPwGroupLabelSet labelSet);
eAtPwGroupLabelSet AtPwGroupTxSelectedLabelGet(AtPwGroup self);
eAtRet AtPwGroupRxLabelSetSelect(AtPwGroup self, eAtPwGroupLabelSet labelSet);
eAtPwGroupLabelSet AtPwGroupRxSelectedLabelGet(AtPwGroup self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWGROUP_H_ */


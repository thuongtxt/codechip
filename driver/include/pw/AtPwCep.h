/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtPwCep.h
 * 
 * Created Date: Mar 15, 2013
 *
 * Description : PW
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWCEP_H_
#define _ATPWCEP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePw.h"
#include "AtPw.h" /* Super class */
#include "AtSdhVc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Equip/Unequip a VC in CEP fraction */
eAtModulePwRet AtPwCepEquip(AtPwCep self, AtChannel subChannel, eBool equip);
eBool AtPwCepIsEquipped(AtPwCep self, AtChannel subChannel);
AtIterator AtPwCepEquippedChannelsIteratorCreate(AtPwCep self);
uint8 AtPwCepNumEquippedChannelsGet(AtPwCep self);

/* Equipped Bit Mask - EBM */
eAtModulePwRet AtPwCepCwEbmEnable(AtPwCep self, eBool enable);
eBool AtPwCepCwEbmIsEnabled(AtPwCep self);

/* Explicit Pointer Adjustment Relay - EPAR */
eAtModulePwRet AtPwCepEparEnable(AtPwCep self, eBool enable);
eBool AtPwCepEparIsEnabled(AtPwCep self);

/* Auto N bit, P bit */
eAtModulePwRet AtPwCepCwAutoNBitEnable(AtPwCep self, eBool enable);
eBool AtPwCepCwAutoNBitIsEnabled(AtPwCep self);
eAtModulePwRet AtPwCepCwAutoPBitEnable(AtPwCep self, eBool enable);
eBool AtPwCepCwAutoPBitIsEnabled(AtPwCep self);

eAtPwCepMode AtPwCepModeGet(AtPwCep self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWCEP_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtPwDeprecated.h
 * 
 * Created Date: Oct 16, 2015
 *
 * Description : PW deprecated interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_INCLUDE_PW_DEPRECATED_ATPWDEPRECATED_H_
#define _DRIVER_INCLUDE_PW_DEPRECATED_ATPWDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Priority */
eAtModulePwRet AtPwPrioritySet(AtPw self, uint8 priority);
uint8 AtPwPriorityGet(AtPw self);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_INCLUDE_PW_DEPRECATED_ATPWDEPRECATED_H_ */


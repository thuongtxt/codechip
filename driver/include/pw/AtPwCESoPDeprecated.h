/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Pseudowire
 * 
 * File        : AtPwCESoPDeprecated.h
 * 
 * Created Date: Jun 1, 2017
 *
 * Description : CESoPSN Pseudowire class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWCESOPDEPRECATED_H_
#define _ATPWCESOPDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModulePwRet AtPwCESoPCwAutoMBitEnable(AtPwCESoP self, eBool enable);
eBool AtPwCESoPCwAutoMBitIsEnabled(AtPwCESoP self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWCESOPDEPRECATED_H_ */


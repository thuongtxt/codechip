/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Pseudowire
 * 
 * File        : AtPwCESoP.h
 * 
 * Created Date: Sep 18, 2012
 *
 * Description : CESoPSN Pseudowire class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWCESOP_H_
#define _ATPWCESOP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePw.h"
#include "AtPw.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPwCESoP
 * @{
 */

/**
 * @brief CESoP with CAS mode
 */
typedef enum eAtPwCESoPCasMode
    {
    cAtPwCESoPCasModeUnknown,   /**< Unknown mode */
    cAtPwCESoPCasModeE1,        /**< CESoP with E1 CAS mode */
    cAtPwCESoPCasModeDs1        /**< CESoP with DS1 CAS mode */
    }eAtPwCESoPCasMode;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModulePwRet AtPwCESoPCwAutoTxMBitEnable(AtPwCESoP self, eBool enable);
eBool AtPwCESoPCwAutoTxMBitIsEnabled(AtPwCESoP self);

eAtModulePwRet AtPwCESoPCwAutoRxMBitEnable(AtPwCESoP self, eBool enable);
eBool AtPwCESoPCwAutoRxMBitIsEnabled(AtPwCESoP self);
eBool AtPwCESoPCwAutoRxMBitIsConfigurable(AtPwCESoP self);

eAtPwCESoPMode AtPwCESoPModeGet(AtPwCESoP self);

eAtModulePwRet AtPwCESoPCasIdleCode1Set(AtPwCESoP self, uint8 abcd1);
uint8 AtPwCESoPCasIdleCode1Get(AtPwCESoP self);
eAtModulePwRet AtPwCESoPCasIdleCode2Set(AtPwCESoP self, uint8 abcd2);
uint8 AtPwCESoPCasIdleCode2Get(AtPwCESoP self);
eAtModulePwRet AtPwCESoPCasAutoIdleEnable(AtPwCESoP self, eBool enable);
eBool AtPwCESoPCasAutoIdleIsEnabled(AtPwCESoP self);

eAtRet AtPwCESoPRxCasModeSet(AtPwCESoP self, eAtPwCESoPCasMode mode);
eAtPwCESoPCasMode AtPwCESoPRxCasModeGet(AtPwCESoP self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWCESOP_H_ */

#include "AtPwCESoPDeprecated.h"

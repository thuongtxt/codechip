/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Pseudowire
 * 
 * File        : AtPwPsn.h
 * 
 * Created Date: Sep 18, 2012
 *
 * Description : Packet Switch Network layer of Pseudowire
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWPSN_H_
#define _ATPWPSN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePw.h"
#include "AtEthVlanTag.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cAtPwPsnUdpUnusedPort 0x85e

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPwPsn
 * @{
 */

/** @brief PSN type */
typedef enum eAtPwPsnType
    {
    cAtPwPsnTypeUnknown, /**< Unknown */
    cAtPwPsnTypeMpls,    /**< MPLS */
    cAtPwPsnTypeMef,     /**< MEF */
    cAtPwPsnTypeUdp,     /**< UDP */
    cAtPwPsnTypeIPv4,    /**< IPv4 */
    cAtPwPsnTypeIPv6     /**< IPv6 */
    }eAtPwPsnType;

/** @brief Pseudowire PSN abstract class */
typedef struct tAtPwPsn * AtPwPsn;

/** @} */

/**
 * @addtogroup AtPwIpPsn
 * @{
 */
/** @brief Pseudowire IP PSN abstract class */
typedef struct tAtPwIpPsn * AtPwIpPsn;

/** @brief IPv4 PSN */
typedef struct tAtPwIpV4Psn * AtPwIpV4Psn;

/** @brief IPv6 PSN */
typedef struct tAtPwIpV6Psn * AtPwIpV6Psn;

/** @} */

/**
 * @addtogroup AtPwMplsPsn
 * @{
 */
/** @brief MPLS PSN */
typedef struct tAtPwMplsPsn * AtPwMplsPsn;

/*
+-------+-------+-------+-------+-------+-------+-------+-------+
|                  Label (20 bits)                              |
+                                                               +
|                                                               |
+                               +-------+-------+-------+-------+
|                               |         EXP           |   S   |
+-------+-------+-------+-------+-------+-------+-------+-------+
|                             TTL                               |
+-------+-------+-------+-------+-------+-------+-------+-------+
*/
/** @brief MPLS label */
typedef struct tAtPwMplsLabel
    {
    uint32 label;       /**< Label */
    uint8 experimental; /**< Experimental */
    uint8 timeToLive;   /**< Time to live */
    }tAtPwMplsLabel;

/** @} */

/**
 * @addtogroup AtPwMefPsn
 * @{
 */
/** @brief MEF */
typedef struct tAtPwMefPsn * AtPwMefPsn;

/** @} */

/**
 * @addtogroup AtPwUdpPsn
 * @{
 */
/** @brief UDP PSN */
typedef struct tAtPwUdpPsn * AtPwUdpPsn;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* PSN generic APIs */
eAtModulePwRet AtPwPsnLowerPsnSet(AtPwPsn self, AtPwPsn lowerPsn);
AtPwPsn AtPwPsnLowerPsnGet(AtPwPsn self);
eAtRet AtPwPsnHigherPsnSet(AtPwPsn self, AtPwPsn higherPsn);
AtPwPsn AtPwPsnHigherPsnGet(AtPwPsn self);
eAtPwPsnType AtPwPsnTypeGet(AtPwPsn self);
const char * AtPwPsnTypeString(AtPwPsn self);
eBool AtPwPsnIsValid(AtPwPsn self);
AtPwPsn AtPwPsnCopy(AtPwPsn self, AtPwPsn replicaPsn);
eBool AtPwPsnIsIdentical(AtPwPsn self, AtPwPsn another);
uint32 AtPwPsnLengthInBytes(AtPwPsn self);
uint32 AtPwPsnExpectedLabelGet(AtPwPsn self);

/* MPLS PSN APIs */
AtPwMplsPsn AtPwMplsPsnNew(void);
eAtModulePwRet AtPwMplsPsnInnerLabelSet(AtPwMplsPsn self, const tAtPwMplsLabel *label);
eAtModulePwRet AtPwMplsPsnInnerLabelGet(AtPwMplsPsn self, tAtPwMplsLabel *label);
eAtModulePwRet AtPwMplsPsnOuterLabelAdd(AtPwMplsPsn self, const tAtPwMplsLabel *label);
eAtModulePwRet AtPwMplsPsnOuterLabelRemove(AtPwMplsPsn self, const tAtPwMplsLabel *label);
uint8 AtPwMplsPsnNumberOfOuterLabelsGet(AtPwMplsPsn self);
uint8 AtPwMplsPsnOuterLabelsGet(AtPwMplsPsn self, tAtPwMplsLabel *label);
tAtPwMplsLabel * AtPwMplsPsnOuterLabelGetByIndex(AtPwMplsPsn self, uint8 labelIndex, tAtPwMplsLabel *labels);
AtIterator AtPwMplsPsnOuterLabelIteratorCreate(AtPwMplsPsn self);
uint8 AtPwMplsPsnMaxNumOuterLabels(AtPwMplsPsn self);
tAtPwMplsLabel *AtPwMplsLabelMake(uint32 label, uint8 experimetal, uint8 timeToLive, tAtPwMplsLabel *mplsLabel);
eAtModulePwRet AtPwMplsPsnExpectedLabelSet(AtPwMplsPsn self, uint32 label);
uint32 AtPwMplsPsnExpectedLabelGet(AtPwMplsPsn self);

/* MEF PSN APIs */
AtPwMefPsn AtPwMefPsnNew(void);
eAtModulePwRet AtPwMefPsnTxEcIdSet(AtPwMefPsn self, uint32 ecId);
uint32 AtPwMefPsnTxEcIdGet(AtPwMefPsn self);
eAtModulePwRet AtPwMefPsnExpectedEcIdSet(AtPwMefPsn self, uint32 ecId);
uint32 AtPwMefPsnExpectedEcIdGet(AtPwMefPsn self);
eAtModulePwRet AtPwMefPsnDestMacSet(AtPwMefPsn self, uint8 *dmac);
eAtModulePwRet AtPwMefPsnDestMacGet(AtPwMefPsn self, uint8 *dmac);
eAtModulePwRet AtPwMefPsnSourceMacSet(AtPwMefPsn self, uint8 *smac);
eAtModulePwRet AtPwMefPsnSourceMacGet(AtPwMefPsn self, uint8 *smac);
eAtModulePwRet AtPwMefPsnVlanTagAdd(AtPwMefPsn self, uint16 vlanType, const tAtEthVlanTag *tag);
eAtModulePwRet AtPwMefPsnVlanTagRemove(AtPwMefPsn self, uint16 vlanType, const tAtEthVlanTag *tag);
uint8 AtPwMefPsnNumVlanTags(AtPwMefPsn self);
uint8 AtPwMefPsnMaxNumVlanTags(AtPwMefPsn self);
eAtModulePwRet AtPwMefPsnVlanTagAtIndex(AtPwMefPsn self, uint8 tagIndex, uint16 *vlanType, tAtEthVlanTag *tag);
eAtModulePwRet AtPwMefPsnExpectedMacSet(AtPwMefPsn self, uint8 *expectedMac);
eAtModulePwRet AtPwMefPsnExpectedMacGet(AtPwMefPsn self, uint8 *expectedMac);

/* UDP PSN APIs */
AtPwUdpPsn AtPwUdpPsnNew(void);
eAtModulePwRet AtPwUdpPsnSourcePortSet(AtPwUdpPsn self, uint16 sourcePort);
uint16 AtPwUdpPsnSourcePortGet(AtPwUdpPsn self);
eAtModulePwRet AtPwUdpPsnDestPortSet(AtPwUdpPsn self, uint16 destPort);
uint16 AtPwUdpPsnDestPortGet(AtPwUdpPsn self);
eAtModulePwRet AtPwUdpPsnExpectedPortSet(AtPwUdpPsn self, uint16 port);
uint16 AtPwUdpPsnExpectedPortGet(AtPwUdpPsn self);
uint32 AtPwUdpPsnCheckSumCalculate(AtPwUdpPsn self);

/* IP PSN APIs */
eAtModulePwRet AtPwIpPsnSourceAddressSet(AtPwIpPsn self, const uint8 *sourceAddress);
eAtModulePwRet AtPwIpPsnSourceAddressGet(AtPwIpPsn self, uint8 *sourceAddress);
eAtModulePwRet AtPwIpPsnDestAddressSet(AtPwIpPsn self, const uint8 *destAddress);
eAtModulePwRet AtPwIpPsnDestAddressGet(AtPwIpPsn self, uint8 *destAddress);

/* IPv4 PSN APIs */
AtPwIpV4Psn AtPwIpV4PsnNew(void);
eAtModulePwRet AtPwIpV4PsnTypeOfServiceSet(AtPwIpV4Psn self, uint8 typeOfService);
uint8 AtPwIpV4PsnTypeOfServiceGet(AtPwIpV4Psn self);
eAtModulePwRet AtPwIpV4PsnTimeToLiveSet(AtPwIpV4Psn self, uint8 timeToLive);
uint8 AtPwIpV4PsnTimeToLiveGet(AtPwIpV4Psn self);
eAtModulePwRet AtPwIpV4PsnFlagsSet(AtPwIpV4Psn self, uint8 flags);
uint8 AtPwIpV4PsnFlagsGet(AtPwIpV4Psn self);
uint32 AtPwIpV4PsnCheckSumCalculate(AtPwIpV4Psn self);

/* IPv6 PSN APIs */
AtPwIpV6Psn AtPwIpV6PsnNew(void);
eAtModulePwRet AtPwIpV6PsnTrafficClassSet(AtPwIpV6Psn self, uint8 trafficClass);
uint8 AtPwIpV6PsnTrafficClassGet(AtPwIpV6Psn self);
eAtModulePwRet AtPwIpV6PsnFlowLabelSet(AtPwIpV6Psn self, uint32 flowLabel);
uint32 AtPwIpV6PsnFlowLabelGet(AtPwIpV6Psn self);
eAtModulePwRet AtPwIpV6PsnHopLimitSet(AtPwIpV6Psn self, uint8 hopLimit);
uint8 AtPwIpV6PsnHopLimitGet(AtPwIpV6Psn self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWPSN_H_ */


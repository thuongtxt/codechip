/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Pseudowire
 * 
 * File        : AtPwToh.h
 * 
 * Created Date: Mar 3, 2014
 *
 * Description : TOH PW interfaces
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWTOH_H_
#define _ATPWTOH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModulePwRet AtPwTohByteEnable(AtPwToh self, uint8 sts1, uint32 tohByteBitmap, eBool enable);
eBool AtPwTohByteIsEnabled(AtPwToh self, uint8 sts1, eAtSdhLineOverheadByte ohByte);

uint16 AtPwTohNumEnabledBytesGet(AtPwToh self);
uint16 AtPwTohAllEnabledBytesGet(AtPwToh self, uint32 * tohByteBitmap, uint8 numBitmap);
uint8 AtPwTohEnabledBytesByStsGet(AtPwToh self, uint8 sts1, uint32 *tohByteBitmap);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWTOH_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Pseudowire
 * 
 * File        : AtPwHdlc.h
 * 
 * Created Date: Oct 8, 2012
 *
 * Description : HDLC Pseudowire
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWHDLC_H_
#define _ATPWHDLC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePw.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPwHdlc
 * @{
 */

/** @brief HDLC Pseudowire payload types */
typedef enum eAtPwHdlcPayloadType
    {
    cAtPwHdlcPayloadTypeInvalid, /**< Invalid payload type */
    cAtPwHdlcPayloadTypeFull,    /**< Full header and payload is encapsulated */
    cAtPwHdlcPayloadTypePdu      /**< Only PDU is encapsulated */
    }eAtPwHdlcPayloadType;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModulePwRet AtPwHdlcPayloadTypeSet(AtPwHdlc self, eAtPwHdlcPayloadType payloadType);
eAtPwHdlcPayloadType AtPwHdlcPayloadTypeGet(AtPwHdlc self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWHDLC_H_ */


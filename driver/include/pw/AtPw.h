/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Pseudowire
 * 
 * File        : AtPw.h
 * 
 * Created Date: Sep 18, 2012
 *
 * Description : Pseudowire class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPW_H_
#define _ATPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePw.h"
#include "AtPwPsn.h"
#include "AtEthPort.h"
#include "AtEthVlanTag.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAtInvalidPwEthPortQueueId cBit7_0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPw
 * @{
 */

/** @brief Pseudowire types */
typedef enum eAtPwType
    {
    cAtPwTypeInvalid, /**< Invalid Pseudowire type */
    cAtPwTypeSAToP,   /**< SAToP Pseudowire */
    cAtPwTypeCESoP,   /**< CESoPSN Pseudowire */
    cAtPwTypeCEP,     /**< CEP Pseudowire */
    cAtPwTypeATM,     /**< ATM Pseudowire */
    cAtPwTypeHdlc,    /**< HDLC Pseudowire */
    cAtPwTypePpp,     /**< PPP Pseudowire */
    cAtPwTypeFr,      /**< Frame Relay Pseudowire */
    cAtPwTypeToh,     /**< Transport Overhead Pseudowire */
    cAtPwTypeMlppp,   /**< MLPPP Pseudowire */
    cAtPwTypeKbyte    /**< KByte Pseudowire */
    }eAtPwType;

/** @brief RTP timestamp mode */
typedef enum eAtPwRtpTimeStampMode
    {
    cAtPwRtpTimeStampModeInvalid,
    cAtPwRtpTimeStampModeAbsolute,
    cAtPwRtpTimeStampModeDifferential
    }eAtPwRtpTimeStampMode;

/** @brief Control word - sequence modes */
typedef enum eAtPwCwSequenceMode
    {
    cAtPwCwSequenceModeInvalid,
    cAtPwCwSequenceModeWrapZero,
    cAtPwCwSequenceModeSkipZero,
    cAtPwCwSequenceModeDisable
    }eAtPwCwSequenceMode;

/** @brief Control word - length modes */
typedef enum eAtPwCwLengthMode
    {
    cAtPwCwLengthModeInvalid,
    cAtPwCwLengthModeFullPacket, /**< Pseudowire CW length field in full packet mode */
    cAtPwCwLengthModePayload     /**< Pseudowire CW length field in payload mode */
    }eAtPwCwLengthMode;

/** @brief Packet replacement modes */
typedef enum eAtPwPktReplaceMode
    {
    cAtPwPktReplaceModeInvalid,
    cAtPwPktReplaceModePreviousGoodPacket,  /**< Lost packet is replaced with previous good packet */
    cAtPwPktReplaceModeAis,                 /**< Lost packet is replaced with AIS */
    cAtPwPktReplaceModeIdleCode             /**< Lost packet is replaced with idle code */
    }eAtPwPktReplaceMode;

/** @brief Alarm types */
typedef enum eAtPwAlarmType
    {
    cAtPwAlarmTypeNone                    = 0,
    cAtPwAlarmTypeLops                    = cBit0,   /**< Loss of packet synchronization alarm */
    cAtPwAlarmTypeJitterBufferOverrun     = cBit1,   /**< Jitter overrun alarm */
    cAtPwAlarmTypeJitterBufferUnderrun    = cBit2,   /**< Jitter underrun alarm */
    cAtPwAlarmTypeLBit                    = cBit3,   /**< L Bit alarm */
    cAtPwAlarmTypeRBit                    = cBit4,   /**< R Bit alarm */
    cAtPwAlarmTypeMBit                    = cBit5,   /**< M Bit alarm */
    cAtPwAlarmTypeNBit                    = cBit6,   /**< N Bit alarm */
    cAtPwAlarmTypePBit                    = cBit7,   /**< P Bit alarm */
    cAtPwAlarmTypeMissingPacket           = cBit8,   /**< Missing consecutive packets above pre-defined configurable threshold */
    cAtPwAlarmTypeExcessivePacketLossRate = cBit9,   /**< When the number of lost packets over a configurable amount of times */
    cAtPwAlarmTypeStrayPacket             = cBit10,  /**< Shall be detected when number of packets mismatch between the expected SSRC value in the
                                                          RTP header and the actually received SSRC field value in the incoming packets over predefined configurable threshold */
    cAtPwAlarmTypeMalformedPacket         = cBit11,  /**< Shall be detected when number of packet mismatch between the expected packet size (or PT value)
                                                          and the actual packet size (or PT value) over predefined configurable threshold */
    cAtPwAlarmTypeRemotePacketLoss        = cBit12,  /**< Shall be detected when the number of the detected packets with the R bit over predefined configurable threshold */
    cAtpwAlarmTypeMisConnection           = cBit13,  /**< Shall be detected when the percentage of stray frames persists above a defined level for a configurable
                                                          period of time  (by default, 2.5 seconds)*/
    cAtPwAlarmTypeAll                     = cBit13_0 /**< All alarms */
    }eAtPwAlarmType;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtPwType AtPwTypeGet(AtPw self);

/* Payload size */
eAtModulePwRet AtPwPayloadSizeSet(AtPw self, uint16 payloadSize);
uint16 AtPwPayloadSizeGet(AtPw self);
uint16 AtPwMinPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize);
uint16 AtPwMaxPayloadSize(AtPw self, AtChannel circuit, uint32 jitterBufferSize);

/* Reordering */
eAtModulePwRet AtPwReorderingEnable(AtPw self, eBool enable);
eBool AtPwReorderingIsEnabled(AtPw self);

/* Suppress */
eAtModulePwRet AtPwSuppressEnable(AtPw self, eBool enable);
eBool AtPwSuppressIsEnabled(AtPw self);

/* Jitter buffer size */
eAtModulePwRet AtPwJitterBufferSizeSet(AtPw self, uint32 microseconds);
uint32 AtPwJitterBufferSizeGet(AtPw self);
uint32 AtPwMinJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize);
uint32 AtPwMaxJitterBufferSize(AtPw self, AtChannel circuit, uint16 payloadSize);

/* Jitter buffer delay */
eAtModulePwRet AtPwJitterBufferDelaySet(AtPw self, uint32 microseconds);
uint32 AtPwJitterBufferDelayGet(AtPw self);
uint32 AtPwMinJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize);
uint32 AtPwMaxJitterDelay(AtPw self, AtChannel circuit, uint16 payloadSize);
uint32 AtPwMaxJitterDelayForJitterBufferSize(AtPw self, uint32 jitterBufferSize_us);

/* Control jitter buffer size and delay in packet unit */
eAtModulePwRet AtPwJitterBufferSizeInPacketSet(AtPw self, uint16 numPackets);
uint16 AtPwJitterBufferSizeInPacketGet(AtPw self);
eAtModulePwRet AtPwJitterBufferDelayInPacketSet(AtPw self, uint16 numPackets);
uint16 AtPwJitterBufferDelayInPacketGet(AtPw self);
uint16 AtPwMinJitterBufferDelayInPacketGet(AtPw self, AtChannel circuit, uint16 payloadSize);
uint16 AtPwMinJitterBufferSizeInPacketGet(AtPw self, AtChannel circuit, uint16 payloadSize);

/* To apply valid combination of jitter buffer parameters and payload size */
eAtModulePwRet AtPwJitterBufferAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_us, uint32 jitterDelay_us, uint16 payloadSize);
eAtModulePwRet AtPwJitterBufferInPacketAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_pkt, uint32 jitterDelay_pkt, uint16 payloadSize);

/* Jitter buffer status */
uint32 AtPwNumCurrentPacketsInJitterBuffer(AtPw self);
uint32 AtPwNumCurrentAdditionalBytesInJitterBuffer(AtPw self);
eAtRet AtPwJitterBufferCenter(AtPw adapter);

/* Jitter buffer water mark */
uint32 AtPwJitterBufferWatermarkMinPackets(AtPw self);
uint32 AtPwJitterBufferWatermarkMaxPackets(AtPw self);
eBool AtPwJitterBufferWatermarkIsSupported(AtPw self);
eAtRet AtPwJitterBufferWatermarkReset(AtPw self);

/* RTP */
eAtModulePwRet AtPwRtpEnable(AtPw self, eBool enable);
eBool AtPwRtpIsEnabled(AtPw self);
eAtModulePwRet AtPwRtpTimeStampModeSet(AtPw self, eAtPwRtpTimeStampMode timeStampMode);
eAtPwRtpTimeStampMode AtPwRtpTimeStampModeGet(AtPw self);

/* RTP's PayloadType */
eAtModulePwRet AtPwRtpPayloadTypeSet(AtPw self, uint8 payloadType);
uint8 AtPwRtpPayloadTypeGet(AtPw self);
eAtModulePwRet AtPwRtpTxPayloadTypeSet(AtPw self, uint8 payloadType);
uint8 AtPwRtpTxPayloadTypeGet(AtPw self);
eAtModulePwRet AtPwRtpExpectedPayloadTypeSet(AtPw self, uint8 payloadType);
uint8 AtPwRtpExpectedPayloadTypeGet(AtPw self);
eAtModulePwRet AtPwRtpPayloadTypeCompare(AtPw self, eBool enableCompare);
eBool AtPwRtpPayloadTypeIsCompared(AtPw self);

/* RTP's SSRC */
eAtModulePwRet AtPwRtpSsrcSet(AtPw self, uint32 ssrc);
uint32 AtPwRtpSsrcGet(AtPw self);
eAtModulePwRet AtPwRtpTxSsrcSet(AtPw self, uint32 ssrc);
uint32 AtPwRtpTxSsrcGet(AtPw self);
eAtModulePwRet AtPwRtpExpectedSsrcSet(AtPw self, uint32 ssrc);
uint32 AtPwRtpExpectedSsrcGet(AtPw self);
eAtModulePwRet AtPwRtpSsrcCompare(AtPw self, eBool enableCompare);
eBool AtPwRtpSsrcIsCompared(AtPw self);

/* Control word */
eAtModulePwRet AtPwCwEnable(AtPw self, eBool enable);
eBool AtPwCwIsEnabled(AtPw self);
eAtModulePwRet AtPwCwAutoTxLBitEnable(AtPw self, eBool enable);
eBool AtPwCwAutoTxLBitIsEnabled(AtPw self);
eAtModulePwRet AtPwCwAutoRxLBitEnable(AtPw self, eBool enable);
eBool AtPwCwAutoRxLBitIsEnabled(AtPw self);
eAtModulePwRet AtPwCwAutoRBitEnable(AtPw self, eBool enable);
eBool AtPwCwAutoRBitIsEnabled(AtPw self);
eAtModulePwRet AtPwCwSequenceModeSet(AtPw self, eAtPwCwSequenceMode sequenceMode);
eAtPwCwSequenceMode AtPwCwSequenceModeGet(AtPw self);
eAtModulePwRet AtPwCwLengthModeSet(AtPw self, eAtPwCwLengthMode lengthMode);
eAtPwCwLengthMode AtPwCwLengthModeGet(AtPw self);
eAtModulePwRet AtPwCwPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode);
eAtPwPktReplaceMode AtPwCwPktReplaceModeGet(AtPw self);
eAtModulePwRet AtPwLopsPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode);
eAtPwPktReplaceMode AtPwLopsPktReplaceModeGet(AtPw self);
eBool AtPwPktReplaceModeIsSupported(AtPw self, eAtPwPktReplaceMode pktReplaceMode);

/* IDLE code */
eAtModulePwRet AtPwIdleCodeSet(AtPw self, uint8 idleCode);
uint8  AtPwIdleCodeGet(AtPw self);

/* PSN */
eAtModulePwRet AtPwPsnSet(AtPw self, AtPwPsn psn);
AtPwPsn AtPwPsnGet(AtPw self);

/* Ethernet */
eAtModulePwRet AtPwEthPortSet(AtPw self, AtEthPort ethPort);
AtEthPort AtPwEthPortGet(AtPw self);
eAtModulePwRet AtPwEthFlowSet(AtPw self, AtEthFlow ethFlow);
AtEthFlow AtPwEthFlowGet(AtPw self);
eAtModulePwRet AtPwEthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
eAtModulePwRet AtPwEthDestMacSet(AtPw self, uint8 *destMac);
eAtModulePwRet AtPwEthDestMacGet(AtPw self, uint8 *destMac);
eAtModulePwRet AtPwEthSrcMacSet(AtPw self, uint8 *srcMac);
eAtModulePwRet AtPwEthSrcMacGet(AtPw self, uint8 *srcMac);
eAtModulePwRet AtPwEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
tAtEthVlanTag *AtPwEthCVlanGet(AtPw self, tAtEthVlanTag *cVlan);
tAtEthVlanTag *AtPwEthSVlanGet(AtPw self, tAtEthVlanTag *sVlan);
eAtModulePwRet AtPwEthCVlanTpidSet(AtPw self, uint16 tpid);
eAtModulePwRet AtPwEthSVlanTpidSet(AtPw self, uint16 tpid);
uint16 AtPwEthCVlanTpidGet(AtPw self);
uint16 AtPwEthSVlanTpidGet(AtPw self);

/* For PW that needs to lookup by VLANs */
eAtModulePwRet AtPwEthExpectedCVlanSet(AtPw self, tAtEthVlanTag *cVlan);
tAtEthVlanTag *AtPwEthExpectedCVlanGet(AtPw self, tAtEthVlanTag *cVlan);
eAtModulePwRet AtPwEthExpectedSVlanSet(AtPw self, tAtEthVlanTag *sVlan);
tAtEthVlanTag *AtPwEthExpectedSVlanGet(AtPw self, tAtEthVlanTag *sVlan);

/* Circuit binding */
eAtModulePwRet AtPwCircuitBind(AtPw self, AtChannel circuit);
eAtModulePwRet AtPwCircuitUnbind(AtPw self);
AtChannel AtPwBoundCircuitGet(AtPw self);

/* Defect Thresholds */
uint32 AtPwLopsThresholdMax(AtPw self);
uint32 AtPwLopsThresholdMin(AtPw self);
eAtModulePwRet AtPwLopsSetThresholdSet(AtPw self, uint32 numPackets);
uint32 AtPwLopsSetThresholdGet(AtPw self);
eAtModulePwRet AtPwLopsClearThresholdSet(AtPw self, uint32 numPackets);
uint32 AtPwLopsClearThresholdGet(AtPw self);

/* Queue configuration */
eAtRet AtPwEthPortQueueSet(AtPw self, uint8 queueId);
uint8 AtPwEthPortQueueGet(AtPw self);
uint8 AtPwEthPortMaxNumQueues(AtPw self);

eAtModulePwRet AtPwMissingPacketDefectThresholdSet(AtPw self, uint32 numPackets);
uint32 AtPwMissingPacketDefectThresholdGet(AtPw self);

eAtModulePwRet AtPwExcessivePacketLossRateDefectThresholdSet(AtPw self, uint32 numPackets, uint32 timeInSecond);
uint32 AtPwExcessivePacketLossRateDefectThresholdGet(AtPw self, uint32* timeInSecond);

eAtModulePwRet AtPwStrayPacketDefectThresholdSet(AtPw self, uint32 numPackets);
uint32 AtPwStrayPacketDefectThresholdGet(AtPw self);

eAtModulePwRet AtPwMalformedPacketDefectThresholdSet(AtPw self, uint32 numPackets);
uint32 AtPwMalformedPacketDefectThresholdGet(AtPw self);

eAtModulePwRet AtPwRemotePacketLossDefectThresholdSet(AtPw self, uint32 numPackets);
uint32 AtPwRemotePacketLossDefectThresholdGet(AtPw self);

eAtModulePwRet AtPwJitterBufferOverrunDefectThresholdSet(AtPw self, uint32 numOverrunEvent);
uint32 AtPwJitterBufferOverrunDefectThresholdGet(AtPw self);

eAtModulePwRet AtPwJitterBufferUnderrunDefectThresholdSet(AtPw self, uint32 numUnderrunEvent);
uint32 AtPwJitterBufferUnderrunDefectThresholdGet(AtPw self);

eAtModulePwRet AtPwMisConnectionDefectThresholdSet(AtPw self, uint32 numPackets);
uint32 AtPwMisConnectionDefectThresholdGet(AtPw self);

/* PW associated groups */
AtPwGroup AtPwApsGroupGet(AtPw self);
AtPwGroup AtPwHsGroupGet(AtPw self);

/* Backup header */
eAtModulePwRet AtPwBackupPsnSet(AtPw self, AtPwPsn backupPsn);
AtPwPsn AtPwBackupPsnGet(AtPw self);
eAtModulePwRet AtPwBackupEthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
eAtModulePwRet AtPwBackupEthSrcMacSet(AtPw self, uint8 *srcMac);
eAtModulePwRet AtPwBackupEthSrcMacGet(AtPw self, uint8 *srcMac);
eAtModulePwRet AtPwBackupEthDestMacSet(AtPw self, uint8 *destMac);
eAtModulePwRet AtPwBackupEthDestMacGet(AtPw self, uint8 *destMac);
eAtModulePwRet AtPwBackupEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan);
tAtEthVlanTag *AtPwBackupEthCVlanGet(AtPw self, tAtEthVlanTag *cVlan);
tAtEthVlanTag *AtPwBackupEthSVlanGet(AtPw self, tAtEthVlanTag *sVlan);

/* Capacity */
eBool AtPwRtpTimeStampModeIsSupported(AtPw self, eAtPwRtpTimeStampMode timeStampMode);
eBool AtPwCwSequenceModeIsSupported(AtPw self, eAtPwCwSequenceMode sequenceMode);
eBool AtPwCwLengthModeIsSupported(AtPw self, eAtPwCwLengthMode lengthMode);
eBool AtPwCwAutoRxLBitCanEnable(AtPw self, eBool enable);

/* Utils */
eBool AtPwIsTdmPw(AtPw self);

/* Number of blocks used by a PW */
uint32 AtPwJitterBufferNumBlocksGet(AtPw self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPW_H_ */

#include "AtPwDeprecated.h"

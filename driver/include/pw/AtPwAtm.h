/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtPwAtm.h
 * 
 * Created Date: Oct 4, 2012
 *
 * Description : ATM PW
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWATM_H_
#define _ATPWATM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Additional methods will be added */

#ifdef __cplusplus
}
#endif
#endif /* _ATPWATM_H_ */


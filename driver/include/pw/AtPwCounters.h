/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Pseudowire
 * 
 * File        : AtPwCounters.h
 * 
 * Created Date: Sep 18, 2012
 *
 * Description : All pseudowire counter classes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWCOUNTERS_H_
#define _ATPWCOUNTERS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPwCounters
 * @{
 */

/** @brief Pseudowire abstract counters */
typedef struct tAtPwCounters * AtPwCounters;

/** @brief TDM Pseudowire abstract counters */
typedef struct tAtPwTdmCounters * AtPwTdmCounters;

/** @brief CESoPSN Pseudowire counters */
typedef struct tAtPwCESoPCounters * AtPwCESoPCounters;

/** @brief SAToP Pseudowire counters */
typedef struct tAtPwSAToPCounters * AtPwSAToPCounters;

/** @brief CEP Pseudowire counters */
typedef struct tAtPwCepCounters * AtPwCepCounters;

/** @brief HDLC Pseudowire counters */
typedef struct tAtPwHdlcCounters * AtPwHdlcCounters;

/** @brief PW counter types */
typedef enum eAtPwCounterType
    {
    cAtPwCounterTypeTxPackets,              /**< PW TX packet counter */
    cAtPwCounterTypeTxPayloadBytes,         /**< PW TX byte counter */
    cAtPwCounterTypeRxPackets,              /**< PW RX packet counter */
    cAtPwCounterTypeRxPayloadBytes,         /**< PW RX byte counter */
    cAtPwCounterTypeRxDiscardedPackets,     /**< PW RX discarded packet counter */
    cAtPwCounterTypeRxMalformedPackets,     /**< PW RX malformed packet counter */
    cAtPwCounterTypeRxReorderedPackets,     /**< PW RX reordered packet counter */
    cAtPwCounterTypeRxLostPackets,          /**< PW RX lost packet counter */
    cAtPwCounterTypeRxOutOfSeqDropPackets,  /**< PW RX out of sequence packet counter */
    cAtPwCounterTypeRxOamPackets,           /**< PW RX OAM packet counter */
    cAtPwCounterTypeRxStrayPackets,         /**< PW RX stray packet counter */
    cAtPwCounterTypeRxDuplicatedPackets,    /**< PW RX duplicated packet counter */

    cAtPwTdmCounterTypeTxLbitPackets,       /**< PW TX lbit packet counter */
    cAtPwTdmCounterTypeTxRbitPackets,       /**< PW TX rbit packet counter */
    cAtPwTdmCounterTypeRxLbitPackets,       /**< PW RX lbit packet counter */
    cAtPwTdmCounterTypeRxRbitPackets,       /**< PW RX rbit packet counter */
    cAtPwTdmCounterTypeRxJitBufOverrun,     /**< PW RX jitter buffer overrun packet counter */
    cAtPwTdmCounterTypeRxJitBufUnderrun,    /**< PW RX jitter buffer underrun packet counter */
    cAtPwTdmCounterTypeRxLops,              /**< PW RX LOPS packet counter */
    cAtPwTdmCounterTypeRxPacketsSentToTdm,  /**< PW RX sent packet to tdm counter */

    cAtPwCESoPCounterTypeTxMbitPackets,     /**< PW TX mbit packet counter */
    cAtPwCESoPCounterTypeRxMbitPackets,     /**< PW RX packet counter */

    cAtPwCepCounterTypeTxNbitPackets,       /**< PW TX nbit packet counter */
    cAtPwCepCounterTypeTxPbitPackets,       /**< PW TX pbit packet counter */
    cAtPwCepCounterTypeRxNbitPackets,       /**< PW RX nbit packet counter */
    cAtPwCepCounterTypeRxPbitPackets        /**< PW RX pbit packet counter */
    } eAtPwCounterType;

    /** @} */


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Access Pseudowire counters */
uint32 AtPwCountersTxPacketsGet(AtPwCounters self);
uint32 AtPwCountersTxPayloadBytesGet(AtPwCounters self);
uint32 AtPwCountersRxPacketsGet(AtPwCounters self);
uint32 AtPwCountersRxPayloadBytesGet(AtPwCounters self);
uint32 AtPwCountersRxDiscardedPacketsGet(AtPwCounters self);
uint32 AtPwCountersRxMalformedPacketsGet(AtPwCounters self);
uint32 AtPwCountersRxReorderedPacketsGet(AtPwCounters self);
uint32 AtPwCountersRxLostPacketsGet(AtPwCounters self);
uint32 AtPwCountersRxOutOfSeqDropPacketsGet(AtPwCounters self);
uint32 AtPwCountersRxOamPacketsGet(AtPwCounters self);
uint32 AtPwCountersRxStrayPacketsGet(AtPwCounters self);
uint32 AtPwCountersRxDuplicatedPacketsGet(AtPwCounters self);

/* Access TDM pseudowire counters */
uint32 AtPwTdmCountersTxLbitPacketsGet(AtPwTdmCounters self);
uint32 AtPwTdmCountersTxRbitPacketsGet(AtPwTdmCounters self);
uint32 AtPwTdmCountersRxLbitPacketsGet(AtPwTdmCounters self);
uint32 AtPwTdmCountersRxRbitPacketsGet(AtPwTdmCounters self);
uint32 AtPwTdmCountersRxJitBufOverrunGet(AtPwTdmCounters self);
uint32 AtPwTdmCountersRxJitBufUnderrunGet(AtPwTdmCounters self);
uint32 AtPwTdmCountersRxLopsGet(AtPwTdmCounters self);
uint32 AtPwTdmCountersRxPacketsSentToTdmGet(AtPwTdmCounters self);

/* Access additional counters of CESoP Pseudowire */
uint32 AtPwCESoPCountersTxMbitPacketsGet(AtPwCESoPCounters self);
uint32 AtPwCESoPCountersRxMbitPacketsGet(AtPwCESoPCounters self);

/* Access additional counters of CEP Pseudowire */
uint32 AtPwCepCountersTxNbitPacketsGet(AtPwCepCounters self);
uint32 AtPwCepCountersTxPbitPacketsGet(AtPwCepCounters self);
uint32 AtPwCepCountersRxNbitPacketsGet(AtPwCepCounters self);
uint32 AtPwCepCountersRxPbitPacketsGet(AtPwCepCounters self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWCOUNTERS_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Pseudowire
 * 
 * File        : AtModulePw.h
 * 
 * Created Date: Sep 18, 2012
 *
 * Description : Pseudowire module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPW_H_
#define _ATMODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h" /* Super class */
#include "AtIterator.h"
#include "AtChannelClasses.h"
#include "AtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePw
 * @{
 */

/**
 * @brief Pseudowire module
 */
typedef struct tAtModulePw * AtModulePw;

/** @brief Pseudowire abstract class */
typedef struct tAtPw * AtPw;

/** @brief Pseudowire group abstract class */
typedef struct tAtPwGroup * AtPwGroup;

/** @brief CEP Psuedowire abstract class */
typedef struct tAtPwCep * AtPwCep;

/** @brief CESoPSN Pseudowire abstract class */
typedef struct tAtPwCESoP * AtPwCESoP;

/** @brief ATM Pseudowire abstract class */
typedef struct tAtPwAtm * AtPwAtm;

/** @brief SAToP Pseudowire abstract class */
typedef struct tAtPwSAToP * AtPwSAToP;

/** @brief HDLC Pseudowire abstract class */
typedef struct tAtPwHdlc * AtPwHdlc;

/** @brief PPP Pseudowire abstract class */
typedef struct tAtPwPpp * AtPwPpp;

/** @brief FR Pseudowire abstract class */
typedef struct tAtPwFr * AtPwFr;

/** @brief TOH Pseudowire abstract class */
typedef struct tAtPwToh * AtPwToh;

/** @brief Pseudowire module return codes */
typedef uint32 eAtModulePwRet;

/**
 * @brief ATM Pseudowire types
 */
typedef enum eAtAtmPwType
    {
    cAtAtmPwTypeVcc1To1, /**< Maps one ATM Virtual Channel Connection (VCC) to one Pseudowire) */
    cAtAtmPwTypeVpc1To1, /**< Maps one ATM Virtual Path Connection (VPC) to one Pseudowire) */
    cAtAtmPwTypeNTo1     /**< Maps N ATM Virtual Connections to one Pseudowire */
    }eAtAtmPwType;

/**
 * @brief DCR clock source
 */
typedef enum eAtPwDcrClockSource
    {
    cAtPwDcrClockSourceUnknown, /**< Unknown clock source */
    cAtPwDcrClockSourcePrc,     /**< PRC Reference Clock */
    cAtPwDcrClockSourceSystem,  /**< System Clock */
    cAtPwDcrClockSourceExt1,    /**< External Reference Clock 1 */
    cAtPwDcrClockSourceExt2     /**< External Reference Clock 2 */
    }eAtPwDcrClockSource;

/**
 * @brief CESoP mode
 */
typedef enum eAtPwCESoPMode
    {
    cAtPwCESoPModeUnknown, /**< Unknown mode */
    cAtPwCESoPModeBasic,   /**< CESoP basic */
    cAtPwCESoPModeWithCas  /**< CESoP with CAS */
    }eAtPwCESoPMode;

/**
 * @brief CEP mode
 */
typedef enum eAtPwCepMode
    {
    cAtPwCepModeUnknown,    /**< Unknown mode */
    cAtPwCepModeBasic,      /**< CEP basic */
    cAtPwCepModeFractional  /**< CEP fractional */
    }eAtPwCepMode;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* TDM PWs are managed in the table of PW pool resource. */
AtPwCep AtModulePwCepCreate(AtModulePw self, uint32 pwId, eAtPwCepMode mode);
AtPwSAToP AtModulePwSAToPCreate(AtModulePw self, uint32 pwId);
AtPwCESoP AtModulePwCESoPCreate(AtModulePw self, uint32 pwId, eAtPwCESoPMode mode);
AtPwToh AtModulePwTohCreate(AtModulePw self, uint32 pwId);

AtPw AtModulePwGetPw(AtModulePw self, uint32 pwId);
eAtModulePwRet AtModulePwDeletePw(AtModulePw self, uint32 pw);
uint32 AtModulePwMaxPwsGet(AtModulePw self);
uint32 AtModulePwFreePwGet(AtModulePw self);

/* Packet PWs are managed by the associated channel. */
AtPwAtm AtModulePwAtmCreate(AtModulePw self, uint32 pwId, eAtAtmPwType atmPwType);
AtPwHdlc AtModulePwHdlcCreate(AtModulePw self, AtHdlcChannel hdlcChannel);
AtPwPpp AtModulePwPppCreate(AtModulePw self, AtPppLink pppLink);
AtPwPpp AtModulePwMlpppCreate(AtModulePw self, AtMpBundle mpBundle);
AtPwFr AtModulePwFrVcCreate(AtModulePw self, AtFrVirtualCircuit frVc);

/* To delete any Pseudo-wire channel. */
eAtModulePwRet AtModulePwDeletePwObject(AtModulePw self, AtPw pw);

/* DCR processing */
eAtModulePwRet AtModulePwDcrClockSourceSet(AtModulePw self, eAtPwDcrClockSource clockSource);
eAtPwDcrClockSource AtModulePwDcrClockSourceGet(AtModulePw self);
eAtModulePwRet AtModulePwDcrClockFrequencySet(AtModulePw self, uint32 khz);
uint32 AtModulePwDcrClockFrequencyGet(AtModulePw self);

/* Timestamp frequency for CEP and CES features */
eAtModulePwRet AtModulePwCepRtpTimestampFrequencySet(AtModulePw self, uint32 khz);
uint32 AtModulePwCepRtpTimestampFrequencyGet(AtModulePw self);
eAtModulePwRet AtModulePwCesRtpTimestampFrequencySet(AtModulePw self, uint32 khz);
uint32 AtModulePwCesRtpTimestampFrequencyGet(AtModulePw self);
eBool AtModulePwRtpTimestampFrequencyIsSupported(AtModulePw self, uint32 khz);

/* Timestamp frequency used for products that do not support mix of CEP and CES.
 * For product that mix of CEP and CES, calling SET API will apply the same
 * configuration for both of them. */
eAtModulePwRet AtModulePwRtpTimestampFrequencySet(AtModulePw self, uint32 khz);
uint32 AtModulePwRtpTimestampFrequencyGet(AtModulePw self);

/* Pseudowire iterator */
AtIterator AtModulePwIteratorCreate(AtModulePw self);

/* Idle code */
eAtModulePwRet AtModulePwIdleCodeSet(AtModulePw self, uint8 idleCode);
uint8 AtModulePwIdleCodeGet(AtModulePw self);
eBool AtModulePwIdleCodeIsSupported(AtModulePw self);
eBool AtModulePwCasIdleCodeIsSupported(AtModulePw self);

/* APS PW groups management */
uint32 AtModulePwMaxApsGroupsGet(AtModulePw self);
AtPwGroup AtModulePwApsGroupCreate(AtModulePw self, uint32 groupId);
eAtRet AtModulePwApsGroupDelete(AtModulePw self, uint32 groupId);
AtPwGroup AtModulePwApsGroupGet(AtModulePw self, uint32 groupId);

/* HS PW groups management */
uint32 AtModulePwMaxHsGroupsGet(AtModulePw self);
AtPwGroup AtModulePwHsGroupCreate(AtModulePw self, uint32 groupId);
eAtRet AtModulePwHsGroupDelete(AtModulePw self, uint32 groupId);
AtPwGroup AtModulePwHsGroupGet(AtModulePw self, uint32 groupId);

/* Jitter centering */
eAtModulePwRet AtModulePwJitterBufferCenteringEnable(AtModulePw self, eBool enable);
eBool AtModulePwJitterBufferCenteringIsEnabled(AtModulePw self);
eBool AtModulePwJitterBufferCenteringIsSupported(AtModulePw self);

/* Jitter buffer RAM blocks resource management */
uint32 AtModulePwJitterBufferMaxBlocksGet(AtModulePw self);
uint32 AtModulePwJitterBufferBlockSizeInBytesGet(AtModulePw self);
uint32 AtModulePwJitterBufferFreeBlocksGet(AtModulePw self);

/* cesop payload size in byte support */
eAtRet AtModulePwCesopPayloadSizeInByteEnable(AtModulePw self, eBool enabled);
eBool AtModulePwCesopPayloadSizeInByteIsEnabled(AtModulePw self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPW_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Pseudowire
 * 
 * File        : AtPwSAToP.h
 * 
 * Created Date: Sep 19, 2012
 *
 * Description : SAToP Pseudowire class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWSATOP_H_
#define _ATPWSATOP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePw.h"
#include "AtPw.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATPWSATOP_H_ */


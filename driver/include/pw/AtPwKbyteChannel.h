/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : AtPwKbyteChannel.h
 * 
 * Created Date: Jun 8, 2018
 *
 * Description : Abstract PW K-byte channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWKBYTECHANNEL_H_
#define _ATPWKBYTECHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhLine.h"
#include "AtPw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/* Move to generic if necessary in the future. */
typedef struct tAtPwKbyteChannel *AtPwKbyteChannel;

typedef enum eAtPwKbyteChannelAlarmType
    {
    cAtPwKbyteChannelAlarmChannelNone       = 0,
    cAtPwKbyteChannelAlarmChannelMismatched = cBit0
    }eAtPwKbyteChannelAlarmType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* K-byte PW */
AtPw AtPwKbyteChannelKbytePwGet(AtPwKbyteChannel self);

/* SDH line mapping */
eAtRet AtPwKbyteChannelSdhLineSet(AtPwKbyteChannel self, AtSdhLine line);
AtSdhLine AtPwKbyteChannelSdhLineGet(AtPwKbyteChannel self);

/* K-byte validation */
eAtRet AtPwKbyteChannelValidationEnable(AtPwKbyteChannel self, eBool enable);
eBool AtPwKbyteChannelValidationIsEnabled(AtPwKbyteChannel self);

/* AIS-L/RDI-L suppression */
eAtRet AtPwKbyteChannelAlarmSuppressionEnable(AtPwKbyteChannel self, eBool enable);
eBool AtPwKbyteChannelAlarmSuppressionIsEnabled(AtPwKbyteChannel self);

/* K-byte overriding */
eAtRet AtPwKbyteChannelOverrideEnable(AtPwKbyteChannel self, eBool enable);
eBool AtPwKbyteChannelOverrideIsEnabled(AtPwKbyteChannel self);

/* Transmit K-byte toward PSN */
eAtRet AtPwKbyteChannelTxK1Set(AtPwKbyteChannel self, uint8 value);
uint8 AtPwKbyteChannelTxK1Get(AtPwKbyteChannel self);
eAtRet AtPwKbyteChannelTxK2Set(AtPwKbyteChannel self, uint8 value);
uint8 AtPwKbyteChannelTxK2Get(AtPwKbyteChannel self);
eAtRet AtPwKbyteChannelTxEK1Set(AtPwKbyteChannel self, uint8 value);
uint8 AtPwKbyteChannelTxEK1Get(AtPwKbyteChannel self);
eAtRet AtPwKbyteChannelTxEK2Set(AtPwKbyteChannel self, uint8 value);
uint8 AtPwKbyteChannelTxEK2Get(AtPwKbyteChannel self);

/* Received K-byte from PSN */
uint8 AtPwKbyteChannelRxK1Get(AtPwKbyteChannel self);
uint8 AtPwKbyteChannelRxK2Get(AtPwKbyteChannel self);
uint8 AtPwKbyteChannelRxEK1Get(AtPwKbyteChannel self);
uint8 AtPwKbyteChannelRxEK2Get(AtPwKbyteChannel self);

/* Channel label */
eAtRet AtPwKbyteChannelExpectedLabelSet(AtPwKbyteChannel self, uint16 value);
uint16 AtPwKbyteChannelExpectedLabelGet(AtPwKbyteChannel self);
eAtRet AtPwKbyteChannelTxLabelSet(AtPwKbyteChannel self, uint16 value);
uint16 AtPwKbyteChannelTxLabelGet(AtPwKbyteChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWKBYTECHANNEL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : IMA
 * 
 * File        : AtImaGroup.h
 * 
 * Created Date: Sep 27, 2012
 *
 * Description : IMA group
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIMAGROUP_H_
#define _ATIMAGROUP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h" /* Super class */
#include "AtModuleIma.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtImaGroup
 * @{
 */
/**
 * @brief IMA group version
 */
typedef enum eAtImaVersion
    {
    cAtImaVersionInvalid, /**< Invalid */
    cAtImaVersion10,      /**< Version 1.0 */
    cAtImaVersion11       /**< Version 1.1 */
    } eAtImaVersion;

/**
 * @brief IMA group Symmetric working mode
 */
typedef enum eAtImaSymmetryMode
    {
    cAtImaASymmetryModeInvalid,                /**< Invalid */
    cAtImaSymmetryModeConfigAndSymOperation,   /**< Symmetric Configuration and Symmetric Operation */
    cAtImaSymmetryModeConfigAndAsymOperation,  /**< Symmetric Configuration and Asymmetric Operation */
    cAtImaASymmetryModeConfigAndAsymOperation  /**< Asymmetric Configuration and Asymmetric Operation */
    } eAtImaSymmetryMode;

/**
 * @brief  TX clock mode for group
 */
typedef enum eAtImaTxClockMode
    {
    cAtImaTxClockModeInvalid,  /**< Invalid */
    cAtImaTxClockModeCtc,      /**< Common Transmit Clock */
    cAtImaTxClockModeItc       /**< Independent Transmit Clock */
    } eAtImaTxClockMode;

/**
 * @brief IMA frame length
 */
typedef enum eAtImaFrameLength
    {
    cAtImaFrameLengthInvalid, /**< Invalid */
    cAtImaFrameLength32,      /**< 32 cells */
    cAtImaFrameLength64,      /**< 64 cells */
    cAtImaFrameLength128,     /**< 128 cells  */
    cAtImaFrameLength256      /**< 256 cells */
    } eAtImaFrameLength;

/**
 * @brief IMA Group State
 */
typedef enum eAtImaGroupState
    {
    cAtImaGroupStateInvalid,
    cAtImaGroupStateStartup,                   /**< Start up */
    cAtImaGroupStateStartupAck,                /**< Start up ACK */
    cAtImaGroupStateConfigAbortedUnsupportM,   /**< Config-Aborted - Unsupported M (IMA frame length) */
    cAtImaGroupStateConfigAbortedSymmetry,     /**< Config-Aborted - Incompatible Group Symmetry */
    cAtImaGroupStateConfigAbortedImaVersion,   /**< Config-Aborted - Unsupported IMA Version */
    cAtImaGroupStateConfigAbortedOtherReasons, /**< Config-Aborted - Other reasons */
    cAtImaGroupStateInsufficientLinks,         /**< Insufficient-Links */
    cAtImaGroupStateBlocked,                   /**< Blocked */
    cAtImaGroupStateOperational                /**< Operational */
    } eAtImaGroupState;

/**
 * @brief Group performance counters type
 */
typedef enum eAtImaGroupCounterType
    {
    cAtImaGroupCounterTypeRxFillerCell, /**< RX filler cell */
    cAtImaGroupCounterTypeRxUserCell,   /**< RX user cell */
    cAtImaGroupCounterTypeTxFillerCell, /**< TX filler cell */
    cAtImaGroupCounterTypeTxUserCell    /**< TX user cell */
    } eAtImaGroupCounterType;

/**
 * @brief IMA group alarms type
 */
typedef enum eAtImaGroupAlarmType
    {
    cAtImaGroupAlarmTypeGsmChange  = cBit0,  /**< Group state machine change */
    cAtImaGroupAlarmTypeGtsmChange = cBit1   /**< Group traffic state machine change */
    }eAtImaGroupAlarmType;
/**
 * @brief All Group performance counters
 */
typedef struct tAtImaGroupCounters
    {
    uint32 rxFillerCell;         /**< RX filler cell counter */
    uint32 rxUserCell;           /**< RX user cell counter */
    uint32 txFillerCell;         /**< TX filler cell counter */
    uint32 txUserCell;           /**< TX user cell counter */
    } tAtImaGroupCounters;

/**
 * @brief Group status
 */
typedef struct tAtImaGroupStatus
    {
    eAtImaGroupState   neGroupState;        /**< Near End GSM state */
    eBool              neGroupTrafficState; /**< Near End GTSM state: Up (cAtTrue), Down (cAtFalse) */
    uint16             neTxScci;            /**< Near End Tx SCCI */
    uint16             neRxNumLinkUnusable; /**< Near End Rx: Number of links is in Usable state */
    uint16             neTxNumLinkActive;   /**< Near End Tx: Number of links is in Active state */
    uint16             neRxNumLinkActive;   /**< Near End Rx: Number of links is in Active state */
    uint16             neTxNumLinkInLasr;   /**< Near End Tx: Number of links is in LASR procedure */
    uint16             neRxNumLinkInLasr;   /**< Near End Rx: Number of links is in LASR procedure */

    eAtImaGroupState   feGroupState;        /**< Far End GSM state */
    uint16             feImaId;             /**< Far End IMA Id */
    uint16             feTrlId;             /**< Far End TRL (Timing Reference Link) Id */
    uint16             feRxNumLinkUnusable; /**< Far End Rx: Number of links is in Usable state */
    uint16             feTxNumLinkActive;   /**< Far End Tx: Number of links is in Active state */
    eAtImaFrameLength  feFrmLen;            /**< Far End Frame length */
    eAtImaSymmetryMode feSymmetryMode;      /**< Far End Symmetry mode */
    eAtImaVersion      feImaVersion;        /**< Far End IMA version */
    } tAtImaGroupStatus;

/**
 * @}
 */
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Set Group properties */
eAtImaRet AtImaGroupVersionSet(AtImaGroup self, eAtImaVersion version);
eAtImaVersion AtImaGroupVersionGet(AtImaGroup self);

eAtImaRet AtImaGroupImaIdSet(AtImaGroup self, uint32 imaId);
uint32 AtImaGroupImaIdGet(AtImaGroup self);

eAtImaRet AtImaGroupSymmetryModeSet(AtImaGroup self, eAtImaSymmetryMode symmetryMode);
eAtImaSymmetryMode AtImaGroupSymmetryModeGet(AtImaGroup self);

eAtImaRet AtImaGroupTxClockModeSet(AtImaGroup self, eAtImaTxClockMode txClockMd);
eAtImaTxClockMode AtImaGroupTxClockModeGet(AtImaGroup self);

eAtImaRet AtImaGroupTrlIdSet(AtImaGroup self, uint8 trlId);
uint8 AtImaGroupTrlIdGet(AtImaGroup self);

eAtImaRet AtImaGroupTxMinNumLinkSet(AtImaGroup self, uint8 txMinNumLink);
uint8 AtImaGroupTxMinNumLinkGet(AtImaGroup self);

eAtImaRet AtImaGroupRxMinNumLinkSet(AtImaGroup self, uint8 rxMinNumLink);
uint8 AtImaGroupRxMinNumLinkGet(AtImaGroup self);

eAtImaRet AtImaGroupFrameLenSet(AtImaGroup self, eAtImaFrameLength frameLen);
eAtImaFrameLength AtImaGroupFrameLenGet(AtImaGroup self);

eAtImaRet AtImaGroupMaxDifferentialLinkDelaySet(AtImaGroup self, uint32 maxDifferentialLinkDelay);
uint32 AtImaGroupMaxDifferentialLinkDelayGet(AtImaGroup self);

/* Group management */
eAtImaRet AtImaGroupInhibit(AtImaGroup self, eBool enable);
eBool AtImaGroupIsInhibited(AtImaGroup self);
eAtImaRet AtImaGroupRestart(AtImaGroup self);

/* Link management */
eAtImaRet AtImaGroupLinkAdd(AtImaGroup self, AtImaLink txLink, AtImaLink rxLink);
uint8 AtImaGroupTxLinksGet(AtImaGroup self, AtImaLink *links);
uint8 AtImaGroupRxLinksGet(AtImaGroup self, AtImaLink *links);

/* Status */
eAtImaRet AtImaGroupStatusGet(AtImaGroup self, tAtImaGroupStatus *status);

/* Test pattern for link in group */
eAtImaRet AtImaGroupTestEnable(AtImaGroup self, AtImaLink link, eBool enable, uint8 pattern);
eBool AtImaGroupTestIsEnabled(AtImaGroup self);
eBool AtImaGroupTestResultGet(AtImaGroup self);

#ifdef __cplusplus
}
#endif
#endif /* _ATIMAGROUP_H_ */


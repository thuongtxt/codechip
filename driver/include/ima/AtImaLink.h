/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : IMA
 * 
 * File        : AtModuleImaLink.h
 * 
 * Created Date: Sep 27, 2012
 *
 * Description : IMA Link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEIMALINK_H_
#define _ATMODULEIMALINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h" /* Super class */
#include "AtAtmTc.h"
#include "AtModuleIma.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @brief Link performance counters
 */
typedef enum eAtImaLinkCounterType
    {
    cAtImaLinkCounterTypeRxFillerCell,  /**< RX filler cell */
    cAtImaLinkCounterTypeRxUserCell,    /**< RX user cell */
    cAtImaLinkCounterTypeRxOifCell,     /**< RX OIF cell */
    cAtImaLinkCounterTypeRxIcpCell,     /**< RX ICP cell */
    cAtImaLinkCounterTypeRxIcpErrCell,  /**< RX OCP cell */
    cAtImaLinkCounterTypeRxStuffCell,   /**< RX stuff cell */
    cAtImaLinkCounterTypeRxErrorCell,   /**< RX error cell */
    cAtImaLinkCounterTypeTxFillerCell,  /**< TX filler cell */
    cAtImaLinkCounterTypeTxUserCell,    /**< TX user cell */
    cAtImaLinkCounterTypeTxStuffCell,   /**< TX stuff cell */
    cAtImaLinkCounterTypeTxIcpCell     /**< TX ICP cell */
    }eAtImaLinkCounterType;

/**
 * @brief IMA link alarms
 */
typedef enum eAtImaLinkAlarmType
    {
    cAtImaLinkAlarmTypeDcbError      = cBit0, /**< DCB process error */
    cAtImaLinkAlarmTypeIcpError      = cBit1, /**< ICP error */
    cAtImaLinkAlarmTypeRxStateChange = cBit2, /**< RX state change */
    cAtImaLinkAlarmTypeTxStateChange = cBit3, /**< TX state change */
    cAtImaLinkAlarmTypeRxFeDefect    = cBit4, /**< Rx FE defect detection*/
    cAtImaLinkAlarmTypeRxOif         = cBit5, /**< Rx Out of IMA frame */
    cAtImaLinkAlarmTypeRxLif         = cBit6, /**< Rx Loss IMA frame */
    cAtImaLinkAlarmTypeRxLods        = cBit7, /**< Rx Loss of delay synchronization */
    cAtImaLinkAlarmTypeLarsFinish    = cBit8  /**< LARS process finish */
    }eAtImaLinkAlarmType;

/**
 * @brief  State of IMA link
 */
typedef enum eAtImaLinkState
    {
    cAtImaLinkStateNotInGroup,           /**< Not in Group */
    cAtImaLinkStateUnusableNoReason,     /**< Unusable - No reason given */
    cAtImaLinkStateUnusableFault,        /**< Unusable - Fault (vendor specific) */
    cAtImaLinkStateUnusableMisconnected, /**< Unusable - Mis-connected */
    cAtImaLinkStateUnusableInhibited,    /**< Unusable - Inhibited (vendor specific) */
    cAtImaLinkStateUnusableFailed,       /**< Unusable - Failed */
    cAtImaLinkStateUsable,               /**< Usable */
    cAtImaLinkStateActive                /**< Active */
    } eAtImaLinkState;

/**
 * @brief  IMA link defect
 */
typedef enum eAtImaLinkDefect
    {
    cAtImaLinkDefectNone,     /**< No defect */
    cAtImaLinkDefectPhysical, /**< Physical Link Defect (e.g.LOS, OOF/LOF, LCD)  */
    cAtImaLinkDefectLif,      /**< Loss of IMA Frame */
    cAtImaLinkDefectLods      /**< Link Out of Delay Synchronization defect */
    } eAtImaLinkDefect;


/**
 * @brief  IMA error state machine
 */
typedef enum eAtImaLinkIesmState
    {
    cAtImaLinkIesmStateWorking, /**< Working state */
    cAtImaLinkIesmStateOif,     /**< OIF Anomaly */
    cAtImaLinkIesmStateLif      /**< LIF Defect */
    } eAtImaLinkIesmState;


/**
 * @brief IMA link status
 */
typedef struct tAtImaLinkStatus
    {
    eAtImaLinkState      neTxLinkState;    /**< Near End: Tx Link State */
    eAtImaLinkState      neRxLinkState;    /**< Near End: Rx Link State */
    eAtImaLinkState      feTxLinkState;    /**< Far End: Tx Link State */
    eAtImaLinkState      feRxLinkState;    /**< Far End: Rx Link State */
    eAtImaLinkDefect     neRxDefect;       /**< Near End: Rx Defect received in ICP cell */
    eAtImaLinkDefect     feRxDefect;       /**< Far End: Rx Defect received in ICP cell */
    eAtImaLinkIesmState  neIesmState;      /**< Near End: IMA Link Error/Maintenance state */
    eBool                nePhysicalDefect; /**< Near End: Physical link defect status: No defect (cAtFalse), else cAtTrue */
    eBool                neLodsDefect;     /**< Near End: LODS defect status: No defect (cAtFalse), else cAtTrue */
    }tAtImaLinkStatus;

/**
 * @brief All counters of IMA link
 */
typedef struct tAtImaLinkCounters
    {
    uint32      rxFillerCells;   /**< RX filler cell counter */
    uint32      rxUserCells;     /**< RX user cell counter */
    uint32      rxOifCells;      /**< RX OIF cell counter */
    uint32      rxIcpCells;      /**< RX ICP cell counter */
    uint32      rxIcpErrorCells; /**< RX OCP cell counter */
    uint32      rxStuffCells;    /**< RX stuff cell counter */
    uint32      rxErrorCells;    /**< RX error cell counter */

    uint32      txFillerCells;   /**< TX filler cell counter */
    uint32      txUserCells;     /**< TX user cell counter */
    uint32      txStuffCells;    /**< TX stuff cell counter */
    uint32      txIcpCells;      /**< TX ICP cell counter */
    }tAtImaLinkCounters;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Physical bind */
eAtImaRet AtImaLinkTxBind(AtImaLink self, AtAtmTc atmTc);
eAtImaRet AtImaLinkRxBind(AtImaLink self, AtAtmTc atmTc);

/* Inhibit */
eAtImaRet AtImaLinkInhibit(AtImaLink self, eBool enable);
eBool AtImaLinkIsInhibited(AtImaLink self);

/* Status */
eAtImaRet AtImaLinkStatusGet(AtImaLink self, tAtImaLinkStatus *status);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEIMALINK_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : IMA
 * 
 * File        : AtModuleIma.h
 * 
 * Created Date: Sep 27, 2012
 *
 * Description : IMA module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEIMA_H_
#define _ATMODULEIMA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"  /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleIma
 * @{
 */

/** @brief Module IMA */
typedef struct tAtModuleIma * AtModuleIma;

/** @brief IMA group class */
typedef struct tAtImaGroup * AtImaGroup;

/** @brief IMA link class */
typedef struct tAtImaLink * AtImaLink;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* IMA group management */
uint32 AtModuleImaMaxGroupsGet(AtModuleIma self);
uint32 AtModuleImaFreeGroupGet(AtModuleIma self);
AtImaGroup AtModuleImaGroupCreate(AtModuleIma self, uint32 groupId);
AtImaGroup AtModuleImaGroupGet(AtModuleIma self, uint32 groupId);
eAtImaRet AtModuleImaGroupDelete(AtModuleIma self, uint32 groupId);

/* IMA Link management */
uint32 AtModuleImaMaxLinksGet(AtModuleIma self);
uint32 AtModuleImaFreeLinkGet(AtModuleIma self);
AtImaLink AtModuleImaLinkCreate(AtModuleIma self, uint32 linkId);
AtImaLink AtModuleImaLinkGet(AtModuleIma self, uint32 linkId);
eAtImaRet AtModuleImaLinkDelete(AtModuleIma self, uint32 linkId);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEIMA_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : AtMpBundle.h
 * 
 * Created Date: Aug 3, 2012
 *
 * Description : MP bundle
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMPBUNDLE_H_
#define _ATMPBUNDLE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHdlcBundle.h"  /* Super class */
#include "AtModulePpp.h"
#include "AtModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePpp
 * @{
 */

/**
 * @brief MLPPP fragmentation sequence format
 */
typedef enum eAtMpSequenceMode
    {
    cAtMpSequenceModeShort,  /**< Short sequence */
    cAtMpSequenceModeLong,   /**< Long sequence */
    cAtMpSequenceModeUnknown /**< Unknown sequence mode */
    }eAtMpSequenceMode;

/**
 * @brief MLPPP bundle working mode
 */
typedef enum eAtMpWorkingMode
    {
    cAtMpWorkingModeLfi  = 0, /**< Link fragment interleave */
    cAtMpWorkingModeMcml = 1  /**< Multi-class multi-link mode */
    }eAtMpWorkingMode;

/**
 * @brief Bundle scheduling mode
 */
typedef enum eAtMpSchedulingMode
    {
    cAtMpSchedulingModeRandom   = 0x0, /**< Random */
    cAtMpSchedulingModeFairRR   = 0x1, /**< Fair Round Robin */
    cAtMpSchedulingModeStrictRR = 0x2, /**< Strict Round Robin */
    cAtMpSchedulingModeSmartRR  = 0x3, /**< Smart Round Robin */
    cAtMpSchedulingModeDWRR     = 0x4  /**< Deficit Weighted Round Robin */
    }eAtMpSchedulingMode;

/**
 * @brief MLPPP bundle configuration
 */
typedef struct tAtMpBundleAllConfig
    {
    eAtMpSequenceMode  seqMd;        /**< Sequence mode */
    eAtMpWorkingMode   workingMd;    /**< Working mode */
    uint32             mrru;         /**< MRRU */
    uint8              maxLinkDelay; /**< Maximum link delay in millisecond */
    eAtFragmentSize    fragmentSize; /**< Fragmentation size */
    eBool              enable;       /**< Enable/disable */
    eAtHdlcPduType     pdu;          /**< Type PDU */
    }tAtMpBundleAllConfig;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Sequence */
eAtModulePppRet AtMpBundleSequenceModeSet(AtMpBundle self, eAtMpSequenceMode seqMd);
eAtMpSequenceMode AtMpBundleSequenceModeGet(AtMpBundle self);
eAtModulePppRet AtMpBundleRxSequenceModeSet(AtMpBundle self, eAtMpSequenceMode seqMd);
eAtModulePppRet AtMpBundleTxSequenceModeSet(AtMpBundle self, eAtMpSequenceMode seqMd);
eAtMpSequenceMode AtMpBundleTxSequenceModeGet(AtMpBundle self);
eAtMpSequenceMode AtMpBundleRxSequenceModeGet(AtMpBundle self);
eAtModulePppRet AtMpBundleSequenceNumberSet(AtMpBundle self, uint8 classNumber, uint32 sequenceNumber);

/* Working mode */
eAtModulePppRet AtMpBundleWorkingModeSet(AtMpBundle self, eAtMpWorkingMode workingMd);
eAtMpWorkingMode AtMpBundleWorkingModeGet(AtMpBundle self);

/* MRRU */
eAtModulePppRet AtMpBundleMrruSet(AtMpBundle self, uint32 mrru);
uint32 AtMpBundleMrruGet(AtMpBundle self);

/* Ethernet flow mapping */
eAtModulePppRet AtMpBundleFlowAdd(AtMpBundle self, AtEthFlow flow, uint8 classNumber);
uint8 AtMpBundleFlowClassNumberGet(AtMpBundle self, AtEthFlow flow);
eAtModulePppRet AtMpBundleFlowRemove(AtMpBundle self, AtEthFlow flow);
uint8 AtMpBundleNumberOfFlowsGet(AtMpBundle self);
uint8 AtMpBundleAllFlowsGet(AtMpBundle self, AtEthFlow * flows);

/* Flows Iterator */
AtIterator AtMpBundleFlowIteratorCreate(AtMpBundle self);

/* Scheduling mode */
eAtModulePppRet AtMpBundleSchedulingModeSet(AtMpBundle self, eAtMpSchedulingMode schedulingMode);
eAtMpSchedulingMode AtMpBundleSchedulingModeGet(AtMpBundle self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMPBUNDLE_H_ */

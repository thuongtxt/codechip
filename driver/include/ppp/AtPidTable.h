/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : AtPidTable.h
 * 
 * Created Date: Sep 20, 2013
 *
 * Description : Table to translate between PID values and Ethernet type
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPIDTABLE_H_
#define _ATPIDTABLE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtModule.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPidTable      * AtPidTable;
typedef struct tAtPidTableEntry * AtPidTableEntry;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Table APIs  */
AtModule AtPidTableModuleGet(AtPidTable self);
uint32 AtPidTableMaxNumEntries(AtPidTable self);
eAtRet AtPidTableInit(AtPidTable self);
AtPidTableEntry AtPidTableEntryGet(AtPidTable self, uint32 entryIndex);
eBool AtPidTableEntryIsEditable(AtPidTable self, uint32 entryIndex);

/* Entry APIs: there are two sets of APIs,
 * - 1: APIs to control two directions
 * - 2: APIs to control each direction separately. */

/* Set 1: To control two directions. */
eAtRet AtPidTableEntryPidSet(AtPidTableEntry self, uint32 pid);
eAtRet AtPidTableEntryEthTypeSet(AtPidTableEntry self, uint32 ethType);

/* Set 2.1: To control PSN to TDM direction */
eAtRet AtPidTableEntryPsnToTdmPidSet(AtPidTableEntry self, uint32 pid);
eAtRet AtPidTableEntryPsnToTdmEthTypeSet(AtPidTableEntry self, uint32 ethType);
eAtRet AtPidTableEntryPsnToTdmEnable(AtPidTableEntry self, eBool enable);
uint32 AtPidTableEntryPsnToTdmPidGet(AtPidTableEntry self);
uint32 AtPidTableEntryPsnToTdmEthTypeGet(AtPidTableEntry self);
eBool AtPidTableEntryPsnToTdmIsEnabled(AtPidTableEntry self);

/* Set 2.2: To control PSN to TDM direction */
eAtRet AtPidTableEntryTdmToPsnPidSet(AtPidTableEntry self, uint32 pid);
eAtRet AtPidTableEntryTdmToPsnEthTypeSet(AtPidTableEntry self, uint32 ethType);
eAtRet AtPidTableEntryTdmToPsnEnable(AtPidTableEntry self, eBool enable);
uint32 AtPidTableEntryTdmToPsnPidGet(AtPidTableEntry self);
uint32 AtPidTableEntryTdmToPsnEthTypeGet(AtPidTableEntry self);
eBool AtPidTableEntryTdmToPsnIsEnabled(AtPidTableEntry self);

AtPidTable AtPidTableEntryTableGet(AtPidTableEntry self);
uint32 AtPidTableEntryIndexGet(AtPidTableEntry self);

eAtRet AtPidTableEntryTdmToPsnPidMaskSet(AtPidTableEntry self, uint16 pidMask);
uint32 AtPidTableEntryTdmToPsnPidMaskGet(AtPidTableEntry self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPIDTABLE_H_ */


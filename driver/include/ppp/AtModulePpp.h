/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : AtModulePpp.h
 * 
 * Created Date: Aug 13, 2012
 *
 * Description : PPP module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPPP_H_
#define _ATMODULEPPP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleClasses.h"
#include "AtChannelClasses.h"
#include "AtIterator.h"
#include "AtPidTable.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePpp
 * @{
 */

/** @brief PPP module return code */
typedef uint32 eAtModulePppRet;

/** @brief MLPPP bundle */
typedef struct tAtMpBundle * AtMpBundle;

/**
 * @brief PPP link
 */
typedef struct tAtPppLink * AtPppLink;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtModulePppMaxLinksPerBundleGet(AtModulePpp self);

/* Following APIs are used for products that have independent MLPPP bundle ID space.
 * For products that have bundles shared between MLPPP and Frame relay,
 * AtModuleEncap will manage */
AtMpBundle AtModulePppMpBundleCreate(AtModulePpp self, uint32 bundleId);
AtMpBundle AtModulePppMpBundleGet(AtModulePpp self, uint32 bundleId);
uint32 AtModulePppFreeMpBundleGet(AtModulePpp self);
eAtModulePppRet AtModulePppMpBundleDelete(AtModulePpp self, uint32 bundleId);
uint32 AtModulePppMaxBundlesGet(AtModulePpp self);

/* MLPPP bundles Iterator */
AtIterator AtModulePppMpBundleIteratorCreate(AtModulePpp self);

/* Get PID table */
AtPidTable AtModulePppPidTableGet(AtModulePpp self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPPP_H_ */


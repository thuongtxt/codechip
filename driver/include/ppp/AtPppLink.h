/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : AtPppLink.h
 * 
 * Created Date: Aug 3, 2012
 *
 * Description : PPP Link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPPPLINK_H_
#define _ATPPPLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePpp.h"
#include "AtHdlcLink.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePpp
 * @{
 */

/**
 * @brief PPP Link counters
 */
typedef struct tAtPppLinkCounters
    {
    uint32 txByte;          /**< Transmit byte count */
    uint32 txPlain;         /**< Transmit plain PPP frames counter */
    uint32 txFragment;      /**< Transmit Multiclass-Multilink PPP fragments */    
    uint32 txOam;           /**< Transmit Control packet (LCP/NCP) */
    uint32 txGoodByte;      /**< Transmit good byte count */
    uint32 txGoodPkt;       /**< Transmit good packet count */
    uint32 txDiscardPkt;    /**< Transmit discard packet count */
    uint32 txDiscardByte;   /**< Transmit discard byte count */

    uint32 rxByte;          /**< Receive byte count */
    uint32 rxPlain;         /**< Receive plain PPP frames counter */
    uint32 rxFragment;      /**< Receive Multiclass-Multilink PPP fragments  */
    uint32 rxOam;           /**< Receive Control packet (LCP/NCP) */
    uint32 rxExceedMru;     /**< MRU exceeded counter */
    uint32 rxGoodByte;      /**< Receive good byte count */
    uint32 rxGoodPkt;       /**< Receive good packet  */
    uint32 rxDiscardPkt;    /**< Receive discard packet */
    uint32 rxDiscardByte;   /**< Receive discard byte count */
    }tAtPppLinkCounters;

/**
 * @brief PPP link phase
 */
typedef enum eAtPppLinkPhase
    {
    cAtPppLinkPhaseUnknown,       /**< Unknown phase */
    cAtPppLinkPhaseDead,          /**< Dead phase. No data packet nor control packet flows */
    cAtPppLinkPhaseEstablish,     /**< Go to establish phase. Only LCP control packets flow. No data packet */
    cAtPppLinkPhaseAuthenticate,  /**< Go to Authenticate phase. Only LCP, authentication
                                       protocol and link quality monitoring packets are allowed */
    cAtPppLinkPhaseEnterNetwork,  /**< Go to Network phase, only LCP/NCP control packets flow. No data packet */
    cAtPppLinkPhaseNetworkActive  /**< Still in Network phase and all of packets can flow */
    }eAtPppLinkPhase;
/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Link phase */
eAtModulePppRet AtPppLinkPhaseSet(AtPppLink self, eAtPppLinkPhase phase);
eAtPppLinkPhase AtPppLinkPhaseGet(AtPppLink self);
AtPidTable AtPppLinkPidTableGet(AtPppLink self);
eAtModulePppRet AtPppLinkTxProtocolFieldCompress(AtPppLink self, eBool compress);
eBool AtPppLinkTxProtocolFieldIsCompressed(AtPppLink self);

/* BCP */
eAtModulePppRet AtPppLinkBcpLanFcsInsertionEnable(AtPppLink self, eBool enable);
eBool AtPppLinkBcpLanFcsInsertionIsEnabled(AtPppLink self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPPPLINK_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : All module
 * 
 * File        : AtFailureProfiler.h
 * 
 * Created Date: Jul 18, 2017
 *
 * Description :  AtFailure profiler
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATFAILUREPROFILER_H_
#define _ATFAILUREPROFILER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtFailureProfiler * AtFailureProfiler;
typedef struct tAtProfilerDefect * AtProfilerDefect;
typedef struct tAtFailureProfilerEvent * AtFailureProfilerEvent;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Read-only attributes */
AtFailureProfiler AtChannelFailureProfilerGet(AtChannel self);
AtChannel AtFailureProfilerChannelGet(AtFailureProfiler self);

/* Start/stop profiling */
eAtRet AtFailureProfilerStart(AtFailureProfiler self);
eAtRet AtFailureProfilerStop(AtFailureProfiler self);

/* Access failure events */
void AtFailureProfilerLock(AtFailureProfiler self);
void AtFailureProfilerUnlock(AtFailureProfiler self);
eAtRet AtFailureProfilerFailuresClear(AtFailureProfiler self);
AtList AtFailureProfilerFailuresGet(AtFailureProfiler self);

/* Access failure event APIs */
uint32 AtFailureProfilerEventType(AtFailureProfilerEvent self);
tAtOsalCurTime *AtFailureProfilerEventReportedTime(AtFailureProfilerEvent self);
uint32 AtFailureProfilerEventStatus(AtFailureProfilerEvent self);
uint32 AtFailureProfilerEventProfiledTimeMs(AtFailureProfilerEvent self);
AtProfilerDefect AtFailureProfilerEventDefect(AtFailureProfilerEvent self);

/* Access defect information */
uint32 AtProfilerDefectType(AtProfilerDefect self);
tAtOsalCurTime *AtProfilerDefectClearTime(AtProfilerDefect self);
tAtOsalCurTime *AtProfilerDefectRaiseTime(AtProfilerDefect self);
uint32 AtProfilerDefectCurrentStatus(AtProfilerDefect self);
AtProfilerDefect AtFailureProfilerDefectInfo(AtFailureProfiler self, uint32 defectType);

/* For self-testing only */
eAtRet AtFailureProfilerDefectInject(AtFailureProfiler self, uint32 defects, uint32 currentStatus);
eAtRet AtFailureProfilerFailureInject(AtFailureProfiler self, uint32 failures, uint32 currentStatus);

#ifdef __cplusplus
}
#endif
#endif /* _ATFAILUREPROFILER_H_ */


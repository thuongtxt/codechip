/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhDe2.h
 * 
 * Created Date: Jun 12, 2014
 *
 * Description : DS2/E2
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHDE2_H_
#define _ATPDHDE2_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannel.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPdhDe2
 * @{
 */

/** @brief Frame types */
typedef enum eAtPdhDe2FrameType
    {
    cAtPdhDe2FrameUnknown ,       /**< Unknown frame */
    cAtPdhDs2T1_107Carrying4Ds1s, /**< DS2 T1.107 carrying 4 DS1s */
    cAtPdhDs2G_747Carrying3E1s,   /**< DS2 G.747 carrying 3 E1s */
    cAtPdhE2G_742Carrying4E1s     /**< E2 G.742 carrying 4 E1s */
    }eAtPdhDe2FrameType;

/**
 * @brief Alarm types
 */
typedef enum eAtPdhDe2AlarmType
    {
    cAtPdhDe2AlarmNone   = 0,      /**< No alarm */
    cAtPdhDe2AlarmLos    = cBit0,  /**< RAI */
    cAtPdhDe2AlarmLof    = cBit1,  /**< LOF */
    cAtPdhDe2AlarmAis    = cBit2,  /**< AIS */
    cAtPdhDe2AlarmRai    = cBit3,  /**< RAI */
    cAtPdhDe2AlarmAll    = cBit3_0 /**< All alarms */
    }eAtPdhDe2AlarmType;

/**
 * @brief DE2 Counter types
 */
typedef enum eAtPdhDe2CounterType
    {
    cAtPdhDe2CounterParity = cBit0,
    cAtPdhDe2CounterFbe = cBit1
    }eAtPdhDe2CounterType;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _ATPDHDE2_H_ */


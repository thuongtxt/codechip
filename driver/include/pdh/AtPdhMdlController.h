/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhMdlController.h
 * 
 * Created Date: May 30, 2015
 *
 * Description : MDL controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHMDLCONTROLLER_H_
#define _ATPDHMDLCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtModulePdh.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*
 * MDL EIC/LIC/FIC/UNIT/PORT_PFI_GEN String Size
 * */
#define cMdlEICStringSize                   10
#define cMdlLICStringSize                   11
#define cMdlFICStringSize                   10
#define cMdlUINTStringSize                  6
#define cMdlPortPfiGenStringSize            38

/* MDL message Type*/
#define cMdlMsgTypePath             0x38
#define cMdlMsgTypeIdleTest         0x34
#define cMdlMsgTestSignal           0x32

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPdhMdlController
 * @{
 */

/** @brief MDL elements */
typedef enum eAtPdhMdlDataElement
    {
    /* Common elements */
    cAtPdhMdlDataElementEic,  /**< Equipment identification code (up to 10 characters) */
    cAtPdhMdlDataElementLic,  /**< Location identification code (up to 11 characters) */
    cAtPdhMdlDataElementFic,  /**< Frame identification code (up to 10 characters) */
    cAtPdhMdlDataElementUnit, /**< Unit identification code (up to 6 characters) */

    /* Specific elements */
    cAtPdhMdlDataElementPfi,  /**< Facility identification code to send in the
                                   MDL message (up to 38 characters) */
    cAtPdhMdlDataElementIdleSignalPortNumber, /**< Equipment port, which initiates the idle
                                                   signal, send in the MDL idle signal message
                                                   (up to 38 characters) */
    cAtPdhMdlDataElementTestSignalGeneratorNumber /**< Generator number to send in the MDL test
                                                       signal message (up to 38 characters), */
    }eAtPdhMdlDataElement;

/**
 * @brief MDL controller type
 */
typedef enum eAtPdhMdlControllerType
    {
    cAtPdhMdlControllerTypeUnknown,     /**< Unknown controller type */
    cAtPdhMdlControllerTypePathMessage, /**< Path message MDL controller */
    cAtPdhMdlControllerTypeIdleSignal,  /**< IDLE signal MDL controller */
    cAtPdhMdlControllerTypeTestSignal   /**< Test signal MDL controller */
    }eAtPdhMdlControllerType;

/**
 * @brief MDL controller Counter type
 */
typedef enum eAtPdhMdlControllerCountersType
    {
    cAtPdhMdlControllerCounterTypeTxPkts,    /**< TX packets Counter */
    cAtPdhMdlControllerCounterTypeTxBytes,   /**< TX bytes Counters */
    cAtPdhMdlControllerCounterTypeRxPkts,    /**< RX packets Counter */
    cAtPdhMdlControllerCounterTypeRxBytes,   /**< RX bytes Counter */
    cAtPdhMdlControllerCounterTypeRxDropPkts /**< RX drop packets Counter */
    }eAtPdhMdlControllerCountersType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Read-only attributes */
eAtPdhMdlControllerType AtPdhMdlControllerTypeGet(AtPdhMdlController self);
AtPdhDe3 AtPdhMdlControllerDe3Get(AtPdhMdlController self);

/* Enabling */
eAtRet AtPdhMdlControllerEnable(AtPdhMdlController self, eBool enable);
eBool AtPdhMdlControllerIsEnabled(AtPdhMdlController self);

/* Transmit and Received */
eAtRet AtPdhMdlControllerSend(AtPdhMdlController self);
eBool AtPdhMdlControllerMessageReceived(AtPdhMdlController self);
uint32 AtPdhMdlControllerCountersGet(AtPdhMdlController self, uint32 counterType);
uint32 AtPdhMdlControllerCountersClear(AtPdhMdlController self, uint32 counterType);

/* Elements */
eAtRet AtPdhMdlControllerTxDataElementSet(AtPdhMdlController self, eAtPdhMdlDataElement element, const uint8 *data, uint16 length);
uint16 AtPdhMdlControllerTxDataElementGet(AtPdhMdlController self, eAtPdhMdlDataElement element, uint8 *data, uint16 bufferSize);
uint16 AtPdhMdlControllerRxDataElementGet(AtPdhMdlController self, eAtPdhMdlDataElement element, uint8 *data, uint16 bufferSize);

/* To query message at both directions */
uint16 AtPdhMdlControllerRxMesssageGet(AtPdhMdlController self, uint8 *data, uint16 bufferSize);
uint16 AtPdhMdlControllerTxMesssageGet(AtPdhMdlController self, uint8 *data, uint16 bufferSize);

/* CR bit */
eAtRet AtPdhMdlControllerTxCRBitSet(AtPdhMdlController self, uint8 value);
uint8  AtPdhMdlControllerTxCRBitGet(AtPdhMdlController self);
eAtRet AtPdhMdlControllerExpectedCRBitSet(AtPdhMdlController self, uint8 value);
uint8  AtPdhMdlControllerExpectedCRBitGet(AtPdhMdlController self);
uint8 AtPdhMdlControllerRxCRBitGet(AtPdhMdlController self);

/* Debug Functions */
void AtPdhMdlControllerDebug(AtPdhMdlController self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHMDLCONTROLLER_H_ */

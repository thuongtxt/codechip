/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtModulePdh.h
 * 
 * Created Date: Aug 3, 2012
 *
 * Description : PDH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPDH_H_
#define _ATMODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"   /* Super class */
#include "AtErrorGenerator.h"
#include "../att/AtAttPdhManager.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePdh
 * @{
 */

/** @brief PDH module class */
typedef struct tAtModulePdh * AtModulePdh;

/** @brief PDH channel */
typedef struct tAtPdhChannel * AtPdhChannel;

/** @brief DS1/E1 class */
typedef struct tAtPdhDe1 * AtPdhDe1;

/** @brief DS2/E2 class */
typedef struct tAtPdhDe2 * AtPdhDe2;

/** @brief DS3/E3 class */
typedef struct tAtPdhDe3 * AtPdhDe3;

/** @brief NxDS0 */
typedef struct tAtPdhNxDS0 * AtPdhNxDS0;

/** @brief PDH Serial Line abstract class */
typedef struct tAtPdhSerialLine * AtPdhSerialLine;

/** @brief PDH module return code */
typedef uint32 eAtModulePdhRet;

/** @brief MDL abstract controller */
typedef struct tAtPdhMdlController * AtPdhMdlController;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* DS1/E1 management */
uint32 AtModulePdhNumberOfDe1sGet(AtModulePdh self);
AtPdhDe1 AtModulePdhDe1Get(AtModulePdh self, uint32 de1Id);

/* DS3/E3 Serial Lines */
uint32 AtModulePdhNumberOfDe3SerialLinesGet(AtModulePdh self);
AtPdhSerialLine AtModulePdhDe3SerialLineGet(AtModulePdh self, uint32 serialLineId);

/* DS3/E3 management */
uint32 AtModulePdhNumberOfDe3sGet(AtModulePdh self);
AtPdhDe3 AtModulePdhDe3Get(AtModulePdh self, uint32 de3Id);

/* Output clock selector (deprecated by AtClockExtractor) */
eAtModulePdhRet AtModulePdhOutputClockSourceSet(AtModulePdh self, uint8 refOutId, AtPdhChannel source);
AtPdhChannel AtModulePdhOutputClockSourceGet(AtModulePdh self, uint8 refOutId);

/* PRM CR comparing at RX side */
eAtModulePdhRet AtModulePdhPrmCrCompareEnable(AtModulePdh self, eBool enable);
eBool AtModulePdhPrmCrCompareIsEnabled(AtModulePdh self);
/* ATT */
AtAttPdhManager AtModulePdhAttManager(AtModulePdh self);
#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPDH_H_ */


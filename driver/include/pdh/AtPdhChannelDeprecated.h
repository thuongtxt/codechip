/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhChannelDeprecated.h
 * 
 * Created Date: Nov 30, 2015
 *
 * Description : PDH channel deprecated interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHCHANNELDEPRECATED_H_
#define _ATPDHCHANNELDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerController AtPdhChannelBerControllerGet(AtPdhChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHCHANNELDEPRECATED_H_ */


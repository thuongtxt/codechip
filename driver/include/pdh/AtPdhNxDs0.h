/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhNxDs0.h
 * 
 * Created Date: Aug 3, 2012
 *
 * Description : NxDS0 class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHNXDS0_H_
#define _ATPDHNXDS0_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannel.h"
#include "AtModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtPdhNxDS0BitmapGet(AtPdhNxDS0 self);
AtPdhDe1 AtPdhNxDS0De1Get(AtPdhNxDS0 self);
uint8 AtPdhNxDs0NumTimeslotsGet(AtPdhNxDS0 self);
eAtRet AtPdhNxDs0CasIdleCodeSet(AtPdhNxDS0 self, uint8 abcd);
uint8 AtPdhNxDs0CasIdleCodeGet(AtPdhNxDS0 self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHNXDS0_H_ */


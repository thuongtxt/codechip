/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhDe1Prm.h
 * 
 * Created Date: Oct 8, 2015
 *
 * Description : PDH DS1 E1 PRM
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHDE1PRM_H_
#define _ATPDHDE1PRM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPdhDe1
 * @{
 */

/**
 * @brief PRM Standard
 */
typedef enum eAtPdhDe1PrmStandard
    {
    cAtPdhDe1PrmStandardUnknown, /**< Unknown standard */
    cAtPdhDe1PrmStandardAnsi,    /**< ANSI standard */
    cAtPdhDe1PrmStandardAtt      /**< AT&T standard */
    }eAtPdhDe1PrmStandard;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* PRM standard */
eAtModulePdhRet AtPdhDe1PrmTxStandardSet(AtPdhDe1 self, eAtPdhDe1PrmStandard standard);
eAtPdhDe1PrmStandard AtPdhDe1PrmTxStandardGet(AtPdhDe1 self);
eAtModulePdhRet AtPdhDe1PrmRxStandardSet(AtPdhDe1 self, eAtPdhDe1PrmStandard standard);
uint32 AtPdhDe1PrmRxStandardGet(AtPdhDe1 self);
eAtModulePdhRet AtPdhDe1PrmStandardSet(AtPdhDe1 self, eAtPdhDe1PrmStandard standard);
eAtPdhDe1PrmStandard AtPdhDe1PrmStandardGet(AtPdhDe1 self);

/* PRM enabling */
eAtModulePdhRet AtPdhDe1PrmEnable(AtPdhDe1 self, eBool enable);
eBool AtPdhDe1PrmIsEnabled(AtPdhDe1 self);

/* PRM CR bit */
eAtModulePdhRet AtPdhDe1PrmTxCRBitSet(AtPdhDe1 self, uint8 value);
uint8 AtPdhDe1PrmTxCRBitGet(AtPdhDe1 self);
eAtModulePdhRet AtPdhDe1PrmExpectedCRBitSet(AtPdhDe1 self, uint8 value);
uint8 AtPdhDe1PrmExpectedCRBitGet(AtPdhDe1 self);

/* PRM LB bit */
eAtModulePdhRet AtPdhDe1PrmTxLBBitSet(AtPdhDe1 self, uint8 value);
uint8 AtPdhDe1PrmTxLBBitGet(AtPdhDe1 self);
uint8 AtPdhDe1PrmRxLBBitGet(AtPdhDe1 self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHDE1PRM_H_ */


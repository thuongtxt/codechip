/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhDe3.h
 * 
 * Created Date: Sep 19, 2012
 *
 * Description : DS3/E3
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHDE3_H_
#define _ATPDHDE3_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannel.h"
#include "AtPdhMdlController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cAtPdhChannelMaxTtiLength 16

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPdhDe3
 * @{
 */

/** @brief Frame types */
typedef enum eAtPdhDe3FrameType
    {
    cAtPdhDe3FrameUnknown ,     /**< Unknown frame */

    /* DS3 framing types */
    cAtPdhDs3Unfrm,             /**< DS3 Unframed */
    cAtPdhDs3FrmCbitUnChn,      /**< T1.107 DS3 Cbit-Parity mapping data-only*/
    cAtPdhDs3FrmCbitChnl28Ds1s, /**< T1.107 DS3 Cbit-Parity channelize 28 DS1 frames */
    cAtPdhDs3FrmCbitChnl21E1s,  /**< G752 E32 + G747 E12: E32(44M) carry 21 E1 (DS3 Cbit-Parity channelize 21 E1 frames) */
    cAtPdhDs3FrmM13Chnl28Ds1s,  /**< T1.107 DS3 M23 + M12 carry 28 DS1 (DS3 M13 channelize 28 DS1 frames) */
    cAtPdhDs3FrmM13Chnl21E1s,   /**< G752 E32 + G747 E12 (DS3 M13 channelize 21 E1 frames) */

    /* E3 framing types */
    cAtPdhE3Unfrm,              /**< E3 Unframed Type */
    cAtPdhE3Frmg832,            /**< E3-G832 frame */
    cAtPdhE3FrmG751,            /**< E3 G.751 */
    cAtPdhE3FrmG751Chnl16E1s,   /**< G751 E31 + G742 E22: E31(34M) carry 16 E1 (E3 G.751 channelize 16 E1 frames) */

    /* Deprecated */
    cAtPdhDs3FrmM23             /**< DS3 M23. Use cAtPdhDs3FrmCbitChnl28Ds1s */
    }eAtPdhDe3FrameType;

/**
 * @brief Alarm types
 */
typedef enum eAtPdhDe3AlarmType
    {
    /* Common alarms */
    cAtPdhDe3AlarmNone              = 0,     /**< No alarm */
    cAtPdhDe3AlarmLos               = cBit0, /**< LOS */
    cAtPdhDe3AlarmLof               = cBit1, /**< LOF */
    cAtPdhDe3AlarmAis               = cBit2, /**< AIS */
    cAtPdhDe3AlarmRai               = cBit3, /**< RAI */
    cAtPdhDe3AlarmSfBer             = cBit4, /**< SF BER */
    cAtPdhDe3AlarmSdBer             = cBit5, /**< SD BER */

    /* DS3 alarms */
    cAtPdhDs3AlarmIdle              = cBit6, /**< Idle */
    cAtPdhDs3AlarmAicChange         = cBit7, /**< AIC change */
    cAtPdhDs3AlarmFeacChange        = cBit8, /**< FEAC change */

    /* E3 alarms */
    cAtPdhE3AlarmTim                = cBit9,  /**< TIM */
    cAtPdhE3AlarmPldTypeChange      = cBit10, /**< Payload Type change */
    cAtPdhE3AlarmTmChange           = cBit11, /**< Timing Maker change */
    cAtPdhDe3AlarmSsmChange         = cBit12, /**< SSM change, either E3 G.832 byte MA[b8] or
                                                   DS3 C-bit SSM BOM-like on UDL bits. */

    cAtPdhDs3AlarmMdlChange         = cBit13, /**< MDL change (DS3 C-bit only) */
    cAtPdhDe3AlarmBerTca            = cBit14, /**< BER TCA */
    cAtPdhDe3AlarmClockStateChange  = cBit15, /**< DS3/E3 CDR clock state change. */

    /* All masks */
    cAtPdhDs3AlarmAll = cAtPdhDe3AlarmLos | cAtPdhDe3AlarmLof |
                        cAtPdhDe3AlarmAis | cAtPdhDe3AlarmRai |
                        cAtPdhDe3AlarmSfBer | cAtPdhDe3AlarmSdBer |
                        cAtPdhDs3AlarmIdle | cAtPdhDs3AlarmAicChange |
                        cAtPdhDs3AlarmFeacChange, /**< All DS3 alarm masks */
    cAtPdhE3AlarmAll = cAtPdhDe3AlarmLos | cAtPdhDe3AlarmLof |
                       cAtPdhDe3AlarmAis | cAtPdhDe3AlarmRai |
                       cAtPdhDe3AlarmSfBer | cAtPdhDe3AlarmSdBer |
                       cAtPdhE3AlarmTim | cAtPdhE3AlarmPldTypeChange |
                       cAtPdhE3AlarmTmChange | cAtPdhDe3AlarmSsmChange /**< All DS3 alarm masks */
    }eAtPdhDe3AlarmType;

/** @brief Error types */
typedef enum eAtPdhDe3ErrorType
    {
    cAtPdhDe3ErrorNone  = 0,        /**< No error */
    cAtPdhDe3ErrorBip8  = cBit0,    /**< BIP-8 error (E3 G.832) */
    cAtPdhDe3ErrorRei   = cBit1,    /**< REI/FEBE error */
    cAtPdhDe3ErrorFbit  = cBit2,    /**< Framing bit error */
    cAtPdhDe3ErrorPbit  = cBit3,    /**< Parity bit error (DS3 M13/C-bit) */
    cAtPdhDe3ErrorCPbit = cBit4,    /**< C-bit parity bit error (DS3 C-bit) */
    cAtPdhDe3ErrorAll   = cBit2_0   /**< All errors */
    }eAtPdhDe3ErrorType;

/** @brief Counter types */
typedef enum eAtPdhDe3CounterType
    {
    cAtPdhDe3CounterBip8   = cBit0, /**< BIP-8 */
    cAtPdhDe3CounterRei    = cBit1, /**< REI/FEBE */
    cAtPdhDe3CounterFBit   = cBit2, /**< F-bit */
    cAtPdhDe3CounterPBit   = cBit3, /**< P-bit */
    cAtPdhDe3CounterCPBit  = cBit4, /**< CP-bit */
    cAtPdhDe3CounterBpvExz = cBit5, /**< BPV EXZ counter */
    cAtPdhDe3CounterTxCs            /**< TX Controlled slip counter */
    }eAtPdhDe3CounterType;

/** @brief TTI mode */
typedef enum eAtPdhTtiMode
    {
    cAtPdhTtiMode1Byte,  /**< 1-byte */
    cAtPdhTtiMode16Bytes /**< 16-bytes */
    }eAtPdhTtiMode;

/** @brief PDH TTI */
typedef struct tAtPdhTti
    {
    eAtPdhTtiMode mode;                       /**< Message mode */
    uint8 message[cAtPdhChannelMaxTtiLength]; /**< Message content */
    }tAtPdhTti;

/** @brief DS3 Cbit FEAC type */
typedef enum eAtPdhDe3FeacType
    {
    cAtPdhDe3FeacTypeUnknown, /**< Unknown type or Stop sending alarm */
    cAtPdhDe3FeacTypeAlarm,   /**< Command for device to send/notify alarm FEAC code.
                                   Device will process the alarm code parameter
                                   from the accompanied API. The alarm code is 6 (x) bit
                                   Alarm/status code in the pattern 0xxxxxx011111111, refer to
                                   figure 22, T1.107 */
    cAtPdhDe3FeacTypeLoopActive, /**< Command for device to send/notify the loop
                                      active signal with Line ID code.
                                      Device will process the Line ID code
                                      parameter from the accompanied API.
                                      The Loop Line ID code is 6 (x) bit command code
                                      in the pattern 0xxxxxx011111111, refer to figure 23, T1.107 */
    cAtPdhDe3FeacTypeLoopDeactive, /**< Command for device to send/notify the
                                       loop de-active signal with Line ID code.
                                       Device will process the Line ID code parameter
                                       from the accompanied API. The Loop Line ID
                                       code is 6 (x) bit command code in the
                                       pattern 0xxxxxx011111111, refer to figure 23, T1.107  */
    cAtPdhDe3FeacTypeHistoryAlarm,       /**< Rx State History is Alarm codeword but current state id IDLE codeword */
    cAtPdhDe3FeacTypeHistoryLoopActive,  /**< Rx State History is Loopactive codeword but current state id IDLE codeword */
    cAtPdhDe3FeacTypeHistoryLoopDeactive /**< Rx State History is Loopdeactive codeword but current state id IDLE codeword */
    } eAtPdhDe3FeacType;

/**
 * @brief MDL Standard
 */
typedef enum eAtPdhMdlStandard
    {
    cAtPdhMdlStandardAnsi,        /**< ANSI standard */
    cAtPdhMdlStandardAtt,         /**< AT&T standard */
    cAtPdhMdlStandardUnknown      /**< Unknown standard */
    }eAtPdhMdlStandard;

/**
 * @brief DS3 DataLink Option
 */
typedef enum eAtPdhDe3DataLinkOption
    {
    cAtPdhDe3DataLinkOptionUnknown, /**< Unknown Data-Link Option */
    cAtPdhDe3DataLinkOptionDlBits,  /**< Data-Link on DL bits for DS3 Cbit */
    cAtPdhDe3DataLinkOptionUdlBits  /**< Data-Link on UDL bits for DS3 Cbit */
    }eAtPdhDe3DataLinkOption;

/**
 * @brief All counters for DS3/E3
 */
typedef struct tAtPdhDe3Counters
    {
    uint32 bip8Error;           /**< Number of BIP-8 error */
    uint32 rei;                 /**< Number of REI/FEBE */
    uint32 fBitError;           /**< Number of framing bit error */
    uint32 pBitError;           /**< Number of parity error */
    uint32 cBitParityError;     /**< Number of C-bit parity error */
    uint32 bpvExz;              /**< Number of BPV EXZ */
    uint32 txCs;                /**< Number of TX Controlled slip counter */
    }tAtPdhDe3Counters;

typedef enum eAtPdhDe3StuffMode
    {
    cAtPdhDe3StuffModeFineStuff, /* default la fine_stuff */
    cAtPdhDe3StuffModeFullStuff,
    }eAtPdhDe3StuffMode;
/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* DS3 IDLE control */
eAtModulePdhRet AtPdhDe3TxIdleEnable(AtPdhDe3 self, eBool enable);
eBool AtPdhDe3TxIdleIsEnabled(AtPdhDe3 self);

/* DS3 FEAC */
eAtModulePdhRet AtPdhDe3TxFeacSet(AtPdhDe3 self, eAtPdhDe3FeacType signalType, uint8 xcode);
eAtModulePdhRet AtPdhDe3TxFeacGet(AtPdhDe3 self, eAtPdhDe3FeacType *signalType, uint8 *xcode);
eAtModulePdhRet AtPdhDe3RxFeacGet(AtPdhDe3 self, eAtPdhDe3FeacType *signalType, uint8 *xcode);
eAtModulePdhRet AtPdhDe3TxFeacWithModeSet(AtPdhDe3 self, eAtPdhDe3FeacType signalType, uint8 xcode, eAtPdhBomSentMode sentMode);
eAtPdhBomSentMode AtPdhDe3TxFeacModeGet(AtPdhDe3 self);

/* DS3 AIC */
uint8 AtPdhDe3RxAicGet(AtPdhDe3 self);

/* E3 TTI */
eAtModulePdhRet AtPdhDe3TxTtiSet(AtPdhDe3 self, const tAtPdhTti *tti);
eAtModulePdhRet AtPdhDe3TxTtiGet(AtPdhDe3 self, tAtPdhTti *tti);
eAtModulePdhRet AtPdhDe3ExpectedTtiSet(AtPdhDe3 self, const tAtPdhTti *tti);
eAtModulePdhRet AtPdhDe3ExpectedTtiGet(AtPdhDe3 self, tAtPdhTti *tti);
eAtModulePdhRet AtPdhDe3RxTtiGet(AtPdhDe3 self, tAtPdhTti *tti);

/* E3 Timing Maker */
eAtModulePdhRet AtPdhDe3TmEnable(AtPdhDe3 self, eBool enable);
eBool AtPdhDe3TmIsEnabled(AtPdhDe3 self);
eAtModulePdhRet AtPdhDe3TxTmCodeSet(AtPdhDe3 self, uint8 tmCode);
uint8 AtPdhDe3TxTmCodeGet(AtPdhDe3 self);
uint8 AtPdhDe3RxTmCodeGet(AtPdhDe3 self);

/* E3/T3 SSM */
eAtModulePdhRet AtPdhDe3TxSsmSet(AtPdhDe3 self, uint8 ssm);
uint8 AtPdhDe3TxSsmGet(AtPdhDe3 self);
uint8 AtPdhDe3RxSsmGet(AtPdhDe3 self);

/* UDL bit ordering */
eAtModulePdhRet AtPdhDe3TxUdlBitOrderSet(AtPdhDe3 self, eAtBitOrder bitOrder);
eAtBitOrder AtPdhDe3TxUdlBitOrderGet(AtPdhDe3 self);
eAtModulePdhRet AtPdhDe3RxUdlBitOrderSet(AtPdhDe3 self, eAtBitOrder bitOrder);
eAtBitOrder AtPdhDe3RxUdlBitOrderGet(AtPdhDe3 self);

/* Payload type */
eAtModulePdhRet AtPdhDe3TxPldTypeSet(AtPdhDe3 self, uint8 pldType);
uint8 AtPdhDe3TxPldTypeGet(AtPdhDe3 self);
uint8 AtPdhDe3RxPldTypeGet(AtPdhDe3 self);

/* Access the channelized DS1s/E1s */
AtPdhDe2 AtPdhDe3De2Get(AtPdhDe3 self, uint8 de2Id);
AtPdhDe1 AtPdhDe3De1Get(AtPdhDe3 self, uint8 de2Id, uint8 de1Id);

/* MDL controllers */
AtPdhMdlController AtPdhDe3PathMessageMdlController(AtPdhDe3 self);
AtPdhMdlController AtPdhDe3TestSignalMdlController(AtPdhDe3 self);
AtPdhMdlController AtPdhDe3IdleSignalMdlController(AtPdhDe3 self);
AtPdhMdlController AtPdhDe3RxCurrentMdlControllerGet(AtPdhDe3 de3);

/* Auxiliary functions */
eBool AtPdhDe3IsDs3(AtPdhDe3 self);
eBool AtPdhDe3IsE3(AtPdhDe3 self);
eBool AtPdhDe3IsChannelized(AtPdhDe3 self);
eBool AtPdhDe3IsE1Channelized(AtPdhDe3 self);
eBool AtPdhDe3IsDs1Channelized(AtPdhDe3 self);
eBool AtPdhDe3IsE1ChannelizedFrameType(uint16 de3FrameType);
eBool AtPdhDe3IsChannelizedFrameType(uint16 de3FrameType);
eBool AtPdhDe3IsChannelizedToDs1(uint16 de3FrameType);
eBool AtPdhDe3FrameTypeIsE3(eAtPdhDe3FrameType frameType);
eBool AtPdhDe3IsDs3CbitFrameType(uint32 frameType);
eBool AtPdhDe3FrameTypeIsUnframed(uint32 frameType);

/* AT&T or ANSI */
eAtRet AtPdhDe3MdlStandardSet(AtPdhDe3 self, eAtPdhMdlStandard standard);
uint32 AtPdhDe3MdlStandardGet(AtPdhDe3 self);

/* Data-Link Options */
eAtRet AtPdhDe3DataLinkOptionSet(AtPdhDe3 self, eAtPdhDe3DataLinkOption option);
eAtPdhDe3DataLinkOption AtPdhDe3DataLinkOptionGet(AtPdhDe3 self);
eAtRet AtPdhDe3StuffModeSet(AtPdhDe3 self, eAtPdhDe3StuffMode mode);
eAtPdhDe3StuffMode AtPdhDe3StuffModeGet(AtPdhDe3 self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHDE3_H_ */

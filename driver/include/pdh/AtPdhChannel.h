/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhChannel.h
 * 
 * Created Date: Aug 3, 2012
 *
 * Description : PDH channel class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHCHANNEL_H_
#define _ATPDHCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h" /* Super class */
#include "AtModulePdh.h"
#include "AtBerController.h"
#include "AtSdhVc.h"
#include "AtErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPdhChannel
 * @{
 */
/**
 * @brief Channel type
 */
typedef enum eAtPdhChannelType
    {
    cAtPdhChannelTypeUnknown,    /**< Unknown */
    cAtPdhChannelTypeDe3Unknown, /**< DE3 Unknown */
    cAtPdhChannelTypeDe2Unknown, /**< DE2 Unknown */
    cAtPdhChannelTypeDe1Unknown, /**< DE1 Unknown */
    
    cAtPdhChannelTypeSerLine,    /** Serial line */
    cAtPdhChannelTypeE3,         /**< E3 */
    cAtPdhChannelTypeDs3,        /**< DS3 */

    cAtPdhChannelTypeE2,         /**< E2 */
    cAtPdhChannelTypeDs2,        /**< DS2 */

    cAtPdhChannelTypeE1,         /**< E1 */
    cAtPdhChannelTypeDs1,        /**< DS1 */
 
    cAtPdhChannelTypeNxDs0       /**< NxDs0 */
    }eAtPdhChannelType;

/** @brief PDH loopback modes */
typedef enum eAtPdhLoopbackMode
    {
    cAtPdhLoopbackModeRelease,        /**< Release loopback */

    cAtPdhLoopbackModeLocalPayload,   /**< Payload Local loopback */
    cAtPdhLoopbackModeRemotePayload,  /**< Payload Remote loopback */

    cAtPdhLoopbackModeLocalLine,      /**< Line Local loopback */
    cAtPdhLoopbackModeRemoteLine      /**< Line Local loopback */
    }eAtPdhLoopbackMode;

/** @brief PDH Data-Link Counter types */
typedef enum eAtPdhDataLinkCounterType
    {
    cAtPdhDataLinkCounterTypeTxPkt,      /**< Tx Packets counter */
    cAtPdhDataLinkCounterTypeTxByte,     /**< Tx Bytes counter */
    cAtPdhDataLinkCounterTypeTxAbortPkt, /**< Tx Abort Packets counter */
    cAtPdhDataLinkCounterTypeRxPkt,      /**< Rx Packets counter */
    cAtPdhDataLinkCounterTypeRxByte,     /**< Rx Bytes counter */
    cAtPdhDataLinkCounterTypeRxAbortPkt, /**< Rx Abort Packets counter */
    cAtPdhDataLinkCounterTypeRxFcsPkt,   /**< Rx Fcs Packets counter */
    cAtPdhDataLinkCounterTypeRxDropPkts, /**< Rx Drop Packets counter */
    cAtPdhDataLinkCounterTypeRxLenVio    /**< Rx Packets has length violation counter */
    }eAtPdhDataLinkCounterType;

/** @brief Error generator mode */
typedef enum eAtPdhErrorGeneratorErrorType
    {
    cAtPdhErrorGeneratorErrorTypeInvalid,       /**< Invalid mode */
    cAtPdhErrorGeneratorErrorTypeFullBitStream, /**< Full bit stream error */
    cAtPdhErrorGeneratorErrorTypePayload,       /**< Payload error only */
    cAtPdhErrorGeneratorErrorTypeFbit           /**< Fbit error only */
    }eAtPdhErrorGeneratorErrorType;

/** @brief DS1 ESF BOM or DS3 CBIT FEAC sent mode */
typedef enum eAtPdhBomSentMode
    {
    cAtPdhBomSentModeInvalid,   /**< To stop sending BOM */
    cAtPdhBomSentModeOneShot,   /**< To transmit 10 consecutive bit-oriented message */
    cAtPdhBomSentModeContinuous /**< To transmit bit-oriented message continuously
                                     until the invalid mode is inputed */
    }eAtPdhBomSentMode;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Channel type */
eAtPdhChannelType AtPdhChannelTypeGet(AtPdhChannel self);

/* Frame type */
eBool AtPdhChannelFrameTypeIsSupported(AtPdhChannel self, uint16 frameType);
eAtModulePdhRet AtPdhChannelFrameTypeSet(AtPdhChannel self, uint16 frameType);
uint16 AtPdhChannelFrameTypeGet(AtPdhChannel self);

/* Line code */
eAtModulePdhRet AtPdhChannelLineCodeSet(AtPdhChannel self, uint16 lineCode);
uint16 AtPdhChannelLineCodeGet(AtPdhChannel self);

/* LED controlling */
eAtModulePdhRet AtPdhChannelLedStateSet(AtPdhChannel self, eAtLedState ledState);
eAtLedState AtPdhChannelLedStateGet(AtPdhChannel self);

/* BER monitoring */
AtBerController AtPdhChannelPathBerControllerGet(AtPdhChannel self);
AtBerController AtPdhChannelLineBerControllerGet(AtPdhChannel self);

/* Data Link */
eAtModulePdhRet AtPdhChannelDataLinkSend(AtPdhChannel self, const uint8 *messageBuffer, uint32 length);
uint32 AtPdhChannelDataLinkReceive(AtPdhChannel self, uint8 *messageBuffer, uint32 bufferLength);
eAtModulePdhRet AtPdhChannelDataLinkEnable(AtPdhChannel self, eBool enable);
eBool AtPdhChannelDataLinkIsEnabled(AtPdhChannel self);
uint32 AtPdhChannelDataLinkCounterGet(AtPdhChannel self, uint32 counterType);
uint32 AtPdhChannelDataLinkCounterClear(AtPdhChannel self, uint32 counterType);
eAtRet  AtPdhChannelDataLinkFcsBitOrderSet(AtPdhChannel self, eAtBitOrder bitOrder);
eAtBitOrder AtPdhChannelDataLinkFcsBitOrderGet(AtPdhChannel self);

/* National bits. Applicable for E1 and E3 */
eAtModulePdhRet AtPdhChannelNationalBitsSet(AtPdhChannel self, uint8 pattern);
uint8 AtPdhChannelNationalBitsGet(AtPdhChannel self);
uint8 AtPdhChannelRxNationalBitsGet(AtPdhChannel self);
eBool AtPdhChannelNationalBitsSupported(AtPdhChannel self);

/* Access sub channels */
uint8 AtPdhChannelNumberOfSubChannelsGet(AtPdhChannel self);
AtPdhChannel AtPdhChannelSubChannelGet(AtPdhChannel self, uint8 subChannelId);
AtPdhChannel AtPdhChannelParentChannelGet(AtPdhChannel self);

/* To get the SDH VC that this channel is mapped to */
AtSdhVc AtPdhChannelVcGet(AtPdhChannel self);

/* Alarm affecting option */
eAtModulePdhRet AtPdhChannelAlarmAffectingEnable(AtPdhChannel self, uint32 alarmType, eBool enable);
eBool AtPdhChannelAlarmAffectingIsEnabled(AtPdhChannel self, uint32 alarmType);

/* To control auto backward RAI when critical alarms happen */
eAtRet AtPdhChannelAutoRaiEnable(AtPdhChannel sel, eBool enable);
eBool AtPdhChannelAutoRaiIsEnabled(AtPdhChannel self);

/* Idle */
eBool AtPdhChannelIdleSignalIsSupported(AtPdhChannel self);
eAtModulePdhRet AtPdhChannelIdleEnable(AtPdhChannel self, eBool enable);
eBool AtPdhChannelIdleIsEnabled(AtPdhChannel self);
eAtModulePdhRet AtPdhChannelIdleCodeSet(AtPdhChannel self, uint8 idleCode);
uint8 AtPdhChannelIdleCodeGet(AtPdhChannel self);

/* Prbs engine */
AtPrbsEngine AtPdhChannelLinePrbsEngineGet(AtPdhChannel self);

/* Retime/Slip buffer */
eAtRet AtPdhChannelTxSlipBufferEnable(AtPdhChannel self, eBool enable);
eBool AtPdhChannelTxSlipBufferIsEnabled(AtPdhChannel self);

eBool AtPdhChannelRxSlipBufferIsSupported(AtPdhChannel self);
eAtRet AtPdhChannelRxSlipBufferEnable(AtPdhChannel self, eBool enable);
eBool AtPdhChannelRxSlipBufferIsEnabled(AtPdhChannel self);

/* SSM */
uint8 AtPdhChannelRxSsmGet(AtPdhChannel self);
eAtModulePdhRet AtPdhChannelTxSsmSet(AtPdhChannel self, uint8 value);
uint8 AtPdhChannelTxSsmGet(AtPdhChannel self);
eAtModulePdhRet AtPdhChannelSsmEnable(AtPdhChannel self, eBool enable);
eBool AtPdhChannelSsmIsEnabled(AtPdhChannel self);

/* LOS detection */
eAtRet AtPdhChannelLosDetectionEnable(AtPdhChannel self, eBool enable);
eBool AtPdhChannelLosDetectionIsEnabled(AtPdhChannel self);

/* Error Generator*/
AtErrorGenerator AtPdhChannelTxErrorGeneratorGet(AtPdhChannel self);
AtErrorGenerator AtPdhChannelRxErrorGeneratorGet(AtPdhChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHCHANNEL_H_ */

#include "AtPdhChannelDeprecated.h"

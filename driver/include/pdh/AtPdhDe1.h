/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhDe1.h
 * 
 * Created Date: Aug 3, 2012
 *
 * Description : DS1/E1
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHDE1_H_
#define _ATPDHDE1_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhChannel.h" /* Super class */
#include "AtModulePdh.h"
#include "AtIterator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cAtPdhDe1FdlIdleFlag 0x7E

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPdhDe1
 * @{
 */

/** @brief Frame types */
typedef enum eAtPdhDe1FrameType
    {
    cAtPdhDe1FrameUnknown , /**< Unknown frame */

    cAtPdhDs1J1UnFrm      , /**< DS1/J1 Unframe */
    cAtPdhDs1FrmSf        , /**< DS1 SF (D4) */
    cAtPdhDs1FrmEsf       , /**< DS1 ESF */
    cAtPdhDs1FrmDDS       , /**< DS1 DDS */
    cAtPdhDs1FrmSLC       , /**< DS1 SLC */

    cAtPdhJ1FrmSf         , /**< J1 SF */
    cAtPdhJ1FrmEsf        , /**< J1 ESF */

    cAtPdhE1UnFrm         , /**< E1 unframe */
    cAtPdhE1Frm           , /**< E1 basic frame, FAS/NFAS framing */
    cAtPdhE1MFCrc           /**< E1 Multi-Frame with CRC4 */
    }eAtPdhDe1FrameType;

/** @brief Line codes */
typedef enum eAtPdhDe1LineCode
    {
    cAtPdhDe1LineCodeUnknown , /**< NRZ code */
    cAtPdhDe1LineCodeNrz     , /**< NRZ code */
    cAtPdhDe1LineCodeAmi     , /**< AMI code */
    cAtPdhDe1LineCodeHdb3    , /**< HDB3 code */
    cAtPdhDe1LineCodeB3zs    , /**< B3ZS code */
    cAtPdhDe1LineCodeB8zs      /**< B8ZS code */
    }eAtPdhDe1LineCode;

/** @brief Error types */
typedef enum eAtPdhDe1ErrorType
    {
    cAtPdhDe1ErrorNone  = 0,        /**< No error */
    cAtPdhDe1ErrorCrc   = cBit0,    /**< CRC error */
    cAtPdhDe1ErrorFe    = cBit1,    /**< Framing bit error */
    cAtPdhDe1ErrorRei   = cBit2,    /**< REI error */
    cAtPdhDe1ErrorAll   = cBit2_0   /**< All errors */
    }eAtPdhDe1ErrorType;

/** @brief Counter types */
typedef enum eAtPdhDe1CounterType
    {
    cAtPdhDe1CounterCrc    = cBit0, /**< CRC error counter */
    cAtPdhDe1CounterFe     = cBit1, /**< Framing bit error counter */
    cAtPdhDe1CounterRei    = cBit2, /**< REI counter */
    cAtPdhDe1CounterBpvExz = cBit3, /**< BPV EXZ counter */
    cAtPdhDe1CounterTxCs,           /**< PSN to TDM controlled Slip counter */
    cAtPdhDe1CounterRxCs            /**< TDM to PSN controlled Slip counter */
    }eAtPdhDe1CounterType;

/**
 * @brief Alarm types
 */
typedef enum eAtPdhDe1AlarmType
    {
    cAtPdhDe1AlarmNone                  = 0,       /**< No alarm */
    cAtPdhDe1AlarmLos                   = cBit0,   /**< LOS */
    cAtPdhDe1AlarmLof                   = cBit1,   /**< LOF */
    cAtPdhDe1AlarmAis                   = cBit2,   /**< AIS */
    cAtPdhDe1AlarmRai                   = cBit3,   /**< RAI */
    cAtPdhDe1AlarmLomf                  = cBit4,   /**< LOMF (E1 CRC-4 only) */
    cAtPdhDe1AlarmSfBer                 = cBit5,   /**< SF BER */
    cAtPdhDe1AlarmSdBer                 = cBit6,   /**< SD BER */
    cAtPdhDe1AlarmSigLof                = cBit7,   /**< LOF on signaling channel (E1 CRC-4 only) */
    cAtPdhDe1AlarmSigRai                = cBit8,   /**< RAI on signaling channel (E1 CRC-4 only) */
    cAtPdhDe1AlarmEbit                  = cBit9,   /**< Ebit (E1 CRC-4 only) */
    cAtPdhDs1AlarmBomChange             = cBit10,  /**< DS1 ESF Bit Oriented Message change */
    cAtPdhDs1AlarmLapdChange            = cBit11,  /**< DS1 ESF A new LAPD message */
    cAtPdhDs1AlarmInbandLoopCodeChange  = cBit12,  /**< DS1 Inband Loopcode change */
    cAtPdhDe1AlarmAisCi                 = cBit14,  /**< AIS CI */
    cAtPdhDe1AlarmRaiCi                 = cBit15,  /**< RDI CI */
    cAtPdhDs1AlarmPrmLBBitChange        = cBit16,  /**< DS1 ESF PRM LB bit change */
    cAtPdhE1AlarmSsmChange              = cBit17,  /**< E1 CRC SSM change */
    cAtPdhDe1AlarmBerTca                = cBit18,  /**< DS1/E1 CRC BER TCA */
    cAtPdhDe1AlarmClockStateChange      = cBit19,  /**< DS1/E1 CDR clock state change. */
	cAtPdhDe1AlarmAll                   = cBit12_0 | cBit19_14 /**< All alarms */
    }eAtPdhDe1AlarmType;

/**
 * @brief Inband loop code, applicable only for DS1 SF and ESF
 */
typedef enum eAtPdhDe1Loopcode
    {
    cAtPdhDe1LoopcodeInvalid,                /**< Invalid Loop code */
    cAtPdhDe1LoopcodeLineActivate,           /**< Line loop activate */
    cAtPdhDe1LoopcodeLineDeactivate,         /**< Line loop de-activate */
    cAtPdhDe1LoopcodeFacility1Activate,      /**< Facility-1 loop activate */
    cAtPdhDe1LoopcodeFacility1Deactivate,    /**< Facility-1 loop de-activate */
    cAtPdhDe1LoopcodeFacility2Activate,      /**< Facility-2 loop activate */
    cAtPdhDe1LoopcodeFacility2Deactivate,    /**< Facility-2 loop de-activate */
    cAtPdhDe1LoopcodeInbandDisable           /**< Stop generating loopcode pattern */
    }eAtPdhDe1Loopcode;

/**
 * @brief E1 Sa bits
 */
typedef enum eAtPdhE1Sa
	{
	cAtPdhE1Sa4 = cBit0,  /**< Sa4 */
	cAtPdhE1Sa5 = cBit1,  /**< Sa5 */
	cAtPdhE1Sa6 = cBit2,  /**< Sa6 */
	cAtPdhE1Sa7 = cBit3,  /**< Sa7 */
	cAtPdhE1Sa8 = cBit4   /**< Sa8 */
	}eAtPdhE1Sa;

/**
 * @brief All counters for DS1/E1
 */
typedef struct tAtPdhDe1Counters
	{
	uint32 bpvExz;     /**< Number of BPV EXZ */
	uint32 crcError;   /**< Number of CRC error */
	uint32 fBitError;  /**< Number of framing bit error */
	uint32 rei;        /**< Number of REI */
	uint32 txCs;       /**< Number of PSN to TDM controlled Slip counter */
	}tAtPdhDe1Counters;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Signaling */
eAtModulePdhRet AtPdhDe1SignalingEnable(AtPdhDe1 self, eBool enable);
eAtModulePdhRet AtPdhDe1TxSignalingEnable(AtPdhDe1 self, eBool enable);
eAtModulePdhRet AtPdhDe1RxSignalingEnable(AtPdhDe1 self, eBool enable);
eBool AtPdhDe1SignalingIsEnabled(AtPdhDe1 self);
eAtModulePdhRet AtPdhDe1SignalingDs0MaskSet(AtPdhDe1 self, uint32 sigBitmap);
uint32 AtPdhDe1SignalingDs0MaskGet(AtPdhDe1 self);
eAtRet AtPdhDe1SignalingPatternSet(AtPdhDe1 self, uint32 ts, uint8 abcd);
uint8 AtPdhDe1SignalingPatternGet(AtPdhDe1 self, uint32 ts);
eBool AtPdhDe1SignalingIsSupported(AtPdhDe1 self);

/* E1 National Bits and International Bit */
eAtModulePdhRet AtPdhDe1InternationalBitSet(AtPdhDe1 self, uint8 zeroOrOne);
uint8 AtPdhDe1InternationalBitGet(AtPdhDe1 self);

/* DS1 SF/ESF Inband loop-code */
eAtModulePdhRet AtPdhDe1TxLoopcodeSet(AtPdhDe1 self, eAtPdhDe1Loopcode loopcode);
eAtPdhDe1Loopcode AtPdhDe1TxLoopcodeGet(AtPdhDe1 self);
eAtPdhDe1Loopcode AtPdhDe1RxLoopcodeGet(AtPdhDe1 self);

/* Auto AIS when PWs receive L-Bit */
eAtModulePdhRet AtPdhDe1AutoTxAisEnable(AtPdhDe1 self, eBool enable);
eBool AtPdhDe1AutoTxAisIsEnabled(AtPdhDe1 self);

/* Access NxDS0 */
eBool AtPdhDe1NxDs0IsSupported(AtPdhDe1 self);
AtPdhNxDS0 AtPdhDe1NxDs0Create(AtPdhDe1 self, uint32 timeslotBitmap);
eAtModulePdhRet AtPdhDe1NxDs0Delete(AtPdhDe1 self, AtPdhNxDS0 nxDs0);
AtPdhNxDS0 AtPdhDe1NxDs0Get(AtPdhDe1 self, uint32 timeslotBitmap);

/* NxDs0 Iterator */
AtIterator AtPdhDe1nxDs0IteratorCreate(AtPdhDe1 self);

/* SA bit for data link */
eAtModulePdhRet AtPdhDe1DataLinkSaBitsMaskSet(AtPdhDe1 self, uint8 saBitsMask);
uint8 AtPdhDe1DataLinkSaBitsMaskGet(AtPdhDe1 self);

/* DS1 ESF BOM */
eAtModulePdhRet AtPdhDe1TxBomSet(AtPdhDe1 self, uint8 bomCode);
uint8 AtPdhDe1TxBomGet(AtPdhDe1 self);
uint8 AtPdhDe1RxBomGet(AtPdhDe1 self);
uint8 AtPdhDe1RxCurrentBomGet(AtPdhDe1 self);
eAtModulePdhRet AtPdhDe1TxBomWithModeSet(AtPdhDe1 self, uint8 bomCode, eAtPdhBomSentMode sentMode);
eAtPdhBomSentMode AtPdhDe1TxBomModeGet(AtPdhDe1 self);

/* E1 CRC SA bit for SSM */
eAtModulePdhRet AtPdhDe1SsmSaBitsMaskSet(AtPdhDe1 self, eAtPdhE1Sa saBit);
eAtPdhE1Sa AtPdhDe1SsmSaBitsMaskGet(AtPdhDe1 self);

/* Utility */
eBool AtPdhDe1IsE1(AtPdhDe1 self);
eBool AtPdhDe1FrameTypeIsE1(eAtPdhDe1FrameType swFrameMode);
eBool AtPdhDe1IsUnframeMode(uint16 frameType);
eBool AtPdhDe1SaBitsIsApplicable(AtPdhDe1 self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHDE1_H_ */

#include "AtPdhDe1Prm.h"
#include "AtPdhDe1Deprecated.h"

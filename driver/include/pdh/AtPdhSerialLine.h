/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : AtPdhSerialLine.h
 * 
 * Created Date: Apr 1, 2015
 *
 * Description : PDH Serial Line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHSERIALLINE_H_
#define _ATPDHSERIALLINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h" /* Super class */
#include "AtModulePdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPdhSerialLine
 * @{
 */
/** @brief PDH DS3/E3 serial line mode  */
typedef enum eAtPdhDe3SerialLineMode
    {
    cAtPdhDe3SerialLineModeUnknown, /**< Unknown */
    cAtPdhDe3SerialLineModeEc1,     /**< EC-1 */
    cAtPdhDe3SerialLineModeDs3,     /**< DS3 */
    cAtPdhDe3SerialLineModeE3       /**< E3 */
    }eAtPdhDe3SerialLineMode;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Serial Line mode */
eAtRet AtPdhSerialLineModeSet(AtPdhSerialLine self, uint32 mode);
uint32 AtPdhSerialLineModeGet(AtPdhSerialLine self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPDHSERIALLINE_H_ */


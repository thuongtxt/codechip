/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : PDH
 *
 * File        : AtPdhDe1Deprecated.h
 *
 * Created Date: Oct 12, 2015
 *
 * Description : PDH DS1/E1 deprecated interface.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPDHDE1DEPRECATED_H_
#define _ATPDHDE1DEPRECATED_H_

/*--------------------------- Include files ----------------------------------*/
#include "attypes.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/*
 * @brief Alarm types [deprecated]
 */
typedef enum eAtPdhDe1AlarmTypeDeprecated
    {
    cAtPdhDs1AlarmOutbandLoopCodeChange = cBit13   /**< Outband Loopcode change, deprecated by cAtPdhDs1AlarmBomChange */
    }eAtPdhDe1AlarmTypeDeprecated;

/*
 * @brief FDL loop code, deprecated by AtPdhDe1TxBomSet/AtPdhDe1RxBomGet.
 */
typedef enum eAtPdhDe1LoopcodeDeprecated
    {
    cAtPdhDe1LoopCodeStartDeprecated = cAtPdhDe1LoopcodeInbandDisable,
    cAtPdhDe1LoopcodeLineRetention,     /**< DS1 ESF Outband Line loop retention */
    cAtPdhDe1LoopcodePayloadActivate,   /**< DS1 ESF Outband Payload loop activate */
    cAtPdhDe1LoopcodePayloadDeactivate, /**< DS1 ESF Outband Payload loop de-activate */
    cAtPdhDe1LoopcodePayloadRetention,  /**< DS1 ESF Outband Payload loop retention */
    cAtPdhDe1LoopcodeNetWorkActivate,   /**< Outband NetWork loop retention */
    cAtPdhDe1LoopcodeNetWorkDeactivate  /**< Outband NetWork loop de-activate */
    }eAtPdhDe1LoopcodeDeprecated;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
/* Loopcode, deprecated by AtPdhDe1TxLoopcodeSet and AtPdhDe1RxLoopcodeGet respectively. */
eAtModulePdhRet AtPdhDe1LoopcodeSend(AtPdhDe1 self, eAtPdhDe1Loopcode loopcode);
eAtPdhDe1Loopcode AtPdhDe1LoopcodeReceive(AtPdhDe1 self);

/* Loopcode enable, software provision only, not any affect on hardware engine. */
eAtModulePdhRet AtPdhDe1LoopcodeEnable(AtPdhDe1 self, eBool enable);
eBool AtPdhDe1LoopcodeIsEnabled(AtPdhDe1 self);

/* BOM, deprecated by AtPdhDe1TxBomSet and AtPdhDe1RxBomGet respectively. */
eAtModulePdhRet AtPdhDe1BomSend(AtPdhDe1 self, uint8 message);
uint8 AtPdhDe1BomReceive(AtPdhDe1 self);

/* BOM, deprecated by AtPdhDe1TxBomWithModeSet() */
eAtModulePdhRet AtPdhDe1TxBomNTimesSend(AtPdhDe1 self, uint8 bomCode, uint32 nTimes);
uint32 AtPdhDe1TxBomSentMaxNTimes(AtPdhDe1 self);
uint32 AtPdhDe1TxBomSentCfgNTimes(AtPdhDe1 self);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* _ATPDHDE1DEPRECATED_H_ */

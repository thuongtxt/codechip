/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : AtClockExtractor.h
 * 
 * Created Date: Aug 27, 2013
 *
 * Description : Clock extractor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCLOCKEXTRACTOR_H_
#define _ATCLOCKEXTRACTOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtObject.h" /* Super class */
#include "AtModuleClock.h"
#include "AtPdhDe1.h"
#include "AtSdhLine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtClockExtractor
 * @{
 */

/**
 * @brief Clock extractor squelching option masks
 */
typedef enum eAtClockExtractorSquelching
    {
    cAtClockExtractorSquelchingNone = 0,      /**< No squelching */
    cAtClockExtractorSquelchingAis  = cBit0,  /**< Squelching on AIS */
    cAtClockExtractorSquelchingLof  = cBit1,  /**< Squelching on LOF */
    cAtClockExtractorSquelchingLos  = cBit2,  /**< Squelching on LOS */

    /* For ETH interfaces */
    cAtClockExtractorSquelchingEthLf = cBit3,      /**< Squelching on 10 XGE Eth Local fault */
    cAtClockExtractorSquelchingEthRf = cBit4,      /**< Squelching on 10 XGE Eth Remote fault */
    cAtClockExtractorSquelchingEthLossync = cBit5, /**< Squelching on 10 XGE Eth Lossync */
    cAtClockExtractorSquelchingEthEer = cBit6,     /**< Squelching on 10 XGE/1GE Eth Excessive error rate */
    cAtClockExtractorSquelchingEthLinkDown = cBit7 /**< Squelching on 1GE Eth link down */
    }eAtClockExtractorSquelching;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint8 AtClockExtractorIdGet(AtClockExtractor self);

/* The following 3 APIs may be hard to use, see atomic APIs */
AtModuleClock AtClockExtractorModuleGet(AtClockExtractor self);
eAtModuleClockRet AtClockExtractorExtract(AtClockExtractor self, eAtTimingMode clockSourceType, AtChannel clockSource);
AtChannel AtClockExtractorSourceGet(AtClockExtractor self);
eAtTimingMode AtClockExtractorSourceTypeGet(AtClockExtractor self);

/* Control global clock extraction */
eAtModuleClockRet AtClockExtractorSystemClockExtract(AtClockExtractor self);
eBool AtClockExtractorSystemClockIsExtracted(AtClockExtractor self);

/* Control SDH clock extraction */
eAtModuleClockRet AtClockExtractorSdhSystemClockExtract(AtClockExtractor self);
eBool AtClockExtractorSdhSystemClockIsExtracted(AtClockExtractor self);
eAtModuleClockRet AtClockExtractorSdhLineClockExtract(AtClockExtractor self, AtSdhLine line);
AtSdhLine AtClockExtractorSdhLineGet(AtClockExtractor self);

/* Control external clock extraction */
eAtModuleClockRet AtClockExtractorExternalClockExtract(AtClockExtractor self, uint8 externalId);
uint8 AtClockExtractorExternalClockGet(AtClockExtractor self);
eBool AtClockExtractorExternalIdIsValid(AtClockExtractor self, uint8 externalId);

/* Control PDH DS1/E1 clock extraction */
eAtModuleClockRet AtClockExtractorPdhDe1ClockExtract(AtClockExtractor self, AtPdhDe1 de1);
eAtModuleClockRet AtClockExtractorPdhDe1LiuClockExtract(AtClockExtractor self, AtPdhDe1 de1);
AtPdhDe1 AtClockExtractorPdhDe1Get(AtClockExtractor self);
eBool AtClockExtractorPdhDe1LiuClockIsExtracted(AtClockExtractor self);

/* Control PDH DS3/E3 clock extraction */
eAtModuleClockRet AtClockExtractorPdhDe3ClockExtract(AtClockExtractor self, AtPdhDe3 de3);
eAtModuleClockRet AtClockExtractorPdhDe3LiuClockExtract(AtClockExtractor self, AtPdhDe3 de3);
AtPdhDe3 AtClockExtractorPdhDe3Get(AtClockExtractor self);
eBool AtClockExtractorPdhDe3LiuClockIsExtracted(AtClockExtractor self);

/* Serdes clock extractor */
eAtModuleClockRet AtClockExtractorSerdesClockExtract(AtClockExtractor self, AtSerdesController serdes);
AtSerdesController AtClockExtractorSerdesGet(AtClockExtractor self);

/* Enabling */
eAtModuleClockRet AtClockExtractorEnable(AtClockExtractor self, eBool enable);
eBool AtClockExtractorIsEnabled(AtClockExtractor self);

/* Debug */
void AtClockExtractorDebug(AtClockExtractor self);
uint32 AtClockExtractorRead(AtClockExtractor self, uint32 address);
void AtClockExtractorWrite(AtClockExtractor self, uint32 address, uint32 value);

/* Squelching */
eAtModuleClockRet AtClockExtractorSquelchingOptionSet(AtClockExtractor self, eAtClockExtractorSquelching options);
eAtClockExtractorSquelching AtClockExtractorSquelchingOptionGet(AtClockExtractor self);
eBool AtClockExtractorSquelchingIsSupported(AtClockExtractor self);
uint32 AtClockExtractorOutputCounterGet(AtClockExtractor self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCLOCKEXTRACTOR_H_ */


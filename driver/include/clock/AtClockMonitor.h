/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : AtClockMonitor.h
 * 
 * Created Date: Aug 7, 2017
 *
 * Description : Clock monitor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCLOCKMONITOR_H_
#define _ATCLOCKMONITOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtClockMonitor
 * @{
 */

/**
 * @brief Clock monitor abstract class
 */
typedef struct tAtClockMonitor * AtClockMonitor;

/**
 * @brief Threshold
 */
typedef enum eAtClockMonitorThreshold
    {
    cAtClockMonitorThresholdLossOfClock,        /**< Loss of clock threshold in PPM */
    cAtClockMonitorThresholdFrequencyOutOfRange /**< Frequency out of range threshold in PPM */
    }eAtClockMonitorThreshold;

/**
 * @brief Alarm types
 */
typedef enum eAtClockMonitorAlarmType
    {
    cAtClockMonitorAlarmNone           = 0,     /**< No alarms */
    cAtClockMonitorAlarmLossOfClock    = cBit0, /**< Loss of clock. This happens
        when current monitored PPM crosses loss of clock threshold */
    cAtClockMonitorFrequencyOutOfRange = cBit1  /**< Frequency out of range. This
        happens when current monitored PPM crosses frequency out of range threshold */
    }eAtClockMonitorAlarmType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtClockMonitorInit(AtClockMonitor self);
AtObject AtClockMonitorMonitoredObjectGet(AtClockMonitor self);

/* PPM */
uint32 AtClockMonitorCurrentPpmGet(AtClockMonitor self);

/* Thresholds */
eAtRet AtClockMonitorThresholdSet(AtClockMonitor self, eAtClockMonitorThreshold threshold, uint32 value);
uint32 AtClockMonitorThresholdGet(AtClockMonitor self, eAtClockMonitorThreshold threshold);
uint32 AtClockMonitorThresholdMax(AtClockMonitor self, eAtClockMonitorThreshold threshold);
eBool AtClockMonitorThresholdIsSupported(AtClockMonitor self, eAtClockMonitorThreshold threshold);

/* Alarm */
uint32 AtClockMonitorAlarmGet(AtClockMonitor self);
uint32 AtClockMonitorAlarmHistoryGet(AtClockMonitor self);
uint32 AtClockMonitorAlarmHistoryClear(AtClockMonitor self);

/* Interrupt mask */
eAtRet AtClockMonitorInterruptMaskSet(AtClockMonitor self, uint32 defectMask, uint32 enableMask);
uint32 AtClockMonitorInterruptMaskGet(AtClockMonitor self);
eBool AtClockMonitorInterruptMaskIsSupported(AtClockMonitor self, uint32 defectMask);

#ifdef __cplusplus
}
#endif
#endif /* _ATCLOCKMONITOR_H_ */


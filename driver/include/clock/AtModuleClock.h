/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : AtModuleClock.h
 * 
 * Created Date: Aug 27, 2013
 *
 * Description : Clock module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULECLOCK_H_
#define _ATMODULECLOCK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleClock
 * @{
 */
/** @brief Clock module */
typedef struct tAtModuleClock * AtModuleClock;

/** @brief Clock extractor */
typedef struct tAtClockExtractor * AtClockExtractor;

/** @brief Clock module specific return codes */
typedef uint32 eAtModuleClockRet;

/**
 * @brief Module alarm types
 */
typedef enum eAtModuleClockAlarmType
    {
    cAtModuleClockAlarmNone             = 0,    /**< No alarms */
    cAtModuleClockAlarmSdhSerdesPllFail = cBit0 /**< SONET/SDH SERDES PLL fail */
    }eAtModuleClockAlarmType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint8 AtModuleClockNumExtractors(AtModuleClock self);
AtClockExtractor AtModuleClockExtractorGet(AtModuleClock self, uint8 extractorId);

uint32 AtModuleClockAllClockCheck(AtModuleClock self);
eBool AtModuleClockAllClockCheckIsSupported(AtModuleClock self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULECLOCK_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtHdlcLinkDeprecated.h
 * 
 * Created Date: Apr 21, 2016
 *
 * Description : HDLC Link abstract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHDLCLINKDEPRECATED_H_
#define _ATHDLCLINKDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Traffic enabling */
eAtModuleEncapRet AtHdlcLinkTxTrafficEnable(AtHdlcLink self, eBool enable);
eBool AtHdlcLinkTxTrafficIsEnabled(AtHdlcLink self);
eAtModuleEncapRet AtHdlcLinkRxTrafficEnable(AtHdlcLink self, eBool enable);
eBool AtHdlcLinkRxTrafficIsEnabled(AtHdlcLink self);

#ifdef __cplusplus
}
#endif
#endif /* _ATHDLCLINKDEPRECATED_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : AtEncapBundle.h
 * 
 * Created Date: Sep 1, 2012
 *
 * Description : Encap bundle abstract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATENCAPBUNDLE_H_
#define _ATENCAPBUNDLE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtEncapBundle
 * @{
 */
/**
 * @brief Bundle counters
 */
typedef enum eAtEncapBundleType
    {
    cAtEncapBundleTypeUnknown, /**< Unknown >*/
    cAtEncapBundleTypeMlppp,   /**< MLPPP >*/
    cAtEncapBundleTypeMfr      /**< MLFR >*/
    }eAtEncapBundleType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtEncapBundleTypeGet(AtEncapBundle self);

#ifdef __cplusplus
}
#endif
#endif /* _ATENCAPBUNDLE_H_ */


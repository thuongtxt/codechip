/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtGfpChannel.h
 * 
 * Created Date: Apr 28, 2014
 *
 * Description : GFP encapsulation channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATGFPCHANNEL_H_
#define _ATGFPCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEncap.h"
#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtGfpChannel
 * @{
 */
/**
 * @brief GFP framing mode
 */
typedef enum eAtGfpFrameMode
    {
    cAtGfpFrameModeGfpF,   /**< GFP-F */
    cAtGfpFrameModeGfpT,   /**< GFP-T */
    cAtGfpFrameModeUnknown /**< Unknown */
    }eAtGfpFrameMode;

/**
 * @brief GFP channel counters
 */
typedef struct tAtGfpChannelCounters
    {
    uint32      txFrm;          /**< TX Frames */
    uint32      txgdFrm;        /**< TX Good frames */
    uint32      txidle;         /**< TX Idle frames */
    uint32      txbyte;         /**< Tx bytes */
    uint32      txcsf;          /**< TX CSF */
    uint32      txcmf;          /**< TX CMF */

    uint32      rxFrm;          /**< RX frames */
    uint32      rxgdFrm;        /**< RX Good frame */
    uint32      rxbyte;         /**< RX byte */
    uint32      rxidleFrm;      /**< RX Idle frame */
    uint32      rxchecCorrErr;  /**< RX cHEC Error correctable */
    uint32      rxchecUCorrErr; /**< RX cHEC Error un-correctable */
    uint32      rxthecCorrErr;  /**< RX tHEC Error correctable */
    uint32      rxthecUCorrErr; /**< RX tHEC Error un-correctable */
    uint32      rxehecErr;      /**< RX eHEC Error */
    uint32      rxfcsErr;       /**< RX FCS Error */
    uint32      rxupm;          /**< RX User Payload mismatch */
    uint32      rxexm;          /**< RX Extension mismatch */
    uint32      rxcsf;          /**< RX CSF frames */
    uint32      rxcmf;          /**< RX CMF frames */
    uint32      rxdropFrm;      /**< RX dropped frames */
    }tAtGfpChannelCounters;

/**
 * @brief GFP alarm
 */
typedef enum eAtGfpChannelAlarmType
    {
    cAtGfpChannelAlarmLfd   = cBit0, /**< LFD defect mask */
    cAtGfpChannelAlarmCsf   = cBit1  /**< CSF defect mask */
    }eAtGfpChannelAlarmType;

/** @brief Counter type */
typedef enum eAtGfpChannelCounterType
    {
    cAtGfpChannelCounterTypeUnknown,        /**< Unknown counter type */
    cAtGfpChannelCounterTypeTxFrames,       /**< Transmitted frames count */
    cAtGfpChannelCounterTypeTxGoodFrames,   /**< Transmitted good frames count */
    cAtGfpChannelCounterTypeTxIdleFrames,   /**< Transmitted IDLE frames count */
    cAtGfpChannelCounterTypeTxBytes,        /**< Transmitted bytes count */
    cAtGfpChannelCounterTypeTxCsf,          /**< Transmitted Client Signal Failure frames count */
    cAtGfpChannelCounterTypeTxCmf,          /**< Transmitted Client Management Frames count */

    cAtGfpChannelCounterTypeRxFrames,       /**< Received frames count */
    cAtGfpChannelCounterTypeRxBytes,        /**< Received bytes count */
    cAtGfpChannelCounterTypeRxGoodFrames,   /**< Received good frames */
    cAtGfpChannelCounterTypeRxIdleFrames,   /**< Received good frames */
    cAtGfpChannelCounterTypeRxCHecCorrected,/**< Received cHEC error corrected frames */
    cAtGfpChannelCounterTypeRxCHecError,    /**< Received cHEC error non-correctable frames */
    cAtGfpChannelCounterTypeRxTHecCorrected,/**< Received tHEC error corrected frames */
    cAtGfpChannelCounterTypeRxTHecError,    /**< Received tHEC error non-correctable frames */
    cAtGfpChannelCounterTypeRxEHecError,    /**< Received eHEC error frames count */
    cAtGfpChannelCounterTypeRxFcsError,     /**< Received FCS error frames count */
    cAtGfpChannelCounterTypeRxUpm,          /**< Received User Payload Mismatch frames count */
    cAtGfpChannelCounterTypeRxExm,          /**< Received Extension Header Mismatch frames count */
    cAtGfpChannelCounterTypeRxCsf,          /**< Received Client Signal Failure frames count */
    cAtGfpChannelCounterTypeRxCmf,          /**< Received Client Management Frames count */
    cAtGfpChannelCounterTypeRxDrops         /**< Received dropped frames count */
    }eAtGfpChannelCounterType;

/**
* @}
*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* PTI */
eAtModuleEncapRet AtGfpChannelTxPtiSet(AtGfpChannel self, uint8 txPti);
uint8 AtGfpChannelTxPtiGet(AtGfpChannel self);
eAtModuleEncapRet AtGfpChannelExpectedPtiSet(AtGfpChannel self, uint8 expectedPti);
uint8 AtGfpChannelExpectedPtiGet(AtGfpChannel self);
eAtModuleEncapRet AtGfpChannelPtiMonitorEnable(AtGfpChannel self, eBool enable);
eBool AtGfpChannelPtiMonitorIsEnabled(AtGfpChannel self);

/* UPI */
eAtModuleEncapRet AtGfpChannelTxUpiSet(AtGfpChannel self, uint8 txUpi);
uint8 AtGfpChannelTxUpiGet(AtGfpChannel self);
eAtModuleEncapRet AtGfpChannelExpectedUpiSet(AtGfpChannel self, uint8 expectedUpi);
uint8 AtGfpChannelExpectedUpiGet(AtGfpChannel self);
eAtModuleEncapRet AtGfpChannelUpiMonitorEnable(AtGfpChannel self, eBool enable);
eBool AtGfpChannelUpiMonitorIsEnabled(AtGfpChannel self);

/* EXI */
eAtModuleEncapRet AtGfpChannelTxExiSet(AtGfpChannel self, uint8 txExi);
uint8 AtGfpChannelTxExiGet(AtGfpChannel self);
eAtModuleEncapRet AtGfpChannelExpectedExiSet(AtGfpChannel self, uint8 expectedExi);
uint8 AtGfpChannelExpectedExiGet(AtGfpChannel self);
eAtModuleEncapRet AtGfpChannelExiMonitorEnable(AtGfpChannel self, eBool enable);
eBool AtGfpChannelExiMonitorIsEnabled(AtGfpChannel self);

/* PFI */
eAtModuleEncapRet AtGfpChannelTxPfiSet(AtGfpChannel self, uint8 txPfi);
uint8 AtGfpChannelTxPfiGet(AtGfpChannel self);

/* FCS */
eAtModuleEncapRet AtGfpChannelTxFcsEnable(AtGfpChannel self, eBool enable);
eBool AtGfpChannelTxFcsIsEnabled(AtGfpChannel self);
eAtModuleEncapRet AtGfpChannelFcsMonitorEnable(AtGfpChannel self, eBool enable);
eBool AtGfpChannelFcsMonitorIsEnabled(AtGfpChannel self);

/* Frame mode */
eBool AtGfpChannelFrameModeIsSupported(AtGfpChannel self, eAtGfpFrameMode frameMappingMode);
eAtModuleEncapRet AtGfpChannelFrameModeSet(AtGfpChannel self, eAtGfpFrameMode frameMappingMode);
eAtGfpFrameMode AtGfpChannelFrameModeGet(AtGfpChannel self);

/* CSF */
eAtModuleEncapRet AtGfpChannelTxCsfUpiSet(AtGfpChannel self, uint8 txUpi);
uint8 AtGfpChannelTxCsfUpiGet(AtGfpChannel self);
eAtModuleEncapRet AtGfpChannelTxCsfEnable(AtGfpChannel self, eBool enable);
eBool AtGfpChannelTxCsfIsEnabled(AtGfpChannel self);
uint8 AtGfpChannelRxCsfUpiGet(AtGfpChannel self);

/* Scramble */
eAtModuleEncapRet AtGfpChannelPayloadScrambleEnable(AtGfpChannel self, eBool enable);
eBool AtGfpChannelPayloadScrambleIsEnabled(AtGfpChannel self);
eAtModuleEncapRet AtGfpChannelCoreHeaderScrambleEnable(AtGfpChannel self, eBool enable);
eBool AtGfpChannelCoreHeaderScrambleIsEnabled(AtGfpChannel self);

#endif /* _ATGFPCHANNEL_H_ */

#include "AtGfpChannelDeprecated.h"

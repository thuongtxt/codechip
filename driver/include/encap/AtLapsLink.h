/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENC
 * 
 * File        : AtLapsLink.h
 * 
 * Created Date: May 25, 2016
 *
 * Description : ENC, LAPS Link
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLAPSLINK_H_
#define _ATLAPSLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/** @brief Counter type */
typedef enum eAtLapsLinkCounterType
    {
    cAtLapsLinkCounterTypeUnknown,     /**< Unknown counter type */
    cAtLapsLinkCounterTypeRxSapiError  /**< Received SAPI error */
    }eAtLapsLinkCounterType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* For Tx */
eAtRet AtLapsLinkTxSapiSet(AtLapsLink self, uint16 value);
uint16 AtLapsLinkTxSapiGet(AtLapsLink self);

/* For Monitor */
eAtRet AtLapsLinkSapiMonitorEnable(AtLapsLink self, eBool enable);
eBool AtLapsLinkSapiMonitorIsEnabled(AtLapsLink self);
eAtRet AtLapsLinkExpectedSapiSet(AtLapsLink self, uint16 value);
uint16 AtLapsLinkExpectedSapiGet(AtLapsLink self);

#ifdef __cplusplus
}
#endif
#endif /* _ATLAPSLINK_H_ */


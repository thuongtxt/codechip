/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtHdlcBundle.h
 * 
 * Created Date: Aug 3, 2012
 *
 * Description : HDLC Bundle abtract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATBUNDLE_H_
#define _ATBUNDLE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEncap.h"
#include "AtEthFlow.h"
#include "AtHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleEncap
 * @{
 */

/**
 * @brief Bundle scheduling mode
 */
typedef enum eAtHdlcBundleSchedulingMode
    {
    cAtHdlcBundleSchedulingModeRandom   = 0x0, /**< Random */
    cAtHdlcBundleSchedulingModeFairRR   = 0x1, /**< Fair Round Robin */
    cAtHdlcBundleSchedulingModeStrictRR = 0x2, /**< Strict Round Robin */
    cAtHdlcBundleSchedulingModeSmartRR  = 0x3, /**< Smart Round Robin */
    cAtHdlcBundleSchedulingModeDWRR     = 0x4  /**< Deficit Weighted Round Robin */
    }eAtHdlcBundleSchedulingMode;

/**
 * @brief Bundle counters
 */
typedef struct tAtHdlcBundleCounters
    {
    uint32 txPkt;             /**< Transmit packet count */
    uint32 txByte;            /**< Transmit byte count */
    uint32 txAbortPkt;        /**< Transmit Abort packet count */
    uint32 txGoodByte;        /**< Transmit Good byte count */
    uint32 txGoodPkt;         /**< Transmit Good packet count */
    uint32 txFragmentPkt;     /**< Transmit Fragment packet count */
    uint32 txDiscardPkt;      /**< Transmit discard packet count */
    uint32 rxPkt;             /**< Receive packet count */   
    uint32 rxByte;            /**< Receive byte count */
    uint32 rxAbortPkt;        /**< Receive Abort packet count */
    uint32 rxDiscardFragment; /**< Discard fragment count */
    uint32 rxFragmentPkt;     /**< Receive Fragment packet count */
    uint32 rxDiscardPkt;      /**< Receive discard packet count */
    uint32 rxGoodByte;        /**< Receive Good byte count */
    uint32 rxGoodPkt;         /**< Receive Good packet count */
    uint32 rxQueueGoodPkt;    /**< Queue good packet count */
    uint32 rxQueueDiscardPkt; /**< Queue discard packet count */
    uint32 rxReasembleFail;   /**< Number of times reassemble fail happen */
    }tAtHdlcBundleCounters;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Link management */
eAtModuleEncapRet AtHdlcBundleLinkRemove(AtHdlcBundle self, AtHdlcLink link);
eAtModuleEncapRet AtHdlcBundleLinkAdd(AtHdlcBundle self, AtHdlcLink link);
uint16 AtHdlcBundleLinksGet(AtHdlcBundle self, AtHdlcLink * links);
uint16 AtHdlcBundleNumberOfLinksGet(AtHdlcBundle self);
AtHdlcLink AtHdlcBundleLinkGet(AtHdlcBundle self, uint16 linkIndex);
uint16 AtHdlcBundleMaxLinksGet(AtHdlcBundle self);

/* Fragment size */
eAtModuleEncapRet AtHdlcBundleFragmentSizeSet(AtHdlcBundle self, eAtFragmentSize fragSize);
eAtFragmentSize AtHdlcBundleFragmentSizeGet(AtHdlcBundle self);

/* Maximum delay */
eAtModuleEncapRet AtHdlcBundleMaxDelaySet(AtHdlcBundle self, uint8 maxDelayInMs);
uint8 AtHdlcBundleMaxDelayGet(AtHdlcBundle self);

/* Links Iterator */
AtIterator AtHdlcBundleLinkIteratorCreate(AtHdlcBundle self);

/* Oam flow */
AtEthFlow AtHdlcBundleOamFlowGet(AtHdlcBundle self);

/* Scheduling mode */
eAtRet AtHdlcBundleSchedulingModeSet(AtHdlcBundle self, eAtHdlcBundleSchedulingMode schedulingMode);
eAtHdlcBundleSchedulingMode AtHdlcBundleSchedulingModeGet(AtHdlcBundle self);

/* Protocol Data Unit */
eAtModuleEncapRet AtHdlcBundlePduTypeSet(AtHdlcBundle self, eAtHdlcPduType pduType);
eAtHdlcPduType AtHdlcBundlePduTypeGet(AtHdlcBundle self);

#ifdef __cplusplus
}
#endif
#endif /* _ATBUNDLE_H_ */


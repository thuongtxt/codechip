/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encapsulation
 * 
 * File        : AtAtmTc.h
 * 
 * Created Date: Sep 27, 2012
 *
 * Description : ATM Transmission Convergence
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATMTC_H_
#define _ATATMTC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtAtmTc
 * @{
 */
/**
 * @brief ATM Network interface mode
 * */
typedef enum eAtAtmNetworkInterfaceType
    {
    cAtAtmNetworkInterfaceTypeInvalid,  /**< Invalid */
    cAtAtmNetworkInterfaceTypeUni,      /**< User network interface mode */
    cAtAtmNetworkInterfaceTypeNni       /**< Network interface mode */
    }eAtAtmNetworkInterfaceType;

/**
 * @brief ATM Mapping mode
 * */
typedef enum eAtAtmCellMappingMode
    {
    cAtAtmCellMappingModeInvalid,   /**< Invalid */
    cAtAtmCellMappingModeDirectly,  /**< Mapping directly */
    cAtAtmCellMappingModePlcp       /**< Mapping to PLCP */
    }eAtAtmCellMappingMode;

/**
 * @brief ATM TC alarm types
 * */
typedef enum eAtAtmTcAlarmType
	{
	cAtAtmTcAlarmTypeLcd = cBit0,	/**< LCD */
	cAtAtmTcAlarmTypeOcd = cBit1 	/**< OCD */
	}eAtAtmTcAlarmType;

/**
 * @brief ATM TC counter types
 * */
typedef enum eAtAtmTcCounterType
	{
	cAtAtmTcCounterTypeRxGoodCell, /**< RX good cell */
	cAtAtmTcCounterTypeRxIdleCell,	/**< RX idle cell */
	cAtAtmTcCounterTypeRxHecErrorCell, /**< RX errored-header cell */
	cAtAtmTcCounterTypeRxHecErrorCorrectedCell, /**< RX corrected-header cell, valid only in ATM protocol mode */
	cAtAtmTcCounterTypeRxFebeCell, /**< RX FEBE Error counter, valid only in PLCP protocol mode */
	cAtAtmTcCounterTypeRxB1Cell, /**< RX cells with B1 error, valid only in PLCP protocol mode */
	cAtAtmTcCounterTypeTxGoodCell, /**< TX good cell */
	cAtAtmTcCounterTypeTxIdleCell  /**< TX idle cell */
	}eAtAtmTcCounterType;

/**
 * @brief ATM TC counters
 * */
typedef struct tAtAtmTcCounters
	{
	uint32       rxGoodCell;             /**< RX good cell */
	uint32       rxIdleCell;             /**< RX idle cell */
	uint32       rxHecErrCell;           /**< RX errored-header cell */
	uint32       rxHecErrCorrectedCell;  /**< RX corrected-header cell, valid only in ATM protocol mode */
	uint32       rxFebe;                 /**< received FEBE Error counter, valid only in PLCP protocol mode */
	uint32       rxB1;                   /**< received cells with B1 error, valid only in PLCP protocol mode */
	uint32       txGoodCell;             /**< TX good cell */
	uint32       txIdleCell;             /**< TX idle cell */
	}tAtAtmTcCounters;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Network interface type */
eAtModuleEncapRet AtAtmTcNetworkInterfaceTypeSet(AtAtmTc self, eAtAtmNetworkInterfaceType interfaceType);
eAtAtmNetworkInterfaceType AtAtmTcNetworkInterfaceTypeGet(AtAtmTc self);

/* Mapping mode */
eAtModuleEncapRet AtAtmTcCellMappingModeSet(AtAtmTc self, eAtAtmCellMappingMode mapMode);
eAtAtmCellMappingMode AtAtmTcCellMappingModeGet(AtAtmTc self);

#ifdef __cplusplus
}
#endif
#endif /* _ATATMTC_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encapsulation
 * 
 * File        : AtEncapChannel.h
 * 
 * Created Date: Aug 2, 2012
 *
 * Description : Encapsulation abstract channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATENCAPCHANNEL_H_
#define _ATENCAPCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h"
#include "AtModuleEncap.h"
#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtEncapChannel
 * @{
 */

/**
 * @brief Channel encapsulation type
 */
typedef enum eAtEncapType
    {
    cAtEncapUnknown,  /**< Invalid encapsulation type */
    cAtEncapHdlc,     /**< HDLC */
    cAtEncapAtm,      /**< ATM */
    cAtEncapGfp       /**< GFP */
    }eAtEncapType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModuleEncapRet AtEncapChannelPhyBind(AtEncapChannel channel, AtChannel phyChannel);
AtChannel AtEncapChannelBoundPhyGet(AtEncapChannel channel);
eAtEncapType AtEncapChannelEncapTypeGet(AtEncapChannel self);
eAtModuleEncapRet AtEncapChannelScrambleEnable(AtEncapChannel self, eBool enable);
eBool AtEncapChannelScrambleIsEnabled(AtEncapChannel self);

/* Flow binding */
eAtModuleEncapRet AtEncapChannelFlowBind(AtEncapChannel self, AtEthFlow flow);
AtEthFlow AtEncapChannelBoundFlowGet(AtEncapChannel self);

/* Drop packet conditions. */
eAtRet AtEncapChannelDropPacketConditionMaskSet(AtEncapChannel self, uint32 conditionMask, uint32 enableMask);
uint32 AtEncapChannelDropPacketConditionMaskGet(AtEncapChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _ATENCAPCHANNEL_H_ */

/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : ENCAP
 *
 * File        : AtGfpChannelDeprecated.h
 *
 * Created Date: Sep 30, 2015
 *
 * Description : Deprecated GFP encapsulation interface.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef ATGFPCHANNELDEPRECATED_H_
#define ATGFPCHANNELDEPRECATED_H_

/*--------------------------- Include files ----------------------------------*/


#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @brief GFP FCS Mode
 */
typedef enum eAtGfpFcsMode
    {
    cAtGfpFcsModeNoFcs,      /**< No FCS */
    cAtGfpFcsModeFcs32,      /**< FCS-32 */
    cAtGfpFcsModeFcs16,      /**< FCS-16 */
    cAtGfpFcsModeFcsUnknown  /**< Unknown */
    }eAtGfpFcsMode;

/**
 * @brief GFP CSF Type
 */
typedef enum eAtGfpCsfType
    {
    cAtGfpLossOfSync,       /**< Loss of syncronization*/
    cAtGfpLossOfClienSignal /**< Loss of Client signal */
    }eAtGfpCsfType;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/* UPI monitoring, deprecated by AtGfpChannelUpiMonitorEnable()/AtGfpChannelUpiMonitorIsEnabled() */
eAtModuleEncapRet AtGfpChannelUpiCheck(AtGfpChannel self, eBool check);
eBool AtGfpChannelUpiIsChecked(AtGfpChannel self);

/* PTI monitoring, deprecated by AtGfpChannelPtiMonitorEnable()/AtGfpChannelPtiMonitorIsEnabled() */
eAtModuleEncapRet AtGfpChannelPtiCheck(AtGfpChannel self, eBool check);
eBool AtGfpChannelPtiIsChecked(AtGfpChannel self);

/* FCS transmission, deprecated by AtGfpChannelTxFcsEnable()/AtGfpChannelTxFcsIsEnabled() */
eAtModuleEncapRet AtGfpChannelFcsModeSet(AtGfpChannel self,eAtGfpFcsMode fcsMode);
eAtGfpFcsMode AtGfpChannelFcsModeGet(AtGfpChannel self);

/* FCS monitoring, deprecated by AtGfpChannelFcsMonitorEnable()/AtGfpChannelFcsMonitorIsEnabled() */
eAtModuleEncapRet AtGfpChannelFcsCheck(AtGfpChannel self, eBool check);
eBool AtGfpChannelFcsIsChecked(AtGfpChannel self);

/* CSF transmission, deprecated by AtGfpChannelTxCsfUpiSet() and AtGfpChannelTxCsfEnable() */
eAtModuleEncapRet AtGfpChannelTxCsfForce(AtGfpChannel self, eBool enable, eAtGfpCsfType csfType);
eBool AtGfpChannelTxCsfForceGet(AtGfpChannel self, eAtGfpCsfType *csfType);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* ATGFPCHANNELDEPRECATED_H_ */

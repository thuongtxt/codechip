/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtHdlcChannelDeprecated.h
 * 
 * Created Date: May 28, 2018
 *
 * Description : Deprecated HDLC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHDLCCHANNELDEPRECATED_H_
#define _ATHDLCCHANNELDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
/* Deprecated alarms. */
#define cAtHdlcChannelAlarmTypeFcs                   cAtHdlcChannelAlarmTypeFcsError
#define cAtHdlcChannelAlarmTypeMinLenViolate         cAtHdlcChannelAlarmTypeUndersize
#define cAtHdlcChannelAlarmTypeMaxLenViolate         cAtHdlcChannelAlarmTypeOversize

/* Deprecated counters. */
#define cAtHdlcChannelCounterTypeRxMinLenVioPackets  cAtHdlcChannelCounterTypeRxUndersizePackets
#define cAtHdlcChannelCounterTypeRxMinLenVioBytes    cAtHdlcChannelCounterTypeRxUndersizeBytes
#define cAtHdlcChannelCounterTypeRxMaxLenVioPackets  cAtHdlcChannelCounterTypeRxOversizePackets
#define cAtHdlcChannelCounterTypeRxMaxLenVioBytes    cAtHdlcChannelCounterTypeRxOversizeBytes

/* Deprecated APIs */
#define AtHdlcChannelFcsCalculationMode     AtHdlcChannelFcsCalculationModeSet

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Address/control insertion */
eBool AtHdlcChannelControlIsInserted(AtHdlcChannel self, uint8 *insertedControl);
eAtModuleEncapRet AtHdlcChannelControlInsert(AtHdlcChannel self, eBool insertEnable, uint8 *control);
eAtModuleEncapRet AtHdlcChannelAddressInsert(AtHdlcChannel self, eBool insertEnable, uint8 *address);
eBool AtHdlcChannelAddressIsInserted(AtHdlcChannel self, uint8 *insertedAddress);

/* Address/control fields comparison */
eAtModuleEncapRet AtHdlcChannelAddressCompare(AtHdlcChannel self, eBool compareEnable, uint8 *expectedAddress);
eBool AtHdlcChannelAddressIsCompared(AtHdlcChannel self, uint8 *expectedAddress);
eAtModuleEncapRet AtHdlcChannelControlCompare(AtHdlcChannel self, eBool compareEnable, uint8 *expectedControl);
eBool AtHdlcChannelControlIsCompared(AtHdlcChannel self, uint8 *expectedControl);

/* FCS error control, deprecated by AtEncapChannelDropPacketConditionMaskSet */
eAtModuleEncapRet AtHdlcChannelFcsErrorBypass(AtHdlcChannel self, eBool bypass);
eBool AtHdlcChannelFcsErrorIsBypassed(AtHdlcChannel self);

/* Address/control error control, deprecated by AtEncapChannelDropPacketConditionMaskSet */
eAtModuleEncapRet AtHdlcChannelAddressControlErrorBypass(AtHdlcChannel self, eBool bypass);
eBool AtHdlcChannelAddressControlErrorIsBypassed(AtHdlcChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _ATHDLCCHANNELDEPRECATED_H_ */


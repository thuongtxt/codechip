/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encapsulation
 * 
 * File        : AtModuleEncap.h
 * 
 * Created Date: Aug 2, 2012
 *
 * Description : Encapsulation module APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEENCAP_H_
#define _ATMODULEENCAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleClasses.h"
#include "AtIterator.h"
#include "AtChannelClasses.h"
#include "AtModulePpp.h"
#include "AtModuleFr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleEncap
 * @{
 */

/** @brief Encapsulation channel class */
typedef struct tAtEncapChannel * AtEncapChannel;

/** @brief HDLC link class */
typedef struct tAtHdlcLink * AtHdlcLink;

/** @brief HDLC channel class */
typedef struct tAtHdlcChannel * AtHdlcChannel;

/** @brief GFP class */
typedef struct tAtGfpChannel * AtGfpChannel;

/** @brief ATM Transmission convergence class */
typedef struct tAtAtmTc * AtAtmTc;

/** @brief AtModuleEncap class */
typedef struct tAtEncapBundle * AtEncapBundle;

/** @brief HDLC Bundle class */
typedef struct tAtHdlcBundle * AtHdlcBundle;

/** @brief FR Virtual Circuit class */
typedef struct tAtFrVirtualCircuit *AtFrVirtualCircuit;

/** @brief LAPS Link class */
typedef struct tAtLapsLink * AtLapsLink;

/** @brief Encapsulation module return code */
typedef uint32 eAtModuleEncapRet;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Manage HDLC channels */
uint16 AtModuleEncapMaxChannelsGet(AtModuleEncap self);
AtHdlcChannel AtModuleEncapHdlcPppChannelCreate(AtModuleEncap self, uint16 channelId);
AtHdlcChannel AtModuleEncapFrChannelCreate(AtModuleEncap self, uint16 channelId);
AtHdlcChannel AtModuleEncapCiscoHdlcChannelCreate(AtModuleEncap self, uint16 channelId);
AtGfpChannel AtModuleEncapGfpChannelCreate(AtModuleEncap self, uint16 channelId);
AtAtmTc AtModuleEncapAtmTcCreate(AtModuleEncap self, uint16 channelId);

AtEncapChannel AtModuleEncapChannelGet(AtModuleEncap self, uint16 channelId);
eAtModuleEncapRet AtModuleEncapChannelDelete(AtModuleEncap self, uint16 channelId);
uint32 AtModuleEncapFreeChannelGet(AtModuleEncap self);

/* Manage shared bundles (MLPPP and MFR) */
uint32 AtModuleEncapMaxBundlesGet(AtModuleEncap self);
AtMpBundle AtModuleEncapMpBundleCreate(AtModuleEncap self, uint32 bundleId);
AtMfrBundle AtModuleEncapMfrBundleCreate(AtModuleEncap self, uint32 bundleId);
eAtRet AtModuleEncapBundleDelete(AtModuleEncap self, uint32 bundleId);
AtHdlcBundle AtModuleEncapBundleGet(AtModuleEncap self, uint32 bundleId);

/* Capacity */
eBool AtModuleEncapGfpIsSupported(AtModuleEncap self);

/* Encapsulation channels Iterator */
AtIterator AtModuleEncapChannelIteratorCreate(AtModuleEncap self);
AtIterator AtModuleEncapBundleIteratorCreate(AtModuleEncap self);

/* Idle pattern when physical interface in unused */
eAtRet AtModuleEncapIdlePatternSet(AtModuleEncap self, uint8 pattern);
uint8 AtModuleEncapIdlePatternGet(AtModuleEncap self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEENCAP_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtHdlcChannel.h
 * 
 * Created Date: Aug 2, 2012
 *
 * Description : HDLC channel abstract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHDLCCHANNEL_H_
#define _ATHDLCCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEncapChannel.h"
#include "AtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtHdlcChannel
 * @{
 */

/**
 * @brief HDLC channel frame type
 */
typedef enum eAtHdlcFrameType
    {
    cAtHdlcFrmUnknown,   /**< Un-known frame type */
    cAtHdlcFrmCiscoHdlc, /**< Cisco HDLC */
    cAtHdlcFrmPpp,       /**< PPP */
    cAtHdlcFrmFr,        /**< Frame relay */
    cAtHdlcFrmLaps,      /**< LAPS */
    cAtHdlcFrmLapd       /**< LAPD */
    }eAtHdlcFrameType;

/**
 * @brief HDLC channel stuffing mode
 */
typedef enum eAtHdlcStuffMode
    {
    cAtHdlcStuffBit,   /**< Bit stuffing */
    cAtHdlcStuffByte   /**< Byte stuffing */
    }eAtHdlcStuffMode;

/** @brief HDLC channel FCS mode */
typedef enum eAtHdlcFcsMode
    {
    cAtHdlcFcsModeNoFcs,  /**< Disable FCS inserting */
    cAtHdlcFcsModeFcs16,  /**< FCS-16 */
    cAtHdlcFcsModeFcs32,  /**< FCS-32 */
    cAtHdlcFcsModeUnknown /**< Un-known FCS mode */
    }eAtHdlcFcsMode;

/**
 * @brief HDLC channel counters
 */
typedef struct tAtHdlcChannelCounters
    {
    uint32 txPkt;           /**< Transmit packet */
    uint32 txByte;          /**< Transmit byte */
    uint32 txIdleByte;      /**< Transmit idle byte */
    uint32 txGoodPkt;       /**< Transmit good packet */
    uint32 txGoodByte;      /**< Transmit good byte */
    uint32 txFragment;      /**< Transmit Multiclass-Multilink PPP fragments */
    uint32 txPlain;         /**< Transmit plain packets counter (without MLPPP/MLFR header) */
    uint32 txOam;           /**< Transmit Control packet (LCP/NCP) */
    uint32 txMTUPkts;       /**< Transmit MTU exceeded counter */
    uint32 txAbortPkt;      /**< Transmit Abort packet */
    uint32 rxPkt;           /**< Receive packet */
    uint32 rxByte;          /**< Receive byte */
    uint32 rxIdleByte;      /**< Receive byte */
    uint32 rxGoodPkt;       /**< Receive good packet */
    uint32 rxGoodByte;      /**< Receive good byte */
    uint32 rxFragment;      /**< Receive fragments counter (with MLPPP/MLFR header) */
    uint32 rxPlain;         /**< Receive plain packets counter (without MLPPP/MLFR header) */
    uint32 rxOam;           /**< Receive control packet */
    uint32 rxMRUPkts;       /**< Receive MRU exceeded counter */
    uint32 rxAbortPkt;      /**< Receive Abort packet */
    uint32 rxFcsErrPkt;     /**< Receive Fcs Error packet */
    uint32 rxAddrCtrlErrPkt;/**< Receive Address Control error packet */
    uint32 rxSapiErrPkt;    /**< Receive Sapi error packet */
    uint32 rxErrPkt;        /**< Receive error packet */
    uint32 rxDiscardPkt;    /**< Receive discard packet */
    uint32 rxMinLenVioPackets;  /**< Min length Violation Frames */
    uint32 rxMinLenVioBytes;    /**< Min length Violation Bytes */
    uint32 rxMaxLenVioPackets;  /**< Max length Violation Frames */
    uint32 rxMaxLenVioBytes;    /**< Max length Violation Bytes */
    }tAtHdlcChannelCounters;

/** @brief Counter type */
typedef enum eAtHdlcChannelCounterType
    {
    cAtHdlcChannelCounterTypeTxPackets,           /**< Transmitted Packet counter */
    cAtHdlcChannelCounterTypeTxBytes,             /**< Transmitted Bytes counter */
    cAtHdlcChannelCounterTypeTxGoodPackets,       /**< Transmitted Good packet counter */
    cAtHdlcChannelCounterTypeTxGoodBytes,         /**< Transmitted Good byte counter */
    cAtHdlcChannelCounterTypeTxFragments,         /**< Transmit Fragments counter (with MLPPP/MLFR header) */
    cAtHdlcChannelCounterTypeTxPlain,             /**< Transmit Plain packets counter (without MLPPP/MLFR header) */
    cAtHdlcChannelCounterTypeTxOam,               /**< Transmit Control packets counter */
    cAtHdlcChannelCounterTypeTxIdleBytes,         /**< Transmitted Idle bytes counter */
    cAtHdlcChannelCounterTypeTxAborts,            /**< Transmitted Abort frames */
    cAtHdlcChannelCounterTypeTxMTUPackets,        /**< Transmit MTU exceeded counter*/
    cAtHdlcChannelCounterTypeTxDiscardFrames,     /**< Transmit discard counter (maybe because of buffer congestion) */
    cAtHdlcChannelCounterTypeRxPackets,           /**< Receive Packets counter */
    cAtHdlcChannelCounterTypeRxBytes,             /**< Received Bytes counter */
    cAtHdlcChannelCounterTypeRxGoodPackets,       /**< Receive Good packet counter */
    cAtHdlcChannelCounterTypeRxGoodBytes,         /**< Receive Good bytes counter */
    cAtHdlcChannelCounterTypeRxFragments,         /**< Receive Fragments counter (with MLPPP/MLFR header) */
    cAtHdlcChannelCounterTypeRxPlains,            /**< Receive Plain packets counter (without MLPPP/MLFR header) */
    cAtHdlcChannelCounterTypeRxOams,              /**< Receive Control packets counter */
    cAtHdlcChannelCounterTypeRxIdleBytes,         /**< Received Idle bytes counter */
    cAtHdlcChannelCounterTypeRxMRUPackets,        /**< Received Idle bytes counter */
    cAtHdlcChannelCounterTypeRxAborts,            /**< Received Abort frames counter */
    cAtHdlcChannelCounterTypeRxFcsErrors,         /**< Received FCS error counter */
    cAtHdlcChannelCounterTypeRxAddrCtrlErrors,    /**< Received Address-Control error */
    cAtHdlcChannelCounterTypeRxDiscardFrames,     /**< Receive Discard frames counter */
    cAtHdlcChannelCounterTypeRxUndersizePackets,  /**< Receive Undersized Packets */
    cAtHdlcChannelCounterTypeRxUndersizeBytes,    /**< Receive Undersized Bytes */
    cAtHdlcChannelCounterTypeRxOversizePackets,   /**< Receive Oversized Packets */
    cAtHdlcChannelCounterTypeRxOversizeBytes,     /**< Receive Oversized Bytes */
    cAtHdlcChannelCounterTypeEnd = cAtHdlcChannelCounterTypeRxOversizeBytes
    }eAtHdlcChannelCounterType;

/**
 * @brief HDLC channel configuration
 */
typedef struct tAtHdlcChannelAllConfig
    {
    eBool               scrEn;                /**< Enable/Disable scrambling */
    eBool               addrCmpEn;            /**< Enable/Disable comparing Address field with expected value */
    uint8               addrExpVal;           /**< Address field expected value */
    eBool               ctrlCmpEn;            /**< Enable/Disable comparing Address field with expected value */
    uint8               ctrlExpVal;           /**< Control field expected value */
    eBool               addrInsEn;            /**< Enable/Disable insertion Address field with expected value */
    uint8               addrInsVal;           /**< Address field insert value */
    eBool               ctrlInsEn;            /**< Enable/Disable insertion Address field with expected value */
    uint8               ctrlInsVal;           /**< Control field insert value */
    eAtHdlcFcsMode      fcsMode;              /**< Length of FCS field: 2bytes or 4 bytes */
    uint8               flag;                 /**< Flag value */
    uint16              mtu;                  /**< MTU */
    eAtHdlcStuffMode    stuffingMd;           /**< Stuffing mode */
    } tAtHdlcChannelAllConfig;

/** @brief HDLC alarm types */
typedef enum eAtHdlcChannelAlarmType
    {
    cAtHdlcChannelAlarmTypeNone            = 0,      /**< No alarm */
    cAtHdlcChannelAlarmTypeFcsError        = cBit0,  /**< FCS error */
    cAtHdlcChannelAlarmTypeUndersize       = cBit1,  /**< Undersized packet has been detected */
    cAtHdlcChannelAlarmTypeOversize        = cBit2,  /**< Oversized packet has been detected */
    cAtHdlcChannelAlarmTypeDecapBufferFull = cBit3,  /**< Buffer is full on Decapsulation path */
    cAtHdlcChannelAlarmTypeEncapBufferFull = cBit4   /**< Buffer is full on Encapsulation path */
    }eAtHdlcChannelAlarmType;

/**
 * @brief FCS calculation mode
 */
typedef enum eAtHdlcFcsCalculationMode
    {
    cAtHdlcFcsCalculationModeUnknown, /**< Unknown */
    cAtHdlcFcsCalculationModeLsb,     /**< Calculation FCS From LSB */
    cAtHdlcFcsCalculationModeMsb      /**< Calculation FCS From MSB */
    }eAtHdlcFcsCalculationMode;

/** @brief Types of Protocol Data Unit */
typedef enum eAtHdlcPduType
    {
    cAtHdlcPduTypeAny,      /**< All types of PDU */
    cAtHdlcPduTypeIPv4,     /**< IPv4 PDU */
    cAtHdlcPduTypeIPv6,     /**< IPv6 PDU */
    cAtHdlcPduTypeEthernet  /**< ETH PDU */
    }eAtHdlcPduType;

/**
 * @brief HDLC packet-dropping conditions
 */
typedef enum eAtHdlcChannelDropCondition
    {
    cAtHdlcChannelDropConditionNone                 = 0,       /**< Do not  drop any packet */
    cAtHdlcChannelDropConditionPktFcsError          = cBit0,   /**< Received packet with FCS Error */
    cAtHdlcChannelDropConditionPktAddrCtrlError     = cBit1    /**< Received packet with mismatched address/control fields */
    }eAtHdlcChannelDropCondition;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcLink AtHdlcChannelHdlcLinkGet(AtHdlcChannel self);

/* Frame type */
eAtHdlcFrameType AtHdlcChannelFrameTypeGet(AtHdlcChannel self);
eAtModuleEncapRet AtHdlcChannelFrameTypeSet(AtHdlcChannel self, uint8 frameType);

/* Address field */
eAtModuleEncapRet AtHdlcChannelAddressSizeSet(AtHdlcChannel self, uint8 numberOfByte);
uint8 AtHdlcChannelAddressSizeGet(AtHdlcChannel self);
eAtModuleEncapRet AtHdlcChannelTxAddressSet(AtHdlcChannel self, uint8 *address);
eAtModuleEncapRet AtHdlcChannelTxAddressGet(AtHdlcChannel self, uint8 *address);
eAtModuleEncapRet AtHdlcChannelExpectedAddressSet(AtHdlcChannel self, uint8 *address);
eAtModuleEncapRet AtHdlcChannelExpectedAddressGet(AtHdlcChannel self, uint8 *address);

/* Control field */
eAtModuleEncapRet AtHdlcChannelControlSizeSet(AtHdlcChannel self, uint8 numberOfByte);
uint8 AtHdlcChannelControlSizeGet(AtHdlcChannel self);
eAtModuleEncapRet AtHdlcChannelTxControlSet(AtHdlcChannel self, uint8 *control);
eAtModuleEncapRet AtHdlcChannelTxControlGet(AtHdlcChannel self, uint8 *control);
eAtModuleEncapRet AtHdlcChannelExpectedControlSet(AtHdlcChannel self, uint8 *control);
eAtModuleEncapRet AtHdlcChannelExpectedControlGet(AtHdlcChannel self, uint8 *control);

/* Address/control field compression */
eAtModuleEncapRet AtHdlcChannelAddressControlCompress(AtHdlcChannel self, eBool compress);
eBool AtHdlcChannelAddressControlIsCompressed(AtHdlcChannel self);
eAtModuleEncapRet AtHdlcChannelTxAddressControlCompress(AtHdlcChannel self, eBool compress);
eBool AtHdlcChannelTxAddressControlIsCompressed(AtHdlcChannel self);
eAtModuleEncapRet AtHdlcChannelRxAddressControlCompress(AtHdlcChannel self, eBool compress);
eBool AtHdlcChannelRxAddressControlIsCompressed(AtHdlcChannel self);

/* Address/control fields insertion */
eAtModuleEncapRet AtHdlcChannelAddressInsertionEnable(AtHdlcChannel self, eBool enable);
eBool AtHdlcChannelAddressInsertionIsEnabled(AtHdlcChannel self);
eAtModuleEncapRet AtHdlcChannelControlInsertionEnable(AtHdlcChannel self, eBool enable);
eBool AtHdlcChannelControlInsertionIsEnabled(AtHdlcChannel self);

/* Address/control fields comparison */
eAtModuleEncapRet AtHdlcChannelAddressMonitorEnable(AtHdlcChannel self, eBool enable);
eBool AtHdlcChannelAddressMonitorIsEnabled(AtHdlcChannel self);
eAtModuleEncapRet AtHdlcChannelControlMonitorEnable(AtHdlcChannel self, eBool enable);
eBool AtHdlcChannelControlMonitorIsEnabled(AtHdlcChannel self);

/* Address/control bypassing to Ethernet side */
eAtModuleEncapRet AtHdlcChannelAddressControlBypass(AtHdlcChannel self, eBool bypass);
eBool AtHdlcChannelAddressControlIsBypassed(AtHdlcChannel self);

/* Flag */
eAtModuleEncapRet AtHdlcChannelFlagSet(AtHdlcChannel self, uint8 flag);
uint8 AtHdlcChannelFlagGet(AtHdlcChannel self);
eAtModuleEncapRet AtHdlcChannelNumFlagsSet(AtHdlcChannel self, uint8 numFlag);
uint8 AtHdlcChannelNumFlagsGet(AtHdlcChannel self);

/* FCS controlling */
eAtModuleEncapRet AtHdlcChannelFcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode);
eAtHdlcFcsMode AtHdlcChannelFcsModeGet(AtHdlcChannel self);
eAtModuleEncapRet AtHdlcChannelFcsCalculationModeSet(AtHdlcChannel self, eAtHdlcFcsCalculationMode mode);
eAtHdlcFcsCalculationMode AtHdlcChannelFcsCalculationModeGet(AtHdlcChannel self);

/* Scrambling */
eAtModuleEncapRet AtHdlcChannelScrambleEnable(AtHdlcChannel self, eBool scrambleEnable);
eBool AtHdlcChannelScrambleIsEnabled(AtHdlcChannel self);

/* Stuffing mode */
eAtModuleEncapRet AtHdlcChannelStuffModeSet(AtHdlcChannel self, eAtHdlcStuffMode stuffMode);
eAtHdlcStuffMode AtHdlcChannelStuffModeGet(AtHdlcChannel self);

/* MTU */
eAtModuleEncapRet AtHdlcChannelMtuSet(AtHdlcChannel self, uint32 mtu);
uint32 AtHdlcChannelMtuGet(AtHdlcChannel self);

/* Idle */
eAtModuleEncapRet AtHdlcChannelIdlePatternSet(AtHdlcChannel self, uint8 idle);
uint8 AtHdlcChannelIdlePatternGet(AtHdlcChannel self);


#ifdef __cplusplus
}
#endif
#endif /* _ATHDLCCHANNEL_H_ */

#include "AtHdlcChannelDeprecated.h"


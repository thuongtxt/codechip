/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : AtHdlcLink.h
 * 
 * Created Date: Aug 3, 2012
 *
 * Description : HDLC Link abstract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHDLCLINK_H_
#define _ATHDLCLINK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEncap.h"
#include "AtEthFlow.h"
#include "AtHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtHdlcLink
 * @{
 */

/**
 * @brief OAM packet mode on HDLC Link
 */
typedef enum eAtHdlcLinkOamMode
    {
    cAtHdlcLinkOamModeInvalid, /** < Invalid */
    cAtHdlcLinkOamModeToPsn,   /** < Send to PSN */
    cAtHdlcLinkOamModeToCpu,   /** < Send to CPU */
    cAtHdlcLinkOamModeDiscard  /** < Discard */
    }eAtHdlcLinkOamMode;

/** @brief Counter type */
typedef enum eAtHdlcLinkCounterType
    {
    cAtHdlcLinkCounterTypeUnKnown,              /**< Unknown counter type */
    cAtHdlcLinkCounterTypeTxByte,               /**< Transmit bytes counter  */
    cAtHdlcLinkCounterTypeTxPkt,                /**< Transmit frames counter */
    cAtHdlcLinkCounterTypeTxGoodByte,           /**< Transmit good bytes counter */
    cAtHdlcLinkCounterTypeTxGoodPkt,            /**< Transmit good frames counter */
    cAtHdlcLinkCounterTypeTxFragments,          /**< Transmit fragments  counter */
    cAtHdlcLinkCounterTypeTxPlains,             /**< Transmit plain packets counter */
    cAtHdlcLinkCounterTypeTxOamsPkt,            /**< Transmit control packets counter */
    cAtHdlcLinkCounterTypeTxAbortPkt,           /**< Transmit Aborted frames counter */
    cAtHdlcLinkCounterTypeTxIdleByte,           /**< Transmit Idle flag counter (byte stuffing only)*/
    cAtHdlcLinkCounterTypeTxExceedMtuPkt,       /**< Transmit MTU exceeded counter */
    cAtHdlcLinkCounterTypeRxByte,               /**< Receive bytes counter */
    cAtHdlcLinkCounterTypeRxPkt,                /**< Receive frames counter */
    cAtHdlcLinkCounterTypeRxGoodByte,           /**< Receive good bytes counter */
    cAtHdlcLinkCounterTypeRxGoodPkt,            /**< Receive good frames counter */
    cAtHdlcLinkCounterTypeRxAbort,              /**< Receive Aborted frames counter */
    cAtHdlcLinkCounterTypeRxIdleByte,           /**< Receive Idle flag counter (byte stuffing only) */
    cAtHdlcLinkCounterTypeRxFcsPkt,             /**< Receive FCS error counter */
    cAtHdlcLinkCounterTypeRxPlains,             /**< Receive plain packets counter */
    cAtHdlcLinkCounterTypeRxOamsPkt,            /**< Receive control packets counter */
    cAtHdlcLinkCounterTypeRxFragments,          /**< Receive fragments  counter */
    cAtHdlcLinkCounterTypeRxExceedMruPkt,       /**< Receive MRU exceeded counter */
    cAtHdlcLinkCounterTypeRxDiscardPkts         /**< Receive Discard frames counter  */
    }eAtHdlcLinkCounterType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/
/* Get HDLC channel that this link belongs to */
AtHdlcChannel AtHdlcLinkHdlcChannelGet(AtHdlcLink self);

/* Get bundle that this link is added to */
AtHdlcBundle AtHdlcLinkBundleGet(AtHdlcLink self);

/* Protocol compression */
eAtModuleEncapRet AtHdlcLinkProtocolCompress(AtHdlcLink self, eBool compress);
eBool AtHdlcLinkProtocolIsCompressed(AtHdlcLink self);

/* Traffic binding */
eAtModuleEncapRet AtHdlcLinkFlowBind(AtHdlcLink self, AtEthFlow flow);
AtEthFlow AtHdlcLinkBoundFlowGet(AtHdlcLink self);

/* OAM */
eAtModuleEncapRet AtHdlcLinkOamPacketModeSet(AtHdlcLink self, eAtHdlcLinkOamMode oamMode);
eAtHdlcLinkOamMode AtHdlcLinkOamPacketModeGet(AtHdlcLink self);
eAtModuleEncapRet AtHdlcLinkOamSend(AtHdlcLink self, uint8 *buffer, uint32 bufferLength);
uint32 AtHdlcLinkOamReceive(AtHdlcLink self, uint8 *buffer, uint32 bufferLength);
eBool AtHdlcLinkAllOamsSent(AtHdlcLink self);

/* OAM flow */
AtEthFlow AtHdlcLinkOamFlowGet(AtHdlcLink self);

/* Protocol Data Unit */
eAtModuleEncapRet AtHdlcLinkPduTypeSet(AtHdlcLink self, eAtHdlcPduType pduType);
eAtHdlcPduType AtHdlcLinkPduTypeGet(AtHdlcLink self);

#ifdef __cplusplus
}
#endif
#endif /* _ATHDLCLINK_H_ */

#include "AtHdlcLinkDeprecated.h"


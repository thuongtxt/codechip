/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : AtBerController.h
 * 
 * Created Date: Feb 6, 2013
 *
 * Description : BER controller abstract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATBERCONTROLLER_H_
#define _ATBERCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtObject.h" /* Super class */
#include "AtModuleBer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModuleBerRet AtBerControllerInit(AtBerController self);
uint32 AtBerControllerIdGet(AtBerController self);
AtChannel AtBerControllerMonitoredChannel(AtBerController self);
AtModule AtBerControllerModuleGet(AtBerController self);

eAtModuleBerRet AtBerControllerEnable(AtBerController self, eBool enable);
eBool AtBerControllerIsEnabled(AtBerController self);

eAtModuleBerRet AtBerControllerSdThresholdSet(AtBerController self, eAtBerRate threshold);
eAtBerRate AtBerControllerSdThresholdGet(AtBerController self);
eAtModuleBerRet AtBerControllerSfThresholdSet(AtBerController self, eAtBerRate threshold);
eAtBerRate AtBerControllerSfThresholdGet(AtBerController self);
eAtModuleBerRet AtBerControllerTcaThresholdSet(AtBerController self, eAtBerRate threshold);
eAtBerRate AtBerControllerTcaThresholdGet(AtBerController self);

eAtBerRate AtBerControllerCurBerGet(AtBerController self);
eBool AtBerControllerIsSd(AtBerController self);
eBool AtBerControllerIsSf(AtBerController self);
eBool AtBerControllerIsTca(AtBerController self);
eBool AtBerControllerSdHistoryGet(AtBerController self, eBool clear);
eBool AtBerControllerSfHistoryGet(AtBerController self, eBool clear);
eBool AtBerControllerTcaHistoryGet(AtBerController self, eBool clear);

/* Concrete controllers */
AtBerController AtSdhLineRsBerSoftControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController AtSdhLineMsBerSoftControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);
AtBerController AtSdhPathBerSoftControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);

void AtBerControllerDebugOn(AtBerController self, eBool debugOn);
eBool AtBerControllerDebugIsOn(AtBerController self);
uint32 AtBerControllerExpectedDetectionTimeInMs(AtBerController self, eAtBerRate rate);
uint32 AtBerControllerExpectedClearingTimeInMs(AtBerController self, eAtBerRate rate);
uint32 AtBerControllerMeasureEngineId(AtBerController self);
uint8  AtBerControllerPartId(AtBerController self);

#ifdef __cplusplus
}
#endif
#endif /* _ATBERCONTROLLER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : AtBerMeasure.h
 * 
 * Created Date: Nov 13, 2017
 *
 * Description : BER-Detection and clearing time Measurement.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATBERMEASURE_H_
#define _ATBERMEASURE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtObject.h" /* Super class */
#include "AtModuleBer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtBerTimerType
    {
    cAtBerTimerTypeSfDetection,
    cAtBerTimerTypeSdDetection,
    cAtBerTimerTypeTcaDetection,
    cAtBerTimerTypeSfClearing,
    cAtBerTimerTypeSdClearing,
    cAtBerTimerTypeTcaClearing
    } eAtBerTimerType;

typedef enum eAtBerMeasureState
    {
    cAtBerMeasureStateUnKnown,
    cAtBerMeasureStateWaitErrorStart,
    cAtBerMeasureStateWaitSfDetectionTime,
    cAtBerMeasureStateWaitErrorStop,
    cAtBerMeasureStateWaitSfClearingTime,
    cAtBerMeasureStateWaitSdClearingTime,
    cAtBerMeasureStateWaitTcaClearingTime,
    cAtBerMeasureStateOneShotDone,
    cAtBerMeasureStateStopInContinous,
    cAtBerMeasureStateWaitToLachInManual,
    cAtBerMeasureStateLatchedInManual
    } eAtBerMeasureState;

typedef enum eAtBerMeasureMode
    {
    cAtBerMeasureModeUnKnown,
    cAtBerMeasureModeManaual,
    cAtBerMeasureModeContinousNoneStop,
    cAtBerMeasureModeContinousAutoStop,
    cAtBerMeasureModeOneShot
    } eAtBerMeasureMode;

typedef struct tAtEstimatedTime
    {
    uint32 estimateTime;
    uint32 estimateTimeMin;
    uint32 estimateTimeMax;
    }tAtEstimatedTime;

typedef struct tAtBerMeasureEstimatedTime
    {
    tAtEstimatedTime sfDetection;
    tAtEstimatedTime sfClearing;
    tAtEstimatedTime sdClearing;
    tAtEstimatedTime tcaClearing;
    uint32 numberOfEstimation;
    }tAtBerMeasureEstimatedTime;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Get controller */
AtBerController AtBerMeasureTimeEngineControllerGet(AtBerMeasureTimeEngine self);

/* Get state */
eAtBerMeasureState AtBerMeasureTimeEngineStateGet(AtBerMeasureTimeEngine self);

/* Set/Get Mode */
eAtRet AtBerMeasureTimeEngineModeSet(AtBerMeasureTimeEngine self, eAtBerMeasureMode mode);
eAtBerMeasureMode AtBerMeasureTimeEngineModeGet(AtBerMeasureTimeEngine self);

/* Get timer base on type */
eAtRet AtBerMeasureTimeEngineEstimatedTimesGet(AtBerMeasureTimeEngine self, void *estimateTimes);
eAtRet AtBerMeasureTimeEngineEstimatedTimesClear(AtBerMeasureTimeEngine self);

/* Connect Input Error to BER controller, default when don't have BER measure */
eAtRet AtBerMeasureTimeEngineInputErrorConnect(AtBerMeasureTimeEngine self);

/* Disconnect Input Error to BER controller (for sync time when measure clearing time)*/
eAtRet AtBerMeasureTimeEngineInputErrorDisConnect(AtBerMeasureTimeEngine self);

/* To handle measure state */
uint32 AtBerMeasureTimeEnginePeriodProcess(AtBerMeasureTimeEngine self);

/* To start polling handle task (run state machine to collect result in do database or list) */
eAtRet AtBerMeasureTimeEnginePollingStart(AtBerMeasureTimeEngine self);

/* To stop polling handle task (stop run state-machine, but still can show result)*/
eAtRet AtBerMeasureTimeEnginePollingStop(AtBerMeasureTimeEngine self);

/* Instant */
AtBerMeasureTimeEngine ThaBerMeasureTimeHardEngineNew(AtBerController controller);
AtBerMeasureTimeEngine ThaBerMeasureTimeSoftEngineNew(AtBerController controller);

#ifdef __cplusplus
}
#endif
#endif /* _ATBERMEASURE_H_ */


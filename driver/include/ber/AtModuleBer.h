/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : AtModuleBerInternal.h
 *
 * Created Date: Aug 31, 2012
 *
 * Author      : nnnam
 *
 * Description : BER monitoring abstract module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEBER_H_
#define _ATMODULEBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h" /* Super class */
#include "AtChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleBer
 * @{
 */

/** @brief BER module return codes */
typedef uint32 eAtModuleBerRet;

/** @brief BER module abstract class */
typedef struct tAtModuleBer * AtModuleBer;

/** @brief BER controller abstract class */
typedef struct tAtBerController * AtBerController;

/** @brief BER Measure abstract class */
typedef struct tAtBerMeasureTimeEngine * AtBerMeasureTimeEngine;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ---------	-------------------------------*/
uint32 AtModuleBerPeriodicProcess(AtModuleBer self, uint32 periodInMs);

/* Manage BER controller */
AtBerController AtModuleBerSdhLineRsBerControlerCreate(AtModuleBer self, AtChannel line);
AtBerController AtModuleBerSdhLineMsBerControlerCreate(AtModuleBer self, AtChannel line);
AtBerController AtModuleBerSdhPathBerControlerCreate(AtModuleBer self, AtChannel path);
AtBerController AtModuleBerPdhChannelPathBerControlerCreate(AtModuleBer self, AtChannel pdhChannel);
AtBerController AtModuleBerPdhChannelLineBerControlerCreate(AtModuleBer self, AtChannel pdhChannel);
void AtModuleBerControllerDelete(AtModuleBer self, AtBerController controller);

/* Measure Engine */
AtBerMeasureTimeEngine AtModuleBerMeasureTimeSoftEngineCreate(AtModuleBer self, AtBerController controller);
AtBerMeasureTimeEngine AtModuleBerMeasureTimeHardEngineCreate(AtModuleBer self, AtBerController controller);
eAtRet AtModuleBerMeasureTimeEngineDelete(AtModuleBer self, AtBerMeasureTimeEngine engine);
AtBerMeasureTimeEngine AtModuleBerMeasureTimeEngineGet(AtModuleBer self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEBER_H_ */

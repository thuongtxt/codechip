/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtPtpPsnGroup.h
 * 
 * Created Date: Jul 18, 2018
 *
 * Description : Abstract PTP multicast group
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPTPPSNGROUP_H_
#define _ATPTPPSNGROUP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePtp.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPtpPsnGroup
 * @{
 */

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtPtpPsnType AtPtpPsnGroupTypeGet(AtPtpPsnGroup self);
uint32 AtPtpPsnGroupIdGet(AtPtpPsnGroup self);
const char * AtPtpPsnGroupTypeString(AtPtpPsnGroup self);
AtModulePtp AtPtpPsnGroupModuleGet(AtPtpPsnGroup self);

/* Address (multicast) */
eAtModulePtpRet AtPtpPsnGroupAddressSet(AtPtpPsnGroup self, const uint8 *address);
eAtModulePtpRet AtPtpPsnGroupAddressGet(AtPtpPsnGroup self, uint8 *address);

/* Port manipulating */
eAtModulePtpRet AtPtpPsnGroupPortAdd(AtPtpPsnGroup self, AtPtpPort port);
eAtModulePtpRet AtPtpPsnGroupPortRemove(AtPtpPsnGroup self, AtPtpPort port);
AtPtpPort AtPtpPsnGroupPortGet(AtPtpPsnGroup self, uint32 atIndex);
AtIterator AtPtpPsnGroupPortIteratorCreate(AtPtpPsnGroup self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPTPPSNGROUP_H_ */


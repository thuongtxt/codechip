/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtModulePtp.h
 * 
 * Created Date: May 10, 2018
 *
 * Description : Precision Time Protocol module.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPTP_H_
#define _ATMODULEPTP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtEthVlanTag.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePtp
 * @{
 */

/** @brief Abstract PTP module */
typedef struct tAtModulePtp *AtModulePtp;

/** @brief Abstract PTP port */
typedef struct tAtPtpPort *AtPtpPort;

/** @brief PTP PSN abstract class */
typedef struct tAtPtpPsn * AtPtpPsn;

/** @brief Abstract PTP multicast group class */
typedef struct tAtPtpPsnGroup * AtPtpPsnGroup;

/**
 *@brief PTP device type
 */
typedef enum eAtPtpMsgType
    {
    cAtPtpMsgTypeSync = 0,          /**< PTP Sync Message */
    cAtPtpMsgTypeFollowUp = 1,      /**< PTP Followup Message */
    cAtPtpMsgTypeDelayRequest = 2,  /**< PTP Delay Request Message */
    cAtPtpMsgTypeDelayRespond = 3,  /**< PTP Delay Respond Message */
    cAtPtpMsgTypeUnknown = 4        /**< PTP Unknown Message */
    }eAtPtpMsgType;

/**
 *@brief PTP device type
 */
typedef enum eAtPtpDeviceType
    {
    cAtPtpDeviceTypeUnknown,         /**< Unknown clock mode */
    cAtPtpDeviceTypeOrdinary,        /**< PTP Ordinary Clock that has a single PTP
                                         port in a domain and maintains the
                                         timescale used in the domain. */
    cAtPtpDeviceTypeBoundary,        /**< PTP Boundary Clock that has multiple PTP
                                         ports in a domain and maintains the
                                         timescale used in the domain. The master port
                                         synchronizes the clocks connected downstream,
                                         while the slave port synchronizes with the
                                         upstream master clock. */
    cAtPtpDeviceTypeTransparent      /**< PTP Transparent Clock is used
                                         in PTP devices, which measure message
                                         transmit time and update the time-interval
                                         field of PTP event messages. */
    }eAtPtpDeviceType;

/**
 *@brief PTP PPS time source
 */
typedef enum eAtPtpPpsSource
    {
    cAtPtpPpsSourceUnknown,  /**< Unknown PPS source */
    cAtPtpPpsSourceInternal, /**< PPS is from internal free-running source */
    cAtPtpPpsSourceExternal  /**< PPS is from external source (+1pps clock pin) */
    }eAtPtpPpsSource;

/**
 *@brief PTP PSN type
 */
typedef enum eAtPtpPsnType
    {
    cAtPtpPsnTypeUnknown, /**< Unknown */
    cAtPtpPsnTypeLayer2,  /**< MAC */
    cAtPtpPsnTypeIpV4,    /**< IPV4 */
    cAtPtpPsnTypeIpV6     /**< IPV6 */
    }eAtPtpPsnType;

/**
 *@brief PTP return codes
 */
typedef uint32 eAtModulePtpRet;

/**
 *@brief PTP Correction Mode (in Transparent Clock)
 */
typedef enum eAtPtpCorrectionMode
    {
    cAtPtpCorrectionModeUnknown,     /**< Unknown correction mode */
    cAtPtpCorrectionModeStandard,    /**< The residence time shall be added to
                                          the correctionField of PTP Sync/DelayReq packets. */
    cAtPtpCorrectionModeAssist       /**< The time arrival of PTP packets shall
                                          be latched and inserted into the
                                          reserved field of PTP Sync/DelayReq packets. */
    }eAtPtpCorrectionMode;

/**
 *@brief PTP T1` and T3` latch to CPU queue or send PTP packet back to backplane
 */
typedef enum eAtPtpT1T3TimeStampCaptureMode
    {
    cAtPtpT1T3TimestampCaptureModeNone,     /**< No capture */
    cAtPtpT1T3TimestampCaptureModeCpu,      /**< Timestamp T1' and T3' are captured
                                                 at the physical interface and stored
                                                 in CPU queue */
    cAtPtpT1T3TimestampCaptureModePacket    /**< Timestamp T1' and T3' are captured
                                                 at the physical interface and
                                                 automatically sent back to the backplane */
    }eAtPtpT1T3TimeStampCaptureMode;

/** @brief Module PTP alarm */
typedef enum eAtModulePtpAlarmType
    {
    cAtModulePtpAlarmTypeCpuQueu     = cBit0  /**< CPU queue having PTP T1' and T3' timestamp packet */
    }eAtModulePtpAlarmType;

/** @brief PTP T1'/T3' timestamp capture structure */
typedef struct tAtPtpT1T3TimestampContent
    {
    AtPtpPort     egressPort;       /**< Outgoing PTP port */
    eAtPtpMsgType ptpMsgType;       /**< Message type */
    uint16        sequenceNumber;   /**< Message sequence number */
    uint64        seconds;          /**< Timestamp in seconds */
    uint32        nanoSeconds;      /**< Timestamp in nanoseconds */
    }tAtPtpT1T3TimestampContent;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* PTP ports */
uint8 AtModulePtpNumPortsGet(AtModulePtp self);
AtPtpPort AtModulePtpPortGet(AtModulePtp self, uint32 portId);

/* PTP Device Type */
eAtModulePtpRet AtModulePtpDeviceTypeSet(AtModulePtp self, eAtPtpDeviceType deviceType);
eAtPtpDeviceType AtModulePtpDeviceTypeGet(AtModulePtp self);

/* Transparent Clock: Correction mode */
eAtModulePtpRet AtModulePtpCorrectionModeSet(AtModulePtp self, eAtPtpCorrectionMode correctMode);
eAtPtpCorrectionMode AtModulePtpCorrectionModeGet(AtModulePtp self);

/* PPS route-strip delay time (RTD) */
eAtModulePtpRet AtModulePtpPpsRouteTripDelaySet(AtModulePtp self, uint32 nanoseconds);
uint32 AtModulePtpPpsRouteTripDelayGet(AtModulePtp self);

/* PPS reference source. */
eAtModulePtpRet AtModulePtpPpsSourceSet(AtModulePtp self, eAtPtpPpsSource source);
eAtPtpPpsSource AtModulePtpPpsSourceGet(AtModulePtp self);

/* Time-of-day */
eAtModulePtpRet AtModulePtpTimeOfDaySet(AtModulePtp self, uint64 seconds);
uint64 AtModulePtpTimeOfDayGet(AtModulePtp self);

/* Base MAC address of PTP device */
eAtModulePtpRet AtModulePtpDeviceMacAddressSet(AtModulePtp self, const uint8 *address);
eAtModulePtpRet AtModulePtpDeviceMacAddressGet(AtModulePtp self, uint8 *address);
eAtModulePtpRet AtModulePtpDeviceMacAddressEnable(AtModulePtp self, eBool enable);
eBool AtModulePtpDeviceMacAddressIsEnabled(AtModulePtp self);

/* Multicast group */
uint32 AtModulePtpNumL2PsnGroupsGet(AtModulePtp self);
AtPtpPsnGroup AtModulePtpL2PsnGroupGet(AtModulePtp self, uint32 groupId);

uint32 AtModulePtpNumIpV4PsnGroupsGet(AtModulePtp self);
AtPtpPsnGroup AtModulePtpIpV4PsnGroupGet(AtModulePtp self, uint32 groupId);

uint32 AtModulePtpNumIpV6PsnGroupsGet(AtModulePtp self);
AtPtpPsnGroup AtModulePtpIpV6PsnGroupGet(AtModulePtp self, uint32 groupId);

/* PTP T1' and T3' capture mode */
eAtModulePtpRet AtModulePtpT1T3CaptureModeSet(AtModulePtp self, eAtPtpT1T3TimeStampCaptureMode mode);
eAtPtpT1T3TimeStampCaptureMode AtModulePtpT1T3CaptureModeGet(AtModulePtp self);
uint32 AtModulePtpT1T3CaptureContentGet(AtModulePtp self, tAtPtpT1T3TimestampContent *timestampInfoBuffer, uint32 numberOfTimestamp);

/* Expected VLANs */
eAtModulePtpRet AtModulePtpExpectedVlanSet(AtModulePtp self, uint8 index, tAtEthVlanTag *atag);
tAtEthVlanTag* AtModulePtpExpectedVlanGet(AtModulePtp self, uint8 index, tAtEthVlanTag *atag);
uint8 AtModulePtpNumExpectedVlan(AtModulePtp self);

/* Timestamp bypass for debugging */
eAtModulePtpRet AtModulePtpTimestampBypassEnable(AtModulePtp self, eBool enabled);
eBool AtModulePtpTimestampBypassIsEnabled(AtModulePtp self);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPTP_H_ */


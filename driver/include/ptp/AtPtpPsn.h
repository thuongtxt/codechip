/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtPtpPsn.h
 * 
 * Created Date: May 11, 2018
 *
 * Description : Packet Switch Network layer of PTP packets
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPTPPSN_H_
#define _ATPTPPSN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPtpPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPtpPsn
 * @{
 */

/** @} */

/**
 * @addtogroup AtPtpL2Psn
 * @{
 */
/** @brief PTP L2 PSN abstract class */
typedef struct tAtPtpL2Psn * AtPtpL2Psn;

/** @} */

/**
 * @addtogroup AtPtpIpPsn
 * @{
 */
/** @brief PTP IP PSN abstract class */
typedef struct tAtPtpIpPsn * AtPtpIpPsn;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* PSN APIs */
AtPtpPort AtPtpPsnPortGet(AtPtpPsn self);
eAtPtpPsnType AtPtpPsnTypeGet(AtPtpPsn self);
const char * AtPtpPsnTypeString(AtPtpPsn self);

/* Unicast destination address */
eAtModulePtpRet AtPtpPsnExpectedUnicastDestAddressSet(AtPtpPsn self, const uint8 *address);
eAtModulePtpRet AtPtpPsnExpectedUnicastDestAddressGet(AtPtpPsn self, uint8 *address);
eAtModulePtpRet AtPtpPsnExpectedUnicastDestAddressEnable(AtPtpPsn self, eBool enable);
eBool AtPtpPsnExpectedUnicastDestAddressIsEnabled(AtPtpPsn self);

/* Multicast destination address */
eAtModulePtpRet AtPtpPsnExpectedMcastDestAddressSet(AtPtpPsn self, const uint8 *address);
eAtModulePtpRet AtPtpPsnExpectedMcastDestAddressGet(AtPtpPsn self, uint8 *address);
eAtModulePtpRet AtPtpPsnExpectedMcastDestAddressEnable(AtPtpPsn self, eBool enable);
eBool AtPtpPsnExpectedMcastDestAddressIsEnabled(AtPtpPsn self);

/* Anycast destination address */
eAtModulePtpRet AtPtpPsnExpectedAnycastDestAddressEnable(AtPtpPsn self, eBool enable);
eBool AtPtpPsnExpectedAnycastDestAddressIsEnabled(AtPtpPsn self);

/* Unicast source address */
eAtModulePtpRet AtPtpPsnExpectedUnicastSourceAddressSet(AtPtpPsn self, const uint8 *address);
eAtModulePtpRet AtPtpPsnExpectedUnicastSourceAddressGet(AtPtpPsn self, uint8 *address);
eAtModulePtpRet AtPtpPsnExpectedUnicastSourceAddressEnable(AtPtpPsn self, eBool enable);
eBool AtPtpPsnExpectedUnicastSourceAddressIsEnabled(AtPtpPsn self);

/* Anycast source address */
eAtModulePtpRet AtPtpPsnExpectedAnycastSourceAddressEnable(AtPtpPsn self, eBool enable);
eBool AtPtpPsnExpectedAnycastSourceAddressIsEnabled(AtPtpPsn self);

/* Expected Vlan */
eAtModulePtpRet AtPtpPsnExpectedCVlanSet(AtPtpPsn self, tAtEthVlanTag *cTag);
tAtEthVlanTag* AtPtpPsnExpectedCVlanGet(AtPtpPsn self, tAtEthVlanTag *cTag);
eAtModulePtpRet AtPtpPsnExpectedSVlanSet(AtPtpPsn self, tAtEthVlanTag *sTag);
tAtEthVlanTag* AtPtpPsnExpectedSVlanGet(AtPtpPsn self, tAtEthVlanTag *sTag);

/* PSN IP APIs */
uint32 AtPtpIpPsnMaxNumUnicastDestAddressEntryGet(AtPtpIpPsn self);
eAtModulePtpRet AtPtpIpPsnExpectedUnicastDestAddressEntrySet(AtPtpIpPsn self, uint32 entryIndex, const uint8 *address);
eAtModulePtpRet AtPtpIpPsnExpectedUnicastDestAddressEntryGet(AtPtpIpPsn self, uint32 entryIndex, uint8 *address);
eAtModulePtpRet AtPtpIpPsnExpectedUnicastDestAddressEntryEnable(AtPtpIpPsn self, uint32 entryIndex, eBool enable);
eBool AtPtpIpPsnExpectedUnicastDestAddressEntryIsEnabled(AtPtpIpPsn self, uint32 entryIndex);

uint32 AtPtpIpPsnMaxNumUnicastSourceAddressEntryGet(AtPtpIpPsn self);
eAtModulePtpRet AtPtpIpPsnExpectedUnicastSourceAddressEntrySet(AtPtpIpPsn self, uint32 entryIndex, const uint8 *address);
eAtModulePtpRet AtPtpIpPsnExpectedUnicastSourceAddressEntryGet(AtPtpIpPsn self, uint32 entryIndex, uint8 *address);
eAtModulePtpRet AtPtpIpPsnExpectedUnicastSourceAddressEntryEnable(AtPtpIpPsn self, uint32 entryIndex, eBool enable);
eBool AtPtpIpPsnExpectedUnicastSourceAddressEntryIsEnabled(AtPtpIpPsn self, uint32 entryIndex);

/* Specifically for backplane/system side port which was not explicitly indicate unicast or multicast address */
uint32 AtPtpPsnMaxNumSourceAddressEntryGet(AtPtpPsn self);
uint32 AtPtpPsnMaxNumDestAddressEntryGet(AtPtpPsn self);
eAtModulePtpRet AtPtpPsnExpectedSourceAddressEntrySet(AtPtpPsn self, uint32 entryIndex, const uint8 *address);
eAtModulePtpRet AtPtpPsnExpectedSourceAddressEntryGet(AtPtpPsn self, uint32 entryIndex, uint8 *address);
eAtModulePtpRet AtPtpPsnExpectedSourceAddressEntryEnable(AtPtpPsn self, uint32 entryIndex, eBool enable);
eBool AtPtpPsnExpectedSourceAddressEntryIsEnabled(AtPtpPsn self, uint32 entryIndex);
eAtModulePtpRet AtPtpPsnExpectedDestAddressEntrySet(AtPtpPsn self, uint32 entryIndex, const uint8 *address);
eAtModulePtpRet AtPtpPsnExpectedDestAddressEntryGet(AtPtpPsn self, uint32 entryIndex, uint8 *address);
eAtModulePtpRet AtPtpPsnExpectedDestAddressEntryEnable(AtPtpPsn self, uint32 entryIndex, eBool enable);
eBool AtPtpPsnExpectedDestAddressEntryIsEnabled(AtPtpPsn self, uint32 entryIndex);

#ifdef __cplusplus
}
#endif
#endif /* _ATPTPPSN_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PTP
 * 
 * File        : AtPtpPort.h
 * 
 * Created Date: May 10, 2018
 *
 * Description : Abstract PTP port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPTPPORT_H_
#define _ATPTPPORT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePtp.h"
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPtpPort
 * @{
 */

/**
 * @brief PTP port state
 */
typedef enum eAtPtpPortState
    {
    cAtPtpPortStateUnknown,  /**< The PTP port is unknown state */
    cAtPtpPortStateMaster,   /**< The PTP port is the source of time on the path
                                  served by the port */
    cAtPtpPortStateSlave,    /**< The PTP port synchronizes to the device on the
                                  path with the port that is in master state. */
    cAtPtpPortStatePassive   /**< The PTP port is not the master on the path nor
                                  synchronizes to a master. */
    }eAtPtpPortState;

/**
 * @brief PTP step mode
 */
typedef enum eAtPtpStepMode
    {
    cAtPtpStepModeUnknown, /**< Unknown step mode */
    cAtPtpStepModeOneStep, /**< One-step clocks */
    cAtPtpStepModeTwoStep  /**< Two-step clocks */
    }eAtPtpStepMode;

/**
 *@brief PTP Network Transport Protocol
 */
typedef enum eAtPtpTransportType
    {
    cAtPtpTransportTypeUnknown, /**< Unknown */
    cAtPtpTransportTypeL2TP,    /**< Layer-2 transport (MAC IEEE 802.3) */
    cAtPtpTransportTypeIpV4,    /**< UDP/IPV4 */
    cAtPtpTransportTypeIpV6,    /**< UDP/IPV6 */
    cAtPtpTransportTypeIpV4Vpn, /**< UDP/IPv4 VPN (over MPLS) */
    cAtPtpTransportTypeIpV6Vpn,  /**< UDP/IPv6 VPN (over MPLS) */
    cAtPtpTransportTypeAny       /**< PTP packet at any layer */
    }eAtPtpTransportType;

/**
 * @brief PTP counter type
 */
typedef enum eAtPtpPortCounterType
    {
    cAtPtpPortCounterTypeTxPackets,                     /**< Number of Transmit PTP messages */
    cAtPtpPortCounterTypeTxSyncPackets,                 /**< Number of Transmit Sync messages */
    cAtPtpPortCounterTypeTxFollowUpPackets,             /**< Number of Transmit Follow_Up messages */
    cAtPtpPortCounterTypeTxDelayReqPackets,             /**< Number of Transmit Delay_Req messages */
    cAtPtpPortCounterTypeTxDelayRespPackets,            /**< Number of Transmit Delay_Resp messages */

    cAtPtpPortCounterTypeRxPackets,                     /**< Number of Receive PTP messages */
    cAtPtpPortCounterTypeRxSyncPackets,                 /**< Number of Receive Sync messages */
    cAtPtpPortCounterTypeRxFollowUpPackets,             /**< Number of Receive Follow_Up messages */
    cAtPtpPortCounterTypeRxDelayReqPackets,             /**< Number of Receive Delay_Req messages */
    cAtPtpPortCounterTypeRxDelayRespPackets,            /**< Number of Receive Delay_Resp messages */
    cAtPtpPortCounterTypeRxErroredPackets               /**< Number of Receive Errored PTP messages */
    }eAtPtpPortCounterType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Asymmetry delay */
eAtModulePtpRet AtPtpPortAsymmetryDelaySet(AtPtpPort self, uint32 nanoseconds);
uint32 AtPtpPortAsymmetryDelayGet(AtPtpPort self);

/* Transport protocol type */
eAtModulePtpRet AtPtpPortTransportTypeSet(AtPtpPort self, eAtPtpTransportType type);
eAtPtpTransportType AtPtpPortTransportTypeGet(AtPtpPort self);

/* PSN */
AtPtpPsn AtPtpPortPsnGet(AtPtpPort self, eAtPtpPsnType layer);

/* State */
eAtModulePtpRet AtPtpPortStateSet(AtPtpPort self, eAtPtpPortState state);
eAtPtpPortState AtPtpPortStateGet(AtPtpPort self);

/* Step mode */
eAtModulePtpRet AtPtpPortStepModeSet(AtPtpPort self, eAtPtpStepMode stepMode);
eAtPtpStepMode AtPtpPortStepModeGet(AtPtpPort self);

/* Ethernet */
AtEthPort AtPtpPortEthPortGet(AtPtpPort self);

/* Round trip delay compensation configuration */
eAtModulePtpRet AtPtpPortTxDelayAdjustSet(AtPtpPort self, uint32 nanoSeconds);
uint32 AtPtpPortTxDelayAdjustGet(AtPtpPort self);
eAtModulePtpRet AtPtpPortRxDelayAdjustSet(AtPtpPort self, uint32 nanoSeconds);
uint32 AtPtpPortRxDelayAdjustGet(AtPtpPort self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPTPPORT_H_ */


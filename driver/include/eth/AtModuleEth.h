/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : AtModuleEth.h
 * 
 * Created Date: Aug 11, 2012
 *
 * Description : Ethernet module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEETH_H_
#define _ATMODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h" /* Super class */
#include "AtIterator.h"
#include "AtSpiController.h"
#include "AtMdio.h"
#include "AtPhysicalClasses.h"
#include "AtModuleEncap.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* VLAN default TPIDs */
#define cAtModuleEthCVlanTpid 0x8100
#define cAtModuleEthSVlanTpid 0x88A8

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleEth
 * @{
 */

/** @brief Module Ethernet class */
typedef struct tAtModuleEth * AtModuleEth;

/** @brief Ethernet port class */
typedef struct tAtEthPort * AtEthPort;

/** @brief Ethernet flow class */
typedef struct tAtEthFlow * AtEthFlow;

/** @brief Ethernet flow control class */
typedef struct tAtEthFlowControl *AtEthFlowControl;

/** @brief Ethernet module return codes */
typedef uint32 eAtModuleEthRet;
enum
    {
    cAtModuleEthErrorStart = cAtErrorEthStartCode, /**< Start error code */
    cAtModuleEthErrorVlanNotExist,    /**< VLAN does not exist */
    cAtModuleEthErrorVlanAlreadyExist /**< VLAN already exists */
    };

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Manage Ethernet port */
uint8 AtModuleEthMaxPortsGet(AtModuleEth self);
AtEthPort AtModuleEthPortGet(AtModuleEth self, uint8 portId);

/* Manage Flow */
uint16 AtModuleEthMaxFlowsGet(AtModuleEth self);
AtEthFlow AtModuleEthFlowGet(AtModuleEth self, uint16 flowId);
AtEthFlow AtModuleEthFlowCreate(AtModuleEth self, uint16 flowId);
eAtModuleEthRet AtModuleEthFlowDelete(AtModuleEth self, uint16 flowId);
uint16 AtModuleEthFreeFlowIdGet(AtModuleEth self);

/* Flows Iterator */
AtIterator AtModuleEthFlowIteratorCreate(AtModuleEth self);

/* Ports Iterator */
AtIterator AtModuleEthPortIteratorCreate(AtModuleEth self);

/* VLAN TPIDs. All of objects that work with ETH Side such as AtEthFlow or AtPw
 * will use this default setting and they may have corresponding APIs to override
 * this */
eAtModuleEthRet AtModuleEthDefaultCVlanTpIdSet(AtModuleEth self, uint16 tpid);
uint16 AtModuleEthDefaultCVlanTpIdGet(AtModuleEth self);
eAtModuleEthRet AtModuleEthDefaultSVlanTpIdSet(AtModuleEth self, uint16 tpid);
uint16 AtModuleEthDefaultSVlanTpIdGet(AtModuleEth self);

/* ISIS MAC */
eAtModuleEthRet AtModuleEthISISMacSet(AtModuleEth self, const uint8 *mac);
eAtModuleEthRet AtModuleEthISISMacGet(AtModuleEth self, uint8 *mac);

/* SPI controller */
uint32 AtModuleEthNumSpiControllers(AtModuleEth self);
AtSpiController AtModuleEthSpiController(AtModuleEth self, uint32 phyId);

/* SERDES controller (physical) */
uint32 AtModuleEthNumSerdesControllers(AtModuleEth self);
AtSerdesController AtModuleEthSerdesController(AtModuleEth self, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEETH_H_ */

#include "AtModuleEthDeprecated.h"

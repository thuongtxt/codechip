/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : AtModuleEthDeprecated.h
 * 
 * Created Date: Jul 31, 2014
 *
 * Description : ETH module deprecated APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEETHDEPRECATED_H_
#define _ATMODULEETHDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlow AtModuleEthNopFlowCreate(AtModuleEth self, uint16 flowId);
AtEthFlow AtModuleEthEopFlowCreate(AtModuleEth self, uint16 flowId);
AtEthFlow AtModuleEthEoPDHFlowCreate(AtModuleEth self, uint16 flowId);
AtEthFlow AtModuleEthEoSFlowCreate(AtModuleEth self, uint16 flowId);
#endif /* _ATMODULEETHDEPRECATED_H_ */


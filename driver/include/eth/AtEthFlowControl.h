/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH module
 * 
 * File        : AtEthFlowControl.h
 * 
 * Created Date: Oct 14, 2013
 *
 * Description : ETH Flow Control
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATETHFLOWCONTROL_H_
#define _ATETHFLOWCONTROL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"
#include "AtChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Channel that flow control is working on */
AtChannel AtEthFlowControlChannelGet(AtEthFlowControl self);

/* Enable/Disable */
eAtModuleEthRet AtEthFlowControlEnable(AtEthFlowControl self, eBool enable);
eBool AtEthFlowControlIsEnabled(AtEthFlowControl self);

/* Threshold */
eAtModuleEthRet AtEthFlowControlHighThresholdSet(AtEthFlowControl self, uint32 threshold);
uint32 AtEthFlowControlHighThresholdGet(AtEthFlowControl self);
eAtModuleEthRet AtEthFlowControlLowThresholdSet(AtEthFlowControl self, uint32 threshold);
uint32 AtEthFlowControlLowThresholdGet(AtEthFlowControl self);
uint32 AtEthFlowControlTxFifoWaterMarkMaxGet(AtEthFlowControl self);
uint32 AtEthFlowControlTxFifoWaterMarkMaxClear(AtEthFlowControl self);
uint32 AtEthFlowControlTxFifoWaterMarkMinGet(AtEthFlowControl self);
uint32 AtEthFlowControlTxFifoWaterMarkMinClear(AtEthFlowControl self);
uint32 AtEthFlowControlTxFifoLevelGet(AtEthFlowControl self);

/* Flow control Ethernet header */
eAtModuleEthRet AtEthFlowControlDestMacSet(AtEthFlowControl self, uint8 *address);
eAtModuleEthRet AtEthFlowControlDestMacGet(AtEthFlowControl self, uint8 *address);
eAtModuleEthRet AtEthFlowControlSourceMacSet(AtEthFlowControl self, uint8 *address);
eAtModuleEthRet AtEthFlowControlSourceMacGet(AtEthFlowControl self, uint8 *address);
eAtModuleEthRet AtEthFlowControlEthTypeSet(AtEthFlowControl self, uint32 ethType);
uint32 AtEthFlowControlEthTypeGet(AtEthFlowControl self);

/* Pause frame period and quanta */
eAtModuleEthRet AtEthFlowControlPauseFramePeriodSet(AtEthFlowControl self, uint32 timeInterval);
uint32 AtEthFlowControlPauseFramePeriodGet(AtEthFlowControl self);
eAtModuleEthRet AtEthFlowControlPauseFrameQuantaSet(AtEthFlowControl self, uint32 quanta);
uint32 AtEthFlowControlPauseFrameQuantaGet(AtEthFlowControl self);

/* Counters */
uint16 AtEthFlowControlTxPauseFrameCounterGet(AtEthFlowControl self);
uint16 AtEthFlowControlTxPauseFrameCounterClear(AtEthFlowControl self);
uint16 AtEthFlowControlRxPauseFrameCounterGet(AtEthFlowControl self);
uint16 AtEthFlowControlRxPauseFrameCounterClear(AtEthFlowControl self);

#ifdef __cplusplus
}
#endif
#endif /* _ATETHFLOWCONTROL_H_ */


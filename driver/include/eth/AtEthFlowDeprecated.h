/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : AtEthFlowDeprecated.h
 * 
 * Created Date: Sep 3, 2015
 *
 * Description : Deprecated APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATETHFLOWDEPRECATED_H_
#define _ATETHFLOWDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModuleEthRet AtEthFlowEgressVlanSet(AtEthFlow self, const tAtEthVlanDesc *desc);
eAtModuleEthRet AtEthFlowEgressVlanGet(AtEthFlow self, tAtEthVlanDesc *desc);
uint32 AtEthFlowIngressAllVlansGet(AtEthFlow self, tAtEthVlanDesc *descs);

#ifdef __cplusplus
}
#endif
#endif /* _ATETHFLOWDEPRECATED_H_ */


/*-----------------------------------------------------------------------------
 *
 *COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 *The information contained herein is confidential property of Arrive Technologies.
 *The use, copying, transfer or disclosure of such information
 *is prohibited except by express written agreement with Arrive Technologies.
 *
 *Module      : Ethernet
 *
 *File        : AtEthFlow.h
 *
 *Created Date: Aug 11, 2012
 *
 *Description : Ethernet traffic flow class
 *
 *Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATETHFLOW_H_
#define _ATETHFLOW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h" /* Super class */
#include "AtChannelClasses.h"
#include "AtModuleEth.h"
#include "AtEthPort.h"
#include "AtEthVlanTag.h"
#include "AtModuleConcate.h"
#include "AtModuleEncap.h"
#include "AtModulePpp.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAtMaxIgTrafficDesc 8

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleEth
 * @{
 */

/**
 *@brief Ethernet flow counters
 */
typedef struct tAtEthFlowCounters
    {
    uint32 txByte;           /**< Transmit byte counter */
    uint32 txPacket;         /**< Transmit packet counter */
    uint32 txGoodByte;       /**< Transmit good byte counter */
    uint32 txFragment;       /**< Transmit fragment counter */
    uint32 txNullFragment;   /**< Transmit null fragments counter */
    uint32 txDiscardPacket;  /**< Transmit discard packet counter */
    uint32 txDiscardByte;    /**< Transmit discard byte counter */
    uint32 txGoodPkt;        /**< Transmit good packet counter */
    uint32 txGoodFragment;   /**< Transmit good fragment */
    uint32 txDiscardFragment;/**< Transmit discard fragment */
    uint32 windowViolation;  /**< Window violation counter */
    uint32 txBECNPacket;     /**< Transmit BECN packet counter */
    uint32 txFECNPacket;     /**< Transmit FECN packet counter */
    uint32 txDEPacket;       /**< Transmit DE packet counter */
    uint32 txTimesOut;       /**< Transmit Time Out packet counter */
    uint32 mrruExceed;       /**< MRRU exceeded counter */

    uint32 rxByte;           /**< Receive byte counter */
    uint32 rxPacket;         /**< Receive packet counter */
    uint32 rxGoodByte;       /**< Receive good byte counter */
    uint32 rxGoodPkt;        /**< Receive good packet counter */
    uint32 rxFragment;       /**< Receive fragment counter */
    uint32 rxNullFragment;   /**< Receive null fragments counter */
    uint32 rxDiscardPkt;     /**< Receive discard packet counter */
    uint32 rxDiscardByte;    /**< Receive discard packet byte */
    uint32 rxBECNPacket;     /**< Receive BECN packet counter */
    uint32 rxFECNPacket;     /**< Receive FECN packet counter */
    uint32 rxDEPacket;       /**< Receive DE packet counter */
    }tAtEthFlowCounters;

/** @brief Counter types */
typedef enum eAtEthFlowCounterType
    {
    cAtEthFlowCounterTxBytes,                   /**< Transmit byte counter */
    cAtEthFlowCounterTxPackets,                 /**< Transmit packet counter */
    cAtEthFlowCounterTxGoodPackets,             /**< Transmit good packet counter */
    cAtEthFlowCounterTxGoodBytes,               /**< Transmit good byte counter */
    cAtEthFlowCounterTxFragmentPackets,         /**< Transmit fragment counter */
    cAtEthFlowCounterTxGoodFragmentPackets,     /**< Transmit good fragment */
    cAtEthFlowCounterTxNullFragmentPackets,     /**< Transmit null fragments counter */
    cAtEthFlowCounterTxDiscardPackets,          /**< Transmit discard packet counter */
    cAtEthFlowCounterTxDiscardBytes,            /**< Transmit discard byte counter */
    cAtEthFlowCounterTxDiscardFragmentPackets,  /**< Transmit discard fragment */
    cAtEthFlowCounterTxTimeOuts,                /**< Transmit Time Out packet counter */
    cAtEthFlowCounterTxWindowViolationPackets,  /**< Transmit Window violation counter */
    cAtEthFlowCounterTxMRRUExceedPackets,       /**< Transmit MRRU exceeded counter */
    cAtEthFlowCounterTxBECNPackets,             /**< Transmit BECN exceeded counter */
    cAtEthFlowCounterTxFECNPackets,             /**< Transmit FECN exceeded counter */
    cAtEthFlowCounterTxDEPackets,               /**< Transmit DE packets counter */

    cAtEthFlowCounterRxBytes,                   /**< Receive byte counter */
    cAtEthFlowCounterRxGoodBytes,               /**< Receive good byte counter */
    cAtEthFlowCounterRxPackets,                 /**< Receive packet counter */
    cAtEthFlowCounterRxGoodPackets,             /**< Receive good packet counter */
    cAtEthFlowCounterRxFragmentPackets,         /**< Receive fragment counter */
    cAtEthFlowCounterRxGoodFragmentPackets,     /**< Receive good fragment */
    cAtEthFlowCounterRxNullFragmentPackets,     /**< Receive null fragments counter */
    cAtEthFlowCounterRxDiscardPackets,          /**< Receive discard packet counter */
    cAtEthFlowCounterRxDiscardBytes,            /**< Receive discard packet byte */
    cAtEthFlowCounterRxBECNPackets,             /**< Receive BECN packet byte */
    cAtEthFlowCounterRxFECNPackets,             /**< Receive FECN packet byte */
    cAtEthFlowCounterRxDEPackets                /**< Receive DE packet byte */
    }eAtEthFlowCounterType;

/**
 * @brief Ethernet flow configuration
 */
typedef struct tAtEthVlanFlowConfig
    {
    uint32 numberOfIgTrafficDesc;                      /**< Number of Ingress traffic descriptor */
    tAtEthVlanDesc igTrafficDesc[cAtMaxIgTrafficDesc]; /**< Ingress traffic descriptors */
    tAtEthVlanDesc egTrafficDesc;                      /**< Egress traffic descriptor */
    uint8 destMacAddress[cAtMacAddressLen];            /**< Destination MAC address */
    uint8 srcMacAddress[cAtMacAddressLen];             /**< Source MAC address for flow */
    eBool enable;                                      /**< Enable Egress and Ingress direction */
    eBool useFlowSMAC;                                 /**< Use Port's MAC or user input Source MAC */
    }tAtEthVlanFlowConfig;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Manage ingress VLAN traffic descriptor */
eAtModuleEthRet AtEthFlowIngressVlanAdd(AtEthFlow self, const tAtEthVlanDesc *desc);
eAtModuleEthRet AtEthFlowIngressVlanRemove(AtEthFlow self, const tAtEthVlanDesc *desc);
uint32 AtEthFlowIngressNumVlansGet(AtEthFlow self);
tAtEthVlanDesc *AtEthFlowIngressVlanAtIndex(AtEthFlow self, uint32 vlanIndex, tAtEthVlanDesc *descs);

/* Manage egress VLAN traffic descriptor */
eAtModuleEthRet AtEthFlowEgressVlanAdd(AtEthFlow self, const tAtEthVlanDesc *desc);
eAtModuleEthRet AtEthFlowEgressVlanRemove(AtEthFlow self, const tAtEthVlanDesc *desc);
uint32 AtEthFlowEgressNumVlansGet(AtEthFlow self);
tAtEthVlanDesc *AtEthFlowEgressVlanAtIndex(AtEthFlow self, uint32 vlanIndex, tAtEthVlanDesc *descs);

/* To change MACs for frames transmitted to Ethernet port */
eAtModuleEthRet AtEthFlowEgressDestMacSet(AtEthFlow self, uint8 *address);
eAtModuleEthRet AtEthFlowEgressDestMacGet(AtEthFlow self, uint8 *address);
eAtModuleEthRet AtEthFlowEgressSourceMacSet(AtEthFlow self, uint8 *address);
eAtModuleEthRet AtEthFlowEgressSourceMacGet(AtEthFlow self, uint8 *address);
eAtModuleEthRet AtEthFlowEgressHeaderSet(AtEthFlow self, uint8 *egressSourceMac, uint8 *egressDestMac, const tAtEthVlanDesc *egressVlan);
eBool AtEthFlowEgressMacIsProgrammable(AtEthFlow self);

/* Objects that this flow can work with */
AtHdlcLink AtEthFlowHdlcLinkGet(AtEthFlow self);
AtMpBundle AtEthFlowMpBundleGet(AtEthFlow self);
AtEncapChannel AtEthFlowBoundEncapChannelGet(AtEthFlow self);
AtFrVirtualCircuit AtEthFlowFrVirtualCircuitGet(AtEthFlow self);
AtPw AtEthFlowPwGet(AtEthFlow self);

/* VLAN TPID */
eAtModuleEthRet AtEthFlowEgressCVlanTpIdSet(AtEthFlow self, uint16 cVlanTpid);
uint16 AtEthFlowEgressCVlanTpIdGet(AtEthFlow self);
eAtModuleEthRet AtEthFlowEgressSVlanTpIdSet(AtEthFlow self, uint16 sVlanTpid);
uint16 AtEthFlowEgressSVlanTpIdGet(AtEthFlow self);

/* The flow control engine associated with this flow */
AtEthFlowControl AtEthFlowFlowControlGet(AtEthFlow self);

#ifdef __cplusplus
}
#endif
#endif /* _ATETHFLOW_H_ */

#include "AtEthFlowDeprecated.h"

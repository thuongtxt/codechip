/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : AtEthPort.h
 * 
 * Created Date: Aug 7, 2012
 *
 * Description : Ethernet port class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATETHPORT_H_
#define _ATETHPORT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h" /* Super class */
#include "AtModuleEth.h"
#include "AtSerdesController.h"
#include "AtEthFlowControl.h"
#include "AtEyeScanController.h"
#include "AtErrorGenerator.h"
#include "AtClockMonitor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cAtEthPortNumQue 8
#define cAtMacAddressLen 6
#define cAtIpv4AddressLen 4
#define cAtIpv6AddressLen 16

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleEth
 * @{
 */

/**
 * @brief Physical interface type
 */
typedef enum eAtEthPortInterface
    {
    cAtEthPortInterfaceUnknown,   /**< Unknown interface */
    cAtEthPortInterface1000BaseX, /**< 1000Base-X interface */
    cAtEthPortInterface2500BaseX, /**< 2500Base-X interface */
    cAtEthPortInterfaceSgmii,     /**< SGMII interface */
    cAtEthPortInterfaceQSgmii,    /**< QSGMII interface */
    cAtEthPortInterfaceRgmii,     /**< RGMII interface */
    cAtEthPortInterfaceGmii,      /**< GMII interface */
    cAtEthPortInterfaceMii,       /**< MII interface */
    cAtEthPortInterfaceXGMii,     /**< XGMII interface */
    cAtEthPortInterfaceXAUI,      /**< XAUI interface */
    cAtEthPortInterfaceBaseR,     /**< Base-R interface */
    cAtEthPortInterfaceBaseKR,    /**< Base-KR interface */
    cAtEthPortInterfaceBaseFx,    /**< Base-FX interface */
    cAtEthPortInterfaceNA         /**< Not applicable */
    }eAtEthPortInterface;

/**
 * @brief Speed of Ethernet port
 */
typedef enum eAtEthPortSpeed
    {
    cAtEthPortSpeedUnknown,    /**< Unknown */
    cAtEthPortSpeed10M,        /**< 10 Mbps */
    cAtEthPortSpeed100M,       /**< 100 Mbps */
    cAtEthPortSpeed1000M,      /**< 1000Mbps (1G) */
    cAtEthPortSpeed2500M,      /**< 2500Mbps (2.5G) */
    cAtEthPortSpeed10G,        /**< 10G */
    cAtEthPortSpeed40G,        /**< 40G */
    cAtEthPortSpeedAutoDetect, /**< Au-to speed detection */
    cAtEthPortSpeedNA          /**< Not applicable */
    }eAtEthPortSpeed;

/**
 * @brief Ethernet port working mode
 */
typedef enum eAtEthPortDuplexMode
    {
    cAtEthPortWorkingModeNone       = 0,      /**< None */
    cAtEthPortWorkingModeHalfDuplex = cBit0,  /**< Half-duplex */
    cAtEthPortWorkingModeFullDuplex = cBit1,  /**< Full-duplex */
    cAtEthPortWorkingModeAutoDetect = cBit2,  /**< Auto-detection */
    cAtEthPortWorkingModeUnknown    = cBit29, /**< Unknown */
    cAtEthPortWorkingModeNA         = cBit30  /**< Not applicable */
    }eAtEthPortDuplexMode;

/** @brief Link status */
typedef enum eAtEthPortLinkStatus
    {
    cAtEthPortLinkStatusDown,        /**< Link is down */
    cAtEthPortLinkStatusUp,          /**< Link is up */
    cAtEthPortLinkStatusUnknown,     /**< Unknown */
    cAtEthPortLinkStatusNotSupported /**< Not support link status */
    }eAtEthPortLinkStatus;

/** @brief Remote fault code */
typedef enum eAtEthPortRemoteFault
    {
    cAtEthPortRemoteFaultUnknown,      /**< Unknown code */
    cAtEthPortRemoteFaultNotSupported, /**< Not supported */
    cAtEthPortRemoteFaultNoError,      /**< No remote fault */
    cAtEthPortRemoteFaultOffline,      /**< Off line */
    cAtEthPortRemoteFaultLinkFailure,  /**< Link failure */
    cAtEthPortRemoteFaultAutoNegError  /**< Auto-Negotiation Error */
    }eAtEthPortRemoteFault;

/**
 * @brief Ethernet port configuration
 */
typedef struct tAtEthPortAllConfig
    {
    uint8 srcMacAddr[cAtMacAddressLen];   /**< Source MAC Address */
    uint8 destMacAddr[cAtMacAddressLen];   /**< Dest MAC Address */
    uint8 srcIpv4Addr[cAtIpv4AddressLen]; /**< Source IPv4 Address */
    uint8 srcIpv6Addr[cAtIpv6AddressLen]; /**< Source IPv6 Address */
    eAtEthPortInterface interface;        /**< Physical interface mode */
    eAtEthPortSpeed speed;                /**< Port speed */
    eAtEthPortDuplexMode workingMd;      /**< Ethernet working mode, default is Full-duplex */
    eBool autoNegEn;                       /**< Auto Negotiation function enable/disable */
    uint8 numTxIpg;                       /**< Number of Inter Packet Gap Clock */
    uint8 numRxIpg;                       /**< Number of Inter Packet Gap Clock */
    eBool flowCtrlEn;                      /**< Enable/Disable flow control process */
    uint32 pauFrmInterval;                 /**< Time interval to send pause frames in quantum
                                                1 quantum = time to send 64 bytes */
    uint32 pauFrmExpTime;                  /**< Expired time of pause frames in quantum
                                                1 quantum = time to send 64 bytes */
    eBool macChkEn;                        /**< Checking Mac Address (For Modify function) */
    eAtLedState ledState;                   /**< Led State */
    }tAtEthPortAllConfig;

/** @brief Ethernet Sub Port (Lane) alarms */
typedef enum eAtEthSubPortAlarmType
    {
    cAtEthSubPortAlarmRxFramingError = cBit0, /**< For lane: RX sync header bits
        framing error. Each PCS Lane has a four-bit bus that indicates how many
        sync header errors were received for that PCS Lane */
    cAtEthSubPortAlarmRxSyncedError = cBit1, /**< For Lane: Word Boundary
        Synchronization Error. These signals indicate whether an error occurred
        during word boundary synchronization in the respective PCS lane. A value
        of 1 indicates that the corresponding PCS lane lost word boundary
        synchronization due to sync header framing bits errors or that a PCS lane
        marker was never received.  */
    cAtEthSubPortAlarmRxMfLenError = cBit2, /**< For lane: PCS Lane Marker Length Error.
        These signals indicate whether a PCS Lane Marker length mismatch occurred
        in the respective lane (that is, PCS Lane Markers were received not every
        ctl_rx_vl_length_minus1 words apart). A value of 1 indicates that the
        corresponding lane is receiving PCS Lane Markers at wrong intervals.  */
    cAtEthSubPortAlarmRxMfRepeatError = cBit3, /**< For lane: PCS Lane Marker
        Consecutive Error. These signals indicate whether four consecutive
        PCS Lane Marker errors occurred in the respective lane. A value of 1
        indicates an error in the corresponding lane. */
    cAtEthSubPortAlarmRxMfError = cBit4, /**< For lane: PCS Lane Marker Word Error. These
        signals indicate that an incorrectly formed PCS Lane Marker Word was
        detected in the respective lane. A value of 1 indicates an error occurred.
        This output is pulsed for one clock cycle to indicate the error condition. */
    cAtEthSubPortAlarmRxBipError = cBit5, /**< For lane: BIP8 error indicator. A non-zero
        value indicates the BIP8 signature byte was in error for the corresponding
        PCS lane. A non-zero value is pulsed for one clock cycle. */
    cAtEthSubPortAlarmAutoNegFecIncCantCorrect = cBit6, /**< Logical indication of uncorrectable errors. */
    cAtEthSubPortAlarmAutoNegFecIncCorrect = cBit7, /**< Logical indication of correctable errors. */
    cAtEthSubPortAlarmAutoNegFecLockError = cBit8 /**< Logical indication of a
        failure to achieve a frame lock. The receiver scans the incoming data
        stream for about 10,000,000 bits, attempting all possible bit alignments
        for frame synchronization. After this time, this signal is asserted High
        and remains High until the receiver achieves a frame lock. There is one
        per lane. */
    }eAtEthSubPortAlarmType;

/** @brief Ethernet Port alarms */
typedef enum eAtEthPortAlarmType
    {
    /* Link alarms */
    cAtEthPortAlarmLinkUp               = cBit0,  /**< Link up */
    cAtEthPortAlarmLinkDown             = cBit1,  /**< Link down */
    cAtEthPortAlarmLos                  = cBit2,  /**< Loss of signal defect mask */
    cAtEthPortAlarmTxFault              = cBit3,  /**< TX Fault */
    cAtEthPortAlarmTxUnderflowError     = cBit4,  /**< TX Underflow error */
    cAtEthPortAlarmLocalFault           = cBit5,  /**< RX Local Fault */
    cAtEthPortAlarmRemoteFault          = cBit6,  /**< RX remote fault */
    cAtEthPortAlarmRxInternalFault      = cBit7,  /**< RX internal fault: test
            pattern generation, bad lane alignment, or high bit error rate.  */
    cAtEthPortAlarmRxReceivedLocalFault = cBit8,  /**< when enough local
        fault words are received from the link partner to trigger a fault condition
        as specified by the IEEE fault state machine. */
    cAtEthPortAlarmRxAlignedError       = cBit9,  /**< Loss of Lane Alignment/Deskew.
        This signal indicates that an error occurred during PCS lane alignment
        or PCS lane alignment was lost. A value of 1 indicates an error occurred. */
    cAtEthPortAlarmRxMisAligned         = cBit10, /**< Alignment Error. This signal
        indicates that the lane aligner did not receive the expected PCS lane
        marker across all lanes. This signal is not asserted until the PCS lane
        marker has been received at least once across all lanes and at least one
        incorrect lane marker has been received. This occurs one metaframe after
        the error. This signal is not asserted if the lane markers have never
        been received correctly. Lane marker errors are indicated by the
        corresponding stat_rx_mf_err signal. */
    cAtEthPortAlarmRxTruncated          = cBit11, /**< Packet truncation indicator.
        A value of 1 indicates that the current packet in flight is truncated due
        to its length exceeding ctl_rx_max_packet_len[14:0]. */
    cAtEthPortAlarmRxHiBer              = cBit12, /**< High Bit Error Rate (BER) indicator.
        When set to 1, the BER is too high as defined by IEEE Std 802.3-2012.
        Corresponds to MDIO register bit 3.32.1 as defined in Clause 82.3. */
    cAtEthPortAlarmExcessiveErrorRatio  = cBit13, /**< Excessive Error Ratio */
    cAtEthPortAlarmLossofClock          = cBit14, /**< Loss of Clock */
    cAtEthPortAlarmFrequencyOutofRange  = cBit15, /**< Frequency Out of Range */
    cAtEthPortAlarmLossofDataSync       = cBit16, /**< Loss of Data Synchronization */
    cAtEthPortAlarmLossofFrame          = cBit17, /**< Loss of Frame */

    /* Autoneg alarms */
    cAtEthPortAlarmAutoNegParallelDetectionFault = cBit18, /**< Indicated a parallel detection fault
                                                                during auto-negotiation. */

    /* Events */
    cAtEthPortAlarmAutoNegStateChange       = cBit19, /**< Auto-Neg state change */

    cAtEthPortAlarmRxFramingError           = cBit20, /**< For lane: RX sync header bits framing error.
                                                           Each PCS Lane has a four-bit bus that indicates
                                                           how many sync header errors were received for
                                                           that PCS Lane */
    cAtEthPortAlarmAutoNegFecIncCantCorrect = cBit21, /**< Logical indication of uncorrectable errors. */
    cAtEthPortAlarmAutoNegFecIncCorrect     = cBit22, /**< Logical indication of correctable errors. */
    cAtEthPortAlarmAutoNegFecLockError      = cBit23, /**< Logical indication of a failure to achieve a frame lock.
                                                           The receiver scans the incoming data stream for
                                                           about 10,000,000 bits, attempting all possible bit
                                                           alignments for frame synchronization. After this time,
                                                           this signal is asserted High and remains High until
                                                           the receiver achieves a frame lock. There is one
                                                           per lane. */

    /* MAC alarms */
    cAtEthPortAlarmFcsError                 = cBit24, /**< Received packets has been detected in FCS error */
    cAtEthPortAlarmOversized                = cBit25  /**< Received packets has been detected in Oversized length
                                                           (i.e. longer than maximum frame length allowed) */
    }eAtEthPortAlarmType;

/** @brief All counters of Ethernet port */
typedef struct tAtEthPortCounters
    {
    uint32 txPackets;                 /**< Number of TX packets */
    uint32 txBytes;                   /**< Number of TX bytes */
    uint32 txGoodPackets;             /**< Number of TX good packets */
    uint32 txGoodBytes;               /**< Number of TX good bytes */
    uint32 txOamPackets;              /**< Number of TX OAM Packets, which are packets sent from CPU or
                                           supported OAM packets such as VCCV, LDP, ARP, ICMP, ...  */
    uint32 txPeriodOamPackets;        /**< Number of OAM packets which are periodically sent by device */
    uint32 txPwPackets;               /**< Number of TX PW data packet counter, not include OAM packets */
    uint32 txPacketsLen0_64;          /**< Number of TX packets with length from 0 to 64 */
    uint32 txPacketsLen65_127;        /**< Number of TX packets with length from 65 to 127 */
    uint32 txPacketsLen128_255;       /**< Number of TX packets with length from 128 to 255 */
    uint32 txPacketsLen256_511;       /**< Number of TX packets with length from 256 to 511 */
    uint32 txPacketsLen512_1024;      /**< Number of TX packets with length from 512 to 1024 */
    uint32 txPacketsLen1025_1528;     /**< Number of TX packets with length from 1025 to 1528 */
    uint32 txPacketsLen1529_2047;     /**< Number of TX packets with length from 1529 to 2047 */
    uint32 txPacketsJumbo;            /**< Number of TX packets with have length >=1519 and <= MTU */
    uint32 txTopPackets;              /**< Number of TX Timing over packet counter */
    uint32 txPausePackets;            /**< Number of TX pause frames */
    uint32 txBrdCastPackets;          /**< Number of TX broadcast packets */
    uint32 txMultCastPackets;         /**< Number of TX multicast packets */
    uint32 txUniCastPackets;          /**< Number of TX Unicast packets */
    uint32 txOversizePackets;         /**< Number of TX packets > MTU */
    uint32 txUndersizePackets;        /**< Number of TX packets with length < 64 */
    uint32 txDiscardedPackets;        /**< Number of discarded packets has packet type is configured to be discarded */
    uint32 txOverFlowDroppedPackets;  /**< Number of TX Over flow dropped */
    uint32 rxPackets;                 /**< Number of RX packets */
    uint32 rxBytes;                   /**< Number of RX bytes */
    uint32 rxGoodPackets;             /**< Number of RX good packets */
    uint32 rxGoodBytes;               /**< Number of RX good bytes */
    uint32 rxErrEthHdrPackets;        /**< Number of Rx error Ethernet header packets */
    uint32 rxErrBusPackets;           /**< Number of Rx bus error packets */
    uint32 rxErrFcsPackets;           /**< Number of Rx FCS error packets */
    uint32 rxOversizePackets;         /**< Number of packets > MTU */
    uint32 rxUndersizePackets;        /**< Number of RX packets with length < 64 */
    uint32 rxPacketsLen0_64;          /**< Number of RX packets with length from 0 to 64 */
    uint32 rxPacketsLen65_127;        /**< Number of RX packets with length from 65 to 127 */
    uint32 rxPacketsLen128_255;       /**< Number of RX packets with length from 128 to 255 */
    uint32 rxPacketsLen256_511;       /**< Number of RX packets with length from 256 to 511 */
    uint32 rxPacketsLen512_1024;      /**< Number of RX packets with length from 512 to 1024 */
    uint32 rxPacketsLen1025_1528;     /**< Number of RX packets with length from 1025 to 1528 */
    uint32 rxPacketsLen1529_2047;     /**< Number of RX packets with length from 1529 to 2047 */
    uint32 rxPacketsJumbo;            /**< Number of RX packets with have length >=1519 and <= MTU */
    uint32 rxPwUnsupportedPackets;    /**< Number of DA MAC address not matched with one of SA MAC addresses */
    uint32 rxErrPwLabelPackets;       /**< Number of packets have Unsupported Ethernet Type or Unknown PW labels */
    uint32 rxDiscardedPackets;        /**< Number of discarded packets has packet type is configured to be discarded */
    uint32 rxPausePackets;            /**< Number of RX Pause packets */
    uint32 rxErrPausePackets;         /**< Number of RX Error Pause packets */
    uint32 rxUniCastPackets;          /**< Number of RX unicast packets */
    uint32 rxBrdCastPackets;          /**< Number of RX broadcast packets */
    uint32 rxMultCastPackets;         /**< Number of RX multicast packets */
    uint32 rxArpPackets;              /**< Number of RX ARP packets */
    uint32 rxOamPackets;              /**< Number of RX classified OAM Packets */
    uint32 rxEfmOamPackets;           /**< Number of RX EFM OAM packets */
    uint32 rxErrEfmOamPackets;        /**< Number of RX Error EFM OAM packets */
    uint32 rxEthOamType1Packets;      /**< Number of RX ETHERNET OAM type 1 packets,
                                           which are packets have DA MAC match the programmable
                                           DA MAC of the type of Ethernet OAM */
    uint32 rxEthOamType2Packets;      /**< Number of RX ETHERNET OAM type 1 packets,
                                           which are packets have DA MAC match the programmable
                                           DA MAC of the other type of Ethernet OAM */
    uint32 rxIpv4Packets;             /**< Number of RX Ipv4 packets */
    uint32 rxErrIpv4Packets;          /**< Number of RX error IPv4 packets */
    uint32 rxIcmpIpv4Packets;         /**< Number of RX ICMP IPv4 packets */
    uint32 rxIpv6Packets;             /**< Number of RX Ipv6 packets */
    uint32 rxErrIpv6Packets;          /**< Number of RX error IPv6 packets */
    uint32 rxIcmpIpv6Packets;         /**< Number of RX ICMP IPv6 packets */
    uint32 rxMefPackets;              /**< Number of RX MEF packets */
    uint32 rxErrMefPackets;           /**< Number of RX Error MEF packets */
    uint32 rxMplsPackets;             /**< Number of RX MPLS packets */
    uint32 rxErrMplsPackets;          /**< Number of RX Error MPLS packets */
    uint32 rxMplsErrOuterLblPackets;  /**< Number of RX Error outer label MPLS packets */
    uint32 rxMplsDataPackets;         /**< Number of RX Data MPLS packets */
    uint32 rxLdpIpv4Packets;          /**< Number of RX LDP Ipv4 packets */
    uint32 rxLdpIpv6Packets;          /**< Number of RX LDP Ipv6 packets */
    uint32 rxMplsIpv4Packets;         /**< Number of RX MPLS Ipv4 packets */
    uint32 rxMplsIpv6Packets;         /**< Number of RX MPLS Ipv6 packets */
    uint32 rxErrL2tpv3Packets;        /**< Number of RX Error L2TPv3 packets */
    uint32 rxL2tpv3Packets;           /**< Number of RX L2TPv3 packets */
    uint32 rxL2tpv3Ipv4Packets;       /**< Number of RX L2TPv3 Ipv4 packets */
    uint32 rxL2tpv3Ipv6Packets;       /**< Number of RX L2TPv3 Ipv6 packets */
    uint32 rxUdpPackets;              /**< Number of RX UDP packets */
    uint32 rxErrUdpPackets;           /**< Number of RX Error UDP packets */
    uint32 rxUdpIpv4Packets;          /**< Number of RX UDP Ipv4 packets */
    uint32 rxUdpIpv6Packets;          /**< Number of RX UDP Ipv6 packets */
    uint32 rxErrPsnPackets;           /**< Number of Rx error PSN header packets */
    uint32 rxPacketsSendToCpu;        /**< Number of packets sent to CPU */
    uint32 rxPacketsSendToPw;         /**< Number of packets sent to all PWs use this ports */
    uint32 rxTopPackets;              /**< Number of Timing Over Packets */
    uint32 rxPacketDaMis;             /**< Number of RX Mismatch DA Packets */
    uint32 rxPhysicalError;           /**< Number of RX physical errors */
    uint32 rxBip8Errors;              /**< Number of RX BIP-8 Error */
    uint32 rxOverFlowDroppedPackets;  /**< Number of RX Over flow dropped */
    uint32 rxFragmentPackets;         /**< Number of RX Fragment */
    uint32 rxJabberPackets;           /**< Number of RX Jabber packets */
    uint32 rxLoopDaPackets;           /**< Number of RX Loop DA packets */
    uint32 rxPcsErrorPackets;         /**< Number of RX PCS error packets */
    uint32 rxPcsInvalidCount;         /**< Number of RX PCS code error.
                                          Similar to rxPcsErrorPackets, but this will count
                                          number of invalid codes instead of frames */

    /* XILINX MAC will support following counters */
    uint32 txPacketsLen64;            /**< Number of TX packets with length 64 */
    uint32 txPacketsLen512_1023;      /**< Number of TX packets with length from 512 to 1023 */
    uint32 txPacketsLen1024_1518;     /**< Number of TX packets with length from 1024 to 1518 */
    uint32 txPacketsLen1519_1522;     /**< Number of TX packets with length from 1519 to 1522 */
    uint32 txPacketsLen1523_1548;     /**< Number of TX packets with length from 1523 to 1548 */
    uint32 txPacketsLen1549_2047;     /**< Number of TX packets with length from 1549 to 2047 */
    uint32 txPacketsLen2048_4095;     /**< Number of TX packets with length from 2048 to 4095 */
    uint32 txPacketsLen4096_8191;     /**< Number of TX packets with length from 4096 to 8191 */
    uint32 txPacketsLen8192_9215;     /**< Number of TX packets with length from 8192 to 9215 */
    uint32 txPacketsLarge;            /**< Number of TX large packets */
    uint32 txPacketsSmall;            /**< Number of TX small packets */
    uint32 txErrFcsPackets;           /**< Number of TX bad FCS packets */
    uint32 txErrPackets;              /**< Number of TX error packets */
    uint32 txVlanPackets;             /**< Number of TX VLAN packets */
    uint32 txUserPausePackets;        /**< Number of TX User Pause packets */
    uint32 rxPacketsLen64;            /**< Number of RX packets with length 64 */
    uint32 rxPacketsLen512_1023;      /**< Number of RX packets with length from 512 to 1023 */
    uint32 rxPacketsLen1024_1518;     /**< Number of RX packets with length from 1024 to 1518 */
    uint32 rxPacketsLen1519_1522;     /**< Number of RX packets with length from 1519 to 1522 */
    uint32 rxPacketsLen1523_1548;     /**< Number of RX packets with length from 1523 to 1548 */
    uint32 rxPacketsLen1549_2047;     /**< Number of RX packets with length from 1549 to 2047 */
    uint32 rxPacketsLen2048_4095;     /**< Number of RX packets with length from 2048 to 4095 */
    uint32 rxPacketsLen4096_8191;     /**< Number of RX packets with length from 4096 to 8191 */
    uint32 rxPacketsLen8192_9215;     /**< Number of RX packets with length from 8192 to 9215 */
    uint32 rxPacketsLarge;            /**< Number of RX large packets */
    uint32 rxPacketsSmall;            /**< Number of RX small packets */
    uint32 rxErrPackets;              /**< Number of RX error packets */
    uint32 rxVlanPackets;             /**< Number of RX VLAN packets */
    uint32 rxUserPausePackets;        /**< Number of RX User Pause packets */
    uint32 rxPacketsTooLong;          /**< Number of RX too long packets */
    uint32 rxStompedFcsPackets;       /**< Number of RX stomped FCS packets */
    uint32 rxInRangeErrPackets;       /**< Number of RX in range error packets */
    uint32 rxTruncatedPackets;        /**< Number of RX truncated packets */
    uint32 rxFecIncCorrectCount;      /**< Number of RX correctable errors */
    uint32 rxFecIncCantCorrectCount;  /**< Number of RX uncorrectable errors */
    uint32 rxFecLockErrorCount;       /**< Number of lock errors */
    }tAtEthPortCounters;

/** @brief Auto-Neg state of Ethernet port */
typedef enum eAtEthPortAutoNegState
    {
    cAtEthPortAutoNegNotSupported,        /**< Not support */
    cAtEthPortAutoNegEnable,              /**< Enabled */
    cAtEthPortAutoNegRestart,             /**< Restart */
    cAtEthPortAutoNegAbilityDetect,       /**< Ability detect */
    cAtEthPortAutoNegAckDetect,           /**< Acknowledge detect */
    cAtEthPortAutoNegNextPageWait,        /**< Next page wait */
    cAtEthPortAutoNegCompleteAck,         /**< Complete Acknowledge */
    cAtEthPortAutoNegIdleDetect,          /**< Idle detect */
    cAtEthPortAutoNegLinkOk,              /**< Link OK */
    cAtEthPortAutoNegDisableLinkOk,       /**< Autoneg disable, link OK */
    cAtEthPortAutoNegInvalid              /**< Invalid state */
    } eAtEthPortAutoNegState;

/** @brief Counter types */
typedef enum eAtEthPortCounterType
    {
    cAtEthPortCounterUnknown,                   /**< Unknown counter */
    cAtEthPortCounterTxPackets,                 /**< Number of TX packets */
    cAtEthPortCounterTxBytes,                   /**< Number of TX bytes */
    cAtEthPortCounterTxGoodPackets,             /**< Number of TX good packets */
    cAtEthPortCounterTxGoodBytes,               /**< Number of TX good bytes */
    cAtEthPortCounterTxOamPackets,              /**< Number of TX OAM Packets, which are packets sent from CPU or
                                                     supported OAM packets such as VCCV, LDP, ARP, ICMP, ...  */
    cAtEthPortCounterTxPeriodOamPackets,        /**< Number of OAM packets which are periodically sent by device */
    cAtEthPortCounterTxPwPackets,               /**< Number of TX PW data packet counter, not include OAM packets */
    cAtEthPortCounterTxPacketsLen0_64,          /**< Number of TX packets with length from 0 to 64 */
    cAtEthPortCounterTxPacketsLen65_127,        /**< Number of TX packets with length from 65 to 127 */
    cAtEthPortCounterTxPacketsLen128_255,       /**< Number of TX packets with length from 128 to 255 */
    cAtEthPortCounterTxPacketsLen256_511,       /**< Number of TX packets with length from 256 to 511 */
    cAtEthPortCounterTxPacketsLen512_1024,      /**< Number of TX packets with length from 512 to 1024 */
    cAtEthPortCounterTxPacketsLen1025_1528,     /**< Number of TX packets with length from 1025 to 1528 */
    cAtEthPortCounterTxPacketsLen1529_2047,     /**< Number of TX packets with length from 1529 to 2047 */
    cAtEthPortCounterTxPacketsJumbo,            /**< Number of TX packets with have length >=1519 and <= MTU */
    cAtEthPortCounterTxTopPackets,              /**< Number of TX Timing over packet counter */
    cAtEthPortCounterTxPausePackets,            /**< Number of TX pause frames */
    cAtEthPortCounterTxBrdCastPackets,          /**< Number of TX broadcast packets */
    cAtEthPortCounterTxMultCastPackets,         /**< Number of TX multicast packets */
    cAtEthPortCounterTxUniCastPackets,          /**< Number of TX Unicast packets */
    cAtEthPortCounterTxOversizePackets,         /**< Number of TX packets > MTU */
    cAtEthPortCounterTxUndersizePackets,        /**< Number of TX packets with length < 64 */
    cAtEthPortCounterTxDiscardedPackets,        /**< Number of discarded packets has packet type is configured to be discarded */
    cAtEthPortCounterTxOverFlowDroppedPackets,  /**< Number of TX Over flow dropped */
    cAtEthPortCounterRxPackets,                 /**< Number of RX packets */
    cAtEthPortCounterRxBytes,                   /**< Number of RX bytes */
    cAtEthPortCounterRxGoodPackets,             /**< Number of RX good packets */
    cAtEthPortCounterRxGoodBytes,               /**< Number of RX good bytes */
    cAtEthPortCounterRxErrEthHdrPackets,        /**< Number of Rx error Ethernet header packets */
    cAtEthPortCounterRxErrBusPackets,           /**< Number of Rx bus error packets */
    cAtEthPortCounterRxErrFcsPackets,           /**< Number of Rx FCS error packets */
    cAtEthPortCounterRxOversizePackets,         /**< Number of packets > MTU */
    cAtEthPortCounterRxUndersizePackets,        /**< Number of packets with length < 64 */
    cAtEthPortCounterRxPacketsLen0_64,          /**< Number of RX packets with length from 0 to 64 */
    cAtEthPortCounterRxPacketsLen65_127,        /**< Number of RX packets with length from 65 to 127 */
    cAtEthPortCounterRxPacketsLen128_255,       /**< Number of RX packets with length from 128 to 255 */
    cAtEthPortCounterRxPacketsLen256_511,       /**< Number of RX packets with length from 256 to 511 */
    cAtEthPortCounterRxPacketsLen512_1024,      /**< Number of RX packets with length from 512 to 1024 */
    cAtEthPortCounterRxPacketsLen1025_1528,     /**< Number of RX packets with length from 1025 to 1528 */
    cAtEthPortCounterRxPacketsLen1529_2047,     /**< Number of RX packets with length from 1529 to 2047 */
    cAtEthPortCounterRxPacketsJumbo,            /**< Number of RX packets with have length >=1519 and <= MTU */
    cAtEthPortCounterRxPwUnsupportedPackets,    /**< Number of DA MAC address not matched with one of SA MAC addresses */
    cAtEthPortCounterRxErrPwLabelPackets,       /**< Number of packets have Unsupported Ethernet Type or Unknown PW labels */
    cAtEthPortCounterRxDiscardedPackets,        /**< Number of discarded packets has packet type is configured to be discarded */
    cAtEthPortCounterRxPausePackets,            /**< Number of RX Pause packets */
    cAtEthPortCounterRxErrPausePackets,         /**< Number of RX Error Pause packets */
    cAtEthPortCounterRxUniCastPackets,          /**< Number of RX unicast packets */
    cAtEthPortCounterRxBrdCastPackets,          /**< Number of RX broadcast packets */
    cAtEthPortCounterRxMultCastPackets,         /**< Number of RX multicast packets */
    cAtEthPortCounterRxArpPackets,              /**< Number of RX ARP packets */
    cAtEthPortCounterRxOamPackets,              /**< Number of RX classified OAM Packets */
    cAtEthPortCounterRxEfmOamPackets,           /**< Number of RX EFM OAM packets */
    cAtEthPortCounterRxErrEfmOamPackets,        /**< Number of RX Error EFM OAM packets */
    cAtEthPortCounterRxEthOamType1Packets,      /**< Number of RX ETHERNET OAM type 1 packets,
                                                     which are packets have DA MAC match the programmable
                                                     DA MAC of the type of Ethernet OAM */
    cAtEthPortCounterRxEthOamType2Packets,      /**< Number of RX ETHERNET OAM type 1 packets,
                                                     which are packets have DA MAC match the programmable
                                                     DA MAC of the other type of Ethernet OAM */
    cAtEthPortCounterRxIpv4Packets,             /**< Number of RX Ipv4 packets */
    cAtEthPortCounterRxErrIpv4Packets,          /**< Number of RX error IPv4 packets */
    cAtEthPortCounterRxIcmpIpv4Packets,         /**< Number of RX ICMP IPv4 packets */
    cAtEthPortCounterRxIpv6Packets,             /**< Number of RX Ipv6 packets */
    cAtEthPortCounterRxErrIpv6Packets,          /**< Number of RX error IPv6 packets */
    cAtEthPortCounterRxIcmpIpv6Packets,         /**< Number of RX ICMP IPv6 packets */
    cAtEthPortCounterRxMefPackets,              /**< Number of RX MEF packets */
    cAtEthPortCounterRxErrMefPackets,           /**< Number of RX Error MEF packets */
    cAtEthPortCounterRxMplsPackets,             /**< Number of RX MPLS packets */
    cAtEthPortCounterRxErrMplsPackets,          /**< Number of RX Error MPLS packets */
    cAtEthPortCounterRxMplsErrOuterLblPackets,  /**< Number of RX Error outer label MPLS packets */
    cAtEthPortCounterRxMplsDataPackets,         /**< Number of RX Data MPLS packets */
    cAtEthPortCounterRxLdpIpv4Packets,          /**< Number of RX LDP Ipv4 packets */
    cAtEthPortCounterRxLdpIpv6Packets,          /**< Number of RX LDP Ipv6 packets */
    cAtEthPortCounterRxMplsIpv4Packets,         /**< Number of RX MPLS Ipv4 packets */
    cAtEthPortCounterRxMplsIpv6Packets,         /**< Number of RX MPLS Ipv6 packets */
    cAtEthPortCounterRxErrL2tpv3Packets,        /**< Number of RX Error L2TPv3 packets */
    cAtEthPortCounterRxL2tpv3Packets,           /**< Number of RX L2TPv3 packets */
    cAtEthPortCounterRxL2tpv3Ipv4Packets,       /**< Number of RX L2TPv3 Ipv4 packets */
    cAtEthPortCounterRxL2tpv3Ipv6Packets,       /**< Number of RX L2TPv3 Ipv6 packets */
    cAtEthPortCounterRxUdpPackets,              /**< Number of RX UDP packets */
    cAtEthPortCounterRxErrUdpPackets,           /**< Number of RX Error UDP packets */
    cAtEthPortCounterRxUdpIpv4Packets,          /**< Number of RX UDP Ipv4 packets */
    cAtEthPortCounterRxUdpIpv6Packets,          /**< Number of RX UDP Ipv6 packets */
    cAtEthPortCounterRxErrPsnPackets,           /**< Number of Rx error PSN header packets */
    cAtEthPortCounterRxPacketsSendToCpu,        /**< Number of packets sent to CPU */
    cAtEthPortCounterRxPacketsSendToPw,         /**< Number of packets sent to all PWs use this ports */
    cAtEthPortCounterRxTopPackets,              /**< Number of Timing Over Packets */
    cAtEthPortCounterRxPacketDaMis,             /**< Number of RX Mismatch DA Packets */
    cAtEthPortCounterRxPhysicalError,           /**< Number of RX physical errors */
    cAtEthPortCounterRxBip8Errors,              /**< Number of RX BIP-8 Error */
    cAtEthPortCounterRxOverFlowDroppedPackets,  /**< Number of RX Over flow dropped */
    cAtEthPortCounterRxFragmentPackets,         /**< Number of RX Fragment */
    cAtEthPortCounterRxJabberPackets,           /**< Number of RX Jabber packets */
    cAtEthPortCounterRxLoopDaPackets,           /**< Number of RX Loop DA packets */
    cAtEthPortCounterRxPcsErrorPackets,         /**< Number of RX PCS error packets */
    cAtEthPortCounterRxPcsInvalidCount,         /**< Number of RX PCS code error.
                                                     Similar to rxPcsErrorPackets, but this will count
                                                     number of invalid codes instead of frames */

    /* XILINX MAC will support following counters */
    cAtEthPortCounterTxPacketsLen64,            /**< Number of TX packets with length 64 */
    cAtEthPortCounterTxPacketsLen512_1023,      /**< Number of TX packets with length from 512 to 1023 */
    cAtEthPortCounterTxPacketsLen1024_1518,     /**< Number of TX packets with length from 1024 to 1518 */
    cAtEthPortCounterTxPacketsLen1519_1522,     /**< Number of TX packets with length from 1519 to 1522 */
    cAtEthPortCounterTxPacketsLen1523_1548,     /**< Number of TX packets with length from 1523 to 1548 */
    cAtEthPortCounterTxPacketsLen1549_2047,     /**< Number of TX packets with length from 1549 to 2047 */
    cAtEthPortCounterTxPacketsLen2048_4095,     /**< Number of TX packets with length from 2048 to 4095 */
    cAtEthPortCounterTxPacketsLen4096_8191,     /**< Number of TX packets with length from 4096 to 8191 */
    cAtEthPortCounterTxPacketsLen8192_9215,     /**< Number of TX packets with length from 8192 to 9215 */
    cAtEthPortCounterTxPacketsLarge,            /**< Number of TX large packets */
    cAtEthPortCounterTxPacketsSmall,            /**< Number of TX small packets */
    cAtEthPortCounterTxErrFcsPackets,           /**< Number of TX bad FCS packets */
    cAtEthPortCounterTxErrPackets,              /**< Number of TX error packets */
    cAtEthPortCounterTxVlanPackets,             /**< Number of TX VLAN packets */
    cAtEthPortCounterTxUserPausePackets,        /**< Number of TX User Pause packets */
    cAtEthPortCounterRxPacketsLen64,            /**< Number of RX packets with length 64 */
    cAtEthPortCounterRxPacketsLen512_1023,      /**< Number of RX packets with length from 512 to 1023 */
    cAtEthPortCounterRxPacketsLen1024_1518,     /**< Number of RX packets with length from 1024 to 1518 */
    cAtEthPortCounterRxPacketsLen1519_1522,     /**< Number of RX packets with length from 1519 to 1522 */
    cAtEthPortCounterRxPacketsLen1523_1548,     /**< Number of RX packets with length from 1523 to 1548 */
    cAtEthPortCounterRxPacketsLen1549_2047,     /**< Number of RX packets with length from 1549 to 2047 */
    cAtEthPortCounterRxPacketsLen2048_4095,     /**< Number of RX packets with length from 2048 to 4095 */
    cAtEthPortCounterRxPacketsLen4096_8191,     /**< Number of RX packets with length from 4096 to 8191 */
    cAtEthPortCounterRxPacketsLen8192_9215,     /**< Number of RX packets with length from 8192 to 9215 */
    cAtEthPortCounterRxPacketsLarge,            /**< Number of RX large packets */
    cAtEthPortCounterRxPacketsSmall,            /**< Number of RX small packets */
    cAtEthPortCounterRxErrPackets,              /**< Number of RX error packets */
    cAtEthPortCounterRxVlanPackets,             /**< Number of RX VLAN packets */
    cAtEthPortCounterRxUserPausePackets,        /**< Number of RX User Pause packets */
    cAtEthPortCounterRxPacketsTooLong,          /**< Number of RX too long packets */
    cAtEthPortCounterRxStompedFcsPackets,       /**< Number of RX stomped FCS packets */
    cAtEthPortCounterRxInRangeErrPackets,       /**< Number of RX in range error packets */
    cAtEthPortCounterRxTruncatedPackets,        /**< Number of RX truncated packets */
    cAtEthPortCounterRxFecIncCorrectCount,      /**< Number of RX correctable errors */
    cAtEthPortCounterRxFecIncCantCorrectCount,  /**< Number of RX uncorrectable errors */
    cAtEthPortCounterRxFecLockErrorCount        /**< Number of lock errors */
    }eAtEthPortCounterType;

/** @brief Error generator of Ethernet port */
typedef enum eAtEthPortErrorGeneratorErrorType
    {
    cAtEthPortErrorGeneratorErrorTypeInvalid,              /**< Invalid error type */
    cAtEthPortErrorGeneratorErrorTypeFcs,                  /**< error at FCS */
    cAtEthPortErrorGeneratorErrorTypeMacPcs                /**< error at the interface between MAC and PCS */
    } eAtEthPortErrorGeneratorErrorType;

/**
 * @brief Drop Packet Conditions
 */
typedef enum eAtEthPortDropCondition
    {
    cAtEthPortDropConditionNone          = 0,       /**< Do not  drop any packet */
    cAtEthPortDropConditionPcsError      = cBit0,   /**< PCS Error */
    cAtEthPortDropConditionPktFcsError   = cBit1,   /**< FCS Error */
    cAtEthPortDropConditionPktUnderSize  = cBit2,   /**< Pkt UnderSize */
    cAtEthPortDropConditionPktOverSize   = cBit3,   /**< Pkt OverSize */
    cAtEthPortDropConditionPktPauseFrame = cBit4,   /**< Pause Frame */
    cAtEthPortDropConditionPktLoopDa     = cBit5    /**< Loop DA packet */
    }eAtEthPortDropCondition;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* SMAC address */
eAtModuleEthRet AtEthPortSourceMacAddressSet(AtEthPort port, uint8 *address);
eAtModuleEthRet AtEthPortSourceMacAddressGet(AtEthPort port, uint8 *address);
eBool AtEthPortHasSourceMac(AtEthPort self);

eAtModuleEthRet AtEthPortMacCheckingEnable(AtEthPort port, eBool enable);
eBool AtEthPortMacCheckingIsEnabled(AtEthPort port);

/* DMAC */
eAtModuleEthRet AtEthPortDestMacAddressSet(AtEthPort port, uint8 *address);
eAtModuleEthRet AtEthPortDestMacAddressGet(AtEthPort port, uint8 *address);
eBool AtEthPortHasDestMac(AtEthPort port);

/* Some products have expected DMAC */
eAtRet AtEthPortExpectedDestMacSet(AtEthPort self, const uint8 *mac);
eAtRet AtEthPortExpectedDestMacGet(AtEthPort self, uint8 *mac);
eBool AtEthPortHasExpectedDestMac(AtEthPort self);
eBool AtEthPortSohTransparentIsSupported(AtEthPort self);

/* VLAN */
eAtRet AtEthPortExpectedCVlanSet(AtEthPort self, const tAtVlan *cVlan);
tAtVlan *AtEthPortExpectedCVlanGet(AtEthPort self, tAtVlan *cVlan);
eAtRet AtEthPortTxCVlanSet(AtEthPort self, const tAtVlan *cVlan);
tAtVlan *AtEthPortTxCVlanGet(AtEthPort self, tAtVlan *cVlan);
eBool AtEthPortHasCVlan(AtEthPort port);

/* IP Address */
eAtModuleEthRet AtEthPortIpV4AddressSet(AtEthPort port, uint8 *address);
eAtModuleEthRet AtEthPortIpV4AddressGet(AtEthPort port, uint8 *address);
eAtModuleEthRet AtEthPortIpV6AddressSet(AtEthPort port, uint8 *address);
eAtModuleEthRet AtEthPortIpV6AddressGet(AtEthPort port, uint8 *address);

/* Interface */
eAtModuleEthRet AtEthPortInterfaceSet(AtEthPort port, eAtEthPortInterface interface);
eAtEthPortInterface AtEthPortInterfaceGet(AtEthPort port);

/* Speed */
eBool AtEthPortSpeedIsSupported(AtEthPort port, eAtEthPortSpeed speed);
eAtModuleEthRet AtEthPortSpeedSet(AtEthPort port, eAtEthPortSpeed speed);
eAtEthPortSpeed AtEthPortSpeedGet(AtEthPort port);

/* Working mode */
eAtModuleEthRet AtEthPortDuplexModeSet(AtEthPort port, eAtEthPortDuplexMode duplexMode);
eAtEthPortDuplexMode AtEthPortDuplexModeGet(AtEthPort port);

/* Auto Negotiation */
eAtModuleEthRet AtEthPortAutoNegEnable(AtEthPort self, eBool enable);
eBool AtEthPortAutoNegIsEnabled(AtEthPort self);
eAtModuleEthRet AtEthPortAutoNegRestartOn(AtEthPort self, eBool on);
eBool AtEthPortAutoNegRestartIsOn(AtEthPort self);
eAtEthPortAutoNegState AtEthPortAutoNegStateGet(AtEthPort self);
eBool AtEthPortAutoNegIsSupported(AtEthPort self);
eAtEthPortSpeed AtEthPortAutoNegSpeedGet(AtEthPort self);
eAtEthPortDuplexMode AtEthPortAutoNegDuplexModeGet(AtEthPort self);
eBool AtEthPortAutoNegIsComplete(AtEthPort self);
eAtEthPortLinkStatus AtEthPortLinkStatus(AtEthPort self);
eAtEthPortRemoteFault AtEthPortRemoteFaultGet(AtEthPort self);

/* FEC */
eAtModuleEthRet AtEthPortRxFecEnable(AtEthPort self, eBool enabled);
eBool AtEthPortRxFecIsEnabled(AtEthPort self);
eAtModuleEthRet AtEthPortTxFecEnable(AtEthPort self, eBool enabled);
eBool AtEthPortTxFecIsEnabled(AtEthPort self);
eBool AtEthPortFecIsSupported(AtEthPort self);

/* Inter Packet Gap */
eAtModuleEthRet AtEthPortTxIpgSet(AtEthPort port, uint8 ipg);
uint8 AtEthPortTxIpgGet(AtEthPort port);
eBool AtEthPortTxIpgIsConfigurable(AtEthPort self);
eAtModuleEthRet AtEthPortRxIpgSet(AtEthPort port, uint8 ipg);
uint8 AtEthPortRxIpgGet(AtEthPort port);
eBool AtEthPortRxIpgIsConfigurable(AtEthPort self);

/* Flow control */
AtEthFlowControl AtEthPortFlowControlGet(AtEthPort self);

/* Controlling LED */
eAtModuleEthRet AtEthPortLedStateSet(AtEthPort self, eAtLedState ledState);
eAtLedState AtEthPortLedStateGet(AtEthPort self);

/* Controlling of SERDES */
AtSerdesController AtEthPortSerdesController(AtEthPort self);

/* Max/min packet size */
eAtModuleEthRet AtEthPortMaxPacketSizeSet(AtEthPort self, uint32 maxPacketSize);
uint32 AtEthPortMaxPacketSizeGet(AtEthPort self);
eAtModuleEthRet AtEthPortMinPacketSizeSet(AtEthPort self, uint32 minPacketSize);
uint32 AtEthPortMinPacketSizeGet(AtEthPort self);

/* Switch/bridge at MAC layer */
eAtModuleEthRet AtEthPortSwitch(AtEthPort self, AtEthPort toPort);
AtEthPort AtEthPortSwitchedPortGet(AtEthPort self);
eAtModuleEthRet AtEthPortSwitchRelease(AtEthPort self);
eAtModuleEthRet AtEthPortBridge(AtEthPort self, AtEthPort toPort);
AtEthPort AtEthPortBridgedPortGet(AtEthPort self);
eAtModuleEthRet AtEthPortBridgeRelease(AtEthPort self);

/* Switch/bridge at SERDES layer */
eAtModuleEthRet AtEthPortRxSerdesSelect(AtEthPort self, AtSerdesController serdes);
AtSerdesController AtEthPortRxSelectedSerdes(AtEthPort self);
eAtModuleEthRet AtEthPortTxSerdesSet(AtEthPort self, AtSerdesController serdes);
AtSerdesController AtEthPortTxSerdesGet(AtEthPort self);
eAtModuleEthRet AtEthPortTxSerdesBridge(AtEthPort self, AtSerdesController serdes);
AtSerdesController AtEthPortTxBridgedSerdes(AtEthPort self);

/* Current in-used bandwidth */
uint32 AtEthPortProvisionedBandwidthInKbpsGet(AtEthPort self);
uint32 AtEthPortRunningBandwidthInKbpsGet(AtEthPort self);

/* HiGig Mode */
eAtModuleEthRet AtEthPortHiGigEnable(AtEthPort self, eBool enable);
eBool AtEthPortHiGigIsEnabled(AtEthPort self);

/* Queue control */
uint32 AtEthPortQueueCurrentBandwidthInBpsGet(AtEthPort self, uint8 queueId);
AtList AtEthPortQueueAllPwsGet(AtEthPort self, uint8 queueId);
uint32 AtEthPortMaxQueuesGet(AtEthPort self);

/* MAC error force */
AtErrorGenerator AtEthPortTxErrorGeneratorGet(AtEthPort self);
AtErrorGenerator AtEthPortRxErrorGeneratorGet(AtEthPort self);

/* Sub ethernet port */
AtEthPort AtEthPortSubPortGet(AtEthPort self, uint32 subPortId);
uint32 AtEthPortMaxSubPortsGet(AtEthPort self);
AtEthPort AtEthPortParentPortGet(AtEthPort self);

/* Drop packet condition. See eAtEthPortDropCondition */
eAtRet AtEthPortDropPacketConditionMaskSet(AtEthPort self, uint32 conditionMask, uint32 enableMask);
uint32 AtEthPortDropPacketConditionMaskGet(AtEthPort self);

/* Disparity forcing */
eAtRet AtEthPortDisparityForce(AtEthPort self, eBool forced);
eBool AtEthPortDisparityIsForced(AtEthPort self);
eBool AtEthPortDisparityCanForce(AtEthPort self);

/* Force K30.7 on 1000basex */
eAtRet AtEthPortK30_7Force(AtEthPort self, eBool forced);
eBool AtEthPortK30_7IsForced(AtEthPort self);
eBool AtEthPortK30_7CanForce(AtEthPort self);

/* Clock monitor */
AtClockMonitor AtEthPortClockMonitorGet(AtEthPort self);

/* Link training */
eAtModuleEthRet AtEthPortLinkTrainingEnable(AtEthPort self, eBool enable);
eBool AtEthPortLinkTrainingIsEnabled(AtEthPort self);
eAtModuleEthRet AtEthPortLinkTrainingRestart(AtEthPort self);
eBool AtEthPortLinkTrainingIsSupported(AtEthPort self);

#ifdef __cplusplus
}
#endif
#endif /* _ATETHPORT_H_ */

#include "AtEthPortDeprecated.h"

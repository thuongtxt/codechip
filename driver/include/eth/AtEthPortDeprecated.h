/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : AtEthPortDeprecated.h
 * 
 * Created Date: Jun 1, 2016
 *
 * Description : Deprecated ETH port interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATETHPORTDEPRECATED_H_
#define _ATETHPORTDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
#define cAtEthPortCounterTxPauFrames cAtEthPortCounterTxPausePackets
#define cAtEthPortCounterRxPauFrames cAtEthPortCounterRxPausePackets

#define cAtEthPortAlarmRxFault       cAtEthPortAlarmLocalFault
#define cAtEthPortAlarmRxRemoteFault cAtEthPortAlarmRemoteFault

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModuleEthRet AtEthPortFlowControlEnable(AtEthPort port, eBool enable);
eBool AtEthPortFlowControlIsEnabled(AtEthPort port);
eAtModuleEthRet AtEthPortPauseFrameIntervalSet(AtEthPort port, uint32 interval);
uint32 AtEthPortPauseFrameIntervalGet(AtEthPort port);
eAtModuleEthRet AtEthPortPauseFrameExpireTimeSet(AtEthPort port, uint32 expireTime);
uint32 AtEthPortPauseFrameExpireTimeGet(AtEthPort port);

/* Eye scan controller */
AtEyeScanController AtEthPortEyeScanControllerGet(AtEthPort self);

#ifdef __cplusplus
}
#endif
#endif /* _ATETHPORTDEPRECATED_H_ */


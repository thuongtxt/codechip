/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : AtDeviceDiagnostic.h
 * 
 * Created Date: Oct 1, 2016
 *
 * Description : Device diagnostic
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEVICEDIAGNOSTIC_H_
#define _ATDEVICEDIAGNOSTIC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtInterruptPin
 * @{
 */

/**
 * @brief Trigger mode
 */
typedef enum eAtTriggerMode
    {
    cAtTriggerModeUnknown,   /**< Unknown mode */
    cAtTriggerModeEdge,      /**< Edge trigger */
    cAtTriggerModeLevelHigh, /**< Low to high */
    cAtTriggerModeLevelLow   /**< High to low */
    }eAtTriggerMode;

/** @brief Interrupt PIN */
typedef struct tAtInterruptPin * AtInterruptPin;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Read-only */
AtDevice AtInterruptPinDeviceGet(AtInterruptPin self);
uint32 AtInterruptPinIdGet(AtInterruptPin self);

/* Trigger mode */
eAtRet AtInterruptPinTriggerModeSet(AtInterruptPin self, eAtTriggerMode triggerMode);
eAtTriggerMode AtInterruptPinTriggerModeGet(AtInterruptPin self);

/* Trigger */
eAtRet AtInterruptPinTrigger(AtInterruptPin self, eBool triggered);
eBool AtInterruptPinIsTriggered(AtInterruptPin self);
eBool AtInterruptPinIsAsserted(AtInterruptPin self);

#ifdef __cplusplus
}
#endif
#endif /* _ATDEVICEDIAGNOSTIC_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : AtDeviceDeprecated.h
 * 
 * Created Date: Jan 20, 2016
 *
 * Description : AtDeviceDeprecated declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEVICEDEPRECATED_H_
#define _ATDEVICEDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSemController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSemController AtDeviceSemControllerGet(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _ATDEVICEDEPRECATED_H_ */


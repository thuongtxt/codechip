/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : AtModuleClasses.h
 * 
 * Created Date: Apr 5, 2015
 *
 * Description : All common modules
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULECLASSES_H_
#define _ATMODULECLASSES_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleEncap
 * @{
 */

/** @brief Encapsulation module class */
typedef struct tAtModuleEncap * AtModuleEncap;

/**
 * @}
 */

/**
 * @addtogroup AtModulePpp
 * @{
 */

/** @brief Module PPP */
typedef struct tAtModulePpp * AtModulePpp;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATMODULECLASSES_H_ */


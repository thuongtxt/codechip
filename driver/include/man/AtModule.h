/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : AtModule.h
 * 
 * Created Date: Jul 25, 2012
 *
 * Description : Module abstract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULE_H_
#define _ATMODULE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup grpAtDevMan
 * @{
 */

/**
 * @brief Supported module types
 */
typedef enum eAtModule
    {
    cAtModuleStart,                        /**< Start module ID */
    cAtModuleSdh = cAtModuleStart,         /**< SDH module */
    cAtModulePdh,                          /**< PDH module */
    cAtModulePpp,                          /**< PPP/MLPPP module */
    cAtModuleFr,                           /**< Frame relay module */
    cAtModuleEncap,                        /**< Encapsulation module */
    cAtModuleEth,                          /**< Ethernet module */
    cAtModulePw,                           /**< Pseudowire module */
    cAtModulePrbs,                         /**< PRBS module */
    cAtModuleIma,                          /**< IMA module */
    cAtModuleAtm,                          /**< ATM module */
    cAtModuleRam,                          /**< RAM module */
    cAtModuleBer,                          /**< BER module */
    cAtModulePktAnalyzer,                  /**< Packet analyzer module */
    cAtModuleClock,                        /**< Clock module */
    cAtModuleConcate,     				   /**< Concate module */
    cAtModuleXc,                           /**< XC module */
    cAtModuleAps,                          /**< APS module */
    cAtModuleSur,                          /**< Surveillance module */
    cAtModulePtp,                          /**< PTP module */
    cAtModuleNumModules,                   /**< Number of possible supported modules */
    cAtModuleInvalid = cAtModuleNumModules /**< Invalid module */
    }eAtModule;

/** @brief Default listener interface */
typedef struct tAtModuleAlarmListener
    {
    void (*AlarmChangeStateWithUserData)(AtModule module, uint32 changedAlarms, uint32 currentStatus, void *userData); /**< Called when Alarm change state */
    }tAtModuleAlarmListener;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModule AtModuleTypeGet(AtModule self);
const char *AtModuleTypeString(AtModule self);
const char *AtModuleCapacityDescription(AtModule self);
AtDevice AtModuleDeviceGet(AtModule self);
eAtRet AtModuleInit(AtModule self);
eAtRet AtModuleInterruptEnable(AtModule self, eBool enable);
eBool AtModuleInterruptIsEnabled(AtModule self);
eAtRet AtModuleLock(AtModule self);
eAtRet AtModuleUnLock(AtModule self);

uint32 AtModulePeriodicProcess(AtModule self, uint32 periodInMs);
uint32 AtModuleRecommendedPeriodInMs(AtModule self);
uint32 AtModuleForcePointerPeriodicProcess(AtModule self, uint32 periodInMs);

/* Debugging */
eBool AtModuleMemoryCanBeTested(AtModule self);
eAtRet AtModuleMemoryTest(AtModule self);
void AtModuleStatusClear(AtModule self);
eAtRet AtModuleDebug(AtModule self);
eAtRet AtModuleDebugEnable(AtModule self, eBool enable);
eBool AtModuleDebugIsEnabled(AtModule self);

/* Loopback at module level */
eAtRet AtModuleDebugLoopbackSet(AtModule self, uint8 loopMode);
uint8 AtModuleDebugLoopbackGet(AtModule self);
eBool AtModuleDebugLoopbackIsSupported(AtModule self, uint8 loopMode);

/* Event listener */
eAtRet AtModuleEventListenerAdd(AtModule self, void *listener, void * userData);
eAtRet AtModuleEventListenerRemove(AtModule self, void *listener);

/* Alarm and Interrup mask */
uint32 AtModuleAlarmGet(AtModule self);
uint32 AtModuleAlarmInterruptGet(AtModule self);
uint32 AtModuleAlarmInterruptClear(AtModule self);
eAtRet AtModuleInterruptMaskSet(AtModule self, uint32 eventMask, uint32 enableMask);
uint32 AtModuleInterruptMaskGet(AtModule self);
eBool AtModuleInterruptMaskIsSupported(AtModule self, uint32 eventMask);
#ifdef __cplusplus
}
#endif
#endif /* _ATMODULE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device Driver management
 * 
 * File        : AtDevice.h
 * 
 * Created Date: Jul 24, 2012
 *
 * Description : AT generic class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEVICE_H_
#define _ATDEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtHal.h"
#include "AtModule.h"
#include "AtIpCore.h"
#include "AtIterator.h"
#include "AtSemController.h"
#include "AtThermalSensor.h"
#include "AtPowerSupplySensor.h"
#include "AtSerdesManager.h"
#include "AtInterruptPin.h"
#include "AtQuerier.h"
#include "AtDebugger.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtDevice
 * @{
 */

/** @brief Register listener interface */
typedef struct tAtRegisterWriteEventListener
    {
    /* For short registers */
    void (*ShortRead)(AtDevice self, uint32 address, uint32 value, uint8 coreId);
    void (*ShortWrite)(AtDevice self, uint32 address, uint32 value, uint8 coreId);

    /* For long registers */
    void (*LongRead)(AtDevice self,   uint32 address, const uint32 *dataBuffer, uint16 length, uint8 moduleId, uint8 coreId);
    void (*LongWrite)(AtDevice self,   uint32 address, const uint32 *dataBuffer, uint16 length, uint8 moduleId, uint8 coreId);
    } tAtRegisterWriteEventListener;

/** @brief Device listener */
typedef struct tAtDeviceListener
    {
    void (*DidReadRegister)(AtDevice self, uint32 address, uint32 value, uint8 coreId, void *userData);  /**< Called after reading a register */
    void (*DidWriteRegister)(AtDevice self, uint32 address, uint32 value, uint8 coreId, void *userData); /**< Called after writing a register */
    void (*DidLongReadOnCore)(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId, void *userData);  /**< Called after long reading a register */
    void (*DidLongWriteOnCore)(AtDevice self, uint32 address, const uint32 *dataBuffer, uint16 numDwords, uint8 coreId, void *userData); /**< Called after long writing a register */
    void (*DidChangeRole)(AtDevice self, uint32 role, void *userData); /**< Called after changing device role is completed */

    /* For cleanup */
    void (*WillRemoveListener)(AtDevice self, void *userData); /**< Called before removing a listener */
    }tAtDeviceListener;

/** @brief Device event listener such as device protection switch */
typedef struct tAtDeviceEventListener
    {
    void (*AlarmChangeStateWithUserData)(AtDevice self, uint32 changedAlarms, uint32 currentStatus, void *userData);
    }tAtDeviceEventListener;

/**
 * @brief Device role
 */
typedef enum eAtDeviceRole
    {
    cAtDeviceRoleActive,  /**< Active */
    cAtDeviceRoleStandby, /**< Standby */
    cAtDeviceRoleAuto,    /**< Only used with AtDeviceRoleSet() or products do
                               not have role attribute */
    cAtDeviceRoleUnknown  /**< Unknown */
    }eAtDeviceRole;

/**
 * @brief Device alarms
 */
typedef enum eAtDeviceAlarmType
    {
    cAtDeviceAlarmProtectionSwitched = cBit0 /**< Event when a device is switched from standby to active in the 1:N protection */
    } eAtDeviceAlarmType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

uint32 AtDeviceProductCodeGet(AtDevice self);
AtDriver AtDeviceDriverGet(AtDevice self);
eAtRet AtDeviceInit(AtDevice self);
eAtRet AtDeviceAsyncInit(AtDevice self);
uint32 AtDeviceAsyncDelayTimeUsGet(AtDevice self);

/* Device role (used for equipment protection) */
eAtRet AtDeviceRoleSet(AtDevice self, eAtDeviceRole role);
eAtDeviceRole AtDeviceRoleGet(AtDevice self);
const char *AtDeviceRole2String(eAtDeviceRole role);
eBool AtDeviceHasRole(AtDevice self);
eAtDeviceRole AtDeviceAutoRoleGet(AtDevice self);

/* Query version information */
const char *AtDeviceVersion(AtDevice self);
uint32 AtDeviceVersionNumber(AtDevice self);
uint32 AtDeviceBuiltNumber(AtDevice self);

/* To explicitly specify version information. They are used when hardware is not
 * accessible and application needs to tell the SDK about hardware version */
eAtRet AtDeviceVersionNumberSet(AtDevice self, uint32 versionNumber);
eAtRet AtDeviceBuiltNumberSet(AtDevice self, uint32 builtNumber);

/* Warm restore */
eAtRet AtDeviceWarmRestore(AtDevice self);

/* There may be some products that the AtDeviceWarmRestore() does not work since
 * hardware does not support. The following APIs are used to help the device
 * recover its database. (See programming guide for more detail) */
eAtRet AtDeviceWarmRestoreStart(AtDevice self);
eAtRet AtDeviceWarmRestoreStop(AtDevice self);
eBool AtDeviceWarmRestoreIsStarted(AtDevice self);

/* IP Cores */
uint8 AtDeviceNumIpCoresGet(AtDevice self);
AtIpCore AtDeviceIpCoreGet(AtDevice self, uint8 coreId);
eAtRet AtDeviceIpCoreHalSet(AtDevice self, uint8 coreId, AtHal hal);
AtHal AtDeviceIpCoreHalGet(AtDevice self, uint8 coreId);

/* Get module instance */
const eAtModule *AtDeviceAllSupportedModulesGet(AtDevice self, uint8 *numModules);
eBool AtDeviceModuleIsSupported(AtDevice self, eAtModule moduleId);
AtModule AtDeviceModuleGet(AtDevice self, eAtModule moduleId);

/* Util */
uint32 AtProductCodeGet(uint32 baseAddressOfCore);
uint32 AtProductCodeGetByHal(AtHal hal);
uint32 AtDeviceVersionNumberFromHwGet(AtDevice self);
uint32 AtDeviceBuiltNumberFromHwGet(AtDevice self);

/* Interrupt */
void AtDeviceInterruptProcess(AtDevice self);
eAtRet AtDeviceInterruptRestore(AtDevice self);

/* Periodic process */
void AtDevicePeriodicProcess(AtDevice self, uint32 periodInMs);

/* Soft Error Mitigation controller */
AtSemController AtDeviceSemControllerGetByIndex(AtDevice self, uint8 semId);
uint8 AtDeviceNumSemControllersGet(AtDevice self);
eBool AtDeviceSemControllerIsSupported(AtDevice self);

/* Physical sensors */
AtThermalSensor AtDeviceThermalSensorGet(AtDevice self);
eBool AtDeviceThermalSensorIsSupported(AtDevice self);
AtPowerSupplySensor AtDevicePowerSupplySensorGet(AtDevice self);

/* SERDES manager */
AtSerdesManager AtDeviceSerdesManagerGet(AtDevice self);

/* Interrupt PINs */
uint32 AtDeviceNumInterruptPins(AtDevice self);
AtInterruptPin AtDeviceInterruptPinGet(AtDevice self, uint32 pinId);

/* Iterators */
AtIterator AtDeviceModuleIteratorCreate(AtDevice self);
AtIterator AtDeviceCoreIteratorCreate(AtDevice self);

/* Long read/write (for debugging purpose) */
uint16 AtDeviceLongReadOnCore(AtDevice self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId);
uint16 AtDeviceLongWriteOnCore(AtDevice self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId);

/* Memory testing */
eAtRet AtDeviceMemoryTest(AtDevice self);

/* Debugging */
AtDebugger AtDeviceDebuggerGet(AtDevice self);
void AtDeviceDebug(AtDevice self);
AtDebugger AtDeviceDebuggerCreate(AtDevice self);
void AtDeviceDebuggerDelete(AtDevice self);
void AtDeviceStatusClear(AtDevice self);
eAtRet AtDeviceSSKeyCheck(AtDevice self);
eAtRet AtDeviceAllServicesDestroy(AtDevice self);
eAtRet AtDeviceListenerAdd(AtDevice self, const tAtDeviceListener *callback, void *userData);
eAtRet AtDeviceListenerRemove(AtDevice self, const tAtDeviceListener *callback);
eAtRet AtDeviceReset(AtDevice self);
eAtRet AtDeviceAsyncReset(AtDevice self);
eBool AtDeviceAsyncRetValIsInState(eAtRet ret);
AtQuerier AtDeviceQuerierGet(AtDevice self);

/* Register Audit */
eAtRet AtDeviceRegisterEventListenerAdd(AtDevice self, tAtRegisterWriteEventListener *listener);
eAtRet AtDeviceRegisterEventListenerRemove(AtDevice self);
eBool AtDeviceRegisterEventListenerIsExisted(AtDevice self);

/* Diagnostic */
eAtRet AtDeviceDiagnosticModeEnable(AtDevice self, eBool enable);
eBool AtDeviceDiagnosticModeIsEnabled(AtDevice self);
eAtRet AtDeviceDiagnosticCheckEnable(AtDevice self, eBool enable);
eBool AtDeviceDiagnosticCheckIsEnabled(AtDevice self);
AtUart AtDeviceDiagnosticUartGet(AtDevice self);

void AtDeviceAllChannelsListenedDefectClear(AtDevice self);

/* Card 1:N protection. Number of sub devices equal to number of protected devices */
AtDevice AtDeviceParentDeviceGet(AtDevice self);
uint8 AtDeviceNumSubDeviceGet(AtDevice self);
AtDevice AtDeviceSubDeviceGet(AtDevice self, uint8 subDeviceIndex);
AtDevice *AtDeviceAllSubDevicesGet(AtDevice self, uint8 *numSubDevices);
uint16 AtDeviceReadOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 *dataBuffer, uint16 bufferLen, uint8 coreId);
uint16 AtDeviceWriteOnDdrOffload(AtDevice self, uint32 subDeviceId, uint32 ddrAddress, uint32 regAddress, const uint32 *dataBuffer, uint16 bufferLen, uint8 coreId);

/* Device interrupt */
eAtRet AtDeviceInterruptMaskSet(AtDevice self, uint32 interruptMask, uint32 enabledMask);
uint32 AtDeviceInterruptMaskGet(AtDevice self);
uint32 AtDeviceAlarmGet(AtDevice self);
uint32 AtDeviceAlarmInterruptGet(AtDevice self);
uint32 AtDeviceAlarmInterruptClear(AtDevice self);
eBool AtDeviceAlarmIsSupported(AtDevice self, uint32 alarmType);
eBool AtDeviceInterruptIsEnabled(AtDevice self);
eBool AtDeviceInterruptMaskIsSupported(AtDevice self, uint32 alarmMask);
eAtRet AtDeviceInterruptEnable(AtDevice self);
eAtRet AtDeviceInterruptDisable(AtDevice self);
eBool AtDeviceInterruptIsEnabled(AtDevice self);

/* Event listener */
eAtRet AtDeviceEventListenerAddWithUserData(AtDevice self, tAtDeviceEventListener *listener, void *userData);
eAtRet AtDeviceEventListenerRemove(AtDevice self, tAtDeviceEventListener *listener);
eBool AtDeviceSerdesResetIsSupportted(AtDevice self);
#ifdef __cplusplus
}
#endif
#endif /* _ATDEVICE_H_ */

#include "AtDeviceDeprecated.h"


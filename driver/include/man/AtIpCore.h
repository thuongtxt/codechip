/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device Driver Management
 * 
 * File        : AtIpCore.h
 * 
 * Created Date: Aug 4, 2012
 *
 * Description : IP Core class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIPCORE_H_
#define _ATIPCORE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtIpCore
 * @{
 */
typedef struct tAtIpCoreListener
    {
    void (*InterruptHappened)(AtIpCore self, void *userData);  /**< Called when interrupt happened */
    void (*InterruptResolved)(AtIpCore self, void *userData);  /**< Only applicable
        for polling mode and it will be called when interrupt has just been resolved */
    void (*WillRemoveListener)(AtIpCore self, void *userData); /**< Call before
        listener is removed. Useful for cleaning up user data */
    }tAtIpCoreListener;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* HAL */
eAtRet AtIpCoreHalSet(AtIpCore self, AtHal hal);
AtHal AtIpCoreHalGet(AtIpCore self);

uint8 AtIpCoreIdGet(AtIpCore self);
AtDevice AtIpCoreDeviceGet(AtIpCore self);

eAtRet AtIpCoreInterruptEnable(AtIpCore self, eBool enable);
eBool AtIpCoreInterruptIsEnabled(AtIpCore self);
eAtRet AtIpCoreSemInterruptEnable(AtIpCore self, eBool enable);
eBool AtIpCoreSemInterruptIsEnabled(AtIpCore self);
eAtRet AtIpCoreSysmonInterruptEnable(AtIpCore self, eBool enable);
eBool AtIpCoreSysmonInterruptIsEnabled(AtIpCore self);

eAtRet AtIpCoreListenerAdd(AtIpCore self, const tAtIpCoreListener *callback, void *userData);
eAtRet AtIpCoreListenerRemove(AtIpCore self, const tAtIpCoreListener *callback);

void AtIpCoreDebug(AtIpCore self);

#ifdef __cplusplus
}
#endif
#endif /* _ATIPCORE_H_ */


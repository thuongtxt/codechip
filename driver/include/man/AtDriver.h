/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device driver management
 * 
 * File        : AtDriver.h
 * 
 * Created Date: Jul 24, 2012
 *
 * Description : This class is to manage all of AT devices in one driver
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDRIVER_H_
#define _ATDRIVER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"
#include "AtDevice.h"
#include "AtLogger.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtDriver
 * @{
 */

/** @brief Driver role */
typedef enum eAtDriverRole
    {
    cAtDriverRoleActive,  /**< Active */
    cAtDriverRoleStandby, /**< Standby */
    cAtDriverRoleUnknown  /**< Unknown */
    }eAtDriverRole;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Driver version */
const char *AtDriverVersion(AtDriver self);
uint32 AtDriverBuiltNumber(AtDriver self);
const char *AtDriverVersionDescription(AtDriver self);

/* Role */
eAtRet AtDriverRoleSet(AtDriver self, eAtDriverRole role);
eAtDriverRole AtDriverRoleGet(AtDriver self);
uint32 AtDriverRestore(AtDriver self); /* Restore driver database after switching from standby to active role */

/* Singletons */
AtDriver AtDriverSharedDriverGet(void);
AtOsal AtDriverOsalGet(AtDriver self);

/* Logger */
eAtRet AtDriverLoggerSet(AtDriver self, AtLogger logger);
AtLogger AtDriverLoggerGet(AtDriver self);
void AtDriverLog(AtDriver self, eAtLogLevel level, const char *format, ...) AtAttributePrintf(3, 4);

/* Create/delete driver */
AtDriver AtDriverCreate(uint8 maxNumberOfDevices, AtOsal osal);
void AtDriverDelete(AtDriver self);

/* Device management */
uint8 AtDriverMaxNumberOfDevicesGet(AtDriver self);
AtDevice *AtDriverAddedDevicesGet(AtDriver self, uint8 *numberOfAddedDevice);
uint8 AtDriverNumAddedDeviceGet(AtDriver self);
eBool AtDriverProductCodeIsValid(AtDriver self, uint32 productCode);
AtDevice AtDriverDeviceCreate(AtDriver self, uint32 productCode);
void AtDriverDeviceDelete(AtDriver self, AtDevice device);
AtDevice AtDriverDeviceGet(AtDriver self, uint8 deviceIndex);
uint8 AtDriverDeviceIndexGet(AtDriver self, AtDevice device);

/* Devices Iterator */
AtIterator AtDriverDeviceIteratorCreate(AtDriver self);
AtLogger AtSharedDriverLoggerGet(void);

/* Util */
eAtRet AtDriverLastErrorCode(void);
const char *AtDriverRet2String(eAtRet ret);

/* For debugging */
eAtRet AtDriverDebugEnable(eBool enable);
eBool AtDriverDebugIsEnabled(void);
void AtDriverDebug(AtDriver self);

eAtRet AtDriverApiLogEnable(eBool enabled);
eBool AtDriverApiLogIsEnabled(void);
eAtRet AtDriverAllApiLogEnable(eBool enabled);
eBool AtDriverAllApiLogIsEnabled(void);
eAtRet AtDriverMultiDevPrefixApiLogEnable(eBool enabled);
eBool AtDriverMultiDevPrefixApiLogIsEnabled(void);

#ifdef __cplusplus
}
#endif
#endif /* _ATDRIVER_H_ */


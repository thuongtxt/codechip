/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : AtRamTestError.h
 * 
 * Created Date: Mar 11, 2015
 *
 * Description : RAM test error definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATRAMTESTERROR_H_
#define _ATRAMTESTERROR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtRamTestError * AtRamTestError;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
const char* AtRamTestErrorDescriptionGet(AtRamTestError self);
uint32 AtRamTestErrorAddressGet(AtRamTestError self);
uint32 AtRamTestErrorExpectedValueGet(AtRamTestError self);
uint32 AtRamTestErrorActualValueGet(AtRamTestError self);

#ifdef __cplusplus
}
#endif
#endif /* _ATRAMTESTERROR_H_ */


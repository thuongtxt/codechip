/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : AtModuleRam.h
 * 
 * Created Date: Jan 31, 2013
 *
 * Description : To manage all RAMs that device is working on
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULERAM_H_
#define _ATMODULERAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/**
 * @addtogroup AtModuleRam
 * @{
 */

/**
 * @brief RAM class
 */
typedef struct tAtModuleRam * AtModuleRam;

/** @brief RAM device that this module manages */
typedef struct tAtRam * AtRam;

/** @brief Internal RAM class. */
typedef struct tAtInternalRam * AtInternalRam;

/** @brief RAM module return codes */
typedef uint32 eAtModuleRamRet;

/** @brief Default RAM listener interface */
typedef struct tAtModuleRamEventListener
    {
    void (*ErrorNotify)(AtInternalRam ram, uint32 errors, void * userData); /**< Called when error occurrence. */
    }tAtModuleRamEventListener;

/**
 * @}
 */

/*--------------------------- Entries ----------------------------------------*/
/* DDR management */
uint8 AtModuleRamNumDdrGet(AtModuleRam self);
AtRam AtModuleRamDdrGet(AtModuleRam self, uint8 ddrId);

/* QDR management */
uint8 AtModuleRamNumQdrGet(AtModuleRam self);
AtRam AtModuleRamQdrGet(AtModuleRam self, uint8 qdrId);

/* ZBT management */
uint8 AtModuleRamNumZbtGet(AtModuleRam self);
AtRam AtModuleRamZbtGet(AtModuleRam self, uint8 zbtId);

/* Internal RAM management. */
uint32 AtModuleRamNumInternalRams(AtModuleRam self);
AtInternalRam AtModuleRamInternalRamGet(AtModuleRam self, uint32 ramId);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULERAM_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : AtDdr.h
 * 
 * Created Date: Feb 21, 2013
 *
 * Description : DDR generic class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDDR_H_
#define _ATDDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtRam.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @brief DDR class
 */
typedef struct tAtDdr * AtDdr;

/**
 * @brief Counter types
 */
typedef enum eAtDdrCounterType
    {
    cAtDdrCounterTypeClock = cAtRamCounterTypeDdrStartCounter, /**< Clock counter */
    cAtDdrCounterTypeRead,  /**< Read counter */
    cAtDdrCounterTypeWrite, /**< Read counter */
    cAtDdrCounterTypeIdle,  /**< Idle counter */
    cAtDdrCounterTypeError  /**< Error counter */
    }eAtDdrCounterType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATDDR_H_ */


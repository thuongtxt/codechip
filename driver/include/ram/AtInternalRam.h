/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : AtInternalRam.h
 * 
 * Created Date: Dec 4, 2015
 *
 * Description : Internal RAM interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATINTERNALRAM_H_
#define _ATINTERNALRAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtModuleRam.h"
#include "AtErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Parity/ECC/CRC monitoring */
eAtRet AtInternalRamParityMonitorEnable(AtInternalRam self, eBool enable);
eBool AtInternalRamParityMonitorIsEnabled(AtInternalRam self);
eBool AtInternalRamParityMonitorIsSupported(AtInternalRam self);

eAtRet AtInternalRamEccMonitorEnable(AtInternalRam self, eBool enable);
eBool AtInternalRamEccMonitorIsEnabled(AtInternalRam self);
eBool AtInternalRamEccMonitorIsSupported(AtInternalRam self);

eAtRet AtInternalRamCrcMonitorEnable(AtInternalRam self, eBool enable);
eBool AtInternalRamCrcMonitorIsEnabled(AtInternalRam self);
eBool AtInternalRamCrcMonitorIsSupported(AtInternalRam self);

/* Error force. */
eAtRet AtInternalRamErrorForce(AtInternalRam self, uint32 errors);
eAtRet AtInternalRamErrorUnForce(AtInternalRam self, uint32 errors);
uint32 AtInternalRamForcedErrorsGet(AtInternalRam self);
uint32 AtInternalRamForcableErrorsGet(AtInternalRam self);

/* Error history. */
uint32 AtInternalRamErrorHistoryGet(AtInternalRam self);
uint32 AtInternalRamErrorHistoryClear(AtInternalRam self);

/* Description */
const char *AtInternalRamDescription(AtInternalRam self);

/* Auxiliary */
uint32 AtInternalRamIdGet(AtInternalRam self);
AtModuleRam AtInternalRamModuleRamGet(AtInternalRam self);

/* RTL module maintain internal RAMs consistency for all products.
 * But in each specific product, not all internal RAMs will be used. They can be
 * defined to be OFF (i.e. 'reserved' here) */
eBool AtInternalRamIsReserved(AtInternalRam self);

AtErrorGenerator AtInternalRamErrorGeneratorGet(AtInternalRam self);
eBool  AtInternalRamCounterTypeIsSupported(AtInternalRam self, uint16 counterType);
uint32 AtInternalRamCounterGet(AtInternalRam self, uint16 counterType);
uint32 AtInternalRamCounterClear(AtInternalRam self, uint16 counterType);

#ifdef __cplusplus
}
#endif
#endif /* _ATINTERNALRAM_H_ */


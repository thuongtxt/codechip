/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : AtRam.h
 * 
 * Created Date: Jan 31, 2013
 *
 * Description : RAM generic class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATRAM_H_
#define _ATRAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtModuleRam.h"
#include "AtIpCore.h"
#include "AtIterator.h"
#include "AtRamTestError.h"
#include "AtErrorGenerator.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtRam
 * @{
 */
/**
 * @brief RAM error type
 */
typedef enum eAtRamAlarm
    {
    cAtRamAlarmNone             = cBit0, /**< No alarm */
    cAtRamAlarmEccCorrectable   = cBit1, /**< Errors detected and correctable */
    cAtRamAlarmEccUnnorrectable = cBit2, /**< Errors detected and uncorrectable */
    cAtRamAlarmCrcError         = cBit3, /**< CRC error */
    cAtRamAlarmParityError      = cBit4  /**< Parity error */
    }eAtRamAlarm;

/**
 * @brief RAM counter type
 */
typedef enum eAtRamCounterType
    {
    cAtRamCounterTypeEccCorrectableErrors,   /**< Number of correctable errors */
    cAtRamCounterTypeEccUncorrectableErrors, /**< Number of uncorrectable errors */
    cAtRamCounterTypeCrcErrors,              /**< Number of CRC errors */
    cAtRamCounterTypeRead,                   /**< Number of Read operations */
    cAtRamCounterTypeWrite,                  /**< Number of Write operations */
    cAtRamCounterTypeErrors,                 /**< Number of general errors */

    /* For internal usages */
    cAtRamCounterTypeDdrStartCounter, /**< DDR start counter type (private used) */
    cAtRamCounterTypeZbtStartCounter = cAtRamCounterTypeDdrStartCounter + 128 /**< ZBT start counter type (private used) */
    }eAtRamCounterType;

/** @brief Error generator type */
typedef enum eAtRamGeneratorErrorType
    {
    cAtRamGeneratorErrorTypeInvalid,          /**< Invalid type */
    cAtRamGeneratorErrorTypeEccCorrectable,   /**< Correctable errors */
    cAtRamGeneratorErrorTypeEccUncorrectable, /**< Uncorrectable errors */
    cAtRamGeneratorErrorTypeCrcError,         /**< CRC errors */
    cAtRamGeneratorErrorTypeError             /**< General errors */
    } eAtRamGeneratorErrorType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModuleRamRet AtRamInit(AtRam self);
uint32 AtRamAddressBusSizeGet(AtRam self);
uint32 AtRamDataBusSizeGet(AtRam self);

/* The module that manage this RAM */
AtModuleRam AtRamModuleGet(AtRam self);
AtIpCore AtRamIpCoreGet(AtRam self);
uint8 AtRamIdGet(AtRam self);

/* Test RAM */
eAtModuleRamRet AtRamInitStatusGet(AtRam self);
eAtModuleRamRet AtRamAddressBusTest(AtRam self);
eAtModuleRamRet AtRamDataBusTest(AtRam self, uint32 address);
eAtModuleRamRet AtRamMemoryTest(AtRam self, uint32 startAddress, uint32 endAddress, uint32 *firstErrorAddress);
eBool AtRamIsTested(AtRam self);
eBool AtRamIsGood(AtRam self);
uint32 AtRamCalibMeasure(AtRam self);

/* Bursting */
uint32 AtRamTestMaxBurst(AtRam self);
eAtRet AtRamTestBurstSet(AtRam self, uint32 burst);
uint32 AtRamTestBurstGet(AtRam self);
eBool AtRamTestBurstIsSupported(AtRam self);

/* Hardware assist to speedup memory testing */
eAtRet AtRamTestHwAssistEnable(AtRam self, eBool enable);
eBool AtRamTestHwAssistIsEnabled(AtRam self);

/* Test result query */
uint32 AtRamDataBusTestErrorCount(AtRam self);
AtRamTestError AtRamDataBusTestNextError(AtRam self);
uint32 AtRamAddressBusTestErrorCount(AtRam self);
AtRamTestError AtRamAddressBusTestNextError(AtRam self);
uint32 AtRamMemoryTestErrorCount(AtRam self, uint32 *firstErrorAddress);

/* Margin detection */
eAtRet AtRamMarginDetect(AtRam self);
uint32 AtRamReadLeftMargin(AtRam self);
uint32 AtRamReadRightMargin(AtRam self);
uint32 AtRamWriteLeftMargin(AtRam self);
uint32 AtRamWriteRightMargin(AtRam self);

/* Alarm and history */
uint32 AtRamAlarmGet(AtRam self);
uint32 AtRamAlarmHistoryGet(AtRam self);
uint32 AtRamAlarmHistoryClear(AtRam self);
eBool AtRamAlarmIsSupported(AtRam self, uint32 alarm);

/* Error counters */
uint32 AtRamCounterGet(AtRam self, uint32 counterType);
uint32 AtRamCounterClear(AtRam self, uint32 counterType);
eBool AtRamCounterIsSupported(AtRam self, uint32 counterType);

/* Testing duration(only for hardware testing method) */
eAtModuleRamRet AtRamTestDurationSet(AtRam self, uint32 durationInMs);
uint32 AtRamTestDurationGet(AtRam self);

/* For long time testing */
eAtModuleRamRet AtRamAddressBusTestWithDuration(AtRam self, uint32 durationMs);
eAtModuleRamRet AtRamDataBusTestWithDuration(AtRam self, uint32 durationMs);
eAtModuleRamRet AtRamMemoryTestWithDuration(AtRam self, uint32 durationMs);
eAtModuleRamRet AtRamTestWithDuration(AtRam self, uint32 durationMs);

/* Enable the accessing RAM from CPU */
eAtModuleRamRet AtRamAccessEnable(AtRam self, eBool enable);
eBool AtRamAccessIsEnabled(AtRam self);

/* Get cell size in number of block 32 bit */
uint32 AtRamCellSizeGet(AtRam self);
uint32 AtRamCellSizeInBits(AtRam self);

/* Read/write a memory cell in Ram */
eAtRet AtRamCellRead(AtRam self, uint32 address, uint32 *value);
eAtRet AtRamCellWrite(AtRam self, uint32 address, const uint32 *value);

/* Address translating helpers for RAM that locates address by bank, column, row
 * such as DRAM */
uint32 AtRamCellAddressGet(AtRam self, uint32 bank, uint32 row, uint32 column);
uint32 AtRamCellBankGet(AtRam self, uint32 address);
uint32 AtRamCellColumnGet(AtRam self, uint32 address);
uint32 AtRamCellRowGet(AtRam self, uint32 address);

/* Maximum RAM address */
uint32 AtRamMaxAddressGet(AtRam self);

/* For product that support hardware base testing */
eAtRet AtRamHwTestStart(AtRam self);
eAtRet AtRamHwTestStop(AtRam self);
eBool AtRamHwTestIsStarted(AtRam self);
eAtRet AtRamHwTestErrorForce(AtRam self, eBool forced);
eBool AtRamHwTestErrorIsForced(AtRam self);
const uint32 *AtRamLatchedErrorData(AtRam self, uint32 *numDwords);
const uint32 *AtRamLatchedExpectedData(AtRam self, uint32 *numDwords);
const uint32 *AtRamLatchedErrorDataBits(AtRam self, uint32 *numDwords);
uint32 AtRamLatchedErrorAddress(AtRam self);
eBool AtRamErrorLatchingSupported(AtRam self);

/* Deprecated, should use "Test result query" APIs for instead */
uint32 AtRamErrorCountGet(AtRam self);
uint32 AtRamFirstErrorAddressGet(AtRam self);

/* For debugging purpose */
void AtRamDebug(AtRam self);
eAtRet AtRamDebugAddressBusSizeSet(AtRam self, uint32 busSize);
eAtRet AtRamDebugDataBusSizeSet(AtRam self, uint32 busSize);

/* For Error Generator */
AtErrorGenerator AtRamErrorGeneratorGet(AtRam self);

#ifdef __cplusplus
}
#endif
#endif /* _ATRAM_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : AtZbt.h
 * 
 * Created Date: Feb 21, 2013
 *
 * Description : ZBT generic class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATZBT_H_
#define _ATZBT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtRam.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @brief ZBT class
 */
typedef struct tAtZbt * AtZbt;

/**
 * @brief Counter types
 */
typedef enum eAtZbtCounterType
    {
    cAtZbtCounterTypeError = cAtRamCounterTypeZbtStartCounter  /**< Error counter */
    }eAtZbtCounterType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATZBT_H_ */


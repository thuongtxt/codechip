/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : AtConcateGroup.h
 * 
 * Created Date: Sep 25, 2014
 *
 * Description : Concate Group VCG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCONCATEGROUP_H_
#define _ATCONCATEGROUP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h" /* Super class */
#include "AtModuleConcate.h"
#include "AtModuleEth.h"
#include "AtConcateMember.h"
#include "AtEncapChannel.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtConcateGroup
 * @{
 */

/** @brief LCAS side */
typedef enum eAtConcateGroupSide
    {
    cAtConcateGroupSideSource, /**< Source side */
    cAtConcateGroupSideSink,   /**< Sink side */
    cAtConcateGroupSideAll     /**< All sides */
    }eAtConcateGroupSide;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Source member management */
AtConcateMember AtConcateGroupSourceMemberProvision(AtConcateGroup self, AtChannel channel);
eAtModuleConcateRet AtConcateGroupSourceMemberDeProvision(AtConcateGroup self, AtChannel channel);
AtConcateMember AtConcateGroupSourceMemberGetByChannel(AtConcateGroup self, AtChannel channel);
AtConcateMember AtConcateGroupSourceMemberGetByIndex(AtConcateGroup self, uint32 memberIndex);
uint32 AtConcateGroupNumSourceMembersGet(AtConcateGroup self);

/* Sink member management */
AtConcateMember AtConcateGroupSinkMemberProvision(AtConcateGroup self, AtChannel channel);
eAtModuleConcateRet AtConcateGroupSinkMemberDeProvision(AtConcateGroup self, AtChannel channel);
AtConcateMember AtConcateGroupSinkMemberGetByChannel(AtConcateGroup self, AtChannel channel);
AtConcateMember AtConcateGroupSinkMemberGetByIndex(AtConcateGroup self, uint32 memberIndex);
uint32 AtConcateGroupNumSinkMembersGet(AtConcateGroup self);

/* Accessors */
uint32 AtConcateGroupMaxNumMembersGet(AtConcateGroup self);
eAtConcateGroupType AtConcateGroupConcatTypeGet(AtConcateGroup self);
eAtConcateMemberType AtConcateGroupMemberTypeGet(AtConcateGroup self);

/* Bound ENCAP channel */
AtEncapChannel AtConcateGroupEncapChannelGet(AtConcateGroup self);

/* Encapsulation mode */
eBool AtConcateGroupEncapTypeIsSupported(AtConcateGroup self, eAtEncapType encapType);
eAtModuleConcateRet AtConcateGroupEncapTypeSet(AtConcateGroup self, eAtEncapType encapType);
uint32 AtConcateGroupEncapTypeGet(AtConcateGroup self);

#endif /* _ATCONCATEGROUP_H_ */

#include "AtConcateGroupDeprecated.h"


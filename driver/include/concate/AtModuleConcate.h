/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : AtModuleConcate.h
 * 
 * Created Date: Apr 28, 2014
 *
 * Description : Concate module (LCAS/VCAT/CCAT)
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULECONCATE_H_
#define _ATMODULECONCATE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleConcate
 * @{
 */

/** @brief Concate module class */
typedef struct tAtModuleConcate *AtModuleConcate;

/** @brief Abstract concatenation group */
typedef struct tAtConcateGroup *AtConcateGroup;

/** @brief Virtual Concatenation Group */
typedef struct tAtVcg *AtVcg;

/** @brief Concate member class */
typedef struct tAtConcateMember *AtConcateMember;

/** @brief VCG binder */
typedef struct tAtVcgBinder *AtVcgBinder;

/** @brief Concate module return codes */
typedef uint32 eAtModuleConcateRet;
enum
    {
    cAtConcateErrorMemNotProvisioned = cAtErrorConcateStartCode, /**< Member has not been provisioned */
    cAtConcateErrorMemberAdded,         /**< Member was added */
    cAtConcateErrorMemberProvisioned,   /**< Member was provisioned */
    cAtConcateErrorMemberRemoveTimeout  /**< Member removing is timeout  */
    };

/** @brief Group concatenation type */
typedef enum eAtConcateGroupType
    {
    cAtConcateGroupTypeUnknown,    /**< Unknown concatenation type */
    cAtConcateGroupTypeVcat,       /**< VCAT */
    cAtConcateGroupTypeCcat,       /**< CCAT */
    cAtConcateGroupTypeNVcat,      /**< NON-VCAT */
    cAtConcateGroupTypeNVcat_g804, /**< NON-VCAT-G804 */
    cAtConcateGroupTypeNVcat_g8040 /**< NON-VCAT-G8040 */
    }eAtConcateGroupType;

/** @brief Concate member type */
typedef enum eAtConcateMemberType
    {
    cAtConcateMemberTypeUnknown,

    /* SDH VCs */
    cAtConcateMemberTypeVc4_64c, /**< VC-4-64c */
    cAtConcateMemberTypeVc4_16c, /**< VC-4-16c */
    cAtConcateMemberTypeVc4_4c,  /**< VC-4-4c */
    cAtConcateMemberTypeVc4_nc,  /**< VC-4-nc */
    cAtConcateMemberTypeVc4,     /**< VC-4 */
    cAtConcateMemberTypeVc3,     /**< VC-3 */
    cAtConcateMemberTypeVc12,    /**< VC-12 */
    cAtConcateMemberTypeVc11,    /**< VC-11 */

    /* PDH */
    cAtConcateMemberTypeDs1,     /**< DS1 */
    cAtConcateMemberTypeE1,      /**< E1 */
    cAtConcateMemberTypeDs3,     /**< DS3 */
    cAtConcateMemberTypeE3,      /**< E3 */

    cAtConcateMemberTypeAny,      /**< For some products that do not require VCG member type */

    /* For using query */
    cAtConcateMemberTypeStart = cAtConcateMemberTypeVc4_64c, /**< Start */
    cAtConcateMemberTypeEnd = cAtConcateMemberTypeE3         /**< End */
    }eAtConcateMemberType;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Capacity */
eBool AtModuleConcateGroupTypeIsSupported(AtModuleConcate self, eAtConcateGroupType concateType);
eBool AtModuleConcateMemberTypeIsSupported(AtModuleConcate self, eAtConcateMemberType memberType);

/* Concate group management */
AtConcateGroup AtModuleConcateGroupCreate(AtModuleConcate self, uint32 groupId, eAtConcateMemberType memberType, eAtConcateGroupType concateType);
eAtModuleConcateRet AtModuleConcateGroupDelete(AtModuleConcate self, uint32 groupId);
AtConcateGroup AtModuleConcateGroupGet(AtModuleConcate self, uint32 groupId);
uint32 AtModuleConcateMaxGroupGet(AtModuleConcate self);
uint32 AtModuleConcateFreeGroupGet(AtModuleConcate self);

/* Return Code to string */
const char *AtModuleConcateRet2String(eAtRet ret);

/* Global threshold for member de-skew latency per type */
uint32 AtModuleConcateDeskewLatencyResolutionInFrames(AtModuleConcate self, eAtConcateMemberType memberType);
eAtRet AtModuleConcateDeskewLatencyThresholdSet(AtModuleConcate self, eAtConcateMemberType memberType, uint32 thresholdInRes);
uint32 AtModuleConcateDeskewLatencyThresholdGet(AtModuleConcate self, eAtConcateMemberType memberType);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULECONCATE_H_ */

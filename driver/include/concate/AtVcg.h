/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : AtVcg.h
 * 
 * Created Date: Sep 25, 2014
 *
 * Description : VCG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATVCG_H_
#define _ATVCG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtConcateGroup.h" /* Super class */
#include "AtModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtVcg
 * @{
 */
/** @brief VCG alarm types */
typedef enum eAtVcgAlarmType
    {
    cAtVcgAlarmTypeNone = 0x0,  /**< No alarm */
    cAtVcgAlarmTypePlcT = 0x1,  /**< Partial Loss of Capacity Transmit */
    cAtVcgAlarmTypeTlcT = 0x2,  /**< Total Loss of Capacity Transmit */
    cAtVcgAlarmTypePlcR = 0x4,  /**< Partial Loss of Capacity Receive */
    cAtVcgAlarmTypeTlcR = 0x8,  /**< Total Loss of Capacity Receive */
    cAtVcgAlarmTypeSqnc = 0x10, /**< Inconsistent Sequence Indicator Numbers */
    cAtVcgAlarmTypeLoa  = 0x20  /**< Loss of Alignment */
    }eAtVcgAlarmType;
/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* VCAT/LCAS hot switch */
eBool AtVcgLcasIsSupported(AtVcg self);
eAtModuleConcateRet AtVcgLcasEnable(AtVcg self, eBool enable);
eBool AtVcgLcasIsEnabled(AtVcg self);

/* Hold-off timer */
eAtModuleConcateRet AtVcgHoldOffTimerSet(AtVcg self, uint32 timerInMs);
uint32 AtVcgHoldOffTimerGet(AtVcg self);

/* WTR timer */
eAtModuleConcateRet AtVcgWtrTimerSet(AtVcg self, uint32 timerInMs);
uint32 AtVcgWtrTimerGet(AtVcg self);

/* RMV timer */
eAtModuleConcateRet AtVcgRmvTimerSet(AtVcg self, uint32 timerInMs);
uint32 AtVcgRmvTimerGet(AtVcg self);

/* RS-ACK */
uint8 AtVcgSourceRsAckGet(AtVcg self);
uint8 AtVcgSinkRsAckGet(AtVcg self);

/* Member management */
eAtModuleConcateRet AtVcgSinkMemberAdd(AtVcg self, AtConcateMember member);
eAtModuleConcateRet AtVcgSinkMemberRemove(AtVcg self, AtConcateMember member);
eAtModuleConcateRet AtVcgSourceMemberAdd(AtVcg self, AtConcateMember member);
eAtModuleConcateRet AtVcgSourceMemberRemove(AtVcg self, AtConcateMember member);

#endif /* _ATVCG_H_ */

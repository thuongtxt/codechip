/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Concate
 * 
 * File        : AtConcateMember.h
 * 
 * Created Date: May 8, 2014
 *
 * Description : VCG member
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCONCATEMEMBER_H_
#define _ATCONCATEMEMBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtChannelClasses.h"
#include "AtModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
* @addtogroup AtConcateMember
* @{
*/

/** @brief The following enumeration define the Source member state. */
typedef enum eAtLcasMemberSourceState
    {
    cAtLcasMemberSourceStateIdle   = 0x0, /**< State of member is Idle */
    cAtLcasMemberSourceStateAdd    = 0x1, /**< State of member is Add */
    cAtLcasMemberSourceStateDnu    = 0x2, /**< State of member is Dnu */
    cAtLcasMemberSourceStateNorm   = 0x3, /**< State of member is Normal */
    cAtLcasMemberSourceStateEos    = 0x4, /**< State of member is EoS */
    cAtLcasMemberSourceStateRmv    = 0x5, /**< State of member is RMV */
    cAtLcasMemberSourceStateFixed  = 0x6, /**< State of member is Fixed */
    cAtLcasMemberSourceStateInvl   = 0xF  /**< State of member is Invalid */
    }eAtLcasMemberSourceState;

/** @brief The following enumeration define the Sink member state. */
typedef enum eAtLcasMemberSinkState
    {
    cAtLcasMemberSinkStateIdle  = 0x0, /**< State of member is Idle */
    cAtLcasMemberSinkStateFail  = 0x1, /**< State of member is Fail */
    cAtLcasMemberSinkStateOk    = 0x2, /**< State of member is OK */
    cAtLcasMemberSinkStateFixed = 0x3, /**< State of member is Fixed */
    cAtLcasMemberSinkStateInvl  = 0x4  /**< State of member is Invalid */
    }eAtLcasMemberSinkState;

/** @brief The following enumeration define the member control word. */
typedef enum eAtLcasMemberCtrl
    {
    cAtLcasMemberCtrlFixed = 0x0, /**< Member control is Fixed */
    cAtLcasMemberCtrlAdd   = 0x1, /**< Member control is Add */
    cAtLcasMemberCtrlNorm  = 0x2, /**< Member control is Normal */
    cAtLcasMemberCtrlEos   = 0x3, /**< Member control is EoS */
    cAtLcasMemberCtrlIdle  = 0x5, /**< Member control is Idle */
    cAtLcasMemberCtrlDnu   = 0xF, /**< Member control is Dnu */
    cAtLcasMemberCtrlInvl  = 0xFF /**< Member control is Invalid */
    }eAtLcasMemberCtrl;

/** @brief VCG member alarm types */
typedef enum eAtConcateMemberAlarmType
    {
    cAtConcateMemberAlarmTypeNone               = 0,      /**< No alarm */

    /* Sink alarms */
    cAtConcateMemberAlarmTypeCrc                = cBit0,  /**< CRC */
    cAtConcateMemberAlarmTypeLom                = cBit1,  /**< LOM */
    cAtConcateMemberAlarmTypeGidM               = cBit2,  /**< VCG GID Mismatch */
    cAtConcateMemberAlarmTypeSqm                = cBit3,  /**< Sequence mismatch */
    cAtConcateMemberAlarmTypeMnd                = cBit4,  /**< Member Not Deskewable */
    cAtConcateMemberAlarmTypeTsf                = cBit5,  /**< Trail Signal Failure */
    cAtConcateMemberAlarmTypeTsd                = cBit6,  /**< Trail Signal Degrade */
    cAtConcateMemberAlarmTypeMsu                = cBit7,  /**< Member Signal Unavailable (alias. MSU_L) */
    cAtConcateMemberAlarmTypeSqnc               = cBit8,  /**< Sequence Not Consistence */
    cAtConcateMemberAlarmTypeLoa                = cBit9,  /**< Loss-Of-Alignment */
    cAtConcateMemberAlarmTypeOom                = cBit10, /**< Out-Of-MultiFrame */
    cAtConcateMemberAlarmTypeSinkStateChange    = cBit11, /**< LCAS sink state change */

    /* Source alarms */
    cAtConcateMemberAlarmTypeSourceStateChange  = cBit12, /**< LCAS source state change */

    cAtConcateMemberAlarmTypeAll                = cBit12_0 /**< All alarms */
    }eAtConcateMemberAlarmType;

/**
 *@}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Source */
eAtLcasMemberSourceState AtConcateMemberSourceStateGet(AtConcateMember self);
eAtModuleConcateRet AtConcateMemberSourceSequenceSet(AtConcateMember self, uint32 sourceSequence);
uint32 AtConcateMemberSourceSequenceGet(AtConcateMember self);
uint32 AtConcateMemberSourceMstGet(AtConcateMember self);
eAtLcasMemberCtrl AtConcateMemberSourceControlGet(AtConcateMember self);
AtChannel AtConcateMemberSourceChannelGet(AtConcateMember self);

/* Sink */
eAtLcasMemberSinkState AtConcateMemberSinkStateGet(AtConcateMember self);
eAtModuleConcateRet AtConcateMemberSinkExpectedSequenceSet(AtConcateMember self, uint32 sinkSequence);
uint32 AtConcateMemberSinkExpectedSequenceGet(AtConcateMember self);
uint32 AtConcateMemberSinkSequenceGet(AtConcateMember self);
uint32 AtConcateMemberSinkMstGet(AtConcateMember self);
eAtLcasMemberCtrl AtConcateMemberSinkControlGet(AtConcateMember self);
AtChannel AtConcateMemberSinkChannelGet(AtConcateMember self);

/* Others */
AtChannel AtConcateMemberChannelGet(AtConcateMember self);
AtConcateGroup AtConcateMemberConcateGroupGet(AtConcateMember self);

#endif /* _ATCONCATEMEMBER_H_ */

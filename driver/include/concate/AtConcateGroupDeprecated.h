/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CONCATE
 * 
 * File        : AtConcateGroupDeprecated.h
 * 
 * Created Date: May 23, 2016
 *
 * Description : Deprecated Concatenation group interface.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCONCATEGROUPDEPRECATED_H_
#define _ATCONCATEGROUPDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Associations */
AtGfpChannel AtConcateGroupGfpChannelGet(AtConcateGroup self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCONCATEGROUPDEPRECATED_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATM
 * 
 * File        : AtAtmVirtualConnection.h
 * 
 * Created Date: Oct 4, 2012
 *
 * Description : ATM Virtual Connection
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATMVIRTUALCONNECTION_H_
#define _ATATMVIRTUALCONNECTION_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h" /* Super class */
#include "AtEthFlow.h"
#include "AtModuleAtm.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtAtmVirtualConnection
 * @{
 */
/**
 * @brief ATM AAL type
 */
typedef enum eAtAtmAalType
	{
	cAtAtmAalTypeInvalid, /**< Invalid */
	cAtAtmAal5TypeSnap,	  /**< SNAP */
	cAtAtmAal5TypeNlpid   /**< NLPID */
	} eAtAtmAalType;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Encapsulation type */
eAtAtmRet AtAtmVirtualConnectionAalTypeSet(AtAtmVirtualConnection self, eAtAtmAalType aalType);
eAtAtmAalType AtAtmVirtualConnectionAalTypeGet(AtAtmVirtualConnection self);

/* Mapping flow */
eAtAtmRet AtAtmVirtualConnectionEthFlowSet(AtAtmVirtualConnection self, AtEthFlow flow);
AtEthFlow AtAtmVirtualConnectionEthFlowGet(AtAtmVirtualConnection self);
eAtAtmRet AtAtmVirtualConnectionBind(AtAtmVirtualConnection self, AtChannel atmChannel);

#ifdef __cplusplus
}
#endif
#endif /* _ATATMVIRTUALCONNECTION_H_ */


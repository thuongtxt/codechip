/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATM
 * 
 * File        : AtModuleAtm.h
 * 
 * Created Date: Oct 3, 2012
 *
 * Description : ATM module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEATM_H_
#define _ATMODULEATM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModuleAtm
 * @{
 */

/** @brief ATM return error */
typedef enum eAtAtmRet
    {
    cAtAtmRetErrorVpiVciRange = cAtErrorAtmStartCode, /**< The VPI/VCI range to be specified for this group
                                                           exceeds the values provided when the IMA service was i nit */
    cAtAtmRetErrorNull, /**< NULL configuration structure.  */
    cAtAtmRetErrorLink, /**< link values specified are either 0 or exceed 32 */
    cAtAtmRetErrorMem,  /**< Unable to allocate resources memory. */
    cAtAtmRetErrorHw,   /**< Hardware access error.*/
    cAtAtmRetError      /**< General error */
    } eAtAtmRet;

/**
 * @brief ATM module class
 */
typedef struct tAtModuleAtm * AtModuleAtm;

/**
 * @brief ATM cross-connect class
 */
typedef struct tAtAtmCrossConnect * AtAtmCrossConnect;

/**
 * @brief ATM Virtual Connection class
 */
typedef struct tAtAtmVirtualConnection * AtAtmVirtualConnection;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* ATM Cross connect management */
uint32 AtModuleAtmMaxCrossConnectsGet(AtModuleAtm self);
uint32 AtModuleAtmFreeCrossConnectGet(AtModuleAtm self);
AtAtmCrossConnect AtModuleAtmCrossConnectCreate(AtModuleAtm self, uint32 xcId);
AtAtmCrossConnect AtModuleAtmCrossConnectGet(AtModuleAtm self, uint32 xcId);
eAtAtmRet AtModuleAtmCrossConnectDelete(AtModuleAtm self, uint32 xcId);

/* ATM Virtual Connection management */
uint32 AtModuleAtmMaxVirtualConnectionsGet(AtModuleAtm self);
uint32 AtModuleAtmFreeVirtualConnectionGet(AtModuleAtm self);
AtAtmVirtualConnection AtModuleAtmVirtualConnectionCreate(AtModuleAtm self, uint32 vcId, uint16 vpi, uint16 vci);
AtAtmVirtualConnection AtModuleAtmVirtualConnectionGet(AtModuleAtm self, uint32 vcId);
eAtAtmRet AtModuleAtmVirtualConnectionDelete(AtModuleAtm self, uint32 vcId);

/* ISIS MAC translation */
eAtAtmRet AtModuleAtmLlcISISMacSet(AtModuleAtm self, const uint8 *mac);
eAtAtmRet AtModuleAtmLlcISISMacGet(AtModuleAtm self, uint8 *mac);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEATM_H_ */


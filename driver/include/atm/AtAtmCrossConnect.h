/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATM
 * 
 * File        : AtAtmCrossConnect.h
 * 
 * Created Date: Oct 4, 2012
 *
 * Description : ATM cross-connect
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATMCROSSCONNECT_H_
#define _ATATMCROSSCONNECT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtChannel.h" /* Super class */
#include "AtModuleAtm.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtAtmCrossConnect
 * @{
 */

/**
 * @brief ATM cross connect type
 */
typedef enum eAtAtmCrossConnectType
    {
    cAtAtmCrossConnectTypeInvalid, /**< Invalid */
    cAtAtmCrossConnectTypeVpc,     /**< Switch based on VPI and Port */
    cAtAtmCrossConnectTypeVcc,     /**< Switch based on VPI, VCI and Port */
    cAtAtmCrossConnectTypePort     /**< Switch just based on Port */
    } eAtAtmCrossConnectType;

/**
 * @brief ATM service type
 */
typedef enum eAtAtmServiceType
    {
    cAtAtmServiceTypeInvalid, /**< Invalid */
    cAtAtmServiceTypeCbr,     /**< CBR */
    cAtAtmServiceTypeVbrNRT,  /**< VBR none real time */
    cAtAtmServiceTypeVbrRT,   /**< VBR real time */
    cAtAtmServiceTypeUbr,     /**< UBR */
    cAtAtmServiceTypeUbrPlus, /**< UBR+ */
    cAtAtmServiceTypeAbr      /**< ABR */
    } eAtAtmServiceType;

/**
 * @brief Policing mode
 */
typedef enum eAtAtmPolicingMode
    {
    cAtAtmPolicingModeInvalid,            /**< Invalid */
    cAtAtmPolicingModePcr0,               /**< PCR 0*/
    cAtAtmPolicingModePcr01,              /**< PCR 0/1 */
    cAtAtmPolicingModePcr01Pcr0Tagging,   /**< PCR 0/1, PCR 0 Tagging */
    cAtAtmPolicingModePcr01Pcr0NoTagging, /**< PCR 0/1, PCR 0 no Tagging */
    cAtAtmPolicingModePcr01Scr0Tagging,   /**< PCR 0/1, SCR 0 Tagging */
    cAtAtmPolicingModePcr01Scr0NoTagging, /**< PCR 0/1, SCR 0 no Tagging */
    cAtAtmPolicingModePcr01Scr01          /**< PCR 0/1, PCR 0/1 */
    } eAtAtmPolicingMode;

/**
 * @brief Shaping mode
 */
typedef enum eAtAtmShapingMode
    {
    cAtAtmShapingModeInvalid, /**< Invalid */
    cAtAtmShapingModePcr,     /**< PCR */
    cAtAtmShapingModePcrScr,  /**< PCR/SCR */
    cAtAtmShapingModePcrMcr   /**< PCR/MCR */
    } eAtAtmShapingMode;

/**
 * @brief Units for Traffic parameters
 */
typedef enum eAtAtmTrafficParamUnit
    {
    cAtAtmTrafficParamUnitInvalid, /**< Invalid */
    cAtAtmTrafficParamUnitKbps,    /**< Kbps */
    cAtAtmTrafficParamUnitMbps,    /**< Mbps */
    cAtAtmTrafficParamUnitCps      /**< Cps */
    } eAtAtmTrafficParamUnit;

/**
 * @brief ATM cross connect alarm types
 */
typedef enum eAtAtmCrossConnectAlarmType
    {
    cAtAtmCrossConnectAlarmTypeBufFull,          /**< Buffer full */
    cAtAtmCrossConnectAlarmTypeAis,              /**< AIS */
    cAtAtmCrossConnectAlarmTypeRdi,              /**< RDI */
    cAtAtmCrossConnectAlarmTypeLoc,              /**< Loss of Continuity */
    cAtAtmCrossConnectAlarmTypeLosc,             /**< Loss of Source Continuity */
    cAtAtmCrossConnectAlarmTypeCrcFailed,        /**< CRC failed */
    cAtAtmCrossConnectAlarmTypeLoopbackSucceded, /**< Loop-back process succeeded */
    cAtAtmCrossConnectAlarmTypeLoopbackExpired   /**< Loop-back process expired */
    } eAtAtmCrossConnectAlarmType;

/**
 * @brief All counter for ATM cross connect
 */
typedef struct tAtAtmCrossConnectCounters
    {
    /* RX counters */
    uint32 rxTotalCells;              /**< RX cell (Data cell + OAM cell + discarded cell) */
    uint32 rxDataCells;               /**< RX good cell counter */
    uint32 rxDataClp1Cells;           /**< RX good CLP 1 cell counter */
    uint32 rxDataClp0Cells;           /**< RX good CLP 0 cell counter */
    uint32 rxOamCells;                /**< RX OAM cell counter */
    uint32 rxAisCells;                /**< RX AIS OAM cell counter */
    uint32 rxRdiCells;                /**< RX RDI OAM cell counter */
    uint32 rxCcCells;                 /**< RX CC OAM cell counter */
    uint32 rxLoopbackCells;           /**< RX loopback OAM cell counter */
    uint32 rxPolDiscardCells;         /**< RX policing discarded cell counter */
    uint32 rxDropOamCells;            /**< RX dropped OAM cell counter */
    uint32 rxBufFullDiscardCells;     /**< Buffer full discarded cell counter */
    uint32 rxOamCrcErrorDiscardCells; /**< RX discarded OAM cell counter because CRC error */

    /* TX counters */
    uint32 txTotalCells;              /**< TX Data and OAM cell */
    uint32 txDataCells;               /**< TX good cell counter */
    uint32 txDataClp1Cells;           /**< TX good CLP 1 cell counter */
    uint32 txDataClp0Cells;           /**< TX good CLP 0 cell counter */
    uint32 fwdOamCells;               /**< OAM forward cell counter */
    uint32 txOamCells;                /**< TX OAM cell */
    uint32 txAisCells;                /**< TX AIS OAM cell counter */
    uint32 txRdiCells;                /**< TX RDI OAM cell counter */
    uint32 txCcCells;                 /**< TX CC OAM cell counter */
    uint32 txLbCells;                 /**< TX loopback OAM cell counter */
    }tAtAtmCrossConnectCounters;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Cross connect type */
eAtAtmRet AtAtmCrossConnectTypeSet(AtAtmCrossConnect self, eAtAtmCrossConnectType type);
eAtAtmCrossConnectType AtAtmCrossConnectTypeGet(AtAtmCrossConnect self);

/* Source */
eAtAtmRet AtAtmCrossConnectSourcePortSet(AtAtmCrossConnect self, AtChannel sourcePort);
AtChannel AtAtmCrossConnectSourcePortGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectSourceVpiSet(AtAtmCrossConnect self, uint16 sourceVpi);
uint16 AtAtmCrossConnectSourceVpiGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectSourceVciSet(AtAtmCrossConnect self, uint16 sourceVci);
uint16 AtAtmCrossConnectSourceVciGet(AtAtmCrossConnect self);

/* Destination */
eAtAtmRet AtAtmCrossConnectDestPortSet(AtAtmCrossConnect self, AtChannel destPort);
AtChannel AtAtmCrossConnectDestPortGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectDestVpiSet(AtAtmCrossConnect self, uint16 destVpi);
uint16 AtAtmCrossConnectDestVpiGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectDestVciSet(AtAtmCrossConnect self, uint16 destVci);
uint16 AtAtmCrossConnectDestVciGet(AtAtmCrossConnect self);

/* Service Categories */
eAtAtmRet AtAtmCrossConnectServiceTypeSet(AtAtmCrossConnect self, eAtAtmServiceType serviceType);
eAtAtmServiceType AtAtmCrossConnectServiceTypeGet(AtAtmCrossConnect self);

/* Policing parameters */
eAtAtmRet AtAtmCrossConnectPolicingModeSet(AtAtmCrossConnect self, eAtAtmPolicingMode policingMode);
eAtAtmPolicingMode AtAtmCrossConnectPolicingModeGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectPolPcrSet(AtAtmCrossConnect self, uint32 polPcr);
uint32 AtAtmCrossConnectPolPcrGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectPolScrSet(AtAtmCrossConnect self, uint32 polScr);
uint32 AtAtmCrossConnectPolScrGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectPolCdvtSet(AtAtmCrossConnect self, uint32 polCdvt);
uint32 AtAtmCrossConnectPolCdvtGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectPolMbsSet(AtAtmCrossConnect self, uint32 polMbs);
uint32 AtAtmCrossConnectPolMbsGet(AtAtmCrossConnect self);

/* Shaping parameters */
eAtAtmRet AtAtmCrossConnectShapingModeSet(AtAtmCrossConnect self, eAtAtmShapingMode shapingMode);
eAtAtmShapingMode AtAtmCrossConnectShapingModeGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectShapPcrSet(AtAtmCrossConnect self, uint32 shapPcr);
uint32 AtAtmCrossConnectShapPcrGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectShapScrSet(AtAtmCrossConnect self, uint32 );
uint32 AtAtmCrossConnectShapScrGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectShapCdvtSet(AtAtmCrossConnect self, uint32 shapCdvt);
uint32 AtAtmCrossConnectShapCdvtGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectShapMbsSet(AtAtmCrossConnect self, uint32 shapMbs);
uint32 AtAtmCrossConnectShapMbsGet(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectShapMcrSet(AtAtmCrossConnect self, uint32 shapMcr);
uint32 AtAtmCrossConnectShapMcrGet(AtAtmCrossConnect self);

/* Unit of Traffic parameters */
eAtAtmRet AtAtmCrossConnectTrafficParamUnitSet(AtAtmCrossConnect self, eAtAtmTrafficParamUnit trafficParamUnit);
eAtAtmTrafficParamUnit AtAtmCrossConnectTrafficParamUnitGet(AtAtmCrossConnect self);

/* Enabling of policing and shaping */
eAtAtmRet AtAtmCrossConnectPolicingEnable(AtAtmCrossConnect self, eBool enable);
eBool AtAtmCrossConnectPolicingIsEnabled(AtAtmCrossConnect self);
eAtAtmRet AtAtmCrossConnectShapingEnable(AtAtmCrossConnect self, eBool enable);
eBool AtAtmCrossConnectShapingIsEnabled(AtAtmCrossConnect self);

#ifdef __cplusplus
}
#endif
#endif /* _ATATMCROSSCONNECT_H_ */


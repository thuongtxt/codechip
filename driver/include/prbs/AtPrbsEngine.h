/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtPrbsEngine.h
 * 
 * Created Date: Sep 20, 2012
 *
 * Description : PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPRBSENGINE_H_
#define _ATPRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtModulePrbs.h"
#include "AtChannelClasses.h"
#include "AtTimer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPrbsEngine
 * @{
 */

/** @brief PRBS modes */
typedef enum eAtPrbsMode
    {
    cAtPrbsModeInvalid,                  /**< Invalid */
    cAtPrbsModeStart,                    /**< Just to start enumeration */
    cAtPrbsModePrbs9 = cAtPrbsModeStart, /**< PRBS9 */
    cAtPrbsModePrbs7,                    /**< PRBS7 */
    cAtPrbsModePrbs11,                   /**< PRBS11 */
    cAtPrbsModePrbs20StdO151,            /**< PRBS20 O.151 (x19 + x16 + 1) */
    cAtPrbsModePrbs20QrssStdO151,        /**< PRBS20 QRSS O.151 */
    cAtPrbsModePrbs20rStdO153,           /**< PRBS20 for rate up 72Kb, (x19 + x2 + 1) */
    cAtPrbsModePrbs15,                   /**< PRBS15 O.151*/
    cAtPrbsModePrbs23,                   /**< PRBS23 */
    cAtPrbsModePrbs31,                   /**< PRBS31 */
    cAtPrbsModePrbsSeq,                  /**< SEQ */
    cAtPrbsModePrbsFixedPattern1Byte,    /**< Fix pattern user defined with 1 byte size */
    cAtPrbsModePrbsFixedPattern2Bytes,   /**< Fix pattern user defined with 2 bytes size */
    cAtPrbsModePrbsFixedPattern3Bytes,   /**< Fix pattern user defined with 3 bytes size */
    cAtPrbsModePrbsFixedPattern4Bytes,   /**< Fix pattern user defined with 4 bytes size */
    cAtPrbsModeNum                       /**< Number of defined PRBS modes */
    }eAtPrbsMode;

typedef enum eAtPrbsDataMode
    {
    cAtPrbsDataModeInvalid,
    cAtPrbsDataMode64kb,/**< 64k or 8bit */
    cAtPrbsDataMode56kb /**< 56k or 7bit */
    }eAtPrbsDataMode;

/** @brief Alarm types */
typedef enum eAtPrbsEngineAlarmType
    {
    cAtPrbsEngineAlarmTypeNone     = 0,     /**< No alarm */
    cAtPrbsEngineAlarmTypeError    = cBit0, /**< At least one error happen */
    cAtPrbsEngineAlarmTypeLossSync = cBit1,  /**< Lost of sync */
    cAtPrbsEngineAlarmTypeInvalid  = cBit2,  /**< Invalid */
    cAtPrbsEngineAlarmTypeFixPatternError  = cBit3
    }eAtPrbsEngineAlarmType;

/** @brief Counters types */
typedef enum eAtPrbsEngineCounterType
    {
    cAtPrbsEngineCounterTxFrame,            /**< Transmit PRBS frames */
    cAtPrbsEngineCounterRxFrame,            /**< Received PRBS frames */
    cAtPrbsEngineCounterTxBit,              /**< Count the bit transmit from the last time PRBS start */
    cAtPrbsEngineCounterRxBit,              /**< Count the bit received from the last time PRBS start */
    cAtPrbsEngineCounterRxBitError,         /**< Count the bit error received from the last time PRBS start */
    cAtPrbsEngineCounterRxBitLastSync,      /**< Count the bit received from the last time PRBS sync */
    cAtPrbsEngineCounterRxBitErrorLastSync, /**< Count the bit error received from the last time PRBS sync */
    cAtPrbsEngineCounterRxBitLoss,          /**< Count the bit loss from the last time PRBS start */
    cAtPrbsEngineCounterRxSync,             /**< Count number of sync from the last time PRBS start */
    cAtPrbsEngineCounterRxLostFrame,        /**< Count number of lost frame */
    cAtPrbsEngineCounterRxErrorFrame,       /**< Count number of error frame */
    cAtPrbsEngineCounterNum                 /**< Number of PRBS counters */
    }eAtPrbsEngineCounterType;

/** @brief Generating/Monitoring sides */
typedef enum eAtPrbsSide
    {
    cAtPrbsSideUnknown, /**< Invalid direction */
    cAtPrbsSideTdm,     /**< Generate to TDM or monitor from TDM */
    cAtPrbsSidePsn      /**< Generate To PSN or monitor from PSN */
    }eAtPrbsSide;

/** @brief Bit order */
typedef enum eAtPrbsBitOrder
    {
    cAtPrbsBitOrderUnknown,      /**< Invalid order */
    cAtPrbsBitOrderMsb,          /**< Bit order MSB for fixed pattern */
    cAtPrbsBitOrderLsb,          /**< Bit order LSB for fixed pattern */
    cAtPrbsBitOrderNotApplicable /**< Not applicable */
    }eAtPrbsBitOrder;


/** @brief PRBS payload length modes */
typedef enum eAtPrbsPayloadLengthMode
    {
    cAtPrbsLengthModeFixed,                  /**< Fixed Length equal to min length */
    cAtPrbsLengthModeIncreased,               /**< Increase from min to max Length */
    cAtPrbsLengthModeDecreased,               /**< Decrease from max to min Length */
    cAtPrbsLengthModeRandom,                  /**< Random between min and max Length */
    }eAtPrbsPayloadLengthMode;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModulePrbsRet AtPrbsEngineInit(AtPrbsEngine self);
uint32 AtPrbsEngineIdGet(AtPrbsEngine self);

/* PRBS inversion */
eAtModulePrbsRet AtPrbsEngineTxInvert(AtPrbsEngine self, eBool invert);
eBool AtPrbsEngineTxIsInverted(AtPrbsEngine self);
eAtModulePrbsRet AtPrbsEngineRxInvert(AtPrbsEngine self, eBool invert);
eBool AtPrbsEngineRxIsInverted(AtPrbsEngine self);

/* Access channel that PRBS engine generates data on */
AtChannel AtPrbsEngineChannelGet(AtPrbsEngine self);
const char *AtPrbsEngineChannelDescription(AtPrbsEngine self);

/* Get alarm */
uint32 AtPrbsEngineAlarmGet(AtPrbsEngine self);
uint32 AtPrbsEngineAlarmHistoryGet(AtPrbsEngine self);
uint32 AtPrbsEngineAlarmHistoryClear(AtPrbsEngine self);

/* Force error. Note, the following 2 APIs are used for some products do not
 * support force error at specified rate. The default rate will be used when
 * calling these two APIs on products that support force error with specified
 * rate */
eAtModulePrbsRet AtPrbsEngineErrorForce(AtPrbsEngine self, eBool force);
eBool AtPrbsEngineErrorIsForced(AtPrbsEngine self);

/* Error insertion/detection. Note, not all of products can support this feature.
 * API AtPrbsEngineErrorForce/AtPrbsEngineErrorIsForced may be used. */
eAtModulePrbsRet AtPrbsEngineTxErrorInject(AtPrbsEngine self, uint32 numErrors);
eAtModulePrbsRet AtPrbsEngineTxErrorRateSet(AtPrbsEngine self, eAtBerRate errorRate);
eAtBerRate AtPrbsEngineTxErrorRateGet(AtPrbsEngine self);
eAtBerRate AtPrbsEngineRxErrorRateGet(AtPrbsEngine self);

/* Start engine with specified direction. Note, these interfaces may not be
 * supported for all of products. See AtPrbsEngineEnable/AtPrbsEngineIsEnabled */
eAtModulePrbsRet AtPrbsEngineTxEnable(AtPrbsEngine self, eBool enable);
eBool AtPrbsEngineTxIsEnabled(AtPrbsEngine self);
eAtModulePrbsRet AtPrbsEngineRxEnable(AtPrbsEngine self, eBool enable);
eBool AtPrbsEngineRxIsEnabled(AtPrbsEngine self);

/* Configure PRBS mode for each direction. Note, these interfaces may not be
 * supported for all of products. See AtPrbsEngineModeSet/AtPrbsEngineModeGet */
eAtModulePrbsRet AtPrbsEngineTxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode);
eAtModulePrbsRet AtPrbsEngineRxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode);
eAtPrbsMode AtPrbsEngineTxModeGet(AtPrbsEngine self);
eAtPrbsMode AtPrbsEngineRxModeGet(AtPrbsEngine self);
eBool AtPrbsEngineModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode);

/* Configure user-defined pattern in case fixed pattern mode is used */
eAtModulePrbsRet AtPrbsEngineTxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern);
eAtModulePrbsRet AtPrbsEngineRxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern);
uint32 AtPrbsEngineTxFixedPatternGet(AtPrbsEngine self);
uint32 AtPrbsEngineRxFixedPatternGet(AtPrbsEngine self);
uint32 AtPrbsEngineRxCurrentFixedPatternGet(AtPrbsEngine self);


/* Generating and monitoring side. For products that do not separate generating and
 * monitoring size, the APIs AtPrbsEngineSideSet/AtPrbsEngineSideGet are used */
eAtModulePrbsRet AtPrbsEngineGeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side);
eAtPrbsSide AtPrbsEngineGeneratingSideGet(AtPrbsEngine self);
eAtModulePrbsRet AtPrbsEngineMonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side);
eAtPrbsSide AtPrbsEngineMonitoringSideGet(AtPrbsEngine self);

/* Get counters */
eBool AtPrbsEngineCounterIsSupported(AtPrbsEngine self, uint16 counterType);
uint32 AtPrbsEngineCounterGet(AtPrbsEngine self, uint16 counterType);
uint32 AtPrbsEngineCounterClear(AtPrbsEngine self, uint16 counterType);
uint64 AtPrbsEngineCounter64BitGet(AtPrbsEngine self, uint16 counterType);
uint64 AtPrbsEngineCounter64BitClear(AtPrbsEngine self, uint16 counterType);

/* It will be good to always latch counters before reading counters. The clear
 * indication is to clear hardware counters */
eAtModulePrbsRet AtPrbsEngineAllCountersLatchAndClear(AtPrbsEngine self, eBool clear);

/* PRBS bit order */
eAtModulePrbsRet AtPrbsEngineTxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order);
eAtPrbsBitOrder AtPrbsEngineTxBitOrderGet(AtPrbsEngine self);
eAtModulePrbsRet AtPrbsEngineRxBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order);
eAtPrbsBitOrder AtPrbsEngineRxBitOrderGet(AtPrbsEngine self);

/* PRBS gating */
AtTimer AtPrbsEngineGatingTimer(AtPrbsEngine self);

/* Debugging */
void AtPrbsEngineDebug(AtPrbsEngine self);
eBool AtPrbsEngineIsRawPrbs(AtPrbsEngine self);

/*------------------------------------------------------------------------------
 * The following APIs are used for products that do not support configuring TX
 * and RX separately
 *----------------------------------------------------------------------------*/

/* Enable/disable engine at both directions */
eAtModulePrbsRet AtPrbsEngineEnable(AtPrbsEngine self, eBool enable);
eBool AtPrbsEngineIsEnabled(AtPrbsEngine self);

/* PRBS mode for both directions */
eAtModulePrbsRet AtPrbsEngineModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode);
eAtPrbsMode AtPrbsEngineModeGet(AtPrbsEngine self);

/* PRBS inversion for both directions */
eAtModulePrbsRet AtPrbsEngineInvert(AtPrbsEngine self, eBool invert);
eBool AtPrbsEngineIsInverted(AtPrbsEngine self);

/* Side to generate and monitor */
eAtModulePrbsRet AtPrbsEngineSideSet(AtPrbsEngine self, eAtPrbsSide side);
eAtPrbsSide AtPrbsEngineSideGet(AtPrbsEngine self);

/* PRBS bit order for both directions */
eAtModulePrbsRet AtPrbsEngineBitOrderSet(AtPrbsEngine self, eAtPrbsBitOrder order);
eAtPrbsBitOrder AtPrbsEngineBitOrderGet(AtPrbsEngine self);

eAtModulePrbsRet AtPrbsEngineFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern);
uint32 AtPrbsEngineFixedPatternGet(AtPrbsEngine self);

/* Utils */
eBool AtPrbsModeIsFixedPattern(eAtPrbsMode mode);
eBool AtPrbsEngineIsValidSide(eAtPrbsSide side);

/* Delay */
eAtRet AtPrbsEngineDelayEnable(AtPrbsEngine self);
eAtRet AtPrbsEngineDelayDisable(AtPrbsEngine self);
float AtPrbsEngineMaxDelayGet(AtPrbsEngine self, eBool r2c);
float AtPrbsEngineMinDelayGet(AtPrbsEngine self, eBool r2c);
float AtPrbsEngineAverageDelayGet(AtPrbsEngine self, eBool r2c);

/* Disruption */
eAtRet AtPrbsEngineDisruptionMaxExpectedTimeSet(AtPrbsEngine self, uint32 time);
uint32 AtPrbsEngineDisruptionMaxExpectedTimeGet(AtPrbsEngine self);
uint32 AtPrbsEngineDisruptionCurTimeGet(AtPrbsEngine self, eBool r2c);
eAtRet AtPrbsEngineDisruptionLossSigThresSet(AtPrbsEngine self, uint32 thres);
eAtRet AtPrbsEngineDisruptionPrbsSyncDectectThresSet(AtPrbsEngine self, uint32 thres);
eAtRet AtPrbsEngineDisruptionPrbsLossDectectThresSet(AtPrbsEngine self, uint32 thres);

/* Special prbs function for eth prbs */
eBool AtPrbsEngineBandwidthControlIsSupported(AtPrbsEngine self);
eAtModulePrbsRet AtPrbsEngineBurstSizeSet(AtPrbsEngine self, uint32 burstSizeInBytes);
uint32 AtPrbsEngineBurstSizeGet(AtPrbsEngine self);
eAtModulePrbsRet AtPrbsEnginePercentageBandwidthSet(AtPrbsEngine self, uint32 percentageOfBandwith);
uint32 AtPrbsEnginePercentageBandwidthGet(AtPrbsEngine self);
eAtModulePrbsRet AtPrbsEnginePayloadLengthModeSet(AtPrbsEngine self, eAtPrbsPayloadLengthMode payloadLenMode);
eAtPrbsPayloadLengthMode AtPrbsEnginePayloadLengthModeGet(AtPrbsEngine self);
eAtModulePrbsRet AtPrbsEngineMinLengthSet(AtPrbsEngine self, uint32 minLength);
eAtModulePrbsRet AtPrbsEngineMaxLengthSet(AtPrbsEngine self, uint32 maxLength);
uint32 AtPrbsEngineMinLengthGet(AtPrbsEngine self);
uint32 AtPrbsEngineMaxLengthGet(AtPrbsEngine self);

eAtRet AtPrbsEngineDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode);
eAtPrbsDataMode AtPrbsEngineDataModeGet(AtPrbsEngine self);
eAtRet AtPrbsEngineTxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode);
eAtPrbsDataMode AtPrbsEngineTxDataModeGet(AtPrbsEngine self);
eAtRet AtPrbsEngineRxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode);
eAtPrbsDataMode AtPrbsEngineRxDataModeGet(AtPrbsEngine self);
eBool AtPrbsEngineDataModeIsSupported(AtPrbsEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPRBSENGINE_H_ */

#include "AtPrbsEngineDeprecated.h"

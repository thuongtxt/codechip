/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtModulePrbs.h
 * 
 * Created Date: Sep 20, 2012
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPRBS_H_
#define _ATMODULEPRBS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h" /* Super class */
#include "AtModulePdh.h"
#include "AtModuleSdh.h"
#include "AtModuleEncap.h"
#include "AtModulePw.h"
#include "AtModuleEth.h"
#include "../att/AtAttPrbsManager.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePrbs
 * @{
 */

/** @brief PRBS module */
typedef struct tAtModulePrbs * AtModulePrbs;

/** @brief PRBS engine class */
typedef struct tAtPrbsEngine * AtPrbsEngine;

/** @brief PRBS module return codes */
typedef uint32 eAtModulePrbsRet;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Engines management */
uint32 AtModulePrbsMaxNumEngines(AtModulePrbs self);
AtPrbsEngine AtModulePrbsEngineGet(AtModulePrbs self, uint32 engineId);
eAtRet AtModulePrbsEngineDelete(AtModulePrbs self, uint32 engineId);
eAtRet AtModulePrbsTimeMeasurementErrorSet(AtModulePrbs self, uint32 period);
uint32 AtModulePrbsTimeMeasurementErrorGet(AtModulePrbs self);
AtAttPrbsManager AtModulePrbsAttManager(AtModulePrbs self);

/* Engine factory APIs */
AtPrbsEngine AtModulePrbsDe3PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe3 de3);
AtPrbsEngine AtModulePrbsDe1PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe1 de1);
AtPrbsEngine AtModulePrbsNxDs0PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0);
AtPrbsEngine AtModulePrbsSdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc);
AtPrbsEngine AtModulePrbsPwPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPw pw);
AtPrbsEngine AtModulePrbsEncapChannelPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtEncapChannel encap);
AtPrbsEngine AtModulePrbsEthPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPRBS_H_ */


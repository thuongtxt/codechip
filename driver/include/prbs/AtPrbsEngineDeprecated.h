/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtPrbsEngineDeprecated.h
 * 
 * Created Date: Dec 6, 2017
 *
 * Description : PRBS engine deprecated APIs
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPRBSENGINEDEPRECATED_H_
#define _ATPRBSENGINEDEPRECATED_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModulePrbsRet AtPrbsEngineAllCountersLatch(AtPrbsEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPRBSENGINEDEPRECATED_H_ */


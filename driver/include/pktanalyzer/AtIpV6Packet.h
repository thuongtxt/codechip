/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtIpV6Packet.h
 * 
 * Created Date: Apr 27, 2013
 *
 * Description : IPv6 packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIPV6PACKET_H_
#define _ATIPV6PACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtIpV4Packet.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/** @brief IPv6 packet */
typedef struct tAtIpV6Packet * AtIpV6Packet;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtIpV6PacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _ATIPV6PACKET_H_ */


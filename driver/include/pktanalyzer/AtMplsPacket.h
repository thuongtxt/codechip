/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PACKET ANALYZER
 * 
 * File        : AtMplsPacket.h
 * 
 * Created Date: Jun 6, 2013
 *
 * Description : MPLS Packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMPLSPACKET_H_
#define _ATMPLSPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h"
#include "AtPwPacket.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtMplsPacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode);
AtPacket AtMplsPwPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp);

uint32 AtMplsPacketInnerLabel(AtPacket self);

void AtMplsPwPacketTypeSet(AtPacket self, eAtPwPacketType type);
void AtMplsPwPacketRtpModeSet(AtPacket self, eAtPwPacketRtpMode rtp);

#ifdef __cplusplus
}
#endif
#endif /* _ATMPLSPACKET_H_ */


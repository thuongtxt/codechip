/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtModulePktAnalyzer.h
 * 
 * Created Date: Apr 26, 2013
 *
 * Description : Packet analyzer module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMODULEPKTANALYZER_H_
#define _ATMODULEPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModule.h" /* Super */
#include "AtPktAnalyzer.h"
#include "AtChannelClasses.h"
#include "AtEthPort.h"
#include "AtEthFlow.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/** @brief Packet analyzer module */
typedef struct tAtModulePktAnalyzer * AtModulePktAnalyzer;

/** @brief Packet factory */
typedef struct tAtPacketFactory * AtPacketFactory;

/** @brief Packet abstract class */
typedef struct tAtPacket * AtPacket;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Get current packet analyzer */
AtPktAnalyzer AtModulePacketAnalyzerCurrentAnalyzerGet(AtModulePktAnalyzer self);

/* Packet analyzer factory */
AtPktAnalyzer AtModulePacketAnalyzerEthPortPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthPort port);
AtPktAnalyzer AtModulePacketAnalyzerEthFlowPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthFlow flow);
AtPktAnalyzer AtModulePacketAnalyzerPppLinkPktAnalyzerCreate(AtModulePktAnalyzer self, AtPppLink pppLink);
AtPktAnalyzer AtModulePacketAnalyzerHdlcChannelPktAnalyzerCreate(AtModulePktAnalyzer self, AtHdlcChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _ATMODULEPKTANALYZER_H_ */


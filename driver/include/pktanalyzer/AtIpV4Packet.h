/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtIpV4Packet.h
 * 
 * Created Date: Apr 27, 2013
 *
 * Description : IPv4 packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATIPV4PACKET_H_
#define _ATIPV4PACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/** @brief IPv4 packet */
typedef struct tAtIpV4Packet * AtIpV4Packet;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtIpV4PacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _ATIPV4PACKET_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtPacket.h
 * 
 * Created Date: Apr 26, 2013
 *
 * Description : Packet abstract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPACKET_H_
#define _ATPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePktAnalyzer.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/** @brief Data caching mode */
typedef enum eAtPacketCacheMode
    {
    cAtPacketCacheModeNoCache,   /**< Input buffer reference is stored inside object. No memory is allocated */
    cAtPacketCacheModeCacheData  /**< Memory is allocated and input message buffer will be copied to this memory */
    }eAtPacketCacheMode;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete packet types */
AtPacket AtPacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode);

/* Access data buffer */
uint8 *AtPacketDataBuffer(AtPacket self, uint32 *numBytesInBuffer);
uint32 AtPacketLengthGet(AtPacket self);
eAtPacketCacheMode AtPacketDataCacheModeGet(AtPacket self);
uint8 *AtPacketPayloadBuffer(AtPacket self, uint32 *payloadSize);

/* Packet factory to create packets in payload */
void AtPacketFactorySet(AtPacket self, AtPacketFactory factory);
AtPacketFactory AtPacketFactoryGet(AtPacket self);

/* Analyze packet */
void AtPacketDisplay(AtPacket self);
void AtPacketDisplayRaw(AtPacket self, uint8 level);

/* Packet complete */
void AtPacketIsCompletedMark(AtPacket self, eBool isCompleted);
eBool AtPacketIsCompleted(AtPacket self);
void AtPacketMarkError(AtPacket self, eBool isErrored);
eBool AtPacketIsErrored(AtPacket self);

#ifdef __cplusplus
}
#endif
#endif /* _ATPACKET_H_ */

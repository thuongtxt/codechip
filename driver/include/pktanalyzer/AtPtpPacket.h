/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtPtpPacket.h
 * 
 * Created Date: Jul 13, 2018
 *
 * Description : PTP packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPTPPACKET_H_
#define _ATPTPPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/** @brief PTP packet */
typedef struct tAtPtpPacket * AtPtpPacket;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtPtpPacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _ATPTPPACKET_H_ */


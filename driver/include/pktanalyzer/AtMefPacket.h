/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PACKET ANALYZER
 * 
 * File        : AtMefPacket.h
 * 
 * Created Date: Sep 16, 2013
 *
 * Description : MEF packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATMEFPACKET_H_
#define _ATMEFPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h"
#include "AtPwPacket.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/** @brief MEF packet */
typedef struct tAtMefPacket * AtMefPacket;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtMefPacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode);
AtPacket AtMefPwPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp);
AtPacket AtMefMplsPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp);

uint32 AtMefPacketEcid(AtPacket self);

/* For PW packets */
void AtMefPwPacketRtpModeSet(AtPacket self, eAtPwPacketRtpMode rtp);
void AtMefPwPacketTypeSet(AtPacket self, eAtPwPacketType type);

#ifdef __cplusplus
}
#endif
#endif /* _ATMEFPACKET_H_ */


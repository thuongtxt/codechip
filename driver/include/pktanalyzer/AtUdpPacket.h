/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtUdpPacket.h
 * 
 * Created Date: May 3, 2013
 *
 * Description : UDP packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATUDPPACKET_H_
#define _ATUDPPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPwPacket.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/** @brief UDP packet */
typedef struct tAtUdpPacket * AtUdpPacket;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtUdpPacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode);
AtPacket AtUdpPwPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp);
void AtUdpPwPacketTypeSet(AtPacket self, eAtPwPacketType type);
void AtUdpPwPacketRtpModeSet(AtPacket self, eAtPwPacketRtpMode rtp);
uint32 AtUdpPacketUdpLookupPort(AtPacket self);

#ifdef __cplusplus
}
#endif
#endif /* _ATUDPPACKET_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtPwPacket.h
 * 
 * Created Date: Mar 29, 2014
 *
 * Description : PW packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPWPACKET_H_
#define _ATPWPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtPwPacket * AtPwPacket;

typedef enum eAtPwPacketType
    {
    cAtPwPacketTypeUnknown,
    cAtPwPacketTypeCEPBasic,
    cAtPwPacketTypeCEPVc3FactionalNoEbm,
    cAtPwPacketTypeCEPVc3FactionalEbm,
    cAtPwPacketTypeCEPVc4FactionalNoEbm,
    cAtPwPacketTypeCEPVc4FactionalEbm,
    cAtPwPacketTypeToh,
    cAtPwPacketTypeSAToP,
    cAtPwPacketTypeCESoP
    }eAtPwPacketType;

typedef enum eAtPwPacketRtpMode
    {
    cAtPwPacketRtpModeNoRtp,
    cAtPwPacketRtpModeRtp
    }eAtPwPacketRtpMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtPwPacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp);

#ifdef __cplusplus
}
#endif
#endif /* _ATPWPACKET_H_ */


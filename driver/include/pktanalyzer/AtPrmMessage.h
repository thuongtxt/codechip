/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtPrmMessage.h
 * 
 * Created Date: Nov 8, 2017
 *
 * Description : PRM message
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPRMMESSAGE_H_
#define _ATPRMMESSAGE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAtandTPrmSize 12
#define cAnsiPrmSize   13

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */
 
/**
 * @brief PRM message type
 */
typedef enum eAtPrmMessageType
    {
    cAtPrmMessageTypeNPRM, /**< NPRM */
    cAtPrmMessageTypePRM   /**< PRM */
    }eAtPrmMessageType;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtPrmMessageNew(eAtPrmMessageType messageType, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _ATPRMMESSAGE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : AtXauiPacket.h
 * 
 * Created Date: May 10, 2016
 *
 * Description : XAUI Packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATXAUIPACKET_H_
#define _ATXAUIPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h" /* Super */
#include "AtPacketFactory.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/** @brief Ethernet packet */
typedef struct tAtEXauiPacket *AtEXauiPacket;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtEXauiPacketNew(uint8 *dataBuffer,
                          uint32 length,
                          eAtPacketCacheMode cacheMode,
                          eAtPacketFactoryDirection direction,
                          uint32 portId);
#ifdef __cplusplus
}
#endif
#endif /* _ATXAUIPACKET_H_ */


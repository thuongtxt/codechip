/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtHdlcPacket.h
 * 
 * Created Date: Apr 27, 2013
 *
 * Description : HDLC packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATHDLCPACKET_H_
#define _ATHDLCPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h" /* Super class */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/** @brief HDLC packet */
typedef struct tAtHdlcPacket * AtHdlcPacket;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtHdlcPacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _ATHDLCPACKET_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtEthPacket.h
 * 
 * Created Date: Apr 27, 2013
 *
 * Description : Ethernet packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATETHPACKET_H_
#define _ATETHPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define sAtEthPacketFieldDMAC "DMAC"

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/** @brief Ethernet packet */
typedef struct tAtEthPacket * AtEthPacket;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtEthPacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _ATETHPACKET_H_ */


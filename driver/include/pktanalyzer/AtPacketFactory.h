/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtPacketFactory.h
 * 
 * Created Date: Mar 29, 2014
 *
 * Description : Packet factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPACKETFACTORY_H_
#define _ATPACKETFACTORY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtModulePktAnalyzer.h"
#include "AtPwPacket.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtPacketFactory
 * @{
 */
/** @brief Direction to analyze */
typedef enum eAtPacketFactoryDirection
    {
    cAtPacketFactoryDirectionNone, /**< Not care */
    cAtPacketFactoryDirectionRx,   /**< RX */
    cAtPacketFactoryDirectionTx    /**< TX */
    }eAtPacketFactoryDirection;
/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacketFactory AtPacketFactoryNew(void);
AtPacketFactory AtPacketFactoryDefaultFactory(void);

AtPacket AtPacketFactoryRawPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
AtPacket AtPacketFactoryEthPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
AtPacket AtPacketFactoryHdlcPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
AtPacket AtPacketFactoryIpV4PacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
AtPacket AtPacketFactoryIpV6PacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
AtPacket AtPacketFactoryLcpPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
AtPacket AtPacketFactoryMefPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
AtPacket AtPacketFactoryMefMplsPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
AtPacket AtPacketFactoryMplsPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
AtPacket AtPacketFactoryPppPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
AtPacket AtPacketFactoryUdpPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);
AtPacket AtPacketFactoryPwPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPwPacketType type, eAtPwPacketRtpMode rtp, eAtPacketFactoryDirection direction);
AtPacket AtPacketFactoryXauiPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction, uint32 portId);
AtPacket AtPacketFactoryPtpPacketCreate(AtPacketFactory self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eAtPacketFactoryDirection direction);

#ifdef __cplusplus
}
#endif
#endif /* _ATPACKETFACTORY_H_ */


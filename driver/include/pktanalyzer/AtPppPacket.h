/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : AtPppPacket.h
 * 
 * Created Date: May 10, 2013
 *
 * Description : PPP packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPPPPACKET_H_
#define _ATPPPPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h" /* Super */

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/** @brief PPP packet */
typedef struct tAtPppPacket * AtPppPacket;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket AtPppPacketNew(uint8 *data, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _ATPPPPACKET_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Debug
 * 
 * File        : AtPktAnalyzer.h
 * 
 * Created Date: Dec 18, 2012
 *
 * Description : Packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATPKTANALYZER_H_
#define _ATPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h" /* Super class */
#include "AtCommon.h"
#include "AtList.h"
#include "AtChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtModulePktAnalyzer
 * @{
 */

/** @brief Pattern compare mode */
typedef enum eAtPktAnalyzerPatternCompareMode
    {
    cAtPktAnalyzerPatternCompareModeAny,
    cAtPktAnalyzerPatternCompareModeSamePkt,
    cAtPktAnalyzerPatternCompareModeDifferent
    }eAtPktAnalyzerPatternCompareMode;

/** @brief PPP packet types */
typedef enum eAtPktAnalyzerPppPktType
    {
    cAtPktAnalyzerPppPktTypeMlpppProtocol,
    cAtPktAnalyzerPppPktTypePppProtocol,
    cAtPktAnalyzerPppPktTypeHdlc,
    cAtPktAnalyzerPppPktTypeOamPacket,
    cAtPktAnalyzerPppPktTypeMlpppPacket,
    cAtPktAnalyzerPppPktTypePppPacket
    }eAtPktAnalyzerPppPktType;

/** @brief Packet dump types */
typedef enum eAtPktAnalyzerPktDumpType
    {
    cAtPktAnalyzerDumpAllPkt     = 0,
    cAtPktAnalyzerDumpErrorPkt   = 1,
    cAtPktAnalyzerDumpGoodPkt    = 2
    }eAtPktAnalyzerPktDumpType;

/** @brief Directions */
typedef enum eAtPktAnalyzerDirection
    {
    cAtPktAnalyzerDirectionNone,
    cAtPktAnalyzerDirectionRx,
    cAtPktAnalyzerDirectionTx
    }eAtPktAnalyzerDirection;

/** @brief SOH packet types */
typedef enum eAtPktAnalyzerSohPktType
    {
    cAtPktAnalyzerSohPktTypeDcc,
    cAtPktAnalyzerSohPktTypeKbyteAps
    }eAtPktAnalyzerSohPktType;

/** @brief AT Packet Analyzer */
typedef struct tAtPktAnalyzer * AtPktAnalyzer;

/**
 * @brief Packet display mode
 */
typedef enum eAtPktAnalyzerDisplayMode
    {
    cAtPktAnalyzerDisplayModeHumanReadable, /**< Default mode */
    cAtPktAnalyzerDisplayModeRaw,           /**< Raw format */
    cAtPktAnalyzerDisplayModeXml            /**< XML */
    }eAtPktAnalyzerDisplayMode;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Basic interfaces */
AtList AtPktAnalyzerCaptureTxPackets(AtPktAnalyzer self);
AtList AtPktAnalyzerCaptureRxPackets(AtPktAnalyzer self);
void AtPktAnalyzerAnalyzeRxPackets(AtPktAnalyzer self);
void AtPktAnalyzerAnalyzeTxPackets(AtPktAnalyzer self);
AtChannel AtPktAnalyzerChannelGet(AtPktAnalyzer self);
eAtRet AtPktAnalyzerChannelSet(AtPktAnalyzer self, AtChannel channel);

/* Start/stop analyzing */
eAtRet AtPktAnalyzerStart(AtPktAnalyzer self);
eAtRet AtPktAnalyzerStop(AtPktAnalyzer self);
eBool AtPktAnalyzerIsStarted(AtPktAnalyzer self);

/* Advance configurations */
void AtPktAnalyzerInit(AtPktAnalyzer self);
void AtPktAnalyzerRecapture(AtPktAnalyzer self);
void AtPktAnalyzerPatternSet(AtPktAnalyzer self, uint8 *pattern);
void AtPktAnalyzerPatternMaskSet(AtPktAnalyzer self, uint32 patternMask);
void AtPktAnalyzerNumPacketsSet(AtPktAnalyzer self, uint32 numPackets);
void AtPktAnalyzerPacketLengthSet(AtPktAnalyzer self, uint32 packetLength);
void AtPktAnalyzerPatternCompareModeSet(AtPktAnalyzer self, eAtPktAnalyzerPatternCompareMode compareMode);
void AtPktAnalyzerPacketDumpTypeSet(AtPktAnalyzer self, eAtPktAnalyzerPktDumpType dumpType);

/* For PPP */
void AtPktAnalyzerPppPacketTypeSet(AtPktAnalyzer self, eAtPktAnalyzerPppPktType packetType);
eAtRet AtPktAnalyzerPacketTypeSet(AtPktAnalyzer self, uint8 pktType);

/* Utils */
void AtPktAnalyzerAllPacketsDisplay(AtList packets, eAtPktAnalyzerDisplayMode rawDisplay);

#ifdef __cplusplus
}
#endif
#endif /* _ATPKTANALYZER_H_ */

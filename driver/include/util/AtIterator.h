/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtIterator.h
 * 
 * Created Date: Oct 6, 2012
 *
 * Author      : nguyennt
 * 
 * Description : Iterator interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATITERATOR_H_
#define _ATITERATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtIteratorCount(AtIterator self);
AtObject AtIteratorNext(AtIterator self);
void AtIteratorRestart(AtIterator self);

/* Delete current element */
AtObject AtIteratorRemove(AtIterator self);

#ifdef __cplusplus
}
#endif
#endif /* _ATITERATOR_H_ */


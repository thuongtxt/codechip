/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2007 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtList.h
 * 
 * Created Date: Oct 5, 2012
 *
 * Author      : nguyennt
 * 
 * Description : List data structure
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLIST_H_
#define _ATLIST_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtObject.h" /* Super class */
#include "AtClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Create list */
AtList AtListCreate(uint32 capacity);
uint32 AtListLengthGet(AtList self);
uint32 AtListCapacityGet(AtList self);

void AtListDeleteWithObjectHandler(AtList self, void (*ObjectHandler)(AtObject object));

/* Object management */
AtObject AtListObjectGet(AtList self, uint32 objectIndex);
eAtRet AtListObjectAdd(AtList self, AtObject object);
uint32 AtListObjectsAddFromList(AtList self, AtList list);
eAtRet AtListObjectRemove(AtList self, AtObject object);
AtObject AtListObjectRemoveAtIndex(AtList self, uint32 objectIndex);
eBool AtListContainsObject(AtList self, AtObject object);

/* Create Iterator */
AtIterator AtListIteratorCreate(AtList self);
AtIterator AtListIteratorGet(AtList self); /* Built-in iterator */

/* Flush */
void AtListFlush(AtList self);
#ifdef __cplusplus
}
#endif
#endif /* _ATLIST_H_ */

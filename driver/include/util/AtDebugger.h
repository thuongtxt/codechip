/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Management
 * 
 * File        : AtModuleDebugger.h
 * 
 * Created Date: May 18, 2017
 *
 * Description : Module debugger
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDEBUGGER_H_
#define _ATDEBUGGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "atclib.h"
#include "AtObject.h" /* Super class */
#include "AtClasses.h"
#include "AtRet.h"
#include "AtChannelClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtDebugEntry * AtDebugEntry;
typedef struct tAtDebugger   * AtDebugger;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDebugger AtDebuggerNew(void);

/* Debuggers */
AtDebugger AtModuleDebuggerCreate(AtModule module);
AtDebugger AtChannelDebuggerCreate(AtChannel channel);

/* Debugger APIs */
AtIterator AtDebuggerEntryIteratorCreate(AtDebugger self);
eAtRet AtDebuggerEntryAdd(AtDebugger self, AtDebugEntry entry);
char *AtDebuggerCharBuffer(AtDebugger self, uint32 *bufferSize);

/* Profile */
uint32 AtDebuggerNumEntries(AtDebugger self);
uint32 AtDebuggerNumCriticalEntries(AtDebugger self);
uint32 AtDebuggerNumInfoEntries(AtDebugger self);
uint32 AtDebuggerNumWarningEntries(AtDebugger self);
uint32 AtDebuggerNumOtherSeverityEntries(AtDebugger self);

/* Entry APIs */
eAtSevLevel AtDebuggerEntrySeverityGet(AtDebugEntry self);
void AtDebuggerEntrySeveritySet(AtDebugEntry self, eAtSevLevel severity);
const char *AtDebuggerEntryNameGet(AtDebugEntry self);
void AtDebuggerEntryNameSet(AtDebugEntry self, const char *name);
const char *AtDebuggerEntryValueGet(AtDebugEntry self);
void AtDebuggerEntryValueSet(AtDebugEntry self, const char *value);

/* Concrete entries */
AtDebugEntry AtDebugEntryNew(eAtSevLevel severity, const char *name, const char *value);
AtDebugEntry AtDebugEntryErrorBitNew(const char *name, uint32 hwValue, uint32 mask);
AtDebugEntry AtDebugEntryWarningBitNew(const char* name, uint32 value, uint32 bitMask);
AtDebugEntry AtDebugEntryInfoBitNew(const char* name, uint32 hwValue, uint32 mask);
AtDebugEntry AtDebugEntryInfoResourceNew(const char* name, uint32 max, uint32 free);
AtDebugEntry AtDebugEntryStringFormatNew(eAtSevLevel severity, char *buffer, uint32 bufferSize, const char *name, const char *valueFormat, ...);

#ifdef __cplusplus
}
#endif
#endif /* _ATDEBUGGER_H_ */


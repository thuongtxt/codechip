/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : UTIL
 * 
 * File        : AtNumber.h
 * 
 * Created Date: Jun 16, 2015
 *
 * Description : Number utilities
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATNUMBER_H_
#define _ATNUMBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
const char* AtNumberDigitGroupingString(uint32 number);
const char* AtNumberUInt64DigitGroupingString(uint64 number);

#ifdef __cplusplus
}
#endif
#endif /* _ATNUMBER_H_ */


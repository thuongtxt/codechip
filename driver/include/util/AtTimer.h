/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : AtTimer.h
 * 
 * Created Date: Apr 6, 2017
 *
 * Description : Abstract timer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATTIMER_H_
#define _ATTIMER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtTimer
 * @{
 */

/** @brief Timer abstract class */
typedef struct tAtTimer * AtTimer;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtTimerDurationSet(AtTimer self, uint32 durationInMs);
uint32 AtTimerDurationGet(AtTimer self);
uint32 AtTimerMaxDuration(AtTimer self);
uint32 AtTimerElapsedTimeGet(AtTimer self);
eAtRet AtTimerStart(AtTimer self);
eAtRet AtTimerStop(AtTimer self);
eBool AtTimerIsStarted(AtTimer self);
eBool  AtTimerIsRunning(AtTimer self);

#ifdef __cplusplus
}
#endif
#endif /* _ATTIMER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtDictionary.h
 * 
 * Created Date: May 2, 2019
 *
 * Description : Dictionary data structure
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATDICTIONARY_H_
#define _ATDICTIONARY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtDictionary
 * @{
 */

/**
 * @brief Dictionary class
 */
typedef struct tAtDictionary * AtDictionary;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 AtDictionaryCount(AtDictionary self);

void AtDictionaryDeleteWithObjectHandler(AtDictionary self, void (*ObjectDelete)(AtObject));

/* Object management */
eAtRet AtDictionaryObjectAdd(AtDictionary self, const void *key, AtObject object);
eAtRet AtDictionaryObjectRemove(AtDictionary self, const void *key);
AtObject AtDictionaryObjectGet(AtDictionary self, const void *key);
eBool AtDictionaryHasKey(AtDictionary self, const void *key);

/* Iterators */
AtIterator AtDictionaryKeyIteratorCreate(AtDictionary self);

/* Constructors */
AtDictionary AtDefaultDictionaryNew(uint32 order);

#ifdef __cplusplus
}
#endif
#endif /* _ATDICTIONARY_H_ */


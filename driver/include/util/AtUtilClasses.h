/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtUtilClasses.h
 * 
 * Created Date: May 16, 2017
 *
 * Description : Util classes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATUTILCLASSES_H_
#define _ATUTILCLASSES_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtCoder        * AtCoder;
typedef struct tAtDefaultCoder * AtDefaultCoder;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATUTILCLASSES_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtLogger.h
 * 
 * Created Date: Mar 19, 2013
 *
 * Description : Logger
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATLOGGER_H_
#define _ATLOGGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include <stdarg.h>
#include "atclib.h"
#include "AtCommon.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtLogger
 * @{
 */

/** @brief AT Logger */
typedef struct tAtLogger * AtLogger;

/** @brief Log level */
typedef enum eAtLogLevel
    {
    cAtLogLevelNone     = 0,
	cAtLogLevelNormal   = cBit0,
    cAtLogLevelInfo     = cBit1,
    cAtLogLevelWarning  = cBit2,
    cAtLogLevelCritical = cBit3,
    cAtLogLevelDebug    = cBit4,
    cAtLogLevelAll      = cBit4_0
    }eAtLogLevel;

/** @brief Listener */
typedef struct tAtLoggerListener
    {
    void (*DidLogMessage)(AtLogger self, eAtLogLevel level, const char *message, void *userData); /**<
        Called after logging a message */
    void (*WillRemoveListener)(AtLogger self, void *userData); /**< Called before removing a listener */
    }tAtLoggerListener;

/**
 * @brief Logger counters
 */
typedef struct tAtLoggerCounters
    {
    uint32 levelNone;     /**< Number of messages have level None */
    uint32 levelNormal;   /**< Number of messages have level Normal */
    uint32 levelInfo;     /**< Number of messages have level Info */
    uint32 levelWarning;  /**< Number of messages have level Warning */
    uint32 levelCritical; /**< Number of messages have level Critical */
    uint32 levelDebug;    /**< Number of messages have level Debug */
    uint32 levelOthers;   /**< Number of messages have undetermined level */
    }tAtLoggerCounters;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Logger methods */
void AtLoggerInit(AtLogger self);
void AtLoggerLog(AtLogger self, eAtLogLevel level, const char *format, va_list args);
void AtLoggerShow(AtLogger self);
void AtLoggerFlush(AtLogger self);
void AtLoggerVerbose(AtLogger self, eBool verbose);
uint32 AtLoggerMaxMessageLength(AtLogger self);
uint32 AtLoggerNumMessages(AtLogger self);
void AtLoggerNoLockLog(AtLogger self, eAtLogLevel level, const char* message);
void AtLoggerShowByLevel(AtLogger self, eAtLogLevel level);
void AtLoggerFileExport(AtLogger self, AtFile file);
void AtLoggerCountersGet(AtLogger self, tAtLoggerCounters *counters);
void AtLoggerCountersClear(AtLogger self, tAtLoggerCounters *counters);

/* Callback */
eAtRet AtLoggerListenerAdd(AtLogger self, const tAtLoggerListener *callback, void *userData);
eAtRet AtLoggerListenerRemove(AtLogger self, const tAtLoggerListener *callback);

/* Enabling */
void AtLoggerEnable(AtLogger self, eBool enable);
eBool AtLoggerIsEnabled(AtLogger self);
void AtLoggerLevelEnable(AtLogger self, uint32 levelMask, eBool enable);
eBool AtLoggerLevelIsEnabled(AtLogger self, eAtLogLevel logLevel);
void AtLoggerShowMessageContainSubString(AtLogger self, const char* pattern);

/* Concrete loggers */
AtLogger AtDefaultLoggerNew(uint32 maxNumMessages, uint32 maxMessageLength);
AtLogger AtFileLoggerNew(const char* fileName, uint32 maxMessageLen);

/* Util */
const char* AtLoggerLevel2String(eAtLogLevel level);
eAtSevLevel AtLoggerLevelColor(eAtLogLevel level);

/* For log message */
char* AtLoggerSharedBufferGet(AtLogger self, uint32* bufferLength);

eAtRet AtLoggerLock(AtLogger self);
eAtRet AtLoggerUnLock(AtLogger self);

#ifdef __cplusplus
}
#endif
#endif /* _ATLOGGER_H_ */


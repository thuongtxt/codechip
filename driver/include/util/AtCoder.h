/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Util
 * 
 * File        : AtCoder.h
 * 
 * Created Date: May 26, 2015
 *
 * Description : Abstract class used to encode/decode data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCODER_H_
#define _ATCODER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtClasses.h"
#include "AtFile.h"
#include "AtUtilClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/* Handers */
typedef void (*AtCoderSerializeHandler)(AtObject self, AtCoder encoder);
typedef AtObject (*AtCoderIndexHandler)(AtCoder self, void *objects, uint32 objectIndex);
typedef const char *(AtCoderObjectToStringHandler)(AtObject self);

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete coders */
AtCoder AtDefaultCoderNew(void);

void AtCoderDelete(AtCoder self);

void AtCoderEncodeObjectWithHandler(AtCoder self, AtObject object, const char *key, AtCoderSerializeHandler handler);
void AtCoderEncodeObjectsWithHandler(AtCoder self, AtObject *objects, uint32 numObjects, const char *key, AtCoderSerializeHandler handler);
void AtCoderEncodeListWithHandler(AtCoder self, AtList list, const char *key, AtCoderSerializeHandler handler);
void AtCoderEncodeObjectsWithHandlers(AtCoder self, void *objects, uint32 numObjects,
                                      AtCoderIndexHandler indexHandler,
                                      const char *key,
                                      AtCoderSerializeHandler serializeHandler);
void AtCoderEncodeObjectsDescriptionWithHandlers(AtCoder self, void *objects, uint32 numObjects,
                                                 AtCoderIndexHandler indexHandler,
                                                 const char *key,
                                                 AtCoderObjectToStringHandler object2String);

void AtCoderEncodeUInt(AtCoder self, uint32 value, const char *key);
void AtCoderEncodeUInt64(AtCoder self, uint64 value, const char *key);
void AtCoderEncodeObject(AtCoder self, AtObject object, const char *key);
void AtCoderEncodeString(AtCoder self, const char *string, const char *key);
void AtCoderEncodeObjects(AtCoder self, AtObject *objects, uint32 numObjects, const char *key);
void AtCoderEncodeList(AtCoder self, AtList list, const char *key);
void AtCoderEncodeUInt32List(AtCoder self, AtList list, const char *key);
void AtCoderEncodeUInt8Array(AtCoder self, uint8 *values, uint32 size, const char *key);
void AtCoderEncodeUInt16Array(AtCoder self, uint16 *values, uint32 size, const char *key);
void AtCoderEncodeBoolArray(AtCoder self, eBool *values, uint32 size, const char *key);
void AtCoderEncodeUInt32Array(AtCoder self, uint32 *values, uint32 size, const char *key);
void AtCoderEncodeUInt64Array(AtCoder self, uint64 *values, uint32 size, const char *key);
void AtCoderEncodeObjectDescriptionInList(AtCoder self, AtList list, const char *key);
void AtCoderEncodeObjectDescriptionInArray(AtCoder self, AtObject *objects, uint32 numObjects, const char *key);

uint32 AtCoderDecodeUInt(AtCoder self, const char *key);
AtObject AtCoderDecodeObject(AtCoder self, const char *key);
const char *AtCoderDecodeString(AtCoder self, const char *key);
uint32 AtCoderDecodeObjects(AtCoder self, AtObject *objects, const char *key);

/* Default coders */
void AtDefaultCoderOutputFileSet(AtDefaultCoder self, AtFile outputFile);
AtFile AtDefaultCoderOutputFileGet(AtDefaultCoder self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCODER_H_ */


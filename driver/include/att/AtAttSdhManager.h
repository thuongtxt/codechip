/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : AtAttPdhManager
 *
 * File        : AtAttPdhManager.h
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : This file contains common prototypes of PDH module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATATTSDHMANAGER_H_
#define _ATATTSDHMANAGER_H_

/*--------------------------- Include files ----------------------------------*/
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedef ----------------------------------------*/
typedef struct tAtAttSdhManager * AtAttSdhManager;
typedef enum eAttSdhForceAlarmDurationUnit
    {
    cAttSdhForceAlarmDurationUnitMillisecond,
    cAttSdhForceAlarmDurationUnitSecond
    } eAttSdhForceAlarmDurationUnit;
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet  AtAttSdhManagerInit(AtAttSdhManager self);

eAtRet AtAttSdhManagerLoForcePeriodSet(AtAttSdhManager self, uint32 numSeconds);
uint32 AtAttSdhManagerLoForcePeriodGet(AtAttSdhManager self);

eAtRet AtAttSdhManagerHoForcePeriodSet(AtAttSdhManager self, uint32 numSeconds);
uint32 AtAttSdhManagerHoForcePeriodGet(AtAttSdhManager self);

uint32 AtAttSdhManagerTiePeriodicProcess(AtAttSdhManager self, uint32 periodInMs);
uint32 AtAttSdhManagerSaveTiePeriodicProcess(AtAttSdhManager self, uint32 periodInMs);

eAtRet AtAttSdhManagerForcePointerAdjEnable(AtAttSdhManager self, eBool en);
uint32 AtAttSdhManagerForcePointerPeriodicProcess(AtAttSdhManager self, uint32 periodInMs);
#ifdef __cplusplus
}
#endif
#endif /* _ATATTSDHMANAGER_H_ */

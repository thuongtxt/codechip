/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : AtAttController.h
 * 
 * Created Date: May 16, 2016
 *
 * Description : ATT controller header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATTEXPECT_H_
#define _ATATTEXPECT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtAttExpect        * AtAttExpect;
typedef struct tAtAttExpectChannel * AtAttExpectChannel;
typedef struct tAtAttExpectChannelTime
    {
    tAtOsalCurTime time;
    eBool          set;
    }tAtAttExpectChannelTime;

AtAttExpect AtAttExpectNew(void);
eAtRet AtAttExpectInit(AtAttExpect self);

AtAttExpectChannel AtAttExpectChannelNew(AtChannel channel, const char* alarmString);
eAtRet AtAttExpectChannelInit(AtAttExpectChannel self);
eAtRet AtAttExpectChannelTypeSet(AtAttExpectChannel self, uint32 type);
uint32 AtAttExpectChannelTypeGet(AtAttExpectChannel self);
eAtRet AtAttExpectChannelDurationSet(AtAttExpectChannel self, uint32 duration);
uint32 AtAttExpectChannelDurationGet(AtAttExpectChannel self);
eAtRet AtAttExpectChannelModeSet(AtAttExpectChannel self, eAtAttForceAlarmMode mode);
eAtAttForceAlarmMode AtAttExpectChannelModeGet(AtAttExpectChannel self);
eAtRet AtAttExpectChannelNumEventSet(AtAttExpectChannel self, uint32 numEvent);
uint32 AtAttExpectChannelNumEventGet(AtAttExpectChannel self);
eAtRet AtAttExpectChannelAlarmCountSet(AtAttExpectChannel self, uint32 count);
uint32 AtAttExpectChannelAlarmCountGet(AtAttExpectChannel self);
AtList AtAttExpectChannelTimeList(AtAttExpectChannel self);
eAtRet AtAttExpectChannelRecordTimeAdd(AtAttExpectChannel self, const char* state);
eAtRet AtAttExpectChannelRecordTimeClear(AtAttExpectChannel self);
uint32 AtAttExpectChannelRecordTimeNumSet(AtAttExpectChannel self);
uint32 AtAttExpectChannelRecordTimeNumClear(AtAttExpectChannel self);
uint32 AtAttExpectChannelRecordTimeDiffGet(AtAttExpectChannel self);
eBool  AtAttExpectChannelIsOkToClear(AtAttExpectChannel self);
eBool  AtAttExpectChannelIsOk(AtAttExpectChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _ATATTEXPECT_H_ */


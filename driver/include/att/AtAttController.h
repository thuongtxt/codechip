/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : AtAttController.h
 * 
 * Created Date: May 16, 2016
 *
 * Description : ATT controller header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATTCONTROLLER_H_
#define _ATATTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPdhDe3.h"
#include "AtPdhDe1.h"
#include "AtAttWanderChannel.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtAttController * AtAttController;
typedef enum eAtSignalingAlarmType
    {
    cAtSignalingLop =cBit0,
    cAtSignalingRpe =cBit1
    }eAtSignalingAlarmType;
typedef enum eAtSdhPathPointerAdjType
    {
    cAtSdhPathPointerAdjTypeIncrease = 0,
    cAtSdhPathPointerAdjTypeDecrease = 1
    }eAtSdhPathPointerAdjType;

typedef enum eAtAttPdhDe1FbitType
    {
    cAtAttPdhDe1FbitTypeFas        = 0 ,
    cAtAttPdhDe1FbitTypeNfas       = 1 ,
    cAtAttPdhDe1FbitTypeCrcMfas    = 2 ,
    cAtAttPdhDe1FbitTypeMfas       = 3
    }eAtAttPdhDe1FbitType;

typedef enum eAtAttPdhDe3ErrorType
    {
    cAtAttPdhDe3ErrorTypeNone     = 0,
    cAtAttPdhDe3ErrorTypeFbit     = cBit0,
    cAtAttPdhDe3ErrorTypeCpbit    = cBit1,
    cAtAttPdhDe3ErrorTypePbit     = cBit2,
    cAtAttPdhDe3ErrorTypeFabyte   = cBit3,
    cAtAttPdhDe3ErrorTypeBip8byte = cBit4,
    cAtAttPdhDe3ErrorTypeReibit   = cBit5,
    cAtAttPdhDe3ErrorTypeNrbyte   = cBit6,
    cAtAttPdhDe3ErrorTypeGcbyte   = cBit7,
    cAtAttPdhDe3ErrorTypeFebe     = cBit8
    }eAtAttPdhDe3ErrorType;

typedef enum eAtAttForceType
    {
    cAtAttForceTypeNone,
    cAtAttForceTypeOneShot,
    cAtAttForceTypeContinuous
    }eAtAttForceType;

typedef enum eAtAttForcePointerAdjMode
	{
	cAtAttForcePointerAdjModeContinuous,
	cAtAttForcePointerAdjModeOneshot,
	cAtAttForcePointerAdjModeNeventInT,
	cAtAttForcePointerAdjModeConsecutive,
	cAtAttForcePointerAdjModeG783a,
	cAtAttForcePointerAdjModeG783b,
	cAtAttForcePointerAdjModeG783c,
	cAtAttForcePointerAdjModeG783d,
	cAtAttForcePointerAdjModeG783e,
	cAtAttForcePointerAdjModeG783f,
	cAtAttForcePointerAdjModeG783gPart1,
	cAtAttForcePointerAdjModeG783gPart2,
	cAtAttForcePointerAdjModeG783gPart3,
	cAtAttForcePointerAdjModeG783gPart4,
	cAtAttForcePointerAdjModeG783hParta,
	cAtAttForcePointerAdjModeG783hPartb,
	cAtAttForcePointerAdjModeG783hPartc,
	cAtAttForcePointerAdjModeG783hPartd,
	cAtAttForcePointerAdjModeG783i,
	cAtAttForcePointerAdjModeG783jParta,
	cAtAttForcePointerAdjModeG783jPartb,
	cAtAttForcePointerAdjModeG783jPartc,
	cAtAttForcePointerAdjModeG783jPartd,
	cAtAttForcePointerAdjModeG7834frames,
	cAtAttForcePointerAdjModeG7834invalid
	}eAtAttForcePointerAdjMode;

typedef enum eAtAttForceAlarmMode
    {
    cAtAttForceAlarmModeContinuous,
    cAtAttForceAlarmModeSetInT,
    cAtAttForceAlarmModeSetInNFrame,
    cAtAttForceAlarmModeNeventInT
    }eAtAttForceAlarmMode;

typedef enum eAtAttForceErrorMode
    {
    cAtAttForceErrorModeContinuous,
    cAtAttForceErrorModeRate,
    cAtAttForceErrorModeOneshot,
    cAtAttForceErrorModeNerrorInT,
	cAtAttForceErrorModeConsecutive
    }eAtAttForceErrorMode;

typedef enum eAtAttSignalingMode
    {
    cAtAttSignalingModeInvalid,
    cAtAttSignalingModePrbs,
    cAtAttSignalingModeFixedAB,
    cAtAttSignalingModeFixedABCD,
    cAtAttSignalingModeSequence,
    }eAtAttSignalingMode;

typedef struct tAtAttForceErrorConfiguration
    {
    uint32 bitPosition;
    uint32 bitPosition2nd;
    uint32 bitPosition3rd;

    uint8 dataMask;
    uint8 dataMask2nd;
    uint8 dataMask3rd;
    uint32 numErrors;

    eAtAttForceType forceType;
    }tAtAttForceErrorConfiguration;

typedef struct tAtAttForceErrorStatus
    {
    uint32 positionMaskStatus;
    uint32 remainForceErrorStatus;
    uint8 errorStatus;
    uint8 oneSecondEnableStatus;
    }tAtAttForceErrorStatus;

typedef struct tAtAttForceAlarmConfiguration
    {
    uint8  duration;
    uint32 numEvent;
    eAtAttForceType forceType;
    }tAtAttForceAlarmConfiguration;

typedef struct tAtAttForceAlarmStatus
    {
    uint32  positionIndex;
    uint32  eventCounter;
    uint32  secondCounter;
    uint32  updatedStatus;
    uint32  miliSecondStatus;
    uint32  secondStatus;
    uint32  enableStatus;
    }tAtAttForceAlarmStatus;

typedef enum eAtAttForcePointerState
    {
    cAtAttForcePointerStateWaitConfig,
    cAtAttForcePointerStateForceOneshot,
    cAtAttForcePointerStateStart,
    cAtAttForcePointerState1stForce,
    cAtAttForcePointerState1stForceDelay,
    cAtAttForcePointerState2ndForce,
    cAtAttForcePointerState2ndForceDelay,
    cAtAttForcePointerState3rdForce,
    cAtAttForcePointerState3rdForceDelay,
    cAtAttForcePointerState4thForce,
    cAtAttForcePointerState4thForceDelay,
    cAtAttForcePointerState5thForce,
    cAtAttForcePointerState5thForceDelay,
    cAtAttForcePointerState6thForce,
    cAtAttForcePointerState6thForceDelay,
    cAtAttForcePointerState7thForce,
    cAtAttForcePointerState7thForceDelay,
    cAtAttForcePointerState8thForce,
    cAtAttForcePointerState8thForceDelay,
    cAtAttForcePointerState9thForce,
    cAtAttForcePointerState9thForceDelay,
    cAtAttForcePointerState10thForce,
    cAtAttForcePointerState10thForceDelay,
    cAtAttForcePointerState11thForce,
    cAtAttForcePointerState11thForceDelay,
    cAtAttForcePointerStateStopForce,
    cAtAttForcePointerStateEnd
    }eAtAttForcePointerState;



/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtAttControllerForceErrorModeSet(AtAttController self, uint32 alarmType, eAtAttForceErrorMode mode);
eAtRet AtAttControllerForceErrorNumErrorsSet(AtAttController self, uint32 errorType, uint32 numErrors);
eAtRet AtAttControllerForceErrorBitPositionSet(AtAttController self, uint32 errorType, uint32 bitPosition);
eAtRet AtAttControllerForceErrorBitPosition2Set(AtAttController self, uint32 errorType, uint32 bitPosition);
eAtRet AtAttControllerForceErrorDataMaskSet(AtAttController self, uint32 errorType, uint32 dataMask);
eAtRet AtAttControllerForceErrorCrcMaskSet(AtAttController self, uint32 errorType, uint32 crcMask);
eAtRet AtAttControllerForceErrorCrcMask2Set(AtAttController self, uint32 errorType, uint32 crcMask);
eAtRet AtAttControllerForceErrorStepSet(AtAttController self, uint32 errorType, uint32 step);
uint32 AtAttControllerForceErrorStepGet(AtAttController self, uint32 errorType);
eAtRet AtAttControllerForceErrorStep2Set(AtAttController self, uint32 errorType, uint32 step);
eAtRet AtAttControllerForceError(AtAttController self, uint32 errorType);
eAtRet AtAttControllerForceErrorContinuous(AtAttController self, uint32 errorType);
eAtRet AtAttControllerForcePrbsModeChange(AtAttController self, uint32 period);
eAtRet AtAttControllerForceErrorPerSecond(AtAttController self, uint32 errorType);
eAtRet AtAttControllerForceErrorRate(AtAttController self, uint32 errorType, eAtBerRate rate);
eAtRet AtAttControllerForceErrorDuration(AtAttController self, uint32 errorType, uint32 duration);
eAtRet AtAttControllerUnForceError(AtAttController self, uint32 errorType);
eAtRet AtAttControllerForceErrorGet(AtAttController self, uint32 errorType, tAtAttForceErrorConfiguration *configuration);
eAtRet AtAttControllerForceErrorStatusGet(AtAttController self, uint32 errorType, tAtAttForceErrorStatus *status);
eAtRet AtAttControllerDebugError(AtAttController self, uint32 errorType);

eAtRet AtAttControllerForceAlarmBitPositionSet(AtAttController self, uint32 alarmType, uint32 bitPosition);
eAtRet AtAttControllerForceAlarmModeSet(AtAttController self, uint32 alarmType, eAtAttForceAlarmMode mode);
eAtRet AtAttControllerForceAlarmNumEventSet(AtAttController self, uint32 alarmType, uint32 numEvent);
eAtRet AtAttControllerForceAlarmNumFrameSet(AtAttController self, uint32 alarmType, uint32 numFrame);
eAtRet AtAttControllerForceAlarm(AtAttController self, uint32 alarmType);
eAtRet AtAttControllerForceAlarmDurationSet(AtAttController self, uint32 alarmType, uint32 duration);
eAtRet AtAttControllerForceAlarmStepSet(AtAttController self, uint32 alarmType, uint32 step);
eAtRet AtAttControllerForceAlarmPerSecond(AtAttController self, uint32 alarmType);
eAtRet AtAttControllerForceAlarmContinuous(AtAttController self, uint32 alarmType);
eAtRet AtAttControllerUnForceAlarm(AtAttController self, uint32 alarmType);
eAtRet AtAttControllerForceAlarmGet(AtAttController self, uint32 alarmType, tAtAttForceAlarmConfiguration *configuration);
eAtRet AtAttControllerForceAlarmStatusGet(AtAttController self, uint32 alarmType, tAtAttForceAlarmStatus *status);
eAtRet AtAttControllerDebugAlarm(AtAttController self, uint32 alarmType);
eAtRet AtAttControllerRxAlarmCaptureDurationSet(AtAttController self, uint32 alarmType, uint32 duration);
uint32 AtAttControllerRxAlarmCaptureDurationGet(AtAttController self, uint32 alarmType);
eAtRet AtAttControllerRxAlarmCaptureModeSet(AtAttController self, uint32 alarmType, eAtAttForceAlarmMode mode);
eAtAttForceAlarmMode AtAttControllerRxAlarmCaptureModeGet(AtAttController self, uint32 alarmType);
eAtRet AtAttControllerRxAlarmCaptureNumEventSet(AtAttController self, uint32 alarmType, uint32 numevent);
uint32 AtAttControllerRxAlarmCaptureNumEventGet(AtAttController self, uint32 alarmType);
AtList AtAttControllerRecordTime(AtAttController self, uint32 alarmType);
eAtRet AtAttControllerRecordTimeAdd(AtAttController self, uint32 alarmType, const char* state);
eAtRet AtAttControllerRecordTimeClear(AtAttController self, uint32 alarmType);
uint32 AtAttControllerRecordTimeNumSet(AtAttController self, uint32 alarmType);
uint32 AtAttControllerRecordTimeNumClear(AtAttController self, uint32 alarmType);
uint32 AtAttControllerRecordTimeDiffGet(AtAttController self, uint32 alarmType);
eBool AtAttControllerRecordTimeIsOk(AtAttController self, uint32 alarmType);
eBool AtAttControllerRecordTimeIsOkToClear(AtAttController self, uint32 alarmType);

AtChannel AtAttControllerChannelGet(AtAttController self);
eAtRet AtAttControllerInit(AtAttController self);
eAtRet AtAttControllerSetUp(AtAttController self);
eBool AtAttControllerAlarmForceIsSupported(AtAttController self, uint32 alarmType);
eBool AtAttControllerErrorForceIsSupported(AtAttController self, uint32 errorType);
eAtRet AtAttControllerForcePeriodSet(AtAttController self, uint32 numSeconds);
uint32 AtAttControllerForcePeriodGet(AtAttController self);

eAtRet AtAttControllerDe1ForceFbitTypeSet(AtAttController self, uint32 type, eBool en);
eBool AtAttControllerDe1ForceFbitTypeGet(AtAttController self, uint32 type);

eAtRet AtAttControllerFrequenceOffsetSet(AtAttController self, float ppm);
float AtAttControllerFrequenceOffsetGet(AtAttController self);
AtAttWanderChannel AtAttControllerWanderGet(AtAttController self);

eAtRet AtAttControllerTxAlarmsSet(AtAttController self, uint32 txAlarmTypes);
eAtRet AtAttControllerTxAlarmsClear(AtAttController self, uint32 txAlarmTypes);
uint32 AtAttControllerTxAlarmsGet(AtAttController self);
eAtRet AtAttControllerDe1CrcGapSet(AtAttController self, uint16 frameType);

eAtRet AtAttControllerForcePointerAdjModeSet(AtAttController self,     uint32 pointerType, eAtAttForcePointerAdjMode mode);
eAtAttForcePointerAdjMode AtAttControllerForcePointerAdjModeGet(AtAttController self,     uint32 pointerType);
eAtRet AtAttControllerForcePointerAdjNumEventSet(AtAttController self, uint32 pointerType, uint32 numEvent);
eAtRet AtAttControllerForcePointerAdjDurationSet(AtAttController self, uint32 pointerType, uint32 duration);
uint32 AtAttControllerForcePointerAdjDurationGet(AtAttController self, uint32 pointerType);
eAtRet AtAttControllerHwForcePointerAdj(AtAttController self,          uint32 pointerType);
eAtRet AtAttControllerForcePointerAdj(AtAttController self,            uint32 pointerType);
eAtRet AtAttControllerUnForcePointerAdj(AtAttController self,          uint32 pointerType);
eAtRet AtAttControllerHwUnForcePointerAdj(AtAttController self, uint32 pointerType);
eAtRet AtAttControllerForcePointerAdjBitPositionSet(AtAttController self, uint32 pointerType, uint32 bitPosition);
eAtRet AtAttControllerForcePointerAdjDataMaskSet(AtAttController self,    uint32 pointerType, uint32 dataMask);
eAtRet AtAttControllerForcePointerAdjStepSet(AtAttController self,        uint32 pointerType, uint32 step);
eAtRet AtAttControllerPointerAdjForcingHwDataMaskSet(AtAttController self, eAtAttPdhDe3ErrorType errorType, uint32 dataMask);
eAtRet AtAttControllerPointerAdjEnable(AtAttController self, eBool force);

eAtRet AtAttControllerTxSignalingPatternSet(AtAttController self, uint8 tsId, eAtAttSignalingMode sigMode, uint8 threshold, uint8 sigPattern);
eAtRet AtAttControllerRxExpectedSignalingPatternSet(AtAttController self, uint8 tsId, eAtAttSignalingMode sigMode, uint8 threshold, uint8 sigPattern);
eAtAttSignalingMode AtAttControllerTxSignalingPatternGet(AtAttController self, uint8 tsId, uint8 *sigPattern, uint8 *threshold);
eAtAttSignalingMode AtAttControllerRxExpectedSignalingPatternGet(AtAttController self, uint8 tsId, uint8 *sigPattern, uint8 *threshold);
eAtRet AtAttControllerTxSignalingEnable(AtAttController       self, uint8 tsId, eBool enable);
eAtRet AtAttControllerRxSignalingEnable(AtAttController       self, uint8 tsId, eBool enable);
eBool  AtAttControllerTxSignalingIsEnabled(AtAttController    self, uint8 tsId);
eBool  AtAttControllerRxSignalingIsEnabled(AtAttController    self, uint8 tsId);
eAtRet AtAttControllerTxSignalingDump(AtAttController self, uint8 tsId, uint8 *value, uint8 length);
eAtRet AtAttControllerRxSignalingDump(AtAttController self, uint8 tsId, uint8 *value, uint8 length);
eAtRet AtAttControllerTxSignalingDumpDebug(AtAttController self, uint8 tsId, uint8 *value, uint8 length);
eAtRet AtAttControllerRxSignalingDumpDebug(AtAttController self, uint8 tsId, uint8 *value, uint8 length);
eBool  AtAttControllerSignalingLopAlarmGet(AtAttController  self, uint8 tsId);
uint32  AtAttControllerSignalingLopStickyGet(AtAttController self, uint8 tsId, eBool r2c);
eBool  AtAttControllerSignalingFramerStickyGet(AtAttController self, uint8 tsId, eBool r2c);
uint32 AtAttControllerBipCounterGet(AtAttController self, eBool r2c);

#ifdef __cplusplus
}
#endif
#endif /* _ATATTCONTROLLER_H_ */


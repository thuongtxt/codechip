/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : AtAttPdhManager
 *
 * File        : AtAttPdhManager.h
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : This file contains common prototypes of PDH module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATATTPDHMANAGER_H_
#define _ATATTPDHMANAGER_H_

/*--------------------------- Include files ----------------------------------*/
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedef ----------------------------------------*/
typedef struct tAtAttPdhManager * AtAttPdhManager;

typedef enum eAttPdhForceAlarmDurationUnit
    {
    cAttPdhForceAlarmDurationUnitMillisecond,
    cAttPdhForceAlarmDurationUnitSecond
    } eAttPdhForceAlarmDurationUnit;
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet AtAttPdhManagerInit(AtAttPdhManager self);
eAtRet AtAttPdhManagerDe1TieInit(AtAttPdhManager self);
eAtRet AtAttPdhManagerDe1TieBufRead(AtAttPdhManager self);
eAtRet AtAttPdhManagerDe1ForcePeriodSet(AtAttPdhManager self, uint32 numSeconds);
uint32 AtAttPdhManagerDe1ForcePeriodGet(AtAttPdhManager self);
eAtRet AtAttPdhManagerDe3ForcePeriodSet(AtAttPdhManager self, uint32 numSeconds);
uint32 AtAttPdhManagerDe3ForcePeriodGet(AtAttPdhManager self);
eAtRet AtAttPdhManagerDe3ForcePerframeEnable(AtAttPdhManager self);
eAtRet AtAttPdhManagerDe3ForcePerframeDisable(AtAttPdhManager self);
eBool  AtAttPdhManagerDe3ForcePerframeIsEnabled(AtAttPdhManager self);
uint32 AtAttPdhManagerTiePeriodicProcess(AtAttPdhManager self, uint32 periodInMs);
uint32 AtAttPdhManagerSaveTiePeriodicProcess(AtAttPdhManager self, uint32 periodInMs);
eAtRet AtAttPdhManagerForcePerframeSet(AtAttPdhManager self, eBool enable);
eBool AtModulePdhAttManagerHasRegister(AtAttPdhManager self, uint32 address);
uint16 AtModulePdhAttManagerLongRead(AtAttPdhManager self, uint32 localAddress, uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
uint16 AtModulePdhAttManagerLongWrite(AtAttPdhManager self, uint32 localAddress, const uint32 *dataBuffer, uint16 bufferLen, AtIpCore core);
#ifdef __cplusplus
}
#endif
#endif /* _ATATTPDHMANAGER_H_ */

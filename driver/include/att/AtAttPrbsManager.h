/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : AtAttPdhManager
 *
 * File        : AtAttPdhManager.h
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : This file contains common prototypes of PDH module
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATATTPRBSMANAGER_H_
#define _ATATTPRBSMANAGER_H_

/*--------------------------- Include files ----------------------------------*/
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedef ----------------------------------------*/
typedef struct tAtAttPrbsManager * AtAttPrbsManager;


typedef enum eAttForceLogic
    {
    cAttForceLogicDut,
    cAttForceLogicAtt
    } eAttForceLogic;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet  AtAttPrbsManagerInit(AtAttPrbsManager self);
eAtRet AtAttPrbsManagerForceLogicSet(AtAttPrbsManager self, eAttForceLogic forceLogic);
eAttForceLogic AtAttPrbsManagerForceLogicGet(AtAttPrbsManager self);

eAtRet AtAttPrbsManagerTimeMeasurementErrorSet(AtAttPrbsManager self, uint32 period);
uint32 AtAttPrbsManagerTimeMeasurementErrorGet(AtAttPrbsManager self);
eAtRet AtAttPrbsManagerUseComplexIdSet(AtAttPrbsManager self, eBool en);
eBool AtAttPrbsManagerUseComplexIdGet(AtAttPrbsManager self);
eBool AtAttPrbsManagerDelayIsSupported(AtAttPrbsManager self);
eAtRet AtAttPrbsManagerTieReset(AtAttPrbsManager self);


#ifdef __cplusplus
}
#endif
#endif /* _ATATTPRBSMANAGER_H_ */

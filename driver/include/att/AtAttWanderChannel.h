/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : AtAttWanderChannel.h
 * 
 * Created Date: Jun 7, 2017
 *
 * Description : ATT Wander Channel header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATATTWANDERCHANNEL_H_
#define _ATATTWANDERCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define cAtAlarmStrBufLength  100
/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtAttWanderChannel * AtAttWanderChannel;
typedef struct tAtAttWanderChannelTime
    {
    tAtOsalCurTime time;
    float          value;
    eBool          sign;
    uint8          type;
    uint32         alarm;
    uint32         regVal;
    }tAtAttWanderChannelTime;
AtAttWanderChannel AtAttWanderChannelNew(AtChannel channel);
eAtRet AtAttWanderChannelInit(AtAttWanderChannel self);
AtList AtAttWanderChannelListGet(AtAttWanderChannel self, uint8 page);
eAtRet AtAttWanderChannelFileNameSet(AtAttWanderChannel self, const char* filename);
eAtRet AtAttWanderChannelSaveAs(AtAttWanderChannel self, const char* filename);
eAtRet AtAttWanderChannelDurationSet(AtAttWanderChannel self, uint32 duration);
uint32 AtAttWanderChannelDurationGet(AtAttWanderChannel self);
eAtRet AtAttWanderChannelDebugEnable(AtAttWanderChannel self);
eAtRet AtAttWanderChannelDebugDisable(AtAttWanderChannel self);
eBool  AtAttWanderChannelDebugIsEnabled(AtAttWanderChannel self);
eAtRet AtAttWanderChannelPeriodicProcess(AtAttWanderChannel self);
eAtRet AtAttWanderChannelSavePeriodicProcess(AtAttWanderChannel self);
eAtRet AtAttWanderChannelSetTimeReset(AtAttWanderChannel self, tAtOsalCurTime *time_start);
eAtRet AtAttWanderChannelIncrease(AtAttWanderChannel self);
eAtRet AtAttWanderChannelStart(AtAttWanderChannel self);
eAtRet AtAttWanderChannelStop(AtAttWanderChannel self);
/*eAtRet AtAttWanderChannelAddValue(AtAttWanderChannel self, uint32 numclock, eBool NClockSign, uint8 NClockCount);*/
#ifdef __cplusplus
}
#endif
#endif /* _ATATTWANDERCHANNEL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : AtEthVlanTag.h
 * 
 * Created Date: Apr 3, 2015
 *
 * Description : VLAN tag
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATETHVLANTAG_H_
#define _ATETHVLANTAG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAtMaxNumVlanTag 2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @brief VLAN Tag
 */
typedef struct tAtEthVlanTag
    {
    uint8 priority; /**< [0..7] Vlan priority, can be used to in QoS. The field
                         indicates the frame priority level which can be used for
                         the priority of traffic. The field can represent
                         8 levels */
    uint8 cfi;      /**< [0..1] Canonical Format Indicator. If the value of this
                         field is 1, the MAC address is in noncanonical format.
                         If the value is 0, the MAC address is in canonical format */
    uint16 vlanId;  /**<  [0..4095] VLAN ID */
    }tAtEthVlanTag;

/**
 *@brief VLAN traffic descriptor
 */
typedef struct tAtEthVlanDesc
    {
    uint8         ethPortId;               /**< Ethernet port ID */
    uint8         numberOfVlans;           /**< Number of VLANs */
    tAtEthVlanTag vlans[cAtMaxNumVlanTag]; /**< VLAN tags */
    }tAtEthVlanDesc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
tAtEthVlanDesc *AtEthFlowVlanDescConstruct(uint8 port, tAtEthVlanTag *sVlan, tAtEthVlanTag *cVlan, tAtEthVlanDesc *desc);
tAtEthVlanTag *AtEthVlanTagConstruct(uint8 priority, uint8 cfi, uint16 vlanId, tAtEthVlanTag *vlanTag);
tAtEthVlanTag *AtEthVlanDescCVlan(tAtEthVlanDesc *descriptor);
tAtEthVlanTag *AtEthVlanDescSVlan(tAtEthVlanDesc *descriptor);

#ifdef __cplusplus
}
#endif
#endif /* _ATETHVLANTAG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Generic
 * 
 * File        : AtCommon.h
 * 
 * Created Date: Aug 3, 2012
 *
 * Description : Common declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCOMMON_H_
#define _ATCOMMON_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtRet.h"
#include "AtHal.h"
#include "AtClasses.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup grpAtCommon
 * @{
 */

/** @brief Side (TX/RX) */
typedef enum eAtSide
    {
    cAtSideRx,  /**< RX */
    cAtSideTx,  /**< TX */
    cAtSideBoth /**< Both RX and TX */
    }eAtSide;

/** @brief BER rates */
typedef enum eAtBerRate
    {
    cAtBerRateUnknown, /**< Unknown error rate */
    cAtBerRate1E2    , /**< Error rate is 10-2 */
    cAtBerRate1E3    , /**< Error rate is 10-3 */
    cAtBerRate1E4    , /**< Error rate is 10-4 */
    cAtBerRate1E5    , /**< Error rate is 10-5 */
    cAtBerRate1E6    , /**< Error rate is 10-6 */
    cAtBerRate1E7    , /**< Error rate is 10-7 */
    cAtBerRate1E8    , /**< Error rate is 10-8 */
    cAtBerRate1E9    , /**< Error rate is 10-9 */
    cAtBerRate1E10     /**< Error rate is 10-10 */
    }eAtBerRate;

/** @brief Timing modes */
typedef enum eAtTimingMode
    {
    cAtTimingModeUnknown     , /**< Unknown timing mode */
    cAtTimingModeSys         , /**< System timing mode */
    cAtTimingModeLoop        , /**< Loop timing mode */
    cAtTimingModeSdhSys      , /**< SDH system clock */
    cAtTimingModeSdhLineRef  , /**< SDH Line */
	cAtTimingModePrc         , /**< PRC timing source mode */
    cAtTimingModeExt1Ref     , /**< Line EXT #1 timing mode */
    cAtTimingModeExt2Ref     , /**< Line EXT #2 timing mode */
    cAtTimingModeAcr         , /**< ACR timing mode */
    cAtTimingModeDcr         , /**< DCR timing mode */
    cAtTimingModeSlave       , /**< Slave timing mode */
    cAtTimingModeFreeRunning , /**< Free-running timing mode */
    cAtTimingModeNotApplicable /**< Not applicable */
    }eAtTimingMode;

/**
 * @brief Clock state
 */
typedef enum eAtClockState
    {
    cAtClockStateUnknown,       /**< Unknown or not applicable state */
    cAtClockStateNotApplicable, /**< Not applicable */
    cAtClockStateInit,          /**< Init state */
    cAtClockStateHoldOver,      /**< Holdover state */
    cAtClockStateLearning,      /**< Learn State */
    cAtClockStateLocked         /**< Locked State */
    }eAtClockState;

/** @brief Common loopback modes */
typedef enum eAtLoopbackMode
    {
    cAtLoopbackModeRelease, /**< Release loopback */
    cAtLoopbackModeLocal,   /**< Local loopback */
    cAtLoopbackModeRemote,  /**< Remote loopback */
    cAtLoopbackModeDual     /**< Dual loopback (Remote and local) */
    }eAtLoopbackMode;

/**
 * @brief Queueing mode
 */
typedef enum eAtQueueMode
    {
    cAtQueueModeInvalid,        /**< Invalid mode */
    cAtQueueModeDwrr,           /**< Deficit Weighted Round Robin (DWRR)*/
    cAtQueueModeStrictPriority  /**< Strict priority queueing (PQ) */
    }eAtQueueMode;

/**
 * @brief Fragment size
 */
typedef enum eAtFragmentSize
    {
    cAtNoFragment       = 0,  /**< No fragment */
    cAtFragmentSize64   = 1,  /**< 64-byte fragment */
    cAtFragmentSize128  = 2,  /**< 128-byte fragment */
    cAtFragmentSize256  = 3,  /**< 256-byte fragment */
    cAtFragmentSize512  = 4   /**< 512-byte fragment */
    }eAtFragmentSize;

/**
 * @brief LED state
 */
typedef enum eAtLedState
    {
    cAtLedStateOff,   /**< Turn off LED */
    cAtLedStateOn,    /**< Turn on LED (but not blink) */
    cAtLedStateBlink,        /**< Turn on LED and make it blink */
    cAtLedStateNotApplicable /**< Not applicable */
    }eAtLedState;

/** @brief Bit order */
typedef enum eAtBitOrder
    {
    cAtBitOrderUnknown,      /**< Invalid order */
    cAtBitOrderMsb,          /**< MSB */
    cAtBitOrderLsb,          /**< LSB */
    cAtBitOrderNotApplicable /**< Not applicable */
    }eAtBitOrder;

/** @brief VLAN */
typedef struct tAtVlan
    {
    uint16 tpid;    /**< Tag Protocol Identifier */
    uint8 priority; /**< Priority Code Point */
    uint8 cfi;      /**< Canonical Format Indicator */
    uint16 vlanId;  /**< VLAN ID */
    }tAtVlan;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool AtLedStateIsValid(eAtLedState ledState);
eBool AtLoopbackModeIsValid(eAtLoopbackMode loopbackMode);
const char *AtLoopbackMode2Str(eAtLoopbackMode loopbackMode);
const char *AtRet2String(eAtRet ret);

#ifdef __cplusplus
}
#endif
#endif /* _ATCOMMON_H_ */


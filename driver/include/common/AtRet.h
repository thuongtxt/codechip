/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Generic
 * 
 * File        : AtRet.h
 * 
 * Created Date: Apr 5, 2015
 *
 * Description : Return codes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATRET_H_
#define _ATRET_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cAtNumErrorCodesPerModule 1024

/*--------------------------- Macros -----------------------------------------*/
#define mNextStartRet(from) (from + cAtNumErrorCodesPerModule + 1)

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup grpAtCommon
 * @{
 */

/**
 * @brief Return codes
 */
typedef enum eAtRet
    {
    cAtOk,                     /**< Success */
    cAtErrorModeNotSupport,    /**< Mode not support */
    cAtErrorOutOfRangParm,     /**< The input param is out of range */
    cAtErrorRsrcNoAvail,       /**< Resource is not available */
    cAtErrorDevBusy ,          /**< Device busy */
    cAtErrorChannelBusy ,      /**< Channel is busy */
    cAtErrorResourceBusy ,     /**< Internal resource is busy */
    cAtErrorIndrAcsTimeOut,    /**< Time out while accessing indirect registers */
    cAtErrorInvlParm,          /**< Invalid parameters */
    cAtErrorDevFail,           /**< Invalid parameters */
    cAtErrorNullPointer,       /**< Null pointer */
    cAtErrorDevicePllNotLocked,/**< PLL is not locked */
    cAtErrorBerEngineFail,     /**< BER engine is fail */
    cAtErrorMemoryTestFail,    /**< Memory testing is fail */
    cAtErrorNotImplemented,    /**< Feature has not been implemented */
    cAtErrorNotEditable,       /**< Attribute is not editable */
    cAtErrorOsalFail,          /**< OS operation is fail */
    cAtErrorHalFail,           /**< HAL is fail */
    cAtErrorClockUnstable,     /**< Clock unstable */
    cAtErrorNotActivate,       /**< Channel, engine, ... has not been activated */
    cAtErrorNotApplicable,     /**< Not applicable */
    cAtErrorObjectNotExist,    /**< Object does not exist */
    cAtErrorNotReady,          /**< Not ready to work */
    cAtErrorNotFound,          /**< Object not found */
    cAtErrorBandwidthExceeded, /**< Bandwidth exceeded */
    cAtErrorAgain,             /**< Operation has not been complete, need to call it again */
    cAtErrorInvalidAddress,    /**< Invalid address */
    cAtErrorNotControllable,   /**< Cannot control */
    cAtErrorInvalidOperation,  /**< Invalid operation */
    cAtErrorOperationTimeout,  /**< Timeout */
    cAtRetNeedDelay,           /**< Need fix delay notice for the asynchronous operation */
    cAtRetMoveNext,            /**< Allow an operation continue to execute its followed code */

    /* Other return codes */
    cAtErrorDdrCalibFail,      /**< DDR calibration fail */
    cAtErrorDdrInitFail,       /**< DDR initializing fail */
    cAtErrorQdrCalibFail,      /**< QDR calibration fail */
    cAtErrorRamAccessTimeout,  /**< RAM access (read/write) timeout */
    cAtErrorInvalidSSkey,      /**< Key invalid */
    cAtErrorIdConvertFail,     /**< ID converting is fail */
    cAtErrorDiagnosticFail,    /**< Diagnostic is fail */
    cAtErrorXfiPcsNotLocked,   /**< XFI PCS is not locked */
    cAtErrorDuplicatedEntries, /**< Duplicated entries */
    cAtErrorNotExist,          /**< Object does not exist */

    /* Eye scan error codes */
    cAtErrorEyeWidthOutOfRange, /**< Eye width is too large */
    cAtErrorEyeSwingOutOfRange, /**< Eye swing is too small */
    cAtErrorEyeScanTimeout,     /**< Eye scan timeout */

    /* Serdes error */
    cAtErrorSerdesResetTimeout,  /**< Serdes reset timeout */
    cAtErrorSerdesTxUnderFlow,   /**< Serdes TX buffer under flow */
    cAtErrorSerdesTxOverFlow,    /**< Serdes TX buffer over flow */
    cAtErrorSerdesRxUnderFlow,   /**< Serdes RX buffer under flow */
    cAtErrorSerdesRxOverFlow,    /**< Serdes RX buffer over flow */
    cAtErrorSerdesFail,          /**< Serdes has unknown error */
    cAtErrorSerdesMdioFail,      /**< Serdes MDIO fail */

    /* Soft Error Mitigation (SEM) IP */
    cAtErrorSemNotActive,        /**< SEM function is not active */
    cAtErrorSemFsmFailed,        /**< SEM state machine does not work correctly */
    cAtErrorSemFatalError,       /**< SEM function has fatal error and can not work properly */
    cAtErrorSemUncorrectable,    /**< SEM can not correct inserted CRC error */
    cAtErrorSemAddressNotFound,  /**< SEM can not find address to inject error */
    cAtErrorSemStateMoveFail,    /**< SEM can not move to specific state */
    cAtErrorSemValidateFail,     /**< SEM can not validate */
    cAtErrorSemTimeOut,          /**< SEM timeout */
    cAtErrorSemError,            /**< SEM general error */
    cAtErrorSemLfaAddressGenerate, /**< SEM could not generate LFA address to force error */
    cAtErrorSemGotZeroErrorCounter, /**< SEM force error but no error counted */
    cAtError,                    /**< General error */

    /* Return codes of other modules */
    cAtErrorAtmStartCode     = mNextStartRet(cAtError),               /**< ATM module start error code */
    cAtErrorImaStartCode     = mNextStartRet(cAtErrorAtmStartCode),   /**< IMA module start error code */
    cAtErrorBerStartCode     = mNextStartRet(cAtErrorImaStartCode),   /**< BER module start error code */
    cAtErrorEncapStartCode   = mNextStartRet(cAtErrorBerStartCode),   /**< ENCAP module start error code */
    cAtErrorEthStartCode     = mNextStartRet(cAtErrorEncapStartCode), /**< ETH module start error code */
    cAtErrorFrStartCode      = mNextStartRet(cAtErrorEthStartCode),   /**< FR module start error code */
    cAtErrorPdhStartCode     = mNextStartRet(cAtErrorFrStartCode),    /**< PDH module start error code */
    cAtErrorPppStartCode     = mNextStartRet(cAtErrorPdhStartCode),   /**< PPP module start error code */
    cAtErrorPrbsStartCode    = mNextStartRet(cAtErrorPppStartCode),   /**< PRBS module start error code */
    cAtErrorPwStartCode      = mNextStartRet(cAtErrorPrbsStartCode),  /**< PW module start error code */
    cAtErrorRamStartCode     = mNextStartRet(cAtErrorPwStartCode),    /**< RAM module start error code */
    cAtErrorSdhStartCode     = mNextStartRet(cAtErrorRamStartCode),   /**< SDH module start error code */
    cAtErrorClockStartCode   = mNextStartRet(cAtErrorSdhStartCode),   /**< Clock module start error code */
    cAtErrorXcStartCode      = mNextStartRet(cAtErrorClockStartCode), /**< XC module start error code */
    cAtErrorSurStartCode     = mNextStartRet(cAtErrorXcStartCode),    /**< SUR module start error code */
    cAtErrorConcateStartCode = mNextStartRet(cAtErrorSurStartCode),   /**< MAP module start error code */
    cAtErrorApsStartCode     = mNextStartRet(cAtErrorConcateStartCode)/**< APS module start error code */
    }eAtRet;

/** @brief IMA return error */
typedef enum eAtImaRet
    {
    cAtImaRetErrorGroupInUse = cAtErrorImaStartCode, /**< IMA group is active.  */
    cAtImaRetErrorGroupNotExist, /**< IMA group was not create.  */
    cAtImaRetErrorVpiVciRange,   /**< The VPI/VCI range to be specified for this group
                                      exceeds the values provided when the IMA service was i nit */
    cAtImaRetErrorNull,          /**< NULL configuration structure.  */
    cAtImaRetErrorLink,          /**< link values specified are either 0 or exceed 32 */
    cAtImaRetErrorMem,           /**< Unable to allocate resources/memory. */
    cAtImaRetErrorHw,            /**< Hardware access Error.*/
    cAtImaRetError               /**< General Error */
    }eAtImaRet;

/** @brief Encapsulation module return code */
enum
    {
    cAtModuleEncapErrorNoBoundChannel = cAtErrorEncapStartCode, /**< There is no bound physical channel */
    cAtModuleEncapErrorNoBoundFlow                              /**< There is no bound ethernet flow */
    };

/** @brief Cross-connect module return code */
typedef enum eAtModuleXcRet
    {
    cAtModuleXcErrorConnectionBusy = cAtErrorXcStartCode, /**< Connection is busy */
    cAtModuleXcErrorDifferentChannelType,    /**< Source and destination do not have the same type */
    cAtModuleXcErrorNotSupportedChannelType, /**< Channel type is not supported */
    cAtModuleXcErrorConnectionNotExist       /**< Connection does not exist */
    }eAtModuleXcRet;

/** @brief PRBS module return codes */
enum
    {
    cAtModulePrbsErrorNoBoundPw = cAtErrorPrbsStartCode /**< No bound PW */
    };

/** @brief APS module return codes */
enum
    {
    cAtModuleApsErrorExtCmdLowPriority = cAtErrorApsStartCode /**< Lower priority external command */
    };

/** @brief SDH module return codes */
enum
    {
    cAtModuleSdhErrorTtiModeMismatch = cAtErrorSdhStartCode, /**< Received TTI mode is mismatched */
    cAtModuleSdhErrorInvalidConcatenation /**< Invalid contiguous concatenation */
    };

/**
 * @}
 */


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATRET_H_ */


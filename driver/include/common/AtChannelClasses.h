/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Generic
 * 
 * File        : AtChannelClasses.h
 * 
 * Created Date: Apr 5, 2015
 *
 * Description : Channel classes
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCHANNELCLASSES_H_
#define _ATCHANNELCLASSES_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup grpAtCommon
 * @{
 */

/**
 * @brief AT channel. It is to abstract all of channel types that AT Device
 * can support. A channel can be Pseudowire, PDH channel or SONET/SDH channel.
 */
typedef struct tAtChannel * AtChannel;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _ATCHANNELCLASSES_H_ */


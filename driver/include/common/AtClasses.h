/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : AtClasses.h
 * 
 * Created Date: Jul 26, 2012
 *
 * Description : Common declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCLASSES_H_
#define _ATCLASSES_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup grpAtDevMan
 * @{
 */

/** @brief The root object class */
typedef struct tAtObject * AtObject;

/** @brief AT Device Driver */
typedef struct tAtDriver* AtDriver;

/** @brief AT Device */
typedef struct tAtDevice * AtDevice;

/** @brief IP core. Each AT device has at least one IP Core. */
typedef struct tAtIpCore * AtIpCore;

/** @brief AT module */
typedef struct tAtModule * AtModule;

/** @brief Any object */
typedef void * AtObjectAny;

/**
 * @}
 */

/**
 * @addtogroup AtIterator
 * @{
 */

/**
 * @brief AtInterator class
 */
typedef struct tAtIterator * AtIterator;

/**
 * @}
 */

/**
 * @addtogroup AtList
 * @{
 */

/**
 * @brief List class
 */
typedef struct tAtList * AtList;

/**
 * @}
 */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _ATCLASSES_H_ */


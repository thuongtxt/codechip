/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2003 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : attypes.h
 *
 * Created Date: 2003
 *
 * Description : This header file contains all the standard types and constants
 * definitions of AT System
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _ATTYPES_
#define _ATTYPES_
#ifdef __cplusplus
extern "C" {
#endif

#ifndef AT_SYSTEM_TYPE_H
#define AT_SYSTEM_TYPE_H <stddef.h>
#endif

#include AT_SYSTEM_TYPE_H

/*------------------------------------------------------------------------------
TYPE DEFINITION
------------------------------------------------------------------------------*/
#ifdef atpublic
#undef atpublic
#endif

#ifdef atfriend
#undef atfriend
#endif

#ifdef atprivate
#undef atprivate
#endif

#define atpublic
#define atfriend
#define atprivate static

typedef unsigned long long  uint64;
typedef unsigned int        uint32;
typedef unsigned short      uint16;
typedef unsigned char       uint8;
typedef signed long long    int64;
typedef signed int          int32;
typedef signed short        int16;
typedef signed char         int8;
typedef size_t              AtSize;

typedef unsigned char atbool;
typedef unsigned char eBool;

/*------------------------- Boolean type -------------------------------------*/
#define cAtFalse 0
#define cAtTrue  1

#ifndef NULL
#define    NULL            ((void*)0)
#endif

#define    null            ((void*)0)

/*------------------------------------------------------------------------------
CONST DEFINITION
------------------------------------------------------------------------------*/
/* Bit mask definitions */
#define cBit0          0x00000001UL
#define cBit1          0x00000002UL
#define cBit2          0x00000004UL
#define cBit3          0x00000008UL
#define cBit4          0x00000010UL
#define cBit5          0x00000020UL
#define cBit6          0x00000040UL
#define cBit7          0x00000080UL
#define cBit8          0x00000100UL
#define cBit9          0x00000200UL
#define cBit10         0x00000400UL
#define cBit11         0x00000800UL
#define cBit12         0x00001000UL
#define cBit13         0x00002000UL
#define cBit14         0x00004000UL
#define cBit15         0x00008000UL
#define cBit16         0x00010000UL
#define cBit17         0x00020000UL
#define cBit18         0x00040000UL
#define cBit19         0x00080000UL
#define cBit20         0x00100000UL
#define cBit21         0x00200000UL
#define cBit22         0x00400000UL
#define cBit23         0x00800000UL
#define cBit24         0x01000000UL
#define cBit25         0x02000000UL
#define cBit26         0x04000000UL
#define cBit27         0x08000000UL
#define cBit28         0x10000000UL
#define cBit29         0x20000000UL
#define cBit30         0x40000000UL
#define cBit31         0x80000000UL
#define cBit0_0        0x00000001UL
#define cBit1_0        0x00000003UL
#define cBit2_0        0x00000007UL
#define cBit3_0        0x0000000FUL
#define cBit4_0        0x0000001FUL
#define cBit5_0        0x0000003FUL
#define cBit6_0        0x0000007FUL
#define cBit7_0        0x000000FFUL
#define cBit8_0        0x000001FFUL
#define cBit9_0        0x000003FFUL
#define cBit10_0       0x000007FFUL
#define cBit11_0       0x00000FFFUL
#define cBit12_0       0x00001FFFUL
#define cBit13_0       0x00003FFFUL
#define cBit14_0       0x00007FFFUL
#define cBit15_0       0x0000FFFFUL
#define cBit16_0       0x0001FFFFUL
#define cBit17_0       0x0003FFFFUL
#define cBit18_0       0x0007FFFFUL
#define cBit19_0       0x000FFFFFUL
#define cBit20_0       0x001FFFFFUL
#define cBit21_0       0x003FFFFFUL
#define cBit22_0       0x007FFFFFUL
#define cBit23_0       0x00FFFFFFUL
#define cBit24_0       0x01FFFFFFUL
#define cBit25_0       0x03FFFFFFUL
#define cBit26_0       0x07FFFFFFUL
#define cBit27_0       0x0FFFFFFFUL
#define cBit28_0       0x1FFFFFFFUL
#define cBit29_0       0x3FFFFFFFUL
#define cBit30_0       0x7FFFFFFFUL
#define cBit31_0       0xFFFFFFFFUL
#define cBit1_1        0x00000002UL
#define cBit2_1        0x00000006UL
#define cBit3_1        0x0000000EUL
#define cBit4_1        0x0000001EUL
#define cBit5_1        0x0000003EUL
#define cBit6_1        0x0000007EUL
#define cBit7_1        0x000000FEUL
#define cBit8_1        0x000001FEUL
#define cBit9_1        0x000003FEUL
#define cBit10_1       0x000007FEUL
#define cBit11_1       0x00000FFEUL
#define cBit12_1       0x00001FFEUL
#define cBit13_1       0x00003FFEUL
#define cBit14_1       0x00007FFEUL
#define cBit15_1       0x0000FFFEUL
#define cBit16_1       0x0001FFFEUL
#define cBit17_1       0x0003FFFEUL
#define cBit18_1       0x0007FFFEUL
#define cBit19_1       0x000FFFFEUL
#define cBit20_1       0x001FFFFEUL
#define cBit21_1       0x003FFFFEUL
#define cBit22_1       0x007FFFFEUL
#define cBit23_1       0x00FFFFFEUL
#define cBit24_1       0x01FFFFFEUL
#define cBit25_1       0x03FFFFFEUL
#define cBit26_1       0x07FFFFFEUL
#define cBit27_1       0x0FFFFFFEUL
#define cBit28_1       0x1FFFFFFEUL
#define cBit29_1       0x3FFFFFFEUL
#define cBit30_1       0x7FFFFFFEUL
#define cBit31_1       0xFFFFFFFEUL
#define cBit2_2        0x00000004UL
#define cBit3_2        0x0000000CUL
#define cBit4_2        0x0000001CUL
#define cBit5_2        0x0000003CUL
#define cBit6_2        0x0000007CUL
#define cBit7_2        0x000000FCUL
#define cBit8_2        0x000001FCUL
#define cBit9_2        0x000003FCUL
#define cBit10_2       0x000007FCUL
#define cBit11_2       0x00000FFCUL
#define cBit12_2       0x00001FFCUL
#define cBit13_2       0x00003FFCUL
#define cBit14_2       0x00007FFCUL
#define cBit15_2       0x0000FFFCUL
#define cBit16_2       0x0001FFFCUL
#define cBit17_2       0x0003FFFCUL
#define cBit18_2       0x0007FFFCUL
#define cBit19_2       0x000FFFFCUL
#define cBit20_2       0x001FFFFCUL
#define cBit21_2       0x003FFFFCUL
#define cBit22_2       0x007FFFFCUL
#define cBit23_2       0x00FFFFFCUL
#define cBit24_2       0x01FFFFFCUL
#define cBit25_2       0x03FFFFFCUL
#define cBit26_2       0x07FFFFFCUL
#define cBit27_2       0x0FFFFFFCUL
#define cBit28_2       0x1FFFFFFCUL
#define cBit29_2       0x3FFFFFFCUL
#define cBit30_2       0x7FFFFFFCUL
#define cBit31_2       0xFFFFFFFCUL
#define cBit3_3        0x00000008UL
#define cBit4_3        0x00000018UL
#define cBit5_3        0x00000038UL
#define cBit6_3        0x00000078UL
#define cBit7_3        0x000000F8UL
#define cBit8_3        0x000001F8UL
#define cBit9_3        0x000003F8UL
#define cBit10_3       0x000007F8UL
#define cBit11_3       0x00000FF8UL
#define cBit12_3       0x00001FF8UL
#define cBit13_3       0x00003FF8UL
#define cBit14_3       0x00007FF8UL
#define cBit15_3       0x0000FFF8UL
#define cBit16_3       0x0001FFF8UL
#define cBit17_3       0x0003FFF8UL
#define cBit18_3       0x0007FFF8UL
#define cBit19_3       0x000FFFF8UL
#define cBit20_3       0x001FFFF8UL
#define cBit21_3       0x003FFFF8UL
#define cBit22_3       0x007FFFF8UL
#define cBit23_3       0x00FFFFF8UL
#define cBit24_3       0x01FFFFF8UL
#define cBit25_3       0x03FFFFF8UL
#define cBit26_3       0x07FFFFF8UL
#define cBit27_3       0x0FFFFFF8UL
#define cBit28_3       0x1FFFFFF8UL
#define cBit29_3       0x3FFFFFF8UL
#define cBit30_3       0x7FFFFFF8UL
#define cBit31_3       0xFFFFFFF8UL
#define cBit4_4        0x00000010UL
#define cBit5_4        0x00000030UL
#define cBit6_4        0x00000070UL
#define cBit7_4        0x000000F0UL
#define cBit8_4        0x000001F0UL
#define cBit9_4        0x000003F0UL
#define cBit10_4       0x000007F0UL
#define cBit11_4       0x00000FF0UL
#define cBit12_4       0x00001FF0UL
#define cBit13_4       0x00003FF0UL
#define cBit14_4       0x00007FF0UL
#define cBit15_4       0x0000FFF0UL
#define cBit16_4       0x0001FFF0UL
#define cBit17_4       0x0003FFF0UL
#define cBit18_4       0x0007FFF0UL
#define cBit19_4       0x000FFFF0UL
#define cBit20_4       0x001FFFF0UL
#define cBit21_4       0x003FFFF0UL
#define cBit22_4       0x007FFFF0UL
#define cBit23_4       0x00FFFFF0UL
#define cBit24_4       0x01FFFFF0UL
#define cBit25_4       0x03FFFFF0UL
#define cBit26_4       0x07FFFFF0UL
#define cBit27_4       0x0FFFFFF0UL
#define cBit28_4       0x1FFFFFF0UL
#define cBit29_4       0x3FFFFFF0UL
#define cBit30_4       0x7FFFFFF0UL
#define cBit31_4       0xFFFFFFF0UL
#define cBit5_5        0x00000020UL
#define cBit6_5        0x00000060UL
#define cBit7_5        0x000000E0UL
#define cBit8_5        0x000001E0UL
#define cBit9_5        0x000003E0UL
#define cBit10_5       0x000007E0UL
#define cBit11_5       0x00000FE0UL
#define cBit12_5       0x00001FE0UL
#define cBit13_5       0x00003FE0UL
#define cBit14_5       0x00007FE0UL
#define cBit15_5       0x0000FFE0UL
#define cBit16_5       0x0001FFE0UL
#define cBit17_5       0x0003FFE0UL
#define cBit18_5       0x0007FFE0UL
#define cBit19_5       0x000FFFE0UL
#define cBit20_5       0x001FFFE0UL
#define cBit21_5       0x003FFFE0UL
#define cBit22_5       0x007FFFE0UL
#define cBit23_5       0x00FFFFE0UL
#define cBit24_5       0x01FFFFE0UL
#define cBit25_5       0x03FFFFE0UL
#define cBit26_5       0x07FFFFE0UL
#define cBit27_5       0x0FFFFFE0UL
#define cBit28_5       0x1FFFFFE0UL
#define cBit29_5       0x3FFFFFE0UL
#define cBit30_5       0x7FFFFFE0UL
#define cBit31_5       0xFFFFFFE0UL
#define cBit6_6        0x00000040UL
#define cBit7_6        0x000000C0UL
#define cBit8_6        0x000001C0UL
#define cBit9_6        0x000003C0UL
#define cBit10_6       0x000007C0UL
#define cBit11_6       0x00000FC0UL
#define cBit12_6       0x00001FC0UL
#define cBit13_6       0x00003FC0UL
#define cBit14_6       0x00007FC0UL
#define cBit15_6       0x0000FFC0UL
#define cBit16_6       0x0001FFC0UL
#define cBit17_6       0x0003FFC0UL
#define cBit18_6       0x0007FFC0UL
#define cBit19_6       0x000FFFC0UL
#define cBit20_6       0x001FFFC0UL
#define cBit21_6       0x003FFFC0UL
#define cBit22_6       0x007FFFC0UL
#define cBit23_6       0x00FFFFC0UL
#define cBit24_6       0x01FFFFC0UL
#define cBit25_6       0x03FFFFC0UL
#define cBit26_6       0x07FFFFC0UL
#define cBit27_6       0x0FFFFFC0UL
#define cBit28_6       0x1FFFFFC0UL
#define cBit29_6       0x3FFFFFC0UL
#define cBit30_6       0x7FFFFFC0UL
#define cBit31_6       0xFFFFFFC0UL
#define cBit7_7        0x00000080UL
#define cBit8_7        0x00000180UL
#define cBit9_7        0x00000380UL
#define cBit10_7       0x00000780UL
#define cBit11_7       0x00000F80UL
#define cBit12_7       0x00001F80UL
#define cBit13_7       0x00003F80UL
#define cBit14_7       0x00007F80UL
#define cBit15_7       0x0000FF80UL
#define cBit16_7       0x0001FF80UL
#define cBit17_7       0x0003FF80UL
#define cBit18_7       0x0007FF80UL
#define cBit19_7       0x000FFF80UL
#define cBit20_7       0x001FFF80UL
#define cBit21_7       0x003FFF80UL
#define cBit22_7       0x007FFF80UL
#define cBit23_7       0x00FFFF80UL
#define cBit24_7       0x01FFFF80UL
#define cBit25_7       0x03FFFF80UL
#define cBit26_7       0x07FFFF80UL
#define cBit27_7       0x0FFFFF80UL
#define cBit28_7       0x1FFFFF80UL
#define cBit29_7       0x3FFFFF80UL
#define cBit30_7       0x7FFFFF80UL
#define cBit31_7       0xFFFFFF80UL
#define cBit8_8        0x00000100UL
#define cBit9_8        0x00000300UL
#define cBit10_8       0x00000700UL
#define cBit11_8       0x00000F00UL
#define cBit12_8       0x00001F00UL
#define cBit13_8       0x00003F00UL
#define cBit14_8       0x00007F00UL
#define cBit15_8       0x0000FF00UL
#define cBit16_8       0x0001FF00UL
#define cBit17_8       0x0003FF00UL
#define cBit18_8       0x0007FF00UL
#define cBit19_8       0x000FFF00UL
#define cBit20_8       0x001FFF00UL
#define cBit21_8       0x003FFF00UL
#define cBit22_8       0x007FFF00UL
#define cBit23_8       0x00FFFF00UL
#define cBit24_8       0x01FFFF00UL
#define cBit25_8       0x03FFFF00UL
#define cBit26_8       0x07FFFF00UL
#define cBit27_8       0x0FFFFF00UL
#define cBit28_8       0x1FFFFF00UL
#define cBit29_8       0x3FFFFF00UL
#define cBit30_8       0x7FFFFF00UL
#define cBit31_8       0xFFFFFF00UL
#define cBit9_9        0x00000200UL
#define cBit10_9       0x00000600UL
#define cBit11_9       0x00000E00UL
#define cBit12_9       0x00001E00UL
#define cBit13_9       0x00003E00UL
#define cBit14_9       0x00007E00UL
#define cBit15_9       0x0000FE00UL
#define cBit16_9       0x0001FE00UL
#define cBit17_9       0x0003FE00UL
#define cBit18_9       0x0007FE00UL
#define cBit19_9       0x000FFE00UL
#define cBit20_9       0x001FFE00UL
#define cBit21_9       0x003FFE00UL
#define cBit22_9       0x007FFE00UL
#define cBit23_9       0x00FFFE00UL
#define cBit24_9       0x01FFFE00UL
#define cBit25_9       0x03FFFE00UL
#define cBit26_9       0x07FFFE00UL
#define cBit27_9       0x0FFFFE00UL
#define cBit28_9       0x1FFFFE00UL
#define cBit29_9       0x3FFFFE00UL
#define cBit30_9       0x7FFFFE00UL
#define cBit31_9       0xFFFFFE00UL
#define cBit10_10      0x00000400UL
#define cBit11_10      0x00000C00UL
#define cBit12_10      0x00001C00UL
#define cBit13_10      0x00003C00UL
#define cBit14_10      0x00007C00UL
#define cBit15_10      0x0000FC00UL
#define cBit16_10      0x0001FC00UL
#define cBit17_10      0x0003FC00UL
#define cBit18_10      0x0007FC00UL
#define cBit19_10      0x000FFC00UL
#define cBit20_10      0x001FFC00UL
#define cBit21_10      0x003FFC00UL
#define cBit22_10      0x007FFC00UL
#define cBit23_10      0x00FFFC00UL
#define cBit24_10      0x01FFFC00UL
#define cBit25_10      0x03FFFC00UL
#define cBit26_10      0x07FFFC00UL
#define cBit27_10      0x0FFFFC00UL
#define cBit28_10      0x1FFFFC00UL
#define cBit29_10      0x3FFFFC00UL
#define cBit30_10      0x7FFFFC00UL
#define cBit31_10      0xFFFFFC00UL
#define cBit11_11      0x00000800UL
#define cBit12_11      0x00001800UL
#define cBit13_11      0x00003800UL
#define cBit14_11      0x00007800UL
#define cBit15_11      0x0000F800UL
#define cBit16_11      0x0001F800UL
#define cBit17_11      0x0003F800UL
#define cBit18_11      0x0007F800UL
#define cBit19_11      0x000FF800UL
#define cBit20_11      0x001FF800UL
#define cBit21_11      0x003FF800UL
#define cBit22_11      0x007FF800UL
#define cBit23_11      0x00FFF800UL
#define cBit24_11      0x01FFF800UL
#define cBit25_11      0x03FFF800UL
#define cBit26_11      0x07FFF800UL
#define cBit27_11      0x0FFFF800UL
#define cBit28_11      0x1FFFF800UL
#define cBit29_11      0x3FFFF800UL
#define cBit30_11      0x7FFFF800UL
#define cBit31_11      0xFFFFF800UL
#define cBit12_12      0x00001000UL
#define cBit13_12      0x00003000UL
#define cBit14_12      0x00007000UL
#define cBit15_12      0x0000F000UL
#define cBit16_12      0x0001F000UL
#define cBit17_12      0x0003F000UL
#define cBit18_12      0x0007F000UL
#define cBit19_12      0x000FF000UL
#define cBit20_12      0x001FF000UL
#define cBit21_12      0x003FF000UL
#define cBit22_12      0x007FF000UL
#define cBit23_12      0x00FFF000UL
#define cBit24_12      0x01FFF000UL
#define cBit25_12      0x03FFF000UL
#define cBit26_12      0x07FFF000UL
#define cBit27_12      0x0FFFF000UL
#define cBit28_12      0x1FFFF000UL
#define cBit29_12      0x3FFFF000UL
#define cBit30_12      0x7FFFF000UL
#define cBit31_12      0xFFFFF000UL
#define cBit13_13      0x00002000UL
#define cBit14_13      0x00006000UL
#define cBit15_13      0x0000E000UL
#define cBit16_13      0x0001E000UL
#define cBit17_13      0x0003E000UL
#define cBit18_13      0x0007E000UL
#define cBit19_13      0x000FE000UL
#define cBit20_13      0x001FE000UL
#define cBit21_13      0x003FE000UL
#define cBit22_13      0x007FE000UL
#define cBit23_13      0x00FFE000UL
#define cBit24_13      0x01FFE000UL
#define cBit25_13      0x03FFE000UL
#define cBit26_13      0x07FFE000UL
#define cBit27_13      0x0FFFE000UL
#define cBit28_13      0x1FFFE000UL
#define cBit29_13      0x3FFFE000UL
#define cBit30_13      0x7FFFE000UL
#define cBit31_13      0xFFFFE000UL
#define cBit14_14      0x00004000UL
#define cBit15_14      0x0000C000UL
#define cBit16_14      0x0001C000UL
#define cBit17_14      0x0003C000UL
#define cBit18_14      0x0007C000UL
#define cBit19_14      0x000FC000UL
#define cBit20_14      0x001FC000UL
#define cBit21_14      0x003FC000UL
#define cBit22_14      0x007FC000UL
#define cBit23_14      0x00FFC000UL
#define cBit24_14      0x01FFC000UL
#define cBit25_14      0x03FFC000UL
#define cBit26_14      0x07FFC000UL
#define cBit27_14      0x0FFFC000UL
#define cBit28_14      0x1FFFC000UL
#define cBit29_14      0x3FFFC000UL
#define cBit30_14      0x7FFFC000UL
#define cBit31_14      0xFFFFC000UL
#define cBit15_15      0x00008000UL
#define cBit16_15      0x00018000UL
#define cBit17_15      0x00038000UL
#define cBit18_15      0x00078000UL
#define cBit19_15      0x000F8000UL
#define cBit20_15      0x001F8000UL
#define cBit21_15      0x003F8000UL
#define cBit22_15      0x007F8000UL
#define cBit23_15      0x00FF8000UL
#define cBit24_15      0x01FF8000UL
#define cBit25_15      0x03FF8000UL
#define cBit26_15      0x07FF8000UL
#define cBit27_15      0x0FFF8000UL
#define cBit28_15      0x1FFF8000UL
#define cBit29_15      0x3FFF8000UL
#define cBit30_15      0x7FFF8000UL
#define cBit31_15      0xFFFF8000UL
#define cBit16_16      0x00010000UL
#define cBit17_16      0x00030000UL
#define cBit18_16      0x00070000UL
#define cBit19_16      0x000F0000UL
#define cBit20_16      0x001F0000UL
#define cBit21_16      0x003F0000UL
#define cBit22_16      0x007F0000UL
#define cBit23_16      0x00FF0000UL
#define cBit24_16      0x01FF0000UL
#define cBit25_16      0x03FF0000UL
#define cBit26_16      0x07FF0000UL
#define cBit27_16      0x0FFF0000UL
#define cBit28_16      0x1FFF0000UL
#define cBit29_16      0x3FFF0000UL
#define cBit30_16      0x7FFF0000UL
#define cBit31_16      0xFFFF0000UL
#define cBit17_17      0x00020000UL
#define cBit18_17      0x00060000UL
#define cBit19_17      0x000E0000UL
#define cBit20_17      0x001E0000UL
#define cBit21_17      0x003E0000UL
#define cBit22_17      0x007E0000UL
#define cBit23_17      0x00FE0000UL
#define cBit24_17      0x01FE0000UL
#define cBit25_17      0x03FE0000UL
#define cBit26_17      0x07FE0000UL
#define cBit27_17      0x0FFE0000UL
#define cBit28_17      0x1FFE0000UL
#define cBit29_17      0x3FFE0000UL
#define cBit30_17      0x7FFE0000UL
#define cBit31_17      0xFFFE0000UL
#define cBit18_18      0x00040000UL
#define cBit19_18      0x000C0000UL
#define cBit20_18      0x001C0000UL
#define cBit21_18      0x003C0000UL
#define cBit22_18      0x007C0000UL
#define cBit23_18      0x00FC0000UL
#define cBit24_18      0x01FC0000UL
#define cBit25_18      0x03FC0000UL
#define cBit26_18      0x07FC0000UL
#define cBit27_18      0x0FFC0000UL
#define cBit28_18      0x1FFC0000UL
#define cBit29_18      0x3FFC0000UL
#define cBit30_18      0x7FFC0000UL
#define cBit31_18      0xFFFC0000UL
#define cBit19_19      0x00080000UL
#define cBit20_19      0x00180000UL
#define cBit21_19      0x00380000UL
#define cBit22_19      0x00780000UL
#define cBit23_19      0x00F80000UL
#define cBit24_19      0x01F80000UL
#define cBit25_19      0x03F80000UL
#define cBit26_19      0x07F80000UL
#define cBit27_19      0x0FF80000UL
#define cBit28_19      0x1FF80000UL
#define cBit29_19      0x3FF80000UL
#define cBit30_19      0x7FF80000UL
#define cBit31_19      0xFFF80000UL
#define cBit20_20      0x00100000UL
#define cBit21_20      0x00300000UL
#define cBit22_20      0x00700000UL
#define cBit23_20      0x00F00000UL
#define cBit24_20      0x01F00000UL
#define cBit25_20      0x03F00000UL
#define cBit26_20      0x07F00000UL
#define cBit27_20      0x0FF00000UL
#define cBit28_20      0x1FF00000UL
#define cBit29_20      0x3FF00000UL
#define cBit30_20      0x7FF00000UL
#define cBit31_20      0xFFF00000UL
#define cBit21_21      0x00200000UL
#define cBit22_21      0x00600000UL
#define cBit23_21      0x00E00000UL
#define cBit24_21      0x01E00000UL
#define cBit25_21      0x03E00000UL
#define cBit26_21      0x07E00000UL
#define cBit27_21      0x0FE00000UL
#define cBit28_21      0x1FE00000UL
#define cBit29_21      0x3FE00000UL
#define cBit30_21      0x7FE00000UL
#define cBit31_21      0xFFE00000UL
#define cBit22_22      0x00400000UL
#define cBit23_22      0x00C00000UL
#define cBit24_22      0x01C00000UL
#define cBit25_22      0x03C00000UL
#define cBit26_22      0x07C00000UL
#define cBit27_22      0x0FC00000UL
#define cBit28_22      0x1FC00000UL
#define cBit29_22      0x3FC00000UL
#define cBit30_22      0x7FC00000UL
#define cBit31_22      0xFFC00000UL
#define cBit23_23      0x00800000UL
#define cBit24_23      0x01800000UL
#define cBit25_23      0x03800000UL
#define cBit26_23      0x07800000UL
#define cBit27_23      0x0F800000UL
#define cBit28_23      0x1F800000UL
#define cBit29_23      0x3F800000UL
#define cBit30_23      0x7F800000UL
#define cBit31_23      0xFF800000UL
#define cBit24_24      0x01000000UL
#define cBit25_24      0x03000000UL
#define cBit26_24      0x07000000UL
#define cBit27_24      0x0F000000UL
#define cBit28_24      0x1F000000UL
#define cBit29_24      0x3F000000UL
#define cBit30_24      0x7F000000UL
#define cBit31_24      0xFF000000UL
#define cBit25_25      0x02000000UL
#define cBit26_25      0x06000000UL
#define cBit27_25      0x0E000000UL
#define cBit28_25      0x1E000000UL
#define cBit29_25      0x3E000000UL
#define cBit30_25      0x7E000000UL
#define cBit31_25      0xFE000000UL
#define cBit26_26      0x04000000UL
#define cBit27_26      0x0C000000UL
#define cBit28_26      0x1C000000UL
#define cBit29_26      0x3C000000UL
#define cBit30_26      0x7C000000UL
#define cBit31_26      0xFC000000UL
#define cBit27_27      0x08000000UL
#define cBit28_27      0x18000000UL
#define cBit29_27      0x38000000UL
#define cBit30_27      0x78000000UL
#define cBit31_27      0xF8000000UL
#define cBit28_28      0x10000000UL
#define cBit29_28      0x30000000UL
#define cBit30_28      0x70000000UL
#define cBit31_28      0xF0000000UL
#define cBit29_29      0x20000000UL
#define cBit30_29      0x60000000UL
#define cBit31_29      0xE0000000UL
#define cBit30_30      0x40000000UL
#define cBit31_30      0xC0000000UL
#define cBit31_31      0x80000000UL

/*---------------------------- Macro -----------------------------------------*/
#define cInvalidUint32 0xFFFFFFFF
#define cInvalidUint16 0xFFFF
#define cInvalidUint8  0xFF

/*------------------------------------------------------------------------------
Prototype   : mOffset(type, member)

Purpose     : This macro is used to
------------------------------------------------------------------------------*/
#define mOffset(type, member) ((dword) &(((type*)0)->member))

/*------------------------------------------------------------------------------
Prototype   : mCount(x)

Purpose     : This macro is used to number member in array.
------------------------------------------------------------------------------*/
#define mCount(x)    (uint32)(sizeof(x) / sizeof(x[0]))

/*------------------------------------------------------------------------------
Prototype   : mSwap(a, b)

Purpose     : This macro is used to swap a and b
------------------------------------------------------------------------------*/
#define mSwap(a, b)  (a ^= b, b ^= a, a ^= b)

/*------------------------------------------------------------------------------
Prototype   : mMin(a,b)

Purpose     : This macro is used to return minimum between a and b
------------------------------------------------------------------------------*/
#define mMin(a,b) (a > b ? b : a)

/*------------------------------------------------------------------------------
Prototype   : mMax(a,b)

Purpose     : This macro is used to return maximum between a and b
------------------------------------------------------------------------------*/
#define mMax(a,b) (a > b ? a : b)

/*------------------------------------------------------------------------------
Prototype     : mOutOfRange(val, min, max)

Purpose     : Check out of range of the value.
------------------------------------------------------------------------------*/
#define mOutOfRange(val, min, max) AtOutOfRange((uint32)val, (uint32)min, (uint32)max)
#define mInRange(val, min, max)    (!AtOutOfRange((uint32)val, (uint32)min, (uint32)max))

#ifdef VXWORKS
#define AtAttributePrintf(formatIndex, firstToCheck) 
#define AtAttributeNoReturn 
#define AtFunction       __FUNCTION__
#else
#define AtAttributePrintf(formatIndex, firstToCheck) __attribute__((format(printf, formatIndex, firstToCheck)))
#define AtAttributeNoReturn __attribute__((__noreturn__))
#define AtFunction       __func__
#endif

#define AtFileLocation   __FILE__
#define AtLineNumber     __LINE__
#define AtSourceLocation AtFileLocation, AtLineNumber
#define AtSourceLocationNone NULL, 0

#define AtUnused(x)	(void)(x)

/* Cast pointer x to any type T. */
#define AtCast(T, x) ((T)((void*)x))

#ifdef  __cplusplus
}
#endif
#endif    /* _ATTYPES_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : AT Device
 * 
 * File        : AtChannel.h
 * 
 * Created Date: Aug 4, 2012
 *
 * Description : AT channel abstract class
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATCHANNEL_H_
#define _ATCHANNEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtOsal.h"
#include "AtCommon.h"
#include "AtChannelClasses.h"
#include "AtModule.h"
#include "AtPrbsEngine.h"
#include "AtSurEngine.h"
#include "AtModuleConcate.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
/**
 * @addtogroup AtChannel
 * @{
 */
/** @brief Default listener interface */
typedef struct tAtChannelEventListener
    {
    void (*AlarmChangeState)(AtChannel channel, uint32 changedAlarms, uint32 currentStatus); /**< Called when Alarm change state */
    void (*AlarmChangeStateWithUserData)(AtChannel channel, uint32 changedAlarms, uint32 currentStatus, void *userData); /**< Called when Alarm change state */
    void (*OamReceived)(AtChannel channel, uint8 *data, uint32 length); /**< Called when OAM come  */
    void (*WillDelete)(AtChannel channel, void *userData); /**< Called before deleting channel */
    }tAtChannelEventListener;

/** @} */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Access module and device */
AtModule AtChannelModuleGet(AtChannel self);
AtDevice AtChannelDeviceGet(AtChannel self);
uint32 AtChannelIdGet(AtChannel self);
eAtRet AtChannelInit(AtChannel self);

/* Enable/disable both RX/TX */
eAtRet AtChannelEnable(AtChannel self, eBool enable);
eBool AtChannelIsEnabled(AtChannel self);
eBool AtChannelCanEnable(AtChannel self, eBool enable);

/* Enable/disable each direction */
eAtRet AtChannelTxEnable(AtChannel self, eBool enable);
eBool AtChannelTxIsEnabled(AtChannel self);
eAtRet AtChannelRxEnable(AtChannel self, eBool enable);
eBool AtChannelRxIsEnabled(AtChannel self);

/* Timing */
eBool AtChannelTimingModeIsSupported(AtChannel self, eAtTimingMode timingMode);
eAtRet AtChannelTimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource);
eAtTimingMode AtChannelTimingModeGet(AtChannel self);
AtChannel AtChannelTimingSourceGet(AtChannel self);
eAtClockState AtChannelClockStateGet(AtChannel self);

/* Defect */
uint32 AtChannelDefectGet(AtChannel self);
uint32 AtChannelDefectInterruptGet(AtChannel self);
uint32 AtChannelDefectInterruptClear(AtChannel self);
uint32 AtChannelSpecificDefectInterruptClear(AtChannel self, uint32 defectTypes);

/* Alarm */
uint32 AtChannelAlarmGet(AtChannel self);
uint32 AtChannelAlarmInterruptGet(AtChannel self);
uint32 AtChannelAlarmInterruptClear(AtChannel self);
eBool AtChannelAlarmIsSupported(AtChannel self, uint32 alarmType);

/* Interrupt mask */
eAtRet AtChannelInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask);
uint32 AtChannelInterruptMaskGet(AtChannel self);
eBool AtChannelInterruptMaskIsSupported(AtChannel self, uint32 defectMask);

/* Interrupt enable */
eAtRet AtChannelInterruptEnable(AtChannel self);
eAtRet AtChannelInterruptDisable(AtChannel self);
eBool AtChannelInterruptIsEnabled(AtChannel self);

/* TX Alarm forcing */
eAtRet AtChannelTxAlarmForce(AtChannel self, uint32 alarmType);
eAtRet AtChannelTxAlarmUnForce(AtChannel self, uint32 alarmType);
uint32 AtChannelTxForcedAlarmGet(AtChannel self);
uint32 AtChannelTxForcableAlarmsGet(AtChannel self);

/* RX Alarm forcing */
eAtRet AtChannelRxAlarmForce(AtChannel self, uint32 alarmType);
eAtRet AtChannelRxAlarmUnForce(AtChannel self, uint32 alarmType);
uint32 AtChannelRxForcedAlarmGet(AtChannel self);
uint32 AtChannelRxForcableAlarmsGet(AtChannel self);

/* Error forcing */
eAtRet AtChannelTxErrorForce(AtChannel self, uint32 errorType);
eAtRet AtChannelTxErrorUnForce(AtChannel self, uint32 errorType);
uint32 AtChannelTxForcedErrorGet(AtChannel self);
uint32 AtChannelTxForcableErrorsGet(AtChannel self);

/* Loopback */
eAtRet AtChannelLoopbackSet(AtChannel self, uint8 loopbackMode);
uint8 AtChannelLoopbackGet(AtChannel self);
eBool AtChannelLoopbackIsSupported(AtChannel self, uint8 loopbackMode);
const char *AtChannelLoopbackMode2String(AtChannel self, uint8 loopbackMode);

/* Atomic counters. Products that sync counters every predefined of time
 * (1s, for example) need to latch all counters before accessing individual
 * counters. Most of products do not require counter latching and calling
 * latching API just does nothing */
uint32 AtChannelCounterGet(AtChannel self, uint16 counterType);
uint32 AtChannelCounterClear(AtChannel self, uint16 counterType);
eAtRet AtChannelAllCountersLatchAndClear(AtChannel self, eBool clear);
eBool AtChannelCounterIsSupported(AtChannel self, uint16 counterType);

/* Get all of counters */
eAtRet AtChannelAllCountersGet(AtChannel self, void *counters);
eAtRet AtChannelAllCountersClear(AtChannel self, void *counters);

/* All configuration */
eAtRet AtChannelAllConfigSet(AtChannel self, void* pAllConfig);
eAtRet AtChannelAllConfigGet(AtChannel self, void* pAllConfig);

/* Event listener */
eAtRet AtChannelEventListenerAdd(AtChannel self, tAtChannelEventListener *listener);
eAtRet AtChannelEventListenerAddWithUserData(AtChannel self, tAtChannelEventListener *listener, void *userData);
eAtRet AtChannelEventListenerRemove(AtChannel self, tAtChannelEventListener *listener);

/* Binding information */
/* Associated bound channels */
AtEncapChannel AtChannelBoundEncapChannelGet(AtChannel self);
AtPw AtChannelBoundPwGet(AtChannel self);

/* Surveillance engine */
AtSurEngine AtChannelSurEngineGet(AtChannel self);

/* Get ID string */
const char *AtChannelIdString(AtChannel self);
const char *AtChannelTypeString(AtChannel self);

/* Working with VCG */
AtVcgBinder AtChannelVcgBinder(AtChannel self);
AtConcateGroup AtChannelSourceGroupGet(AtChannel self);
AtConcateGroup AtChannelSinkGroupGet(AtChannel self);

/* Debug */
void AtChannelDebug(AtChannel self);
void AtChannelStatusClear(AtChannel self);
AtPrbsEngine AtChannelPrbsEngineGet(AtChannel self);

/* Message logging */
eAtRet AtChannelLogEnable(AtChannel self, eBool enable);
eBool AtChannelLogIsEnabled(AtChannel self);
void AtChannelLogShow(AtChannel self);

/* Defect database emulation. */
uint32 AtChannelListenedDefectGet(AtChannel self, uint32 * listenedCount);
uint32 AtChannelListenedDefectClear(AtChannel self, uint32 * listenedCount);

/* Test */
AtObjectAny AtChannelAttController(AtChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _ATCHANNEL_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : AtObject.h
 * 
 * Created Date: Aug 21, 2012
 *
 * Description : Root object
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATOBJECT_H_
#define _ATOBJECT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "attypes.h"
#include "AtRet.h"
#include "AtClasses.h"
#include "AtCoder.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
/* Access methods */
#define mMethodsGet(object) (object)->methods
#define mMethodsSet(object, methods) mMethodsGet(object) = methods
#define mMethodOverride(methods, methodName) (methods).methodName = methodName

/*--------------------------- Typedefs ---------------------------------------*/
/** @brief Property listener */
typedef struct tAtObjectPropertyListener
    {
    void (*PropertyWillChange)(AtObject self, uint32 propertyId, AtSize value, void *userData); /**< Call before a properly is changed */
    void (*PropertyDidChange)(AtObject self, uint32 propertyId, AtSize value, void *userData);  /**< Call after a properly is changed successfully */
    void (*ListenerWillRemove)(AtObject self, void *userData); /**< Call before listener is removed */
    }tAtObjectPropertyListener;

/** @brief Methods */
typedef struct tAtObjectMethods
    {
    void (*WillDelete)(AtObject self);       /**< Will Delete Object */
    void (*Delete)(AtObject self);           /**< Delete object */

    const char *(*ToString)(AtObject self);  /**< Get object description */
    AtObject (*Clone)(AtObject self);        /**< To copy an object */

    const char *(*DescriptionBuild)(AtObject self, char *buffer, uint32 bufferSize); /**< Build object description */
    void (*Serialize)(AtObject self, AtCoder encoder);   /**< Serialize */
    void (*Deserialize)(AtObject self, AtCoder decoder); /**< Deserialize */

    /* Property listeners */
    AtList *(*PropertyListenerListAddress)(AtObject self); /**< Address to allocate property listener */
    }tAtObjectMethods;

/** @brief Representation */
typedef struct tAtObject
    {
    const tAtObjectMethods *methods; /**< Methods */
    }tAtObject;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtObject AtObjectInit(AtObject self); /* Constructor */
void AtObjectDelete(AtObject self);
const char *AtObjectToString(AtObject self);
AtObject AtObjectClone(AtObject self);

void AtObjectSerialize(AtObject self, AtCoder encoder);
void AtObjectDeserialize(AtObject self, AtCoder decoder);

/* Property listeners */
eAtRet AtObjectPropertyListenerAdd(AtObject self, const tAtObjectPropertyListener *listener, void *userData);
eAtRet AtObjectPropertyListenerRemove(AtObject self, const tAtObjectPropertyListener *listener, void *userData);
uint32 AtObjectNumPropertyListenersGet(AtObject self);
const tAtObjectPropertyListener *AtObjectPropertyListenerAtIndex(AtObject self, uint32 listenerIndex, void **pUserData);

#ifdef __cplusplus
}
#endif
#endif /* _ATOBJECT_H_ */


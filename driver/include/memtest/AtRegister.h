/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : AtRegister.h
 * 
 * Created Date: Jan 6, 2013
 *
 * Description : To abstract an register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATREGISTER_H_
#define _ATREGISTER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtObject.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tAtRegister * AtRegister;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Create new register */
AtRegister AtRegisterNew(uint8 coreId, uint32 address, uint32 noneEditableFieldsMask);

/* Access register attributes */
uint8 AtRegisterCoreId(AtRegister self);
void AtRegisterCoreIdSet(AtRegister self, uint8 coreId);
uint32 AtRegisterAddress(AtRegister self);
void AtRegisterAddressSet(AtRegister self, uint32 address);
uint32 AtRegisterNoneEditableFieldsMask(AtRegister self);
void AtRegisterNoneEditableFieldsMaskSet(AtRegister self, uint32 noneEditableFieldsMask);

/* Read/write */
uint32 AtRegisterRead(AtRegister self, AtHal byHal);
uint32 AtRegisterWrite(AtRegister self, uint32 value, AtHal byHal);

#ifdef __cplusplus
}
#endif
#endif /* _ATREGISTER_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaPdhPwDeviceInternal.h
 * 
 * Created Date: May 6, 2013
 *
 * Description : PDH PW device
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWDEVICEINTERNAL_H_
#define _THAPDHPWDEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/man/ThaDevicePwInternal.h"

#include "ThaPdhPwDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tThaPdhPwDevice
    {
    tThaDevicePw super;

    /* Private data */
    }tThaPdhPwDevice;

typedef struct tThaPdhPwDeviceV2
    {
    tThaPdhPwDevice super;

    /* Private */
    }tThaPdhPwDeviceV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice ThaPdhPwDeviceObjectInit  (AtDevice self, AtDriver driver, uint32 productCode);
AtDevice ThaPdhPwDeviceV2ObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWDEVICEINTERNAL_H_ */


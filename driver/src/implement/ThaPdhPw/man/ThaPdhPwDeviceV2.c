/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : ThaPdhPwDeviceV2.c
 *
 * Created Date: Jan 18, 2013
 *
 * Description : PDH PW version 2
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "ThaPdhPwDeviceInternal.h"
#include "../../default/eth/ThaModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class */
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods m_AtDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleId_ = (uint32)moduleId;

    if (moduleId_ == cThaModuleCos)        return ThaModuleCosV2New(self);
    if (moduleId_ == cThaModulePwe)        return ThaModulePweV2New(self);
    if (moduleId_ == cThaModulePda)        return ThaModulePdaV2New(self);
    if (moduleId_ == cThaModuleCla)        return ThaModuleClaPwV2New(self);
    if (moduleId_ == cThaModuleBmt)        return ThaModuleBmtV2New(self);
    if (moduleId  == cAtModulePktAnalyzer) return (AtModule)ThaModulePktAnalyzerPwNew(self);
    if (moduleId  == cAtModuleEth)         return (AtModule)ThaModuleEthPwV2New(self);

    /* Let super create other modules */
    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static uint8 NumIpCoresGet(AtDevice self)
    {
	AtUnused(self);
    return 1;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, NumIpCoresGet);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaPdhPwDeviceV2);
    }

AtDevice ThaPdhPwDeviceV2ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Initialize its super */
    if (ThaPdhPwDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Override */
    Override(self);

    /* Only initialize method structures one time */
    m_methodsInit = 1;

    return self;
    }


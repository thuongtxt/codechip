/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : ThaPdhPwDevice.h
 * 
 * Created Date: May 6, 2013
 *
 * Description : Device of PW product
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THAPDHPWDEVICE_H_
#define _THAPDHPWDEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Concrete devices */
AtDevice ThaPdhPwDeviceV2New(AtDriver driver, uint32 productCode);

AtDevice Tha60030011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60031021DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60031021EpDeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60035011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60030111DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60071011DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60091023DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60070041DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60070051DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60000031DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60031071DeviceNew(AtDriver driver, uint32 productCode);

AtDevice Tha60220031DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha60220041DeviceNew(AtDriver driver, uint32 productCode);

AtDevice Tha60240021DeviceNew(AtDriver driver, uint32 productCode);
AtDevice Tha6A210021DeviceNew(AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THAPDHPWDEVICE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60070013ModuleEth.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : ETH Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60070013MODULEETH_H_
#define _THA60070013MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint8 Tha6007ModuleEthMaxPortsGet(AtModuleEth self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60070013MODULEETH_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha6007EthPortSerdesController.c
 *
 * Created Date: Aug 16, 2013
 *
 * Description : SERDES controller for ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/physical/ThaSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* Turn-off */
#define cThaRegGeTurnOff              0xf00041
#define cThaRegGeTurnOffMask(portId)  (cBit0 << (portId))
#define cThaRegGeTurnOffShift(portId) (portId)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6007EthPortSerdesController
    {
    tThaSerdesController super;

    /* Private data */
    }tTha6007EthPortSerdesController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods  m_AtSerdesControllerOverride;
static tThaSerdesControllerMethods m_ThaSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods  *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaSerdesController self)
    {
	AtUnused(self);
    return 0xf28000;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return ((mode == cAtSerdesModeGe) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeGe;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6007EthPortSerdesController);
    }

static eAtRet Init(AtSerdesController self)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtSerdesControllerMethods->Init(self);

    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamVod, 0x3f);
    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamPreEmphasisPreTap, 0x0);
    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamPreEmphasisFirstPostTap, 0x9);
    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamPreEmphasisSecondPostTap, 0x0);

    return ret;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    uint32 portId = AtSerdesControllerIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cThaRegGeTurnOff, cAtModuleEth);

    mFieldIns(&regVal, cThaRegGeTurnOffMask(portId), cThaRegGeTurnOffShift(portId), enable ? 0 : 1);
    AtSerdesControllerWrite(self, cThaRegGeTurnOff, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    uint32 portId = AtSerdesControllerIdGet(self);
    uint32 regVal = AtSerdesControllerRead(self, cThaRegGeTurnOff, cAtModuleEth);

    return (regVal & cThaRegGeTurnOffMask(portId)) ? cAtFalse : cAtTrue;
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideThaSerdesController(AtSerdesController self)
    {
    ThaSerdesController controller = (ThaSerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSerdesControllerOverride, mMethodsGet(controller), sizeof(m_ThaSerdesControllerOverride));

        mMethodOverride(m_ThaSerdesControllerOverride, BaseAddress);
        }

    mMethodsSet(controller, &m_ThaSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideThaSerdesController(self);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtEthPort ethPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSerdesControllerObjectInit(self, (AtChannel)ethPort, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha6007EthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, ethPort, serdesId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60070013De1CdrController.c
 *
 * Created Date: Aug 4, 2014
 *
 * Description : CDR controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cdr/controllers/ThaCdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaRegPdhLIUExt1Cfg  0x700313

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070013De1CdrController
    {
    tThaDe1CdrController super;
    }tTha60070013De1CdrController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaDe1CdrControllerMethods m_ThaDe1CdrControllerOverride;

/* Save super implementation */
static const tThaDe1CdrControllerMethods *m_ThaDe1CdrControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet LiuTimingModeSet(ThaDe1CdrController self, eAtTimingMode timingMode)
    {
    uint32 regVal;
    ThaCdrController controller = (ThaCdrController)self;
    uint32 de1Id = AtChannelIdGet(ThaCdrControllerChannelGet(controller));

    m_ThaDe1CdrControllerMethods->LiuTimingModeSet(self, timingMode);

    regVal = ThaCdrControllerRead(controller, cThaRegPdhLIUExt1Cfg, cAtModulePdh);
    mFieldIns(&regVal, (cBit0 << de1Id), de1Id, (timingMode == cAtTimingModeExt1Ref) ? 0x1 : 0x0);
    ThaCdrControllerWrite(controller, cThaRegPdhLIUExt1Cfg, regVal, cAtModulePdh);

    return cAtOk;
    }

static void OverrideThaDe1CdrController(ThaCdrController self)
    {
	ThaDe1CdrController controller = (ThaDe1CdrController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDe1CdrControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDe1CdrControllerOverride, m_ThaDe1CdrControllerMethods, sizeof(m_ThaDe1CdrControllerOverride));

        mMethodOverride(m_ThaDe1CdrControllerOverride, LiuTimingModeSet);
        }

    mMethodsSet(controller, &m_ThaDe1CdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
	OverrideThaDe1CdrController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070013De1CdrController);
    }

static ThaCdrController ObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaDe1CdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController Tha60070013De1CdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, engineId, channel);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60070013ModuleCdr.c
 *
 * Created Date: Aug 4, 2014
 *
 * Description : CDR module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/cdr/ThaModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070013ModuleCdr
    {
    tThaModuleCdr super;
    }tTha60070013ModuleCdr;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCdrMethods m_ThaModuleCdrOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaCdrController De1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
    AtChannel channel = (AtChannel)de1;
	AtUnused(self);
    return Tha60070013De1CdrControllerNew(AtChannelIdGet(channel), channel);
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, mMethodsGet(cdrModule), sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, De1CdrControllerCreate);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60070013ModuleCdr);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60070013ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

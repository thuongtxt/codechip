/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60070013IpCore.c
 *
 * Created Date: May 15, 2013
 *
 * Description : IP Core
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../../../default/man/ThaIpCoreInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60070013IpCore *Tha60070013IpCore;
typedef struct tTha60070013IpCore
    {
    tThaIpCore super;
    }tTha60070013IpCore;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
/* Implementation of this class */
static uint8 m_methodsInit = 0;
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Reference to super implementation (option) */
static const tThaIpCoreMethods *m_ThaIpCoreImplement;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool FastRamTestIsEnabled(ThaIpCore self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaIpCore(Tha60070013IpCore self)
    {
    ThaIpCore thaIp = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(thaIp);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));
        mMethodOverride(m_ThaIpCoreOverride, FastRamTestIsEnabled);
        }

    mMethodsSet(thaIp, &m_ThaIpCoreOverride);
    }

static void Override(Tha60070013IpCore self)
    {
    OverrideThaIpCore(self);
    }

/* Constructor of Tha60070013IpCore object */
static AtIpCore ObjectInit(AtIpCore self, uint8 coreId, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60070013IpCore));

    /* Super constructor */
    if (ThaIpCoreObjectInit(self, coreId, device) == NULL)
        return NULL;

    /* Setup class */
    Override((Tha60070013IpCore)self);
    m_methodsInit = 1;

    return self;
    }

/* Create new IP core object */
AtIpCore Tha60070013IpCoreNew(uint8 coreId, AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtIpCore newCore = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60070013IpCore));
    if (newCore == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newCore, coreId, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60030111IpCore.c
 *
 * Created Date: Nov 14, 2013
 *
 * Description : IP Core
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaIpCoreInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030111IpCore
    {
    tThaIpCorePwV2 super;
    }tTha60030111IpCore;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaIpCoreMethods m_ThaIpCoreOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030111IpCore);
    }

static uint32 RamSoftResetRegister(ThaIpCore self)
    {
	AtUnused(self);
    return 0x4;
    }

static void OverrideThaIpCore(AtIpCore self)
    {
    ThaIpCore thaIp = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, mMethodsGet(thaIp), sizeof(m_ThaIpCoreOverride));
        mMethodOverride(m_ThaIpCoreOverride, RamSoftResetRegister);
        }

    mMethodsSet(thaIp, &m_ThaIpCoreOverride);
    }

static void Override(AtIpCore self)
    {
    OverrideThaIpCore(self);
    }

static AtIpCore ObjectInit(AtIpCore self, uint8 coreId, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaIpCorePwV2ObjectInit(self, coreId, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtIpCore Tha60030111IpCoreNew(uint8 coreId, AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtIpCore newCore = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newCore == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newCore, coreId, device);
    }


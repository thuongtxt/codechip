/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60030111Device.c
 *
 * Created Date: Nov 11, 2013
 *
 * Description : Product 60030111
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaPdhPwProduct/man/ThaPdhPwProductDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030111Device
    {
    tThaPdhPwProductDevice super;
    }tTha60030111Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (phyModule == cThaModulePda) return (AtModule)Tha60030111ModulePdaNew(self);

    if (moduleId == cAtModulePdh) return (AtModule)Tha60030111ModulePdhNew(self);
    if (moduleId == cAtModuleEth) return (AtModule)Tha60030111ModuleEthNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static AtIpCore IpCoreCreate(AtDevice self, uint8 coreId)
    {
    return Tha60030111IpCoreNew(coreId, self);
    }

static eBool CanCheckPll(ThaDevice self)
    {
	AtUnused(self);
    /* No PLL checking for this product (required by hardware) */
    return cAtFalse;
    }

static eBool NeedCheckSSKey(AtDevice self)
    {
	AtUnused(self);
    /* Do not need to check SSKey for this private product */
    return cAtFalse;
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice thaDev = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(thaDev), sizeof(tThaDeviceMethods));
        mMethodOverride(m_ThaDeviceOverride, CanCheckPll);
        }

    mMethodsSet(thaDev, &m_ThaDeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, IpCoreCreate);
        mMethodOverride(m_AtDeviceOverride, NeedCheckSSKey);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030111Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPdhPwProductDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60030111DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newDevice, driver, productCode);
    }


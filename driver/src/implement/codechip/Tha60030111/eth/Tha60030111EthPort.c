/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60030111EthPort.c
 *
 * Created Date: May 29, 2014
 *
 * Description : ETH Port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60030111EthPort
    {
    tThaEthPort super;
    }tTha60030111EthPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods  m_AtChannelOverride;
static tAtEthPortMethods  m_AtEthPortOverride;
static tThaEthPortMethods m_ThaEthPortOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet MacActivate(ThaEthPort self, eBool activate)
    {
	AtUnused(activate);
	AtUnused(self);
    return cAtOk;
    }

static eAtRet PcsReset(ThaEthPort self)
    {
	AtUnused(self);
    return cAtOk;
    }

static eAtRet HwAutoControlPhysicalEnable(ThaEthPort self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtOk;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
	AtUnused(interface);
	AtUnused(self);
    return cAtOk;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
	AtUnused(self);
    return cAtEthPortInterfaceNA;
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
	AtUnused(speed);
	AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
	AtUnused(speed);
	AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
	AtUnused(self);
    return cAtEthPortSpeedNA;
    }

static eAtModuleEthRet AutoNegEnable(AtEthPort self, eBool enable)
    {
	AtUnused(enable);
	AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eBool AutoNegIsEnabled(AtEthPort self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static eBool AutoNegIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtEthPortAutoNegState AutoNegStateGet(AtEthPort self)
    {
	AtUnused(self);
    return cAtEthPortAutoNegInvalid;
    }

static eAtModuleEthRet DuplexModeSet(AtEthPort self, eAtEthPortDuplexMode duplexMode)
    {
	AtUnused(duplexMode);
	AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtEthPortDuplexMode DuplexModeGet(AtEthPort self)
    {
	AtUnused(self);
    return cAtEthPortWorkingModeNA;
    }

static eAtModuleEthRet TxIpgSet(AtEthPort self, uint8 txIpg)
    {
	AtUnused(txIpg);
	AtUnused(self);
    return cAtErrorNotApplicable;
    }

static uint8 TxIpgGet(AtEthPort self)
    {
	AtUnused(self);
    return 0;
    }

static eAtModuleEthRet RxIpgSet(AtEthPort self, uint8 rxIpg)
    {
	AtUnused(rxIpg);
	AtUnused(self);
    return cAtErrorNotApplicable;
    }

static uint8 RxIpgGet(AtEthPort self)
    {
	AtUnused(self);
    return 0;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    if (loopbackMode == cAtLoopbackModeRelease)
        return cAtOk;
    return cAtErrorNotApplicable;
    }

static uint8 LoopbackGet(AtChannel self)
    {
	AtUnused(self);
    return cAtLoopbackModeRelease;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
	AtUnused(self);
	return (loopbackMode == cAtLoopbackModeRelease) ? cAtTrue : cAtFalse;
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegEnable);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsEnabled);
        mMethodOverride(m_AtEthPortOverride, AutoNegStateGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsSupported);
        mMethodOverride(m_AtEthPortOverride, DuplexModeSet);
        mMethodOverride(m_AtEthPortOverride, DuplexModeGet);
        mMethodOverride(m_AtEthPortOverride, TxIpgSet);
        mMethodOverride(m_AtEthPortOverride, TxIpgGet);
        mMethodOverride(m_AtEthPortOverride, RxIpgSet);
        mMethodOverride(m_AtEthPortOverride, RxIpgGet);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort ethPort = (ThaEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, mMethodsGet(ethPort), sizeof(m_ThaEthPortOverride));
        mMethodOverride(m_ThaEthPortOverride, MacActivate);
        mMethodOverride(m_ThaEthPortOverride, PcsReset);
        mMethodOverride(m_ThaEthPortOverride, HwAutoControlPhysicalEnable);
        }

    mMethodsSet(ethPort, &m_ThaEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60030111EthPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60030111EthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, portId, module);
    }

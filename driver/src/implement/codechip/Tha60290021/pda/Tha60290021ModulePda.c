/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60290021ModulePda.c
 *
 * Created Date: Sep 29, 2015
 *
 * Description : Module PDA of 60210051
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210051/pda/Tha60210051ModulePdaInternal.h"
#include "Tha60290021ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60290021ModulePda *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods 			m_AtObjectOverride;
static tThaModulePdaMethods         m_ThaModulePdaOverride;
static tTha60210011ModulePdaMethods m_Tha60210011ModulePdaOverride;
static tTha60210051ModulePdaMethods m_Tha60210051ModulePdaOverride;

/* Save super methods */
static const tAtObjectMethods             *m_AtObjectMethods             = NULL;
static const tTha60210011ModulePdaMethods *m_Tha60210011ModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportLowRateCesop(Tha60210011ModulePda self)
    {
    AtUnused(self);
    return 0;
    }

static eBool JitterBufferCenterIsSupported(ThaModulePda self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HwValueUnit1PacketIsSupported(Tha60210051ModulePda self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HwPwIdCacheIsSupported(Tha60210051ModulePda self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool UsePmcCounter(Tha60210011ModulePda self)
	{
	ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
	if (ThaModulePwDebugCountersModuleGet(pwModule) == cThaModulePwPmc)
		return cAtTrue;

	return cAtFalse;
	}

static Tha60210011PdaPwCounterGetter PmcCounterGetter(Tha60210011ModulePda self)
    {
    if (mThis(self)->pmcCounterGetter == NULL)
        mThis(self)->pmcCounterGetter = Tha60290021PdaPwPmcCounterGetterNew();
    return mThis(self)->pmcCounterGetter;
    }

static Tha60210011PdaPwCounterGetter CounterGetter(Tha60210011ModulePda self)
    {
	if (UsePmcCounter(self))
		return PmcCounterGetter(self);

	return m_Tha60210011ModulePdaMethods->CounterGetter(self);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->pmcCounterGetter);
    mThis(self)->pmcCounterGetter = NULL;
    m_AtObjectMethods->Delete((AtObject)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290021ModulePda *object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(pmcCounterGetter);
    }

static eBool JitterBufferWatermarkIsSupported(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtTrue;
    }

static eBool PwCwAutoRxMBitIsConfigurable(ThaModulePda self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool PwJitterBufferSizeIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    return ThaModulePdaPwVariantJitterBufferSizeIsSupported(self, pw, microseconds);
    }

static eBool PwJitterBufferDelayIsSupported(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    return ThaModulePdaPwVariantJitterBufferDelayIsSupported(self, pw, microseconds);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideTha60210051ModulePda(AtModule self)
    {
    Tha60210051ModulePda pdaModule = (Tha60210051ModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051ModulePdaOverride, mMethodsGet(pdaModule), sizeof(m_Tha60210051ModulePdaOverride));

        mMethodOverride(m_Tha60210051ModulePdaOverride, HwValueUnit1PacketIsSupported);
        mMethodOverride(m_Tha60210051ModulePdaOverride, HwPwIdCacheIsSupported);
        }

    mMethodsSet(pdaModule, &m_Tha60210051ModulePdaOverride);
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, mMethodsGet(pdaModule), sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, JitterBufferCenterIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, JitterBufferWatermarkIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, PwCwAutoRxMBitIsConfigurable);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferSizeIsSupported);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferDelayIsSupported);
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void OverrideTha60210011ModulePda(AtModule self)
    {
    Tha60210011ModulePda pdaModule = (Tha60210011ModulePda)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePdaOverride, m_Tha60210011ModulePdaMethods, sizeof(m_Tha60210011ModulePdaOverride));

        mMethodOverride(m_Tha60210011ModulePdaOverride, StartVersionSupportLowRateCesop);
        mMethodOverride(m_Tha60210011ModulePdaOverride, CounterGetter);
        }

    mMethodsSet(pdaModule, &m_Tha60210011ModulePdaOverride);
    }

static void Override(AtModule self)
    {
	OverrideAtObject(self);
    OverrideThaModulePda(self);
    OverrideTha60210011ModulePda(self);
    OverrideTha60210051ModulePda(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModulePda);
    }

AtModule Tha60290021ModulePdaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290021ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModulePdaObjectInit(newModule, device);
    }

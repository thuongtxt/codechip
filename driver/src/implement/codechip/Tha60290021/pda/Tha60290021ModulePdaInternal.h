/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60290021ModulePdaInternal.h
 * 
 * Created Date: Apr 23, 2017
 *
 * Description : PDA module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPDAINTERNAL_H_
#define _THA60290021MODULEPDAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/pda/Tha60210051ModulePdaInternal.h"
#include "Tha60290021ModulePda.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModulePda
    {
    tTha60210051ModulePda super;

    /* Private data */
    Tha60210011PdaPwCounterGetter pmcCounterGetter;
    }tTha60290021ModulePda;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290021ModulePdaObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPDAINTERNAL_H_ */


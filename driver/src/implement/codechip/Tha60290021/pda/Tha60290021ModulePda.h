/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60290021ModulePda.h
 * 
 * Created Date: Oct 10, 2016
 *
 * Description : PDA module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPDA_H_
#define _THA60290021MODULEPDA_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/pda/Tha60210011ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290021ModulePdaNew(AtDevice device);
Tha60210011PdaPwCounterGetter Tha60290021PdaPwPmcCounterGetterNew(void);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPDA_H_ */


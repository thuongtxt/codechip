/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60290021PdaPwPmcCounterGetter.c
 *
 * Created Date: Nov 2, 2016
 *
 * Description : PDA counters
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"
#include "../man/Tha60290021Device.h"
#include "Tha60290021ModulePda.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021PdaPwPmcCounterGetter
	{
	tTha60210011PdaPwCounterGetter super;
	}tTha60290021PdaPwPmcCounterGetter;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011PdaPwCounterGetterMethods m_Tha60210011PdaPwCounterGetterOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterReadWithHandler(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear,
                                     uint32 (*CounterGetFunc)(ThaModulePmc self, AtPw pw, eBool clear))
    {
    ThaModulePmc pmcModule = (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pw), cThaModulePwPmc);
    AtUnused(self);
    AtUnused(modulePda);
    return CounterGetFunc(pmcModule, pw, clear);
    }

static uint32 RxBytesGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, modulePda, pw, clear, ThaModulePmcPwRxBytesGet);
    }

static uint32 RxReorderedPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, modulePda, pw, clear, ThaModulePmcPwRxReorderedPacketsGet);
    }

static uint32 RxLostPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, modulePda, pw, clear, ThaModulePmcPwRxLostPacketsGet);
    }

static uint32 RxOutOfSeqDropedPacketsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, modulePda, pw, clear, ThaModulePmcPwRxOutOfSeqDropedPacketsGet);
    }

static uint32 RxJitBufOverrunGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, modulePda, pw, clear, ThaModulePmcPwRxJitBufOverrunGet);
    }

static uint32 RxJitBufUnderrunGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, modulePda, pw, clear, ThaModulePmcPwRxJitBufUnderrunGet);
    }

static uint32 RxLopsGet(Tha60210011PdaPwCounterGetter self, ThaModulePda modulePda, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, modulePda, pw, clear, ThaModulePmcPwRxLopsGet);
    }

static void OverrideTha60210011PdaPwCounterGetter(Tha60210011PdaPwCounterGetter self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011PdaPwCounterGetterOverride, mMethodsGet(self), sizeof(m_Tha60210011PdaPwCounterGetterOverride));

        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxBytesGet);
        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxReorderedPacketsGet);
        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxLostPacketsGet);
        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxJitBufOverrunGet);
        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxJitBufUnderrunGet);
        mMethodOverride(m_Tha60210011PdaPwCounterGetterOverride, RxLopsGet);
        }

    mMethodsSet(self, &m_Tha60210011PdaPwCounterGetterOverride);
    }

static void Override(Tha60210011PdaPwCounterGetter self)
    {
    OverrideTha60210011PdaPwCounterGetter(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PdaPwPmcCounterGetter);
    }

static Tha60210011PdaPwCounterGetter ObjectInit(Tha60210011PdaPwCounterGetter self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PdaPwCounterGetterObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60210011PdaPwCounterGetter Tha60290021PdaPwPmcCounterGetterNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60210011PdaPwCounterGetter newGetter = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGetter == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newGetter);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60290021ModulePweReg.h
 * 
 * Created Date: Feb 14, 2017
 *
 * Description : PWE register definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPWEREG_H_
#define _THA60290021MODULEPWEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Transmit Ethernet Header Mode Control
Reg Addr   : 0x00000000 - 0x001FFF #The address format for these registers is 0x00000000 + PWID
Reg Formula: 0x00000000 +  PWID
    Where  :
           + $PWID(0-8191): Pseudowire ID
Reg Desc   :
This register is used to control read/write Pseudowire Transmit Ethernet Header Value from/to DDR

------------------------------------------------------------------------------*/
#define cAf6Reg_pw_txeth_hdr_mode_Base                                                              0x00000000
#define cAf6Reg_pw_txeth_hdr_mode_WidthVal                                                                  32

/*--------------------------------------
BitField Name: TxEthPwSubVlanSel
BitField Type: RW
BitField Desc: Tx PW Subport VLAN selection 1: Select second global transmit PW
subport VLAN 0: Select first global transmit PW subport VLAN
BitField Bits: [9]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwSubVlanSel_Mask                                                    cBit9
#define cAf6_pw_txeth_hdr_mode_TxEthPwSubVlanSel_Shift                                                       9

/*--------------------------------------
BitField Name: TxEthPadMode
BitField Type: RW
BitField Desc: this is configuration for insertion PAD in short packets 0:
Insert PAD when total Ethernet packet length is less than 64 bytes. Refer to
IEEE 802.3 1: Insert PAD when control word plus payload length is less than 64
bytes 2: Insert PAD when total Ethernet packet length minus VLANs and MPLS outer
labels(if exist) is less than 64 bytes
BitField Bits: [8:7]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_Mask                                                       cBit8_7
#define cAf6_pw_txeth_hdr_mode_TxEthPadMode_Shift                                                            7

/*--------------------------------------
BitField Name: TxEthPwRtpEn
BitField Type: RW
BitField Desc: this is RTP enable for TDM PW packet 1: Enable RTP 0: Disable RTP
BitField Bits: [6]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_Mask                                                         cBit6
#define cAf6_pw_txeth_hdr_mode_TxEthPwRtpEn_Shift                                                            6

/*--------------------------------------
BitField Name: TxEthPwPsnType
BitField Type: RW
BitField Desc: this is Transmit PSN header mode working 1: PW PSN header is
UDP/IPv4 2: PW PSN header is UDP/IPv6 3: PW MPLS no outer label over Ipv4 (total
1 MPLS label) 4: PW MPLS no outer label over Ipv6 (total 1 MPLS label) 5: PW
MPLS one outer label over Ipv4 (total 2 MPLS label) 6: PW MPLS one outer label
over Ipv6 (total 2 MPLS label) 7: PW MPLS two outer label over Ipv4 (total 3
MPLS label) 8: PW MPLS two outer label over Ipv6 (total 3 MPLS label) Others:
for other PW PSN header type
BitField Bits: [5:2]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_Mask                                                     cBit5_2
#define cAf6_pw_txeth_hdr_mode_TxEthPwPsnType_Shift                                                          2

/*--------------------------------------
BitField Name: TxEthPwNumVlan
BitField Type: RW
BitField Desc: This is number of vlan in Transmit Ethernet packet 0: no vlan 1:
1 vlan 2: 2 vlan
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_Mask                                                     cBit1_0
#define cAf6_pw_txeth_hdr_mode_TxEthPwNumVlan_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count0_64 bytes packet
Reg Addr   : 0x0004000(RO)
Reg Formula: 0x0004000 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt0_64_ro_Base                                                                  0x0004000

/*--------------------------------------
BitField Name: CLAEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Mask                                                        cBit31_0
#define cAf6_Eth_cnt0_64_ro_CLAEthCnt0_64_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count0_64 bytes packet
Reg Addr   : 0x0004800(RC)
Reg Formula: 0x0004800 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt0_64_rc_Base                                                                  0x0004800

/*--------------------------------------
BitField Name: CLAEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Mask                                                        cBit31_0
#define cAf6_Eth_cnt0_64_rc_CLAEthCnt0_64_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count65_127 bytes packet
Reg Addr   : 0x0004002(RO)
Reg Formula: 0x0004002 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt65_127_ro_Base                                                                0x0004002

/*--------------------------------------
BitField Name: CLAEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Mask                                                    cBit31_0
#define cAf6_Eth_cnt65_127_ro_CLAEthCnt65_127_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count65_127 bytes packet
Reg Addr   : 0x0004802(RC)
Reg Formula: 0x0004802 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt65_127_rc_Base                                                                0x0004802

/*--------------------------------------
BitField Name: CLAEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Mask                                                    cBit31_0
#define cAf6_Eth_cnt65_127_rc_CLAEthCnt65_127_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count128_255 bytes packet
Reg Addr   : 0x0004004(RO)
Reg Formula: 0x0004004 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt128_255_ro_Base                                                               0x0004004

/*--------------------------------------
BitField Name: CLAEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Mask                                                  cBit31_0
#define cAf6_Eth_cnt128_255_ro_CLAEthCnt128_255_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count128_255 bytes packet
Reg Addr   : 0x0004804(RC)
Reg Formula: 0x0004804 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt128_255_rc_Base                                                               0x0004804

/*--------------------------------------
BitField Name: CLAEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Mask                                                  cBit31_0
#define cAf6_Eth_cnt128_255_rc_CLAEthCnt128_255_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count256_511 bytes packet
Reg Addr   : 0x0004006(RO)
Reg Formula: 0x0004006 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt256_511_ro_Base                                                               0x0004006

/*--------------------------------------
BitField Name: CLAEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Mask                                                  cBit31_0
#define cAf6_Eth_cnt256_511_ro_CLAEthCnt256_511_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count256_511 bytes packet
Reg Addr   : 0x0004806(RC)
Reg Formula: 0x0004806 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt256_511_rc_Base                                                               0x0004806

/*--------------------------------------
BitField Name: CLAEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Mask                                                  cBit31_0
#define cAf6_Eth_cnt256_511_rc_CLAEthCnt256_511_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count512_1023 bytes packet
Reg Addr   : 0x0004008(RO)
Reg Formula: 0x0004008 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt512_1024_ro_Base                                                              0x0004008

/*--------------------------------------
BitField Name: CLAEthCnt512_1024
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt512_1024_ro_CLAEthCnt512_1024_Mask                                                cBit31_0
#define cAf6_Eth_cnt512_1024_ro_CLAEthCnt512_1024_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count512_1023 bytes packet
Reg Addr   : 0x0004808(RC)
Reg Formula: 0x0004808 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt512_1024_rc_Base                                                              0x0004808

/*--------------------------------------
BitField Name: CLAEthCnt512_1024
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt512_1024_rc_CLAEthCnt512_1024_Mask                                                cBit31_0
#define cAf6_Eth_cnt512_1024_rc_CLAEthCnt512_1024_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1024_1518 bytes packet
Reg Addr   : 0x000400A(RO)
Reg Formula: 0x000400A + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1025_1528_ro_Base                                                             0x000400A

/*--------------------------------------
BitField Name: CLAEthCnt1025_1528
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1025_1528_ro_CLAEthCnt1025_1528_Mask                                              cBit31_0
#define cAf6_Eth_cnt1025_1528_ro_CLAEthCnt1025_1528_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1024_1518 bytes packet
Reg Addr   : 0x000480A(RC)
Reg Formula: 0x000480A + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1025_1528_rc_Base                                                             0x000480A

/*--------------------------------------
BitField Name: CLAEthCnt1025_1528
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1025_1528_rc_CLAEthCnt1025_1528_Mask                                              cBit31_0
#define cAf6_Eth_cnt1025_1528_rc_CLAEthCnt1025_1528_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1519_2047 bytes packet
Reg Addr   : 0x000400C(RO)
Reg Formula: 0x000400C + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1529_2047_ro_Base                                                             0x000400C

/*--------------------------------------
BitField Name: CLAEthCnt1529_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1529_2047_ro_CLAEthCnt1529_2047_Mask                                              cBit31_0
#define cAf6_Eth_cnt1529_2047_ro_CLAEthCnt1529_2047_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1519_2047 bytes packet
Reg Addr   : 0x000480C(RC)
Reg Formula: 0x000480C + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1529_2047_rc_Base                                                             0x000480C

/*--------------------------------------
BitField Name: CLAEthCnt1529_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1529_2047_rc_CLAEthCnt1529_2047_Mask                                              cBit31_0
#define cAf6_Eth_cnt1529_2047_rc_CLAEthCnt1529_2047_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Jumbo packet
Reg Addr   : 0x000400E(RO)
Reg Formula: 0x000400E + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_jumbo_ro_Base                                                                0x000400E

/*--------------------------------------
BitField Name: CLAEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Mask                                                     cBit31_0
#define cAf6_Eth_cnt_jumbo_ro_CLAEthCntJumbo_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Jumbo packet
Reg Addr   : 0x000480E(RC)
Reg Formula: 0x000480E + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_jumbo_rc_Base                                                                0x000480E

/*--------------------------------------
BitField Name: CLAEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Mask                                                     cBit31_0
#define cAf6_Eth_cnt_jumbo_rc_CLAEthCntJumbo_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Unicast packet
Reg Addr   : 0x0004010(RO)
Reg Formula: 0x0004010 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the unicast packet

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_Unicast_ro_Base                                                              0x0004010

/*--------------------------------------
BitField Name: CLAEthCntUnicast
BitField Type: RO
BitField Desc: This is statistic counter for the unicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Mask                                                 cBit31_0
#define cAf6_Eth_cnt_Unicast_ro_CLAEthCntUnicast_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Unicast packet
Reg Addr   : 0x0004810(RC)
Reg Formula: 0x0004810 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the unicast packet

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_Unicast_rc_Base                                                              0x0004810

/*--------------------------------------
BitField Name: CLAEthCntUnicast
BitField Type: RO
BitField Desc: This is statistic counter for the unicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Mask                                                 cBit31_0
#define cAf6_Eth_cnt_Unicast_rc_CLAEthCntUnicast_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Total packet
Reg Addr   : 0x0004012(RO)
Reg Formula: 0x0004012 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the total packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_pkt_cnt_ro_Base                                                               0x0004012

/*--------------------------------------
BitField Name: CLAEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_Mask                                                    cBit31_0
#define cAf6_eth_tx_pkt_cnt_ro_CLAEthCntTotal_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Total packet
Reg Addr   : 0x0004812(RC)
Reg Formula: 0x0004812 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the total packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_pkt_cnt_rc_Base                                                               0x0004812

/*--------------------------------------
BitField Name: CLAEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_Mask                                                    cBit31_0
#define cAf6_eth_tx_pkt_cnt_rc_CLAEthCntTotal_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Broadcast packet
Reg Addr   : 0x0004014(RO)
Reg Formula: 0x0004014 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the Broadcast packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_bcast_pkt_cnt_ro_Base                                                         0x0004014

/*--------------------------------------
BitField Name: CLAEthCntBroadcast
BitField Type: RO
BitField Desc: This is statistic counter broadcast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Mask                                          cBit31_0
#define cAf6_eth_tx_bcast_pkt_cnt_ro_CLAEthCntBroadcast_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Broadcast packet
Reg Addr   : 0x0004814(RC)
Reg Formula: 0x0004814 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the Broadcast packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_bcast_pkt_cnt_rc_Base                                                         0x0004814

/*--------------------------------------
BitField Name: CLAEthCntBroadcast
BitField Type: RO
BitField Desc: This is statistic counter broadcast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Mask                                          cBit31_0
#define cAf6_eth_tx_bcast_pkt_cnt_rc_CLAEthCntBroadcast_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Multicast packet
Reg Addr   : 0x0004016(RO)
Reg Formula: 0x0004016 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the Multicast packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_mcast_pkt_cnt_ro_Base                                                         0x0004016

/*--------------------------------------
BitField Name: CLAEthCntMulticast
BitField Type: RO
BitField Desc: This is statistic counter multicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_Mask                                          cBit31_0
#define cAf6_eth_tx_mcast_pkt_cnt_ro_CLAEthCntMulticast_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Multicast packet
Reg Addr   : 0x0004816(RC)
Reg Formula: 0x0004816 + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic counter for the Multicast packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_mcast_pkt_cnt_rc_Base                                                         0x0004816

/*--------------------------------------
BitField Name: CLAEthCntMulticast
BitField Type: RO
BitField Desc: This is statistic counter multicast packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_Mask                                          cBit31_0
#define cAf6_eth_tx_mcast_pkt_cnt_rc_CLAEthCntMulticast_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count number of bytes of packet
Reg Addr   : 0x000401E(RO)
Reg Formula: 0x000401E + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic count number of bytes of packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_byte_ro_Base                                                                 0x000401E

/*--------------------------------------
BitField Name: CLAEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Mask                                                       cBit31_0
#define cAf6_Eth_cnt_byte_ro_CLAEthCntByte_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count number of bytes of packet
Reg Addr   : 0x000481E(RC)
Reg Formula: 0x000481E + eth_port
    Where  :
           + $eth_port(0-3): Transmit Ethernet port ID
Reg Desc   :
This register is statistic count number of bytes of packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt_byte_rc_Base                                                                 0x000481E

/*--------------------------------------
BitField Name: CLAEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Mask                                                       cBit31_0
#define cAf6_Eth_cnt_byte_rc_CLAEthCntByte_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Parity Register Control
Reg Addr   : 0x000F000
Reg Formula:
    Where  :
Reg Desc   :
This register using for Force Parity

------------------------------------------------------------------------------*/
#define cAf6Reg_pwe_Parity_control_Base                                                              0x000F000
#define cAf6Reg_pwe_Parity_control_WidthVal                                                                 32

/*--------------------------------------
BitField Name: PwForceErr
BitField Type: RW
BitField Desc: Force parity error enable for "Pseudowire Transmit Ethernet
Header Mode Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pwe_Parity_control_PwForceErr_Mask                                                          cBit0
#define cAf6_pwe_Parity_control_PwForceErr_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Parity Disable register Control
Reg Addr   : 0x000F001
Reg Formula:
    Where  :
Reg Desc   :
This register using for Disable Parity

------------------------------------------------------------------------------*/
#define cAf6Reg_pwe_Parity_Disable_control_Base                                                      0x000F001
#define cAf6Reg_pwe_Parity_Disable_control_WidthVal                                                         32

/*--------------------------------------
BitField Name: PwDisChkErr
BitField Type: RW
BitField Desc: Disable parity error check for "Pseudowire Transmit Ethernet
Header Mode Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pwe_Parity_Disable_control_PwDisChkErr_Mask                                                 cBit0
#define cAf6_pwe_Parity_Disable_control_PwDisChkErr_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Pseudowire Parity sticky error
Reg Addr   : 0x000F002
Reg Formula:
    Where  :
Reg Desc   :
This register using for checking sticky error

------------------------------------------------------------------------------*/
#define cAf6Reg_pwe_Parity_stk_err_Base                                                              0x000F002
#define cAf6Reg_pwe_Parity_stk_err_WidthVal                                                                 32

/*--------------------------------------
BitField Name: PwStkErr
BitField Type: W1C
BitField Desc: parity error for "Pseudowire Transmit Ethernet Header Mode
Control"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pwe_Parity_stk_err_PwStkErr_Mask                                                            cBit0
#define cAf6_pwe_Parity_stk_err_PwStkErr_Shift                                                               0

#define cAf6Reg_pw_tx_glb_ctrl                                                                          0x88020

/*--------------------------------------
BitField Name: ForceFSM
BitField Type: RW
BitField Desc: Force FSM state for selftest
BitField Bits: [1]
--------------------------------------*/
#define cAf6_pw_tx_glb_ctrl_ForceFSM_Mask                                                                cBit1
#define cAf6_pw_tx_glb_ctrl_ForceFSM_Shift                                                                   1

/*--------------------------------------
BitField Name: FSMPinEn
BitField Type: RW
BitField Desc: Enable FSM from pin outside
BitField Bits: [0]
--------------------------------------*/
#define cAf6_pw_tx_glb_ctrl_FSMPinEn_Mask                                                                cBit0
#define cAf6_pw_tx_glb_ctrl_FSMPinEn_Shift                                                                   0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPWEREG_H_ */

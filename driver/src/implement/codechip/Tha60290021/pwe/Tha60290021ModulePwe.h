/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60290021ModulePwe.h
 * 
 * Created Date: Oct 10, 2016
 *
 * Description : PWE module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPWE_H_
#define _THA60290021MODULEPWE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290021ModulePweNew(AtDevice device);
Tha60210011PwePwCounterGetter Tha60290021PwePwPmcCounterGetterNew(void);

eAtRet Tha60290021ModulePwePwSubPortVlanIndexSet(ThaModulePwe self, AtPw adapter, uint32 subPortVlanIndex);
uint32 Tha60290021ModulePwePwSubPortVlanIndexGet(ThaModulePwe self, AtPw adapter);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPWE_H_ */


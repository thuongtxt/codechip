/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE module
 *
 * File        : Tha60290021PwePwPmcCounterGetter.c
 *
 * Created Date: Nov 1, 2016
 *
 * Description : PWE counters
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePweInternal.h"
#include "../man/Tha60290021Device.h"
#include "Tha60290021ModulePwe.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290021PwePwPmcCounterGetter)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021PwePwPmcCounterGetter
    {
    tTha60210011PwePwCounterGetter super;
    }tTha60290021PwePwPmcCounterGetter;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011PwePwCounterGetterMethods m_Tha60210011PwePwCounterGetterOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePmc PmcModule(AtPw self)
	{
	return (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModulePwPmc);
	}

static uint32 CounterGetWithHandler(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear,
                                    uint32 (*CounterGetFunc)(ThaModulePmc self, AtPw pw, eBool clear))
    {
    AtUnused(self);
    AtUnused(modulePwe);
    return CounterGetFunc(PmcModule(pw), pw, clear);
    }

static uint32 TxPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    return CounterGetWithHandler(self, modulePwe, pw, clear, ThaModulePmcPwTxPacketsGet);
    }

static uint32 TxBytesGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    return CounterGetWithHandler(self, modulePwe, pw, clear, ThaModulePmcPwTxBytesGet);
    }

static uint32 TxLbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    return CounterGetWithHandler(self, modulePwe, pw, clear, ThaModulePmcPwTxLbitPacketsGet);
    }

static uint32 TxRbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    return CounterGetWithHandler(self, modulePwe, pw, clear, ThaModulePmcPwTxRbitPacketsGet);
    }

static uint32 TxMbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    return CounterGetWithHandler(self, modulePwe, pw, clear, ThaModulePmcPwTxMbitPacketsGet);
    }

static uint32 TxPbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    return CounterGetWithHandler(self, modulePwe, pw, clear, ThaModulePmcPwTxPbitPacketsGet);
    }

static uint32 TxNbitPacketsGet(Tha60210011PwePwCounterGetter self, ThaModulePwe modulePwe, AtPw pw, eBool clear)
    {
    return CounterGetWithHandler(self, modulePwe, pw, clear, ThaModulePmcPwTxNbitPacketsGet);
    }

static void OverrideTha60210011PwePwCounterGetter(Tha60210011PwePwCounterGetter self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011PwePwCounterGetterOverride, mMethodsGet(self), sizeof(m_Tha60210011PwePwCounterGetterOverride));

        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxPacketsGet);
        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxBytesGet);
        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxLbitPacketsGet);
        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxRbitPacketsGet);
        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxMbitPacketsGet);
        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxPbitPacketsGet);
        mMethodOverride(m_Tha60210011PwePwCounterGetterOverride, TxNbitPacketsGet);
        }

    mMethodsSet(self, &m_Tha60210011PwePwCounterGetterOverride);
    }

static void Override(Tha60210011PwePwCounterGetter self)
    {
    OverrideTha60210011PwePwCounterGetter(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PwePwPmcCounterGetter);
    }

static Tha60210011PwePwCounterGetter ObjectInit(Tha60210011PwePwCounterGetter self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PwePwCounterGetterObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60210011PwePwCounterGetter Tha60290021PwePwPmcCounterGetterNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60210011PwePwCounterGetter newGetter = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGetter == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newGetter);
    }

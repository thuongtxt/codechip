/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60290021ModulePwe.c
 *
 * Created Date: Oct 10, 2016
 *
 * Description : PWE module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/sdh/AtSdhVcInternal.h"
#include "../../../../generic/xc/AtCrossConnectInternal.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../eth/Tha60290021FaceplateSerdesEthPortReg.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../man/Tha60290021DeviceFsm.h"
#include "Tha60290021ModulePweReg.h"
#include "Tha60290021ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha60290021ModulePwe *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tThaModulePweMethods         m_ThaModulePweOverride;
static tTha60210011ModulePweMethods m_Tha60210011ModulePweOverride;

/* Save super methods */
static const tAtObjectMethods             *m_AtObjectMethods             = NULL;
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;
static const tThaModulePweMethods         *m_ThaModulePweMethods         = NULL;
static const tTha60210011ModulePweMethods *m_Tha60210011ModulePweMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionControlJitterAttenuator(Tha60210011ModulePwe self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 HwPwType(ThaModulePwe self, AtPw pwAdapter, eAtPwType pwType)
    {
    AtUnused(self);

    if (pwType == cAtPwTypeCESoP)
        {
        AtPw pw = ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
        if (pw)
            return (AtPwCESoPModeGet((AtPwCESoP)pw) == cAtPwCESoPModeBasic) ? 0x1 : 0x3;
        else
            return 0x3;
        }

    if (pwType == cAtPwTypeSAToP) return 0x0;
    if (pwType == cAtPwTypeCEP)   return 0x2;

    /* Should Log with this case!*/
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "Lo-Circuit-PW Type set as default SaTop!\r\n");

    return 0x0;
    }

static eBool ShouldGetCountersFromPmc(Tha60210011ModulePwe self)
    {
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModulePw);
    uint32 counterMode = ThaModulePwDebugCountersModuleGet(pwModule);
    return (counterMode == cThaModulePwPmc) ? cAtTrue : cAtFalse;
    }

static Tha60210011PwePwCounterGetter PmcCounterGetter(Tha60210011ModulePwe self)
    {
    if (mThis(self)->counterGetter == NULL)
        mThis(self)->counterGetter = Tha60290021PwePwPmcCounterGetterNew();
    return mThis(self)->counterGetter;
    }

static Tha60210011PwePwCounterGetter CounterGetter(Tha60210011ModulePwe self)
    {
	if (ShouldGetCountersFromPmc(self))
		return PmcCounterGetter(self);

    return m_Tha60210011ModulePweMethods->CounterGetter(self);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->counterGetter);
    mThis(self)->counterGetter = NULL;
    m_AtObjectMethods->Delete((AtObject)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290021ModulePwe *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(counterGetter);
    }

static eBool TxPwActiveHwIsReady(ThaModulePwe self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasRole(AtModule self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtDevice Device(AtModule self)
    {
    return AtModuleDeviceGet(self);
    }

static eAtRet RoleActiveForce(AtModule self, eBool forced)
    {
    return Tha60290021DeviceRoleActiveForce(Device(self), forced);
    }

static eBool RoleActiveIsForced(AtModule self)
    {
    return Tha60290021DeviceRoleActiveIsForced(Device(self));
    }

static eAtRet RoleInputEnable(AtModule self, eBool enabled)
    {
    return Tha60290021DeviceRoleInputEnable(Device(self), enabled);
    }

static eBool RoleInputIsEnabled(AtModule self)
    {
    return Tha60290021DeviceRoleInputIsEnabled(Device(self));
    }

static eAtDeviceRole RoleInputStatus(AtModule self)
    {
    return Tha60290021DeviceRoleInputStatus(Device(self));
    }

static uint32 SubportVlanAddress(ThaModulePwe self)
    {
    AtUnused(self);
    return (uint32) (cAf6Reg_upen_cfgpwvlaninsen_Base + Tha60290021ModuleEthBypassBaseAddress());
    }

static eAtRet SubPortVlanBypass(ThaModulePwe self, eBool bypass)
    {
    uint32 regAddr = SubportVlanAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_cfgpwvlaninsen_PwVlanInsEn_, bypass ? 0 : 1);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool SubPortVlanIsBypassed(ThaModulePwe self)
    {
    uint32 regAddr = SubportVlanAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_upen_cfgpwvlaninsen_PwVlanInsEn_Mask) ? cAtFalse : cAtTrue;
    }

static void PweParityErrorDebug(ThaModulePwe self)
    {
    uint32 regAddress, regValue;
    AtDebugger debugger = NULL;

    regAddress = cAf6Reg_pwe_Parity_stk_err_Base + Tha60210011ModulePweBaseAddress();
    regValue   = mModuleHwRead(self, regAddress);

    Tha6021DebugPrintNeedTwoCollumnsEnable(cAtFalse);

    Tha6021DebugPrintRegName (debugger, "* PWE Parity Error", regAddress, regValue);
    Tha6021DebugPrintErrorBit(debugger, " PWE Parity Sticky Error", regValue, cAf6_pwe_Parity_stk_err_PwStkErr_Mask);

    Tha6021DebugPrintNeedTwoCollumnsEnable(cAtTrue);
    Tha6021DebugPrintStop(debugger);

    /* Clear sticky */
    mModuleHwWrite(self, regAddress, regValue);
    }

static eAtRet Debug(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Debug(self);

    PweParityErrorDebug((ThaModulePwe)self);

    return ret;
    }

static uint32 TxEthHdrModeAddress(ThaModulePwe self, AtPw adapter)
    {
    return cAf6Reg_pw_txeth_hdr_mode_Base + ThaModulePwePwPweDefaultOffset(self, adapter);
    }

static eAtRet PwSubPortVlanIndexSet(ThaModulePwe self, AtPw adapter, uint32 subPortVlanIndex)
    {
    uint32 regAddr = TxEthHdrModeAddress(self, adapter);
    uint32 regVal  = mChannelHwRead(adapter, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cAf6_pw_txeth_hdr_mode_TxEthPwSubVlanSel_, subPortVlanIndex);
    mChannelHwWrite(adapter, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint32 PwSubPortVlanIndexGet(ThaModulePwe self, AtPw adapter)
    {
    uint32 regAddr = TxEthHdrModeAddress(self, adapter);
    uint32 regVal  = mChannelHwRead(adapter, regAddr, cThaModulePwe);
    return (uint8)mRegField(regVal, cAf6_pw_txeth_hdr_mode_TxEthPwSubVlanSel_);
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    const char *ramName = AtInternalRamName(ram);

    if (AtStrcmp(ramName, sTha60210011ModulePweRamPWEPseudowireTransmitEthernetHeaderModeControlslice1) == 0)
        return cAtTrue;

    if (AtStrcmp(ramName, sTha60210011ModulePweRamPLAOutputPWEPort2ControlRegister) == 0)
        return cAtTrue;

    return m_AtModuleMethods->InternalRamIsReserved(self, ram);
    }

static eBool PwGroupingShow(Tha60210011ModulePwe self, AtPw pw)
    {
    /* This product does not have PW group concept. So do not need to display
     * and return cAtTrue to let super know that it should not print debug
     * information */
    AtUnused(self);
    AtUnused(pw);
    return cAtTrue;
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, HasRole);
        mMethodOverride(m_AtModuleOverride, RoleActiveForce);
        mMethodOverride(m_AtModuleOverride, RoleActiveIsForced);
        mMethodOverride(m_AtModuleOverride, RoleInputEnable);
        mMethodOverride(m_AtModuleOverride, RoleInputIsEnabled);
        mMethodOverride(m_AtModuleOverride, RoleInputStatus);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, m_ThaModulePweMethods, sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, HwPwType);
        mMethodOverride(m_ThaModulePweOverride, TxPwActiveHwIsReady);
        mMethodOverride(m_ThaModulePweOverride, SubPortVlanBypass);
        mMethodOverride(m_ThaModulePweOverride, SubPortVlanIsBypassed);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void OverrideTha60210011ModulePwe(AtModule self)
    {
    Tha60210011ModulePwe modulePwe = (Tha60210011ModulePwe)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModulePweMethods = mMethodsGet(modulePwe);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePweOverride, m_Tha60210011ModulePweMethods, sizeof(m_Tha60210011ModulePweOverride));

        mMethodOverride(m_Tha60210011ModulePweOverride, StartVersionControlJitterAttenuator);
        mMethodOverride(m_Tha60210011ModulePweOverride, CounterGetter);
        mMethodOverride(m_Tha60210011ModulePweOverride, PwGroupingShow);
        }

    mMethodsSet(modulePwe, &m_Tha60210011ModulePweOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModulePwe(self);
    OverrideTha60210011ModulePwe(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModulePwe);
    }

AtModule Tha60290021ModulePweObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePweObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290021ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModulePweObjectInit(newModule, device);
    }

eAtRet Tha60290021ModulePwePwSubPortVlanIndexSet(ThaModulePwe self, AtPw adapter, uint32 subPortVlanIndex)
    {
    if (self)
        return PwSubPortVlanIndexSet(self, adapter, subPortVlanIndex);
    return cAtErrorObjectNotExist;
    }

uint32 Tha60290021ModulePwePwSubPortVlanIndexGet(ThaModulePwe self, AtPw adapter)
    {
    if (self)
        return PwSubPortVlanIndexGet(self, adapter);
    return cInvalidUint32;
    }

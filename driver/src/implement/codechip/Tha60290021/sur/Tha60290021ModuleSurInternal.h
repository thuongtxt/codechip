/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SUR
 * 
 * File        : Tha60290021ModuleSurInternal.h
 * 
 * Created Date: May 11, 2017
 *
 * Description : Surveillance module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULESURINTERNAL_H_
#define _THA60290021MODULESURINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/sur/Tha60210011ModuleSurInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModuleSur
    {
    tTha60210011ModuleSur super;
    }tTha60290021ModuleSur;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSur Tha60290021ModuleSurObjectInit(AtModuleSur self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULESURINTERNAL_H_ */


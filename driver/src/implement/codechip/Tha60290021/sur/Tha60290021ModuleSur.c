/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SUR
 *
 * File        : Tha60290021ModuleSur.c
 *
 * Created Date: Oct 7, 2016
 *
 * Description : Tha60290021ModuleSur implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/sur/hard/engine/ThaHardSurEngine.h"
#include "../common/Tha602900xxCommon.h"
#include "Tha60290021ModuleSurInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290021ModuleSur *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleHardSurMethods m_ThaModuleHardSurOverride;
static tAtModuleSurMethods      m_AtModuleSurOverride;
static tAtModuleMethods         m_AtModuleOverride;

/* Save super implementation */
static const tAtModuleMethods  *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSurEngine SdhLineSurEngineObjectCreate(ThaModuleHardSur self, AtChannel line)
    {
    return ThaHardSurEngineSdhLineNew((AtModuleSur)self, line);
    }

static eBool ShouldEnablePwCounters(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool NeedLatchHoPw(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 NumThresholdProfiles(AtModuleSur self)
    {
    /* This is type ID */
    AtUnused(self);
    return 8;
    }

static eBool ShouldMaskFailuresWhenNonAutonomousFailures(AtModuleSur self)
    {
    return Tha602900xxShouldMaskFailuresWhenNonAutonomousFailures(self);
    }

static eBool HasExpireTimeout(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool TcaIsSupported(AtModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanEnableHwPmEngine(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    AtUnused(self);
    return 2;
    }

static eBool LineIsSupported(AtModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasMonitorTimer(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    const char *ramName = AtInternalRamName(ram);

    if (AtStrcmp(ramName, sTha60210011ModuleSurRamFMPMPWDefInterruptMASKPerTypePerPW) == 0)
        return cAtTrue;

    if (AtStrcmp(ramName, sTha60210011ModuleSurRamFMPMPWDefFramerInterruptMASKPerPW) == 0)
        return cAtTrue;

    return m_AtModuleMethods->InternalRamIsReserved(self, ram);
    }

static ThaVersionReader VersionReader(ThaModuleHardSur self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaDeviceVersionReader(device);
    }

static eBool FailureForwardingStatusIsSupported(ThaModuleHardSur self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x0, 0);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool FailureHoldOffTimerConfigurable(AtModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool FailureHoldOnTimerConfigurable(AtModuleSur self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x6, 0);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((ThaModuleHardSur)self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eAtSurPeriodExpireMethod DefaultExpireMethod(ThaModuleHardSur self)
    {
    return Tha602900xxModuleSurDefaultExpireMethod(self);
    }

static eAtSurTickSource DefaultTickSourceGet(ThaModuleHardSur self)
    {
    return Tha602900xxModuleSurDefaultTickSource(self);
    }

static eBool ExpireInterruptIsSupported(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaModuleHardSur(AtModuleSur self)
    {
    ThaModuleHardSur surModule = (ThaModuleHardSur)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardSurOverride, mMethodsGet(surModule), sizeof(m_ThaModuleHardSurOverride));

        mMethodOverride(m_ThaModuleHardSurOverride, SdhLineSurEngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, ShouldEnablePwCounters);
        mMethodOverride(m_ThaModuleHardSurOverride, NeedLatchHoPw);
        mMethodOverride(m_ThaModuleHardSurOverride, HasExpireTimeout);
        mMethodOverride(m_ThaModuleHardSurOverride, CanEnableHwPmEngine);
        mMethodOverride(m_ThaModuleHardSurOverride, HasMonitorTimer);
        mMethodOverride(m_ThaModuleHardSurOverride, FailureForwardingStatusIsSupported);
        mMethodOverride(m_ThaModuleHardSurOverride, DefaultExpireMethod);
        mMethodOverride(m_ThaModuleHardSurOverride, DefaultTickSourceGet);
        mMethodOverride(m_ThaModuleHardSurOverride, ExpireInterruptIsSupported);
        }

    mMethodsSet(surModule, &m_ThaModuleHardSurOverride);
    }

static void OverrideAtModuleSur(AtModuleSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSurOverride, mMethodsGet(self), sizeof(m_AtModuleSurOverride));

        mMethodOverride(m_AtModuleSurOverride, TcaIsSupported);
        mMethodOverride(m_AtModuleSurOverride, NumThresholdProfiles);
        mMethodOverride(m_AtModuleSurOverride, LineIsSupported);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOffTimerConfigurable);
        mMethodOverride(m_AtModuleSurOverride, FailureHoldOnTimerConfigurable);
        mMethodOverride(m_AtModuleSurOverride, ShouldMaskFailuresWhenNonAutonomousFailures);
        mMethodOverride(m_AtModuleSurOverride, TcaIsSupported);
        }

    mMethodsSet(self, &m_AtModuleSurOverride);
    }

static void OverrideAtModule(AtModuleSur self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleSur self)
    {
    OverrideAtModule(self);
    OverrideAtModuleSur(self);
    OverrideThaModuleHardSur(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModuleSur);
    }

AtModuleSur Tha60290021ModuleSurObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur Tha60290021ModuleSurNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (module == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModuleSurObjectInit(module, device);
    }

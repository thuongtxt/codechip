/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60290021ModuleCdrInternal.h
 * 
 * Created Date: Apr 19, 2017
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULECDRINTERNAL_H_
#define _THA60290021MODULECDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/cdr/Tha60210051ModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModuleCdr
    {
    tTha60210051ModuleCdr super;
    }tTha60290021ModuleCdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290021ModuleCdrObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULECDRINTERNAL_H_ */


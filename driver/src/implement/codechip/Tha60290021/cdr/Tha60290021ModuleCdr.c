/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60290021ModuleCdr.c
 *
 * Created Date: Oct 28, 2016
 *
 * Description : CDR module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290021ModuleCdrInternal.h"
#include "../sdh/Tha60290021ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCdrMethods    m_ThaModuleCdrOverride;
static tThaModuleCdrStmMethods m_ThaModuleCdrStmOverride;

/* Save super implementation */
static const tThaModuleCdrStmMethods *m_ThaModuleCdrStmMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportInterrupt(ThaModuleCdr self)
    {
    AtUnused(self);
    return 0;
    }

static eBool ShouldEnableJitterAttenuator(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet StsPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId, eThaOcnVc3PldType payloadType)
    {
    if (Tha60290021ModuleSdhIsTerminatedVc(channel))
        return m_ThaModuleCdrStmMethods->StsPldSet(self, channel, slice, hwStsId, payloadType);
    return cAtOk;
    }

static eAtRet Vc3CepModeEnable(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, eBool enable)
    {
    if (Tha60290021ModuleSdhIsTerminatedVc(channel))
        return m_ThaModuleCdrStmMethods->Vc3CepModeEnable(self, channel, slice, hwSts, enable);
    return cAtOk;
    }

static void OverrideThaModuleCdrStm(AtModule self)
    {
    ThaModuleCdrStm module = (ThaModuleCdrStm)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrStmMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrStmOverride, m_ThaModuleCdrStmMethods, sizeof(m_ThaModuleCdrStmOverride));

        mMethodOverride(m_ThaModuleCdrStmOverride, StsPldSet);
        mMethodOverride(m_ThaModuleCdrStmOverride, Vc3CepModeEnable);
        }

    mMethodsSet(module, &m_ThaModuleCdrStmOverride);
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, mMethodsGet(cdrModule), sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, StartVersionSupportInterrupt);
        mMethodOverride(m_ThaModuleCdrOverride, ShouldEnableJitterAttenuator);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCdr(self);
    OverrideThaModuleCdrStm(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModuleCdr);
    }

AtModule Tha60290021ModuleCdrObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290021ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModuleCdrObjectInit(newModule, device);
    }

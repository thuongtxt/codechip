/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : Tha60290021ModuleXcInternal.h
 * 
 * Created Date: Apr 16, 2017
 *
 * Description : XC module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEXCINTERNAL_H_
#define _THA60290021MODULEXCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/xc/Tha60210011ModuleXcInternal.h"
#include "Tha60290021ModuleXc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModuleXc
    {
    tTha60210011ModuleXc super;

    /* Private data */
    eTha60290021XcHideMode hideMode;
    }tTha60290021ModuleXc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleXc Tha60290021ModuleXcObjectInit(AtModuleXc self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEXCINTERNAL_H_ */


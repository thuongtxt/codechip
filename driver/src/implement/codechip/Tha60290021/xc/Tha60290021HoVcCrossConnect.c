/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC
 *
 * File        : Tha60290021HoVcCrossConnect.c
 *
 * Created Date: Jul 12, 2016
 *
 * Description : HO Cross-connect
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/sdh/ThaSdhVcInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../sdh/Tha6029SdhTerminatedLineAuVcInternal.h"
#include "Tha60290021HoVcCrossConnectInternal.h"
#include "Tha60290021ModuleXc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtCrossConnectMethods        m_AtCrossConnectOverride;
static tThaVcCrossConnectMethods     m_ThaVcCrossConnectOverride;

/* To save super implementation */
static const tAtCrossConnectMethods *m_AtCrossConnectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumHwSourceSts(ThaVcCrossConnect self)
    {
    AtUnused(self);
    return 3;
    }

static eBool ShouldSortHwStsBeforeConnecting(ThaVcCrossConnect self)
    {
    /* Without this, B3 will happen at the remote end */
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleXcRet ChannelConnect(AtCrossConnect self, AtChannel source, AtChannel dest)
    {
    eAtRet ret;

    ret = m_AtCrossConnectMethods->ChannelConnect(self, source, dest);
    if (ret != cAtOk)
        return ret;

    if (Tha60290021ModuleSdhIsTerminatedVc((AtSdhChannel)source) &&
        Tha60290021ModuleSdhIsLineSideVc((AtSdhChannel)dest))
        ret |= Tha6029SdhTerminatedLineAuVcTxSquelchingUpdate((AtSdhChannel)source);

    return ret;
    }

static eAtModuleXcRet ChannelDisconnect(AtCrossConnect self, AtChannel source, AtChannel dest)
    {
    eAtRet ret;

    ret = m_AtCrossConnectMethods->ChannelDisconnect(self, source, dest);
    if (ret != cAtOk)
        return ret;

    if (Tha60290021ModuleSdhIsTerminatedVc((AtSdhChannel)source) &&
        Tha60290021ModuleSdhIsLineSideVc((AtSdhChannel)dest))
        ret |= AtChannelTxSquelch(dest, cAtFalse);

    return ret;
    }

static void OverrideAtCrossConnect(AtCrossConnect self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtCrossConnectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtCrossConnectOverride, m_AtCrossConnectMethods, sizeof(m_AtCrossConnectOverride));

        mMethodOverride(m_AtCrossConnectOverride, ChannelConnect);
        mMethodOverride(m_AtCrossConnectOverride, ChannelDisconnect);
        }

    mMethodsSet(self, &m_AtCrossConnectOverride);
    }

static void OverrideThaVcCrossConnect(AtCrossConnect self)
    {
    ThaVcCrossConnect vcCrossConnect = (ThaVcCrossConnect)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVcCrossConnectOverride, mMethodsGet(vcCrossConnect), sizeof(m_ThaVcCrossConnectOverride));

        mMethodOverride(m_ThaVcCrossConnectOverride, NumHwSourceSts);
        mMethodOverride(m_ThaVcCrossConnectOverride, ShouldSortHwStsBeforeConnecting);
        }

    mMethodsSet(vcCrossConnect, &m_ThaVcCrossConnectOverride);
    }

static void Override(AtCrossConnect self)
    {
    OverrideAtCrossConnect(self);
    OverrideThaVcCrossConnect(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021HoVcCrossConnect);
    }

AtCrossConnect Tha60290021HoVcCrossConnectObjectInit(AtCrossConnect self, AtModuleXc module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcCrossConnectObjectInit(self, module) == NULL)
        return NULL;

    /* Over-ride */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtCrossConnect Tha60290021HoVcCrossConnectNew(AtModuleXc module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtCrossConnect newXc = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newXc == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021HoVcCrossConnectObjectInit(newXc, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC Module
 * 
 * File        : Tha60290021ModuleXc.h
 * 
 * Created Date: Jul 11, 2016
 *
 * Description : 60290021XC module header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDK_THA60290021MODULEXC_H_
#define _ATSDK_THA60290021MODULEXC_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleXc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eTha60290021XcHideMode
    {
    cTha60290021XcHideModeNone,     /* Do not hide anything, this must be default mode */
    cTha60290021XcHideModeDirect,   /* Hide XC but the connection between MATE/Faceplate and Terminated lines are 1:1 */
    cTha60290021XcHideModeMate,     /* Hide XC at MATE, all faceplate ports are looped */
    cTha60290021XcHideModeFaceplate /* Hide XC at Faceplate, all MATE ports are looped */
    }eTha60290021XcHideMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtCrossConnect Tha60290021HoVcCrossConnectNew(AtModuleXc module);

eAtRet Tha60290021ModuleXcHideModeSet(AtModuleXc self, eTha60290021XcHideMode hideMode);
eTha60290021XcHideMode Tha60290021ModuleXcHideModeGet(AtModuleXc self);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDK_THA60290021MODULEXC_H_ */


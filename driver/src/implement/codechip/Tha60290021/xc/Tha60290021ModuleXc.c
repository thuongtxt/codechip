/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : XC Module
 *
 * File        : Tha60290021ModuleXc.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : 60290021XC module source
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannel.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/xc/ThaModuleXcReg.h" /* TODO: not good, just quick work. */
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../ocn/Tha60290021ModuleOcnReg.h"
#include "Tha60290021ModuleXcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290021ModuleXc *)self)
#define mSdhModule(self) SdhModule((AtModule)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods            m_AtObjectOverride;
static tAtModuleMethods            m_AtModuleOverride;
static tAtModuleXcMethods          m_AtModuleXcOverride;
static tThaModuleXcMethods         m_ThaModuleXcOverride;
static tTha60210011ModuleXcMethods m_Tha60210011ModuleXcOverride;

/* To save super implementation */
static const tAtObjectMethods            *m_AtObjectMethods            = NULL;
static const tAtModuleMethods            *m_AtModuleMethods            = NULL;
static const tAtModuleXcMethods          *m_AtModuleXcMethods          = NULL;
static const tTha60210011ModuleXcMethods *m_Tha60210011ModuleXcMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh SdhModule(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return (AtModuleSdh) AtDeviceModuleGet(device, cAtModuleSdh);
    }

static uint8 HwSliceIdGet(Tha60210011ModuleXc self, uint8 lineId)
    {
    AtModuleSdh sdhModule = mSdhModule(self);

    if (Tha60290021ModuleSdhLineIsTerminated(sdhModule,lineId))
        return (uint8) (lineId - Tha60290021ModuleSdhTerminatedLineStartId(sdhModule));

    if (Tha60290021ModuleSdhLineIsMate(sdhModule, lineId))
        return (uint8) (lineId - Tha60290021ModuleSdhMateLineStartId(sdhModule));

    return m_Tha60210011ModuleXcMethods->HwSliceIdGet(self, lineId);
    }

static AtCrossConnect VcCrossConnectObjectCreate(AtModuleXc self)
    {
    return Tha60290021HoVcCrossConnectNew(self);
    }

static uint8 StartHoLineId(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 RxBrigdeAndRollSxcReg(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6Reg_tfi5rxbarsxcramctl_Base;
    }

static uint32 TxBrigdeAndRollSxcReg(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6Reg_tfi5txbarsxcramctl_Base;
    }

static uint32 TxSxcLineIdMask(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_sxcramctl0_SxcLineIdPage0_Mask;
    }

static uint8 TxSxcLineIdShift(Tha60210011ModuleXc self)
    {
    AtUnused(self);
    return cAf6_sxcramctl0_SxcLineIdPage0_Shift;
    }

static const char *HideModeString(AtModule self)
    {
    eTha60290021XcHideMode mode = Tha60290021ModuleXcHideModeGet((AtModuleXc)self);

    switch (mode)
        {
        case cTha60290021XcHideModeNone     : return "none";
        case cTha60290021XcHideModeDirect   : return "direct";
        case cTha60290021XcHideModeMate     : return "mate";
        case cTha60290021XcHideModeFaceplate: return "faceplate";
        default:
            return "unknown";
        }
    }

static eAtRet Debug(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Debug(self);

    AtPrintc(cSevNormal, "* XC hiding: %s\r\n", HideModeString(self));

    return ret;
    }

static AtModule OcnModule(AtModule self)
    {
    return AtDeviceModuleGet(AtModuleDeviceGet(self), cThaModuleOcn);
    }

static eAtDeviceRole RoleGet(AtModule self)
    {
    return AtModuleRoleGet(OcnModule(self));
    }

static eAtRet RoleSet(AtModule self, eAtDeviceRole role)
    {
    return AtModuleRoleSet(OcnModule(self), role);
    }

static eBool HasRole(AtModule self)
    {
    return AtModuleHasRole(OcnModule(self));
    }

static eAtRet RoleInputEnable(AtModule self, eBool enable)
    {
    return AtModuleRoleInputEnable(OcnModule(self), enable);
    }

static eAtRet RoleActiveForce(AtModule self, eBool forced)
    {
    return AtModuleRoleActiveForce(OcnModule(self), forced);
    }

static eBool MateHideModeCanBeApplied(AtModuleXc self)
    {
    AtModuleSdh sdhModule = mSdhModule(mThis(self));
    uint8 numLines = Tha60290021ModuleSdhNumFaceplateLines(sdhModule);
    uint8 line_i;

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = Tha60290021ModuleSdhFaceplateLineGet(sdhModule, line_i);

        if ((line == NULL) || (AtSdhLineRateIsSupported(line, cAtSdhLineRateStm16) == cAtFalse))
            continue;

        if (AtSdhLineRateGet(line) == cAtSdhLineRateStm16)
            continue;

        AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation,
                                "To hide at MATE, lines at Faceplate must be in STM-16. This "
                                "constrain is to simplify internal cross-connect hiding logic.\r\n");
        return cAtFalse;
        }

    return cAtTrue;
    }

static eBool HideModeCanBeApplied(AtModuleXc self, eTha60290021XcHideMode hideMode)
    {
    if (hideMode == cTha60290021XcHideModeMate)
        return MateHideModeCanBeApplied(self);

    /* May add more constrains */
    return cAtTrue;
    }

static eAtRet HideModeSet(AtModuleXc self, eTha60290021XcHideMode hideMode)
    {
    if (!HideModeCanBeApplied(self, hideMode))
        return cAtErrorModeNotSupport;

    mThis(self)->hideMode = hideMode;
    return cAtOk;
    }

static eTha60290021XcHideMode HideModeGet(AtModuleXc self)
    {
    return mThis(self)->hideMode;
    }

static uint32 StsDefaultOffset(ThaModuleXc self, AtSdhChannel sdhChannel, uint8 stsId)
    {
    uint8 hwSlice, hwSts;

    if (ThaSdhChannelHwStsGet(sdhChannel, cAtModuleXc, stsId, &hwSlice, &hwSts) == cAtOk)
        return ThaModuleXcBaseAddress(self) + 256UL * hwSlice + hwSts;

    return cBit31_0;
    }

static void RegsDisplay(ThaModuleXc self, AtSdhChannel sdhChannel, uint32 regAddress, const char* regName)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 address;

        address = regAddress + StsDefaultOffset(self, sdhChannel, (uint8)(startSts + sts_i));
        ThaDeviceChannelRegValueDisplay(channel, address, cThaModuleOcn);
        }
    }

static void SdhChannelRegsShow(ThaModuleXc self, AtSdhChannel sdhChannel)
    {
    uint8 slice, hwSts, sts1;

    if (!AtSdhChannelIsHoVc(sdhChannel))
        return;

    sts1 = AtSdhChannelSts1Get(sdhChannel);
    ThaSdhChannel2HwMasterStsId(sdhChannel, cAtModuleXc, &slice, &hwSts);
    AtPrintc(cSevInfo,   "* SW-STS    Slice-oc48    HW-STS(0-48)\r\n");
    AtPrintc(cSevNormal, "%6u    %5u         %6u\r\n", sts1, slice, hwSts);

    RegsDisplay(self, sdhChannel, cAf6Reg_sxcramctl0_Base, "OCN SXC Control 1 - Page 0 & Page 1");
    RegsDisplay(self, sdhChannel, cAf6Reg_sxcramctl1_Base, "OCN SXC Control 2 - Page 2");
    RegsDisplay(self, sdhChannel, cAf6Reg_apsramctl_Base,  "OCN SXC Control 3 - Config APS");
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290021ModuleXc* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(hideMode);
    }

static void OverrideAtObject(AtModuleXc self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModuleXc self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, RoleGet);
        mMethodOverride(m_AtModuleOverride, RoleSet);
        mMethodOverride(m_AtModuleOverride, HasRole);
        mMethodOverride(m_AtModuleOverride, RoleInputEnable);
        mMethodOverride(m_AtModuleOverride, RoleActiveForce);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleXc(AtModuleXc self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleXcMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleXcOverride, m_AtModuleXcMethods, sizeof(m_AtModuleXcOverride));

        mMethodOverride(m_AtModuleXcOverride, VcCrossConnectObjectCreate);
        }

    mMethodsSet(self, &m_AtModuleXcOverride);
    }

static void OverrideThaModuleXc(AtModuleXc self)
    {
    ThaModuleXc thaModuleXc = (ThaModuleXc)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleXcOverride, mMethodsGet(thaModuleXc), sizeof(m_ThaModuleXcOverride));

        mMethodOverride(m_ThaModuleXcOverride, SdhChannelRegsShow);
        }

    mMethodsSet(thaModuleXc, &m_ThaModuleXcOverride);
    }

static void OverrideTha60210011ModuleXc(AtModuleXc self)
    {
    Tha60210011ModuleXc module = (Tha60210011ModuleXc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleXcMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleXcOverride, m_Tha60210011ModuleXcMethods, sizeof(m_Tha60210011ModuleXcOverride));

        mMethodOverride(m_Tha60210011ModuleXcOverride, HwSliceIdGet);
        mMethodOverride(m_Tha60210011ModuleXcOverride, StartHoLineId);
        mMethodOverride(m_Tha60210011ModuleXcOverride, RxBrigdeAndRollSxcReg);
        mMethodOverride(m_Tha60210011ModuleXcOverride, TxBrigdeAndRollSxcReg);
        mMethodOverride(m_Tha60210011ModuleXcOverride, TxSxcLineIdMask);
        mMethodOverride(m_Tha60210011ModuleXcOverride, TxSxcLineIdShift);
        }

    mMethodsSet(module, &m_Tha60210011ModuleXcOverride);
    }

static void Override(AtModuleXc self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleXc(self);
    OverrideThaModuleXc(self);
    OverrideTha60210011ModuleXc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModuleXc);
    }

AtModuleXc Tha60290021ModuleXcObjectInit(AtModuleXc self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleXcObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleXc Tha60290021ModuleXcNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleXc newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModuleXcObjectInit(newModule, device);
    }

eAtRet Tha60290021ModuleXcHideModeSet(AtModuleXc self, eTha60290021XcHideMode hideMode)
    {
    if (self)
        return HideModeSet(self, hideMode);
    return cAtErrorNullPointer;
    }

eTha60290021XcHideMode Tha60290021ModuleXcHideModeGet(AtModuleXc self)
    {
    if (self)
        return HideModeGet(self);
    return cTha60290021XcHideModeNone;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : XC
 * 
 * File        : Tha60290021HoVcCrossConnectInternal.h
 * 
 * Created Date: Aug 1, 2017
 *
 * Description : HO VC cross-connection
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021HOVCCROSSCONNECTINTERNAL_H_
#define _THA60290021HOVCCROSSCONNECTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/xc/ThaVcCrossConnectInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021HoVcCrossConnect
    {
    tThaVcCrossConnect super;
    }tTha60290021HoVcCrossConnect;

/*--------------------------- Forward declarations ---------------------------*/
AtCrossConnect Tha60290021HoVcCrossConnectObjectInit(AtCrossConnect self, AtModuleXc module);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290021HOVCCROSSCONNECTINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60290021ModuleRam.h
 * 
 * Created Date: Aug 12, 2016
 *
 * Description : RAM module of product 60290021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THA60290021MODULERAMINTERNAL_H_
#define THA60290021MODULERAMINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60290021ModuleRam.h"
#include "../../Tha60210011/ram/Tha60210011ModuleRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModuleRam
    {
    tTha60210011ModuleRam super;
    }tTha60290021ModuleRam;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha60290021ModuleRamObjectInit(AtModuleRam self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* THA60290021MODULERAMINTERNAL_H_ */


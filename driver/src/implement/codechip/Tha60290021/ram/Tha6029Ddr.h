/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha6029Ddr.h
 * 
 * Created Date: Sep 25, 2016
 *
 * Description : DDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029DDR_H_
#define _THA6029DDR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRam Tha6029Ddr32BitNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha6029Ddr64BitNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029DDR_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60290021ModuleRam.h
 * 
 * Created Date: Aug 12, 2016
 *
 * Description : RAM module of product 60290021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THA60290021MODULERAM_H_
#define THA60290021MODULERAM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ram/ThaModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleRam Tha60290021ModuleRamNew(AtDevice device);

eBool Tha60290021ModuleRamCalibDone(AtRam ram, uint32 localAddress, uint32 mask);
void Tha60290021ModuleRamResetStatusShow(AtRam ram, uint32 localAddress, uint32 mask);
AtInternalRam Tha60290021InternalRamOcnNew(AtModule phyModule, uint32 ramId, uint32 localId);

#ifdef __cplusplus
}
#endif
#endif /* THA60290021MODULERAM_H_ */


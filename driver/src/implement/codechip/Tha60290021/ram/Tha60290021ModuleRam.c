/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290021ModuleRam.c
 *
 * Created Date: Aug 12, 2016
 *
 * Description : RAM module of product 60290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/ram/AtRamInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../man/Tha60290021Device.h"
#include "Tha60290021ModuleRamInternal.h"
#include "Tha6029Qdr.h"
#include "Tha6029Ddr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleRamMethods  m_AtModuleRamOverride;
static tThaModuleRamMethods m_ThaModuleRamOverride;

/* Save super implementation */
static const tAtModuleRamMethods   *m_AtModuleRamMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool RunningOnOtherPlatform(AtModuleRam self)
    {
    return Tha60290021DeviceIsRunningOnOtherPlatform(AtModuleDeviceGet((AtModule)self));
    }

static AtRam DdrCreate(AtModuleRam self, AtIpCore core, uint8 ddrId)
    {
    if (RunningOnOtherPlatform(self))
        return m_AtModuleRamMethods->DdrCreate(self, core, ddrId);

    switch (ddrId)
        {
        case 0: return Tha6029Ddr64BitNew(self, core, ddrId);
        case 1: return Tha6029Ddr64BitNew(self, core, ddrId);
        case 2: return Tha6029Ddr32BitNew(self, core, ddrId);
        case 3: return Tha6029Ddr32BitNew(self, core, ddrId);
        default:
            return NULL;
        }
    }

static uint8 NumDdrGet(AtModuleRam self)
    {
    if (RunningOnOtherPlatform(self))
        return m_AtModuleRamMethods->NumDdrGet(self);

    AtUnused(self);
    return 4;
    }

static uint8 NumQdrGet(AtModuleRam self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 DdrUserClock(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 300000000;
    }

static const char *DdrTypeString(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return "DDR4";
    }

static AtRam QdrCreate(AtModuleRam self, AtIpCore core, uint8 qdrId)
    {
    if (RunningOnOtherPlatform(self))
        return m_AtModuleRamMethods->QdrCreate(self, core, qdrId);
    return Tha6029QdrNew(self, core, qdrId);
    }

static eBool InternalRamMonitoringIsSupported(ThaModuleRam self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void StickyDebug(ThaModuleRam self)
    {
    AtUnused(self);
    }

static eBool DdrIsUsed(AtModuleRam self, uint8 ddrId)
    {
    /* Although there are 4 DDRs but at this time, only 2 of them are used.
     * Handle this to avoid necessary access to hardware */
    AtUnused(self);
    return (ddrId < 2) ? cAtTrue : cAtFalse;
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, m_AtModuleRamMethods, sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, DdrCreate);
        mMethodOverride(m_AtModuleRamOverride, NumDdrGet);
        mMethodOverride(m_AtModuleRamOverride, NumQdrGet);
        mMethodOverride(m_AtModuleRamOverride, DdrIsUsed);
        mMethodOverride(m_AtModuleRamOverride, QdrCreate);
        mMethodOverride(m_AtModuleRamOverride, DdrTypeString);
        mMethodOverride(m_AtModuleRamOverride, DdrUserClock);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void OverrideThaModuleRam(AtModuleRam self)
    {
    ThaModuleRam ramModule = (ThaModuleRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleRamOverride, mMethodsGet(ramModule), sizeof(m_ThaModuleRamOverride));

        mMethodOverride(m_ThaModuleRamOverride, InternalRamMonitoringIsSupported);
        mMethodOverride(m_ThaModuleRamOverride, StickyDebug);
        }

    mMethodsSet(ramModule, &m_ThaModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModuleRam(self);
    OverrideThaModuleRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModuleRam);
    }

AtModuleRam Tha60290021ModuleRamObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleRamObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha60290021ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModuleRamObjectInit(newModule, device);
    }

eBool Tha60290021ModuleRamCalibDone(AtRam ram, uint32 localAddress, uint32 mask)
    {
    static const uint32 cTimeoutMs = 5000;
    uint32 regAddr = cTopBaseAddress + localAddress;
    AtDevice device = AtModuleDeviceGet((AtModule)AtRamModuleGet(ram));
    eBool isGood = ThaDeviceStickyIsGood(device, regAddr, mask, cTimeoutMs);

    if (AtRamIsSimulated(ram))
        return cAtTrue;

    return isGood;
    }

void Tha60290021ModuleRamResetStatusShow(AtRam ram, uint32 localAddress, uint32 mask)
    {
    uint32 regAddr = cTopBaseAddress + localAddress;
    uint32 regVal = AtRamRead(ram, regAddr);
    uint32 reset = regVal & mask;
    AtRamWrite(ram, regAddr, mask);
    AtPrintc(cSevNormal, "* User reset: ");
    AtPrintc(reset ? cSevCritical : cSevInfo, "%s\r\n", reset ? "yes" : "no");
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha6029Qdr.c
 *
 * Created Date: Sep 25, 2016
 *
 * Description : QDR
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ram/Tha60210011QdrInternal.h"
#include "../man/Tha60290021DeviceReg.h"
#include "Tha6029Qdr.h"
#include "Tha60290021ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6029Qdr
    {
    tTha60210011Qdr super;
    }tTha6029Qdr;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtRamMethods          m_AtRamOverride;
static tTha60210011QdrMethods m_Tha60210011QdrOverride;

/* Save super implementation */
static const tAtRamMethods *m_AtRamMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldClearStateMachineBeforeAccessing(AtRam self)
    {
    /* As result from testbench, this is necessary */
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleRamRet InitStatusGet(AtRam self)
    {
    eBool calibDone = Tha60290021ModuleRamCalibDone(self, cAf6Reg_i_sticky0_Base, cAf6_i_sticky0_QdrCalib_Mask);
    return calibDone ? cAtOk : cAtErrorQdrCalibFail;
    }

static void Debug(AtRam self)
    {
    m_AtRamMethods->Debug(self);
    Tha60290021ModuleRamResetStatusShow(self, cAf6Reg_i_sticky0_Base, cAf6_i_sticky0_QdrUrst_Mask);
    }

static uint32 BaseAddress(Tha60210011Qdr self)
    {
    AtUnused(self);
    return 0xF28800;
    }

static void OverrideTha60210011Qdr(AtRam self)
    {
    Tha60210011Qdr qdr = (Tha60210011Qdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011QdrOverride, mMethodsGet(qdr), sizeof(m_Tha60210011QdrOverride));

        mMethodOverride(m_Tha60210011QdrOverride, BaseAddress);
        }

    mMethodsSet(qdr, &m_Tha60210011QdrOverride);
    }

static void OverrideAtRam(AtRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamOverride, m_AtRamMethods, sizeof(m_AtRamOverride));

        mMethodOverride(m_AtRamOverride, InitStatusGet);
        mMethodOverride(m_AtRamOverride, Debug);
        mMethodOverride(m_AtRamOverride, ShouldClearStateMachineBeforeAccessing);
        }

    mMethodsSet(self, &m_AtRamOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtRam(self);
    OverrideTha60210011Qdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029Qdr);
    }

static AtRam ObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011QdrObjectInit(self, ramModule, core, ramId) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtRam Tha6029QdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRam newDdr = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDdr == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newDdr, ramModule, core, ramId);
    }

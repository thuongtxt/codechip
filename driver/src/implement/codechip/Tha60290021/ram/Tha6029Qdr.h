/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha6029Qdr.h
 * 
 * Created Date: Sep 25, 2016
 *
 * Description : QDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029QDR_H_
#define _THA6029QDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtRam.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRam Tha6029QdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029QDR_H_ */


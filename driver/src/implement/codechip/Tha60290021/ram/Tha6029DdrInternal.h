/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha6029Ddr.h
 * 
 * Created Date: Sep 25, 2016
 *
 * Description : DDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029DDRINTERNAL_H_
#define _THA6029DDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/ram/Tha6021DdrInternal.h"
#include "Tha6029Ddr.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029Ddr
    {
    tTha6021Ddr super;

    /* Private */
    uint8 cellSize;
    }tTha6029Ddr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRam Tha6029DdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId, uint8 cellSize);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029DDRINTERNAL_H_ */


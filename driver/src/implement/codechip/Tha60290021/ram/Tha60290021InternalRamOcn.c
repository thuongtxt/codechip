/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290021InternalRamOcn.c
 *
 * Created Date: Apr 14, 2017
 *
 * Description : Implementation for OCN internal RAM functions
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ocn/Tha60290021ModuleOcn.h"
#include "Tha60290021InternalRamInternal.h"
#include "Tha60290021ModuleRam.h"

/*--------------------------- Define -----------------------------------------*/
#define cLine_Side_OCN_Tx_J0_Insertion_Buffer_0_Id                32
#define cOCN_RXPP_Per_STS_payload_Control_0_Id                    49
#define cOCN_SXC_Control_1_Config_Page_0_and_Page_1_of_SXC_0_Id    69
#define cOCN_SXC_Control_2_Config_Page_2_of_SXC_0_Id               81
#define cOCN_SXC_Control_3_Config_APS_0_Id                         93


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override. */
static tThaInternalRamMethods m_ThaInternalRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtInternalRam self)
    {
    return ThaModuleOcnBaseAddress((ThaModuleOcn)AtInternalRamPhyModuleGet(self));
    }

static uint32 NumRams(ThaInternalRam self)
    {
    AtModule ocn = AtInternalRamPhyModuleGet((AtInternalRam)self);
    uint32 numRams = 0;
    Tha60290021ModuleOcnAllInternalRamsDescription(ocn, &numRams);
    return numRams;
    }

static uint32 ErrorForceRegister(ThaInternalRam self)
    {
    AtInternalRam internalRam = (AtInternalRam)self;
    uint32 localId = AtInternalRamLocalIdGet(internalRam);

    if (localId < cLine_Side_OCN_Tx_J0_Insertion_Buffer_0_Id)
        return BaseAddress(internalRam) + 0xF020;

    if (localId < cOCN_RXPP_Per_STS_payload_Control_0_Id)
        return BaseAddress(internalRam) + 0x20020;

    if (localId < cOCN_SXC_Control_1_Config_Page_0_and_Page_1_of_SXC_0_Id)
        return BaseAddress(internalRam) + 0xF0020;

    if (localId < cOCN_SXC_Control_2_Config_Page_2_of_SXC_0_Id)
        return BaseAddress(internalRam) + 0xF0024;

    if (localId < cOCN_SXC_Control_3_Config_APS_0_Id)
        return BaseAddress(internalRam) + 0xF0028;

    if (localId < NumRams(self))
        return BaseAddress(internalRam) + 0xF002C;

    return cInvalidUint32;
    }

static uint32 LocalIdMask(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);

    if (localId < cLine_Side_OCN_Tx_J0_Insertion_Buffer_0_Id)
        return cBit0 << localId;

    if (localId < cOCN_RXPP_Per_STS_payload_Control_0_Id)
        return cBit0 << (localId - cLine_Side_OCN_Tx_J0_Insertion_Buffer_0_Id);

    if (localId < cOCN_SXC_Control_1_Config_Page_0_and_Page_1_of_SXC_0_Id)
        return cBit0 << (localId - cOCN_RXPP_Per_STS_payload_Control_0_Id);

    if (localId < cOCN_SXC_Control_2_Config_Page_2_of_SXC_0_Id)
        return cBit0 << (localId - cOCN_SXC_Control_1_Config_Page_0_and_Page_1_of_SXC_0_Id);

    if (localId < cOCN_SXC_Control_3_Config_APS_0_Id)
        return cBit0 << (localId - cOCN_SXC_Control_2_Config_Page_2_of_SXC_0_Id);

    if (localId < NumRams(self))
        return cBit0 << (localId - cOCN_SXC_Control_3_Config_APS_0_Id);

    return 0;
    }

static uint32 HoSliceIdGet(ThaInternalRam self, uint32 offset)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);
    return (uint32)((localId - offset) % 8);
    }

static uint32 FirstCellOffset(ThaInternalRam self)
    {
    uint32 localId = AtInternalRamLocalIdGet((AtInternalRam)self);

    if (AtInternalRamIsReserved((AtInternalRam)self))
        return cInvalidUint32;

    /* OCN Rx Bridge and Roll SXC Control - TFI5 Side */
    if (localId < 8)
        return (uint32)(HoSliceIdGet(self, 0) * 0x100 + 0x6000);

    /* OCN Tx Bridge and Roll SXC Control - TFI5 Side */
    if (localId < 16)
        return (uint32)(HoSliceIdGet(self, 8) * 0x100 + 0x7000);

    /* OCN STS Pointer Interpreter Per Channel Control - TFI5 Side */
    if (localId < 24)
        return (uint32)(HoSliceIdGet(self, 16) * 0x200 + 0x2000);

    /* OCN STS Pointer Generator Per Channel Control - TFI5 Side */
    if (localId < 32)
        return (uint32)(HoSliceIdGet(self, 24) * 0x200 + 0x3000);

    /* OCN Tx J0 Insertion Buffer - line side */
    if (localId < 40)
        return (uint32)(((localId - 32) % 8) * 0x100 + 0x21010);

    /* OCN STS Pointer Interpreter Per Channel Control - line side */
    if (localId < 44)
        return (uint32)(HoSliceIdGet(self, 40) * 0x200 + 0x22000);

    /* OCN STS Pointer Generator Per Channel Control - line side */
    if (localId < 48)
        return (uint32)(HoSliceIdGet(self, 44) * 0x200 + 0x23000);

    /* OCN TOH Monitoring Per Line Control */
    if (localId < 49)
        return (uint32)(HoSliceIdGet(self, 48) + 0x27010);

    /* OCN RXPP Per STS payload Control */
    if (localId < 53)
        return (uint32)(HoSliceIdGet(self, 49) * 0x4000 + 0x60000);

    /* OCN VTTU Pointer Interpreter Per Channel Control */
    if (localId < 57)
        return (uint32)(HoSliceIdGet(self, 53) * 0x4000 + 0x60800);

    /* OCN TXPP Per STS Multiplexing Control */
    if (localId < 61)
        return (uint32)(HoSliceIdGet(self, 57) * 0x4000 + 0x80000);

    /* OCN VTTU Pointer Generator Per Channel Control */
    if (localId < 65)
        return (uint32)(HoSliceIdGet(self, 61) * 0x4000 + 0x80800);

    /* OCN Rx High Order Map concatenate configuration */
    if (localId < 69)
        return (uint32)(HoSliceIdGet(self, 65) * 0x200 + 0x48000);

    /* OCN SXC Control 1 - Config Page 0 and Page 1 of SXC */
    if (localId < 81)
        return (uint32)((localId - 69) * 0x100 + 0x44000);

    /* OCN SXC Control 2 - Config Page 2 of SXC */
    if (localId < 93)
        return (uint32)((localId - 81) * 0x100 + 0x44040);

    /* OCN SXC Control 3 - Config APS */
    if (localId < 105)
        return (uint32)((localId - 93) * 0x100 + 0x44080);

    return cInvalidUint32;
    }

static uint32 FirstCellAddress(ThaInternalRam self)
    {
    uint32 offset = FirstCellOffset(self);

    if (offset == cInvalidUint32)
        return cInvalidUint32;

    return (uint32)(BaseAddress((AtInternalRam)self) + offset);
    }

static void OverrideThaInternalRam(AtInternalRam self)
    {
    ThaInternalRam ram = (ThaInternalRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInternalRamOverride, mMethodsGet(ram), sizeof(m_ThaInternalRamOverride));

        mMethodOverride(m_ThaInternalRamOverride, ErrorForceRegister);
        mMethodOverride(m_ThaInternalRamOverride, LocalIdMask);
        mMethodOverride(m_ThaInternalRamOverride, FirstCellAddress);
        }

    mMethodsSet(ram, &m_ThaInternalRamOverride);
    }

static void Override(AtInternalRam self)
    {
    OverrideThaInternalRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021InternalRamOcn);
    }

 AtInternalRam Tha60290021InternalRamOcnObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInternalRamObjectInit(self, phyModule, ramId, localId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInternalRam Tha60290021InternalRamOcnNew(AtModule phyModule, uint32 ramId, uint32 localId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInternalRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newRam == NULL)
        return NULL;

    return Tha60290021InternalRamOcnObjectInit(newRam, phyModule, ramId, localId);
    }

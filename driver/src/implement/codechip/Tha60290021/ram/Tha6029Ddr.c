/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha6029Ddr.c
 *
 * Created Date: Sep 25, 2016
 *
 * Description : DDR
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../man/Tha60290021DeviceReg.h"
#include "Tha60290021ModuleRam.h"
#include "Tha6029DdrInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6029Ddr *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtRamMethods    m_AtRamOverride;
static tThaDdrMethods   m_ThaDdrOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtRamMethods    *m_AtRamMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StickyMask(AtRam self)
    {
    return cBit0 << AtRamIdGet(self);
    }

static eAtModuleRamRet InitStatusGet(AtRam self)
    {
    eBool calibDone = Tha60290021ModuleRamCalibDone(self, cAf6Reg_i_sticky0_Base, StickyMask(self));
    return calibDone ? cAtOk : cAtErrorDdrCalibFail;
    }

static uint32 DefaultOffset(ThaDdr self)
    {
    return (0x2000UL * AtRamIdGet((AtRam)self));
    }

static uint32 CellSizeGet(AtRam self)
    {
    return mThis(self)->cellSize;
    }

static uint32 UserResetMask(AtRam self)
    {
    return cAf6_i_sticky0_Ddr41Urst_Mask << (AtRamIdGet(self));
    }

static void Debug(AtRam self)
    {
    m_AtRamMethods->Debug(self);
    Tha60290021ModuleRamResetStatusShow(self, cAf6Reg_i_sticky0_Base, UserResetMask(self));
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha6029Ddr *object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(cellSize);
    }

static void OverrideAtObject(AtRam self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtRam(AtRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtRamMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamOverride, m_AtRamMethods, sizeof(m_AtRamOverride));

        mMethodOverride(m_AtRamOverride, InitStatusGet);
        mMethodOverride(m_AtRamOverride, CellSizeGet);
        mMethodOverride(m_AtRamOverride, Debug);
        }

    mMethodsSet(self, &m_AtRamOverride);
    }

static void OverrideThaDdr(AtRam self)
    {
    ThaDdr ram = (ThaDdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDdrOverride, mMethodsGet(ram), sizeof(m_ThaDdrOverride));

        mMethodOverride(m_ThaDdrOverride, DefaultOffset);
        }

    mMethodsSet(ram, &m_ThaDdrOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtObject(self);
    OverrideAtRam(self);
    OverrideThaDdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029Ddr);
    }

AtRam Tha6029DdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId, uint8 cellSize)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6021DdrObjectInit(self, ramModule, core, ramId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->cellSize = cellSize;

    return self;
    }

static AtRam Tha6029DdrNewWithCellSize(AtModuleRam ramModule, AtIpCore core, uint8 ramId, uint8 cellSize)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRam newRam = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    return Tha6029DdrObjectInit(newRam, ramModule, core, ramId, cellSize);
    }

AtRam Tha6029Ddr32BitNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    return Tha6029DdrNewWithCellSize(ramModule, core, ramId, 1);
    }

AtRam Tha6029Ddr64BitNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    return Tha6029DdrNewWithCellSize(ramModule, core, ramId, 2);
    }

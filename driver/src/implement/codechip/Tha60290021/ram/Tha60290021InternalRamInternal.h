/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc..c'
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60290021InternalRamInternal.h
 *
 * Created Date: Aug 21, 2017
 *
 * Description : Tha60290021InternalRam declarations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021INTERNALRAMINTERNAL_H_
#define _THA60290021INTERNALRAMINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ram/ThaInternalRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021InternalRamOcn * Tha60290021InternalRamOcn;

typedef struct tTha60290021InternalRamOcn 
    {
    tThaInternalRam super;
    }tTha60290021InternalRamOcn;
    
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInternalRam Tha60290021InternalRamOcnObjectInit(AtInternalRam self, AtModule phyModule, uint32 ramId, uint32 localId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021INTERNALRAMINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6029SgmiiSerdesPrbsEngine.c
 *
 * Created Date: Sep 24, 2016
 *
 * Description : SGMII SERDES PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../eth/Tha6029SerdesSgmiiDiagReg.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "gating/Tha60290021PrbsGatingTimer.h"
#include "Tha60290021ModulePrbs.h"
#include "Tha6029SgmiiSerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6029SgmiiSerdesPrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6029SgmiiSerdesPrbsEngineMethods m_methods;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController Serdes(AtPrbsEngine self)
    {
    return Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    }

static uint32 HwId(Tha6029SgmiiSerdesPrbsEngine self)
    {
    return AtSerdesControllerHwIdGet(Serdes((AtPrbsEngine)self));
    }

static uint32 BaseAddress(AtPrbsEngine self)
    {
    AtUnused(self);
    return Tha60290021ModuleEthSgmiiDiagnosticBaseAddress();
    }

static uint32 Offset(AtPrbsEngine self)
    {
    return mMethodsGet(mThis(self))->HwId(mThis(self)) * 16;
    }

static uint32 AbsoluteOffset(AtPrbsEngine self)
    {
    return Offset(self) + BaseAddress(self);
    }

static uint32 AddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    return AbsoluteOffset(self) + localAddress;
    }

static uint32 ControlRegister(AtPrbsEngine self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_control_pen1_Base);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = ControlRegister(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_control_pen1_Test_enable_, enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr = ControlRegister(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & cAf6_control_pen1_Test_enable_Mask) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet BurstNumPacketsSet(AtPrbsEngine self, uint32 numPackets)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_control_pen2_Base);
    AtPrbsEngineWrite(self, regAddr, numPackets, cAtModulePrbs);
    return cAtOk;
    }

static uint32 ControlPen3Register(AtPrbsEngine self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_control_pen3_Base);
    }

static eAtRet TestInit(AtPrbsEngine self)
    {
    uint32 regAddr = ControlPen3Register(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    mRegFieldSet(regVal, cAf6_control_pen3_Configure_test_mode_, 0); /* Continuous */

    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr = ControlPen3Register(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_control_pen3_Force_error_, force ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddr = ControlPen3Register(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & cAf6_control_pen3_Force_error_Mask) ? cAtTrue : cAtFalse;
    }

static eBool ErrorForcingIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 ModeSw2Hw(eAtPrbsMode hwMode)
    {
    switch ((uint32)hwMode)
        {
        case cAtPrbsModePrbs7                 : return 0;
        case cAtPrbsModePrbs15                : return 1;
        case cAtPrbsModePrbs23                : return 2;
        case cAtPrbsModePrbs31                : return 3;
        case cAtPrbsModePrbsFixedPattern1Byte : return 4;
        default:
            return 0;
        }
    }

static eAtPrbsMode ModeHw2Sw(uint32 hwMode)
    {
    switch (hwMode)
        {
        case 0 : return cAtPrbsModePrbs7;
        case 1 : return cAtPrbsModePrbs15;
        case 2 : return cAtPrbsModePrbs23;
        case 3 : return cAtPrbsModePrbs31;
        case 4 : return cAtPrbsModePrbsFixedPattern1Byte;
        default:
            return cAtPrbsModeInvalid;
        }
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr = ControlPen3Register(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_control_pen3_Data_mode_, ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    uint32 regAddr = ControlPen3Register(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return ModeHw2Sw(mRegField(regVal, cAf6_control_pen3_Data_mode_));
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    uint32 regAddr = ControlPen3Register(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_control_pen3_Fix_data_value_, fixedPattern);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    uint32 regAddr = ControlPen3Register(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return mRegField(regVal, cAf6_control_pen3_Fix_data_value_);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    switch ((uint32)prbsMode)
        {
        case cAtPrbsModePrbs7                 : return cAtTrue;
        case cAtPrbsModePrbs15                : return cAtTrue;
        case cAtPrbsModePrbs23                : return cAtTrue;
        case cAtPrbsModePrbs31                : return cAtTrue;
        case cAtPrbsModePrbsFixedPattern1Byte : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eAtModulePrbsRet DefaultSet(AtPrbsEngine self)
    {
    eAtRet ret = cAtOk;

    ret |= BurstNumPacketsSet(self, 0);
    ret |= TestInit(self);
    ret |= AtPrbsEngineModeSet(self, cAtPrbsModePrbs31);

    return ret;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet(self);
    }

static uint32 StickyRead2Clear(AtPrbsEngine self, eBool read2Clear)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_Alarm_sticky_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    if (read2Clear)
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return regVal;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool read2Clear)
    {
    uint32 hwAlarms = StickyRead2Clear(self, read2Clear);
    uint32 alarms = 0;

    if (hwAlarms & cAf6_Alarm_sticky_Mon_data_error_Mask)
        alarms |= cAtPrbsEngineAlarmTypeError;

    return alarms;
    }

static void HwAlarmDetailShow(AtPrbsEngine self, uint32 hwAlarm, const char *title)
    {
    AtUnused(self);

    AtPrintc(cSevNormal, "* %s: ", title);

    if (hwAlarm & cAf6_Alarm_sticky_Mon_packet_error_Mask)
        AtPrintc(cSevCritical, "packet_error ");
    if (hwAlarm & cAf6_Alarm_sticky_Mon_packet_len_error_Mask)
        AtPrintc(cSevCritical, "packet_len_error ");
    if (hwAlarm & cAf6_Alarm_sticky_Mon_FCS_error_Mask)
        AtPrintc(cSevCritical, "fcs_error ");
    if (hwAlarm & cAf6_Alarm_sticky_Mon_data_error_Mask)
        AtPrintc(cSevCritical, "data_error ");
    if (hwAlarm & cAf6_Alarm_sticky_Mon_MAC_header_error_Mask)
        AtPrintc(cSevCritical, "MAC_header_error ");
    }

static uint32 AlarmRegister(AtPrbsEngine self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_Alarm_status_Base);
    }

static const char *BandwidthString(uint32 hwValue)
    {
    switch (hwValue)
        {
        case 0: return "100%";
        case 1: return "90%";
        case 2: return "80%";
        case 3: return "70%";
        default: return "unknown";
        }
    }

static void HwConfigureShow(AtPrbsEngine self)
    {
    uint32 regAddr = ControlPen3Register(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    AtPrintc(cSevNormal, "* HW configure detail: \r\n");
    AtPrintc(cSevNormal, "  - Test mode: %s\r\n", (regVal & cAf6_control_pen3_Configure_test_mode_Mask) ? "Continuous" : "Burst");
    AtPrintc(cSevNormal, "  - FCS invert: %s\r\n", (regVal & cAf6_control_pen3_Invert_FCS_enable_Mask) ? "Yes" : "No");
    AtPrintc(cSevNormal, "  - Bandwidth: %s\r\n", BandwidthString(mRegField(regVal, cAf6_control_pen3_Band_width_)));
    AtPrintc(cSevNormal, "  - Payload: %d (bytes)\r\n", mRegField(regVal, cAf6_control_pen3_Payload_size_));
    }

static void Debug(AtPrbsEngine self)
    {
    m_AtPrbsEngineMethods->Debug(self);

    HwConfigureShow(self);
    HwAlarmDetailShow(self, StickyRead2Clear(self, cAtTrue), "Sticky");
    HwAlarmDetailShow(self, AtPrbsEngineRead(self, AlarmRegister(self), cAtModulePrbs), "Alarm");
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 hwAlarms  = AtPrbsEngineRead(self, AlarmRegister(self), cAtModulePrbs);
    uint32 errorMask = cAf6_Alarm_status_Mon_data_error_Mask;
    uint32 syncMask  = cAf6_Alarm_status_Mon_Data_sync_Mask;

    if ((hwAlarms & syncMask) == 0)
        return cAtPrbsEngineAlarmTypeLossSync;

    if (hwAlarms & errorMask)
        return cAtPrbsEngineAlarmTypeError;

    return cAtPrbsEngineAlarmTypeNone;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static uint32 CounterLocalAddress(uint16 counterType, eBool r2c)
    {
    switch (counterType)
        {
        case cAtPrbsEngineCounterRxBitError: return r2c ? cAf6Reg_counter8_Base : cAf6Reg_counter7_Base;
        case cAtPrbsEngineCounterTxFrame   : return r2c ? cAf6Reg_counter1_Base : cAf6Reg_counter4_Base;
        case cAtPrbsEngineCounterRxFrame   : return r2c ? cAf6Reg_counter2_Base : cAf6Reg_counter5_Base;
        default:
            return cInvalidUint32;
        }
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    if (CounterLocalAddress(counterType, cAtTrue) == cInvalidUint32)
        return cAtFalse;
    return cAtTrue;
    }

static uint32 CounterRead2Clear(AtPrbsEngine self, uint16 counterType, eBool read2Clear)
    {
    uint32 regAddr = AddressWithLocalAddress(self, CounterLocalAddress(counterType, read2Clear));
    if (regAddr == cInvalidUint32)
        return 0;
    return AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static AtTimer TimerObjectCreate(AtPrbsEngine self)
    {
    Tha60290021PrbsGatingFactory factory = Tha6029SerdesPrbsEnginePrbsGatingFactory((Tha6029SerdesPrbsEngine)self);
    return Tha60290021PrbsGatingFactorySgmiiSerdesTimerCreate(factory, self);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, Debug);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForcingIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, TimerObjectCreate);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static void MethodsInit(AtPrbsEngine self)
    {
    Tha6029SgmiiSerdesPrbsEngine engine = (Tha6029SgmiiSerdesPrbsEngine)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HwId);
        }

    mMethodsSet(engine, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SgmiiSerdesPrbsEngine);
    }

AtPrbsEngine Tha6029SgmiiSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6029SgmiiSerdesPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6029SgmiiSerdesPrbsEngineObjectInit(newEngine, serdesController);
    }

uint32 Tha6029SgmiiSerdesPrbsEngineAddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    if (self)
        return AddressWithLocalAddress(self, localAddress);
    return cInvalidUint32;
    }

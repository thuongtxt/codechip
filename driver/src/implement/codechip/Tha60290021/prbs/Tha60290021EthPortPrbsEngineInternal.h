/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290021EthPortPrbsEngineInternal.h
 * 
 * Created Date: Sep 23, 2016
 *
 * Description : ETH Port PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021ETHPORTPRBSENGINEINTERNAL_H_
#define _THA60290021ETHPORTPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"
#include "Tha6029SerdesPrbsEngineInternal.h"
#include "../../../../generic/man/AtModuleInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021EthPortPrbsEngine
    {
    tAtPrbsEngine super;

    /* Private data */
    AtEthPort port;
    }tTha60290021EthPortPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60290021ETHPORTPRBSENGINEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290021BackplaneSerdesPrbsEngine.c
 *
 * Created Date: Sep 11, 2016
 *
 * Description : Backplane 40G SERDES PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../physical/Tha6029SerdesEth40GReg.h"
#include "gating/Tha60290021PrbsGatingTimer.h"
#include "Tha6029SerdesPrbsEngineInternal.h"
#include "Tha60290021ModulePrbs.h"
#include "Tha60290021BackplaneSerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtPrbsEngine self)
    {
    AtSerdesController controller = Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    return AtSerdesControllerBaseAddress(controller);
    }

static uint32 AddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    return BaseAddress(self) + localAddress;
    }

static uint32 DiagCtrl0Register(AtPrbsEngine self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_ETH_40G_Diag_ctrl0_Base);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 address = DiagCtrl0Register(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ETH_40G_Diag_ctrl0_diag_ferr_, force ? 1 : 0);
    AtPrbsEngineWrite(self, address, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 address = DiagCtrl0Register(self);
    uint32 regVal = AtPrbsEngineRead(self, address, cAtModuleEth);
    return (regVal & cAf6_ETH_40G_Diag_ctrl0_diag_ferr_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet SerdesReset(AtPrbsEngine self)
    {
    /* Give hardware a moment then reset it to bring it to default safe state */
    AtSerdesController serdes = Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    AtOsalUSleep(1000);
    return AtSerdesControllerReset(serdes);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 address = DiagCtrl0Register(self);
    uint32 regVal  = AtPrbsEngineRead(self, address, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ETH_40G_Diag_ctrl0_diag_enb_, enable ? 1 : 0);
    AtPrbsEngineWrite(self, address, regVal, cAtModuleEth);

    return SerdesReset(self);
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 address = DiagCtrl0Register(self);
    uint32 regVal  = AtPrbsEngineRead(self, address, cAtModuleEth);
    return (regVal & cAf6_ETH_40G_Diag_ctrl0_diag_enb_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    return AtPrbsEngineAlarmHistoryGet(self);
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool read2Clear)
    {
    uint32 regAddr   = DiagCtrl0Register(self);
    uint32 regValue  = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    uint32 error     = regValue & cAf6_ETH_40G_Diag_ctrl0_diag_err_Mask;

    if (read2Clear && error)
        {
        mRegFieldSet(regValue, cAf6_ETH_40G_Diag_ctrl0_diag_err_, 1);
        AtPrbsEngineWrite(self, regAddr, regValue, cAtModuleEth);
        }

    return error ? cAtPrbsEngineAlarmTypeError : cAtPrbsEngineAlarmTypeNone;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    switch ((uint32)prbsMode)
        {
        case cAtPrbsModePrbs31 : return cAtTrue;
        case cAtPrbsModePrbsSeq: return cAtTrue;
        default:                 return cAtFalse;
        }
    }

static uint32 ModeSw2Hw(eAtPrbsMode mode)
    {
    switch ((uint32)mode)
        {
        case cAtPrbsModePrbs31 : return 2;
        case cAtPrbsModePrbsSeq: return 1;
        default:                 return 0;
        }
    }

static eAtPrbsMode ModeHw2Sw(uint32 hwMode)
    {
    switch (hwMode)
        {
        case 2 : return cAtPrbsModePrbs31;
        case 1 : return cAtPrbsModePrbsSeq;
        default: return cAtPrbsModeInvalid;
        }
    }

static eAtModulePrbsRet HwModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr = DiagCtrl0Register(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ETH_40G_Diag_ctrl0_diag_datmod_, ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    if (AtPrbsEngineModeIsSupported(self, prbsMode))
        return HwModeSet(self, prbsMode);
    return cAtErrorModeNotSupport;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    uint32 regAddr = DiagCtrl0Register(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    return ModeHw2Sw(mRegField(regVal, cAf6_ETH_40G_Diag_ctrl0_diag_datmod_));
    }

static eBool SideIsSupported(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSidePsn) ? cAtTrue : cAtFalse);
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    return ModeGet(self);
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    return ModeGet(self);
    }

static void LengthDefaultSet(AtPrbsEngine self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_ETH_40G_Diag_ctrl1_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ETH_40G_Diag_ctrl1_diag_lenmax_, 0x7f);
    mRegFieldSet(regVal, cAf6_ETH_40G_Diag_ctrl1_diag_lenmin_, 0x7f);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    LengthDefaultSet(self);
    AtPrbsEngineErrorForce(self, cAtFalse);
    return AtPrbsEngineModeSet(self, cAtPrbsModePrbs31);
    }

static uint32 CounterLocalAddress(uint16 counterType)
    {
    switch (counterType)
        {
        case cAtPrbsEngineCounterTxFrame: return cAf6Reg_ETH_40G_Diag_TXPKT_Base;
        case cAtPrbsEngineCounterRxFrame: return cAf6Reg_ETH_40G_Diag_RXPKT_Base;
        case cAtPrbsEngineCounterTxBit  : return cAf6Reg_ETH_40G_Diag_TXNOB_Base;
        case cAtPrbsEngineCounterRxBit  : return cAf6Reg_ETH_40G_Diag_RXNOB_Base;
        default:
            return cInvalidUint32;
        }
    }

static uint32 CounterRead2Clear(AtPrbsEngine self, uint16 counterType, eBool read2Clear)
    {
    uint32 regAddr = AddressWithLocalAddress(self, CounterLocalAddress(counterType));
    uint32 regVal;

    AtUnused(read2Clear);

    if (regAddr == cInvalidUint32)
        return 0;

    regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    if ((counterType == cAtPrbsEngineCounterTxBit) ||
        (counterType == cAtPrbsEngineCounterRxBit))
        regVal = regVal * 8;

    return regVal;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    return (CounterLocalAddress(counterType) == cInvalidUint32) ? cAtFalse : cAtTrue;
    }

static AtTimer TimerObjectCreate(AtPrbsEngine self)
    {
    return Tha60290021PrbsGatingFactoryBackplaneSerdesTimerCreate(Tha6029SerdesPrbsEnginePrbsGatingFactory((Tha6029SerdesPrbsEngine)self), self);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, SideIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, TimerObjectCreate);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021BackplaneSerdesPrbsEngine);
    }

AtPrbsEngine Tha60290021BackplaneSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290021BackplaneSerdesPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021BackplaneSerdesPrbsEngineObjectInit(newEngine, serdesController);
    }

uint32 Tha60290021BackplaneSerdesPrbsEngineAddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    if (self)
        return AddressWithLocalAddress(self, localAddress);
    return cInvalidUint32;
    }

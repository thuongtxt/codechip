/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6029XfiSerdesPrbsEngine.c
 *
 * Created Date: Sep 25, 2016
 *
 * Description : Faceplace XFI SERDES PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../physical/Tha60290021SerdesManager.h"
#include "gating/Tha60290021PrbsGatingTimer.h"
#include "Tha60290021ModulePrbs.h"
#include "Tha6029XfiDiagReg.h"
#include "Tha6029XfiSerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6029XfiSerdesPrbsEngine *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtPrbsEngine self)
    {
    return mThis(self)->baseAddress;
    }

static uint32 AddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    return localAddress + BaseAddress(self);
    }

static uint32 ConfigureRegister1(AtPrbsEngine self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_Configure_register_1_Base);
    }

static uint32 ConfigureRegister7(AtPrbsEngine self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_Configure_register_7_Base);
    }

static eAtModulePrbsRet DefaultSet(AtPrbsEngine self)
    {
    uint32 regAddr, regVal;

    regAddr = ConfigureRegister1(self);
    regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_Configure_register_1_cfgdainc_, 0);     /* Fix DMAC */
    mRegFieldSet(regVal, cAf6_Configure_register_1_cfglengthinc_, 1); /* Increase */
    mRegFieldSet(regVal, cAf6_Configure_register_1_cfgnumall_, 1); /* Continuous */
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

    regAddr = ConfigureRegister7(self);
    regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_Configure_register_7_cfgfcsdrop_, 1);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

    return AtPrbsEngineModeSet(self, cAtPrbsModePrbs31);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr = ConfigureRegister1(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_Configure_register_1_cfgins_err_, force ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddr = ConfigureRegister1(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & cAf6_Configure_register_1_cfgins_err_Mask) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = ConfigureRegister1(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_Configure_register_1_cfggenen_, enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr = ConfigureRegister1(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & cAf6_Configure_register_1_cfggenen_Mask) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet(self);
    }

static uint32 ModeSw2Hw(uint32 mode)
    {
    switch (mode)
        {
        case cAtPrbsModePrbsFixedPattern1Byte: return 1;
        case cAtPrbsModePrbs31               : return 2;
        case cAtPrbsModePrbs15               : return 3;
        case cAtPrbsModePrbs7                : return 4;
        default:
            return 3;
        }
    }

static uint32 ModeHw2Sw(uint32 mode)
    {
    switch (mode)
        {
        case 1: return cAtPrbsModePrbsFixedPattern1Byte;
        case 2: return cAtPrbsModePrbs31;
        case 3: return cAtPrbsModePrbs15;
        case 4: return cAtPrbsModePrbs7;
        default:
            return cAtPrbsModeInvalid;
        }
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_Configure_register_4_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_Configure_register_4_cfgdatmod_, ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_Configure_register_4_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return ModeHw2Sw(mRegField(regVal, cAf6_Configure_register_4_cfgdatmod_));
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regAddr = ConfigureRegister7(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_Configure_register_7_cfgdatmod_, ModeSw2Hw(prbsMode));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    uint32 regAddr = ConfigureRegister7(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return ModeHw2Sw(mRegField(regVal, cAf6_Configure_register_7_cfgdatmod_));
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtModulePrbsRet ret = cAtOk;

    ret |= AtPrbsEngineTxModeSet(self, prbsMode);
    ret |= AtPrbsEngineRxModeSet(self, prbsMode);

    return ret;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return AtPrbsEngineRxModeGet(self);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_Configure_register_6_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    if (regVal & cAf6_Configure_register_6_err_sta_Mask)
        return cAtPrbsEngineAlarmTypeLossSync;

    return 0;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool r2c)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_Configure_register_5_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    if (r2c)
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

    if (regVal & cAf6_Configure_register_5_err_stk_Mask)
        return cAtPrbsEngineAlarmTypeLossSync;

    return 0;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static uint32 CounterRead2ClearAddress(AtPrbsEngine self, uint16 counterType, eBool r2c)
    {
    eBool ro = r2c ? cAtFalse : cAtTrue;

    AtUnused(self);

    switch (counterType)
        {
        case cAtPrbsEngineCounterTxFrame    : return ro ? cAf6Reg_Status_register_1_Base : cAf6Reg_Status_register_2_Base;
        case cAtPrbsEngineCounterRxFrame    : return ro ? cAf6Reg_Status_register_3_Base : cAf6Reg_Status_register_14_Base;
        case cAtPrbsEngineCounterRxLostFrame: return ro ? cAf6Reg_Status_register_7_Base : cAf6Reg_Status_register_18_Base;
        default:
            return cInvalidUint32;
        }
    }

static uint32 CounterRead2Clear(AtPrbsEngine self, uint16 counterType, eBool r2c)
    {
    uint32 regAddr = CounterRead2ClearAddress(self, counterType, r2c);

    if (regAddr == cInvalidUint32)
        return 0;

    return AtPrbsEngineRead(self, AddressWithLocalAddress(self, regAddr), cAtModulePrbs);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    if (CounterRead2ClearAddress(self, counterType, cAtTrue) == cInvalidUint32)
        return cAtFalse;
    return cAtTrue;
    }

static AtTimer TimerObjectCreate(AtPrbsEngine self)
    {
    Tha60290021PrbsGatingFactory factory = Tha6029SerdesPrbsEnginePrbsGatingFactory((Tha6029SerdesPrbsEngine)self);
    return Tha60290021PrbsGatingFactoryXfiSerdesTimerCreate(factory, self);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    switch ((uint32) prbsMode)
        {
        case cAtPrbsModePrbs7  : return cAtTrue;
        case cAtPrbsModePrbs15 : return cAtTrue;
        case cAtPrbsModePrbs31 : return cAtTrue;
        default: return cAtFalse;
        }
    }

static eBool SideIsSupported(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSidePsn) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSidePsn) ? cAtOk : cAtErrorNotApplicable;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha6029XfiSerdesPrbsEngine* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(baseAddress);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, SideIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, TimerObjectCreate);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029XfiSerdesPrbsEngine);
    }

AtPrbsEngine Tha6029XfiSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController, uint32 baseAddress)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->baseAddress = baseAddress;

    return self;
    }

AtPrbsEngine Tha6029XfiSerdesPrbsEngineNew(AtSerdesController serdesController, uint32 baseAddress)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6029XfiSerdesPrbsEngineObjectInit(newEngine, serdesController, baseAddress);
    }

uint32 Tha6029XfiSerdesPrbsEngineAddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    if (self)
        return AddressWithLocalAddress(self, localAddress);
    return cInvalidUint32;
    }

uint32 Tha6029XfiSerdesPrbsEngineBaseAddress(AtPrbsEngine self)
    {
    return mThis(self)->baseAddress;
    }

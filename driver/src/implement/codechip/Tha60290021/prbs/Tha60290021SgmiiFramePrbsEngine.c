/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290021SgmiiFramePrbsEngine.c
 *
 * Created Date: Oct 29, 2017
 *
 * Description : SGMII PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthPort.h"
#include "../eth/Tha60290021DccKByteGMIIReg.h"
#include "Tha6029SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021SgmiiFramePrbsEngine
    {
    tTha6029SerdesPrbsEngine super;
    }tTha60290021SgmiiFramePrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController Serdes(AtPrbsEngine self)
    {
    return Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    }

static AtEthPort EthPort(AtPrbsEngine self)
    {
    return (AtEthPort)AtSerdesControllerPhysicalPortGet(Serdes(self));
    }

static uint32 MacAddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    return ThaEthPortMacBaseAddress((ThaEthPort)EthPort(self)) + localAddress;
    }

static uint32 DcckgmiidiagcfgAddress(AtPrbsEngine self)
    {
    return MacAddressWithLocalAddress(self, cAf6Reg_dcckgmiidiagcfg_Base);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr = DcckgmiidiagcfgAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_dcckgmiidiagcfg_DcckGmiiDiagForceDatErr_, force ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddr = DcckgmiidiagcfgAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_dcckgmiidiagcfg_DcckGmiiDiagForceDatErr_Mask) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = DcckgmiidiagcfgAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    uint32 hwEnabled = enable ? 1 : 0;
    mRegFieldSet(regVal, cAf6_dcckgmiidiagcfg_DcckGmiiDiagGenPkEn_, hwEnabled);
    mRegFieldSet(regVal, cAf6_dcckgmiidiagcfg_DcckGmiiDiagEn_, hwEnabled);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr = DcckgmiidiagcfgAddress(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    if (regVal & (cAf6_dcckgmiidiagcfg_DcckGmiiDiagGenPkEn_Mask | cAf6_dcckgmiidiagcfg_DcckGmiiDiagEn_Mask))
        return cAtTrue;
    return cAtFalse;
    }

static uint16 CounterPrbs2Eth(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cAtPrbsEngineCounterTxFrame     : return cAtEthPortCounterTxPackets;
        case cAtPrbsEngineCounterRxFrame     : return cAtEthPortCounterRxPackets;
        default:
            return cInvalidUint16;
        }
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return AtChannelCounterGet((AtChannel)EthPort(self), CounterPrbs2Eth(self, counterType));
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return AtChannelCounterClear((AtChannel)EthPort(self), CounterPrbs2Eth(self, counterType));
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);

    if ((counterType == cAtPrbsEngineCounterTxFrame) ||
        (counterType == cAtPrbsEngineCounterRxFrame))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool r2c)
    {
    uint32 alarms = 0;
    uint32 regAddr = MacAddressWithLocalAddress(self, cAf6Reg_dcckgmiistk_Base);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    uint32 errorMask = cAf6_dcckgmiistk_DcckGmiiTxFiFoConvErr_Mask  |
                       cAf6_dcckgmiistk_DcckGmiiTxFcsErr_Mask       |
                       cAf6_dcckgmiistk_DcckGmiiTxLostSop_Mask      |
                       cAf6_dcckgmiistk_DcckGmiiRxPreErr_Mask       |
                       cAf6_dcckgmiistk_DcckGmiiRxSfdErr_Mask       |
                       cAf6_dcckgmiistk_DcckGmiiRxDvErr_Mask        |
                       cAf6_dcckgmiistk_DcckGmiiRxFcsErr_Mask       |
                       cAf6_dcckgmiistk_DcckGmiiRxErr_Mask          |
                       cAf6_dcckgmiistk_DcckGmiiRxFiFoConvErr_Mask  |
                       cAf6_dcckgmiistk_DcckGmiiRxMinLenErr_Mask    |
                       cAf6_dcckgmiistk_DcckGmiiRxMaxLenErr_Mask    |
                       cAf6_dcckgmiistk_DcckGmiiDiagRxDatErr_Mask;
    if (regVal & errorMask)
        alarms |= cAtPrbsEngineAlarmTypeError;

    if (r2c & alarms)
        AtPrbsEngineWrite(self, regAddr, errorMask, cAtModuleEth);

    return alarms;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eAtPrbsMode DefaultMode(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsModePrbs15;
    }

static eAtModulePrbsRet DefaultModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    return (prbsMode == DefaultMode(self)) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return DefaultMode(self);
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return DefaultModeSet(self, prbsMode);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return DefaultModeSet(self, prbsMode);
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    return DefaultMode(self);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return DefaultModeSet(self, prbsMode);
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    return DefaultMode(self);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return (prbsMode == DefaultMode(self)) ? cAtTrue : cAtFalse;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    AtPrbsEngine engine = (AtPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(engine), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        }

    mMethodsSet(engine, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SgmiiFramePrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290021SgmiiFramePrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

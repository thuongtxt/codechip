/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290021MateSerdesPrbsEngine.c
 *
 * Created Date: Sep 24, 2016
 *
 * Description : SGMII SERDES PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../physical/Tha6029MateSerdesController.h"
#include "gating/Tha60290021PrbsGatingTimer.h"
#include "Tha6029SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021MateSerdesPrbsEngine
    {
    tTha6029SerdesPrbsEngine super;
    }tTha60290021MateSerdesPrbsEngine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController Serdes(AtPrbsEngine self)
    {
    return Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    }

static Tha6029SerdesTuner Tuner(AtPrbsEngine self)
    {
    return Tha6029MateSerdesTuner(Serdes(self));
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    return Tha6029SerdesTunerRawPrbsEnable(Tuner(self), enable);
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    return Tha6029SerdesTunerRawPrbsIsEnabled(Tuner(self));
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    return Tha6029SerdesTunerRawPrbsErrorForce(Tuner(self), force);
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    return Tha6029SerdesTunerRawPrbsErrorIsForced(Tuner(self));
    }

static eBool ErrorForcingIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtModulePrbsRet ret = cAtOk;
    eBool isEnabled = cAtFalse;

    if (prbsMode == AtPrbsEngineModeGet(self))
        return cAtOk;

    isEnabled = AtPrbsEngineIsEnabled(self);

    /* Turn off/on PRBS engine while changing mode for safe operation. */
    ret |= AtPrbsEngineEnable(self, cAtFalse);
    ret |= Tha6029SerdesTunerRawPrbsModeSet(Tuner(self), prbsMode);
    ret |= AtPrbsEngineEnable(self, isEnabled);

    return ret;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    return Tha6029SerdesTunerRawPrbsModeGet(Tuner(self));
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    return Tha6029SerdesTunerRawPrbsModeIsSupported(Tuner(self), prbsMode);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    eAtRet ret;

    ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AtPrbsEngineModeSet(self, cAtPrbsModePrbs31);
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool read2Clear)
    {
    return Tha6029SerdesTunerRawPrbsAlarmHistoryRead2Clear(Tuner(self), read2Clear);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    return Tha6029SerdesTunerRawPrbsAlarmGet(Tuner(self));
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    if (counterType == cAtPrbsEngineCounterRxBitError)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 CounterRead2Clear(AtPrbsEngine self, uint16 counterType, eBool read2Clear)
    {
    if (counterType == cAtPrbsEngineCounterRxBitError)
        return Tha6029SerdesTunerRawPrbsErrorCounterRead2Clear(Tuner(self), read2Clear);
    return 0;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static AtTimer TimerObjectCreate(AtPrbsEngine self)
    {
    Tha60290021PrbsGatingFactory factory = Tha6029SerdesPrbsEnginePrbsGatingFactory((Tha6029SerdesPrbsEngine)self);
    return Tha60290021PrbsGatingFactorySerdesTuningTimerCreate(factory, Tuner(self));
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForcingIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, TimerObjectCreate);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021MateSerdesPrbsEngine);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290021MateSerdesPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newEngine, serdesController);
    }

uint32 Tha60290021MateSerdesPrbsEngineRegisterWithLocalAddress(AtPrbsEngine self, uint32 localAddress)
    {
    return Tha6029SerdesTunerRegisterWithLocalAddress(Tuner(self), localAddress);
    }

uint32 Tha60290021MateSerdesPrbsEngineLocalPortIdInGroup(AtPrbsEngine self)
    {
    AtSerdesController serdes = Serdes(self);
    return Tha6029MateSerdesControllerLocalPortIdInGroup(serdes);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6029SerdesPrbsEngine.h
 * 
 * Created Date: Sep 24, 2016
 *
 * Description : SERDES PRBS common engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SERDESPRBSENGINE_H_
#define _THA6029SERDESPRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPhysicalClasses.h"
#include "AtModulePrbs.h"
#include "gating/Tha60290021PrbsGatingFactory.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SerdesPrbsEngine * Tha6029SerdesPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha6029SerdesPrbsEngineSerdesControllerGet(Tha6029SerdesPrbsEngine self);
Tha60290021PrbsGatingFactory Tha6029SerdesPrbsEnginePrbsGatingFactory(Tha6029SerdesPrbsEngine self);

/* Concretes */
AtPrbsEngine Tha6029SgmiiSerdesPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60290021BackplaneSerdesPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60290021FacePlateSerdesPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha6029XfiSerdesPrbsEngineNew(AtSerdesController serdesController, uint32 baseAddress);
AtPrbsEngine Tha60290021MateSerdesPrbsEngineNew(AtSerdesController serdesController);
AtPrbsEngine Tha60290021XfiSerdesPrbsEngineNew(AtSerdesController serdesController, uint32 baseAddress);
AtPrbsEngine Tha60290021SgmiiFramePrbsEngineNew(AtSerdesController serdesController);

uint32 Tha60290021BackplaneSerdesPrbsEngineAddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress);
uint32 Tha6029SgmiiSerdesPrbsEngineAddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress);
uint32 Tha60290021MateSerdesPrbsEngineRegisterWithLocalAddress(AtPrbsEngine self, uint32 localAddress);
uint32 Tha6029XfiSerdesPrbsEngineAddressWithLocalAddress(AtPrbsEngine self, uint32 localAddress);
uint32 Tha60290021MateSerdesPrbsEngineLocalPortIdInGroup(AtPrbsEngine self);
uint32 Tha60290021FacePlateSerdesPrbsEnginePortId(AtPrbsEngine self);
uint32 Tha6029XfiSerdesPrbsEngineBaseAddress(AtPrbsEngine self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SERDESPRBSENGINE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290021AuVcPrbsEngineInternal.h
 * 
 * Created Date: May 2, 2017
 *
 * Description : AU-VC PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021AUVCPRBSENGINEINTERNAL_H_
#define _THA60290021AUVCPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/prbs/Tha60210011AuVcPrbsEngineInternal.h"
#include "Tha60290021AuVcPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021AuVcPrbsEngine
    {
    tTha60210011AuVcPrbsEngine super;
    }tTha60290021AuVcPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60290021AuVcPrbsEngineObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021AUVCPRBSENGINEINTERNAL_H_ */


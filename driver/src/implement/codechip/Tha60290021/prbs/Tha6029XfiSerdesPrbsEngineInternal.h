/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6029XfiSerdesPrbsEngineInternal.h
 * 
 * Created Date: Oct 22, 2016
 *
 * Description : XFI PRBS engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029XFISERDESPRBSENGINEINTERNAL_H_
#define _THA6029XFISERDESPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha6029SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029XfiSerdesPrbsEngine
    {
    tTha6029SerdesPrbsEngine super;

    /* Private data */
    uint32 baseAddress;
    }tTha6029XfiSerdesPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6029XfiSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController, uint32 baseAddress);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029XFISERDESPRBSENGINEINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290021ModulePrbs.h
 * 
 * Created Date: Oct 1, 2016
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPRBS_H_
#define _THA60290021MODULEPRBS_H_

/*--------------------------- Includes ---------------------------------------*/
#include "gating/Tha60290021PrbsGatingFactory.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModulePrbs * Tha60290021ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha6029ModulePrbsDurationFromMs(uint32 durationInMs);
Tha60290021PrbsGatingFactory Tha60290021ModulePrbsGatingFactory(Tha60290021ModulePrbs self);
eBool Tha60290021ModulePrbsShouldRedirectToHoBus(AtSdhChannel vc);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPRBS_H_ */


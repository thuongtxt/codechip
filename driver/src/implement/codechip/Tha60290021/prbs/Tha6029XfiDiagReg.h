/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies. The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      :
 *
 * File        :
 *
 * Created Date:
 *
 * Description : This file contain all constance definitions of  block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_XFI_RD_DIAG_H_
#define _AF6_REG_AF6CNC0021_XFI_RD_DIAG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Pattern Generate Configure
Reg Addr   : 0x0
Reg Formula: 0x0
    Where  : 
Reg Desc   : 
XFI PRBS Pattern Generate Configure

------------------------------------------------------------------------------*/
#define cAf6Reg_Configure_register_1_Base                                                                  0x0

/*--------------------------------------
BitField Name: cfgdainc
BitField Type: R/W
BitField Desc: Configure DA Address mode 1 : INC / 0: fixed
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Configure_register_1_cfgdainc_Mask                                                          cBit0
#define cAf6_Configure_register_1_cfgdainc_Shift                                                             0

/*--------------------------------------
BitField Name: cfglengthinc
BitField Type: R/W
BitField Desc: Configure length mode 1 : INC / 0: fixed
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Configure_register_1_cfglengthinc_Mask                                                      cBit1
#define cAf6_Configure_register_1_cfglengthinc_Shift                                                         1

/*--------------------------------------
BitField Name: cfgnumall
BitField Type: R/W
BitField Desc: Configure Packet number mode 1 : continuity   / 0: fixed
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Configure_register_1_cfgnumall_Mask                                                         cBit2
#define cAf6_Configure_register_1_cfgnumall_Shift                                                            2

/*--------------------------------------
BitField Name: cfgins_err
BitField Type: R/W
BitField Desc: Configure Packet Insert Error 1 : Insert / 0: Un-insert
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Configure_register_1_cfgins_err_Mask                                                        cBit3
#define cAf6_Configure_register_1_cfgins_err_Shift                                                           3

/*--------------------------------------
BitField Name: cfgforcsop
BitField Type: R/W
BitField Desc: Configure Force Start of packet 1 : Force / 0: Un-Force
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Configure_register_1_cfgforcsop_Mask                                                        cBit4
#define cAf6_Configure_register_1_cfgforcsop_Shift                                                           4

/*--------------------------------------
BitField Name: cfgforceop
BitField Type: R/W
BitField Desc: Configure Force End of packet 1 : Force / 0: Un-Force
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Configure_register_1_cfgforceop_Mask                                                        cBit5
#define cAf6_Configure_register_1_cfgforceop_Shift                                                           5

/*--------------------------------------
BitField Name: cfggenen
BitField Type: R/W
BitField Desc: Configure PRBS packet test enable 1 : Enable / 0: Disnable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_Configure_register_1_cfggenen_Mask                                                         cBit16
#define cAf6_Configure_register_1_cfggenen_Shift                                                            16

/*--------------------------------------
BitField Name: flush
BitField Type: R/W
BitField Desc: Configure flush PRBS packet gen  1 : flush / 0: Un-flush
BitField Bits: [17]
--------------------------------------*/
#define cAf6_Configure_register_1_flush_Mask                                                            cBit17
#define cAf6_Configure_register_1_flush_Shift                                                               17


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Pattern Packet Number Configure
Reg Addr   : 0x1
Reg Formula: 0x1
    Where  : 
Reg Desc   : 
XFI PRBS Pattern Packet Number Configure

------------------------------------------------------------------------------*/
#define cAf6Reg_Configure_register_2_Base                                                                  0x1

/*--------------------------------------
BitField Name: cfgnumpkt
BitField Type: R/W
BitField Desc: Configure number of packet generate
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Configure_register_2_cfgnumpkt_Mask                                                      cBit31_0
#define cAf6_Configure_register_2_cfgnumpkt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Pattern Packet lengthg Configure
Reg Addr   : 0x2
Reg Formula: 0x2
    Where  : 
Reg Desc   : 
XFI PRBS Pattern Packet lengthg Configure

------------------------------------------------------------------------------*/
#define cAf6Reg_Configure_register_3_Base                                                                  0x2

/*--------------------------------------
BitField Name: cfglengthmin
BitField Type: R/W
BitField Desc: Configure length minimum of packet generate
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Configure_register_3_cfglengthmin_Mask                                                   cBit15_0
#define cAf6_Configure_register_3_cfglengthmin_Shift                                                         0

/*--------------------------------------
BitField Name: cfglengthmax
BitField Type: R/W
BitField Desc: Configure length maximum of packet generate
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_Configure_register_3_cfglengthmax_Mask                                                  cBit31_16
#define cAf6_Configure_register_3_cfglengthmax_Shift                                                        16


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Pattern Packet Data Configure
Reg Addr   : 0x3
Reg Formula: 0x3
    Where  : 
Reg Desc   : 
XFI PRBS Pattern Packet Data Configure

------------------------------------------------------------------------------*/
#define cAf6Reg_Configure_register_4_Base                                                                  0x3

/*--------------------------------------
BitField Name: cfgdatfix
BitField Type: R/W
BitField Desc: Configure Fixed byte Data generate
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_Configure_register_4_cfgdatfix_Mask                                                       cBit7_0
#define cAf6_Configure_register_4_cfgdatfix_Shift                                                            0

/*--------------------------------------
BitField Name: cfgdatmod
BitField Type: R/W
BitField Desc: Configure data mode generate 0x0 : Unused 0x1 : Fixed Data 0x2 :
PRBS31 data 0x3 : PRBS15 data 0x4 : PRBS7  data
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_Configure_register_4_cfgdatmod_Mask                                                      cBit11_8
#define cAf6_Configure_register_4_cfgdatmod_Shift                                                            8


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Diagnostic Gatetime Configuration
Reg Addr   : 0x8
Reg Formula: 0x8
    Where  :
Reg Desc   :
XFI PRBS Diagnostic Gatetime Configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_icfgtime_Base                                                                              0x8

/*--------------------------------------
BitField Name: status_gatetime_diag
BitField Type: RO
BitField Desc: Status Gatetime diagnostic 1:Running 0:Done-Ready
BitField Bits: [25]
--------------------------------------*/
#define cAf6_icfgtime_status_gatetime_diag_Mask                                                         cBit25
#define cAf6_icfgtime_status_gatetime_diag_Shift                                                            25

/*--------------------------------------
BitField Name: start_gatetime_diag
BitField Type: RW
BitField Desc: Config start Diagnostic trigger 0 to 1 for Start auto run with
Gatetime Configuration
BitField Bits: [24]
--------------------------------------*/
#define cAf6_icfgtime_start_gatetime_diag_Mask                                                          cBit24
#define cAf6_icfgtime_start_gatetime_diag_Shift                                                             24

/*--------------------------------------
BitField Name: Unsed
BitField Type: R/W
BitField Desc: Unsed
BitField Bits: [23:17]
--------------------------------------*/
#define cAf6_icfgtime_Unsed_Mask                                                                     cBit23_17
#define cAf6_icfgtime_Unsed_Shift                                                                           17

/*--------------------------------------
BitField Name: time_cfg
BitField Type: RW
BitField Desc: Gatetime Configuration 1-86400 second
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_icfgtime_time_cfg_Mask                                                                   cBit16_0
#define cAf6_icfgtime_time_cfg_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Diagnostic Gatetime Current
Reg Addr   : 0x9
Reg Formula: 0x9
    Where  : 
Reg Desc   : 
XFI PRBS Diagnostic Gatetime_Status

------------------------------------------------------------------------------*/
#define cAf6Reg_istatuscfgtime_Base                                                                        0x9

/*--------------------------------------
BitField Name: Unsed
BitField Type: R/W
BitField Desc: Unsed
BitField Bits: [31:17]
--------------------------------------*/
#define cAf6_istatuscfgtime_Unsed_Mask                                                               cBit31_17
#define cAf6_istatuscfgtime_Unsed_Shift                                                                     17

/*--------------------------------------
BitField Name: currert_gatetime_diag
BitField Type: RO
BitField Desc: Current running time of Gatetime diagnostic
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_istatuscfgtime_currert_gatetime_diag_Mask                                                cBit16_0
#define cAf6_istatuscfgtime_currert_gatetime_diag_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Pattern Packet generate Counter RO
Reg Addr   : 0x10
Reg Formula: 0x10
    Where  : 
Reg Desc   : 
XFI PRBS Pattern Packet generate Counter RO

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_1_Base                                                                    0x10

/*--------------------------------------
BitField Name: penr_opkttotal
BitField Type: RO
BitField Desc: TX Packet Counterer
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_1_penr_opkttotal_Mask                                                    cBit31_0
#define cAf6_Status_register_1_penr_opkttotal_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Pattern Packet generate Counter R2C
Reg Addr   : 0x20
Reg Formula: 0x20
    Where  : 
Reg Desc   : 
XFI PRBS Pattern Packet generate Counter R2C

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_2_Base                                                                    0x20

/*--------------------------------------
BitField Name: penr2cpkttotal
BitField Type: R2C
BitField Desc: TX Packet Counterer
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_2_penr2cpkttotal_Mask                                                    cBit31_0
#define cAf6_Status_register_2_penr2cpkttotal_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Pattern Packet analyzer PRBS Sticky Error
Reg Addr   : 0x700
Reg Formula: 0x700
    Where  : 
Reg Desc   : 
XFI PRBS Pattern Packet analyzer PRBS Sticky Error

------------------------------------------------------------------------------*/
#define cAf6Reg_Configure_register_5_Base                                                                0x700

/*--------------------------------------
BitField Name: err_stk
BitField Type: R/W/C
BitField Desc: Packet analyzer PRBS Sticky Error <0 = RX is synchronized /1 = RX
is not synchronized>
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Configure_register_5_err_stk_Mask                                                           cBit0
#define cAf6_Configure_register_5_err_stk_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Pattern Packet analyzer PRBS Status Error
Reg Addr   : 0x701
Reg Formula: 0x701
    Where  : 
Reg Desc   : 
XFI PRBS Pattern Packet analyzer PRBS Status Error

------------------------------------------------------------------------------*/
#define cAf6Reg_Configure_register_6_Base                                                                0x701

/*--------------------------------------
BitField Name: err_sta
BitField Type: RO
BitField Desc: Packet analyzer PRBS Status Error <0 = RX is synchronized /1 = RX
is not synchronized>
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Configure_register_6_err_sta_Mask                                                           cBit0
#define cAf6_Configure_register_6_err_sta_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Pattern Packet analyzer Data Configure
Reg Addr   : 0x601
Reg Formula: 0x601
    Where  : 
Reg Desc   : 
XFI PRBS Pattern Packet analyzer Data Configure

------------------------------------------------------------------------------*/
#define cAf6Reg_Configure_register_7_Base                                                                0x601

/*--------------------------------------
BitField Name: cfgfcsdrop
BitField Type: R/W
BitField Desc: Configure FCS Drop
BitField Bits: [28]
--------------------------------------*/
#define cAf6_Configure_register_7_cfgfcsdrop_Mask                                                       cBit28
#define cAf6_Configure_register_7_cfgfcsdrop_Shift                                                          28

/*--------------------------------------
BitField Name: cfgdatmod
BitField Type: R/W
BitField Desc: Configure data mode generate 0x0 : Unused 0x1 : Fixed Data 0x2 :
PRBS31 data 0x3 : PRBS15 data 0x4 : PRBS7  data
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_Configure_register_7_cfgdatmod_Mask                                                      cBit11_8
#define cAf6_Configure_register_7_cfgdatmod_Shift                                                            8


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkttotal
Reg Addr   : 0x400
Reg Formula: 0x400
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkttotal

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_3_Base                                                                   0x400

/*--------------------------------------
BitField Name: pkttotal
BitField Type: RO
BitField Desc: RX Packet total
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_3_pkttotal_Mask                                                          cBit31_0
#define cAf6_Status_register_3_pkttotal_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pktgood
Reg Addr   : 0x401
Reg Formula: 0x401
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pktgood

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_4_Base                                                                   0x401

/*--------------------------------------
BitField Name: pktgood
BitField Type: RO
BitField Desc: RX Packet good
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_4_pktgood_Mask                                                           cBit31_0
#define cAf6_Status_register_4_pktgood_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pktfcserr
Reg Addr   : 0x402
Reg Formula: 0x402
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pktfcserr

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_5_Base                                                                   0x402

/*--------------------------------------
BitField Name: pktfcserr
BitField Type: RO
BitField Desc: RX Packet FCS error
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_5_pktfcserr_Mask                                                         cBit31_0
#define cAf6_Status_register_5_pktfcserr_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length err
Reg Addr   : 0x403
Reg Formula: 0x403
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length err

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_6_Base                                                                   0x403

/*--------------------------------------
BitField Name: pktlengtherr
BitField Type: RO
BitField Desc: RX Packet lengthgth error
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_6_pktlengtherr_Mask                                                      cBit31_0
#define cAf6_Status_register_6_pktlengtherr_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt PRBS Data err
Reg Addr   : 0x407
Reg Formula: 0x407
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt PRBS Data err

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_7_Base                                                                   0x407

/*--------------------------------------
BitField Name: pktdaterr
BitField Type: RO
BitField Desc: RX Packet PRBS data error
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_7_pktdaterr_Mask                                                         cBit31_0
#define cAf6_Status_register_7_pktdaterr_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length 0-64
Reg Addr   : 0x40A
Reg Formula: 0x40A
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length 0-64

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_8_Base                                                                   0x40A

/*--------------------------------------
BitField Name: pktlen64
BitField Type: RO
BitField Desc: RX Counter pkt length 0-64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_8_pktlen64_Mask                                                          cBit31_0
#define cAf6_Status_register_8_pktlen64_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length 65-128
Reg Addr   : 0x40B
Reg Formula: 0x40B
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length 65-128

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_9_Base                                                                   0x40B

/*--------------------------------------
BitField Name: pktlen128
BitField Type: RO
BitField Desc: RX Counter pkt length 65-128
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_9_pktlen128_Mask                                                         cBit31_0
#define cAf6_Status_register_9_pktlen128_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length 129-256
Reg Addr   : 0x40C
Reg Formula: 0x40C
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length 129-256

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_10_Base                                                                  0x40C

/*--------------------------------------
BitField Name: pktlen256
BitField Type: RO
BitField Desc: RX Counter pkt length 129-256
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_10_pktlen256_Mask                                                        cBit31_0
#define cAf6_Status_register_10_pktlen256_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length 257-512
Reg Addr   : 0x40D
Reg Formula: 0x40D
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length 257-512

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_11_Base                                                                  0x40D

/*--------------------------------------
BitField Name: pktlen512
BitField Type: RO
BitField Desc: RX Counter pkt length 257-215
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_11_pktlen512_Mask                                                        cBit31_0
#define cAf6_Status_register_11_pktlen512_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length 513-1024
Reg Addr   : 0x40E
Reg Formula: 0x40E
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length 513-1024

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_12_Base                                                                  0x40E

/*--------------------------------------
BitField Name: pktlen1024
BitField Type: RO
BitField Desc: RX Counter pkt length 513-1024
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_12_pktlen1024_Mask                                                       cBit31_0
#define cAf6_Status_register_12_pktlen1024_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length 1025-2048
Reg Addr   : 0x40F
Reg Formula: 0x40F
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length 1025-2048

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_13_Base                                                                  0x40F

/*--------------------------------------
BitField Name: pktlen2048
BitField Type: RO
BitField Desc: RX Counter pkt length 1025-2048
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_13_pktlen2048_Mask                                                       cBit31_0
#define cAf6_Status_register_13_pktlen2048_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkttotal
Reg Addr   : 0x500
Reg Formula: 0x500
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkttotal

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_14_Base                                                                  0x500

/*--------------------------------------
BitField Name: pkttotal
BitField Type: R2C
BitField Desc: RX Packet total
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_14_pkttotal_Mask                                                         cBit31_0
#define cAf6_Status_register_14_pkttotal_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pktgood
Reg Addr   : 0x501
Reg Formula: 0x501
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pktgood

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_15_Base                                                                  0x501

/*--------------------------------------
BitField Name: pktgood
BitField Type: R2C
BitField Desc: RX Packet good
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_15_pktgood_Mask                                                          cBit31_0
#define cAf6_Status_register_15_pktgood_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pktfcserr
Reg Addr   : 0x502
Reg Formula: 0x502
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pktfcserr

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_16_Base                                                                  0x502

/*--------------------------------------
BitField Name: pktfcserr
BitField Type: R2C
BitField Desc: RX Packet FCS error
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_16_pktfcserr_Mask                                                        cBit31_0
#define cAf6_Status_register_16_pktfcserr_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length err
Reg Addr   : 0x503
Reg Formula: 0x503
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length err

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_17_Base                                                                  0x503

/*--------------------------------------
BitField Name: pktlengtherr
BitField Type: R2C
BitField Desc: RX Packet lengthgth error
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_17_pktlengtherr_Mask                                                     cBit31_0
#define cAf6_Status_register_17_pktlengtherr_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt PRBS Data err
Reg Addr   : 0x507
Reg Formula: 0x507
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt PRBS Data err

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_18_Base                                                                  0x507

/*--------------------------------------
BitField Name: pktdaterr
BitField Type: R2C
BitField Desc: RX Packet PRBS data error
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_18_pktdaterr_Mask                                                        cBit31_0
#define cAf6_Status_register_18_pktdaterr_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length 0-64
Reg Addr   : 0x50A
Reg Formula: 0x50A
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length 0-64

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_19_Base                                                                  0x50A

/*--------------------------------------
BitField Name: pktlen64
BitField Type: R2C
BitField Desc: RX Counter pkt length 0-64
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_19_pktlen64_Mask                                                         cBit31_0
#define cAf6_Status_register_19_pktlen64_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length 65-128
Reg Addr   : 0x50B
Reg Formula: 0x50B
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length 65-128

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_20_Base                                                                  0x50B

/*--------------------------------------
BitField Name: pktlen128
BitField Type: R2C
BitField Desc: RX Counter pkt length 65-128
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_20_pktlen128_Mask                                                        cBit31_0
#define cAf6_Status_register_20_pktlen128_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length 129-256
Reg Addr   : 0x50C
Reg Formula: 0x50C
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length 129-256

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_21_Base                                                                  0x50C

/*--------------------------------------
BitField Name: pktlen256
BitField Type: R2C
BitField Desc: RX Counter pkt length 129-256
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_21_pktlen256_Mask                                                        cBit31_0
#define cAf6_Status_register_21_pktlen256_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length 257-512
Reg Addr   : 0x50D
Reg Formula: 0x50D
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length 257-512

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_22_Base                                                                  0x50D

/*--------------------------------------
BitField Name: pktlen512
BitField Type: R2C
BitField Desc: RX Counter pkt length 257-215
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_22_pktlen512_Mask                                                        cBit31_0
#define cAf6_Status_register_22_pktlen512_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length 513-1024
Reg Addr   : 0x50E
Reg Formula: 0x50E
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length 513-1024

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_23_Base                                                                  0x50E

/*--------------------------------------
BitField Name: pktlen1024
BitField Type: R2C
BitField Desc: RX Counter pkt length 513-1024
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_23_pktlen1024_Mask                                                       cBit31_0
#define cAf6_Status_register_23_pktlen1024_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : XFI PRBS Packet analyzer Counter pkt length 1025-2048
Reg Addr   : 0x50F
Reg Formula: 0x50F
    Where  : 
Reg Desc   : 
XFI PRBS Packet analyzer Counter pkt length 1025-2048

------------------------------------------------------------------------------*/
#define cAf6Reg_Status_register_24_Base                                                                  0x50F

/*--------------------------------------
BitField Name: pktlen2048
BitField Type: R2C
BitField Desc: RX Counter pkt length 1025-2048
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Status_register_24_pktlen2048_Mask                                                       cBit31_0
#define cAf6_Status_register_24_pktlen2048_Shift                                                             0

#endif /* _AF6_REG_AF6CNC0021_XFI_RD_DIAG_H_ */

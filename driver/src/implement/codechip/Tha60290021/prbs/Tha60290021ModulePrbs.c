/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290021ModulePrbs.c
 *
 * Created Date: Jul 12, 2016
 *
 * Description : PRBS  management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/prbs/ThaPrbsRegProvider.h"
#include "../man/Tha60290021Device.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../sdh/Tha6029SdhLineSideAuVcInternal.h"
#include "../common/Tha602900xxCommon.h"
#include "Tha60290021ModulePrbs.h"
#include "Tha60290021AuVcPrbsEngine.h"
#include "Tha60290021ModuleMapHoReg.h"
#include "Tha60290021ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cTha6029NumberEnginePerGroup 	16

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021ModulePrbs)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021ModulePrbsMethods m_methods;

/* Override */
static tAtObjectMethods      m_AtObjectOverride;
static tAtModuleMethods      m_AtModuleOverride;
static tAtModulePrbsMethods  m_AtModulePrbsOverride;
static tThaModulePrbsMethods m_ThaModulePrbsOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods     = NULL;
static const tAtModuleMethods       *m_AtModuleMethods     = NULL;
static const tAtModulePrbsMethods   *m_AtModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char *CapacityDescription(AtModule self)
    {
    AtUnused(self);
    return "28 engines for LO + 28 engines for HO";
    }

static uint32 MaxNumLoPrbsEngine(ThaModulePrbs self)
    {
    AtUnused(self);
    return 28;
    }

static uint32 MaxNumHoPrbsEngine(ThaModulePrbs self)
    {
    AtUnused(self);
    return 28;
    }

static eBool LineCanCreatePrbsEngine(AtSdhLine self)
    {
    uint8 lineId = (uint8) AtChannelIdGet((AtChannel) self);
    AtModuleSdh sdhModule = (AtModuleSdh) AtChannelModuleGet((AtChannel) self);

    /* Current design not support PRBS Line for terminated Line */
    return Tha60290021ModuleSdhLineIsTerminated(sdhModule, lineId) ? cAtFalse : cAtTrue;
    }

static AtPrbsEngine SdhLinePrbsEngineCreate(AtModulePrbs self, AtSdhLine line)
    {
    if (LineCanCreatePrbsEngine(line))
        return m_AtModulePrbsMethods->SdhLinePrbsEngineCreate(self, line);

    return NULL;
    }

static eBool SdhChannelCanHavePrbsEngine(AtSdhChannel self)
    {
    uint8 lineId = AtSdhChannelLineGet(self);
    AtModuleSdh sdhModule = (AtModuleSdh) AtChannelModuleGet((AtChannel) self);

    /* Current design only support PRBS VC for terminated Line */
    return Tha60290021ModuleSdhLineIsTerminated(sdhModule, lineId);
    }

static eBool XcHidingShouldHide(AtModulePrbs self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleXc xcModule = (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    eTha60290021XcHideMode xcHideMode = Tha60290021ModuleXcHideModeGet(xcModule);

    if (xcHideMode == cTha60290021XcHideModeNone)
        return cAtFalse;

    return cAtTrue;
    }

static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
	AtSdhChannel vc = sdhVc;

	if (XcHidingShouldHide(self))
	    {
	    if (Tha60290021ModuleSdhIsLineSideVc(sdhVc))
	        vc = AtSdhChannelHideChannelGet(sdhVc);
        if (vc == NULL)
            vc = sdhVc;
	    }

    if (SdhChannelCanHavePrbsEngine(vc))
        return m_AtModulePrbsMethods->SdhVcPrbsEngineCreate(self, engineId, vc);

    return NULL;
    }

static AtPrbsEngine AuVcPrbsEngineObjectCreate(ThaModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhVc);

    if (channelType == cAtSdhChannelTypeVc4_64c)
        return mMethodsGet(mThis(self))->AuVc4_64cPrbsEngineObjectCreate(mThis(self), engineId, sdhVc);

    return mMethodsGet(mThis(self))->AuVcxPrbsEngineObjectCreate(mThis(self), engineId, sdhVc);
    }

static ThaPrbsRegProvider RegProvider(ThaModulePrbs self)
    {
    AtUnused(self);
    return Tha60290021PrbsRegProvider();
    }

static eBool HoVcPrbsIsSupported(ThaModulePrbs self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PwPrbsIsSupported(ThaModulePrbs self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 NumSlices(ThaModulePrbs self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleSdh sdhModule = (AtModuleSdh) AtDeviceModuleGet(device, cAtModuleSdh);
    return Tha60290021ModuleSdhNumUseableTerminatedLines(sdhModule);
    }

static eAtRet HoPrbsEngineRegisterInit(ThaModulePrbs self)
    {
    uint32 engineId;

    for (engineId = 0; engineId < mMethodsGet(self)->MaxNumHoPrbsEngine(self); engineId++)
        {
        uint8 sliceId;
        uint32 regOffset = Tha60290021AuVcPrbsEngineDefaultOffset(engineId);
        mModuleHwWrite(self, cAf6Reg_sel_ho_bert_gen0_Base + regOffset, Tha60210011AuVcPrbsEngineBertGenChannelIdMask());
        for (sliceId = 0; sliceId < NumSlices(self); sliceId++)
            {
            regOffset = Tha60290021AuVcPrbsEngineOffsetWithSliceId(engineId, sliceId);
            mModuleHwWrite(self, cAf6Reg_ctrl_pen_tdm_mon_Base + regOffset, 0xA2000);
            mModuleHwWrite(self, cAf6Reg_ctrl_pen_pw_mon_Base + regOffset, 0xA2000);
            }
        }

    return cAtOk;
    }

static eBool PrbsCounterReadIndirectly(ThaModulePrbs self, ThaPrbsEngine engine)
    {
    AtUnused(self);
    AtUnused(engine);
	return cAtFalse;
    }

static eBool ErrorForcingRateIsSupported(ThaModulePrbs self, eAtBerRate errorRate)
    {
    AtUnused(self);
    if (errorRate == cAtBerRateUnknown)
        return cAtFalse;

    if (errorRate > cAtBerRate1E9)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 GroupIdGet(ThaModulePrbs self, uint32 engineId)
    {
    AtUnused(self);
    return (engineId < cTha6029NumberEnginePerGroup) ? 0 : 1;
    }

static uint32 LocalIdGet(ThaModulePrbs self, uint32 engineId)
    {
    AtUnused(self);
    return engineId;
    }

static Tha60290021PrbsGatingFactory PrbsGatingFactoryObjectCreate(Tha60290021ModulePrbs self)
    {
    return Tha60290021PrbsGatingFactoryNew((AtModulePrbs)self);
    }

static Tha60290021PrbsGatingFactory PrbsGatingFactory(Tha60290021ModulePrbs self)
    {
    if (self->gatingTimerFactory == NULL)
        self->gatingTimerFactory = mMethodsGet(self)->PrbsGatingFactoryObjectCreate(self);
    return self->gatingTimerFactory;
    }

static AtPrbsEngine AuVc4_64cPrbsEngineObjectCreate(Tha60290021ModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    AtUnused(engineId);
    AtUnused(sdhVc);
    /* Let subclass determine */
    return NULL;
    }

static AtPrbsEngine AuVcxPrbsEngineObjectCreate(Tha60290021ModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    return Tha60290021AuVcPrbsEngineNew(sdhVc, engineId);
    }

static uint32 NumAuVc4_64cEngines(Tha60290021ModulePrbs self)
    {
    AtUnused(self);
    return 0;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->gatingTimerFactory);
    mThis(self)->gatingTimerFactory = NULL;
    m_AtObjectMethods->Delete(self);
    }

static eBool FlexiblePsnSide(ThaModulePrbs self)
    {
    return Tha602900xxFlexiblePsnSide(self);
    }

static eBool ShouldInvalidateStatusWhenRxIsDisabled(AtModulePrbs self)
    {
    /* As this is required by customer */
    AtUnused(self);
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290021ModulePrbs object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObject(gatingTimerFactory);
    }

static eBool ChannelizedPrbsAllowed(AtModulePrbs self)
    {
    return Tha602900xxModulePrbsChannelizedPrbsAllowed(self);
    }

static eBool PrbsShouldRedirectToHoBus(AtSdhChannel vc)
    {
    uint32 vcType = AtSdhChannelTypeGet(vc);
    AtDevice device = AtChannelDeviceGet((AtChannel)vc);
    ThaModulePrbs prbsModule = (ThaModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    eBool hasPrbsEngine;

    if (!AtModulePrbsChannelizedPrbsAllowed((AtModulePrbs)prbsModule))
        return cAtFalse;

    hasPrbsEngine = AtChannelPrbsEngineGet((AtChannel)vc) ? cAtTrue : cAtFalse;
    if (vcType == cAtSdhChannelTypeVc4)
        return hasPrbsEngine;

    if (vcType == cAtSdhChannelTypeVc3)
        {
        if (AtSdhChannelTypeGet(AtSdhChannelParentChannelGet(vc)) == cAtSdhChannelTypeTu3)
            return cAtFalse;
        return hasPrbsEngine;
        }

    return cAtFalse;
    }

static void OverrideAtObject(AtModulePrbs self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, CapacityDescription);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, m_AtModulePrbsMethods, sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, SdhLinePrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, ShouldInvalidateStatusWhenRxIsDisabled);
        mMethodOverride(m_AtModulePrbsOverride, ChannelizedPrbsAllowed);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideThaModulePrbs(ThaModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePrbsOverride, mMethodsGet(self), sizeof(m_ThaModulePrbsOverride));

        mMethodOverride(m_ThaModulePrbsOverride, AuVcPrbsEngineObjectCreate);
        mMethodOverride(m_ThaModulePrbsOverride, RegProvider);
        mMethodOverride(m_ThaModulePrbsOverride, MaxNumLoPrbsEngine);
        mMethodOverride(m_ThaModulePrbsOverride, MaxNumHoPrbsEngine);
        mMethodOverride(m_ThaModulePrbsOverride, HoVcPrbsIsSupported);
        mMethodOverride(m_ThaModulePrbsOverride, PwPrbsIsSupported);
        mMethodOverride(m_ThaModulePrbsOverride, HoPrbsEngineRegisterInit);
        mMethodOverride(m_ThaModulePrbsOverride, PrbsCounterReadIndirectly);
        mMethodOverride(m_ThaModulePrbsOverride, ErrorForcingRateIsSupported);
        mMethodOverride(m_ThaModulePrbsOverride, GroupIdGet);
        mMethodOverride(m_ThaModulePrbsOverride, LocalIdGet);
        mMethodOverride(m_ThaModulePrbsOverride, FlexiblePsnSide);
        }

    mMethodsSet(self, &m_ThaModulePrbsOverride);
    }

static void MethodsInit(Tha60290021ModulePrbs self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PrbsGatingFactoryObjectCreate);
        mMethodOverride(m_methods, AuVc4_64cPrbsEngineObjectCreate);
        mMethodOverride(m_methods, AuVcxPrbsEngineObjectCreate);
        mMethodOverride(m_methods, NumAuVc4_64cEngines);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePrbs(self);
    OverrideThaModulePrbs((ThaModulePrbs) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModulePrbs);
    }

AtModulePrbs Tha60290021ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Over-ride */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha60290021ModulePrbsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModulePrbsObjectInit(newModule, device);
    }

uint32 Tha6029ModulePrbsDurationFromMs(uint32 durationInMs)
    {
    /* Must be in second unit */
    static const uint32 cNumMsPerSecond = 1000;
    if (durationInMs % cNumMsPerSecond)
        return cInvalidUint32;

    return durationInMs / cNumMsPerSecond;
    }

Tha60290021PrbsGatingFactory Tha60290021ModulePrbsGatingFactory(Tha60290021ModulePrbs self)
    {
    if (self)
        return PrbsGatingFactory(self);
    return NULL;
    }

eBool Tha60290021ModulePrbsShouldRedirectToHoBus(AtSdhChannel vc)
    {
    if (vc)
        return PrbsShouldRedirectToHoBus(vc);
    return cAtFalse;
    }

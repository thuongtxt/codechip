/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290021PrbsGatingTimerBackplaneSerdes.c
 *
 * Created Date: Apr 7, 2017
 *
 * Description : Gating timer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../util/coder/AtCoderUtil.h"
#include "../../../../../generic/prbs/AtPrbsGatingTimerInternal.h"
#include "../../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "../../../../default/man/ThaDevice.h"
#include "../../physical/Tha6029SerdesEth40GReg.h"
#include "../Tha6029SerdesPrbsEngine.h"
#include "Tha60290021PrbsGatingTimer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290021PrbsGatingTimerBackplaneSerdes *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021PrbsGatingTimerBackplaneSerdes
    {
    tAtPrbsGatingTimer super;

    /* Private data */
    eBool elapseTimeLogicEnabled;
    }tTha60290021PrbsGatingTimerBackplaneSerdes;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtTimerMethods   m_AtTimerOverride;
static tAtHwTimerMethods m_AtHwTimerOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtHwTimerMethods *m_AtHwTimerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPrbsEngine PrbsEngine(AtHwTimer self)
    {
    return AtPrbsGatingTimerPrbsEngine((AtPrbsGatingTimer)self);
    }

static uint32 AddressWithLocalAddress(AtHwTimer self, uint32 localAddress)
    {
    AtPrbsEngine engine = PrbsEngine(self);
    return Tha60290021BackplaneSerdesPrbsEngineAddressWithLocalAddress(engine, localAddress);
    }

static uint32 DiagCtrl7Register(AtHwTimer self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_ETH_40G_Diag_ctrl7_Base);
    }

static eAtRet HwDurationSet(AtHwTimer self, uint32 hwDuration)
    {
    uint32 regAddr = DiagCtrl7Register(self);
    uint32 regVal = AtHwTimerRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_ETH_40G_Diag_ctrl7_time_cfg_, hwDuration);
    AtHwTimerWrite(self, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static uint32 HwDurationGet(AtHwTimer self)
    {
    uint32 regAddr = DiagCtrl7Register(self);
    uint32 regVal  = AtHwTimerRead(self, regAddr, cAtModulePrbs);
    return mRegField(regVal, cAf6_ETH_40G_Diag_ctrl7_time_cfg_);
    }

static eAtRet HwStart(AtHwTimer self, eBool start)
    {
    uint32 regAddr = DiagCtrl7Register(self);
    uint32 regVal  = AtHwTimerRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_ETH_40G_Diag_ctrl7_start_gatetime_diag_, start ? 1 : 0);
    AtHwTimerWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool HwIsRunning(AtHwTimer self)
    {
    uint32 regAddr = DiagCtrl7Register(self);
    uint32 regVal  = AtHwTimerRead(self, regAddr, cAtModulePrbs);
    return (regVal & cAf6_ETH_40G_Diag_ctrl7_status_gatetime_diag_Mask) ? cAtTrue : cAtFalse;
    }

static eBool IsStarted(AtTimer self)
    {
    uint32 regAddr = DiagCtrl7Register((AtHwTimer)self);
    uint32 regVal  = AtHwTimerRead((AtHwTimer)self, regAddr, cAtModulePrbs);
    return (regVal & cAf6_ETH_40G_Diag_ctrl7_start_gatetime_diag_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 HwDurationMax(AtHwTimer self)
    {
    AtUnused(self);
    return (cAf6_ETH_40G_Diag_ctrl7_time_cfg_Mask >> cAf6_ETH_40G_Diag_ctrl7_time_cfg_Shift);
    }

static ThaVersionReader VersionReader(AtHwTimer self)
    {
    AtPrbsEngine engine = PrbsEngine(self);
    AtDevice device = AtPrbsEngineDeviceGet(engine);
    return ThaDeviceVersionReader(device);
    }

static eBool HasElapsedTimeByVersionChecking(AtHwTimer self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x7, 0x0);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool HasElapsedTime(AtHwTimer self)
    {
    if (mThis(self)->elapseTimeLogicEnabled)
        return cAtTrue;

    return HasElapsedTimeByVersionChecking(self);
    }

static uint32 HwElapsedTimeGet(AtHwTimer self)
    {
    if (HasElapsedTime(self))
        {
        uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_Gatetime_current_Base);
        uint32 regVal = AtHwTimerRead(self, regAddr, cAtModulePrbs);
        return mRegField(regVal, cAf6_Gatetime_current_currert_gatetime_diag_);
        }

    return m_AtHwTimerMethods->HwElapsedTimeGet(self);
    }

static void ElapseTimeLogicEnable(AtTimer self, eBool enabled)
    {
    mThis(self)->elapseTimeLogicEnabled = enabled;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290021PrbsGatingTimerBackplaneSerdes *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(elapseTimeLogicEnabled);
    }

static void OverrideAtObject(AtTimer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideTimer(AtTimer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtTimerOverride, mMethodsGet(self), sizeof(m_AtTimerOverride));

        mMethodOverride(m_AtTimerOverride, IsStarted);
        }

    mMethodsSet(self, &m_AtTimerOverride);
    }

static void OverrideAtHwTimer(AtTimer self)
    {
    AtHwTimer timer = (AtHwTimer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHwTimerMethods = mMethodsGet(timer);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHwTimerOverride, mMethodsGet(timer), sizeof(m_AtHwTimerOverride));

        mMethodOverride(m_AtHwTimerOverride, HwDurationSet);
        mMethodOverride(m_AtHwTimerOverride, HwDurationGet);
        mMethodOverride(m_AtHwTimerOverride, HwDurationMax);
        mMethodOverride(m_AtHwTimerOverride, HwElapsedTimeGet);
        mMethodOverride(m_AtHwTimerOverride, HwStart);
        mMethodOverride(m_AtHwTimerOverride, HwIsRunning);
        }

    mMethodsSet(timer, &m_AtHwTimerOverride);
    }

static void Override(AtTimer self)
    {
    OverrideAtObject(self);
    OverrideTimer(self);
    OverrideAtHwTimer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PrbsGatingTimerBackplaneSerdes);
    }

static AtTimer ObjectInit(AtTimer self, AtPrbsEngine engine)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsGatingTimerObjectInit(self, engine) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtTimer Tha60290021PrbsGatingTimerBackplaneSerdesNew(AtPrbsEngine engine)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtTimer newTimer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newTimer, engine);
    }

void Tha60290021PrbsGatingTimerBackplaneSerdesElapseTimeLogicEnable(AtTimer self, eBool enabled)
    {
    if (self)
        ElapseTimeLogicEnable(self, enabled);
    }

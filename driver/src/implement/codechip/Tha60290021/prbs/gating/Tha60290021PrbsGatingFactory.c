/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290021PrbsGatingFactory.c
 *
 * Created Date: Apr 25, 2017
 *
 * Description : PRBS gating timer factory
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../util/coder/AtCoderUtil.h"
#include "../../../../default/man/ThaDevice.h"
#include "Tha60290021PrbsGatingFactoryInternal.h"
#include "Tha60290021PrbsGatingTimer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021PrbsGatingFactory)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021PrbsGatingFactoryMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtTimer BackplaneSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine)
    {
    AtUnused(self);
    return Tha60290021PrbsGatingTimerBackplaneSerdesNew(engine);
    }

static AtTimer FaceplateSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine)
    {
    AtUnused(self);
    return Tha60290021PrbsGatingTimerFaceplateSerdesNew(engine);
    }

static ThaVersionReader VersionReader(Tha60290021PrbsGatingFactory self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self->module);
    return ThaDeviceVersionReader(device);
    }

static eBool MateGatingSupported(Tha60290021PrbsGatingFactory self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x7, 0x00);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static AtTimer SerdesTuningTimerCreate(Tha60290021PrbsGatingFactory self, Tha6029SerdesTuner tuner)
    {
    if (mMethodsGet(self)->MateGatingSupported(self))
        return Tha60290021PrbsGatingTimerSerdesTuningNew(tuner);
    return NULL;
    }

static AtTimer SgmiiSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine)
    {
    AtUnused(self);
    return Tha60290021PrbsGatingTimerSgmiiSerdesNew(engine);
    }

static AtTimer XfiSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine)
    {
    AtUnused(self);
    return Tha60290021PrbsGatingTimerXfiSerdesNew(engine);
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "gating_factory";
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290021PrbsGatingFactory object = (Tha60290021PrbsGatingFactory)self;
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(module);
    }

static void OverrideAtObject(Tha60290021PrbsGatingFactory self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(Tha60290021PrbsGatingFactory self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(Tha60290021PrbsGatingFactory self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, BackplaneSerdesTimerCreate);
        mMethodOverride(m_methods, FaceplateSerdesTimerCreate);
        mMethodOverride(m_methods, SerdesTuningTimerCreate);
        mMethodOverride(m_methods, SgmiiSerdesTimerCreate);
        mMethodOverride(m_methods, XfiSerdesTimerCreate);
        mMethodOverride(m_methods, MateGatingSupported);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PrbsGatingFactory);
    }

Tha60290021PrbsGatingFactory Tha60290021PrbsGatingFactoryObjectInit(Tha60290021PrbsGatingFactory self, AtModulePrbs module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->module = module;

    return self;
    }

Tha60290021PrbsGatingFactory Tha60290021PrbsGatingFactoryNew(AtModulePrbs module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60290021PrbsGatingFactory newFactory = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60290021PrbsGatingFactoryObjectInit(newFactory, module);
    }

AtTimer Tha60290021PrbsGatingFactoryBackplaneSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine)
    {
    if (self)
        return mMethodsGet(self)->BackplaneSerdesTimerCreate(self, engine);
    return NULL;
    }

AtTimer Tha60290021PrbsGatingFactoryFaceplateSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine)
    {
    if (self)
        return mMethodsGet(self)->FaceplateSerdesTimerCreate(self, engine);
    return NULL;
    }

AtTimer Tha60290021PrbsGatingFactorySerdesTuningTimerCreate(Tha60290021PrbsGatingFactory self, Tha6029SerdesTuner tuner)
    {
    if (self)
        return mMethodsGet(self)->SerdesTuningTimerCreate(self, tuner);
    return NULL;
    }

AtTimer Tha60290021PrbsGatingFactorySgmiiSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine)
    {
    if (self)
        return mMethodsGet(self)->SgmiiSerdesTimerCreate(self, engine);
    return NULL;
    }

AtTimer Tha60290021PrbsGatingFactoryXfiSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine)
    {
    if (self)
        return mMethodsGet(self)->XfiSerdesTimerCreate(self, engine);
    return NULL;
    }

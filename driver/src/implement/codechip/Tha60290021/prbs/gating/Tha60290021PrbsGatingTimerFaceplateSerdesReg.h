/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_Faceplate_Serdeses_Gatetime_H_
#define _AF6_REG_AF6CNC0021_Faceplate_Serdeses_Gatetime_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Faceplate serdeses Gatetime for PRBS Raw Diagnostic
Reg Addr   : 0x40-0x4F
Reg Formula: Base_0x40 + 0x40 + PortID
    Where  : 
           + Port ID ( 0 - 15 )
Reg Desc   : 
This is Faceplate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 16

------------------------------------------------------------------------------*/
#define cAf6Reg_gatetime_ctr_Base                                                                         0x40

/*--------------------------------------
BitField Name: start_diag
BitField Type: RW
BitField Desc: Config start Diagnostic trigger 0 to 1 for Start auto run with
Gatetime Configuration
BitField Bits: [17]
--------------------------------------*/
#define cAf6_gatetime_ctr_start_diag_Mask                                                               cBit17
#define cAf6_gatetime_ctr_start_diag_Shift                                                                  17

/*--------------------------------------
BitField Name: time_cfg
BitField Type: RW
BitField Desc: Gatetime Configuration 1-86400 second
BitField Bits: [16:00]
--------------------------------------*/
#define cAf6_gatetime_ctr_time_cfg_Mask                                                               cBit16_0
#define cAf6_gatetime_ctr_time_cfg_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Faceplate serdeses Gatetime
Reg Addr   : 0x60
Reg Formula: 
    Where  : 
Reg Desc   : 
Faceplate_serdeses_Gatetime_Status

------------------------------------------------------------------------------*/
#define cAf6Reg_Faceplate_serdeses_Gatetime_st_Base                                                       0x60

/*--------------------------------------
BitField Name: Unsed
BitField Type: R/W
BitField Desc: Unsed
BitField Bits: [31:19]
--------------------------------------*/
#define cAf6_Faceplate_serdeses_Gatetime_st_Unsed_Mask                                               cBit31_19
#define cAf6_Faceplate_serdeses_Gatetime_st_Unsed_Shift                                                     19

/*--------------------------------------
BitField Name: status_gatetime_diag
BitField Type: RO
BitField Desc: Status Gatetime diagnostic port 16 to port1 bit per port
1:Running 0:Done-Ready
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_Faceplate_serdeses_Gatetime_st_status_gatetime_diag_Mask                                 cBit15_0
#define cAf6_Faceplate_serdeses_Gatetime_st_status_gatetime_diag_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Faceplate serdeses Gatetime Current
Reg Addr   : 0x61-0x70
Reg Formula: Baas0x61 +0x60+Port ID
    Where  :
           + Port ID ( 1 - 16 )
Reg Desc   :
Faceplate_serdeses_Gatetime_Status

------------------------------------------------------------------------------*/
#define cAf6Reg_Faceplate_serdeses_Gatetime_current_Base                                                  0x61

/*--------------------------------------
BitField Name: Unsed
BitField Type: R/W
BitField Desc: Unsed
BitField Bits: [31:17]
--------------------------------------*/
#define cAf6_Faceplate_serdeses_Gatetime_current_Unsed_Mask                                          cBit31_17
#define cAf6_Faceplate_serdeses_Gatetime_current_Unsed_Shift                                                17

/*--------------------------------------
BitField Name: currert_gatetime_diag
BitField Type: RO
BitField Desc: Current running time of Gatetime diagnostic
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_Faceplate_serdeses_Gatetime_current_currert_gatetime_diag_Mask                                cBit16_0
#define cAf6_Faceplate_serdeses_Gatetime_current_currert_gatetime_diag_Shift                                       0

#endif /* _AF6_REG_AF6CNC0021_Faceplate_Serdeses_Gatetime_H_ */

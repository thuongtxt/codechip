/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290021PrbsGatingFactoryInternal.h
 * 
 * Created Date: Apr 25, 2017
 *
 * Description : PRBS gating timer factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021PRBSGATINGFACTORYINTERNAL_H_
#define _THA60290021PRBSGATINGFACTORYINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/man/AtDriverInternal.h" /* For OSAL macros */
#include "AtObject.h"
#include "Tha60290021PrbsGatingFactory.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021PrbsGatingFactoryMethods
    {
    AtTimer (*BackplaneSerdesTimerCreate)(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine);
    AtTimer (*FaceplateSerdesTimerCreate)(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine);
    AtTimer (*SerdesTuningTimerCreate)(Tha60290021PrbsGatingFactory self, Tha6029SerdesTuner tuner);
    AtTimer (*SgmiiSerdesTimerCreate)(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine);
    AtTimer (*XfiSerdesTimerCreate)(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine);
    eBool (*MateGatingSupported)(Tha60290021PrbsGatingFactory self);
    }tTha60290021PrbsGatingFactoryMethods;

typedef struct tTha60290021PrbsGatingFactory
    {
    tAtObject super;
    const tTha60290021PrbsGatingFactoryMethods *methods;

    /* Private data */
    AtModulePrbs module;
    }tTha60290021PrbsGatingFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60290021PrbsGatingFactory Tha60290021PrbsGatingFactoryObjectInit(Tha60290021PrbsGatingFactory self, AtModulePrbs module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021PRBSGATINGFACTORYINTERNAL_H_ */


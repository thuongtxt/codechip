/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : AtPrbsGatingTimer.h
 * 
 * Created Date: Apr 8, 2017
 *
 * Description : PRBS gating timer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029PRBSGATINGTIMER_H_
#define _THA6029PRBSGATINGTIMER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/prbs/AtPrbsGatingTimer.h"
#include "../../physical/Tha6029SerdesTuner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtTimer Tha60290021PrbsGatingTimerBackplaneSerdesNew(AtPrbsEngine engine);
AtTimer Tha60290021PrbsGatingTimerFaceplateSerdesNew(AtPrbsEngine engine);
AtTimer Tha60290021PrbsGatingTimerSgmiiSerdesNew(AtPrbsEngine engine);
AtTimer Tha60290021PrbsGatingTimerXfiSerdesNew(AtPrbsEngine engine);
AtTimer Tha60290021PrbsGatingTimerSerdesTuningNew(Tha6029SerdesTuner tuner);

/* Backplane specific APIs */
void Tha60290021PrbsGatingTimerBackplaneSerdesElapseTimeLogicEnable(AtTimer self, eBool enabled);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029PRBSGATINGTIMER_H_ */


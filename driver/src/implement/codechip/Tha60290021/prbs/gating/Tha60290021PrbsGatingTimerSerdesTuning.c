/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6029PrbsGatingTimerSerdesTuning.c
 *
 * Created Date: Apr 8, 2017
 *
 * Description : PRBS gating timer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../util/coder/AtCoderUtil.h"
#include "../../../../../generic/prbs/AtPrbsGatingTimerInternal.h"
#include "../../physical/Tha6029SerdesTunningReg.h"
#include "Tha60290021PrbsGatingTimer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290021PrbsGatingTimerSerdesTuning *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021PrbsGatingTimerSerdesTuning
    {
    tAtHwTimer super;

    /* Private data */
    Tha6029SerdesTuner tuner;
    }tTha60290021PrbsGatingTimerSerdesTuning;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtTimerMethods   m_AtTimerOverride;
static tAtHwTimerMethods m_AtHwTimerOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Read(AtHwTimer self, uint32 address, eAtModule module)
    {
    return Tha6029SerdesTunerRead(mThis(self)->tuner, address, module);
    }

static void Write(AtHwTimer self, uint32 address, uint32 value, eAtModule module)
    {
    Tha6029SerdesTunerWrite(mThis(self)->tuner, address, value, module);
    }

static eBool IsSimulated(AtHwTimer self)
    {
    return Tha6029SerdesTunerIsSimulated(mThis(self)->tuner);
    }

static uint32 LocalId(AtHwTimer self)
    {
    return Tha6029SerdesTunerLocalId(mThis(self)->tuner);
    }

static uint32 AddressWithLocalAddress(AtHwTimer self, uint32 localAddress)
    {
    return Tha6029SerdesTunerRegisterWithLocalAddress(mThis(self)->tuner, localAddress);
    }

static uint32 ConfigureRegister(AtHwTimer self)
    {
    uint32 localAddress = cAf6Reg_gatetime_cfg_Base + LocalId(self);
    return AddressWithLocalAddress(self, localAddress);
    }

static uint32 CtrRegister(AtHwTimer self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_gatetime_ctr_Base);
    }

static uint32 CurrentRegister(AtHwTimer self)
    {
    uint32 localAddress = cAf6Reg_Gatetime_current_Base + LocalId(self);
    return AddressWithLocalAddress(self, localAddress);
    }

static eAtRet HwDurationSet(AtHwTimer self, uint32 hwDuration)
    {
    uint32 regAddr = ConfigureRegister(self);
    uint32 regVal = AtHwTimerRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_gatetime_cfg_time_cfg_, hwDuration);
    AtHwTimerWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static uint32 HwDurationGet(AtHwTimer self)
    {
    uint32 regAddr = ConfigureRegister(self);
    uint32 regVal  = AtHwTimerRead(self, regAddr, cAtModuleSdh);
    return mRegField(regVal, cAf6_gatetime_cfg_time_cfg_);
    }

static uint32 HwDurationMax(AtHwTimer self)
    {
    AtUnused(self);
    return cAf6_gatetime_cfg_time_cfg_Mask;
    }

static uint32 HwElapsedTimeGet(AtHwTimer self)
    {
    uint32 regAddr = CurrentRegister(self);
    uint32 regVal = AtHwTimerRead(self, regAddr, cAtModuleSdh);
    return mRegField(regVal, cAf6_Gatetime_current_currert_gatetime_diag_);
    }

static eAtRet HwStart(AtHwTimer self, eBool start)
    {
    uint32 regAddr = CtrRegister(self);
    uint32 regVal = AtHwTimerRead(self, regAddr, cAtModuleSdh);
    uint32 startShift = LocalId(self);
    uint32 startMask = cAf6_gatetime_ctr_start_diag1_Mask << startShift;
    mRegFieldSet(regVal, start, start);
    AtHwTimerWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eBool IsStarted(AtTimer self)
    {
    AtHwTimer timer = (AtHwTimer)self;
    uint32 regAddr = CtrRegister(timer);
    uint32 regVal = AtHwTimerRead(timer, regAddr, cAtModuleSdh);
    uint32 startShift = LocalId(timer);
    uint32 startMask = cAf6_gatetime_ctr_start_diag1_Mask << startShift;
    return (regVal & startMask) ? cAtTrue : cAtFalse;
    }

static eBool HwIsRunning(AtHwTimer self)
    {
    uint32 regAddr = CurrentRegister(self);
    uint32 regVal = AtHwTimerRead(self, regAddr, cAtModuleSdh);
    return (regVal & cAf6_Gatetime_current_status_gatetime_diag_Mask) ? cAtTrue : cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290021PrbsGatingTimerSerdesTuning *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeObjectDescription(tuner);
    }

static void OverrideAtObject(AtTimer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtHwTimer(AtTimer self)
    {
    AtHwTimer timer = (AtHwTimer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHwTimerOverride, mMethodsGet(timer), sizeof(m_AtHwTimerOverride));

        mMethodOverride(m_AtHwTimerOverride, Read);
        mMethodOverride(m_AtHwTimerOverride, Write);
        mMethodOverride(m_AtHwTimerOverride, IsSimulated);
        mMethodOverride(m_AtHwTimerOverride, HwDurationSet);
        mMethodOverride(m_AtHwTimerOverride, HwDurationGet);
        mMethodOverride(m_AtHwTimerOverride, HwDurationMax );
        mMethodOverride(m_AtHwTimerOverride, HwElapsedTimeGet);
        mMethodOverride(m_AtHwTimerOverride, HwStart);
        mMethodOverride(m_AtHwTimerOverride, HwIsRunning);
        }

    mMethodsSet(timer, &m_AtHwTimerOverride);
    }

static void OverrideTimer(AtTimer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtTimerOverride, mMethodsGet(self), sizeof(m_AtTimerOverride));

        mMethodOverride(m_AtTimerOverride, IsStarted);
        }

    mMethodsSet(self, &m_AtTimerOverride);
    }

static void Override(AtTimer self)
    {
    OverrideAtObject(self);
    OverrideTimer(self);
    OverrideAtHwTimer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PrbsGatingTimerSerdesTuning);
    }

static AtTimer ObjectInit(AtTimer self, Tha6029SerdesTuner tuner)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtHwTimerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->tuner = tuner;

    return self;
    }

AtTimer Tha60290021PrbsGatingTimerSerdesTuningNew(Tha6029SerdesTuner tuner)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtTimer newTimer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newTimer, tuner);
    }

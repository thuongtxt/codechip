/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290021PrbsGatingFactory.h
 * 
 * Created Date: Apr 25, 2017
 *
 * Description : PRBS gating timer factory
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021PRBSGATINGFACTORY_H_
#define _THA60290021PRBSGATINGFACTORY_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../physical/Tha6029SerdesTuner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021PrbsGatingFactory * Tha60290021PrbsGatingFactory;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60290021PrbsGatingFactory Tha60290021PrbsGatingFactoryNew(AtModulePrbs module);

AtTimer Tha60290021PrbsGatingFactoryBackplaneSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine);
AtTimer Tha60290021PrbsGatingFactoryFaceplateSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine);
AtTimer Tha60290021PrbsGatingFactorySerdesTuningTimerCreate(Tha60290021PrbsGatingFactory self, Tha6029SerdesTuner tuner);
AtTimer Tha60290021PrbsGatingFactorySgmiiSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine);
AtTimer Tha60290021PrbsGatingFactoryXfiSerdesTimerCreate(Tha60290021PrbsGatingFactory self, AtPrbsEngine engine);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021PRBSGATINGFACTORY_H_ */


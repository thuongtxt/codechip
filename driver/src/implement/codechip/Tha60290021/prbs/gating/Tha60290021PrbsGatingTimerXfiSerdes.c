/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290021PrbsGatingTimerXfiSerdes.c
 *
 * Created Date: Apr 7, 2017
 *
 * Description : Gating timer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../generic/prbs/AtPrbsGatingTimerInternal.h"
#include "../Tha6029XfiDiagReg.h"
#include "../Tha6029SerdesPrbsEngine.h"
#include "Tha60290021PrbsGatingTimer.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021PrbsGatingTimerXfiSerdes
    {
    tAtPrbsGatingTimer super;
    }tTha60290021PrbsGatingTimerXfiSerdes;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtTimerMethods   m_AtTimerOverride;
static tAtHwTimerMethods m_AtHwTimerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 AddressWithLocalAddress(AtHwTimer self, uint32 localAddress)
    {
    AtPrbsEngine engine = AtPrbsGatingTimerPrbsEngine((AtPrbsGatingTimer)self);
    return Tha6029XfiSerdesPrbsEngineAddressWithLocalAddress(engine, localAddress);
    }

static uint32 ControlRegister(AtHwTimer self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_icfgtime_Base);
    }

static eAtRet HwDurationSet(AtHwTimer self, uint32 hwDuration)
    {
    uint32 regAddr = ControlRegister(self);
    uint32 regVal = AtHwTimerRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_icfgtime_time_cfg_, hwDuration);
    AtHwTimerWrite(self, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static uint32 HwDurationGet(AtHwTimer self)
    {
    uint32 regAddr = ControlRegister(self);
    uint32 regVal  = AtHwTimerRead(self, regAddr, cAtModulePrbs);
    return mRegField(regVal, cAf6_icfgtime_time_cfg_);
    }

static eAtRet HwStart(AtHwTimer self, eBool start)
    {
    uint32 regAddr = ControlRegister(self);
    uint32 regVal  = AtHwTimerRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_icfgtime_start_gatetime_diag_, start ? 1 : 0);
    AtHwTimerWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool HwIsRunning(AtHwTimer self)
    {
    uint32 regAddr = ControlRegister(self);
    uint32 regVal  = AtHwTimerRead(self, regAddr, cAtModulePrbs);
    return (regVal & cAf6_icfgtime_status_gatetime_diag_Mask) ? cAtTrue : cAtFalse;
    }

static eBool IsStarted(AtTimer self)
    {
    AtHwTimer timer = (AtHwTimer)self;
    uint32 regAddr = ControlRegister(timer);
    uint32 regVal  = AtHwTimerRead(timer, regAddr, cAtModulePrbs);
    return (regVal & cAf6_icfgtime_start_gatetime_diag_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 HwDurationMax(AtHwTimer self)
    {
    AtUnused(self);
    return (cAf6_icfgtime_time_cfg_Mask >> cAf6_icfgtime_time_cfg_Shift);
    }

static uint32 HwElapsedTimeGet(AtHwTimer self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_istatuscfgtime_Base);
    uint32 regVal  = AtHwTimerRead(self, regAddr, cAtModulePrbs);
    uint32 elapsedSeconds = regVal & cAf6_istatuscfgtime_currert_gatetime_diag_Mask;
    return elapsedSeconds;
    }

static void OverrideTimer(AtTimer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtTimerOverride, mMethodsGet(self), sizeof(m_AtTimerOverride));

        mMethodOverride(m_AtTimerOverride, IsStarted);
        }

    mMethodsSet(self, &m_AtTimerOverride);
    }

static void OverrideAtHwTimer(AtTimer self)
    {
    AtHwTimer timer = (AtHwTimer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHwTimerOverride, mMethodsGet(timer), sizeof(m_AtHwTimerOverride));

        mMethodOverride(m_AtHwTimerOverride, HwDurationSet);
        mMethodOverride(m_AtHwTimerOverride, HwDurationGet);
        mMethodOverride(m_AtHwTimerOverride, HwDurationMax);
        mMethodOverride(m_AtHwTimerOverride, HwElapsedTimeGet);
        mMethodOverride(m_AtHwTimerOverride, HwStart);
        mMethodOverride(m_AtHwTimerOverride, HwIsRunning);
        }

    mMethodsSet(timer, &m_AtHwTimerOverride);
    }

static void Override(AtTimer self)
    {
    OverrideTimer(self);
    OverrideAtHwTimer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PrbsGatingTimerXfiSerdes);
    }

static AtTimer ObjectInit(AtTimer self, AtPrbsEngine engine)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsGatingTimerObjectInit(self, engine) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtTimer Tha60290021PrbsGatingTimerXfiSerdesNew(AtPrbsEngine engine)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtTimer newTimer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newTimer, engine);
    }

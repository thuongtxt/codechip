/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6029SgmiiSerdesPrbsEngineInternal.h
 * 
 * Created Date: Oct 22, 2016
 *
 * Description : SGMII SERDES PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SGMIISERDESPRBSENGINEINTERNAL_H_
#define _THA6029SGMIISERDESPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha6029SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SgmiiSerdesPrbsEngine * Tha6029SgmiiSerdesPrbsEngine;

typedef struct tTha6029SgmiiSerdesPrbsEngineMethods
    {
    uint32 (*HwId)(Tha6029SgmiiSerdesPrbsEngine self);
    }tTha6029SgmiiSerdesPrbsEngineMethods;

typedef struct tTha6029SgmiiSerdesPrbsEngine
    {
    tTha6029SerdesPrbsEngine super;
    const tTha6029SgmiiSerdesPrbsEngineMethods *methods;
    }tTha6029SgmiiSerdesPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6029SgmiiSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SGMIISERDESPRBSENGINEINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290021FacePlateSerdesPrbsEngineInternal.h
 * 
 * Created Date: Sep 25, 2016
 *
 * Description : Faceplate SERDES controller PRBS
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021FACEPLATESERDESPRBSENGINEINTERNAL_H_
#define _THA60290021FACEPLATESERDESPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha6029SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021FacePlateSerdesPrbsEngine * Tha60290021FacePlateSerdesPrbsEngine;

typedef struct tTha60290021FacePlateSerdesPrbsEngineMethods
    {
    eBool (*Prbs15DeprecatedByPrbs23)(Tha60290021FacePlateSerdesPrbsEngine self);
    }tTha60290021FacePlateSerdesPrbsEngineMethods;

typedef struct tTha60290021FacePlateSerdesPrbsEngine
    {
    tTha6029SerdesPrbsEngine super;
    const tTha60290021FacePlateSerdesPrbsEngineMethods *methods;

    /* Private data */
    eAtPrbsMode mode;
    }tTha60290021FacePlateSerdesPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60290021FacePlateSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021FACEPLATESERDESPRBSENGINEINTERNAL_H_ */


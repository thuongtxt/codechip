/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6029SerdesPrbsEngineInternal.h
 * 
 * Created Date: Sep 24, 2016
 *
 * Description : PRBS common engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SERDESPRBSENGINEINTERNAL_H_
#define _THA6029SERDESPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/prbs/AtPrbsEngineInternal.h"
#include "Tha6029SerdesPrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SerdesPrbsEngine
    {
    tAtPrbsEngine super;

    /* Private data */
    AtSerdesController serdes;
    }tTha6029SerdesPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6029SerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SERDESPRBSENGINEINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290021BackplaneSerdesPrbsEngineInternal.h
 * 
 * Created Date: Nov 9, 2017
 *
 * Description : Backplane serdes prbs engine internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290021_PRBS_THA60290021BACKPLANESERDESPRBSENGINEINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290021_PRBS_THA60290021BACKPLANESERDESPRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha6029SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021BackplaneSerdesPrbsEngine
    {
    tTha6029SerdesPrbsEngine super;
    }tTha60290021BackplaneSerdesPrbsEngine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha60290021BackplaneSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60290021_PRBS_THA60290021BACKPLANESERDESPRBSENGINEINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290021FacePlateSerdesPrbsEngine.c
 *
 * Created Date: Sep 21, 2016
 *
 * Description : Eth FacePlate SERDES PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../physical/Tha6029Physical.h"
#include "gating/Tha60290021PrbsGatingTimer.h"
#include "Tha60290021FacePlateSerdesPrbsEngineInternal.h"
#include "Tha60290021ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#define cRawStm64Prbs7  0x5
#define cRawStm64Prbs15 0x6
#define cRawStm64Prbs23 cRawStm64Prbs15 /* Later on, this replace PRBS15 */
#define cRawStm64Prbs32 0x7
#define cRawStm16Prbs7  0x8
#define cRawStm16Prbs15 0x9
#define cRawStm16Prbs23 cRawStm16Prbs15 /* Later on, this replace PRBS15 */
#define cRawStm16Prbs32 0xA

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021FacePlateSerdesPrbsEngine)self)
#define mSerdes(self) Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021FacePlateSerdesPrbsEngineMethods m_methods;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(void)
    {
    return cTopBaseAddress;
    }

static uint32 PortId(AtPrbsEngine self)
    {
    AtSerdesController controller = Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    return AtSerdesControllerHwIdGet(controller);
    }

static uint32 HwSerdesMode(AtSerdesController serdes)
    {
    return Tha60290021FacePlateSerdesControllerPrbsDefaultMode(serdes);
    }

static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    AtSerdesController serdes;
    uint32 ret = m_AtPrbsEngineMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    serdes = mSerdes(self);
    return Tha60290021FacePlateSerdesControllerPrbsHwModeSet(serdes, HwSerdesMode(serdes));
    }

static uint32 ErrInsShift(AtPrbsEngine self)
    {
    return 16 +  PortId(self);
    }

static uint32 ErrInsMask(AtPrbsEngine self)
    {
    return cBit16 << PortId(self);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    uint32 regAddr = cAf6Reg_o_control5_Base + BaseAddress();
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 errInsFieldMask  = ErrInsMask(self);
    uint32 errInsFieldShift = ErrInsShift(self);
    mRegFieldSet(regVal, errInsField, force ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    uint32 regAddr = cAf6Reg_o_control5_Base + BaseAddress();
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & ErrInsMask(self)) ? cAtTrue : cAtFalse;
    }

static ThaVersionReader VersionReader(AtPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet(self);
    return ThaDeviceVersionReader(device);
    }

static eBool Prbs15DeprecatedByPrbs23(Tha60290021FacePlateSerdesPrbsEngine self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x7, 0x00);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtPrbsEngine)self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool Prbs15IsSupported(AtPrbsEngine self)
    {
    return mMethodsGet(mThis(self))->Prbs15DeprecatedByPrbs23(mThis(self)) ? cAtFalse : cAtTrue;
    }

static eBool Prbs23IsSupported(AtPrbsEngine self)
    {
    return mMethodsGet(mThis(self))->Prbs15DeprecatedByPrbs23(mThis(self)) ? cAtTrue : cAtFalse;
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);

    switch ((uint32) prbsMode)
        {
        case cAtPrbsModePrbs7  : return cAtTrue;
        case cAtPrbsModePrbs15 : return Prbs15IsSupported(self);
        case cAtPrbsModePrbs23 : return Prbs23IsSupported(self);
        case cAtPrbsModePrbs31 : return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 Stm64PrbsModeSw2Hw(eAtPrbsMode mode)
    {
    switch ((uint32) mode)
        {
        case cAtPrbsModePrbs7 : return cRawStm64Prbs7;
        case cAtPrbsModePrbs15: return cRawStm64Prbs15;
        case cAtPrbsModePrbs23: return cRawStm64Prbs23;
        case cAtPrbsModePrbs31: return cRawStm64Prbs32;
        default:
            return cRawStm64Prbs15;
        }
    }

static eAtPrbsMode Stm64PrbsModeHw2Sw(AtPrbsEngine self, uint32 hwMode)
    {
    switch (hwMode)
        {
        case cRawStm64Prbs7 : return cAtPrbsModePrbs7;
        case cRawStm64Prbs15: return mMethodsGet(mThis(self))->Prbs15DeprecatedByPrbs23(mThis(self)) ? cAtPrbsModePrbs23 : cAtPrbsModePrbs15;
        case cRawStm64Prbs32: return cAtPrbsModePrbs31;
        default:
            return cAtPrbsModeInvalid;
        }
    }

static uint32 LowRatePrbsModeSw2Hw(eAtPrbsMode mode)
    {
    switch ((uint32) mode)
        {
        case cAtPrbsModePrbs7 : return cRawStm16Prbs7;
        case cAtPrbsModePrbs15: return cRawStm16Prbs15;
        case cAtPrbsModePrbs23: return cRawStm16Prbs23;
        case cAtPrbsModePrbs31: return cRawStm16Prbs32;
        default:
            return cRawStm16Prbs15;
        }
    }

static eAtPrbsMode LowRatePrbsModeHw2Sw(AtPrbsEngine self, uint32 hwMode)
    {
    switch (hwMode)
        {
        case cRawStm16Prbs7 : return cAtPrbsModePrbs7;
        case cRawStm16Prbs15: return mMethodsGet(mThis(self))->Prbs15DeprecatedByPrbs23(mThis(self)) ? cAtPrbsModePrbs23 : cAtPrbsModePrbs15;
        case cRawStm16Prbs32: return cAtPrbsModePrbs31;
        default:
            return cAtPrbsModeInvalid;
        }
    }

static uint32 PrbsModeSw2Hw(AtPrbsEngine self, eAtPrbsMode mode)
    {
    eAtSerdesMode serdesMode = AtSerdesControllerModeGet(mSerdes(self));

    if ((serdesMode == cAtSerdesModeStm64) ||
        (serdesMode == cAtSerdesModeEth10G))
        return Stm64PrbsModeSw2Hw(mode);

    return LowRatePrbsModeSw2Hw(mode);
    }

static eAtPrbsMode PrbsModeHw2Sw(AtPrbsEngine self, uint32 hwMode)
    {
    eAtSerdesMode serdesMode = AtSerdesControllerModeGet(mSerdes(self));

    if ((serdesMode == cAtSerdesModeStm64) ||
        (serdesMode == cAtSerdesModeEth10G))
        return Stm64PrbsModeHw2Sw(self, hwMode);

    return LowRatePrbsModeHw2Sw(self, hwMode);
    }

static uint32 EnableShift(AtPrbsEngine self)
    {
    return PortId(self);
    }

static uint32 EnableMask(AtPrbsEngine self)
    {
    return cBit0 << PortId(self);
    }

static eAtModulePrbsRet RawPrbsEnable(AtPrbsEngine self, eBool enable)
    {
    uint32 regAddr = cAf6Reg_o_control5_Base + BaseAddress();
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 enableFieldMask  = EnableMask(self);
    uint32 enableFieldShift = EnableShift(self);
    mRegFieldSet(regVal, enableField, enable ? 1 : 0);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static eBool RawPrbsIsEnabled(AtPrbsEngine self)
    {
    uint32 regAddr = cAf6Reg_o_control5_Base + BaseAddress();
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return (regVal & EnableMask(self)) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint32 mode = HwSerdesMode(mSerdes(self));

    RawPrbsEnable(self, enable);

    /* Need to go back from raw PRBS mode to framer mode if PRBS is disabled */
    if (enable)
        mode = PrbsModeSw2Hw(self, mThis(self)->mode);

    return Tha60290021FacePlateSerdesControllerPrbsHwModeSet(mSerdes(self), mode);
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    return RawPrbsIsEnabled(self);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsEngineAlarmTypeNone;
    }

static uint32 AlarmHistoryRead2Clear(AtPrbsEngine self, eBool read2Clear)
    {
    uint32 regAddr = cAf6Reg_i_sticky1_Base + BaseAddress();
    uint32 regValue  = AtPrbsEngineRead(self, regAddr, cAtModuleEth);
    uint32 errorMask = cBit0 << PortId(self);

    if (read2Clear)
        AtPrbsEngineWrite(self, regAddr, errorMask, cAtModuleEth);

    return (regValue & errorMask) ? cAtPrbsEngineAlarmTypeError : cAtPrbsEngineAlarmTypeNone;
    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    if (!AtPrbsEngineModeIsSupported(self, prbsMode))
        return cAtErrorModeNotSupport;

    /* Raw PRBS and framer mode cannot work at the same time */
    mThis(self)->mode = prbsMode;
    if (AtPrbsEngineIsEnabled(self))
        return Tha60290021FacePlateSerdesControllerPrbsHwModeSet(mSerdes(self), PrbsModeSw2Hw(self, prbsMode));

    return cAtOk;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    if (AtPrbsEngineIsEnabled(self))
        return PrbsModeHw2Sw(self, Tha60290021FacePlateSerdesControllerPrbsHwModeGet(mSerdes(self)));

    return mThis(self)->mode;
    }

static eBool SideIsSupported(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorNotApplicable;
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static AtTimer TimerObjectCreate(AtPrbsEngine self)
    {
    Tha60290021PrbsGatingFactory factory = Tha6029SerdesPrbsEnginePrbsGatingFactory((Tha6029SerdesPrbsEngine)self);
    return Tha60290021PrbsGatingFactoryFaceplateSerdesTimerCreate(factory, self);
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290021FacePlateSerdesPrbsEngine object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(mode);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, SideIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, TimerObjectCreate);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static void MethodsInit(Tha60290021FacePlateSerdesPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Prbs15DeprecatedByPrbs23);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021FacePlateSerdesPrbsEngine);
    }

AtPrbsEngine Tha60290021FacePlateSerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesPrbsEngineObjectInit(self, serdesController) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->mode = cAtPrbsModePrbs15;

    return self;
    }

AtPrbsEngine Tha60290021FacePlateSerdesPrbsEngineNew(AtSerdesController serdesController)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021FacePlateSerdesPrbsEngineObjectInit(newEngine, serdesController);
    }

uint32 Tha60290021FacePlateSerdesPrbsEnginePortId(AtPrbsEngine self)
    {
    if (self)
        return PortId(self);
    return cInvalidUint32;
    }

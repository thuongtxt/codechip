/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290021AuVcPrbsEngine.c
 *
 * Created Date: Nov 8, 2016
 *
 * Description : Ho-Au-Vc-PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhPathInternal.h"
#include "../../Tha60210011/map/Tha60210011ModuleMapInternal.h"
#include "../sdh/Tha60290021SdhLineSideAug.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../sdh/Tha6029SdhTerminatedLineAuVc.h"
#include "Tha60290021AuVcPrbsEngine.h"
#include "Tha60290021ModuleMapHoReg.h"
#include "Tha60290021AuVcPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mHwSlice(self) ThaPrbsEngineHwSliceId(mThaPrbsEngine(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods                 m_AtPrbsEngineOverride;
static tThaPrbsEngineMethods                m_ThaPrbsEngineOverride;
static tThaPrbsEngineAuVcMethods            m_ThaPrbsEngineAuVcOverride;
static tTha60210011AuVcPrbsEngineMethods    m_Tha60210011AuVcPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods          *m_AtPrbsEngineMethods      = NULL;
static const tThaPrbsEngineMethods         *m_ThaPrbsEngineMethods     = NULL;
static const tThaPrbsEngineAuVcMethods     *m_ThaPrbsEngineAuVcMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleXc XcModule(AtPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet(self);
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static eBool XcHidingShouldRedirect(AtPrbsEngine self)
    {
    return (Tha60290021ModuleXcHideModeGet(XcModule(self)) == cTha60290021XcHideModeDirect);
    }

static const char *ChannelDescription(AtPrbsEngine self)
    {
    if (XcHidingShouldRedirect(self))
        {
        AtChannel terVc = AtPrbsEngineChannelGet(self);
        return AtChannelIdDescriptionBuild(AtChannelSourceGet(terVc));
        }

    return m_AtPrbsEngineMethods->ChannelDescription(self);
    }

static uint32 OffsetWithSliceId(uint32 engineId, uint32 sliceId)
    {
    return cHoBaseAddress + sliceId * 2048UL + engineId;
    }

static uint32 DefaultOffset(uint32 engineId)
    {
    uint32 groupId = engineId/8;
    uint32 localId = engineId%8;
    return cHoBaseAddress + localId * 2048UL + groupId;
    }

static uint32 EngineOffset(ThaPrbsEngine self)
    {
    uint32 engineId = mHwEngine(self);
    return DefaultOffset(engineId);
    }

static uint32 ChannelOffsetWithSliceAndEngineId(Tha60210011AuVcPrbsEngine self)
    {
    return OffsetWithSliceId(mHwEngine(self), mHwSlice(self));
    }

static uint32 ChannelOffsetWithSliceId(Tha60210011AuVcPrbsEngine self)
    {
    return OffsetWithSliceId(0, mHwSlice(self));
    }

static uint32 TdmGenControlRegister(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_ctrl_pen_gen_Base + ChannelOffsetWithSliceAndEngineId(self);
    }

static uint32 TdmMonControlRegister(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_ctrl_pen_tdm_mon_Base + ChannelOffsetWithSliceAndEngineId(self);
    }

static uint32 PsnMonControlRegister(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_ctrl_pen_pw_mon_Base + ChannelOffsetWithSliceAndEngineId(self);
    }

static uint32 TxTdmGoodBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_goodbit_ber_pen_Base + ChannelOffsetWithSliceAndEngineId(self);
    }

static uint32 RxTdmGoodBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_goodbit_pen_tdm_mon_Base + ChannelOffsetWithSliceAndEngineId(self);
    }

static uint32 RxTdmErrorBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_err_pen_tdm_mon_Base + ChannelOffsetWithSliceAndEngineId(self);
    }

static uint32 RxTdmLossBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_lossbit_pen_tdm_mon_Base + ChannelOffsetWithSliceAndEngineId(self);
    }

static uint32 RxPsnGoodBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_goodbit_pen_pw_mon_Base + ChannelOffsetWithSliceAndEngineId(self);
    }

static uint32 RxPsnErrorBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_errbit_pen_pw_mon_Base + ChannelOffsetWithSliceAndEngineId(self);
    }

static uint32 RxPsnLossBitCounterReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_lossbit_pen_pw_mon_Base + ChannelOffsetWithSliceAndEngineId(self);
    }

static uint32 RxTdmLossStickyReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_loss_tdm_mon_Base + ChannelOffsetWithSliceId(self);
    }

static uint32 RxPsnLossStickyReg(Tha60210011AuVcPrbsEngine self)
    {
    return cAf6Reg_loss_pw_mon_Base + ChannelOffsetWithSliceId(self);
    }

static uint32 LossAlarmStickyMask(Tha60210011AuVcPrbsEngine self)
    {
    uint32 engineId = mHwEngine(self);
    return (cBit0 << engineId);
    }

static uint32 BertGenSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    return cAf6Reg_sel_ho_bert_gen0_Base + EngineOffset(self);
    }

static uint32 BertMonTdmSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    return cAf6Reg_sel_ho_bert_tdm_mon0_Base + EngineOffset(self);
    }

static uint32 BertMonPsnSelectedChannelRegAddress(ThaPrbsEngine self)
    {
    return cAf6Reg_sel_ho_bert_pw_mon_g0_Base + EngineOffset(self);
    }

static uint32 TdmMonitoringStatusRegAddress(ThaPrbsEngine self, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    return cAf6Reg_stt_tdm_mon_Base + OffsetWithSliceId(engineId, slice);
    }

static uint32 PsnMonitoringStatusRegAddress(ThaPrbsEngine self, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    return cAf6Reg_stt_pw_mon_Base + OffsetWithSliceId(engineId, slice);
    }

static uint32 BertGenErrorRateInsertRegAddress(ThaPrbsEngine self)
    {
    Tha60210011AuVcPrbsEngine engine = (Tha60210011AuVcPrbsEngine)self;
    return cAf6Reg_ctrl_ber_pen_Base + ChannelOffsetWithSliceAndEngineId(engine);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    if (prbsMode == cAtPrbsModePrbs31)
        return cAtTrue;

    return m_AtPrbsEngineMethods->ModeIsSupported(self, prbsMode);
    }

static void PohInsert(ThaPrbsEngineAuVc self, AtSdhChannel channel, eBool enable)
    {
    AtUnused(self);
    AtSdhPathPohInsertionEnable((AtSdhPath)channel, enable);
    }

static eAtModulePrbsRet HwTxErrorInject(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet(self);
    uint32 regAddr = cAf6Reg_ctrl_ber_pen_single_Base + cHoBaseAddress + ThaPrbsEngineHwSliceId((ThaPrbsEngine)self) * 2048UL;
    uint32 regVal = mChannelHwRead(channel, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cAf6_ctrl_ber_pen_single_ID_force_, mHwEngine(self));
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtModulePrbsRet TxErrorInject(AtPrbsEngine self, uint32 numErrors)
    {
    if (numErrors == 1)
        return HwTxErrorInject(self);

    return (numErrors == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool TxErrorInjectionIsSupported(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtSdhChannel ChannelForPohInsertion(ThaPrbsEngineAuVc self, AtSdhChannel channel)
    {
    if (!Tha60290021ModuleSdhIsTerminatedVc(channel))
        return m_ThaPrbsEngineAuVcMethods->ChannelForPohInsertion(self, channel);

    return (AtSdhChannel)AtChannelSourceGet((AtChannel)channel);
    }

static eAtRet MapTypeUpdate(ThaPrbsEngine self)
    {
    AtSdhChannel vc = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    return Tha6029SdhTerminatedLineAuVcMapTypeUpdate(vc, AtSdhChannelMapTypeGet(vc));
    }

static eAtRet Channelize(ThaPrbsEngine self)
    {
    eAtRet ret;

    ret = m_ThaPrbsEngineMethods->Channelize(self);
    if (ret != cAtOk)
        return ret;

    return MapTypeUpdate(self);
    }

static eAtRet Unchannelize(ThaPrbsEngine self)
    {
    eAtRet ret;

    ret = m_ThaPrbsEngineMethods->Unchannelize(self);
    if (ret != cAtOk)
        return ret;

    return MapTypeUpdate(self);
    }

static void OverrideThaPrbsEngineAuVc(AtPrbsEngine self)
    {
    ThaPrbsEngineAuVc engine = (ThaPrbsEngineAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineAuVcMethods = mMethodsGet(engine);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineAuVcOverride, m_ThaPrbsEngineAuVcMethods, sizeof(m_ThaPrbsEngineAuVcOverride));

        mMethodOverride(m_ThaPrbsEngineAuVcOverride, PohInsert);
        mMethodOverride(m_ThaPrbsEngineAuVcOverride, ChannelForPohInsertion);
        }

    mMethodsSet(engine, &m_ThaPrbsEngineAuVcOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, ChannelDescription);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorInject);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorInjectionIsSupported);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideThaPrbsEngine(ThaPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsEngineOverride, mMethodsGet(self), sizeof(m_ThaPrbsEngineOverride));

        mMethodOverride(m_ThaPrbsEngineOverride, BertGenSelectedChannelRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, BertMonTdmSelectedChannelRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, BertMonPsnSelectedChannelRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, BertGenErrorRateInsertRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, TdmMonitoringStatusRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, PsnMonitoringStatusRegAddress);
        mMethodOverride(m_ThaPrbsEngineOverride, Unchannelize);
        mMethodOverride(m_ThaPrbsEngineOverride, Channelize);
        }

    mMethodsSet(self, &m_ThaPrbsEngineOverride);
    }

static void OverrideTha60210011AuVcPrbsEngine(ThaPrbsEngine self)
    {
    Tha60210011AuVcPrbsEngine engine = (Tha60210011AuVcPrbsEngine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011AuVcPrbsEngineOverride, mMethodsGet(engine), sizeof(m_Tha60210011AuVcPrbsEngineOverride));

        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, TdmGenControlRegister);
        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, TdmMonControlRegister);
        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, PsnMonControlRegister);
        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, TxTdmGoodBitCounterReg);
        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, RxTdmGoodBitCounterReg);
        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, RxTdmErrorBitCounterReg);
        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, RxTdmLossBitCounterReg);
        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, RxPsnGoodBitCounterReg);
        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, RxPsnErrorBitCounterReg);
        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, RxPsnLossBitCounterReg);
        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, RxTdmLossStickyReg);
        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, RxPsnLossStickyReg);
        mMethodOverride(m_Tha60210011AuVcPrbsEngineOverride, LossAlarmStickyMask);
        }

    mMethodsSet(engine, &m_Tha60210011AuVcPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideThaPrbsEngineAuVc(self);
    OverrideTha60210011AuVcPrbsEngine((ThaPrbsEngine)self);
    OverrideThaPrbsEngine((ThaPrbsEngine)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021AuVcPrbsEngine);
    }

AtPrbsEngine Tha60290021AuVcPrbsEngineObjectInit(AtPrbsEngine self, AtSdhChannel vc, uint32 engineId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011AuVcPrbsEngineObjectInit(self, vc, engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha60290021AuVcPrbsEngineNew(AtSdhChannel vc, uint32 engineId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60290021AuVcPrbsEngineObjectInit(newEngine, vc, engineId);
    }

uint32 Tha60290021AuVcPrbsEngineDefaultOffset(uint32 engineId)
    {
    return DefaultOffset(engineId);
    }

uint32 Tha60290021AuVcPrbsEngineOffsetWithSliceId(uint32 engineId, uint32 sliceId)
    {
    return OffsetWithSliceId(engineId, sliceId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha60290021PrbsRegProvider.c
 *
 * Created Date: Oct 11, 2016
 *
 * Description : Register provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290021PrbsRegProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cBertTdmGenBaseAddress                       (0x350000UL)
#define cBertTdmMonBaseAddress                       (0x360000UL)
#define cBertPsnMonBaseAddress                       (0x370000UL)

/* GEN */
#define cThaRegMapBERTGenFixedPatternControl         (0x8320 + cBertTdmGenBaseAddress)
#define cThaRegMapBERTGenErrorRateInsert             (0x83C0 + cBertTdmGenBaseAddress)
#define cThaRegMapBERTGenSingleBitErrInsertG0        (0x83EE + cBertTdmGenBaseAddress)
#define cThaRegMapBERTGenSingleBitErrInsertG1        (0x83FE + cBertTdmGenBaseAddress)

/* MON PW */
#define cThaRegMapBERTMonSelectedChannel             (0x8500 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonMode                        (0x8420 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonFixedPatternControl         (0x8440 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonStatus                      (0x84E0 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonErrorCounter                (0x8540 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonGoodBitCounter              (0x8580 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonLossBitCounter              (0x85C0 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonStickyG0                    (0x8402 + cBertPsnMonBaseAddress)
#define cThaRegMapBERTMonStickyG1                    (0x8412 + cBertPsnMonBaseAddress)

/* MON TDM */
#define cThaRegDemapBERTMonSelectedChannel          (0x8500 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonMode                     (0x8420 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonFixedPatternControl      (0x8440 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonStatus                   (0x84E0 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonErrorCounter             (0x8540 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonGoodBitCounter           (0x8580 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonLossBitCounter           (0x85C0 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonStickyG0                 (0x8402 + cBertTdmMonBaseAddress)
#define cThaRegDemapBERTMonStickyG1                 (0x8412 + cBertTdmMonBaseAddress)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPrbsRegProviderMethods m_ThaPrbsRegProviderOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RegMapBERTGenTxSingleErrMask(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return cBit4_0;
    }

static uint8  RegMapBERTGenTxSingleErrShift(ThaPrbsRegProvider self, ThaModulePrbs prbsModule)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    return 0;
    }

static uint32 RegMapBERTGenSingleBitErrInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 groupId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return (groupId == 0) ? cThaRegMapBERTGenSingleBitErrInsertG0 : cThaRegMapBERTGenSingleBitErrInsertG1;
    }

static uint32 RegMapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 groupId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return (groupId == 0) ? cThaRegMapBERTMonStickyG0 : cThaRegMapBERTMonStickyG1;
    }

static uint32 RegDemapBERTMonSticky(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 groupId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return (groupId == 0) ? cThaRegDemapBERTMonStickyG0 : cThaRegDemapBERTMonStickyG1;
    }

static uint32 RegMapBERTGenErrorRateInsert(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTGenErrorRateInsert + engineId;
    }

static uint32 RegMapBERTGenFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(slice);
    AtUnused(prbsModule);
    return cThaRegMapBERTGenFixedPatternControl + engineId;
    }

static uint32 RegMapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonSelectedChannel + engineId;
    }

static uint32 RegMapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonMode + engineId;
    }

static uint32 RegMapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonFixedPatternControl + engineId;
    }

static uint32 RegMapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonStatus + engineId;
    }

static uint32 RegMapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonErrorCounter + engineId;
    }

static uint32 RegMapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonGoodBitCounter + engineId;
    }

static uint32 RegMapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegMapBERTMonLossBitCounter + engineId;
    }

static uint32 RegDemapBERTMonSelectedChannel(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonSelectedChannel + engineId;
    }

static uint32 RegDemapBERTMonMode(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonMode + engineId;
    }

static uint32 RegDemapBERTMonFixedPatternControl(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonFixedPatternControl + engineId;
    }

static uint32 RegDemapBERTMonErrorCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonErrorCounter + engineId;
    }

static uint32 RegDemapBERTMonGoodBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonGoodBitCounter + engineId;
    }

static uint32 RegDemapBERTMonLossBitCounter(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonLossBitCounter + engineId;
    }

static uint32 RegDemapBERTMonStatus(ThaPrbsRegProvider self, ThaModulePrbs prbsModule, uint32 engineId, uint32 slice)
    {
    AtUnused(self);
    AtUnused(prbsModule);
    AtUnused(slice);
    return cThaRegDemapBERTMonStatus + engineId;
    }

static void OverrideThaPrbsRegProvider(ThaPrbsRegProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPrbsRegProviderOverride, mMethodsGet(self), sizeof(m_ThaPrbsRegProviderOverride));

        /* GEN */
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenErrorRateInsert);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenSingleBitErrInsert);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenFixedPatternControl);

        /* MON */
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonMode);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonFixedPatternControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonStatus);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonErrorCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonLossBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTMonSticky);

        /* MON PW */
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonSelectedChannel);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonMode);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonFixedPatternControl);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonErrorCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonGoodBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonLossBitCounter);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonStatus);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegDemapBERTMonSticky);

        /* Bit fields */
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxSingleErrMask);
        mMethodOverride(m_ThaPrbsRegProviderOverride, RegMapBERTGenTxSingleErrShift);
        }

    mMethodsSet(self, &m_ThaPrbsRegProviderOverride);
    }

static void Override(ThaPrbsRegProvider self)
    {
    OverrideThaPrbsRegProvider(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PrbsRegProvider);
    }

ThaPrbsRegProvider Tha60290021PrbsRegProviderObjectInit(ThaPrbsRegProvider self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PrbsRegProviderObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPrbsRegProvider Tha60290021PrbsRegProvider(void)
    {
    static tTha60290021PrbsRegProvider shareProvider;
    static ThaPrbsRegProvider pShareProvider = NULL;
    if (pShareProvider == NULL)
        pShareProvider = Tha60290021PrbsRegProviderObjectInit((ThaPrbsRegProvider)&shareProvider);
    return pShareProvider;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290021ModulePrbsInternal.h
 * 
 * Created Date: Apr 25, 2017
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPRBSINTERNAL_H_
#define _THA60290021MODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/prbs/Tha60210051ModulePrbsInternal.h"
#include "Tha60290021ModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModulePrbsMethods
    {
    Tha60290021PrbsGatingFactory (*PrbsGatingFactoryObjectCreate)(Tha60290021ModulePrbs self);
    AtPrbsEngine (*AuVc4_64cPrbsEngineObjectCreate)(Tha60290021ModulePrbs self, uint32 engineId, AtSdhChannel sdhVc);
    AtPrbsEngine (*AuVcxPrbsEngineObjectCreate)(Tha60290021ModulePrbs self, uint32 engineId, AtSdhChannel sdhVc);
    uint32 (*NumAuVc4_64cEngines)(Tha60290021ModulePrbs self);
    }tTha60290021ModulePrbsMethods;

typedef struct tTha60290021ModulePrbs
    {
    tTha60210051ModulePrbs super;
    const tTha60290021ModulePrbsMethods *methods;

    /* Private data */
    Tha60290021PrbsGatingFactory gatingTimerFactory;
    }tTha60290021ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha60290021ModulePrbsObjectInit(AtModulePrbs self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPRBSINTERNAL_H_ */


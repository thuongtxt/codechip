/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290021AuVcPrbsEngine.h
 * 
 * Created Date: Nov 8, 2016
 *
 * Description : Tha60290021AuVcPrbsEngine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021AUVCPRBSENGINE_H_
#define _THA60290021AUVCPRBSENGINE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60290021AuVcPrbsEngineDefaultOffset(uint32 engineId);
uint32 Tha60290021AuVcPrbsEngineOffsetWithSliceId(uint32 engineId, uint32 sliceId);

AtPrbsEngine Tha60290021AuVcPrbsEngineNew(AtSdhChannel vc, uint32 engineId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021AUVCPRBSENGINE_H_ */


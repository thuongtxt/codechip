/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha60290021PrbsRegProviderInternal.h
 * 
 * Created Date: May 1, 2017
 *
 * Description : Register provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021PRBSREGPROVIDERINTERNAL_H_
#define _THA60290021PRBSREGPROVIDERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/prbs/Tha60210011PrbsRegProvider.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021PrbsRegProvider
    {
    tTha60210011PrbsRegProvider super;
    }tTha60290021PrbsRegProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPrbsRegProvider Tha60290021PrbsRegProviderObjectInit(ThaPrbsRegProvider self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021PRBSREGPROVIDERINTERNAL_H_ */


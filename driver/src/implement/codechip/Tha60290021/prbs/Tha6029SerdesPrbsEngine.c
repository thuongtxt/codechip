/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6029SerdesPrbsEngine.c
 *
 * Created Date: Sep 21, 2016
 *
 * Description : SERDES PRBS engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "Tha60290021ModulePrbs.h"
#include "AtSerdesController.h"
#include "Tha6029SerdesPrbsEngineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha6029SerdesPrbsEngine)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods     m_AtObjectOverride;
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtObjectMethods     *m_AtObjectMethods     = NULL;
static const tAtPrbsEngineMethods *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModulePrbsRet Init(AtPrbsEngine self)
    {
    uint32 ret = AtPrbsEngineErrorForce(self, cAtFalse);
    if (ret != cAtOk)
        return ret;

    return AtPrbsEngineEnable(self, cAtFalse);
    }

static AtDevice DeviceGet(AtPrbsEngine self)
    {
    return AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(mThis(self)->serdes));
    }

static AtModule Module(AtPrbsEngine self, eAtModule module)
    {
    return AtDeviceModuleGet(AtPrbsEngineDeviceGet(self), module);
    }

static uint32 Read(AtPrbsEngine self, uint32 address, eAtModule module)
    {
    return mModuleHwRead(Module(self, module), address);
    }

static void Write(AtPrbsEngine self, uint32 address, uint32 value, eAtModule module)
    {
    mModuleHwWrite(Module(self, module), address, value);
    }

static eAtModulePrbsRet SideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return ((side == cAtPrbsSidePsn) ? cAtOk : cAtErrorModeNotSupport);
    }

static eAtPrbsSide SideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSidePsn;
    }

static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    return AtPrbsEngineSideGet(self);
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    return AtPrbsEngineSideGet(self);
    }

static const char *ChannelDescription(AtPrbsEngine self)
    {
    AtSerdesController serdes = Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    return AtObjectToString((AtObject)serdes);
    }

static AtChannel ChannelGet(AtPrbsEngine self)
    {
    return AtSerdesControllerPhysicalPortGet(mThis(self)->serdes);
    }

static Tha60290021PrbsGatingFactory PrbsGatingFactory(Tha6029SerdesPrbsEngine self)
    {
    AtSerdesController serdes = Tha6029SerdesPrbsEngineSerdesControllerGet((Tha6029SerdesPrbsEngine)self);
    AtDevice device = AtSerdesControllerDeviceGet(serdes);
    Tha60290021ModulePrbs prbsModule = (Tha60290021ModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);
    return Tha60290021ModulePrbsGatingFactory(prbsModule);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha6029SerdesPrbsEngine object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(serdes);
    }

static void OverrideAtObject(AtPrbsEngine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Init);
        mMethodOverride(m_AtPrbsEngineOverride, Read);
        mMethodOverride(m_AtPrbsEngineOverride, Write);
        mMethodOverride(m_AtPrbsEngineOverride, SideSet);
        mMethodOverride(m_AtPrbsEngineOverride, SideGet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, DeviceGet);
        mMethodOverride(m_AtPrbsEngineOverride, ChannelDescription);
        mMethodOverride(m_AtPrbsEngineOverride, ChannelGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtObject(self);
    OverrideAtPrbsEngine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SerdesPrbsEngine);
    }

AtPrbsEngine Tha6029SerdesPrbsEngineObjectInit(AtPrbsEngine self, AtSerdesController serdesController)
    {
    uint32 engineId = AtSerdesControllerIdGet(serdesController);
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPrbsEngineObjectInit(self, AtSerdesControllerPhysicalPortGet(serdesController), engineId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->serdes = serdesController;

    return self;
    }

AtSerdesController Tha6029SerdesPrbsEngineSerdesControllerGet(Tha6029SerdesPrbsEngine self)
    {
    if (self)
        return mThis(self)->serdes;

    return NULL;
    }

Tha60290021PrbsGatingFactory Tha6029SerdesPrbsEnginePrbsGatingFactory(Tha6029SerdesPrbsEngine self)
    {
    if (self)
        return PrbsGatingFactory(self);
    return NULL;
    }

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_RD_MAP_HO_H_
#define _AF6_REG_AF6CNC0021_RD_MAP_HO_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Demap Channel Control
Reg Addr   : 0x0000-0x007FF
Reg Formula: 0x0000 + 256*slice + stsid
    Where  : 
           + $slice(0-7): is OC48 slice
           + $stsid(0-47): is STS in OC48
Reg Desc   : 
The registers are used by the hardware to configure PW channel

------------------------------------------------------------------------------*/
#define cAf6Reg_demap_channel_ctrl_Base                                                                 0x0000

/*--------------------------------------
BitField Name: Demapsr_vc3n3c
BitField Type: RW
BitField Desc: 0:slave of VC3_N3c  1:master of VC3-N3c
BitField Bits: [2]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_Mask                                                      cBit2
#define cAf6_demap_channel_ctrl_Demapsr_vc3n3c_Shift                                                         2

/*--------------------------------------
BitField Name: Demapsrctype
BitField Type: RW
BitField Desc: 0:VC3 1:VC3-3c
BitField Bits: [1]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_Demapsrctype_Mask                                                        cBit1
#define cAf6_demap_channel_ctrl_Demapsrctype_Shift                                                           1

/*--------------------------------------
BitField Name: DemapChEn
BitField Type: RW
BitField Desc: PW/Channels Enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapChEn_Mask                                                           cBit0
#define cAf6_demap_channel_ctrl_DemapChEn_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Map Line Control
Reg Addr   : 0x4000-0x47FF
Reg Formula: 0x4000 + 256*slice + stsid
    Where  : 
           + $slice(0-7): is OC48 slice
           + $stsid(0-47): is STS in OC48
Reg Desc   : 
The registers provide the per line configurations for STS

------------------------------------------------------------------------------*/
#define cAf6Reg_map_line_ctrl_Base                                                                      0x4000

/*--------------------------------------
BitField Name: Mapsrc_vc3n3c
BitField Type: RW
BitField Desc: 0:slave of VC3_N3c  1:master of VC3-N3c
BitField Bits: [9]
--------------------------------------*/
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_Mask                                                            cBit9
#define cAf6_map_line_ctrl_Mapsrc_vc3n3c_Shift                                                               9

/*--------------------------------------
BitField Name: Mapsrc_type
BitField Type: RW
BitField Desc: 0:VC3 1:VC3-3c
BitField Bits: [8]
--------------------------------------*/
#define cAf6_map_line_ctrl_Mapsrc_type_Mask                                                              cBit8
#define cAf6_map_line_ctrl_Mapsrc_type_Shift                                                                 8

/*--------------------------------------
BitField Name: MapChEn
BitField Type: RW
BitField Desc: PW/Channels Enable
BitField Bits: [7]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapChEn_Mask                                                                  cBit7
#define cAf6_map_line_ctrl_MapChEn_Shift                                                                     7

/*--------------------------------------
BitField Name: MapTimeSrcMaster
BitField Type: RW
BitField Desc: This bit is used to indicate the master timing or the master VC3
in VC4/VC4-Xc
BitField Bits: [6]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapTimeSrcMaster_Mask                                                         cBit6
#define cAf6_map_line_ctrl_MapTimeSrcMaster_Shift                                                            6

/*--------------------------------------
BitField Name: MapTimeSrcId
BitField Type: RW
BitField Desc: The reference line ID used for timing reference or the master VC3
ID in VC4/VC4-Xc
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_map_line_ctrl_MapTimeSrcId_Mask                                                           cBit5_0
#define cAf6_map_line_ctrl_MapTimeSrcId_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Map Global Control
Reg Addr   : 0x40FF
Reg Formula: 0x40FF
    Where  : 
Reg Desc   : 
The registers provide the per line configurations for STS

------------------------------------------------------------------------------*/
#define cAf6Reg_map_global_ctrl_Base                                                                    0x40FF

/*--------------------------------------
BitField Name: MapLoopOut
BitField Type: RW
BitField Desc: This bit is used to loopout full bus
BitField Bits: [8]
--------------------------------------*/
#define cAf6_map_global_ctrl_MapLoopOut_Mask                                                             cBit8
#define cAf6_map_global_ctrl_MapLoopOut_Shift                                                                8

/*--------------------------------------
BitField Name: MapIdlePattern
BitField Type: RW
BitField Desc: Idle pattern
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_map_global_ctrl_MapIdlePattern_Mask                                                       cBit7_0
#define cAf6_map_global_ctrl_MapIdlePattern_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert Gen0
Reg Addr   : 0x8_200 - 0xB_A00
Reg Formula: 0x8_200 + 2048*engid_g0
    Where  : 
           + $engid_g0 (0-7): id 1-8 of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_gen0_Base                                                                   0x8200

/*--------------------------------------
BitField Name: gen_en
BitField Type: RW
BitField Desc: set "1" to enable bert gen
BitField Bits: [9]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen0_gen_en_Mask                                                                cBit9
#define cAf6_sel_ho_bert_gen0_gen_en_Shift                                                                   9

/*--------------------------------------
BitField Name: line_id
BitField Type: RW
BitField Desc: line id OC48
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen0_line_id_Mask                                                             cBit8_6
#define cAf6_sel_ho_bert_gen0_line_id_Shift                                                                  6

/*--------------------------------------
BitField Name: stsid
BitField Type: RW
BitField Desc: STS ID
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen0_stsid_Mask                                                               cBit5_0
#define cAf6_sel_ho_bert_gen0_stsid_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert Gen1
Reg Addr   : 0x8_201 - 0xB_A01
Reg Formula: 0x8_201 + 2048*engid_g1
    Where  : 
           + $engid_g1(0-7): id 9-16 of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_gen1_Base                                                                   0x8201

/*--------------------------------------
BitField Name: gen_en
BitField Type: RW
BitField Desc: set "1" to enable bert gen
BitField Bits: [9]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen1_gen_en_Mask                                                                cBit9
#define cAf6_sel_ho_bert_gen1_gen_en_Shift                                                                   9

/*--------------------------------------
BitField Name: line_id
BitField Type: RW
BitField Desc: line id OC48
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen1_line_id_Mask                                                             cBit8_6
#define cAf6_sel_ho_bert_gen1_line_id_Shift                                                                  6

/*--------------------------------------
BitField Name: stsid
BitField Type: RW
BitField Desc: STS ID
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen1_stsid_Mask                                                               cBit5_0
#define cAf6_sel_ho_bert_gen1_stsid_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert Gen2
Reg Addr   : 0x8_202 - 0xB_A02
Reg Formula: 0x8_202 + 2048*engid_g2
    Where  : 
           + $engid_g2(0-7): id 17-24 of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_gen2_Base                                                                   0x8202

/*--------------------------------------
BitField Name: gen_en
BitField Type: RW
BitField Desc: set "1" to enable bert gen
BitField Bits: [9]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen2_gen_en_Mask                                                                cBit9
#define cAf6_sel_ho_bert_gen2_gen_en_Shift                                                                   9

/*--------------------------------------
BitField Name: line_id
BitField Type: RW
BitField Desc: line id OC48
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen2_line_id_Mask                                                             cBit8_6
#define cAf6_sel_ho_bert_gen2_line_id_Shift                                                                  6

/*--------------------------------------
BitField Name: stsid
BitField Type: RW
BitField Desc: STS ID
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen2_stsid_Mask                                                               cBit5_0
#define cAf6_sel_ho_bert_gen2_stsid_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert Gen3
Reg Addr   : 0x8_203 - 0xB_A03
Reg Formula: 0x8_203 + 2048*engid_g3
    Where  : 
           + $engid_g3(0-7): id 25-32 of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_gen3_Base                                                                   0x8203

/*--------------------------------------
BitField Name: gen_en
BitField Type: RW
BitField Desc: set "1" to enable bert gen
BitField Bits: [9]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen3_gen_en_Mask                                                                cBit9
#define cAf6_sel_ho_bert_gen3_gen_en_Shift                                                                   9

/*--------------------------------------
BitField Name: line_id
BitField Type: RW
BitField Desc: line id OC48
BitField Bits: [8:6]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen3_line_id_Mask                                                             cBit8_6
#define cAf6_sel_ho_bert_gen3_line_id_Shift                                                                  6

/*--------------------------------------
BitField Name: stsid
BitField Type: RW
BitField Desc: STS ID
BitField Bits: [5:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_gen3_stsid_Mask                                                               cBit5_0
#define cAf6_sel_ho_bert_gen3_stsid_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Sel Mode Bert Gen
Reg Addr   : 0x8_300 - 0xB_B1F
Reg Formula: 0x8_300 + 2048*slcid + engid
    Where  : 
           + $slcid(0-7): slice id of HO_BERT
           + $engid(0-31) :id of BERT
Reg Desc   : 
The registers select mode bert gen

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_pen_gen_Base                                                                       0x8300

/*--------------------------------------
BitField Name: swapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [09]
--------------------------------------*/
#define cAf6_ctrl_pen_gen_swapmode_Mask                                                                  cBit9
#define cAf6_ctrl_pen_gen_swapmode_Shift                                                                     9

/*--------------------------------------
BitField Name: invmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [08]
--------------------------------------*/
#define cAf6_ctrl_pen_gen_invmode_Mask                                                                   cBit8
#define cAf6_ctrl_pen_gen_invmode_Shift                                                                      8

/*--------------------------------------
BitField Name: patt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x01 : all0 # 0x02 : prbs15 # 0x04 : prbs20r #
0x08 : prbs20 # 0x10 : prbs23 # 0x20 : prbs31 # other: all1
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ctrl_pen_gen_patt_mode_Mask                                                               cBit7_0
#define cAf6_ctrl_pen_gen_patt_mode_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Inser Error
Reg Addr   : 0x8_320 - 0xB_B3F
Reg Formula: 0x8_320 + 2048*slcid + engid
    Where  : 
           + $slcid(0-7): id of HO_BERT
           + $engid(0-31) :id of BERT
Reg Desc   : 
The registers select rate inser error

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_ber_pen_Base                                                                       0x8320

/*--------------------------------------
BitField Name: ber_rate
BitField Type: RW
BitField Desc: TxBerMd [31:0] == BER_level_val  : Bit Error Rate inserted to
Pattern Generator [31:0] == 32'd0          :disable  BER_level_val BER_level
100:            BER 10e-2 1000:               BER 10e-3 10_000:       BER 10e-4
100_000:      BER 10e-5 1_000_000:    BER 10e-6 10_000_000:   BER 10e-7
100_000_000:  BER 10e-8 1_000_000_000 : BER 10e-9
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ctrl_ber_pen_ber_rate_Mask                                                               cBit31_0
#define cAf6_ctrl_ber_pen_ber_rate_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Inser Error
Reg Addr   : 0x8_3C0
Reg Formula: 
    Where  : 
Reg Desc   : 
The registers select rate inser error

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_ber_pen_single_Base                                                                0x83C0

/*--------------------------------------
BitField Name: ID_force
BitField Type: RW
BitField Desc: ID of BERT   :select id for force error
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_ctrl_ber_pen_single_ID_force_Mask                                                         cBit4_0
#define cAf6_ctrl_ber_pen_single_ID_force_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter num of bit gen
Reg Addr   : 0x8_380 - 0xB_B80
Reg Formula: 0x8_380 + 2048*engid
    Where  : 
           + $engid(0-7): id of HO_BERT
Reg Desc   : 
The registers counter bit genertaie in tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_ber_pen_Base                                                                    0x8380

/*--------------------------------------
BitField Name: goodbit
BitField Type: R2C
BitField Desc: counter goodbit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_ber_pen_goodbit_Mask                                                             cBit31_0
#define cAf6_goodbit_ber_pen_goodbit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert TDM Mon0
Reg Addr   : 0x8_400 - 0xB_C00
Reg Formula: 0x8_400 + 2048*engid_g0
    Where  : 
           + $engid_g0(0-7): id1-id8 of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_tdm_mon0_Base                                                               0x8400

/*--------------------------------------
BitField Name: montdmen
BitField Type: RW
BitField Desc: set "1" to enable bert tdm mon
BitField Bits: [13]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon0_montdmen_Mask                                                         cBit13
#define cAf6_sel_ho_bert_tdm_mon0_montdmen_Shift                                                            13

/*--------------------------------------
BitField Name: tdmlineid
BitField Type: RW
BitField Desc: OC48 line
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon0_tdmlineid_Mask                                                     cBit12_10
#define cAf6_sel_ho_bert_tdm_mon0_tdmlineid_Shift                                                           10

/*--------------------------------------
BitField Name: masterID
BitField Type: RW
BitField Desc: chanel ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon0_masterID_Mask                                                        cBit9_0
#define cAf6_sel_ho_bert_tdm_mon0_masterID_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert TDM Mon1
Reg Addr   : 0x8_401 - 0xB_C01
Reg Formula: 0x8_401 + 2048*engid_g1
    Where  : 
           + $engid_g1(0-7): id9-id16  of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_tdm_mon1_Base                                                               0x8401

/*--------------------------------------
BitField Name: montdmen
BitField Type: RW
BitField Desc: set "1" to enable bert tdm mon
BitField Bits: [13]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon1_montdmen_Mask                                                         cBit13
#define cAf6_sel_ho_bert_tdm_mon1_montdmen_Shift                                                            13

/*--------------------------------------
BitField Name: tdmlineid
BitField Type: RW
BitField Desc: OC48 line
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon1_tdmlineid_Mask                                                     cBit12_10
#define cAf6_sel_ho_bert_tdm_mon1_tdmlineid_Shift                                                           10

/*--------------------------------------
BitField Name: masterID
BitField Type: RW
BitField Desc: chanel ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon1_masterID_Mask                                                        cBit9_0
#define cAf6_sel_ho_bert_tdm_mon1_masterID_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert TDM Mon2
Reg Addr   : 0x8_402 - 0xB_C02
Reg Formula: 0x8_402 + 2048*engid_g2
    Where  : 
           + $engid_g2(0-7): id17-id24 of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_tdm_mon2_Base                                                               0x8402

/*--------------------------------------
BitField Name: montdmen
BitField Type: RW
BitField Desc: set "1" to enable bert tdm mon
BitField Bits: [13]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon2_montdmen_Mask                                                         cBit13
#define cAf6_sel_ho_bert_tdm_mon2_montdmen_Shift                                                            13

/*--------------------------------------
BitField Name: tdmlineid
BitField Type: RW
BitField Desc: OC48 line
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon2_tdmlineid_Mask                                                     cBit12_10
#define cAf6_sel_ho_bert_tdm_mon2_tdmlineid_Shift                                                           10

/*--------------------------------------
BitField Name: masterID
BitField Type: RW
BitField Desc: chanel ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon2_masterID_Mask                                                        cBit9_0
#define cAf6_sel_ho_bert_tdm_mon2_masterID_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert TDM Mon3
Reg Addr   : 0x8_403 - 0xB_C03
Reg Formula: 0x8_403 + 2048*engid_g3
    Where  : 
           + $engid_g3(0-7): id25-id32 of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_tdm_mon3_Base                                                               0x8403

/*--------------------------------------
BitField Name: montdmen
BitField Type: RW
BitField Desc: set "1" to enable bert tdm mon
BitField Bits: [13]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon3_montdmen_Mask                                                         cBit13
#define cAf6_sel_ho_bert_tdm_mon3_montdmen_Shift                                                            13

/*--------------------------------------
BitField Name: tdmlineid
BitField Type: RW
BitField Desc: OC48 line
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon3_tdmlineid_Mask                                                     cBit12_10
#define cAf6_sel_ho_bert_tdm_mon3_tdmlineid_Shift                                                           10

/*--------------------------------------
BitField Name: masterID
BitField Type: RW
BitField Desc: chanel ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_tdm_mon3_masterID_Mask                                                        cBit9_0
#define cAf6_sel_ho_bert_tdm_mon3_masterID_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert PW Mon0
Reg Addr   : 0x8_600 - 0xB_E00
Reg Formula: 0x8_600 + 2048*engid_g0
    Where  : 
           + $engid_g0(0-7): id1-id8 of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_pw_mon_g0_Base                                                              0x8600

/*--------------------------------------
BitField Name: monpwen
BitField Type: RW
BitField Desc: set "1" to enable bert pw mon
BitField Bits: [13]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_g0_monpwen_Mask                                                         cBit13
#define cAf6_sel_ho_bert_pw_mon_g0_monpwen_Shift                                                            13

/*--------------------------------------
BitField Name: pwlineid
BitField Type: RW
BitField Desc: OC48 line
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_g0_pwlineid_Mask                                                     cBit12_10
#define cAf6_sel_ho_bert_pw_mon_g0_pwlineid_Shift                                                           10

/*--------------------------------------
BitField Name: masterID
BitField Type: RW
BitField Desc: chanel ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_g0_masterID_Mask                                                       cBit9_0
#define cAf6_sel_ho_bert_pw_mon_g0_masterID_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert PW Mon1
Reg Addr   : 0x8_601 - 0xB_E01
Reg Formula: 0x8_601 + 2048*engid_g1
    Where  : 
           + $engid_g1(0-7): id9-id16 of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_pw_mon_g1_Base                                                              0x8601

/*--------------------------------------
BitField Name: monpwen
BitField Type: RW
BitField Desc: set "1" to enable bert pw mon
BitField Bits: [13]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_g1_monpwen_Mask                                                         cBit13
#define cAf6_sel_ho_bert_pw_mon_g1_monpwen_Shift                                                            13

/*--------------------------------------
BitField Name: pwlineid
BitField Type: RW
BitField Desc: OC48 line
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_g1_pwlineid_Mask                                                     cBit12_10
#define cAf6_sel_ho_bert_pw_mon_g1_pwlineid_Shift                                                           10

/*--------------------------------------
BitField Name: masterID
BitField Type: RW
BitField Desc: chanel ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_g1_masterID_Mask                                                       cBit9_0
#define cAf6_sel_ho_bert_pw_mon_g1_masterID_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert PW Mon2
Reg Addr   : 0x8_602 - 0xB_E02
Reg Formula: 0x8_602 + 2048*engid_g2
    Where  : 
           + $engid_g2(0-7): id17-id24 of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_pw_mon_g2_Base                                                              0x8602

/*--------------------------------------
BitField Name: monpwen
BitField Type: RW
BitField Desc: set "1" to enable bert pw mon
BitField Bits: [13]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_g2_monpwen_Mask                                                         cBit13
#define cAf6_sel_ho_bert_pw_mon_g2_monpwen_Shift                                                            13

/*--------------------------------------
BitField Name: pwlineid
BitField Type: RW
BitField Desc: OC48 line
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_g2_pwlineid_Mask                                                     cBit12_10
#define cAf6_sel_ho_bert_pw_mon_g2_pwlineid_Shift                                                           10

/*--------------------------------------
BitField Name: masterID
BitField Type: RW
BitField Desc: chanel ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_g2_masterID_Mask                                                       cBit9_0
#define cAf6_sel_ho_bert_pw_mon_g2_masterID_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Sel Ho Bert PW Mon3
Reg Addr   : 0x8_603 - 0xB_E03
Reg Formula: 0x8_603 + 2048*engid_g3
    Where  : 
           + $engid_g3(0-7): id25-id32 of HO_BERT
Reg Desc   : 
The registers select 1id in line to gen bert data

------------------------------------------------------------------------------*/
#define cAf6Reg_sel_ho_bert_pw_mon_g3_Base                                                              0x8603

/*--------------------------------------
BitField Name: monpwen
BitField Type: RW
BitField Desc: set "1" to enable bert pw mon
BitField Bits: [13]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_g3_monpwen_Mask                                                         cBit13
#define cAf6_sel_ho_bert_pw_mon_g3_monpwen_Shift                                                            13

/*--------------------------------------
BitField Name: pwlineid
BitField Type: RW
BitField Desc: OC48 line
BitField Bits: [12:10]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_g3_pwlineid_Mask                                                     cBit12_10
#define cAf6_sel_ho_bert_pw_mon_g3_pwlineid_Shift                                                           10

/*--------------------------------------
BitField Name: masterID
BitField Type: RW
BitField Desc: chanel ID
BitField Bits: [9:0]
--------------------------------------*/
#define cAf6_sel_ho_bert_pw_mon_g3_masterID_Mask                                                       cBit9_0
#define cAf6_sel_ho_bert_pw_mon_g3_masterID_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Sel Mode Bert TDM mon
Reg Addr   : 0x8_520 - 0xB_D3F
Reg Formula: 0x8_520 + 2048*slcid + engid
    Where  : 
           + $slcid(0-7) slice id
           + $engid(0-31): id of HO_BERT
Reg Desc   : 
The registers select mode bert mon in tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_pen_tdm_mon_Base                                                                   0x8520

/*--------------------------------------
BitField Name: thrhold_pattern_sync
BitField Type: RW
BitField Desc: minimum number of byte sync to go sync state
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_Mask                                              cBit19_16
#define cAf6_ctrl_pen_tdm_mon_thrhold_pattern_sync_Shift                                                    16

/*--------------------------------------
BitField Name: thrhold_error
BitField Type: RW
BitField Desc: maximum err in state sync, if more than thrhold go loss sync
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_Mask                                                     cBit15_12
#define cAf6_ctrl_pen_tdm_mon_thrhold_error_Shift                                                           12

/*--------------------------------------
BitField Name: reservee
BitField Type: RW
BitField Desc: reserve
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_reservee_Mask                                                          cBit11_10
#define cAf6_ctrl_pen_tdm_mon_reservee_Shift                                                                10

/*--------------------------------------
BitField Name: swapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [09]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_swapmode_Mask                                                              cBit9
#define cAf6_ctrl_pen_tdm_mon_swapmode_Shift                                                                 9

/*--------------------------------------
BitField Name: invmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [08]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_invmode_Mask                                                               cBit8
#define cAf6_ctrl_pen_tdm_mon_invmode_Shift                                                                  8

/*--------------------------------------
BitField Name: patt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x01 : all1 # 0x02 : all0 # 0x04 : prbs15 #
0x08 : prbs20r # 0x10 : prbs20 # 0x20 : prss23 # 0x40 : prbs31
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ctrl_pen_tdm_mon_patt_mode_Mask                                                           cBit7_0
#define cAf6_ctrl_pen_tdm_mon_patt_mode_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Sel Mode Bert PW mon
Reg Addr   : 0x8_720 - 0xB_F3f
Reg Formula: 0x8_720 + 2048*slcid + engid
    Where  : 
           + $slcid(0-7) slice id
           + $engid(0-31): id of HO_BERT
Reg Desc   : 
The registers select mode bert mon in pw side

------------------------------------------------------------------------------*/
#define cAf6Reg_ctrl_pen_pw_mon_Base                                                                    0x8720

/*--------------------------------------
BitField Name: thrhold_pattern_sync
BitField Type: RW
BitField Desc: minimum number of byte sync to go sync state
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_Mask                                               cBit19_16
#define cAf6_ctrl_pen_pw_mon_thrhold_pattern_sync_Shift                                                     16

/*--------------------------------------
BitField Name: thrhold_error
BitField Type: RW
BitField Desc: maximum err in state sync, if more than thrhold go loss sync
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_thrhold_error_Mask                                                      cBit15_12
#define cAf6_ctrl_pen_pw_mon_thrhold_error_Shift                                                            12

/*--------------------------------------
BitField Name: pw_reserve
BitField Type: RW
BitField Desc: pw reserve
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_pw_reserve_Mask                                                         cBit11_10
#define cAf6_ctrl_pen_pw_mon_pw_reserve_Shift                                                               10

/*--------------------------------------
BitField Name: swapmode
BitField Type: RW
BitField Desc: swap data
BitField Bits: [09]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_swapmode_Mask                                                               cBit9
#define cAf6_ctrl_pen_pw_mon_swapmode_Shift                                                                  9

/*--------------------------------------
BitField Name: invmode
BitField Type: RW
BitField Desc: invert data
BitField Bits: [08]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_invmode_Mask                                                                cBit8
#define cAf6_ctrl_pen_pw_mon_invmode_Shift                                                                   8

/*--------------------------------------
BitField Name: patt_mode
BitField Type: RW
BitField Desc: sel pattern gen # 0x01 : all1 # 0x02 : all0 # 0x04 : prbs15 #
0x08 : prbs20r # 0x10 : prbs20 # 0x20 : prss23 # 0x40 : prbs31
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ctrl_pen_pw_mon_patt_mode_Mask                                                            cBit7_0
#define cAf6_ctrl_pen_pw_mon_patt_mode_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : TDM loss sync
Reg Addr   : 0x8_51F - 0xB_D1F
Reg Formula: 0x8_51F + 2048*slc
    Where  : 
           + $slc(0-7): slice id of HO_BERT
Reg Desc   : 
The registers indicate bert mon loss sync in tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_loss_tdm_mon_Base                                                                       0x851F

/*--------------------------------------
BitField Name: sticky_err
BitField Type: WC
BitField Desc: "1" indicate loss sync
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_loss_tdm_mon_sticky_err_Mask                                                             cBit31_0
#define cAf6_loss_tdm_mon_sticky_err_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : TDM mon status
Reg Addr   : 0x8_500 - 0xB_D1B
Reg Formula: 0x8_500 + 2048*slcid + engid
    Where  : 
           + $slcid(0-7) slice id
           + $engid(0-27): id of HO_BERT
Reg Desc   : 
The registers indicate status of bert mon  in tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_stt_tdm_mon_Base                                                                        0x8500

/*--------------------------------------
BitField Name: tdmstate
BitField Type: RO
BitField Desc: Status [1:0]: Prbs status LOPSTA = 2'd0; SRCSTA = 2'd1; VERSTA =
2'd2; INFSTA = 2'd3;
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_stt_tdm_mon_tdmstate_Mask                                                                 cBit1_0
#define cAf6_stt_tdm_mon_tdmstate_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : PW loss sync
Reg Addr   : 0x8_71F - 0xB_F1F
Reg Formula: 0x8_71F + 2048*engid
    Where  : 
           + $engid(0-7): id of HO_BERT
Reg Desc   : 
The registers indicate bert mon loss sync in pw side

------------------------------------------------------------------------------*/
#define cAf6Reg_loss_pw_mon_Base                                                                        0x871F

/*--------------------------------------
BitField Name: sticky_err
BitField Type: WC
BitField Desc: "1" indicate loss sync
BitField Bits: [0:0]
--------------------------------------*/
#define cAf6_loss_pw_mon_sticky_err_Mask                                                                 cBit0
#define cAf6_loss_pw_mon_sticky_err_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : PW mon stt
Reg Addr   : 0x8_700 - 0xB_F1B
Reg Formula: 0x8_700 + 2048*engid
    Where  : 
           + $engid(0-7): id of HO_BERT
Reg Desc   : 
The registers indicate bert mon state in pw side

------------------------------------------------------------------------------*/
#define cAf6Reg_stt_pw_mon_Base                                                                         0x8700

/*--------------------------------------
BitField Name: pwstate
BitField Type: RO
BitField Desc: Status [1:0]: Prbs status LOPSTA = 2'd0; SRCSTA = 2'd1; VERSTA =
2'd2; INFSTA = 2'd3;
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_stt_pw_mon_pwstate_Mask                                                                   cBit1_0
#define cAf6_stt_pw_mon_pwstate_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : counter good bit TDM mon
Reg Addr   : 0x8_580 - 0xB_D9F
Reg Formula: 0x8_580 + 2048*slcid + engid
    Where  : 
           + $slcid(0-7) slice id
           + $engid(0-27): id of HO_BERT
Reg Desc   : 
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_tdm_mon_Base                                                                0x8580

/*--------------------------------------
BitField Name: cnt_goodbit
BitField Type: R2C
BitField Desc: counter goodbit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_Mask                                                     cBit31_0
#define cAf6_goodbit_pen_tdm_mon_cnt_goodbit_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : counter error bit in TDM mon
Reg Addr   : 0x8_540 - 0xB_D5F
Reg Formula: 0x8_540 + 2048*slcid + engid
    Where  : 
           + $slcid(0-7) slice id
           + $engid(0-27): id of HO_BERT
Reg Desc   : 
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_err_pen_tdm_mon_Base                                                                    0x8540

/*--------------------------------------
BitField Name: cnt_errbit
BitField Type: R2C
BitField Desc: counter err bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_err_pen_tdm_mon_cnt_errbit_Mask                                                          cBit31_0
#define cAf6_err_pen_tdm_mon_cnt_errbit_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Counter loss bit in TDM mon
Reg Addr   : 0x8_5C0 - 0xB_DDf
Reg Formula: 0x8_5C0 + 2048*slcid + engid
    Where  : 
           + $slcid(0-7) slice id
           + $engid(0-27): id of HO_BERT
Reg Desc   : 
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_lossbit_pen_tdm_mon_Base                                                                0x85C0

/*--------------------------------------
BitField Name: cnt_errbit
BitField Type: R2C
BitField Desc: counter err bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_Mask                                                      cBit31_0
#define cAf6_lossbit_pen_tdm_mon_cnt_errbit_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : counter good bit TDM mon
Reg Addr   : 0x8_780 - 0xB_F9F
Reg Formula: 0x8_780 + 2048*slcid + engid
    Where  : 
           + $slcid(0-7) slice id
           + $engid(0-27): id of HO_BERT
Reg Desc   : 
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_goodbit_pen_pw_mon_Base                                                                 0x8780

/*--------------------------------------
BitField Name: cnt_pwgoodbit
BitField Type: R2C
BitField Desc: counter goodbit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_Mask                                                    cBit31_0
#define cAf6_goodbit_pen_pw_mon_cnt_pwgoodbit_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : counter error bit in TDM mon
Reg Addr   : 0x8_740 - 0xB_F5F
Reg Formula: 0x8_740 + 2048*slcid + engid
    Where  : 
           + $slcid(0-7) slice id
           + $engid(0-27): id of HO_BERT
Reg Desc   : 
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_errbit_pen_pw_mon_Base                                                                  0x8740

/*--------------------------------------
BitField Name: cnt_pwerrbit
BitField Type: R2C
BitField Desc: counter err bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_Mask                                                      cBit31_0
#define cAf6_errbit_pen_pw_mon_cnt_pwerrbit_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter loss bit in TDM mon
Reg Addr   : 0x8_7C0 - 0xB_FDF
Reg Formula: 0x8_7C0 + 2048*slcid + engid
    Where  : 
           + $slcid(0-7) slice id
           + $engid(0-27): id of HO_BERT
Reg Desc   : 
The registers count goodbit in  mon tdm side

------------------------------------------------------------------------------*/
#define cAf6Reg_lossbit_pen_pw_mon_Base                                                                 0x87C0

/*--------------------------------------
BitField Name: cnt_pwlossbi
BitField Type: R2C
BitField Desc: counter loss bit
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_Mask                                                     cBit31_0
#define cAf6_lossbit_pen_pw_mon_cnt_pwlossbi_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : RAM Map Parity Force Control
Reg Addr   : 0x4808
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Map_Parity_Force_Control_Base                                                       0x4808

/*--------------------------------------
BitField Name: MAPSlc7Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice7"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_Mask                                     cBit7
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_Shift                                        7

/*--------------------------------------
BitField Name: MAPSlc6Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice6"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_Mask                                     cBit6
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_Shift                                        6

/*--------------------------------------
BitField Name: MAPSlc5Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice5"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_Mask                                     cBit5
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_Shift                                        5

/*--------------------------------------
BitField Name: MAPSlc4Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice4"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_Mask                                     cBit4
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_Shift                                        4

/*--------------------------------------
BitField Name: MAPSlc3Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice3"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_Mask                                     cBit3
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_Shift                                        3

/*--------------------------------------
BitField Name: MAPSlc2Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice2"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_Mask                                     cBit2
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_Shift                                        2

/*--------------------------------------
BitField Name: MAPSlc1Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice1"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_Mask                                     cBit1
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_Shift                                        1

/*--------------------------------------
BitField Name: MAPSlc0Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice0"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_Mask                                     cBit0
#define cAf6_RAM_Map_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : RAM Map Parity Disable Control
Reg Addr   : 0x4809
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Map_Parity_Disable_Control_Base                                                     0x4809

/*--------------------------------------
BitField Name: MAPSlc7Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice7"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_Mask                                   cBit7
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_Shift                                       7

/*--------------------------------------
BitField Name: MAPSlc6Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice6"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_Mask                                   cBit6
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_Shift                                       6

/*--------------------------------------
BitField Name: MAPSlc5Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice5"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_Mask                                   cBit5
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_Shift                                       5

/*--------------------------------------
BitField Name: MAPSlc4Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice4"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_Mask                                   cBit4
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_Shift                                       4

/*--------------------------------------
BitField Name: MAPSlc3Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice3"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_Mask                                   cBit3
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_Shift                                       3

/*--------------------------------------
BitField Name: MAPSlc2Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice2"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_Mask                                   cBit2
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_Shift                                       2

/*--------------------------------------
BitField Name: MAPSlc1Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice1"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_Mask                                   cBit1
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_Shift                                       1

/*--------------------------------------
BitField Name: MAPSlc0Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice0"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_Mask                                   cBit0
#define cAf6_RAM_Map_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RAM Map parity Error Sticky
Reg Addr   : 0x480a
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_Map_Parity_Error_Sticky_Base                                                        0x480a

/*--------------------------------------
BitField Name: MAPSlc7Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice7"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_Mask                                      cBit7
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_Shift                                         7

/*--------------------------------------
BitField Name: MAPSlc6Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice6"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_Mask                                      cBit6
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_Shift                                         6

/*--------------------------------------
BitField Name: MAPSlc5Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice5"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_Mask                                      cBit5
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_Shift                                         5

/*--------------------------------------
BitField Name: MAPSlc4Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice4"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_Mask                                      cBit4
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_Shift                                         4

/*--------------------------------------
BitField Name: MAPSlc3Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice3"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_Mask                                      cBit3
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_Shift                                         3

/*--------------------------------------
BitField Name: MAPSlc2Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice2"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_Mask                                      cBit2
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_Shift                                         2

/*--------------------------------------
BitField Name: MAPSlc1Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice1"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_Mask                                      cBit1
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_Shift                                         1

/*--------------------------------------
BitField Name: MAPSlc0Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice0"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_Mask                                      cBit0
#define cAf6_RAM_Map_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : RAM DeMap Parity Force Control
Reg Addr   : 0x0808
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_DeMap_Parity_Force_Control_Base                                                     0x0808

/*--------------------------------------
BitField Name: MAPSlc7Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice7"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_Mask                                   cBit7
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc7Ctrl_ParErrFrc_Shift                                       7

/*--------------------------------------
BitField Name: MAPSlc6Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice6"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_Mask                                   cBit6
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc6Ctrl_ParErrFrc_Shift                                       6

/*--------------------------------------
BitField Name: MAPSlc5Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice5"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_Mask                                   cBit5
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc5Ctrl_ParErrFrc_Shift                                       5

/*--------------------------------------
BitField Name: MAPSlc4Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice4"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_Mask                                   cBit4
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc4Ctrl_ParErrFrc_Shift                                       4

/*--------------------------------------
BitField Name: MAPSlc3Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice3"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_Mask                                   cBit3
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc3Ctrl_ParErrFrc_Shift                                       3

/*--------------------------------------
BitField Name: MAPSlc2Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice2"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_Mask                                   cBit2
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc2Ctrl_ParErrFrc_Shift                                       2

/*--------------------------------------
BitField Name: MAPSlc1Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice1"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_Mask                                   cBit1
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc1Ctrl_ParErrFrc_Shift                                       1

/*--------------------------------------
BitField Name: MAPSlc0Ctrl_ParErrFrc
BitField Type: RW
BitField Desc: Force parity For RAM Control "Thalassa Map Line Control Slice0"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_Mask                                   cBit0
#define cAf6_RAM_DeMap_Parity_Force_Control_MAPSlc0Ctrl_ParErrFrc_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RAM DeMap Parity Disable Control
Reg Addr   : 0x0809
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures force parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_DeMap_Parity_Disable_Control_Base                                                   0x0809

/*--------------------------------------
BitField Name: MAPSlc7Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice7"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_Mask                                   cBit7
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc7Ctrl_ParErrDis_Shift                                       7

/*--------------------------------------
BitField Name: MAPSlc6Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice6"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_Mask                                   cBit6
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc6Ctrl_ParErrDis_Shift                                       6

/*--------------------------------------
BitField Name: MAPSlc5Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice5"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_Mask                                   cBit5
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc5Ctrl_ParErrDis_Shift                                       5

/*--------------------------------------
BitField Name: MAPSlc4Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice4"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_Mask                                   cBit4
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc4Ctrl_ParErrDis_Shift                                       4

/*--------------------------------------
BitField Name: MAPSlc3Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice3"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_Mask                                   cBit3
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc3Ctrl_ParErrDis_Shift                                       3

/*--------------------------------------
BitField Name: MAPSlc2Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice2"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_Mask                                   cBit2
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc2Ctrl_ParErrDis_Shift                                       2

/*--------------------------------------
BitField Name: MAPSlc1Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice1"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_Mask                                   cBit1
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc1Ctrl_ParErrDis_Shift                                       1

/*--------------------------------------
BitField Name: MAPSlc0Ctrl_ParErrDis
BitField Type: RW
BitField Desc: Disable parity For RAM Control "Thalassa Map Line Control Slice0"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_Mask                                   cBit0
#define cAf6_RAM_DeMap_Parity_Disable_Control_MAPSlc0Ctrl_ParErrDis_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : RAM DeMap parity Error Sticky
Reg Addr   : 0x080a
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures disable parity for internal RAM

------------------------------------------------------------------------------*/
#define cAf6Reg_RAM_DeMap_Parity_Error_Sticky_Base                                                      0x080a

/*--------------------------------------
BitField Name: MAPSlc7Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice7"
BitField Bits: [7]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_Mask                                    cBit7
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc7Ctrl_ParErrStk_Shift                                       7

/*--------------------------------------
BitField Name: MAPSlc6Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice6"
BitField Bits: [6]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_Mask                                    cBit6
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc6Ctrl_ParErrStk_Shift                                       6

/*--------------------------------------
BitField Name: MAPSlc5Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice5"
BitField Bits: [5]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_Mask                                    cBit5
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc5Ctrl_ParErrStk_Shift                                       5

/*--------------------------------------
BitField Name: MAPSlc4Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice4"
BitField Bits: [4]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_Mask                                    cBit4
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc4Ctrl_ParErrStk_Shift                                       4

/*--------------------------------------
BitField Name: MAPSlc3Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice3"
BitField Bits: [3]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_Mask                                    cBit3
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc3Ctrl_ParErrStk_Shift                                       3

/*--------------------------------------
BitField Name: MAPSlc2Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice2"
BitField Bits: [2]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_Mask                                    cBit2
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc2Ctrl_ParErrStk_Shift                                       2

/*--------------------------------------
BitField Name: MAPSlc1Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice1"
BitField Bits: [1]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_Mask                                    cBit1
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc1Ctrl_ParErrStk_Shift                                       1

/*--------------------------------------
BitField Name: MAPSlc0Ctrl_ParErrStk
BitField Type: RW
BitField Desc: Error parity For RAM Control "Thalassa Map Line Control Slice0"
BitField Bits: [0]
--------------------------------------*/
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_Mask                                    cBit0
#define cAf6_RAM_DeMap_Parity_Error_Sticky_MAPSlc0Ctrl_ParErrStk_Shift                                       0

#endif /* _AF6_REG_AF6CNC0021_RD_MAP_HO_H_ */

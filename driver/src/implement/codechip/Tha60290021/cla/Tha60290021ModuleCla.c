/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290021ModuleCla.c
 *
 * Created Date: Aug 11, 2016
 *
 * Description : CLA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/cla/Tha60210051ModuleClaInternal.h"
#include "controller/Tha60290021ClaPwController.h"
#include "controller/Tha60290021ClaPwEthPortController.h"
#include "Tha60290021ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290021ModuleCla*)(self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleClaMethods         m_ThaModuleClaOverride;
static tTha60210011ModuleClaMethods m_Tha60210011ModuleClaOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaClaEthPortController EthPortControllerCreate(ThaModuleCla self)
    {
    return Tha60290021ClaPwEthPortControllerNew(self);
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return Tha60290021ClaPwControllerNew(self);
    }

static uint32 StartHwVersionControlSuppression(Tha60210011ModuleCla self)
    {
    /* This is supposed to be supported from the beginning */
    AtUnused(self);
    return 0;
    }

static eBool EthPortDebugShouldDisplay(Tha60210011ModuleCla self, uint32 portId)
    {
    AtUnused(self);
    return (portId == 0) ? cAtTrue : cAtFalse;
    }

static eBool HasPwGroup(Tha60210011ModuleCla self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideTha60210011ModuleCla(AtModule self)
    {
    Tha60210011ModuleCla claModule = (Tha60210011ModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleClaOverride, mMethodsGet(claModule), sizeof(m_Tha60210011ModuleClaOverride));

        mMethodOverride(m_Tha60210011ModuleClaOverride, StartHwVersionControlSuppression);
        mMethodOverride(m_Tha60210011ModuleClaOverride, EthPortDebugShouldDisplay);
        mMethodOverride(m_Tha60210011ModuleClaOverride, HasPwGroup);
        }

    mMethodsSet(claModule, &m_Tha60210011ModuleClaOverride);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, EthPortControllerCreate);
        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCla(self);
    OverrideTha60210011ModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModuleCla);
    }

AtModule Tha60290021ModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290021ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModuleClaObjectInit(newModule, device);
    }

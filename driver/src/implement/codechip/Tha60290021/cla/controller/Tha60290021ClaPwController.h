/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290021ClaPwController.h
 * 
 * Created Date: Nov 2, 2016
 *
 * Description : PW CLA controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021CLAPWCONTROLLER_H_
#define _THA60290021CLAPWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210011/cla/controllers/Tha60210011ClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaPwController Tha60290021ClaPwControllerNew(ThaModuleCla cla);
Tha60210011ClaPwCounterGetter Tha60290021ClaPwPmcCounterGetterNew(void);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021CLAPWCONTROLLER_H_ */


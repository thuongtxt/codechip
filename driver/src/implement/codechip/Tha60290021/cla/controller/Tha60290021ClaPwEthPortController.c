/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290021ClaEthPortController.c
 *
 * Created Date: Aug 11, 2016
 *
 * Description : CLA ETH port controller concrete code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60210011/cla/Tha60210011ModuleCla.h"
#include "../../../Tha60210051/cla/controllers/Tha60210051ClaPwEthPortControllerInternal.h"
#include "../../eth/Tha60290021PwEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_Eth_cnt_discard_ro_Base                             0x00000007

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021ClaPwEthPortController
    {
    tTha60210051ClaPwEthPortController super;
    }tTha60290021ClaPwEthPortController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaEthPortControllerMethods m_ThaClaEthPortControllerOverride;

/* Save super implementation */
static const tThaClaEthPortControllerMethods *m_ThaClaEthPortControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    tAtEthPortSimulationDb *simDb = AtChannelSimulationDatabaseGet((AtChannel)port);

    if (simDb)
        return simDb->maxPacketSize;

    return m_ThaClaEthPortControllerMethods->MaxPacketSizeGet(self, port);
    }

static eAtRet MaxPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 maxPacketSize)
    {
    tAtEthPortSimulationDb *simDb = AtChannelSimulationDatabaseGet((AtChannel)port);

    if (simDb)
        {
        simDb->maxPacketSize = maxPacketSize;
        return cAtOk;
        }

    return m_ThaClaEthPortControllerMethods->MaxPacketSizeSet(self, port, maxPacketSize);
    }

static eAtRet MinPacketSizeSet(ThaClaEthPortController self, AtEthPort port, uint32 minPacketSize)
    {
    tAtEthPortSimulationDb *simDb = AtChannelSimulationDatabaseGet((AtChannel)port);

    if (simDb)
        {
        simDb->minPacketSize = minPacketSize;
        return cAtOk;
        }

    return m_ThaClaEthPortControllerMethods->MinPacketSizeSet(self, port, minPacketSize);
    }

static uint32 MinPacketSizeGet(ThaClaEthPortController self, AtEthPort port)
    {
    tAtEthPortSimulationDb *simDb = AtChannelSimulationDatabaseGet((AtChannel)port);

    if (simDb)
        return simDb->minPacketSize;

    return m_ThaClaEthPortControllerMethods->MinPacketSizeGet(self, port);
    }

static ThaModuleCla ClaModule(ThaClaEthPortController self)
    {
    return (ThaModuleCla)ThaClaControllerModuleGet((ThaClaController)self);
    }

static uint32 EthPktDscdRead2Clear(ThaClaEthPortController self, AtEthPort port, eBool r2c)
    {
    uint32 r2cOffset = (r2c) ? 0x1 : 0x0;
    uint32 portId = AtChannelIdGet((AtChannel)port);
    uint32 offset = (portId * 2UL) + Tha60210011ModuleClaBaseAddress(ClaModule(self)) + r2cOffset;
    return mChannelHwRead(port, cAf6Reg_Eth_cnt_discard_ro_Base + offset, cThaModuleCla);
    }

static void OverrideThaClaEthPortController(ThaClaEthPortController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaEthPortControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthPortControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthPortControllerOverride));

        mMethodOverride(m_ThaClaEthPortControllerOverride, MaxPacketSizeSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MaxPacketSizeGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MinPacketSizeSet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, MinPacketSizeGet);
        mMethodOverride(m_ThaClaEthPortControllerOverride, EthPktDscdRead2Clear);
        }

    mMethodsSet(self, &m_ThaClaEthPortControllerOverride);
    }

static void Override(ThaClaEthPortController self)
    {
    OverrideThaClaEthPortController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ClaPwEthPortController);
    }

static ThaClaEthPortController ObjectInit(ThaClaEthPortController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ClaPwEthPortControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthPortController Tha60290021ClaPwEthPortControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthPortController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }

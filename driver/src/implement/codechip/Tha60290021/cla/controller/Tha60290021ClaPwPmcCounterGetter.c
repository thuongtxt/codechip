/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290021ClaPwPmcCounterGetter.c
 *
 * Created Date: Nov 2, 2016
 *
 * Description : CLA PW controller counter
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/pmc/ThaModulePmc.h"
#include "../../../Tha60290021/man/Tha60290021Device.h"
#include "../../../Tha60210011/cla/controllers/Tha60210011ClaPwControllerInternal.h"
#include "Tha60290021ClaPwController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct  tTha60290021ClaPwPmcCounterGetter * Tha60290021ClaPwPmcCounterGetter;

typedef struct tTha60290021ClaPwPmcCounterGetter
	{
	tTha60210011ClaPwCounterGetter super;
	}tTha60290021ClaPwPmcCounterGetter;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011ClaPwCounterGetterMethods m_Tha60210011ClaPwCounterGetterOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CounterReadWithHandler(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear,
                                     uint32 (*CounterGetFunc)(ThaModulePmc self, AtPw pw, eBool clear))
    {
    ThaModulePmc pmcModule = (ThaModulePmc)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)pw), cThaModulePwPmc);
    AtUnused(self);
    AtUnused(controller);
    return CounterGetFunc(pmcModule, pw, clear);
    }

static uint32 RxPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, controller, pw, clear, ThaModulePmcPwRxPacketsGet);
    }

static uint32 RxMalformedPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, controller, pw, clear, ThaModulePmcPwRxMalformedPacketsGet);
    }

static uint32 RxStrayPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, controller, pw, clear, ThaModulePmcPwRxStrayPacketsGet);
    }

static uint32 RxLbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, controller, pw, clear, ThaModulePmcPwRxLbitPacketsGet);
    }

static uint32 RxRbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, controller, pw, clear, ThaModulePmcPwRxRbitPacketsGet);
    }

static uint32 RxMbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, controller, pw, clear, ThaModulePmcPwRxMbitPacketsGet);
    }

static uint32 RxNbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, controller, pw, clear, ThaModulePmcPwRxNbitPacketsGet);
    }

static uint32 RxPbitPacketsGet(Tha60210011ClaPwCounterGetter self, ThaClaPwController controller, AtPw pw, eBool clear)
    {
    return CounterReadWithHandler(self, controller, pw, clear, ThaModulePmcPwRxPbitPacketsGet);
    }

static void OverrideTha60210011ClaPwCounterGetter(Tha60210011ClaPwCounterGetter self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ClaPwCounterGetterOverride, mMethodsGet(self), sizeof(m_Tha60210011ClaPwCounterGetterOverride));

        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxMalformedPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxStrayPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxLbitPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxRbitPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxMbitPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxNbitPacketsGet);
        mMethodOverride(m_Tha60210011ClaPwCounterGetterOverride, RxPbitPacketsGet);
        }

    mMethodsSet(self, &m_Tha60210011ClaPwCounterGetterOverride);
    }

static void Override(Tha60210011ClaPwCounterGetter self)
    {
    OverrideTha60210011ClaPwCounterGetter(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ClaPwPmcCounterGetter);
    }

static Tha60210011ClaPwCounterGetter ObjectInit(Tha60210011ClaPwCounterGetter self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ClaPwCounterGetterObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha60210011ClaPwCounterGetter Tha60290021ClaPwPmcCounterGetterNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60210011ClaPwCounterGetter newGetter = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newGetter == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newGetter);
    }

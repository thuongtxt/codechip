/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290021ClaPwController.c
 *
 * Created Date: Nov 2, 2016
 *
 * Description : CLA PW counters
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../util/coder/AtCoderUtil.h"
#include "Tha60290021ClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290021ClaPwController *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                   m_AtObjectOverride;
static tTha60210011ClaPwControllerMethods m_Tha60210011ClaPwControllerOverride;

/* Save super implementation */
static const tAtObjectMethods                   *m_AtObjectMethods                   = NULL;
static const tTha60210011ClaPwControllerMethods *m_Tha60210011ClaPwControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool UsePmcCounter(Tha60210011ClaPwController self)
	{
	AtDevice device = AtModuleDeviceGet((AtModule)ThaClaControllerModuleGet((ThaClaController)self));
	ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);

	if (ThaModulePwDebugCountersModuleGet(pwModule) == cThaModulePwPmc)
		return cAtTrue;

	return cAtFalse;
	}

static Tha60210011ClaPwCounterGetter PmcCounterGetter(Tha60210011ClaPwController self)
    {
    if (mThis(self)->pmcCounterGetter == NULL)
        mThis(self)->pmcCounterGetter = Tha60290021ClaPwPmcCounterGetterNew();
    return mThis(self)->pmcCounterGetter;
    }

static Tha60210011ClaPwCounterGetter CounterGetter(Tha60210011ClaPwController self)
	{
	if (UsePmcCounter(self))
		return PmcCounterGetter(self);

	return m_Tha60210011ClaPwControllerMethods->CounterGetter(self);
	}

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->pmcCounterGetter);
    mThis(self)->pmcCounterGetter = NULL;
    m_AtObjectMethods->Delete((AtObject)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(pmcCounterGetter);
    }

static void OverrideAtObject(ThaClaPwController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideTha60210011ClaPwController(ThaClaPwController self)
    {
    Tha60210011ClaPwController controller = (Tha60210011ClaPwController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ClaPwControllerMethods = mMethodsGet(controller);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ClaPwControllerOverride, m_Tha60210011ClaPwControllerMethods, sizeof(m_Tha60210011ClaPwControllerOverride));

        mMethodOverride(m_Tha60210011ClaPwControllerOverride, CounterGetter);
        }

    mMethodsSet(controller, &m_Tha60210011ClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
	{
	OverrideAtObject(self);
	OverrideTha60210011ClaPwController(self);
	}

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ClaPwController);
    }

ThaClaPwController Tha60290021ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60290021ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ClaPwControllerObjectInit(newController, cla);
    }

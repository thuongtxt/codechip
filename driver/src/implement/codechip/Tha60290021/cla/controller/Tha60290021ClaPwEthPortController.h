/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290021ClaEthPortController.h
 * 
 * Created Date: Aug 11, 2016
 *
 * Description : CLA ETH port controller header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021CLAETHPORTCONTROLLER_H_
#define _THA60290021CLAETHPORTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaEthPortController Tha60290021ClaPwEthPortControllerNew(ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021CLAETHPORTCONTROLLER_H_ */


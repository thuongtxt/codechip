/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60290021ClaPwControllerInternal.h
 * 
 * Created Date: Apr 27, 2017
 *
 * Description : PW controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021CLAPWCONTROLLERINTERNAL_H_
#define _THA60290021CLAPWCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../Tha60210011/cla/controllers/Tha60210011ClaPwControllerInternal.h"
#include "Tha60290021ClaPwController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ClaPwController
    {
    tTha60210011ClaPwController super;

    /* Private data */
    Tha60210011ClaPwCounterGetter pmcCounterGetter;
    }tTha60290021ClaPwController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaPwController Tha60290021ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021CLAPWCONTROLLERINTERNAL_H_ */


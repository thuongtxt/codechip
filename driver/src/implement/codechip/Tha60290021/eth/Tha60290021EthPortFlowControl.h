/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290021EthPortFlowControl.h
 * 
 * Created Date: May 30, 2019
 *
 * Description : 6029xxxx Ethernet Flow Control
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021ETHPORTFLOWCONTROL_H_
#define _THA60290021ETHPORTFLOWCONTROL_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021EthPortFlowControl *Tha60290021EthPortFlowControl;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60290021EthPortFlowControlTxBufferLevelGet(Tha60290021EthPortFlowControl self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021ETHPORTFLOWCONTROL_H_ */


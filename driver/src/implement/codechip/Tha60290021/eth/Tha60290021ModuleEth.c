/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290021ModuleEth.c
 *
 * Created Date: Jul 12, 2016
 *
 * Description : ETH  management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../man/Tha60290021Device.h"
#include "../man/Tha60290021DeviceFsm.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../physical/Tha60290021SerdesManager.h"
#include "../physical/Tha60290021SerdesManagerInternal.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "Tha60290021ModuleEth.h"
#include "Tha60290021EthFlowControlReg.h"
#include "Tha60290021SerdesEthPortInternal.h"
#include "Tha60290021FaceplateSerdesEthPortReg.h"
#include "Tha60290021ModuleEthInternal.h"
#include "Tha60290021PwEthPortInternal.h"
#include "Tha60290021SerdesBackplaneEthPort.h"
#include "Tha6029SerdesSgmiiDiagReg.h"
#include "Tha6029SerdesSgmiiMultirateReg.h"
#include "Tha60290021Eth10gReg.h"

/*--------------------------- Define -----------------------------------------*/
/* MAC-BASE ADDRESS FOR ALL PORTs */
#define cGeDccBaseAddress   0x00E0000
#define cGeKbyteBaseAddress 0x00E1000
#define cGeSpareBaseAddress 0x00E2000
#define cEthBypassBaseAddress 0xC0000UL

/* TPID Insert Value */
#define cAf6_upen_enbtpid_ins_cfgtpid_Mask(index)                                                           (cBit15_0 << ((index) * 16))
#define cAf6_upen_enbtpid_ins_cfgtpid_Shift(index)                                                          ((index) * 16)

#define cSgmiiDccKbytePortUsedId 17

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021ModuleEth)self)
#define cIteratorMask(_no)     (cBit0 << _no)
#define mShouldOpenFeature(self, versionWithBuilt) ShouldOpenFeature((AtModule)self, versionWithBuilt)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021ModuleEthMethods m_methods;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tAtModuleEthMethods          m_AtModuleEthOverride;
static tThaModuleEthMethods         m_ThaModuleEthOverride;
static tTha60210051ModuleEthMethods m_Tha60210051ModuleEthOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods     = NULL;
static const tAtModuleMethods    *m_AtModuleMethods     = NULL;
static const tAtModuleEthMethods *m_AtModuleEthMethods  = NULL;
static const tThaModuleEthMethods  *m_ThaModuleEthMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaEthSerdesManager SerdesManagerCreate(ThaModuleEth self)
    {
    AtUnused(self);
    return NULL;
    }

static uint8 NumFaceplatePorts(AtModuleEth self)
    {
    Tha60290021Device device = (Tha60290021Device)AtModuleDeviceGet((AtModule)self);
    return (uint8)Tha60290021DeviceNumFaceplateSerdes(device);
    }

static AtSerdesManager SerdesManager(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return AtDeviceSerdesManagerGet(device);
    }

static uint8 NumSgmiiPorts(AtModuleEth self)
    {
    Tha60290021Device device = (Tha60290021Device)AtModuleDeviceGet((AtModule)self);
    return (uint8)Tha60290021DeviceNumSgmiiSerdes(device);
    }

static uint8 NumInternal40GMacs(AtModuleEth self)
    {
    AtUnused(self);
    return 1;
    }

static uint8 Num40GBackplaneMacs(AtModuleEth self)
    {
    AtUnused(self);
    return 2;
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (Tha60290021DeviceIsRunningOnOtherPlatform(device))
        return 1;

    return (uint8)(NumFaceplatePorts(self) + NumSgmiiPorts(self) + NumInternal40GMacs(self) + Num40GBackplaneMacs(self));
    }

static eBool Is40GInternalMac(AtEthPort port)
    {
    uint8 portId = (uint8) AtChannelIdGet((AtChannel)port);
    return (portId == 0) ? cAtTrue : cAtFalse;
    }

static uint32 StartFaceplatePortId(AtModuleEth self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 StopFaceplatePortId(AtModuleEth self)
    {
    return NumFaceplatePorts(self);
    }

static uint32 StartSgmiiPortId(AtModuleEth self)
    {
    return StopFaceplatePortId(self) + 1;
    }

static uint32 StopSgmiiPortId(AtModuleEth self)
    {
    return StartSgmiiPortId(self) + NumSgmiiPorts(self) - 1;
    }

static uint32 Start40GBackplaneMacId(AtModuleEth self)
    {
    return StopSgmiiPortId(self) + 1;
    }

static uint32 Stop40GBackplaneMacId(AtModuleEth self)
    {
    return Start40GBackplaneMacId(self) + Num40GBackplaneMacs(self) - 1;
    }

static eBool IsFaceplatePort(Tha60290021ModuleEth self, uint8 portId)
    {
    AtUnused(self);
    return ((portId >= StartFaceplatePortId((AtModuleEth)self)) && (portId <= StopFaceplatePortId((AtModuleEth)self))) ? cAtTrue : cAtFalse;
    }

static AtEthPort FaceplatePortGet(AtModuleEth self, uint8 localPortId)
    {
    return AtModuleEthPortGet(self, (uint8)(localPortId + StartFaceplatePortId(self)));
    }

static eBool IsSgmiiPort(Tha60290021ModuleEth self, uint8 portId)
    {
    uint32 startId = StartSgmiiPortId((AtModuleEth)self);
    uint32 stopId = StopSgmiiPortId((AtModuleEth)self);
    return ((portId >= startId) && (portId <= stopId)) ? cAtTrue : cAtFalse;
    }

static eBool IsSgmiiDccPort(AtModuleEth self, uint8 portId)
    {
    AtSerdesManager serdesManager = SerdesManager(self);
    uint32 localId = (uint32)(portId - StartSgmiiPortId(self));
    return (localId == Tha60290021SerdesManagerSgmiiSerdesLocalDccId(serdesManager)) ? cAtTrue: cAtFalse;
    }

static eBool IsSgmiiKBytePort(AtModuleEth self, uint8 portId)
    {
    AtSerdesManager serdesManager = SerdesManager(self);
    uint32 localId = (uint32)(portId - StartSgmiiPortId(self));
    return (localId == Tha60290021SerdesManagerSgmiiSerdesLocalKByteId(serdesManager)) ? cAtTrue: cAtFalse;
    }

static eBool IsSgmiiDccKBytePort(AtModuleEth self, uint8 portId)
    {
    return IsSgmiiKBytePort(self, portId);
    }

static uint32 FaceplateMacBaseAddress(AtModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(port);
    return cInvalidUint32; /* TODO: Need To be Update */
    }

static uint32 SgmiiMacBaseAddress(AtModuleEth self, AtEthPort port)
    {
    uint8 portId = (uint8)AtChannelIdGet((AtChannel)port);
    eBool hasOnlyOneSgmiiMac = cAtFalse;

    /* From the version that has SGMII diagnostic removed, hardware just keep
     * only one SGMII MAC. This is to solve timing closure issue */
    if (!mMethodsGet(mThis(self))->HasSgmiiDiagnosticLogic(mThis(self)))
        hasOnlyOneSgmiiMac = cAtTrue;
    if (hasOnlyOneSgmiiMac && IsSgmiiDccKBytePort(self, portId))
        return cGeKbyteBaseAddress;

    if (IsSgmiiDccPort(self, portId))
        return cGeDccBaseAddress;

    if (IsSgmiiKBytePort(self, portId))
        return cGeKbyteBaseAddress;

    return cGeSpareBaseAddress;
    }

static uint32 BackplaneMacPortBaseAddress(AtModuleEth module)
    {
    AtUnused(module);
    return 0xF80000;
    }

static eBool IsBackplaneMac(Tha60290021ModuleEth self, uint8 portId)
    {
    uint32 startId = Start40GBackplaneMacId((AtModuleEth)self);
    uint32 stopId  = Stop40GBackplaneMacId((AtModuleEth)self);
    return ((portId >= startId) && (portId <= stopId));
    }

static uint32 MacBaseAddress(ThaModuleEth self, AtEthPort port)
    {
    AtModuleEth module = (AtModuleEth)self;
    uint8 portId = (uint8)AtChannelIdGet((AtChannel)port);

    if (mMethodsGet(mThis(self))->IsBackplaneMac(mThis(self), portId))
        return BackplaneMacPortBaseAddress(module);

    if (mMethodsGet(mThis(self))->IsFaceplatePort(mThis(self), portId))
        return FaceplateMacBaseAddress(module, port);

    if (mMethodsGet(mThis(self))->IsSgmiiPort(mThis(module), portId))
        return SgmiiMacBaseAddress(module, port);

    /* Just keep Old-Logic */
    return m_ThaModuleEthMethods->MacBaseAddress(self, port);
    }

static AtEthPort FaceplatePortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    return Tha60290021FaceplateSerdesEthPortNew(portId, (AtModuleEth)self);
    }

static AtEthPort KByteDccPortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    return Tha60290021SgmiiSohEthPortNew(portId, (AtModuleEth)self);
    }

static AtEthPort SgmiiPortCreate(AtModuleEth self, uint8 portId)
    {
    if (IsSgmiiDccKBytePort(self, portId))
        return mMethodsGet(mThis(self))->KByteDccPortCreate(mThis(self), portId);

    return Tha60290021SgmiiSerdesEthPortNew(portId, self);
    }

static AtEthPort SerdesBackplaneEthPortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    return Tha60290021SerdesBackplaneEthPortNew(portId, (AtModuleEth)self);
    }

static AtEthPort PwPortCreate(Tha60290021ModuleEth self, uint8 portId)
    {
    return Tha60290021PwEthPortNew(portId, (AtModuleEth)self);
    }

static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    if (mMethodsGet(mThis(self))->IsFaceplatePort(mThis(self), portId))
        return mMethodsGet(mThis(self))->FaceplatePortCreate(mThis(self), portId);

    if (mMethodsGet(mThis(self))->IsSgmiiPort(mThis(self), portId))
        return SgmiiPortCreate(self, portId);

    if (mMethodsGet(mThis(self))->IsBackplaneMac(mThis(self), portId))
        return mMethodsGet(mThis(self))->SerdesBackplaneEthPortCreate(mThis(self), portId);

    return mMethodsGet(mThis(self))->PwPortCreate(mThis(self), portId);
    }

static uint32 MdioPortAddress(AtMdio self, uint32 selectedPortId)
    {
    AtUnused(self);
    return cBit0 << selectedPortId;
    }

static const tAtMdioAddressCalculator *MdioAddressCalculator(void)
    {
    static tAtMdioAddressCalculator calculator;
    static tAtMdioAddressCalculator *pCalculator = NULL;

    if (pCalculator)
        return pCalculator;

    AtOsalMemInit(&calculator, 0, sizeof(calculator));
    calculator.PortAddress = MdioPortAddress;
    pCalculator = &calculator;

    return pCalculator;
    }

static uint32 MDIO1GBaseAddress(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return cMDIO1GBaseAddress;
    }

static AtMdio SgmiiSerdesMdioCreate(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtHal hal = AtDeviceIpCoreHalGet(device, AtModuleDefaultCoreGet((AtModule)self));
    uint32 mdio1GBaseAddress = mMethodsGet(mThis(self))->MDIO1GBaseAddress(mThis(self));
    AtMdio mdio = AtMdioSgmiiNew(mdio1GBaseAddress, hal);
    AtMdioAddressCalculatorSet(mdio, MdioAddressCalculator());
    AtMdioSimulateEnable(mdio, AtDeviceIsSimulated(device));

    if (AtDeviceTestbenchIsEnabled(device))
        AtMdioSgmiiTimeoutMsSet(mdio, AtDeviceTestbenchDefaultTimeoutMs());

    return mdio;
    }

static eBool NeedSwapMdioPort(AtModuleEth self)
    {
    /* TODO: old 10G version may need this swap, need test to confirm the versions
     * need this */
    AtUnused(self);
    return cAtFalse;
    }

static uint32 MdioPort(AtModuleEth self, AtSerdesManager serdesManager, uint32 serdesId)
    {
    uint32 localId = Tha60290021SerdesManagerSgmiiSerdesLocalId(serdesManager, serdesId);

    if (NeedSwapMdioPort(self))
        {
        if (localId == 0)
            return 1;
        if (localId == 1)
            return 0;
        }

    return localId;
    }

static AtMdio SerdesSgmiiMdio(AtModuleEth self, AtSerdesManager serdesManager, uint32 serdesId)
    {
    if (mThis(self)->sgmiiMdio == NULL)
        mThis(self)->sgmiiMdio = SgmiiSerdesMdioCreate(self);

    AtMdioPortSelect(mThis(self)->sgmiiMdio, MdioPort(self, serdesManager, serdesId));
    return mThis(self)->sgmiiMdio;
    }

static AtMdio SerdesMdio(AtModuleEth self, AtSerdesController serdes)
    {
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    AtSerdesManager serdesManager = SerdesManager(self);

    if (Tha60290021SerdesManagerSerdesIsSgmii(serdesManager, serdesId))
        return SerdesSgmiiMdio(self, serdesManager, serdesId);

    return NULL;
    }

static uint32 NumSerdesControllers(AtModuleEth self)
    {
    return AtSerdesManagerNumSerdesControllers(SerdesManager(self));
    }

static AtSerdesController SerdesController(AtModuleEth self, uint32 serdesId)
    {
    return AtSerdesManagerSerdesControllerGet(SerdesManager(self), serdesId);
    }

static AtSerdesController PortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort)
    {
    AtUnused(self);
    AtUnused(ethPort);
    return NULL;
    }

static eBool PortIsInternalMac(ThaModuleEth self, uint8 portId)
    {
    AtUnused(self);
    return (portId == 0) ? cAtTrue : cAtFalse;
    }

static eBool SerdesApsSupportedOnPort(AtModuleEth self, AtEthPort port)
    {
    return PortIsInternalMac((ThaModuleEth)self, (uint8)AtChannelIdGet((AtChannel)port));
    }

static eBool HasSerdesGroup(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 MaxNumVlanId(AtModuleEth self)
    {
    AtUnused(self);
    return 4096;
    }

static eBool IsExpectedVlanIdUsed(AtModuleEth self, uint16 vlanId)
    {
    return (ThaBitMaskBitVal(mThis(self)->expectedVlanIdMask, vlanId) == 1) ? cAtTrue : cAtFalse;
    }

static void UpdateExpectedVlanIdBitMask(AtModuleEth self, uint16 oldVlanId, uint16 newVlanId)
    {
    if (oldVlanId == newVlanId)
        return;
    ThaBitMaskClearBit(mThis(self)->expectedVlanIdMask, oldVlanId);
    ThaBitMaskSetBit(mThis(self)->expectedVlanIdMask, newVlanId);
    }

static uint8 NumSubPortVlanTpids(AtModuleEth self)
    {
    AtUnused(self);
    return cNumSubPortTpids;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret;
    uint32 bit_i;
    uint32 numBits = MaxNumVlanId((AtModuleEth)self);

    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    if (mThis(self)->expectedVlanIdMask)
        return cAtOk;

    mThis(self)->expectedVlanIdMask = ThaBitMaskNew(numBits);

    for (bit_i = 0; bit_i < numBits; bit_i ++)
        ThaBitMaskClearBit(mThis(self)->expectedVlanIdMask, bit_i);

    return cAtOk;
    }

static void Delete(AtObject self)
    {
    AtMdioDelete(mThis(self)->sgmiiMdio);
    mThis(self)->sgmiiMdio = NULL;
    AtObjectDelete((AtObject)mThis(self)->expectedVlanIdMask);
    mThis(self)->expectedVlanIdMask = NULL;
    AtObjectDelete((AtObject)mThis(self)->faceplateCounterLongAccess);
    mThis(self)->faceplateCounterLongAccess = NULL;

    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290021ModuleEth* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(expectedVlanIdMask);
    mEncodeNone(sgmiiMdio);
    mEncodeNone(cache);
    mEncodeObject(faceplateCounterLongAccess);
    }

static AtEthFlowControl PortFlowControlCreate(AtModuleEth self, AtEthPort port)
    {
    if (Tha60290021ModuleEthIsFaceplatePort(port))
        return mMethodsGet(mThis(self))->FaceplatePortFlowControlCreate(mThis(self), port);

    return NULL;
    }

static AtEthFlowControl FaceplatePortFlowControlCreate(Tha60290021ModuleEth self, AtEthPort port)
    {
    return Tha60290021EthPortFlowControlNew((AtModule)self, port);
    }

static eAtEthPortInterface FaceplatePortDefaultInterface(AtEthPort port)
    {
    AtSerdesController serdes = AtEthPortSerdesController(port);
    eAtSerdesMode mode = AtSerdesControllerModeGet(serdes);

    if (mode == cAtSerdesModeEth10G)
        return cAtEthPortInterfaceBaseR;

    if ((mode == cAtSerdesModeEth100M) ||
        (mode == cAtSerdesModeEth10M))
        return cAtEthPortInterfaceBaseFx;

    return cAtEthPortInterface1000BaseX;
    }

static eAtEthPortInterface PortDefaultInterface(AtModuleEth self, AtEthPort port)
    {
    uint8 portId = (uint8)AtChannelIdGet((AtChannel)port);

    if (mMethodsGet(mThis(self))->IsFaceplatePort(mThis(self), portId))
        return FaceplatePortDefaultInterface(port);

    if (mMethodsGet(mThis(self))->IsSgmiiPort(mThis(self), portId))
        return cAtEthPortInterfaceSgmii;

    if (mMethodsGet(mThis(self))->IsBackplaneMac(mThis(self), portId))
        return cAtEthPortInterfaceBaseKR;

    /* Backplane use XGMII, let super handle this */
    return m_AtModuleEthMethods->PortDefaultInterface(self, port);
    }

static uint32 AddressWithLocalAddress(uint32 localAddress)
    {
    return Tha60290021ModuleEthBypassBaseAddress() + localAddress;
    }

static uint32 Af6Reg_upen_enbflwctrl_Base(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return cAf6Reg_upen_enbflwctrl_Base;
    }

static eAtModuleEthRet FlowControlEnable(AtModuleEth self, eBool enable)
    {
    uint32 upen_enbflwctrl_Base = mMethodsGet(mThis(self))->Af6Reg_upen_enbflwctrl_Base(mThis(self));
    uint32 regAddr = AddressWithLocalAddress(upen_enbflwctrl_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_enbflwctrl_out_enbflw_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool FlowControlIsEnabled(AtModuleEth self)
    {
    uint32 upen_enbflwctrl_Base = mMethodsGet(mThis(self))->Af6Reg_upen_enbflwctrl_Base(mThis(self));
    uint32 regAddr = AddressWithLocalAddress(upen_enbflwctrl_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_upen_enbflwctrl_out_enbflw_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 TenGeBaseAddress(Tha60290021ModuleEth self, uint32 groupId)
    {
    AtUnused(self);

    if (groupId == 0)
        return 0x0020000;

    if (groupId == 1)
        return 0x0040000;

    if (groupId == 2)
        return 0x0060000;

    if (groupId == 3)
        return 0x0070000;

    return cInvalidUint32;
    }

static eAtRet TenGePortsInterruptDisable(AtModuleEth self)
    {
    uint8 group_i;

    for (group_i = 0; group_i < mMethodsGet(mThis(self))->NumberOf10GFaceplatePorts(mThis(self)); group_i++)
        {
        uint32 baseAddress = Tha60290021ModuleEthTenGeBaseAddress(self, group_i);
        uint32 regAddr = cAf6Reg_eth10g_link_fault_int_enb_Base + baseAddress;
        uint32 regVal = mModuleHwRead(self, regAddr);

        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gDataOrFrameSyncEnb_, 0);
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gClockLossEnb_, 0);
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gClockOutRangeEnb_, 0);
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gExErrorRatioEnb_, 0);
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gLocalFaultEnb_, 0);
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gRemoteFaultEnb_, 0);
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gInterrptionEnb_, 0);

        mModuleHwWrite(self, regAddr, 0);
        }

    return cAtOk;
    }

static eAtRet OneGePortsInterruptDisable(AtModuleEth self)
    {
    uint8 numSixteenPortGroup = Tha60290021ModuleEthNumSixteenPortGroup(self);
    uint8 groupId = 0;

    for (groupId = 0; groupId < numSixteenPortGroup; groupId++)
        {
        uint32 baseAddress = Tha602900xxModuleEthMultirateBaseAddress(self, groupId);

        mModuleHwWrite(self, baseAddress + cAf6Reg_an_int_enb_pen_Base, 0);
        mModuleHwWrite(self, baseAddress + cAf6Reg_an_rfi_int_enb_pen_Base, 0);
        mModuleHwWrite(self, baseAddress + cAf6Reg_los_sync_int_enb_pen_Base, 0);
        mModuleHwWrite(self, baseAddress + cAf6Reg_rxexcer_enb_pen_Base, 0);
        }
    return cAtOk;
    }

static eAtRet PortsInterruptDisable(AtModuleEth self)
    {
    eAtRet ret = cAtOk;

    ret |= TenGePortsInterruptDisable(self);
    ret |= OneGePortsInterruptDisable(self);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    AtModuleEth ethModule = (AtModuleEth)self;
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Global flow control should be enabled as default */
    Tha60290021ModuleEthFlowControlEnable(ethModule, cAtTrue);

    /* Set default VLAN TPID */
    Tha60290021ModuleEthSubPortVlanTxTpidSet(ethModule, 0, 0x9200);
    Tha60290021ModuleEthSubPortVlanTxTpidSet(ethModule, 1, 0x9300);
    Tha60290021ModuleEthSubPortVlanExpectedTpidSet(ethModule, 0, 0x9200);
    Tha60290021ModuleEthSubPortVlanExpectedTpidSet(ethModule, 1, 0x9300);

    /* Duplicate traffic for backplan serdes, enable HW FSM pins */
    Tha60210021ModuleEthTxBackPlaneSerdesBridgeEnable((ThaModuleEth)self, cAtTrue);
    ThaModuleEthRxFsmPinEnable((ThaModuleEth)self, cAtTrue);
    ThaModuleEthTxFsmPinEnable((ThaModuleEth)self, cAtTrue);

    /* Init Bitmask for expected subport vlan */
    ThaBitMaskInit(mThis(self)->expectedVlanIdMask);

    return PortsInterruptDisable(ethModule);
    }

static uint32 StartHardwareVersionSupportRxErrPsnPacketsCounter(Tha60210051ModuleEth self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 StartVersionSupportsSerdesAps(ThaModuleEth self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet SubPortVlanTransmitTpidSet(AtModuleEth self, uint8 tpidIndex, uint16 tpid)
    {
    uint32 regAddr = AddressWithLocalAddress(cAf6Reg_upen_cfginstpid_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, cAf6_upen_enbtpid_ins_cfgtpid_Mask(tpidIndex), cAf6_upen_enbtpid_ins_cfgtpid_Shift(tpidIndex), tpid);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool TpidIndexIsInRange(uint8 tpidIndex)
    {
    return (tpidIndex < cNumSubPortTpids) ? cAtTrue : cAtFalse;
    }

static uint16 SubPortVlanTransmitTpidGet(AtModuleEth self, uint8 tpidIndex)
    {
    uint16 tpid;
    uint32 regAddr = AddressWithLocalAddress(cAf6Reg_upen_cfginstpid_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mFieldGet(regVal, cAf6_upen_enbtpid_ins_cfgtpid_Mask(tpidIndex), cAf6_upen_enbtpid_ins_cfgtpid_Shift(tpidIndex), uint16, &tpid);

    return tpid;
    }

static eAtRet SubPortVlanExpectedTpidSet(AtModuleEth self, uint8 tpidIndex, uint16 tpid)
    {
    uint32 regAddr = AddressWithLocalAddress(cAf6Reg_upen_cfgrmtpid_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, cAf6_upen_enbtpid_ins_cfgtpid_Mask(tpidIndex), cAf6_upen_enbtpid_ins_cfgtpid_Shift(tpidIndex), tpid);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint16 SubPortVlanExpectedTpidGet(AtModuleEth self, uint8 tpidIndex)
    {
    uint16 tpid;
    uint32 regAddr = AddressWithLocalAddress(cAf6Reg_upen_cfgrmtpid_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mFieldGet(regVal, cAf6_upen_enbtpid_ins_cfgtpid_Mask(tpidIndex), cAf6_upen_enbtpid_ins_cfgtpid_Shift(tpidIndex), uint16, &tpid);

    return tpid;
    }

static AtLongRegisterAccess FaceplateCounterLongAccessCreate(AtModule self)
    {
    static uint32 holdRegs = cEthBypassBaseAddress + cAf6Reg_epa_hold_status_Base;
    AtUnused(self);
    return AtDefaultLongRegisterAccessNew(&holdRegs, 1, &holdRegs, 1);
    }

static AtLongRegisterAccess FaceplateCounterLongAccess(AtModule self)
    {
    if (mThis(self)->faceplateCounterLongAccess)
        return mThis(self)->faceplateCounterLongAccess;

    mThis(self)->faceplateCounterLongAccess = FaceplateCounterLongAccessCreate(self);
    return mThis(self)->faceplateCounterLongAccess;
    }

static eBool IsEthPassThroughAddress(AtModule self, uint32 localAddress)
    {
    uint32 startAddress = Tha60290021ModuleEthBypassBaseAddress();
    uint32 stopAddress = startAddress + 0xFFFF;
    AtUnused(self);
    return mInRange(localAddress, startAddress, stopAddress);
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    if (IsEthPassThroughAddress(self, localAddress))
        return FaceplateCounterLongAccess(self);

    return m_AtModuleMethods->LongRegisterAccess(self, localAddress);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    if (IsEthPassThroughAddress(self, localAddress))
        return cAtTrue;

    return m_AtModuleMethods->HasRegister(self, localAddress);
    }

static AtDevice Device(AtModule self)
    {
    return AtModuleDeviceGet(self);
    }

static eBool HasRole(AtModule self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet RoleActiveForce(AtModule self, eBool forced)
    {
    return Tha60290021DeviceRoleActiveForce(Device(self), forced);
    }

static eBool RoleActiveIsForced(AtModule self)
    {
    return Tha60290021DeviceRoleActiveIsForced(Device(self));
    }

static eAtRet RoleInputEnable(AtModule self, eBool enabled)
    {
    return Tha60290021DeviceRoleInputEnable(Device(self), enabled);
    }

static eBool RoleInputIsEnabled(AtModule self)
    {
    return Tha60290021DeviceRoleInputIsEnabled(Device(self));
    }

static eAtDeviceRole RoleInputStatus(AtModule self)
    {
    return Tha60290021DeviceRoleInputStatus(Device(self));
    }

static eAtRet TxFsmPinEnable(ThaModuleEth self, eBool enabled)
    {
    return Tha60290021DeviceRoleInputEnable(Device((AtModule)self), enabled);
    }

static eBool TxFsmPinIsEnabled(ThaModuleEth self)
    {
    return Tha60290021DeviceRoleInputIsEnabled(Device((AtModule)self));
    }

static eBool TxFsmPinStatusGet(ThaModuleEth self)
    {
    return Tha60290021DeviceTxFsmPinStatus(Device((AtModule)self));
    }

static eAtRet RxFsmPinEnable(ThaModuleEth self, eBool enabled)
    {
    return Tha60290021DeviceRxFsmPinEnable(Device((AtModule)self), enabled);
    }

static eBool RxFsmPinIsEnabled(ThaModuleEth self)
    {
    return Tha60290021DeviceRxFsmPinIsEnabled(Device((AtModule)self));
    }

static eBool RxFsmPinStatusGet(ThaModuleEth self)
    {
    return Tha60290021DeviceRxFsmPinStatusGet(Device((AtModule)self));
    }

static eAtRet RxBackPlaneSerdesSelect(ThaModuleEth self, uint32 serId)
    {
    return Tha60290021DeviceRxBackPlaneSerdesSelect(Device((AtModule)self), serId);
    }

static uint32 RxBackPlaneSelectedSerdes(ThaModuleEth self)
    {
    return Tha60290021DeviceRxBackPlaneSelectedSerdes(Device((AtModule)self));
    }

static eAtRet TxBackPlaneSerdesBridgeEnable(ThaModuleEth self, eBool enabled)
    {
    return Tha60290021DeviceTxBackPlaneSerdesBridgeEnable(Device((AtModule)self), enabled);
    }

static eBool FsmIsSupported(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtModuleSdh SdhModule(AtModuleEth self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static eBool SgmiiPortIsRemoved(AtModuleEth self, uint8 portId)
    {
    AtUnused(self);

    if (portId == cSgmiiDccKbytePortUsedId)
        return cAtFalse;

    return cAtTrue;
    }

static eBool PortCanBeUsed(AtModuleEth self, uint8 portId)
    {
    if (mMethodsGet(mThis(self))->IsFaceplatePort(mThis(self), portId))
        {
        uint8 localId = Tha60290021ModuleEthFaceplateLocalId(self, portId);
        if (localId < Tha60290021ModuleSdhNumUseableFaceplateLines(SdhModule(self)))
            return cAtTrue;
        return cAtFalse;
        }

    if (mMethodsGet(mThis(self))->IsSgmiiPort(mThis(self), portId) && SgmiiPortIsRemoved(self, portId))
        return cAtFalse;

    return m_AtModuleEthMethods->PortCanBeUsed(self, portId);
    }

static eAtRet Version1_5SgmiiDiagnosticModeEnable(AtModuleEth self, eBool enable)
    {
    uint32 regAddr = Tha60290021ModuleEthSgmiiDiagnosticBaseAddress() + cAf6Reg_control_pen9_Base;
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 port_i;

    for (port_i = 0; port_i < NumSgmiiPorts(self); port_i++)
        {
        uint32 enableMask = cAf6_control_pen9_Channel0_Testen_Mask << port_i;
        uint32 enableShift = cAf6_control_pen9_Channel0_Testen_Shift + port_i;
        mRegFieldSet(regVal, enable, enable ? 1 : 0);
        }

    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet Version1_7SgmiiDiagnosticModeEnable(AtModuleEth self, eBool enable)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint32 regAddr = Tha60290021DeviceTopRegisterWithLocalAddress(device, cAf6Reg_o_control10_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 port_i;

    for (port_i = 0; port_i < NumSgmiiPorts(self); port_i++)
        {
        uint32 enableMask = cAf6_o_control10_Channel0_Testen_Mask << port_i;
        uint32 enableShift = cAf6_o_control10_Channel0_Testen_Shift + port_i;
        mRegFieldSet(regVal, enable, enable ? 1 : 0);
        }

    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static ThaVersionReader VersionReader(AtModuleEth self)
    {
    return ThaDeviceVersionReader(AtModuleDeviceGet((AtModule)self));
    }

static eBool ShouldOpenFeature(AtModule self, uint32 versionWithBuilt)
    {
    ThaVersionReader versionReader = VersionReader((AtModuleEth)self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= versionWithBuilt) ? cAtTrue : cAtFalse;
    }

static eBool UsePdhSgmiiPrbs(Tha60290021ModuleEth self)
    {
    return mShouldOpenFeature(self, ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x7, 0x0));
    }

static eAtRet SgmiiDiagnosticModeEnable(AtModuleEth self, eBool enable)
    {
    ThaVersionReader versionReader = VersionReader(self);
    uint32 version1_5 = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x5, 0x0);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);

    if (mMethodsGet(mThis(self))->UsePdhSgmiiPrbs(mThis(self)))
        return Version1_7SgmiiDiagnosticModeEnable(self, enable);

    /* At version 1.5, hardware needs to write these new controls */
    if (currentVersion >= version1_5)
        return Version1_5SgmiiDiagnosticModeEnable(self, enable);

    return cAtOk;
    }

static eBool PortCauseInterrupt(ThaModuleEth self, uint32 glbIntr)
    {
    AtUnused(self);
    return (glbIntr & (cAf6_global_interrupt_status_TenGeIntStatus_Mask|
                       cAf6_global_interrupt_status_GeIntStatus_Mask   |
                       cAf6_global_interrupt_status_Mac40gIntStatus_Mask)) ? cAtTrue : cAtFalse;
    }

static void TenGePortsInterruptProcess(ThaModuleEth self)
    {
    uint8 groupId;
    uint8 numGroups = mMethodsGet(mThis(self))->NumberOfFaceplateGroupsGet(mThis(self));
    static uint8 numPortsInGroup = 8;

    for (groupId = 0; groupId < numGroups; groupId++)
        {
        uint8 eth10gLocalId = (uint8)(groupId * numPortsInGroup);
        AtEthPort port = Tha60290021ModuleEthFaceplatePortGet((AtModuleEth)self, eth10gLocalId);

        AtChannelInterruptProcess((AtChannel)port, 0);
        }
    }

static void GePortsInterruptProcess(ThaModuleEth self)
    {
    uint8 portId, groupId;
    uint8 numGroups = mMethodsGet(mThis(self))->NumberOfFaceplateGroupsGet(mThis(self));
    const uint8 numPortsInGroup = 8;

    for (groupId = 0; groupId < numGroups; groupId++)
        {
        uint8 numSixteenPortGroup = groupId / 2;
        uint32 portBaseAddress = Tha602900xxModuleEthMultirateBaseAddress((AtModuleEth)self, numSixteenPortGroup);
        uint32 ethIntr = mModuleHwRead(self, portBaseAddress + cAf6Reg_GE_Interrupt_OR_Base);
        for (portId = 0; portId < numPortsInGroup; portId++)
            {
            AtEthPort port = Tha60290021ModuleEthFaceplatePortGet((AtModuleEth)self, (uint8)(groupId * numPortsInGroup + portId));
            Tha60290021FaceplateEthPortOneGeInterruptProcess(port, ethIntr);
            }
        }
    }

static void Backplane40GPortsInterruptProcess(ThaModuleEth self, uint32 portIntr)
    {
    uint8 portId;
    uint8 num40GPorts = Num40GBackplaneMacs((AtModuleEth)self);

    for (portId = 0; portId < num40GPorts; portId++)
        {
        if (portIntr & cIteratorMask(portId))
            {
            uint8 subPort_i;
            AtEthPort port = Tha60290021ModuleEth40GMacPortGet((AtModuleEth)self, portId);
            AtChannelInterruptProcess((AtChannel)port, 0);

            for (subPort_i = 0; subPort_i < 4; subPort_i++)
                {
                AtEthPort subPort = AtEthPortSubPortGet(port, subPort_i);

                AtChannelInterruptProcess((AtChannel)subPort, subPort_i);
                }
            }
        }
    }

static void PortInterruptProcess(ThaModuleEth self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(hal);
    if (glbIntr & cAf6_global_interrupt_status_TenGeIntStatus_Mask)
        TenGePortsInterruptProcess(self);

    if (glbIntr & cAf6_global_interrupt_status_GeIntStatus_Mask)
        GePortsInterruptProcess(self);

    if (glbIntr & cAf6_global_interrupt_status_Mac40gIntStatus_Mask)
        {
        uint32 portIntr = mRegField(glbIntr, cAf6_global_interrupt_status_Mac40gIntStatus_);
        Backplane40GPortsInterruptProcess(self, portIntr);
        }
    }

static eBool InterruptIsSupported(ThaModuleEth self)
    {
    return mShouldOpenFeature(self, ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x2, 0x0));
    }

static uint8 NumberOfFaceplateGroupsGet(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return 1;
    }

static eBool HasSgmiiDiagnosticLogic(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet CacheSubPortVlanTransmitTpidSet(AtModuleEth self, uint8 tpidIndex, uint16 tpid)
    {
    tTha60290021ModuleEthCache *cache = AtModuleCacheGet((AtModule)self);

    if (cache)
        cache->subportVlanTxTpids[tpidIndex] = tpid;

    return cAtOk;
    }

static uint16 CacheSubPortVlanTransmitTpidGet(AtModuleEth self, uint8 tpidIndex)
    {
    tTha60290021ModuleEthCache *cache = AtModuleCacheGet((AtModule)self);

    if (cache)
        return cache->subportVlanTxTpids[tpidIndex];

    return cInvalidUint16;
    }

static eAtRet CacheSubPortVlanExpectedTpidSet(AtModuleEth self, uint8 tpidIndex, uint16 tpid)
    {
    tTha60290021ModuleEthCache *cache = AtModuleCacheGet((AtModule)self);

    if (cache)
        cache->subportVlanExpectedTpids[tpidIndex] = tpid;

    return cAtOk;
    }

static uint16 CacheSubPortVlanExpectedTpidGet(AtModuleEth self, uint8 tpidIndex)
    {
    tTha60290021ModuleEthCache *cache = AtModuleCacheGet((AtModule)self);

    if (cache)
        return cache->subportVlanExpectedTpids[tpidIndex];

    return cInvalidUint16;
    }

static uint32 CacheSize(AtModule self)
    {
    AtUnused(self);
    return sizeof(tTha60290021ModuleEthCache);
    }

static void **CacheMemoryAddress(AtModule self)
    {
    return &(mThis(self)->cache);
    }

static eBool KbyteDccShouldUseSameSgmiiPort(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtEthPort DccEthPortGet(ThaModuleEth self)
    {
    if (KbyteDccShouldUseSameSgmiiPort(self))
        return Tha60290021ModuleEthSgmiiKbytePortGet((AtModuleEth)self);

    return Tha60290021ModuleEthSgmiiPortGet((AtModuleEth)self, 1);
    }

static uint32 MultirateBaseAddress(Tha60290021ModuleEth self, uint8 sixteenPortGroup)
    {
    AtUnused(self);
    AtUnused(sixteenPortGroup);
    return 0x0030000;
    }

static uint8 NumSixteenPortGroup(Tha60290021ModuleEth self)
    {
    AtUnused(self);
    return 1;
    }

static Tha60290021EthPortCounters FaceplatePortCountersCreate(Tha60290021ModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    return Tha60290021EthPortCountersNew(port);
    }

static uint8 NumberOf10GFaceplatePorts(Tha60290021ModuleEth self)
    {
    return mMethodsGet(self)->NumberOfFaceplateGroupsGet(self);
    }

static void OverrideTha60210051ModuleEth(AtModuleEth self)
    {
    Tha60210051ModuleEth module = (Tha60210051ModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051ModuleEthOverride, mMethodsGet(module), sizeof(m_Tha60210051ModuleEthOverride));

        mMethodOverride(m_Tha60210051ModuleEthOverride, StartHardwareVersionSupportRxErrPsnPacketsCounter);
        }

    mMethodsSet(module, &m_Tha60210051ModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(ethModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, m_ThaModuleEthMethods, sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, SerdesManagerCreate);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesControllerCreate);
        mMethodOverride(m_ThaModuleEthOverride, PortIsInternalMac);
        mMethodOverride(m_ThaModuleEthOverride, HasSerdesGroup);
        mMethodOverride(m_ThaModuleEthOverride, StartVersionSupportsSerdesAps);
        mMethodOverride(m_ThaModuleEthOverride, MacBaseAddress);
        mMethodOverride(m_ThaModuleEthOverride, RxFsmPinEnable);
        mMethodOverride(m_ThaModuleEthOverride, RxFsmPinIsEnabled);
        mMethodOverride(m_ThaModuleEthOverride, RxFsmPinStatusGet);
        mMethodOverride(m_ThaModuleEthOverride, TxFsmPinEnable);
        mMethodOverride(m_ThaModuleEthOverride, TxFsmPinIsEnabled);
        mMethodOverride(m_ThaModuleEthOverride, TxFsmPinStatusGet);
        mMethodOverride(m_ThaModuleEthOverride, FsmIsSupported);
        mMethodOverride(m_ThaModuleEthOverride, PortCauseInterrupt);
        mMethodOverride(m_ThaModuleEthOverride, PortInterruptProcess);
        mMethodOverride(m_ThaModuleEthOverride, InterruptIsSupported);
        mMethodOverride(m_ThaModuleEthOverride, DccEthPortGet);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, NumSerdesControllers);
        mMethodOverride(m_AtModuleEthOverride, SerdesController);
        mMethodOverride(m_AtModuleEthOverride, SerdesApsSupportedOnPort);
        mMethodOverride(m_AtModuleEthOverride, PortFlowControlCreate);
        mMethodOverride(m_AtModuleEthOverride, PortDefaultInterface);
        mMethodOverride(m_AtModuleEthOverride, SerdesMdio);
        mMethodOverride(m_AtModuleEthOverride, PortCanBeUsed);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, CacheSize);
        mMethodOverride(m_AtModuleOverride, CacheMemoryAddress);
        mMethodOverride(m_AtModuleOverride, HasRole);
        mMethodOverride(m_AtModuleOverride, RoleActiveForce);
        mMethodOverride(m_AtModuleOverride, RoleActiveIsForced);
        mMethodOverride(m_AtModuleOverride, RoleInputEnable);
        mMethodOverride(m_AtModuleOverride, RoleInputIsEnabled);
        mMethodOverride(m_AtModuleOverride, RoleInputStatus);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtObject(AtModuleEth self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    OverrideTha60210051ModuleEth(self);
    }

static void MethodsInit(Tha60290021ModuleEth self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, UsePdhSgmiiPrbs);
        mMethodOverride(m_methods, FaceplatePortCreate);
        mMethodOverride(m_methods, SerdesBackplaneEthPortCreate);
        mMethodOverride(m_methods, NumberOfFaceplateGroupsGet);
        mMethodOverride(m_methods, PwPortCreate);
        mMethodOverride(m_methods, KByteDccPortCreate);
        mMethodOverride(m_methods, HasSgmiiDiagnosticLogic);
        mMethodOverride(m_methods, IsBackplaneMac);
        mMethodOverride(m_methods, IsFaceplatePort);
        mMethodOverride(m_methods, IsSgmiiPort);
        mMethodOverride(m_methods, MultirateBaseAddress);
        mMethodOverride(m_methods, NumSixteenPortGroup);
        mMethodOverride(m_methods, TenGeBaseAddress);
        mMethodOverride(m_methods, MDIO1GBaseAddress);
        mMethodOverride(m_methods, Af6Reg_upen_enbflwctrl_Base);
        mMethodOverride(m_methods, FaceplatePortCountersCreate);
        mMethodOverride(m_methods, FaceplatePortFlowControlCreate);
        mMethodOverride(m_methods, NumberOf10GFaceplatePorts);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModuleEth);
    }

AtModuleEth Tha60290021ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60290021ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModuleEthObjectInit(newModule, device);
    }

eBool Tha60290021ModuleEthIsFaceplatePort(AtEthPort self)
    {
    if (self)
        {
        AtModuleEth module = (AtModuleEth) AtChannelModuleGet((AtChannel) self);
        uint8 portId = (uint8) AtChannelIdGet((AtChannel) self);
        return mMethodsGet(mThis(module))->IsFaceplatePort(mThis(module), portId);
        }

    return cAtFalse;
    }

eBool Tha60290021ModuleEthIs40GInternalPort(AtEthPort port)
    {
    if (port)
        return Is40GInternalMac(port);
    return cAtFalse;
    }

eBool Tha60290021ModuleEthIsExpectedVlanIdInUse(AtModuleEth self, uint16 vlanId)
    {
    if (self)
        return IsExpectedVlanIdUsed(self, vlanId);
    return cAtFalse;
    }

void Tha60290021ModuleEthUpdateExpectedVlanIdBitMask(AtModuleEth self, uint16 oldVlanId, uint16 newVlanId)
    {
    if (self)
        UpdateExpectedVlanIdBitMask(self, oldVlanId, newVlanId);
    }

AtEthPort Tha60290021ModuleEthFaceplatePortGet(AtModuleEth self, uint8 localPortId)
    {
    if (self)
        return FaceplatePortGet(self, localPortId);
    return NULL;
    }

uint8 Tha60290021ModuleEthFaceplateLocalId(AtModuleEth self, uint8 portId)
    {
    return (uint8)(portId - StartFaceplatePortId(self));
    }

AtEthPort Tha60290021ModuleEthBackplanePortGet(AtModuleEth self)
    {
    return AtModuleEthPortGet(self, 0);
    }

AtEthPort Tha60290021ModuleEthSgmiiKbytePortGet(AtModuleEth self)
    {
    return Tha60290021ModuleEthSgmiiPortGet(self, 0);
    }

AtEthPort Tha60290021ModuleEthSgmiiDccPortGet(AtModuleEth self)
    {
    return ThaModuleEthDccEthPortGet((ThaModuleEth)self);
    }

AtEthPort Tha60290021ModuleEthSgmiiPortGet(AtModuleEth self, uint32 localPortId)
    {
    return AtModuleEthPortGet(self, (uint8)(StartSgmiiPortId(self) + localPortId));
    }

uint8 Tha60290021ModuleEthSgmiiLocalId(AtModuleEth self, uint32 portFlatId)
    {
    AtUnused(self);
    return (uint8)(portFlatId - StartSgmiiPortId(self));
    }

uint8 Tha60290021ModuleEth40GBackplaneMacLocalId(AtModuleEth self, uint32 portFlatId)
    {
    AtUnused(self);
    return (uint8)(portFlatId - Start40GBackplaneMacId(self));
    }

uint8 Tha60290021ModuleEthNumSubPortVlanTpids(AtModuleEth self)
    {
    if (self)
        return NumSubPortVlanTpids(self);
    return 0;
    }

eAtRet Tha60290021ModuleEthSubPortVlanTxTpidSet(AtModuleEth self, uint8 tpidIndex, uint16 tpid)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (TpidIndexIsInRange(tpidIndex))
        {
        CacheSubPortVlanTransmitTpidSet(self, tpidIndex, tpid);
        return SubPortVlanTransmitTpidSet(self, tpidIndex, tpid);
        }

    return cAtErrorOutOfRangParm;
    }

uint16 Tha60290021ModuleEthSubPortVlanTransmitTpidGet(AtModuleEth self, uint8 tpidIndex)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (TpidIndexIsInRange(tpidIndex))
        {
        if (AtModuleInAccessible((AtModule)self))
            return CacheSubPortVlanTransmitTpidGet(self, tpidIndex);
        return SubPortVlanTransmitTpidGet(self, tpidIndex);
        }

    return cFaceplatePortInvalidTpId;
    }

eAtRet Tha60290021ModuleEthSubPortVlanExpectedTpidSet(AtModuleEth self, uint8 tpidIndex, uint16 tpid)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (TpidIndexIsInRange(tpidIndex))
        {
        CacheSubPortVlanExpectedTpidSet(self, tpidIndex, tpid);
        return SubPortVlanExpectedTpidSet(self, tpidIndex, tpid);
        }

    return cAtErrorOutOfRangParm;
    }

uint16 Tha60290021ModuleEthSubPortVlanExpectedTpidGet(AtModuleEth self, uint8 tpidIndex)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (TpidIndexIsInRange(tpidIndex))
        {
        if (AtModuleInAccessible((AtModule)self))
            return CacheSubPortVlanExpectedTpidGet(self, tpidIndex);
        return SubPortVlanExpectedTpidGet(self, tpidIndex);
        }

    return cFaceplatePortInvalidTpId;
    }

uint8 Tha60290021ModuleEthNumFaceplatePorts(AtModuleEth self)
    {
    if (self)
        return NumFaceplatePorts(self);

    return 0;
    }

uint32 Tha60290021ModuleEthBypassBaseAddress(void)
    {
    return cEthBypassBaseAddress;
    }

eBool Tha60290021ModuleEthFlowControlIsEnabled(AtModuleEth self)
    {
    if (self)
        return FlowControlIsEnabled(self);
    return cAtFalse;
    }

eAtModuleEthRet Tha60290021ModuleEthFlowControlEnable(AtModuleEth self, eBool enable)
    {
    if (self)
        return FlowControlEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60290021ModuleEthIsSgmiiPort(AtModuleEth self, uint8 portId)
    {
    if (self)
        return mMethodsGet(mThis(self))->IsSgmiiPort(mThis(self), portId);
    return cAtFalse;
    }

eBool Tha60290021EthPortIsSgmiiPort(AtEthPort self)
    {
    if (self)
        {
        AtModuleEth module = (AtModuleEth) AtChannelModuleGet((AtChannel) self);
        uint8 portId = (uint8) AtChannelIdGet((AtChannel) self);
        return mMethodsGet(mThis(module))->IsSgmiiPort(mThis(module), portId);
        }

    return cAtFalse;
    }

AtMdio Tha6029SgmiiSerdesMdioCreate(AtModuleEth self)
    {
    return SgmiiSerdesMdioCreate(self);
    }

AtEthPort Tha60290021ModuleEth40GMacPortGet(AtModuleEth self, uint32 localPortId)
    {
    return AtModuleEthPortGet(self, (uint8)(localPortId + Start40GBackplaneMacId(self)));
    }

eBool Tha60290021ModuleEthIsBackplaneMac(AtEthPort self)
    {
    if (self)
        {
        AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
        return mMethodsGet(mThis(ethModule))->IsBackplaneMac(mThis(ethModule), (uint8)AtChannelIdGet((AtChannel)self));
        }

    return cAtFalse;
    }

eBool Tha60290021ModuleEthIsSgmiiKBytePort(AtEthPort self)
    {
    if (self)
        {
        AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
        return IsSgmiiKBytePort(ethModule, (uint8)AtChannelIdGet((AtChannel)self));
        }

    return cAtFalse;
    }

eBool Tha60290021ModuleEthIsSgmiiDccPort(AtEthPort self)
    {
    if (self)
        {
        AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
        return IsSgmiiDccPort(ethModule, (uint8)AtChannelIdGet((AtChannel)self));
        }

    return cAtFalse;
    }

eAtModuleEthRet Tha602900xxEthPortCounterTickModeSet(AtEthPort port, eTha602900xxEthPortCounterTickMode mode)
    {
    if (port == NULL)
        return cAtErrorNullPointer;

    if (AtEthPortIsBackplaneMac(port))
        return Tha602900xxSerdesBackplaneCounterTickModeSet(port, mode);

    return (mode == cTha602900xxEthPortCounterTickModeAuto) ? cAtOk : cAtErrorModeNotSupport;
    }

eTha602900xxEthPortCounterTickMode Tha602900xxEthPortCounterTickModeGet(AtEthPort port)
    {
    if (port == NULL)
        return cTha602900xxEthPortCounterTickModeUnknown;

    if (AtEthPortIsBackplaneMac(port))
        return Tha602900xxSerdesBackplaneCounterTickModeGet(port);

    return cTha602900xxEthPortCounterTickModeAuto;
    }

const char *Tha602900xxEthPortCounterTickMode2String(eTha602900xxEthPortCounterTickMode mode)
    {
    switch (mode)
        {
        case cTha602900xxEthPortCounterTickModeUnknown: return "unknown";
        case cTha602900xxEthPortCounterTickModeDisable: return "disable";
        case cTha602900xxEthPortCounterTickModeAuto   : return "auto";
        case cTha602900xxEthPortCounterTickModeManual : return "manual";
        default:
            return NULL;
        }
    }

eAtRet Tha60290021ModuleEthRxBackPlaneSerdesSelect(ThaModuleEth self, uint32 serdesId)
    {
    return RxBackPlaneSerdesSelect(self, serdesId);
    }

uint32 Tha60290021ModuleEthRxBackPlaneSelectedSerdes(ThaModuleEth self)
    {
    return RxBackPlaneSelectedSerdes(self);
    }

eAtRet Tha60210021ModuleEthTxBackPlaneSerdesBridgeEnable(ThaModuleEth self, eBool enabled)
    {
    return TxBackPlaneSerdesBridgeEnable(self, enabled);
    }

AtEthPort Tha60290021ModuleEthSgmiiDccKbytePortGet(AtModuleEth self)
    {
    return Tha60290021ModuleEthSgmiiKbytePortGet(self);
    }

uint32 Tha60290021ModuleEthSgmiiDiagnosticBaseAddress(void)
    {
    return cSerdesSgmiiDiagBaseAdress;
    }

eAtRet Tha60290021ModuleEthSgmiiDiagnosticModeEnable(AtModuleEth self, eBool enabled)
    {
    if (self)
        return SgmiiDiagnosticModeEnable(self, enabled);
    return cAtErrorNullPointer;
    }

uint32 Tha602900xxModuleEthMultirateBaseAddress(AtModuleEth self, uint8 sixteenPortGroupId)
    {
    return mMethodsGet(mThis(self))->MultirateBaseAddress(mThis(self), sixteenPortGroupId);
    }

uint32 Tha60290021ModuleEthTenGeBaseAddress(AtModuleEth self, uint32 groupId)
    {
    if (self)
        return mMethodsGet(mThis(self))->TenGeBaseAddress(mThis(self), groupId);
    return cInvalidUint32;
    }

eBool Tha60290021ModuleEthHasSgmiiDiagnosticLogic(Tha60290021ModuleEth self)
    {
    if (self)
        return mMethodsGet(self)->HasSgmiiDiagnosticLogic(self);
    return cAtFalse;
    }

eBool Tha60290021ModuleEthIsSgmiiDccKBytePort(AtModuleEth self, uint8 portId)
    {
    if (self)
        return IsSgmiiDccKBytePort(self, portId);
    return cAtFalse;
    }

uint8 Tha60290021ModuleEthNumSixteenPortGroup(AtModuleEth self)
    {
    return mMethodsGet(mThis(self))->NumSixteenPortGroup(mThis(self));
    }

uint32 Tha60290021ModuleEthStart40GBackplaneMacId(AtModuleEth self)
    {
    return Start40GBackplaneMacId(self);
    }

uint32 Tha60290021ModuleEthStop40GBackplaneMacId(AtModuleEth self)
    {
    return Stop40GBackplaneMacId(self);
    }

uint32 Tha60290021ModuleEthStartFaceplatePortId(AtModuleEth self)
    {
    return StartFaceplatePortId(self);
    }

uint32 Tha60290021ModuleEthStopFaceplatePortId(AtModuleEth self)
    {
    return StopFaceplatePortId(self);
    }

Tha60290021EthPortCounters Tha60290021ModuleEthFaceplatePortCountersCreate(Tha60290021ModuleEth self, AtEthPort port)
    {
    if (self)
        return mMethodsGet(self)->FaceplatePortCountersCreate(self, port);
    return NULL;
    }

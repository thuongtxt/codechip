/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290021SgmiiSerdesEthPort.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Face-plate SERDES ETH Port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../physical/Tha60290021SerdesManager.h"
#include "../pw/Tha60290021DccKbyteReg.h"
#include "Tha60290021SerdesEthPortInternal.h"
#include "Tha6029SerdesSgmiiDiagReg.h"
#include "Tha60290021DccKByteGMIIReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021SgmiiSerdesEthPort)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021SgmiiSerdesEthPortMethods m_methods;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtChannelMethods            m_AtChannelOverride;
static tAtEthPortMethods            m_AtEthPortOverride;
static tThaEthPortMethods           m_ThaEthPortOverride;

/* Save super implementation */
static const tAtChannelMethods      *m_AtChannelMethods = NULL;
static const tAtEthPortMethods      *m_AtEthPortMethods = NULL;
static const tThaEthPortMethods     *m_ThaEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool DiagnosticEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceDiagnosticModeIsEnabled(device);
    }

static AtModuleEth ModuleEth(AtEthPort self)
    {
    return (AtModuleEth) AtChannelModuleGet((AtChannel) self);
    }

static uint8 LocalId(AtEthPort self)
    {
    return Tha60290021ModuleEthSgmiiLocalId(ModuleEth(self), AtChannelIdGet((AtChannel)self));
    }

static uint32 DiagnosticOffset(AtEthPort self)
    {
    uint8 portId = LocalId(self);
    return (portId * 0x10UL);
    }

static uint32 AbsoluteOffset(AtEthPort self)
    {
    return Tha60290021ModuleEthSgmiiDiagnosticBaseAddress() + DiagnosticOffset(self);
    }

static eBool UseNewMac(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiLoopIn_Mask_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiLoopIn_Mask;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiLoopIn_Shift_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiLoopIn_Shift;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiLoopOut_Mask_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiLoopOut_Mask;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiLoopOut_Shift_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiLoopOut_Shift;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiPreNum_Mask_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiPreNum_Mask;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiPreNum_Shift_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiPreNum_Shift;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiIpgNum_Mask_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiIpgNum_Mask;
    }

static uint32 Af6_dcckgmiicfg_DcckGmiiIpgNum_Shift_Get(Tha60290021SgmiiSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6_dcckgmiicfg_DcckGmiiIpgNum_Shift;
    }

static uint32 MacAddressWithLocalAddress(AtChannel self, uint32 localAddress)
    {
    return ThaEthPortMacBaseAddress((ThaEthPort)self) + localAddress;
    }

static eAtRet NewMacDiagDefaultSet(AtEthPort self)
    {
    uint32 regAddr = MacAddressWithLocalAddress((AtChannel)self, cAf6Reg_dcckgmiidiagcfg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_dcckgmiidiagcfg_DcckGmiiDiagGenPkEn_, 0);
    mRegFieldSet(regVal, cAf6_dcckgmiidiagcfg_DcckGmiiDiagEn_, 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtRet DiagnosticDefaultSet(AtEthPort self)
    {
    uint32 regVal;
    uint32 regAddr;
    uint32 offset = AbsoluteOffset(self);

    /* Enable Test Mode */
    regAddr = cAf6Reg_control_pen1_Base + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_control_pen1_Test_enable_, 1);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* Disable BURT */
    regAddr = cAf6Reg_control_pen2_Base + offset;
    mChannelHwWrite(self, regAddr, 0x0, cAtModuleEth);

    /* MAC check, continuous sending pkt, stop all force, set default fix pattern SEQ mode */
    regAddr = cAf6Reg_control_pen3_Base + offset;
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_control_pen3_Loopout_enable_, 0);
    mRegFieldSet(regVal, cAf6_control_pen3_Mac_header_check_enable_, 0);
    mRegFieldSet(regVal, cAf6_control_pen3_Configure_test_mode_, 0); /* Continues */
    mRegFieldSet(regVal, cAf6_control_pen3_Force_error_, 0);
    mRegFieldSet(regVal, cAf6_control_pen3_Data_mode_, 5); /* Default */
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* ETH Type */
    regAddr = cAf6Reg_control_pen8_Base + offset;
    mChannelHwWrite(self, regAddr, 0x0800, cAtModuleEth);

    if (mMethodsGet(mThis(self))->UseNewMac(mThis(self)))
        NewMacDiagDefaultSet(self);

    return cAtOk;
    }

static eBool HasDiagnosticLogic(AtEthPort self)
    {
    Tha60290021ModuleEth ethModule = (Tha60290021ModuleEth)AtChannelModuleGet((AtChannel)self);
    return Tha60290021ModuleEthHasSgmiiDiagnosticLogic(ethModule);
    }

static eAtModuleEthRet DiagMacCheckingEnable(AtEthPort self, eBool enable)
    {
    uint32 regAddr = cAf6Reg_control_pen3_Base + AbsoluteOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_control_pen3_Mac_header_check_enable_, enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool DiagMacCheckingIsEnabled(AtEthPort self)
    {
    uint32 regAddr = cAf6Reg_control_pen3_Base + AbsoluteOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_control_pen3_Mac_header_check_enable_Mask) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet MacCheckingEnable(AtEthPort self, eBool enable)
    {
    if (HasDiagnosticLogic(self))
        return DiagMacCheckingEnable(self, enable);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool MacCheckingIsEnabled(AtEthPort self)
    {
    if (HasDiagnosticLogic(self))
        return DiagMacCheckingIsEnabled(self);
    return cAtFalse;
    }

static eBool MacCheckingCanEnable(AtEthPort self, eBool enable)
    {
    if (HasDiagnosticLogic(self))
        return cAtTrue;
    return enable ? cAtFalse : cAtTrue;
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed1000M) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed1000M) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeed1000M;
    }

static eAtModuleEthRet DiagMacAddressSet(AtEthPort self,
                                         uint32 msbAddressBase,
                                         uint32 lsbAddressBase,
                                         uint8 *address)
    {
    uint32 regVal;
    uint32 baseAddress = Tha60290021ModuleEthSgmiiDiagnosticBaseAddress() + DiagnosticOffset(self);

    /* Write MSB MAC */
    regVal = (uint32) (address[5] << 24);
    regVal |= (uint32) (address[4] << 16);
    regVal |= (uint32) (address[3] << 8);
    regVal |= (uint32) (address[2]);
    mChannelHwWrite(self, baseAddress + msbAddressBase, regVal, cAtModuleEth);

    /* Write LSB MAC */
    regVal = (uint32) (address[1] << 8);
    regVal |= (uint32) (address[0]);
    mChannelHwWrite(self, baseAddress + lsbAddressBase, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtModuleEthRet DiagMacAddressGet(AtEthPort self,
                                         uint32 msbAddressBase,
                                         uint32 lsbAddressBase,
                                         uint8 *address)
    {
    uint32 regVal;
    uint32 baseAddress = Tha60290021ModuleEthSgmiiDiagnosticBaseAddress() + DiagnosticOffset(self);

    /* Read MSB MAC */
    regVal = mChannelHwRead(self, baseAddress + msbAddressBase, cAtModuleEth);
    address[5] = (uint8) ((regVal & cBit31_24) >> 24);
    address[4] = (uint8) ((regVal & cBit23_16) >> 16);
    address[3] = (uint8) ((regVal & cBit15_8) >> 8);
    address[2] = (uint8) ((regVal & cBit7_0));

    /* Read MSB MAC */
    regVal = mChannelHwRead(self, baseAddress + lsbAddressBase, cAtModuleEth);
    address[1] = (uint8) ((regVal & cBit15_8) >> 8);
    address[0] = (uint8) ((regVal & cBit7_0));

    return cAtOk;
    }

static eAtModuleEthRet DiagSourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    return DiagMacAddressSet(self,
                             cAf6Reg_control_pen6_Base,
                             cAf6Reg_control_pen7_Base,
                             address);
    }

static eAtModuleEthRet DiagSourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    return DiagMacAddressGet(self,
                             cAf6Reg_control_pen6_Base,
                             cAf6Reg_control_pen7_Base,
                             address);
    }

static eAtModuleEthRet SourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    if (address == NULL)
        return cAtErrorNullPointer;

    if (HasDiagnosticLogic(self))
    return DiagSourceMacAddressSet(self, address);

    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet SourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    if (address == NULL)
        return cAtErrorNullPointer;

    if (HasDiagnosticLogic(self))
    return DiagSourceMacAddressGet(self, address);

    return cAtErrorModeNotSupport;
    }

static eBool HasSourceMac(AtEthPort self)
    {
    return HasDiagnosticLogic(self);
    }

static eAtModuleEthRet DiagDestMacAddressSet(AtEthPort self, uint8 *address)
    {
    return DiagMacAddressSet(self,
                             cAf6Reg_control_pen4_Base,
                             cAf6Reg_control_pen5_Base,
                             address);
    }

static eAtModuleEthRet DiagDestMacAddressGet(AtEthPort self, uint8 *address)
    {
    return DiagMacAddressGet(self,
                             cAf6Reg_control_pen4_Base,
                             cAf6Reg_control_pen5_Base,
                             address);
    }

static eAtModuleEthRet DestMacAddressSet(AtEthPort self, uint8 *address)
    {
    if (address == NULL)
        return cAtErrorNullPointer;

    if (HasDiagnosticLogic(self))
    return DiagDestMacAddressSet(self, address);

    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet DestMacAddressGet(AtEthPort self, uint8 *address)
    {
    if (address == NULL)
        return cAtErrorNullPointer;

    if (HasDiagnosticLogic(self))
    return DiagDestMacAddressGet(self, address);

    return cAtErrorModeNotSupport;
    }

static eBool HasDestMac(AtEthPort self)
    {
    return HasDiagnosticLogic(self);
    }

static eAtModuleEthRet IpV4AddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV4AddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV6AddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV6AddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static uint32 HwIdGet(AtChannel self)
	{
    AtUnused(self);
	return 0x0;
	}

static eAtRet PcsReset(ThaEthPort self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet HwAutoControlPhysicalEnable(ThaEthPort self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtOk;
    }

static eAtModuleEthRet AutoNegEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool AutoNegIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool AutoNegIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet DuplexModeSet(AtEthPort self, eAtEthPortDuplexMode duplexMode)
    {
    AtUnused(self);
    return (duplexMode == cAtEthPortWorkingModeFullDuplex) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortDuplexMode DuplexModeGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortWorkingModeFullDuplex;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret = AtChannelEnable(self, cAtTrue);
    if (ret != cAtOk)
        return ret;

    if (HasDiagnosticLogic((AtEthPort)self))
    return DiagnosticDefaultSet((AtEthPort) self);

    return ret;
    }

static eAtRet DiagnosticLoopOutEnable(AtEthPort self, eBool enable)
    {
    uint32 baseAddress = AbsoluteOffset(self);
    uint32 regAddr = cAf6Reg_control_pen3_Base + baseAddress;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_control_pen3_Loopout_enable_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool DiagnosticLoopOutIsEnable(AtEthPort self)
    {
    uint32 baseAddress = AbsoluteOffset(self);
    uint32 regAddr = cAf6Reg_control_pen3_Base + baseAddress;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_control_pen3_Loopout_enable_Mask) ? cAtTrue : cAtFalse;
    }

static eBool DiagnosticLoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    if ((loopbackMode == cAtLoopbackModeRelease) ||
        (loopbackMode == cAtLoopbackModeRemote))
        return cAtTrue;
    return cAtFalse;
    }

static eBool MacCanLoopback(AtChannel self)
    {
    return mMethodsGet(mThis(self))->UseNewMac(mThis(self));
    }

static eBool MacLoopbackIsSupported(uint8 loopbackMode)
    {
    switch (loopbackMode)
        {
        case cAtLoopbackModeRelease: return cAtTrue;
        case cAtLoopbackModeLocal  : return cAtTrue;
        case cAtLoopbackModeRemote : return cAtTrue;
        case cAtLoopbackModeDual   : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eAtRet MacLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    uint32 regAddr = MacAddressWithLocalAddress(self, cAf6Reg_dcckgmiicfg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 loopIn_Field_Mask = mMethodsGet(mThis(self))->Af6_dcckgmiicfg_DcckGmiiLoopIn_Mask_Get(mThis(self));
    uint32 loopIn_Field_Shift = mMethodsGet(mThis(self))->Af6_dcckgmiicfg_DcckGmiiLoopIn_Shift_Get(mThis(self));
    uint32 loopOut_Field_Mask = mMethodsGet(mThis(self))->Af6_dcckgmiicfg_DcckGmiiLoopOut_Mask_Get(mThis(self));
    uint32 loopOut_Field_Shift = mMethodsGet(mThis(self))->Af6_dcckgmiicfg_DcckGmiiLoopOut_Shift_Get(mThis(self));

    mRegFieldSet(regVal, loopIn_Field_,  (loopbackMode == cAtLoopbackModeLocal)  ? 1 : 0);
    mRegFieldSet(regVal, loopOut_Field_, (loopbackMode == cAtLoopbackModeRemote) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint8 MacLoopbackGet(AtChannel self)
    {
    uint32 regAddr = MacAddressWithLocalAddress(self, cAf6Reg_dcckgmiicfg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 loopIn_Field_Mask = mMethodsGet(mThis(self))->Af6_dcckgmiicfg_DcckGmiiLoopIn_Mask_Get(mThis(self));
    uint32 loopOut_Field_Mask = mMethodsGet(mThis(self))->Af6_dcckgmiicfg_DcckGmiiLoopOut_Mask_Get(mThis(self));

    if (regVal & loopIn_Field_Mask)
        return cAtLoopbackModeLocal;

    if (regVal & loopOut_Field_Mask)
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (MacCanLoopback(self))
        return MacLoopbackIsSupported(loopbackMode);

    if (loopbackMode == cAtLoopbackModeRelease)
        return cAtTrue;

    if (DiagnosticEnabled(self))
        return DiagnosticLoopbackIsSupported(self, loopbackMode);

    return AtSerdesControllerLoopbackIsSupported(AtEthPortSerdesController((AtEthPort)self), loopbackMode);
    }

static eAtRet LoopOutEnable(ThaEthPort self, eBool enable)
    {
    eAtRet ret = cAtOk;
    ret |= m_ThaEthPortMethods->LoopOutEnable(self, enable);
    ret |= DiagnosticLoopOutEnable((AtEthPort)self, enable);
    return ret;
    }

static eBool LoopOutIsEnabled(ThaEthPort self)
    {
    if (DiagnosticEnabled((AtChannel)self))
        return DiagnosticLoopOutIsEnable((AtEthPort) self);
    return m_ThaEthPortMethods->LoopOutIsEnabled(self);
    }

static uint8 LoopbackGet(AtChannel self)
    {
    if (DiagnosticEnabled(self))
        return DiagnosticLoopOutIsEnable((AtEthPort) self) ? cAtLoopbackModeRemote : cAtLoopbackModeRelease;

    if (MacCanLoopback(self))
        return MacLoopbackGet(self);

    return AtSerdesControllerLoopbackGet(AtEthPortSerdesController((AtEthPort)self));
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    if (DiagnosticEnabled(self))
        return m_AtChannelMethods->LoopbackSet(self, loopbackMode);

    if (MacCanLoopback(self))
        return MacLoopbackSet(self, loopbackMode);

    return AtSerdesControllerLoopbackSet(AtEthPortSerdesController((AtEthPort)self), loopbackMode);
    }

static eBool DiagnosticCounterIsSupported(AtChannel self, uint16 counterType)
    {
    eAtEthPortCounterType typeCounter = counterType;
    AtUnused(self);
    if ((typeCounter == cAtEthPortCounterTxPackets) ||
        (typeCounter == cAtEthPortCounterRxPackets) ||
        (typeCounter == cAtEthPortCounterRxErrFcsPackets))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 MacCounterAddressBase(uint16 counterType)
    {
    switch (counterType)
        {
        case cAtEthPortCounterTxPackets      : return cAf6Reg_dcckgmiitxpktcnt_Base;
        case cAtEthPortCounterTxBytes        : return cAf6Reg_dcckgmiitxbytecnt_Base;
        case cAtEthPortCounterTxErrFcsPackets: return cAf6Reg_dcckgmiitxfcserrcnt_Base;
        case cAtEthPortCounterRxPackets      : return cAf6Reg_dcckgmiirxpktcnt_Base;
        case cAtEthPortCounterRxBytes        : return cAf6Reg_dcckgmiirxbytecnt_Base;
        case cAtEthPortCounterRxErrFcsPackets: return cAf6Reg_dcckgmiirxfcserrcnt_Base;
        default:
            return cInvalidUint32;
        }
    }

static uint32 MacCounterAddress(ThaEthPort self, uint16 counterType)
    {
    uint32 regBase = MacCounterAddressBase(counterType);
    if (regBase == cInvalidUint32)
        return cInvalidUint32;
    return regBase + ThaEthPortMacBaseAddress(self);
    }

static uint32 MacCounterRead2Clear(AtChannel self, uint16 counterType, eBool r2c)
    {
    uint32 regAddr = MacCounterAddress((ThaEthPort)self, counterType);
    AtUnused(r2c);
    if (regAddr == cInvalidUint32)
        return 0x0;
    return mChannelHwRead(self, regAddr, cAtModuleEth);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (DiagnosticEnabled((AtChannel)self))
        return DiagnosticCounterIsSupported(self, counterType);

    if (mMethodsGet(mThis(self))->UseNewMac(mThis(self)))
        return (MacCounterAddressBase(counterType) == cInvalidUint32) ? cAtFalse : cAtTrue;

    return cAtFalse;
    }

static uint32 DiagnosticCounterRead(AtEthPort self, uint32 localAddress)
    {
    uint32 baseAddress = AbsoluteOffset(self);
    uint32 regAddr = localAddress + baseAddress;
    return mChannelHwRead(self, regAddr, cAtModuleEth);
    }

static uint32 DiagnosticCounterAddress(AtEthPort self, uint16 counterType, eBool r2c)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cAtEthPortCounterTxPackets      : return r2c ? cAf6Reg_counter1_Base : cAf6Reg_counter4_Base;
        case cAtEthPortCounterRxPackets      : return r2c ? cAf6Reg_counter2_Base : cAf6Reg_counter5_Base;
        case cAtEthPortCounterRxErrFcsPackets: return r2c ? cAf6Reg_counter3_Base : cAf6Reg_counter6_Base;
        default:
            return cInvalidUint32;
        }
    }

static uint32 DiagnosticCounterGet(AtChannel self, uint16 counterType, eBool r2c)
    {
    if (AtChannelCounterIsSupported(self, counterType))
        return DiagnosticCounterRead((AtEthPort) self,  DiagnosticCounterAddress((AtEthPort) self, counterType, r2c));
    return 0x0;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (DiagnosticEnabled(self))
        return DiagnosticCounterGet(self, counterType, cAtFalse);

    if (mMethodsGet(mThis(self))->UseNewMac(mThis(self)))
        return MacCounterRead2Clear(self, counterType, cAtFalse);

    return 0x0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (DiagnosticEnabled(self))
        return DiagnosticCounterGet(self, counterType, cAtTrue);

    if (mMethodsGet(mThis(self))->UseNewMac(mThis(self)))
        return MacCounterRead2Clear(self, counterType, cAtTrue);

    return 0x0;
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtSerdesManager manager = AtDeviceSerdesManagerGet(device);
    return Tha60290021SerdesManagerSgmiiSerdesControllerGet(manager, LocalId(self));
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
	return ThaEthPortSoftEnable((ThaEthPort)self, enable);
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    AtSerdesController serdes = AtEthPortSerdesController((AtEthPort)self);
    AtSerdesManager manager = AtSerdesControllerManagerGet(serdes);
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    return Tha60290021SerdesManagerSgmiiDescription(manager, serdesId, description, sizeof(description));
    }

static eBool IpgIsInRange(uint8 ipg)
    {
    return mInRange(ipg, 4, 15);
    }

static eBool TxIpgIsInRange(AtEthPort self, uint8 ipg)
    {
    AtUnused(self);
    return IpgIsInRange(ipg);
    }

static eAtModuleEthRet HwIpgSet(AtEthPort self, uint8 ipg)
    {
    uint32 regAddr = MacAddressWithLocalAddress((AtChannel)self, cAf6Reg_dcckgmiicfg_Base);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 field_Mask = mMethodsGet(mThis(self))->Af6_dcckgmiicfg_DcckGmiiIpgNum_Mask_Get(mThis(self));
    uint32 field_Shift = mMethodsGet(mThis(self))->Af6_dcckgmiicfg_DcckGmiiIpgNum_Shift_Get(mThis(self));

    mRegFieldSet(regVal, field_, ipg);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtModuleEthRet IpgSet(AtEthPort self, uint8 ipg)
    {
    if (AtEthPortTxIpgIsInRange(self, ipg))
        return HwIpgSet(self, ipg);
    return cAtErrorOutOfRangParm;
    }

static uint8 IpgGet(AtEthPort self)
    {
    uint32 regAddr = MacAddressWithLocalAddress((AtChannel)self, cAf6Reg_dcckgmiicfg_Base);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 field_Mask = mMethodsGet(mThis(self))->Af6_dcckgmiicfg_DcckGmiiIpgNum_Mask_Get(mThis(self));
    uint32 field_Shift = mMethodsGet(mThis(self))->Af6_dcckgmiicfg_DcckGmiiIpgNum_Shift_Get(mThis(self));

    return (uint8)(mRegField(regVal, field_));
    }

static eAtModuleEthRet TxIpgSet(AtEthPort self, uint8 txIpg)
    {
    if (mMethodsGet(mThis(self))->UseNewMac(mThis(self)))
        return IpgSet(self, txIpg);

    return cAtErrorModeNotSupport;
    }

static uint8 TxIpgGet(AtEthPort self)
    {
    if (mMethodsGet(mThis(self))->UseNewMac(mThis(self)))
        return IpgGet(self);

    return 0;
    }

static eBool TxIpgIsConfigurable(AtEthPort self)
    {
    return mMethodsGet(mThis(self))->UseNewMac(mThis(self));
    }

static eAtModuleEthRet RxIpgSet(AtEthPort self, uint8 rxIpg)
    {
    AtUnused(self);
    AtUnused(rxIpg);
    return cAtErrorModeNotSupport;
    }

static uint8 RxIpgGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static eBool RxIpgIsConfigurable(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    AtUnused(self);
    return (interface == cAtEthPortInterfaceSgmii) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortInterfaceSgmii;
    }

static eAtEthPortInterface DefaultInterface(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortInterfaceSgmii;
    }

static uint32 DefectGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static void ShowSticky(AtChannel self)
    {
    uint32 regAddr = MacAddressWithLocalAddress(self, cAf6Reg_dcckgmiistk_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    AtDebugger debugger = NULL;

    Tha6021DebugPrintRegName (debugger, "* DCCK GMII Datapath and Diagnostic sticky", regAddr, regVal);
    Tha6021DebugPrintErrorBit(debugger, " DCCK GMII Tx FIFO Convert Error", regVal, cAf6_dcckgmiistk_DcckGmiiTxFiFoConvErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " DCCK GMII Tx FCS Error", regVal, cAf6_dcckgmiistk_DcckGmiiTxFcsErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " DCCK GMII Tx Lost SOP", regVal, cAf6_dcckgmiistk_DcckGmiiTxLostSop_Mask);
    Tha6021DebugPrintErrorBit(debugger, " DCCK GMII Rx Preamble Error", regVal, cAf6_dcckgmiistk_DcckGmiiRxPreErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " DCCK GMII Rx SFD Error", regVal, cAf6_dcckgmiistk_DcckGmiiRxSfdErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " DCCK GMII Rx DV Error", regVal, cAf6_dcckgmiistk_DcckGmiiRxDvErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " DCCK GMII Rx FCS Error", regVal, cAf6_dcckgmiistk_DcckGmiiRxFcsErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " DCCK GMII Rx Error", regVal, cAf6_dcckgmiistk_DcckGmiiRxErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " DCCK GMII Rx FIFO Convert Error", regVal, cAf6_dcckgmiistk_DcckGmiiRxFiFoConvErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " DCCK GMII Rx Min Len Error", regVal, cAf6_dcckgmiistk_DcckGmiiRxMinLenErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " DCCK GMII Rx Max Len Error", regVal, cAf6_dcckgmiistk_DcckGmiiRxMaxLenErr_Mask);
    Tha6021DebugPrintErrorBit(debugger, " DCCK GMII Diagnostic Rx Data Error", regVal, cAf6_dcckgmiistk_DcckGmiiDiagRxDatErr_Mask);
    Tha6021DebugPrintStop(debugger);

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    }

static eAtRet Debug(AtChannel self)
    {
    /* Super */
    eAtRet ret = m_AtChannelMethods->Debug(self);
    if (ret != cAtOk)
        return ret;

    if (mMethodsGet(mThis(self))->UseNewMac(mThis(self)))
        ShowSticky(self);

    return cAtOk;
    }

static void ShowCurrentBandwidth(ThaEthPort self)
    {
    AtUnused(self);
    }

static eAtEthPortLinkStatus LinkStatusFromSerdes(eAtSerdesLinkStatus status)
    {
    switch (status)
        {
        case cAtSerdesLinkStatusDown        : return cAtEthPortLinkStatusDown;
        case cAtSerdesLinkStatusUp          : return cAtEthPortLinkStatusUp;
        case cAtSerdesLinkStatusUnknown     : return cAtEthPortLinkStatusUnknown;
        case cAtSerdesLinkStatusNotSupported: return cAtEthPortLinkStatusNotSupported;
        default:
            return cAtEthPortLinkStatusUnknown;
        }
    }

static eAtEthPortLinkStatus LinkStatus(AtEthPort self)
    {
    AtSerdesController serdes = AtEthPortSerdesController(self);
    return LinkStatusFromSerdes(AtSerdesControllerLinkStatus(serdes));
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(tAtEthPortMethods));

        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, HasDestMac);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressGet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressGet);
        mMethodOverride(m_AtEthPortOverride, MacCheckingEnable);
        mMethodOverride(m_AtEthPortOverride, MacCheckingIsEnabled);
        mMethodOverride(m_AtEthPortOverride, MacCheckingCanEnable);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, AutoNegEnable);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsEnabled);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsSupported);
        mMethodOverride(m_AtEthPortOverride, DuplexModeSet);
        mMethodOverride(m_AtEthPortOverride, DuplexModeGet);
        mMethodOverride(m_AtEthPortOverride, TxIpgIsInRange);
        mMethodOverride(m_AtEthPortOverride, TxIpgSet);
        mMethodOverride(m_AtEthPortOverride, TxIpgGet);
        mMethodOverride(m_AtEthPortOverride, TxIpgIsConfigurable);
        mMethodOverride(m_AtEthPortOverride, RxIpgSet);
        mMethodOverride(m_AtEthPortOverride, RxIpgGet);
        mMethodOverride(m_AtEthPortOverride, RxIpgIsConfigurable);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, DefaultInterface);
        mMethodOverride(m_AtEthPortOverride, LinkStatus);
        mMethodOverride(m_AtEthPortOverride, HasSourceMac);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, Debug);

        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort ethPort = (ThaEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(ethPort);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, mMethodsGet(ethPort), sizeof(m_ThaEthPortOverride));

        mMethodOverride(m_ThaEthPortOverride, LoopOutEnable);
        mMethodOverride(m_ThaEthPortOverride, LoopOutIsEnabled);
        mMethodOverride(m_ThaEthPortOverride, PcsReset);
        mMethodOverride(m_ThaEthPortOverride, HwAutoControlPhysicalEnable);
        mMethodOverride(m_ThaEthPortOverride, ShowCurrentBandwidth);
        }

    mMethodsSet(ethPort, &m_ThaEthPortOverride);
    }

static void MethodsInit(Tha60290021SgmiiSerdesEthPort self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, UseNewMac);
        mMethodOverride(m_methods, Af6_dcckgmiicfg_DcckGmiiLoopIn_Mask_Get);
        mMethodOverride(m_methods, Af6_dcckgmiicfg_DcckGmiiLoopIn_Shift_Get);
        mMethodOverride(m_methods, Af6_dcckgmiicfg_DcckGmiiLoopOut_Mask_Get);
        mMethodOverride(m_methods, Af6_dcckgmiicfg_DcckGmiiLoopOut_Shift_Get);
        mMethodOverride(m_methods, Af6_dcckgmiicfg_DcckGmiiPreNum_Mask_Get);
        mMethodOverride(m_methods, Af6_dcckgmiicfg_DcckGmiiPreNum_Shift_Get);
        mMethodOverride(m_methods, Af6_dcckgmiicfg_DcckGmiiIpgNum_Mask_Get);
        mMethodOverride(m_methods, Af6_dcckgmiicfg_DcckGmiiIpgNum_Shift_Get);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SgmiiSerdesEthPort);
    }

AtEthPort Tha60290021SgmiiSerdesEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290021SgmiiSerdesEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021SgmiiSerdesEthPortObjectInit(newPort, portId, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290021FaceplateSerdesEthPort.h
 * 
 * Created Date: Aug 8, 2017
 *
 * Description : Face-plate SERDES ETH Port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021FACEPLATESERDESETHPORT_H_
#define _THA60290021FACEPLATESERDESETHPORT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60290021FaceplateEthPortTenGeAlarmAddressWithLocalAddress(AtEthPort self, uint32 localAddress);

/* Excessive error ratio */
eAtRet Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioUpperThresholdSet(AtEthPort self, uint32 threshold);
uint32 Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioUpperThresholdGet(AtEthPort self);
uint32 Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioUpperThresholdMax(AtEthPort self);
eAtRet Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioLowerThresholdSet(AtEthPort self, uint32 threshold);
uint32 Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioLowerThresholdGet(AtEthPort self);
uint32 Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioLowerThresholdMax(AtEthPort self);
eBool Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioIsSupported(AtEthPort self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021FACEPLATESERDESETHPORT_H_ */


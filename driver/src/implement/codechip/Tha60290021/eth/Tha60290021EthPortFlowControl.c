/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290021EthPortFlowControl.c
 *
 * Created Date: Aug 11, 2016
 *
 * Description : ETH port flow control concrete code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290021EthPortFlowControlInternal.h"
#include "Tha60290021EthFlowControlReg.h"
#include "Tha60290021ModuleEth.h"
#include "Tha60290021PwEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaEthFlowControlHighThresholdValMax 0xFF
#define cThaEthFlowControlLowThresholdValMax  0xFF

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021EthPortFlowControl)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021EthPortFlowControlMethods m_methods;

/* Override */
static tAtEthFlowControlMethods m_AtEthFlowControlOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleEth EthModule(AtEthFlowControl self)
    {
    return (AtModuleEth) AtChannelModuleGet(AtEthFlowControlChannelGet(self));
    }

static uint32 OffSet(AtEthFlowControl self)
    {
    AtChannel port = AtEthFlowControlChannelGet(self);
    AtModuleEth ethModule = (AtModuleEth) AtChannelModuleGet(port);
    return (uint32) Tha60290021ModuleEthFaceplateLocalId(ethModule, (uint8) AtChannelIdGet(port));
    }

static uint32 ThresholdControlReg(AtEthFlowControl self)
    {
    return OffSet(self) + cAf6Reg_upen_cfgthreshold_Base + Tha60290021ModuleEthBypassBaseAddress();
    }

static eAtModuleEthRet HighThresholdSet(AtEthFlowControl self, uint32 threshold)
    {
	uint32 regAddr, regVal;
    AtChannel port = AtEthFlowControlChannelGet(self);

    if ((threshold > cThaEthFlowControlHighThresholdValMax) || (threshold == 0))
        return cAtErrorOutOfRangParm;

    if (threshold < AtEthFlowControlLowThresholdGet(self))
        return cAtErrorNotApplicable;

    regAddr = ThresholdControlReg(self);
    regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_cfgthreshold_val_thres2_, threshold);
    mChannelHwWrite(port, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 HighThresholdGet(AtEthFlowControl self)
    {
    AtChannel port = AtEthFlowControlChannelGet(self);
    uint32 regAddr = ThresholdControlReg(self);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_upen_cfgthreshold_val_thres2_);
    }

static eAtModuleEthRet LowThresholdSet(AtEthFlowControl self, uint32 threshold)
    {
    uint32 regAddr, regVal;
	AtChannel port = AtEthFlowControlChannelGet(self);

    if ((threshold > cThaEthFlowControlLowThresholdValMax) || (threshold == 0))
        return cAtErrorOutOfRangParm;

    if (threshold > AtEthFlowControlHighThresholdGet(self))
        return cAtErrorNotApplicable;

    regAddr = ThresholdControlReg(self);
    regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_cfgthreshold_val_thres1_, threshold);
    mChannelHwWrite(port, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 LowThresholdGet(AtEthFlowControl self)
    {
    AtChannel port = AtEthFlowControlChannelGet(self);
    uint32 regAddr = ThresholdControlReg(self);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_upen_cfgthreshold_val_thres1_);
    }

static eAtModuleEthRet Enable(AtEthFlowControl self, eBool enable)
    {
    if (enable == Tha60290021ModuleEthFlowControlIsEnabled(EthModule(self)))
        return cAtOk;

    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical,
                            AtSourceLocation, "Flow control enabling is module attribute\r\n");
    return cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtEthFlowControl self)
    {
    return Tha60290021ModuleEthFlowControlIsEnabled(EthModule(self));
    }

static eAtModuleEthRet PauseFramePeriodSet(AtEthFlowControl self, uint32 pauseFrameInterval)
    {
    AtUnused(self);
    AtUnused(pauseFrameInterval);
    return cAtErrorModeNotSupport;
    }

static uint32 PauseFramePeriodGet(AtEthFlowControl self)
    {
    AtUnused(self);
    return 0x0;
    }

static eAtModuleEthRet PauseFrameQuantaSet(AtEthFlowControl self, uint32 quanta)
    {
    AtUnused(self);
    AtUnused(quanta);
    return cAtErrorModeNotSupport;
    }

static uint32 PauseFrameQuantaGet(AtEthFlowControl self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleEthRet DestMacSet(AtEthFlowControl self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet DestMacGet(AtEthFlowControl self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet SourceMacSet(AtEthFlowControl self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet SourceMacGet(AtEthFlowControl self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet EthTypeSet(AtEthFlowControl self, uint32 ethType)
    {
    AtUnused(self);
    AtUnused(ethType);
    return cAtErrorModeNotSupport;
    }

static uint32 EthTypeGet(AtEthFlowControl self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideAtEthFlowControl(AtEthFlowControl self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthFlowControlOverride, mMethodsGet(self), sizeof(m_AtEthFlowControlOverride));

        mMethodOverride(m_AtEthFlowControlOverride, HighThresholdSet);
        mMethodOverride(m_AtEthFlowControlOverride, HighThresholdGet);
        mMethodOverride(m_AtEthFlowControlOverride, LowThresholdSet);
        mMethodOverride(m_AtEthFlowControlOverride, LowThresholdGet);
        mMethodOverride(m_AtEthFlowControlOverride, Enable);
        mMethodOverride(m_AtEthFlowControlOverride, IsEnabled);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFramePeriodSet);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFramePeriodGet);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFrameQuantaSet);
        mMethodOverride(m_AtEthFlowControlOverride, PauseFrameQuantaGet);
        mMethodOverride(m_AtEthFlowControlOverride, DestMacSet);
        mMethodOverride(m_AtEthFlowControlOverride, DestMacGet);
        mMethodOverride(m_AtEthFlowControlOverride, SourceMacSet);
        mMethodOverride(m_AtEthFlowControlOverride, SourceMacGet);
        mMethodOverride(m_AtEthFlowControlOverride, EthTypeSet);
        mMethodOverride(m_AtEthFlowControlOverride, EthTypeGet);
        }

    mMethodsSet(self, &m_AtEthFlowControlOverride);
    }

static void Override(AtEthFlowControl self)
    {
    OverrideAtEthFlowControl(self);
    }

static uint32 TxBufferLevelGet(Tha60290021EthPortFlowControl self)
    {
    AtUnused(self);
    return 0;
    }

static void MethodsInit(Tha60290021EthPortFlowControl self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, TxBufferLevelGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021EthPortFlowControl);
    }

AtEthFlowControl Tha60290021EthPortFlowControlObjectInit(AtEthFlowControl self, AtModule module, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortFlowControlObjectInit(self, module, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtEthFlowControl Tha60290021EthPortFlowControlNew(AtModule module, AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthFlowControl newFlowCtrl = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newFlowCtrl == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021EthPortFlowControlObjectInit(newFlowCtrl, module, port);
    }

uint32 Tha60290021EthPortFlowControlTxBufferLevelGet(Tha60290021EthPortFlowControl self)
    {
    if (self)
        return mMethodsGet(self)->TxBufferLevelGet(self);
    return 0;
    }

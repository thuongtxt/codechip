/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290021SerdesBackplaneEthPortInternal.h
 * 
 * Created Date: Apr 25, 2017
 *
 * Description : ETH port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021SERDESBACKPLANEETHPORTINTERNAL_H_
#define _THA60290021SERDESBACKPLANEETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/eth/AtEthPortInternal.h"
#include "Tha60290021SerdesBackplaneEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021SerdesBackplaneEthPortMethods
    {
    eBool (*CounterTxOversizeUndersizeSupported)(Tha60290021SerdesBackplaneEthPort self);
    eBool (*HasInterruptMaskRegister)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*AddressWithLocalAddress)(Tha60290021SerdesBackplaneEthPort self, uint32 localAddress);
    uint32 (*CounterLocalAddress)(Tha60290021SerdesBackplaneEthPort self, uint16 counterType);
    uint32 (*TxCfgAddress)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*RxCfgAddress)(Tha60290021SerdesBackplaneEthPort self);
    eAtModuleEthRet (*CounterTickModeSet)(Tha60290021SerdesBackplaneEthPort self, eTha602900xxEthPortCounterTickMode mode);
    eTha602900xxEthPortCounterTickMode (*CounterTickModeGet)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*Reg_ETH_CFG_TICK_REG_Base)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*ETH_RX_CFG_minlen_Mask)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*ETH_RX_CFG_minlen_Shift)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*ETH_RX_CFG_maxlen_Mask)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*ETH_RX_CFG_maxlen_Shift)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*Reg_ETH_AN_STICKY_Base)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*Reg_ETH_RX_INTEN_Base)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*Reg_ETH_TX_INTEN_Base)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*Reg_ETH_AN_INTEN_Base)(Tha60290021SerdesBackplaneEthPort self);
    eAtRet (*RxInterruptMaskSet)(Tha60290021SerdesBackplaneEthPort self, uint32 defectMask, uint32 enableMask);
    eAtRet (*TxInterruptMaskSet)(Tha60290021SerdesBackplaneEthPort self, uint32 defectMask, uint32 enableMask);
    eAtRet (*AutoNegInterruptMaskSet)(Tha60290021SerdesBackplaneEthPort self, uint32 defectMask, uint32 enableMask);
    uint32 (*RxInterruptMaskGet)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*TxInterruptMaskGet)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*AutoNegInterruptMaskGet)(Tha60290021SerdesBackplaneEthPort self);
    uint32 (*RxInterruptProcess)(AtEthPort self);
    uint32 (*TxInterruptProcess)(AtEthPort self);
    uint32 (*AutoNegInterruptProcess)(AtEthPort self);
    uint32 (*RxDefectGet)(AtEthPort self);
    uint32 (*TxDefectGet)(AtEthPort self);
    uint32 (*AutoNegDefectGet)(AtEthPort self);
    uint32 (*RxDefectHistoryRead2Clear)(AtEthPort self, eBool read2Clear);
    uint32 (*TxDefectHistoryRead2Clear)(AtEthPort self, eBool read2Clear);
    uint32 (*AutoNegHistoryRead2Clear)(AtEthPort self, eBool read2Clear);

    }tTha60290021SerdesBackplaneEthPortMethods;

typedef struct tTha60290021SerdesBackplaneEthPort
    {
    tAtEthPort super;
    const tTha60290021SerdesBackplaneEthPortMethods *methods;
    }tTha60290021SerdesBackplaneEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290021SerdesBackplaneEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021SERDESBACKPLANEETHPORTINTERNAL_H_ */


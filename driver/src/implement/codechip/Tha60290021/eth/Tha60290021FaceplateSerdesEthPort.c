/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290021FaceplateSerdesEthPort.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Face-plate SERDES ETH Port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../implement/default/man/ThaDeviceInternal.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "../physical/Tha60290021SerdesManager.h"
#include "../physical/Tha6029Physical.h"
#include "../prbs/Tha6029XfiDiagReg.h"
#include "../man/Tha60290021Device.h"
#include "Tha60290021Eth10gReg.h"
#include "Tha60290021FaceplateSerdesEthPortReg.h"
#include "Tha6029SerdesSgmiiMultirateReg.h"
#include "Tha60290021FaceplateSerdesEthPortInternal.h"
#include "Tha60290021EthPortFlowControl.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290021FaceplateSerdesEthPort)self)

/* XFI Port only support on this */
#define cTha60290021FaceplateSerdesEthPortXfi1  0
#define cTha60290021FaceplateSerdesEthPortXfi2  9

/* Counter kind */
#define cGoodCounter  0
#define cErrorCounter 1
#define cInfoCounter  2

/* TPID Insert Value */
#define cAf6_upen_enbtpid_ins_cfgtpid_Mask(index)   (cBit15_0 << ((index) * 16))
#define cAf6_upen_enbtpid_ins_cfgtpid_Shift(index)  ((index) * 16)

/* Define to get counters */
#define cAf6CounterLoMask                                           cBit31_24
#define cAf6CounterLoShift                                                 24

#define cAf6CounterHiMask                                            cBit15_0
#define cAf6CounterHiShift                                                  0

/* Central MAC */
#define cAf6_Eth_Central_Mac_SA_5_Mask                               cBit15_8
#define cAf6_Eth_Central_Mac_SA_5_Shift                                     8
#define cAf6_Eth_Central_Mac_SA_4_Mask                                cBit7_0
#define cAf6_Eth_Central_Mac_SA_4_Shift                                     0
#define cAf6_Eth_Central_Mac_SA_3_Mask                              cBit31_24
#define cAf6_Eth_Central_Mac_SA_3_Shift                                    24
#define cAf6_Eth_Central_Mac_SA_2_Mask                              cBit23_16
#define cAf6_Eth_Central_Mac_SA_2_Shift                                    16
#define cAf6_Eth_Central_Mac_SA_1_Mask                               cBit15_8
#define cAf6_Eth_Central_Mac_SA_1_Shift                                     8
#define cAf6_Eth_Central_Mac_SA_0_Mask                                cBit7_0
#define cAf6_Eth_Central_Mac_SA_0_Shift                                     0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021FaceplateSerdesEthPortMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtEthPortMethods m_AtEthPortOverride;

/* Save super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethod  = NULL;
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TpidPortMask(uint8 portId)
	{
	return (uint32)(cBit0 << (portId));
	}

static uint32 TpidPortShift(uint8 portId)
	{
	return (uint32)(portId);
	}

static uint32 cfgvlan1_prior_Mask(uint8 portId)
	{
	return (uint32)(cBit15_13 << (((portId) % 2) * 16));
	}

static uint32 cfgvlan1_prior_Shift(uint8 portId)
	{
	return (uint32)((portId % 2) ? 29 : 13);
	}

static uint32 cfgvlan1_cfi_Mask(uint8 portId)
	{
	return (uint32)(cBit12 << (((portId) % 2) * 16));
	}

static uint32 cfgvlan1_cfi_Shift(uint8 portId)
	{
	return (uint32)(((portId) % 2) ? 28 : 12);
	}

static uint32 cfgvlan1_vlanID_p1_Mask(uint8 portId)
	{
	return (uint32)(cBit11_0 << (((portId) % 2) * 16));
	}

static uint32 cfgvlan1_vlanID_p1_Shift(uint8 portId)
	{
	return (uint32)(((portId) % 2) ? 16 : 0);
	}

static uint8 LocalId(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    return Tha60290021ModuleEthFaceplateLocalId(ethModule, (uint8)AtChannelIdGet((AtChannel)self));
    }

static uint8 SixteenPortGroupId(AtEthPort self)
    {
    uint8 portId = LocalId(self);
    return portId / 16;
    }

static uint32 AddressWithLocalAddress(uint32 localAddress)
    {
    return Tha60290021ModuleEthBypassBaseAddress() + localAddress;
    }

static eAtRet TxSubportVlanInsert(AtEthPort self, eBool inserted)
    {
    uint8 portId = LocalId(self);
    uint32 regAddr = AddressWithLocalAddress(cAf6Reg_upen_enbins_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, TpidPortMask(portId), TpidPortShift(portId), inserted ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool TxSubportVlanIsInserted(AtEthPort self)
    {
    uint8 portId = LocalId(self);
    uint32 regVal = mChannelHwRead(self, AddressWithLocalAddress(cAf6Reg_upen_enbins_Base), cAtModuleEth);
    return (regVal & TpidPortMask(portId)) ? cAtTrue : cAtFalse;
    }

static uint32 MultirateAddressWithLocalAddress(AtEthPort self, uint32 localAddress)
    {
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint8 sixteenPortGroupId = Tha60290021FaceplateSerdesEthPortSixteenPortGroupId(self);

    return localAddress + Tha602900xxModuleEthMultirateBaseAddress(module, sixteenPortGroupId);
    }

static uint32 Af6Reg_ramrxepacfg1_Base(Tha60290021FaceplateSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ramrxepacfg1_Base;
    }

static eAtModuleEthRet HwMaxPacketSizeSet(AtEthPort self, uint32 maxPacketSize)
    {
    uint8 portId = LocalId(self);
    uint32 localAddress = mMethodsGet(mThis(self))->Af6Reg_ramrxepacfg1_Base(mThis(self));
    uint32 regAddr = AddressWithLocalAddress(localAddress) + portId;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ramrxepacfg1_EthPassMtuSize_, maxPacketSize);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtModuleEthRet MaxPacketSizeSet(AtEthPort self, uint32 maxPacketSize)
    {
    if (maxPacketSize > cAf6_ramrxepacfg1_EthPassMtuSize_Max)
        return cAtErrorOutOfRangParm;
    return HwMaxPacketSizeSet(self, maxPacketSize);
    }

static uint32 MaxPacketSizeGet(AtEthPort self)
    {
    uint8 portId = LocalId(self);
    uint32 localAddress = mMethodsGet(mThis(self))->Af6Reg_ramrxepacfg1_Base(mThis(self));
    uint32 regAddr = AddressWithLocalAddress(localAddress) + portId;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_ramrxepacfg1_EthPassMtuSize_);
    }

static eAtRet BypassEnable(AtEthPort self, eBool enabled)
    {
    uint8 portId = LocalId(self);
    uint32 localAddress = mMethodsGet(mThis(self))->Af6Reg_ramrxepacfg1_Base(mThis(self));
    uint32 regAddr = AddressWithLocalAddress(localAddress) + portId;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
	mRegFieldSet(regVal, cAf6_ramrxepacfg1_EthPassPortEn_, mBoolToBin(enabled));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool BypassIsEnabled(AtEthPort self)
    {
    uint8 portId = LocalId(self);
    uint32 localAddress = mMethodsGet(mThis(self))->Af6Reg_ramrxepacfg1_Base(mThis(self));
    uint32 regAddr = AddressWithLocalAddress(localAddress) + portId;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
	return mBinToBool(mRegField(regVal, cAf6_ramrxepacfg1_EthPassPortEn_));
    }

static uint32 TpidIndex(AtModuleEth self, uint16 tpid, uint16 (*TpidGet)(AtModuleEth self, uint8 tpidIndex))
    {
    uint8 tpidIndex;

    for (tpidIndex = 0; tpidIndex < Tha60290021ModuleEthNumSubPortVlanTpids(self); tpidIndex ++)
        {
        if (TpidGet(self, tpidIndex) == tpid)
            return tpidIndex;
        }

    return cInvalidUint32;
    }

static eAtRet TxTpidSet(AtEthPort self, uint16 tpid)
    {
    uint32 regAddr, regVal = 0;
    uint8 portId = LocalId(self);
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint32 tpidIndex = TpidIndex(module, tpid, Tha60290021ModuleEthSubPortVlanTransmitTpidGet);
    if (tpidIndex == cInvalidUint32)
        return cAtErrorNotApplicable;

    /* Set TPID type */
    regAddr = AddressWithLocalAddress(cAf6Reg_upen_enbinstpid_Base);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, TpidPortMask(portId), TpidPortShift(portId), tpidIndex);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    
    return cAtOk;
    }

static eAtRet ExpectedTpidSet(AtEthPort self, uint16 tpid)
    {
    uint32 regAddr;
    uint32 regVal = 0;
    uint8 portId = LocalId(self);
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint32 tpidIndex = TpidIndex(module, tpid, Tha60290021ModuleEthSubPortVlanExpectedTpidGet);
    if (tpidIndex == cInvalidUint32)
        return cAtErrorNotApplicable;

    /* Set TPID */
    regAddr = AddressWithLocalAddress(cAf6Reg_upen_enbtpid_Base);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, TpidPortMask(portId), TpidPortShift(portId), tpidIndex);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    
    return cAtOk;
    }

static uint32 Af6Reg_upen_cfginsvlan_Base(Tha60290021FaceplateSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_upen_cfginsvlan_Base;
    }

static eAtRet SubportTxVlanSet(AtEthPort self, const tAtVlan *vlan)
    {
    eAtRet ret;
    uint32 regVal = 0;
    uint8 portId = LocalId(self);
    uint32 localAddress = mMethodsGet(mThis(self))->Af6Reg_upen_cfginsvlan_Base(mThis(self));

    /* Application disable VLAN insertion by inputing NULL VLAN */
    if (vlan == NULL)
        return TxSubportVlanInsert(self, cAtFalse);

    ret = TxTpidSet(self, vlan->tpid);
    if (ret != cAtOk)
        return ret;

    /* Set VLAN tag */
    mRegFieldSet(regVal, cAf6_upen_cfginsvlan_prior_, vlan->priority);
    mRegFieldSet(regVal, cAf6_upen_cfginsvlan_cfi_, vlan->cfi);
    mRegFieldSet(regVal, cAf6_upen_cfginsvlan_vlanID_, vlan->vlanId);
    mChannelHwWrite(self, AddressWithLocalAddress(localAddress) + portId, regVal, cAtModuleEth);

    /* Enable insertion */
    return TxSubportVlanInsert(self, cAtTrue);
    }

static eAtRet SubportTxVlanGet(AtEthPort self, tAtVlan *vlan)
    {
    uint32 regVal;
    uint8 tpidIndex;
    uint8 portId = LocalId(self);
    uint32 localAddress = mMethodsGet(mThis(self))->Af6Reg_upen_cfginsvlan_Base(mThis(self));

    AtOsalMemInit(vlan, 0, sizeof(tAtVlan));

    /* Sub port VLAN has not been enabled to be inserted */
    if (!TxSubportVlanIsInserted(self))
        return cAtModuleEthErrorVlanNotExist;

    regVal = mChannelHwRead(self, AddressWithLocalAddress(localAddress) + portId, cAtModuleEth);
    vlan->priority = (uint8)mRegField(regVal, cAf6_upen_cfginsvlan_prior_);
    vlan->cfi      = (uint8)mRegField(regVal, cAf6_upen_cfginsvlan_cfi_);
    vlan->vlanId   = (uint16)mRegField(regVal, cAf6_upen_cfginsvlan_vlanID_);

    regVal = mChannelHwRead(self, AddressWithLocalAddress(cAf6Reg_upen_enbinstpid_Base), cAtModuleEth);
	mFieldGet(regVal, TpidPortMask(portId), TpidPortShift(portId), uint8, &tpidIndex);

    regVal = mChannelHwRead(self, AddressWithLocalAddress(cAf6Reg_upen_cfginstpid_Base), cAtModuleEth);
    mFieldGet(regVal, cAf6_upen_enbtpid_ins_cfgtpid_Mask(tpidIndex), cAf6_upen_enbtpid_ins_cfgtpid_Shift(tpidIndex), uint16, &vlan->tpid);
    return cAtOk;
    }

static void UpdateExpectedVlanModuleMaskBit(AtEthPort self, uint16 currentVlanId, uint16 newVlanId)
    {
    AtModuleEth module = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    Tha60290021ModuleEthUpdateExpectedVlanIdBitMask(module, currentVlanId, newVlanId);
    }

static eAtRet SubportExpectedVlanSet(AtEthPort self, const tAtVlan *expectedVlan)
    {
    eAtRet ret;
    tAtVlan currentExpectedVlan;
    uint32 regAddr, regVal = 0;
    uint8 portId = LocalId(self);

    /* Get current tag */
    Tha60290021ModuleEthPortSubportExpectedVlanGet(self, &currentExpectedVlan);

    /* Set TPID */
    ret = ExpectedTpidSet(self, expectedVlan->tpid);
    if (ret != cAtOk)
        return ret;

    /* Set VLAN tag */
    regAddr = AddressWithLocalAddress(cAf6Reg_upen_cfgvlan1_Base) + (portId / 2);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldIns(&regVal, cfgvlan1_prior_Mask(portId), cfgvlan1_prior_Shift(portId), expectedVlan->priority);
    mFieldIns(&regVal, cfgvlan1_cfi_Mask(portId), cfgvlan1_cfi_Shift(portId), expectedVlan->cfi);
    mFieldIns(&regVal, cfgvlan1_vlanID_p1_Mask(portId), cfgvlan1_vlanID_p1_Shift(portId), expectedVlan->vlanId);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    UpdateExpectedVlanModuleMaskBit(self, currentExpectedVlan.vlanId, expectedVlan->vlanId);
    return cAtOk;
    }

static eAtRet SubportExpectedVlanGet(AtEthPort self, tAtVlan *expectedVlan)
    {
    uint32 regAddr, regVal;
    uint8 tpidIndex;
    uint8 portId = LocalId(self);

    regAddr = AddressWithLocalAddress(cAf6Reg_upen_cfgvlan1_Base);
    regVal = mChannelHwRead(self, regAddr + (portId / 2), cAtModuleEth);
    mFieldGet(regVal, cfgvlan1_prior_Mask(portId), cfgvlan1_prior_Shift(portId), uint8, &expectedVlan->priority);
    mFieldGet(regVal, cfgvlan1_cfi_Mask(portId), cfgvlan1_cfi_Shift(portId), uint8, &expectedVlan->cfi);
    mFieldGet(regVal, cfgvlan1_vlanID_p1_Mask(portId), cfgvlan1_vlanID_p1_Shift(portId), uint16, &expectedVlan->vlanId);

    regAddr = AddressWithLocalAddress(cAf6Reg_upen_enbtpid_Base);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldGet(regVal, TpidPortMask(portId), TpidPortShift(portId), uint8, &tpidIndex);

    regAddr = AddressWithLocalAddress(cAf6Reg_upen_cfgrmtpid_Base);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mFieldGet(regVal, cAf6_upen_enbtpid_ins_cfgtpid_Mask(tpidIndex), cAf6_upen_enbtpid_ins_cfgtpid_Shift(tpidIndex), uint16, &expectedVlan->tpid);

    return cAtOk;
    }

static eBool IsSameWithCurrentExpectedVlanId(AtEthPort self, const tAtVlan *newExpectedVlan)
    {
    tAtVlan currentExpectedVlan;
    eAtRet ret = Tha60290021ModuleEthPortSubportExpectedVlanGet(self, &currentExpectedVlan);

    if (ret == cAtOk)
        return (currentExpectedVlan.vlanId == newExpectedVlan->vlanId) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static eBool IsExpectedVlanIdInUseByOtherPort(AtEthPort self, const tAtVlan *newExpectedVlan)
    {
    if (IsSameWithCurrentExpectedVlanId(self, newExpectedVlan))
        return cAtFalse;
    return Tha60290021ModuleEthIsExpectedVlanIdInUse((AtModuleEth)AtChannelModuleGet((AtChannel)self), newExpectedVlan->vlanId);
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    AtChannel port = (AtChannel)self;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(AtChannelDeviceGet(port));
    if (serdesManager)
        return AtSerdesManagerSerdesControllerGet(serdesManager, LocalId(self));
    return NULL;
    }

static eBool SerdesIsIn10GMode(AtEthPort self)
    {
    if (AtSerdesControllerModeGet(SerdesController(self)) == cAtSerdesModeEth10G)
        return cAtTrue;
    return cAtFalse;
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    eAtEthPortInterface interface;

    if (SerdesIsIn10GMode(self))
        return (speed == cAtEthPortSpeed10G) ? cAtTrue : cAtFalse;

    interface = AtEthPortInterfaceGet(self);
    if (interface == cAtEthPortInterface1000BaseX)
        return (speed == cAtEthPortSpeed1000M) ? cAtTrue : cAtFalse;

    if ((speed == cAtEthPortSpeed10M) ||
        (speed == cAtEthPortSpeed100M))
        return cAtTrue;

    if (speed == cAtEthPortSpeed1000M)
        return (interface == cAtEthPortInterfaceBaseFx) ? cAtFalse : cAtTrue;

    return cAtFalse;
    }

static uint32 SpeedSw2Hw(uint32 swSpeed)
    {
    switch (swSpeed)
        {
        case cAtEthPortSpeed10M:
            return 0;
        case cAtEthPortSpeed100M:
            return 1;
        case cAtEthPortSpeed1000M:
            return 2;
        default:
            return 0;
        }
    }

static uint32 SpeedHw2Sw(uint32 hwSpeed)
    {
    switch (hwSpeed)
        {
        case 0: return cAtEthPortSpeed10M;
        case 1: return cAtEthPortSpeed100M;
        case 2: return cAtEthPortSpeed1000M;
        default:
            return cAtEthPortSpeedUnknown;
        }
    }

static uint32 LocalIdForSgmiiMultiRate(AtEthPort self)
    {
    uint32 ret = LocalId(self) % 16;
    return ret;
    }

static uint32 SgmiiSpeedMask(AtEthPort self)
    {
    return cAf6_an_spd_pen0_cfg_spd00_Mask << (LocalIdForSgmiiMultiRate(self) * 2);
    }

static uint32 SgmiiSpeedShift(AtEthPort self)
    {
    return (uint32) (LocalIdForSgmiiMultiRate(self) * 2);
    }

static eAtRet SgmiiSpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtChannel port = (AtChannel)self;
    uint32 regAddr = MultirateAddressWithLocalAddress(self, cAf6Reg_an_spd_pen0_Base);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    uint32 speedMask = SgmiiSpeedMask(self);
    uint32 speedShift = SgmiiSpeedShift(self);

    mRegFieldSet(regVal, speed, SpeedSw2Hw(speed));
    mChannelHwWrite(port, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtEthPortSpeed SgmiiSpeedGet(AtEthPort self)
    {
    AtChannel port = (AtChannel)self;
    uint32 regAddr = MultirateAddressWithLocalAddress(self, cAf6Reg_an_spd_pen0_Base);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    uint32 speedMask = SgmiiSpeedMask(self);
    uint32 speedShift = SgmiiSpeedShift(self);
    return SpeedHw2Sw(mRegField(regVal, speed));
    }

static uint32 AutoNegRxAbilityGroup(AtEthPort self)
    {
    return LocalIdForSgmiiMultiRate(self) / 2;
    }

static uint32 AutoNegRxAbilityLocalIdInGroup(AtEthPort self)
    {
    return LocalIdForSgmiiMultiRate(self) % 2;
    }

static uint32 AutoNegRxAbilityRegister(AtEthPort self)
    {
    uint32 localAddress = cAf6Reg_an_rxab_pen00_Base + AutoNegRxAbilityGroup(self);
    return MultirateAddressWithLocalAddress(self, localAddress);
    }

static uint32 AutoNegStatusSpeedMask(AtEthPort self)
    {
    uint32 localId = AutoNegRxAbilityLocalIdInGroup(self);

    if (localId == 0)
        return cAf6_an_rxab_pen00_speed00_Mask;
    if (localId == 1)
        return cAf6_an_rxab_pen00_speed01_Mask;

    return cInvalidUint32;
    }

static uint32 AutoNegStatusSpeedShift(AtEthPort self)
    {
    uint32 localId = AutoNegRxAbilityLocalIdInGroup(self);

    if (localId == 0)
        return cAf6_an_rxab_pen00_speed00_Shift;
    if (localId == 1)
        return cAf6_an_rxab_pen00_speed01_Shift;

    return cInvalidUint32;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    eAtEthPortInterface interface;

    if (!AtEthPortSpeedIsSupported(self, speed))
        return cAtErrorModeNotSupport;

    if (SerdesIsIn10GMode(self))
        return (speed == cAtEthPortSpeed10G) ? cAtOk : cAtErrorModeNotSupport;

    interface = AtEthPortInterfaceGet(self);
    if (interface == cAtEthPortInterfaceBaseFx)
        return (speed == cAtEthPortSpeed100M) ? cAtOk : cAtErrorModeNotSupport;

    if (interface == cAtEthPortInterface1000BaseX)
        return (speed == cAtEthPortSpeed1000M) ? cAtOk : cAtErrorModeNotSupport;

    SgmiiSpeedSet(self, speed);

    return cAtOk;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    eAtEthPortInterface interface;

    if (SerdesIsIn10GMode(self))
        return cAtEthPortSpeed10G;

    interface = AtEthPortInterfaceGet(self);
    if (interface == cAtEthPortInterfaceBaseFx)
        return cAtEthPortSpeed100M;

    if (interface == cAtEthPortInterface1000BaseX)
        return cAtEthPortSpeed1000M;

    return SgmiiSpeedGet(self);
    }

static uint32 AutoNegDuplexModeMask(AtEthPort self)
    {
    uint32 localId = AutoNegRxAbilityLocalIdInGroup(self);

    if (localId == 0)
        return cAf6_an_rxab_pen00_duplex00_Mask;
    if (localId == 1)
        return cAf6_an_rxab_pen00_duplex01_Mask;

    return cInvalidUint32;
    }

static eAtEthPortDuplexMode AutoNegSgmiiDetectedDuplexMode(AtEthPort self)
    {
    AtChannel port = (AtChannel)self;
    uint32 regAddr = AutoNegRxAbilityRegister(self);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    return (regVal & AutoNegDuplexModeMask(self)) ? cAtEthPortWorkingModeFullDuplex : cAtEthPortWorkingModeHalfDuplex;
    }

static uint32 AutoNeg1000BasexHalfDuplexMask(AtEthPort self)
    {
    uint32 localId = AutoNegRxAbilityLocalIdInGroup(self);
    return (localId == 0) ? cAf6_an_rxab_pen00_Hduplex00_Mask : cAf6_an_rxab_pen00_Hduplex01_Mask;
    }

static uint32 AutoNeg1000BasexFullDuplexMask(AtEthPort self)
    {
    uint32 localId = AutoNegRxAbilityLocalIdInGroup(self);
    return (localId == 0) ? cAf6_an_rxab_pen00_Fduplex00_Mask : cAf6_an_rxab_pen00_Fduplex01_Mask;
    }

static eAtEthPortDuplexMode AutoNeg1000BasexDetectedDuplexMode(AtEthPort self)
    {
    uint32 regAddr = AutoNegRxAbilityRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    eAtEthPortDuplexMode duplexMode = cAtEthPortWorkingModeNone;

    if (regVal & AutoNeg1000BasexHalfDuplexMask(self))
        duplexMode |= cAtEthPortWorkingModeHalfDuplex;
    if (regVal & AutoNeg1000BasexFullDuplexMask(self))
        duplexMode |= cAtEthPortWorkingModeFullDuplex;

    return duplexMode;
    }

static eAtEthPortDuplexMode DuplexModeGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortWorkingModeFullDuplex;
    }

static uint32 InterfaceSw2Hw(uint32 swInterface)
    {
    switch (swInterface)
        {
        case cAtEthPortInterface1000BaseX: return 0;
        case cAtEthPortInterfaceSgmii:     return 1;
        default:
            return cInvalidUint32;
        }
    }

static uint32 InterfaceHw2Sw(uint32 hwInterface)
    {
    switch (hwInterface)
        {
        case 0: return cAtEthPortInterface1000BaseX;
        case 1: return cAtEthPortInterfaceSgmii;
        default:
            return cAtEthPortInterfaceUnknown;
        }
    }

static uint32 AutoNegInterfaceMask(AtEthPort self)
    {
    return (cBit0 << LocalIdForSgmiiMultiRate(self));
    }

static uint32 AutoNegInterfaceShift(AtEthPort self)
    {
    return LocalIdForSgmiiMultiRate(self);
    }

static eAtModuleEthRet HwInterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress(self, cAf6Reg_an_mod_pen0_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 txPortModeMask = AutoNegInterfaceMask(self);
    uint32 txPortModeShift = AutoNegInterfaceShift(self);
    mRegFieldSet(regVal, txPortMode, InterfaceSw2Hw(interface));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtEthPortInterface HwInterfaceGet(AtEthPort self)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress(self, cAf6Reg_an_mod_pen0_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 txPortModeMask = AutoNegInterfaceMask(self);
    uint32 txPortModeShift = AutoNegInterfaceShift(self);
    return InterfaceHw2Sw(mRegField(regVal, txPortMode));
    }

static eBool SerdesIsInBaseFxMode(AtEthPort self)
    {
    eAtSerdesMode mode = AtSerdesControllerModeGet(SerdesController(self));

    if (mode == cAtSerdesModeEth100M)
        return cAtTrue;

    return cAtFalse;
    }

static eBool InterfaceIsSupported(AtEthPort self, eAtEthPortInterface interface)
    {
    if (SerdesIsIn10GMode(self))
        return (interface == cAtEthPortInterfaceBaseR) ? cAtTrue : cAtFalse;

    if (SerdesIsInBaseFxMode(self))
        return (interface == cAtEthPortInterfaceBaseFx) ? cAtTrue : cAtFalse;

    if (InterfaceSw2Hw(interface) == cInvalidUint32)
        return cAtFalse;

    return cAtTrue;
    }

static ThaVersionReader VersionReader(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return ThaDeviceVersionReader(device);
    }

static eBool ShouldOpenFeatureWithVersionNumber(AtChannel self, uint32 startVersionHasThis)
    {
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtChannel)self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool ShouldOpenFeatureWithBuiltDetail(AtChannel self, uint32 major, uint32 minor, uint32 betaBuild)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(major, minor, betaBuild);
    return ShouldOpenFeatureWithVersionNumber(self, startVersionHasThis);
    }

static eBool Interface100BaseFxIsSupported(Tha60290021FaceplateSerdesEthPort self)
    {
    return ShouldOpenFeatureWithBuiltDetail((AtChannel)self, 0x1, 0x8, 0x0);
    }

static eAtRet Interface100BaseFxEnable(AtChannel self, eBool enabled)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress((AtEthPort)self, cAf6Reg_fx_txenb_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 enabledShift = LocalIdForSgmiiMultiRate((AtEthPort)self);
    uint32 enabledMask = cBit0 << enabledShift;
    mRegFieldSet(regVal, enabled, enabled);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool Interface100BaseFxIsEnabled(AtChannel self)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress((AtEthPort)self, cAf6Reg_fx_txenb_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 enabledMask = cBit0 << LocalIdForSgmiiMultiRate((AtEthPort)self);
    return (regVal & enabledMask) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    if (!InterfaceIsSupported(self, interface))
        return cAtErrorModeNotSupport;

    if (SerdesIsIn10GMode(self))
        return (interface == cAtEthPortInterfaceBaseR) ? cAtOk : cAtErrorModeNotSupport;

    if (SerdesIsInBaseFxMode(self))
        return (interface == cAtEthPortInterfaceBaseFx) ? cAtOk : cAtErrorModeNotSupport;

    return HwInterfaceSet(self, interface);
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    if (SerdesIsIn10GMode(self))
        return cAtEthPortInterfaceBaseR;

    if (SerdesIsInBaseFxMode(self))
        return cAtEthPortInterfaceBaseFx;

    return HwInterfaceGet(self);
    }

static eBool IsVlanValid(AtEthPort self, const tAtVlan *expectedVlan)
    {
    AtUnused(self);
    if (expectedVlan->vlanId > 4095)
        return cAtFalse;

    if (expectedVlan->priority > 7)
        return cAtFalse;

    if (expectedVlan->cfi > 1)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 Af6Reg_ramrxepacfg0_Base(Tha60290021FaceplateSerdesEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ramrxepacfg0_Base;
    }

static uint32 SourceMacReg(AtEthPort self)
    {
    uint32 localAddress = mMethodsGet(mThis(self))->Af6Reg_ramrxepacfg0_Base(mThis(self));
    return AddressWithLocalAddress(localAddress) + LocalId(self);
    }

static eAtModuleEthRet SourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr =  SourceMacReg(self);
    mChannelHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleEth);
    mRegFieldSet(longRegVal[1], cAf6_Eth_Central_Mac_SA_5_, address[0]);
    mRegFieldSet(longRegVal[1], cAf6_Eth_Central_Mac_SA_4_, address[1]);
    mRegFieldSet(longRegVal[0], cAf6_Eth_Central_Mac_SA_3_, address[2]);
    mRegFieldSet(longRegVal[0], cAf6_Eth_Central_Mac_SA_2_, address[3]);
    mRegFieldSet(longRegVal[0], cAf6_Eth_Central_Mac_SA_1_, address[4]);
    mRegFieldSet(longRegVal[0], cAf6_Eth_Central_Mac_SA_0_, address[5]);
    mChannelHwLongWrite(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleEth);
    return cAtOk;
    }

static eAtModuleEthRet SourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr =  SourceMacReg(self);
    mChannelHwLongRead(self, regAddr, longRegVal, cThaLongRegMaxSize, cAtModuleEth);
    address[0] = (uint8)mRegField(longRegVal[1], cAf6_Eth_Central_Mac_SA_5_);
    address[1] = (uint8)mRegField(longRegVal[1], cAf6_Eth_Central_Mac_SA_4_);
    address[2] = (uint8)mRegField(longRegVal[0], cAf6_Eth_Central_Mac_SA_3_);
    address[3] = (uint8)mRegField(longRegVal[0], cAf6_Eth_Central_Mac_SA_2_);
    address[4] = (uint8)mRegField(longRegVal[0], cAf6_Eth_Central_Mac_SA_1_);
    address[5] = (uint8)mRegField(longRegVal[0], cAf6_Eth_Central_Mac_SA_0_);
    return cAtOk;
    }

static eAtModuleEthRet DestMacAddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet DestMacAddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eBool HasDestMac(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet IpV4AddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV4AddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV6AddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet IpV6AddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static uint32 AutoNegResetMask(AtEthPort self)
    {
    return cBit0 << LocalIdForSgmiiMultiRate(self);
    }

static uint32 AutoNegResetShift(AtEthPort self)
    {
    return LocalIdForSgmiiMultiRate(self);
    }

static uint32 AutoNegEnableMask(AtEthPort self)
    {
    return cBit0 << LocalIdForSgmiiMultiRate(self);
    }

static uint32 AutoNegEnableShift(AtEthPort self)
    {
    return LocalIdForSgmiiMultiRate(self);
    }

static eAtRet HwAutoNegEnable(AtEthPort self, eBool enable)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress(self, cAf6Reg_an_enb_pen0_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 autoNegEnableMask = AutoNegEnableMask(self);
    uint32 autoNegEnableShift = AutoNegEnableShift(self);

    mRegFieldSet(regVal, autoNegEnable, enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    AtEthPortAutoNegRestartOn(self, cAtTrue);
    AtEthPortAutoNegRestartOn(self, cAtFalse);

    return cAtOk;
    }

static eBool HwAutoNegIsEnabled(AtEthPort self)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress(self, cAf6Reg_an_enb_pen0_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 autoNegEnableMask = AutoNegEnableMask(self);
    return (regVal & autoNegEnableMask) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet AutoNegEnable(AtEthPort self, eBool enable)
    {
    return HwAutoNegEnable(self, enable);
    }

static eBool AutoNegIsEnabled(AtEthPort self)
    {
    return HwAutoNegIsEnabled(self);
    }

static eAtEthPortAutoNegState AutoNegStateHw2Sw(uint32 hwState)
    {
    switch (hwState)
        {
        case 0:  return cAtEthPortAutoNegEnable;
        case 1:  return cAtEthPortAutoNegRestart;
        case 2:  return cAtEthPortAutoNegDisableLinkOk;
        case 3:  return cAtEthPortAutoNegAbilityDetect;
        case 4:  return cAtEthPortAutoNegAckDetect;
        case 5:  return cAtEthPortAutoNegCompleteAck;
        case 6:  return cAtEthPortAutoNegNextPageWait;
        case 7:  return cAtEthPortAutoNegIdleDetect;
        case 8:  return cAtEthPortAutoNegLinkOk;
        default: return cAtEthPortAutoNegInvalid;
        }
    }

static uint32 Faceplate8PortGroupIn32PortGroup(AtEthPort self)
    {
    return LocalId(self) / 8;
    }

static uint32 Faceplate8PortGroupIn16PortGroup(AtEthPort self)
    {
    return LocalIdForSgmiiMultiRate(self) / 8;
    }

static uint32 FaceplateLocalIdInGroup(AtEthPort self)
    {
    return LocalIdForSgmiiMultiRate(self) % 8;
    }

static uint32 AutoNegStateMask(AtEthPort self)
    {
    return cAf6_an_sta_pen00_an_sta00_Mask << (FaceplateLocalIdInGroup(self) * 4);
    }

static uint32 AutoNegStateShift(AtEthPort self)
    {
    return FaceplateLocalIdInGroup(self) * 4;
    }

static uint32 AutoNegStateRegister(AtEthPort self)
    {
    uint32 localAddress = cAf6Reg_an_sta_pen00_Base + Faceplate8PortGroupIn16PortGroup(self);
    return MultirateAddressWithLocalAddress(self, localAddress);
    }

static eAtEthPortAutoNegState AutoNegStateGet(AtEthPort self)
    {
    uint32 regAddr = AutoNegStateRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 autoNegStateMask = AutoNegStateMask(self);
    uint32 autoNegStateShift = AutoNegStateShift(self);
    uint32 hwState = mRegField(regVal, autoNegState);

    return AutoNegStateHw2Sw(hwState);
    }

static eBool AutoNegIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 DiagXfiBaseAddress(AtEthPort self)
    {
    AtSerdesController serdes = AtEthPortSerdesController(self);
    return Tha60290021SerdesManagerXfiDiagnosticBaseAddress(serdes);
    }

static uint32 DiagXfiCounterAddress(AtEthPort self, uint16 counterType, eBool r2c)
    {
    eBool ro = r2c ? cAtFalse : cAtTrue; /* Just to easily map to RD which specify RO counters first */

    AtUnused(self);

    switch (counterType)
        {
        case cAtEthPortCounterTxPackets            : return ro ? cAf6Reg_Status_register_1_Base  : cAf6Reg_Status_register_2_Base;
        case cAtEthPortCounterRxPackets            : return ro ? cAf6Reg_Status_register_3_Base  : cAf6Reg_Status_register_14_Base;
        case cAtEthPortCounterRxErrFcsPackets      : return ro ? cAf6Reg_Status_register_5_Base  : cAf6Reg_Status_register_16_Base;
        case cAtEthPortCounterRxPhysicalError      : return ro ? cAf6Reg_Status_register_6_Base  : cAf6Reg_Status_register_17_Base;
        case cAtEthPortCounterRxPacketsLen64       : return ro ? cAf6Reg_Status_register_8_Base  : cAf6Reg_Status_register_19_Base;
        case cAtEthPortCounterRxPacketsLen65_127   : return ro ? cAf6Reg_Status_register_9_Base  : cAf6Reg_Status_register_20_Base;
        case cAtEthPortCounterTxPacketsLen128_255  : return ro ? cAf6Reg_Status_register_10_Base : cAf6Reg_Status_register_21_Base;
        case cAtEthPortCounterTxPacketsLen256_511  : return ro ? cAf6Reg_Status_register_11_Base : cAf6Reg_Status_register_22_Base;
        case cAtEthPortCounterTxPacketsLen512_1023 : return ro ? cAf6Reg_Status_register_12_Base : cAf6Reg_Status_register_23_Base;
        case cAtEthPortCounterTxPacketsLen1529_2047: return ro ? cAf6Reg_Status_register_13_Base : cAf6Reg_Status_register_24_Base;

        default:
            return cInvalidUint32;
        }
    }

static eBool FaceplateCounterIsSupported(AtChannel self, uint32 counterType)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtEthPortCounterTxPackets                 :return cAtTrue;
        case cAtEthPortCounterTxPacketsLen64            :return cAtTrue;
        case cAtEthPortCounterTxOversizePackets         :return cAtTrue;
        case cAtEthPortCounterTxBytes                   :return cAtTrue;
        case cAtEthPortCounterTxDiscardedPackets        :return cAtTrue;
        case cAtEthPortCounterTxUndersizePackets        :return cAtTrue;
        case cAtEthPortCounterTxPacketsLen65_127        :return cAtTrue;
        case cAtEthPortCounterTxPacketsLen256_511       :return cAtTrue;
        case cAtEthPortCounterTxPacketsLen1024_1518     :return cAtTrue;
        case cAtEthPortCounterTxPacketsLen128_255       :return cAtTrue;
        case cAtEthPortCounterTxPacketsLen512_1023      :return cAtTrue;
        case cAtEthPortCounterTxPacketsJumbo            :return cAtTrue;
        case cAtEthPortCounterTxPausePackets            :return cAtTrue;
        case cAtEthPortCounterTxBrdCastPackets          :return cAtTrue;
        case cAtEthPortCounterTxOverFlowDroppedPackets  :return cAtTrue;
        case cAtEthPortCounterTxMultCastPackets         :return cAtTrue;
        case cAtEthPortCounterRxPacketsLen65_127        :return cAtTrue;
        case cAtEthPortCounterRxPacketsLen256_511       :return cAtTrue;
        case cAtEthPortCounterRxPacketsLen1024_1518     :return cAtTrue;
        case cAtEthPortCounterRxPacketsLen128_255       :return cAtTrue;
        case cAtEthPortCounterRxPacketsLen512_1023      :return cAtTrue;
        case cAtEthPortCounterRxPacketsJumbo            :return cAtTrue;
        case cAtEthPortCounterRxBytes                   :return cAtTrue;
        case cAtEthPortCounterRxErrFcsPackets           :return cAtTrue;
        case cAtEthPortCounterRxDiscardedPackets        :return cAtTrue;
        case cAtEthPortCounterRxPackets                 :return cAtTrue;
        case cAtEthPortCounterRxPacketsLen64            :return cAtTrue;
        case cAtEthPortCounterRxLoopDaPackets           :return cAtTrue;
        case cAtEthPortCounterRxPcsErrorPackets         :return cAtFalse;
        case cAtEthPortCounterRxPcsInvalidCount         :return cAtTrue;
        case cAtEthPortCounterRxFragmentPackets         :return cAtTrue;
        case cAtEthPortCounterRxUndersizePackets        :return cAtTrue;
        case cAtEthPortCounterRxJabberPackets           :return cAtTrue;
        case cAtEthPortCounterRxPausePackets            :return cAtTrue;
        case cAtEthPortCounterRxOversizePackets         :return cAtTrue;
        case cAtEthPortCounterRxMultCastPackets         :return cAtTrue;
        case cAtEthPortCounterRxOverFlowDroppedPackets  :return cAtTrue;
        case cAtEthPortCounterRxBrdCastPackets          :return cAtTrue;

        default: return cAtFalse;
        }

    return cAtFalse;
    }

static eBool DiagnosticEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceDiagnosticModeIsEnabled(device);
    }

static eBool DiagnosticCounterIsSupported(AtEthPort self, uint16 counterType)
    {
    /* 1G and 100M are still not ready for now */
    if (!SerdesIsIn10GMode(self))
        return cAtFalse;

    if (DiagXfiCounterAddress(self, counterType, cAtTrue) == cInvalidUint32)
        return cAtFalse;
    return cAtTrue;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (DiagnosticEnabled(self))
        return DiagnosticCounterIsSupported((AtEthPort)self, counterType);

    return FaceplateCounterIsSupported(self, counterType);
    }

static uint32 DiagXfiCounterRead2Clear(AtEthPort self, uint16 counterType, eBool r2c)
    {
    uint32 regAddr = DiagXfiCounterAddress(self, counterType, r2c) + DiagXfiBaseAddress(self);
    return mChannelHwRead(self, regAddr, cAtModuleEth);
    }

static Tha60290021EthPortCounters CountersCreate(AtChannel self)
    {
    Tha60290021ModuleEth ethModule = (Tha60290021ModuleEth)AtChannelModuleGet(self);
    return Tha60290021ModuleEthFaceplatePortCountersCreate(ethModule, (AtEthPort)self);
    }

static Tha60290021EthPortCounters CountersGet(AtChannel self)
    {
    if (mThis(self)->counters == NULL)
        mThis(self)->counters = CountersCreate(self);
    return mThis(self)->counters;
    }

static eAtRet AllCountersLatchAndClear(AtChannel self, eBool clear)
    {
    return Tha60290021EthPortAllCountersLatchAndClear(CountersGet(self), clear);
    }

static uint32 CounterRead2Clear(AtChannel self, uint16 counterType, eBool r2c)
    {
    AtEthPort port = (AtEthPort)self;

    if (DiagnosticEnabled(self) && SerdesIsIn10GMode(port))
        return DiagXfiCounterRead2Clear(port, counterType, r2c);

    if (Tha60290021ModuleEthIsFaceplatePort(port))
        return Tha60290021EthPortCounterRead2Clear(CountersGet(self), counterType, r2c);

    return 0;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static eAtRet AllCountersRead2Clear(AtChannel self, void *pAllCounters, eBool clear)
    {
    if (pAllCounters)
        AtOsalMemInit(pAllCounters, 0, sizeof(tAtEthPortCounters));

    return Tha60290021EthPortAllCountersRead2Clear(CountersGet(self), pAllCounters, clear);
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
	{
	return AllCountersRead2Clear(self, pAllCounters, cAtFalse);
	}

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
	{
	return AllCountersRead2Clear(self, pAllCounters, cAtTrue);
	}

static eAtSevLevel CounterColor(uint32 counterValue, uint32 counterKind)
    {
    switch (counterKind)
        {
        case cGoodCounter : return counterValue ? cSevInfo : cSevCritical;
        case cErrorCounter: return counterValue ? cSevCritical : cSevNormal;
        case cInfoCounter : return counterValue ? cSevInfo : cSevNormal;
        default:
            return cSevNormal;
        }
    }

static void DiagXfiCounterShow(AtEthPort self, const char *title, uint32 localAddress, uint32 counterKind)
    {
    uint32 counterValue = mChannelHwRead(self, localAddress + DiagXfiBaseAddress(self), cAtModuleEth);
    eAtSevLevel color = CounterColor(counterValue, counterKind);
    AtPrintc(color, "%s%d\r\n", title, counterValue);
    }

static void DiagTenGeCountersDetailShow(AtEthPort self)
    {
    AtPrintc(cSevNormal, "* XFI Counter detail:\r\n");
    DiagXfiCounterShow(self, "  - TX pkttotal        : ", cAf6Reg_Status_register_2_Base, cGoodCounter);
    DiagXfiCounterShow(self, "  - RX pkttotal        : ", cAf6Reg_Status_register_14_Base, cGoodCounter);
    DiagXfiCounterShow(self, "  - RX pktgood         : ", cAf6Reg_Status_register_15_Base, cGoodCounter);
    DiagXfiCounterShow(self, "  - RX pktfcserr       : ", cAf6Reg_Status_register_16_Base, cErrorCounter);
    DiagXfiCounterShow(self, "  - RX pktlengtherr    : ", cAf6Reg_Status_register_17_Base, cErrorCounter);
    DiagXfiCounterShow(self, "  - RX pktdaterr       : ", cAf6Reg_Status_register_18_Base, cErrorCounter);
    DiagXfiCounterShow(self, "  - RX pktlen0-64      : ", cAf6Reg_Status_register_19_Base, cInfoCounter);
    DiagXfiCounterShow(self, "  - RX pktlen65-128    : ", cAf6Reg_Status_register_20_Base, cInfoCounter);
    DiagXfiCounterShow(self, "  - RX pktlen129-256   : ", cAf6Reg_Status_register_21_Base, cInfoCounter);
    DiagXfiCounterShow(self, "  - RX pktlen257-512   : ", cAf6Reg_Status_register_22_Base, cInfoCounter);
    DiagXfiCounterShow(self, "  - RX pktlen513-1024  : ", cAf6Reg_Status_register_23_Base, cInfoCounter);
    DiagXfiCounterShow(self, "  - RX pktlen1025-2048 : ", cAf6Reg_Status_register_24_Base, cInfoCounter);
    }

static uint32 TenGeLossDataOrFrameSyncMask(AtEthPort self)
	{
	return (LocalId(self) == 0) ? cBit0 : cBit1;/*FIXME: for 40G ETH PASSTHROUGH*/
	}

static uint32 TenGeExErrorRatioMask(AtEthPort self)
	{
	return (LocalId(self) == 0) ? cBit2 : cBit3;
	}

static uint32 TenGeClockLossMask(AtEthPort self)
	{
	return (LocalId(self) == 0) ? cBit4 : cBit5;
	}

static uint32 TenGeClockOutRangeMask(AtEthPort self)
	{
	return (LocalId(self) == 0) ? cBit6 : cBit7;
	}

static uint32 TenGeLocalFaultMask(AtEthPort self)
	{
	return (LocalId(self) == 0) ? cBit8 : cBit9;
	}

static uint32 TenGeRemoteFaultMask(AtEthPort self)
	{
	return (LocalId(self) == 0) ? cBit10 : cBit11;
	}

static eBool ShouldGetTenGeLfRfFromEthPassThroughModule(void)
    {
    /* Will remove this once LF/RF testing is completed */
    return cAtFalse;
    }

static uint32 TenGeEthPassThroughDefectRead2ClearWithAddress(AtEthPort self, uint32 regAddr, eBool read2Clear)
    {
    uint32 swAlarm = 0;
    uint32 alarmMask;
    uint32 hwAlarmClearBitMask = 0;
    uint32 hwAlarm = mChannelHwRead(self, regAddr, cAtModuleEth);

    alarmMask = TenGeClockLossMask(self);
    if (hwAlarm & alarmMask)
        {
        swAlarm |= cAtEthPortAlarmLossofClock;
        hwAlarmClearBitMask |= alarmMask;
        }

    alarmMask = TenGeExErrorRatioMask(self);
    if (hwAlarm & alarmMask)
        {
        swAlarm |= cAtEthPortAlarmExcessiveErrorRatio;
        hwAlarmClearBitMask |= alarmMask;
        }

    alarmMask = TenGeLossDataOrFrameSyncMask(self);
    if (hwAlarm & alarmMask)
        {
        swAlarm |= cAtEthPortAlarmLossofDataSync;
        hwAlarmClearBitMask |= alarmMask;
        }

    alarmMask = TenGeClockOutRangeMask(self);
    if (hwAlarm & alarmMask)
        {
        swAlarm |= cAtEthPortAlarmFrequencyOutofRange;
        hwAlarmClearBitMask |= alarmMask;
        }

    if (ShouldGetTenGeLfRfFromEthPassThroughModule())
        {
        alarmMask = TenGeLocalFaultMask(self);
        if (hwAlarm & alarmMask)
            {
            swAlarm |= cAtEthPortAlarmLocalFault;
            hwAlarmClearBitMask |= alarmMask;
            }

        alarmMask = TenGeRemoteFaultMask(self);
        if (hwAlarm & alarmMask)
            {
            swAlarm |= cAtEthPortAlarmRemoteFault;
            hwAlarmClearBitMask |= alarmMask;
            }
        }

    /* Clear Alarm sticky */
    if (read2Clear)
        mChannelHwWrite(self, regAddr, hwAlarmClearBitMask, cAtModuleEth);

    return swAlarm;
    }

static uint32 TenGeBaseAddress(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    return Tha60290021ModuleEthTenGeBaseAddress(ethModule, Faceplate8PortGroupIn32PortGroup(self));
    }

static uint32 TenGeAlarmAddressWithLocalAddress(AtEthPort self, uint32 localAddress)
    {
    return TenGeBaseAddress(self) + localAddress;
    }

static uint32 TenGePassThroughDefectHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    uint32 regAddr = AddressWithLocalAddress(cAf6Reg_upen_epatengestk0_Base);
    return TenGeEthPassThroughDefectRead2ClearWithAddress(self, regAddr, read2Clear);
    }

static uint32 GeExErrorRatioMask(AtEthPort self)
	{
	return (uint32) (cBit16 << LocalId(self));
	}

static uint32 OneGeEthPassThroughDefectRead2ClearWithAddress(AtEthPort self, uint32 regAddr, eBool read2Clear)
    {
    uint32 swAlarm = 0;
    uint32 alarmMask;
    uint32 hwAlarmClearBitMask = 0;
    uint32 hwAlarm = mChannelHwRead(self, regAddr, cAtModuleEth);

    alarmMask = GeExErrorRatioMask(self);
    if (hwAlarm & alarmMask)
        {
        swAlarm |= cAtEthPortAlarmExcessiveErrorRatio;
        hwAlarmClearBitMask |= alarmMask;
        }

    /* Clear Alarm sticky */
    if (read2Clear)
        mChannelHwWrite(self, regAddr, hwAlarmClearBitMask, cAtModuleEth);

    return swAlarm;
    }

static uint32 OneGePassThroughDefectHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    uint32 regAddr = AddressWithLocalAddress(cAf6Reg_upen_epagestk0_Base);
    return OneGeEthPassThroughDefectRead2ClearWithAddress(self, regAddr, read2Clear);
    }

static uint32 PassThroughDefectHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    if (SerdesIsIn10GMode(self))
        return TenGePassThroughDefectHistoryRead2Clear(self, read2Clear);
    else
        return OneGePassThroughDefectHistoryRead2Clear(self, read2Clear);
    }

static uint32 AutoNegLinkStatusMask(AtEthPort self)
    {
    uint32 localId = AutoNegRxAbilityLocalIdInGroup(self);

    if (localId == 0)
        return cAf6_an_rxab_pen00_link00_Mask;
    if (localId == 1)
        return cAf6_an_rxab_pen00_link01_Mask;

    return cInvalidUint32;
    }

static eAtEthPortLinkStatus AutoNegSgmiiAbilityLinkStatusGet(AtEthPort self)
    {
    uint32 regAddr = AutoNegRxAbilityRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & AutoNegLinkStatusMask(self)) ? cAtEthPortLinkStatusUp : cAtEthPortLinkStatusDown;
    }

static eAtSevLevel LinkStatusColor(eAtEthPortLinkStatus linkStatus)
    {
    switch (linkStatus)
        {
        case cAtEthPortLinkStatusDown        : return cSevCritical;
        case cAtEthPortLinkStatusUp          : return cSevInfo;
        case cAtEthPortLinkStatusNotSupported: return cSevNormal;
        case cAtEthPortLinkStatusUnknown:
        default:
            return cSevWarning;
        }
    }

static const char *LinkStatusString(eAtEthPortLinkStatus linkStatus)
    {
    switch (linkStatus)
        {
        case cAtEthPortLinkStatusDown        : return "down";
        case cAtEthPortLinkStatusUp          : return "up";
        case cAtEthPortLinkStatusNotSupported: return "Not supported";
        case cAtEthPortLinkStatusUnknown:
        default:
            return "unknown";
        }
    }

static void DisplayBitValue(const char *title, uint32 bitVal)
    {
    AtPrintc(cSevNormal, "%s: ", title);
    AtPrintc(bitVal ? cSevCritical : cSevInfo, "%s\r\n", bitVal ? "set" : "clear");
    }

static eAtRet AutoNegDebug(AtEthPort self)
    {
    if (AtEthPortInterfaceGet(self) == cAtEthPortInterfaceSgmii)
        {
        eAtEthPortLinkStatus linkStatus = AutoNegSgmiiAbilityLinkStatusGet(self);
        eAtSevLevel color = LinkStatusColor(linkStatus);
        AtPrintc(cSevNormal, "* SGMII ability link: ");
        AtPrintc(color, "%s\r\n", LinkStatusString(linkStatus));
        }

    if (AtEthPortInterfaceGet(self) == cAtEthPortInterface1000BaseX)
        {
        uint32 regAddr = AutoNegRxAbilityRegister(self);
        uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
        uint32 localId = AutoNegRxAbilityLocalIdInGroup(self);
        uint32 fieldMask;

        AtPrintc(cSevNormal, "* 1000BaseX ability: \r\n");

        fieldMask = (localId == 0) ? cAf6_an_rxab_pen00_NP00_Mask : cAf6_an_rxab_pen00_NP01_Mask;
        DisplayBitValue("  * Next page", regVal & fieldMask);

        fieldMask = (localId == 0) ? cAf6_an_rxab_pen00_Ack00_Mask : cAf6_an_rxab_pen00_Ack01_Mask;
        DisplayBitValue("  * ACK      ", regVal & fieldMask);
        }

    return cAtOk;
    }

static void TenGeStatusDebug(AtEthPort self)
    {
    uint32 regAddr, regVal;
    uint32 stickyStatus, currentStatus;

    regAddr = TenGeAlarmAddressWithLocalAddress(self, cAf6Reg_eth10g_link_fault_sticky_Base);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    stickyStatus = regVal & cAf6_eth10g_link_fault_sticky_Eth10gInterrptionStk_Mask;
    mChannelHwWrite(self, regAddr, cAf6_eth10g_link_fault_sticky_Eth10gInterrptionStk_Mask, cAtModuleEth);

    regAddr = TenGeAlarmAddressWithLocalAddress(self, cAf6Reg_eth10g_link_fault_Cur_Sta_Base);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    currentStatus = regVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gInterrptionCur_Mask;

    AtPrintc(cSevNormal, "* Status:\r\n");
    AtPrintc(cSevNormal, "  * Interruption sticky: "); AtPrintc(stickyStatus  ? cSevCritical : cSevInfo, "%s\r\n", stickyStatus  ? "set" : "clear");
    AtPrintc(cSevNormal, "  * Interruption status: "); AtPrintc(currentStatus ? cSevCritical : cSevInfo, "%s\r\n", currentStatus ? "set" : "clear");
    }

static eAtRet TenGeDebug(AtEthPort self)
    {
    DiagTenGeCountersDetailShow(self);
    TenGeStatusDebug(self);
    return cAtOk;
    }

static uint32 MultirateCommonAlarmMask(AtEthPort self)
    {
    return cBit0 << LocalIdForSgmiiMultiRate(self);
    }

static uint32 MultirateCommonAlarmShift(AtEthPort self)
    {
    return LocalIdForSgmiiMultiRate(self);
    }

static eAtRet OneGeDebug(AtEthPort self)
    {
    return AutoNegDebug(self);
    }

static void Interface100BaseFxStickyDisplay(AtEthPort self, const char *title, uint32 localAddress)
    {
    uint32 bitMask = cBit0 << LocalIdForSgmiiMultiRate(self);
    uint32 regAddr = MultirateAddressWithLocalAddress(self, localAddress);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    DisplayBitValue(title, regVal & bitMask);
    mChannelHwWrite(self, regAddr, bitMask, cAtModuleEth);
    }

static eAtRet Interface100BaseFxDebug(AtEthPort self)
    {
    AtPrintc(cSevNormal, "* 100BaseFX:\r\n");
    Interface100BaseFxStickyDisplay(self, "  * Pattern detect sticky   ", cAf6Reg_fx_patstken_Base);
    Interface100BaseFxStickyDisplay(self, "  * Code error detect sticky", cAf6Reg_fx_codestken_Base);
    return cAtOk;
    }

static eBool Is100BaseFx(AtEthPort self)
    {
    if (AtSerdesControllerModeGet(SerdesController(self)) == cAtSerdesModeEth100M)
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet Debug(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;

    m_AtChannelMethod->Debug(self);

    if (SerdesIsIn10GMode(port))
        return TenGeDebug(port);

    if (Is100BaseFx(port) && mMethodsGet(mThis(self))->Interface100BaseFxIsSupported(mThis(self)))
        return Interface100BaseFxDebug(port);

    return OneGeDebug(port);
    }

static eAtEthPortLinkStatus MultirateLinkStatus(AtEthPort self)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress(self, cAf6Reg_lnk_sync_pen0_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 statusMask = cBit0 << LocalIdForSgmiiMultiRate(self);
    return (regVal & statusMask) ? cAtEthPortLinkStatusUp : cAtEthPortLinkStatusDown;
    }

static eAtEthPortLinkStatus LinkStatus(AtEthPort self)
    {
    if (AtEthPortSpeedGet(self) == cAtEthPortSpeed10G)
        return cAtEthPortLinkStatusNotSupported;

    return MultirateLinkStatus(self);
    }

static ThaModuleEth EthModule(AtEthPort self)
    {
    return (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static eBool PassThroughDefectIsSupported(AtEthPort self)
    {
    return ThaModuleEthInterruptIsSupported(EthModule(self)) ? cAtFalse : cAtTrue;
    }

static eBool ExcessiveErrorRatioDefectIsSupported(AtEthPort self)
    {
    return ThaModuleEthInterruptIsSupported(EthModule(self));
    }

static uint32 TenGeDefectHw2Sw(AtEthPort self, uint32 regVal)
    {
    uint32 alarms = 0;

    if (regVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gLocalFaultCur_Mask)
        alarms |= cAtEthPortAlarmLocalFault;
    if (regVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gRemoteFaultCur_Mask)
        alarms |= cAtEthPortAlarmRemoteFault;

    if (ExcessiveErrorRatioDefectIsSupported(self) == cAtFalse)
        return alarms;

    if (regVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gExErrorRatioCur_Mask)
        alarms |= cAtEthPortAlarmExcessiveErrorRatio;
    if (regVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gClockOutRangeCur_Mask)
        alarms |= cAtEthPortAlarmFrequencyOutofRange;
    if (regVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gClockLossCur_Mask)
        alarms |= cAtEthPortAlarmLossofClock;
    if (regVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gDataOrFrameSyncCur_Mask)
        alarms |= cAtEthPortAlarmLossofFrame;

    return alarms;
    }

static uint32 TenGeDefectGet(AtEthPort self)
    {
    uint32 regAddr = TenGeAlarmAddressWithLocalAddress(self, cAf6Reg_eth10g_link_fault_Cur_Sta_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return TenGeDefectHw2Sw(self, regVal);
    }

static uint32 OneGeDefectGetWithLocalAddress(AtEthPort self, uint32 alarmType, uint32 localAddress)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress(self, localAddress);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 fieldMask = MultirateCommonAlarmMask(self);
    return (regVal & fieldMask) ? alarmType : 0;
    }

static uint32 OneGeDefectGet(AtEthPort self)
    {
    uint32 alarms = 0;

    alarms |= OneGeDefectGetWithLocalAddress(self, cAtEthPortAlarmLossofDataSync, cAf6Reg_los_sync_pen_Base);
    if (ExcessiveErrorRatioDefectIsSupported(self))
        alarms |= OneGeDefectGetWithLocalAddress(self, cAtEthPortAlarmExcessiveErrorRatio, cAf6Reg_rxexcer_sta_pen_Base);

    return alarms;
    }

static uint32 MultirateDefectGet(AtEthPort self)
    {
    if (SerdesIsIn10GMode(self))
        return TenGeDefectGet(self);
    else
        return OneGeDefectGet(self);
    }

static uint32 PassThroughDefectGet(AtEthPort self)
    {
    uint32 regAddr;
    uint32 defects = 0;

    if (SerdesIsIn10GMode(self))
        {
        regAddr = AddressWithLocalAddress(cAf6Reg_upen_epatengesta0_Base);
        defects |= TenGeEthPassThroughDefectRead2ClearWithAddress(self, regAddr, cAtFalse);
        }
    else
        {
        regAddr = AddressWithLocalAddress(cAf6Reg_upen_epagesta0_Base);
        defects |= OneGeEthPassThroughDefectRead2ClearWithAddress((AtEthPort)self, regAddr, cAtFalse);
        }

    return defects;
    }

static uint32 EthMask2ClockMonitorMask(uint32 ethMask)
    {
    uint32 clockMonMask = 0;

    if (ethMask & cAtEthPortAlarmLossofClock)
        clockMonMask |= cAtClockMonitorAlarmLossOfClock;
    if (ethMask & cAtEthPortAlarmFrequencyOutofRange)
        clockMonMask |= cAtClockMonitorFrequencyOutOfRange;

    return clockMonMask;
    }

static uint32 ClockMonitorMask2EthMask(uint32 clockMonMask)
    {
    uint32 ethMask = 0;

    if (clockMonMask & cAtClockMonitorAlarmLossOfClock)
        ethMask |= cAtEthPortAlarmLossofClock;
    if (clockMonMask & cAtClockMonitorFrequencyOutOfRange)
        ethMask |= cAtEthPortAlarmFrequencyOutofRange;

    return ethMask;
    }

static uint32 ClockMonitorDefectGet(AtChannel self)
    {
    AtClockMonitor monitor = AtEthPortClockMonitorGet((AtEthPort)self);
    if (monitor)
        return ClockMonitorMask2EthMask(AtClockMonitorAlarmGet(monitor));
    return 0;
    }

static uint32 ClockMonitorDefectHistoryRead2Clear(AtChannel self, eBool read2Clear)
    {
    AtClockMonitor monitor = AtEthPortClockMonitorGet((AtEthPort)self);
    uint32 alarms = 0;

    if (monitor == NULL)
        return 0;

    if (read2Clear)
        alarms = AtClockMonitorAlarmHistoryClear(monitor);
    else
        alarms = AtClockMonitorAlarmHistoryGet(monitor);

    return ClockMonitorMask2EthMask(alarms);
    }

static eAtRet ClockMonitorInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 clockDefectMask = EthMask2ClockMonitorMask(defectMask);
    uint32 clockEnableMask = EthMask2ClockMonitorMask(enableMask);
    AtClockMonitor clockMonitor = AtEthPortClockMonitorGet((AtEthPort)self);

    if (clockMonitor == NULL)
        return (clockEnableMask == 0) ? cAtOk : cAtErrorModeNotSupport;

    return AtClockMonitorInterruptMaskSet(clockMonitor, clockDefectMask, clockEnableMask);
    }

static uint32 ClockMonitorInterruptMaskGet(AtChannel self)
    {
    AtClockMonitor clockMonitor = AtEthPortClockMonitorGet((AtEthPort)self);

    if (clockMonitor)
        {
        uint32 mask = AtClockMonitorInterruptMaskGet(clockMonitor);
        return ClockMonitorMask2EthMask(mask);
        }

    return 0;
    }

static eBool CanMonitorClock(AtEthPort self)
    {
    if (AtEthPortSpeedGet(self) == cAtEthPortSpeed10G)
        return cAtTrue;
    return cAtFalse;
    }

static AtClockMonitor ClockMonitorCreateIfNeed(AtEthPort self)
    {
    ThaModuleClock clockModule;

    if (!mMethodsGet(mThis(self))->HasClockMonitor(mThis(self)))
        return NULL;

    if (mThis(self)->clockMonitor != NULL)
        return mThis(self)->clockMonitor;

    clockModule = (ThaModuleClock)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleClock);
    mThis(self)->clockMonitor = ThaModuleClockEthPortClockMonitorCreate(clockModule, self);
    return mThis(self)->clockMonitor;
    }

static AtClockMonitor ClockMonitorGet(AtEthPort self)
    {
    if (!CanMonitorClock(self))
        return NULL;

    return ClockMonitorCreateIfNeed(self);
    }

static uint32 DefectGet(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 defects = 0;

    defects |= MultirateDefectGet(port);
    if (PassThroughDefectIsSupported(port))
        defects |= PassThroughDefectGet(port);

    defects |= ClockMonitorDefectGet(self);

    return defects;
    }

static uint32 OneGeDefectHistoryRead2ClearWithLocalAddress(AtChannel self, eBool read2Clear, uint32 alarmType, uint32 localAddress)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 regAddr = MultirateAddressWithLocalAddress((AtEthPort)self, localAddress);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 fieldMask = MultirateCommonAlarmMask(port);

    if (read2Clear)
        mChannelHwWrite(self, regAddr, fieldMask, cAtModuleEth);

    return (regVal & fieldMask) ? alarmType : 0;
    }

static uint32 OneGeDefectHistoryRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 alarms = 0;

    alarms |= OneGeDefectHistoryRead2ClearWithLocalAddress(self, read2Clear, cAtEthPortAlarmAutoNegStateChange, cAf6Reg_an_sta_stk_pen_Base);
    alarms |= OneGeDefectHistoryRead2ClearWithLocalAddress(self, read2Clear, cAtEthPortAlarmRemoteFault, cAf6Reg_an_rfi_stk_pen_Base);
    alarms |= OneGeDefectHistoryRead2ClearWithLocalAddress(self, read2Clear, cAtEthPortAlarmLossofDataSync, cAf6Reg_los_sync_stk_pen_Base);
    if (ExcessiveErrorRatioDefectIsSupported((AtEthPort)self))
        alarms |= OneGeDefectHistoryRead2ClearWithLocalAddress(self, read2Clear, cAtEthPortAlarmExcessiveErrorRatio, cAf6Reg_rxexcer_stk_pen_Base);

    return alarms;
    }

static uint32 TenGeDefectHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    static const uint32 unclearedMask = cAf6_eth10g_link_fault_sticky_Eth10gInterrptionStk_Mask;
    uint32 regAddr, regVal;
    uint32 alarms;

    regAddr = TenGeAlarmAddressWithLocalAddress(self, cAf6Reg_eth10g_link_fault_sticky_Base);
    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    alarms = TenGeDefectHw2Sw(self, regVal);

    if (read2Clear)
        mChannelHwWrite(self, regAddr, (regVal & ~unclearedMask), cAtModuleEth);

    return alarms;
    }

static uint32 MultirateDefectHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    if (SerdesIsIn10GMode(self))
        return TenGeDefectHistoryRead2Clear(self, read2Clear);
    else
        return OneGeDefectHistoryRead2Clear((AtChannel)self, read2Clear);
    }

static uint32 DefectHistoryRead2Clear(AtChannel self, eBool read2Clear)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 defects = 0;

    defects |= MultirateDefectHistoryRead2Clear(port, read2Clear);
    if (PassThroughDefectIsSupported(port))
        defects |= PassThroughDefectHistoryRead2Clear(port, read2Clear);

    defects |= ClockMonitorDefectHistoryRead2Clear(self, read2Clear);

    return defects;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return DefectHistoryRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return DefectHistoryRead2Clear(self, cAtTrue);
    }

static uint32 OneGeInterruptProcess(AtEthPort self, uint32 ethIntr)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint8 sixteenPortGroupId = Tha60290021FaceplateSerdesEthPortSixteenPortGroupId(self);
    uint32 baseAddress = Tha602900xxModuleEthMultirateBaseAddress(ethModule, sixteenPortGroupId);
    uint32 portMask = MultirateCommonAlarmMask(self);
    uint32 events = 0, defects = 0;
    uint32 regAddr, regVal;

    if (ethIntr & cAf6_GE_Interrupt_OR_los_sync_int_or_Mask)
        {
        regAddr = baseAddress + cAf6Reg_GE_Loss_Of_Sync_AND_MASK_Base;
        regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
        if (regVal & portMask)
            {
            events  |= cAtEthPortAlarmLossofDataSync;

            regAddr = baseAddress + cAf6Reg_los_sync_pen_Base;
            regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
            if (regVal & portMask)
                defects |= cAtEthPortAlarmLossofDataSync;

            regAddr = baseAddress + cAf6Reg_los_sync_stk_pen_Base;
            mChannelHwWrite(self, regAddr, portMask, cAtModuleEth);
            }
        }

    if (ethIntr & cAf6_GE_Interrupt_OR_ant_int_or_Mask)
        {
        regAddr = baseAddress + cAf6Reg_GE_Auto_neg_State_Change_AND_MASK_Base;
        regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
        if (regVal & portMask)
            {
            events  |= cAtEthPortAlarmAutoNegStateChange;

            regAddr = baseAddress + cAf6Reg_an_sta_stk_pen_Base;
            mChannelHwWrite(self, regAddr, portMask, cAtModuleEth);
            }
        }

    if (ethIntr & cAf6_GE_Interrupt_OR_an_rfi_int_or_Mask)
        {
        regAddr = baseAddress + cAf6Reg_GE_Remote_Fault_AND_MASK_Base;
        regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
        if (regVal & portMask)
            {
            events  |= cAtEthPortAlarmRemoteFault;

            regAddr = baseAddress + cAf6Reg_an_rfi_stk_pen_Base;
            mChannelHwWrite(self, regAddr, portMask, cAtModuleEth);
            }
        }

    if (ethIntr & cAf6_GE_Interrupt_OR_rxexcer_int_or_Mask)
        {
        regAddr = baseAddress + cAf6Reg_GE_Excessive_Error_Ratio_AND_MASK_Base;
        regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
        if (regVal & portMask)
            {
            events  |= cAtEthPortAlarmExcessiveErrorRatio;

            regAddr = baseAddress + cAf6Reg_rxexcer_sta_pen_Base;
            regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
            if (regVal & portMask)
                defects |= cAtEthPortAlarmExcessiveErrorRatio;

            regAddr = baseAddress + cAf6Reg_rxexcer_stk_pen_Base;
            mChannelHwWrite(self, regAddr, portMask, cAtModuleEth);
            }
        }

    if (events)
        AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);

    return defects;
    }

static uint32 TenGeInterruptProcess(AtEthPort self)
    {
    uint32 baseAddress = TenGeBaseAddress(self);
    uint32 intrEnReg  = baseAddress + cAf6Reg_eth10g_link_fault_int_enb_Base;
    uint32 intrStaReg = baseAddress + cAf6Reg_eth10g_link_fault_sticky_Base;
    uint32 currStaReg = baseAddress + cAf6Reg_eth10g_link_fault_Cur_Sta_Base;
    uint32 intrEnVal  = mChannelHwRead(self, intrEnReg, cAtModuleEth);
    uint32 intrStaVal = mChannelHwRead(self, intrStaReg, cAtModuleEth);
    uint32 currStaVal = mChannelHwRead(self, currStaReg, cAtModuleEth);
    uint32 events = 0, defects = 0;

    /* Clear marked interrupts */
    intrStaVal &= intrEnVal;
    mChannelHwWrite(self, intrStaReg, intrStaVal, cAtModuleEth);

    if (intrStaVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gLocalFaultCur_Mask)
        {
        events |= cAtEthPortAlarmLocalFault;
        if (currStaVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gLocalFaultCur_Mask)
            defects |= cAtEthPortAlarmLocalFault;
        }

    if (intrStaVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gRemoteFaultCur_Mask)
        {
        events |= cAtEthPortAlarmRemoteFault;
        if (currStaVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gRemoteFaultCur_Mask)
            defects |= cAtEthPortAlarmRemoteFault;
        }

    if (intrStaVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gExErrorRatioCur_Mask)
        {
        events |= cAtEthPortAlarmExcessiveErrorRatio;
        if (currStaVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gExErrorRatioCur_Mask)
            defects |= cAtEthPortAlarmExcessiveErrorRatio;
        }

    if (intrStaVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gClockOutRangeCur_Mask)
        {
        events |= cAtEthPortAlarmFrequencyOutofRange;
        if (currStaVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gClockOutRangeCur_Mask)
            defects |= cAtEthPortAlarmFrequencyOutofRange;
        }

    if (intrStaVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gClockLossCur_Mask)
        {
        events |= cAtEthPortAlarmLossofClock;
        if (currStaVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gClockLossCur_Mask)
            defects |= cAtEthPortAlarmLossofClock;
        }

    if (intrStaVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gDataOrFrameSyncCur_Mask)
        {
        events |= cAtEthPortAlarmLossofFrame;
        if (currStaVal & cAf6_eth10g_link_fault_Cur_Sta_Eth10gDataOrFrameSyncCur_Mask)
            defects |= cAtEthPortAlarmLossofFrame;
        }

    if (events)
        AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);

    return defects;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    AtUnused(offset);
    return TenGeInterruptProcess((AtEthPort)self);
    }

static uint32 AutoNegTxEnableMask(AtChannel self)
    {
    return cBit16 << LocalIdForSgmiiMultiRate((AtEthPort)self);
    }

static uint32 AutoNegTxEnableShift(AtChannel self)
    {
    return (uint32) (16 + LocalIdForSgmiiMultiRate((AtEthPort) self));
    }

static eAtRet AutoNegTxEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress((AtEthPort)self, cAf6Reg_an_mod_pen0_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 txPortEnableMask = AutoNegTxEnableMask(self);
    uint32 txPortEnableShift = AutoNegTxEnableShift(self);

    mRegFieldSet(regVal, txPortEnable, enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool AutoNegTxIsEnabled(AtChannel self)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress((AtEthPort)self, cAf6Reg_an_mod_pen0_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 txPortEnableMask = AutoNegTxEnableMask(self);
    return (regVal & txPortEnableMask) ? cAtTrue : cAtFalse;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= AutoNegTxEnable(self, enable);

    if (mMethodsGet(mThis(self))->Interface100BaseFxIsSupported(mThis(self)))
        ret |= Interface100BaseFxEnable(self, enable);

    return ret;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    if (Is100BaseFx((AtEthPort)self))
        return Interface100BaseFxIsEnabled(self);

    return AutoNegTxIsEnabled(self);
    }

static eBool TxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtTrue;
    }

static eBool RxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    return (enabled ? cAtTrue : cAtFalse);
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return (enable ? cAtOk : cAtErrorModeNotSupport);
    }

static eBool RxIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    return AtChannelTxEnable(self, enable);
    }

static eBool IsEnabled(AtChannel self)
    {
    return AtChannelTxIsEnabled(self);
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    return AtChannelTxCanEnable(self, enable);
    }

static eAtRet FlowControlDefaultSet(AtEthPort self)
    {
    eAtRet ret = cAtOk;
    AtEthFlowControl flowControl;

    flowControl = AtEthPortFlowControlGet(self);
    if (flowControl == NULL)
        return cAtOk;

    ret |= AtEthFlowControlHighThresholdSet(flowControl, 192); /* (256 * (3/4)) */
    ret |= AtEthFlowControlLowThresholdSet(flowControl, 64); /* (256 / 4) */

    return ret;
    }

static eBool HasDisparityFeature(Tha60290021FaceplateSerdesEthPort self)
    {
    return ShouldOpenFeatureWithBuiltDetail((AtChannel)self, 0x1, 0x7, 0x00);
    }

static eAtRet Init(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;
    AtClockMonitor clockMonitor;
    eAtRet ret = m_AtChannelMethod->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= AtEthPortMaxPacketSizeSet(port, cAf6_ramrxepacfg1_EthPassMtuSize_Max); /* To match default requirement */
    ret |= AtEthPortDropPacketConditionMaskSet(port, 0xFFFFFFFF, 0xFFFFFFFF);
    ret |= Tha60290021ModuleEthPortBypassEnable(port, cAtFalse);

    if (mMethodsGet(mThis(self))->HasDisparityFeature(mThis(self)))
        Tha6029FaceplateSerdesControllerDisparityForce(SerdesController(port), cAtFalse);

    /* As clock monitor is only available for 10G, so clock monitor interrupt
     * mask should be disabled as default to avoid interrupt flooding */
    if ((clockMonitor = ClockMonitorCreateIfNeed(port)))
        ret |= AtClockMonitorInit(clockMonitor);

    return ret;
    }

static eAtRet SubportVlanCheck(AtEthPort self, const tAtVlan *vlan)
    {
    if ((self == NULL) || (vlan == NULL))
        return cAtErrorNullPointer;
        
    return Tha60290021ModuleEthIsFaceplatePort(self) ? cAtOk : cAtErrorNotApplicable;
    }

static eAtRet DropPacketConditionMaskSet(AtEthPort self, uint32 conditionMask, uint32 enableMask)
    {
    uint8 portId = LocalId(self);
    uint32 localAddress = mMethodsGet(mThis(self))->Af6Reg_ramrxepacfg1_Base(mThis(self));
    uint32 regAddr = AddressWithLocalAddress(localAddress) + portId;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (conditionMask & cAtEthPortDropConditionPcsError)
        {
        mRegFieldSet(regVal, cAf6_ramrxepacfg1_EthPassPcsErrDropEn_, (enableMask & cAtEthPortDropConditionPcsError) ? 1 : 0);
        }

    if (conditionMask & cAtEthPortDropConditionPktFcsError)
        {
        mRegFieldSet(regVal, cAf6_ramrxepacfg1_EthPassFcsErrDropEn_, (enableMask & cAtEthPortDropConditionPktFcsError) ? 1 : 0);
        }

    if (conditionMask & cAtEthPortDropConditionPktUnderSize)
        {
        mRegFieldSet(regVal, cAf6_ramrxepacfg1_EthPassUndersizeDropEn_, (enableMask & cAtEthPortDropConditionPktUnderSize) ? 1 : 0);
        }

    if (conditionMask & cAtEthPortDropConditionPktOverSize)
        {
        mRegFieldSet(regVal, cAf6_ramrxepacfg1_EthPassOversizeDropEn_, (enableMask & cAtEthPortDropConditionPktOverSize) ? 1 : 0);
        }

    if (conditionMask & cAtEthPortDropConditionPktPauseFrame)
        {
        mRegFieldSet(regVal, cAf6_ramrxepacfg1_EthPassPausefrmDropEn_, (enableMask & cAtEthPortDropConditionPktPauseFrame) ? 1 : 0);
        }

    if (conditionMask & cAtEthPortDropConditionPktLoopDa)
        {
        mRegFieldSet(regVal, cAf6_ramrxepacfg1_EthPassDaloopDropEn_, (enableMask & cAtEthPortDropConditionPktLoopDa) ? 1 : 0);
        }

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 DropPacketConditionMaskGet(AtEthPort self)
    {
    uint32 conditionMask = 0;
    uint8 portId = LocalId(self);
    uint32 localAddress = mMethodsGet(mThis(self))->Af6Reg_ramrxepacfg1_Base(mThis(self));
    uint32 regAddr = AddressWithLocalAddress(localAddress) + portId;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (mRegField(regVal, cAf6_ramrxepacfg1_EthPassPcsErrDropEn_))
        conditionMask |= cAtEthPortDropConditionPcsError;

    if (mRegField(regVal, cAf6_ramrxepacfg1_EthPassFcsErrDropEn_))
        conditionMask |= cAtEthPortDropConditionPktFcsError;

    if (mRegField(regVal, cAf6_ramrxepacfg1_EthPassUndersizeDropEn_))
        conditionMask |= cAtEthPortDropConditionPktUnderSize;

    if (mRegField(regVal, cAf6_ramrxepacfg1_EthPassOversizeDropEn_))
        conditionMask |= cAtEthPortDropConditionPktOverSize;

    if (mRegField(regVal, cAf6_ramrxepacfg1_EthPassPausefrmDropEn_))
        conditionMask |= cAtEthPortDropConditionPktPauseFrame;

    if (mRegField(regVal, cAf6_ramrxepacfg1_EthPassDaloopDropEn_))
        conditionMask |= cAtEthPortDropConditionPktLoopDa;

    return conditionMask;
    }

static uint32 OneGeSupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return cAtEthPortAlarmLossofDataSync      |
           cAtEthPortAlarmExcessiveErrorRatio |
           cAtEthPortAlarmAutoNegStateChange  |
           cAtEthPortAlarmRemoteFault;
    }

static uint32 TenGeSupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return cAtEthPortAlarmLossofClock         |
           cAtEthPortAlarmLossofFrame         |
           cAtEthPortAlarmLocalFault          |
           cAtEthPortAlarmRemoteFault         |
           cAtEthPortAlarmExcessiveErrorRatio |
           cAtEthPortAlarmFrequencyOutofRange;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    if (SerdesIsIn10GMode((AtEthPort)self))
        return TenGeSupportedInterruptMasks(self);
    else
        return OneGeSupportedInterruptMasks(self);
    }

static eAtEthPortSpeed AutoNegSgmiiRxAbilitySpeedGet(AtEthPort self)
    {
    uint32 regAddr = AutoNegRxAbilityRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 speedMask = AutoNegStatusSpeedMask(self);
    uint32 speedShift = AutoNegStatusSpeedShift(self);
    return SpeedHw2Sw(mRegField(regVal, speed));
    }

static eAtEthPortSpeed AutoNegSpeedGet(AtEthPort self)
    {
    if (AtEthPortInterfaceGet(self) == cAtEthPortInterfaceSgmii)
        return AutoNegSgmiiRxAbilitySpeedGet(self);

    return cAtEthPortSpeedNA;
    }

static eAtEthPortDuplexMode AutoNegDuplexModeGet(AtEthPort self)
    {
    uint32 interface = AtEthPortInterfaceGet(self);

    switch (interface)
        {
        case cAtEthPortInterfaceSgmii:
            return AutoNegSgmiiDetectedDuplexMode(self);
        case cAtEthPortInterface1000BaseX:
            return AutoNeg1000BasexDetectedDuplexMode(self);

        default:
            return cAtEthPortWorkingModeFullDuplex;
        }
    }

static eAtModuleEthRet AutoNegRestartOn(AtEthPort self, eBool on)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress(self, cAf6Reg_an_rst_pen0_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 autoNegResetMask = AutoNegResetMask(self);
    uint32 autoNegResetShift = AutoNegResetShift(self);
    mRegFieldSet(regVal, autoNegReset, on ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool AutoNegRestartIsOn(AtEthPort self)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress(self, cAf6Reg_an_rst_pen0_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 autoNegResetMask = AutoNegResetMask(self);
    return (regVal & autoNegResetMask) ? cAtTrue : cAtFalse;
    }

static eBool AutoNegIsComplete(AtEthPort self)
    {
    /* This interface does not support complete indication */
    return (AtEthPortAutoNegStateGet(self) == cAtEthPortAutoNegLinkOk) ? cAtTrue : cAtFalse;
    }

static eAtEthPortRemoteFault AutoNegRemoteFaultHw2Sw(uint32 hwRemoteFault)
    {
    switch (hwRemoteFault)
        {
        case 0:  return cAtEthPortRemoteFaultNoError;
        case 1:  return cAtEthPortRemoteFaultLinkFailure;
        case 2:  return cAtEthPortRemoteFaultOffline;
        case 3:  return cAtEthPortRemoteFaultAutoNegError;
        default: return cAtEthPortRemoteFaultUnknown;
        }
    }

static eAtEthPortRemoteFault AutoNeg1000BasexRemoteFaultGet(AtEthPort self)
    {
    uint32 regAddr = AutoNegRxAbilityRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 localId = AutoNegRxAbilityLocalIdInGroup(self);
    uint32 remoteFaultMask  = (localId == 0) ? cAf6_an_rxab_pen00_ReFault00_Mask  : cAf6_an_rxab_pen00_ReFault01_Mask;
    uint32 remoteFaultShift = (localId == 0) ? cAf6_an_rxab_pen00_ReFault00_Shift : cAf6_an_rxab_pen00_ReFault01_Shift;
    return AutoNegRemoteFaultHw2Sw(mRegField(regVal, remoteFault));
    }

static eAtEthPortRemoteFault RemoteFaultGet(AtEthPort self)
    {
    if (AtEthPortInterfaceGet(self) == cAtEthPortInterface1000BaseX)
        return AutoNeg1000BasexRemoteFaultGet(self);

    return m_AtEthPortMethods->RemoteFaultGet(self);
    }

static eAtRet OneGeInterruptEnableWithLocalAddress(AtEthPort self, eBool enabled, uint32 localAddress)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress(self, localAddress);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 fieldMask = MultirateCommonAlarmMask(self);
    uint32 fieldShift = MultirateCommonAlarmShift(self);
    mRegFieldSet(regVal, field, enabled ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool OneGeInterruptIsEnabledWithLocalAddress(AtEthPort self, uint32 localAddress)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress(self, localAddress);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 fieldMask = MultirateCommonAlarmMask(self);
    return (regVal & fieldMask) ? cAtTrue : cAtFalse;
    }

static eAtRet OneGeInterruptMaskSet(AtEthPort self, uint32 defectMask, uint32 enableMask)
    {
    eAtRet ret = cAtOk;
    eBool enabled;

    if (defectMask & cAtEthPortAlarmAutoNegStateChange)
        {
        enabled = (enableMask & cAtEthPortAlarmAutoNegStateChange) ? cAtTrue : cAtFalse;
        ret |= OneGeInterruptEnableWithLocalAddress(self, enabled, cAf6Reg_an_int_enb_pen_Base);
        }

    if (defectMask & cAtEthPortAlarmRemoteFault)
        {
        enabled = (enableMask & cAtEthPortAlarmRemoteFault) ? cAtTrue : cAtFalse;
        ret |= OneGeInterruptEnableWithLocalAddress(self, enabled, cAf6Reg_an_rfi_int_enb_pen_Base);
        }

    if (defectMask & cAtEthPortAlarmLossofDataSync)
        {
        enabled = (enableMask & cAtEthPortAlarmLossofDataSync) ? cAtTrue : cAtFalse;
        ret |= OneGeInterruptEnableWithLocalAddress(self, enabled, cAf6Reg_los_sync_int_enb_pen_Base);
        }

    if (ExcessiveErrorRatioDefectIsSupported(self))
        {
        if (defectMask & cAtEthPortAlarmExcessiveErrorRatio)
            {
            enabled = (enableMask & cAtEthPortAlarmExcessiveErrorRatio) ? cAtTrue : cAtFalse;
            ret |= OneGeInterruptEnableWithLocalAddress(self, enabled, cAf6Reg_rxexcer_enb_pen_Base);
            }
        }

    return ret;
    }

static uint32 OneGeInterruptMaskGet(AtEthPort self)
    {
    uint32 masks = 0;

    if (OneGeInterruptIsEnabledWithLocalAddress(self, cAf6Reg_an_int_enb_pen_Base))
        masks |= cAtEthPortAlarmAutoNegStateChange;

    if (OneGeInterruptIsEnabledWithLocalAddress(self, cAf6Reg_an_rfi_int_enb_pen_Base))
        masks |= cAtEthPortAlarmRemoteFault;

    if (OneGeInterruptIsEnabledWithLocalAddress(self, cAf6Reg_los_sync_int_enb_pen_Base))
        masks |= cAtEthPortAlarmLossofDataSync;

    if (ExcessiveErrorRatioDefectIsSupported(self))
        {
        if (OneGeInterruptIsEnabledWithLocalAddress(self, cAf6Reg_rxexcer_enb_pen_Base))
            masks |= cAtEthPortAlarmExcessiveErrorRatio;
        }

    return masks;
    }

static eAtRet TenGeInterruptMaskSet(AtEthPort self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = TenGeAlarmAddressWithLocalAddress(self, cAf6Reg_eth10g_link_fault_int_enb_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 enabled;

    if (defectMask & cAtEthPortAlarmLocalFault)
        {
        enabled = (enableMask & cAtEthPortAlarmLocalFault) ? 1 : 0;
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gLocalFaultEnb_, enabled);
        }

    if (defectMask & cAtEthPortAlarmRemoteFault)
        {
        enabled = (enableMask & cAtEthPortAlarmRemoteFault) ? 1 : 0;
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gRemoteFaultEnb_, enabled);
        }

    if (ExcessiveErrorRatioDefectIsSupported(self) == cAtFalse)
        {
        mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
        return cAtOk;
        }

    if (defectMask & cAtEthPortAlarmExcessiveErrorRatio)
        {
        enabled = (enableMask & cAtEthPortAlarmExcessiveErrorRatio) ? 1 : 0;
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gExErrorRatioEnb_, enabled);
        }

    if (defectMask & cAtEthPortAlarmFrequencyOutofRange)
        {
        enabled = (enableMask & cAtEthPortAlarmFrequencyOutofRange) ? 1 : 0;
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gClockOutRangeEnb_, enabled);
        }

    if (defectMask & cAtEthPortAlarmLossofClock)
        {
        enabled = (enableMask & cAtEthPortAlarmLossofClock) ? 1 : 0;
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gClockLossEnb_, enabled);
        }

    if (defectMask & cAtEthPortAlarmLossofFrame)
        {
        enabled = (enableMask & cAtEthPortAlarmLossofFrame) ? 1 : 0;
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gDataOrFrameSyncEnb_, enabled);
        }

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 TenGeInterruptMaskGet(AtEthPort self)
    {
    uint32 regAddr = TenGeAlarmAddressWithLocalAddress(self, cAf6Reg_eth10g_link_fault_int_enb_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return TenGeDefectHw2Sw(self, regVal);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    AtEthPort port = (AtEthPort)self;
    eAtRet ret = cAtOk;

    if (SerdesIsIn10GMode(port))
        ret = TenGeInterruptMaskSet(port, defectMask, enableMask);
    else
        ret = OneGeInterruptMaskSet(port, defectMask, enableMask);

    ret |= ClockMonitorInterruptMaskSet(self, defectMask, enableMask);

    return ret;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 mask = 0;

    if (SerdesIsIn10GMode(port))
        mask = TenGeInterruptMaskGet(port);
    else
        mask = OneGeInterruptMaskGet(port);

    mask |= ClockMonitorInterruptMaskGet(self);

    return mask;
    }

static eAtRet DisparityForce(AtEthPort self, eBool forced)
    {
    if (AtEthPortDisparityCanForce(self))
        return Tha6029FaceplateSerdesControllerDisparityForce(SerdesController(self), forced);

    return m_AtEthPortMethods->DisparityForce(self, forced);
    }

static eBool DisparityIsForced(AtEthPort self)
    {
    if (AtEthPortDisparityCanForce(self))
        return Tha6029FaceplateSerdesControllerDisparityIsForced(SerdesController(self));

    return m_AtEthPortMethods->DisparityIsForced(self);
    }

static eBool InterfaceTypeHasDisparity(AtEthPort self)
    {
    if (AtEthPortInterfaceGet(self) == cAtEthPortInterface1000BaseX)
        return cAtTrue;
    return cAtFalse;
    }

static eBool DisparityCanForce(AtEthPort self)
    {
    if (mMethodsGet(mThis(self))->HasDisparityFeature(mThis(self)))
        return InterfaceTypeHasDisparity(self);

    return cAtFalse;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290021FaceplateSerdesEthPort* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(speed);
    mEncodeObject(clockMonitor);
    mEncodeNone(counters);
    }

static void StatusClear(AtChannel self)
    {
    AtSerdesController serdes = SerdesController((AtEthPort)self);

    if (AtSerdesControllerIsControllable(serdes))
        m_AtChannelMethod->StatusClear(self);
    }

static eAtRet TenGeRxAlarmForce(AtChannel self, uint32 alarmType, eBool forced)
    {
    uint32 regAddr = TenGeAlarmAddressWithLocalAddress((AtEthPort)self, cAf6Reg_eth10g_link_fault_int_enb_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 hwVal = forced ? 1 : 0;

    if (alarmType & cAtEthPortAlarmLocalFault)
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gForceRXLF_, hwVal);

    if (alarmType & cAtEthPortAlarmRemoteFault)
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gForceRXRF_, hwVal);

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 TenGeRxForcedAlarmGet(AtChannel self)
    {
    uint32 regAddr = TenGeAlarmAddressWithLocalAddress((AtEthPort)self, cAf6Reg_eth10g_link_fault_int_enb_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 alarms = 0;

    if (regVal & cAf6_eth10g_link_fault_int_enb_Eth10gForceRXLF_Mask)
        alarms |= cAtEthPortAlarmLocalFault;

    if (regVal & cAf6_eth10g_link_fault_int_enb_Eth10gForceRXRF_Mask)
        alarms |= cAtEthPortAlarmRemoteFault;

    return alarms;
    }

static uint32 ForcibleAlarmsGet(AtChannel self)
    {
    if (SerdesIsIn10GMode((AtEthPort)self))
        return cAtEthPortAlarmLocalFault | cAtEthPortAlarmRemoteFault;
    return 0;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    return ForcibleAlarmsGet(self);
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    uint32 unSupportedAlarms = ~(AtChannelRxForcableAlarmsGet(self));

    if (alarmType & unSupportedAlarms)
        {
        AtChannelLog(self, cAtLogLevelWarning, AtSourceLocation, "Alarm 0x%0x cannot be forced\r\n", alarmType & unSupportedAlarms);
        return cAtErrorModeNotSupport;
        }

    return TenGeRxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return TenGeRxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    return TenGeRxForcedAlarmGet(self);
    }

static eBool InterfaceTypeHasK30_7(AtEthPort self)
    {
    if (AtEthPortInterfaceGet(self) == cAtEthPortInterface1000BaseX)
        return cAtTrue;
    return cAtFalse;
    }

static eBool HasK30_7Feature(Tha60290021FaceplateSerdesEthPort self)
    {
    /* Will now mainly working on 20G */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet K30_7HwForce(AtEthPort self, eBool forced)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress((AtEthPort)self, cAf6Reg_GE_1000basex_Force_K30_7_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 enabledShift = LocalIdForSgmiiMultiRate((AtEthPort)self);
    uint32 enabledMask = cBit0 << enabledShift;
    mRegFieldSet(regVal, enabled, forced ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool K30_7HwIsForced(AtEthPort self)
    {
    uint32 regAddr = MultirateAddressWithLocalAddress((AtEthPort)self, cAf6Reg_GE_1000basex_Force_K30_7_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 enabledMask = cBit0 << LocalIdForSgmiiMultiRate((AtEthPort)self);
    return (regVal & enabledMask) ? cAtTrue : cAtFalse;
    }

static eBool K30_7CanForce(AtEthPort self)
    {
    if (mMethodsGet(mThis(self))->HasK30_7Feature(mThis(self)))
        return InterfaceTypeHasK30_7(self);

    return cAtFalse;
    }

static eAtRet K30_7Force(AtEthPort self, eBool forced)
    {
    if (AtEthPortK30_7CanForce(self))
        return K30_7HwForce(self, forced);

    return m_AtEthPortMethods->K30_7Force(self, forced);
    }

static eBool K30_7IsForced(AtEthPort self)
    {
    return K30_7HwIsForced(self);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->clockMonitor);
    AtObjectDelete((AtObject)mThis(self)->counters);
    mThis(self)->clockMonitor = NULL;
    mThis(self)->counters = NULL;

    /* Super */
    m_AtObjectMethods->Delete(self);
    }

static uint32 StartVersionSupportTxAlarmForcing(Tha60290021FaceplateSerdesEthPort self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x2, 0x1445);
    }

static eBool TxAlarmForceIsSupported(AtChannel self)
    {
    return ShouldOpenFeatureWithVersionNumber(self, mMethodsGet(mThis(self))->StartVersionSupportTxAlarmForcing(mThis(self)));
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (TxAlarmForceIsSupported(self))
        return ForcibleAlarmsGet(self);
    return 0;
    }

static eAtRet TenGeTxAlarmForce(AtChannel self, uint32 alarmType, eBool forced)
    {
    uint32 regAddr = TenGeAlarmAddressWithLocalAddress((AtEthPort)self, cAf6Reg_eth10g_link_fault_int_enb_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 hwVal = forced ? 1 : 0;

    if (alarmType & cAtEthPortAlarmLocalFault)
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gForceTXLF_, hwVal);

    if (alarmType & cAtEthPortAlarmRemoteFault)
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gForceTXRF_, hwVal);

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 TenGeTxForcedAlarmGet(AtChannel self)
    {
    uint32 regAddr = TenGeAlarmAddressWithLocalAddress((AtEthPort)self, cAf6Reg_eth10g_link_fault_int_enb_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 alarms = 0;

    if (regVal & cAf6_eth10g_link_fault_int_enb_Eth10gForceTXLF_Mask)
        alarms |= cAtEthPortAlarmLocalFault;

    if (regVal & cAf6_eth10g_link_fault_int_enb_Eth10gForceTXRF_Mask)
        alarms |= cAtEthPortAlarmRemoteFault;

    return alarms;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    uint32 unSupportedAlarms = ~(AtChannelTxForcableAlarmsGet(self));

    if (alarmType & unSupportedAlarms)
        {
        AtChannelLog(self, cAtLogLevelWarning, AtSourceLocation, "Alarm 0x%0x cannot be forced\r\n", alarmType & unSupportedAlarms);
        return cAtErrorModeNotSupport;
        }

    return TenGeTxAlarmForce(self, alarmType, cAtTrue);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return TenGeTxAlarmForce(self, alarmType, cAtFalse);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    return TenGeTxForcedAlarmGet(self);
    }

static eBool ExcessiveErrorRatioIsSupported(Tha60290021FaceplateSerdesEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtDevice device = AtChannelDeviceGet(channel);

    if (Tha60290021DeviceIsRunningOnVu13P(device))
        return ShouldOpenFeatureWithBuiltDetail(channel, 0x4, 0x1, 0x1445);

    return cAtFalse;
    }

static uint32 RxExcerThresAddressBase(AtEthPort self)
    {
    uint32 groupId = Faceplate8PortGroupIn16PortGroup(self);

    if (groupId == 0)
        return cAf6Reg_GE_TenG_Excessive_Error_Threshold_Group0_Base;
    if (groupId == 1)
        return cAf6Reg_GE_TenG_Excessive_Error_Threshold_Group1_Base;

    return cInvalidUint32;
    }

static uint32 RxExcerThresAddress(AtEthPort self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet((AtChannel)self);
    uint8 sixteenPortGroupId = Tha60290021FaceplateSerdesEthPortSixteenPortGroupId(self);
    return RxExcerThresAddressBase(self) + Tha602900xxModuleEthMultirateBaseAddress(ethModule, sixteenPortGroupId);
    }

static eAtRet ExcessiveErrorRatioUpperThresholdSet(AtEthPort self, uint32 threshold)
    {
    uint32 regAddr = RxExcerThresAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_GE_TenG_Excessive_Error_Threshold_Group0_rxexcer_timer_uthres0_, threshold);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 ExcessiveErrorRatioUpperThresholdGet(AtEthPort self)
    {
    uint32 regAddr = RxExcerThresAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_GE_TenG_Excessive_Error_Threshold_Group0_rxexcer_timer_uthres0_);
    }

static uint32 ExcessiveErrorRatioUpperThresholdMax(AtEthPort self)
    {
    AtUnused(self);
    return cAf6_GE_TenG_Excessive_Error_Threshold_Group0_rxexcer_timer_uthres0_Mask >> cAf6_GE_TenG_Excessive_Error_Threshold_Group0_rxexcer_timer_uthres0_Shift;
    }

static eAtRet ExcessiveErrorRatioLowerThresholdSet(AtEthPort self, uint32 threshold)
    {
    uint32 regAddr = RxExcerThresAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_GE_TenG_Excessive_Error_Threshold_Group0_rxexcer_timer_lthres0_, threshold);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 ExcessiveErrorRatioLowerThresholdGet(AtEthPort self)
    {
    uint32 regAddr = RxExcerThresAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_GE_TenG_Excessive_Error_Threshold_Group0_rxexcer_timer_lthres0_);
    }

static uint32 ExcessiveErrorRatioLowerThresholdMax(AtEthPort self)
    {
    AtUnused(self);
    return cAf6_GE_TenG_Excessive_Error_Threshold_Group0_rxexcer_timer_lthres0_Mask >> cAf6_GE_TenG_Excessive_Error_Threshold_Group0_rxexcer_timer_lthres0_Shift;
    }

static eAtRet ExcessiveErrorRatioThresholdCheck(AtEthPort self, uint32 threshold, uint32 maxValue)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (!Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioIsSupported(self))
        return cAtErrorModeNotSupport;

    if (threshold > maxValue)
        return cAtErrorOutOfRangParm;

    return cAtOk;
    }

static eBool MacCheckingCanEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtFalse : cAtTrue;
    }

static uint32 HwIdGet(AtChannel self)
    {
    return Tha60290021ModuleEthFaceplateLocalId((AtModuleEth)EthModule((AtEthPort)self), (uint8)AtChannelIdGet(self));
    }

static eBool HasClockMonitor(Tha60290021FaceplateSerdesEthPort self)
    {
    uint32 portId = AtChannelHwIdGet((AtChannel)self);

    if ((portId == 0) || (portId == 8))
        return cAtTrue;

    return cAtFalse;
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethod = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethod, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
		mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxCanEnable);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxCanEnable);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        mMethodOverride(m_AtChannelOverride, StatusClear);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, HwIdGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(tAtEthPortMethods));

        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, MacCheckingCanEnable);
        mMethodOverride(m_AtEthPortOverride, HasDestMac);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV4AddressGet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressSet);
        mMethodOverride(m_AtEthPortOverride, IpV6AddressGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegEnable);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsEnabled);
        mMethodOverride(m_AtEthPortOverride, AutoNegStateGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsSupported);
        mMethodOverride(m_AtEthPortOverride, DuplexModeGet);
        mMethodOverride(m_AtEthPortOverride, FlowControlDefaultSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, LinkStatus);
        mMethodOverride(m_AtEthPortOverride, AutoNegSpeedGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegDuplexModeGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegRestartOn);
        mMethodOverride(m_AtEthPortOverride, AutoNegRestartIsOn);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsComplete);
        mMethodOverride(m_AtEthPortOverride, RemoteFaultGet);
        mMethodOverride(m_AtEthPortOverride, ClockMonitorGet);

        /* MTU */
        mMethodOverride(m_AtEthPortOverride, MaxPacketSizeSet);
        mMethodOverride(m_AtEthPortOverride, MaxPacketSizeGet);

        /* Drop Packets*/
        mMethodOverride(m_AtEthPortOverride, DropPacketConditionMaskSet);
        mMethodOverride(m_AtEthPortOverride, DropPacketConditionMaskGet);

        /* Disparity force */
        mMethodOverride(m_AtEthPortOverride, DisparityForce);
        mMethodOverride(m_AtEthPortOverride, DisparityIsForced);
        mMethodOverride(m_AtEthPortOverride, DisparityCanForce);

        /* K30.7 */
        mMethodOverride(m_AtEthPortOverride, K30_7Force);
        mMethodOverride(m_AtEthPortOverride, K30_7IsForced);
        mMethodOverride(m_AtEthPortOverride, K30_7CanForce);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtEthPort(self);
    OverrideAtChannel(self);
    }

static void MethodsInit(Tha60290021FaceplateSerdesEthPort self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, Interface100BaseFxIsSupported);
        mMethodOverride(m_methods, HasDisparityFeature);
        mMethodOverride(m_methods, HasK30_7Feature);
        mMethodOverride(m_methods, StartVersionSupportTxAlarmForcing);
        mMethodOverride(m_methods, ExcessiveErrorRatioIsSupported);
        mMethodOverride(m_methods, OneGeInterruptProcess);
        mMethodOverride(m_methods, Af6Reg_ramrxepacfg1_Base);
        mMethodOverride(m_methods, Af6Reg_upen_cfginsvlan_Base);
        mMethodOverride(m_methods, Af6Reg_ramrxepacfg0_Base);
        mMethodOverride(m_methods, HasClockMonitor);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021FaceplateSerdesEthPort);
    }

AtEthPort Tha60290021FaceplateSerdesEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SerdesEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->speed = cAtEthPortSpeedUnknown;

    return self;
    }

AtEthPort Tha60290021FaceplateSerdesEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021FaceplateSerdesEthPortObjectInit(newPort, portId, module);
    }

eAtRet Tha60290021ModuleEthPortBypassEnable(AtEthPort self, eBool enabled)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (Tha60290021ModuleEthIsFaceplatePort(self))
        return BypassEnable(self, enabled);

    return enabled ? cAtErrorModeNotSupport : cAtOk;
    }

eBool Tha60290021ModuleEthPortBypassIsEnabled(AtEthPort self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (Tha60290021ModuleEthIsFaceplatePort(self))
        return BypassIsEnabled(self);

    return cAtFalse;
    }

eAtRet Tha60290021ModuleEthPortSubportTxVlanSet(AtEthPort self, const tAtVlan *vlan)
    {
    eAtRet ret = SubportVlanCheck(self, vlan);
    if (ret != cAtOk)
        return ret;

    if (IsVlanValid(self, vlan) == cAtFalse)
        return cAtErrorInvlParm;

    return SubportTxVlanSet(self, vlan);
    }

eAtRet Tha60290021ModuleEthPortSubportTxVlanGet(AtEthPort self, tAtVlan *vlan)
    {
    eAtRet ret = SubportVlanCheck(self, vlan);
    if (ret != cAtOk)
        return ret;

    return SubportTxVlanGet(self, vlan);
    }

eAtRet Tha60290021ModuleEthPortSubportExpectedVlanSet(AtEthPort self, const tAtVlan *expectedVlan)
    {
    eAtRet ret = SubportVlanCheck(self, expectedVlan);
    if (ret != cAtOk)
        return ret;

    if (IsVlanValid(self, expectedVlan) == cAtFalse)
        return cAtErrorInvlParm;

    if (IsExpectedVlanIdInUseByOtherPort(self, expectedVlan))
        return cAtErrorRsrcNoAvail;

    return SubportExpectedVlanSet(self, expectedVlan);
    }

eAtRet Tha60290021ModuleEthPortSubportExpectedVlanGet(AtEthPort self, tAtVlan *expectedVlan)
    {
    eAtRet ret = SubportVlanCheck(self, expectedVlan);
    if (ret != cAtOk)
        return ret;

    return SubportExpectedVlanGet(self, expectedVlan);
    }

eBool Tha60290021ModuleEthPortBypassIsSupported(AtEthPort self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (Tha60290021ModuleEthIsFaceplatePort(self))
        return cAtTrue;

    return cAtFalse;
    }

eAtModuleEthRet Tha60290021FaceplateEthPortFlowControlHighThresholdSet(AtEthPort self, uint32 threshold)
    {
    AtEthFlowControl flowControl = AtEthPortFlowControlGet(self);
    if (flowControl)
        return AtEthFlowControlHighThresholdSet(flowControl, threshold);

    return cAtErrorModeNotSupport;
    }

uint32 Tha60290021FaceplateEthPortFlowControlHighThresholdGet(AtEthPort self)
    {
    AtEthFlowControl flowControl = AtEthPortFlowControlGet(self);
    if (flowControl)
        return AtEthFlowControlHighThresholdGet(flowControl);

    return 0;
    }

eAtModuleEthRet Tha60290021FaceplateEthPortFlowControlLowThresholdSet(AtEthPort self, uint32 threshold)
    {
    AtEthFlowControl flowControl = AtEthPortFlowControlGet(self);
    if (flowControl)
        return AtEthFlowControlLowThresholdSet(flowControl, threshold);

    return cAtErrorModeNotSupport;
    }

uint32 Tha60290021FaceplateEthPortFlowControlLowThresholdGet(AtEthPort self)
    {
    AtEthFlowControl flowControl = AtEthPortFlowControlGet(self);
    if (flowControl)
        return AtEthFlowControlLowThresholdGet(flowControl);

    return 0;
    }

uint32 Tha60290021FaceplateEthPortOneGeInterruptProcess(AtEthPort self, uint32 ethIntr)
    {
    if (self)
        return mMethodsGet(mThis(self))->OneGeInterruptProcess(self, ethIntr);
    return 0;
    }

uint32 Tha60290021FaceplateEthPortTenGeAlarmAddressWithLocalAddress(AtEthPort self, uint32 localAddress)
    {
    if (self)
        return TenGeAlarmAddressWithLocalAddress(self, localAddress);
    return cInvalidUint32;
    }

eBool Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioIsSupported(AtEthPort self)
    {
    if (self)
        return mMethodsGet(mThis(self))->ExcessiveErrorRatioIsSupported(mThis(self));
    return cAtFalse;
    }

eAtRet Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioUpperThresholdSet(AtEthPort self, uint32 threshold)
    {
    eAtRet ret = ExcessiveErrorRatioThresholdCheck(self, threshold, Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioUpperThresholdMax(self));
    if (ret == cAtOk)
        return ExcessiveErrorRatioUpperThresholdSet(self, threshold);
    return ret;
    }

uint32 Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioUpperThresholdGet(AtEthPort self)
    {
    if (self)
        return ExcessiveErrorRatioUpperThresholdGet(self);
    return 0;
    }

uint32 Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioUpperThresholdMax(AtEthPort self)
    {
    if (self)
        return ExcessiveErrorRatioUpperThresholdMax(self);
    return 0;
    }

eAtRet Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioLowerThresholdSet(AtEthPort self, uint32 threshold)
    {
    eAtRet ret = ExcessiveErrorRatioThresholdCheck(self, threshold, Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioLowerThresholdMax(self));
    if (ret == cAtOk)
        return ExcessiveErrorRatioLowerThresholdSet(self, threshold);
    return ret;
    }

uint32 Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioLowerThresholdGet(AtEthPort self)
    {
    if (self)
        return ExcessiveErrorRatioLowerThresholdGet(self);
    return cAtFalse;
    }

uint32 Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioLowerThresholdMax(AtEthPort self)
    {
    if (self)
        return ExcessiveErrorRatioLowerThresholdMax(self);
    return 0;
    }

uint32 Tha60290021FaceplateEthPortFlowControlTxWaterMarkMaxGet(AtEthPort self)
    {
    AtEthFlowControl flowControl = AtEthPortFlowControlGet(self);
    if (flowControl)
        return AtEthFlowControlTxFifoWaterMarkMaxGet(flowControl);

    return 0;
    }

uint32 Tha60290021FaceplateEthPortFlowControlTxWaterMarkMaxClear(AtEthPort self)
    {
    AtEthFlowControl flowControl = AtEthPortFlowControlGet(self);
    if (flowControl)
        return AtEthFlowControlTxFifoWaterMarkMaxClear(flowControl);

    return 0;
    }

uint32 Tha60290021FaceplateEthPortFlowControlTxWaterMarkMinGet(AtEthPort self)
    {
    AtEthFlowControl flowControl = AtEthPortFlowControlGet(self);
    if (flowControl)
        return AtEthFlowControlTxFifoWaterMarkMinGet(flowControl);

    return 0;
    }

uint32 Tha60290021FaceplateEthPortFlowControlTxWaterMarkMinClear(AtEthPort self)
    {
    AtEthFlowControl flowControl = AtEthPortFlowControlGet(self);
    if (flowControl)
        return AtEthFlowControlTxFifoWaterMarkMinClear(flowControl);

    return 0;
    }

uint32 Tha60290021FaceplateEthPortFlowControlOOBBufferLevelGet(AtEthPort self)
    {
    AtEthFlowControl flowControl = AtEthPortFlowControlGet(self);
    if (flowControl)
        return AtEthFlowControlTxFifoLevelGet(flowControl);
    return 0;
    }

uint32 Tha60290021FaceplateEthPortFlowControlTxBufferLevelGet(AtEthPort self)
    {
    AtEthFlowControl flowControl = AtEthPortFlowControlGet(self);
    if (flowControl)
        return Tha60290021EthPortFlowControlTxBufferLevelGet((Tha60290021EthPortFlowControl)flowControl);
    return 0;
    }

uint8 Tha60290021FaceplateSerdesEthPortSixteenPortGroupId(AtEthPort self)
    {
    return SixteenPortGroupId(self);
    }

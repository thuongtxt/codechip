/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290021SerdesEthPort.c
 *
 * Created Date: Jul 21, 2016
 *
 * Description : Faceplate ETH port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290021SerdesEthPortInternal.h"
#include "../../../../util/coder/AtCoderUtil.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290021SerdesEthPort *)self)
#define mSimulationDb(self) ((tTha60290021SerdesEthPortSimulationDatabase *)AtChannelSimulationDatabaseGet((AtChannel)self))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtEthPortMethods m_AtEthPortOverride;

/* Cache super implementation */
static const tAtObjectMethods *m_AtObjectMethods = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 SimulationDatabaseSize(AtChannel self)
    {
    AtUnused(self);
    return sizeof(tTha60290021SerdesEthPortSimulationDatabase);
    }

static void **SimulationDatabaseAddress(AtChannel self)
    {
    return &(mThis(self)->simDb);
    }

static eAtModuleEthRet TxIpgSet(AtEthPort self, uint8 txIpg)
    {
    tTha60290021SerdesEthPortSimulationDatabase *simDb = mSimulationDb(self);

    if (simDb == NULL)
        return cAtErrorNotReady;

    if (!AtEthPortTxIpgIsInRange(self, txIpg))
        return cAtErrorOutOfRangParm;

    simDb->txIpg = txIpg;
    return cAtOk;
    }

static uint8 TxIpgGet(AtEthPort self)
    {
    tTha60290021SerdesEthPortSimulationDatabase *simDb = mSimulationDb(self);

    if (simDb == NULL)
        return 0;

    return simDb->txIpg;
    }

static eAtModuleEthRet RxIpgSet(AtEthPort self, uint8 rxIpg)
    {
    tTha60290021SerdesEthPortSimulationDatabase *simDb = mSimulationDb(self);

    if (simDb == NULL)
        return cAtErrorNotReady;

    if (!AtEthPortRxIpgIsInRange(self, rxIpg))
        return cAtErrorOutOfRangParm;

    simDb->rxIpg = rxIpg;
    return cAtOk;
    }

static uint8 RxIpgGet(AtEthPort self)
    {
    tTha60290021SerdesEthPortSimulationDatabase *simDb = mSimulationDb(self);

    if (simDb == NULL)
        return 0;

    return simDb->rxIpg;
    }

static eAtModuleEthRet AutoNegEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool AutoNegIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool AutoNegIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    tTha60290021SerdesEthPortSimulationDatabase *simDb = mSimulationDb(self);
    if (simDb == NULL)
        return cAtErrorNotReady;

    simDb->loopbackMode = loopbackMode;
    return cAtOk;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    tTha60290021SerdesEthPortSimulationDatabase *simDb = mSimulationDb(self);
    if (simDb == NULL)
        return cAtLoopbackModeRelease;

    return simDb->loopbackMode;
    }

static eAtModuleEthRet MacCheckingEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool MacCheckingIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    return (interface == mMethodsGet(self)->DefaultInterface(self)) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    return mMethodsGet(self)->DefaultInterface(self);
    }

static eAtModuleEthRet DuplexModeSet(AtEthPort self, eAtEthPortDuplexMode duplexMode)
    {
    AtUnused(self);
    return (duplexMode == cAtEthPortWorkingModeFullDuplex) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortDuplexMode DuplexModeGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortWorkingModeFullDuplex;
    }

static eBool HwIsReady(AtChannel self)
    {
    /* Remove this overriding when hardware RD is ready */
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet DestMacAddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    address[0] = 0xCA;
    address[1] = 0xFE;
    address[2] = 0xCA;
    address[3] = 0xFE;
    address[4] = 0xCA;
    address[5] = 0xFE;
    return cAtOk;
    }

static uint8 LocalId(AtEthPort self)
    {
    AtChannel port = (AtChannel)self;
    uint8 portId = (uint8)AtChannelIdGet(port);
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet(port);
    return Tha60290021ModuleEthFaceplateLocalId(ethModule, portId);
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    AtSnprintf(description, sizeof(description), "eth_faceplate.%u", LocalId((AtEthPort)self) + 1);
    return description;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(simDb);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(tAtEthPortMethods));

        mMethodOverride(m_AtEthPortOverride, TxIpgSet);
        mMethodOverride(m_AtEthPortOverride, TxIpgGet);
        mMethodOverride(m_AtEthPortOverride, RxIpgSet);
        mMethodOverride(m_AtEthPortOverride, RxIpgGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegEnable);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsEnabled);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsSupported);
        mMethodOverride(m_AtEthPortOverride, MacCheckingEnable);
        mMethodOverride(m_AtEthPortOverride, MacCheckingIsEnabled);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, DuplexModeSet);
        mMethodOverride(m_AtEthPortOverride, DuplexModeGet);
        mMethodOverride(m_AtEthPortOverride, DestMacAddressGet);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, SimulationDatabaseSize);
        mMethodOverride(m_AtChannelOverride, SimulationDatabaseAddress);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, HwIsReady);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SerdesEthPort);
    }

AtEthPort Tha60290021SerdesEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

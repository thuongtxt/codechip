/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290021SerdesBackplaneEthSubPort.c
 *
 * Created Date: Jan 18, 2017
 *
 * Description : Subport
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/eth/AtEthPortInternal.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../physical/Tha6029SerdesEth40GReg.h"
#include "../physical/Tha60290021SerdesManager.h"
#include "Tha60290021SerdesBackplaneEthPort.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290021SerdesBackplaneEthSubPort *)self)
#define mHwId(self) AtChannelHwIdGet((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021SerdesBackplaneEthSubPort
    {
    tAtEthPort super;
    }tTha60290021SerdesBackplaneEthSubPort;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtEthPortMethods m_AtEthPortOverride;

/* To save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RegisterWithLocalAddress(AtChannel self, uint32 localAddress)
    {
    AtEthPort parent;

    if (localAddress == cInvalidUint32)
        return cInvalidUint32;

    parent = AtEthPortParentPortGet((AtEthPort)self);
    return Tha60290021SerdesBackplaneAddressWithLocalAddress(parent, localAddress);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return (cAtEthSubPortAlarmRxFramingError           |
            cAtEthSubPortAlarmRxSyncedError            |
            cAtEthSubPortAlarmRxMfLenError             |
            cAtEthSubPortAlarmRxMfRepeatError          |
            cAtEthSubPortAlarmRxMfError                |
            cAtEthSubPortAlarmRxBipError               |
            cAtEthSubPortAlarmAutoNegFecIncCantCorrect |
            cAtEthSubPortAlarmAutoNegFecIncCorrect     |
            cAtEthSubPortAlarmAutoNegFecLockError);
    }

static eBool HasInterruptMaskRegister(AtChannel self)
    {
    AtEthPort parent = AtEthPortParentPortGet((AtEthPort)self);
    return Tha60290021SerdesbackplaneEthPortHasInterruptMaskRegister(parent);
    }

static uint32 FecInterruptStatusRegister(AtChannel self)
    {
    if (HasInterruptMaskRegister(self))
        return RegisterWithLocalAddress(self, cAf6Reg_ETH_40G_FEC_AN_STICKY);
    else
        return RegisterWithLocalAddress(self, cAf6Reg_ETH_40G_FEC_STICKY_Base);
    }

static uint32 FecLockErrorMask(AtChannel self)
    {
    if (HasInterruptMaskRegister(self))
        return cBit2 << mHwId(self);
    else
        return cBit0 << mHwId(self);
    }

static uint32 FecIncCorrectMask(AtChannel self)
    {
    if (HasInterruptMaskRegister(self))
        return cBit6 << mHwId(self);
    else
        return cBit4 << mHwId(self);
    }

static uint32 FecIncCantCorrectMask(AtChannel self)
    {
    if (HasInterruptMaskRegister(self))
        return cBit10 << mHwId(self);
    else
        return cBit8 << mHwId(self);
    }

static uint32 FecDefectHistoryRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 swDefect = 0;
    uint32 hwDefect = 0;
    uint32 regAddr = FecInterruptStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 bitMask;

    bitMask = FecLockErrorMask(self);
    if (regVal & bitMask)
        {
        swDefect |= cAtEthSubPortAlarmAutoNegFecLockError;
        hwDefect |= bitMask;
        }

    bitMask = FecIncCorrectMask(self);
    if (regVal & bitMask)
        {
        swDefect |= cAtEthSubPortAlarmAutoNegFecIncCorrect;
        hwDefect |= bitMask;
        }

    bitMask = FecIncCantCorrectMask(self);
    if (regVal & bitMask)
        {
        swDefect |= cAtEthSubPortAlarmAutoNegFecIncCantCorrect;
        hwDefect |= bitMask;
        }

    if (read2Clear && hwDefect)
        mChannelHwWrite(self, regAddr, hwDefect, cAtModuleEth);

    return swDefect;
    }

static uint32 RxInterruptStatusRegister(AtChannel self)
    {
    if (HasInterruptMaskRegister(self))
        return RegisterWithLocalAddress(self, cAf6Reg_ETH_40G_RX_STICKY_New);
    else
        return RegisterWithLocalAddress(self, cAf6Reg_ETH_40G_RX_STICKY_Old);
    }

static uint32 RxFramingErrorMask(AtChannel self)
    {
    return cBit24 << mHwId(self);
    }

static uint32 RxSyncedErrorMask(AtChannel self)
    {
    return cBit20 << mHwId(self);
    }

static uint32 RxMfLenErrorMask(AtChannel self)
    {
    return cBit16 << mHwId(self);
    }

static uint32 RxMfLenRepeatMask(AtChannel self)
    {
    return cBit12 << mHwId(self);
    }

static uint32 RxMfErrorMask(AtChannel self)
    {
    return cBit0 << mHwId(self);
    }

static uint32 RxBipErrorMask(AtChannel self)
    {
    return cBit4 << mHwId(self);
    }

static uint32 RxDefectHistoryRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 swDefect = 0;
    uint32 hwDefects = 0;
    uint32 regAddr = RxInterruptStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 bitMask;

    bitMask = RxFramingErrorMask(self);
    if (regVal & bitMask)
        {
        swDefect |= cAtEthSubPortAlarmRxFramingError;
        hwDefects |= bitMask;
        }

    bitMask = RxSyncedErrorMask(self);
    if (regVal & bitMask)
        {
        swDefect |= cAtEthSubPortAlarmRxSyncedError;
        hwDefects |= bitMask;
        }

    bitMask = RxMfLenErrorMask(self);
    if (regVal & bitMask)
        {
        swDefect |= cAtEthSubPortAlarmRxMfLenError;
        hwDefects |= bitMask;
        }

    bitMask = RxMfLenRepeatMask(self);
    if (regVal & bitMask)
        {
        swDefect |= cAtEthSubPortAlarmRxMfRepeatError;
        hwDefects |= bitMask;
        }

    bitMask = RxMfErrorMask(self);
    if (regVal & bitMask)
        {
        swDefect |= cAtEthSubPortAlarmRxMfError;
        hwDefects |= bitMask;
        }

    bitMask = RxBipErrorMask(self);
    if (regVal & bitMask)
        {
        swDefect |= cAtEthSubPortAlarmRxBipError;
        hwDefects |= bitMask;
        }

    if (read2Clear && hwDefects)
        mChannelHwWrite(self, regAddr, hwDefects, cAtModuleEth);

    return swDefect;
    }

static uint32 DefectHistoryRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 swDefect = 0;

    swDefect |= FecDefectHistoryRead2Clear(self, read2Clear);
    swDefect |= RxDefectHistoryRead2Clear(self, read2Clear);

    return swDefect;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return DefectHistoryRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return DefectHistoryRead2Clear(self, cAtTrue);
    }

static uint32 RxStatusRegister(AtChannel self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_ETH_40G_RX_ALARM);
    }

static uint32 RxDefectGet(AtChannel self)
    {
    uint32 swDefect = 0;
    uint32 regAddr = RxStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 bitMask;

    bitMask = RxFramingErrorMask(self);
    if (regVal & bitMask)
        swDefect |= cAtEthSubPortAlarmRxFramingError;

    bitMask = RxSyncedErrorMask(self);
    if (regVal & bitMask)
        swDefect |= cAtEthSubPortAlarmRxSyncedError;

    bitMask = RxMfLenErrorMask(self);
    if (regVal & bitMask)
        swDefect |= cAtEthSubPortAlarmRxMfLenError;

    bitMask = RxMfLenRepeatMask(self);
    if (regVal & bitMask)
        swDefect |= cAtEthSubPortAlarmRxMfRepeatError;

    bitMask = RxMfErrorMask(self);
    if (regVal & bitMask)
        swDefect |= cAtEthSubPortAlarmRxMfError;

    bitMask = RxBipErrorMask(self);
    if (regVal & bitMask)
        swDefect |= cAtEthSubPortAlarmRxBipError;

    return swDefect;
    }

static uint32 FecStatusRegister(AtChannel self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_ETH_40G_FEC_AN_ALARM);
    }

static uint32 FecDefectGet(AtChannel self)
    {
    uint32 swDefect = 0;
    uint32 regAddr = FecStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 bitMask;

    bitMask = FecLockErrorMask(self);
    if (regVal & bitMask)
        swDefect |= cAtEthSubPortAlarmAutoNegFecLockError;

    bitMask = FecIncCorrectMask(self);
    if (regVal & bitMask)
        swDefect |= cAtEthSubPortAlarmAutoNegFecIncCorrect;

    bitMask = FecIncCantCorrectMask(self);
    if (regVal & bitMask)
        swDefect |= cAtEthSubPortAlarmAutoNegFecIncCantCorrect;

    return swDefect;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 defects = 0;

    if (!HasInterruptMaskRegister(self))
        return 0;

    defects |= RxDefectGet(self);
    defects |= FecDefectGet(self);

    return defects;
    }

static uint32 LocalId(AtSerdesController serdes)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(serdes);
    return Tha60290021SerdesManagerBackplaneSerdesLocalId(manager, AtSerdesControllerIdGet(serdes));
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    AtEthPort parent = AtEthPortParentPortGet((AtEthPort)self);
    uint32 portId = LocalId(AtEthPortSerdesController(parent));
    uint32 laneId = AtChannelIdGet((AtChannel)self);
    AtSnprintf(description, sizeof(description), "backplane.%d.%d", portId + 1, laneId + 1);
    return description;
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed10G) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed10G) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeed10G;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    if (interface == AtEthPortInterfaceGet(self))
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    return AtEthPortInterfaceGet(AtEthPortParentPortGet(self));
    }

static eAtModuleEthRet SourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet SourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static uint32 Bip8CounterAddress(AtChannel self)
    {
    uint32 laneId = AtChannelIdGet(self);

    switch (laneId)
        {
        case 0: return 0x20A4;
        case 1: return 0x20A5;
        case 2: return 0x20A6;
        case 3: return 0x20A7;
        default:
            return cInvalidUint32;
        }
    }

static uint32 ErrorCounterAddress(AtChannel self)
    {
    uint32 laneId = AtChannelIdGet(self);

    switch (laneId)
        {
        case 0: return 0x20A8;
        case 1: return 0x20A9;
        case 2: return 0x20AA;
        case 3: return 0x20AB;
        default:
            return cInvalidUint32;
        }
    }

static uint32 FecIncCorrectCounterAddress(AtChannel self)
    {
    uint32 laneId = AtChannelIdGet(self);

    switch (laneId)
        {
        case 0: return 0x20A2;
        case 1: return 0x20AD;
        case 2: return 0x20AE;
        case 3: return 0x20AF;
        default:
            return cInvalidUint32;
        }
    }

static uint32 RxFecIncCantCorrectCounterAddress(AtChannel self)
    {
    uint32 laneId = AtChannelIdGet(self);

    switch (laneId)
        {
        case 0: return 0x20A3;
        case 1: return 0x20B0;
        case 2: return 0x20B1;
        case 3: return 0x20B2;
        default:
            return cInvalidUint32;
        }
    }

static uint32 RxFecLockErrorCounterAddress(AtChannel self)
    {
    uint32 laneId = AtChannelIdGet(self);

    switch (laneId)
        {
        case 0: return 0x20B3;
        case 1: return 0x20B4;
        case 2: return 0x20B5;
        case 3: return 0x20B6;
        default:
            return cInvalidUint32;
        }
    }

static uint32 CounterAddress(AtChannel self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cAtEthPortCounterRxBip8Errors            : return Bip8CounterAddress(self);
        case cAtEthPortCounterRxErrPackets            : return ErrorCounterAddress(self);
        case cAtEthPortCounterRxFecIncCorrectCount    : return FecIncCorrectCounterAddress(self);
        case cAtEthPortCounterRxFecIncCantCorrectCount: return RxFecIncCantCorrectCounterAddress(self);
        case cAtEthPortCounterRxFecLockErrorCount     : return RxFecLockErrorCounterAddress(self);
        default:
            return cInvalidUint32;
        }
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (CounterAddress(self, counterType) == cInvalidUint32)
        return cAtFalse;
    return cAtTrue;
    }

static uint32 CounterRead2Clear(AtChannel self, uint16 counterType, eBool clear)
    {
    uint32 regAddr = RegisterWithLocalAddress(self, CounterAddress(self, counterType));
    uint32 regVal;

    if (regAddr == cInvalidUint32)
        return 0;

    regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    if (clear)
        mChannelHwWrite(self, regAddr, 0, cAtModuleEth);

    return regVal;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static eAtRet AllCounterRead2Clear(AtChannel self, void *pAllCounters, eBool read2Clear)
    {
    tAtEthPortCounters *counters = (tAtEthPortCounters *)pAllCounters;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 values;

    /* Read-only must have input counters */
    if ((pAllCounters == NULL) && (!read2Clear))
        return cAtErrorNullPointer;

    if (counters)
        mMethodsGet(osal)->MemInit(osal, counters, 0, sizeof(tAtEthPortCounters));

    mSaveCounter(rxBip8Errors,
                 CounterRead2Clear(self, cAtEthPortCounterRxBip8Errors, read2Clear),
                 cAtEthPortCounterRxBip8Errors);
    mSaveCounter(rxErrPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxErrPackets, read2Clear),
                 cAtEthPortCounterRxErrPackets);
    mSaveCounter(rxFecIncCorrectCount,
                 CounterRead2Clear(self, cAtEthPortCounterRxFecIncCorrectCount, read2Clear),
                 cAtEthPortCounterRxFecIncCorrectCount);
    mSaveCounter(rxFecIncCantCorrectCount,
                 CounterRead2Clear(self, cAtEthPortCounterRxFecIncCantCorrectCount, read2Clear),
                 cAtEthPortCounterRxFecIncCantCorrectCount);
    mSaveCounter(rxFecLockErrorCount,
                 CounterRead2Clear(self, cAtEthPortCounterRxFecLockErrorCount, read2Clear),
                 cAtEthPortCounterRxFecLockErrorCount);

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCounterRead2Clear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCounterRead2Clear(self, pAllCounters, cAtTrue);
    }

static uint32 RxInterruptMaskRegister(AtChannel self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_ETH_40G_RX_INTEN);
    }

static eAtRet RxInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = RxInterruptMaskRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 bitMask;

    if (defectMask & cAtEthSubPortAlarmRxFramingError)
        {
        bitMask = RxFramingErrorMask(self);
        if (enableMask & cAtEthSubPortAlarmRxFramingError)
            regVal |= bitMask;
        else
            regVal &= ~bitMask;
        }

    if (defectMask & cAtEthSubPortAlarmRxSyncedError)
        {
        bitMask = RxSyncedErrorMask(self);
        if (enableMask & cAtEthSubPortAlarmRxSyncedError)
            regVal |= bitMask;
        else
            regVal &= ~bitMask;
        }

    if (defectMask & cAtEthSubPortAlarmRxMfLenError)
        {
        bitMask = RxMfLenErrorMask(self);
        if (enableMask & cAtEthSubPortAlarmRxMfLenError)
            regVal |= bitMask;
        else
            regVal &= ~bitMask;
        }

    if (defectMask & cAtEthSubPortAlarmRxMfRepeatError)
        {
        bitMask = RxMfLenRepeatMask(self);
        if (enableMask & cAtEthSubPortAlarmRxMfRepeatError)
            regVal |= bitMask;
        else
            regVal &= ~bitMask;
        }

    if (defectMask & cAtEthSubPortAlarmRxMfError)
        {
        bitMask = RxMfErrorMask(self);
        if (enableMask & cAtEthSubPortAlarmRxMfError)
            regVal |= bitMask;
        else
            regVal &= ~bitMask;
        }

    if (defectMask & cAtEthSubPortAlarmRxBipError)
        {
        bitMask = RxBipErrorMask(self);
        if (enableMask & cAtEthSubPortAlarmRxBipError)
            regVal |= bitMask;
        else
            regVal &= ~bitMask;
        }

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 FecInterruptMaskRegister(AtChannel self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_ETH_40G_FEC_AN_INTEN);
    }

static eAtRet FecInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = FecInterruptMaskRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 bitMask;

    if (defectMask & cAtEthSubPortAlarmAutoNegFecLockError)
        {
        bitMask = FecLockErrorMask(self);
        if (enableMask & cAtEthSubPortAlarmAutoNegFecLockError)
            regVal |= bitMask;
        else
            regVal &= ~bitMask;
        }

    if (defectMask & cAtEthSubPortAlarmAutoNegFecIncCorrect)
        {
        bitMask = FecIncCorrectMask(self);
        if (enableMask & cAtEthSubPortAlarmAutoNegFecIncCorrect)
            regVal |= bitMask;
        else
            regVal &= ~bitMask;
        }

    if (defectMask & cAtEthSubPortAlarmAutoNegFecIncCantCorrect)
        {
        bitMask = FecIncCantCorrectMask(self);
        if (enableMask & cAtEthSubPortAlarmAutoNegFecIncCantCorrect)
            regVal |= bitMask;
        else
            regVal &= ~bitMask;
        }

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    eAtRet ret = cAtOk;

    if (!HasInterruptMaskRegister(self))
        return cAtErrorModeNotSupport;

    ret |= RxInterruptMaskSet(self, defectMask, enableMask);
    ret |= FecInterruptMaskSet(self, defectMask, enableMask);

    return ret;
    }

static uint32 RxInterruptMaskGet(AtChannel self)
    {
    uint32 defects = 0;
    uint32 regAddr = RxInterruptMaskRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 bitMask;

    bitMask = RxFramingErrorMask(self);
    if (regVal & bitMask)
        defects |= cAtEthSubPortAlarmRxFramingError;

    bitMask = RxSyncedErrorMask(self);
    if (regVal & bitMask)
        defects |= cAtEthSubPortAlarmRxSyncedError;

    bitMask = RxMfLenErrorMask(self);
    if (regVal & bitMask)
        defects |= cAtEthSubPortAlarmRxMfLenError;

    bitMask = RxMfLenRepeatMask(self);
    if (regVal & bitMask)
        defects |= cAtEthSubPortAlarmRxMfRepeatError;

    bitMask = RxMfErrorMask(self);
    if (regVal & bitMask)
        defects |= cAtEthSubPortAlarmRxMfError;

    bitMask = RxBipErrorMask(self);
    if (regVal & bitMask)
        defects |= cAtEthSubPortAlarmRxBipError;

    return defects;
    }

static uint32 FecInterruptMaskGet(AtChannel self)
    {
    uint32 defects = 0;
    uint32 regAddr = FecInterruptMaskRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 bitMask;

    bitMask = FecLockErrorMask(self);
    if (regVal & bitMask)
        defects |= cAtEthSubPortAlarmAutoNegFecLockError;

    bitMask = FecIncCorrectMask(self);
    if (regVal & bitMask)
        defects |= cAtEthSubPortAlarmAutoNegFecIncCorrect;

    bitMask = FecIncCantCorrectMask(self);
    if (regVal & bitMask)
        defects |= cAtEthSubPortAlarmAutoNegFecIncCantCorrect;

    return defects;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 defects = 0;

    if (!HasInterruptMaskRegister(self))
        return 0;

    defects |= RxInterruptMaskGet(self);
    defects |= FecInterruptMaskGet(self);

    return defects;
    }

static uint32 RxInterruptProcess(AtChannel self, uint32 offset)
    {
    uint32 intrEnReg  = RxInterruptMaskRegister(self);
    uint32 intrStaReg = RxInterruptStatusRegister(self);
    uint32 currStaReg = RxStatusRegister(self);
    uint32 intrEnVal  = mChannelHwRead(self, intrEnReg, cAtModuleEth);
    uint32 intrStaVal = mChannelHwRead(self, intrStaReg, cAtModuleEth);
    uint32 currStaVal = mChannelHwRead(self, currStaReg, cAtModuleEth);
    uint32 events = 0, defects = 0;
    uint32 intrSta2Clear = 0;
    uint32 bitMask;
    AtUnused(offset);

    /* Clear marked interrupts */
    intrStaVal &= intrEnVal;

    bitMask = RxFramingErrorMask(self);
    if (intrStaVal & bitMask)
        {
        events |= cAtEthSubPortAlarmRxFramingError;
        intrSta2Clear |= bitMask;

        if (currStaVal & bitMask)
            defects |= cAtEthSubPortAlarmRxFramingError;
        }

    bitMask = RxSyncedErrorMask(self);
    if (intrStaVal & bitMask)
        {
        events |= cAtEthSubPortAlarmRxSyncedError;
        intrSta2Clear |= bitMask;

        if (currStaVal & bitMask)
            defects |= cAtEthSubPortAlarmRxSyncedError;
        }

    bitMask = RxMfLenErrorMask(self);
    if (intrStaVal & bitMask)
        {
        events |= cAtEthSubPortAlarmRxMfLenError;
        intrSta2Clear |= bitMask;

        if (currStaVal & bitMask)
            defects |= cAtEthSubPortAlarmRxMfLenError;
        }

    bitMask = RxMfLenRepeatMask(self);
    if (intrStaVal & bitMask)
        {
        events |= cAtEthSubPortAlarmRxMfRepeatError;
        intrSta2Clear |= bitMask;

        if (currStaVal & bitMask)
            defects |= cAtEthSubPortAlarmRxMfRepeatError;
        }

    bitMask = RxMfErrorMask(self);
    if (intrStaVal & bitMask)
        {
        events |= cAtEthSubPortAlarmRxMfError;
        intrSta2Clear |= bitMask;

        if (currStaVal & bitMask)
            defects |= cAtEthSubPortAlarmRxMfError;
        }

    bitMask = RxBipErrorMask(self);
    if (intrStaVal & bitMask)
        {
        events |= cAtEthSubPortAlarmRxBipError;
        intrSta2Clear |= bitMask;

        if (currStaVal & bitMask)
            defects |= cAtEthSubPortAlarmRxBipError;
        }

    if (events)
        {
        mChannelHwWrite(self, intrStaReg, intrSta2Clear, cAtModuleEth);

        AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
        }

    return defects;
    }

static uint32 FecInterruptProcess(AtChannel self, uint32 offset)
    {
    uint32 intrEnReg  = FecInterruptMaskRegister(self);
    uint32 intrStaReg = FecInterruptStatusRegister(self);
    uint32 currStaReg = FecStatusRegister(self);
    uint32 intrEnVal  = mChannelHwRead(self, intrEnReg, cAtModuleEth);
    uint32 intrStaVal = mChannelHwRead(self, intrStaReg, cAtModuleEth);
    uint32 currStaVal = mChannelHwRead(self, currStaReg, cAtModuleEth);
    uint32 events = 0, defects = 0;
    uint32 intrSta2Clear = 0;
    uint32 bitMask;
    AtUnused(offset);

    /* Clear marked interrupts */
    intrStaVal &= intrEnVal;

    bitMask = FecLockErrorMask(self);
    if (intrStaVal & bitMask)
        {
        events |= cAtEthSubPortAlarmAutoNegFecLockError;
        intrSta2Clear |= bitMask;

        if (currStaVal & bitMask)
            defects |= cAtEthSubPortAlarmAutoNegFecLockError;
        }

    bitMask = FecIncCorrectMask(self);
    if (intrStaVal & bitMask)
        {
        events |= cAtEthSubPortAlarmAutoNegFecIncCorrect;
        intrSta2Clear |= bitMask;

        if (currStaVal & bitMask)
            defects |= cAtEthSubPortAlarmAutoNegFecIncCorrect;
        }

    bitMask = FecIncCantCorrectMask(self);
    if (intrStaVal & bitMask)
        {
        events |= cAtEthSubPortAlarmAutoNegFecIncCantCorrect;
        intrSta2Clear |= bitMask;

        if (currStaVal & bitMask)
            defects |= cAtEthSubPortAlarmAutoNegFecIncCantCorrect;
        }

    if (events)
        {
        mChannelHwWrite(self, intrStaReg, intrSta2Clear, cAtModuleEth);

        AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
        }

    return defects;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    uint32 defects = 0;

    defects |= RxInterruptProcess(self, offset);
    defects |= FecInterruptProcess(self, offset);

    return defects;
    }

static eBool MacCheckingCanEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtFalse : cAtTrue;
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, MacCheckingCanEnable);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SerdesBackplaneEthSubPort);
    }

static AtEthPort ObjectInit(AtEthPort self, uint8 subPortId, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEthPortObjectInit(self, subPortId, (AtModuleEth)AtChannelModuleGet((AtChannel)port)) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290021SerdesBackplaneEthSubPortNew(uint8 subPortId, AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPort, subPortId, port);
    }

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _THA60290021FACEPLATESERDESETHPORTREG_H_
#define _THA60290021FACEPLATESERDESETHPORTREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN REMOVED port 1-2
Reg Addr   : 0x00
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgvlan1_Base                                                                        0x00

/*--------------------------------------
BitField Name: prior_p2
BitField Type: R/W
BitField Desc: priority
BitField Bits: [31:29]
--------------------------------------*/
#define cAf6_upen_cfgvlan1_prior_p2_Mask                                                             cBit31_29
#define cAf6_upen_cfgvlan1_prior_p2_Shift                                                                   29

/*--------------------------------------
BitField Name: cfi_p2
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_cfgvlan1_cfi_p2_Mask                                                                  cBit28
#define cAf6_upen_cfgvlan1_cfi_p2_Shift                                                                     28

/*--------------------------------------
BitField Name: vlanID_p2
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_upen_cfgvlan1_vlanID_p2_Mask                                                            cBit27_16
#define cAf6_upen_cfgvlan1_vlanID_p2_Shift                                                                  16

/*--------------------------------------
BitField Name: prior_p1
BitField Type: R/W
BitField Desc: priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_upen_cfgvlan1_prior_p1_Mask                                                             cBit15_13
#define cAf6_upen_cfgvlan1_prior_p1_Shift                                                                   13

/*--------------------------------------
BitField Name: cfi_p1
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cfgvlan1_cfi_p1_Mask                                                                  cBit12
#define cAf6_upen_cfgvlan1_cfi_p1_Shift                                                                     12

/*--------------------------------------
BitField Name: vlanID_p1
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_cfgvlan1_vlanID_p1_Mask                                                             cBit11_0
#define cAf6_upen_cfgvlan1_vlanID_p1_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN REMOVED port 3-4
Reg Addr   : 0x01
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgvlan2_Base                                                                        0x01

/*--------------------------------------
BitField Name: prior_p2
BitField Type: R/W
BitField Desc: priority
BitField Bits: [31:29]
--------------------------------------*/
#define cAf6_upen_cfgvlan2_prior_p2_Mask                                                             cBit31_29
#define cAf6_upen_cfgvlan2_prior_p2_Shift                                                                   29

/*--------------------------------------
BitField Name: cfi_p2
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_cfgvlan2_cfi_p2_Mask                                                                  cBit28
#define cAf6_upen_cfgvlan2_cfi_p2_Shift                                                                     28

/*--------------------------------------
BitField Name: vlanID_p2
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_upen_cfgvlan2_vlanID_p2_Mask                                                            cBit27_16
#define cAf6_upen_cfgvlan2_vlanID_p2_Shift                                                                  16

/*--------------------------------------
BitField Name: prior_p1
BitField Type: R/W
BitField Desc: priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_upen_cfgvlan2_prior_p1_Mask                                                             cBit15_13
#define cAf6_upen_cfgvlan2_prior_p1_Shift                                                                   13

/*--------------------------------------
BitField Name: cfi_p1
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cfgvlan2_cfi_p1_Mask                                                                  cBit12
#define cAf6_upen_cfgvlan2_cfi_p1_Shift                                                                     12

/*--------------------------------------
BitField Name: vlanID_p1
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_cfgvlan2_vlanID_p1_Mask                                                             cBit11_0
#define cAf6_upen_cfgvlan2_vlanID_p1_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN REMOVED port 5-6
Reg Addr   : 0x02
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgvlan3_Base                                                                        0x02

/*--------------------------------------
BitField Name: prior_p2
BitField Type: R/W
BitField Desc: priority
BitField Bits: [31:29]
--------------------------------------*/
#define cAf6_upen_cfgvlan3_prior_p2_Mask                                                             cBit31_29
#define cAf6_upen_cfgvlan3_prior_p2_Shift                                                                   29

/*--------------------------------------
BitField Name: cfi_p2
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_cfgvlan3_cfi_p2_Mask                                                                  cBit28
#define cAf6_upen_cfgvlan3_cfi_p2_Shift                                                                     28

/*--------------------------------------
BitField Name: vlanID_p2
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_upen_cfgvlan3_vlanID_p2_Mask                                                            cBit27_16
#define cAf6_upen_cfgvlan3_vlanID_p2_Shift                                                                  16

/*--------------------------------------
BitField Name: prior_p1
BitField Type: R/W
BitField Desc: priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_upen_cfgvlan3_prior_p1_Mask                                                             cBit15_13
#define cAf6_upen_cfgvlan3_prior_p1_Shift                                                                   13

/*--------------------------------------
BitField Name: cfi_p1
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cfgvlan3_cfi_p1_Mask                                                                  cBit12
#define cAf6_upen_cfgvlan3_cfi_p1_Shift                                                                     12

/*--------------------------------------
BitField Name: vlanID_p1
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_cfgvlan3_vlanID_p1_Mask                                                             cBit11_0
#define cAf6_upen_cfgvlan3_vlanID_p1_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN REMOVED port 7-8
Reg Addr   : 0x03
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgvlan4_Base                                                                        0x03

/*--------------------------------------
BitField Name: prior_p2
BitField Type: R/W
BitField Desc: priority
BitField Bits: [31:29]
--------------------------------------*/
#define cAf6_upen_cfgvlan4_prior_p2_Mask                                                             cBit31_29
#define cAf6_upen_cfgvlan4_prior_p2_Shift                                                                   29

/*--------------------------------------
BitField Name: cfi_p2
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_cfgvlan4_cfi_p2_Mask                                                                  cBit28
#define cAf6_upen_cfgvlan4_cfi_p2_Shift                                                                     28

/*--------------------------------------
BitField Name: vlanID_p2
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_upen_cfgvlan4_vlanID_p2_Mask                                                            cBit27_16
#define cAf6_upen_cfgvlan4_vlanID_p2_Shift                                                                  16

/*--------------------------------------
BitField Name: prior_p1
BitField Type: R/W
BitField Desc: priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_upen_cfgvlan4_prior_p1_Mask                                                             cBit15_13
#define cAf6_upen_cfgvlan4_prior_p1_Shift                                                                   13

/*--------------------------------------
BitField Name: cfi_p1
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cfgvlan4_cfi_p1_Mask                                                                  cBit12
#define cAf6_upen_cfgvlan4_cfi_p1_Shift                                                                     12

/*--------------------------------------
BitField Name: vlanID_p1
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_cfgvlan4_vlanID_p1_Mask                                                             cBit11_0
#define cAf6_upen_cfgvlan4_vlanID_p1_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN REMOVED  port 9-10
Reg Addr   : 0x04
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgvlan5_Base                                                                        0x04

/*--------------------------------------
BitField Name: prior_p2
BitField Type: R/W
BitField Desc: priority
BitField Bits: [31:29]
--------------------------------------*/
#define cAf6_upen_cfgvlan5_prior_p2_Mask                                                             cBit31_29
#define cAf6_upen_cfgvlan5_prior_p2_Shift                                                                   29

/*--------------------------------------
BitField Name: cfi_p2
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_cfgvlan5_cfi_p2_Mask                                                                  cBit28
#define cAf6_upen_cfgvlan5_cfi_p2_Shift                                                                     28

/*--------------------------------------
BitField Name: vlanID_p2
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_upen_cfgvlan5_vlanID_p2_Mask                                                            cBit27_16
#define cAf6_upen_cfgvlan5_vlanID_p2_Shift                                                                  16

/*--------------------------------------
BitField Name: prior_p1
BitField Type: R/W
BitField Desc: priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_upen_cfgvlan5_prior_p1_Mask                                                             cBit15_13
#define cAf6_upen_cfgvlan5_prior_p1_Shift                                                                   13

/*--------------------------------------
BitField Name: cfi_p1
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cfgvlan5_cfi_p1_Mask                                                                  cBit12
#define cAf6_upen_cfgvlan5_cfi_p1_Shift                                                                     12

/*--------------------------------------
BitField Name: vlanID_p1
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_cfgvlan5_vlanID_p1_Mask                                                             cBit11_0
#define cAf6_upen_cfgvlan5_vlanID_p1_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN REMOVED  port 11-12
Reg Addr   : 0x05
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgvlan6_Base                                                                        0x05

/*--------------------------------------
BitField Name: prior_p2
BitField Type: R/W
BitField Desc: priority
BitField Bits: [31:29]
--------------------------------------*/
#define cAf6_upen_cfgvlan6_prior_p2_Mask                                                             cBit31_29
#define cAf6_upen_cfgvlan6_prior_p2_Shift                                                                   29

/*--------------------------------------
BitField Name: cfi_p2
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_cfgvlan6_cfi_p2_Mask                                                                  cBit28
#define cAf6_upen_cfgvlan6_cfi_p2_Shift                                                                     28

/*--------------------------------------
BitField Name: vlanID_p2
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_upen_cfgvlan6_vlanID_p2_Mask                                                            cBit27_16
#define cAf6_upen_cfgvlan6_vlanID_p2_Shift                                                                  16

/*--------------------------------------
BitField Name: prior_p1
BitField Type: R/W
BitField Desc: priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_upen_cfgvlan6_prior_p1_Mask                                                             cBit15_13
#define cAf6_upen_cfgvlan6_prior_p1_Shift                                                                   13

/*--------------------------------------
BitField Name: cfi_p1
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cfgvlan6_cfi_p1_Mask                                                                  cBit12
#define cAf6_upen_cfgvlan6_cfi_p1_Shift                                                                     12

/*--------------------------------------
BitField Name: vlanID_p1
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_cfgvlan6_vlanID_p1_Mask                                                             cBit11_0
#define cAf6_upen_cfgvlan6_vlanID_p1_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN REMOVED port 13-14
Reg Addr   : 0x06
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgvlan7_Base                                                                        0x06

/*--------------------------------------
BitField Name: prior_p2
BitField Type: R/W
BitField Desc: priority
BitField Bits: [31:29]
--------------------------------------*/
#define cAf6_upen_cfgvlan7_prior_p2_Mask                                                             cBit31_29
#define cAf6_upen_cfgvlan7_prior_p2_Shift                                                                   29

/*--------------------------------------
BitField Name: cfi_p2
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_cfgvlan7_cfi_p2_Mask                                                                  cBit28
#define cAf6_upen_cfgvlan7_cfi_p2_Shift                                                                     28

/*--------------------------------------
BitField Name: vlanID_p2
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_upen_cfgvlan7_vlanID_p2_Mask                                                            cBit27_16
#define cAf6_upen_cfgvlan7_vlanID_p2_Shift                                                                  16

/*--------------------------------------
BitField Name: prior_p1
BitField Type: R/W
BitField Desc: priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_upen_cfgvlan7_prior_p1_Mask                                                             cBit15_13
#define cAf6_upen_cfgvlan7_prior_p1_Shift                                                                   13

/*--------------------------------------
BitField Name: cfi_p1
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cfgvlan7_cfi_p1_Mask                                                                  cBit12
#define cAf6_upen_cfgvlan7_cfi_p1_Shift                                                                     12

/*--------------------------------------
BitField Name: vlanID_p1
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_cfgvlan7_vlanID_p1_Mask                                                             cBit11_0
#define cAf6_upen_cfgvlan7_vlanID_p1_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN REMOVED port 15-16
Reg Addr   : 0x07
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgvlan8_Base                                                                        0x07

/*--------------------------------------
BitField Name: prior_p2
BitField Type: R/W
BitField Desc: priority
BitField Bits: [31:29]
--------------------------------------*/
#define cAf6_upen_cfgvlan8_prior_p2_Mask                                                             cBit31_29
#define cAf6_upen_cfgvlan8_prior_p2_Shift                                                                   29

/*--------------------------------------
BitField Name: cfi_p2
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_cfgvlan8_cfi_p2_Mask                                                                  cBit28
#define cAf6_upen_cfgvlan8_cfi_p2_Shift                                                                     28

/*--------------------------------------
BitField Name: vlanID_p2
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_upen_cfgvlan8_vlanID_p2_Mask                                                            cBit27_16
#define cAf6_upen_cfgvlan8_vlanID_p2_Shift                                                                  16

/*--------------------------------------
BitField Name: prior_p1
BitField Type: R/W
BitField Desc: priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_upen_cfgvlan8_prior_p1_Mask                                                             cBit15_13
#define cAf6_upen_cfgvlan8_prior_p1_Shift                                                                   13

/*--------------------------------------
BitField Name: cfi_p1
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cfgvlan8_cfi_p1_Mask                                                                  cBit12
#define cAf6_upen_cfgvlan8_cfi_p1_Shift                                                                     12

/*--------------------------------------
BitField Name: vlanID_p1
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_cfgvlan8_vlanID_p1_Mask                                                             cBit11_0
#define cAf6_upen_cfgvlan8_vlanID_p1_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN REMOVED PW
Reg Addr   : 0x08
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_pwvlan_Base                                                                      0x08

/*--------------------------------------
BitField Name: prior_2
BitField Type: R/W
BitField Desc: priority
BitField Bits: [31:29]
--------------------------------------*/
#define cAf6_upen_cfg_pwvlan_prior_2_Mask                                                            cBit31_29
#define cAf6_upen_cfg_pwvlan_prior_2_Shift                                                                  29

/*--------------------------------------
BitField Name: cfi_2
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_cfg_pwvlan_cfi_2_Mask                                                                 cBit28
#define cAf6_upen_cfg_pwvlan_cfi_2_Shift                                                                    28

/*--------------------------------------
BitField Name: vlanID_2
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [27:16]
--------------------------------------*/
#define cAf6_upen_cfg_pwvlan_vlanID_2_Mask                                                           cBit27_16
#define cAf6_upen_cfg_pwvlan_vlanID_2_Shift                                                                 16

/*--------------------------------------
BitField Name: prior_1
BitField Type: R/W
BitField Desc: priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_upen_cfg_pwvlan_prior_1_Mask                                                            cBit15_13
#define cAf6_upen_cfg_pwvlan_prior_1_Shift                                                                  13

/*--------------------------------------
BitField Name: cfi_1
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cfg_pwvlan_cfi_1_Mask                                                                 cBit12
#define cAf6_upen_cfg_pwvlan_cfi_1_Shift                                                                    12

/*--------------------------------------
BitField Name: vlanID_1
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_cfg_pwvlan_vlanID_1_Mask                                                            cBit11_0
#define cAf6_upen_cfg_pwvlan_vlanID_1_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE REMOVE VLAN TPID
Reg Addr   : 0x10
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enbtpid_Base                                                                         0x10
#define cAf6Reg_upen_enbtpid_WidthVal                                                                       32

/*--------------------------------------
BitField Name: out_enbtpid_pw
BitField Type: R/W
BitField Desc: enable compare PW VLAN,
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_upen_enbtpid_out_enbtpid_pw_Mask                                                           cBit17
#define cAf6_upen_enbtpid_out_enbtpid_pw_Shift                                                              17

/*--------------------------------------
BitField Name: out_cfg_tpid
BitField Type: R/W
BitField Desc: bit corresponding port to config ( bit 0 is port 0),
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_enbtpid_out_cfg_tpid_Mask                                                           cBit15_0
#define cAf6_upen_enbtpid_out_cfg_tpid_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE REMOVE VLAN TPID
Reg Addr   : 0x11
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgrmtpid_Base                                                                       0x11

/*--------------------------------------
BitField Name: out_cfgtpid2
BitField Type: R/W
BitField Desc: value TPID 2
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_cfgrmtpid_out_cfgtpid2_Mask                                                        cBit31_16
#define cAf6_upen_cfgrmtpid_out_cfgtpid2_Shift                                                              16

/*--------------------------------------
BitField Name: out_cfgtpid1
BitField Type: R/W
BitField Desc: value TPID 1
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_cfgrmtpid_out_cfgtpid1_Mask                                                         cBit15_0
#define cAf6_upen_cfgrmtpid_out_cfgtpid1_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE REMOVE VLAN TPID
Reg Addr   : 0x12
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgrmtpid_pw_Base                                                                    0x12

/*--------------------------------------
BitField Name: out_cfgtpid_pw2
BitField Type: R/W
BitField Desc: value TPID PW 2
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_cfgrmtpid_pw_out_cfgtpid_pw2_Mask                                                  cBit31_16
#define cAf6_upen_cfgrmtpid_pw_out_cfgtpid_pw2_Shift                                                        16

/*--------------------------------------
BitField Name: out_cfgtpid_pw1
BitField Type: R/W
BitField Desc: value TPID PW 1
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_cfgrmtpid_pw_out_cfgtpid_pw1_Mask                                                   cBit15_0
#define cAf6_upen_cfgrmtpid_pw_out_cfgtpid_pw1_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN INSERTED
Reg Addr   : 0x100-0x107
Reg Formula: 0x100+ $PID
    Where  : 
           + $PID(0-15) : Port ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfginsvlan_Base                                                                     0x100
#define cAf6Reg_upen_cfginsvlan_WidthVal                                                                    32

/*--------------------------------------
BitField Name: prior
BitField Type: R/W
BitField Desc: priority
BitField Bits: [15:13]
--------------------------------------*/
#define cAf6_upen_cfginsvlan_prior_Mask                                                              cBit15_13
#define cAf6_upen_cfginsvlan_prior_Shift                                                                    13

/*--------------------------------------
BitField Name: cfi
BitField Type: R/W
BitField Desc: CFI bit
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_upen_cfginsvlan_cfi_Mask                                                                   cBit12
#define cAf6_upen_cfginsvlan_cfi_Shift                                                                      12

/*--------------------------------------
BitField Name: vlanID
BitField Type: R/W
BitField Desc: VLan ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_cfginsvlan_vlanID_Mask                                                              cBit11_0
#define cAf6_upen_cfginsvlan_vlanID_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE INSERT VLAN TPID
Reg Addr   : 0x110
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enbinstpid_Base                                                                     0x110
#define cAf6Reg_upen_enbinstpid_WidthVal                                                                    32

/*--------------------------------------
BitField Name: out_enbtpid
BitField Type: R/W
BitField Desc: bit corresponding port to config ( bit 0 is port 0),(0)  use
config TPID1, (1) use config TPID 2
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_enbinstpid_out_enbtpid_Mask                                                         cBit15_0
#define cAf6_upen_enbinstpid_out_enbtpid_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE INSERT VLAN TPID
Reg Addr   : 0x111
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfginstpid_Base                                                                     0x111

/*--------------------------------------
BitField Name: out_cfgtpid2
BitField Type: R/W
BitField Desc: valud TPID 2
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_cfginstpid_out_cfgtpid2_Mask                                                       cBit31_16
#define cAf6_upen_cfginstpid_out_cfgtpid2_Shift                                                             16

/*--------------------------------------
BitField Name: out_cfgtpid1
BitField Type: R/W
BitField Desc: value TPID 1
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_cfginstpid_out_cfgtpid1_Mask                                                        cBit15_0
#define cAf6_upen_cfginstpid_out_cfgtpid1_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE INSERT VLAN TPID
Reg Addr   : 0x112
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enbins_Base                                                                         0x112
#define cAf6Reg_upen_enbins_WidthVal                                                                        32

/*--------------------------------------
BitField Name: out_enbins
BitField Type: R/W
BitField Desc: bit corresponding port to config ( bit 0 is port 0), (0) is
disable, (1) is enable
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_enbins_out_enbins_Mask                                                              cBit15_0
#define cAf6_upen_enbins_out_enbins_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG THRESHOLD
Reg Addr   : 0x200-0x20F
Reg Formula: 0x200+ $PID
    Where  : 
           + $PID(0-15) : Port ID
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgthreshold_Base                                                                   0x200
#define cAf6Reg_upen_cfgthreshold_WidthVal                                                                  32

/*--------------------------------------
BitField Name: val_thres2
BitField Type: R/W
BitField Desc: threshold for 3/4 of max buffer contain (256*3/4)kbytes
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_cfgthreshold_val_thres2_Mask                                                        cBit15_8
#define cAf6_upen_cfgthreshold_val_thres2_Shift                                                              8

/*--------------------------------------
BitField Name: val_thes1
BitField Type: R/W
BitField Desc: threshold for 1/4 of max buffer contain (256/4)kbytes
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_cfgthreshold_val_thes1_Mask                                                          cBit7_0
#define cAf6_upen_cfgthreshold_val_thes1_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE FLOW CONTROL
Reg Addr   : 0x210
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enbflwctrl_Base                                                                     0x210
#define cAf6Reg_upen_enbflwctrl_WidthVal                                                                    32

/*--------------------------------------
BitField Name: out_enbflw
BitField Type: R/W
BitField Desc: (0) is disable, (1) is enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_enbflwctrl_out_enbflw_Mask                                                             cBit0
#define cAf6_upen_enbflwctrl_out_enbflw_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Hold Register Status
Reg Addr   : 0x80
Reg Formula: 0x80 +  HID
    Where  : 
           + $HID(0-0): Hold ID
Reg Desc   : 
This register using for hold remain that more than 128bits

------------------------------------------------------------------------------*/
#define cAf6Reg_epa_hold_status_Base                                                                      0x80

/*--------------------------------------
BitField Name: EpaHoldStatus
BitField Type: RW
BitField Desc: Hold 32bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_epa_hold_status_EpaHoldStatus_Mask                                                       cBit31_0
#define cAf6_epa_hold_status_EpaHoldStatus_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group1 Counter
Reg Addr   : 0xA00 - 0xA2F(RO)
Reg Formula: 0xA00 + 16*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-15)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txepagrp1cnt_Base                                                                   0xA00

/*--------------------------------------
BitField Name: txethgrp1cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : txpkt255byte cntoffset = 1 : txpkt1023byte
cntoffset = 2 : txpktjumbo
BitField Bits: [47:24]
--------------------------------------*/
#define cAf6_upen_txepagrp1cnt_txethgrp1cnt1_01_Mask                                                 cBit31_24
#define cAf6_upen_txepagrp1cnt_txethgrp1cnt1_01_Shift                                                       24
#define cAf6_upen_txepagrp1cnt_txethgrp1cnt1_02_Mask                                                  cBit15_0
#define cAf6_upen_txepagrp1cnt_txethgrp1cnt1_02_Shift                                                        0

/*--------------------------------------
BitField Name: txethgrp1cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : txpkt127byte cntoffset = 1 : txpkt511byte
cntoffset = 2 : txpkt1518byte
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_txepagrp1cnt_txethgrp1cnt0_Mask                                                     cBit23_0
#define cAf6_upen_txepagrp1cnt_txethgrp1cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group2 Counter
Reg Addr   : 0xA80 - 0xAAF(RO)
Reg Formula: 0xA80 + 16*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-15)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txepagrp2cnt_Base                                                                   0xA80

/*--------------------------------------
BitField Name: txethgrp2cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : txpktgood cntoffset = 1 : unused cntoffset = 2 :
txoversize
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_txepagrp2cnt_txethgrp2cnt1_Mask                                                     cBit31_0
#define cAf6_upen_txepagrp2cnt_txethgrp2cnt1_Shift                                                           0

/*--------------------------------------
BitField Name: txethgrp2cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : txbytecnt cntoffset = 1 : txpkttotaldis cntoffset
= 2 : txundersize
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txepagrp2cnt_txethgrp2cnt0_Mask                                                     cBit31_0
#define cAf6_upen_txepagrp2cnt_txethgrp2cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group3 Counter
Reg Addr   : 0xB00 - 0xB2F(RC)
Reg Formula: 0xB00 + 16*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-15)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txepagrp3cnt_Base                                                                   0xB00

/*--------------------------------------
BitField Name: txethgrp1cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : txpktoverrun cntoffset = 1 : txpktmulticast
cntoffset = 2 : unused
BitField Bits: [47:24]
--------------------------------------*/
#define cAf6_upen_txepagrp3cnt_txethgrp1cnt1_01_Mask                                                 cBit31_24
#define cAf6_upen_txepagrp3cnt_txethgrp1cnt1_01_Shift                                                       24
#define cAf6_upen_txepagrp3cnt_txethgrp1cnt1_02_Mask                                                  cBit15_0
#define cAf6_upen_txepagrp3cnt_txethgrp1cnt1_02_Shift                                                        0

/*--------------------------------------
BitField Name: txethgrp1cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : txpktpausefrm cntoffset = 1 : txpktbroadcast
cntoffset = 2 : txpkt64byte
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_txepagrp3cnt_txethgrp1cnt0_Mask                                                     cBit23_0
#define cAf6_upen_txepagrp3cnt_txethgrp1cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group1 Counter
Reg Addr   : 0xE00 - 0xE2F(RO)
Reg Formula: 0xE00 + 16*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-15)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxepagrp1cnt_Base                                                                   0xE00

/*--------------------------------------
BitField Name: rxethgrp1cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpkt255byte cntoffset = 1 : rxpkt1023byte
cntoffset = 2 : rxpktjumbo
BitField Bits: [47:24]
--------------------------------------*/
#define cAf6_upen_rxepagrp1cnt_rxethgrp1cnt1_01_Mask                                                 cBit31_24
#define cAf6_upen_rxepagrp1cnt_rxethgrp1cnt1_01_Shift                                                       24
#define cAf6_upen_rxepagrp1cnt_rxethgrp1cnt1_02_Mask                                                  cBit15_0
#define cAf6_upen_rxepagrp1cnt_rxethgrp1cnt1_02_Shift                                                        0

/*--------------------------------------
BitField Name: rxethgrp1cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpkt127byte cntoffset = 1 : rxpkt511byte
cntoffset = 2 : rxpkt1518byte
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_rxepagrp1cnt_rxethgrp1cnt0_Mask                                                     cBit23_0
#define cAf6_upen_rxepagrp1cnt_rxethgrp1cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group2 Counter
Reg Addr   : 0xE80 - 0xEAF(RO)
Reg Formula: 0xE80 + 16*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-15)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxepagrp2cnt_Base                                                                   0xE80

/*--------------------------------------
BitField Name: rxethgrp2cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpkttotal cntoffset = 1 : unused cntoffset = 2 :
rxpktdaloop
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_rxepagrp2cnt_rxethgrp2cnt1_Mask                                                     cBit31_0
#define cAf6_upen_rxepagrp2cnt_rxethgrp2cnt1_Shift                                                           0

/*--------------------------------------
BitField Name: rxethgrp2cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : rxbytecnt cntoffset = 1 : rxpktfcserr cntoffset =
2 : rxpkttotaldis
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxepagrp2cnt_rxethgrp2cnt0_Mask                                                     cBit31_0
#define cAf6_upen_rxepagrp2cnt_rxethgrp2cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group3 Counter
Reg Addr   : 0xF00 - 0xF2F(RO)
Reg Formula: 0xF00 + 16*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-15)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxepagrp3cnt_Base                                                                   0xF00

/*--------------------------------------
BitField Name: rxethgrp2cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpktjabber cntoffset = 1 : rxpktpausefrm
cntoffset = 2 : rxpktoversize
BitField Bits: [47:24]
--------------------------------------*/
#define cAf6_upen_rxepagrp3cnt_rxethgrp2cnt1_01_Mask                                                 cBit31_24
#define cAf6_upen_rxepagrp3cnt_rxethgrp2cnt1_01_Shift                                                       24
#define cAf6_upen_rxepagrp3cnt_rxethgrp2cnt1_02_Mask                                                  cBit15_0
#define cAf6_upen_rxepagrp3cnt_rxethgrp2cnt1_02_Shift                                                        0

/*--------------------------------------
BitField Name: rxethgrp2cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpktpcserr cntoffset = 1 : rxpktfragment
cntoffset = 2 : rxpktundersize
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_rxepagrp3cnt_rxethgrp2cnt0_Mask                                                     cBit23_0
#define cAf6_upen_rxepagrp3cnt_rxethgrp2cnt0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group4 Counter
Reg Addr   : 0xF80 - 0xFBF(RO)
Reg Formula: 0xF80 + 16*$cntoffset + $ethpid
    Where  : 
           + $ethpid(0-15)
           + $cntoffset(0-2)
Reg Desc   : 
The register count information as below. Depending on cntoffset it 's the events specify.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxepagrp4cnt_Base                                                                   0xF80

/*--------------------------------------
BitField Name: rxethgrp2cnt1
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpktbroadcast cntoffset = 1 : rxpkt64byte
cntoffset = 2 : unused
BitField Bits: [47:24]
--------------------------------------*/
#define cAf6_upen_rxepagrp4cnt_rxethgrp2cnt1_01_Mask                                                 cBit31_24
#define cAf6_upen_rxepagrp4cnt_rxethgrp2cnt1_01_Shift                                                       24
#define cAf6_upen_rxepagrp4cnt_rxethgrp2cnt1_02_Mask                                                  cBit15_0
#define cAf6_upen_rxepagrp4cnt_rxethgrp2cnt1_02_Shift                                                        0

/*--------------------------------------
BitField Name: rxethgrp2cnt0
BitField Type: RC
BitField Desc: cntoffset = 0 : rxpktoverrun cntoffset = 1 : rxpktmulticast
cntoffset = 2 : unused
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_rxepagrp4cnt_rxethgrp2cnt0_Mask                                                     cBit23_0
#define cAf6_upen_rxepagrp4cnt_rxethgrp2cnt0_Shift                                                           0

/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through GE Alarm
Reg Addr   : 0x090
Reg Formula:
    Where  :
Reg Desc   :
The register show GE alarm

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_epagestk0_Base                                                                      0x090

/*--------------------------------------
BitField Name: GeExErrorRatio
BitField Type: W1C
BitField Desc: bit corresponding ge port (bit 0 is port 0), (1) is Excess , (0)
is ok
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_epagestk0_GeExErrorRatio_Mask                                                      cBit31_16
#define cAf6_upen_epagestk0_GeExErrorRatio_Shift                                                            16


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through TENGE Alarm
Reg Addr   : 0x092
Reg Formula:
    Where  :
Reg Desc   :
The register show TENGE alarm

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_epatengestk0_Base                                                                   0x092
#define cAf6Reg_upen_epatengestk0_WidthVal                                                                  32

/*--------------------------------------
BitField Name: TenGeLossDataOrFrameSync
BitField Type: W1C
BitField Desc: bit corresponding tenge port (bit 0 is port 0, bit 1 is port 8),
(1) is Loss, (0) is ok
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_upen_epatengestk0_TenGeLossDataOrFrameSync_Mask                                           cBit1_0
#define cAf6_upen_epatengestk0_TenGeLossDataOrFrameSync_Shift                                                0

/*--------------------------------------
BitField Name: TenGeExErrorRatio
BitField Type: W1C
BitField Desc: bit corresponding tenge port (bit 0 is port 0, bit 1 is port 8),
(1) is Excess, (0) is ok
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_upen_epatengestk0_TenGeExErrorRatio_Mask                                                  cBit3_2
#define cAf6_upen_epatengestk0_TenGeExErrorRatio_Shift                                                       2

/*--------------------------------------
BitField Name: TenGeClockLoss
BitField Type: W1C
BitField Desc: bit corresponding tenge port (bit 0 is port 0, bit 1 is port 8),
(1) is Loss, (0) is ok
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_upen_epatengestk0_TenGeClockLoss_Mask                                                     cBit5_4
#define cAf6_upen_epatengestk0_TenGeClockLoss_Shift                                                          4

/*--------------------------------------
BitField Name: TenGeClockOutRange
BitField Type: W1C
BitField Desc: bit corresponding tenge port (bit 0 is port 0, bit 1 is port 8),
(1) is Out Of Range, (0) is ok
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_upen_epatengestk0_TenGeClockOutRange_Mask                                                 cBit7_6
#define cAf6_upen_epatengestk0_TenGeClockOutRange_Shift                                                      6

/*--------------------------------------
BitField Name: TenGeLocalFault
BitField Type: W1C
BitField Desc: bit corresponding tenge port (bit 0 is port 0, bit 1 is port 8),
(1) is local fault, (0) is ok
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_upen_epatengestk0_TenGeLocalFault_Mask                                                    cBit9_8
#define cAf6_upen_epatengestk0_TenGeLocalFault_Shift                                                         8

/*--------------------------------------
BitField Name: TenGeRemoteFault
BitField Type: W1C
BitField Desc: bit corresponding tenge port (bit 0 is port 0, bit 1 is port 8),
(1) is remote fault, (0) is ok
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_upen_epatengestk0_TenGeRemoteFault_Mask                                                 cBit11_10
#define cAf6_upen_epatengestk0_TenGeRemoteFault_Shift                                                       10


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through GE Status
Reg Addr   : 0x094
Reg Formula:
    Where  :
Reg Desc   :
The register show GE status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_epagesta0_Base                                                                      0x094

/*--------------------------------------
BitField Name: GeExErrorRatio_Sta
BitField Type: RO
BitField Desc: bit corresponding ge port (bit 0 is port 0), (1) is Excess , (0)
is ok
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_epagesta0_GeExErrorRatio_Sta_Mask                                                  cBit31_16
#define cAf6_upen_epagesta0_GeExErrorRatio_Sta_Shift                                                        16


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through TENGE Status
Reg Addr   : 0x096
Reg Formula:
    Where  :
Reg Desc   :
The register show TENGE status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_epatengesta0_Base                                                                   0x096
#define cAf6Reg_upen_epatengesta0_WidthVal                                                                  32

/*--------------------------------------
BitField Name: TenGeLossDataOrFrameSync_Sta
BitField Type: RO
BitField Desc: bit corresponding tenge port (bit 0 is port 0, bit 1 is port 8),
(1) is Loss, (0) is ok
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_upen_epatengesta0_TenGeLossDataOrFrameSync_Sta_Mask                                       cBit1_0
#define cAf6_upen_epatengesta0_TenGeLossDataOrFrameSync_Sta_Shift                                            0

/*--------------------------------------
BitField Name: TenGeExErrorRatio_Sta
BitField Type: RO
BitField Desc: bit corresponding tenge port (bit 0 is port 0, bit 1 is port 8),
(1) is Excess, (0) is ok
BitField Bits: [3:2]
--------------------------------------*/
#define cAf6_upen_epatengesta0_TenGeExErrorRatio_Sta_Mask                                              cBit3_2
#define cAf6_upen_epatengesta0_TenGeExErrorRatio_Sta_Shift                                                   2

/*--------------------------------------
BitField Name: TenGeClockLoss_Sta
BitField Type: RO
BitField Desc: bit corresponding tenge port (bit 0 is port 0, bit 1 is port 8),
(1) is Loss, (0) is ok
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_upen_epatengesta0_TenGeClockLoss_Sta_Mask                                                 cBit5_4
#define cAf6_upen_epatengesta0_TenGeClockLoss_Sta_Shift                                                      4

/*--------------------------------------
BitField Name: TenGeClockOutRange_Sta
BitField Type: RO
BitField Desc: bit corresponding tenge port (bit 0 is port 0, bit 1 is port 8),
(1) is Out Of Range, (0) is ok
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_upen_epatengesta0_TenGeClockOutRange_Sta_Mask                                             cBit7_6
#define cAf6_upen_epatengesta0_TenGeClockOutRange_Sta_Shift                                                  6

/*--------------------------------------
BitField Name: TenGeLocalFault_Sta
BitField Type: RO
BitField Desc: bit corresponding tenge port (bit 0 is port 0, bit 1 is port 8),
(1) is local fault, (0) is ok
BitField Bits: [9:8]
--------------------------------------*/
#define cAf6_upen_epatengesta0_TenGeLocalFault_Sta_Mask                                                cBit9_8
#define cAf6_upen_epatengesta0_TenGeLocalFault_Sta_Shift                                                     8

/*--------------------------------------
BitField Name: TenGeRemoteFault_Sta
BitField Type: RO
BitField Desc: bit corresponding tenge port (bit 0 is port 0, bit 1 is port 8),
(1) is remote fault, (0) is ok
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_upen_epatengesta0_TenGeRemoteFault_Sta_Mask                                             cBit11_10
#define cAf6_upen_epatengesta0_TenGeRemoteFault_Sta_Shift                                                   10

/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN1 INSERTED PW
Reg Addr   : 0x0A0
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgpwvlan1_Base                                                                     0x0A0

/*--------------------------------------
BitField Name: PwSubVlan1
BitField Type: R/W
BitField Desc: Pw subport VLAN1 value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cfgpwvlan1_PwSubVlan1_Mask                                                          cBit31_0
#define cAf6_upen_cfgpwvlan1_PwSubVlan1_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN2 INSERTED PW
Reg Addr   : 0x0A1
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgpwvlan2_Base                                                                     0x0A1

/*--------------------------------------
BitField Name: PwSubVlan2
BitField Type: R/W
BitField Desc: Pw subport VLAN2 value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_cfgpwvlan2_PwSubVlan2_Mask                                                          cBit31_0
#define cAf6_upen_cfgpwvlan2_PwSubVlan2_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG VLAN INSERTED PW ENABLE
Reg Addr   : 0x0A2
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgpwvlaninsen_Base                                                                 0x0A2
#define cAf6Reg_upen_cfgpwvlaninsen_WidthVal                                                                32

/*--------------------------------------
BitField Name: PwVlanSel
BitField Type: R/W
BitField Desc: Select VLAN to insert 0: select VLAN1 1: select VLAN2
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_cfgpwvlaninsen_PwVlanSel_Mask                                                          cBit1
#define cAf6_upen_cfgpwvlaninsen_PwVlanSel_Shift                                                             1

/*--------------------------------------
BitField Name: PwVlanInsEn
BitField Type: R/W
BitField Desc: Enable insert VLAN 0: disable insert 1: enable insert
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_cfgpwvlaninsen_PwVlanInsEn_Mask                                                        cBit0
#define cAf6_upen_cfgpwvlaninsen_PwVlanInsEn_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through MAC Address Control
Reg Addr   : 0xC30 - 0xC37 #The address format for these registers is 0xC30 + ethpid
Reg Formula: 0xC30 +  ethpid
    Where  : 
           + $ethpid(0-15): Ethernet port ID
Reg Desc   : 
This register configures parameters per ethernet pass through port

------------------------------------------------------------------------------*/
#define cAf6Reg_ramrxepacfg0_Base                                                                        0xC30
#define cAf6Reg_ramrxepacfg0_WidthVal                                                                       64

/*--------------------------------------
BitField Name: EthPassMacAdd
BitField Type: RW
BitField Desc: Ethernet pass through MAC address used for Loop DA counter
BitField Bits: [47:0]
--------------------------------------*/
#define cAf6_ramrxepacfg0_EthPassMacAdd_01_Mask                                                       cBit31_0
#define cAf6_ramrxepacfg0_EthPassMacAdd_01_Shift                                                             0
#define cAf6_ramrxepacfg0_EthPassMacAdd_02_Mask                                                       cBit15_0
#define cAf6_ramrxepacfg0_EthPassMacAdd_02_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Port Enable and MTU Control
Reg Addr   : 0xC40 - 0xC47 #The address format for these registers is 0xC40 + ethpid
Reg Formula: 0xC40 +  ethpid
    Where  : 
           + $ethpid(0-15): Ethernet port ID
Reg Desc   : 
This register configures parameters per ethernet pass through port

------------------------------------------------------------------------------*/
#define cAf6Reg_ramrxepacfg1_Base                                                                        0xC40
#define cAf6Reg_ramrxepacfg1_WidthVal                                                                       32

/*--------------------------------------
BitField Name: EthPassPortEn
BitField Type: RW
BitField Desc: Ethernet pass port enable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassPortEn_Mask                                                            cBit20
#define cAf6_ramrxepacfg1_EthPassPortEn_Shift                                                               20

/*--------------------------------------
BitField Name: EthPassPcsErrDropEn
BitField Type: RW
BitField Desc: Ethernet PCS error drop enable
BitField Bits: [19]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassPcsErrDropEn_Mask                                                      cBit19
#define cAf6_ramrxepacfg1_EthPassPcsErrDropEn_Shift                                                         19

/*--------------------------------------
BitField Name: EthPassFcsErrDropEn
BitField Type: RW
BitField Desc: Ethernet FCS error drop enable
BitField Bits: [18]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassFcsErrDropEn_Mask                                                      cBit18
#define cAf6_ramrxepacfg1_EthPassFcsErrDropEn_Shift                                                         18

/*--------------------------------------
BitField Name: EthPassUndersizeDropEn
BitField Type: RW
BitField Desc: Ethernet Undersize drop enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassUndersizeDropEn_Mask                                                   cBit17
#define cAf6_ramrxepacfg1_EthPassUndersizeDropEn_Shift                                                      17

/*--------------------------------------
BitField Name: EthPassOversizeDropEn
BitField Type: RW
BitField Desc: Ethernet Oversize drop enable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassOversizeDropEn_Mask                                                    cBit16
#define cAf6_ramrxepacfg1_EthPassOversizeDropEn_Shift                                                       16

/*--------------------------------------
BitField Name: EthPassPausefrmDropEn
BitField Type: RW
BitField Desc: Ethernet Pausefrm drop enable
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassPausefrmDropEn_Mask                                                    cBit15
#define cAf6_ramrxepacfg1_EthPassPausefrmDropEn_Shift                                                       15

/*--------------------------------------
BitField Name: EthPassDaloopDropEn
BitField Type: RW
BitField Desc: Ethernet Daloop drop enable
BitField Bits: [14]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassDaloopDropEn_Mask                                                      cBit14
#define cAf6_ramrxepacfg1_EthPassDaloopDropEn_Shift                                                         14

/*--------------------------------------
BitField Name: EthPassMtuSize
BitField Type: RW
BitField Desc: Ethernet pass through MTU size
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_ramrxepacfg1_EthPassMtuSize_Mask                                                         cBit13_0
#define cAf6_ramrxepacfg1_EthPassMtuSize_Shift                                                               0
#define cAf6_ramrxepacfg1_EthPassMtuSize_Max                                                          16000

/*------------------------------------------------------------------------------
Reg Name   : Ethernet Pass Through Group4 Counter
Reg Addr   : 0xC60 - 0xC6F(R2C), 0xC70 - 0xC7F(RO),
Reg Formula: 0xC60 + $ethpid
    Where  : 
           + $ethpid(0-15)
Reg Desc   : 
The register count PCS code word error

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxepapcserrcnt_Base                                                                 0xC60
#define cAf6Reg_upen_rxepapcserrcnt_WidthVal                                                                32

/*--------------------------------------
BitField Name: rxpcscodeerrcnt
BitField Type: RC/RO
BitField Desc: Receive PCS Code Word Error Counter
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_rxepapcserrcnt_rxpcscodeerrcnt_Mask                                                 cBit15_0
#define cAf6_upen_rxepapcserrcnt_rxpcscodeerrcnt_Shift                                                       0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021FACEPLATESERDESETHPORTREG_H_ */

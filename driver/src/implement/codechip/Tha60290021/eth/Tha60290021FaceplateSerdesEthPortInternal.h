/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290021FaceplateSerdesEthPortInternal.h
 * 
 * Created Date: Apr 25, 2017
 *
 * Description : ETH Port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021FACEPLATESERDESETHPORTINTERNAL_H_
#define _THA60290021FACEPLATESERDESETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60290021SerdesEthPortInternal.h"
#include "Tha60290021FaceplateSerdesEthPort.h"
#include "Tha60290021EthPortCounters.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021FaceplateSerdesEthPort * Tha60290021FaceplateSerdesEthPort;

typedef struct tTha60290021FaceplateSerdesEthPortMethods
    {
    eBool (*Interface100BaseFxIsSupported)(Tha60290021FaceplateSerdesEthPort self);
    eBool (*HasDisparityFeature)(Tha60290021FaceplateSerdesEthPort self);
    eBool (*HasK30_7Feature)(Tha60290021FaceplateSerdesEthPort self);
    eBool (*HasClockMonitor)(Tha60290021FaceplateSerdesEthPort self);
    uint32 (*StartVersionSupportTxAlarmForcing)(Tha60290021FaceplateSerdesEthPort self);
    eBool (*ExcessiveErrorRatioIsSupported)(Tha60290021FaceplateSerdesEthPort self);
    uint32 (*OneGeInterruptProcess)(AtEthPort self, uint32 ethIntr);
    uint32 (*Af6Reg_ramrxepacfg1_Base)(Tha60290021FaceplateSerdesEthPort self);
    uint32 (*Af6Reg_upen_cfginsvlan_Base)(Tha60290021FaceplateSerdesEthPort self);
    uint32 (*Af6Reg_ramrxepacfg0_Base)(Tha60290021FaceplateSerdesEthPort self);
    }tTha60290021FaceplateSerdesEthPortMethods;

typedef struct tTha60290021FaceplateSerdesEthPort
    {
    tTha60290021SerdesEthPort super;
    const tTha60290021FaceplateSerdesEthPortMethods *methods;

    /* Private data */
    eAtEthPortSpeed    speed;
    Tha60290021EthPortCounters counters;
    AtClockMonitor clockMonitor;
    }tTha60290021FaceplateSerdesEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290021FaceplateSerdesEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021FACEPLATESERDESETHPORTINTERNAL_H_ */


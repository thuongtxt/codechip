/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290021EthPortCounters.h
 * 
 * Created Date: Mar 14, 2019
 *
 * Description : 60290021 Ethernet port counters
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021ETHPORTCOUNTER_H_
#define _THA60290021ETHPORTCOUNTER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021EthPortCounters *Tha60290021EthPortCounters;

typedef struct tTha60290021FaceplateSerdesEthCounters
    {
    uint32 txPackets;
    uint32 txBytes;
    uint32 txPacketsLen64;
    uint32 txPacketsLen65_127;
    uint32 txPacketsLen128_255;
    uint32 txPacketsLen256_511;
    uint32 txPacketsLen512_1023;
    uint32 txPacketsLen1024_1518;
    uint32 txPacketsJumbo;
    uint32 txPausePackets;
    uint32 txBrdCastPackets;
    uint32 txMultCastPackets;
    uint32 txUniCastPackets;
    uint32 txOversizePackets;
    uint32 txUndersizePackets;
    uint32 txDiscardedPackets;
    uint32 txOverFlowDroppedPackets;

    uint32 rxPackets;
    uint32 rxBytes;
    uint32 rxErrFcsPackets;
    uint32 rxOversizePackets;
    uint32 rxUndersizePackets;
    uint32 rxPacketsLen64;
    uint32 rxPacketsLen65_127;
    uint32 rxPacketsLen128_255;
    uint32 rxPacketsLen256_511;
    uint32 rxPacketsLen512_1023;
    uint32 rxPacketsLen1024_1518;
    uint32 rxPacketsJumbo;
    uint32 rxDiscardedPackets;
    uint32 rxPausePackets;
    uint32 rxBrdCastPackets;
    uint32 rxMultCastPackets;
    uint32 rxOverFlowDroppedPackets;
    uint32 rxFragmentPackets;
    uint32 rxJabberPackets;
    uint32 rxLoopDaPackets;
    uint32 rxPcsInvalidCount;
    }tTha60290021FaceplateSerdesEthCounters;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60290021EthPortCounters Tha60290021EthPortCountersNew(AtEthPort port);

/* Counter retrieval */
eAtRet Tha60290021EthPortAllCountersLatchAndClear(Tha60290021EthPortCounters self, eBool clear);
uint32 Tha60290021EthPortCounterRead2Clear(Tha60290021EthPortCounters self, uint16 counterType, eBool clear);
eAtRet Tha60290021EthPortAllCountersRead2Clear(Tha60290021EthPortCounters self,
                                               tAtEthPortCounters *counters, eBool clear);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021ETHPORTCOUNTER_H_ */


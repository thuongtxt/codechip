/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290021ModuleEth.h
 * 
 * Created Date: Jul 21, 2016
 *
 * Description : ETH Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEETH_H_
#define _THA60290021MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/eth/ThaModuleEth.h"
#include "../common/Tha602900xxCommon.h"
#include "Tha60290021EthPortCounters.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModuleEth * Tha60290021ModuleEth;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60290021ModuleEthNew(AtDevice device);

/* Port */
AtEthPort Tha60290021PwEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290021FaceplateSerdesEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290021SgmiiSerdesEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290021SgmiiSohEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290021SgmiiKbyteEthPortNew(uint8 portId, AtModuleEth module);
AtEthPort Tha60290021SerdesBackplaneEthSubPortNew(uint8 subPortId, AtEthPort port);

uint32 Tha60290021ModuleEthSgmiiDiagnosticBaseAddress(void);
eAtRet Tha60290021ModuleEthSgmiiDiagnosticModeEnable(AtModuleEth self, eBool enabled);
uint32 Tha602900xxModuleEthMultirateBaseAddress(AtModuleEth self, uint8 sixteenPortGroupId);
uint32 Tha60290021ModuleEthTenGeBaseAddress(AtModuleEth self, uint32 groupId);
eBool Tha60290021ModuleEthHasSgmiiDiagnosticLogic(Tha60290021ModuleEth self);
uint8 Tha60290021FaceplateSerdesEthPortSixteenPortGroupId(AtEthPort self);
uint8 Tha60290021ModuleEthNumSixteenPortGroup(AtModuleEth self);
uint32 Tha60290021ModuleEthStart40GBackplaneMacId(AtModuleEth self);
uint32 Tha60290021ModuleEthStop40GBackplaneMacId(AtModuleEth self);
uint32 Tha60290021ModuleEthStartFaceplatePortId(AtModuleEth self);
uint32 Tha60290021ModuleEthStopFaceplatePortId(AtModuleEth self);

eBool Tha60290021EthPortIsSgmiiPort(AtEthPort self);
eBool Tha60290021ModuleEthIs40GInternalPort(AtEthPort self);
eBool Tha60290021ModuleEthIsFaceplatePort(AtEthPort self);
eBool Tha60290021ModuleEthIsBackplaneMac(AtEthPort self);
eBool Tha60290021ModuleEthIsSgmiiKBytePort(AtEthPort self);
eBool Tha60290021ModuleEthIsSgmiiDccPort(AtEthPort self);
eBool Tha60290021ModuleEthIsSgmiiPort(AtModuleEth self, uint8 portId);
eBool Tha60290021ModuleEthIsSgmiiDccKBytePort(AtModuleEth self, uint8 portId);

AtEthPort Tha60290021ModuleEthFaceplatePortGet(AtModuleEth self, uint8 localPortId);
AtEthPort Tha60290021ModuleEthBackplanePortGet(AtModuleEth self);
uint8 Tha60290021ModuleEthFaceplateLocalId(AtModuleEth self, uint8 portId);

/* K-byte-PW and DCC-PW is use each specific ETH Port */
AtEthPort Tha60290021ModuleEthSgmiiDccPortGet(AtModuleEth self);
AtEthPort Tha60290021ModuleEthSgmiiKbytePortGet(AtModuleEth self);
AtEthPort Tha60290021ModuleEthSgmiiDccKbytePortGet(AtModuleEth self);
AtEthPort Tha60290021ModuleEthSgmiiPortGet(AtModuleEth self, uint32 localPortId);
uint8 Tha60290021ModuleEthSgmiiLocalId(AtModuleEth self, uint32 portFlatId);

uint32 Tha60290021ModuleEthBypassBaseAddress(void);
eBool Tha60290021ModuleEthIsExpectedVlanIdInUse(AtModuleEth self, uint16 vlanId);
void Tha60290021ModuleEthUpdateExpectedVlanIdBitMask(AtModuleEth self, uint16 oldVlanId, uint16 newVlanId);
uint8 Tha60290021ModuleEthNumFaceplatePorts(AtModuleEth self);

uint8 Tha60290021ModuleEth40GBackplaneMacLocalId(AtModuleEth self, uint32 portFlatId);
AtEthPort Tha60290021ModuleEth40GMacPortGet(AtModuleEth self, uint32 localPortId);

eAtRet Tha60290021ModuleEthPortBypassEnable(AtEthPort self, eBool enabled);
eBool Tha60290021ModuleEthPortBypassIsEnabled(AtEthPort self);
uint8 Tha60290021ModuleEthNumSubPortVlanTpids(AtModuleEth self);
eAtRet Tha60290021ModuleEthSubPortVlanTxTpidSet(AtModuleEth self, uint8 tpidIndex, uint16 tpid);
uint16 Tha60290021ModuleEthSubPortVlanTransmitTpidGet(AtModuleEth self, uint8 tpidIndex);
eAtRet Tha60290021ModuleEthSubPortVlanExpectedTpidSet(AtModuleEth self, uint8 tpidIndex, uint16 tpid);
uint16 Tha60290021ModuleEthSubPortVlanExpectedTpidGet(AtModuleEth self, uint8 tpidIndex);
eAtRet Tha60290021ModuleEthPortSubportTxVlanSet(AtEthPort self, const tAtVlan *vlan);
eAtRet Tha60290021ModuleEthPortSubportTxVlanGet(AtEthPort self, tAtVlan *vlan);
eAtRet Tha60290021ModuleEthPortSubportExpectedVlanSet(AtEthPort self, const tAtVlan *expectedVlan);
eAtRet Tha60290021ModuleEthPortSubportExpectedVlanGet(AtEthPort self, tAtVlan *expectedVlan);
eBool Tha60290021ModuleEthPortBypassIsSupported(AtEthPort self);

/* FaceplateEthPort Flow-Control */
eAtModuleEthRet Tha60290021FaceplateEthPortFlowControlHighThresholdSet(AtEthPort self, uint32 threshold);
uint32 Tha60290021FaceplateEthPortFlowControlHighThresholdGet(AtEthPort self);
eAtModuleEthRet Tha60290021FaceplateEthPortFlowControlLowThresholdSet(AtEthPort self, uint32 threshold);
uint32 Tha60290021FaceplateEthPortFlowControlLowThresholdGet(AtEthPort self);
uint32 Tha60290021FaceplateEthPortFlowControlTxWaterMarkMinGet(AtEthPort self);
uint32 Tha60290021FaceplateEthPortFlowControlTxWaterMarkMinClear(AtEthPort self);
uint32 Tha60290021FaceplateEthPortFlowControlTxWaterMarkMaxGet(AtEthPort self);
uint32 Tha60290021FaceplateEthPortFlowControlTxWaterMarkMaxClear(AtEthPort self);
uint32 Tha60290021FaceplateEthPortFlowControlOOBBufferLevelGet(AtEthPort self);
uint32 Tha60290021FaceplateEthPortFlowControlTxBufferLevelGet(AtEthPort self);

/* Interrupt */
uint32 Tha60290021FaceplateEthPortOneGeInterruptProcess(AtEthPort self, uint32 ethIntr);

/* Flow control */
uint32 Tha60290021ModuleEthFlowControlBaseAddress(AtModuleEth self);
eAtModuleEthRet Tha60290021ModuleEthFlowControlEnable(AtModuleEth self, eBool enable);
eBool Tha60290021ModuleEthFlowControlIsEnabled(AtModuleEth self);

/* Common */
AtMdio Tha6029SgmiiSerdesMdioCreate(AtModuleEth self);

/* serdes select/bridge */
eAtRet Tha60290021ModuleEthRxBackPlaneSerdesSelect(ThaModuleEth self, uint32 serdesId);
uint32 Tha60290021ModuleEthRxBackPlaneSelectedSerdes(ThaModuleEth self);
eAtRet Tha60210021ModuleEthTxBackPlaneSerdesBridgeEnable(ThaModuleEth self, eBool enabled);

/* Counters */
Tha60290021EthPortCounters Tha60290021ModuleEthFaceplatePortCountersCreate(Tha60290021ModuleEth self, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEETH_H_ */


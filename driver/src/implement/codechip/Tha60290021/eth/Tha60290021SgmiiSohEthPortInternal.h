/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290021SgmiiSohEthPortInternal.h
 * 
 * Created Date: Sep 19, 2017
 *
 * Description : SGMII overhead port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021SGMIISOHETHPORTINTERNAL_H_
#define _THA60290021SGMIISOHETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60290021SerdesEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021SgmiiSohEthPort
    {
    tTha60290021SgmiiSerdesEthPort super;
    }tTha60290021SgmiiSohEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290021SgmiiSohEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021SGMIISOHETHPORTINTERNAL_H_ */

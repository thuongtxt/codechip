/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290021SerdesBackplaneEthPort.c
 *
 * Created Date: Oct 6, 2016
 *
 * Description : Serdes Ethernet MAC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaModuleEth.h"
#include "../../../default/man/ThaDevice.h"
#include "../physical/Tha6029SerdesEth40GReg.h"
#include "../physical/Tha60290021SerdesManager.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "Tha60290021SerdesBackplaneEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021SerdesBackplaneEthPort)self)
#define mAddressWithLocalAddress(self, localAddress) mMethodsGet(mThis(self))->AddressWithLocalAddress(mThis(self), localAddress)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021SerdesBackplaneEthPortMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtChannelMethods m_AtChannelOverride;
static tAtEthPortMethods m_AtEthPortOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtEthPortMethods *m_AtEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtChannel self)
    {
    return ThaModuleEthMacBaseAddress((ThaModuleEth)AtChannelModuleGet(self), (AtEthPort)self);
    }

static uint32 LocalId(AtChannel self)
    {
    AtModuleEth ethModule = (AtModuleEth)AtChannelModuleGet(self);
    return Tha60290021ModuleEth40GBackplaneMacLocalId(ethModule, AtChannelHwIdGet(self));
    }

static uint32 AddressWithLocalAddress(Tha60290021SerdesBackplaneEthPort self, uint32 localAddress)
    {
    AtChannel port = (AtChannel)self;
    return localAddress + (0x10000 * LocalId(port)) + BaseAddress(port);
    }

static ThaVersionReader VersionReader(AtEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return ThaDeviceVersionReader(device);
    }

static eBool CounterTxOversizeUndersizeSupported(Tha60290021SerdesBackplaneEthPort self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x7, 0x0);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtEthPort)self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 CounterLocalAddress(Tha60290021SerdesBackplaneEthPort self, uint16 counterType)
    {
    switch (counterType)
        {
        case cAtEthPortCounterTxPackets               : return 0x2000;
        case cAtEthPortCounterTxBytes                 : return 0x2001;
        case cAtEthPortCounterTxGoodPackets           : return 0x2002;
        case cAtEthPortCounterTxGoodBytes             : return 0x2003;
        case cAtEthPortCounterTxPacketsLen64          : return 0x2004;
        case cAtEthPortCounterTxPacketsLen65_127      : return 0x2005;
        case cAtEthPortCounterTxPacketsLen128_255     : return 0x2006;
        case cAtEthPortCounterTxPacketsLen256_511     : return 0x2007;
        case cAtEthPortCounterTxPacketsLen512_1023    : return 0x2008;
        case cAtEthPortCounterTxPacketsLen1024_1518   : return 0x2009;
        case cAtEthPortCounterTxPacketsLen1519_1522   : return 0x200A;
        case cAtEthPortCounterTxPacketsLen1523_1548   : return 0x200B;
        case cAtEthPortCounterTxPacketsLen1549_2047   : return 0x200C;
        case cAtEthPortCounterTxPacketsLen2048_4095   : return 0x200D;
        case cAtEthPortCounterTxPacketsLen4096_8191   : return 0x200E;
        case cAtEthPortCounterTxPacketsLen8192_9215   : return 0x200F;
        case cAtEthPortCounterTxPacketsSmall          : return 0x2010;
        case cAtEthPortCounterTxPacketsLarge          : return 0x2011;
        case cAtEthPortCounterTxErrFcsPackets         : return 0x2012;
        case cAtEthPortCounterTxErrPackets            : return 0x2013;
        case cAtEthPortCounterTxUniCastPackets        : return 0x2014;
        case cAtEthPortCounterTxMultCastPackets       : return 0x2015;
        case cAtEthPortCounterTxBrdCastPackets        : return 0x2016;
        case cAtEthPortCounterTxVlanPackets           : return 0x2017;
        case cAtEthPortCounterTxPausePackets          : return 0x2018;
        case cAtEthPortCounterTxUserPausePackets      : return 0x2019;
        case cAtEthPortCounterTxUndersizePackets      : return mMethodsGet(mThis(self))->CounterTxOversizeUndersizeSupported(mThis(self)) ? 0x201A : cInvalidUint32;
        case cAtEthPortCounterTxOversizePackets       : return mMethodsGet(mThis(self))->CounterTxOversizeUndersizeSupported(mThis(self)) ? 0x201B : cInvalidUint32;
        case cAtEthPortCounterRxPackets               : return 0x2080;
        case cAtEthPortCounterRxBytes                 : return 0x2081;
        case cAtEthPortCounterRxGoodPackets           : return 0x2082;
        case cAtEthPortCounterRxGoodBytes             : return 0x2083;
        case cAtEthPortCounterRxPacketsLen64          : return 0x2084;
        case cAtEthPortCounterRxPacketsLen65_127      : return 0x2085;
        case cAtEthPortCounterRxPacketsLen128_255     : return 0x2086;
        case cAtEthPortCounterRxPacketsLen256_511     : return 0x2087;
        case cAtEthPortCounterRxPacketsLen512_1023    : return 0x2088;
        case cAtEthPortCounterRxPacketsLen1024_1518   : return 0x2089;
        case cAtEthPortCounterRxPacketsLen1519_1522   : return 0x208A;
        case cAtEthPortCounterRxPacketsLen1523_1548   : return 0x208B;
        case cAtEthPortCounterRxPacketsLen1549_2047   : return 0x208C;
        case cAtEthPortCounterRxPacketsLen2048_4095   : return 0x208D;
        case cAtEthPortCounterRxPacketsLen4096_8191   : return 0x208E;
        case cAtEthPortCounterRxPacketsLen8192_9215   : return 0x208F;
        case cAtEthPortCounterRxPacketsSmall          : return 0x2090;
        case cAtEthPortCounterRxPacketsLarge          : return 0x2091;
        case cAtEthPortCounterRxErrFcsPackets         : return 0x2093;
        case cAtEthPortCounterRxUniCastPackets        : return 0x2094;
        case cAtEthPortCounterRxMultCastPackets       : return 0x2095;
        case cAtEthPortCounterRxBrdCastPackets        : return 0x2096;
        case cAtEthPortCounterRxVlanPackets           : return 0x2097;
        case cAtEthPortCounterRxPausePackets          : return 0x2098;
        case cAtEthPortCounterRxUserPausePackets      : return 0x2099;
        case cAtEthPortCounterRxUndersizePackets      : return 0x209A;
        case cAtEthPortCounterRxFragmentPackets       : return 0x209B;
        case cAtEthPortCounterRxOversizePackets       : return 0x209C;
        case cAtEthPortCounterRxPacketsTooLong        : return 0x209D;
        case cAtEthPortCounterRxJabberPackets         : return 0x209E;
        case cAtEthPortCounterRxStompedFcsPackets     : return 0x209F;
        case cAtEthPortCounterRxInRangeErrPackets     : return 0x20A0;
        case cAtEthPortCounterRxTruncatedPackets      : return 0x20A1;
        case cAtEthPortCounterRxPcsInvalidCount       : return 0x20AC;
        default:
            return cInvalidUint32;
        }
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    Tha60290021SerdesBackplaneEthPort port = (Tha60290021SerdesBackplaneEthPort)self;

    if (mMethodsGet(port)->CounterLocalAddress(port, counterType) == cInvalidUint32)
        return cAtFalse;
    return cAtTrue;
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    AtSerdesController serdes = AtEthPortSerdesController((AtEthPort)self);
    AtSnprintf(description, sizeof(description), "%s.mac", AtObjectToString((AtObject)serdes));
    return description;
    }

static uint32 CounterRead2Clear(AtChannel self, uint16 counterType, eBool r2c)
    {
    Tha60290021SerdesBackplaneEthPort port = (Tha60290021SerdesBackplaneEthPort)self;
    uint32 localAddress = mMethodsGet(port)->CounterLocalAddress(port, counterType);

    AtUnused(r2c);
    if (localAddress == cInvalidUint32)
        return 0;

    return mChannelHwRead(self, mAddressWithLocalAddress((AtEthPort)self, localAddress), cAtModuleEth);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    return CounterRead2Clear(self, counterType, cAtTrue);
    }

static uint32 Reg_ETH_AN_STICKY_Base(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ETH_40G_FEC_AN_STICKY;
    }

static void AutoNegStickyClear(AtEthPort self)
    {
    uint32 localAddress = mMethodsGet(mThis(self))->Reg_ETH_AN_STICKY_Base(mThis(self));
    uint32 regAddr = mAddressWithLocalAddress(self, localAddress);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    }

static eAtModuleEthRet AutoNegRestartOn(AtEthPort self, eBool on)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_AutoNeg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ETH_40G_AutoNeg_an_restart_, on ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    if (on)
        AutoNegStickyClear(self);

    return cAtOk;
    }

static eBool AutoNegRestartIsOn(AtEthPort self)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_AutoNeg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_ETH_40G_AutoNeg_an_restart_Mask) ? cAtTrue : cAtFalse;
    }

static eBool IsSimulated(AtEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return AtDeviceIsSimulated(device);
    }

static eAtRet HwAutoNegEnable(AtEthPort self, eBool enable)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_AutoNeg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ETH_40G_AutoNeg_an_enb_, enable ? 1 : 0);
    mRegFieldSet(regVal, cAf6_ETH_40G_AutoNeg_an_bypass_, enable ? 0 : 1);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    AtEthPortAutoNegRestartOn(self, cAtTrue);
    if (!IsSimulated(self))
        AtOsalUSleep(1000);
    AtEthPortAutoNegRestartOn(self, cAtFalse);

    return cAtOk;
    }

static eAtModuleEthRet AutoNegEnable(AtEthPort self, eBool enable)
    {
    return HwAutoNegEnable(self, enable);
    }

static eBool AutoNegIsEnabled(AtEthPort self)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_AutoNeg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_ETH_40G_AutoNeg_an_enb_Mask) ? cAtTrue : cAtFalse;
    }

static eAtEthPortAutoNegState AutoNegStateGet(AtEthPort self)
    {
    /* XILINX IP does not output any signal for this */
    AtUnused(self);
    return cAtEthPortAutoNegNotSupported;
    }

static eBool AutoNegIsComplete(AtEthPort self)
    {
    uint32 localAddress = mMethodsGet(mThis(self))->Reg_ETH_AN_STICKY_Base(mThis(self));
    uint32 regAddr = mAddressWithLocalAddress(self, localAddress);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_ETH_40G_FEC_AN_STICKY_stkan_autoneg_complete_Mask) ? cAtTrue : cAtFalse;
    }

static eBool AutoNegIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 TxCfgAddress(Tha60290021SerdesBackplaneEthPort self)
    {
    return mAddressWithLocalAddress((AtEthPort)self, cAf6Reg_ETH_40G_TX_CFG_Base);
    }

static uint32 RxCfgAddress(Tha60290021SerdesBackplaneEthPort self)
    {
    return mAddressWithLocalAddress((AtEthPort)self, cAf6Reg_ETH_40G_RX_CFG_Base);
    }

static uint32 Eth40GTxCfgAddress(AtEthPort self)
    {
    return mMethodsGet(mThis(self))->TxCfgAddress(mThis(self));
    }

static eAtModuleEthRet HwTxIpgSet(AtEthPort self, uint8 txIpg)
    {
    uint32 regAddr = Eth40GTxCfgAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ETH_40G_TX_CFG_ipg_cfg_, txIpg);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtModuleEthRet TxIpgSet(AtEthPort self, uint8 txIpg)
    {
    if (AtEthPortTxIpgIsInRange(self, txIpg))
        return HwTxIpgSet(self, txIpg);
    return cAtErrorOutOfRangParm;
    }

static uint8 TxIpgGet(AtEthPort self)
    {
    uint32 regAddr = Eth40GTxCfgAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (uint8)mRegField(regVal, cAf6_ETH_40G_TX_CFG_ipg_cfg_);
    }

static eAtModuleEthRet RxIpgSet(AtEthPort self, uint8 rxIpg)
    {
    AtUnused(self);
    AtUnused(rxIpg);
    return cAtErrorModeNotSupport;
    }

static uint8 RxIpgGet(AtEthPort self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 Eth40GRxCfgAddress(AtEthPort self)
    {
    return mMethodsGet(mThis(self))->RxCfgAddress(mThis(self));
    }

static uint32 ETH_RX_CFG_maxlen_Mask(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6_ETH_40G_RX_CFG_cfg_maxlen_Mask;
    }

static uint32 ETH_RX_CFG_maxlen_Shift(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6_ETH_40G_RX_CFG_cfg_maxlen_Shift;
    }

static uint32 ETH_RX_CFG_minlen_Mask(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6_ETH_40G_RX_CFG_cfg_minlen_Mask;
    }

static uint32 ETH_RX_CFG_minlen_Shift(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6_ETH_40G_RX_CFG_cfg_minlen_Shift;
    }

static eAtModuleEthRet MaxPacketSizeCheck(AtEthPort self, uint32 packetSize)
    {
    if (packetSize <= AtEthPortMinPacketSizeGet(self))
        return cAtErrorNotApplicable;

    if (packetSize > cAf6_ETH_40G_RX_CFG_cfg_maxlen_Max)
        return cAtErrorOutOfRangParm;

    return cAtOk;
    }

static eAtModuleEthRet MaxPacketSizeSet(AtEthPort self, uint32 packetSize)
    {
    eAtRet ret = MaxPacketSizeCheck(self, packetSize);
    uint32 maxlen_Mask = mMethodsGet(mThis(self))->ETH_RX_CFG_maxlen_Mask(mThis(self));
    uint32 maxlen_Shift = mMethodsGet(mThis(self))->ETH_RX_CFG_maxlen_Shift(mThis(self));

    if (ret == cAtOk)
        {
        uint32 regAddr = Eth40GRxCfgAddress(self);
        uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

        mRegFieldSet(regVal, maxlen_, packetSize);
        mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

        return cAtOk;
        }

    return ret;
    }

static uint32 MaxPacketSizeGet(AtEthPort self)
    {
    uint32 regAddr = Eth40GRxCfgAddress(self);
    uint32 maxlen_Mask = mMethodsGet(mThis(self))->ETH_RX_CFG_maxlen_Mask(mThis(self));
    uint32 maxlen_Shift = mMethodsGet(mThis(self))->ETH_RX_CFG_maxlen_Shift(mThis(self));
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, maxlen_);
    }

static eAtModuleEthRet MinPacketSizeCheck(AtEthPort self, uint32 minPacketSize)
    {
    uint32 minlen_Mask = mMethodsGet(mThis(self))->ETH_RX_CFG_minlen_Mask(mThis(self));
    uint32 minlen_Shift = mMethodsGet(mThis(self))->ETH_RX_CFG_minlen_Shift(mThis(self));

    if (minPacketSize >= AtEthPortMaxPacketSizeGet(self))
        return cAtErrorNotApplicable;

    if (minPacketSize > (minlen_Mask >> minlen_Shift))
        return cAtErrorOutOfRangParm;

    return cAtOk;
    }

static eAtModuleEthRet MinPacketSizeSet(AtEthPort self, uint32 minPacketSize)
    {
    eAtRet ret = MinPacketSizeCheck(self, minPacketSize);

    if (ret == cAtOk)
        {
        uint32 regAddr = Eth40GRxCfgAddress(self);
        uint32 minlen_Mask = mMethodsGet(mThis(self))->ETH_RX_CFG_minlen_Mask(mThis(self));
        uint32 minlen_Shift = mMethodsGet(mThis(self))->ETH_RX_CFG_minlen_Shift(mThis(self));
        uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

        mRegFieldSet(regVal, minlen_, minPacketSize);
        mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

        return cAtOk;
        }

    return ret;
    }

static uint32 MinPacketSizeGet(AtEthPort self)
    {
    uint32 regAddr = Eth40GRxCfgAddress(self);
    uint32 minlen_Mask = mMethodsGet(mThis(self))->ETH_RX_CFG_minlen_Mask(mThis(self));
    uint32 minlen_Shift = mMethodsGet(mThis(self))->ETH_RX_CFG_minlen_Shift(mThis(self));
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, minlen_);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= AtChannelTxEnable(self, enable);
    ret |= AtChannelRxEnable(self, enable);

    return ret;
    }

static eBool IsEnabled(AtChannel self)
    {
    return AtChannelTxIsEnabled(self);
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr = Eth40GTxCfgAddress((AtEthPort)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ETH_40G_TX_CFG_cfg_txen_, enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    uint32 regAddr = Eth40GTxCfgAddress((AtEthPort)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_ETH_40G_TX_CFG_cfg_txen_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr = Eth40GRxCfgAddress((AtEthPort)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ETH_40G_RX_CFG_cfg_rxen_, enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    uint32 regAddr = Eth40GRxCfgAddress((AtEthPort)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_ETH_40G_RX_CFG_cfg_rxen_Mask) ? cAtTrue : cAtFalse;
    }

static eBool RxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtTrue;
    }

static eBool TxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtTrue;
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtSerdesManager manager = AtDeviceSerdesManagerGet(device);
    return Tha60290021SerdesManagerBackplaneSerdesWithLocalIndex(manager, (uint8)LocalId((AtChannel)self));
    }

static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed40G) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed40G) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeed40G;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    return (loopbackMode == cAtLoopbackModeRelease) ? cAtTrue : cAtFalse;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    return (loopbackMode == cAtLoopbackModeRelease) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    AtUnused(self);
    return cAtLoopbackModeRelease;
    }

static eAtModuleEthRet MacCheckingEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool MacCheckingIsEnabled(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet SourceMacAddressSet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eAtModuleEthRet SourceMacAddressGet(AtEthPort self, uint8 *address)
    {
    AtUnused(self);
    AtUnused(address);
    return cAtErrorModeNotSupport;
    }

static eBool HasSourceMac(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModuleEthRet InterfaceSet(AtEthPort self, eAtEthPortInterface interface)
    {
    AtUnused(self);
    return (interface == cAtEthPortInterfaceBaseKR) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortInterface InterfaceGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortInterfaceBaseKR;
    }

static eAtModuleEthRet DuplexModeSet(AtEthPort self, eAtEthPortDuplexMode duplexMode)
    {
    AtUnused(self);
    return (duplexMode == cAtEthPortWorkingModeFullDuplex) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortDuplexMode DuplexModeGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortWorkingModeFullDuplex;
    }

static eBool RxIpgIsConfigurable(AtEthPort self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 Eth40gCfgGlbEnAddress(AtEthPort self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_CFG_GLBEN_Base);
    }

static uint32 Reg_ETH_RX_INTEN_Base(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ETH_40G_RX_INTEN;
    }

static uint32 Reg_ETH_TX_INTEN_Base(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ETH_40G_TX_INTEN;
    }

static uint32 Reg_ETH_AN_INTEN_Base(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ETH_40G_FEC_AN_INTEN;
    }

static uint32 RxInterruptMaskRegister(AtEthPort self)
    {
    uint32 localAddress = mMethodsGet(mThis(self))->Reg_ETH_RX_INTEN_Base(mThis(self));
    return mAddressWithLocalAddress(self, localAddress);
    }

static uint32 TxInterruptMaskRegister(AtEthPort self)
    {
    uint32 localAddress = mMethodsGet(mThis(self))->Reg_ETH_TX_INTEN_Base(mThis(self));
    return mAddressWithLocalAddress(self, localAddress);
    }

static uint32 AutoNegInterruptMaskRegister(AtEthPort self)
    {
    uint32 localAddress = mMethodsGet(mThis(self))->Reg_ETH_AN_INTEN_Base(mThis(self));
    return mAddressWithLocalAddress(self, localAddress);
    }

static eAtRet AllInterruptDisable(AtEthPort self)
    {
    mChannelHwWrite(self, RxInterruptMaskRegister(self), 0, cAtModuleEth);
    mChannelHwWrite(self, TxInterruptMaskRegister(self), 0, cAtModuleEth);
    mChannelHwWrite(self, AutoNegInterruptMaskRegister(self), 0, cAtModuleEth);
    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;
    eAtRet ret;

    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= AtEthPortMaxPacketSizeSet(port, cAf6_ETH_40G_RX_CFG_cfg_maxlen_Max); /* 16000 + 4-bytes subport VLAN */
    ret |= Tha602900xxSerdesBackplaneCounterTickModeSet(port, cTha602900xxEthPortCounterTickModeAuto);
    ret |= AllInterruptDisable(port);

    return ret;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    uint32 supportedMask = cAtEthPortAlarmTxFault              |
                           cAtEthPortAlarmRemoteFault        |
                           cAtEthPortAlarmRxInternalFault      |
                           cAtEthPortAlarmRxReceivedLocalFault |
                           cAtEthPortAlarmLocalFault              |
                           cAtEthPortAlarmRxAlignedError       |
                           cAtEthPortAlarmRxMisAligned         |
                           cAtEthPortAlarmRxTruncated          |
                           cAtEthPortAlarmRxHiBer              |
                           cAtEthPortAlarmAutoNegParallelDetectionFault;

    return supportedMask | m_AtChannelMethods->SupportedInterruptMasks(self);
    }

static AtEthPort SubPortObjectCreate(AtEthPort self, uint8 subPortId)
    {
    return Tha60290021SerdesBackplaneEthSubPortNew(subPortId, self);
    }

static uint32 MaxSubPortsGet(AtEthPort self)
    {
    AtUnused(self);
    return 4;
    }

static eBool HasInterruptMaskRegister(Tha60290021SerdesBackplaneEthPort self)
    {
    /* Interrupt mask registers are added and some interrupt status registers
     * change address */
    ThaVersionReader versionReader = VersionReader((AtEthPort)self);
    uint32 startVersionHas40GMacInterruptMask = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x0);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= startVersionHas40GMacInterruptMask) ? cAtTrue : cAtFalse;
    }

static uint32 RxCurrentStatusRegister(AtEthPort self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_RX_ALARM);
    }

static uint32 TxCurrentStatusRegister(AtEthPort self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_TX_ALARM);
    }

static uint32 AutoNegCurrentStatusRegister(AtEthPort self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_FEC_AN_ALARM);
    }

static uint32 RxDefectGet(AtEthPort self)
    {
    uint32 swDefect = 0;
    uint32 regAddr = RxCurrentStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cAf6_ETH_40G_RX_ALARM_stat_rx_local_fault_Mask)
        swDefect |= cAtEthPortAlarmLocalFault;

    if (regVal & cAf6_ETH_40G_RX_ALARM_stat_rx_remote_fault_Mask)
        swDefect |= cAtEthPortAlarmRemoteFault;

    if (regVal & cAf6_ETH_40G_RX_ALARM_stat_rx_internal_local_fault_Mask)
        swDefect |= cAtEthPortAlarmRxInternalFault;

    if (regVal & cAf6_ETH_40G_RX_ALARM_stat_rx_received_local_fault_Mask)
        swDefect |= cAtEthPortAlarmRxReceivedLocalFault;

    if (regVal & cAf6_ETH_40G_RX_ALARM_stat_rx_aligned_err_Mask)
        swDefect |= cAtEthPortAlarmRxAlignedError;

    if (regVal & cAf6_ETH_40G_RX_ALARM_stat_rx_misaligned_Mask)
        swDefect |= cAtEthPortAlarmRxMisAligned;

    if (regVal & cAf6_ETH_40G_RX_ALARM_stat_rx_truncated_Mask)
        swDefect |= cAtEthPortAlarmRxTruncated;

    if (regVal & cAf6_ETH_40G_RX_ALARM_stat_rx_hi_ber_Mask)
        swDefect |= cAtEthPortAlarmRxHiBer;

    return swDefect;
    }

static uint32 TxDefectGet(AtEthPort self)
    {
    uint32 swDefect = 0;
    uint32 regAddr = TxCurrentStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cAf6_ETH_40G_TX_ALARM_stat_tx_local_fault_Mask)
        swDefect |= cAtEthPortAlarmTxFault;

    return swDefect;
    }

static uint32 AutoNegDefectGet(AtEthPort self)
    {
    uint32 swDefect = 0;
    uint32 regAddr = AutoNegCurrentStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cAf6_ETH_40G_FEC_AN_ALARM_stat_an_parallel_detection_fault_Mask)
        swDefect |= cAtEthPortAlarmAutoNegParallelDetectionFault;

    return swDefect;
    }

static uint32 DefectGet(AtChannel self)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 defects = 0;

    if (!mMethodsGet(mThis(self))->HasInterruptMaskRegister(mThis(self)))
        return 0;

    defects |= mMethodsGet(mThis(self))->RxDefectGet(port);
    defects |= mMethodsGet(mThis(self))->TxDefectGet(port);
    defects |= mMethodsGet(mThis(self))->AutoNegDefectGet(port);

    return defects;
    }

static uint32 RxInterruptStatusRegister(AtEthPort self)
    {
    if (mMethodsGet(mThis(self))->HasInterruptMaskRegister(mThis(self)))
        return mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_RX_STICKY_New);
    else
        return mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_RX_STICKY_Old);
    }

static uint32 TxInterruptStatusRegister(AtEthPort self)
    {
    if (mMethodsGet(mThis(self))->HasInterruptMaskRegister(mThis(self)))
        return mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_TX_STICKY_New);
    else
        return mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_TX_STICKY_Old);
    }

static uint32 AutoNegInterruptStatusRegister(AtEthPort self)
    {
    uint32 localAddress = mMethodsGet(mThis(self))->Reg_ETH_AN_STICKY_Base(mThis(self));
    return mAddressWithLocalAddress(self, localAddress);
    }

static uint32 RxDefectHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    uint32 swDefect = 0;
    uint32 hwDefect = 0;
    uint32 regAddr = RxInterruptStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cAf6_ETH_40G_RX_STICKY_stkrx_local_fault_Mask)
        {
        swDefect |= cAtEthPortAlarmLocalFault;
        hwDefect |= cAf6_ETH_40G_RX_STICKY_stkrx_local_fault_Mask;
        }

    if (regVal & cAf6_ETH_40G_RX_STICKY_stkrx_remote_fault_Mask)
        {
        swDefect |= cAtEthPortAlarmRemoteFault;
        hwDefect |= cAf6_ETH_40G_RX_STICKY_stkrx_remote_fault_Mask;
        }

    if (regVal & cAf6_ETH_40G_RX_STICKY_stkrx_internal_local_fault_Mask)
        {
        swDefect |= cAtEthPortAlarmRxInternalFault;
        hwDefect |= cAf6_ETH_40G_RX_STICKY_stkrx_internal_local_fault_Mask;
        }

    if (regVal & cAf6_ETH_40G_RX_STICKY_stkrx_received_local_fault_Mask)
        {
        swDefect |= cAtEthPortAlarmRxReceivedLocalFault;
        hwDefect |= cAf6_ETH_40G_RX_STICKY_stkrx_received_local_fault_Mask;
        }

    if (regVal & cAf6_ETH_40G_RX_STICKY_stkrx_aligned_err_Mask)
        {
        swDefect |= cAtEthPortAlarmRxAlignedError;
        hwDefect |= cAf6_ETH_40G_RX_STICKY_stkrx_aligned_err_Mask;
        }

    if (regVal & cAf6_ETH_40G_RX_STICKY_stkrx_misaligned_Mask)
        {
        swDefect |= cAtEthPortAlarmRxMisAligned;
        hwDefect |= cAf6_ETH_40G_RX_STICKY_stkrx_misaligned_Mask;
        }

    if (regVal & cAf6_ETH_40G_RX_STICKY_stkrx_truncated_Mask)
        {
        swDefect |= cAtEthPortAlarmRxTruncated;
        hwDefect |= cAf6_ETH_40G_RX_STICKY_stkrx_truncated_Mask;
        }

    if (regVal & cAf6_ETH_40G_RX_STICKY_stkrx_hi_ber_Mask)
        {
        swDefect |= cAtEthPortAlarmRxHiBer;
        hwDefect |= cAf6_ETH_40G_RX_STICKY_stkrx_hi_ber_Mask;
        }

    if (read2Clear && hwDefect)
        mChannelHwWrite(self, regAddr, hwDefect, cAtModuleEth);

    return swDefect;
    }

static uint32 TxDefectHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    uint32 swDefect = 0;
    uint32 hwDefect = 0;
    uint32 regAddr = TxInterruptStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cAf6_ETH_40G_TX_STICKY_stktx_local_fault_Mask)
        {
        swDefect |= cAtEthPortAlarmTxFault;
        hwDefect |= cAf6_ETH_40G_TX_STICKY_stktx_local_fault_Mask;
        }

    if (read2Clear && hwDefect)
        mChannelHwWrite(self, regAddr, hwDefect, cAtModuleEth);

    return swDefect;
    }

static uint32 AutoNegHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    uint32 swDefect = 0;
    uint32 hwDefect = 0;
    uint32 regAddr = AutoNegInterruptStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (regVal & cAf6_ETH_40G_FEC_AN_STICKY_stkan_parallel_detection_fault_Mask)
        {
        swDefect |= cAtEthPortAlarmAutoNegParallelDetectionFault;
        hwDefect |= cAf6_ETH_40G_FEC_AN_STICKY_stkan_parallel_detection_fault_Mask;
        }

    if (read2Clear && hwDefect)
        mChannelHwWrite(self, regAddr, hwDefect, cAtModuleEth);

    return swDefect;
    }

static uint32 DefectHistoryRead2Clear(AtEthPort self, eBool read2Clear)
    {
    uint32 swDefect = 0;

    swDefect |= mMethodsGet(mThis(self))->RxDefectHistoryRead2Clear(self, read2Clear);
    swDefect |= mMethodsGet(mThis(self))->TxDefectHistoryRead2Clear(self, read2Clear);
    swDefect |= mMethodsGet(mThis(self))->AutoNegHistoryRead2Clear(self, read2Clear);

    return swDefect;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return DefectHistoryRead2Clear((AtEthPort)self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return DefectHistoryRead2Clear((AtEthPort)self, cAtTrue);
    }

static eAtRet RxInterruptMaskSet(Tha60290021SerdesBackplaneEthPort self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = RxInterruptMaskRegister((AtEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (defectMask & cAtEthPortAlarmLocalFault)
        mRegFieldSet(regVal, cAf6_ETH_40G_RX_INTEN_int_en_rx_local_fault_,
                     (enableMask & cAtEthPortAlarmLocalFault) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRemoteFault)
        mRegFieldSet(regVal, cAf6_ETH_40G_RX_INTEN_int_en_rx_remote_fault_,
                     (enableMask & cAtEthPortAlarmRemoteFault) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRxInternalFault)
        mRegFieldSet(regVal, cAf6_ETH_40G_RX_INTEN_int_en_rx_internal_local_fault_,
                     (enableMask & cAtEthPortAlarmRxInternalFault) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRxReceivedLocalFault)
        mRegFieldSet(regVal, cAf6_ETH_40G_RX_INTEN_int_en_rx_received_local_fault_,
                     (enableMask & cAtEthPortAlarmRxReceivedLocalFault) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRxAlignedError)
        mRegFieldSet(regVal, cAf6_ETH_40G_RX_INTEN_int_en_rx_aligned_err_,
                     (enableMask & cAtEthPortAlarmRxAlignedError) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRxMisAligned)
        mRegFieldSet(regVal, cAf6_ETH_40G_RX_INTEN_int_en_rx_misaligned_,
                     (enableMask & cAtEthPortAlarmRxMisAligned) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRxTruncated)
        mRegFieldSet(regVal, cAf6_ETH_40G_RX_INTEN_int_en_rx_truncated_,
                     (enableMask & cAtEthPortAlarmRxTruncated) ? 1 : 0);

    if (defectMask & cAtEthPortAlarmRxHiBer)
        mRegFieldSet(regVal, cAf6_ETH_40G_RX_INTEN_int_en_rx_hi_ber_,
                     (enableMask & cAtEthPortAlarmRxHiBer) ? 1 : 0);

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet TxInterruptMaskSet(Tha60290021SerdesBackplaneEthPort self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = TxInterruptMaskRegister((AtEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (defectMask & cAtEthPortAlarmTxFault)
        mRegFieldSet(regVal, cAf6_ETH_40G_TX_INTEN_int_en_tx_local_fault_,
                     (enableMask & cAtEthPortAlarmTxFault) ? 1 : 0);

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet AutoNegInterruptMaskSet(Tha60290021SerdesBackplaneEthPort self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = AutoNegInterruptMaskRegister((AtEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);

    if (defectMask & cAtEthPortAlarmAutoNegParallelDetectionFault)
        mRegFieldSet(regVal, cAf6_ETH_40G_FEC_AN_INTEN_int_en_an_parallel_detection_fault_,
                     (enableMask & cAtEthPortAlarmAutoNegParallelDetectionFault) ? 1 : 0);

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->HasInterruptMaskRegister(mThis(self)))
        return cAtErrorModeNotSupport;

    ret |= mMethodsGet(mThis(self))->RxInterruptMaskSet(mThis(self), defectMask, enableMask);
    ret |= mMethodsGet(mThis(self))->TxInterruptMaskSet(mThis(self), defectMask, enableMask);
    ret |= mMethodsGet(mThis(self))->AutoNegInterruptMaskSet(mThis(self), defectMask, enableMask);

    return ret;
    }

static uint32 RxInterruptMaskGet(Tha60290021SerdesBackplaneEthPort self)
    {
    uint32 regAddr = RxInterruptMaskRegister((AtEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 defectMask = 0;

    if (regVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_local_fault_Mask)
        defectMask |= cAtEthPortAlarmLocalFault;

    if (regVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_remote_fault_Mask)
        defectMask |= cAtEthPortAlarmRemoteFault;

    if (regVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_internal_local_fault_Mask)
        defectMask |= cAtEthPortAlarmRxInternalFault;

    if (regVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_received_local_fault_Mask)
        defectMask |= cAtEthPortAlarmRxReceivedLocalFault;

    if (regVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_aligned_err_Mask)
        defectMask |= cAtEthPortAlarmRxAlignedError;

    if (regVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_misaligned_Mask)
        defectMask |= cAtEthPortAlarmRxMisAligned;

    if (regVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_truncated_Mask)
        defectMask |= cAtEthPortAlarmRxTruncated;

    if (regVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_hi_ber_Mask)
        defectMask |= cAtEthPortAlarmRxHiBer;

    return defectMask;
    }

static uint32 TxInterruptMaskGet(Tha60290021SerdesBackplaneEthPort self)
    {
    uint32 regAddr = TxInterruptMaskRegister((AtEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 defectMask = 0;

    if (regVal & cAf6_ETH_40G_TX_INTEN_int_en_tx_local_fault_Mask)
        defectMask |= cAtEthPortAlarmTxFault;

    return defectMask;
    }

static uint32 AutoNegInterruptMaskGet(Tha60290021SerdesBackplaneEthPort self)
    {
    uint32 regAddr = AutoNegInterruptMaskRegister((AtEthPort)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 defectMask = 0;

    if (regVal & cAf6_ETH_40G_FEC_AN_INTEN_int_en_an_parallel_detection_fault_Mask)
        defectMask |= cAtEthPortAlarmAutoNegParallelDetectionFault;

    return defectMask;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 defects = 0;

    if (!mMethodsGet(mThis(self))->HasInterruptMaskRegister(mThis(self)))
        return 0;

    defects |= mMethodsGet(mThis(self))->RxInterruptMaskGet(mThis(self));
    defects |= mMethodsGet(mThis(self))->TxInterruptMaskGet(mThis(self));
    defects |= mMethodsGet(mThis(self))->AutoNegInterruptMaskGet(mThis(self));

    return defects;
    }

static uint32 RxInterruptProcess(AtEthPort self)
    {
    uint32 intrEnReg  = RxInterruptMaskRegister(self);
    uint32 intrStaReg = RxInterruptStatusRegister(self);
    uint32 currStaReg = RxCurrentStatusRegister(self);
    uint32 intrEnVal  = mChannelHwRead(self, intrEnReg, cAtModuleEth);
    uint32 intrStaVal = mChannelHwRead(self, intrStaReg, cAtModuleEth);
    uint32 currStaVal = mChannelHwRead(self, currStaReg, cAtModuleEth);
    uint32 events = 0, defects = 0;
    uint32 intrSta2Clear = 0;

    /* Clear marked interrupts */
    intrStaVal &= intrEnVal;

    if (intrStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_local_fault_Mask)
        {
        events |= cAtEthPortAlarmLocalFault;
        intrSta2Clear |= cAf6_ETH_40G_RX_INTEN_int_en_rx_local_fault_Mask;

        if (currStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_local_fault_Mask)
            defects |= cAtEthPortAlarmLocalFault;
        }

    if (intrStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_remote_fault_Mask)
        {
        events |= cAtEthPortAlarmRemoteFault;
        intrSta2Clear |= cAf6_ETH_40G_RX_INTEN_int_en_rx_remote_fault_Mask;

        if (currStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_remote_fault_Mask)
            defects |= cAtEthPortAlarmRemoteFault;
        }

    if (intrStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_internal_local_fault_Mask)
        {
        events |= cAtEthPortAlarmRxInternalFault;
        intrSta2Clear |= cAf6_ETH_40G_RX_INTEN_int_en_rx_internal_local_fault_Mask;

        if (currStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_internal_local_fault_Mask)
            defects |= cAtEthPortAlarmRxInternalFault;
        }

    if (intrStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_received_local_fault_Mask)
        {
        events |= cAtEthPortAlarmRxReceivedLocalFault;
        intrSta2Clear |= cAf6_ETH_40G_RX_INTEN_int_en_rx_received_local_fault_Mask;

        if (currStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_received_local_fault_Mask)
            defects |= cAtEthPortAlarmRxReceivedLocalFault;
        }

    if (intrStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_misaligned_Mask)
        {
        events |= cAtEthPortAlarmRxMisAligned;
        intrSta2Clear |= cAf6_ETH_40G_RX_INTEN_int_en_rx_misaligned_Mask;

        if (currStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_misaligned_Mask)
            defects |= cAtEthPortAlarmRxMisAligned;
        }

    if (intrStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_aligned_err_Mask)
        {
        events |= cAtEthPortAlarmRxAlignedError;
        intrSta2Clear |= cAf6_ETH_40G_RX_INTEN_int_en_rx_aligned_err_Mask;

        if (currStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_aligned_err_Mask)
            defects |= cAtEthPortAlarmRxAlignedError;
        }

    if (intrStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_truncated_Mask)
        {
        events |= cAtEthPortAlarmRxTruncated;
        intrSta2Clear |= cAf6_ETH_40G_RX_INTEN_int_en_rx_truncated_Mask;

        if (currStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_truncated_Mask)
            defects |= cAtEthPortAlarmRxTruncated;
        }

    if (intrStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_hi_ber_Mask)
         {
         events |= cAtEthPortAlarmRxHiBer;
         intrSta2Clear |= cAf6_ETH_40G_RX_INTEN_int_en_rx_hi_ber_Mask;

         if (currStaVal & cAf6_ETH_40G_RX_INTEN_int_en_rx_hi_ber_Mask)
             defects |= cAtEthPortAlarmRxHiBer;
         }

    if (events)
        {
        mChannelHwWrite(self, intrStaReg, intrSta2Clear, cAtModuleEth);

        AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
        }

    return defects;
    }

static uint32 TxInterruptProcess(AtEthPort self)
    {
    uint32 intrEnReg  = TxInterruptMaskRegister(self);
    uint32 intrStaReg = TxInterruptStatusRegister(self);
    uint32 currStaReg = TxCurrentStatusRegister(self);
    uint32 intrEnVal  = mChannelHwRead(self, intrEnReg, cAtModuleEth);
    uint32 intrStaVal = mChannelHwRead(self, intrStaReg, cAtModuleEth);
    uint32 currStaVal = mChannelHwRead(self, currStaReg, cAtModuleEth);
    uint32 events = 0, defects = 0;
    uint32 intrSta2Clear = 0;

    /* Clear marked interrupts */
    intrStaVal &= intrEnVal;

    if (intrStaVal & cAf6_ETH_40G_TX_INTEN_int_en_tx_local_fault_Mask)
        {
        events |= cAtEthPortAlarmTxFault;
        intrSta2Clear |= cAf6_ETH_40G_TX_INTEN_int_en_tx_local_fault_Mask;

        if (currStaVal & cAf6_ETH_40G_TX_INTEN_int_en_tx_local_fault_Mask)
            defects |= cAtEthPortAlarmTxFault;
        }

    if (events)
        {
        mChannelHwWrite(self, intrStaReg, intrSta2Clear, cAtModuleEth);

        AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
        }

    return defects;
    }

static uint32 AutoNegInterruptProcess(AtEthPort self)
    {
    uint32 intrEnReg  = AutoNegInterruptMaskRegister(self);
    uint32 intrStaReg = AutoNegInterruptStatusRegister(self);
    uint32 currStaReg = AutoNegCurrentStatusRegister(self);
    uint32 intrEnVal  = mChannelHwRead(self, intrEnReg, cAtModuleEth);
    uint32 intrStaVal = mChannelHwRead(self, intrStaReg, cAtModuleEth);
    uint32 currStaVal = mChannelHwRead(self, currStaReg, cAtModuleEth);
    uint32 events = 0, defects = 0;
    uint32 intrSta2Clear = 0;

    /* Clear marked interrupts */
    intrStaVal &= intrEnVal;

    if (intrStaVal & cAf6_ETH_40G_FEC_AN_INTEN_int_en_an_parallel_detection_fault_Mask)
        {
        events |= cAtEthPortAlarmAutoNegParallelDetectionFault;
        intrSta2Clear |= cAf6_ETH_40G_FEC_AN_INTEN_int_en_an_parallel_detection_fault_Mask;

        if (currStaVal & cAf6_ETH_40G_FEC_AN_INTEN_int_en_an_parallel_detection_fault_Mask)
            defects |= cAtEthPortAlarmAutoNegParallelDetectionFault;
        }

    if (events)
        {
        mChannelHwWrite(self, intrStaReg, intrSta2Clear, cAtModuleEth);

        AtChannelAllAlarmListenersCall((AtChannel)self, events, defects);
        }

    return defects;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 defects = 0;
    AtUnused(offset);

    defects |= mMethodsGet(mThis(self))->RxInterruptProcess(port);
    defects |= mMethodsGet(mThis(self))->TxInterruptProcess(port);
    defects |= mMethodsGet(mThis(self))->AutoNegInterruptProcess(port);

    return defects;
    }

static eAtEthPortLinkStatus LinkStatus(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortLinkStatusNotSupported;
    }

static eAtEthPortSpeed AutoNegSpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeed40G;
    }

static eAtEthPortDuplexMode AutoNegDuplexModeGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortWorkingModeFullDuplex;
    }

static eAtModuleEthRet CounterTickModeSet(Tha60290021SerdesBackplaneEthPort self, eTha602900xxEthPortCounterTickMode mode)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 regAddr = Eth40gCfgGlbEnAddress(port);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    uint32 hwMode = 1; /* To work with input PM tick by default */

    /* Need to always enable counter logic */
    mRegFieldSet(regVal, cAf6_ETH_40G_CFG_GLBEN_cfg_enable_cnt_, 1);

    if (mode == cTha602900xxEthPortCounterTickModeManual)
        hwMode = 0;
    mRegFieldSet(regVal, cAf6_ETH_40G_CFG_GLBEN_cfg_sel_tick_, hwMode);

    mChannelHwWrite(port, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eTha602900xxEthPortCounterTickMode CounterTickModeGet(Tha60290021SerdesBackplaneEthPort self)
    {
    AtEthPort port = (AtEthPort)self;
    uint32 regAddr = Eth40gCfgGlbEnAddress(port);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModuleEth);

    if ((regVal & cAf6_ETH_40G_CFG_GLBEN_cfg_enable_cnt_Mask) == 0)
        return cTha602900xxEthPortCounterTickModeDisable;

    if (regVal & cAf6_ETH_40G_CFG_GLBEN_cfg_sel_tick_Mask)
        return cTha602900xxEthPortCounterTickModeAuto;

    return cTha602900xxEthPortCounterTickModeManual;
    }

static eAtSevLevel CounterTickModeColor(eTha602900xxEthPortCounterTickMode mode)
    {
    switch (mode)
        {
        case cTha602900xxEthPortCounterTickModeUnknown: return cSevCritical;
        case cTha602900xxEthPortCounterTickModeDisable: return cSevCritical;
        case cTha602900xxEthPortCounterTickModeAuto   : return cSevInfo;
        case cTha602900xxEthPortCounterTickModeManual : return cSevWarning;
        default:
            return cSevCritical;
        }
    }

static eAtRet Debug(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Debug(self);
    eTha602900xxEthPortCounterTickMode tickMode = Tha602900xxSerdesBackplaneCounterTickModeGet((AtEthPort)self);

    AtPrintc(cSevNormal, "* Counter tick mode: ");
    AtPrintc(CounterTickModeColor(tickMode), "%s\r\n", Tha602900xxEthPortCounterTickMode2String(tickMode));

    return ret;
    }

static uint32 Reg_ETH_CFG_TICK_REG_Base(Tha60290021SerdesBackplaneEthPort self)
    {
    AtUnused(self);
    return cAf6Reg_ETH_40G_CFG_TICK_REG_Base;
    }

static eAtRet CountersTick(AtEthPort self)
    {
    static const uint32 cTimeoutMs = 1000;
    uint32 regAddr = mAddressWithLocalAddress(self, mMethodsGet(mThis(self))->Reg_ETH_CFG_TICK_REG_Base(mThis(self)));
    uint32 regVal = cAf6_ETH_40G_CFG_TICK_REG_tick_reg_Mask;
    tAtOsalCurTime startTime, curTime;
    uint32 elapseTimeMs = 0;
    eBool simulated = AtDeviceIsSimulated(AtChannelDeviceGet((AtChannel)self));

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    AtOsalCurTimeGet(&startTime);
    while (elapseTimeMs < cTimeoutMs)
        {
        regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
        if ((regVal & cAf6_ETH_40G_CFG_TICK_REG_tick_reg_Mask) == 0)
            return cAtOk;

        /* Retry */
        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);

        if (simulated)
            return cAtOk;
        }

    AtChannelLog((AtChannel)self, cAtLogLevelCritical, AtSourceLocation, "Tick counter timeout\r\n");
    return cAtErrorOperationTimeout;
    }

static eAtRet AllCountersLatchAndClear(AtChannel self, eBool clear)
    {
    eTha602900xxEthPortCounterTickMode tickMode = Tha602900xxSerdesBackplaneCounterTickModeGet((AtEthPort)self);

    AtUnused(clear);
    if (tickMode == cTha602900xxEthPortCounterTickModeAuto)
        return cAtOk;

    return CountersTick((AtEthPort)self);
    }

static eAtRet RxFecHwEnable(AtEthPort self, eBool enabled)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_AutoNeg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ETH_40G_AutoNeg_fec_rx_enb_, enabled ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool RxFecHwIsEnabled(AtEthPort self)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_AutoNeg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_ETH_40G_AutoNeg_fec_rx_enb_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet TxFecHwEnable(AtEthPort self, eBool enabled)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_AutoNeg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ETH_40G_AutoNeg_fec_tx_enb_, enabled ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool TxFecHwIsEnabled(AtEthPort self)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_AutoNeg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_ETH_40G_AutoNeg_fec_tx_enb_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet RxFecEnable(AtEthPort self, eBool enabled)
    {
    if (AtEthPortFecIsSupported(self))
        return RxFecHwEnable(self, enabled);
    return m_AtEthPortMethods->RxFecEnable(self, enabled);
    }

static eBool RxFecIsEnabled(AtEthPort self)
    {
    if (AtEthPortFecIsSupported(self))
        return RxFecHwIsEnabled(self);
    return m_AtEthPortMethods->RxFecIsEnabled(self);
    }

static eAtRet TxFecEnable(AtEthPort self, eBool enabled)
    {
    if (AtEthPortFecIsSupported(self))
        return TxFecHwEnable(self, enabled);
    return m_AtEthPortMethods->TxFecEnable(self, enabled);
    }

static eBool TxFecIsEnabled(AtEthPort self)
    {
    if (AtEthPortFecIsSupported(self))
        return TxFecHwIsEnabled(self);
    return m_AtEthPortMethods->TxFecIsEnabled(self);
    }

static eBool FecIsSupported(AtEthPort self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x0, 0);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eAtRet AllCounterRead2Clear(AtChannel self, void *pAllCounters, eBool read2Clear)
    {
    tAtEthPortCounters *counters = (tAtEthPortCounters *)pAllCounters;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 values;

    /* Read-only must have input counters */
    if ((pAllCounters == NULL) && (!read2Clear))
        return cAtErrorNullPointer;

    if (counters)
        mMethodsGet(osal)->MemInit(osal, counters, 0, sizeof(tAtEthPortCounters));

    mSaveCounter(txPackets,
                 CounterRead2Clear(self, cAtEthPortCounterTxPackets, read2Clear),
                 cAtEthPortCounterTxPackets);
    mSaveCounter(txBytes,
                 CounterRead2Clear(self, cAtEthPortCounterTxBytes, read2Clear),
                 cAtEthPortCounterTxBytes);
    mSaveCounter(txGoodPackets,
                 CounterRead2Clear(self, cAtEthPortCounterTxGoodPackets, read2Clear),
                 cAtEthPortCounterTxGoodPackets);
    mSaveCounter(txGoodBytes,
                 CounterRead2Clear(self, cAtEthPortCounterTxGoodBytes, read2Clear),
                 cAtEthPortCounterTxGoodBytes);
    mSaveCounter(txPacketsLen64,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLen64, read2Clear),
                 cAtEthPortCounterTxPacketsLen64);
    mSaveCounter(txPacketsLen65_127,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLen65_127, read2Clear),
                 cAtEthPortCounterTxPacketsLen65_127);
    mSaveCounter(txPacketsLen128_255,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLen128_255, read2Clear),
                 cAtEthPortCounterTxPacketsLen128_255);
    mSaveCounter(txPacketsLen256_511,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLen256_511, read2Clear),
                 cAtEthPortCounterTxPacketsLen256_511);
    mSaveCounter(txPacketsLen512_1023,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLen512_1023, read2Clear),
                 cAtEthPortCounterTxPacketsLen512_1023);
    mSaveCounter(txPacketsLen1024_1518,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLen1024_1518, read2Clear),
                 cAtEthPortCounterTxPacketsLen1024_1518);
    mSaveCounter(txPacketsLen1519_1522,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLen1519_1522, read2Clear),
                 cAtEthPortCounterTxPacketsLen1519_1522);
    mSaveCounter(txPacketsLen1523_1548,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLen1523_1548, read2Clear),
                 cAtEthPortCounterTxPacketsLen1523_1548);
    mSaveCounter(txPacketsLen1549_2047,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLen1549_2047, read2Clear),
                 cAtEthPortCounterTxPacketsLen1549_2047);
    mSaveCounter(txPacketsLen2048_4095,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLen2048_4095, read2Clear),
                 cAtEthPortCounterTxPacketsLen2048_4095);
    mSaveCounter(txPacketsLen4096_8191,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLen4096_8191, read2Clear),
                 cAtEthPortCounterTxPacketsLen4096_8191);
    mSaveCounter(txPacketsLen8192_9215,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLen8192_9215, read2Clear),
                 cAtEthPortCounterTxPacketsLen8192_9215);
    mSaveCounter(txPacketsLarge,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsSmall, read2Clear),
                 cAtEthPortCounterTxPacketsSmall);
    mSaveCounter(txPacketsSmall,
                 CounterRead2Clear(self, cAtEthPortCounterTxPacketsLarge, read2Clear),
                 cAtEthPortCounterTxPacketsLarge);
    mSaveCounter(txErrFcsPackets,
                 CounterRead2Clear(self, cAtEthPortCounterTxErrFcsPackets, read2Clear),
                 cAtEthPortCounterTxErrFcsPackets);
    mSaveCounter(txErrPackets,
                 CounterRead2Clear(self, cAtEthPortCounterTxErrPackets, read2Clear),
                 cAtEthPortCounterTxErrPackets);
    mSaveCounter(txUniCastPackets,
                 CounterRead2Clear(self, cAtEthPortCounterTxUniCastPackets, read2Clear),
                 cAtEthPortCounterTxUniCastPackets);
    mSaveCounter(txMultCastPackets,
                 CounterRead2Clear(self, cAtEthPortCounterTxMultCastPackets, read2Clear),
                 cAtEthPortCounterTxMultCastPackets);
    mSaveCounter(txBrdCastPackets,
                 CounterRead2Clear(self, cAtEthPortCounterTxBrdCastPackets, read2Clear),
                 cAtEthPortCounterTxBrdCastPackets);
    mSaveCounter(txVlanPackets,
                 CounterRead2Clear(self, cAtEthPortCounterTxVlanPackets, read2Clear),
                 cAtEthPortCounterTxVlanPackets);
    mSaveCounter(txPausePackets,
                 CounterRead2Clear(self, cAtEthPortCounterTxPausePackets, read2Clear),
                 cAtEthPortCounterTxPausePackets);
    mSaveCounter(txUserPausePackets,
                 CounterRead2Clear(self, cAtEthPortCounterTxUserPausePackets, read2Clear),
                 cAtEthPortCounterTxUserPausePackets);
    mSaveCounter(txUndersizePackets,
                 CounterRead2Clear(self, cAtEthPortCounterTxUndersizePackets, read2Clear),
                 cAtEthPortCounterTxUndersizePackets);
    mSaveCounter(txOversizePackets,
                 CounterRead2Clear(self, cAtEthPortCounterTxOversizePackets, read2Clear),
                 cAtEthPortCounterTxOversizePackets);
    mSaveCounter(rxPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxPackets, read2Clear),
                 cAtEthPortCounterRxPackets);
    mSaveCounter(rxBytes,
                 CounterRead2Clear(self, cAtEthPortCounterRxBytes, read2Clear),
                 cAtEthPortCounterRxBytes);
    mSaveCounter(rxGoodPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxGoodPackets, read2Clear),
                 cAtEthPortCounterRxGoodPackets);
    mSaveCounter(rxGoodBytes,
                 CounterRead2Clear(self, cAtEthPortCounterRxGoodBytes, read2Clear),
                 cAtEthPortCounterRxGoodBytes);
    mSaveCounter(rxPacketsLen64,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLen64, read2Clear),
                 cAtEthPortCounterRxPacketsLen64);
    mSaveCounter(rxPacketsLen65_127,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLen65_127, read2Clear),
                 cAtEthPortCounterRxPacketsLen65_127);
    mSaveCounter(rxPacketsLen128_255,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLen128_255, read2Clear),
                 cAtEthPortCounterRxPacketsLen128_255);
    mSaveCounter(rxPacketsLen256_511,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLen256_511, read2Clear),
                 cAtEthPortCounterRxPacketsLen256_511);
    mSaveCounter(rxPacketsLen512_1023,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLen512_1023, read2Clear),
                 cAtEthPortCounterRxPacketsLen512_1023);
    mSaveCounter(rxPacketsLen1024_1518,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLen1024_1518, read2Clear),
                 cAtEthPortCounterRxPacketsLen1024_1518);
    mSaveCounter(rxPacketsLen1519_1522,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLen1519_1522, read2Clear),
                 cAtEthPortCounterRxPacketsLen1519_1522);
    mSaveCounter(rxPacketsLen1523_1548,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLen1523_1548, read2Clear),
                 cAtEthPortCounterRxPacketsLen1523_1548);
    mSaveCounter(rxPacketsLen1549_2047,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLen1549_2047, read2Clear),
                 cAtEthPortCounterRxPacketsLen1549_2047);
    mSaveCounter(rxPacketsLen2048_4095,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLen2048_4095, read2Clear),
                 cAtEthPortCounterRxPacketsLen2048_4095);
    mSaveCounter(rxPacketsLen4096_8191,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLen4096_8191, read2Clear),
                 cAtEthPortCounterRxPacketsLen4096_8191);
    mSaveCounter(rxPacketsLen8192_9215,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLen8192_9215, read2Clear),
                 cAtEthPortCounterRxPacketsLen8192_9215);
    mSaveCounter(rxPacketsLarge,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsSmall, read2Clear),
                 cAtEthPortCounterRxPacketsSmall);
    mSaveCounter(rxPacketsSmall,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsLarge, read2Clear),
                 cAtEthPortCounterRxPacketsLarge);
    mSaveCounter(rxErrFcsPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxErrFcsPackets, read2Clear),
                 cAtEthPortCounterRxErrFcsPackets);
    mSaveCounter(rxUniCastPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxUniCastPackets, read2Clear),
                 cAtEthPortCounterRxUniCastPackets);
    mSaveCounter(rxMultCastPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxMultCastPackets, read2Clear),
                 cAtEthPortCounterRxMultCastPackets);
    mSaveCounter(rxBrdCastPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxBrdCastPackets, read2Clear),
                 cAtEthPortCounterRxBrdCastPackets);
    mSaveCounter(rxVlanPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxVlanPackets, read2Clear),
                 cAtEthPortCounterRxVlanPackets);
    mSaveCounter(rxPausePackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxPausePackets, read2Clear),
                 cAtEthPortCounterRxPausePackets);
    mSaveCounter(rxUserPausePackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxUserPausePackets, read2Clear),
                 cAtEthPortCounterRxUserPausePackets);
    mSaveCounter(rxUndersizePackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxUndersizePackets, read2Clear),
                 cAtEthPortCounterRxUndersizePackets);
    mSaveCounter(rxFragmentPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxFragmentPackets, read2Clear),
                 cAtEthPortCounterRxFragmentPackets);
    mSaveCounter(rxOversizePackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxOversizePackets, read2Clear),
                 cAtEthPortCounterRxOversizePackets);
    mSaveCounter(rxPacketsTooLong,
                 CounterRead2Clear(self, cAtEthPortCounterRxPacketsTooLong, read2Clear),
                 cAtEthPortCounterRxPacketsTooLong);
    mSaveCounter(rxJabberPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxJabberPackets, read2Clear),
                 cAtEthPortCounterRxJabberPackets);
    mSaveCounter(rxStompedFcsPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxStompedFcsPackets, read2Clear),
                 cAtEthPortCounterRxStompedFcsPackets);
    mSaveCounter(rxInRangeErrPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxInRangeErrPackets, read2Clear),
                 cAtEthPortCounterRxInRangeErrPackets);
    mSaveCounter(rxTruncatedPackets,
                 CounterRead2Clear(self, cAtEthPortCounterRxTruncatedPackets, read2Clear),
                 cAtEthPortCounterRxTruncatedPackets);

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCounterRead2Clear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCounterRead2Clear(self, pAllCounters, cAtTrue);
    }

static void StatusClear(AtChannel self)
    {
    m_AtChannelMethods->StatusClear(self);

    AtChannelAllCountersLatchAndClear(self, cAtTrue);
    }

static eBool MacCheckingCanEnable(AtEthPort self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtFalse : cAtTrue;
    }

static eBool IsBackplaneMac(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtModuleEthRet LinkTrainingEnable(AtEthPort self, eBool enable)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_AutoNeg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ETH_40G_AutoNeg_lt_enb_, enable ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool LinkTrainingIsEnabled(AtEthPort self)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_AutoNeg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_ETH_40G_AutoNeg_lt_enb_) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet LinkTrainingRestart(AtEthPort self)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_ETH_40G_AutoNeg_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);

    mRegFieldSet(regVal, cAf6_ETH_40G_AutoNeg_lt_restart_, 1);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    /* Wait a moment */
    AtOsalUSleep(100);
    mRegFieldSet(regVal, cAf6_ETH_40G_AutoNeg_lt_restart_, 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool LinkTrainingIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxCanEnable);
        mMethodOverride(m_AtChannelOverride, TxCanEnable);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, StatusClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, AutoNegEnable);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsEnabled);
        mMethodOverride(m_AtEthPortOverride, AutoNegStateGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsSupported);
        mMethodOverride(m_AtEthPortOverride, AutoNegIsComplete);
        mMethodOverride(m_AtEthPortOverride, AutoNegRestartOn);
        mMethodOverride(m_AtEthPortOverride, AutoNegRestartIsOn);
        mMethodOverride(m_AtEthPortOverride, LinkStatus);
        mMethodOverride(m_AtEthPortOverride, AutoNegSpeedGet);
        mMethodOverride(m_AtEthPortOverride, AutoNegDuplexModeGet);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, TxIpgSet);
        mMethodOverride(m_AtEthPortOverride, TxIpgGet);
        mMethodOverride(m_AtEthPortOverride, RxIpgSet);
        mMethodOverride(m_AtEthPortOverride, RxIpgGet);
        mMethodOverride(m_AtEthPortOverride, RxIpgIsConfigurable);
        mMethodOverride(m_AtEthPortOverride, MacCheckingEnable);
        mMethodOverride(m_AtEthPortOverride, MacCheckingIsEnabled);
        mMethodOverride(m_AtEthPortOverride, MaxPacketSizeSet);
        mMethodOverride(m_AtEthPortOverride, MaxPacketSizeGet);
        mMethodOverride(m_AtEthPortOverride, MinPacketSizeSet);
        mMethodOverride(m_AtEthPortOverride, MinPacketSizeGet);
        mMethodOverride(m_AtEthPortOverride, InterfaceSet);
        mMethodOverride(m_AtEthPortOverride, InterfaceGet);
        mMethodOverride(m_AtEthPortOverride, DuplexModeSet);
        mMethodOverride(m_AtEthPortOverride, DuplexModeGet);
        mMethodOverride(m_AtEthPortOverride, MaxSubPortsGet);
        mMethodOverride(m_AtEthPortOverride, SubPortObjectCreate);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressSet);
        mMethodOverride(m_AtEthPortOverride, SourceMacAddressGet);
        mMethodOverride(m_AtEthPortOverride, HasSourceMac);
        mMethodOverride(m_AtEthPortOverride, RxFecEnable);
        mMethodOverride(m_AtEthPortOverride, RxFecIsEnabled);
        mMethodOverride(m_AtEthPortOverride, TxFecEnable);
        mMethodOverride(m_AtEthPortOverride, TxFecIsEnabled);
        mMethodOverride(m_AtEthPortOverride, FecIsSupported);
        mMethodOverride(m_AtEthPortOverride, MacCheckingCanEnable);
        mMethodOverride(m_AtEthPortOverride, IsBackplaneMac);
        mMethodOverride(m_AtEthPortOverride, LinkTrainingEnable);
        mMethodOverride(m_AtEthPortOverride, LinkTrainingIsEnabled);
        mMethodOverride(m_AtEthPortOverride, LinkTrainingRestart);
        mMethodOverride(m_AtEthPortOverride, LinkTrainingIsSupported);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    }

static void MethodsInit(Tha60290021SerdesBackplaneEthPort self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CounterTxOversizeUndersizeSupported);
        mMethodOverride(m_methods, HasInterruptMaskRegister);
        mMethodOverride(m_methods, AddressWithLocalAddress);
        mMethodOverride(m_methods, CounterLocalAddress);
        mMethodOverride(m_methods, TxCfgAddress);
        mMethodOverride(m_methods, RxCfgAddress);
        mMethodOverride(m_methods, CounterTickModeSet);
        mMethodOverride(m_methods, CounterTickModeGet);
        mMethodOverride(m_methods, Reg_ETH_CFG_TICK_REG_Base);
        mMethodOverride(m_methods, ETH_RX_CFG_maxlen_Mask);
        mMethodOverride(m_methods, ETH_RX_CFG_maxlen_Shift);
        mMethodOverride(m_methods, ETH_RX_CFG_minlen_Mask);
        mMethodOverride(m_methods, ETH_RX_CFG_minlen_Shift);
        mMethodOverride(m_methods, Reg_ETH_AN_STICKY_Base);
        mMethodOverride(m_methods, Reg_ETH_TX_INTEN_Base);
        mMethodOverride(m_methods, Reg_ETH_RX_INTEN_Base);
        mMethodOverride(m_methods, Reg_ETH_AN_INTEN_Base);
        mMethodOverride(m_methods, RxInterruptMaskSet);
        mMethodOverride(m_methods, TxInterruptMaskSet);
        mMethodOverride(m_methods, AutoNegInterruptMaskSet);
        mMethodOverride(m_methods, RxInterruptMaskGet);
        mMethodOverride(m_methods, TxInterruptMaskGet);
        mMethodOverride(m_methods, AutoNegInterruptMaskGet);
        mMethodOverride(m_methods, RxInterruptProcess);
        mMethodOverride(m_methods, TxInterruptProcess);
        mMethodOverride(m_methods, AutoNegInterruptProcess);
        mMethodOverride(m_methods, RxDefectGet);
        mMethodOverride(m_methods, TxDefectGet);
        mMethodOverride(m_methods, AutoNegDefectGet);
        mMethodOverride(m_methods, RxDefectHistoryRead2Clear);
        mMethodOverride(m_methods, TxDefectHistoryRead2Clear);
        mMethodOverride(m_methods, AutoNegHistoryRead2Clear);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SerdesBackplaneEthPort);
    }

AtEthPort Tha60290021SerdesBackplaneEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290021SerdesBackplaneEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021SerdesBackplaneEthPortObjectInit(newPort, portId, module);
    }

uint32 Tha60290021SerdesBackplaneAddressWithLocalAddress(AtEthPort self, uint32 localAddress)
    {
    if (self)
        return mAddressWithLocalAddress(self, localAddress);
    return cInvalidUint32;
    }

eAtModuleEthRet Tha602900xxSerdesBackplaneCounterTickModeSet(AtEthPort self, eTha602900xxEthPortCounterTickMode mode)
    {
    Tha60290021SerdesBackplaneEthPort port = (Tha60290021SerdesBackplaneEthPort)self;
    if (port)
        return mMethodsGet(port)->CounterTickModeSet(port, mode);
    return cAtErrorNullPointer;
    }

eTha602900xxEthPortCounterTickMode Tha602900xxSerdesBackplaneCounterTickModeGet(AtEthPort self)
    {
    Tha60290021SerdesBackplaneEthPort port = (Tha60290021SerdesBackplaneEthPort)self;
    if (port)
        return mMethodsGet(port)->CounterTickModeGet(port);
    return cTha602900xxEthPortCounterTickModeUnknown;
    }

eBool Tha60290021SerdesbackplaneEthPortHasInterruptMaskRegister(AtEthPort self)
    {
    if (self)
        return mMethodsGet(mThis(self))->HasInterruptMaskRegister(mThis(self));
    return cAtFalse;
    }

uint32 Tha60290021SerdesbackplaneEthPortRxInterruptMaskRegister(AtEthPort self)
    {
    if (self)
        return RxInterruptMaskRegister(self);
    return cInvalidUint32;
    }

uint32 Tha60290021SerdesbackplaneEthPortTxInterruptMaskRegister(AtEthPort self)
    {
    if (self)
        return TxInterruptMaskRegister(self);
    return cInvalidUint32;
    }

uint32 Tha60290021SerdesbackplaneEthPortAutoNegInterruptMaskRegister(AtEthPort self)
    {
    if (self)
        return AutoNegInterruptMaskRegister(self);
    return cInvalidUint32;
    }

uint32 Tha60290021SerdesbackplaneEthPortAddressWithLocalAddress(Tha60290021SerdesBackplaneEthPort self, uint32 localAddress)
    {
    if (self)
        return mMethodsGet(self)->AddressWithLocalAddress(self, localAddress);
    return cInvalidUint32;
    }

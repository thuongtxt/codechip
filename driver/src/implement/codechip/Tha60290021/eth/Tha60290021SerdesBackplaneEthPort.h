/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290021SerdesBackplaneEthPort.h
 * 
 * Created Date: Feb 20, 2017
 *
 * Description : Backplane SERDES ETH Port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021SERDESBACKPLANEETHPORT_H_
#define _THA60290021SERDESBACKPLANEETHPORT_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60290021ModuleEth.h"
#include "../common/Tha602900xxCommon.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mSaveCounter(fieldName, call, counterType)                             \
    if (AtChannelCounterIsSupported(self, counterType))                        \
        {                                                                      \
        values = call;                                                         \
        if (counters)                                                          \
            counters->fieldName = values;                                      \
        }

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021SerdesBackplaneEthPort * Tha60290021SerdesBackplaneEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290021SerdesBackplaneEthPortNew(uint8 portId, AtModuleEth module);

uint32 Tha60290021SerdesBackplaneAddressWithLocalAddress(AtEthPort self, uint32 localAddress);

eBool Tha60290021SerdesbackplaneEthPortHasInterruptMaskRegister(AtEthPort self);

uint32 Tha60290021SerdesbackplaneEthPortRxInterruptMaskRegister(AtEthPort self);
uint32 Tha60290021SerdesbackplaneEthPortTxInterruptMaskRegister(AtEthPort self);
uint32 Tha60290021SerdesbackplaneEthPortAutoNegInterruptMaskRegister(AtEthPort self);
uint32 Tha60290021SerdesbackplaneEthPortAddressWithLocalAddress(Tha60290021SerdesBackplaneEthPort self, uint32 localAddress);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021SERDESBACKPLANEETHPORT_H_ */


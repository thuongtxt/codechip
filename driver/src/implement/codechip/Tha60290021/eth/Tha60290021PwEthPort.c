/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290021PwEthPort.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Internal MAC Ethernet Port used for PW traffic
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../physical/Tha60290021SerdesManager.h"
#include "../man/Tha60290021Device.h"
#include "Tha60290021PwEthPortInternal.h"
#include "Tha60290021ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290021PwEthPort *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods   m_AtObjectOverride;
static tAtChannelMethods  m_AtChannelOverride;
static tAtEthPortMethods  m_AtEthPortOverride;
static tThaEthPortMethods m_ThaEthPortOverride;

/* Save super implementation */
static const tAtObjectMethods   *m_AtObjectMethods   = NULL;
static const tAtChannelMethods  *m_AtChannelMethods  = NULL;
static const tAtEthPortMethods  *m_AtEthPortMethods  = NULL;
static const tThaEthPortMethods *m_ThaEthPortMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SpeedIsSupported(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed40G) ? cAtTrue : cAtFalse;
    }

static eAtModuleEthRet SpeedSet(AtEthPort self, eAtEthPortSpeed speed)
    {
    AtUnused(self);
    return (speed == cAtEthPortSpeed40G) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtEthPortSpeed SpeedGet(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortSpeed40G;
    }

static AtSerdesController SerdesController(AtEthPort self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(device);
    return Tha60290021SerdesManagerBackplaneSerdesWithLocalIndex(serdesManager, 0);
    }

static eBool HwIsReady(AtChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "40g_pw";
    }

static uint32 MaxBandwidthInKbps(ThaEthPort self)
    {
    AtUnused(self);
    return 20000000; /* 20G */
    }

static eBool UseSerdesMac(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool RunningOnOtherPlatform(AtChannel self)
    {
    return Tha60290021DeviceIsRunningOnOtherPlatform(AtChannelDeviceGet(self));
    }

static eBool HasMacFunctionality(AtEthPort self)
    {
    if (RunningOnOtherPlatform((AtChannel)self) || UseSerdesMac(self))
        return cAtFalse;

    return m_AtEthPortMethods->HasMacFunctionality(self);
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    if (RunningOnOtherPlatform(self))
        {
        /* HW is using hard loop inside. We can declare release loopback
         * is not supported, but doing so require common logic change. Do not
         * need to take risk with this debug feature */
        if ((loopbackMode == cAtLoopbackModeLocal) ||
            (loopbackMode == cAtLoopbackModeRelease))
            return cAtOk;

        return cAtErrorModeNotSupport;
        }

    /* When SERDES MAC is used, loopback should will be done at SERDES level */
    if (UseSerdesMac((AtEthPort)self))
        return (loopbackMode == cAtLoopbackModeRelease) ? cAtOk : cAtErrorModeNotSupport;

    return m_AtChannelMethods->LoopbackSet(self, loopbackMode);
    }

static uint8 LoopbackGet(AtChannel self)
    {
    if (RunningOnOtherPlatform(self))
        return cAtLoopbackModeLocal;

    /* When SERDES MAC is used, loopback should will be done at SERDES level */
    if (UseSerdesMac((AtEthPort)self))
        return cAtLoopbackModeRelease;

    return m_AtChannelMethods->LoopbackGet(self);
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (RunningOnOtherPlatform(self))
        return (loopbackMode == cAtLoopbackModeLocal) ? cAtTrue : cAtFalse;

    /* When SERDES MAC is used, loopback should will be done at SERDES level */
    if (UseSerdesMac((AtEthPort)self))
        return (loopbackMode == cAtLoopbackModeRelease) ? cAtTrue : cAtFalse;

    return m_AtChannelMethods->LoopbackIsSupported(self, loopbackMode);
    }

static eAtModuleEthRet TxIpgSet(AtEthPort self, uint8 txIpg)
    {
    /* When running on other platform, this is ignored */
    if (RunningOnOtherPlatform((AtChannel)self))
        return cAtOk;

    return m_AtEthPortMethods->TxIpgSet(self, txIpg);
    }

static uint8 TxIpgGet(AtEthPort self)
    {
    /* When running on other platform, this is ignored */
    if (RunningOnOtherPlatform((AtChannel)self))
        return 0;

    return m_AtEthPortMethods->TxIpgGet(self);
    }

static eAtModuleEthRet RxIpgSet(AtEthPort self, uint8 rxIpg)
    {
    /* When running on other platform, this is ignored */
    if (RunningOnOtherPlatform((AtChannel)self))
        return cAtOk;

    return m_AtEthPortMethods->RxIpgSet(self, rxIpg);
    }

static uint8 RxIpgGet(AtEthPort self)
    {
    /* When running on other platform, this is ignored */
    if (RunningOnOtherPlatform((AtChannel)self))
        return 0;

    return m_AtEthPortMethods->RxIpgGet(self);
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtFalse;
    }

static eAtEthPortLinkStatus LinkStatus(AtEthPort self)
    {
    AtUnused(self);
    return cAtEthPortLinkStatusNotSupported;
    }

static AtSerdesManager SerdesManager(AtEthPort self)
    {
    return AtDeviceSerdesManagerGet(AtChannelDeviceGet((AtChannel)self));
    }

static ThaModuleEth EthModule(AtEthPort self)
    {
    return (ThaModuleEth)AtChannelModuleGet((AtChannel)self);
    }

static eAtModuleEthRet RxSerdesSelect(AtEthPort self, AtSerdesController serdes)
    {
    ThaModuleEth module = (ThaModuleEth)EthModule(self);
    if (serdes == NULL)
        return ThaModuleEthRxFsmPinEnable(module, cAtTrue);

    if (Tha60290021SerdesManagerSerdesIsBackplane(SerdesManager(self), AtSerdesControllerIdGet(serdes)))
        return Tha60290021ModuleEthRxBackPlaneSerdesSelect(module, AtSerdesControllerIdGet(serdes));

    return cAtErrorInvlParm;
    }

static eBool CanSelectNullSerdes(AtEthPort self)
    {
    /* Select a NULL SERDES to work base on FSM input signal */
    AtUnused(self);
    return cAtTrue;
    }

static AtSerdesController RxSelectedSerdesOnConfiguration(AtEthPort self)
    {
    uint32 serdesId = Tha60290021ModuleEthRxBackPlaneSelectedSerdes((ThaModuleEth)EthModule(self));
    return Tha60290021SerdesManagerBackplaneSerdesController(SerdesManager(self), serdesId);
    }

static AtSerdesController RxSelectedSerdesOnRxFsmPinStatus(AtEthPort self)
    {
    eBool rxFsmPinStatus = ThaModuleEthRxFsmPinStatusGet((ThaModuleEth)EthModule(self));
    uint32 localSerdesId = (rxFsmPinStatus ? 0 : 1);

    return Tha60290021SerdesManagerBackplaneSerdesWithLocalIndex(SerdesManager(self), (uint8)localSerdesId);
    }

static AtSerdesController RxSelectedSerdes(AtEthPort self)
    {
    if (ThaModuleEthRxFsmPinIsEnabled((ThaModuleEth)EthModule(self)))
        return RxSelectedSerdesOnRxFsmPinStatus(self);
    return RxSelectedSerdesOnConfiguration(self);
    }

static eAtModuleEthRet TxSerdesSet(AtEthPort self, AtSerdesController serdes)
    {
    if (serdes == AtEthPortSerdesController(self))
        return cAtOk;
    return cAtErrorModeNotSupport;
    }

static AtSerdesController TxSerdesGet(AtEthPort self)
    {
    return AtEthPortSerdesController(self);
    }

static eAtModuleEthRet TxSerdesBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cAtErrorModeNotSupport;
    }

static eBool TxSerdesCanBridge(AtEthPort self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);

    return cAtFalse;
    }

static AtSerdesController TxBridgedSerdes(AtEthPort self)
    {
    return Tha60290021SerdesManagerBackplaneSerdesWithLocalIndex(SerdesManager(self), 1);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort port = (ThaEthPort)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(port);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, m_ThaEthPortMethods, sizeof(m_ThaEthPortOverride));

        mMethodOverride(m_ThaEthPortOverride, MaxBandwidthInKbps);
        }

    mMethodsSet(port, &m_ThaEthPortOverride);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, m_AtEthPortMethods, sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, SpeedIsSupported);
        mMethodOverride(m_AtEthPortOverride, SpeedSet);
        mMethodOverride(m_AtEthPortOverride, SpeedGet);
        mMethodOverride(m_AtEthPortOverride, SerdesController);
        mMethodOverride(m_AtEthPortOverride, HasMacFunctionality);
        mMethodOverride(m_AtEthPortOverride, TxIpgGet);
        mMethodOverride(m_AtEthPortOverride, TxIpgSet);
        mMethodOverride(m_AtEthPortOverride, RxIpgGet);
        mMethodOverride(m_AtEthPortOverride, RxIpgSet);
        mMethodOverride(m_AtEthPortOverride, LinkStatus);
        mMethodOverride(m_AtEthPortOverride, RxSerdesSelect);
        mMethodOverride(m_AtEthPortOverride, RxSelectedSerdes);
        mMethodOverride(m_AtEthPortOverride, CanSelectNullSerdes);
        mMethodOverride(m_AtEthPortOverride, TxBridgedSerdes);
        mMethodOverride(m_AtEthPortOverride, TxSerdesSet);
        mMethodOverride(m_AtEthPortOverride, TxSerdesGet);
        mMethodOverride(m_AtEthPortOverride, TxSerdesBridge);
        mMethodOverride(m_AtEthPortOverride, TxSerdesCanBridge);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, HwIsReady);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PwEthPort);
    }

AtEthPort Tha60290021PwEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051EthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290021PwEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021PwEthPortObjectInit(newPort, portId, module);
    }

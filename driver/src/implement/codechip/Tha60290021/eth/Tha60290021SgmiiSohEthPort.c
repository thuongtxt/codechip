/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH Module
 *
 * File        : Tha60290021SgmiiKbyteEthPort.c
 *
 * Created Date: Nov 11, 2016
 *
 * Description : SGMII KByte Port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../physical/Tha60290021SerdesManager.h"
#include "../pw/Tha60290021DccKbyteReg.h"
#include "../pw/Tha60290021ModulePw.h"
#include "../common/Tha6029DccKbyte.h"
#include "Tha6029SerdesSgmiiDiagReg.h"
#include "Tha60290021ModuleEth.h"
#include "Tha60290021SgmiiSohEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtEthPortMethods            m_AtEthPortOverride;
static tThaEthPortMethods           m_ThaEthPortOverride;
static tAtChannelMethods            m_AtChannelOverride;

/* Save super implementation */
/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods   = NULL;
static const tAtChannelMethods      *m_AtChannelMethods  = NULL;
static const tAtEthPortMethods      *m_AtEthPortMethods  = NULL;
static const tThaEthPortMethods     *m_ThaEthPortMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void MacDwordsSw2Hw(uint32 macLsbVal, uint32 macMsbVal, uint32 *hwMacLsb, uint32 *hwMacMsb)
    {
    *hwMacLsb = (uint32) ((macLsbVal >> 5) + ((macMsbVal & cBit4_0) << 27));
    *hwMacMsb = (uint32)(macMsbVal >> 5);
    }

static void MacDwordsHw2Sw(uint32 *longRegVal, uint32 *macLsb, uint32 *macMsb)
    {
    *macLsb = (uint32)((longRegVal[0] & cBit26_0) << 5);
    *macMsb = (uint32)((longRegVal[0] >> 27) | ((longRegVal[1] & cBit10_0) << 5));
    }

static eAtRet ExpectedDestMacSet(AtEthPort self, const uint8 *mac)
    {
    return Tha6029DccKByteSgmiiExpectedDestMacSet((ThaEthPort)self, mac, cAf6_upen_dccrxhdr_MAC_DA_02_Mask, cAf6_upen_dccrxhdr_MAC_DA_02_Shift, MacDwordsSw2Hw);
    }

static eAtRet ExpectedDestMacGet(AtEthPort self, uint8 *mac)
    {
    return Tha6029DccKByteSgmiiExpectedDestMacGet((ThaEthPort)self, mac, MacDwordsHw2Sw);
    }

static eBool HasExpectedDestMac(AtEthPort self)
    {
    return AtEthPortSohTransparentIsSupported(self);
    }

static eBool ExpectedCVlanIsSupported(AtEthPort self)
    {
    return Tha6029DccKByteSgmiiExpectedCVlanIsSupported(self);
    }

static eAtRet ExpectedCVlanIdSet(AtEthPort self, uint16 vlanId)
    {
    return Tha6029DccKByteSgmiiExpectedCVlanSet(self, vlanId, cAf6_upen_dccrxhdr_CVID_Mask, cAf6_upen_dccrxhdr_CVID_Shift);
    }

static uint16 ExpectedCVlanIdGet(AtEthPort self)
    {
    return Tha6029DccKByteSgmiiExpectedCVlanGet(self, cAf6_upen_dccrxhdr_CVID_Mask, cAf6_upen_dccrxhdr_CVID_Shift);
    }

static eBool ShouldUseInterruptV2(AtChannel self)
    {
    Tha60290021ModulePw pwModule = (Tha60290021ModulePw)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModulePw);
    return Tha60290021ModulePwDccKbyteInterruptIsSupported(pwModule);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiInterruptMaskSet(self, defectMask, enableMask);

    return Tha6029DccKByteSgmiiInterruptMaskSet(self, defectMask, enableMask);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiInterruptMaskGet(self);

    return Tha6029DccKByteSgmiiInterruptMaskGet(self);
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiInterruptProcess(self, offset);
    return 0;
    }

static uint32 DefectGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiDefectGet(self);

    return Tha6029DccKByteSgmiiDefectGet(self);
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiDefectHistoryRead2Clear(self, cAtFalse);

    return Tha6029DccKByteSgmiiDefectHistoryRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiDefectHistoryRead2Clear(self, cAtTrue);

    return Tha6029DccKByteSgmiiDefectHistoryRead2Clear(self, cAtTrue);
    }

static uint32 CounterLocalAddress(ThaEthPort self, uint16 counterType)
    {
    return Tha6029DccKbyteEthPortCounterV1BaseAddress(self, counterType);
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    eBool supported = m_AtChannelMethods->CounterIsSupported(self, counterType);
    if (supported)
        return supported;

    return Tha6029DccKByteSgmiiCounterIsSupported(self, counterType);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (Tha6029DccKByteSgmiiIsDccKByteCounter(self, counterType))
        return Tha6029DccKByteSgmiiCounterRead2Clear(self, counterType, cAtFalse);

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (Tha6029DccKByteSgmiiIsDccKByteCounter(self, counterType))
        return Tha6029DccKByteSgmiiCounterRead2Clear(self, counterType, cAtTrue);

    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static eAtRet Init(AtChannel self)
	{
	eAtRet ret = cAtOk;

	ret |= m_AtChannelMethods->Init(self);
	ret |= AtChannelInterruptMaskSet(self, AtChannelInterruptMaskGet(self), 0);

	return ret;
	}

static const char *ToString(AtObject self)
    {
    AtUnused(self);
    return "sgmii_kbyte_dcc";
    }

static eBool SohTransparentIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet AllCountersRead2Clear(AtChannel self, void *pAllCounters, eBool read2Clear)
    {
    tAtEthPortCounters *counters = (tAtEthPortCounters *)pAllCounters;
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 values;

    if ((pAllCounters == NULL) && (!read2Clear))
        return cAtErrorNullPointer;

    if (counters)
        mMethodsGet(osal)->MemInit(osal, counters, 0, sizeof(tAtEthPortCounters));

    mSaveCounter(txPackets,
                 Tha6029DccKByteSgmiiCounterRead2Clear(self, cAtEthPortCounterTxPackets, read2Clear), cAtEthPortCounterTxPackets);
    mSaveCounter(rxPackets,
                 Tha6029DccKByteSgmiiCounterRead2Clear(self, cAtEthPortCounterRxPackets, read2Clear), cAtEthPortCounterRxPackets);
    mSaveCounter(txBytes,
                 Tha6029DccKByteSgmiiCounterRead2Clear(self, cAtEthPortCounterTxBytes, read2Clear), cAtEthPortCounterTxBytes);
    mSaveCounter(rxBytes,
                 Tha6029DccKByteSgmiiCounterRead2Clear(self, cAtEthPortCounterRxBytes, read2Clear), cAtEthPortCounterRxBytes);
    mSaveCounter(rxErrPsnPackets,
                 Tha6029DccKByteSgmiiCounterRead2Clear(self, cAtEthPortCounterRxErrPsnPackets, read2Clear), cAtEthPortCounterRxErrPsnPackets);
    mSaveCounter(rxErrEthHdrPackets,
                 Tha6029DccKByteSgmiiCounterRead2Clear(self, cAtEthPortCounterRxErrEthHdrPackets, read2Clear), cAtEthPortCounterRxErrEthHdrPackets);

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersRead2Clear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersRead2Clear(self, pAllCounters, cAtTrue);
    }

static void InternalSgmiiPortCounterClear(AtChannel self)
    {
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeDccTxFrames, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeDccRxFrames, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeKByteTxFrames, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeKByteRxFrames, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeDccTxBytes, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeDccTxErrors, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeDccRxBytes, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeDccRxErrors, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeKByteTxBytes, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeDccRxUnrecognizedFrames, cAtTrue);
    Tha6029DccKByteSgmiiCounterRead2Clear(self, cTha602SgmiiPortCounterTypeKbyteRxUnrecognizedFrames, cAtTrue);
    }

static void StatusClear(AtChannel self)
    {
    m_AtChannelMethods->StatusClear(self);

    InternalSgmiiPortCounterClear(self);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return Tha6029DccKByteV2SgmiiSupportedInterruptMasks(self);

    return Tha6029DccKByteSgmiiSupportedInterruptMasks(self);
    }

static void OverrideThaEthPort(AtEthPort self)
    {
    ThaEthPort object = (ThaEthPort)self;
    
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthPortMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPortOverride, m_ThaEthPortMethods, sizeof(m_ThaEthPortOverride));

        mMethodOverride(m_ThaEthPortOverride, CounterLocalAddress);
        }

    mMethodsSet(object, &m_ThaEthPortOverride);
    }

static void OverrideAtEthPort(AtEthPort self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPortMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPortOverride, mMethodsGet(self), sizeof(m_AtEthPortOverride));

        mMethodOverride(m_AtEthPortOverride, ExpectedCVlanIsSupported);
        mMethodOverride(m_AtEthPortOverride, ExpectedCVlanIdSet);
        mMethodOverride(m_AtEthPortOverride, ExpectedCVlanIdGet);

        mMethodOverride(m_AtEthPortOverride, ExpectedDestMacSet);
        mMethodOverride(m_AtEthPortOverride, ExpectedDestMacGet);
        mMethodOverride(m_AtEthPortOverride, HasExpectedDestMac);
        mMethodOverride(m_AtEthPortOverride, SohTransparentIsSupported);
        }

    mMethodsSet(self, &m_AtEthPortOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);

        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, StatusClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtEthPort self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtEthPort(self);
    OverrideThaEthPort(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SgmiiSohEthPort);
    }

AtEthPort Tha60290021SgmiiSohEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SgmiiSerdesEthPortObjectInit(self, portId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60290021SgmiiSohEthPortNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021SgmiiSohEthPortObjectInit(newPort, portId, module);
    }

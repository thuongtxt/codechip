/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290021EthPortFlowControlInternal.h
 * 
 * Created Date: Apr 9, 2018
 *
 * Description : Internal data of the 60290021 eth port flow control
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021ETHPORTFLOWCONTROLINTERNAL_H_
#define _THA60290021ETHPORTFLOWCONTROLINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtEthPort.h"
#include "../../../../generic/eth/AtEthFlowControlInternal.h"
#include "../../../default/eth/ThaEthPortFlowControlInternal.h"
#include "Tha60290021EthPortFlowControl.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021EthPortFlowControlMethods
    {
    uint32 (*TxBufferLevelGet)(Tha60290021EthPortFlowControl self);
    }tTha60290021EthPortFlowControlMethods;

typedef struct tTha60290021EthPortFlowControl
    {
    tThaEthPortFlowControl super;
    const tTha60290021EthPortFlowControlMethods *methods;
    }tTha60290021EthPortFlowControl;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthFlowControl Tha60290021EthPortFlowControlObjectInit(AtEthFlowControl self, AtModule module, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021ETHPORTFLOWCONTROLINTERNAL_H_ */


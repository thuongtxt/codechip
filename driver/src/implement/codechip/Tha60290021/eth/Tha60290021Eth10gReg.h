/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CCI0011_RD_ETH10G_H_
#define _AF6_REG_AF6CCI0011_RD_ETH10G_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G Version Control
Reg Addr   : 0x00
Reg Formula: 
    Where  : 
Reg Desc   : 
This register checks version ID

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_ver_ctr_Base                                                                       0x00
#define cAf6Reg_eth10g_ver_ctr_WidthVal                                                                     32

/*--------------------------------------
BitField Name: Eth10gVendor
BitField Type: RO
BitField Desc: Arrive ETH 10G MAC
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_eth10g_ver_ctr_Eth10gVendor_Mask                                                         cBit15_8
#define cAf6_eth10g_ver_ctr_Eth10gVendor_Shift                                                               8

/*--------------------------------------
BitField Name: Eth10GVer
BitField Type: RO
BitField Desc: Version ID
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_eth10g_ver_ctr_Eth10GVer_Mask                                                             cBit7_0
#define cAf6_eth10g_ver_ctr_Eth10GVer_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G Flush Control
Reg Addr   : 0x01
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to flush engine

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_flush_ctr_Base                                                                     0x01
#define cAf6Reg_eth10g_flush_ctr_WidthVal                                                                   32

/*--------------------------------------
BitField Name: Eth10gFlushCtrl
BitField Type: RW
BitField Desc: Flush engine
BitField Bits: [2]
--------------------------------------*/
#define cAf6_eth10g_flush_ctr_Eth10gFlushCtrl_Mask                                                       cBit2
#define cAf6_eth10g_flush_ctr_Eth10gFlushCtrl_Shift                                                          2


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G Protection Control
Reg Addr   : 0x02
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to flush engine

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_protect_ctr_Base                                                                   0x02
#define cAf6Reg_eth10g_protect_ctr_WidthVal                                                                 32

/*--------------------------------------
BitField Name: Eth10gRxPortCtrl
BitField Type: RW
BitField Desc: Active/Standby port
BitField Bits: [12]
--------------------------------------*/
#define cAf6_eth10g_protect_ctr_Eth10gRxPortCtrl_Mask                                                   cBit12
#define cAf6_eth10g_protect_ctr_Eth10gRxPortCtrl_Shift                                                      12

/*--------------------------------------
BitField Name: Eth10gRxDICCtrl
BitField Type: RW
BitField Desc: enable DIC(deficit idle count) funtion
BitField Bits: [9]
--------------------------------------*/
#define cAf6_eth10g_protect_ctr_Eth10gRxDICCtrl_Mask                                                     cBit9
#define cAf6_eth10g_protect_ctr_Eth10gRxDICCtrl_Shift                                                        9


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G Link Fault Interrupt Enable
Reg Addr   : 0x1003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to control link fault function

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_link_fault_int_enb_Base                                                          0x1003

/*--------------------------------------
BitField Name: Eth10gForceTXRF
BitField Type: RW
BitField Desc: Force TX Remote fault, TX will transmit remote fault code to
remote side
BitField Bits: [25]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gForceTXRF_Mask                                             cBit25
#define cAf6_eth10g_link_fault_int_enb_Eth10gForceTXRF_Shift                                                25

/*--------------------------------------
BitField Name: Eth10gForceTXLF
BitField Type: RW
BitField Desc: Force TX Local fault, TX will transmit local fault code to remote
side,
BitField Bits: [24]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gForceTXLF_Mask                                             cBit24
#define cAf6_eth10g_link_fault_int_enb_Eth10gForceTXLF_Shift                                                24

/*--------------------------------------
BitField Name: Eth10gForceRXRF
BitField Type: RW
BitField Desc: Force RX Remote fault, RX will rasie RF interrupt, TX will
transmit idle code to remote side
BitField Bits: [23]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gForceRXRF_Mask                                             cBit23
#define cAf6_eth10g_link_fault_int_enb_Eth10gForceRXRF_Shift                                                23

/*--------------------------------------
BitField Name: Eth10gForceRXLF
BitField Type: RW
BitField Desc: Force RX Local fault, RX will raise LF interrupt, TX will
transmit remote fault code to remote side,
BitField Bits: [22]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gForceRXLF_Mask                                             cBit22
#define cAf6_eth10g_link_fault_int_enb_Eth10gForceRXLF_Shift                                                22

/*--------------------------------------
BitField Name: Eth10gDisLFault
BitField Type: RW
BitField Desc: Disable Link Fault function
BitField Bits: [20]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gDisLFault_Mask                                             cBit20
#define cAf6_eth10g_link_fault_int_enb_Eth10gDisLFault_Shift                                                20

/*--------------------------------------
BitField Name: Eth10gSwitchAyns
BitField Type: RW
BitField Desc: switch data and code words of link fault is asynchronous
BitField Bits: [16]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gSwitchAyns_Mask                                            cBit16
#define cAf6_eth10g_link_fault_int_enb_Eth10gSwitchAyns_Shift                                               16

/*--------------------------------------
BitField Name: Eth10gDataOrFrameSyncEnb
BitField Type: RW
BitField Desc: interrupt enable of Loss of Data Sync
BitField Bits: [6]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gDataOrFrameSyncEnb_Mask                                     cBit6
#define cAf6_eth10g_link_fault_int_enb_Eth10gDataOrFrameSyncEnb_Shift                                        6

/*--------------------------------------
BitField Name: Eth10gClockLossEnb
BitField Type: RW
BitField Desc: interrupt enable of Loss of Clock
BitField Bits: [5]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gClockLossEnb_Mask                                           cBit5
#define cAf6_eth10g_link_fault_int_enb_Eth10gClockLossEnb_Shift                                              5

/*--------------------------------------
BitField Name: Eth10gClockOutRangeEnb
BitField Type: RW
BitField Desc: interrupt enable of Frequency Out of Range
BitField Bits: [4]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gClockOutRangeEnb_Mask                                       cBit4
#define cAf6_eth10g_link_fault_int_enb_Eth10gClockOutRangeEnb_Shift                                          4

/*--------------------------------------
BitField Name: Eth10gExErrorRatioEnb
BitField Type: RW
BitField Desc: interrupt enable of Excessive Error Ratio
BitField Bits: [3]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gExErrorRatioEnb_Mask                                        cBit3
#define cAf6_eth10g_link_fault_int_enb_Eth10gExErrorRatioEnb_Shift                                           3

/*--------------------------------------
BitField Name: Eth10gLocalFaultEnb
BitField Type: RW
BitField Desc: interrupt enable of local fault
BitField Bits: [2]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gLocalFaultEnb_Mask                                          cBit2
#define cAf6_eth10g_link_fault_int_enb_Eth10gLocalFaultEnb_Shift                                             2

/*--------------------------------------
BitField Name: Eth10gRemoteFaultEnb
BitField Type: RW
BitField Desc: interrupt enable of remote fault
BitField Bits: [1]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gRemoteFaultEnb_Mask                                         cBit1
#define cAf6_eth10g_link_fault_int_enb_Eth10gRemoteFaultEnb_Shift                                            1

/*--------------------------------------
BitField Name: Eth10gInterrptionEnb
BitField Type: RW
BitField Desc: interrupt enable of interruption
BitField Bits: [0]
--------------------------------------*/
#define cAf6_eth10g_link_fault_int_enb_Eth10gInterrptionEnb_Mask                                         cBit0
#define cAf6_eth10g_link_fault_int_enb_Eth10gInterrptionEnb_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G Link Fault Sticky
Reg Addr   : 0x100B
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to report sticky of link fault

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_link_fault_sticky_Base                                                           0x100B

/*--------------------------------------
BitField Name: Eth10gDataOrFrameSyncStk
BitField Type: W1C
BitField Desc: sticky state change of Loss of Data Syncsticky state change
BitField Bits: [6]
--------------------------------------*/
#define cAf6_eth10g_link_fault_sticky_Eth10gDataOrFrameSyncStk_Mask                                      cBit6
#define cAf6_eth10g_link_fault_sticky_Eth10gDataOrFrameSyncStk_Shift                                         6

/*--------------------------------------
BitField Name: Eth10gClockLossStk
BitField Type: W1C
BitField Desc: sticky state change of Loss of Clocksticky state change
BitField Bits: [5]
--------------------------------------*/
#define cAf6_eth10g_link_fault_sticky_Eth10gClockLossStk_Mask                                            cBit5
#define cAf6_eth10g_link_fault_sticky_Eth10gClockLossStk_Shift                                               5

/*--------------------------------------
BitField Name: Eth10gClockOutRangeStk
BitField Type: W1C
BitField Desc: sticky state change of Frequency Out of Rangesticky state change
BitField Bits: [4]
--------------------------------------*/
#define cAf6_eth10g_link_fault_sticky_Eth10gClockOutRangeStk_Mask                                        cBit4
#define cAf6_eth10g_link_fault_sticky_Eth10gClockOutRangeStk_Shift                                           4

/*--------------------------------------
BitField Name: Eth10gExErrorRatioStk
BitField Type: W1C
BitField Desc: sticky state change of Excessive Error Ratiosticky state change
BitField Bits: [3]
--------------------------------------*/
#define cAf6_eth10g_link_fault_sticky_Eth10gExErrorRatioStk_Mask                                         cBit3
#define cAf6_eth10g_link_fault_sticky_Eth10gExErrorRatioStk_Shift                                            3

/*--------------------------------------
BitField Name: Eth10gLocalFaultStk
BitField Type: W1C
BitField Desc: sticky state change of local fault sticky state change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_eth10g_link_fault_sticky_Eth10gLocalFaultStk_Mask                                           cBit2
#define cAf6_eth10g_link_fault_sticky_Eth10gLocalFaultStk_Shift                                              2

/*--------------------------------------
BitField Name: Eth10gRemoteFaultStk
BitField Type: W1C
BitField Desc: sticky state change of remote fault
BitField Bits: [1]
--------------------------------------*/
#define cAf6_eth10g_link_fault_sticky_Eth10gRemoteFaultStk_Mask                                          cBit1
#define cAf6_eth10g_link_fault_sticky_Eth10gRemoteFaultStk_Shift                                             1

/*--------------------------------------
BitField Name: Eth10gInterrptionStk
BitField Type: W1C
BitField Desc: sticky state change of interruption
BitField Bits: [0]
--------------------------------------*/
#define cAf6_eth10g_link_fault_sticky_Eth10gInterrptionStk_Mask                                          cBit0
#define cAf6_eth10g_link_fault_sticky_Eth10gInterrptionStk_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G Link Fault Current Status
Reg Addr   : 0x100D
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to report current status of link fault

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_link_fault_Cur_Sta_Base                                                          0x100D

/*--------------------------------------
BitField Name: Eth10gDataOrFrameSyncCur
BitField Type: RO
BitField Desc: sticky state change of Loss of Data Syncsticky state change
BitField Bits: [6]
--------------------------------------*/
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gDataOrFrameSyncCur_Mask                                     cBit6
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gDataOrFrameSyncCur_Shift                                        6

/*--------------------------------------
BitField Name: Eth10gClockLossCur
BitField Type: RO
BitField Desc: sticky state change of Loss of Clocksticky state change
BitField Bits: [5]
--------------------------------------*/
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gClockLossCur_Mask                                           cBit5
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gClockLossCur_Shift                                              5

/*--------------------------------------
BitField Name: Eth10gClockOutRangeCur
BitField Type: RO
BitField Desc: sticky state change of Frequency Out of Rangesticky state change
BitField Bits: [4]
--------------------------------------*/
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gClockOutRangeCur_Mask                                       cBit4
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gClockOutRangeCur_Shift                                          4

/*--------------------------------------
BitField Name: Eth10gExErrorRatioCur
BitField Type: RO
BitField Desc: sticky state change of Excessive Error Ratiosticky state change
BitField Bits: [3]
--------------------------------------*/
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gExErrorRatioCur_Mask                                        cBit3
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gExErrorRatioCur_Shift                                           3

/*--------------------------------------
BitField Name: Eth10gLocalFaultCur
BitField Type: RO
BitField Desc: current status of local fault
BitField Bits: [2]
--------------------------------------*/
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gLocalFaultCur_Mask                                          cBit2
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gLocalFaultCur_Shift                                             2

/*--------------------------------------
BitField Name: Eth10gRemoteFaultCur
BitField Type: RO
BitField Desc: current status of remote fault
BitField Bits: [1]
--------------------------------------*/
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gRemoteFaultCur_Mask                                         cBit1
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gRemoteFaultCur_Shift                                            1

/*--------------------------------------
BitField Name: Eth10gInterrptionCur
BitField Type: RO
BitField Desc: current status of interruption
BitField Bits: [0]
--------------------------------------*/
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gInterrptionCur_Mask                                         cBit0
#define cAf6_eth10g_link_fault_Cur_Sta_Eth10gInterrptionCur_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G Loopback Control
Reg Addr   : 0x40
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure loopback funtion

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_loop_ctr_Base                                                                      0x40
#define cAf6Reg_eth10g_loop_ctr_WidthVal                                                                    32

/*--------------------------------------
BitField Name: Eth10gStandPortLoopoutCtrl
BitField Type: RW
BitField Desc: Standby port loop-out (XGMII rx to XGMII tx)
BitField Bits: [6]
--------------------------------------*/
#define cAf6_eth10g_loop_ctr_Eth10gStandPortLoopoutCtrl_Mask                                             cBit6
#define cAf6_eth10g_loop_ctr_Eth10gStandPortLoopoutCtrl_Shift                                                6

/*--------------------------------------
BitField Name: Eth10gActPortLoopoutCtrl
BitField Type: RW
BitField Desc: Active  port loop-out (XGMII rx to XGMII tx)
BitField Bits: [5]
--------------------------------------*/
#define cAf6_eth10g_loop_ctr_Eth10gActPortLoopoutCtrl_Mask                                               cBit5
#define cAf6_eth10g_loop_ctr_Eth10gActPortLoopoutCtrl_Shift                                                  5

/*--------------------------------------
BitField Name: Eth10gStandPortLoopinCtrl
BitField Type: RW
BitField Desc: Standby port loop-in (XGMII tx to XGMII rx)
BitField Bits: [2]
--------------------------------------*/
#define cAf6_eth10g_loop_ctr_Eth10gStandPortLoopinCtrl_Mask                                              cBit2
#define cAf6_eth10g_loop_ctr_Eth10gStandPortLoopinCtrl_Shift                                                 2

/*--------------------------------------
BitField Name: Eth10gActPortLoopinCtrl
BitField Type: RW
BitField Desc: Active  port loop-in (XGMII tx to XGMII rx)
BitField Bits: [1]
--------------------------------------*/
#define cAf6_eth10g_loop_ctr_Eth10gActPortLoopinCtrl_Mask                                                cBit1
#define cAf6_eth10g_loop_ctr_Eth10gActPortLoopinCtrl_Shift                                                   1


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G MAC Reveive Control
Reg Addr   : 0x42
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure MAC at the reveiver direction

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_mac_rx_ctr_Base                                                                    0x42
#define cAf6Reg_eth10g_mac_rx_ctr_WidthVal                                                                  32

/*--------------------------------------
BitField Name: Eth10gMACRxIpgCtrl
BitField Type: RW
BitField Desc: RX inter packet gap
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_eth10g_mac_rx_ctr_Eth10gMACRxIpgCtrl_Mask                                                cBit11_8
#define cAf6_eth10g_mac_rx_ctr_Eth10gMACRxIpgCtrl_Shift                                                      8

/*--------------------------------------
BitField Name: Eth10gMACRxFCSBypassCtrl
BitField Type: RW
BitField Desc: Bypass the received FCS 4-byte
BitField Bits: [0]
--------------------------------------*/
#define cAf6_eth10g_mac_rx_ctr_Eth10gMACRxFCSBypassCtrl_Mask                                             cBit0
#define cAf6_eth10g_mac_rx_ctr_Eth10gMACRxFCSBypassCtrl_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : ETH 10G MAC Transmit Control
Reg Addr   : 0x43
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is used to configure MAC at the reveiver direction

------------------------------------------------------------------------------*/
#define cAf6Reg_eth10g_mac_tx_ctr_Base                                                                    0x43
#define cAf6Reg_eth10g_mac_tx_ctr_WidthVal                                                                  32

/*--------------------------------------
BitField Name: Eth10gMACTxIpgCtrl
BitField Type: RW
BitField Desc: TX inter packet gap
BitField Bits: [12:8]
--------------------------------------*/
#define cAf6_eth10g_mac_tx_ctr_Eth10gMACTxIpgCtrl_Mask                                                cBit12_8
#define cAf6_eth10g_mac_tx_ctr_Eth10gMACTxIpgCtrl_Shift                                                      8

/*--------------------------------------
BitField Name: Eth10gMACTxFCSInsCtrl
BitField Type: RW
BitField Desc: Insert FCS 4-byte
BitField Bits: [0]
--------------------------------------*/
#define cAf6_eth10g_mac_tx_ctr_Eth10gMACTxFCSInsCtrl_Mask                                                cBit0
#define cAf6_eth10g_mac_tx_ctr_Eth10gMACTxFCSInsCtrl_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count0_64 bytes packet
Reg Addr   : 0x0003000(RO)
Reg Formula: 0x0003000 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt0_64_ro_Base                                                                0x0003000

/*--------------------------------------
BitField Name: TxEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt0_64_ro_TxEthCnt0_64_Mask                                                       cBit31_0
#define cAf6_TxEth_cnt0_64_ro_TxEthCnt0_64_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count0_64 bytes packet
Reg Addr   : 0x0003800(RC)
Reg Formula: 0x0003800 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 0 to 64 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt0_64_rc_Base                                                                0x0003800

/*--------------------------------------
BitField Name: TxEthCnt0_64
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 0 to 64 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt0_64_rc_TxEthCnt0_64_Mask                                                       cBit31_0
#define cAf6_TxEth_cnt0_64_rc_TxEthCnt0_64_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count65_127 bytes packet
Reg Addr   : 0x0003002(RO)
Reg Formula: 0x0003002 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt65_127_ro_Base                                                              0x0003002

/*--------------------------------------
BitField Name: TxEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt65_127_ro_TxEthCnt65_127_Mask                                                   cBit31_0
#define cAf6_TxEth_cnt65_127_ro_TxEthCnt65_127_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count65_127 bytes packet
Reg Addr   : 0x0003802(RC)
Reg Formula: 0x0003802 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 65 to 127 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt65_127_rc_Base                                                              0x0003802

/*--------------------------------------
BitField Name: TxEthCnt65_127
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 65 to 127 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt65_127_rc_TxEthCnt65_127_Mask                                                   cBit31_0
#define cAf6_TxEth_cnt65_127_rc_TxEthCnt65_127_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count128_255 bytes packet
Reg Addr   : 0x0003004(RO)
Reg Formula: 0x0003004 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt128_255_ro_Base                                                             0x0003004

/*--------------------------------------
BitField Name: TxEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt128_255_ro_TxEthCnt128_255_Mask                                                 cBit31_0
#define cAf6_TxEth_cnt128_255_ro_TxEthCnt128_255_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count128_255 bytes packet
Reg Addr   : 0x0003804(RC)
Reg Formula: 0x0003804 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 128 to 255 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt128_255_rc_Base                                                             0x0003804

/*--------------------------------------
BitField Name: TxEthCnt128_255
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 128 to 255 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt128_255_rc_TxEthCnt128_255_Mask                                                 cBit31_0
#define cAf6_TxEth_cnt128_255_rc_TxEthCnt128_255_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count256_511 bytes packet
Reg Addr   : 0x0003006(RO)
Reg Formula: 0x0003006 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt256_511_ro_Base                                                             0x0003006

/*--------------------------------------
BitField Name: TxEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt256_511_ro_TxEthCnt256_511_Mask                                                 cBit31_0
#define cAf6_TxEth_cnt256_511_ro_TxEthCnt256_511_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count256_511 bytes packet
Reg Addr   : 0x0003806(RC)
Reg Formula: 0x0003806 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 256 to 511 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt256_511_rc_Base                                                             0x0003806

/*--------------------------------------
BitField Name: TxEthCnt256_511
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 256 to 511 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt256_511_rc_TxEthCnt256_511_Mask                                                 cBit31_0
#define cAf6_TxEth_cnt256_511_rc_TxEthCnt256_511_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count512_1023 bytes packet
Reg Addr   : 0x0003008(RO)
Reg Formula: 0x0003008 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt512_1024_ro_Base                                                            0x0003008

/*--------------------------------------
BitField Name: TxEthCnt512_1024
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt512_1024_ro_TxEthCnt512_1024_Mask                                               cBit31_0
#define cAf6_TxEth_cnt512_1024_ro_TxEthCnt512_1024_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count512_1023 bytes packet
Reg Addr   : 0x0003808(RC)
Reg Formula: 0x0003808 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 512 to 1023 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt512_1024_rc_Base                                                            0x0003808

/*--------------------------------------
BitField Name: TxEthCnt512_1024
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 512 to 1023 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt512_1024_rc_TxEthCnt512_1024_Mask                                               cBit31_0
#define cAf6_TxEth_cnt512_1024_rc_TxEthCnt512_1024_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1024_1518 bytes packet
Reg Addr   : 0x000300A(RO)
Reg Formula: 0x000300A + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1025_1528_ro_Base                                                             0x000300A

/*--------------------------------------
BitField Name: TxEthCnt1025_1528
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1025_1528_ro_TxEthCnt1025_1528_Mask                                               cBit31_0
#define cAf6_Eth_cnt1025_1528_ro_TxEthCnt1025_1528_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1024_1518 bytes packet
Reg Addr   : 0x000380A(RC)
Reg Formula: 0x000380A + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1024 to 1518 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_Eth_cnt1025_1528_rc_Base                                                             0x000380A

/*--------------------------------------
BitField Name: TxEthCnt1025_1528
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1024 to 1518
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_Eth_cnt1025_1528_rc_TxEthCnt1025_1528_Mask                                               cBit31_0
#define cAf6_Eth_cnt1025_1528_rc_TxEthCnt1025_1528_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1519_2047 bytes packet
Reg Addr   : 0x000300C(RO)
Reg Formula: 0x000300C + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt1529_2047_ro_Base                                                           0x000300C

/*--------------------------------------
BitField Name: TxEthCnt1529_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt1529_2047_ro_TxEthCnt1529_2047_Mask                                             cBit31_0
#define cAf6_TxEth_cnt1529_2047_ro_TxEthCnt1529_2047_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count1519_2047 bytes packet
Reg Addr   : 0x000380C(RC)
Reg Formula: 0x000380C + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having 1519 to 2047 bytes

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt1529_2047_rc_Base                                                           0x000380C

/*--------------------------------------
BitField Name: TxEthCnt1529_2047
BitField Type: RO
BitField Desc: This is statistic counter for the packet having 1519 to 2047
bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt1529_2047_rc_TxEthCnt1529_2047_Mask                                             cBit31_0
#define cAf6_TxEth_cnt1529_2047_rc_TxEthCnt1529_2047_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Jumbo packet
Reg Addr   : 0x000300E(RO)
Reg Formula: 0x000300E + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt_jumbo_ro_Base                                                              0x000300E

/*--------------------------------------
BitField Name: TxEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt_jumbo_ro_TxEthCntJumbo_Mask                                                    cBit31_0
#define cAf6_TxEth_cnt_jumbo_ro_TxEthCntJumbo_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Jumbo packet
Reg Addr   : 0x000380E(RC)
Reg Formula: 0x000380E + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the packet having more than 2048 bytes (jumbo)

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt_jumbo_rc_Base                                                              0x000380E

/*--------------------------------------
BitField Name: TxEthCntJumbo
BitField Type: RO
BitField Desc: This is statistic counter for the packet more than 2048 bytes
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt_jumbo_rc_TxEthCntJumbo_Mask                                                    cBit31_0
#define cAf6_TxEth_cnt_jumbo_rc_TxEthCntJumbo_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Total packet
Reg Addr   : 0x0003012(RO)
Reg Formula: 0x0003012 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the total packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_pkt_cnt_ro_Base                                                               0x0003012

/*--------------------------------------
BitField Name: TxEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_pkt_cnt_ro_TxEthCntTotal_Mask                                                     cBit31_0
#define cAf6_eth_tx_pkt_cnt_ro_TxEthCntTotal_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count Total packet
Reg Addr   : 0x0003812(RC)
Reg Formula: 0x0003812 + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic counter for the total packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_eth_tx_pkt_cnt_rc_Base                                                               0x0003812

/*--------------------------------------
BitField Name: TxEthCntTotal
BitField Type: RO
BitField Desc: This is statistic counter total packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_eth_tx_pkt_cnt_rc_TxEthCntTotal_Mask                                                     cBit31_0
#define cAf6_eth_tx_pkt_cnt_rc_TxEthCntTotal_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count number of bytes of packet
Reg Addr   : 0x000301E(RO)
Reg Formula: 0x000301E + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic count number of bytes of packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt_byte_ro_Base                                                               0x000301E

/*--------------------------------------
BitField Name: TxEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt_byte_ro_TxEthCntByte_Mask                                                      cBit31_0
#define cAf6_TxEth_cnt_byte_ro_TxEthCntByte_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Transmit Ethernet port Count number of bytes of packet
Reg Addr   : 0x000381E(RC)
Reg Formula: 0x000381E + eth_port
    Where  : 
           + $eth_port(0-0): Transmit Ethernet port ID
Reg Desc   : 
This register is statistic count number of bytes of packet at Transmit side

------------------------------------------------------------------------------*/
#define cAf6Reg_TxEth_cnt_byte_rc_Base                                                               0x000381E

/*--------------------------------------
BitField Name: TxEthCntByte
BitField Type: RO
BitField Desc: This is statistic counter number of bytes of packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_TxEth_cnt_byte_rc_TxEthCntByte_Mask                                                      cBit31_0
#define cAf6_TxEth_cnt_byte_rc_TxEthCntByte_Shift                                                            0

#endif /* _AF6_REG_AF6CCI0011_RD_ETH10G_H_ */

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0022_RD_DCCK_GMII_H_
#define _AF6_REG_AF6CNC0022_RD_DCCK_GMII_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : DCCK GMII Control
Reg Addr   : 0x10
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures parameters for DCCK GMII

------------------------------------------------------------------------------*/
#define cAf6Reg_dcckgmiicfg_Base                                                                          0x10
#define cAf6Reg_dcckgmiicfg_WidthVal                                                                        32

/*--------------------------------------
BitField Name: DcckGmiiLoopOut
BitField Type: RW
BitField Desc: DCCK GMII Remote Loop Out
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dcckgmiicfg_DcckGmiiLoopOut_Mask                                                            cBit9
#define cAf6_dcckgmiicfg_DcckGmiiLoopOut_Shift                                                               9

/*--------------------------------------
BitField Name: DcckGmiiLoopIn
BitField Type: RW
BitField Desc: DCCK GMII Local Loop In
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dcckgmiicfg_DcckGmiiLoopIn_Mask                                                             cBit8
#define cAf6_dcckgmiicfg_DcckGmiiLoopIn_Shift                                                                8

/*--------------------------------------
BitField Name: DcckGmiiPreNum
BitField Type: RW
BitField Desc: DCCK GMII Preamble Number of Byte
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_dcckgmiicfg_DcckGmiiPreNum_Mask                                                           cBit7_4
#define cAf6_dcckgmiicfg_DcckGmiiPreNum_Shift                                                                4

/*--------------------------------------
BitField Name: DcckGmiiIpgNum
BitField Type: RW
BitField Desc: DCCK GMII Interpacket GAP Number of Byte
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_dcckgmiicfg_DcckGmiiIpgNum_Mask                                                           cBit3_0
#define cAf6_dcckgmiicfg_DcckGmiiIpgNum_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : DCCK GMII Diagnostic Enable Control
Reg Addr   : 0x11
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures parameters for DCCK GMII

------------------------------------------------------------------------------*/
#define cAf6Reg_dcckgmiidiagcfg_Base                                                                      0x11

/*--------------------------------------
BitField Name: DcckGmiiDiagMaxLen
BitField Type: RW
BitField Desc: DCCK GMII Diagnostic Max length count from zero
BitField Bits: [31:18]
--------------------------------------*/
#define cAf6_dcckgmiidiagcfg_DcckGmiiDiagMaxLen_Mask                                                 cBit31_18
#define cAf6_dcckgmiidiagcfg_DcckGmiiDiagMaxLen_Shift                                                       18

/*--------------------------------------
BitField Name: DcckGmiiDiagMinLen
BitField Type: RW
BitField Desc: DCCK GMII Diagnostic Min length count from zero
BitField Bits: [17:4]
--------------------------------------*/
#define cAf6_dcckgmiidiagcfg_DcckGmiiDiagMinLen_Mask                                                  cBit17_4
#define cAf6_dcckgmiidiagcfg_DcckGmiiDiagMinLen_Shift                                                        4

/*--------------------------------------
BitField Name: DcckGmiiForceFcsErr
BitField Type: RW
BitField Desc: DCCK GMII Force FCS error
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dcckgmiidiagcfg_DcckGmiiForceFcsErr_Mask                                                    cBit3
#define cAf6_dcckgmiidiagcfg_DcckGmiiForceFcsErr_Shift                                                       3

/*--------------------------------------
BitField Name: DcckGmiiDiagForceDatErr
BitField Type: RW
BitField Desc: DCCK GMII Diagnostic Force Data error
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dcckgmiidiagcfg_DcckGmiiDiagForceDatErr_Mask                                                cBit2
#define cAf6_dcckgmiidiagcfg_DcckGmiiDiagForceDatErr_Shift                                                   2

/*--------------------------------------
BitField Name: DcckGmiiDiagGenPkEn
BitField Type: RW
BitField Desc: DCCK GMII Diagnostic Generate packet enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dcckgmiidiagcfg_DcckGmiiDiagGenPkEn_Mask                                                    cBit1
#define cAf6_dcckgmiidiagcfg_DcckGmiiDiagGenPkEn_Shift                                                       1

/*--------------------------------------
BitField Name: DcckGmiiDiagEn
BitField Type: RW
BitField Desc: DCCK GMII Diagnostic enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dcckgmiidiagcfg_DcckGmiiDiagEn_Mask                                                         cBit0
#define cAf6_dcckgmiidiagcfg_DcckGmiiDiagEn_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : DCCK GMII Datapath and Diagnostic sticky
Reg Addr   : 0x12
Reg Formula: 
    Where  : 
Reg Desc   : 
This register show alarms of DCCK GMII

------------------------------------------------------------------------------*/
#define cAf6Reg_dcckgmiistk_Base                                                                          0x12
#define cAf6Reg_dcckgmiistk_WidthVal                                                                        32

/*--------------------------------------
BitField Name: DcckGmiiTxFiFoConvErr
BitField Type: RWC
BitField Desc: DCCK GMII Tx FIFO Convert Error
BitField Bits: [11]
--------------------------------------*/
#define cAf6_dcckgmiistk_DcckGmiiTxFiFoConvErr_Mask                                                     cBit11
#define cAf6_dcckgmiistk_DcckGmiiTxFiFoConvErr_Shift                                                        11

/*--------------------------------------
BitField Name: DcckGmiiTxFcsErr
BitField Type: RWC
BitField Desc: DCCK GMII Tx FCS Error
BitField Bits: [10]
--------------------------------------*/
#define cAf6_dcckgmiistk_DcckGmiiTxFcsErr_Mask                                                          cBit10
#define cAf6_dcckgmiistk_DcckGmiiTxFcsErr_Shift                                                             10

/*--------------------------------------
BitField Name: DcckGmiiTxLostSop
BitField Type: RWC
BitField Desc: DCCK GMII Tx Lost SOP
BitField Bits: [9]
--------------------------------------*/
#define cAf6_dcckgmiistk_DcckGmiiTxLostSop_Mask                                                          cBit9
#define cAf6_dcckgmiistk_DcckGmiiTxLostSop_Shift                                                             9

/*--------------------------------------
BitField Name: DcckGmiiRxPreErr
BitField Type: RWC
BitField Desc: DCCK GMII Rx Preamble Error
BitField Bits: [8]
--------------------------------------*/
#define cAf6_dcckgmiistk_DcckGmiiRxPreErr_Mask                                                           cBit8
#define cAf6_dcckgmiistk_DcckGmiiRxPreErr_Shift                                                              8

/*--------------------------------------
BitField Name: DcckGmiiRxSfdErr
BitField Type: RWC
BitField Desc: DCCK GMII Rx SFD Error
BitField Bits: [7]
--------------------------------------*/
#define cAf6_dcckgmiistk_DcckGmiiRxSfdErr_Mask                                                           cBit7
#define cAf6_dcckgmiistk_DcckGmiiRxSfdErr_Shift                                                              7

/*--------------------------------------
BitField Name: DcckGmiiRxDvErr
BitField Type: RWC
BitField Desc: DCCK GMII Rx DV Error
BitField Bits: [6]
--------------------------------------*/
#define cAf6_dcckgmiistk_DcckGmiiRxDvErr_Mask                                                            cBit6
#define cAf6_dcckgmiistk_DcckGmiiRxDvErr_Shift                                                               6

/*--------------------------------------
BitField Name: DcckGmiiRxFcsErr
BitField Type: RWC
BitField Desc: DCCK GMII Rx FCS Error
BitField Bits: [5]
--------------------------------------*/
#define cAf6_dcckgmiistk_DcckGmiiRxFcsErr_Mask                                                           cBit5
#define cAf6_dcckgmiistk_DcckGmiiRxFcsErr_Shift                                                              5

/*--------------------------------------
BitField Name: DcckGmiiRxErr
BitField Type: RWC
BitField Desc: DCCK GMII Rx Error
BitField Bits: [4]
--------------------------------------*/
#define cAf6_dcckgmiistk_DcckGmiiRxErr_Mask                                                              cBit4
#define cAf6_dcckgmiistk_DcckGmiiRxErr_Shift                                                                 4

/*--------------------------------------
BitField Name: DcckGmiiRxFiFoConvErr
BitField Type: RWC
BitField Desc: DCCK GMII Rx FIFO Convert Error
BitField Bits: [3]
--------------------------------------*/
#define cAf6_dcckgmiistk_DcckGmiiRxFiFoConvErr_Mask                                                      cBit3
#define cAf6_dcckgmiistk_DcckGmiiRxFiFoConvErr_Shift                                                         3

/*--------------------------------------
BitField Name: DcckGmiiRxMinLenErr
BitField Type: RWC
BitField Desc: DCCK GMII Rx Min Len Error
BitField Bits: [2]
--------------------------------------*/
#define cAf6_dcckgmiistk_DcckGmiiRxMinLenErr_Mask                                                        cBit2
#define cAf6_dcckgmiistk_DcckGmiiRxMinLenErr_Shift                                                           2

/*--------------------------------------
BitField Name: DcckGmiiRxMaxLenErr
BitField Type: RWC
BitField Desc: DCCK GMII Rx Max Len Error
BitField Bits: [1]
--------------------------------------*/
#define cAf6_dcckgmiistk_DcckGmiiRxMaxLenErr_Mask                                                        cBit1
#define cAf6_dcckgmiistk_DcckGmiiRxMaxLenErr_Shift                                                           1

/*--------------------------------------
BitField Name: DcckGmiiDiagRxDatErr
BitField Type: RWC
BitField Desc: DCCK GMII Diagnostic Rx Data Error
BitField Bits: [0]
--------------------------------------*/
#define cAf6_dcckgmiistk_DcckGmiiDiagRxDatErr_Mask                                                       cBit0
#define cAf6_dcckgmiistk_DcckGmiiDiagRxDatErr_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCCK GMII Tx Packet Counter
Reg Addr   : 0x00
Reg Formula: 
    Where  : 
Reg Desc   : 
This register show GMII Tx Packet Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_dcckgmiitxpktcnt_Base                                                                     0x00
#define cAf6Reg_dcckgmiitxpktcnt_WidthVal                                                                   32

/*--------------------------------------
BitField Name: DcckGmiiTxPktCnt
BitField Type: RC
BitField Desc: DCCK GMII Tx Packet Counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dcckgmiitxpktcnt_DcckGmiiTxPktCnt_Mask                                                   cBit15_0
#define cAf6_dcckgmiitxpktcnt_DcckGmiiTxPktCnt_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : DCCK GMII Tx Packet Counter
Reg Addr   : 0x01
Reg Formula: 
    Where  : 
Reg Desc   : 
This register show GMII Tx Byte Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_dcckgmiitxbytecnt_Base                                                                    0x01

/*--------------------------------------
BitField Name: DcckGmiiTxByteCnt
BitField Type: RC
BitField Desc: DCCK GMII Tx Byte Counter
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dcckgmiitxbytecnt_DcckGmiiTxByteCnt_Mask                                                 cBit31_0
#define cAf6_dcckgmiitxbytecnt_DcckGmiiTxByteCnt_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCCK GMII Tx Packet FCS Error Counter
Reg Addr   : 0x02
Reg Formula: 
    Where  : 
Reg Desc   : 
This register show GMII Tx Packet FCS Error Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_dcckgmiitxfcserrcnt_Base                                                                  0x02
#define cAf6Reg_dcckgmiitxfcserrcnt_WidthVal                                                                32

/*--------------------------------------
BitField Name: DcckGmiiTxPktFcsErrCnt
BitField Type: RC
BitField Desc: DCCK GMII Tx Packet FCS Error Counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dcckgmiitxfcserrcnt_DcckGmiiTxPktFcsErrCnt_Mask                                          cBit15_0
#define cAf6_dcckgmiitxfcserrcnt_DcckGmiiTxPktFcsErrCnt_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : DCCK GMII Rx Packet Counter
Reg Addr   : 0x03
Reg Formula: 
    Where  : 
Reg Desc   : 
This register show GMII Rx Packet Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_dcckgmiirxpktcnt_Base                                                                     0x03
#define cAf6Reg_dcckgmiirxpktcnt_WidthVal                                                                   32

/*--------------------------------------
BitField Name: DcckGmiiRxPktCnt
BitField Type: RC
BitField Desc: DCCK GMII Rx Packet Counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dcckgmiirxpktcnt_DcckGmiiRxPktCnt_Mask                                                   cBit15_0
#define cAf6_dcckgmiirxpktcnt_DcckGmiiRxPktCnt_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : DCCK GMII Rx Packet Counter
Reg Addr   : 0x04
Reg Formula: 
    Where  : 
Reg Desc   : 
This register show GMII Rx Byte Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_dcckgmiirxbytecnt_Base                                                                    0x04

/*--------------------------------------
BitField Name: DcckGmiiRxByteCnt
BitField Type: RC
BitField Desc: DCCK GMII Rx Byte Counter
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dcckgmiirxbytecnt_DcckGmiiRxByteCnt_Mask                                                 cBit31_0
#define cAf6_dcckgmiirxbytecnt_DcckGmiiRxByteCnt_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCCK GMII Rx Packet FCS Error Counter
Reg Addr   : 0x05
Reg Formula: 
    Where  : 
Reg Desc   : 
This register show GMII Rx Packet FCS Error Counter

------------------------------------------------------------------------------*/
#define cAf6Reg_dcckgmiirxfcserrcnt_Base                                                                  0x05
#define cAf6Reg_dcckgmiirxfcserrcnt_WidthVal                                                                32

/*--------------------------------------
BitField Name: DcckGmiiRxPktFcsErrCnt
BitField Type: RC
BitField Desc: DCCK GMII Rx Packet FCS Error Counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_dcckgmiirxfcserrcnt_DcckGmiiRxPktFcsErrCnt_Mask                                          cBit15_0
#define cAf6_dcckgmiirxfcserrcnt_DcckGmiiRxPktFcsErrCnt_Shift                                                0

#endif /* _AF6_REG_AF6CNC0022_RD_DCCK_GMII_H_ */

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_GMII_RD_DIAG_H_
#define _AF6_REG_AF6CNC0021_GMII_RD_DIAG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : MII Test Enable.
Reg Addr   : 0x302
Reg Formula: 0x302
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to enable test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_pen9_Base                                                                        0x302
#define cAf6Reg_control_pen9_WidthVal                                                                       32

/*--------------------------------------
BitField Name: Channel0_Testen
BitField Type: R/W
BitField Desc: Set 1 to enable test MII channel 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_control_pen9_Channel0_Testen_Mask                                                           cBit0
#define cAf6_control_pen9_Channel0_Testen_Shift                                                              0

/*--------------------------------------
BitField Name: Channel1_Testen
BitField Type: R/W
BitField Desc: Set 1 to enable test MII channel 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_control_pen9_Channel1_Testen_Mask                                                           cBit1
#define cAf6_control_pen9_Channel1_Testen_Shift                                                              1

/*--------------------------------------
BitField Name: Channel2_Testen
BitField Type: R/W
BitField Desc: Set 1 to enable test MII channel 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_control_pen9_Channel2_Testen_Mask                                                           cBit2
#define cAf6_control_pen9_Channel2_Testen_Shift                                                              2

/*--------------------------------------
BitField Name: Channel3_Testen
BitField Type: R/W
BitField Desc: Set 1 to enable test MII channel 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_control_pen9_Channel3_Testen_Mask                                                           cBit3
#define cAf6_control_pen9_Channel3_Testen_Shift                                                              3


/*------------------------------------------------------------------------------
Reg Name   : MII Channels Enable.
Reg Addr   : 0x00 - 0x30
Reg Formula: 0x00 + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_pen1_Base                                                                         0x00

/*--------------------------------------
BitField Name: Test_enable
BitField Type: R/W
BitField Desc: Set 1 to enable Tx test generator
BitField Bits: [0]
--------------------------------------*/
#define cAf6_control_pen1_Test_enable_Mask                                                               cBit0
#define cAf6_control_pen1_Test_enable_Shift                                                                  0

/*--------------------------------------
BitField Name: start_gatetime_diag
BitField Type: RW
BitField Desc: Config start Diagnostic trigger 0 to 1 for Start auto run with
Gatetime Configuration
BitField Bits: [1]
--------------------------------------*/
#define cAf6_control_pen1_start_gatetime_diag_Mask                                                       cBit1
#define cAf6_control_pen1_start_gatetime_diag_Shift                                                          1

/*--------------------------------------
BitField Name: status_gatetime_diag
BitField Type: RO
BitField Desc: Status Gatetime diagnostic 1:Running 0:Done-Ready
BitField Bits: [2]
--------------------------------------*/
#define cAf6_control_pen1_status_gatetime_diag_Mask                                                      cBit2
#define cAf6_control_pen1_status_gatetime_diag_Shift                                                         2

/*--------------------------------------
BitField Name: Unsed
BitField Type: R/W
BitField Desc: Unsed
BitField Bits: [14:2]
--------------------------------------*/
#define cAf6_control_pen1_Unsed_Mask                                                                  cBit14_2
#define cAf6_control_pen1_Unsed_Shift                                                                        2

/*--------------------------------------
BitField Name: time_cfg
BitField Type: RW
BitField Desc: Gatetime Configuration 1-86400 second
BitField Bits: [31:15]
--------------------------------------*/
#define cAf6_control_pen1_time_cfg_Mask                                                              cBit31_15
#define cAf6_control_pen1_time_cfg_Shift                                                                    15


/*------------------------------------------------------------------------------
Reg Name   : MII Burst test enable gen packet.
Reg Addr   : 0x0f - 0x3f
Reg Formula: 0x0f + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_pen2_Base                                                                         0x0f

/*--------------------------------------
BitField Name: Burst_gen_packet_number
BitField Type: R/W
BitField Desc: Burst gen packet number, must configure burst number > 0 to
generator packet, hardware will clear number packet configure when finish.
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_control_pen2_Burst_gen_packet_number_Mask                                                cBit31_0
#define cAf6_control_pen2_Burst_gen_packet_number_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : MII Channels Sticky alarm.
Reg Addr   : 0x01 - 0x31
Reg Formula: 0x01 + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_Alarm_sticky_Base                                                                         0x01

/*--------------------------------------
BitField Name: Mon_packet_error
BitField Type: R/W/C
BitField Desc: Set 1 when monitor packet error.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Alarm_sticky_Mon_packet_error_Mask                                                          cBit0
#define cAf6_Alarm_sticky_Mon_packet_error_Shift                                                             0

/*--------------------------------------
BitField Name: Mon_packet_len_error
BitField Type: R/W/C
BitField Desc: Set 1 when monitor packet len error.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Alarm_sticky_Mon_packet_len_error_Mask                                                      cBit1
#define cAf6_Alarm_sticky_Mon_packet_len_error_Shift                                                         1

/*--------------------------------------
BitField Name: Mon_FCS_error
BitField Type: R/W/C
BitField Desc: Set 1 when monitor FCS error.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Alarm_sticky_Mon_FCS_error_Mask                                                             cBit2
#define cAf6_Alarm_sticky_Mon_FCS_error_Shift                                                                2

/*--------------------------------------
BitField Name: Mon_data_error
BitField Type: R/W/C
BitField Desc: Set 1 when monitor data error.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Alarm_sticky_Mon_data_error_Mask                                                            cBit3
#define cAf6_Alarm_sticky_Mon_data_error_Shift                                                               3

/*--------------------------------------
BitField Name: Mon_MAC_header_error
BitField Type: R/W/C
BitField Desc: Set 1 when monitor MAC header error.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Alarm_sticky_Mon_MAC_header_error_Mask                                                      cBit4
#define cAf6_Alarm_sticky_Mon_MAC_header_error_Shift                                                         4

/*--------------------------------------
BitField Name: Mon_Data_sync
BitField Type: R/W/C
BitField Desc: Set 1 when monitor Data sync.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Alarm_sticky_Mon_Data_sync_Mask                                                             cBit5
#define cAf6_Alarm_sticky_Mon_Data_sync_Shift                                                                5


/*------------------------------------------------------------------------------
Reg Name   : MII Channels Status alarm.
Reg Addr   : 0x0E - 0x3E
Reg Formula: 0x0E + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_Alarm_status_Base                                                                         0x0E

/*--------------------------------------
BitField Name: Mon_packet_error
BitField Type: RO
BitField Desc: Set 1 when monitor packet error.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_Alarm_status_Mon_packet_error_Mask                                                          cBit0
#define cAf6_Alarm_status_Mon_packet_error_Shift                                                             0

/*--------------------------------------
BitField Name: Mon_packet_len_error
BitField Type: RO
BitField Desc: Set 1 when monitor packet len error.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_Alarm_status_Mon_packet_len_error_Mask                                                      cBit1
#define cAf6_Alarm_status_Mon_packet_len_error_Shift                                                         1

/*--------------------------------------
BitField Name: Mon_FCS_error
BitField Type: RO
BitField Desc: Set 1 when monitor FCS error.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_Alarm_status_Mon_FCS_error_Mask                                                             cBit2
#define cAf6_Alarm_status_Mon_FCS_error_Shift                                                                2

/*--------------------------------------
BitField Name: Mon_data_error
BitField Type: RO
BitField Desc: Set 1 when monitor data error.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_Alarm_status_Mon_data_error_Mask                                                            cBit3
#define cAf6_Alarm_status_Mon_data_error_Shift                                                               3

/*--------------------------------------
BitField Name: Mon_MAC_header_error
BitField Type: RO
BitField Desc: Set 1 when monitor MAC header error.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_Alarm_status_Mon_MAC_header_error_Mask                                                      cBit4
#define cAf6_Alarm_status_Mon_MAC_header_error_Shift                                                         4

/*--------------------------------------
BitField Name: Mon_Data_sync
BitField Type: RO
BitField Desc: Set 1 when monitor Data sync.
BitField Bits: [5]
--------------------------------------*/
#define cAf6_Alarm_status_Mon_Data_sync_Mask                                                             cBit5
#define cAf6_Alarm_status_Mon_Data_sync_Shift                                                                5

/*--------------------------------------
BitField Name: currert_gatetime_diag
BitField Type: RO
BitField Desc: Current running time of Gatetime diagnostic
BitField Bits: [23:7]
--------------------------------------*/
#define cAf6_Alarm_status_currert_gatetime_diag_Mask                                                  cBit23_7
#define cAf6_Alarm_status_currert_gatetime_diag_Shift                                                        7


/*------------------------------------------------------------------------------
Reg Name   : MII Channels Test mode control.
Reg Addr   : 0x02 - 0x32
Reg Formula: 0x02 + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_pen3_Base                                                                         0x02

/*--------------------------------------
BitField Name: Loopout_enable
BitField Type: R/W
BitField Desc: FPGA loopout enable: 0/1 -> Dis/En.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_control_pen3_Loopout_enable_Mask                                                            cBit0
#define cAf6_control_pen3_Loopout_enable_Shift                                                               0

/*--------------------------------------
BitField Name: Mac_header_check_enable
BitField Type: R/W
BitField Desc: Mac header check enable: 0/1 -> Dis/En.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_control_pen3_Mac_header_check_enable_Mask                                                   cBit1
#define cAf6_control_pen3_Mac_header_check_enable_Shift                                                      1

/*--------------------------------------
BitField Name: Configure_test_mode
BitField Type: R/W
BitField Desc: Configure test mode: 0/1 -> Continues/Burst.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_control_pen3_Configure_test_mode_Mask                                                       cBit2
#define cAf6_control_pen3_Configure_test_mode_Shift                                                          2

/*--------------------------------------
BitField Name: Invert_FCS_enable
BitField Type: R/W
BitField Desc: Invert FCS enable: 0/1 -> Dis/En.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_control_pen3_Invert_FCS_enable_Mask                                                         cBit3
#define cAf6_control_pen3_Invert_FCS_enable_Shift                                                            3

/*--------------------------------------
BitField Name: Force_error
BitField Type: R/W
BitField Desc: Force error: 0/1/2/3 -> Normal/Data/Force FCS/Force Framer.
BitField Bits: [5:4]
--------------------------------------*/
#define cAf6_control_pen3_Force_error_Mask                                                             cBit5_4
#define cAf6_control_pen3_Force_error_Shift                                                                  4

/*--------------------------------------
BitField Name: Band_width
BitField Type: R/W
BitField Desc: Bandwidth: 0/1/2/3 -> 100%/90%/80%/70%.
BitField Bits: [7:6]
--------------------------------------*/
#define cAf6_control_pen3_Band_width_Mask                                                              cBit7_6
#define cAf6_control_pen3_Band_width_Shift                                                                   6

/*--------------------------------------
BitField Name: Data_mode
BitField Type: R/W
BitField Desc: Data mode: 0:PRBS7 1:PRBS15 2:PRBS23 3:PRBS31 4:FIX
BitField Bits: [10:8]
--------------------------------------*/
#define cAf6_control_pen3_Data_mode_Mask                                                              cBit10_8
#define cAf6_control_pen3_Data_mode_Shift                                                                    8

/*--------------------------------------
BitField Name: Fix_data_value
BitField Type: R/W
BitField Desc: Fix data value.
BitField Bits: [19:12]
--------------------------------------*/
#define cAf6_control_pen3_Fix_data_value_Mask                                                        cBit19_12
#define cAf6_control_pen3_Fix_data_value_Shift                                                              12

/*--------------------------------------
BitField Name: Payload_size
BitField Type: R/W
BitField Desc: Payload size.
BitField Bits: [31:20]
--------------------------------------*/
#define cAf6_control_pen3_Payload_size_Mask                                                          cBit31_20
#define cAf6_control_pen3_Payload_size_Shift                                                                20


/*------------------------------------------------------------------------------
Reg Name   : MII Channels Configure Mac DA MSB.
Reg Addr   : 0x03 - 0x33
Reg Formula: 0x03 + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_pen4_Base                                                                         0x03

/*--------------------------------------
BitField Name: Mac_DA_configure_MSB
BitField Type: R/W
BitField Desc: Mac DA MSB configure value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_control_pen4_Mac_DA_configure_MSB_Mask                                                   cBit31_0
#define cAf6_control_pen4_Mac_DA_configure_MSB_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : MII Channels Configure Mac DA LSB.
Reg Addr   : 0x04 - 0x34
Reg Formula: 0x04 + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_pen5_Base                                                                         0x04
#define cAf6Reg_control_pen5_WidthVal                                                                       32

/*--------------------------------------
BitField Name: Mac_DA_configure_LSB
BitField Type: R/W
BitField Desc: Mac DA LSB configure value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_control_pen5_Mac_DA_configure_LSB_Mask                                                   cBit15_0
#define cAf6_control_pen5_Mac_DA_configure_LSB_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : MII Channels Configure Mac SA MSB.
Reg Addr   : 0x05 - 0x35
Reg Formula: 0x05 + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_pen6_Base                                                                         0x05

/*--------------------------------------
BitField Name: Mac_SA_configure_MSB
BitField Type: R/W
BitField Desc: Mac SA MSB configure value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_control_pen6_Mac_SA_configure_MSB_Mask                                                   cBit31_0
#define cAf6_control_pen6_Mac_SA_configure_MSB_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : MII Channels Configure Mac SA LSB.
Reg Addr   : 0x06 - 0x36
Reg Formula: 0x06 + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_pen7_Base                                                                         0x06
#define cAf6Reg_control_pen7_WidthVal                                                                       32

/*--------------------------------------
BitField Name: Mac_SA_configure_LSB
BitField Type: R/W
BitField Desc: Mac SA LSB configure value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_control_pen7_Mac_SA_configure_LSB_Mask                                                   cBit15_0
#define cAf6_control_pen7_Mac_SA_configure_LSB_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : MII Channels Configure Mac Type.
Reg Addr   : 0x07 - 0x37
Reg Formula: 0x07 + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_control_pen8_Base                                                                         0x07
#define cAf6Reg_control_pen8_WidthVal                                                                       32

/*--------------------------------------
BitField Name: Mac_Type_configure
BitField Type: R/W
BitField Desc: Mac Type configure value
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_control_pen8_Mac_Type_configure_Mask                                                     cBit15_0
#define cAf6_control_pen8_Mac_Type_configure_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : MII Tx counter R2C.
Reg Addr   : 0x08 - 0x38
Reg Formula: 0x08 + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter1_Base                                                                             0x08

/*--------------------------------------
BitField Name: Tx_counter_R2C
BitField Type: R2C
BitField Desc: Tx counter R2C
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter1_Tx_counter_R2C_Mask                                                             cBit31_0
#define cAf6_counter1_Tx_counter_R2C_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : MII Rx counter R2C.
Reg Addr   : 0x09 - 0x39
Reg Formula: 0x09 + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter2_Base                                                                             0x09

/*--------------------------------------
BitField Name: Rx_counter_R2C
BitField Type: R2C
BitField Desc: Rx counter R2C
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter2_Rx_counter_R2C_Mask                                                             cBit31_0
#define cAf6_counter2_Rx_counter_R2C_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : MII FCS error counter R2C.
Reg Addr   : 0x0A - 0x3A
Reg Formula: 0x0A + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter3_Base                                                                             0x0A

/*--------------------------------------
BitField Name: FCS_error_counter_R2C
BitField Type: R2C
BitField Desc: FCS error counter R2C
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter3_FCS_error_counter_R2C_Mask                                                      cBit31_0
#define cAf6_counter3_FCS_error_counter_R2C_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : MII PRBS error counter R2C.
Reg Addr   : 0x401 - 0x431
Reg Formula: 0x401 + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter8_Base                                                                            0x401

/*--------------------------------------
BitField Name: PRBS_error_Counter_R2C
BitField Type: R2C
BitField Desc: PRBS error counter R2C
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter8_PRBS_error_Counter_R2C_Mask                                                     cBit31_0
#define cAf6_counter8_PRBS_error_Counter_R2C_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : MII Tx counter RO.
Reg Addr   : 0x0B - 0x3B
Reg Formula: 0x0B + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter4_Base                                                                             0x0B

/*--------------------------------------
BitField Name: Tx_counter_RO
BitField Type: RO
BitField Desc: Tx counter RO
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter4_Tx_counter_RO_Mask                                                              cBit31_0
#define cAf6_counter4_Tx_counter_RO_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MII Rx counter RO.
Reg Addr   : 0x0C - 0x3C
Reg Formula: 0x0C + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter5_Base                                                                             0x0C

/*--------------------------------------
BitField Name: Rx_counter_RO
BitField Type: RO
BitField Desc: Rx counter RO
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter5_Rx_counter_RO_Mask                                                              cBit31_0
#define cAf6_counter5_Rx_counter_RO_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : MII FCS error counter RO.
Reg Addr   : 0x0D - 0x3D
Reg Formula: 0x0D + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter6_Base                                                                             0x0D

/*--------------------------------------
BitField Name: FCS_error_counter_RO
BitField Type: RO
BitField Desc: FCS error counter RO
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter6_FCS_error_counter_RO_Mask                                                       cBit31_0
#define cAf6_counter6_FCS_error_counter_RO_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : MII PRBS error counter RO.
Reg Addr   : 0x400 - 0x430
Reg Formula: 0x400 + channel_id*16
    Where  : 
           + $channel_id(0-3)
Reg Desc   : 
Register to test MII interface

------------------------------------------------------------------------------*/
#define cAf6Reg_counter7_Base                                                                            0x400

/*--------------------------------------
BitField Name: PRBS_error_Counter_RO
BitField Type: RO
BitField Desc: PRBS error counter RO
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_counter7_PRBS_error_Counter_RO_Mask                                                      cBit31_0
#define cAf6_counter7_PRBS_error_Counter_RO_Shift                                                            0

#endif /* _AF6_REG_AF6CNC0021_GMII_RD_DIAG_H_ */

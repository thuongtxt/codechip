/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_RD_EPA_FLOWCTRL_H_
#define _AF6_REG_AF6CNC0021_RD_EPA_FLOWCTRL_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : CONFIG THRESHOLD
Reg Addr   : 0x00-0x0F
Reg Formula: 
    Where  : 
Reg Desc   : 
config control Stuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgthreshold_Base                                                                    0x200
#define cAf6Reg_upen_cfgthreshold_WidthVal                                                                  32

/*--------------------------------------
BitField Name: val_thres2
BitField Type: R/W
BitField Desc: threshold for 3/4 of max buffer contain (256*3/4)kbytes
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_cfgthreshold_val_thres2_Mask                                                        cBit15_8
#define cAf6_upen_cfgthreshold_val_thres2_Shift                                                              8

/*--------------------------------------
BitField Name: val_thes1
BitField Type: R/W
BitField Desc: threshold for 1/4 of max buffer contain (256/4)kbytes
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_cfgthreshold_val_thres1_Mask                                                          cBit7_0
#define cAf6_upen_cfgthreshold_val_thres1_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG ENABLE FLOW CONTROL
Reg Addr   : 0x10
Reg Formula: 
    Where  : 
Reg Desc   : 
config control Stuff global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_enbflwctrl_Base                                                                      0x210

/*--------------------------------------
BitField Name: out_enbflw
BitField Type: R/W
BitField Desc: (0) is disable, (1) is enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_enbflwctrl_out_enbflw_Mask                                                             cBit0
#define cAf6_upen_enbflwctrl_out_enbflw_Shift                                                                0

#endif /* _AF6_REG_AF6CNC0021_RD_EPA_FLOWCTRL_H_ */

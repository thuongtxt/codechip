/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290021SerdesEthPortInternal.h
 * 
 * Created Date: Jul 22, 2016
 *
 * Description : Common implementation for SERDES ETH Port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021SERDESETHPORTINTERNAL_H_
#define _THA60290021SERDESETHPORTINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/eth/ThaEthPortInternal.h"
#include "Tha60290021ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cFaceplatePortInvalidTpId			  cBit15_0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021SerdesEthPortSimulationDatabase
    {
    tAtChannelSimulationDb super;

    uint8 txIpg;
    uint8 rxIpg;
    uint8 loopbackMode;
    }tTha60290021SerdesEthPortSimulationDatabase;

typedef struct tTha60290021SerdesEthPort
    {
    tAtEthPort super;

    /* Private database */
    void *simDb;
    }tTha60290021SerdesEthPort;

typedef struct tTha60290021SgmiiSerdesEthPort * Tha60290021SgmiiSerdesEthPort;

typedef struct tTha60290021SgmiiSerdesEthPortMethods
    {
    eBool (*UseNewMac)(Tha60290021SgmiiSerdesEthPort self);
    uint32 (*Af6_dcckgmiicfg_DcckGmiiLoopIn_Mask_Get)(Tha60290021SgmiiSerdesEthPort self);
    uint32 (*Af6_dcckgmiicfg_DcckGmiiLoopIn_Shift_Get)(Tha60290021SgmiiSerdesEthPort self);
    uint32 (*Af6_dcckgmiicfg_DcckGmiiLoopOut_Mask_Get)(Tha60290021SgmiiSerdesEthPort self);
    uint32 (*Af6_dcckgmiicfg_DcckGmiiLoopOut_Shift_Get)(Tha60290021SgmiiSerdesEthPort self);
    uint32 (*Af6_dcckgmiicfg_DcckGmiiPreNum_Mask_Get)(Tha60290021SgmiiSerdesEthPort self);
    uint32 (*Af6_dcckgmiicfg_DcckGmiiPreNum_Shift_Get)(Tha60290021SgmiiSerdesEthPort self);
    uint32 (*Af6_dcckgmiicfg_DcckGmiiIpgNum_Mask_Get)(Tha60290021SgmiiSerdesEthPort self);
    uint32 (*Af6_dcckgmiicfg_DcckGmiiIpgNum_Shift_Get)(Tha60290021SgmiiSerdesEthPort self);
    }tTha60290021SgmiiSerdesEthPortMethods;

typedef struct tTha60290021SgmiiSerdesEthPort
    {
    tThaEthPort super;
    const tTha60290021SgmiiSerdesEthPortMethods *methods;
    }tTha60290021SgmiiSerdesEthPort;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtEthPort Tha60290021SerdesEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);
AtEthPort Tha60290021SgmiiSerdesEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);
AtEthPort Tha60290021SgmiiSohEthPortObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021SERDESETHPORTINTERNAL_H_ */


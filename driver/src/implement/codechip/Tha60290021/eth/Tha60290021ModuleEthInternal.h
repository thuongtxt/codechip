/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290021ModuleEth.h
 * 
 * Created Date: Jul 21, 2016
 *
 * Description : ETH Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEETHINTERNAL_H_
#define _THA60290021MODULEETHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60290021ModuleEth.h"
#include "../../../default/util/ThaBitMask.h"
#include "../../Tha60210051/eth/Tha60210051ModuleEthInternal.h"
#include "Tha60290021EthPortCounters.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cNumSubPortTpids  2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModuleEthMethods
    {
    eBool (*UsePdhSgmiiPrbs)(Tha60290021ModuleEth self);
    AtEthPort (*FaceplatePortCreate)(Tha60290021ModuleEth self, uint8 portId);
    AtEthPort (*PwPortCreate)(Tha60290021ModuleEth self, uint8 portId);
    AtEthPort (*SerdesBackplaneEthPortCreate)(Tha60290021ModuleEth self, uint8 portId);
    AtEthPort (*KByteDccPortCreate)(Tha60290021ModuleEth self, uint8 portId);
    uint8 (*NumberOfFaceplateGroupsGet)(Tha60290021ModuleEth self);/* 8 port faceplate group */
    eBool (*HasSgmiiDiagnosticLogic)(Tha60290021ModuleEth self);
    AtEthFlowControl (*FaceplatePortFlowControlCreate)(Tha60290021ModuleEth self, AtEthPort port);
    uint8 (*NumberOf10GFaceplatePorts)(Tha60290021ModuleEth self);

    /*Resources*/
    eBool (*IsBackplaneMac)(Tha60290021ModuleEth self, uint8 portId);
    eBool (*IsFaceplatePort)(Tha60290021ModuleEth self, uint8 portId);
    eBool (*IsSgmiiPort)(Tha60290021ModuleEth self, uint8 portId);
    uint32 (*MultirateBaseAddress)(Tha60290021ModuleEth self, uint8 sixteenPortGroup);
    uint8 (*NumSixteenPortGroup)(Tha60290021ModuleEth self);
    uint32 (*TenGeBaseAddress)(Tha60290021ModuleEth self, uint32 eightPortGroupId);
    uint32 (*MDIO1GBaseAddress)(Tha60290021ModuleEth self);
    uint32 (*Af6Reg_upen_enbflwctrl_Base)(Tha60290021ModuleEth self);

    /* Counters */
    Tha60290021EthPortCounters (*FaceplatePortCountersCreate)(Tha60290021ModuleEth self, AtEthPort port);
    }tTha60290021ModuleEthMethods;

typedef struct tTha60290021ModuleEthCache
    {
    uint16 subportVlanTxTpids[cNumSubPortTpids];
    uint16 subportVlanExpectedTpids[cNumSubPortTpids];
    }tTha60290021ModuleEthCache;

typedef struct tTha60290021ModuleEth
    {
    tTha60210051ModuleEth super;
    const tTha60290021ModuleEthMethods *methods;

    /* Private data */
    ThaBitMask expectedVlanIdMask;
    AtMdio sgmiiMdio;
    AtLongRegisterAccess faceplateCounterLongAccess;

    /* For warm restore */
    void *cache;
    }tTha60290021ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60290021ModuleEthObjectInit(AtModuleEth self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEETHINTERNAL_H_ */


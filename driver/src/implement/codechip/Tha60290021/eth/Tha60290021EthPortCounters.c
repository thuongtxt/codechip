/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60290021EthPortCounters.c
 *
 * Created Date: Mar 14, 2019
 *
 * Description : 60290021 Ethernet port counters
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/util/AtUtil.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "Tha60290021FaceplateSerdesEthPortReg.h"
#include "Tha60290021EthPortCountersInternal.h"
#include "Tha60290021ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaEthLongRegMaxSize 2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)         ((Tha60290021EthPortCounters)self)

#define mSaveCounter(fieldName, calling, counterType)                          \
    if (AtChannelCounterIsSupported(port, counterType))                        \
        {                                                                      \
        values = calling;                                                      \
        if (counters)                                                          \
            {                                                                  \
            if (accumulate) counters->fieldName += values;                     \
            else            counters->fieldName  = values;                     \
            }                                                                  \
        }                                                                      \

#define mLoadCounter(fieldName, counterType)                                   \
    case counterType:                                                          \
        if (AtChannelCounterIsSupported(port, counterType))                    \
            {                                                                  \
            values = counters->fieldName;                                      \
            if (clear)                                                         \
                counters->fieldName = 0;                                       \
            return values;                                                     \
            }                                                                  \
        else                                                                   \
            return 0;                                                          \

#define mGetCounter(fieldName, counterType)                                    \
    if (AtChannelCounterIsSupported(port, counterType))                        \
        {                                                                      \
        counters->fieldName = cache->fieldName;                                \
        if (clear)                                                             \
            cache->fieldName = 0;                                              \
        }

#define mCounterParams(direction, group, offset)           \
    cAf6Reg_upen_##direction##epagrp##group##cnt_Base, offset

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021EthPortCountersMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;

/* Save super implementation */
static const tAtObjectMethods   *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 UpperCounterGet(Tha60290021EthPortCounters self, uint32 numBits, uint32 *longRegVal)
    {
    AtUnused(self);
    return AtUtilLongRegisterFieldGet(longRegVal, cThaEthLongRegMaxSize, numBits, numBits * 2U - 1U);
    }

static uint32 LowerCounterGet(Tha60290021EthPortCounters self, uint32 numBits, uint32 *longRegVal)
    {
    AtUnused(self);
    return AtUtilLongRegisterFieldGet(longRegVal, cThaEthLongRegMaxSize, 0, numBits - 1U);
    }

static uint32 UpperCounter24Get(Tha60290021EthPortCounters self, uint32 *longRegVal)
    {
    return mMethodsGet(self)->UpperCounterGet(self, 24, longRegVal);
    }

static uint32 LowerCounter24Get(Tha60290021EthPortCounters self, uint32 *longRegVal)
    {
    return mMethodsGet(self)->LowerCounterGet(self, 24, longRegVal);
    }

static uint32 UpperCounter32Get(Tha60290021EthPortCounters self, uint32 *longRegVal)
    {
    return mMethodsGet(self)->UpperCounterGet(self, 32, longRegVal);
    }

static uint32 LowerCounter32Get(Tha60290021EthPortCounters self, uint32 *longRegVal)
    {
    return mMethodsGet(self)->LowerCounterGet(self, 32, longRegVal);
    }

static uint32 CounterOffsetFactor(Tha60290021EthPortCounters self)
    {
    AtUnused(self);
    return 16;
    }

static uint32 CounterAddress(Tha60290021EthPortCounters self,
                             AtChannel port, uint32 localAddress, uint32 offset)
    {
    uint32 counterOffset = offset * mMethodsGet(self)->CounterOffsetFactor(self);
    uint32 regAddr = localAddress + AtChannelHwIdGet(port) + counterOffset;
    return Tha60290021ModuleEthBypassBaseAddress() + regAddr;
    }

static void HwCounterRead2Clear(AtChannel port, uint32 address, uint32 *buffers, eBool clear)
    {
    static uint32 zeroValues[cThaEthLongRegMaxSize] = {0};

    mChannelHwLongRead(port, address, buffers, cThaEthLongRegMaxSize, cAtModuleEth);
    if (clear)
        mChannelHwLongWrite(port, address, zeroValues, cThaEthLongRegMaxSize, cAtModuleEth);
    }

static void HwCounterRead2ClearWithOffset(Tha60290021EthPortCounters self,
                                          uint32 localAddress, uint8 cntIndex,
                                          uint32 *buffers, eBool clear)
    {
    AtChannel port = (AtChannel)self->port;
    HwCounterRead2Clear(port, CounterAddress(self, port, localAddress, cntIndex), buffers, clear);
    }

static uint32 CountersCacheSize(Tha60290021EthPortCounters self)
    {
    AtUnused(self);
    return (uint32)sizeof(tTha60290021FaceplateSerdesEthCounters);
    }

static tTha60290021FaceplateSerdesEthCounters *CountersCacheGet(Tha60290021EthPortCounters self)
    {
    if (self->counters == NULL)
        self->counters = AtOsalMemAlloc(mMethodsGet(self)->CountersCacheSize(self));
    return self->counters;
    }

static eBool ShouldAccumulate(Tha60290021EthPortCounters self)
    {
    AtUnused(self);
    /* Due to hardware R2C counters, we have to accumulate the counters to provide
     * read-only mode */
    return cAtTrue;
    }

static eBool ShouldClearHwCounters(Tha60290021EthPortCounters self, eBool accumulated, eBool read2Clear)
    {
    AtUnused(self);
    if (accumulated) /* If accumulate, do not clear hardware */
        return cAtFalse;

    return read2Clear;
    }

static eAtRet AllCountersLatchAndClear(Tha60290021EthPortCounters self, eBool clear)
    {
    tTha60290021FaceplateSerdesEthCounters *counters = CountersCacheGet(self);
    AtChannel port = (AtChannel)self->port;
    uint32 buffers[cThaEthLongRegMaxSize];
    uint32 values;
    eBool accumulate = mMethodsGet(self)->ShouldAccumulate(self);
    eBool read2Clear = ShouldClearHwCounters(self, accumulate, clear);

    /* TX */
    HwCounterRead2ClearWithOffset(self, mCounterParams(tx, 1, 0), buffers, read2Clear);
    mSaveCounter(txPacketsLen65_127,    LowerCounter24Get(self, buffers), cAtEthPortCounterTxPacketsLen65_127);
    mSaveCounter(txPacketsLen128_255,   UpperCounter24Get(self, buffers), cAtEthPortCounterTxPacketsLen128_255);

    HwCounterRead2ClearWithOffset(self, mCounterParams(tx, 1, 1), buffers, read2Clear);
    mSaveCounter(txPacketsLen256_511,   LowerCounter24Get(self, buffers), cAtEthPortCounterTxPacketsLen256_511);
    mSaveCounter(txPacketsLen512_1023,  UpperCounter24Get(self, buffers), cAtEthPortCounterTxPacketsLen512_1023);

    HwCounterRead2ClearWithOffset(self, mCounterParams(tx, 1, 2), buffers, read2Clear);
    mSaveCounter(txPacketsLen1024_1518, LowerCounter24Get(self, buffers), cAtEthPortCounterTxPacketsLen1024_1518);
    mSaveCounter(txPacketsJumbo,        UpperCounter24Get(self, buffers), cAtEthPortCounterTxPacketsJumbo);

    HwCounterRead2ClearWithOffset(self, mCounterParams(tx, 2, 0), buffers, read2Clear);
    mSaveCounter(txBytes,               LowerCounter32Get(self, buffers), cAtEthPortCounterTxBytes);
    mSaveCounter(txPackets,             UpperCounter32Get(self, buffers), cAtEthPortCounterTxPackets);

    HwCounterRead2ClearWithOffset(self, mCounterParams(tx, 2, 1), buffers, read2Clear);
    mSaveCounter(txDiscardedPackets,    LowerCounter32Get(self, buffers), cAtEthPortCounterTxDiscardedPackets);

    HwCounterRead2ClearWithOffset(self, mCounterParams(tx, 2, 2), buffers, read2Clear);
    mSaveCounter(txUndersizePackets,    LowerCounter32Get(self, buffers), cAtEthPortCounterTxUndersizePackets);
    mSaveCounter(txOversizePackets,     UpperCounter32Get(self, buffers), cAtEthPortCounterTxOversizePackets);

    HwCounterRead2ClearWithOffset(self, mCounterParams(tx, 3, 0), buffers, read2Clear);
    mSaveCounter(txPausePackets,            LowerCounter24Get(self, buffers), cAtEthPortCounterTxPausePackets);
    mSaveCounter(txOverFlowDroppedPackets,  UpperCounter24Get(self, buffers), cAtEthPortCounterTxOverFlowDroppedPackets);

    HwCounterRead2ClearWithOffset(self, mCounterParams(tx, 3, 1), buffers, read2Clear);
    mSaveCounter(txBrdCastPackets,      LowerCounter24Get(self, buffers), cAtEthPortCounterTxBrdCastPackets);
    mSaveCounter(txMultCastPackets,     UpperCounter24Get(self, buffers), cAtEthPortCounterTxMultCastPackets);

    HwCounterRead2ClearWithOffset(self, mCounterParams(tx, 3, 2), buffers, read2Clear);
    mSaveCounter(txPacketsLen64,        LowerCounter24Get(self, buffers), cAtEthPortCounterTxPacketsLen64);

    /* RX */
    HwCounterRead2ClearWithOffset(self, mCounterParams(rx, 1, 0), buffers, read2Clear);
    mSaveCounter(rxPacketsLen65_127,    LowerCounter24Get(self, buffers), cAtEthPortCounterRxPacketsLen65_127);
    mSaveCounter(rxPacketsLen128_255,   UpperCounter24Get(self, buffers), cAtEthPortCounterRxPacketsLen128_255);

    HwCounterRead2ClearWithOffset(self, mCounterParams(rx, 1, 1), buffers, read2Clear);
    mSaveCounter(rxPacketsLen256_511,   LowerCounter24Get(self, buffers), cAtEthPortCounterRxPacketsLen256_511);
    mSaveCounter(rxPacketsLen512_1023,  UpperCounter24Get(self, buffers), cAtEthPortCounterRxPacketsLen512_1023);

    HwCounterRead2ClearWithOffset(self, mCounterParams(rx, 1, 2), buffers, read2Clear);
    mSaveCounter(rxPacketsLen1024_1518, LowerCounter24Get(self, buffers), cAtEthPortCounterRxPacketsLen1024_1518);
    mSaveCounter(rxPacketsJumbo,        UpperCounter24Get(self, buffers), cAtEthPortCounterRxPacketsJumbo);

    HwCounterRead2ClearWithOffset(self, mCounterParams(rx, 2, 0), buffers, read2Clear);
    mSaveCounter(rxBytes,               LowerCounter32Get(self, buffers), cAtEthPortCounterRxBytes);
    mSaveCounter(rxPackets,             UpperCounter32Get(self, buffers), cAtEthPortCounterRxPackets);

    HwCounterRead2ClearWithOffset(self, mCounterParams(rx, 2, 1), buffers, read2Clear);
    mSaveCounter(rxErrFcsPackets,       LowerCounter32Get(self, buffers), cAtEthPortCounterRxErrFcsPackets);

    HwCounterRead2ClearWithOffset(self, mCounterParams(rx, 2, 2), buffers, read2Clear);
    mSaveCounter(rxDiscardedPackets,    LowerCounter32Get(self, buffers), cAtEthPortCounterRxDiscardedPackets);
    mSaveCounter(rxLoopDaPackets,       UpperCounter32Get(self, buffers), cAtEthPortCounterRxLoopDaPackets);

    HwCounterRead2ClearWithOffset(self, mCounterParams(rx, 3, 0), buffers, read2Clear);
    mSaveCounter(rxPcsInvalidCount,     LowerCounter24Get(self, buffers), cAtEthPortCounterRxPcsInvalidCount);
    mSaveCounter(rxJabberPackets,       UpperCounter24Get(self, buffers), cAtEthPortCounterRxJabberPackets);

    HwCounterRead2ClearWithOffset(self, mCounterParams(rx, 3, 1), buffers, read2Clear);
    mSaveCounter(rxFragmentPackets,     LowerCounter24Get(self, buffers), cAtEthPortCounterRxFragmentPackets);
    mSaveCounter(rxPausePackets,        UpperCounter24Get(self, buffers), cAtEthPortCounterRxPausePackets);

    HwCounterRead2ClearWithOffset(self, mCounterParams(rx, 3, 2), buffers, read2Clear);
    mSaveCounter(rxUndersizePackets,    LowerCounter24Get(self, buffers), cAtEthPortCounterRxUndersizePackets);
    mSaveCounter(rxOversizePackets,     UpperCounter24Get(self, buffers), cAtEthPortCounterRxOversizePackets);

    HwCounterRead2ClearWithOffset(self, mCounterParams(rx, 4, 0), buffers, read2Clear);
    mSaveCounter(rxOverFlowDroppedPackets,  LowerCounter24Get(self, buffers), cAtEthPortCounterRxOverFlowDroppedPackets);
    mSaveCounter(rxBrdCastPackets,          UpperCounter24Get(self, buffers), cAtEthPortCounterRxBrdCastPackets);

    HwCounterRead2ClearWithOffset(self, mCounterParams(rx, 4, 1), buffers, read2Clear);
    mSaveCounter(rxMultCastPackets,     LowerCounter24Get(self, buffers), cAtEthPortCounterRxMultCastPackets);
    mSaveCounter(rxPacketsLen64,        UpperCounter24Get(self, buffers), cAtEthPortCounterRxPacketsLen64);

    return cAtOk;
    }

static eAtRet AllCountersRead2Clear(Tha60290021EthPortCounters self, tAtEthPortCounters *counters, eBool clear)
    {
    tTha60290021FaceplateSerdesEthCounters *cache = CountersCacheGet(self);
    AtChannel port = (AtChannel)self->port;

    mGetCounter(txPacketsLen65_127,    cAtEthPortCounterTxPacketsLen65_127)
    mGetCounter(txPacketsLen128_255,   cAtEthPortCounterTxPacketsLen128_255)
    mGetCounter(txPacketsLen256_511,   cAtEthPortCounterTxPacketsLen256_511)
    mGetCounter(txPacketsLen512_1023,  cAtEthPortCounterTxPacketsLen512_1023)
    mGetCounter(txPacketsLen1024_1518, cAtEthPortCounterTxPacketsLen1024_1518)
    mGetCounter(txPacketsJumbo,        cAtEthPortCounterTxPacketsJumbo)
    mGetCounter(txBytes,               cAtEthPortCounterTxBytes);
    mGetCounter(txPackets,             cAtEthPortCounterTxPackets);
    mGetCounter(txDiscardedPackets,    cAtEthPortCounterTxDiscardedPackets);
    mGetCounter(txPacketsLen64,        cAtEthPortCounterTxPacketsLen64);
    mGetCounter(txUndersizePackets,    cAtEthPortCounterTxUndersizePackets);
    mGetCounter(txOversizePackets,     cAtEthPortCounterTxOversizePackets);
    mGetCounter(txPausePackets,        cAtEthPortCounterTxPausePackets);
    mGetCounter(txOverFlowDroppedPackets,  cAtEthPortCounterTxOverFlowDroppedPackets);
    mGetCounter(txBrdCastPackets,      cAtEthPortCounterTxBrdCastPackets);
    mGetCounter(txMultCastPackets,     cAtEthPortCounterTxMultCastPackets);

    mGetCounter(rxPacketsLen65_127,    cAtEthPortCounterRxPacketsLen65_127);
    mGetCounter(rxPacketsLen128_255,   cAtEthPortCounterRxPacketsLen128_255);
    mGetCounter(rxPacketsLen256_511,   cAtEthPortCounterRxPacketsLen256_511);
    mGetCounter(rxPacketsLen512_1023,  cAtEthPortCounterRxPacketsLen512_1023);
    mGetCounter(rxPacketsLen1024_1518, cAtEthPortCounterRxPacketsLen1024_1518);
    mGetCounter(rxPacketsJumbo,        cAtEthPortCounterRxPacketsJumbo);
    mGetCounter(rxBytes,               cAtEthPortCounterRxBytes);
    mGetCounter(rxPackets,             cAtEthPortCounterRxPackets);
    mGetCounter(rxErrFcsPackets,       cAtEthPortCounterRxErrFcsPackets);
    mGetCounter(rxPacketsLen64,        cAtEthPortCounterRxPacketsLen64);
    mGetCounter(rxDiscardedPackets,    cAtEthPortCounterRxDiscardedPackets);
    mGetCounter(rxLoopDaPackets,       cAtEthPortCounterRxLoopDaPackets);
    mGetCounter(rxPcsInvalidCount,     cAtEthPortCounterRxPcsInvalidCount);
    mGetCounter(rxJabberPackets,       cAtEthPortCounterRxJabberPackets);
    mGetCounter(rxFragmentPackets,     cAtEthPortCounterRxFragmentPackets);
    mGetCounter(rxPausePackets,        cAtEthPortCounterRxPausePackets);
    mGetCounter(rxUndersizePackets,    cAtEthPortCounterRxUndersizePackets);
    mGetCounter(rxOversizePackets,     cAtEthPortCounterRxOversizePackets);
    mGetCounter(rxOverFlowDroppedPackets,  cAtEthPortCounterRxOverFlowDroppedPackets);
    mGetCounter(rxBrdCastPackets,      cAtEthPortCounterRxBrdCastPackets);
    mGetCounter(rxMultCastPackets,     cAtEthPortCounterRxMultCastPackets);

    return cAtOk;
    }

static uint32 CounterRead2Clear(Tha60290021EthPortCounters self, uint16 counterType, eBool clear)
    {
    tTha60290021FaceplateSerdesEthCounters *counters = CountersCacheGet(self);
    AtChannel port = (AtChannel)self->port;
    uint32 values = 0;

    switch (counterType)
        {
        mLoadCounter(txPacketsLen65_127,    cAtEthPortCounterTxPacketsLen65_127)
        mLoadCounter(txPacketsLen128_255,   cAtEthPortCounterTxPacketsLen128_255)
        mLoadCounter(txPacketsLen256_511,   cAtEthPortCounterTxPacketsLen256_511)
        mLoadCounter(txPacketsLen512_1023,  cAtEthPortCounterTxPacketsLen512_1023)
        mLoadCounter(txPacketsLen1024_1518, cAtEthPortCounterTxPacketsLen1024_1518)
        mLoadCounter(txPacketsJumbo,        cAtEthPortCounterTxPacketsJumbo)
        mLoadCounter(txBytes,               cAtEthPortCounterTxBytes);
        mLoadCounter(txPackets,             cAtEthPortCounterTxPackets);
        mLoadCounter(txDiscardedPackets,    cAtEthPortCounterTxDiscardedPackets);
        mLoadCounter(txPacketsLen64,        cAtEthPortCounterTxPacketsLen64);
        mLoadCounter(txUndersizePackets,    cAtEthPortCounterTxUndersizePackets);
        mLoadCounter(txOversizePackets,     cAtEthPortCounterTxOversizePackets);
        mLoadCounter(txPausePackets,        cAtEthPortCounterTxPausePackets);
        mLoadCounter(txOverFlowDroppedPackets,  cAtEthPortCounterTxOverFlowDroppedPackets);
        mLoadCounter(txBrdCastPackets,      cAtEthPortCounterTxBrdCastPackets);
        mLoadCounter(txMultCastPackets,     cAtEthPortCounterTxMultCastPackets);

        mLoadCounter(rxPacketsLen65_127,    cAtEthPortCounterRxPacketsLen65_127);
        mLoadCounter(rxPacketsLen128_255,   cAtEthPortCounterRxPacketsLen128_255);
        mLoadCounter(rxPacketsLen256_511,   cAtEthPortCounterRxPacketsLen256_511);
        mLoadCounter(rxPacketsLen512_1023,  cAtEthPortCounterRxPacketsLen512_1023);
        mLoadCounter(rxPacketsLen1024_1518, cAtEthPortCounterRxPacketsLen1024_1518);
        mLoadCounter(rxPacketsJumbo,        cAtEthPortCounterRxPacketsJumbo);
        mLoadCounter(rxBytes,               cAtEthPortCounterRxBytes);
        mLoadCounter(rxPackets,             cAtEthPortCounterRxPackets);
        mLoadCounter(rxErrFcsPackets,       cAtEthPortCounterRxErrFcsPackets);
        mLoadCounter(rxPacketsLen64,        cAtEthPortCounterRxPacketsLen64);
        mLoadCounter(rxDiscardedPackets,    cAtEthPortCounterRxDiscardedPackets);
        mLoadCounter(rxLoopDaPackets,       cAtEthPortCounterRxLoopDaPackets);
        mLoadCounter(rxPcsInvalidCount,     cAtEthPortCounterRxPcsInvalidCount);
        mLoadCounter(rxJabberPackets,       cAtEthPortCounterRxJabberPackets);
        mLoadCounter(rxFragmentPackets,     cAtEthPortCounterRxFragmentPackets);
        mLoadCounter(rxPausePackets,        cAtEthPortCounterRxPausePackets);
        mLoadCounter(rxUndersizePackets,    cAtEthPortCounterRxUndersizePackets);
        mLoadCounter(rxOversizePackets,     cAtEthPortCounterRxOversizePackets);
        mLoadCounter(rxOverFlowDroppedPackets,  cAtEthPortCounterRxOverFlowDroppedPackets);
        mLoadCounter(rxBrdCastPackets,      cAtEthPortCounterRxBrdCastPackets);
        mLoadCounter(rxMultCastPackets,     cAtEthPortCounterRxMultCastPackets);
        default:
            return 0;
        }
    }

static void Delete(AtObject self)
    {
    AtOsalMemFree(mThis(self)->counters);
    mThis(self)->counters = NULL;

    /* Super */
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290021EthPortCounters object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(port);
    mEncodeNone(counters);
    }

static void OverrideAtObject(Tha60290021EthPortCounters self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(Tha60290021EthPortCounters self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(Tha60290021EthPortCounters self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CountersCacheSize);
        mMethodOverride(m_methods, ShouldAccumulate);
        mMethodOverride(m_methods, UpperCounterGet);
        mMethodOverride(m_methods, LowerCounterGet);
        mMethodOverride(m_methods, CounterOffsetFactor);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021EthPortCounters);
    }

Tha60290021EthPortCounters Tha60290021EthPortCountersObjectInit(Tha60290021EthPortCounters self, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    self->port = port;

    return self;
    }

Tha60290021EthPortCounters Tha60290021EthPortCountersNew(AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha60290021EthPortCounters newCounters = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newCounters == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021EthPortCountersObjectInit(newCounters, port);
    }

eAtRet Tha60290021EthPortAllCountersLatchAndClear(Tha60290021EthPortCounters self, eBool clear)
    {
    if (self)
        return AllCountersLatchAndClear(self, clear);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290021EthPortAllCountersRead2Clear(Tha60290021EthPortCounters self, tAtEthPortCounters *counters, eBool clear)
    {
    if (self && counters)
        return AllCountersRead2Clear(self, counters, clear);
    return cAtErrorNullPointer;
    }

uint32 Tha60290021EthPortCounterRead2Clear(Tha60290021EthPortCounters self, uint16 counterType, eBool clear)
    {
    if (self)
        return CounterRead2Clear(self, counterType, clear);
    return 0;
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60290021EthPortCountersInternal.h
 * 
 * Created Date: Mar 15, 2019
 *
 * Description : 60290021 Ethernet port counters
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021ETHPORTCOUNTERSINTERNAL_H_
#define _THA60290021ETHPORTCOUNTERSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtObject.h"
#include "Tha60290021EthPortCounters.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021EthPortCountersMethods
    {
    uint32 (*CountersCacheSize)(Tha60290021EthPortCounters self);
    eBool (*ShouldAccumulate)(Tha60290021EthPortCounters self); /* Hardware R2C only */
    uint32 (*UpperCounterGet)(Tha60290021EthPortCounters self, uint32 numBits, uint32 *longRegVal);
    uint32 (*LowerCounterGet)(Tha60290021EthPortCounters self, uint32 numBits, uint32 *longRegVal);
    uint32 (*CounterOffsetFactor)(Tha60290021EthPortCounters self);
    }tTha60290021EthPortCountersMethods;

typedef struct tTha60290021EthPortCounters
    {
    tAtObject super;
    const tTha60290021EthPortCountersMethods *methods;

    /* Private data */
    AtEthPort port;
    void *counters;   /* Counters cache */
    }tTha60290021EthPortCounters;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha60290021EthPortCounters Tha60290021EthPortCountersObjectInit(Tha60290021EthPortCounters self, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021ETHPORTCOUNTERSINTERNAL_H_ */


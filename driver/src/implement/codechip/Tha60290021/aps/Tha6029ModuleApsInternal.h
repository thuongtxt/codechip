/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : Tha6029ModuleApsInternal.h
 * 
 * Created Date: Jul 11, 2016
 *
 * Description : Internal APS Moulde of device Tha6029
 * 
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA6029MODULEAPSINTERNAL_H_
#define _THA6029MODULEAPSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/aps/ThaModuleHardApsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029ModuleAps
    {
    tThaModuleHardAps super;
    }tTha6029ModuleAps;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleAps Tha6029ModuleApsObjectInit(AtModuleAps self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029MODULEAPSINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : Tha60290021ModuleAps.c
 *
 * Created Date: Jul 12, 2016
 *
 * Description : APS moudule of product 60290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "Tha6029ModuleApsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleHardApsMethods m_ThaModuleHardApsOverride;
static tAtModuleApsMethods      m_AtModuleApsOverride;

/* Save superclass implementation */
static const tThaModuleHardApsMethods *m_ThaModuleHardApsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool LinearIsSupported(ThaModuleHardAps self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool UpsrIsSupported(ThaModuleHardAps self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 MaxNumUpsrEngines(AtModuleAps self)
    {
    AtUnused(self);
    return 384;
    }

static uint32 MaxNumSelectors(AtModuleAps self)
    {
    AtUnused(self);
    return 1152; /* 384 * 3 */
    }

static uint32 MaxNumGroups(AtModuleAps self)
    {
    AtUnused(self);
    return 16;
    }

static uint32 MaxNumStsGroups(AtModuleAps self)
    {
    AtUnused(self);
    return 16;
    }

static uint32 BaseAddress(ThaModuleHardAps self)
    {
    AtUnused(self);
    return 0xB0000UL;
    }

static uint32 UpsrPathDefectMaskOffset(ThaModuleHardAps self, AtSdhPath vc)
    {
    ThaModuleOcn ocnModule = ThaModuleOcnFromChannel((AtChannel)vc);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)vc);
    uint8 lineId = AtSdhChannelLineGet((AtSdhChannel)vc);
    eTha6029LineSide side = Tha60290021ModuleOcnPohProcessorSideGet(ocnModule);
    const uint32 mateSliceApsOffset = 8 * 64UL;

    if ((side == cTha6029LineSideFaceplate) && Tha60290021ModuleSdhLineIsFaceplate(sdhModule, lineId))
        return m_ThaModuleHardApsMethods->UpsrPathDefectMaskOffset(self, vc);

    if ((side == cTha6029LineSideMate) && Tha60290021ModuleSdhLineIsMate(sdhModule, lineId))
        return m_ThaModuleHardApsMethods->UpsrPathDefectMaskOffset(self, vc) - mateSliceApsOffset;

    return cBit31_0;
    }

static ThaVersionReader VersionReader(ThaModuleHardAps self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaDeviceVersionReader(device);
    }

static eBool InterruptIsSupported(ThaModuleHardAps self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x1, 0x0);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static AtModuleSdh SdhModule(ThaModuleHardAps self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static uint32 NumLines(ThaModuleHardAps self)
    {
    return Tha60290021ModuleSdhNumUseableFaceplateLines(SdhModule(self));
    }

static void OverrideThaModuleHardAps(AtModuleAps self)
    {
    ThaModuleHardAps module = (ThaModuleHardAps)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleHardApsMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardApsOverride, mMethodsGet(module), sizeof(m_ThaModuleHardApsOverride));

        mMethodOverride(m_ThaModuleHardApsOverride, LinearIsSupported);
        mMethodOverride(m_ThaModuleHardApsOverride, UpsrIsSupported);
        mMethodOverride(m_ThaModuleHardApsOverride, BaseAddress);
        mMethodOverride(m_ThaModuleHardApsOverride, UpsrPathDefectMaskOffset);
        mMethodOverride(m_ThaModuleHardApsOverride, InterruptIsSupported);
        mMethodOverride(m_ThaModuleHardApsOverride, NumLines);
        }

    mMethodsSet(module, &m_ThaModuleHardApsOverride);
    }

static void OverrideAtModuleAps(AtModuleAps self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleApsOverride, mMethodsGet(self), sizeof(m_AtModuleApsOverride));

        mMethodOverride(m_AtModuleApsOverride, MaxNumUpsrEngines);
        mMethodOverride(m_AtModuleApsOverride, MaxNumSelectors);
        mMethodOverride(m_AtModuleApsOverride, MaxNumGroups);
        mMethodOverride(m_AtModuleApsOverride, MaxNumStsGroups);
        }

    mMethodsSet(self, &m_AtModuleApsOverride);
    }

static void Override(AtModuleAps self)
    {
    OverrideThaModuleHardAps(self);
    OverrideAtModuleAps(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029ModuleAps);
    }

AtModuleAps Tha6029ModuleApsObjectInit(AtModuleAps self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleHardApsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup Class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleAps Tha6029ModuleApsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleAps object = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (object == NULL)
        return NULL;

    /* Construct it */
    return Tha6029ModuleApsObjectInit(object, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PMC
 *
 * File        : Tha60290021ModulePmc.c
 *
 * Created Date: Dec 10, 2016
 *
 * Description : PMC module concrete class for 60290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/pmc/ThaModulePmcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021ModulePmc
    {
    tThaModulePmc super;
    }tTha60290021ModulePmc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePmcMethods m_ThaModulePmcOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaVersionReader VersionReader(ThaModulePmc self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaDeviceVersionReader(device);
    }

static eBool VcTu1xCounterGroupDivided(ThaModulePmc self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x00);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool LineSupportedBlockCounters(ThaModulePmc self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x5, 0x00);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static void OverrideThaModulePmc(AtModule self)
	{
	ThaModulePmc module = (ThaModulePmc)self;

	if (!m_methodsInit)
		{
		AtOsal osal = AtSharedDriverOsalGet();
		mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePmcOverride, mMethodsGet(module), sizeof(m_ThaModulePmcOverride));

        mMethodOverride(m_ThaModulePmcOverride, VcTu1xCounterGroupDivided);
        mMethodOverride(m_ThaModulePmcOverride, LineSupportedBlockCounters);
		}

	mMethodsSet(module, &m_ThaModulePmcOverride);
	}

static void Override(AtModule self)
	{
	OverrideThaModulePmc(self);
	}

static uint32 ObjectSize(void)
	{
	return sizeof(tTha60290021ModulePmc);
	}

static AtModule ObjectInit(AtModule self, AtDevice device)
	{
	/* Clear memory */
	AtOsal osal = AtSharedDriverOsalGet();
	mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

	/* Super constructor */
	if (ThaModulePmcObjectInit(self, device) == NULL)
		return NULL;
    
    /* Setup class */
	Override(self);
	m_methodsInit = 1;

	return self;
	}

AtModule Tha60290021ModulePmcNew(AtDevice self)
	{
	/* Allocate memory */
	AtOsal osal = AtSharedDriverOsalGet();
	AtModule module = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
	if (module == NULL)
		return NULL;

	/* Construct it */
	return ObjectInit(module, self);
	}

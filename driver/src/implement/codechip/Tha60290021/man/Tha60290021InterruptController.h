/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : Interrupt Controller
 *
 * File        : Tha6A290021InterruptControllerInternal.h
 *
 * Created Date: Jul 27, 2015
 *
 * Description : Interrupt Controller representation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021INTERRUPTCONTROLLER_H_
#define _THA60290021INTERRUPTCONTROLLER_H_

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290021InterruptControllerInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
ThaIntrController Tha60290021IntrControllerNew(AtIpCore core);
ThaIntrController Tha60290021IntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore);

#ifdef __cplusplus
}
#endif

#endif /* _THA60290021INTERRUPTCONTROLLER_H_ */

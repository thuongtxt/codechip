/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEVICE
 * 
 * File        : Tha60290021Device.h
 * 
 * Created Date: Jul 12, 2016
 *
 * Description : Device Management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021DEVICEINTERNAL_H_
#define _THA60290021DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/man/Tha60210051Device.h"
#include "../../Tha60210051/man/Tha60210051DeviceInternal.h"
#include "Tha60290021Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021DeviceMethods
    {
    eBool (*IsRunningOnOtherPlatform)(Tha60290021Device self);
    eBool (*Refout8kCanDisplay)(Tha60290021Device self);
    eBool (*HasUpsrEthInterrupt)(Tha60290021Device self);
    eBool (*IsRunningOnVu13P)(Tha60290021Device self);
    eAtRet (*DccHdlcPktGenInit)(Tha60290021Device self);
    uint32 (*ExpectedFrequencyOf100FxSerdes)(Tha60290021Device self);
    /* Resources */
    uint32 (*NumFaceplateSerdes)(Tha60290021Device self);
    uint32 (*NumMateSerdes)(Tha60290021Device self);
    uint32 (*NumSgmiiSerdes)(Tha60290021Device self);
    uint32 (*NumBackplaneSerdes)(Tha60290021Device self);
    uint8 (*FaceplateSerdesStartId)(Tha60290021Device self);
    uint8 (*FaceplateSerdesEndId)(Tha60290021Device self);
    uint8 (*MateSerdesStartId)(Tha60290021Device self);
    uint8 (*MateSerdesEndId)(Tha60290021Device self);
    uint8 (*SgmiiSerdesStartId)(Tha60290021Device self);
    uint8 (*SgmiiSerdesEndId)(Tha60290021Device self);
    uint8 (*BackplaneSerdesStartId)(Tha60290021Device self);
    uint8 (*BackplaneSerdesEndId)(Tha60290021Device self);
    }tTha60290021DeviceMethods;

typedef struct tTha60290021Device
    {
    tTha60210051Device super;
    const tTha60290021DeviceMethods *methods;
    }tTha60290021Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60290021DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021DEVICEINTERNAL_H_ */


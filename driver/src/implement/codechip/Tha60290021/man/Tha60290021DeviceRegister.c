/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60290021DeviceRegister.c
 *
 * Created Date: Aug 25, 2016
 *
 * Description : To manage register spaces
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "Tha60290021Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
eBool Tha60290021IsOcnTfi5SideRegister(AtDevice self, uint32 address)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(self, cThaModuleOcn);
    uint32 baseAddress = Tha60210011ModuleOcnBaseAddress(ocnModule);
    uint32 localAddress = address - baseAddress;
    if (mInRange(localAddress, 0x00000, 0x00000) ||
        mInRange(localAddress, 0x00006, 0x00006) ||
        mInRange(localAddress, 0x00007, 0x00007) ||
        mInRange(localAddress, 0x20000, 0x20e00) ||
        mInRange(localAddress, 0x20001, 0x20e01) ||
        mInRange(localAddress, 0x20003, 0x20e03) ||
        mInRange(localAddress, 0x20002, 0x20e02) ||
        mInRange(localAddress, 0x00001, 0x00001) ||
        mInRange(localAddress, 0x02000, 0x02e2f) ||
        mInRange(localAddress, 0x03000, 0x03e2f) ||
        mInRange(localAddress, 0x06000, 0x0672f) ||
        mInRange(localAddress, 0x07000, 0x0772f) ||
        mInRange(localAddress, 0x05000, 0x05e00) ||
        mInRange(localAddress, 0x05001, 0x05e01) ||
        mInRange(localAddress, 0x05002, 0x05e02) ||
        mInRange(localAddress, 0x05003, 0x05e03) ||
        mInRange(localAddress, 0x02140, 0x02f6f) ||
        mInRange(localAddress, 0x02180, 0x02faf) ||
        mInRange(localAddress, 0x02080, 0x02eaf) ||
        mInRange(localAddress, 0x03140, 0x03f6f) ||
        mInRange(localAddress, 0x03080, 0x03eaf) ||
        mInRange(localAddress, 0x44000, 0x4432F))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60290021IsOcnFaceplateSideRegister(AtDevice self, uint32 address)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(self, cThaModuleOcn);
    ThaModulePoh pohModule = (ThaModulePoh)AtDeviceModuleGet(self, cThaModulePoh);
    uint32 baseAddress = Tha60210011ModuleOcnBaseAddress(ocnModule);
    uint32 localAddress = address - baseAddress;
    uint32 stopOffset;

    if (mInRange(localAddress, 0x20000, 0x20000) ||
        mInRange(localAddress, 0x2000d, 0x2000d) ||
        mInRange(localAddress, 0x2000e, 0x2000e) ||
        mInRange(localAddress, 0x20001, 0x20001) ||
        mInRange(localAddress, 0x2000a, 0x2000a) ||
        mInRange(localAddress, 0x20009, 0x20009) ||
        mInRange(localAddress, 0x20010, 0x20010) ||
        mInRange(localAddress, 0x20011, 0x20011) ||
        mInRange(localAddress, 0x20012, 0x20012) ||
        mInRange(localAddress, 0x20013, 0x20013) ||
        mInRange(localAddress, 0x22000, 0x22e2f) ||
        mInRange(localAddress, 0x23000, 0x23e2f) ||
        mInRange(localAddress, 0x22140, 0x22f6f) ||
        mInRange(localAddress, 0x22180, 0x22faf) ||
        mInRange(localAddress, 0x22080, 0x22eaf) ||
        mInRange(localAddress, 0x23140, 0x23f6f) ||
        mInRange(localAddress, 0x23080, 0x23eaf) ||
        mInRange(localAddress, 0x21000, 0x21700) ||
        mInRange(localAddress, 0x21010, 0x2171f) ||
        mInRange(localAddress, 0x27000, 0x27000) ||
        mInRange(localAddress, 0x27001, 0x27001) ||
        mInRange(localAddress, 0x27002, 0x27002) ||
        mInRange(localAddress, 0x27003, 0x27003) ||
        mInRange(localAddress, 0x27004, 0x27004) ||
        mInRange(localAddress, 0x27005, 0x27005) ||
        mInRange(localAddress, 0x27006, 0x27006) ||
        mInRange(localAddress, 0x27007, 0x27007) ||
        mInRange(localAddress, 0x27010, 0x27017) ||
        mInRange(localAddress, 0x27100, 0x27107) ||
        mInRange(localAddress, 0x27140, 0x27147) ||
        mInRange(localAddress, 0x27180, 0x27187) ||
        mInRange(localAddress, 0x271c0, 0x271c7) ||
        mInRange(localAddress, 0x27108, 0x2710f) ||
        mInRange(localAddress, 0x27148, 0x2714f) ||
        mInRange(localAddress, 0x27188, 0x2718f) ||
        mInRange(localAddress, 0x271c8, 0x271cf) ||
        mInRange(localAddress, 0x27128, 0x2712f) ||
        mInRange(localAddress, 0x27168, 0x2716f) ||
        mInRange(localAddress, 0x271a8, 0x271af) ||
        mInRange(localAddress, 0x271e8, 0x270ef) ||
        mInRange(localAddress, 0x27110, 0x27117) ||
        mInRange(localAddress, 0x27118, 0x2711f) ||
        mInRange(localAddress, 0x27120, 0x27127) ||
        mInRange(localAddress, 0x27080, 0x27087) ||
        mInRange(localAddress, 0x27088, 0x2708f) ||
        mInRange(localAddress, 0x27090, 0x27097) ||
        mInRange(localAddress, 0x2709e, 0x2709e) ||
        mInRange(localAddress, 0x2709f, 0x2709f) ||
        mInRange(localAddress, 0x44400, 0x44730))
        return cAtTrue;

    /* TTI related registers */
    localAddress = address - Tha60210011ModulePohBaseAddress(pohModule);
    stopOffset = 16 + (128 * 4);
    if (mInRange(localAddress, 0xB0600, 0xB077F) ||
        mInRange(localAddress, 0xB1600, 0xB177F) ||
        mInRange(localAddress, 0x2A180, 0x2A1DE) ||
        mInRange(localAddress, 0xD0040, 0xD0040 + stopOffset) ||
        mInRange(localAddress, 0xD0020, 0xD0020 + stopOffset))
        return cAtTrue;

    return cAtFalse;
    }

eBool Tha60290021IsOcnTerminatedSideRegister(AtDevice self, uint32 address)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(self, cThaModuleOcn);
    uint32 baseAddress = Tha60210011ModuleOcnBaseAddress(ocnModule);
    uint32 localAddress = address - baseAddress;

    if (mInRange(localAddress, 0x22080, 0x22eaf) ||
        mInRange(localAddress, 0x23080, 0x23eaf) ||
        mInRange(localAddress, 0x44800, 0x44B2F) ||
        mInRange(localAddress, 0x48000, 0x4872f) ||
        mInRange(localAddress, 0x60000, 0x7402f) ||
        mInRange(localAddress, 0x60800, 0x74fff) ||
        mInRange(localAddress, 0x80000, 0x9402f) ||
        mInRange(localAddress, 0x80800, 0x94fff) ||
        mInRange(localAddress, 0x62800, 0x76fff) ||
        mInRange(localAddress, 0x63000, 0x77fff) ||
        mInRange(localAddress, 0x61000, 0x75fff) ||
        mInRange(localAddress, 0x82800, 0x96fff) ||
        mInRange(localAddress, 0x81000, 0x95fff))

        return cAtTrue;

    return cAtFalse;
    }

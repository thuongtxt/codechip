/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEVICE
 * 
 * File        : Tha60290021Device.h
 * 
 * Created Date: Jul 12, 2016
 *
 * Description : Device Management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021DEVICE_H_
#define _THA60290021DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleEth.h"
#include "../../Tha60210051/man/Tha60210051Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021Device*  Tha60290021Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleBer  Tha60290021ModuleBerNew(AtDevice device);
AtModulePdh  Tha60290021ModulePdhNew(AtDevice device);
AtModule     Tha60290021ModulePohNew(AtDevice device);
AtModulePrbs Tha60290021ModulePrbsNew(AtDevice device);
AtModule     Tha60290021ModuleOcnNew(AtDevice device);

eBool Tha60290021DeviceIsRunningOnOtherPlatform(AtDevice device);
void Tha6029DevicePinsDiagnosticFrequencyDisplay(AtDevice self, uint32 regAddr, const char *name);
void Tha6029DeviceTickClockStatusDisplay(AtDevice self, uint32 regAddr, const char *name);
void Tha60290021DeviceClockStatusDisplay(AtDevice self);
eBool Tha60290021DeviceIsRunningOnVu13P(AtDevice self);
uint32 Tha60290021DeviceTopRegisterWithLocalAddress(AtDevice self, uint32 localAddress);
uint32 Tha6029DeviceRefout8kStatusCounter(AtDevice self, uint8 clockId);
eAtRet Tha60290021DeviceFaceplateSerdesTimingModeInvalidate(AtDevice self);
void Tha60290021DeviceInputClocksCheck(AtDevice self);

uint32 Tha60290021DeviceNumFaceplateSerdes(Tha60290021Device self);
uint32 Tha60290021DeviceNumMateSerdes(Tha60290021Device self);
uint32 Tha60290021DeviceNumBackplaneSerdes(Tha60290021Device self);
uint32 Tha60290021DeviceNumSgmiiSerdes(Tha60290021Device self);
uint8 Tha60290021DeviceFaceplateSerdesStartId(Tha60290021Device self);
uint8 Tha60290021DeviceFaceplateSerdesEndId(Tha60290021Device self);
uint8 Tha60290021DeviceMateSerdesStartId(Tha60290021Device self);
uint8 Tha60290021DeviceMateSerdesEndId(Tha60290021Device self);
uint8 Tha60290021DeviceBackplaneSerdesStartId(Tha60290021Device self);
uint8 Tha60290021DeviceBackplaneSerdesEndId(Tha60290021Device self);
uint8 Tha60290021DeviceSgmiiSerdesStartId(Tha60290021Device self);
uint8 Tha60290021DeviceSgmiiSerdesEndId(Tha60290021Device self);
/* Register ranges */
eBool Tha60290021IsOcnTfi5SideRegister(AtDevice self, uint32 address);
eBool Tha60290021IsOcnFaceplateSideRegister(AtDevice self, uint32 address);
eBool Tha60290021IsOcnTerminatedSideRegister(AtDevice self, uint32 address);

/* clock monitoring */
void Tha60290021DeviceBackplaneClockStatusDisplay(AtDevice self);
void Tha60290021DeviceRefClockStatusDisplay(AtDevice self);
uint32 Tha60290021DeviceFaceplateFreqExpectedValue(AtDevice self, uint32 serdesId);
void Tha60290021DeviceSlowClockStatusDisplay(AtDevice self, uint32 expectedFreq);
void Tha60290021DeviceFsmFrequencyDisplay(AtDevice self);
void Tha60290021DeviceSpareGpioFrequencyDisplay(AtDevice self, uint8 maxPins);
void Tha60290021DeviceSpareFrequencyDisplay(AtDevice self);
void Tha60290021DeviceRefout8kDisplay(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021DEVICE_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device
 *
 * File        : Tha60290021InterruptController.c
 *
 * Created Date: Apr 26, 2017
 *
 * Description : 60290021 interrupt controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290021InterruptController.h"
#include "Tha60290021InterruptControllerInternal.h"
#include "../pw/Tha60290021ModulePw.h"
#include "Tha60290021DeviceReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_global_interrupt_mask_enable_DCCKIntEnable_Mask        cBit19

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tThaIpCoreMethods m_ThaIpCoreOverride;

/* Reference to super implementation */
static const tThaIpCoreMethods *m_ThaIpCoreImplement = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ApsCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & cAf6_global_interrupt_status_UpsrIntStatus_Mask) ? cAtTrue : cAtFalse;
    }

static eBool EthCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    AtUnused(self);
    return (intrStatus & (cAf6_global_interrupt_status_TenGeIntStatus_Mask|
                          cAf6_global_interrupt_status_GeIntStatus_Mask   |
                          cAf6_global_interrupt_status_Mac40gIntStatus_Mask)) ? cAtTrue : cAtFalse;
    }

static eBool PwCauseInterrupt(ThaIpCore self, uint32 intrStatus)
    {
    if (m_ThaIpCoreImplement->PwCauseInterrupt(self, intrStatus))
        return cAtTrue;

    return (intrStatus & cAf6_global_interrupt_mask_enable_DCCKIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static void ApsHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_mask_enable_UpsrIntEnable_Mask, enable);
    }

static void EthHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    ThaIpCoreHwInterruptEnable(self,
                               cAf6_global_interrupt_mask_enable_TenGeIntEnable_Mask|
                               cAf6_global_interrupt_mask_enable_GeIntEnable_Mask   |
                               cAf6_global_interrupt_mask_enable_Mac40gIntEnable_Mask, enable);
    }

static void PwHwInterruptEnable(ThaIpCore self, eBool enable)
    {
    m_ThaIpCoreImplement->PwHwInterruptEnable(self, enable);

    ThaIpCoreHwInterruptEnable(self, cAf6_global_interrupt_mask_enable_DCCKIntEnable_Mask, enable);
    }

static eBool CanEnableHwInterruptPin(ThaIpCore self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool InterruptRestoreIsSupported(ThaIpCore self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaIpCore(ThaIntrController self)
    {
    ThaIpCore core = (ThaIpCore)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaIpCoreImplement = mMethodsGet(core);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaIpCoreOverride, m_ThaIpCoreImplement, sizeof(m_ThaIpCoreOverride));

        mMethodOverride(m_ThaIpCoreOverride, ApsCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, EthCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, PwCauseInterrupt);
        mMethodOverride(m_ThaIpCoreOverride, ApsHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, EthHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, PwHwInterruptEnable);
        mMethodOverride(m_ThaIpCoreOverride, CanEnableHwInterruptPin);
        mMethodOverride(m_ThaIpCoreOverride, InterruptRestoreIsSupported);
        }

    mMethodsSet(core, &m_ThaIpCoreOverride);
    }

static void Override(ThaIntrController self)
    {
    OverrideThaIpCore(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021InterruptController);
    }

ThaIntrController Tha60290021IntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011IntrControllerObjectInit(self, ipCore) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaIntrController Tha60290021IntrControllerNew(AtIpCore ipCore)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaIntrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021IntrControllerObjectInit(newController, ipCore);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAN
 * 
 * File        : Tha60290021DeviceFsm.h
 * 
 * Created Date: Oct 19, 2018
 *
 * Description : 60290021 Device FSM interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021DEVICEFSM_H_
#define _THA60290021DEVICEFSM_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290021DeviceRoleActiveForce(AtDevice self, eBool forced);
eBool Tha60290021DeviceRoleActiveIsForced(AtDevice self);
eAtRet Tha60290021DeviceRoleInputEnable(AtDevice self, eBool enabled);
eBool Tha60290021DeviceRoleInputIsEnabled(AtDevice self);
eBool Tha60290021DeviceTxFsmPinStatus(AtDevice self);
eAtDeviceRole Tha60290021DeviceRoleInputStatus(AtDevice self);
eAtRet Tha60290021DeviceRxFsmPinEnable(AtDevice self, eBool enabled);
eAtRet Tha60290021DeviceTxBackPlaneSerdesBridgeEnable(AtDevice self, eBool enabled);
uint32 Tha60290021DeviceRxBackPlaneSelectedSerdes(AtDevice self);
eAtRet Tha60290021DeviceRxBackPlaneSerdesSelect(AtDevice self, uint32 serId);
eBool Tha60290021DeviceRxFsmPinStatusGet(AtDevice self);
eBool Tha60290021DeviceRxFsmPinIsEnabled(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021DEVICEFSM_H_ */


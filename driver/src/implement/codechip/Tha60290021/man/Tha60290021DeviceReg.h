/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies. The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      :
 *
 * File        :
 *
 * Created Date:
 *
 * Description : This file contain all constance definitions of  block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_RD_TOP_GLB_H_
#define _AF6_REG_AF6CNC0021_RD_TOP_GLB_H_

/*--------------------------- Define -----------------------------------------*/
#define cTopBaseAddress                     0xF00000
#define cInterruptPinBaseAdress             0xF42000
#define cMDIO1GBaseAddress                  0xF54000
#define cSerdesMateG0ConfigureBaseAdress    0xF70000
#define cSerdesMateG1ConfigureBaseAdress    0xF72000
#define cSerdesEth40GFirstPortBaseAdress    0xF80000
#define cSerdesEth40GSecondPortBaseAdress   0xF90000
#define cSerdesOcnGbeBaseAdress             0xF60000
#define cSerdesSgmiiDiagBaseAdress          0xF53000
#define cSerdesXfi0DiagBaseAdress           0xFA2000
#define cSerdesXfi8DiagBaseAdress           0xFA6000
#define cSerdesFaceplateGateTimeBaseAdress  0xFA0800


/*------------------------------------------------------------------------------
Reg Name   : Device Product ID
Reg Addr   : 0xF0_0000
Reg Formula:
    Where  :
Reg Desc   :
This register indicates Product ID.

------------------------------------------------------------------------------*/
#define cAf6Reg_ProductID_Base                                                                        0xF00000

/*--------------------------------------
BitField Name: ProductID
BitField Type: RO
BitField Desc: ProductId
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_ProductID_ProductID_Mask                                                                 cBit31_0
#define cAf6_ProductID_ProductID_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Device Year Month Day Version ID
Reg Addr   : 0xF0_0001
Reg Formula:
    Where  :
Reg Desc   :
This register indicates Year Month Day and main version ID.

------------------------------------------------------------------------------*/
#define cAf6Reg_YYMMDD_VerID_Base                                                                     0xF00001

/*--------------------------------------
BitField Name: Year
BitField Type: RO
BitField Desc: Year
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_YYMMDD_VerID_Year_Mask                                                                  cBit31_24
#define cAf6_YYMMDD_VerID_Year_Shift                                                                        24

/*--------------------------------------
BitField Name: Month
BitField Type: RO
BitField Desc: Month
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_YYMMDD_VerID_Month_Mask                                                                 cBit23_16
#define cAf6_YYMMDD_VerID_Month_Shift                                                                       16

/*--------------------------------------
BitField Name: Day
BitField Type: RO
BitField Desc: Day
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_YYMMDD_VerID_Day_Mask                                                                    cBit15_8
#define cAf6_YYMMDD_VerID_Day_Shift                                                                          8

/*--------------------------------------
BitField Name: Version
BitField Type: RO
BitField Desc: Version
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_YYMMDD_VerID_Version_Mask                                                                 cBit7_0
#define cAf6_YYMMDD_VerID_Version_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Device Internal ID
Reg Addr   : 0xF0_0003
Reg Formula:
    Where  :
Reg Desc   :
This register indicates internal ID.

------------------------------------------------------------------------------*/
#define cAf6Reg_InternalID_Base                                                                       0xF00003
#define cAf6Reg_InternalID_WidthVal                                                                         32

/*--------------------------------------
BitField Name: InternalID
BitField Type: RO
BitField Desc: InternalID
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_InternalID_InternalID_Mask                                                               cBit15_0
#define cAf6_InternalID_InternalID_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : general purpose inputs for TX Uart
Reg Addr   : 0x5
Reg Formula: 0x5
    Where  :
Reg Desc   :
This is the global configuration register for the MIG Reset Control

------------------------------------------------------------------------------*/
#define cAf6Reg_o_reset_ctr_Base                                                                           0x5

/*--------------------------------------
BitField Name: QDR1_Reset
BitField Type: RW
BitField Desc: Reset Control for QDR#1 0: Disable 1: Enable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_o_reset_ctr_QDR1_Reset_Mask                                                                 cBit4
#define cAf6_o_reset_ctr_QDR1_Reset_Shift                                                                    4

/*--------------------------------------
BitField Name: DDR4_Reset
BitField Type: RW
BitField Desc: Reset Control for DDR#4 0: Disable 1: Enable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_o_reset_ctr_DDR4_Reset_Mask                                                                 cBit3
#define cAf6_o_reset_ctr_DDR4_Reset_Shift                                                                    3

/*--------------------------------------
BitField Name: DDR3_Reset
BitField Type: RW
BitField Desc: Reset Control for DDR#3 0: Disable 1: Enable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_o_reset_ctr_DDR3_Reset_Mask                                                                 cBit2
#define cAf6_o_reset_ctr_DDR3_Reset_Shift                                                                    2

/*--------------------------------------
BitField Name: DDR2_Reset
BitField Type: RW
BitField Desc: Reset Control for DDR#2 0: Disable 1: Enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_o_reset_ctr_DDR2_Reset_Mask                                                                 cBit1
#define cAf6_o_reset_ctr_DDR2_Reset_Shift                                                                    1

/*--------------------------------------
BitField Name: DDR1_Reset
BitField Type: RW
BitField Desc: Reset Control for DDR#1 0: Disable 1: Enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_o_reset_ctr_DDR1_Reset_Mask                                                                 cBit0
#define cAf6_o_reset_ctr_DDR1_Reset_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : general purpose inputs for TX Uart
Reg Addr   : 0x40
Reg Formula: 0x40
    Where  :
Reg Desc   :
This is the global configuration register for the Global Serdes Control

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control0_Base                                                                           0x40

/*--------------------------------------
BitField Name: eth_40g_tx_dup
BitField Type: RW
BitField Desc: Duplicate TX for 2 eth 40G
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_o_control0_eth_40g_tx_dup_Mask                                                             cBit31
#define cAf6_o_control0_eth_40g_tx_dup_Shift                                                                31

/*--------------------------------------
BitField Name: fsm_eth_40g_rx_gsel
BitField Type: RW
BitField Desc: global selection between external FSM-RXMAC and inernal selection
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_o_control0_fsm_eth_40g_rx_gsel_Mask                                                        cBit30
#define cAf6_o_control0_fsm_eth_40g_rx_gsel_Shift                                                           30

/*--------------------------------------
BitField Name: fsm_eth_40g_rx_isel
BitField Type: RW
BitField Desc: internal selection
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_o_control0_fsm_eth_40g_rx_isel_Mask                                                        cBit29
#define cAf6_o_control0_fsm_eth_40g_rx_isel_Shift                                                           29

/*--------------------------------------
BitField Name: fsm_card_pw_tx_gsel
BitField Type: RW
BitField Desc: global selection between external FSM-CARD and inernal selection
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_o_control0_fsm_card_pw_tx_gsel_Mask                                                        cBit28
#define cAf6_o_control0_fsm_card_pw_tx_gsel_Shift                                                           28

/*--------------------------------------
BitField Name: fsm_card_pw_tx_isel
BitField Type: RW
BitField Desc: internal selection
BitField Bits: [27:27]
--------------------------------------*/
#define cAf6_o_control0_fsm_card_pw_tx_isel_Mask                                                        cBit27
#define cAf6_o_control0_fsm_card_pw_tx_isel_Shift                                                           27

/*--------------------------------------
BitField Name: multirate_sel
BitField Type: RW
BitField Desc: select 2 two RX group of Multirate serdes to CORE
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_o_control0_multirate_sel_Mask                                                              cBit16
#define cAf6_o_control0_multirate_sel_Shift                                                                 16

/*--------------------------------------
BitField Name: o_control0
BitField Type: RW
BitField Desc: Uart TX 8-bit data out
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_o_control0_o_control0_Mask                                                                cBit7_0
#define cAf6_o_control0_o_control0_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Flow Control Mechanism for Ethernet pass-through Diagnostic Value
Reg Addr   : 0x41
Reg Formula: 0x41
    Where  :
Reg Desc   :
This is the global configuration register for Flow Control Mechanism for Ethernet pass-through Diagnostic

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control1_Base                                                                           0x41

/*--------------------------------------
BitField Name: flowctl_gr2
BitField Type: RW
BitField Desc: FIFO status of Group 2
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_o_control1_flowctl_gr2_Mask                                                             cBit31_16
#define cAf6_o_control1_flowctl_gr2_Shift                                                                   16

/*--------------------------------------
BitField Name: flowctl_gr1
BitField Type: RW
BitField Desc: FIFO status of Group 1
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_o_control1_flowctl_gr1_Mask                                                              cBit15_0
#define cAf6_o_control1_flowctl_gr1_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Force Error for Flow Control Mechanism Diagnostic
Reg Addr   : 0x42
Reg Formula: 0x42
    Where  :
Reg Desc   :
This is the global configuration Error register for Flow Control Mechanism for Ethernet pass-through Diagnostic

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control2_Base                                                                           0x42

/*--------------------------------------
BitField Name: flowctl_diagen
BitField Type: RW
BitField Desc: Ethernet pass through flow Control Diagnostic Enable 0: Normal
operation, select engine flow control 1: Diagnostic operation, select diagnostic
flow control
BitField Bits: [1:1]
--------------------------------------*/
#define cAf6_o_control2_flowctl_diagen_Mask                                                              cBit1
#define cAf6_o_control2_flowctl_diagen_Shift                                                                 1

/*--------------------------------------
BitField Name: flowctl_force
BitField Type: RW
BitField Desc: Force Error for Flow Control Mechanism Diagnostic 0: Release 1:
Insert Error
BitField Bits: [0:0]
--------------------------------------*/
#define cAf6_o_control2_flowctl_force_Mask                                                               cBit0
#define cAf6_o_control2_flowctl_force_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Diagnostic Enable Control
Reg Addr   : 0x43
Reg Formula: 0x43
    Where  :
Reg Desc   :
This is the global configuration register for the MIG (DDR/QDR) Diagnostic Control

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control3_Base                                                                           0x43

/*--------------------------------------
BitField Name: SFP_OCN_ETH_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for SFP_OCN_ETH #1-#16 (Per bit per port) 0:
Disable 1: Enable
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_o_control3_SFP_OCN_ETH_DiagEn_Mask                                                      cBit31_16
#define cAf6_o_control3_SFP_OCN_ETH_DiagEn_Shift                                                            16

/*--------------------------------------
BitField Name: TSI_Mate_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for TSI_Mate #1-#8 (Per bit per port) 0:
Disable 1: Enable
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_o_control3_TSI_Mate_DiagEn_Mask                                                          cBit15_8
#define cAf6_o_control3_TSI_Mate_DiagEn_Shift                                                                8

/*--------------------------------------
BitField Name: DDR4_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for DDR#2 0: Disable 1: Enable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_o_control3_DDR4_DiagEn_Mask                                                                 cBit4
#define cAf6_o_control3_DDR4_DiagEn_Shift                                                                    4

/*--------------------------------------
BitField Name: DDR3_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for DDR#1 0: Disable 1: Enable
BitField Bits: [3]
--------------------------------------*/
#define cAf6_o_control3_DDR3_DiagEn_Mask                                                                 cBit3
#define cAf6_o_control3_DDR3_DiagEn_Shift                                                                    3

/*--------------------------------------
BitField Name: DDR2_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for DDR#2 0: Disable 1: Enable
BitField Bits: [2]
--------------------------------------*/
#define cAf6_o_control3_DDR2_DiagEn_Mask                                                                 cBit2
#define cAf6_o_control3_DDR2_DiagEn_Shift                                                                    2

/*--------------------------------------
BitField Name: DDR1_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for DDR#1 0: Disable 1: Enable
BitField Bits: [1]
--------------------------------------*/
#define cAf6_o_control3_DDR1_DiagEn_Mask                                                                 cBit1
#define cAf6_o_control3_DDR1_DiagEn_Shift                                                                    1

/*--------------------------------------
BitField Name: QDR1_DiagEn
BitField Type: RW
BitField Desc: Enable Daignostic for QDR#1 0: Disable 1: Enable
BitField Bits: [0]
--------------------------------------*/
#define cAf6_o_control3_QDR1_DiagEn_Mask                                                                 cBit0
#define cAf6_o_control3_QDR1_DiagEn_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Bus Type port 1 to 8
Reg Addr   : 0x44
Reg Formula: 0x44
    Where  : 
Reg Desc   : 
This is the global configuration register for the Global Serdes Bus Type for Diagnostic OC192_OC48_OC12_10G_1G_100FX_16ch_Diag port 1 to 8

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control4_Base                                                                           0x44

/*--------------------------------------
BitField Name: Bus_Type_Sel8
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 8 0: OC192 1: XFI 2: OC48/OC12/OC3
3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7
9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [31:28]
--------------------------------------*/
#define cAf6_o_control4_Bus_Type_Sel8_Mask                                                           cBit31_28
#define cAf6_o_control4_Bus_Type_Sel8_Shift                                                                 28

/*--------------------------------------
BitField Name: Bus_Type_Sel7
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 7 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_o_control4_Bus_Type_Sel7_Mask                                                           cBit27_24
#define cAf6_o_control4_Bus_Type_Sel7_Shift                                                                 24

/*--------------------------------------
BitField Name: Bus_Type_Sel6
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 6 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_o_control4_Bus_Type_Sel6_Mask                                                           cBit23_20
#define cAf6_o_control4_Bus_Type_Sel6_Shift                                                                 20

/*--------------------------------------
BitField Name: Bus_Type_Sel5
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 5 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_o_control4_Bus_Type_Sel5_Mask                                                           cBit19_16
#define cAf6_o_control4_Bus_Type_Sel5_Shift                                                                 16

/*--------------------------------------
BitField Name: Bus_Type_Sel4
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 4 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_o_control4_Bus_Type_Sel4_Mask                                                           cBit15_12
#define cAf6_o_control4_Bus_Type_Sel4_Shift                                                                 12

/*--------------------------------------
BitField Name: Bus_Type_Sel3
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 3 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_o_control4_Bus_Type_Sel3_Mask                                                            cBit11_8
#define cAf6_o_control4_Bus_Type_Sel3_Shift                                                                  8

/*--------------------------------------
BitField Name: Bus_Type_Sel2
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 2 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_o_control4_Bus_Type_Sel2_Mask                                                             cBit7_4
#define cAf6_o_control4_Bus_Type_Sel2_Shift                                                                  4

/*--------------------------------------
BitField Name: Bus_Type_Sel1
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 1 0: OC192 1: XFI 2: OC48/OC12/OC3
3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31 8: RAW16 PRBS 7
9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_o_control4_Bus_Type_Sel1_Mask                                                             cBit3_0
#define cAf6_o_control4_Bus_Type_Sel1_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Raw PRSB test for OC192_OC48_OC12_10G_1G_100FX_16ch Serdes
Reg Addr   : 0x45
Reg Formula: 0x45
    Where  :
Reg Desc   :
This is the TOP global configuration register unused

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control5_Base                                                                           0x45

/*--------------------------------------
BitField Name: prbs_ier
BitField Type: RW
BitField Desc: Insert Error for Raw PRBS for port 16 to port 1 (per port per
bit) 1: Insert 0: Normal
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_o_control5_prbs_ier_Mask                                                                cBit31_16
#define cAf6_o_control5_prbs_ier_Shift                                                                      16

/*--------------------------------------
BitField Name: prbs_en
BitField Type: RW
BitField Desc: Enable test for Raw PRBS ( Serdes should init mode OC192 )for
port 16 to port 1 (per port per bit) 1: Enable (Must config PRBS Bus_Type_Sel at
o_control4) 0: Disable
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_o_control5_prbs_en_Mask                                                                  cBit15_0
#define cAf6_o_control5_prbs_en_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : unused
Reg Addr   : 0x46
Reg Formula: 0x46
    Where  :
Reg Desc   :
This is the TOP global configuration registerunused

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control6_Base                                                                           0x46

/*--------------------------------------
BitField Name: Bus_Type_Sel16
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 16 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [31:28]
--------------------------------------*/
#define cAf6_o_control6_Bus_Type_Sel16_Mask                                                          cBit31_28
#define cAf6_o_control6_Bus_Type_Sel16_Shift                                                                28

/*--------------------------------------
BitField Name: Bus_Type_Sel15
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 15 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_o_control6_Bus_Type_Sel15_Mask                                                          cBit27_24
#define cAf6_o_control6_Bus_Type_Sel15_Shift                                                                24

/*--------------------------------------
BitField Name: Bus_Type_Sel14
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 14 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_o_control6_Bus_Type_Sel14_Mask                                                          cBit23_20
#define cAf6_o_control6_Bus_Type_Sel14_Shift                                                                20

/*--------------------------------------
BitField Name: Bus_Type_Sel13
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 13 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_o_control6_Bus_Type_Sel13_Mask                                                          cBit19_16
#define cAf6_o_control6_Bus_Type_Sel13_Shift                                                                16

/*--------------------------------------
BitField Name: Bus_Type_Sel12
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 12 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_o_control6_Bus_Type_Sel12_Mask                                                          cBit15_12
#define cAf6_o_control6_Bus_Type_Sel12_Shift                                                                12

/*--------------------------------------
BitField Name: Bus_Type_Sel11
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 11 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_o_control6_Bus_Type_Sel11_Mask                                                           cBit11_8
#define cAf6_o_control6_Bus_Type_Sel11_Shift                                                                 8

/*--------------------------------------
BitField Name: Bus_Type_Sel10
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 10 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [7:4]
--------------------------------------*/
#define cAf6_o_control6_Bus_Type_Sel10_Mask                                                            cBit7_4
#define cAf6_o_control6_Bus_Type_Sel10_Shift                                                                 4

/*--------------------------------------
BitField Name: Bus_Type_Sel9
BitField Type: RW
BitField Desc: Configure Bus Type for Diagnostic
OC192_OC48_OC12_10G_1G_100FX_16ch_Diag Port 9 0: unused 1: unused 2:
OC48/OC12/OC3 3: 1G 4: 100Fx 5: RAW64 PRBS 7 6: RAW64 PRBS 23 7: RAW64 PRBS 31
8: RAW16 PRBS 7 9: RAW16 PRBS 23 a: RAW16 PRBS 31
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_o_control6_Bus_Type_Sel9_Mask                                                             cBit3_0
#define cAf6_o_control6_Bus_Type_Sel9_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : 8Khz clock out enable for Diagnostic
Reg Addr   : 0x47
Reg Formula: 0x47
    Where  :
Reg Desc   :
This is the TOP global configuration register

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control7_Base                                                                           0x47

/*-----------------------------------------------------------------------------
Field: [26]    %% ref2disable      %% Configure refout2 disable
            {0} :enable send refout2 8Khz
            {1} :disable send refout2 8Khz
-------------------------------------------------------------------------------*/
#define cAf6_o_control7_ref2disable_Mask  cBit26
#define cAf6_o_control7_ref2disable_Shift 26

/*-----------------------------------------------------------------------------
Field: [25]    %% ref1disable      %% Configure refout1 disable
            {0} :enable send refout1 8Khz
            {1} :disable send refout1 8Khz
-------------------------------------------------------------------------------*/
#define cAf6_o_control7_ref1disable_Mask  cBit25
#define cAf6_o_control7_ref1disable_Shift 25

/*--------------------------------------
BitField Name: cfgref2mod
BitField Type: RW
BitField Desc: Configure refout2 is output from rxlclk of Faceplate_Serdeses or
refout2 is PLL lock status
BitField Bits: [24]
--------------------------------------*/
#define cAf6_o_control7_cfgref2mod_Mask                                                                 cBit24
#define cAf6_o_control7_cfgref2mod_Shift                                                                    24

#define cAf6_o_control7_cfgrefpid_Mask(clockId)               (cBit19_16 << ((clockId * 4)))
#define cAf6_o_control7_cfgrefpid_Shift(clockId)              (16 + (clockId * 4))

/*--------------------------------------
BitField Name: cfgref2pid
BitField Type: RW
BitField Desc: Configure refout2 source from Faceplate_Serdeses value 0 to 15
for 16 port
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_o_control7_cfgref2pid_Mask                                                              cBit23_20
#define cAf6_o_control7_cfgref2pid_Shift                                                                    20

/*--------------------------------------
BitField Name: cfgref1pid
BitField Type: RW
BitField Desc: Configure refout1 source from Faceplate_Serdeses value 0 to 15
for 16 port
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_o_control7_cfgref1pid_Mask                                                              cBit19_16
#define cAf6_o_control7_cfgref1pid_Shift                                                                    16

/*--------------------------------------
BitField Name: cfgdiagen
BitField Type: RW
BitField Desc: select diag refout8k or user refout8k from core,bit per sub port,
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_o_control7_cfgdiagen_Mask(pId)                   (cBit0 << pId)
#define cAf6_o_control7_cfgdiagen_Shift(pId)                  (0 + pId)


/*------------------------------------------------------------------------------
Reg Name   : Configure Frequency of Faceplate_Serdeses RX CLK
Reg Addr   : 0x48
Reg Formula: 0x48
    Where  :
Reg Desc   :
This is the TOP global configuration register

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control8_Base                                                                           0x48

/*--------------------------------------
BitField Name: cfgrate
BitField Type: RW
BitField Desc: Configure rate for Faceplate Serdeses RX CLK  2 bit per sub port
<exp : [1:0] for port 0.....[31:30] for port 15>
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_o_control8_cfgrate_Mask(pId)                (cBit1_0 << (pId * 2))
#define cAf6_o_control8_cfgrate_Shift(pId)               (0 + (pId * 2))


/*------------------------------------------------------------------------------
Reg Name   : Configure Clock monitor output
Reg Addr   : 0x49
Reg Formula: 0x49
    Where  :
Reg Desc   :
This is the TOP global configuration register

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control9_Base                                                                           0x49

/*--------------------------------------
BitField Name: cfgrefpid
BitField Type: RW
BitField Desc: Configure Source Clock monitor output for clock_mon
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_o_control9_cfgrefpid_Mask                                                                 cBit4_0
#define cAf6_o_control9_cfgrefpid_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Configure MII test enable
Reg Addr   : 0x4A
Reg Formula: 0x4A
    Where  :
Reg Desc   :
This is the TOP global configuration register

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control10_Base                                                                          0x4A
#define cAf6Reg_o_control10_WidthVal                                                                        32

/*--------------------------------------
BitField Name: Channel0_Testen
BitField Type: R/W
BitField Desc: Set 1 to enable test MII channel 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_o_control10_Channel0_Testen_Mask                                                            cBit0
#define cAf6_o_control10_Channel0_Testen_Shift                                                               0

/*--------------------------------------
BitField Name: Channel1_Testen
BitField Type: R/W
BitField Desc: Set 1 to enable test MII channel 1
BitField Bits: [1]
--------------------------------------*/
#define cAf6_o_control10_Channel1_Testen_Mask                                                            cBit1
#define cAf6_o_control10_Channel1_Testen_Shift                                                               1

/*--------------------------------------
BitField Name: Channel2_Testen
BitField Type: R/W
BitField Desc: Set 1 to enable test MII channel 2
BitField Bits: [2]
--------------------------------------*/
#define cAf6_o_control10_Channel2_Testen_Mask                                                            cBit2
#define cAf6_o_control10_Channel2_Testen_Shift                                                               2

/*--------------------------------------
BitField Name: Channel3_Testen
BitField Type: R/W
BitField Desc: Set 1 to enable test MII channel 3
BitField Bits: [3]
--------------------------------------*/
#define cAf6_o_control10_Channel3_Testen_Mask                                                            cBit3
#define cAf6_o_control10_Channel3_Testen_Shift                                                               3


/*------------------------------------------------------------------------------
Reg Name   : Configure Lost of Clock Threshold For 10Ge
Reg Addr   : 0x4D
Reg Formula: 0x4D
    Where  :
Reg Desc   :
This is the TOP global configuration register

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control13_Base                                                                          0x4D
#define cAf6Reg_o_control13_WidthVal                                                                        32

/*--------------------------------------
BitField Name: Lost_clock_thres_10g
BitField Type: R/W
BitField Desc: This value will be multiply PPM value by 156
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_o_control13_Lost_clock_thres_10g_Mask                                                    cBit27_0
#define cAf6_o_control13_Lost_clock_thres_10g_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Configure Out of Frequency Threshold For 10Ge
Reg Addr   : 0x4E
Reg Formula: 0x4E
    Where  :
Reg Desc   :
This is the TOP global configuration register

------------------------------------------------------------------------------*/
#define cAf6Reg_o_control14_Base                                                                          0x4E
#define cAf6Reg_o_control14_WidthVal                                                                        32

/*--------------------------------------
BitField Name: Out_of_freq_thres_10g
BitField Type: R/W
BitField Desc: This value will be multiply PPM value by 156
BitField Bits: [27:0]
--------------------------------------*/
#define cAf6_o_control14_Out_of_freq_thres_10g_Mask                                                   cBit27_0
#define cAf6_o_control14_Out_of_freq_thres_10g_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : general purpose outputs from RX Uart
Reg Addr   : 0x60
Reg Formula: 0x60
    Where  :
Reg Desc   :
This is value output from RX Uart

------------------------------------------------------------------------------*/
#define cAf6Reg_c_uart_Base                                                                               0x60

/*--------------------------------------
BitField Name: fsm_rxmac
BitField Type: RW
BitField Desc: 0: external FSM pin tongle rate 1Mhz to select first mac40g 1:
external FSM pin tongle rate 2Mhz to select second mac40g
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_c_uart_fsm_rxmac_Mask                                                                      cBit31
#define cAf6_c_uart_fsm_rxmac_Shift                                                                         31

/*--------------------------------------
BitField Name: fsm_rxmac_cur
BitField Type: RW
BitField Desc: 0: current selected first mac40g 1: current selected second
mac40g
BitField Bits: [30:30]
--------------------------------------*/
#define cAf6_c_uart_fsm_rxmac_cur_Mask                                                                  cBit30
#define cAf6_c_uart_fsm_rxmac_cur_Shift                                                                     30

/*--------------------------------------
BitField Name: fsm_txpw
BitField Type: RW
BitField Desc: 0: external FSM pin tongle rate 1Mhz to active card 1: external
FSM pin tongle rate 2Mhz to standby card
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_c_uart_fsm_txpw_Mask                                                                       cBit29
#define cAf6_c_uart_fsm_txpw_Shift                                                                          29

/*--------------------------------------
BitField Name: fsm_txpw_cur
BitField Type: RW
BitField Desc: 0: current card is in active state 1: current card is in standby
state
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_c_uart_fsm_txpw_cur_Mask                                                                   cBit28
#define cAf6_c_uart_fsm_txpw_cur_Shift                                                                      28

/*--------------------------------------
BitField Name: i_status0
BitField Type: RO
BitField Desc: Uart RX 8-bit data in
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_c_uart_i_status0_Mask                                                                     cBit7_0
#define cAf6_c_uart_i_status0_Shift                                                                          0


/*------------------------------------------------------------------------------
Reg Name   : unused
Reg Addr   : 0x61
Reg Formula: 0x61
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_unused_Base                                                                               0x61


/*------------------------------------------------------------------------------
Reg Name   : Top Interface Alarm Sticky
Reg Addr   : 0x50
Reg Formula:
    Where  :
Reg Desc   :
Top Interface Alarm Sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_i_sticky0_Base                                                                            0x50

/*--------------------------------------
BitField Name: ot_out
BitField Type: W1C
BitField Desc: Set 1 When Over-Temperature alarm Happens.
BitField Bits: [25]
--------------------------------------*/
#define cAf6_i_sticky0_ot_out_Mask                                                                      cBit25
#define cAf6_i_sticky0_ot_out_Shift                                                                         25

/*--------------------------------------
BitField Name: vbram_alarm_out
BitField Type: W1C
BitField Desc: Set 1 When VCCBRAM-sensor alarm Happens.
BitField Bits: [24]
--------------------------------------*/
#define cAf6_i_sticky0_vbram_alarm_out_Mask                                                             cBit24
#define cAf6_i_sticky0_vbram_alarm_out_Shift                                                                24

/*--------------------------------------
BitField Name: vccaux_alarm_out
BitField Type: W1C
BitField Desc: Set 1 When VCCAUX-sensor alarm Happens.
BitField Bits: [23]
--------------------------------------*/
#define cAf6_i_sticky0_vccaux_alarm_out_Mask                                                            cBit23
#define cAf6_i_sticky0_vccaux_alarm_out_Shift                                                               23

/*--------------------------------------
BitField Name: vccint_alarm_out
BitField Type: W1C
BitField Desc: Set 1 When VCCINT-sensor alarm Happens.
BitField Bits: [22]
--------------------------------------*/
#define cAf6_i_sticky0_vccint_alarm_out_Mask                                                            cBit22
#define cAf6_i_sticky0_vccint_alarm_out_Shift                                                               22

/*--------------------------------------
BitField Name: user_temp_alarm_out
BitField Type: W1C
BitField Desc: Set 1 When Temperature-sensor alarm Happens.
BitField Bits: [21]
--------------------------------------*/
#define cAf6_i_sticky0_user_temp_alarm_out_Mask                                                         cBit21
#define cAf6_i_sticky0_user_temp_alarm_out_Shift                                                            21

/*--------------------------------------
BitField Name: alarm_out
BitField Type: W1C
BitField Desc: Set 1 When SysMon Error Happens.
BitField Bits: [20]
--------------------------------------*/
#define cAf6_i_sticky0_alarm_out_Mask                                                                   cBit20
#define cAf6_i_sticky0_alarm_out_Shift                                                                      20

/*--------------------------------------
BitField Name: SystemPll
BitField Type: W1C
BitField Desc: Set 1 while PLL Locked state change event happens when System PLL
not locked.
BitField Bits: [16]
--------------------------------------*/
#define cAf6_i_sticky0_SystemPll_Mask                                                                   cBit16
#define cAf6_i_sticky0_SystemPll_Shift                                                                      16

/*--------------------------------------
BitField Name: QdrUrst
BitField Type: W1C
BitField Desc: Set 1 while user Reset state Change Happens when QDR   Reset
User.
BitField Bits: [12]
--------------------------------------*/
#define cAf6_i_sticky0_QdrUrst_Mask                                                                     cBit12
#define cAf6_i_sticky0_QdrUrst_Shift                                                                        12

/*--------------------------------------
BitField Name: Ddr44Urst
BitField Type: W1C
BitField Desc: Set 1 while user Reset State Change Happens When DDR#4 Reset
User.
BitField Bits: [11]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr44Urst_Mask                                                                   cBit11
#define cAf6_i_sticky0_Ddr44Urst_Shift                                                                      11

/*--------------------------------------
BitField Name: Ddr43Urst
BitField Type: W1C
BitField Desc: Set 1 while user Reset State Change Happens When DDR#3 Reset
User.
BitField Bits: [10]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr43Urst_Mask                                                                   cBit10
#define cAf6_i_sticky0_Ddr43Urst_Shift                                                                      10

/*--------------------------------------
BitField Name: Ddr42Urst
BitField Type: W1C
BitField Desc: Set 1 while user Reset State Change Happens When DDR#2 Reset
User.
BitField Bits: [9]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr42Urst_Mask                                                                    cBit9
#define cAf6_i_sticky0_Ddr42Urst_Shift                                                                       9

/*--------------------------------------
BitField Name: Ddr41Urst
BitField Type: W1C
BitField Desc: Set 1 while user Reset State Change Happens When DDR#1 Reset
User.
BitField Bits: [8]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr41Urst_Mask                                                                    cBit8
#define cAf6_i_sticky0_Ddr41Urst_Shift                                                                       8

/*--------------------------------------
BitField Name: QdrCalib
BitField Type: W1C
BitField Desc: Set 1 while Calib state change event happens when QDR Calib Fail.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_i_sticky0_QdrCalib_Mask                                                                     cBit4
#define cAf6_i_sticky0_QdrCalib_Shift                                                                        4

/*--------------------------------------
BitField Name: Ddr44Calib
BitField Type: W1C
BitField Desc: Set 1 while Calib State Change Event Happens When DDR#4 Calib
Fail.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr44Calib_Mask                                                                   cBit3
#define cAf6_i_sticky0_Ddr44Calib_Shift                                                                      3

/*--------------------------------------
BitField Name: Ddr43Calib
BitField Type: W1C
BitField Desc: Set 1 while Calib State Change Event Happens When DDR#3 Calib
Fail.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr43Calib_Mask                                                                   cBit2
#define cAf6_i_sticky0_Ddr43Calib_Shift                                                                      2

/*--------------------------------------
BitField Name: Ddr42Calib
BitField Type: W1C
BitField Desc: Set 1 while Calib State Change Event Happens When DDR#2 Calib
Fail.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr42Calib_Mask                                                                   cBit1
#define cAf6_i_sticky0_Ddr42Calib_Shift                                                                      1

/*--------------------------------------
BitField Name: Ddr41Calib
BitField Type: W1C
BitField Desc: Set 1 while Calib State Change Event Happens When DDR#1 Calib
Fail.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_i_sticky0_Ddr41Calib_Mask                                                                   cBit0
#define cAf6_i_sticky0_Ddr41Calib_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Raw PRSB test for OC192_OC48_OC12_10G_1G_100FX_16ch Serdes Status
Reg Addr   : 0x51
Reg Formula: 
    Where  : 
Reg Desc   : 
Top Interface Alarm Sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_i_sticky1_Base                                                                            0x51

/*--------------------------------------
BitField Name: prbs_sta
BitField Type: W1C
BitField Desc: Per bit Set 1 while PRBS Monitor not syn Event Happens(per port
per bit).
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_i_sticky1_prbs_sta_Mask                                                                  cBit15_0
#define cAf6_i_sticky1_prbs_sta_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Clock Monitoring Status
Reg Addr   : 0x46000 - 0x4601F
Reg Formula: 0x46000 + port_id
    Where  : 
           + $port_id(0-31
Reg Desc   : 

0x1f : mate_txclk[7]        (155.52 Mhz)
0x1e : mate_txclk[6]        (155.52 Mhz)
0x1d : mate_txclk[5]        (155.52 Mhz)
0x1c : mate_txclk[4]        (155.52 Mhz)
0x1b : mate_txclk[3]        (155.52 Mhz)
0x1a : mate_txclk[2]        (155.52 Mhz)
0x19 : mate_txclk[1]        (155.52 Mhz)
0x18 : mate_txclk[0]        (155.52 Mhz)
0x17 : mate_rxclk[7]        (155.52 Mhz)
0x16 : mate_rxclk[6]        (155.52 Mhz)
0x15 : mate_rxclk[5]        (155.52 Mhz)
0x14 : mate_rxclk[4]        (155.52 Mhz)
0x13 : mate_rxclk[3]        (155.52 Mhz)
0x12 : mate_rxclk[2]        (155.52 Mhz)
0x11 : mate_rxclk[1]        (155.52 Mhz)
0x10 : mate_rxclk[0]        (155.52 Mhz)
0x0f : unused               (0 Mhz)
0x0e : ext_ref clock        (19.44 Mhz)
0x0d : prc_ref clock        (19.44 Mhz)
0x0c : spgmii_clk           (125 Mhz)
0x0b : kbgmii_clk           (125 Mhz)
0x0a : dccgmii_clk          (125 Mhz)
0x09 : eth_40g_1_rxclk      (312.5 Mhz)
0x08 : eth_40g_0_rxclk      (312.5 Mhz)
0x07 : eth_40g_1_rxclk      (312.5 Mhz)
0x06 : eth_40g_0_rxclk      (312.5 Mhz)
0x05 : DDR4#4 user clock    (300 Mhz)
0x04 : DDR4#3 user clock    (300 Mhz)
0x03 : DDR4#2 user clock    (300 Mhz)
0x02 : DDR4#1 user clock    (300 Mhz)
0x01 : qdr1_clk user clock  (250 Mhz)
0x00 : pcie_upclk clock     (62.5 Mhz)

------------------------------------------------------------------------------*/
#define cAf6Reg_clock_mon_high_Base                                                                    0x46000

/*--------------------------------------
BitField Name: i_statusx
BitField Type: RO
BitField Desc: Clock Value Monitor Change from hex format to DEC format to get
Clock Value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_clock_mon_high_i_statusx_Mask                                                            cBit31_0
#define cAf6_clock_mon_high_i_statusx_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Clock Monitoring Status
Reg Addr   : 0x43000 - 0x4301F
Reg Formula: 0x43000 + port_id
    Where  : 
           + $port_id(0-31
Reg Desc   : 

0x1f : serdes_multi_rate_txclk[15] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x1e : serdes_multi_rate_txclk[14] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x1d : serdes_multi_rate_txclk[13] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x1c : serdes_multi_rate_txclk[12] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x1b : serdes_multi_rate_txclk[11] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x1a : serdes_multi_rate_txclk[10] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x19 : serdes_multi_rate_txclk[9]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x18 : serdes_multi_rate_txclk[8]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x17 : serdes_multi_rate_txclk[7]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x16 : serdes_multi_rate_txclk[6]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x15 : serdes_multi_rate_txclk[5]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x14 : serdes_multi_rate_txclk[4]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x13 : serdes_multi_rate_txclk[3]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x12 : serdes_multi_rate_txclk[2]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x11 : serdes_multi_rate_txclk[1]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x10 : serdes_multi_rate_txclk[0]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x0f : serdes_multi_rate_rxclk[15] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x0e : serdes_multi_rate_rxclk[14] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x0d : serdes_multi_rate_rxclk[13] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x0c : serdes_multi_rate_rxclk[12] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x0b : serdes_multi_rate_rxclk[11] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x0a : serdes_multi_rate_rxclk[10] (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x09 : serdes_multi_rate_rxclk[9]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x08 : serdes_multi_rate_rxclk[8]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x07 : serdes_multi_rate_rxclk[7]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x06 : serdes_multi_rate_rxclk[6]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x05 : serdes_multi_rate_rxclk[5]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x04 : serdes_multi_rate_rxclk[4]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x03 : serdes_multi_rate_rxclk[3]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x02 : serdes_multi_rate_rxclk[2]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x01 : serdes_multi_rate_rxclk[1]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)
0x00 : serdes_multi_rate_rxclk[0]  (OC192+OC48=155.52 Mhz/OC12=38.88Mhz/XFI=156.25Mhz/1G+100FX=125Mhz)

------------------------------------------------------------------------------*/
#define cAf6Reg_clock_mon_highclock_mon_multi_Base                                                     0x43000

/*--------------------------------------
BitField Name: i_statusx
BitField Type: RO
BitField Desc: Clock Value Monitor Change from hex format to DEC format to get
Clock Value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_clock_mon_highclock_mon_multi_i_statusx_Mask                                             cBit31_0
#define cAf6_clock_mon_highclock_mon_multi_i_statusx_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Clock Monitoring Status
Reg Addr   : 0x47000 - 0x4701f
Reg Formula: 0x47000 + port_id
    Where  : 
           + $port_id(0-31
Reg Desc   : 

0x1f : unused (0 Mhz)
0x1e : unused (0 Mhz)
0x1d : pm_tick              (1 Hz) for get value frequency SW must use : F = 155520000/register value
0x1c : one_pps              (1 Hz) for get value frequency SW must use : F = 155520000/register value
0x1b : unused (0 Mhz)
0x1a : unused (0 Mhz)
0x19 : unused (0 Mhz)
0x18 : unused (0 Mhz)
0x17 : unused (0 Mhz)
0x16 : unused (0 Mhz)
0x15 : refout8k_2 (0 Mhz)
0x14 : refout8k_1 (0 Mhz)
0x13 : fsm_pcp[2]            (1 Mhz or 2 Mhz  frequency clk Diagnostic)
0x12 : fsm_pcp[1]            (1 Mhz or 2 Mhz  frequency clk Diagnostic)
0x11 : fsm_pcp[0]            (1 Mhz or 2 Mhz  frequency clk Diagnostic)
0x10 : spare_gpio[13]       (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x0f : spare_gpio[12]       (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x0e : spare_gpio[11]       (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x0d : spare_gpio[10]       (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x0c : spare_gpio[9]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x0b : spare_gpio[8]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x0a : spare_gpio[7]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x09 : spare_gpio[6]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x08 : spare_gpio[5]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x07 : spare_gpio[4]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x06 : spare_gpio[3]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x05 : spare_gpio[2]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x04 : spare_gpio[1]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x03 : spare_gpio[0]        (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x02 : spare_clk[3]         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x01 : spare_clk[2]         (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)
0x00 : spare_clk0 clock     (1 Mhz or 2 Mhz  frequency clk Diagnostic for Spare PIN)

------------------------------------------------------------------------------*/
#define cAf6Reg_clock_mon_slow_Base                                                                    0x47000

/*--------------------------------------
BitField Name: i_statusx
BitField Type: RO
BitField Desc: Clock Value Monitor Change from hex format to DEC format to get
Clock Value
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_clock_mon_slow_i_statusx_Mask                                                            cBit31_0
#define cAf6_clock_mon_slow_i_statusx_Shift                                                                  0

/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Status
Reg Addr   : 0x00_0002
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicate global interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_interrupt_status                                                               0x000002

/*--------------------------------------
BitField Name: Mac40gIntStatus
BitField Type: RO
BitField Desc: Two Mac40G Interrupt Status  1: Set  0: Clear
BitField Bits: [18:17]
--------------------------------------*/
#define cAf6_global_interrupt_status_Mac40gIntStatus_Mask                                            cBit18_17
#define cAf6_global_interrupt_status_Mac40gIntStatus_Shift                                                  17

/*--------------------------------------
BitField Name: UpsrIntStatus
BitField Type: RO
BitField Desc: UPSR Interrupt Status  1: Set  0: Clear
BitField Bits: [16]
--------------------------------------*/
#define cAf6_global_interrupt_status_UpsrIntStatus_Mask                                                 cBit16
#define cAf6_global_interrupt_status_UpsrIntStatus_Shift                                                    16

/*--------------------------------------
BitField Name: TenGeIntStatus
BitField Type: RO
BitField Desc: Chip SEM Interrupt Status  1: Set  0: Clear
BitField Bits: [15]
--------------------------------------*/
#define cAf6_global_interrupt_status_TenGeIntStatus_Mask                                                cBit15
#define cAf6_global_interrupt_status_TenGeIntStatus_Shift                                                   15

/*--------------------------------------
BitField Name: GeIntStatus
BitField Type: RO
BitField Desc: Chip SEM Interrupt Status  1: Set  0: Clear
BitField Bits: [14]
--------------------------------------*/
#define cAf6_global_interrupt_status_GeIntStatus_Mask                                                   cBit14
#define cAf6_global_interrupt_status_GeIntStatus_Shift                                                      14

/*--------------------------------------
BitField Name: GlobalPmc
BitField Type: RO
BitField Desc: Global PMC 2 Page Interrupt Status  1: Set  0: Clear
BitField Bits: [13]
--------------------------------------*/
#define cAf6_global_interrupt_status_GlobalPmc_Mask                                                     cBit13
#define cAf6_global_interrupt_status_GlobalPmc_Shift                                                        13

/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Mask Enable
Reg Addr   : 0x00_0003
Reg Formula: 
    Where  : 
Reg Desc   : 
This register configures global interrupt mask enable.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_interrupt_mask_enable                                                          0x000003
#define cAf6Reg_global_interrupt_mask_enable_WidthVal                                                       32

/*--------------------------------------
BitField Name: Mac40gIntEnable
BitField Type: RO
BitField Desc: Two MAC40G Interrupt Enable  1: Set  0: Clear
BitField Bits: [18:17]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_Mac40gIntEnable_Mask                                       cBit18_17
#define cAf6_global_interrupt_mask_enable_Mac40gIntEnable_Shift                                             17

/*--------------------------------------
BitField Name: UpsrIntEnable
BitField Type: RO
BitField Desc: UPSR Interrupt Enable  1: Set  0: Clear
BitField Bits: [16]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_UpsrIntEnable_Mask                                            cBit16
#define cAf6_global_interrupt_mask_enable_UpsrIntEnable_Shift                                               16

/*--------------------------------------
BitField Name: TenGeIntEnable
BitField Type: RO
BitField Desc: Ten Ge Interrupt Enable  1: Set  0: Clear
BitField Bits: [15]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_TenGeIntEnable_Mask                                           cBit15
#define cAf6_global_interrupt_mask_enable_TenGeIntEnable_Shift                                              15

/*--------------------------------------
BitField Name: GeIntEnable
BitField Type: RO
BitField Desc: Ge Interrupt Enable  1: Set  0: Clear
BitField Bits: [14]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_GeIntEnable_Mask                                              cBit14
#define cAf6_global_interrupt_mask_enable_GeIntEnable_Shift                                                 14

/*--------------------------------------
BitField Name: GlbpmcIntEnable
BitField Type: RO
BitField Desc: GLB PMC Interrupt Enable  1: Set  0: Clear
BitField Bits: [13]
--------------------------------------*/
#define cAf6_global_interrupt_mask_enable_GlbpmcIntEnable_Mask                                          cBit13
#define cAf6_global_interrupt_mask_enable_GlbpmcIntEnable_Shift                                             13

/*------------------------------------------------------------------------------
Reg Name   : Global PMC 2page Interrupt Status
Reg Addr   : 0x00_000A
Reg Formula: 
    Where  : 
Reg Desc   : 
This register indicate global low order CDR interrupt status.

------------------------------------------------------------------------------*/
#define cAf6Reg_global_pmc_2page_interrupt_status                                                     0x00000A
#define cAf6Reg_global_pmc_2page_interrupt_status_WidthVal                                                  32

/*--------------------------------------
BitField Name: PmrIntStatus
BitField Type: RO
BitField Desc: Pmr     Interrupt Status  1: Set  0: Clear
BitField Bits: [6]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_PmrIntStatus_Mask                                         cBit6
#define cAf6_global_pmc_2page_interrupt_status_PmrIntStatus_Shift                                            6

/*--------------------------------------
BitField Name: PohpmvtIntStatus
BitField Type: RO
BitField Desc: Pohpmvt Interrupt Status  1: Set  0: Clear
BitField Bits: [5]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_PohpmvtIntStatus_Mask                                     cBit5
#define cAf6_global_pmc_2page_interrupt_status_PohpmvtIntStatus_Shift                                        5

/*--------------------------------------
BitField Name: PohpmstsIntStatus
BitField Type: RO
BitField Desc: Pohpmsts Interrupt Status  1: Set  0: Clear
BitField Bits: [4]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_PohpmstsIntStatus_Mask                                    cBit4
#define cAf6_global_pmc_2page_interrupt_status_PohpmstsIntStatus_Shift                                       4

/*--------------------------------------
BitField Name: PohIntStatus
BitField Type: RO
BitField Desc: Poh    Interrupt Status  1: Set  0: Clear
BitField Bits: [3]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_PohIntStatus_Mask                                         cBit3
#define cAf6_global_pmc_2page_interrupt_status_PohIntStatus_Shift                                            3

/*--------------------------------------
BitField Name: Pdhde1IntStatus
BitField Type: RO
BitField Desc: Pdhde1 Interrupt Status  1: Set  0: Clear
BitField Bits: [2]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_Pdhde1IntStatus_Mask                                      cBit2
#define cAf6_global_pmc_2page_interrupt_status_Pdhde1IntStatus_Shift                                         2

/*--------------------------------------
BitField Name: Pdhde3IntStatus
BitField Type: RO
BitField Desc: Pdhde3 Interrupt Status  1: Set  0: Clear
BitField Bits: [1]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_Pdhde3IntStatus_Mask                                      cBit1
#define cAf6_global_pmc_2page_interrupt_status_Pdhde3IntStatus_Shift                                         1

/*--------------------------------------
BitField Name: PwcntIntStatus
BitField Type: RO
BitField Desc: Pwcnt  Interrupt Status  1: Set  0: Clear
BitField Bits: [0]
--------------------------------------*/
#define cAf6_global_pmc_2page_interrupt_status_PwcntIntStatus_Mask                                       cBit0
#define cAf6_global_pmc_2page_interrupt_status_PwcntIntStatus_Shift                                          0

#endif /* _AF6_REG_AF6CNC0021_RD_TOP_GLB_H_ */

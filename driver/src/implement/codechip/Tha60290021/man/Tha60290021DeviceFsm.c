/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60290021DeviceFsm.c
 *
 * Created Date: Oct 19, 2018
 *
 * Description : 60290021 FSM implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtIpCoreInternal.h"
#include "Tha60290021DeviceInternal.h"
#include "Tha60290021DeviceReg.h"
#include "../physical/Tha60290021SerdesManager.h"
#include "Tha60290021DeviceFsm.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Read(AtDevice self, uint32 address)
    {
    return AtIpCoreRead(AtDeviceIpCoreGet(self, 0), address);
    }

static void Write(AtDevice self, uint32 address, uint32 value)
    {
    AtIpCoreWrite(AtDeviceIpCoreGet(self, 0), address, value);
    }

static eBool HwRoleIsFixed(AtDevice self)
    {
    /* TODO: there is a mismatch between TOP and PLA, they are expecting reversed
     * value. This has to be fixed, otherwise, when device needs to be taken input
     * from external FSM, there will be issue that PW will not send any packets
     * to PSN */
    AtUnused(self);
    return cAtTrue;
    }

static uint32 RoleSw2Hw(AtDevice self, eBool isActive)
    {
    if (HwRoleIsFixed(self))
        return isActive ? 0 : 1;
    return isActive ? 1 : 0;
    }

static eBool RoleHw2Sw(AtDevice self, uint32 hwRole)
    {
    if (HwRoleIsFixed(self))
        return hwRole ? cAtFalse : cAtTrue;

    return hwRole ? cAtTrue : cAtFalse;
    }

static uint32 Control0Address(AtDevice self)
    {
    AtUnused(self);
    return cTopBaseAddress + cAf6Reg_o_control0_Base;
    }

static eAtRet RoleActiveForce(AtDevice self, eBool forced)
    {
    uint32 address = Control0Address(self);
    uint32 regVal;

    regVal = Read(self, address);
    mRegFieldSet(regVal, cAf6_o_control0_fsm_card_pw_tx_isel_, RoleSw2Hw(self, forced));
    Write(self, address, regVal);

    return cAtOk;
    }

static eBool RoleActiveIsForced(AtDevice self)
    {
    uint32 address = Control0Address(self);
    uint32 regVal = Read(self, address);
    return RoleHw2Sw(self, regVal & cAf6_o_control0_fsm_card_pw_tx_isel_Mask);
    }

static eAtRet RoleInputEnable(AtDevice self, eBool enabled)
    {
    uint32 address = Control0Address(self);
    uint32 value = Read(self, address);

    mRegFieldSet(value, cAf6_o_control0_fsm_card_pw_tx_gsel_, enabled ? 1 : 0);
    Write(self, address, value);

    return cAtOk;
    }

static eBool RoleInputIsEnabled(AtDevice self)
    {
    uint32 address = Control0Address(self);
    uint32 value = Read(self, address);
    return (value & cAf6_o_control0_fsm_card_pw_tx_gsel_Mask) ? cAtTrue : cAtFalse;
    }

static eBool TxFsmPinStatus(AtDevice self)
    {
    uint32 address = cTopBaseAddress + cAf6Reg_c_uart_Base;
    uint32 regVal = Read(self, address);
    return (regVal & cAf6_c_uart_fsm_txpw_cur_Mask) ? cAtFalse : cAtTrue;
    }

static eAtDeviceRole RoleInputStatus(AtDevice self)
    {
    return TxFsmPinStatus(self) ? cAtDeviceRoleActive : cAtDeviceRoleStandby;
    }

static eAtRet RxFsmPinEnable(AtDevice self, eBool enabled)
    {
    uint32 address = cTopBaseAddress + cAf6Reg_o_control0_Base;
    uint32 regVal;

    regVal = Read(self, address);
    mRegFieldSet(regVal, cAf6_o_control0_fsm_eth_40g_rx_gsel_, enabled ? 1 : 0);
    Write(self, address, regVal);

    return cAtOk;
    }

static eBool RxFsmPinIsEnabled(AtDevice self)
    {
    uint32 address = cTopBaseAddress + cAf6Reg_o_control0_Base;
    uint32 regVal = Read(self, address);
    return (regVal & cAf6_o_control0_fsm_eth_40g_rx_gsel_Mask) ? cAtTrue : cAtFalse;
    }

static eBool RxFsmPinStatusGet(AtDevice self)
    {
    uint32 address = cTopBaseAddress + cAf6Reg_c_uart_Base;
    uint32 regVal = Read(self, address);
    return (regVal & cAf6_c_uart_fsm_rxmac_cur_Mask) ? cAtFalse : cAtTrue;
    }

static eAtRet RxBackPlaneSerdesSelect(AtDevice self, uint32 serId)
    {
    uint32 regAddr = cTopBaseAddress + cAf6Reg_o_control0_Base;
    uint32 regVal = Read(self, regAddr);
    uint32 localSerdesId = Tha60290021SerdesManagerBackplaneSerdesLocalId(AtDeviceSerdesManagerGet(self), serId);

    mRegFieldSet(regVal, cAf6_o_control0_fsm_eth_40g_rx_gsel_, 0);
    mRegFieldSet(regVal, cAf6_o_control0_fsm_eth_40g_rx_isel_, localSerdesId);
    Write(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 RxBackPlaneSelectedSerdes(AtDevice self)
    {
    uint32 regAddr = cTopBaseAddress + cAf6Reg_o_control0_Base;
    uint32 regVal = Read(self, regAddr);
    uint32 regField;

    regField = mRegField(regVal, cAf6_o_control0_fsm_eth_40g_rx_isel_);
    return regField + Tha60290021SerdesManagerBackplaneSerdesStartId(AtDeviceSerdesManagerGet(self));
    }

static eAtRet TxBackPlaneSerdesBridgeEnable(AtDevice self, eBool enabled)
    {
    uint32 regAddr = cTopBaseAddress + cAf6Reg_o_control0_Base;
    uint32 regVal = Read(self, regAddr);

    mRegFieldSet(regVal, cAf6_o_control0_eth_40g_tx_dup_, enabled ? 1 : 0);
    Write(self, regAddr, regVal);

    return cAtOk;
    }

eAtRet Tha60290021DeviceRoleActiveForce(AtDevice self, eBool forced)
    {
    return RoleActiveForce(self, forced);
    }

eBool Tha60290021DeviceRoleActiveIsForced(AtDevice self)
    {
    return RoleActiveIsForced(self);
    }

eAtRet Tha60290021DeviceRoleInputEnable(AtDevice self, eBool enabled)
    {
    return RoleInputEnable(self, enabled);
    }

eBool Tha60290021DeviceRoleInputIsEnabled(AtDevice self)
    {
    return RoleInputIsEnabled(self);
    }

eBool Tha60290021DeviceTxFsmPinStatus(AtDevice self)
    {
    return TxFsmPinStatus(self);
    }

eAtDeviceRole Tha60290021DeviceRoleInputStatus(AtDevice self)
    {
    return RoleInputStatus(self);
    }

eAtRet Tha60290021DeviceRxFsmPinEnable(AtDevice self, eBool enabled)
    {
    return RxFsmPinEnable(self, enabled);
    }

eAtRet Tha60290021DeviceTxBackPlaneSerdesBridgeEnable(AtDevice self, eBool enabled)
    {
    return TxBackPlaneSerdesBridgeEnable(self, enabled);
    }

uint32 Tha60290021DeviceRxBackPlaneSelectedSerdes(AtDevice self)
    {
    return RxBackPlaneSelectedSerdes(self);
    }

eAtRet Tha60290021DeviceRxBackPlaneSerdesSelect(AtDevice self, uint32 serId)
    {
    return RxBackPlaneSerdesSelect(self, serId);
    }

eBool Tha60290021DeviceRxFsmPinStatusGet(AtDevice self)
    {
    return RxFsmPinStatusGet(self);
    }

eBool Tha60290021DeviceRxFsmPinIsEnabled(AtDevice self)
    {
    return RxFsmPinIsEnabled(self);
    }

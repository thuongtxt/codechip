/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DEVICE
 *
 * File        : Tha60290021Device.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : 10G CEM product
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesManagerInternal.h"
#include "../../../../generic/sdh/AtSdhLineInternal.h"
#include "../../../default/aps/ThaModuleHardAps.h"
#include "../../../default/physical/ThaThermalSensor.h"
#include "../../../default/physical/ThaPowerSupplySensor.h"
#include "../../../default/sur/hard/ThaModuleHardSurInternal.h"
#include "../../../default/xc/ThaModuleXc.h"
#include "../../../default/pmc/ThaModulePmc.h"
#include "../../Tha60210051/man/Tha60210051DeviceInternal.h"
#include "../../Tha60210012/man/Tha60210012InterruptController.h"
#include "../../Tha60290021/physical/Tha60290021SerdesManager.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "../ram/Tha60290021ModuleRam.h"
#include "../physical/Tha6029Physical.h"
#include "../physical/Tha60290021SerdesManager.h"
#include "../pda/Tha60290021ModulePda.h"
#include "../pwe/Tha60290021ModulePwe.h"
#include "../common/Tha6029CommonDevice.h"
#include "../common/Tha602900xxCommon.h"
#include "../physical/Tha60290021SemController.h"
#include "../encap/Tha6029DccHdlcPktGenerator.h"
#include "Tha60290021DeviceInternal.h"
#include "Tha60290021DeviceReg.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290021Device)self)

/*--------------------------- Macros -----------------------------------------*/
/* OCN/ETH Port */
#define cDiagSfpOcnOrEthPortNumbs                16
#define cDiagSfpOcnOrEthDiagEnableMask(lineId)   (cBit16 << (lineId))
#define cDiagSfpOcnOrEthDiagEnableShift(lineId)  (16 + (lineId))

/* MATE Line */
#define cDiagMateLineNumbs                       8
#define cDiagMateLineDiagEnableMask(lineId)      (cBit8 << (lineId))
#define cDiagMateLineDiagEnableShift(lineId)     (8 + (lineId))

/* DDR */
#define cDiagDdrNumbs                            4
#define cDiagDdrDiagEnableMask(ddrId)            (cBit1 << (ddrId))
#define cDiagDdrDiagEnableShift(ddrId)           (1 + (ddrId))

/* QDR */
#define cDiagQdr1DiagEnableMask                   cBit0
#define cDiagQdr1DiagEnableShift                  0

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021DeviceMethods m_methods;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;
static tTha60210011DeviceMethods m_Tha60210011DeviceOverride;
static tTha60210051DeviceMethods m_Tha60210051DeviceOverride;

/* Super implementation */
static const tAtDeviceMethods          *m_AtDeviceMethods          = NULL;
static const tTha60210011DeviceMethods *m_Tha60210011DeviceMethods = NULL;
static const tTha60150011DeviceMethods *m_Tha60150011DeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleXc,
                                                 cAtModuleSdh,
                                                 cAtModuleSur,
                                                 cAtModulePktAnalyzer,
                                                 cAtModuleBer,
                                                 cAtModulePrbs,
                                                 cAtModuleClock,
                                                 cAtModuleAps};

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = moduleId;

    if ((moduleValue == cAtModuleAps) ||
        (moduleValue == cThaModulePmc))
        return cAtTrue;

    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    /* Public modules */
    if (moduleId  == cAtModuleSdh)         return (AtModule)Tha60290021ModuleSdhNew(self);
    if (moduleId  == cAtModuleXc)          return (AtModule)Tha60290021ModuleXcNew(self);
    if (moduleId  == cAtModulePw)          return (AtModule)Tha60290021ModulePwNew(self);
    if (moduleId  == cAtModuleAps)         return (AtModule)Tha6029ModuleApsNew(self);
    if (moduleId  == cAtModuleEth)         return (AtModule)Tha60290021ModuleEthNew(self);
    if (moduleId  == cAtModulePrbs)        return (AtModule)Tha60290021ModulePrbsNew(self);
    if (moduleId  == cAtModulePdh)         return (AtModule)Tha60290021ModulePdhNew(self);
    if (moduleId  == cAtModuleBer)         return (AtModule)Tha60290021ModuleBerNew(self);
    if (moduleId  == cAtModuleRam)         return (AtModule)Tha60290021ModuleRamNew(self);
    if (moduleId  == cAtModuleSur)         return (AtModule)Tha60290021ModuleSurNew(self);
    if (moduleId  == cAtModuleClock)       return (AtModule)Tha60290021ModuleClockNew(self);
    if (moduleId  == cAtModulePktAnalyzer) return (AtModule)Tha60290021ModulePktAnalyzerNew(self);

    /* Physical modules */
    if (phyModule == cThaModulePoh)    return Tha60290021ModulePohNew(self);
    if (phyModule == cThaModuleOcn)    return Tha60290021ModuleOcnNew(self);
    if (phyModule == cThaModuleCla)    return Tha60290021ModuleClaNew(self);
    if (phyModule == cThaModulePda)    return Tha60290021ModulePdaNew(self);
    if (phyModule == cThaModulePwe)    return Tha60290021ModulePweNew(self);
    if (phyModule == cThaModuleCdr)    return Tha60290021ModuleCdrNew(self);
    if (phyModule == cThaModulePmc)    return Tha60290021ModulePmcNew(self);

	/* Use super devices methods */
    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static  AtSerdesManager SerdesManagerObjectCreate(AtDevice self)
    {
    return Tha60290021SerdesManagerNew(self);
    }

static eBool ShouldCheckRegisterAccess(AtDevice self)
    {
    /* Turn this to True to debug violate accessing */
    AtUnused(self);
    return cAtFalse;
    }

static eBool SemUartIsSupported(Tha60150011Device self)
    {
    /* This must be supported from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static uint32 RegisterWithLocalAddress(AtDevice self, uint32 localAddress)
    {
    AtUnused(self);
    return localAddress + cTopBaseAddress;
    }

static eAtRet AllDdrsResetDisable(AtDevice self)
    {
    uint32 regAddr = RegisterWithLocalAddress(self, cAf6Reg_o_reset_ctr_Base);
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    AtIpCoreWrite(core, regAddr, 0);
    return cAtOk;
    }

static eAtRet ResetStatusCheck(Tha60150011Device self)
    {
    AllDdrsResetDisable((AtDevice)self);
    return m_Tha60150011DeviceMethods->ResetStatusCheck(self);
    }

static eAtRet HwDiagnosticModeEnable(AtDevice self, eBool enable)
    {
    uint32 i;
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    uint32 regAddr = RegisterWithLocalAddress(self, cAf6Reg_o_control3_Base);
    uint32 regVal = AtIpCoreRead(core, regAddr);
    uint8 diag = enable ? 1 : 0;
    uint32 enableMask;
    uint32 enableShift;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(self);
    AtModuleRam ramModule = (AtModuleRam)AtDeviceModuleGet(self, cAtModuleRam);

    if (Tha60290021DeviceIsRunningOnOtherPlatform(self))
        return m_Tha60210011DeviceMethods->HwDiagnosticModeEnable(self, enable);

    /* SFP OCN or ETH PORT */
    for (i = 0; i < Tha60290021SerdesManagerNumFaceplateSerdes(serdesManager); i++)
        {
        enableMask  = cDiagSfpOcnOrEthDiagEnableMask(i);
        enableShift = cDiagSfpOcnOrEthDiagEnableShift(i);
        mRegFieldSet(regVal, enable, diag);
        }

    /* MATE Line */
    for (i = 0; i < Tha60290021SerdesManagerNumMateSerdes(serdesManager); i++)
        {
        enableMask  = cDiagMateLineDiagEnableMask(i);
        enableShift = cDiagMateLineDiagEnableShift(i);
        mRegFieldSet(regVal, enable, diag);
        }

    /* DDR */
    for (i = 0; i < AtModuleRamNumDdrGet(ramModule); i++)
        {
        enableMask  = cDiagDdrDiagEnableMask(i);
        enableShift = cDiagDdrDiagEnableShift(i);
        mRegFieldSet(regVal, enable, diag);
        }

    mRegFieldSet(regVal, cDiagQdr1DiagEnable, diag);
    AtIpCoreWrite(core, regAddr, regVal);

    Tha60290021ModuleEthSgmiiDiagnosticModeEnable((AtModuleEth)AtDeviceModuleGet(self, cAtModuleEth), enable);
    AllDdrsResetDisable(self);

    return cAtOk;
    }

static uint32 StickyAddress(AtDevice self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_i_sticky0_Base);
    }

static eAtRet QdrDiagnostic(AtDevice self)
    {
    static const uint32 cTimeoutMs = 5000;
    uint32 masks = cAf6_i_sticky0_QdrCalib_Mask;
    uint32 regAddr = StickyAddress(self);
    eBool isGood;

    if (Tha60290021DeviceIsRunningOnOtherPlatform(self))
        return m_Tha60150011DeviceMethods->QdrDiagnostic(self);

    isGood = ThaDeviceStickyIsGood(self, regAddr, masks, cTimeoutMs);
    if (isGood)
        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s: QDR calib done\r\n", AtFunction);
    else
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: QDR calib fail\r\n", AtFunction);

    return isGood ? cAtOk : cAtErrorQdrCalibFail;
    }

static eBool DdrCalibIsGood(AtDevice self)
    {
    static const uint32 cTimeoutMs = 5000;
    uint32 regVal;
    uint32 masks = cAf6_i_sticky0_Ddr44Calib_Mask | cAf6_i_sticky0_Ddr43Calib_Mask| cAf6_i_sticky0_Ddr42Calib_Mask| cAf6_i_sticky0_Ddr41Calib_Mask;
    eBool isGood;
    AtIpCore core = AtDeviceIpCoreGet(self, 0);
    tAtOsalCurTime startTime, stopTime;
    uint32 regAddr = StickyAddress(self);

    if (!AtDeviceDiagnosticCheckIsEnabled(self))
        return cAtTrue;

    if (Tha60290021DeviceIsRunningOnOtherPlatform(self))
        return m_Tha60150011DeviceMethods->DdrCalibIsGood(self);

    AtOsalCurTimeGet(&startTime);
    isGood = ThaDeviceStickyIsGood(self, regAddr, masks, cTimeoutMs);
    AtOsalCurTimeGet(&stopTime);

    if (isGood)
        {
        AtDeviceLog(self, cAtLogLevelInfo, AtSourceLocation, "%s, All DDRs calib done\r\n", AtFunction);
        return isGood;
        }

    regVal = AtIpCoreRead(core, regAddr);

    if (regVal & cAf6_i_sticky0_Ddr41Calib_Mask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#1 calib fail\r\n", AtFunction);
    if (regVal & cAf6_i_sticky0_Ddr42Calib_Mask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#2 calib fail\r\n", AtFunction);
    if (regVal & cAf6_i_sticky0_Ddr43Calib_Mask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#3 calib fail\r\n", AtFunction);
    if (regVal & cAf6_i_sticky0_Ddr44Calib_Mask)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "%s: DDR#4 calib fail\r\n", AtFunction);

    return isGood;
    }

static const char* NameBuild(const char* name, uint32 id)
    {
    static char bufferName[64];
    AtSprintf(bufferName, "%s_%u", name, (id + 1));
    return bufferName;
    }

static uint32 ExpectedFrequencyOf100FxSerdes(Tha60290021Device self)
    {
    AtUnused(self);
    return 62500000;
    }

static uint32 FaceplateFreqExpectedValue(AtDevice self, uint32 serdesId)
    {
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(self);
    AtSerdesController controller = AtSerdesManagerSerdesControllerGet(serdesManager, serdesId);
    eTha6029FaceplateSerdesHwMode hwMode = Tha6029FaceplateSerdesControllerHwModeGet(controller);

    switch ((uint32)hwMode)
        {
        /* OCN */
        case cTha6029FaceplateSerdesHwModeOc12 : return 38880000;
        case cTha6029FaceplateSerdesHwModeOc48 : return 155520000;
        case cTha6029FaceplateSerdesHwModeOc192: return 155520000;

        /* ETH */
        case cTha6029FaceplateSerdesHwMode100Fx: return mMethodsGet(mThis(self))->ExpectedFrequencyOf100FxSerdes(mThis(self));
        case cTha6029FaceplateSerdesHwMode1G   : return 62500000;
        case cTha6029FaceplateSerdesHwMode10G  : return 156250000;
        default:
            return 0;
        }
    }

static uint32 ClockMonitorRegister(AtDevice self, uint32 portId)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_clock_mon_high_Base + portId);
    }

static void MateClockStatusDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* MATE clock:\r\n");
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_txclk_7" : "  - mate_txclk_7", ClockMonitorRegister(self, 0x1f), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_rxclk_7" : "  - mate_rxclk_7", ClockMonitorRegister(self, 0x17), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_txclk_6" : "  - mate_txclk_6", ClockMonitorRegister(self, 0x1e), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_rxclk_6" : "  - mate_rxclk_6", ClockMonitorRegister(self, 0x16), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_txclk_5" : "  - mate_txclk_5", ClockMonitorRegister(self, 0x1d), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_rxclk_5" : "  - mate_rxclk_5", ClockMonitorRegister(self, 0x15), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_txclk_4" : "  - mate_txclk_4", ClockMonitorRegister(self, 0x1c), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_rxclk_4" : "  - mate_rxclk_4", ClockMonitorRegister(self, 0x14), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_txclk_3" : "  - mate_txclk_3", ClockMonitorRegister(self, 0x1b), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_rxclk_3" : "  - mate_rxclk_3", ClockMonitorRegister(self, 0x13), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_txclk_2" : "  - mate_txclk_2", ClockMonitorRegister(self, 0x1a), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_rxclk_2" : "  - mate_rxclk_2", ClockMonitorRegister(self, 0x12), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_txclk_1" : "  - mate_txclk_1", ClockMonitorRegister(self, 0x19), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_rxclk_1" : "  - mate_rxclk_1", ClockMonitorRegister(self, 0x11), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_txclk_0" : "  - mate_txclk_0", ClockMonitorRegister(self, 0x18), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "mate_rxclk_0" : "  - mate_rxclk_0", ClockMonitorRegister(self, 0x10), 155520000, 5);

    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void SgmiiClockStatusDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* SGMII clocks:\r\n");
    ThaDeviceClockResultPrint(self, debugger, debugger ? "spare  sgmii_clk" : "  - spare sgmii_clk", ClockMonitorRegister(self, 0x0c), 125000000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "k-byte sgmii_clk" : "  - k-byte sgmii_clk", ClockMonitorRegister(self, 0x0b), 125000000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "dcc    sgmii_clk" : "  - dcc sgmii_clk", ClockMonitorRegister(self, 0x0a), 125000000, 5);
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void BackplaneClockStatusDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* Backplane 40G clocks:\r\n");
    ThaDeviceClockResultPrint(self, debugger, debugger ? "eth_40g_1_txclk" : "  - eth_40g_1_txclk", ClockMonitorRegister(self, 0x09), 312500000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "eth_40g_1_rxclk" : "  - eth_40g_1_rxclk", ClockMonitorRegister(self, 0x08), 312500000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "eth_40g_0_txclk" : "  - eth_40g_0_txclk", ClockMonitorRegister(self, 0x07), 312500000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "eth_40g_0_rxclk" : "  - eth_40g_0_rxclk", ClockMonitorRegister(self, 0x06), 312500000, 5);
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void DdrQdrClockStatusDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* QDR/DDR clocks:\r\n");
    ThaDeviceClockResultPrint(self, debugger, debugger ? "DDR4#4 user clock  " : "  - DDR4#4 user clock", ClockMonitorRegister(self, 0x05), 300000000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "DDR4#3 user clock  " : "  - DDR4#3 user clock", ClockMonitorRegister(self, 0x04), 300000000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "DDR4#2 user clock  " : "  - DDR4#2 user clock", ClockMonitorRegister(self, 0x03), 300000000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "DDR4#1 user clock  " : "  - DDR4#1 user clock", ClockMonitorRegister(self, 0x02), 300000000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "qdr1_clk user clock" : "  - qdr1_clk user clock", ClockMonitorRegister(self, 0x01), 250000000, 5);
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void RefClockStatusDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* Ref clocks:\r\n");
    ThaDeviceClockResultPrint(self, debugger, debugger ? "ext_ref clock   " : "  - ext_ref clock", ClockMonitorRegister(self, 0x0e), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "prc_ref clock   " : "  - prc_ref clock", ClockMonitorRegister(self, 0x0d), 155520000, 5);
    ThaDeviceClockResultPrint(self, debugger, debugger ? "pcie_upclk clock" : "  - pcie_upclk clock", ClockMonitorRegister(self, 0x00),  62500000, 10);
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static float TickFrequencyAgainstExpectedClockGet(AtDevice self, uint32 regAddr, uint32 expectedFreq)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal = AtHalRead(hal, regAddr);
    float retVal = 0;

    if (regVal == 0)
        return (float)cInvalidUint32;

    retVal = (float)((float)expectedFreq / (float)regVal);
    return retVal;
    }

static float TickFrequencyGet(AtDevice self, uint32 regAddr)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal = AtHalRead(hal, regAddr);
    if (regVal == 0)
        return (float)cInvalidUint32;

    return (float)((float)155520000 / (float)regVal);
    }

static char *Buffer(AtDebugger debugger, uint32 *bufferSize)
    {
    if (debugger)
        return AtDebuggerCharBuffer(debugger, bufferSize);
    return AtSharedDriverSharedBuffer(bufferSize);
    }

static void TickClockStatusDisplayAgainstExpectedClock(AtDevice self, uint32 regAddr, uint32 expectedFreq, const char *name)
    {
    uint32 bufferSize;
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    float frequency = TickFrequencyAgainstExpectedClockGet(self, regAddr, expectedFreq);
    char *buffer = Buffer(debugger, &bufferSize);
    eAtSevLevel severity = (frequency >= (float)cInvalidUint32) ? cSevCritical : cSevNormal;

    AtSnprintf(buffer, bufferSize, (frequency >= (float)cInvalidUint32) ? "None" : "%0.3f (Hz)", frequency);
    if (debugger)
        {
        AtDebuggerEntryAdd(debugger, AtDebugEntryNew(severity, name, buffer));
        return;
        }
    AtPrintc(cSevNormal  , "%-25s: ", name);
    AtPrintc(severity, "%s\r\n", buffer);
    }

static void TickClockStatusDisplay(AtDevice self, uint32 regAddr, const char *name)
    {
    uint32 bufferSize;
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    float frequency = TickFrequencyGet(self, regAddr);
    char *buffer = Buffer(debugger, &bufferSize);
    eAtSevLevel severity = (frequency >= (float)cInvalidUint32) ? cSevCritical : cSevNormal;

    AtSnprintf(buffer, bufferSize, (frequency >= (float)cInvalidUint32) ? "None" : "%0.3f (Hz)", frequency);
    if (debugger)
        {
        AtDebuggerEntryAdd(debugger, AtDebugEntryNew(severity, name, buffer));
        return;
        }
    AtPrintc(cSevNormal  , "%-25s: ", name);
    AtPrintc(severity, "%s\r\n", buffer);
    }

static uint32 SlowClockMonitorRegister(AtDevice self, uint32 portId)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_clock_mon_slow_Base + portId);
    }

static uint32 Read(AtDevice self, uint32 address)
    {
    return AtIpCoreRead(AtDeviceIpCoreGet(self, 0), address);
    }

static void PinsDiagnosticFrequencyDisplay(AtDevice self, uint32 regAddr, const char *name)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    uint32 regVal = Read(self, regAddr);
    char freqStr[32] = {0};

    AtSprintf(freqStr, "%7.3f",  (double)(regVal / 1000));
    if (debugger)
        {
        AtDebuggerEntryAdd(debugger, AtDebugEntryNew(cSevNormal, name, freqStr));
        return;
        }
    AtPrintc(cSevNormal, "%-25s: %11s (KHz)\r\n", name, freqStr);
    }

static uint32 PmTickRegister(AtDevice self)
    {
    return SlowClockMonitorRegister(self, 0x1d);
    }

static uint32 OnePpsTickRegister(AtDevice self)
    {
    return SlowClockMonitorRegister(self, 0x1c);
    }

static eBool ShouldCheckInputClocks(AtDevice self)
    {
    if (AtDeviceIsSimulated(self) ||
        Tha60290021DeviceIsRunningOnOtherPlatform(self) ||
        AtDeviceWarmRestoreIsStarted(self))
        return cAtFalse;
    return cAtTrue;
    }

static void InputTicksCheck(AtDevice self)
    {
    if (TickFrequencyGet(self, PmTickRegister(self)) >= (float)cInvalidUint32)
        AtDeviceLog(self, cAtLogLevelCritical, AtSourceLocation, "1Hz PM tick is not input, counters will not work\n");
    }

static void SlowClockStatusDisplayAgainstExpectedClock(AtDevice self, uint32 expectedClockFreq)
    {
    AtPrintc(cSevInfo, "* Ticks:\r\n");
    TickClockStatusDisplayAgainstExpectedClock(self, PmTickRegister(self), expectedClockFreq, "  - pm_tick");
    TickClockStatusDisplayAgainstExpectedClock(self, OnePpsTickRegister(self), expectedClockFreq, "  - one_pps");
    AtPrintf("\r\n");
    }

static void SlowClockStatusDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* Ticks:\r\n");
    TickClockStatusDisplay(self, PmTickRegister(self), debugger ? "pm_tick" : " - pm_tick");
    TickClockStatusDisplay(self, OnePpsTickRegister(self), debugger ? "one_pps" : " - one_pps");
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static uint32 NumFsmInputs(AtDevice self)
    {
    AtUnused(self);
    return 3;
    }

static uint32 FsmInputRegister(AtDevice self, uint32 fsmIndex)
    {
    return SlowClockMonitorRegister(self, 0x11 + fsmIndex);
    }

static void FsmInputCheck(AtDevice self)
    {
    uint32 fsm_i;

    for (fsm_i = 0; fsm_i < NumFsmInputs(self); fsm_i++)
        {
        if (Read(self, FsmInputRegister(self, fsm_i)) == 0)
            AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation, "FSM#%d is not input and that may make equipment protection do not work\r\n", fsm_i + 1);
        }
    }

static void FsmFrequencyDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* FSM PCP Pins:\r\n");
    PinsDiagnosticFrequencyDisplay(self, FsmInputRegister(self, 0), debugger ? "fsm_pcp_0" : "  - fsm_pcp_0");
    PinsDiagnosticFrequencyDisplay(self, FsmInputRegister(self, 1), debugger ? "fsm_pcp_1" : "  - fsm_pcp_1");
    PinsDiagnosticFrequencyDisplay(self, FsmInputRegister(self, 2), debugger ? "fsm_pcp_2" : "  - fsm_pcp_2");
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void SpareGpioFrequencyDisplay(AtDevice self, uint8 maxPins)
    {
    uint8 pin_i;
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* Spare GPIO Pins:\r\n");
    for (pin_i = 0; pin_i < maxPins; pin_i++)
        {
        char buf[32];
        AtSprintf(buf, debugger ? "spare_gpio_%u" : "  - spare_gpio_%u", pin_i);
        PinsDiagnosticFrequencyDisplay(self, SlowClockMonitorRegister(self, (uint32)(0x03 + pin_i)), buf);
        }
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void SpareFrequencyDisplay(AtDevice self)
    {
    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* Spare Pins:\r\n");
    PinsDiagnosticFrequencyDisplay(self, SlowClockMonitorRegister(self, 0x0), debugger ? "spare_clk_0" : "  - spare_clk_0");
    PinsDiagnosticFrequencyDisplay(self, SlowClockMonitorRegister(self, 0x1), debugger ? "spare_clk_2" : "  - spare_clk_2");
    PinsDiagnosticFrequencyDisplay(self, SlowClockMonitorRegister(self, 0x2), debugger ? "spare_clk_3" : "  - spare_clk_3");
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static eBool Refout8kCanDisplay(Tha60290021Device self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x7, 0x00);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader((AtDevice)self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 Refout8kStatusCounterRegAddress(AtDevice self, uint8 clockId)
    {
    uint32 address = SlowClockMonitorRegister(self, (uint32)(0x14 + clockId));
    return address;
    }

static void Refout8kDisplay(AtDevice self)
    {
    uint8 numPins = 2;
    uint8 pin_i;

    AtDebugger debugger = AtDeviceDebuggerGet(self);
    if (!mMethodsGet(mThis(self))->Refout8kCanDisplay(mThis(self)))
        return;

    mNoneDebuggerPrint(cSevInfo, "* refout8k:\r\n");
    for (pin_i = 0; pin_i < numPins; pin_i++)
        {
        char buf[32];
        if (debugger)
            AtSprintf(buf, "refout8k_%u", pin_i + 1);
        else
            AtSprintf(buf, "  - refout8k_%u", pin_i + 1);
        PinsDiagnosticFrequencyDisplay(self, Refout8kStatusCounterRegAddress(self, pin_i), buf);
        }
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static uint32 Refout8kStatusCounter(AtDevice self, uint8 clockId)
    {
    uint32 address = Refout8kStatusCounterRegAddress(self, clockId);
    uint32 regVal = Read(self, address);
    return regVal;
    }

static void MultiRateClockStatusDisplay(AtDevice self)
    {
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(self);
    uint32 serdes_i;

    AtDebugger debugger = AtDeviceDebuggerGet(self);
    mNoneDebuggerPrint(cSevInfo, "* SERDES Multi-rate clocks:\r\n");
    for (serdes_i = 0; serdes_i < Tha60290021SerdesManagerNumFaceplateSerdes(serdesManager); serdes_i++)
        {
        uint32 freqExpected;
        uint32 regAddr;
        uint32 serdesId = (uint32)Tha60290021SerdesManagerFaceplateSerdesStartId(serdesManager) + serdes_i;

        if (!AtSerdesManagerSerdesIsControllable(serdesManager, serdesId))
            continue;

        freqExpected = FaceplateFreqExpectedValue(self, serdes_i);
        regAddr = RegisterWithLocalAddress(self, cAf6Reg_clock_mon_highclock_mon_multi_Base + serdes_i);
        ThaDeviceClockResultPrint(self, debugger, NameBuild(debugger ? "multi_rate_rxclk" : "  - multi_rate_rxclk", serdes_i), regAddr, freqExpected, 5);
        ThaDeviceClockResultPrint(self, debugger, NameBuild(debugger ? "multi_rate_txclk" : "  - multi_rate_txclk", serdes_i), regAddr + 0x10UL, freqExpected, 5);
        }
    mNoneDebuggerPrint(cSevNormal, "\r\n");
    }

static void ClockStatusDisplay(Tha60150011Device self)
    {
    AtDevice device = (AtDevice)self;

    MateClockStatusDisplay(device);
    SgmiiClockStatusDisplay(device);
    BackplaneClockStatusDisplay(device);
    DdrQdrClockStatusDisplay(device);
    RefClockStatusDisplay(device);
    MultiRateClockStatusDisplay(device);
    SlowClockStatusDisplay(device);
    FsmFrequencyDisplay(device);
    SpareGpioFrequencyDisplay(device, 14);
    SpareFrequencyDisplay(device);
    Refout8kDisplay(device);
    }

static AtUart DiagnosticUartObjectCreate(AtDevice self)
    {
    return Tha6029DiagUartNew(self);
    }

static eBool HasRole(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet RoleSet(AtDevice self, eAtDeviceRole role)
    {
    AtModule module = AtDeviceModuleGet(self, cAtModuleEth);
    return AtModuleRoleSet(module, role);
    }

static eAtDeviceRole RoleGet(AtDevice self)
    {
    AtModule module = AtDeviceModuleGet(self, cAtModuleEth);
    return AtModuleRoleGet(module);
    }

static eAtDeviceRole AutoRoleGet(AtDevice self)
    {
    AtModule pweModule = AtDeviceModuleGet(self, cThaModulePwe);
    return AtModuleRoleInputStatus(pweModule);
    }

static eBool PllIsLocked(AtDevice self)
    {
    static const uint32 cTimeoutMs = 5000;
    return ThaDeviceStickyIsGood(self, StickyAddress(self), cAf6_i_sticky0_SystemPll_Mask, cTimeoutMs);
    }

static uint32 NumInterruptPins(AtDevice self)
    {
    if (Tha60290021DeviceIsRunningOnOtherPlatform(self))
        return 0;
    return Tha6029DeviceNumInterruptPins(self);
    }

static AtInterruptPin InterruptPinObjectCreate(AtDevice self, uint32 pinId)
    {
    return Tha6029DeviceInterruptPinObjectCreate(self, pinId);
    }

static eAtTriggerMode InterruptPinDefaultTriggerMode(AtDevice self, AtInterruptPin pin)
    {
    return Tha6029DeviceInterruptPinDefaultTriggerMode(self, pin);
    }

static AtThermalSensor ThermalSensorObjectCreate(AtDevice self)
    {
    return Tha60290021ThermalSensorNew(self);
    }

static eBool ThermalSensorIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtPowerSupplySensor PowerSupplySensorObjectCreate(AtDevice self)
    {
    return Tha60290021PowerSupplySensorNew(self);
    }

static eBool PowerSupplySensorIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 NumUsedOc48Slice(Tha60210011Device self)
    {
    AtUnused(self);
    return 4;
    }

static eAtRet PlatformSetup(AtDevice self)
    {
    AtIpCore core = AtDeviceIpCoreGet(self, 0);

    /* Following registers are all hardware internal, no documentation. But their
     * values are necessary to have this kind of this testing work */
    AtIpCoreWrite(core, 0xF00041, 0x10000033);
    AtIpCoreWrite(core, 0xF00044, 0xFF0000FF);
    AtIpCoreWrite(core, 0xF00045, 0x000000C3);

    return cAtOk;
    }

static void InputClocksCheck(AtDevice self)
    {
    if (!ShouldCheckInputClocks(self))
        return;

    InputTicksCheck(self);
    FsmInputCheck(self);

    /* More checking will be added */
    }

static eAtRet DccPwCleanup(AtDevice self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(self, cAtModuleSdh);
    uint8 numLines = Tha60290021ModuleSdhNumUseableFaceplateLines(sdhModule);
    uint8 line_i;
    eAtRet ret = cAtOk;

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = Tha60290021ModuleSdhFaceplateLineGet(sdhModule, line_i);
        if (line)
            ret |= AtSdhLineDccHdlcCleanup(line);
        }

    return ret;
    }

static eAtRet KBytePwCleanup(AtDevice self)
    {
    /* Will need to put */
    AtUnused(self);
    return cAtOk;
    }

static eAtRet DccKBytePwCleanUp(AtDevice self)
    {
    eAtRet ret = cAtOk;

    ret |= DccPwCleanup(self);
    ret |= KBytePwCleanup(self);

    return ret;
    }

static eAtRet FaceplateSerdesTimingModeInvalidate(AtDevice self)
    {
    AtSerdesManager manager = AtDeviceSerdesManagerGet(self);
    uint32 numSerdes = Tha60290021SerdesManagerNumFaceplateSerdes(manager);
    uint32 serdes_i;
    eAtRet ret = cAtOk;

    for (serdes_i = 0; serdes_i < numSerdes; serdes_i++)
        {
        AtSerdesController serdes = Tha60290021SerdesManagerFacePlateSerdesGet(manager, (uint8)serdes_i);
        if (AtSerdesControllerIsControllable(serdes))
            ret |= AtSerdesControllerTimingModeSet(serdes, cAtSerdesTimingModeLockToRef);
        }

    return ret;
    }

static eAtRet Init(AtDevice self)
    {
    eAtRet ret;

    AtDeviceAllDiagServiceStop(self);
    DccKBytePwCleanUp(self);
    FaceplateSerdesTimingModeInvalidate(self);

    ret = m_AtDeviceMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    InputClocksCheck(self);

    if (Tha60290021DeviceIsRunningOnOtherPlatform(self))
        return PlatformSetup(self);

    mMethodsGet(mThis(self))->DccHdlcPktGenInit(mThis(self));

    return cAtOk;
    }

static uint32 StartVersionSupportSurveillance(ThaDevice self)
    {
    AtUnused(self);
    return 0;
    }

static ThaVersionReader VersionReaderCreate(ThaDevice self)
    {
    return Tha60290021VersionReaderNew((AtDevice)self);
    }

static eBool NeedToDisableJnRequestDdr(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportFullPwCounters(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool HasUpsrEthInterrupt(Tha60290021Device self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x0, 0x0);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader((AtDevice)self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    if (mMethodsGet(mThis(self))->HasUpsrEthInterrupt(mThis(self)))
        return Tha60290021IntrControllerNew(core);

    return Tha60210012IntrControllerNew(core);
    }

static uint32 StartVersionSupportDistinctErdi(AtDevice self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionSupportNewPsn(AtDevice self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionSupportPowerControl(AtDevice self)
    {
    mVersionReset(self);
    }

static eBool RamErrorGeneratorIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ErrorGeneratorIsSupported(Tha60210051Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ShouldCheckVersionInSimulation(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool IsRunningOnOtherPlatform(Tha60290021Device self)
    {
    AtDevice device = (AtDevice)self;
    uint32 platform = AtDevicePlatformGet(device);

    if ((platform == 0x60210011) ||
        (platform == 0x60210051))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet DccHdlcPktGenInit(Tha60290021Device self)
	{
	if (self)
		return Tha6029DccHdlcPktGenInit((AtDevice)self);
	return cAtErrorNullPointer;
	}


static eBool IsRunningOnVu13P(Tha60290021Device self)
    {
    ThaVersionReader versionReader = ThaDeviceVersionReader((AtDevice)self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x0, 0x0);
    return (currentVersion >= startVersion) ? cAtTrue : cAtFalse;
    }

static eBool ClockStateValueV2IsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtSemController SemControllerObjectCreate(AtDevice self, uint32 semId)
    {
    return Tha60290021SemControllerNew(self, semId);
    }

static eBool SemControllerIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 NumSemControllersGet(AtDevice self)
    {
    AtUnused(self);
    return 1;
    }

static eAtRet OcnHwFlush(Tha60210011Device self)
    {
    AtModule ocnModule = AtDeviceModuleGet((AtDevice)self, cThaModuleOcn);
    eAtRet ret = cAtOk;

    ret = m_Tha60210011DeviceMethods->OcnHwFlush(self);

    Tha60290021ModuleOcnHwFlush(ocnModule);

    return ret;
    }

static eBool ShouldRestoreDatabaseOnWarmRestoreStop(AtDevice self)
    {
    return Tha602900xxShouldRestoreDatabaseOnWarmRestoreStop(self);
    }

static eBool ShouldResetSerdesWhenEnablingDiagnosticMode(Tha60210011Device self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static eAtRet AllSerdesReset(Tha60210011Device self)
    {
    return AtSerdesManagerReset(AtDeviceSerdesManagerGet((AtDevice)self));
    }

static eBool ShouldApplyNewReset(Tha60210011Device self)
    {
    /* TODO: As SEM is not ready now */
    AtUnused(self);
    return cAtFalse;
    }

static eBool Accessible(AtDevice self)
    {
    return Tha602900xxDeviceAccessible(self);
    }

static eBool ShouldEnableInterruptAfterProcessing(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 NumFaceplateSerdes(Tha60290021Device self)
    {
    AtUnused(self);
    return 16;
    }

static uint32 NumMateSerdes(Tha60290021Device self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 NumBackplaneSerdes(Tha60290021Device self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 NumSgmiiSerdes(Tha60290021Device self)
    {
    AtUnused(self);
    return 4; /* 1: K-Byte, 1: DCC, 2 spares */
    }

static uint8 FaceplateSerdesStartId(Tha60290021Device self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 FaceplateSerdesEndId(Tha60290021Device self)
    {
    AtUnused(self);
    return 15;
    }

static uint8 MateSerdesStartId(Tha60290021Device self)
    {
    AtUnused(self);
    return 16;
    }

static uint8 MateSerdesEndId(Tha60290021Device self)
    {
    AtUnused(self);
    return 23;
    }

static uint8 BackplaneSerdesStartId(Tha60290021Device self)
    {
    AtUnused(self);
    return 24;
    }

static uint8 BackplaneSerdesEndId(Tha60290021Device self)
    {
    AtUnused(self);
    return 25;
    }

static uint8 SgmiiSerdesStartId(Tha60290021Device self)
    {
    AtUnused(self);
    return 26;
    }

static uint8 SgmiiSerdesEndId(Tha60290021Device self)
    {
    AtUnused(self);
    return 29;
    }

static void SemControllerSerialize(AtDevice object, AtCoder encoder)
    {
    AtUnused(object);
    AtUnused(encoder);
    }

static void OverrideTha60210051Device(AtDevice self)
    {
    Tha60210051Device device = (Tha60210051Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210051DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210051DeviceOverride));

        mMethodOverride(m_Tha60210051DeviceOverride, StartVersionSupportDistinctErdi);
        mMethodOverride(m_Tha60210051DeviceOverride, StartVersionSupportNewPsn);
        mMethodOverride(m_Tha60210051DeviceOverride, StartVersionSupportPowerControl);
        mMethodOverride(m_Tha60210051DeviceOverride, ErrorGeneratorIsSupported);
        }

    mMethodsSet(device, &m_Tha60210051DeviceOverride);
    }

static void OverrideTha60210011Device(AtDevice self)
    {
    Tha60210011Device device = (Tha60210011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210011DeviceOverride));

        mMethodOverride(m_Tha60210011DeviceOverride, HwDiagnosticModeEnable);
        mMethodOverride(m_Tha60210011DeviceOverride, NumUsedOc48Slice);
        mMethodOverride(m_Tha60210011DeviceOverride, NeedToDisableJnRequestDdr);
        mMethodOverride(m_Tha60210011DeviceOverride, StartVersionSupportFullPwCounters);
        mMethodOverride(m_Tha60210011DeviceOverride, RamErrorGeneratorIsSupported);
        mMethodOverride(m_Tha60210011DeviceOverride, OcnHwFlush);
        mMethodOverride(m_Tha60210011DeviceOverride, ShouldResetSerdesWhenEnablingDiagnosticMode);
        mMethodOverride(m_Tha60210011DeviceOverride, AllSerdesReset);
        mMethodOverride(m_Tha60210011DeviceOverride, ShouldApplyNewReset);
        }

    mMethodsSet(device, &m_Tha60210011DeviceOverride);
    }

static void OverrideTha60150011Device(AtDevice self)
    {
    Tha60150011Device device = (Tha60150011Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60150011DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(device), sizeof(m_Tha60150011DeviceOverride));

        mMethodOverride(m_Tha60150011DeviceOverride, SemUartIsSupported);
        mMethodOverride(m_Tha60150011DeviceOverride, QdrDiagnostic);
        mMethodOverride(m_Tha60150011DeviceOverride, DdrCalibIsGood);
        mMethodOverride(m_Tha60150011DeviceOverride, ClockStatusDisplay);
        mMethodOverride(m_Tha60150011DeviceOverride, PllIsLocked);
        mMethodOverride(m_Tha60150011DeviceOverride, ResetStatusCheck);
        }

    mMethodsSet(device, &m_Tha60150011DeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    AtDevice object = (AtDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(m_AtDeviceOverride));

        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, SerdesManagerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, ShouldCheckRegisterAccess);
        mMethodOverride(m_AtDeviceOverride, DiagnosticUartObjectCreate);
        mMethodOverride(m_AtDeviceOverride, HasRole);
        mMethodOverride(m_AtDeviceOverride, RoleSet);
        mMethodOverride(m_AtDeviceOverride, RoleGet);
        mMethodOverride(m_AtDeviceOverride, AutoRoleGet);
        mMethodOverride(m_AtDeviceOverride, NumInterruptPins);
        mMethodOverride(m_AtDeviceOverride, InterruptPinObjectCreate);
        mMethodOverride(m_AtDeviceOverride, InterruptPinDefaultTriggerMode);
        mMethodOverride(m_AtDeviceOverride, ThermalSensorObjectCreate);
        mMethodOverride(m_AtDeviceOverride, ThermalSensorIsSupported);
        mMethodOverride(m_AtDeviceOverride, PowerSupplySensorObjectCreate);
        mMethodOverride(m_AtDeviceOverride, PowerSupplySensorIsSupported);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, ShouldCheckVersionInSimulation);
        mMethodOverride(m_AtDeviceOverride, SemControllerIsSupported);
        mMethodOverride(m_AtDeviceOverride, NumSemControllersGet);
        mMethodOverride(m_AtDeviceOverride, SemControllerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, ShouldRestoreDatabaseOnWarmRestoreStop);
        mMethodOverride(m_AtDeviceOverride, Accessible);
        mMethodOverride(m_AtDeviceOverride, ShouldEnableInterruptAfterProcessing);
        mMethodOverride(m_AtDeviceOverride, SemControllerSerialize);
        }

    mMethodsSet(object, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(tThaDeviceMethods));

        mMethodOverride(m_ThaDeviceOverride, StartVersionSupportSurveillance);
        mMethodOverride(m_ThaDeviceOverride, VersionReaderCreate);
        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        mMethodOverride(m_ThaDeviceOverride, ClockStateValueV2IsSupported);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60150011Device(self);
    OverrideTha60210011Device(self);
    OverrideTha60210051Device(self);
    }

static void MethodsInit(AtDevice self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, IsRunningOnOtherPlatform);
        mMethodOverride(m_methods, Refout8kCanDisplay);
        mMethodOverride(m_methods, HasUpsrEthInterrupt);
        mMethodOverride(m_methods, IsRunningOnVu13P);
        mMethodOverride(m_methods, DccHdlcPktGenInit);
        mMethodOverride(m_methods, ExpectedFrequencyOf100FxSerdes);
        mMethodOverride(m_methods, NumFaceplateSerdes);
        mMethodOverride(m_methods, NumMateSerdes);
        mMethodOverride(m_methods, NumSgmiiSerdes);
        mMethodOverride(m_methods, NumBackplaneSerdes);
        mMethodOverride(m_methods, FaceplateSerdesStartId);
        mMethodOverride(m_methods, FaceplateSerdesEndId);
        mMethodOverride(m_methods, MateSerdesStartId);
        mMethodOverride(m_methods, MateSerdesEndId);
        mMethodOverride(m_methods, SgmiiSerdesStartId);
        mMethodOverride(m_methods, SgmiiSerdesEndId);
        mMethodOverride(m_methods, BackplaneSerdesStartId);
        mMethodOverride(m_methods, BackplaneSerdesEndId);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021Device);
    }

AtDevice Tha60290021DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051DeviceObjectInit((AtDevice)self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60290021DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021DeviceObjectInit(newDevice, driver, productCode);
    }

eBool Tha60290021DeviceIsRunningOnOtherPlatform(AtDevice device)
    {
    if (device)
        return mMethodsGet(mThis(device))->IsRunningOnOtherPlatform(mThis(device));

    return cAtFalse;
    }

void Tha6029DevicePinsDiagnosticFrequencyDisplay(AtDevice self, uint32 regAddr, const char *name)
    {
    if (self)
        PinsDiagnosticFrequencyDisplay(self, regAddr, name);
    }

void Tha6029DeviceTickClockStatusDisplay(AtDevice self, uint32 regAddr, const char *name)
    {
    if (self)
        TickClockStatusDisplay(self, regAddr, name);
    }

void Tha60290021DeviceClockStatusDisplay(AtDevice self)
    {
    if (self)
        ClockStatusDisplay((Tha60150011Device)self);
    }

eBool Tha60290021DeviceIsRunningOnVu13P(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->IsRunningOnVu13P(mThis(self));
    return cAtFalse;
    }

uint32 Tha60290021DeviceTopRegisterWithLocalAddress(AtDevice self, uint32 localAddress)
    {
    if (self)
        return RegisterWithLocalAddress(self, localAddress);
    return cInvalidUint32;
    }

uint32 Tha6029DeviceRefout8kStatusCounter(AtDevice self, uint8 clockId)
    {
    return Refout8kStatusCounter(self, clockId);
    }

eAtRet Tha60290021DeviceFaceplateSerdesTimingModeInvalidate(AtDevice self)
    {
    return FaceplateSerdesTimingModeInvalidate(self);
    }

void Tha60290021DeviceInputClocksCheck(AtDevice self)
    {
    InputClocksCheck(self);
    }

uint32 Tha60290021DeviceNumFaceplateSerdes(Tha60290021Device self)
    {
    return mMethodsGet(self)->NumFaceplateSerdes(self);
    }

uint32 Tha60290021DeviceNumMateSerdes(Tha60290021Device self)
    {
    return mMethodsGet(self)->NumMateSerdes(self);
    }

uint32 Tha60290021DeviceNumBackplaneSerdes(Tha60290021Device self)
    {
    return mMethodsGet(self)->NumBackplaneSerdes(self);
    }

uint32 Tha60290021DeviceNumSgmiiSerdes(Tha60290021Device self)
    {
    return mMethodsGet(self)->NumSgmiiSerdes(self);
    }

uint8 Tha60290021DeviceFaceplateSerdesStartId(Tha60290021Device self)
    {
    return mMethodsGet(self)->FaceplateSerdesStartId(self);
    }

uint8 Tha60290021DeviceFaceplateSerdesEndId(Tha60290021Device self)
    {
    return mMethodsGet(self)->FaceplateSerdesEndId(self);
    }

uint8 Tha60290021DeviceMateSerdesStartId(Tha60290021Device self)
    {
    return mMethodsGet(self)->MateSerdesStartId(self);
    }

uint8 Tha60290021DeviceMateSerdesEndId(Tha60290021Device self)
    {
    return mMethodsGet(self)->MateSerdesEndId(self);
    }

uint8 Tha60290021DeviceBackplaneSerdesStartId(Tha60290021Device self)
    {
    return mMethodsGet(self)->BackplaneSerdesStartId(self);
    }

uint8 Tha60290021DeviceBackplaneSerdesEndId(Tha60290021Device self)
    {
    return mMethodsGet(self)->BackplaneSerdesEndId(self);
    }

uint8 Tha60290021DeviceSgmiiSerdesStartId(Tha60290021Device self)
    {
    return mMethodsGet(self)->SgmiiSerdesStartId(self);
    }

uint8 Tha60290021DeviceSgmiiSerdesEndId(Tha60290021Device self)
    {
    return mMethodsGet(self)->SgmiiSerdesEndId(self);
    }

void Tha60290021DeviceBackplaneClockStatusDisplay(AtDevice self)
    {
    BackplaneClockStatusDisplay(self);
    }

void Tha60290021DeviceRefClockStatusDisplay(AtDevice self)
    {
    RefClockStatusDisplay(self);
    }

uint32 Tha60290021DeviceFaceplateFreqExpectedValue(AtDevice self, uint32 serdesId)
    {
    return FaceplateFreqExpectedValue(self, serdesId);
    }

void Tha60290021DeviceSlowClockStatusDisplay(AtDevice self, uint32 expectedFreq)
    {
    SlowClockStatusDisplayAgainstExpectedClock(self, expectedFreq);
    }

void Tha60290021DeviceFsmFrequencyDisplay(AtDevice self)
    {
    FsmFrequencyDisplay(self);
    }

void Tha60290021DeviceSpareGpioFrequencyDisplay(AtDevice self, uint8 maxPins)
    {
    SpareGpioFrequencyDisplay(self, maxPins);
    }

void Tha60290021DeviceSpareFrequencyDisplay(AtDevice self)
    {
    SpareFrequencyDisplay(self);
    }

void Tha60290021DeviceRefout8kDisplay(AtDevice self)
    {
    Refout8kDisplay(self);
    }

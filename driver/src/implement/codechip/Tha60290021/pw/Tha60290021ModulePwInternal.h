/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW Module
 * 
 * File        : Tha60290021ModulePw.h
 * 
 * Created Date: Sep 4, 2016
 *
 * Description : Tha60290021 Module PW header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPWINTERNAL_H_
#define _THA60290021MODULEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/pw/Tha60210051ModulePwInternal.h"
#include "Tha60290021ModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModulePwMethods
    {
    eBool (*SubportVlanHwIsReady)(Tha60290021ModulePw self);
    eBool (*CanSelectSubPortVlanPerPw)(Tha60290021ModulePw self);
    AtPw  (*KBytePwObjectCreate)(Tha60290021ModulePw self, uint32 pwId);
    AtPwKbyteChannel (*KByteChannelObjectCreate)(Tha60290021ModulePw self, uint32 channelId, AtPw pw);
    eBool (*DccKbyteInterruptIsSupported)(Tha60290021ModulePw self);
    }tTha60290021ModulePwMethods;

typedef struct tTha60290021ModulePw
    {
    tTha60210051ModulePw super;
    const tTha60290021ModulePwMethods *methods;

    /* Private data */
    AtPw kBytePw; /* To Create Kbyte Pws */
    AtChannel hoVcToRestoreXc;
    }tTha60290021ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha60290021ModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPWINTERNAL_H_ */


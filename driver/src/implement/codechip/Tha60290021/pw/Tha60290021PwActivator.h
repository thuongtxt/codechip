/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60290021PwActivator.h
 * 
 * Created Date: Sep 9, 2018
 *
 * Description : PW activator for 60210011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021PWACTIVATOR_H_
#define _THA60290021PWACTIVATOR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021PwActivator * Tha60290021PwActivator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021PWACTIVATOR_H_ */


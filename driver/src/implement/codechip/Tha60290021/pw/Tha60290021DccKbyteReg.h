/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_RD_DCCK_H_
#define _AF6_REG_AF6CNC0021_RD_DCCK_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : DCCK CPU Reg Hold Control
Reg Addr   : 0x0000A-0x0000B
Reg Formula: 0x0000A + $HoldId
    Where  : 
           + $HoldId(0-1): Hold register
Reg Desc   : 
The register provides hold register for two word 32-bits MSB when CPU access to engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcck_cpu_hold_Base                                                                     0x0000A

/*--------------------------------------
BitField Name: HoldReg
BitField Type: RW
BitField Desc: Hold 32 bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dcck_cpu_hold_HoldReg_Mask                                                               cBit31_0
#define cAf6_dcck_cpu_hold_HoldReg_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Status
Reg Addr   : 0x02001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides global interrupt status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_glbint_stt_Base                                                                   0x02001
#define cAf6Reg_upen_glbint_stt_WidthVal                                                                    32

/*--------------------------------------
BitField Name: kbyte_interrupt
BitField Type: RO
BitField Desc: interrupt from KByte     event
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_glbint_stt_kbyte_interrupt_Mask                                                        cBit1
#define cAf6_upen_glbint_stt_kbyte_interrupt_Shift                                                           1

/*--------------------------------------
BitField Name: dcc_interrupt
BitField Type: RO
BitField Desc: interrupt from DCC event
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_glbint_stt_dcc_interrupt_Mask                                                          cBit0
#define cAf6_upen_glbint_stt_dcc_interrupt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Status Reg0
Reg Addr   : 0x02004
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_rxdcc0_stt_Base                                                               0x02004

/*--------------------------------------
BitField Name: dcc_channel_disable
BitField Type: W1C
BitField Desc: DCC Local Channel Identifier mapping is disable Bit[00] ->
Bit[31] indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_int_rxdcc0_stt_dcc_channel_disable_Mask                                             cBit31_0
#define cAf6_upen_int_rxdcc0_stt_dcc_channel_disable_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Status Reg1
Reg Addr   : 0x02005
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_rxdcc1_stt_Base                                                               0x02005
#define cAf6Reg_upen_int_rxdcc1_stt_WidthVal                                                                32

/*--------------------------------------
BitField Name: dcc_sgm_ovrsize_len
BitField Type: W1C
BitField Desc: Received DCC packet's length from SGMII port over maximum allowed
length (1318 bytes)
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_sgm_ovrsize_len_Mask                                                cBit3
#define cAf6_upen_int_rxdcc1_stt_dcc_sgm_ovrsize_len_Shift                                                   3

/*--------------------------------------
BitField Name: dcc_sgm_crc_error
BitField Type: W1C
BitField Desc: Received packet from SGMII port has FCS error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_sgm_crc_error_Mask                                                  cBit2
#define cAf6_upen_int_rxdcc1_stt_dcc_sgm_crc_error_Shift                                                     2

/*--------------------------------------
BitField Name: dcc_cvlid_mismat
BitField Type: W1C
BitField Desc: Received 12b CVLAN ID value of DCC frame different from global
provisioned CVID
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_cvlid_mismat_Mask                                                   cBit1
#define cAf6_upen_int_rxdcc1_stt_dcc_cvlid_mismat_Shift                                                      1

/*--------------------------------------
BitField Name: dcc_macda_mismat
BitField Type: W1C
BitField Desc: Received 43b MAC DA value of DCC frame different from global
provisioned DA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_macda_mismat_Mask                                                   cBit0
#define cAf6_upen_int_rxdcc1_stt_dcc_macda_mismat_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Status Reg1
Reg Addr   : 0x02006
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_eth2ocn_blk_ept_status_Base                                                           0x02006

/*--------------------------------------
BitField Name: dcc_eth2ocn_blk_ept
BitField Type: W1C
BitField Desc: DCC packet buffer for ETH2OCN direction was fulled, some packets
will be lost. Bit[00] -> Bit[31] indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_int_rxdcc2_stt_dcc_eth2ocn_blk_ept_Mask                                             cBit31_0
#define cAf6_upen_int_rxdcc2_stt_dcc_eth2ocn_blk_ept_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt Buffer Full Status
Reg Addr   : 0x02008
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_ocn2eth_blk_ept_status_Base                                                          0x02008

/*--------------------------------------
BitField Name: dcc_ocn2eth_blk_ept
BitField Type: W1C
BitField Desc: DCC packet buffer for OCN2ETH direction was fulled, some packets
will be lost Bit[00] -> Bit[31] indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_int_txdcc0_stt_dcc_ocn2eth_blk_ept_Mask                                             cBit31_0
#define cAf6_upen_int_txdcc0_stt_dcc_ocn2eth_blk_ept_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt CRC Error Status
Reg Addr   : 0x02009
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_crc_error_status_Base                                                               0x02009

/*--------------------------------------
BitField Name: dcc_hdlc_crc_error
BitField Type: W1C
BitField Desc: Received HDLC frame from OCN has FCS error
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_int_txdcc1_stt_dcc_hdlc_crc_error_Mask                                              cBit31_0
#define cAf6_upen_int_txdcc1_stt_dcc_hdlc_crc_error_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt Oversize Length Status
Reg Addr   : 0x0200D
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_hdlc_ovrsize_len_status_Base                                                           0x0200D

/*--------------------------------------
BitField Name: dcc_hdlc_ovrsize_len
BitField Type: W1C
BitField Desc: Received HDLC packet's length from OCN overed maximum allowed
length     (1536 bytes)
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_int_txdcc2_stt_dcc_hdlc_ovrsize_len_Mask                                            cBit31_0
#define cAf6_upen_int_txdcc2_stt_dcc_hdlc_ovrsize_len_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt Status
Reg Addr   : 0x0200E
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_hdlc_undsize_len_status_Base                                                           0x0200E

/*--------------------------------------
BitField Name: dcc_hdlc_undsize_len
BitField Type: W1C
BitField Desc: Received HDLC packet's length from OCN undered minimum allowed
length (2 bytes)
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_int_txdcc3_stt_dcc_hdlc_undsize_len_Mask                                            cBit31_0
#define cAf6_upen_int_txdcc3_stt_dcc_hdlc_undsize_len_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : KByte Interupt Status
Reg Addr   : 0x0200F
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of Kbyte event

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_rxk12_stt_Base                                                                0x0200F
#define cAf6Reg_upen_int_rxk12_stt_WidthVal                                                                 32

/*--------------------------------------
BitField Name: aps_per_channel_mismat
BitField Type: W1C
BitField Desc: Received CHANNEL ID value of APS frame different from configured
value. This is a per channel status. Bit 23 -> 08 represent for channel 15 -> 0
respectively
BitField Bits: [23:08]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_per_channel_mismat_Mask                                           cBit23_8
#define cAf6_upen_int_rxk12_stt_aps_per_channel_mismat_Shift                                                 8

/*--------------------------------------
BitField Name: aps_watdog_alarm
BitField Type: W1C
BitField Desc: No packets received in the period defined in watchdog timer
register
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_watdog_alarm_Mask                                                    cBit7
#define cAf6_upen_int_rxk12_stt_aps_watdog_alarm_Shift                                                       7

/*--------------------------------------
BitField Name: aps_glb_channel_mismat
BitField Type: W1C
BitField Desc: There is one or more mismatch between received CHANNELID value of
APS frame and configuration value of that channelID. This bit is set whenever a
mismatch happen in one or more of 16 channels
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_glb_channel_mismat_Mask                                              cBit6
#define cAf6_upen_int_rxk12_stt_aps_glb_channel_mismat_Shift                                                 6

/*--------------------------------------
BitField Name: aps_lencount_mismat
BitField Type: W1C
BitField Desc: Received PACKET BYTE COUNTER value of APS frame different from
configuration
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_lencount_mismat_Mask                                                 cBit5
#define cAf6_upen_int_rxk12_stt_aps_lencount_mismat_Shift                                                    5

/*--------------------------------------
BitField Name: aps_lenfield_mismat
BitField Type: W1C
BitField Desc: Received LENGTH FIELD value of APS frame different from
configuration
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_lenfield_mismat_Mask                                                 cBit4
#define cAf6_upen_int_rxk12_stt_aps_lenfield_mismat_Shift                                                    4

/*--------------------------------------
BitField Name: aps_ver_mismat
BitField Type: W1C
BitField Desc: Received VERSION value of APS frame different from configuration
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_ver_mismat_Mask                                                      cBit3
#define cAf6_upen_int_rxk12_stt_aps_ver_mismat_Shift                                                         3

/*--------------------------------------
BitField Name: aps_apstp_mismat
BitField Type: W1C
BitField Desc: Received TYPE value of APS frame different from configuration
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_apstp_mismat_Mask                                                    cBit2
#define cAf6_upen_int_rxk12_stt_aps_apstp_mismat_Shift                                                       2

/*--------------------------------------
BitField Name: aps_ethtp_mismat
BitField Type: W1C
BitField Desc: Received ETHERNET TYPE value of APS frame different from
configuration
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_ethtp_mismat_Mask                                                    cBit1
#define cAf6_upen_int_rxk12_stt_aps_ethtp_mismat_Shift                                                       1

/*--------------------------------------
BitField Name: aps_macda_mismat
BitField Type: W1C
BitField Desc: Received DA value of APS frame different from configed DA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_macda_mismat_Mask                                                    cBit0
#define cAf6_upen_int_rxk12_stt_aps_macda_mismat_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Loopback Enable Configuration
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides loopback enable configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_loopen_Base                                                                       0x00001
#define cAf6Reg_upen_loopen_WidthVal                                                                        32

/*--------------------------------------
BitField Name: sgmii_cap_tx_DCC_KByte
BitField Type: RW
BitField Desc: Select capture TX of DCC or Kbyte data     .
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_loopen_sgmii_cap_tx_DCC_KByte_Mask                                                     cBit7
#define cAf6_upen_loopen_sgmii_cap_tx_DCC_KByte_Shift                                                        7

/*--------------------------------------
BitField Name: sgmii_cap_select
BitField Type: RW
BitField Desc: Select capture TX or RX SGMII data             .
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_loopen_sgmii_cap_select_Mask                                                           cBit6
#define cAf6_upen_loopen_sgmii_cap_select_Shift                                                              6

/*--------------------------------------
BitField Name: dcc_buffer_loop_en
BitField Type: RW
BitField Desc: Enable loopback of RX-BUFFER to TX-BUFFER      .
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_loopen_dcc_buffer_loop_en_Mask                                                         cBit5
#define cAf6_upen_loopen_dcc_buffer_loop_en_Shift                                                            5

/*--------------------------------------
BitField Name: genmon_loop_en
BitField Type: RW
BitField Desc: Enable DCC GEN to RX-DCC                       .
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_loopen_genmon_loop_en_Mask                                                             cBit4
#define cAf6_upen_loopen_genmon_loop_en_Shift                                                                4

/*--------------------------------------
BitField Name: ksdh_loop_en
BitField Type: RW
BitField Desc: Enable loopback of Kbyte information (TDM side)          .
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_loopen_ksdh_loop_en_Mask                                                               cBit3
#define cAf6_upen_loopen_ksdh_loop_en_Shift                                                                  3

/*--------------------------------------
BitField Name: ksgm_loop_en
BitField Type: RW
BitField Desc: Enable SGMII loopback of Kbyte Port.
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_loopen_ksgm_loop_en_Mask                                                               cBit2
#define cAf6_upen_loopen_ksgm_loop_en_Shift                                                                  2

/*--------------------------------------
BitField Name: hdlc_loop_en
BitField Type: RW
BitField Desc: Enable loopback from HDLC Encap to HDLC DEcap of DCC byte.
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_loopen_hdlc_loop_en_Mask                                                               cBit1
#define cAf6_upen_loopen_hdlc_loop_en_Shift                                                                  1

/*--------------------------------------
BitField Name: dsgm_loop_en
BitField Type: RW
BitField Desc: Enable TX-SGMII to RX-SGMII loopback of DCC Port.
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_loopen_dsgm_loop_en_Mask                                                               cBit0
#define cAf6_upen_loopen_dsgm_loop_en_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Enable
Reg Addr   : 0x00002
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable global interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_glb_intenb_Base                                                                   0x00002
#define cAf6Reg_upen_glb_intenb_WidthVal                                                                    32

/*--------------------------------------
BitField Name: kbyte_interrupt
BitField Type: W1C
BitField Desc: Enable interrupt for KByte events
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_glb_intenb_kbyte_interrupt_Mask                                                        cBit1
#define cAf6_upen_glb_intenb_kbyte_interrupt_Shift                                                           1

/*--------------------------------------
BitField Name: dcc_interrupt
BitField Type: W1C
BitField Desc: Enable interrupt for DCC event
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_glb_intenb_dcc_interrupt_Mask                                                          cBit0
#define cAf6_upen_glb_intenb_dcc_interrupt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Enable Reg0
Reg Addr   : 0x00004
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdcc_intenb_r0_Base                                                              0x00004

/*--------------------------------------
BitField Name: enb_int_dcc_chan_dis
BitField Type: RW
BitField Desc: Enable Interrupt of DCC Local Channel Identifier mapping per
channel Bit[00] -> Bit[31] indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxdcc_intenb_r0_enb_int_dcc_chan_dis_Mask                                           cBit31_0
#define cAf6_upen_rxdcc_intenb_r0_enb_int_dcc_chan_dis_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Enable Reg1
Reg Addr   : 0x00005
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdcc_intenb_r1_Base                                                              0x00005
#define cAf6Reg_upen_rxdcc_intenb_r1_WidthVal                                                               32

/*--------------------------------------
BitField Name: enb_int_dcc_sgm_ovrsize_len
BitField Type: RW
BitField Desc: Enable Interrupt of "Received DCC packet's length from SGMII port
over maximum allowed length (1318 bytes)"
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_sgm_ovrsize_len_Mask                                       cBit3
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_sgm_ovrsize_len_Shift                                          3

/*--------------------------------------
BitField Name: enb_int_dcc_sgm_crc_error
BitField Type: RW
BitField Desc: Enable Interrupt of "Received packet from SGMII port has FCS
error"
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_sgm_crc_error_Mask                                         cBit2
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_sgm_crc_error_Shift                                            2

/*--------------------------------------
BitField Name: enb_int_dcc_cvlid_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received 12b CVLAN ID value of DCC frame
different from global provisioned CVID "
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_cvlid_mismat_Mask                                          cBit1
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_cvlid_mismat_Shift                                             1

/*--------------------------------------
BitField Name: enb_int_dcc_macda_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received 43b MAC DA value of DCC frame
different from global provisioned DA"
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_macda_mismat_Mask                                          cBit0
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_macda_mismat_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Enable Reg2
Reg Addr   : 0x00006
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdcc_intenb_eth2ocn_blk_ept_Base                                                0x00006

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_blk_ept
BitField Type: RW
BitField Desc: Enable Interrupt of "DCC packet buffer for ETH2OCN direction was
fulled, some packets will be lost "     per channel Bit[00] -> Bit[31] indicate
channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_rxdcc_intenb_r2_enb_int_dcc_eth2ocn_blk_ept_Mask                                    cBit31_0
#define cAf6_upen_rxdcc_intenb_r2_enb_int_dcc_eth2ocn_blk_ept_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt Enable Reg0
Reg Addr   : 0x00008
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_ocn2eth_blk_ept_Base                                                 0x00008

/*--------------------------------------
BitField Name: enb_int_dcc_ocn2eth_blk_ept
BitField Type: RW
BitField Desc: Enable Interrupt of "DCC packet buffer for OCN2ETH direction was
fulled, some packets will be lost" per channel Bit[00] -> Bit[31] indicate
channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_ept_enb_int_dcc_ocn2eth_blk_ept_Mask                                   cBit31_0
#define cAf6_upen_txdcc_intenb_ept_enb_int_dcc_ocn2eth_blk_ept_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt Enable Reg0
Reg Addr   : 0x00009
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_crc_Base                                                             0x00009

/*--------------------------------------
BitField Name: enb_int_dcc_hdlc_crc_error
BitField Type: RW
BitField Desc: Enable Interrupt of "Received HDLC frame from OCN has FCS error
" per channel Bit[00] -> Bit[31] indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_crc_enb_int_dcc_hdlc_crc_error_Mask                                    cBit31_0
#define cAf6_upen_txdcc_intenb_crc_enb_int_dcc_hdlc_crc_error_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt Enable Reg2
Reg Addr   : 0x0000D
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_ovrsize_Base                                                       0x0000D

/*--------------------------------------
BitField Name: enb_int_dcc_hdlc_ovrsize_len
BitField Type: RW
BitField Desc: Enable Interrupt of "Received HDLC packet's length from OCN
overed maximum allowed length (1536 bytes)   " per channel Bit[00] -> Bit[31]
indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_ovr_enb_int_dcc_hdlc_ovrsize_len_Mask                                  cBit31_0
#define cAf6_upen_txdcc_intenb_ovr_enb_int_dcc_hdlc_ovrsize_len_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Direction Interupt Enable Reg0
Reg Addr   : 0x0000E
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC event OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_txdcc_intenb_undsize_Base                                                        0x0000E

/*--------------------------------------
BitField Name: enb_int_dcc_hdlc_undsize_len
BitField Type: RW
BitField Desc: Enable Interrupt of "Received HDLC packet's length from OCN
undered minimum allowed length (2 bytes) " per channel Bit[00] -> Bit[31]
indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_txdcc_intenb_crc_enb_int_dcc_hdlc_undsize_len_Mask                                  cBit31_0
#define cAf6_upen_txdcc_intenb_crc_enb_int_dcc_hdlc_undsize_len_Shift                                        0


/*------------------------------------------------------------------------------
Reg Name   : KByte Interupt Enable
Reg Addr   : 0x0000F
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of Kbyte events

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxk12_intenb_Base                                                                 0x0000F
#define cAf6Reg_upen_rxk12_intenb_WidthVal                                                                  32

/*--------------------------------------
BitField Name: enb_int_aps_per_channel_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received CHANNEL ID value of APS frame
different from configuration per channel ". Bit 23->08 represent for mismatch of
channelID 15->0 respectively
BitField Bits: [23:08]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_per_channel_mismat_Mask                                    cBit23_8
#define cAf6_upen_rxk12_intenb_enb_int_aps_per_channel_mismat_Shift                                          8

/*--------------------------------------
BitField Name: enb_int_aps_watdog_alarm
BitField Type: RW
BitField Desc: Enable Interrupt of "No packets received in the period defined in
watchdog timer register"
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_watdog_alarm_Mask                                             cBit7
#define cAf6_upen_rxk12_intenb_enb_int_aps_watdog_alarm_Shift                                                7

/*--------------------------------------
BitField Name: enb_int_aps_glb_channel_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received CHANNEL ID value of APS frame
different from configuration ". This bit is set when there is one or more
mismatching channelID happen
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_glb_channel_mismat_Mask                                       cBit6
#define cAf6_upen_rxk12_intenb_enb_int_aps_glb_channel_mismat_Shift                                          6

/*--------------------------------------
BitField Name: enb_int_aps_lencount_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received PACKET BYTE COUNTER value of APS
frame different from configuration "
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_lencount_mismat_Mask                                          cBit5
#define cAf6_upen_rxk12_intenb_enb_int_aps_lencount_mismat_Shift                                             5

/*--------------------------------------
BitField Name: enb_int_aps_lenfield_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received LENGTH FIELD value of APS frame
different from configuration "
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_lenfield_mismat_Mask                                          cBit4
#define cAf6_upen_rxk12_intenb_enb_int_aps_lenfield_mismat_Shift                                             4

/*--------------------------------------
BitField Name: enb_int_aps_ver_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received VERSION value of APS frame
different from configuration "
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_ver_mismat_Mask                                               cBit3
#define cAf6_upen_rxk12_intenb_enb_int_aps_ver_mismat_Shift                                                  3

/*--------------------------------------
BitField Name: enb_int_aps_apstp_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received TYPE value of APS frame different
from configuration "
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_apstp_mismat_Mask                                             cBit2
#define cAf6_upen_rxk12_intenb_enb_int_aps_apstp_mismat_Shift                                                2

/*--------------------------------------
BitField Name: enb_int_aps_ethtp_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received ETHERNET TYPE value of APS frame
different from configuration "
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_ethtp_mismat_Mask                                             cBit1
#define cAf6_upen_rxk12_intenb_enb_int_aps_ethtp_mismat_Shift                                                1

/*--------------------------------------
BitField Name: enb_int_aps_macda_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received DA value of APS frame different
from configed DA "
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_macda_mismat_Mask                                             cBit0
#define cAf6_upen_rxk12_intenb_enb_int_aps_macda_mismat_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Buffer_Linklist_Init
Reg Addr   : 0x10001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register use to trigger Link-List initialization

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_llst_Base                                                                     0x10001
#define cAf6Reg_upen_cfg_llst_WidthVal                                                                      32

/*--------------------------------------
BitField Name: tx_ini_done
BitField Type: W1C
BitField Desc: Indicate TX Link-list initialization successed
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_cfg_llst_tx_ini_done_Mask                                                              cBit3
#define cAf6_upen_cfg_llst_tx_ini_done_Shift                                                                 3

/*--------------------------------------
BitField Name: rx_ini_done
BitField Type: W1C
BitField Desc: Indicate RX Link-list initialization successed
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_cfg_llst_rx_ini_done_Mask                                                              cBit2
#define cAf6_upen_cfg_llst_rx_ini_done_Shift                                                                 2

/*--------------------------------------
BitField Name: tx_ll_init
BitField Type: RW
BitField Desc: Use to trigger Linklist of DCC TX packet buffer. Write 0 first,
then write 1 to trigger
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_cfg_llst_tx_ll_init_Mask                                                               cBit1
#define cAf6_upen_cfg_llst_tx_ll_init_Shift                                                                  1

/*--------------------------------------
BitField Name: rx_ll_init
BitField Type: RW
BitField Desc: Use to trigger Linklist of DCC RX packet buffer. Write 0 first,
then write 1 to trigger
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_llst_rx_ll_init_Mask                                                               cBit0
#define cAf6_upen_cfg_llst_rx_ll_init_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : DDC_TX_Header_Per_Channel_reg0
Reg Addr   : 0x11200 - 0x1127C
Reg Formula: 0x11200 + $channelid*16
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
The register provides data for configuration of 22bytes Header of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcctxhdr_reg0_Base                                                                0x11200

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: MAC DA value of Channel ID
BitField Bits: [63:16]
--------------------------------------*/
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_01_Mask                                                       cBit31_16
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_01_Shift                                                             16
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_02_Mask                                                        cBit31_0
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_02_Shift                                                              0

/*--------------------------------------
BitField Name: MAC_SA
BitField Type: RW
BitField Desc: 16b MSB MAC SA value of Channel ID
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_Mask                                                           cBit15_0
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : DDC_TX_Header_Per_Channel_Reg1
Reg Addr   : 0x11201 - 0x1127D
Reg Formula: 0x11201 + $channelid*4
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
The register provides data for configuration of 22bytes Header of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcctxhdr_reg1_Base                                                                0x11201

/*--------------------------------------
BitField Name: MAC_SA
BitField Type: RW
BitField Desc: 48b LSB MAC SA value of Channel ID
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_dcctxhdr_reg1_MAC_SA_Mask                                                           cBit31_0
#define cAf6_upen_dcctxhdr_reg1_MAC_SA_Shift                                                                 0

/*--------------------------------------
BitField Name: VLAN
BitField Type: RW
BitField Desc: 16b
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_dcctxhdr_reg1_VLAN_Mask                                                            cBit31_16
#define cAf6_upen_dcctxhdr_reg1_VLAN_Shift                                                                  16

/*--------------------------------------
BitField Name: VERSION
BitField Type: RW
BitField Desc: 8b Type field
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_dcctxhdr_reg1_VERSION_Mask                                                          cBit15_8
#define cAf6_upen_dcctxhdr_reg1_VERSION_Shift                                                                8

/*--------------------------------------
BitField Name: TYPE
BitField Type: RW
BitField Desc: 8b Version Field
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_dcctxhdr_reg1_TYPE_Mask                                                              cBit7_0
#define cAf6_upen_dcctxhdr_reg1_TYPE_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Channel_Enable
Reg Addr   : 0x11000
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration for enable transmission DDC packet per channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcctx_enacid_Base                                                                 0x11000

/*--------------------------------------
BitField Name: Channel_Enable
BitField Type: RW
BitField Desc: Enable transmitting of DDC packet per channel. Bit[31:0]
represent for channel 31->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dcctx_enacid_Channel_Enable_Mask                                                    cBit31_0
#define cAf6_upen_dcctx_enacid_Channel_Enable_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_Global_ProvisionedHeader_Configuration
Reg Addr   : 0x12001
Reg Formula: 0x12001
    Where  : 
Reg Desc   : 
The register provides data for configuration of 22bytes Header of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccrxhdr_Base                                                                     0x12001
#define cAf6Reg_upen_dccrxhdr_WidthVal                                                                      96

/*--------------------------------------
BitField Name: ETH_TYP
BitField Type: RW
BitField Desc: 16b value of Provisioned ETHERTYPE of DCC
BitField Bits: [70:55]
--------------------------------------*/
#define cAf6_upen_dccrxhdr_ETH_TYP_01_Mask                                                           cBit31_23
#define cAf6_upen_dccrxhdr_ETH_TYP_01_Shift                                                                 23
#define cAf6_upen_dccrxhdr_ETH_TYP_02_Mask                                                             cBit6_0
#define cAf6_upen_dccrxhdr_ETH_TYP_02_Shift                                                                  0

/*--------------------------------------
BitField Name: CVID
BitField Type: RW
BitField Desc: 12b value of Provisioned C-VLAN ID. This value is used to
compared with received CVLAN ID value.
BitField Bits: [54:43]
--------------------------------------*/
#define cAf6_upen_dccrxhdr_CVID_Mask                                                                 cBit22_11
#define cAf6_upen_dccrxhdr_CVID_Shift                                                                       11

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: 43b MSB of Provisioned MAC DA value. This value is used to
compared with received MAC_DA[47:05] value. If a match is confirmed,
MAC_DA[04:00] is used to represent channelID value before mapping.
BitField Bits: [42:00]
--------------------------------------*/
#define cAf6_upen_dccrxhdr_MAC_DA_01_Mask                                                             cBit31_0
#define cAf6_upen_dccrxhdr_MAC_DA_01_Shift                                                                   0
#define cAf6_upen_dccrxhdr_MAC_DA_02_Mask                                                             cBit10_0
#define cAf6_upen_dccrxhdr_MAC_DA_02_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_MAC_Check_Enable_Configuration
Reg Addr   : 0x12002
Reg Formula: 0x12002
    Where  : 
Reg Desc   : 
The register provides configuration that enable base MAC DA check

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccrxmacenb_Base                                                                  0x12002

/*--------------------------------------
BitField Name: MAC_check_enable
BitField Type: RW
BitField Desc: Enable checking of received MAC DA compare to globally
provisioned Base MAC.On mismatch, frame is discarded.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dccrxmacenb_MAC_check_enable_Mask                                                   cBit31_0
#define cAf6_upen_dccrxmacenb_MAC_check_enable_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_CVLAN_Check_Enable_Configuration
Reg Addr   : 0x12003
Reg Formula: 0x12003
    Where  : 
Reg Desc   : 
The register provides configuration that enable base CVLAN Check

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccrxcvlenb_Base                                                                  0x12003

/*--------------------------------------
BitField Name: CVL_check_enable
BitField Type: RW
BitField Desc: Enable checking of received CVLAN ID compare to globally
provisioned CVLAN ID.On mismatch, frame is discarded.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dccrxcvlenb_CVL_check_enable_Mask                                                   cBit31_0
#define cAf6_upen_dccrxcvlenb_CVL_check_enable_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_Channel_Mapping
Reg Addr   : 0x12200 - 0x1221F
Reg Formula: 0x12200 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
The register provides channel mapping from received MAC DA to internal channelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccdec_Base                                                                       0x12200
#define cAf6Reg_upen_dccdec_WidthVal                                                                        32

/*--------------------------------------
BitField Name: Channel_enable
BitField Type: RW
BitField Desc: Enable Local Channel Identifier. Rx packet is discarded if enable
is not set
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_dccdec_Channel_enable_Mask                                                             cBit5
#define cAf6_upen_dccdec_Channel_enable_Shift                                                                5

/*--------------------------------------
BitField Name: Mapping_ChannelID
BitField Type: RW
BitField Desc: Local ChannelID that is mapped from received bit[4:0] of MAC DA
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_upen_dccdec_Mapping_ChannelID_Mask                                                        cBit4_0
#define cAf6_upen_dccdec_Mapping_ChannelID_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Provisioned_APS_KByte_Value_Per_Channel
Reg Addr   : 0x21200  - 0x2120F
Reg Formula: 0x21200 + $channelid
    Where  : 
           + $channelid(0-15): Channel ID
Reg Desc   : 
The register provides configuration for Kbyte value to overwrite

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12pro_Base                                                                       0x21200

/*--------------------------------------
BitField Name: CHANNELID_APS
BitField Type: RW
BitField Desc: 32b of ChannelID APS value to overwrite This Kbyte value is used
to overwrite Kbyte value get from RX-OCN when enable [31:24]: K1 byte value
[23:16]: K2 byte value [15:08]: D1(EK1&EK2) (extend K) byte value [07:00]: set
to 0x0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_k12pro_CHANNELID_APS_Mask                                                           cBit31_0
#define cAf6_upen_k12pro_CHANNELID_APS_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Enable Transmit Validated KByte Change Per ChannelID Configuration
Reg Addr   : 0x21001
Reg Formula: 0x21001
    Where  : 
Reg Desc   : 
The register configures channelID for provisioned APS packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgenacid_Base                                                                    0x21001
#define cAf6Reg_upen_cfgenacid_WidthVal                                                                     32

/*--------------------------------------
BitField Name: ChannelID_Enable
BitField Type: RW
BitField Desc: Enable provisioned ChannelID. Bit[15:0] represents channelID
15->0 This channelID bitmap is used to allow transmission of newly validated K
byte change.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_cfgenacid_ChannelID_Enable_Mask                                                     cBit15_0
#define cAf6_upen_cfgenacid_ChannelID_Enable_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Timer_TX_Provisioned_APS_Packet
Reg Addr   : 0x21002
Reg Formula: 0x21002
    Where  : 
Reg Desc   : 
The register configures timer for transmiting provisioned APS packets of configured channelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgtim_Base                                                                       0x21002

/*--------------------------------------
BitField Name: Timer_Ena
BitField Type: RW
BitField Desc: Enable Timer for TX provisoned APS packet
BitField Bits: [31]
--------------------------------------*/
#define cAf6_upen_cfgtim_Timer_Ena_Mask                                                                 cBit31
#define cAf6_upen_cfgtim_Timer_Ena_Shift                                                                    31

/*--------------------------------------
BitField Name: Timer_Val
BitField Type: RW
BitField Desc: Timer value for TX provisoned APS packet. This value is the
number of clock counter represent the time interval For ex: to set an interval
256us with the clock 155Mz The clock counter is: 256x10^3/(10^3/155) = 256*155 =
39680 Timer range from 125us to 8ms. This timer is used only when bit 31 (Timer
Ena) is set Each time the timer reach configurated value, an APS packet will be
transmit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_upen_cfgtim_Timer_Val_Mask                                                               cBit23_0
#define cAf6_upen_cfgtim_Timer_Val_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Trigger_TX_Provisioned_APS_Packet
Reg Addr   : 0x21003
Reg Formula: 0x21003
    Where  : 
Reg Desc   : 
The register configures channelID for provisioned APS packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgcid_Base                                                                       0x21003
#define cAf6Reg_upen_cfgcid_WidthVal                                                                        32

/*--------------------------------------
BitField Name: SW_Trig
BitField Type: RW
BitField Desc: SW trigger transmission of Provisioned APS packet Write 0, then
write 1 to trigger
BitField Bits: [0:0]
--------------------------------------*/
#define cAf6_upen_cfgcid_SW_Trig_Mask                                                                    cBit0
#define cAf6_upen_cfgcid_SW_Trig_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Enable KByte Overwrite Per ChannelID Configuration
Reg Addr   : 0x21004
Reg Formula: 0x21004
    Where  : 
Reg Desc   : 
The register configures channelID for provisioned APS packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_upen_cfgovwcid_Base                                                               0x21004
#define cAf6Reg_upen_upen_cfgovwcid_WidthVal                                                                32

/*--------------------------------------
BitField Name: ChannelID_Enable
BitField Type: RW
BitField Desc: Enable overwrite Kbyte value of channel Bit[15:0] represents
channelID 15->0 This channelID bitmap is used to allow overwrite of channel's
Kbyte. When this bit is set. Kbyte will get value from configurated Provisioned
Kbyte instead of Kbyte received from RX-OCN
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_upen_cfgovwcid_ChannelID_Enable_Mask                                                cBit15_0
#define cAf6_upen_upen_cfgovwcid_ChannelID_Enable_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : APS_TX_Header_Mac_DA
Reg Addr   : 0x21006
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration of TX APS Packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_hdrmda_Base                                                                   0x21006
#define cAf6Reg_upen_cfg_hdrmda_WidthVal                                                                    64

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: MAC DA value of TX APS Packet
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_cfg_hdrmda_MAC_DA_01_Mask                                                           cBit31_0
#define cAf6_upen_cfg_hdrmda_MAC_DA_01_Shift                                                                 0
#define cAf6_upen_cfg_hdrmda_MAC_DA_02_Mask                                                           cBit15_0
#define cAf6_upen_cfg_hdrmda_MAC_DA_02_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : APS_TX_Header_Mac_SA
Reg Addr   : 0x21007
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration of TX APS Packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_hdrmsa_Base                                                                   0x21007
#define cAf6Reg_upen_cfg_hdrmsa_WidthVal                                                                    64

/*--------------------------------------
BitField Name: MAC_SA
BitField Type: RW
BitField Desc: MAC SA value of TX APS Packet
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_cfg_hdrmsa_MAC_SA_01_Mask                                                           cBit31_0
#define cAf6_upen_cfg_hdrmsa_MAC_SA_01_Shift                                                                 0
#define cAf6_upen_cfg_hdrmsa_MAC_SA_02_Mask                                                           cBit15_0
#define cAf6_upen_cfg_hdrmsa_MAC_SA_02_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : APS_TX_Header_VTL
Reg Addr   : 0x21008
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration of 18bytes Header of each channel ID receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_hdrvtl_Base                                                                   0x21008
#define cAf6Reg_upen_cfg_hdrvtl_WidthVal                                                                    64

/*--------------------------------------
BitField Name: VLAN
BitField Type: RW
BitField Desc: 16b
BitField Bits: [47:32]
--------------------------------------*/
#define cAf6_upen_cfg_hdrvtl_VLAN_Mask                                                                cBit15_0
#define cAf6_upen_cfg_hdrvtl_VLAN_Shift                                                                      0

/*--------------------------------------
BitField Name: ETHTYPE
BitField Type: RW
BitField Desc: 16b Ethernet Type field
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_cfg_hdrvtl_ETHTYPE_Mask                                                            cBit31_16
#define cAf6_upen_cfg_hdrvtl_ETHTYPE_Shift                                                                  16

/*--------------------------------------
BitField Name: APSTYPE
BitField Type: RW
BitField Desc: 8b Type field
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_cfg_hdrvtl_APSTYPE_Mask                                                             cBit15_8
#define cAf6_upen_cfg_hdrvtl_APSTYPE_Shift                                                                   8

/*--------------------------------------
BitField Name: VERSION
BitField Type: RW
BitField Desc: 8b Version Field
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_cfg_hdrvtl_VERSION_Mask                                                              cBit7_0
#define cAf6_upen_cfg_hdrvtl_VERSION_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x21100  - 0x2110F
Reg Formula: 0x21100 + $portID
    Where  : 
           + $portID(0-15): Port ID
Reg Desc   : 
The register provides channel mapping from portID to ChannelID configuraton

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cid2pid_Base                                                                      0x21100
#define cAf6Reg_upen_cid2pid_WidthVal                                                                       32

/*--------------------------------------
BitField Name: CHANNELID_VAL
BitField Type: RW
BitField Desc: 16b value of ChannelID of PortID
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_cid2pid_CHANNELID_VAL_Mask                                                          cBit15_0
#define cAf6_upen_cid2pid_CHANNELID_VAL_Shift                                                                0

#define cAf6_upen_cid2pid_channel12b_Mask                                                             cBit15_4
#define cAf6_upen_cid2pid_channel12b_Shift                                                                   4
#define cAf6_upen_cid2pid_channelId_Mask                                                               cBit3_0
#define cAf6_upen_cid2pid_channelId_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x21009
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides the initialization Kbyte value of each channelID
To init the content of each channelID, write bit31 = 0 first, then
write bit[31] = 1 and the channelId as well of Kbyte value of that
channelID. For example: to Init data FF for channel 0.
Step1: wr 0x21009 0
Step2: wr 0x21009 800000FF

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_gencid_Base                                                                   0x21009

/*--------------------------------------
BitField Name: Init_Enable
BitField Type: RW
BitField Desc: Enable ChannelID Data Initializtion
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_upen_cfg_gencid_Init_Enable_Mask                                                           cBit31
#define cAf6_upen_cfg_gencid_Init_Enable_Shift                                                              31

/*--------------------------------------
BitField Name: Init_ChannelId
BitField Type: RW
BitField Desc: ChannelID value
BitField Bits: [30:24]
--------------------------------------*/
#define cAf6_upen_cfg_gencid_Init_ChannelId_Mask                                                     cBit30_24
#define cAf6_upen_cfg_gencid_Init_ChannelId_Shift                                                           24

/*--------------------------------------
BitField Name: Init_KbyteVal
BitField Type: RW
BitField Desc: Default 24b value of Kbyte
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_cfg_gencid_Init_KbyteVal_Mask                                                       cBit23_0
#define cAf6_upen_cfg_gencid_Init_KbyteVal_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Header_MAC_DA_Configuration
Reg Addr   : 0x22001
Reg Formula: 0x22001
    Where  : 
Reg Desc   : 
The register provides MAC DA value of provisioned APS port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_macda_Base                                                                  0x22001
#define cAf6Reg_upen_k12rx_macda_WidthVal                                                                   64

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: Provisioned MAC DA value
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_k12rx_macda_MAC_DA_01_Mask                                                          cBit31_0
#define cAf6_upen_k12rx_macda_MAC_DA_01_Shift                                                                0
#define cAf6_upen_k12rx_macda_MAC_DA_02_Mask                                                          cBit15_0
#define cAf6_upen_k12rx_macda_MAC_DA_02_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Header_MAC_SA_Configuration
Reg Addr   : 0x22002
Reg Formula: 0x22002
    Where  : 
Reg Desc   : 
The register provides MAC SA value of provisioned APS port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_macsa_Base                                                                  0x22002
#define cAf6Reg_upen_k12rx_macsa_WidthVal                                                                   64

/*--------------------------------------
BitField Name: MAC_SA
BitField Type: RW
BitField Desc: Provisioned MAC SA value
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_k12rx_macsa_MAC_SA_01_Mask                                                          cBit31_0
#define cAf6_upen_k12rx_macsa_MAC_SA_01_Shift                                                                0
#define cAf6_upen_k12rx_macsa_MAC_SA_02_Mask                                                          cBit15_0
#define cAf6_upen_k12rx_macsa_MAC_SA_02_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Header_EVT_Configuration
Reg Addr   : 0x22003
Reg Formula: 0x22003
    Where  : 
Reg Desc   : 
The register provides EVT(Ethernet Type, Version, APS Type) value of provisioned APS port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_evt_Base                                                                    0x22003

/*--------------------------------------
BitField Name: ETHTYP
BitField Type: RW
BitField Desc: Provisioned Ethernet Type
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_k12rx_evt_ETHTYP_Mask                                                              cBit31_16
#define cAf6_upen_k12rx_evt_ETHTYP_Shift                                                                    16

/*--------------------------------------
BitField Name: VER
BitField Type: RW
BitField Desc: Provisioned Version Number
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_k12rx_evt_VER_Mask                                                                  cBit15_8
#define cAf6_upen_k12rx_evt_VER_Shift                                                                        8

/*--------------------------------------
BitField Name: TYPE
BitField Type: RW
BitField Desc: Provisioned APS Type
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_k12rx_evt_TYPE_Mask                                                                  cBit7_0
#define cAf6_upen_k12rx_evt_TYPE_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_ChannelID_Configuration
Reg Addr   : 0x22100 - 0x2210F
Reg Formula: 0x22100 + $PortID
    Where  : 
           + $PortID(0-15): PortID value
Reg Desc   : 
The register provides ChannelID configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_chcfg_Base                                                                  0x22100
#define cAf6Reg_upen_k12rx_chcfg_WidthVal                                                                   32

/*--------------------------------------
BitField Name: ChannelID
BitField Type: RW
BitField Desc: 12b represent for Channel ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_k12rx_chcfg_ChannelID_Mask                                                          cBit11_0
#define cAf6_upen_k12rx_chcfg_ChannelID_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_ChannelID_Mapping_Configuration
Reg Addr   : 0x22400 - 0x2240F
Reg Formula: 0x22400 + $ChannelID
    Where  : 
           + $ChannelID(0-15): ChannelID value
Reg Desc   : 
The register provides ChannelID configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_chmap_Base                                                                  0x22400
#define cAf6Reg_upen_k12rx_chmap_WidthVal                                                                   32

/*--------------------------------------
BitField Name: PortID
BitField Type: RW
BitField Desc: PortID value map from received Channel ID
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_k12rx_chmap_PortID_Mask                                                              cBit3_0
#define cAf6_upen_k12rx_chmap_PortID_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Alarm_Sticky
Reg Addr   : 0x22010
Reg Formula: 0x22010
    Where  : 
Reg Desc   : 
The register provides Receive APS Frame Alarm sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_alarm_Base                                                                  0x22010
#define cAf6Reg_upen_k12rx_alarm_WidthVal                                                                   32

/*--------------------------------------
BitField Name: per_channel_mismat
BitField Type: W1C
BitField Desc: Received CHANNEL ID value of APS frame different from configured
value. This is a per channel status. Bit 23 -> 08 represent for channel 15 -> 0
respectively
BitField Bits: [23:08]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_per_channel_mismat_Mask                                                 cBit23_8
#define cAf6_upen_k12rx_alarm_per_channel_mismat_Shift                                                       8

/*--------------------------------------
BitField Name: Frame_miss
BitField Type: W1C
BitField Desc: APS FRAME missed When watchdog timer is enable. This bit is set
if no frame is received in pre-defined interval
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_Frame_miss_Mask                                                            cBit7
#define cAf6_upen_k12rx_alarm_Frame_miss_Shift                                                               7

/*--------------------------------------
BitField Name: channel_miss
BitField Type: W1C
BitField Desc: CHANNEL mismatch. Received CHANNELID value different from
configed
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_channel_miss_Mask                                                          cBit6
#define cAf6_upen_k12rx_alarm_channel_miss_Shift                                                             6

/*--------------------------------------
BitField Name: real_len_miss
BitField Type: W1C
BitField Desc: ACTUAL LENGH mismatch. acket LENGTH value different 0x60
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_real_len_miss_Mask                                                         cBit5
#define cAf6_upen_k12rx_alarm_real_len_miss_Shift                                                            5

/*--------------------------------------
BitField Name: len_miss
BitField Type: W1C
BitField Desc: LENGH mismatch. Received LENGTH value different 0x60
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_len_miss_Mask                                                              cBit4
#define cAf6_upen_k12rx_alarm_len_miss_Shift                                                                 4

/*--------------------------------------
BitField Name: ver_miss
BitField Type: W1C
BitField Desc: VERSION  mismatch. Received VERSION value different from configed
VER
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_ver_miss_Mask                                                              cBit3
#define cAf6_upen_k12rx_alarm_ver_miss_Shift                                                                 3

/*--------------------------------------
BitField Name: type_miss
BitField Type: W1C
BitField Desc: TYPE mismatch. Received TYPE value different from configed TYPE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_type_miss_Mask                                                             cBit2
#define cAf6_upen_k12rx_alarm_type_miss_Shift                                                                2

/*--------------------------------------
BitField Name: ethtype_miss
BitField Type: W1C
BitField Desc: ETHERNET TYPE mismatch. Received ETHTYPE value different from
configed E
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_ethtype_miss_Mask                                                          cBit1
#define cAf6_upen_k12rx_alarm_ethtype_miss_Shift                                                             1

/*--------------------------------------
BitField Name: mac_da_miss
BitField Type: W1C
BitField Desc: DA mismatch. Received DA value different from configed DA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_mac_da_miss_Mask                                                           cBit0
#define cAf6_upen_k12rx_alarm_mac_da_miss_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_WatchDog_Timer
Reg Addr   : 0x22008
Reg Formula: 0x22008
    Where  : 
Reg Desc   : 
The register provides WatchDog Timer configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_watdog_Base                                                                 0x22008

/*--------------------------------------
BitField Name: Enable
BitField Type: RW
BitField Desc: Enable Watchdog Timer
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_upen_k12rx_watdog_Enable_Mask                                                              cBit31
#define cAf6_upen_k12rx_watdog_Enable_Shift                                                                 31

/*--------------------------------------
BitField Name: Timer_Val
BitField Type: RW
BitField Desc: Watch Dog Timer value. This value is the number of clock counter
represent the expected time interval For ex: to set an interval 256us with the
clock 155Mz The clock counter is: 256x10^3/(10^3/155) = 256*155 = 39680 This
value will be write to this field. Timer range from 256us to 16ms. If in this
timer window, no frames received, alarm will be set
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_k12rx_watdog_Timer_Val_Mask                                                         cBit23_0
#define cAf6_upen_k12rx_watdog_Timer_Val_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Pkt_Length_Alarm_Sticky
Reg Addr   : 0x11004
Reg Formula: 0x11004
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stkerr_pktlen_Base                                                                0x11004

/*--------------------------------------
BitField Name: dcc_overize_err
BitField Type: W1C
BitField Desc: HDLC Length Oversize Error. Alarm per channel Received HDLC Frame
from OCN has frame length over maximum allowed length (1536bytes) Bit[63] ->
Bit[32]: channel ID 31 ->0
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_stkerr_pktlen_dcc_overize_err_Mask                                                  cBit31_0
#define cAf6_upen_stkerr_pktlen_dcc_overize_err_Shift                                                        0

/*--------------------------------------
BitField Name: dcc_undsize_err
BitField Type: W1C
BitField Desc: HDLC Length Undersize Error. Alarm per channel Received HDLC
Frame from OCN has frame length below minimum allowed length (2bytes) Bit[31] ->
Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_stkerr_pktlen_dcc_undsize_err_Mask                                                  cBit31_0
#define cAf6_upen_stkerr_pktlen_dcc_undsize_err_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Pkt_Error_And_Buffer_Full_Alarm_Sticky
Reg Addr   : 0x11005
Reg Formula: 0x11005
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC CRC Error and Buffer Full (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stkerr_crcbuf_Base                                                                0x11005

/*--------------------------------------
BitField Name: dcc_crc_err
BitField Type: W1C
BitField Desc: HDLC Packet has CRC error (OCN to ETH direction) . Alarm per
channel Bit[63] -> Bit[32]: channel ID 31 ->0
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_stkerr_crcbuf_dcc_crc_err_Mask                                                      cBit31_0
#define cAf6_upen_stkerr_crcbuf_dcc_crc_err_Shift                                                            0

/*--------------------------------------
BitField Name: dcc_buffull_err
BitField Type: W1C
BitField Desc: HDLC Packet buffer full (OCN to ETH direction) . Alarm per
channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[31]
-> Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_stkerr_crcbuf_dcc_buffull_err_Mask                                                  cBit31_0
#define cAf6_upen_stkerr_crcbuf_dcc_buffull_err_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : DCC_ETH2OCN_Alarm_Sticky
Reg Addr   : 0x12008
Reg Formula: 0x12008
    Where  : 
Reg Desc   : 
The register provides Alarms of ETH2OCN Direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stkerr_rx_eth2ocn_Base                                                            0x12008
#define cAf6Reg_upen_stkerr_rx_eth2ocn_WidthVal                                                             96

/*--------------------------------------
BitField Name: dcc_eth2ocn_buffull_err
BitField Type: W1C
BitField Desc: Packet buffer full (ETH to OCN direction) . Alarm per channel
Buffer of Ethernet Frame has been full. Some frames will be dropped Bit[68] ->
Bit[37]: channel ID 31 ->0
BitField Bits: [68:37]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_eth2ocn_buffull_err_01_Mask                                   cBit31_5
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_eth2ocn_buffull_err_01_Shift                                         5
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_eth2ocn_buffull_err_02_Mask                                    cBit4_0
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_eth2ocn_buffull_err_02_Shift                                         0

/*--------------------------------------
BitField Name: dcc_rxeth_maxlenerr
BitField Type: W1C
BitField Desc: Received DCC packet from SGMII port has violated maximum packet's
length error
BitField Bits: [36:36]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_rxeth_maxlenerr_Mask                                             cBit4
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_rxeth_maxlenerr_Shift                                                4

/*--------------------------------------
BitField Name: dcc_rxeth_crcerr
BitField Type: W1C
BitField Desc: Received DCC packet from SGMII port has CRC error
BitField Bits: [35:35]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_rxeth_crcerr_Mask                                                cBit3
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_rxeth_crcerr_Shift                                                   3

/*--------------------------------------
BitField Name: dcc_channel_disable
BitField Type: W1C
BitField Desc: DCC Local Channel Identifier mapping is disable Bit[34] ->
Bit[03] indicate channel 31-> 00.
BitField Bits: [34:03]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_channel_disable_01_Mask                                       cBit31_3
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_channel_disable_01_Shift                                             3
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_channel_disable_02_Mask                                        cBit2_0
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_channel_disable_02_Shift                                             0

/*--------------------------------------
BitField Name: dcc_cvlid_mismat
BitField Type: W1C
BitField Desc: Received 12b CVLAN ID value of DCC frame different from global
provisioned CVID
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_cvlid_mismat_Mask                                                cBit2
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_cvlid_mismat_Shift                                                   2

/*--------------------------------------
BitField Name: dcc_macda_mismat
BitField Type: W1C
BitField Desc: Received 43b MAC DA value of DCC frame different from global
provisioned DA
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_macda_mismat_Mask                                                cBit1
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_macda_mismat_Shift                                                   1

/*--------------------------------------
BitField Name: dcc_ethtp_mismat
BitField Type: W1C
BitField Desc: Received Ethernet Type of DCC frame different from global
provisioned value
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_ethtp_mismat_Mask                                                cBit0
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_ethtp_mismat_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Alarm_Status
Reg Addr   : 0x22012
Reg Formula: 0x22012
    Where  : 
Reg Desc   : 
The register provides current status of APS alarm

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_cur_err_Base                                                                0x22012
#define cAf6Reg_upen_k12rx_cur_err_WidthVal                                                                 32

/*--------------------------------------
BitField Name: per_channel_mismat
BitField Type: R_O
BitField Desc: Received CHANNEL ID value of APS frame different from configured
value. This is a per channel status. Bit 23 -> 08 represent for channel 15 -> 0
respectively
BitField Bits: [23:08]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_per_channel_mismat_Mask                                               cBit23_8
#define cAf6_upen_k12rx_cur_err_per_channel_mismat_Shift                                                     8

/*--------------------------------------
BitField Name: Frame_miss
BitField Type: RO
BitField Desc: Current status of APS FRAME missed alarm. When watchdog timer is
enable. This bit is set if no frame is received in pre-defined interval
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_Frame_miss_Mask                                                          cBit7
#define cAf6_upen_k12rx_cur_err_Frame_miss_Shift                                                             7

/*--------------------------------------
BitField Name: channel_miss
BitField Type: RO
BitField Desc: Current status of global CHANNEL mismatch alarm. Received
CHANNELID value different from configed
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_channel_miss_Mask                                                        cBit6
#define cAf6_upen_k12rx_cur_err_channel_miss_Shift                                                           6

/*--------------------------------------
BitField Name: real_len_miss
BitField Type: RO
BitField Desc: Current status of ACTUAL LENGH mismatch alarm. acket LENGTH value
different 0x60
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_real_len_miss_Mask                                                       cBit5
#define cAf6_upen_k12rx_cur_err_real_len_miss_Shift                                                          5

/*--------------------------------------
BitField Name: len_miss
BitField Type: RO
BitField Desc: Current status of LENGH mismatch alarm. Received LENGTH value
different 0x60
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_len_miss_Mask                                                            cBit4
#define cAf6_upen_k12rx_cur_err_len_miss_Shift                                                               4

/*--------------------------------------
BitField Name: ver_miss
BitField Type: RO
BitField Desc: Current status of VERSION  mismatch alarm. Received VERSION value
different from configed VER
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_ver_miss_Mask                                                            cBit3
#define cAf6_upen_k12rx_cur_err_ver_miss_Shift                                                               3

/*--------------------------------------
BitField Name: type_miss
BitField Type: RO
BitField Desc: Current status of TYPE mismatch alarm. Received TYPE value
different from configed TYPE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_type_miss_Mask                                                           cBit2
#define cAf6_upen_k12rx_cur_err_type_miss_Shift                                                              2

/*--------------------------------------
BitField Name: ethtype_miss
BitField Type: RO
BitField Desc: Current status of ETHERNET TYPE mismatch alarm. Received ETHTYPE
value different from configed value
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_ethtype_miss_Mask                                                        cBit1
#define cAf6_upen_k12rx_cur_err_ethtype_miss_Shift                                                           1

/*--------------------------------------
BitField Name: mac_da_miss
BitField Type: RO
BitField Desc: DA mismatch. Received DA value different from configed DA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_mac_da_miss_Mask                                                         cBit0
#define cAf6_upen_k12rx_cur_err_mac_da_miss_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Min_Pkt_Length_Alarm_Status
Reg Addr   : 0x11008
Reg Formula: 0x11008
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_pktlen1_Base                                                               0x11008

/*--------------------------------------
BitField Name: dcc_undsize_err
BitField Type: RO
BitField Desc: HDLC Length Undersize Error. Status per channel Received HDLC
Frame from OCN has frame length below minimum allowed length (2bytes) Bit[31] ->
Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_curerr_pktlen1_dcc_undsize_err_Mask                                                 cBit31_0
#define cAf6_upen_curerr_pktlen1_dcc_undsize_err_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Max_Pkt_Length_Alarm_Status
Reg Addr   : 0x11009
Reg Formula: 0x11009
    Where  : 
Reg Desc   : 
The register provides Alarm Status Related to HDLC Length Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_pktlen2_Base                                                               0x11009

/*--------------------------------------
BitField Name: dcc_overize_err
BitField Type: RO
BitField Desc: HDLC Length Oversize Error. Status per channel Received HDLC
Frame from OCN has frame length over maximum allowed length (1536bytes) Bit[31]
-> Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_curerr_pktlen2_dcc_overize_err_Mask                                                 cBit31_0
#define cAf6_upen_curerr_pktlen2_dcc_overize_err_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Pkt_CRC_Error_Alarm_Status
Reg Addr   : 0x1100A
Reg Formula: 0x1100A
    Where  : 
Reg Desc   : 
The register provides Alarm Status Related to HDLC CRC Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_crcbuf_Base                                                                0x1100A

/*--------------------------------------
BitField Name: dcc_crc_err
BitField Type: RO
BitField Desc: HDLC Packet has CRC error (OCN to ETH direction) . Alarm per
channel Bit[31] -> Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_curerr_crcbuf_dcc_crc_err_Mask                                                      cBit31_0
#define cAf6_upen_curerr_crcbuf_dcc_crc_err_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Buffer_Full_Alarm_Status
Reg Addr   : 0x1100B
Reg Formula: 0x1100B
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC Buffer Full (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_bufept_Base                                                                0x1100B

/*--------------------------------------
BitField Name: dcc_buffull_err
BitField Type: RO
BitField Desc: HDLC Packet buffer full (OCN to ETH direction) . Alarm per
channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[31]
-> Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_curerr_bufept_dcc_buffull_err_Mask                                                  cBit31_0
#define cAf6_upen_curerr_bufept_dcc_buffull_err_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : DCC_ETH2OCN_Alarm_Status
Reg Addr   : 0x1200A
Reg Formula: 0x1200A
    Where  : 
Reg Desc   : 
The register provides Alarms of ETH2OCN Direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_rx_glb_sta_Base                                                               0x1200A
#define cAf6Reg_upen_dcc_rx_glb_sta_WidthVal                                                                96

/*--------------------------------------
BitField Name: sta_dcc_eth2ocn_buffull_err
BitField Type: RO
BitField Desc: Packet buffer full (ETH to OCN direction) . Status per channel
Buffer of Ethernet Frame has been full. Some frames will be dropped Bit[67] ->
Bit[36]: channel ID 31 ->0
BitField Bits: [67:36]
--------------------------------------*/
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_buffull_err_01_Mask                                  cBit31_4
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_buffull_err_01_Shift                                        4
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_buffull_err_02_Mask                                   cBit3_0
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_eth2ocn_buffull_err_02_Shift                                        0

/*--------------------------------------
BitField Name: sta_dcc_rxeth_maxlenerr
BitField Type: RO
BitField Desc: "Received DCC packet from SGMII port has violated maximum
packet's length error" status
BitField Bits: [35:35]
--------------------------------------*/
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_rxeth_maxlenerr_Mask                                            cBit3
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_rxeth_maxlenerr_Shift                                               3

/*--------------------------------------
BitField Name: sta_dcc_channel_disable
BitField Type: RO
BitField Desc: "DCC Local Channel Identifier mapping is disable" status Bit[34]
-> Bit[03] indicate channel 31-> 00.
BitField Bits: [34:03]
--------------------------------------*/
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_channel_disable_01_Mask                                      cBit31_3
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_channel_disable_01_Shift                                            3
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_channel_disable_02_Mask                                       cBit2_0
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_channel_disable_02_Shift                                            0

/*--------------------------------------
BitField Name: sta_dcc_cvlid_mismat
BitField Type: RO
BitField Desc: Received 12b CVLAN ID value of DCC frame different from global
provisioned CVID
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_cvlid_mismat_Mask                                               cBit2
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_cvlid_mismat_Shift                                                  2

/*--------------------------------------
BitField Name: sta_dcc_macda_mismat
BitField Type: RO
BitField Desc: Received 43b MAC DA value of DCC frame different from global
provisioned DA
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_macda_mismat_Mask                                               cBit1
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_macda_mismat_Shift                                                  1

/*--------------------------------------
BitField Name: sta_dcc_ethtp_mismat
BitField Type: RO
BitField Desc: Received Ethernet Type of DCC frame different from global
provisioned value
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_ethtp_mismat_Mask                                               cBit0
#define cAf6_upen_dcc_rx_glb_sta_sta_dcc_ethtp_mismat_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Trig_En_Cap
Reg Addr   : 0x22006
Reg Formula: 0x22006
    Where  : 
Reg Desc   : 
The register provides WatchDog Timer configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_trig_encap_Base                                                             0x22006
#define cAf6Reg_upen_k12rx_trig_encap_WidthVal                                                              32

/*--------------------------------------
BitField Name: Enable
BitField Type: RW
BitField Desc: Trigger Enable Capturing Ethernet Header - Write '0' first, then
write '1' to enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_k12rx_trig_encap_Enable_Mask                                                           cBit0
#define cAf6_upen_k12rx_trig_encap_Enable_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Packet_Header_Cap_Reg0
Reg Addr   : 0x22200 - 0x2220C
Reg Formula: 0x22200 + 3*$pktnum
    Where  : 
           + $pktnum(0-4): Number of packet capture
Reg Desc   : 
The register provides captured header data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_cap_reg0_Base                                                               0x22200

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RO
BitField Desc: Captured MAC DA value
BitField Bits: [63:16]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg0_MAC_DA_01_Mask                                                      cBit31_16
#define cAf6_upen_k12rx_cap_reg0_MAC_DA_01_Shift                                                            16
#define cAf6_upen_k12rx_cap_reg0_MAC_DA_02_Mask                                                       cBit31_0
#define cAf6_upen_k12rx_cap_reg0_MAC_DA_02_Shift                                                             0

/*--------------------------------------
BitField Name: MAC_SA
BitField Type: RO
BitField Desc: 16b MSB of Captured MAC SA value
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg0_MAC_SA_Mask                                                          cBit15_0
#define cAf6_upen_k12rx_cap_reg0_MAC_SA_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Packet_Header_Cap_Reg1
Reg Addr   : 0x22201 - 0x2220D
Reg Formula: 0x22201 + 3*$pktnum
    Where  : 
           + $pktnum(0-4): Number of packet capture
Reg Desc   : 
The register provides captured header data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_cap_reg1_Base                                                               0x22201

/*--------------------------------------
BitField Name: MAC_SA
BitField Type: RO
BitField Desc: 32b MSB of Captured MAC SA value
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg1_MAC_SA_Mask                                                          cBit31_0
#define cAf6_upen_k12rx_cap_reg1_MAC_SA_Shift                                                                0

/*--------------------------------------
BitField Name: VLAN
BitField Type: RO
BitField Desc: Captured VLAN value:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg1_VLAN_Mask                                                            cBit31_0
#define cAf6_upen_k12rx_cap_reg1_VLAN_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Packet_Header_Cap_Reg2
Reg Addr   : 0x22202 - 0x2220E
Reg Formula: 0x22202 + 3*$pktnum
    Where  : 
           + $pktnum(0-4): Number of packet capture
Reg Desc   : 
The register provides captured header data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_cap_reg2_Base                                                               0x22202

/*--------------------------------------
BitField Name: ETH_TYP
BitField Type: RO
BitField Desc: 16b value of Ethernet Type
BitField Bits: [63:48]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg2_ETH_TYP_Mask                                                        cBit31_16
#define cAf6_upen_k12rx_cap_reg2_ETH_TYP_Shift                                                              16

/*--------------------------------------
BitField Name: VER
BitField Type: RO
BitField Desc: 8b Version
BitField Bits: [47:40]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg2_VER_Mask                                                             cBit15_8
#define cAf6_upen_k12rx_cap_reg2_VER_Shift                                                                   8

/*--------------------------------------
BitField Name: TYP
BitField Type: RO
BitField Desc: 8b APS Type
BitField Bits: [39:32]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg2_TYP_Mask                                                              cBit7_0
#define cAf6_upen_k12rx_cap_reg2_TYP_Shift                                                                   0

/*--------------------------------------
BitField Name: LENGTH
BitField Type: RO
BitField Desc: 16b packet length
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg2_LENGTH_Mask                                                         cBit31_16
#define cAf6_upen_k12rx_cap_reg2_LENGTH_Shift                                                               16


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Unknow_From_SGMII_Port0
Reg Addr   : 0x22024
Reg Formula: 0x22024
    Where  : 
Reg Desc   : 
Counter of number of Unknow Packet (not APS packet) receive from SGMII port 0

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12_glbcnt_sfail_Base                                                             0x22024

/*--------------------------------------
BitField Name: glb_sgm2ocn_unk_counter
BitField Type: WC
BitField Desc: Counter of unknow receive packet from SGMII port 0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12_glbcnt_sfail_glb_sgm2ocn_unk_counter_Mask                                       cBit31_0
#define cAf6_upen_k12_glbcnt_sfail_glb_sgm2ocn_unk_counter_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Rx_APS_From_SGMII_Port0
Reg Addr   : 0x22025
Reg Formula: 0x22025
    Where  : 
Reg Desc   : 
Counter of total number of detected APS Packet receive from SGMII port 0

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12_glbcnt_sok_Base                                                               0x22025
#define cAf6Reg_upen_k12_glbcnt_sok_matched_etype_Base                                                 0x22021

/*--------------------------------------
BitField Name: glb_sgm2ocn_sok_counter
BitField Type: WC
BitField Desc: Counter of detected APS packet from SGMII port 0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12_glbcnt_sok_glb_sgm2ocn_sok_counter_Mask                                         cBit31_0
#define cAf6_upen_k12_glbcnt_sok_glb_sgm2ocn_sok_counter_Shift                                               0


/*-------------------------------------
# 2.3.1.6 Counter Total RX APS Packet from SGMII Port
# ******************
// Register Full Name: Counter_Total_Rx_APS_From_SGMII_Port0
// RTL Instant Name  : upen_k12_glbcnt_sok
// Address: 0x22025
// Formula: Address
// Where:
// Description: Counter of total number of detected APS Packet receive from SGMII port 0
// Width: 32
// Register Type: {Config}
// Field: [31:0]    %% glb_sgm2ocn_sok_counter  %% Counter of detected APS packet from SGMII port 0     %% WC   %% 0x0      %% 0x0
----------------------------------------*/
#define cAf6Reg_upen_k12_glbcnt_sok_V3_Base                                                            0x22025

/*--------------------------------------
# 2.3.1.8 Counter RX Discared APS Packet from SGMII Port
# ******************
// Register Full Name: Counter_Rx_Discarded_APS_From_SGMII_Port0
// RTL Instant Name  : upen_k12_glbcnt_serr
// Address: 0x22026
// Formula: Address
// Where:
// Description: Counter of number of Unknow Packet (not APS packet) receive from SGMII port 0
// Width: 32
// Register Type: {Config}
// Field: [31:0]    %% glb_sgm2ocn_serr_counter     %% Counter of discarded APS packets received from SGMII port 0
//                                                     Ethernet Type of packet is detect as Kbyte packet but
//                                                     the packet has fail other conditions to extract Kbyte value in packet     %% WC  %% 0x0      %% 0x0
------------------------------------------*/
#define cAf6Reg_upen_k12_glbcnt_serr_V3_Base                                                           0x22026


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_APS_From_SGMII_Port0_Per_Chan
Reg Addr   : 0x22300 - 0x2230F
Reg Formula: 0x22300 + $channelID
    Where  : 
           + $channelID(0-15): APS channelID
Reg Desc   : 
Counter of number of detected APS Packet per channel receive from SGMII port 0 per channelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12_sokpkt_pcid_Base                                                              0x22300

/*--------------------------------------
BitField Name: pcid_sgm2ocn_sok_counter
BitField Type: WC
BitField Desc: Counter of APS packets receive from SGMII port 0 per channelID
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12_sokpkt_pcid_pcid_sgm2ocn_sok_counter_Mask                                       cBit31_0
#define cAf6_upen_k12_sokpkt_pcid_pcid_sgm2ocn_sok_counter_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_APS_BYTE_From_RxOcn_Per_Chan
Reg Addr   : 0x21330 - 0x2133F
Reg Formula: 0x21330 + $channelID
    Where  : 
           + $channelID(0-15): APS channelID
Reg Desc   : 
Counter number of BYTE of APS packet receive from RX-OCN Per Channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_byt2k12_pcid_Base                                                                 0x21330

/*--------------------------------------
BitField Name: byte_counter
BitField Type: WC
BitField Desc: Counter number of BYTE of APS packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_byt2k12_pcid_byte_counter_Mask                                                      cBit31_0
#define cAf6_upen_byt2k12_pcid_byte_counter_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Rx_APS_Packet_From_RxOcn
Reg Addr   : 0x21360
Reg Formula: 
    Where  : 
Reg Desc   : 
Counter Total number of RX APS packet receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_glbsop2k12_Base                                                                   0x21360

/*--------------------------------------
BitField Name: rx_aps_pkt_cnt
BitField Type: WC
BitField Desc: Counter number of APS packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_glbsop2k12_rx_aps_pkt_cnt_Mask                                                      cBit31_0
#define cAf6_upen_glbsop2k12_rx_aps_pkt_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_APS_BYTE_From_RxOcn_Per_Chan
Reg Addr   : 0x21361
Reg Formula: 
    Where  : 
Reg Desc   : 
Counter number of Total BYTE of APS packet receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_glbbyt2k12_Base                                                                   0x21361

/*--------------------------------------
BitField Name: rx_aps_byte_counter
BitField Type: WC
BitField Desc: Counter number of BYTE of APS packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_glbbyt2k12_rx_aps_byte_counter_Mask                                                 cBit31_0
#define cAf6_upen_glbbyt2k12_rx_aps_byte_counter_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_APS_SOP_To_SGMII
Reg Addr   : 0x21350
Reg Formula: 
    Where  : 
Reg Desc   : 
Counter number of SOP of TX APS packet to SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12sop2sgm_pcid_Base                                                              0x21350

/*--------------------------------------
BitField Name: sop_counter
BitField Type: WC
BitField Desc: Counter number of SOP of TX APS packets to SGMII
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12sop2sgm_pcid_sop_counter_Mask                                                    cBit31_0
#define cAf6_upen_k12sop2sgm_pcid_sop_counter_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_APS_EOP_To_SGMII
Reg Addr   : 0x21351
Reg Formula: 
    Where  : 
Reg Desc   : 
Counter number of EOP of TX APS packet to SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12eop2sgm_pcid_Base                                                              0x21351

/*--------------------------------------
BitField Name: eop_counter
BitField Type: WC
BitField Desc: Counter number of EOP of TX APS packets to SGMII
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12eop2sgm_pcid_eop_counter_Mask                                                    cBit31_0
#define cAf6_upen_k12eop2sgm_pcid_eop_counter_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_APS_BYTE_To_SGMII
Reg Addr   : 0x21352
Reg Formula: 
    Where  : 
Reg Desc   : 
Counter number of BYTE of TX APS packet to SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12byt2sgm_pcid_Base                                                              0x21352

/*--------------------------------------
BitField Name: byte_counter
BitField Type: WC
BitField Desc: Counter number of BYTE of TX APS packets to SGMII
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12byt2sgm_pcid_byte_counter_Mask                                                   cBit31_0
#define cAf6_upen_k12byt2sgm_pcid_byte_counter_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Sop_From_SGMII_Port
Reg Addr   : 0x12020
Reg Formula: 0x12020
    Where  : 
Reg Desc   : 
Counter of number of SOP receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxsop_Base                                                          0x12020

/*--------------------------------------
BitField Name: sop_counter
BitField Type: WC
BitField Desc: Counter of SOP receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxsop_sop_counter_Mask                                                cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxsop_sop_counter_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Eop_From_SGMII_Port
Reg Addr   : 0x12021
Reg Formula: 0x12021
    Where  : 
Reg Desc   : 
Counter of number of EOP receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxeop_Base                                                          0x12021

/*--------------------------------------
BitField Name: eop_counter
BitField Type: WC
BitField Desc: Counter of EOP receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxeop_eop_counter_Mask                                                cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxeop_eop_counter_Shift                                                      0

/*--------------------------------------
# 2.3.3.6 Counter RX Passed DCC Packet from SGMII Port
# ******************
// Register Full Name: Counter_Rx_Passed_From_SGMII_Port
// RTL Instant Name  : upen_dcc_glbcnt_sgmrxpass
// Address: 0x12031
// Formula: Address
// Where:
// Description: Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1
// Width: 32
// Register Type: {Config}
// Field: [31:0]    %% pass_counter     %% Counter of passed received DCC packet from SGMII port 1
//                                         .Packet is detected as DCC packet and classify into channel successfully  %% WC  %% 0x0      %% 0x0
---------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxpass_Base                                                          0x12031

/*-------------------------------------------
# 2.3.3.7 Counter RX Discarded DCC Packet from SGMII Port
# ******************
// Register Full Name: Counter_Rx_Discarded_From_SGMII_Port
// RTL Instant Name  : upen_dcc_glbcnt_sgmrxdisc
// Address: 0x12033
// Formula: Address
// Where:
// Description: Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1
// Width: 32
// Register Type: {Config}
// Field: [31:0]    %% disc_counter     %% Counter of discarded received DCC packet from SGMII port 1
//                                         .Packet is detected as DCC packet but fail other conditions to classify into channel   %% WC %% 0x0      %% 0x0
---------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxdisc_Base                                                          0x12033

/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Err_From_SGMII_Port
Reg Addr   : 0x12022
Reg Formula: 0x12022
    Where  : 
Reg Desc   : 
Counter of number of ERR receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxerr_Base                                                          0x12022

/*--------------------------------------
BitField Name: err_counter
BitField Type: WC
BitField Desc: Counter of ERR receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxerr_err_counter_Mask                                                cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxerr_err_counter_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Byte_From_SGMII_Port
Reg Addr   : 0x12023
Reg Formula: 0x12023
    Where  : 
Reg Desc   : 
Counter of number of BYTE receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxbyt_Base                                                          0x12023

/*--------------------------------------
BitField Name: byte_counter
BitField Type: WC
BitField Desc: Counter of BYTE receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxbyt_byte_counter_Mask                                               cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxbyt_byte_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Unknow_From_SGMII_Port
Reg Addr   : 0x12024
Reg Formula: 0x12024
    Where  : 
Reg Desc   : 
Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxfail_Base                                                         0x12024

/*--------------------------------------
BitField Name: unk_counter
BitField Type: WC
BitField Desc: Counter of unknow receive packet from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxfail_unk_counter_Mask                                               cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxfail_unk_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_Pkt_From_SGMII_Port1_Per_Chan
Reg Addr   : 0x12100 - 0x1211F
Reg Formula: 0x12100 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter of number of DCC Packet From SGMII Port Per Channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_sokpkt_pcid_Base                                                              0x12100

/*--------------------------------------
BitField Name: dcc_pkt_cnt
BitField Type: WC
BitField Desc: Counter of DCC packets receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_sokpkt_pcid_dcc_pkt_cnt_Mask                                                    cBit31_0
#define cAf6_upen_dcc_sokpkt_pcid_dcc_pkt_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_Byte_From_SGMII_Port1_Per_Chan
Reg Addr   : 0x121C0 - 0x121DF
Reg Formula: 0x121C0 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter of number of DCC Bytes From SGMII Port Per Channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_sokbyt_pcid_Base                                                              0x121C0

/*--------------------------------------
BitField Name: dcc_byte_cnt
BitField Type: WC
BitField Desc: Counter of DCC Bytes receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_sokbyt_pcid_dcc_byte_cnt_Mask                                                   cBit31_0
#define cAf6_upen_dcc_sokbyt_pcid_dcc_byte_cnt_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_SOP_From_RxOcn_Per_Chan
Reg Addr   : 0x11400 - 0x1141F
Reg Formula: 0x11400 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of SOP of DCC packet receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sop2dcc_pcid_Base                                                                 0x11400

/*--------------------------------------
BitField Name: sop_ocn2sgm_cnt_pcid
BitField Type: WC
BitField Desc: Counter number of SOP of DCC packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_sop2dcc_pcid_sop_ocn2sgm_cnt_pcid_Mask                                              cBit31_0
#define cAf6_upen_sop2dcc_pcid_sop_ocn2sgm_cnt_pcid_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_EOP_From_RxOcn_Per_Chan
Reg Addr   : 0x11420 - 0x1143F
Reg Formula: 0x11420 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of EOP of DCC packet receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eop2dcc_pcid_Base                                                                 0x11420

/*--------------------------------------
BitField Name: eop_ocn2sgm_cnt_pcid
BitField Type: WC
BitField Desc: Counter number of EOP of DCC packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_eop2dcc_pcid_eop_ocn2sgm_cnt_pcid_Mask                                              cBit31_0
#define cAf6_upen_eop2dcc_pcid_eop_ocn2sgm_cnt_pcid_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_ERR_From_RxOcn_Per_Chan
Reg Addr   : 0x11440 - 0x1145F
Reg Formula: 0x11440 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of ERR of DCC packet receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_err2dcc_pcid_Base                                                                 0x11440

/*--------------------------------------
BitField Name: err_ocn2sgm_cnt_pcid
BitField Type: WC
BitField Desc: Counter number of ERR of DCC packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_err2dcc_pcid_err_ocn2sgm_cnt_pcid_Mask                                              cBit31_0
#define cAf6_upen_err2dcc_pcid_err_ocn2sgm_cnt_pcid_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_BYTE_From_RxOcn_Per_Chan
Reg Addr   : 0x11460 - 0x1147F
Reg Formula: 0x11460 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of BYTE of DCC packet receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_byt2dcc_pcid_Base                                                                 0x11460

/*--------------------------------------
BitField Name: byt_ocn2sgm_cnt_pcid
BitField Type: WC
BitField Desc: Counter number of BYTE of DCC packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_byt2dcc_pcid_byt_ocn2sgm_cnt_pcid_Mask                                              cBit31_0
#define cAf6_upen_byt2dcc_pcid_byt_ocn2sgm_cnt_pcid_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_DCC_Drp_From_RxOcn_Per_Chan
Reg Addr   : 0x11480	  - 0x1149F
Reg Formula: 0x11480 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of Drop of DCC packet (from OCN to ETH direction) due to Buffer full

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_errfull_pcid_Base                                                                 0x11480

/*--------------------------------------
BitField Name: errfull_ocn2sgm_cnt_pcid
BitField Type: WC
BitField Desc: Counter number of Dropped DCC packets receive from OCN due to
buffer full
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_errfull_pcid_errfull_ocn2sgm_cnt_pcid_Mask                                          cBit31_0
#define cAf6_upen_errfull_pcid_errfull_ocn2sgm_cnt_pcid_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Sop_From_OCN
Reg Addr   : 0x11504
Reg Formula: 0x11504
    Where  : 
Reg Desc   : 
Counter of number of SOP receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocnrxsop_Base                                                          0x11504

/*--------------------------------------
BitField Name: sop_ocn2sgm_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total SOP receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocnrxsop_sop_ocn2sgm_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocnrxsop_sop_ocn2sgm_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Eop_From_OCN
Reg Addr   : 0x11505
Reg Formula: 0x11505
    Where  : 
Reg Desc   : 
Counter of number of EOP receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocnrxeop_Base                                                          0x11505

/*--------------------------------------
BitField Name: eop_ocn2sgm_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total EOP receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocnrxeop_eop_ocn2sgm_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocnrxeop_eop_ocn2sgm_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Err_From_OCN
Reg Addr   : 0x11506
Reg Formula: 0x11506
    Where  : 
Reg Desc   : 
Counter of number of ERR receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocnrxerr_Base                                                          0x11506

/*--------------------------------------
BitField Name: err_ocn2sgm_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total ERR  receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocnrxerr_err_ocn2sgm_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocnrxerr_err_ocn2sgm_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Byte_From_OCN
Reg Addr   : 0x11507
Reg Formula: 0x11507
    Where  : 
Reg Desc   : 
Counter of number of BYTE receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocnrxbyt_Base                                                          0x11507

/*--------------------------------------
BitField Name: byt_ocn2sgm_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total RX BYTE from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocnrxbyt_byt_ocn2sgm_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocnrxbyt_byt_ocn2sgm_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_DCC_SOP_To_SGMII_Per_Chan
Reg Addr   : 0x114A0 - 0x114BF
Reg Formula: 0x114A0 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of SOP of TX DCC packet to SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccsop2sgm_pcid_Base                                                              0x114A0

/*--------------------------------------
BitField Name: sop_counter
BitField Type: WC
BitField Desc: Counter number of SOP of TX DCC packets to SGMII
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dccsop2sgm_pcid_sop_counter_Mask                                                    cBit31_0
#define cAf6_upen_dccsop2sgm_pcid_sop_counter_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_DCC_EOP_To_SGMII_Per_Chan
Reg Addr   : 0x114C0 - 0x114DF
Reg Formula: 0x114C0 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of EOP of TX DCC packet to SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcceop2sgm_pcid_Base                                                              0x114C0

/*--------------------------------------
BitField Name: eop_counter
BitField Type: WC
BitField Desc: Counter number of EOP of TX DCC packets to SGMII
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcceop2sgm_pcid_eop_counter_Mask                                                    cBit31_0
#define cAf6_upen_dcceop2sgm_pcid_eop_counter_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_DCC_BYTE_To_SGMII_Per_Chan
Reg Addr   : 0x114E0 - 0x114FF
Reg Formula: 0x114E0 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of BYTE of TX DCC packet to SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccbyt2sgm_pcid_Base                                                              0x114E0

/*--------------------------------------
BitField Name: byte_counter
BitField Type: WC
BitField Desc: Counter number of BYTE of TX DCC packets to SGMII
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dccbyt2sgm_pcid_byte_counter_Mask                                                   cBit31_0
#define cAf6_upen_dccbyt2sgm_pcid_byte_counter_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Sop_To_SGMII_Port1
Reg Addr   : 0x11500
Reg Formula: 0x11500
    Where  : 
Reg Desc   : 
Counter of number of SOP transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxsop_Base                                                          0x11500

/*--------------------------------------
BitField Name: sgm_txglb_sop_counter
BitField Type: WC
BitField Desc: Counter of SOP transmit to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxsop_sgm_txglb_sop_counter_Mask                                      cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxsop_sgm_txglb_sop_counter_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Eop_To_SGMII_Port1
Reg Addr   : 0x11501
Reg Formula: 0x11501
    Where  : 
Reg Desc   : 
Counter of number of EOP transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxeop_Base                                                          0x11501
#define cAf6Reg_upen_dcc_glbcnt_sgmtxeop_V2_Base                                                       0x11011

/*--------------------------------------
BitField Name: sgm_txglb_eop_counter
BitField Type: WC
BitField Desc: Counter of EOP transmit to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxeop_sgm_txglb_eop_counter_Mask                                      cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxeop_sgm_txglb_eop_counter_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Err_To_SGMII_Port1
Reg Addr   : 0x11502
Reg Formula: 0x11502
    Where  : 
Reg Desc   : 
Counter of number of ERR transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxerr_Base                                                          0x11502
#define cAf6Reg_upen_dcc_glbcnt_sgmtxerr_V2_Base                                                       0x11012

/*--------------------------------------
BitField Name: sgm_txglb_err_counter
BitField Type: WC
BitField Desc: Counter of ERR  transmit to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxerr_sgm_txglb_err_counter_Mask                                      cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxerr_sgm_txglb_err_counter_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Byte_To_SGMII_Port1
Reg Addr   : 0x11503
Reg Formula: 0x11503
    Where  : 
Reg Desc   : 
Counter of number of BYTE transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxbyt_Base                                                          0x11503
#define cAf6Reg_upen_dcc_glbcnt_sgmtxbyt_V2_Base                                                       0x11013

/*--------------------------------------
BitField Name: sgm_txglb_byte_counter
BitField Type: WC
BitField Desc: Counter of TX BYTE to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxbyt_sgm_txglb_byte_counter_Mask                                     cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxbyt_sgm_txglb_byte_counter_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter_DCC_SOP_to_RxOcn_Per_Chan
Reg Addr   : 0x12120 - 0x1213f
Reg Formula: 0x12120 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of SOP of DCC packet transmit to RX-OCN Per ChannelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_sop2ocn_pcid_Base                                                                 0x12120

/*--------------------------------------
BitField Name: sop_sgm2ocn_counter
BitField Type: WC
BitField Desc: Counter number of SOP of DCC packets transmit to RX-OCN  Per
ChannelID
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_sop2ocn_pcid_sop_sgm2ocn_counter_Mask                                               cBit31_0
#define cAf6_upen_sop2ocn_pcid_sop_sgm2ocn_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_DCC_EOP_to_RxOcn_Per_Chan
Reg Addr   : 0x12140 - 0x1215F
Reg Formula: 0x12140 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of EOP of DCC packet transmit to RX-OCN Per ChannelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_eop2ocn_pcid_Base                                                                 0x12140

/*--------------------------------------
BitField Name: eop_sgm2ocn_counter
BitField Type: WC
BitField Desc: Counter number of EOP of DCC packets transmit to RX-OCN Per
ChannelID
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_eop2ocn_pcid_eop_sgm2ocn_counter_Mask                                               cBit31_0
#define cAf6_upen_eop2ocn_pcid_eop_sgm2ocn_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_DCC_ERR_to_RxOcn_Per_Chan
Reg Addr   : 0x12160 - 0x1217F
Reg Formula: 0x12160 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of ERR of DCC packet transmit to RX-OCN Per ChannelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_err2ocn_pcid_Base                                                                 0x12160

/*--------------------------------------
BitField Name: err_sgm2ocn_counter
BitField Type: WC
BitField Desc: Counter number of ERR of DCC packets transmit to RX-OCN Per
ChannelID
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_err2ocn_pcid_err_sgm2ocn_counter_Mask                                               cBit31_0
#define cAf6_upen_err2ocn_pcid_err_sgm2ocn_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_DCC_EPT_to_RxOcn_Per_Chan
Reg Addr   : 0x12180 - 0x1219F
Reg Formula: 0x12180 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of EPT of DCC packet transmit to RX-OCN Per ChannelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ept2ocn_pcid_Base                                                                 0x12180

/*--------------------------------------
BitField Name: ept_sgm2ocn_counter
BitField Type: WC
BitField Desc: Counter number of EPT signal respond to request from RX-OCN Per
ChannelID. An empty indicate data is not available when OCN request DCC data
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_ept2ocn_pcid_ept_sgm2ocn_counter_Mask                                               cBit31_0
#define cAf6_upen_ept2ocn_pcid_ept_sgm2ocn_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_DCC_BYTE_to_RxOcn_Per_Chan
Reg Addr   : 0x121A0 - 0x121BF
Reg Formula: 0x121A0 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of BYTE of DCC packet transmit to RX-OCN Per ChannelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_byt2ocn_pcid_Base                                                                 0x121A0

/*--------------------------------------
BitField Name: byt_sgm2ocn_counter
BitField Type: WC
BitField Desc: Counter number of BYTE of DCC packets transmit to RX-OCN Per
ChannelID
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_byt2ocn_pcid_byt_sgm2ocn_counter_Mask                                               cBit31_0
#define cAf6_upen_byt2ocn_pcid_byt_sgm2ocn_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_DCC_Drop_to_RxOcn_Per_Chan
Reg Addr   : 0x121E0 - 0x121FF
Reg Formula: 0x121E0 + $channelID
    Where  : 
           + $channelID(0-31): DCC channelID
Reg Desc   : 
Counter number of Drop DCC packet transmit to RX-OCN Due to Buffer Full Per ChannelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_ful2ocn_pcid_Base                                                                 0x121E0

/*--------------------------------------
BitField Name: drp_sgm2ocn_counter
BitField Type: WC
BitField Desc: Counter number of Drop DCC packets transmit to RX-OCN due to
Buffer Full Per ChannelID
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_ful2ocn_pcid_drp_sgm2ocn_counter_Mask                                               cBit31_0
#define cAf6_upen_ful2ocn_pcid_drp_sgm2ocn_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Tx_Sop_To_OCN
Reg Addr   : 0x12025
Reg Formula: 0x12025
    Where  : 
Reg Desc   : 
Counter Total number of SOP transmit to RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocntxsop_Base                                                          0x12025

/*--------------------------------------
BitField Name: sop_sgm2ocn_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total SOP transmit to RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocntxsop_sop_sgm2ocn_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocntxsop_sop_sgm2ocn_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Tx_Eop_To_OCN
Reg Addr   : 0x12026
Reg Formula: 0x12026
    Where  : 
Reg Desc   : 
Counter Total number of EOP transmit to RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocntxeop_Base                                                          0x12026

/*--------------------------------------
BitField Name: eop_sgm2ocn_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total EOP transmit to RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocntxeop_eop_sgm2ocn_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocntxeop_eop_sgm2ocn_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Tx_Err_To_OCN
Reg Addr   : 0x12027
Reg Formula: 0x12027
    Where  : 
Reg Desc   : 
Counter Total number of ERR transmit to RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocntxerr_Base                                                          0x12027

/*--------------------------------------
BitField Name: err_sgm2ocn_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total ERR transmit to RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocntxerr_err_sgm2ocn_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocntxerr_err_sgm2ocn_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Tx_Byte_To_OCN
Reg Addr   : 0x12028
Reg Formula: 0x12028
    Where  : 
Reg Desc   : 
Counter Total number of BYTE transmit to RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ocntxbyt_Base                                                          0x12028

/*--------------------------------------
BitField Name: byt_sgm2ocn_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total BYTE transmit to RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ocntxbyt_byt_sgm2ocn_cnt_glb_Mask                                        cBit31_0
#define cAf6_upen_dcc_glbcnt_ocntxbyt_byt_sgm2ocn_cnt_glb_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Drop_To_RxOcn_Pkt
Reg Addr   : 0x12029
Reg Formula: 0x12029
    Where  : 
Reg Desc   : 
Counter Total number Packet Drop due to buffer full (ETH2OCN Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_ful2ocn_Base                                                           0x12029

/*--------------------------------------
BitField Name: drp_pkt_sgm2ocn_cnt_glb
BitField Type: WC
BitField Desc: Counter of Total Packet Dropped Due to Buffer full (ETH to RX-
OCN)
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_ful2ocn_drp_pkt_sgm2ocn_cnt_glb_Mask                                     cBit31_0
#define cAf6_upen_dcc_glbcnt_ful2ocn_drp_pkt_sgm2ocn_cnt_glb_Shift                                           0

/*--------------------------------------------------------------------------
Register Full Name: Counter_Packet_And_Byte_Per_Channel
RTL Instant Name  : upen_dcc_cnt
Address: 0x13000 - 0x13FFF
Formula: Address + $cnt_type*64 + $channelID
Where: {$cnt_type(1-14): counter type}%%{$channelID(0-47): channel value}
Description: Counter of DCC at different point represent by cnt_type value:
{1} : counter RX bytes from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction
{2} : counter RX good packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction
{3} : counter RX error packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction
{4} : counter RX lost packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction
{5} : counter TX bytes from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction
{6} : counter TX packets from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction
{7} : counter TX error packet from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction
{8} : counter RX bytes from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction
{9} : counter RX good packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction
{10}: counter RX error packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction
{11}: counter RX lost packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction
{12}: counter TX bytes from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction
{13}: counter TX good packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction
{14}: counter TX error packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction
-------------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cnt_V2_Base                                                                   0x13000
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_byte       1
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_good_pkt   2
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_error_pkt  3
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_lost_pkt   4
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_BUF2SGM_byte       5
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_BUF2SGM_pkt        6
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_BUF2SGM_error_pkt  7
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGM2BUF_byte       8
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGM2BUF_good_pkt   9
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGM2BUF_error_pkt  10
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGMI2BUF_lost_pkt  11
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_BUF2ENC_byte       12
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_BUF2ENC_good_pkt   13
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_BUF2ENC_error_pkt  14


/*------------------------------------------------------------------------------
Reg Name   : CONFIG HDLC LO DEC
Reg Addr   : 0x04000 - 0x0401F
Reg Formula: 0x04000+ $CID
    Where  : 
           + $CID (0-31) : Channel ID
Reg Desc   : 
config HDLC ID 0-31
HDL_PATH: iaf6cci0012_lodec_core.ihdlc_cfg.imem113x.ram.ram[$CID]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_locfg_Base                                                                   0x04000
#define cAf6Reg_upen_hdlc_locfg_WidthVal                                                                    32

/*--------------------------------------
BitField Name: cfg_scren
BitField Type: R/W
BitField Desc: config to enable scramble, (1) is enable, (0) is disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_scren_Mask                                                              cBit4
#define cAf6_upen_hdlc_locfg_cfg_scren_Shift                                                                 4

/*--------------------------------------
BitField Name: cfg_bitstuff
BitField Type: R/W
BitField Desc: config to select bit stuff or byte sutff, (1) is bit stuff, (0)
is byte stuff
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_Mask                                                           cBit2
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_Shift                                                              2

/*--------------------------------------
BitField Name: cfg_fcsmsb
BitField Type: R/W
BitField Desc: config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_Mask                                                             cBit1
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_Shift                                                                1

/*--------------------------------------
BitField Name: cfg_fcsmode
BitField Type: R/W
BitField Desc: config to calculate FCS 32 or FCS 16, (1) is FCS 32, (0) is FCS
16
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_Mask                                                            cBit0
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_Shift                                                               0

/*----------------------------------------
Reg Name: Dcc_Fcs_Rem_Mode
Address: 0x10003
Reg Desc: The register provides configuration to remove FCS32 or FCS16
----------------------------------------*/
#define cAf6Reg_upen_dcctx_fcsrem_Base 0x10003

/*------------------------------------------------------------------------------
Reg Name   : CONFIG HDLC GLOBAL LO DEC
Reg Addr   : 0x04404
Reg Formula: 
    Where  : 
Reg Desc   : 
config HDLC global

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_loglbcfg_Base                                                                0x04404
#define cAf6Reg_upen_hdlc_loglbcfg_WidthVal                                                                 32

/*--------------------------------------
BitField Name: cfg_lsbfirst
BitField Type: R/W
BitField Desc: config to receive LSB first, (1) is LSB first, (0) is default MSB
BitField Bits: [3]
--------------------------------------*/
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_Mask                                                        cBit3
#define cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_Shift                                                           3


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Master Control
Reg Addr   : 0x30003
Reg Formula: 
    Where  : 
Reg Desc   : 
config HDLC Encode Master

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_master_ctrl_Base                                                         0x30003
#define cAf6Reg_upen_hdlc_enc_master_ctrl_WidthVal                                                          32

/*--------------------------------------
BitField Name: encap_lsbfirst
BitField Type: R/W
BitField Desc: config to transmit LSB first, (1) is LSB first, (0) is default
MSB
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_master_ctrl_encap_lsbfirst_Mask                                               cBit0
#define cAf6_upen_hdlc_enc_master_ctrl_encap_lsbfirst_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Control Reg 1
Reg Addr   : 0x38000 - 0x3801F
Reg Formula: 0x38000+ $CID
    Where  : 
           + $CID (0-31) : Channel ID
Reg Desc   : 
config HDLC Encode Control Register 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_ctrl_reg1_Base                                                           0x38000

/*--------------------------------------
BitField Name: encap_fcsmsb
BitField Type: R/W
BitField Desc: Select FCS calculate MSB or LSB first (1) MSB first   , (0) LSB
first
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsmsb_Mask                                                  cBit10
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsmsb_Shift                                                     10

/*--------------------------------------
BitField Name: encap_screnb
BitField Type: R/W
BitField Desc: Enable Scamble (1) Enable      , (0) disable
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_screnb_Mask                                                   cBit9
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_screnb_Shift                                                      9

/*--------------------------------------
BitField Name: encap_idlemod
BitField Type: R/W
BitField Desc: This bit is only used in Bit Stuffing mode Used to configured
IDLE Mode When it is active, the ENC engine will insert '1' pattern when the ENC
is idle. Otherwise the ENC will insert FLAG '7E' pattern (1) Enable            ,
(0) Disabe
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_idlemod_Mask                                                  cBit7
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_idlemod_Shift                                                     7

/*--------------------------------------
BitField Name: encap_sabimod
BitField Type: R/W
BitField Desc: Sabi/Protocol Field Mode (1) Field has 2 bytes , (0) field has 1
byte
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabimod_Mask                                                  cBit6
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabimod_Shift                                                     6

/*--------------------------------------
BitField Name: encap_sabiins
BitField Type: R/W
BitField Desc: Sabi/Protocol Field Insert Enable (1) Enable Insert     , (0)
Disable Insert
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabiins_Mask                                                  cBit5
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabiins_Shift                                                     5

/*--------------------------------------
BitField Name: encap_ctrlins
BitField Type: R/W
BitField Desc: Address/Control Field Insert Enable (1) Enable Insert     , (0)
Disable Insert
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_ctrlins_Mask                                                  cBit4
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_ctrlins_Shift                                                     4

/*--------------------------------------
BitField Name: encap_fcsmod
BitField Type: R/W
BitField Desc: FCS Select Mode (1) 32b FCS           , (0) 16b FCS
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsmod_Mask                                                   cBit3
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsmod_Shift                                                      3

/*--------------------------------------
BitField Name: encap_fcsins
BitField Type: R/W
BitField Desc: FCS Insert Enable (1) Enable Insert     , (0) Disable Insert
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsins_Mask                                                   cBit2
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsins_Shift                                                      2

/*--------------------------------------
BitField Name: encap_flgmod
BitField Type: R/W
BitField Desc: Flag Mode (1) Minimum 2 Flag    , (0) Minimum 1 Flag
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_flgmod_Mask                                                   cBit1
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_flgmod_Shift                                                      1

/*--------------------------------------
BitField Name: encap_stfmod
BitField Type: R/W
BitField Desc: Stuffing Mode (1) Bit Stuffing      , (0) Byte Stuffing
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_stfmod_Mask                                                   cBit0
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_stfmod_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Control Reg 2
Reg Addr   : 0x39000 - 0x3901F
Reg Formula: 0x39000+ $CID
    Where  : 
           + $CID (0-31) : Channel ID
Reg Desc   : 
config HDLC Encode Control Register 2 Byte Stuff Mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_ctrl_reg2_Base                                                           0x39000

/*--------------------------------------
BitField Name: encap_addrval
BitField Type: R/W
BitField Desc: Address Field
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_addrval_Mask                                              cBit31_24
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_addrval_Shift                                                    24

/*--------------------------------------
BitField Name: encap_ctlrval
BitField Type: R/W
BitField Desc: Control Field
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_ctlrval_Mask                                              cBit23_16
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_ctlrval_Shift                                                    16

/*--------------------------------------
BitField Name: encap_sapival0
BitField Type: R/W
BitField Desc: SAPI/PROTOCOL Field 1st byte
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival0_Mask                                              cBit15_8
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival0_Shift                                                    8

/*--------------------------------------
BitField Name: encap_sapival1
BitField Type: R/W
BitField Desc: SAPI/PROTOCOL Field 2nd byte
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival1_Mask                                               cBit7_0
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival1_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Enable Packet Capture
Reg Addr   : 0x0A000
Reg Formula: 
    Where  : 
Reg Desc   : 
config enable capture ethernet packet.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_trig_encap_Base                                                                   0x0A000
#define cAf6Reg_upen_trig_encap_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ram_trig_fls
BitField Type: 
BitField Desc: Trigger flush packets captured RAM Write '0' first, then write
'1' to trigger flush RAM process
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_trig_encap_ram_trig_fls_Mask                                                           cBit3
#define cAf6_upen_trig_encap_ram_trig_fls_Shift                                                              3

/*--------------------------------------
BitField Name: enb_cap_dec
BitField Type: R/W
BitField Desc: Enable capture packets from HDLC Decapsulation. Only one type of
packet (DCC/APS/HDLC) is captured at a time Write '0' first, then write '1' to
trigger capture process (1) Enable            , (0) Disable
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_trig_encap_enb_cap_dec_Mask                                                            cBit2
#define cAf6_upen_trig_encap_enb_cap_dec_Shift                                                               2

/*--------------------------------------
BitField Name: enb_cap_aps
BitField Type: R/W
BitField Desc: Enable capture APS packets from SGMII interface. Only one type of
packet (DCC/APS/HDLC) is captured at a time Write '0' first, then write '1' to
trigger capture process (1) Enable            , (0) Disable
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_trig_encap_enb_cap_aps_Mask                                                            cBit1
#define cAf6_upen_trig_encap_enb_cap_aps_Shift                                                               1

/*--------------------------------------
BitField Name: enb_cap_dcc
BitField Type: R/W
BitField Desc: Enable capture DCC packets from SGMII interface. Only one type of
packet (DCC/APS/HDLC) is captured at a time Write '0' first, then write '1' to
trigger capture process (1) Enable            , (0) Disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_trig_encap_enb_cap_dcc_Mask                                                            cBit0
#define cAf6_upen_trig_encap_enb_cap_dcc_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Captured Packet Data
Reg Addr   : 0x08000 - 0x080FF
Reg Formula: 0x08000 + $loc
    Where  : 
           + $loc(0-255) : data location
Reg Desc   : 
Captured packet data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pktcap_Base                                                                       0x08000
#define cAf6Reg_upen_pktcap_WidthVal                                                                        96

/*--------------------------------------
BitField Name: cap_sop
BitField Type: RO
BitField Desc: Start of packet
BitField Bits: [69:69]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_sop_Mask                                                                    cBit5
#define cAf6_upen_pktcap_cap_sop_Shift                                                                       5

/*--------------------------------------
BitField Name: cap_eop
BitField Type: RO
BitField Desc: End   of packet
BitField Bits: [68:68]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_eop_Mask                                                                    cBit4
#define cAf6_upen_pktcap_cap_eop_Shift                                                                       4

/*--------------------------------------
BitField Name: cap_err
BitField Type: RO
BitField Desc: Error of packet
BitField Bits: [67:67]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_err_Mask                                                                    cBit3
#define cAf6_upen_pktcap_cap_err_Shift                                                                       3

/*--------------------------------------
BitField Name: cap_nob
BitField Type: RO
BitField Desc: Number of valid bytes in 8-bytes data captured
BitField Bits: [66:64]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_nob_Mask                                                                  cBit2_0
#define cAf6_upen_pktcap_cap_nob_Shift                                                                       0

/*--------------------------------------
BitField Name: cap_dat
BitField Type: RO
BitField Desc: packet data captured
BitField Bits: [63:00]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_dat_01_Mask                                                              cBit31_0
#define cAf6_upen_pktcap_cap_dat_01_Shift                                                                    0
#define cAf6_upen_pktcap_cap_dat_02_Mask                                                              cBit31_0
#define cAf6_upen_pktcap_cap_dat_02_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : APS_Acc_Received_Kbyte_Value_Per_Channel
Reg Addr   : 0x22800 - 0x228FF
Reg Formula: 0x22800 + $channelid*16 + $loc
    Where  : 
           + $channelid(0-31): Channel ID
           + $loc(0-15): addresses reserved per channel
Reg Desc   : 
The register provides received Kbyte of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12acc_Base                                                                       0x22800
#define cAf6Reg_upen_k12acc_WidthVal                                                                        32

/*--------------------------------------
BitField Name: Kbyte_Value
BitField Type: RO
BitField Desc: Received Kbyte value of each channel [23:16]: K1 Byte [15:08]: K2
Byte [07:00]: Extend Kbyte
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_k12acc_Kbyte_Value_Mask                                                             cBit23_0
#define cAf6_upen_k12acc_Kbyte_Value_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Sdh_Req_Interval_Configuration
Reg Addr   : 0x00003
Reg Formula: 
    Where  : 
Reg Desc   : 
The register is used to configure time interval to generate fake SDH request signal

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_genmon_reqint_Base                                                                0x00003
#define cAf6Reg_upen_genmon_reqint_WidthVal                                                                 32

/*--------------------------------------
BitField Name: Timer_Enable
BitField Type: RW
BitField Desc: Enable generation of fake request SDH signal
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_genmon_reqint_Timer_Enable_Mask                                                       cBit28
#define cAf6_upen_genmon_reqint_Timer_Enable_Shift                                                          28

/*--------------------------------------
BitField Name: Timer_Value
BitField Type: RW
BitField Desc: Counter of number of clk 155MHz between fake request SDH signal.
This counter is used make a delay interval between 2 consecutive SDH requests
generated by DCC GENERATOR. For example: to create a delay of 125us between SDH
request signals, with a clock 155Mz, the value of counter to be configed to this
field is (125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_upen_genmon_reqint_Timer_Value_Mask                                                      cBit27_0
#define cAf6_upen_genmon_reqint_Timer_Value_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Header_Per_Channel
Reg Addr   : 0x0C100 - 0x0C120
Reg Formula: 0x0C100 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
The register provides data for configuration of VID and MAC DA Header of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_hdr_Base                                                          0x0C100
#define cAf6Reg_upen_dcc_cfg_testgen_hdr_WidthVal                                                           32

/*--------------------------------------
BitField Name: HDR_VID
BitField Type: RW
BitField Desc: 12b of VID value in header of Generated Packet
BitField Bits: [19:08]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_hdr_HDR_VID_Mask                                                    cBit19_8
#define cAf6_upen_dcc_cfg_testgen_hdr_HDR_VID_Shift                                                          8

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: Bit [7:0] of MAC DA value [47:0] of generated Packet
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_hdr_MAC_DA_Mask                                                      cBit7_0
#define cAf6_upen_dcc_cfg_testgen_hdr_MAC_DA_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Mode_Per_Channel
Reg Addr   : 0x0C200 - 0x0C220
Reg Formula: 0x0C200 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
The register provides data for configuration of generation mode of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_mod_Base                                                          0x0C200

/*--------------------------------------
BitField Name: gen_payload_mod
BitField Type: RW
BitField Desc: Modes of generated packet payload:
BitField Bits: [31:30]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_mod_gen_payload_mod_Mask                                           cBit31_30
#define cAf6_upen_dcc_cfg_testgen_mod_gen_payload_mod_Shift                                                 30

/*--------------------------------------
BitField Name: gen_len_mode
BitField Type: RW
BitField Desc: Modes of generated packet length :
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_mod_gen_len_mode_Mask                                              cBit29_28
#define cAf6_upen_dcc_cfg_testgen_mod_gen_len_mode_Shift                                                    28

/*--------------------------------------
BitField Name: gen_fix_pattern
BitField Type: RW
BitField Desc: 8b fix pattern payload if mode "fix payload" is used
BitField Bits: [27:20]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_mod_gen_fix_pattern_Mask                                           cBit27_20
#define cAf6_upen_dcc_cfg_testgen_mod_gen_fix_pattern_Shift                                                 20

/*--------------------------------------
BitField Name: gen_number_of_packet
BitField Type: RW
BitField Desc: Number of packets the GEN generates for each channelID in gen
burst mode
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_mod_gen_number_of_packet_Mask                                       cBit19_0
#define cAf6_upen_dcc_cfg_testgen_mod_gen_number_of_packet_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Global_Gen_Enable_Channel
Reg Addr   : 0x0C000
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable Channel to generate DCC packets

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_enacid_Base                                                       0x0C000

/*--------------------------------------
BitField Name: Channel_enable
BitField Type: RW
BitField Desc: Enable Channel to generate DCC packets. Bit[31:0] <-> Channel
31->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_enacid_Channel_enable_Mask                                          cBit31_0
#define cAf6_upen_dcc_cfg_testgen_enacid_Channel_enable_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Global_Gen_Mode
Reg Addr   : 0x0C001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides global configuration of GEN mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_glb_gen_mode_Base                                                 0x0C001

/*--------------------------------------
BitField Name: gen_force_err_len
BitField Type: RW
BitField Desc: Create wrong packet's length fiedl generated packets
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_len_Mask                                    cBit6
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_len_Shift                                       6

/*--------------------------------------
BitField Name: gen_force_err_fcs
BitField Type: RW
BitField Desc: Create wrong FCS field in generated packets
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_fcs_Mask                                    cBit5
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_fcs_Shift                                       5

/*--------------------------------------
BitField Name: gen_force_err_dat
BitField Type: RW
BitField Desc: Create wrong data value in generated packets
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_dat_Mask                                    cBit4
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_dat_Shift                                       4

/*--------------------------------------
BitField Name: gen_force_err_seq
BitField Type: RW
BitField Desc: Create wrong Sequence field in generated packets
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_seq_Mask                                    cBit3
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_seq_Shift                                       3

/*--------------------------------------
BitField Name: gen_force_err_vcg
BitField Type: RW
BitField Desc: Create wrong VCG field in generated packets
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_vcg_Mask                                    cBit2
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_vcg_Shift                                       2

/*--------------------------------------
BitField Name: gen_burst_mod
BitField Type: RW
BitField Desc: Gen each Channel a number of packet as configured in "Gen mode
per channel" resgister
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_burst_mod_Mask                                        cBit1
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_burst_mod_Shift                                           1

/*--------------------------------------
BitField Name: gen_conti_mod
BitField Type: RW
BitField Desc: Gen packet forever without stopping
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_conti_mod_Mask                                        cBit0
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_conti_mod_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Global_Packet_Length
Reg Addr   : 0x0C002
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration min length max length of generated Packets

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_glb_length_Base                                                   0x0C002

/*--------------------------------------
BitField Name: gen_max_length
BitField Type: RW
BitField Desc: Maximum length of generated packet
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_length_gen_max_length_Mask                                     cBit31_16
#define cAf6_upen_dcc_cfg_testgen_glb_length_gen_max_length_Shift                                           16

/*--------------------------------------
BitField Name: gen_min_length
BitField Type: RW
BitField Desc: Minimum length of generated packet, also used for fix length
packets in case fix length mode is used
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_length_gen_min_length_Mask                                      cBit15_0
#define cAf6_upen_dcc_cfg_testgen_glb_length_gen_min_length_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Global_Gen_Interval
Reg Addr   : 0x0C003
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration for packet generating interval

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_glb_gen_interval_Base                                             0x0C003
#define cAf6Reg_upen_dcc_cfg_testgen_glb_gen_interval_WidthVal                                              32

/*--------------------------------------
BitField Name: Gen_Intv_Enable
BitField Type: RW
BitField Desc: Enable using this interval value
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Enable_Mask                                  cBit28
#define cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Enable_Shift                                      28

/*--------------------------------------
BitField Name: Gen_Intv_Value
BitField Type: RW
BitField Desc: Counter of number of clk 155MHz between packet generation. This
counter is used make a delay interval between 2 consecutive packet generation.
For example: to create a delay of 125us between 2 packet generating, with a
clock 155Mz, the value of counter to be configed to this field is
(125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Value_Mask                                cBit27_0
#define cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Value_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Good_Packet_Counter
Reg Addr   : 0x0E100 - 0x0E120
Reg Formula: 0x0E100 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
Counter of receive good packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_godpkt_Base                                                                   0x0E100

/*--------------------------------------
BitField Name: dcc_test_mon_good_cnt
BitField Type: WC
BitField Desc: Counter of Receive Good Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_godpkt_dcc_test_mon_good_cnt_Mask                                               cBit31_0
#define cAf6_upen_mon_godpkt_dcc_test_mon_good_cnt_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Error_Data_Packet_Counter
Reg Addr   : 0x0E200 - 0x0E220
Reg Formula: 0x0E200 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
Counter of received packets has error data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_errpkt_Base                                                                   0x0E200

/*--------------------------------------
BitField Name: dcc_test_mon_errdat_cnt
BitField Type: WC
BitField Desc: Counter of Receive Error Data Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_errpkt_dcc_test_mon_errdat_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_errpkt_dcc_test_mon_errdat_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Error_VCG_Packet_Counter
Reg Addr   : 0x0E300 - 0x0E320
Reg Formula: 0x0E300 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
Counter of received packet has wrong VCG value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_errvcg_Base                                                                   0x0E300

/*--------------------------------------
BitField Name: dcc_test_mon_errvcg_cnt
BitField Type: WC
BitField Desc: Counter of Receive Error VCG Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_errvcg_dcc_test_mon_errvcg_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_errvcg_dcc_test_mon_errvcg_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Error_SEQ_Packet_Counter
Reg Addr   : 0x0E400 - 0x0E420
Reg Formula: 0x0E400 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
Counter of received packet has wrong Sequence value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_errseq_Base                                                                   0x0E400

/*--------------------------------------
BitField Name: dcc_test_mon_errseq_cnt
BitField Type: WC
BitField Desc: Counter of Receive Error Sequence Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_errseq_dcc_test_mon_errseq_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_errseq_dcc_test_mon_errseq_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Error_FCS_Packet_Counter
Reg Addr   : 0x0E500 - 0x0E520
Reg Formula: 0x0E500 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
Counter of received packet has wrong FCS value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_errfcs_Base                                                                   0x0E500

/*--------------------------------------
BitField Name: dcc_test_mon_errfcs_cnt
BitField Type: WC
BitField Desc: Counter of Receive Error FCS Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_errfcs_dcc_test_mon_errfcs_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_errfcs_dcc_test_mon_errfcs_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Abort_VCG_Packet_Counter
Reg Addr   : 0x0E600 - 0x0E620
Reg Formula: 0x0E600 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
Counter of received packet has been abbort due to wrong length information

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_abrpkt_Base                                                                   0x0E600

/*--------------------------------------
BitField Name: dcc_test_mon_abrpkt_cnt
BitField Type: WC
BitField Desc: Counter of Abbort Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_abrpkt_dcc_test_mon_abrpkt_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_abrpkt_dcc_test_mon_abrpkt_cnt_Shift                                                   0

#endif /* _AF6_REG_AF6CNC0021_RD_DCCK_H_ */

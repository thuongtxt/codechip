/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW Module
 *
 * File        : Tha60290021KBytePw.c
 *
 * Created Date: Sep 4, 2016
 *
 * Description : Tha60290021 Pw Kbyte Implement code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCounters.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/pw/counters/AtPwCountersInternal.h"
#include "../../../../generic/common/AtEthVlanTagInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../man/Tha60290021Device.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "Tha60290021PwKbyteInternal.h"
#include "Tha60290021ModulePw.h"
#include "Tha60290021DccKbyteReg.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290021PwKByte)self)

/* Timer */
#define cTha6029KbyteTimerIntervalValueMaxInUs  8000 /* 8000  us ~  8ms */
#define cTha6029KbyteTimerWathdogValueMaxInUs  16000 /* 16000 us ~ 16ms */

/* DA, SA same define bitMask */
#define cAf6_upen_k12_Mac_Byte_5_Mask       cBit15_8
#define cAf6_upen_k12_Mac_Byte_5_Shift             8
#define cAf6_upen_k12_Mac_Byte_4_Mask        cBit7_0
#define cAf6_upen_k12_Mac_Byte_4_Shift             0
#define cAf6_upen_k12_Mac_Byte_3_Mask      cBit31_24
#define cAf6_upen_k12_Mac_Byte_3_Shift            24
#define cAf6_upen_k12_Mac_Byte_2_Mask      cBit23_16
#define cAf6_upen_k12_Mac_Byte_2_Shift            16
#define cAf6_upen_k12_Mac_Byte_1_Mask       cBit15_8
#define cAf6_upen_k12_Mac_Byte_1_Shift             8
#define cAf6_upen_k12_Mac_Byte_0_Mask        cBit7_0
#define cAf6_upen_k12_Mac_Byte_0_Shift             0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021PwKByteMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtChannelMethods        m_AtChannelOverride;
static tAtPwMethods             m_AtPwOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtPwMethods      *m_AtPwMethods      = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static void KbyteChannelsDelete(Tha60290021PwKByte self);

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(AtPw self)
    {
    return AtChannelDeviceGet((AtChannel) self);
    }

static ThaModuleOcn OcnModule(AtPw self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(DeviceGet(self), cThaModuleOcn);
    }

static AtIpCore Core(AtPw self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static uint32 BaseAddress(AtPw self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self));
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "kbyte_pw";
    }

static eAtRet ExpectedEthTypeSet(AtPw self, uint16 ethType)
    {
    uint32 regAddr = cAf6Reg_upen_k12rx_evt_Base + BaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_k12rx_evt_ETHTYP_, ethType);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static uint16 ExpectedEthTypeGet(AtPw self)
    {
    uint32 regAddr = cAf6Reg_upen_k12rx_evt_Base + BaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleSdh);
    return (uint16)mRegField(regVal, cAf6_upen_k12rx_evt_ETHTYP_);
    }

static eAtRet TxEthTypeSet(AtPw self, uint16 ethType)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_upen_cfg_hdrvtl_Base + BaseAddress(self);
    AtIpCore core = Core(self);
    ThaModuleOcn ocnModule = OcnModule(self);

    ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[0], cAf6_upen_cfg_hdrvtl_ETHTYPE_, ethType);
    ThaModuleOcnSohOverEthLongWriteOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static uint16 TxEthTypeGet(AtPw self)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_upen_cfg_hdrvtl_Base + BaseAddress(self);
    AtIpCore core = Core(self);
    ThaModuleOcn ocnModule = OcnModule(self);

    ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    return (uint16) mRegField(longRegVal[0], cAf6_upen_cfg_hdrvtl_ETHTYPE_);
    }

static eAtRet ExpectedTypeSet(AtPw self, uint8 type)
    {
    uint32 regAddr = cAf6Reg_upen_k12rx_evt_Base + BaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_k12rx_evt_TYPE_, type);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static uint8 ExpectedTypeGet(AtPw self)
    {
    uint32 regAddr = cAf6Reg_upen_k12rx_evt_Base + BaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleSdh);
    return mRegField(regVal, cAf6_upen_k12rx_evt_TYPE_);
    }

static eAtRet TxTypeSet(AtPw self, uint8 type)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_upen_cfg_hdrvtl_Base + BaseAddress(self);
    AtIpCore core = Core(self);
    ThaModuleOcn ocnModule = OcnModule(self);

    ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[0], cAf6_upen_cfg_hdrvtl_APSTYPE_, type);
    ThaModuleOcnSohOverEthLongWriteOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static uint8 TxTypeGet(AtPw self)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_upen_cfg_hdrvtl_Base + BaseAddress(self);
    AtIpCore core = Core(self);
    ThaModuleOcn ocnModule = OcnModule(self);

    ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    return (uint8) mRegField(longRegVal[0], cAf6_upen_cfg_hdrvtl_APSTYPE_);
    }

static eAtRet ExpectedVersionSet(AtPw self, uint8 version)
    {
    uint32 regAddr = cAf6Reg_upen_k12rx_evt_Base + BaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_k12rx_evt_VER_, version);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static uint8 ExpectedVersionGet(AtPw self)
    {
    uint32 regAddr = cAf6Reg_upen_k12rx_evt_Base + BaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleSdh);
    return (uint8)mRegField(regVal, cAf6_upen_k12rx_evt_VER_);
    }

static eAtRet TxVersionSet(AtPw self, uint8 version)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_upen_cfg_hdrvtl_Base + BaseAddress(self);
    AtIpCore core = Core(self);
    ThaModuleOcn ocnModule = OcnModule(self);

    ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[0], cAf6_upen_cfg_hdrvtl_VERSION_, version);
    ThaModuleOcnSohOverEthLongWriteOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static uint8 TxVersionGet(AtPw self)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_upen_cfg_hdrvtl_Base + BaseAddress(self);
    AtIpCore core = Core(self);
    ThaModuleOcn ocnModule = OcnModule(self);

    ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    return (uint8) mRegField(longRegVal[0], cAf6_upen_cfg_hdrvtl_VERSION_);
    }

static eAtModulePwRet EthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    eAtModulePwRet ret = cAtOk;

    ret |= AtPwEthDestMacSet(self, destMac);
    ret |= AtPwEthVlanSet(self, cVlan, sVlan);

    return ret;
    }

static eAtModulePwRet MacSet(AtPw self, uint8 *mac, uint32 addressBase)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = addressBase + BaseAddress(self);
    AtIpCore core = Core(self);
    ThaModuleOcn ocnModule = OcnModule(self);

    ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[1], cAf6_upen_k12_Mac_Byte_5_, mac[0]);
    mRegFieldSet(longRegVal[1], cAf6_upen_k12_Mac_Byte_4_, mac[1]);
    mRegFieldSet(longRegVal[0], cAf6_upen_k12_Mac_Byte_3_, mac[2]);
    mRegFieldSet(longRegVal[0], cAf6_upen_k12_Mac_Byte_2_, mac[3]);
    mRegFieldSet(longRegVal[0], cAf6_upen_k12_Mac_Byte_1_, mac[4]);
    mRegFieldSet(longRegVal[0], cAf6_upen_k12_Mac_Byte_0_, mac[5]);
    ThaModuleOcnSohOverEthLongWriteOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static eAtModulePwRet MacGet(AtPw self, uint8 *mac, uint32 addressBase)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = addressBase + BaseAddress(self);
    AtIpCore core = Core(self);
    ThaModuleOcn ocnModule = OcnModule(self);

    ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    mac[5] = (uint8)mRegField(longRegVal[0], cAf6_upen_k12_Mac_Byte_0_);
    mac[4] = (uint8)mRegField(longRegVal[0], cAf6_upen_k12_Mac_Byte_1_);
    mac[3] = (uint8)mRegField(longRegVal[0], cAf6_upen_k12_Mac_Byte_2_);
    mac[2] = (uint8)mRegField(longRegVal[0], cAf6_upen_k12_Mac_Byte_3_);
    mac[1] = (uint8)mRegField(longRegVal[1], cAf6_upen_k12_Mac_Byte_4_);
    mac[0] = (uint8)mRegField(longRegVal[1], cAf6_upen_k12_Mac_Byte_5_);

    return cAtOk;
    }

static eAtModulePwRet EthDestMacSet(AtPw self, uint8 *destMac)
    {
    eAtModulePwRet ret = cAtOk;

    if (destMac == NULL)
        return cAtErrorNullPointer;

    ret |= MacSet(self, destMac, cAf6Reg_upen_cfg_hdrmda_Base);
    ret |= MacSet(self, destMac, cAf6Reg_upen_k12rx_macsa_Base);

    return ret;
    }

static eAtModulePwRet EthDestMacGet(AtPw self, uint8 *destMac)
    {
    if (destMac)
        return MacGet(self, destMac, cAf6Reg_upen_cfg_hdrmda_Base);
    return cAtErrorNullPointer;
    }

static eAtModulePwRet EthSrcMacSet(AtPw self, uint8 *srcMac)
    {
    eAtModulePwRet ret = cAtOk;

    if (srcMac == NULL)
        return cAtErrorNullPointer;

    ret |= MacSet(self, srcMac, cAf6Reg_upen_cfg_hdrmsa_Base);
    ret |= MacSet(self, srcMac, cAf6Reg_upen_k12rx_macda_Base);

    return ret;
    }

static eAtModulePwRet EthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    if (srcMac)
        return MacGet(self, srcMac, cAf6Reg_upen_cfg_hdrmsa_Base);
    return cAtErrorNullPointer;
    }

static eAtModulePwRet HwEthCVlanSet(AtPw self, uint32 hwVlan)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_upen_cfg_hdrvtl_Base + BaseAddress(self);
    AtIpCore core = Core(self);
    ThaModuleOcn ocnModule = OcnModule(self);

    ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[1], cAf6_upen_cfg_hdrvtl_VLAN_, hwVlan);
    ThaModuleOcnSohOverEthLongWriteOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);

    return cAtOk;
    }

static tAtEthVlanTag* HwEthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = cAf6Reg_upen_cfg_hdrvtl_Base + BaseAddress(self);
    AtIpCore core = Core(self);
    ThaModuleOcn ocnModule = OcnModule(self);

    ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    return AtEthVlanTagFromUint32(mRegField(longRegVal[1], cAf6_upen_cfg_hdrvtl_VLAN_), cVlan);
    }

static eAtModulePwRet EthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    /* Not Support S-VLAN */
    if (sVlan)
        return cAtErrorModeNotSupport;

    if (!cVlan)
        return cAtErrorModeNotSupport;

    return HwEthCVlanSet(self, AtEthVlanTagToUint32(cVlan));
    }

static tAtEthVlanTag* EthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    if (cVlan == NULL)
        return NULL;

    return HwEthCVlanGet(self, cVlan);
    }

static tAtEthVlanTag* EthSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    AtUnused(self);
    AtUnused(sVlan);
    return NULL;
    }

static AtEthPort DefaultSgmiiEthPort(AtChannel self)
    {
    AtModuleEth ethModule = (AtModuleEth) AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleEth);
    return Tha60290021ModuleEthSgmiiDccKbytePortGet(ethModule);
    }

static eAtRet HeaderDefault(AtChannel self)
    {
    AtPw pw = (AtPw) self;
    eAtRet ret = cAtOk;

    ret |= Tha6029PwKbytePwTxEthTypeSet(pw, 0x88B7);
    ret |= Tha6029PwKbytePwExpectedEthTypeSet(pw, 0x88B7);
    if (ret != cAtOk)
        return ret;

    ret = Tha6029PwKbyteTxVersionSet(pw, 0x0);
    ret = Tha6029PwKbyteExpectedVersionSet(pw, 0x0);
    if (ret != cAtOk)
        return ret;

    ret |= Tha6029PwKbyteTxTypeSet(pw, 0x54);
    ret |= Tha6029PwKbyteExpectedTypeSet(pw, 0x54);

    return ret;
    }

static eAtRet AllChannelsInit(AtPw self)
    {
    uint8 channelId;
    eAtRet ret = cAtOk;

    for (channelId = 0; channelId < Tha60290021PwKbyteNumKbyteChannelsGet(self); channelId++)
        {
        AtPwKbyteChannel channel = Tha60290021PwKbyteChannelGet(self, channelId);
        if (channel)
            ret |= AtChannelInit((AtChannel)channel);
        }

    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = cAtOk;

    ret |= Tha6029PwKbyteIntervalSet((AtPw)self, 0);
    ret |= Tha6029PwKbyteWatchdogSet((AtPw)self, 0);
    ret |= HeaderDefault(self);

    /* Disable all interrupt mask */
    ret |= AtChannelInterruptMaskSet(self, AtChannelSupportedInterruptMasks(self), 0);
    ret |= AtPwEthPortSet((AtPw)self, DefaultSgmiiEthPort(self));

    ret |= AllChannelsInit((AtPw)self);

    return ret;
    }

static eAtModulePwRet EthPortSet(AtPw self, AtEthPort ethPort)
    {
    if (ethPort == NULL)
        return cAtErrorNotApplicable;

    if (ethPort != DefaultSgmiiEthPort((AtChannel) self))
        return cAtErrorNotApplicable;

    mThis(self)->port = ethPort;
    return cAtOk;
    }

static AtEthPort EthPortGet(AtPw self)
    {
    return mThis(self)->port;
    }

static const char *ToString(AtObject self)
    {
    return AtChannelTypeString((AtChannel)self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290021PwKByte object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(port);
    mEncodeNone(counters);
    mEncodeObjects(kbyteChannels, Tha60290021PwKbyteNumKbyteChannelsGet((AtPw)self));
    }

static uint32 CounterBaseAddress(AtChannel self, uint16 counterType, eBool r2c)
    {
    AtUnused(self);
    AtUnused(r2c);

    switch (counterType)
        {
        case cAtPwCounterTypeTxPackets         : return cAf6Reg_upen_k12eop2sgm_pcid_Base;
        case cAtPwCounterTypeTxPayloadBytes    : return cAf6Reg_upen_k12byt2sgm_pcid_Base;
        case cAtPwCounterTypeRxPackets         : return cAf6Reg_upen_k12_glbcnt_sok_Base;
        case cAtPwCounterTypeRxMalformedPackets: return cAf6Reg_upen_k12_glbcnt_sfail_Base;
        default:                                 return cInvalidUint32;
        }
    }

static uint32 HwCounterRead2Clear(AtChannel self, uint32 counterBaseAddress, eBool r2c)
    {
    uint32 regAddr = BaseAddress((AtPw) self) + counterBaseAddress + AtChannelIdGet(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    if (r2c)
        mChannelHwWrite(self, regAddr, 0x0, cAtModuleSdh);

    return regVal;
    }

static uint32 CounterReadGet(AtChannel self, uint16 counterType, eBool r2c)
    {
    uint32 counterAddress = CounterBaseAddress(self, counterType, r2c);
    if (counterAddress == cInvalidUint32)
        return 0x0;

    return HwCounterRead2Clear(self, counterAddress, r2c);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (AtChannelCounterIsSupported(self, counterType))
        return CounterReadGet(self, counterType, cAtFalse);

    return 0x0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (AtChannelCounterIsSupported(self, counterType))
        return CounterReadGet(self, counterType, cAtTrue);
    return 0x0;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cAtPwCounterTypeTxPackets:          return cAtTrue;
        case cAtPwCounterTypeTxPayloadBytes:     return cAtTrue;
        case cAtPwCounterTypeRxPackets:          return cAtTrue;
        case cAtPwCounterTypeRxMalformedPackets: return cAtTrue;
        default:                                 return cAtFalse;
        }
    }

static AtPwCounters CountersGet(AtChannel self)
    {
    if (mThis(self)->counters == NULL)
        mThis(self)->counters = AtPwCountersNew();

    return mThis(self)->counters;
    }

static eAtRet AllCountersReadToClear(AtChannel self, void *pAllCounters, eBool clear)
    {
    AtPwCounters counters = CountersGet(self);

    if (counters == NULL)
        return cAtOk;

    AtPwCountersInit(counters);
    AtPwCountersTxPacketsSet(counters, CounterReadGet(self, cAtPwCounterTypeTxPackets, clear));
    AtPwCountersTxPayloadBytesSet(counters, CounterReadGet(self, cAtPwCounterTypeTxPayloadBytes, clear));
    AtPwCountersRxPacketsSet(counters, CounterReadGet(self, cAtPwCounterTypeRxPackets, clear));
    AtPwCountersRxMalformedPacketsSet(counters, CounterReadGet(self, cAtPwCounterTypeRxMalformedPackets, clear));

    if (pAllCounters)
        *((AtPwCounters *)pAllCounters) = counters;

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static uint32 IntervalSw2Hw(uint32 intervalInUs)
    {
    return (uint32) (intervalInUs * 155);
    }

static uint32 IntervalHw2Sw(uint32 hwTimer)
    {
    return (uint32) (hwTimer / 155);
    }

static uint32 IntervalRegister(AtPw self)
    {
    return BaseAddress(self) + cAf6Reg_upen_cfgtim_Base;
    }

static eAtRet IntervalSet(AtPw self, uint32 intervalInUs)
    {
    uint32 regVal;
    uint32 hwTimer;

    if (intervalInUs > cTha6029KbyteTimerIntervalValueMaxInUs)
        return cAtErrorOutOfRangParm;

    hwTimer = IntervalSw2Hw(intervalInUs);
    regVal = 0x0;
    mRegFieldSet(regVal, cAf6_upen_cfgtim_Timer_Ena_, (hwTimer != 0));
    mRegFieldSet(regVal, cAf6_upen_cfgtim_Timer_Val_, hwTimer);
    mChannelHwWrite(self, IntervalRegister(self), regVal, cAtModuleSdh);

    return cAtOk;
    }

static uint32 IntervalGet(AtPw self)
    {
    uint32 regVal = mChannelHwRead(self, IntervalRegister(self), cAtModuleSdh);
    return IntervalHw2Sw(mRegField(regVal, cAf6_upen_cfgtim_Timer_Val_));
    }

static eAtRet AllSonetLinesTrigger(AtPw self)
    {
    uint32 channelId = 0;

    for (channelId = 0; channelId < Tha60290021PwKbyteNumKbyteChannelsGet(self); channelId++)
        Tha60290021ModuleOcnFacePlateKbyteOcnToKbyteDccProcessorTrigger(OcnModule(self), channelId);

    return cAtOk;
    }

static eAtRet Trigger(AtPw self)
    {
    uint32 regAddr;
    eAtRet ret = AllSonetLinesTrigger(self);

    /* SW trigger Write 0, then write 1 to trigger */
    regAddr = BaseAddress(self) + cAf6Reg_upen_cfgcid_Base;
    mChannelHwWrite(self, regAddr, 0, cAtModuleSdh);

    /* May need to take some delay Here! */
    mChannelHwWrite(self, regAddr, 1, cAtModuleSdh);
    return ret;
    }

static uint32 WatchdogTimerRegister(AtPw self)
    {
    return BaseAddress(self) + cAf6Reg_upen_k12rx_watdog_Base;
    }

static eAtRet WatchdogSet(AtPw self, uint32 timerInUs)
    {
    uint32 regVal;
    uint32 hwTimer;

    if (timerInUs > cTha6029KbyteTimerWathdogValueMaxInUs)
        return cAtErrorOutOfRangParm;

    hwTimer = IntervalSw2Hw(timerInUs);
    regVal = 0x0;
    mRegFieldSet(regVal, cAf6_upen_k12rx_watdog_Enable_, (hwTimer != 0));
    mRegFieldSet(regVal, cAf6_upen_k12rx_watdog_Timer_Val_, hwTimer);
    mChannelHwWrite(self, WatchdogTimerRegister(self), regVal, cAtModuleSdh);

    return cAtOk;
    }

static uint32 WatchdogGet(AtPw self)
    {
    uint32 regVal = mChannelHwRead(self, WatchdogTimerRegister(self), cAtModuleSdh);
    return IntervalHw2Sw(mRegField(regVal, cAf6_upen_k12rx_watdog_Timer_Val_));
    }

static eAtModulePwRet PsnSet(AtPw self, AtPwPsn psn)
    {
    AtUnused(psn);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static AtChannel BoundCircuitGet(AtPw self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtModulePwRet CircuitBind(AtPw self, AtChannel circuit)
    {
    AtUnused(self);
    AtUnused(circuit);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CircuitUnbind(AtPw self)
    {
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet LopsSetThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(numPackets);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet LopsClearThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(numPackets);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet PayloadSizeSet(AtPw self, uint16 sizeInBytes)
    {
    AtUnused(sizeInBytes);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet PrioritySet(AtPw self, uint8 priority)
    {
    AtUnused(priority);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet ReorderingEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferSizeSet(AtPw self, uint32 microseconds)
    {
    AtUnused(microseconds);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferSizeInPacketSet(AtPw self, uint16 numPackets)
    {
    AtUnused(numPackets);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferDelaySet(AtPw self, uint32 microseconds)
    {
    AtUnused(microseconds);
    AtUnused(self);

    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferDelayInPacketSet(AtPw self, uint16 numPackets)
    {
    AtUnused(numPackets);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);

    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpTxPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    AtUnused(payloadType);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpExpectedPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    AtUnused(payloadType);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpPayloadTypeCompare(AtPw self, eBool enableCompare)
    {
    AtUnused(enableCompare);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    AtUnused(payloadType);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpTxSsrcSet(AtPw self, uint32 ssrc)
    {
    AtUnused(ssrc);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpExpectedSsrcSet(AtPw self, uint32 ssrc)
    {
    AtUnused(ssrc);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpSsrcCompare(AtPw self, eBool enableCompare)
    {
    AtUnused(enableCompare);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpSsrcSet(AtPw self, uint32 ssrc)
    {
    AtUnused(ssrc);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpTimeStampModeSet(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
    AtUnused(timeStampMode);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet SuppressEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwAutoTxLBitEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwAutoRxLBitEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwAutoRBitEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwSequenceModeSet(AtPw self, eAtPwCwSequenceMode sequenceMode)
    {
    AtUnused(sequenceMode);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwLengthModeSet(AtPw self, eAtPwCwLengthMode lengthMode)
    {
    AtUnused(lengthMode);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    AtUnused(pktReplaceMode);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet EthExpectedCVlanSet(AtPw self, tAtEthVlanTag *cVlan)
    {
    AtUnused(cVlan);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet EthExpectedSVlanSet(AtPw self, tAtEthVlanTag *sVlan)
    {
    AtUnused(sVlan);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_us, uint32 jitterDelay_us, uint16 payloadSize)
    {
    AtUnused(payloadSize);
    AtUnused(jitterDelay_us);
    AtUnused(jitterBufferSize_us);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferInPacketAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_pkt, uint32 jitterDelay_pkt, uint16 payloadSize)
    {
    AtUnused(payloadSize);
    AtUnused(jitterDelay_pkt);
    AtUnused(jitterBufferSize_pkt);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet IdleCodeSet(AtPw self, uint8 idleCode)
    {
    AtUnused(idleCode);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet MissingPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet ExcessivePacketLossRateDefectThresholdSet(AtPw self, uint32 numPackets, uint32 timeInSecond)
    {
    AtUnused(self);
    AtUnused(numPackets);
    AtUnused(timeInSecond);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet StrayPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet MalformedPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RemotePacketLossDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferOverrunDefectThresholdSet(AtPw self, uint32 numOverrunEvent)
    {
    AtUnused(self);
    AtUnused(numOverrunEvent);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferUnderrunDefectThresholdSet(AtPw self, uint32 numUnderrunEvent)
    {
    AtUnused(self);
    AtUnused(numUnderrunEvent);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet MisConnectionDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorNotApplicable;
    }

static AtPwGroup ApsGroupGet(AtPw self)
    {
    AtUnused(self);
    return NULL;
    }

static AtPwGroup HsGroupGet(AtPw self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtModulePwRet BackupPsnSet(AtPw self, AtPwPsn backupPsn)
    {
    AtUnused(self);
    AtUnused(backupPsn);
    return cAtErrorNotApplicable;
    }

static AtPwPsn BackupPsnGet(AtPw self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtModulePwRet BackupEthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    AtUnused(self);
    AtUnused(destMac);
    AtUnused(cVlan);
    AtUnused(sVlan);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet BackupEthDestMacSet(AtPw self, uint8 *destMac)
    {
    AtUnused(self);
    AtUnused(destMac);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet BackupEthSrcMacSet(AtPw self, uint8 *srcMac)
    {

    AtUnused(self);
    AtUnused(srcMac);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet BackupEthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    AtUnused(self);
    AtUnused(srcMac);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet BackupEthDestMacGet(AtPw self, uint8 *destMac)
    {
    AtUnused(self);
    AtUnused(destMac);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet BackupEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    AtUnused(self);
    AtUnused(cVlan);
    AtUnused(sVlan);
    return cAtErrorNotApplicable;
    }

static eAtRet EthPortQueueSet(AtPw self, uint8 queueId)
    {
    AtUnused(self);
    AtUnused(queueId);
    return cAtErrorNotApplicable;
    }

static eAtRet PtchInsertionModeSet(AtPw self, eAtPtchMode insertMode)
    {
    AtUnused(self);
    AtUnused(insertMode);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet TxActiveForce(AtPw self, eBool active)
    {
    AtUnused(self);
    AtUnused(active);
    return cAtErrorNotApplicable;
    }

static eBool CwLengthModeIsSupported(AtPw self, eAtPwCwLengthMode lengthMode)
    {
    AtUnused(self);
    AtUnused(lengthMode);
    return cAtFalse;
    }

static eBool CwAutoRxLBitCanEnable(AtPw self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtFalse;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return cTha6029KbyteApsPwAlarmTypeAll;
    }

static uint32 MaskSw2Hw(uint32 regVal, uint32 defectMask, uint32 enableMask)
    {
    if (defectMask & cTha6029KbyteApsPwAlarmTypeDaMacMisMatch)
        mRegFieldSet(regVal, cAf6_upen_rxk12_intenb_enb_int_aps_macda_mismat_, (enableMask & cTha6029KbyteApsPwAlarmTypeDaMacMisMatch) ? 1 : 0);

    if (defectMask & cTha6029KbyteApsPwAlarmTypeEthTypeMisMatch)
        mRegFieldSet(regVal, cAf6_upen_rxk12_intenb_enb_int_aps_ethtp_mismat_, (enableMask & cTha6029KbyteApsPwAlarmTypeEthTypeMisMatch) ? 1 : 0);

    if (defectMask & cTha6029KbyteApsPwAlarmTypeTypeMisMatch)
        mRegFieldSet(regVal, cAf6_upen_rxk12_intenb_enb_int_aps_apstp_mismat_, (enableMask & cTha6029KbyteApsPwAlarmTypeTypeMisMatch) ? 1 : 0);

    if (defectMask & cTha6029KbyteApsPwAlarmTypeVerMisMatch)
        mRegFieldSet(regVal, cAf6_upen_rxk12_intenb_enb_int_aps_ver_mismat_, (enableMask & cTha6029KbyteApsPwAlarmTypeVerMisMatch) ? 1 : 0);

    if (defectMask & cTha6029KbyteApsPwAlarmTypeLengthFieldMisMatch)
        mRegFieldSet(regVal, cAf6_upen_rxk12_intenb_enb_int_aps_lenfield_mismat_, (enableMask & cTha6029KbyteApsPwAlarmTypeLengthFieldMisMatch) ? 1 : 0);

    if (defectMask & cTha6029KbyteApsPwAlarmTypeLengthCountMisMatch)
        mRegFieldSet(regVal, cAf6_upen_rxk12_intenb_enb_int_aps_lencount_mismat_, (enableMask & cTha6029KbyteApsPwAlarmTypeLengthCountMisMatch) ? 1 : 0);

    if (defectMask & cTha6029KbyteApsPwAlarmTypeChannelMisMatch)
        mRegFieldSet(regVal, cAf6_upen_rxk12_intenb_enb_int_aps_glb_channel_mismat_, (enableMask & cTha6029KbyteApsPwAlarmTypeChannelMisMatch) ? 1 : 0);

    if (defectMask & cTha6029KbyteApsPwAlarmTypeWatchdogTimer)
        mRegFieldSet(regVal, cAf6_upen_rxk12_intenb_enb_int_aps_watdog_alarm_, (enableMask & cTha6029KbyteApsPwAlarmTypeWatchdogTimer) ? 1 : 0);

    return regVal;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->InterruptEnableRegister(mThis(self)) + BaseAddress((AtPw) self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    regVal = MaskSw2Hw(regVal, defectMask, enableMask);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    if (regVal)
        return AtChannelInterruptEnable(self);
    else
        return AtChannelInterruptDisable(self);
    }

static uint32 MaskHw2Sw(uint32 hwMask)
    {
    uint32 alarmMask = 0;

    if (hwMask & cAf6_upen_rxk12_intenb_enb_int_aps_macda_mismat_Mask)
        alarmMask |= cTha6029KbyteApsPwAlarmTypeDaMacMisMatch;

    if (hwMask & cAf6_upen_rxk12_intenb_enb_int_aps_ethtp_mismat_Mask) /* TODO: move to DCC */
        alarmMask |= cTha6029KbyteApsPwAlarmTypeEthTypeMisMatch;

    if (hwMask & cAf6_upen_rxk12_intenb_enb_int_aps_apstp_mismat_Mask)
        alarmMask |= cTha6029KbyteApsPwAlarmTypeTypeMisMatch;

    if (hwMask & cAf6_upen_rxk12_intenb_enb_int_aps_ver_mismat_Mask)
        alarmMask |= cTha6029KbyteApsPwAlarmTypeVerMisMatch;

    if (hwMask & cAf6_upen_rxk12_intenb_enb_int_aps_lenfield_mismat_Mask)
        alarmMask |= cTha6029KbyteApsPwAlarmTypeLengthFieldMisMatch;

    if (hwMask & cAf6_upen_rxk12_intenb_enb_int_aps_lencount_mismat_Mask)
        alarmMask |= cTha6029KbyteApsPwAlarmTypeLengthCountMisMatch;

    if (hwMask & cAf6_upen_rxk12_intenb_enb_int_aps_glb_channel_mismat_Mask)
        alarmMask |= cTha6029KbyteApsPwAlarmTypeChannelMisMatch;

    if (hwMask & cAf6_upen_rxk12_intenb_enb_int_aps_watdog_alarm_Mask)
        alarmMask |= cTha6029KbyteApsPwAlarmTypeWatchdogTimer;

    return alarmMask;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->InterruptEnableRegister(mThis(self)) + BaseAddress((AtPw) self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    return MaskHw2Sw(regVal);
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    return AtChannelInterruptMaskGet(self) ? cAtTrue : cAtFalse;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    Tha60290021PwKByte pw = (Tha60290021PwKByte)self;
    uint32 baseAddress = BaseAddress((AtPw) self);
    uint32 intrMask = mChannelHwRead(self, baseAddress + mMethodsGet(pw)->InterruptEnableRegister(pw), cAtModulePw);
    uint32 intrStatus = mChannelHwRead(self, baseAddress + mMethodsGet(pw)->InterruptStatusRegister(pw), cAtModulePw);
    uint32 currentStatus = mChannelHwRead(self, baseAddress + mMethodsGet(pw)->CurrentStatusRegister(pw), cAtModulePw);
    uint32 changedDefects = 0;
    uint32 defects = 0;
    AtUnused(offset);

    intrStatus &= intrMask;
    mChannelHwWrite(self, baseAddress + mMethodsGet(pw)->InterruptStatusRegister(pw), intrStatus, cAtModulePw);

    if (intrStatus & cAf6_upen_rxk12_intenb_enb_int_aps_macda_mismat_Mask)
        {
        changedDefects |= cTha6029KbyteApsPwAlarmTypeDaMacMisMatch;
        if (currentStatus & cAf6_upen_rxk12_intenb_enb_int_aps_macda_mismat_Mask)
            defects |= cTha6029KbyteApsPwAlarmTypeDaMacMisMatch;
        }

    if (intrStatus & cAf6_upen_rxk12_intenb_enb_int_aps_ethtp_mismat_Mask)
        {
        changedDefects |= cTha6029KbyteApsPwAlarmTypeEthTypeMisMatch;
        if (currentStatus & cAf6_upen_rxk12_intenb_enb_int_aps_ethtp_mismat_Mask)
            defects |= cTha6029KbyteApsPwAlarmTypeEthTypeMisMatch;
        }

    if (intrStatus & cAf6_upen_rxk12_intenb_enb_int_aps_apstp_mismat_Mask)
        {
        changedDefects |= cTha6029KbyteApsPwAlarmTypeTypeMisMatch;
        if (currentStatus & cAf6_upen_rxk12_intenb_enb_int_aps_apstp_mismat_Mask)
            defects |= cTha6029KbyteApsPwAlarmTypeTypeMisMatch;
        }

    if (intrStatus & cAf6_upen_rxk12_intenb_enb_int_aps_ver_mismat_Mask)
        {
        changedDefects |= cTha6029KbyteApsPwAlarmTypeVerMisMatch;
        if (currentStatus & cAf6_upen_rxk12_intenb_enb_int_aps_ver_mismat_Mask)
            defects |= cTha6029KbyteApsPwAlarmTypeVerMisMatch;
        }

    if (intrStatus & cAf6_upen_rxk12_intenb_enb_int_aps_lenfield_mismat_Mask)
        {
        changedDefects |= cTha6029KbyteApsPwAlarmTypeLengthFieldMisMatch;
        if (currentStatus & cAf6_upen_rxk12_intenb_enb_int_aps_lenfield_mismat_Mask)
            defects |= cTha6029KbyteApsPwAlarmTypeLengthFieldMisMatch;
        }

    if (intrStatus & cAf6_upen_rxk12_intenb_enb_int_aps_lencount_mismat_Mask)
        {
        changedDefects |= cTha6029KbyteApsPwAlarmTypeLengthCountMisMatch;
        if (currentStatus & cAf6_upen_rxk12_intenb_enb_int_aps_lencount_mismat_Mask)
            defects |= cTha6029KbyteApsPwAlarmTypeLengthCountMisMatch;
        }

    if (intrStatus & cAf6_upen_rxk12_intenb_enb_int_aps_watdog_alarm_Mask)
        {
        changedDefects |= cTha6029KbyteApsPwAlarmTypeWatchdogTimer;
        if (currentStatus & cAf6_upen_rxk12_intenb_enb_int_aps_watdog_alarm_Mask)
            defects |= cTha6029KbyteApsPwAlarmTypeWatchdogTimer;
        }

    if (intrStatus & cAf6_upen_rxk12_intenb_enb_int_aps_glb_channel_mismat_Mask)
        {
        changedDefects |= cTha6029KbyteApsPwAlarmTypeChannelMisMatch;
        if (currentStatus & cAf6_upen_rxk12_intenb_enb_int_aps_glb_channel_mismat_Mask)
            defects |= cTha6029KbyteApsPwAlarmTypeChannelMisMatch;
        }

    AtChannelAllAlarmListenersCall(self, changedDefects, defects);

    return 0;
    }

static uint32 HwInterruptGet(AtChannel self, eBool r2c)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->InterruptStatusRegister(mThis(self))  + BaseAddress((AtPw) self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 alarmMask = MaskHw2Sw(regVal);

    /* Clear Interrupt Status */
    if (alarmMask && r2c)
        {
        regVal &= ~cAf6_upen_int_rxk12_stt_aps_per_channel_mismat_Mask;
        mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
        }

    return alarmMask;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return HwInterruptGet(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return HwInterruptGet(self, cAtTrue);
    }

static uint32 SpecificDefectInterruptClear(AtChannel self, uint32 defectTypes)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->InterruptStatusRegister(mThis(self)) + BaseAddress((AtPw) self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 alarmMask = MaskHw2Sw(regVal);

    /* Only set 1 (clear) for defects in 'defectTypes', otherwise, set 0. */
    regVal = MaskSw2Hw(regVal, cBit31_0, defectTypes);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    /* Return interrupt for defects on request */
    return (alarmMask & defectTypes);
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)(mThis(self)->counters));
    mThis(self)->counters = NULL;
    KbyteChannelsDelete(mThis(self));
    m_AtObjectMethods->Delete(self);
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->CurrentStatusRegister(mThis(self)) + BaseAddress((AtPw)self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleSdh);
    return MaskHw2Sw(regVal);
    }

static eAtRet Debug(AtChannel self)
    {
    AtPw pw = (AtPw)self;
    eAtRet ret = m_AtChannelMethods->Debug(self);

    AtPrintc(cSevNormal, "* TX       ETHTYPE: 0x%04x\r\n", TxEthTypeGet(pw));
    AtPrintc(cSevNormal, "* Expected ETHTYPE: 0x%04x\r\n", ExpectedEthTypeGet(pw));
    AtPrintc(cSevNormal, "* TX       TYPE   : 0x%02x\r\n", TxTypeGet(pw));
    AtPrintc(cSevNormal, "* Expected TYPE   : 0x%02x\r\n", ExpectedTypeGet(pw));
    AtPrintc(cSevNormal, "* TX       VERSION: 0x%02x\r\n", TxVersionGet(pw));
    AtPrintc(cSevNormal, "* Expected VERSION: 0x%02x\r\n", ExpectedVersionGet(pw));

    return ret;
    }

static uint32 InterruptEnableRegister(Tha60290021PwKByte self)
    {
    AtUnused(self);
    return cAf6Reg_upen_rxk12_intenb_Base;
    }

static uint32 InterruptStatusRegister(Tha60290021PwKByte self)
    {
    AtUnused(self);
    return cAf6Reg_upen_int_rxk12_stt_Base;
    }

static uint32 CurrentStatusRegister(Tha60290021PwKByte self)
    {
    AtUnused(self);
    return cAf6Reg_upen_k12rx_cur_err_Base;
    }

static AtPwKbyteChannel *KbyteChannelsCreate(AtPw self)
    {
    uint32 numChannels = Tha60290021PwKbyteNumKbyteChannelsGet(self);
    AtPwKbyteChannel *kbyteChannels;

    kbyteChannels = AtOsalMemAlloc(sizeof(AtPwKbyteChannel) * numChannels);
    if (kbyteChannels)
        AtOsalMemInit(kbyteChannels, 0, sizeof(AtPwKbyteChannel) * numChannels);

    return kbyteChannels;
    }

static AtPwKbyteChannel KbyteChannelGet(AtPw self, uint32 channelId)
    {
    Tha60290021ModulePw module = (Tha60290021ModulePw)AtChannelModuleGet((AtChannel)self);
    Tha60290021PwKByte pw = (Tha60290021PwKByte)self;
    uint32 numChannels = Tha60290021PwKbyteNumKbyteChannelsGet(self);

    if (channelId >= numChannels)
        return NULL;

    if (pw->kbyteChannels == NULL)
        pw->kbyteChannels = KbyteChannelsCreate(self);

    if (pw->kbyteChannels)
        {
        if (pw->kbyteChannels[channelId] == NULL)
            pw->kbyteChannels[channelId] = Tha60290021ModulePwKByteChannelObjectCreate(module, channelId, self);
        return pw->kbyteChannels[channelId];
        }

    return NULL;
    }

static void KbyteChannelsDelete(Tha60290021PwKByte self)
    {
    uint32 numChannels = Tha60290021PwKbyteNumKbyteChannelsGet((AtPw)self);
    uint32 channel_i;

    if (self->kbyteChannels == NULL)
        return;

    for (channel_i = 0; channel_i < numChannels; channel_i++)
        {
        if (self->kbyteChannels[channel_i])
            {
            AtObjectDelete((AtObject)self->kbyteChannels[channel_i]);
            self->kbyteChannels[channel_i] = NULL;
            }
        }

    AtOsalMemFree(self->kbyteChannels);
    self->kbyteChannels = NULL;
    }

static void MethodsInit(AtPw self)
    {
    Tha60290021PwKByte kBytePw = (Tha60290021PwKByte)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, InterruptEnableRegister);
        mMethodOverride(m_methods, InterruptStatusRegister);
        mMethodOverride(m_methods, CurrentStatusRegister);
        }

    mMethodsSet(kBytePw, &m_methods);
    }

static void OverrideAtPw(AtPw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, EthHeaderSet);
        mMethodOverride(m_AtPwOverride, EthDestMacSet);
        mMethodOverride(m_AtPwOverride, EthDestMacGet);
        mMethodOverride(m_AtPwOverride, EthSrcMacSet);
        mMethodOverride(m_AtPwOverride, EthSrcMacGet);

        mMethodOverride(m_AtPwOverride, EthVlanSet);
        mMethodOverride(m_AtPwOverride, EthCVlanGet);
        mMethodOverride(m_AtPwOverride, EthSVlanGet);
        mMethodOverride(m_AtPwOverride, EthPortSet);
        mMethodOverride(m_AtPwOverride, EthPortGet);
        mMethodOverride(m_AtPwOverride, PsnSet);
        mMethodOverride(m_AtPwOverride, BoundCircuitGet);
        mMethodOverride(m_AtPwOverride, CircuitBind);
        mMethodOverride(m_AtPwOverride, CircuitUnbind);
        mMethodOverride(m_AtPwOverride, LopsSetThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdSet);
        mMethodOverride(m_AtPwOverride, PayloadSizeSet);
        mMethodOverride(m_AtPwOverride, PrioritySet);
        mMethodOverride(m_AtPwOverride, ReorderingEnable);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeSet);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeInPacketSet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelaySet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelayInPacketSet);
        mMethodOverride(m_AtPwOverride, RtpEnable);
        mMethodOverride(m_AtPwOverride, RtpTxPayloadTypeSet);
        mMethodOverride(m_AtPwOverride, RtpExpectedPayloadTypeSet);
        mMethodOverride(m_AtPwOverride, RtpPayloadTypeCompare);
        mMethodOverride(m_AtPwOverride, RtpPayloadTypeSet);
        mMethodOverride(m_AtPwOverride, RtpTxSsrcSet);
        mMethodOverride(m_AtPwOverride, RtpExpectedSsrcSet);
        mMethodOverride(m_AtPwOverride, RtpSsrcCompare);
        mMethodOverride(m_AtPwOverride, RtpSsrcSet);
        mMethodOverride(m_AtPwOverride, RtpTimeStampModeSet);
        mMethodOverride(m_AtPwOverride, SuppressEnable);
        mMethodOverride(m_AtPwOverride, CwEnable);
        mMethodOverride(m_AtPwOverride, CwAutoTxLBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRBitEnable);
        mMethodOverride(m_AtPwOverride, CwSequenceModeSet);
        mMethodOverride(m_AtPwOverride, CwLengthModeSet);
        mMethodOverride(m_AtPwOverride, CwPktReplaceModeSet);
        mMethodOverride(m_AtPwOverride, EthExpectedCVlanSet);
        mMethodOverride(m_AtPwOverride, EthExpectedSVlanSet);
        mMethodOverride(m_AtPwOverride, JitterBufferAndPayloadSizeSet);
        mMethodOverride(m_AtPwOverride, JitterBufferInPacketAndPayloadSizeSet);
        mMethodOverride(m_AtPwOverride, IdleCodeSet);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, ExcessivePacketLossRateDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, StrayPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MalformedPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, RemotePacketLossDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, JitterBufferOverrunDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, JitterBufferUnderrunDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MisConnectionDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, ApsGroupGet);
        mMethodOverride(m_AtPwOverride, HsGroupGet);
        mMethodOverride(m_AtPwOverride, BackupPsnSet);
        mMethodOverride(m_AtPwOverride, BackupPsnGet);
        mMethodOverride(m_AtPwOverride, BackupEthHeaderSet);
        mMethodOverride(m_AtPwOverride, BackupEthDestMacSet);
        mMethodOverride(m_AtPwOverride, BackupEthSrcMacSet);
        mMethodOverride(m_AtPwOverride, BackupEthSrcMacGet);
        mMethodOverride(m_AtPwOverride, BackupEthDestMacGet);
        mMethodOverride(m_AtPwOverride, BackupEthVlanSet);
        mMethodOverride(m_AtPwOverride, EthPortQueueSet);
        mMethodOverride(m_AtPwOverride, PtchInsertionModeSet);
        mMethodOverride(m_AtPwOverride, TxActiveForce);
        mMethodOverride(m_AtPwOverride, CwLengthModeIsSupported);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitCanEnable);
        }

    mMethodsSet(self, &m_AtPwOverride);
    }

static void OverrideAtChannel(AtPw self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);

        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SpecificDefectInterruptClear);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, Debug);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtPw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPw self)
    {
    OverrideAtPw(self);
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PwKByte);
    }

AtPw Tha60290021PwKbyteObjectInit(AtPw self, uint32 pwId, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPwObjectInit(self, pwId, module, cAtPwTypeKbyte) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPw Tha60290021PwKbyteNew(uint32 pwId, AtModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPw newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021PwKbyteObjectInit(newChannel, pwId, module);
    }

AtPwKbyteChannel Tha60290021PwKbyteChannelGet(AtPw self, uint32 channelId)
    {
    if (self)
        return KbyteChannelGet(self, channelId);
    return NULL;
    }

uint32 Tha60290021PwKbyteNumKbyteChannelsGet(AtPw self)
    {
    AtUnused(self);
    return 16;
    }

eAtRet Tha6029PwKbyteIntervalSet(AtPw self, uint32 intervalInUs)
    {
    if (self)
        return IntervalSet(self, intervalInUs);
    return cAtErrorNullPointer;
    }

uint32 Tha6029PwKbyteIntervalGet(AtPw self)
    {
    if (self)
        return IntervalGet(self);
    return 0;
    }

eAtRet Tha6029PwKbyteTrigger(AtPw self)
    {
    if (self)
        return Trigger(self);
    return cAtErrorNullPointer;
    }

eAtRet Tha6029PwKbyteWatchdogSet(AtPw self, uint32 timerInUs)
    {
    if (self)
        return WatchdogSet(self, timerInUs);
    return cAtErrorNullPointer;
    }

uint32 Tha6029PwKbyteWatchdogGet(AtPw self)
    {
    if (self)
        return WatchdogGet(self);
    return 0;
    }

eAtRet Tha6029PwKbytePwTxEthTypeSet(AtPw self, uint16 ethType)
    {
    if (self)
        return TxEthTypeSet(self, ethType);
    return cAtErrorObjectNotExist;
    }

uint16 Tha6029PwKbyteTxEthTypeGet(AtPw self)
    {
    if (self)
        return TxEthTypeGet(self);
    return cInvalidUint16;
    }

eAtRet Tha6029PwKbytePwExpectedEthTypeSet(AtPw self, uint16 ethType)
    {
    if (self)
        return ExpectedEthTypeSet(self, ethType);
    return cAtErrorObjectNotExist;
    }

uint16 Tha6029PwKbytePwExpectedEthTypeGet(AtPw self)
    {
    if (self)
        return ExpectedEthTypeGet(self);
    return cInvalidUint16;
    }

eAtRet Tha6029PwKbyteExpectedTypeSet(AtPw self, uint8 type)
    {
    if (self)
        return ExpectedTypeSet(self, type);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha6029PwKbyteTxTypeSet(AtPw self, uint8 type)
    {
    if (self)
        return TxTypeSet(self, type);
    return cAtErrorObjectNotExist;
    }

uint8 Tha6029PwKbyteTxTypeGet(AtPw self)
    {
    if (self)
        return TxTypeGet(self);
    return cInvalidUint8;
    }

eAtRet Tha6029PwKbyteTxVersionSet(AtPw self, uint8 version)
    {
    if (self)
        return TxVersionSet(self, version);
    return cAtErrorObjectNotExist;
    }

uint8 Tha6029PwKbyteTxVersionGet(AtPw self)
    {
    if (self)
        return TxVersionGet(self);
    return cInvalidUint8;
    }

eAtRet Tha6029PwKbyteExpectedVersionSet(AtPw self, uint8 version)
    {
    if (self)
        return ExpectedVersionSet(self, version);
    return cAtErrorObjectNotExist;
    }

uint32 Tha60290021PwKByteBaseAddress(AtPw self)
    {
    return BaseAddress(self);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290021PwHdlcV2.c
 *
 * Created Date: Dec 12, 2017
 *
 * Description : DCC PW HDLC version2 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/common/AtEthVlanTagInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../encap/Tha60290021HdlcChannel.h"
#include "Tha60290021DccKbyteV2Reg.h"
#include "Tha60290021PwHdlcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mAddressWithLocalAddress(self, localAddress) mMethodsGet((Tha60290021SerdesBackplaneEthPort)self)->AddressWithLocalAddress((Tha60290021SerdesBackplaneEthPort)self, localAddress)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods         m_AtChannelOverride;
static tAtPwMethods              m_AtPwOverride;
static tTha60290021PwHdlcMethods m_Tha60290021PwHdlcOverride;

/* Save super implementation */
static const tAtChannelMethods         *m_AtChannelMethods = NULL;
static const tAtPwMethods              *m_AtPwMethods              = NULL;
static const tTha60290021PwHdlcMethods *m_Tha60290021PwHdlcMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DccTxHeaderRegAddress(Tha60290021PwHdlc self)
    {
    return Tha60290021DccAddressWithLocalAddress((AtPw)self, cAf6Reg_upen_dcchdr_Base + Tha60290021DccChannelId((AtPw)self) * 16UL);
    }

static uint32 DccTxHeaderRegWithOffset(AtPw self, uint32 offset)
    {
    return Tha60290021PwHdlcDccTxHeaderRegAddress((Tha60290021PwHdlc)self) + offset;
    }

static uint32 DestMacReg(AtPw self)
    {
    return DccTxHeaderRegWithOffset(self, 0);
    }

static uint32 TxSrcMacReg(AtPw self)
    {
    return DccTxHeaderRegWithOffset(self, 6);
    }

static uint32 EthVlanReg(AtPw self)
    {
    return DccTxHeaderRegWithOffset(self, 12);
    }

static uint32 VersionReg(Tha60290021PwHdlc self)
    {
    return DccTxHeaderRegWithOffset((AtPw)self, 14);
    }

static uint32 TypeReg(Tha60290021PwHdlc self)
    {
    return DccTxHeaderRegWithOffset((AtPw)self, 15);
    }

static eAtModulePwRet HwEthDestMacSet(AtPw self, uint8 *destMac)
    {
    uint32 regAddr = DestMacReg(self);

    mChannelHwWrite(self, regAddr, destMac[0], cAtModuleSdh);
    mChannelHwWrite(self, (regAddr + 1), destMac[1], cAtModuleSdh);
    mChannelHwWrite(self, (regAddr + 2), destMac[2], cAtModuleSdh);
    mChannelHwWrite(self, (regAddr + 3), destMac[3], cAtModuleSdh);
    mChannelHwWrite(self, (regAddr + 4), destMac[4], cAtModuleSdh);
    mChannelHwWrite(self, (regAddr + 5), destMac[5], cAtModuleSdh);

    return cAtOk;
    }

static eAtModulePwRet EthDestMacSet(AtPw self, uint8 *destMac)
    {
    if (destMac == NULL)
        return cAtErrorNullPointer;

    return HwEthDestMacSet(self, destMac);
    }

static eAtModulePwRet HwEthDestMacGet(AtPw self, uint8 *destMac)
    {
    uint32 regAddr = DestMacReg(self);

    destMac[0] = (uint8)mChannelHwRead(self, regAddr, cAtModuleSdh);
    destMac[1] = (uint8)mChannelHwRead(self, (regAddr + 1), cAtModuleSdh);
    destMac[2] = (uint8)mChannelHwRead(self, (regAddr + 2), cAtModuleSdh);
    destMac[3] = (uint8)mChannelHwRead(self, (regAddr + 3), cAtModuleSdh);
    destMac[4] = (uint8)mChannelHwRead(self, (regAddr + 4), cAtModuleSdh);
    destMac[5] = (uint8)mChannelHwRead(self, (regAddr + 5), cAtModuleSdh);

    return cAtOk;
    }

static eAtModulePwRet EthDestMacGet(AtPw self, uint8 *destMac)
    {
    if (destMac == NULL)
        return cAtErrorNullPointer;

    return HwEthDestMacGet(self, destMac);
    }

static eAtModulePwRet TxSrcMacSet(AtPw self, uint8 *srcMac)
    {
    uint32 regAddr = TxSrcMacReg(self);

    mChannelHwWrite(self, regAddr, srcMac[0], cAtModuleSdh);
    mChannelHwWrite(self, (regAddr + 1), srcMac[1], cAtModuleSdh);
    mChannelHwWrite(self, (regAddr + 2), srcMac[2], cAtModuleSdh);
    mChannelHwWrite(self, (regAddr + 3), srcMac[3], cAtModuleSdh);
    mChannelHwWrite(self, (regAddr + 4), srcMac[4], cAtModuleSdh);
    mChannelHwWrite(self, (regAddr + 5), srcMac[5], cAtModuleSdh);

    return cAtOk;
    }

static eAtModulePwRet EthSrcMacSet(AtPw self, uint8 *srcMac)
    {
    if (srcMac == NULL)
        return cAtErrorNullPointer;
    return TxSrcMacSet(self, srcMac);
    }

static eAtModulePwRet HwEthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    uint32 regAddr = TxSrcMacReg(self);

    srcMac[0] = (uint8)mChannelHwRead(self, regAddr,       cAtModuleSdh);
    srcMac[1] = (uint8)mChannelHwRead(self, (regAddr + 1), cAtModuleSdh);
    srcMac[2] = (uint8)mChannelHwRead(self, (regAddr + 2), cAtModuleSdh);
    srcMac[3] = (uint8)mChannelHwRead(self, (regAddr + 3), cAtModuleSdh);
    srcMac[4] = (uint8)mChannelHwRead(self, (regAddr + 4), cAtModuleSdh);
    srcMac[5] = (uint8)mChannelHwRead(self, (regAddr + 5), cAtModuleSdh);

    return cAtOk;
    }

static eAtModulePwRet EthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    if (srcMac)
        return HwEthSrcMacGet(self, srcMac);
    return cAtErrorNullPointer;
    }

static eAtModulePwRet HwEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan)
    {
    uint32 regAddr = EthVlanReg(self);
    uint32 vlan = AtEthVlanTagToUint32(cVlan);

    mChannelHwWrite(self, regAddr + 1, vlan & cBit7_0, cAtModuleSdh);
    mChannelHwWrite(self, regAddr, (vlan & cBit15_8) >> 8, cAtModuleSdh);

    return cAtOk;
    }

static eAtModulePwRet EthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    /* Not Support S-VLAN */
    if (sVlan)
        return cAtErrorModeNotSupport;

    if (!cVlan)
        return cAtErrorModeNotSupport;

    return HwEthVlanSet(self, cVlan);
    }

static tAtEthVlanTag* HwEthVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    uint32 regAddr = EthVlanReg(self);
    uint8 vlanBytes[2];
    uint32 vlan;

    vlanBytes[1] = (uint8)mChannelHwRead(self, regAddr + 1, cAtModuleSdh);
    vlanBytes[0] = (uint8)mChannelHwRead(self, regAddr, cAtModuleSdh);

    mFieldIns(&vlan, cBit7_0, 0, vlanBytes[1]);
    mFieldIns(&vlan, cBit15_8, 8, vlanBytes[0]);

    return AtEthVlanTagFromUint32(vlan, cVlan);
    }

static tAtEthVlanTag* EthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    if (cVlan == NULL)
        return NULL;

    return HwEthVlanGet(self, cVlan);
    }

static eAtRet DccPwVersionSet(Tha60290021PwHdlc self, uint8 version)
    {
    mChannelHwWrite(self, VersionReg(self), version, cAtModuleSdh);
    return cAtOk;
    }

static uint8 DccPwVersionGet(Tha60290021PwHdlc self)
    {
    return (uint8)mChannelHwRead(self, VersionReg(self), cAtModuleSdh);
    }

static eAtRet DccPwTypeSet(Tha60290021PwHdlc self, uint8 type)
    {
    mChannelHwWrite(self, TypeReg(self), type, cAtModuleSdh);
    return cAtOk;
    }

static uint8 DccPwTypeGet(Tha60290021PwHdlc self)
    {
    return (uint8)mChannelHwRead(self, TypeReg(self), cAtModuleSdh);
    }

static uint32 CounterBaseAddress(Tha60290021PwHdlc self, uint16 counterType, eBool r2c)
    {
    Tha60290021HdlcChannel hdlc;

    AtUnused(r2c);
    hdlc = (Tha60290021HdlcChannel)AtPwBoundCircuitGet((AtPw)self);
    if (hdlc == NULL)
        return cInvalidUint32;

    switch (counterType)
        {
        case cAtPwCounterTypeTxPackets:      return Tha60290021HdlcChannelCounterBaseAddressByHwType(hdlc, cAf6Reg_upen_dcc_cnt_type_TDM2PSN_BUF2SGM_pkt);
        case cAtPwCounterTypeTxPayloadBytes: return Tha60290021HdlcChannelCounterBaseAddressByHwType(hdlc, cAf6Reg_upen_dcc_cnt_type_TDM2PSN_BUF2SGM_byte);
        case cAtPwCounterTypeRxPackets:      return Tha60290021HdlcChannelCounterBaseAddressByHwType(hdlc, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGM2BUF_good_pkt);
        case cAtPwCounterTypeRxPayloadBytes: return Tha60290021HdlcChannelCounterBaseAddressByHwType(hdlc, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGM2BUF_byte);
        case cAtPwCounterTypeRxLostPackets:  return Tha60290021HdlcChannelCounterBaseAddressByHwType(hdlc, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGMI2BUF_lost_pkt);
        case cAtPwCounterTypeRxDiscardedPackets:  return Tha60290021HdlcChannelCounterBaseAddressByHwType(hdlc, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGM2BUF_error_pkt);

        default:                             return cInvalidUint32;
        }
    }

static uint32 DccChannelId(AtPw self)
    {
    AtChannel circuit = AtPwBoundCircuitGet(self);
    return AtChannelIdGet(circuit);
    }

static uint32 V1DefectGet(AtChannel self)
    {
    uint32 regAddr = Tha60290021DccAddressWithLocalAddress((AtPw)self, cAf6Reg_upen_cur_rxdcc0);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 channelMask = cBit0 << DccChannelId((AtPw) self);

    if (regVal & channelMask)
        return cTha6029DccPwAlarmTypePktDiscards;

    return 0;
    }

static eBool ShouldUseInterruptV2(AtChannel self)
    {
    return mMethodsGet((Tha60290021PwHdlc)self)->HasInterruptV2((Tha60290021PwHdlc)self);
    }

static ThaModuleOcn OcnModule(AtHdlcChannel self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);
    }

static uint32 BaseAddress(AtHdlcChannel self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self));
    }

static uint32 DefaultOffset(AtChannel self)
    {
    return BaseAddress((AtHdlcChannel)self) + AtChannelIdGet(self);
    }

static uint32 CurrentStatusRegister(AtChannel self)
    {
    return cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_Base + DefaultOffset(self);
    }

static uint32 DefectHw2Sw(AtChannel self, uint32 regVal)
    {
    uint32 swAlarm = 0;
    AtUnused(self);

    if (regVal & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_eth2ocn_dcc_chan_dis_Mask)
        swAlarm |= cTha6029DccPwAlarmTypePktDiscards;

    return swAlarm;
    }

static uint32 V2DefectGet(AtChannel self)
    {
    uint32 regAddr = CurrentStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return DefectHw2Sw(self, regVal);
    }

static uint32 DefectGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return V2DefectGet(self);

    return V1DefectGet(self);
    }

static uint32 InterruptStatusRegister(AtChannel self)
    {
    return cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_Base + DefaultOffset(self);
    }

static uint32 DefectHistoryRead2Clear(AtChannel self, eBool clear)
    {
    static const uint32 cDccPwHwDefect = cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_eth2ocn_dcc_chan_dis_Mask;
    uint32 regAddr = InterruptStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 swAlarm = DefectHw2Sw(self, regVal);
    if (clear)
        mChannelHwWrite(self, regAddr, (regVal & cDccPwHwDefect), cThaModuleOcn);
    return swAlarm;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return DefectHistoryRead2Clear(self, cAtFalse);

    return m_AtChannelMethods->DefectHistoryGet(self);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return DefectHistoryRead2Clear(self, cAtTrue);

    return m_AtChannelMethods->DefectHistoryClear(self);
    }

static uint32 InterruptMaskRegister(AtChannel self)
    {
    return cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_Base + DefaultOffset(self);
    }

static eAtRet HwInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = InterruptMaskRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (defectMask & cTha6029DccPwAlarmTypePktDiscards)
        mRegFieldSet(regVal, cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_eth2ocn_dcc_chan_dis_,
                     (enableMask & cTha6029DccPwAlarmTypePktDiscards) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    if (ShouldUseInterruptV2(self))
        return HwInterruptMaskSet(self, defectMask, enableMask);

    return m_AtChannelMethods->InterruptMaskSet(self, defectMask, enableMask);
    }

static uint32 HwInterruptMaskGet(AtChannel self)
    {
    uint32 regAddr = InterruptMaskRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return DefectHw2Sw(self, regVal);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return HwInterruptMaskGet(self);

    return m_AtChannelMethods->InterruptMaskGet(self);
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    uint32 intrAddress = InterruptStatusRegister(self);
    uint32 intrStatus = mChannelHwRead(self, intrAddress, cAtModulePw);
    uint32 intrMask = mChannelHwRead(self, InterruptMaskRegister(self), cAtModulePw);
    uint32 currentStatus = mChannelHwRead(self, CurrentStatusRegister(self), cAtModulePw);
    uint32 changedDefects = 0;
    uint32 defects = 0;
    AtUnused(offset);

    intrStatus &= intrMask;
    mChannelHwWrite(self, intrAddress, cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_eth2ocn_dcc_chan_dis_Mask, cAtModulePw);

    if (intrStatus & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_eth2ocn_dcc_chan_dis_Mask)
        {
        changedDefects |= cTha6029DccPwAlarmTypePktDiscards;
        if (currentStatus & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_eth2ocn_dcc_chan_dis_Mask)
            defects |= cTha6029DccPwAlarmTypePktDiscards;
        }

    AtChannelAllAlarmListenersCall(self, changedDefects, defects);
    return 0;
    }

static eBool HasInterruptV2(Tha60290021PwHdlc self)
    {
    Tha60290021ModulePw pwModule = (Tha60290021ModulePw)AtChannelModuleGet((AtChannel)self);
    return Tha60290021ModulePwDccKbyteInterruptIsSupported(pwModule);
    }

static void OverrideTha60290021PwHdlc(AtPwHdlc self)
    {
    Tha60290021PwHdlc pwHdlc = (Tha60290021PwHdlc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021PwHdlcMethods = mMethodsGet(pwHdlc);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021PwHdlcOverride, m_Tha60290021PwHdlcMethods, sizeof(m_Tha60290021PwHdlcOverride));

        mMethodOverride(m_Tha60290021PwHdlcOverride, DccPwTypeSet);
        mMethodOverride(m_Tha60290021PwHdlcOverride, DccPwTypeGet);
        mMethodOverride(m_Tha60290021PwHdlcOverride, DccPwVersionSet);
        mMethodOverride(m_Tha60290021PwHdlcOverride, DccPwVersionGet);
        mMethodOverride(m_Tha60290021PwHdlcOverride, DccTxHeaderRegAddress);
        mMethodOverride(m_Tha60290021PwHdlcOverride, CounterBaseAddress);
        mMethodOverride(m_Tha60290021PwHdlcOverride, HasInterruptV2);
        }

    mMethodsSet(pwHdlc, &m_Tha60290021PwHdlcOverride);
    }

static void OverrideAtPw(AtPwHdlc self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, EthDestMacSet);
        mMethodOverride(m_AtPwOverride, EthDestMacGet);
        mMethodOverride(m_AtPwOverride, EthSrcMacSet);
        mMethodOverride(m_AtPwOverride, EthSrcMacGet);
        mMethodOverride(m_AtPwOverride, EthVlanSet);
        mMethodOverride(m_AtPwOverride, EthCVlanGet);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideAtChannel(AtPwHdlc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPwHdlc self)
    {
    OverrideTha60290021PwHdlc(self);
    OverrideAtPw(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PwHdlcV2);
    }

AtPwHdlc Tha60290021PwDccV2ObjectInit(AtPwHdlc self, AtHdlcChannel dcc, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (Tha60290021PwDccObjectInit(self, dcc, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPwHdlc Tha60290021PwDccV2New(AtHdlcChannel dcc, AtModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwHdlc newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021PwDccV2ObjectInit(newChannel, dcc, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW Module
 * 
 * File        : Tha60290021PwHdlcInternal.c
 * 
 * Created Date: Sep 4, 2016
 *
 * Description : Tha60290021 HdlcPw Internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021PWHDLCINTERNAL_H_
#define _THA60290021PWHDLCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/pw/AtPwInternal.h"
#include "Tha60290021ModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021PwHdlcMethods
	{
    eAtRet (*DccPwEthTypeSet)(Tha60290021PwHdlc self, uint16 ethType);
    uint16 (*DccPwEthTypeGet)(Tha60290021PwHdlc self);
    eAtRet (*DccPwTypeSet)(Tha60290021PwHdlc self, uint8 type);
    uint8 (*DccPwTypeGet)(Tha60290021PwHdlc self);
    eAtRet (*DccPwVersionSet)(Tha60290021PwHdlc self, uint8 version);
    uint8 (*DccPwVersionGet)(Tha60290021PwHdlc self);
    eAtRet (*DccPwMacCheckEnable)(Tha60290021PwHdlc self, eBool enable);
    eBool (*DccPwMacCheckIsEnabled)(Tha60290021PwHdlc self);
    eAtRet (*DccPwCVlanCheckEnable)(Tha60290021PwHdlc self, eBool enable);
    eBool (*DccPwCVlanCheckIsEnabled)(Tha60290021PwHdlc self);
    AtEthPort (*DefaultSgmiiEthPort)(Tha60290021PwHdlc self);
    eAtRet (*DccPwExpectedLabelSet)(Tha60290021PwHdlc self, uint8 label);
    uint8 (*DccPwExpectedLabelGet)(Tha60290021PwHdlc self);
    uint8 (*LabelMaxValue)(Tha60290021PwHdlc self);
    eBool (*IsExpectedLabelIsUsed)(Tha60290021PwHdlc self, uint8 label);
    uint16 (*LongReadOnCore)(Tha60290021PwHdlc self, uint32 regAddr, uint32 *longRegVal, uint16 bufferLen);
    uint16 (*LongWriteOnCore)(Tha60290021PwHdlc self, uint32 regAddr, uint32 *longRegVal, uint16 bufferLen);
    uint32 (*DccTxHeaderRegAddress)(Tha60290021PwHdlc self);
    eAtRet (*HwDccPwLabelChannelSet)(Tha60290021PwHdlc self, uint8 label, uint32 channelId);
    uint32 (*HwDccPwLabelChannelGet)(Tha60290021PwHdlc self, uint8 label);
    uint32 (*CounterBaseAddress)(Tha60290021PwHdlc self, uint16 counterType, eBool r2c);
    eBool (*HasInterruptV2)(Tha60290021PwHdlc self);
	}tTha60290021PwHdlcMethods;

typedef struct tTha60290021PwHdlc
    {
    tAtPwHdlc super;
    const tTha60290021PwHdlcMethods * methods;

    /* Private data */
    AtChannel circuit;
    AtEthPort port;
    AtPwCounters counters;
    uint8 expectedLabel; /* To audit */
    }tTha60290021PwHdlc;

typedef struct tTha60290021PwHdlcV2
    {
    tTha60290021PwHdlc super;
    }tTha60290021PwHdlcV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPwHdlc Tha60290021PwDccObjectInit(AtPwHdlc self, AtHdlcChannel dcc, AtModulePw module);
AtPwHdlc Tha60290021PwDccV2ObjectInit(AtPwHdlc self, AtHdlcChannel dcc, AtModulePw module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021PWHDLCINTERNAL_H_ */


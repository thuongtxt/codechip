/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      : PW                                                               
 *                                                                              
 * File        : Tha60290021DccKbyteV2Reg.h                                                               
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of DCC/Kbyte block 
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _THA60290021DCCKBYTEV2REG_H_
#define _THA60290021DCCKBYTEV2REG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : DCC Version Reg
Reg Addr   : 0x00000
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides DCC's code version

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_ver                                                                           0x00000

/*--------------------------------------
BitField Name: DCC_VER
BitField Type: RO
BitField Desc: DAY_MON_YEAR_HOUR [31:24]: DAY [23:16]: MONTH [15:08]: YEAR
[07:00]: HOUR
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_ver_DCC_VER_Mask                                                                cBit31_0
#define cAf6_upen_dcc_ver_DCC_VER_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : DCCK CPU Reg Hold Control
Reg Addr   : 0x0000A-0x0000B
Reg Formula: 0x0000A + $HoldId
    Where  : 
           + $HoldId(0-1): Hold register
Reg Desc   : 
The register provides hold register for two word 32-bits MSB when CPU access to engine.

------------------------------------------------------------------------------*/
#define cAf6Reg_dcck_cpu_hold_Base                                                                     0x0000A
#define cAf6Reg_dcck_cpu_hold(HoldId)                                                       (0x0000A+(HoldId))

/*--------------------------------------
BitField Name: HoldReg
BitField Type: RW
BitField Desc: Hold 32 bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dcck_cpu_hold_HoldReg_Mask                                                               cBit31_0
#define cAf6_dcck_cpu_hold_HoldReg_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Status
Reg Addr   : 0x02001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides global interrupt status

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_glbint_stt                                                                        0x02001
#define cAf6Reg_upen_glbint_stt_WidthVal                                                                    32

/*--------------------------------------
BitField Name: kbyte_interrupt
BitField Type: RO
BitField Desc: interrupt from KByte     event
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_glbint_stt_kbyte_interrupt_Mask                                                        cBit2
#define cAf6_upen_glbint_stt_kbyte_interrupt_Shift                                                           2

/*--------------------------------------
BitField Name: dcc_interrupt
BitField Type: RO
BitField Desc: interrupt from DCC event
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_glbint_stt_dcc_interrupt_Mask                                                          cBit1
#define cAf6_upen_glbint_stt_dcc_interrupt_Shift                                                             1

/*--------------------------------------
BitField Name: hdlc_interrupt
BitField Type: RO
BitField Desc: interrupt from HDLC event
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_glbint_stt_hdlc_interrupt_Mask                                                         cBit0
#define cAf6_upen_glbint_stt_hdlc_interrupt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Global Interrupt Enable
Reg Addr   : 0x00002
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable global interrupt

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_glb_intenb                                                                        0x00002
#define cAf6Reg_upen_glb_intenb_WidthVal                                                                    32

/*--------------------------------------
BitField Name: enb_kbyte_interrupt
BitField Type: W1C
BitField Desc: Enable interrupt for KByte events
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_glb_intenb_enb_kbyte_interrupt_Mask                                                    cBit2
#define cAf6_upen_glb_intenb_enb_kbyte_interrupt_Shift                                                       2

/*--------------------------------------
BitField Name: enb_dcc_interrupt
BitField Type: W1C
BitField Desc: Enable interrupt for DCC event
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_glb_intenb_enb_dcc_interrupt_Mask                                                      cBit1
#define cAf6_upen_glb_intenb_enb_dcc_interrupt_Shift                                                         1

/*--------------------------------------
BitField Name: enb_hdlc_interrupt
BitField Type: W1C
BitField Desc: Enable interrupt for HDLC event
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_glb_intenb_enb_hdlc_interrupt_Mask                                                     cBit0
#define cAf6_upen_glb_intenb_enb_hdlc_interrupt_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Status of Packet Classification Error
Reg Addr   : 0x01080
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtldcc_rx__upen_glb_sta_cur_glb                                               0x01080
#define cAf6Reg_af6ces10grtldcc_rx__upen_glb_sta_cur_glb_WidthVal                                           32

/*--------------------------------------
BitField Name: dcc_eth2ocn_cur_sta_type_mismat
BitField Type: RO
BitField Desc: Current status of event "Received ETHERNET TYPE value of DCC
frame different from global provisioned TYPE"
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_af6ces10grtldcc_rx__upen_glb_sta_cur_glb_dcc_eth2ocn_cur_sta_type_mismat_Mask                                   cBit4
#define cAf6_af6ces10grtldcc_rx__upen_glb_sta_cur_glb_dcc_eth2ocn_cur_sta_type_mismat_Shift                                       4

/*--------------------------------------
BitField Name: dcc_eth2ocn_cur_sta_ovrsize_len
BitField Type: RO
BitField Desc: Current status of event "Received DCC packet's length from SGMII
port over maximum allowed length (1318 bytes)"
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_af6ces10grtldcc_rx__upen_glb_sta_cur_glb_dcc_eth2ocn_cur_sta_ovrsize_len_Mask                                   cBit3
#define cAf6_af6ces10grtldcc_rx__upen_glb_sta_cur_glb_dcc_eth2ocn_cur_sta_ovrsize_len_Shift                                       3

/*--------------------------------------
BitField Name: dcc_eth2ocn_cur_sta_crc_error
BitField Type: RO
BitField Desc: Current status of event "Received packet from SGMII port has FCS
error"
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_af6ces10grtldcc_rx__upen_glb_sta_cur_glb_dcc_eth2ocn_cur_sta_crc_error_Mask                                   cBit2
#define cAf6_af6ces10grtldcc_rx__upen_glb_sta_cur_glb_dcc_eth2ocn_cur_sta_crc_error_Shift                                       2

/*--------------------------------------
BitField Name: dcc_eth2ocn_cur_sta_cvlid_mismat
BitField Type: RO
BitField Desc: Current status of event "Received 12b CVLAN ID value of DCC frame
different from global provisioned CVID"
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_af6ces10grtldcc_rx__upen_glb_sta_cur_glb_dcc_eth2ocn_cur_sta_cvlid_mismat_Mask                                   cBit1
#define cAf6_af6ces10grtldcc_rx__upen_glb_sta_cur_glb_dcc_eth2ocn_cur_sta_cvlid_mismat_Shift                                       1

/*--------------------------------------
BitField Name: dcc_eth2ocn_cur_sta_macda_mismat
BitField Type: RO
BitField Desc: Current status of event "Received 43b MAC DA value of DCC frame
different from global provisioned DA"
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_af6ces10grtldcc_rx__upen_glb_sta_cur_glb_dcc_eth2ocn_cur_sta_macda_mismat_Mask                                   cBit0
#define cAf6_af6ces10grtldcc_rx__upen_glb_sta_cur_glb_dcc_eth2ocn_cur_sta_macda_mismat_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Interupt Status of Packet Classification Error
Reg Addr   : 0x01040
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_rxdcc1_stt                                                                    0x01040
#define cAf6Reg_upen_int_rxdcc1_stt_WidthVal                                                                32

/*--------------------------------------
BitField Name: dcc_eth2ocn_type_mismat
BitField Type: W1C
BitField Desc: Received ETHERNET TYPE value of DCC frame different from global
provisioned TYPE
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_type_mismat_Mask                                            cBit4
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_type_mismat_Shift                                               4

/*--------------------------------------
BitField Name: dcc_eth2ocn_ovrsize_len
BitField Type: W1C
BitField Desc: Received DCC packet's length from SGMII port over maximum allowed
length (1318 bytes)
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_ovrsize_len_Mask                                            cBit3
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_ovrsize_len_Shift                                               3

/*--------------------------------------
BitField Name: dcc_eth2ocn_crc_error
BitField Type: W1C
BitField Desc: Received packet from SGMII port has FCS error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_crc_error_Mask                                              cBit2
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_crc_error_Shift                                                 2

/*--------------------------------------
BitField Name: dcc_eth2ocn_cvlid_mismat
BitField Type: W1C
BitField Desc: Received 12b CVLAN ID value of DCC frame different from global
provisioned CVID
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_cvlid_mismat_Mask                                           cBit1
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_cvlid_mismat_Shift                                              1

/*--------------------------------------
BitField Name: dcc_eth2ocn_macda_mismat
BitField Type: W1C
BitField Desc: Received 43b MAC DA value of DCC frame different from global
provisioned DA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_macda_mismat_Mask                                           cBit0
#define cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_macda_mismat_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Enable Interrupt of "Packet Classification Error"
Reg Addr   : 0x01000
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxdcc_intenb_r1                                                                   0x01000
#define cAf6Reg_upen_rxdcc_intenb_r1_WidthVal                                                               32

/*--------------------------------------
BitField Name: enb_inb_dcc_eth2ocn_type_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received ETHERNET TYPE value of DCC frame
different from global provisioned TYPE" (reg 0x02005)
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_rxdcc_intenb_r1_enb_inb_dcc_eth2ocn_type_mismat_Mask                                   cBit4
#define cAf6_upen_rxdcc_intenb_r1_enb_inb_dcc_eth2ocn_type_mismat_Shift                                       4

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_ovrsize_len
BitField Type: RW
BitField Desc: Enable Interrupt of "Received DCC packet's length from SGMII port
over maximum allowed length (1318 bytes)"(reg 0x02005)
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_ovrsize_len_Mask                                   cBit3
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_ovrsize_len_Shift                                       3

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_crc_error
BitField Type: RW
BitField Desc: Enable Interrupt of "Received packet from SGMII port has FCS
error" (reg 0x02005)
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_crc_error_Mask                                     cBit2
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_crc_error_Shift                                        2

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_cvlid_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received 12b CVLAN ID value of DCC frame
different from global provisioned CVID " (reg 0x02005)
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_cvlid_mismat_Mask                                   cBit1
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_cvlid_mismat_Shift                                       1

/*--------------------------------------
BitField Name: enb_int_dcc_eth2ocn_macda_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received 43b MAC DA value of DCC frame
different from global provisioned DA" (reg 0x02005)
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_macda_mismat_Mask                                   cBit0
#define cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_macda_mismat_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH2OCN DCC "Packet Clasification Error" Global OR Status
Reg Addr   : 0x000010FF
Reg Formula: 
    Where  : 
Reg Desc   : 
This is status OR interrupt of Global.

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtldcck_corepi__upen_rxdcc_or_stt                                          0x000010FF

/*--------------------------------------
BitField Name: Global_Intr_Or
BitField Type: RW
BitField Desc: Set to 1 if any interrupt bit of corresponding Group is set and
its interrupt is enable
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__upen_rxdcc_or_stt_Global_Intr_Or_Mask                                cBit31_0
#define cAf6_af6ces10grtldcck_corepi__upen_rxdcc_or_stt_Global_Intr_Or_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH2OCN DCC "Packet Clasification Error" Global Interrupt OR Enable
Reg Addr   : 0x000010FE
Reg Formula: 
    Where  : 
Reg Desc   : 
This is OR interrupt Enable per HDLC Channel Group. Each bit represent enable for that Group

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtldcck_corepi__upen_rxdcc_or_enb                                          0x000010FE

/*--------------------------------------
BitField Name: Global_Intr_Or_En
BitField Type: RW
BitField Desc: Set to 1 to enable Group interrupt. Bit[0] is for Group 0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__upen_rxdcc_or_enb_Global_Intr_Or_En_Mask                                cBit31_0
#define cAf6_af6ces10grtldcck_corepi__upen_rxdcc_or_enb_Global_Intr_Or_En_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC HDLC per Channel Interrupt Enable Control
Reg Addr   : 0x00003000-0x0000301F
Reg Formula: 0x00003000 +  ChannelID
    Where  : 
           + $ChannelID(0-31): HDLC Channel ID
Reg Desc   : 
This is the per Channel interrupt enable of HDLC DCC

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_Base                              0x00003000
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en(ChannelID)                (0x00003000+(ChannelID))
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_WidthVal                                      32

/*--------------------------------------
BitField Name: enb_int_ocn2eth_dcc_erro_und
BitField Type: RW
BitField Desc: Set 1 to enable Interrupt of "OCN2ETH packet is undersize "
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_und_Mask                                   cBit5
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_und_Shift                                       5

/*--------------------------------------
BitField Name: enb_int_ocn2eth_dcc_erro_ovr
BitField Type: RW
BitField Desc: Set 1 to enable Interrupt of "OCN2ETH packet is oversize  "
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_ovr_Mask                                   cBit4
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_ovr_Shift                                       4

/*--------------------------------------
BitField Name: enb_int_ocn2eth_dcc_erro_fcs
BitField Type: RW
BitField Desc: Set 1 to enable Interrupt of "OCN2ETH packet has FCS error"
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_fcs_Mask                                   cBit3
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_fcs_Shift                                       3

/*--------------------------------------
BitField Name: enb_int_ocn2eth_dcc_buff_ful
BitField Type: RW
BitField Desc: Set 1 to enable Interrupt of "OCN2ETH buffer full indication"
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_buff_ful_Mask                                   cBit2
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_buff_ful_Shift                                       2

/*--------------------------------------
BitField Name: enb_int_eth2ocn_dcc_buff_ful
BitField Type: RW
BitField Desc: Set 1 to enable Interrupt of "ETH2OCN buffer full indication"
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_eth2ocn_dcc_buff_ful_Mask                                   cBit1
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_eth2ocn_dcc_buff_ful_Shift                                       1

/*--------------------------------------
BitField Name: enb_int_eth2ocn_dcc_chan_dis
BitField Type: RW
BitField Desc: Set 1 to enable Interrupt of "DCC Local Channel Identifier
mapping Is Disable"
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_eth2ocn_dcc_chan_dis_Mask                                   cBit0
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_eth2ocn_dcc_chan_dis_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC HDLC per Channel Interrupt Status
Reg Addr   : 0x00003040-0x0000305F
Reg Formula: 0x00003040 + ChannelID
    Where  : 
           + $ChannelID(0-31): HDLC Channel ID
Reg Desc   : 
This is the per Channel interrupt status of HDLC DCC

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_Base                              0x00003040
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta(ChannelID)                (0x00003040+(ChannelID))
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_WidthVal                                      32

/*--------------------------------------
BitField Name: sta_int_ocn2eth_dcc_erro_und
BitField Type: W1C
BitField Desc: Set 1 if there is a change event "OCN2ETH packet is undersize "
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_ocn2eth_dcc_erro_und_Mask                                   cBit5
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_ocn2eth_dcc_erro_und_Shift                                       5

/*--------------------------------------
BitField Name: sta_int_ocn2eth_dcc_erro_ovr
BitField Type: W1C
BitField Desc: Set 1 if there is a change event "OCN2ETH packet is oversize  "
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_ocn2eth_dcc_erro_ovr_Mask                                   cBit4
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_ocn2eth_dcc_erro_ovr_Shift                                       4

/*--------------------------------------
BitField Name: sta_int_ocn2eth_dcc_erro_fcs
BitField Type: W1C
BitField Desc: Set 1 if there is a change event "OCN2ETH packet has FCS error"
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_ocn2eth_dcc_erro_fcs_Mask                                   cBit3
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_ocn2eth_dcc_erro_fcs_Shift                                       3

/*--------------------------------------
BitField Name: sta_int_ocn2eth_dcc_buff_ful
BitField Type: W1C
BitField Desc: Set 1 if there is a change event "OCN2ETH buffer full indication"
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_ocn2eth_dcc_buff_ful_Mask                                   cBit2
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_ocn2eth_dcc_buff_ful_Shift                                       2

/*--------------------------------------
BitField Name: sta_int_eth2ocn_dcc_buff_ful
BitField Type: W1C
BitField Desc: Set 1 if there is a change event "ETH2OCN buffer full indication"
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_eth2ocn_dcc_buff_ful_Mask                                   cBit1
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_eth2ocn_dcc_buff_ful_Shift                                       1

/*--------------------------------------
BitField Name: sta_int_eth2ocn_dcc_chan_dis
BitField Type: W1C
BitField Desc: Set 1 if there is a change event "DCC Local Channel Identifier
mapping Is Disable"
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_eth2ocn_dcc_chan_dis_Mask                                   cBit0
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_eth2ocn_dcc_chan_dis_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC HDLC per Channel Currnet Status
Reg Addr   : 0x00003080-0x0000309F
Reg Formula: 0x00003080 +  ChannelID
    Where  : 
           + $ChannelID(0-31): HDLC Channel ID
Reg Desc   : 
This is the per Channel current status of HDLC DCC

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_Base                              0x00003080
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta(ChannelID)                (0x00003080+(ChannelID))
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_WidthVal                                      32

/*--------------------------------------
BitField Name: sta_cur_ocn2eth_dcc_erro_und
BitField Type: RW
BitField Desc: Current Status of event "OCN2ETH packet is undersize "
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_sta_cur_ocn2eth_dcc_erro_und_Mask                                   cBit5
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_sta_cur_ocn2eth_dcc_erro_und_Shift                                       5

/*--------------------------------------
BitField Name: sta_cur_ocn2eth_dcc_erro_ovr
BitField Type: RW
BitField Desc: Current Status of event "OCN2ETH packet is oversize  "
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_sta_cur_ocn2eth_dcc_erro_ovr_Mask                                   cBit4
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_sta_cur_ocn2eth_dcc_erro_ovr_Shift                                       4

/*--------------------------------------
BitField Name: sta_cur_ocn2eth_dcc_erro_fcs
BitField Type: RW
BitField Desc: Current Status of event "OCN2ETH packet has FCS error"
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_sta_cur_ocn2eth_dcc_erro_fcs_Mask                                   cBit3
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_sta_cur_ocn2eth_dcc_erro_fcs_Shift                                       3

/*--------------------------------------
BitField Name: sta_cur_ocn2eth_dcc_buff_ful
BitField Type: RW
BitField Desc: Current Status of event "OCN2ETH buffer full indication"
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_sta_cur_ocn2eth_dcc_buff_ful_Mask                                   cBit2
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_sta_cur_ocn2eth_dcc_buff_ful_Shift                                       2

/*--------------------------------------
BitField Name: sta_cur_eth2ocn_dcc_buff_ful
BitField Type: RW
BitField Desc: Current Status of event "ETH2OCN buffer full indication"
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_sta_cur_eth2ocn_dcc_buff_ful_Mask                                   cBit1
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_sta_cur_eth2ocn_dcc_buff_ful_Shift                                       1

/*--------------------------------------
BitField Name: sta_cur_eth2ocn_dcc_chan_dis
BitField Type: RW
BitField Desc: Current Status of event "DCC Local Channel Identifier mapping Is
Disable"
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_sta_cur_eth2ocn_dcc_chan_dis_Mask                                   cBit0
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_sta_cur_eth2ocn_dcc_chan_dis_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC HDLC per Channel Interrupt OR Status
Reg Addr   : 0x000030C0
Reg Formula: 0x000030C0 +  GrpID*32
    Where  : 
           + $GrpID(0): Group 0 for HDLC Channel 0-31
           + $ChannelID(0-31)
Reg Desc   : 
This is status OR interrupt of per HDLC channel. Each bit represent OR status of a HDLC channel

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_or_sta_Base                              0x000030C0
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_or_sta(GrpID, ChannelID)                 (0x000030C0+(GrpID)*32)

/*--------------------------------------
BitField Name: HDLC_Chid_Intr_Or
BitField Type: RW
BitField Desc: Set to 1 if any interrupt bit of corresponding channelID is set
and its interrupt is enable
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_or_sta_HDLC_Chid_Intr_Or_Mask                                cBit31_0
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_or_sta_HDLC_Chid_Intr_Or_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC HDLC per Group Interrupt OR Status
Reg Addr   : 0x000030FF
Reg Formula: 
    Where  : 
Reg Desc   : 
This is status OR interrupt of per HDLC group. Each bit represent OR status of a HDLC group. Group 0 include channelID 0->31

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_sta                                   0x000030FF

/*--------------------------------------
BitField Name: HDLC_Group_Intr_Or
BitField Type: RW
BitField Desc: Set to 1 if any interrupt bit of corresponding Group is set and
its interrupt is enable
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_sta_HDLC_Group_Intr_Or_Mask                                cBit31_0
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_sta_HDLC_Group_Intr_Or_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DCC HDLC per Group Interrupt OR Enable
Reg Addr   : 0x000030FE
Reg Formula: 
    Where  : 
Reg Desc   : 
This is OR interrupt Enable per HDLC Channel Group. Each bit represent enable for that Group

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_en                                    0x000030FE

/*--------------------------------------
BitField Name: HDLC_Group_Intr_Or_En
BitField Type: RW
BitField Desc: Set to 1 to enable Group interrupt. Bit[0] is for Group 0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_en_HDLC_Group_Intr_Or_En_Mask                                cBit31_0
#define cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_en_HDLC_Group_Intr_Or_En_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : KByte Interupt Status
Reg Addr   : 0x02440
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides interrupt status of Kbyte event

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_int_rxk12_stt                                                                     0x02440
#define cAf6Reg_upen_int_rxk12_stt_WidthVal                                                                 32

/*--------------------------------------
BitField Name: aps_per_channel_mismat
BitField Type: W1C
BitField Desc: Received CHANNEL ID value of APS frame different from configured
value. This is a per channel status. Bit 23 -> 08 represent for channel 15 -> 0
respectively
BitField Bits: [23:08]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_per_channel_mismat_Mask                                           cBit23_8
#define cAf6_upen_int_rxk12_stt_aps_per_channel_mismat_Shift                                                 8

/*--------------------------------------
BitField Name: aps_watdog_alarm
BitField Type: W1C
BitField Desc: No packets received in the period defined in watchdog timer
register
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_watdog_alarm_Mask                                                    cBit7
#define cAf6_upen_int_rxk12_stt_aps_watdog_alarm_Shift                                                       7

/*--------------------------------------
BitField Name: aps_glb_channel_mismat
BitField Type: W1C
BitField Desc: There is one or more mismatch between received CHANNELID value of
APS frame and configuration value of that channelID. This bit is set whenever a
mismatch happen in one or more of 16 channels
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_glb_channel_mismat_Mask                                              cBit6
#define cAf6_upen_int_rxk12_stt_aps_glb_channel_mismat_Shift                                                 6

/*--------------------------------------
BitField Name: aps_lencount mismat
BitField Type: W1C
BitField Desc: Received PACKET BYTE COUNTER value of APS frame different from
configuration
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_lencount_mismat_Mask                                                 cBit5
#define cAf6_upen_int_rxk12_stt_aps_lencount_mismat_Shift                                                    5

/*--------------------------------------
BitField Name: aps_lenfield mismat
BitField Type: W1C
BitField Desc: Received LENGTH FIELD value of APS frame different from
configuration
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_lenfield_mismat_Mask                                                 cBit4
#define cAf6_upen_int_rxk12_stt_aps_lenfield_mismat_Shift                                                    4

/*--------------------------------------
BitField Name: aps_ver_mismat
BitField Type: W1C
BitField Desc: Received VERSION value of APS frame different from configuration
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_ver_mismat_Mask                                                      cBit3
#define cAf6_upen_int_rxk12_stt_aps_ver_mismat_Shift                                                         3

/*--------------------------------------
BitField Name: aps_apstp_mismat
BitField Type: W1C
BitField Desc: Received TYPE value of APS frame different from configuration
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_apstp_mismat_Mask                                                    cBit2
#define cAf6_upen_int_rxk12_stt_aps_apstp_mismat_Shift                                                       2

/*--------------------------------------
BitField Name: aps_ethtp_mismat
BitField Type: W1C
BitField Desc: Received ETHERNET TYPE value of APS frame different from
configuration
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_ethtp_mismat_Mask                                                    cBit1
#define cAf6_upen_int_rxk12_stt_aps_ethtp_mismat_Shift                                                       1

/*--------------------------------------
BitField Name: aps_macda_mismat
BitField Type: W1C
BitField Desc: Received DA value of APS frame different from configed DA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_int_rxk12_stt_aps_macda_mismat_Mask                                                    cBit0
#define cAf6_upen_int_rxk12_stt_aps_macda_mismat_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : KByte Interupt Enable
Reg Addr   : 0x02400
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of Kbyte events

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_rxk12_intenb                                                                      0x02400
#define cAf6Reg_upen_rxk12_intenb_WidthVal                                                                  32

/*--------------------------------------
BitField Name: enb_int_aps_per_channel_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received CHANNEL ID value of APS frame
different from configuration per channel ". Bit 23->08 represent for mismatch of
channelID 15->0 respectively
BitField Bits: [23:08]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_per_channel_mismat_Mask                                    cBit23_8
#define cAf6_upen_rxk12_intenb_enb_int_aps_per_channel_mismat_Shift                                          8

/*--------------------------------------
BitField Name: enb_int_aps_watdog_alarm
BitField Type: RW
BitField Desc: Enable Interrupt of "No packets received in the period defined in
watchdog timer register"
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_watdog_alarm_Mask                                             cBit7
#define cAf6_upen_rxk12_intenb_enb_int_aps_watdog_alarm_Shift                                                7

/*--------------------------------------
BitField Name: enb_int_aps_glb_channel_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received CHANNEL ID value of APS frame
different from configuration ". This bit is set when there is one or more
mismatching channelID happen
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_glb_channel_mismat_Mask                                       cBit6
#define cAf6_upen_rxk12_intenb_enb_int_aps_glb_channel_mismat_Shift                                          6

/*--------------------------------------
BitField Name: enb_int_aps_lencount mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received PACKET BYTE COUNTER value of APS
frame different from configuration "
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_lencount_mismat_Mask                                          cBit5
#define cAf6_upen_rxk12_intenb_enb_int_aps_lencount_mismat_Shift                                             5

/*--------------------------------------
BitField Name: enb_int_aps_lenfield mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received LENGTH FIELD value of APS frame
different from configuration "
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_lenfield_mismat_Mask                                          cBit4
#define cAf6_upen_rxk12_intenb_enb_int_aps_lenfield_mismat_Shift                                             4

/*--------------------------------------
BitField Name: enb_int_aps_ver_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received VERSION value of APS frame
different from configuration "
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_ver_mismat_Mask                                               cBit3
#define cAf6_upen_rxk12_intenb_enb_int_aps_ver_mismat_Shift                                                  3

/*--------------------------------------
BitField Name: enb_int_aps_apstp_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received TYPE value of APS frame different
from configuration "
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_apstp_mismat_Mask                                             cBit2
#define cAf6_upen_rxk12_intenb_enb_int_aps_apstp_mismat_Shift                                                2

/*--------------------------------------
BitField Name: enb_int_aps_ethtp_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received ETHERNET TYPE value of APS frame
different from configuration "
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_ethtp_mismat_Mask                                             cBit1
#define cAf6_upen_rxk12_intenb_enb_int_aps_ethtp_mismat_Shift                                                1

/*--------------------------------------
BitField Name: enb_int_aps_macda_mismat
BitField Type: RW
BitField Desc: Enable Interrupt of "Received DA value of APS frame different
from configed DA "
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_rxk12_intenb_enb_int_aps_macda_mismat_Mask                                             cBit0
#define cAf6_upen_rxk12_intenb_enb_int_aps_macda_mismat_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Alarm_Status
Reg Addr   : 0x02480
Reg Formula: 0x02480
    Where  : 
Reg Desc   : 
The register provides current status of APS alarm

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_cur_err                                                                     0x02480
#define cAf6Reg_upen_k12rx_cur_err_WidthVal                                                                 32

/*--------------------------------------
BitField Name: per_channel_mismat
BitField Type: R_O
BitField Desc: Received CHANNEL ID value of APS frame different from configured
value. This is a per channel status. Bit 23 -> 08 represent for channel 15 -> 0
respectively
BitField Bits: [23:08]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_per_channel_mismat_Mask                                               cBit23_8
#define cAf6_upen_k12rx_cur_err_per_channel_mismat_Shift                                                     8

/*--------------------------------------
BitField Name: Frame_miss
BitField Type: RO
BitField Desc: Current status of APS FRAME missed alarm. When watchdog timer is
enable. This bit is set if no frame is received in pre-defined interval
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_Frame_miss_Mask                                                          cBit7
#define cAf6_upen_k12rx_cur_err_Frame_miss_Shift                                                             7

/*--------------------------------------
BitField Name: channel_miss
BitField Type: RO
BitField Desc: Current status of global CHANNEL mismatch alarm. Received
CHANNELID value different from configed
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_channel_miss_Mask                                                        cBit6
#define cAf6_upen_k12rx_cur_err_channel_miss_Shift                                                           6

/*--------------------------------------
BitField Name: real_len_miss
BitField Type: RO
BitField Desc: Current status of ACTUAL LENGH mismatch alarm. acket LENGTH value
different 0x60
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_real_len_miss_Mask                                                       cBit5
#define cAf6_upen_k12rx_cur_err_real_len_miss_Shift                                                          5

/*--------------------------------------
BitField Name: len_miss
BitField Type: RO
BitField Desc: Current status of LENGH mismatch alarm. Received LENGTH value
different 0x60
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_len_miss_Mask                                                            cBit4
#define cAf6_upen_k12rx_cur_err_len_miss_Shift                                                               4

/*--------------------------------------
BitField Name: ver_miss
BitField Type: RO
BitField Desc: Current status of VERSION  mismatch alarm. Received VERSION value
different from configed VER
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_ver_miss_Mask                                                            cBit3
#define cAf6_upen_k12rx_cur_err_ver_miss_Shift                                                               3

/*--------------------------------------
BitField Name: type_miss
BitField Type: RO
BitField Desc: Current status of TYPE mismatch alarm. Received TYPE value
different from configed TYPE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_type_miss_Mask                                                           cBit2
#define cAf6_upen_k12rx_cur_err_type_miss_Shift                                                              2

/*--------------------------------------
BitField Name: ethtype_miss
BitField Type: RO
BitField Desc: Current status of ETHERNET TYPE mismatch alarm. Received ETHTYPE
value different from configed value
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_ethtype_miss_Mask                                                        cBit1
#define cAf6_upen_k12rx_cur_err_ethtype_miss_Shift                                                           1

/*--------------------------------------
BitField Name: mac_da_miss
BitField Type: RO
BitField Desc: DA mismatch. Received DA value different from configed DA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_k12rx_cur_err_mac_da_miss_Mask                                                         cBit0
#define cAf6_upen_k12rx_cur_err_mac_da_miss_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : KByte Interupt OR Status
Reg Addr   : 0x000024FF
Reg Formula: 
    Where  : 
Reg Desc   : 
This is status OR interrupt of Global.

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtldcck_corepi__upen_k12rx_or_stt                                          0x000024FF

/*--------------------------------------
BitField Name: KByte_Intr_Or_Stt
BitField Type: RW
BitField Desc: Set to 1 if any interrupt bit of corresponding Group is set and
its interrupt is enable
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__upen_k12rx_or_stt_KByte_Intr_Or_Stt_Mask                                cBit31_0
#define cAf6_af6ces10grtldcck_corepi__upen_k12rx_or_stt_KByte_Intr_Or_Stt_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : KByte Interupt OR Enable
Reg Addr   : 0x000024FE
Reg Formula: 
    Where  : 
Reg Desc   : 
This is status OR interrupt of Global.

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtldcck_corepi__upen_k12rx_or_enb                                          0x000024FE

/*--------------------------------------
BitField Name: KByte_Intr_Or_Enb
BitField Type: RW
BitField Desc: Set to 1 to enable Group interrupt. Bit[0] is for Group 0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_af6ces10grtldcck_corepi__upen_k12rx_or_enb_KByte_Intr_Or_Enb_Mask                                cBit31_0
#define cAf6_af6ces10grtldcck_corepi__upen_k12rx_or_enb_KByte_Intr_Or_Enb_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Loopback Enable Configuration
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides loopback enable configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_loopen                                                                            0x00001
#define cAf6Reg_upen_loopen_WidthVal                                                                        32

/*--------------------------------------
BitField Name: sel_cap_DCC_ENC_DEC
BitField Type: RW
BitField Desc: Select capture DCC ENC or DEC data .
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_upen_loopen_sel_cap_DCC_ENC_DEC_Mask                                                        cBit8
#define cAf6_upen_loopen_sel_cap_DCC_ENC_DEC_Shift                                                           8

/*--------------------------------------
BitField Name: sgmii_cap_tx_DCC_KByte
BitField Type: RW
BitField Desc: Select capture TX of DCC or Kbyte data     .
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_loopen_sgmii_cap_tx_DCC_KByte_Mask                                                     cBit7
#define cAf6_upen_loopen_sgmii_cap_tx_DCC_KByte_Shift                                                        7

/*--------------------------------------
BitField Name: sgmii_cap_select
BitField Type: RW
BitField Desc: Select capture TX or RX SGMII data             .
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_loopen_sgmii_cap_select_Mask                                                           cBit6
#define cAf6_upen_loopen_sgmii_cap_select_Shift                                                              6

/*--------------------------------------
BitField Name: dcc_buffer_loop_en
BitField Type: RW
BitField Desc: Enable loopback of RX-BUFFER to TX-BUFFER      .
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_loopen_dcc_buffer_loop_en_Mask                                                         cBit5
#define cAf6_upen_loopen_dcc_buffer_loop_en_Shift                                                            5

/*--------------------------------------
BitField Name: genmon_loop_en
BitField Type: RW
BitField Desc: Enable DCC GEN to RX-DCC                       .
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_loopen_genmon_loop_en_Mask                                                             cBit4
#define cAf6_upen_loopen_genmon_loop_en_Shift                                                                4

/*--------------------------------------
BitField Name: ksdh_loop_en
BitField Type: RW
BitField Desc: Enable loopback of Kbyte information (TDM side)          .
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_loopen_ksdh_loop_en_Mask                                                               cBit3
#define cAf6_upen_loopen_ksdh_loop_en_Shift                                                                  3

/*--------------------------------------
BitField Name: ksgm_loop_en
BitField Type: RW
BitField Desc: Enable SGMII loopback of Kbyte Port.
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_loopen_ksgm_loop_en_Mask                                                               cBit2
#define cAf6_upen_loopen_ksgm_loop_en_Shift                                                                  2

/*--------------------------------------
BitField Name: hdlc_loop_en
BitField Type: RW
BitField Desc: Enable loopback from HDLC Encap to HDLC DEcap of DCC byte.
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_loopen_hdlc_loop_en_Mask                                                               cBit1
#define cAf6_upen_loopen_hdlc_loop_en_Shift                                                                  1

/*--------------------------------------
BitField Name: dsgm_loop_en
BitField Type: RW
BitField Desc: Enable TX-SGMII to RX-SGMII loopback of DCC Port.
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_loopen_dsgm_loop_en_Mask                                                               cBit0
#define cAf6_upen_loopen_dsgm_loop_en_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Configure Data Captured Channel ID
Reg Addr   : 0x00030
Reg Formula: 
    Where  : 
Reg Desc   : 
The register is used to configure channel ID to capture packet of ENC/DEC

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_dump_lid                                                                      0x00030
#define cAf6Reg_upen_cfg_dump_lid_WidthVal                                                                  32

/*--------------------------------------
BitField Name: cfg_dump_lid
BitField Type: RW
BitField Desc: Captured ENC/DEC Channel ID value
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_cfg_dump_lid_cfg_dump_lid_Mask                                                       cBit7_0
#define cAf6_upen_cfg_dump_lid_cfg_dump_lid_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Enable DCC Engine
Reg Addr   : 0x0001F
Reg Formula: 
    Where  : 
Reg Desc   : 
The register is used to enable DCC Engine to receive/transmit data from/to OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_dcc_en                                                                        0x0001F
#define cAf6Reg_upen_cfg_dcc_en_WidthVal                                                                    32

/*--------------------------------------
BitField Name: cfg_dcc_en
BitField Type: RW
BitField Desc: Enable DCC Engine to receive/transmit data from/to OCN.
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_cfg_dcc_en_cfg_dcc_en_Mask                                                             cBit0
#define cAf6_upen_cfg_dcc_en_cfg_dcc_en_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Dcc_Fcs_Rem_Mode
Reg Addr   : 0x10003
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to remove FCS32 or FCS16

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcctx_fcsrem                                                                      0x10003

/*--------------------------------------
BitField Name: FCS_Remove_Mode
BitField Type: RW
BitField Desc: Remove 4 bytes FCS32 or 2byte FCS16 This configuration depend on
the FCS checking mode at HDLC DEC (which is configed at reg address : 0x04000 -
0x04037) Bit 0 -> 31 : channel 0 -> 31
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dcctx_fcsrem_FCS_Remove_Mode_Mask                                                   cBit31_0
#define cAf6_upen_dcctx_fcsrem_FCS_Remove_Mode_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : DDC_TX_Header_Per_Channel
Reg Addr   : 0x11200
Reg Formula: 0x11200 + $channelid*16 + $hdrpos
    Where  : 
           + $channelid(0-31): Channel ID
           + $hdrpos(0-15):header's byte position
Reg Desc   : 
The register provides data for configuration of 22bytes Header of each channel ID, in which, only 16byte is configurable
the configuration postion of header byte is as follow; DA(6byte) + SA(6byte) + {PCP,DEI,VID}(2byte) + VERSION(1byte) + TYPE(1byte)
hdrpos = 0 is position of DA[47:40] and hdrpos = 15 is position of TYPE field

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcchdr_Base                                                                       0x11200
#define cAf6Reg_upen_dcchdr(channelid, hdrpos)                               (0x11200+(channelid)*16+(hdrpos))
#define cAf6Reg_upen_dcchdr_WidthVal                                                                        32

/*--------------------------------------
BitField Name: HEADER_POS
BitField Type: RW
BitField Desc: 8b value of header per Channel ID
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_upen_dcchdr_HEADER_POS_Mask                                                               cBit7_0
#define cAf6_upen_dcchdr_HEADER_POS_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Channel_Enable
Reg Addr   : 0x11000
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration for enable transmission DDC packet per channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcctx_enacid                                                                      0x11000

/*--------------------------------------
BitField Name: Channel_Enable
BitField Type: RW
BitField Desc: Enable transmitting of DDC packet per channel. Bit[31:0]
represent for channel 31->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dcctx_enacid_Channel_Enable_Mask                                                    cBit31_0
#define cAf6_upen_dcctx_enacid_Channel_Enable_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_Global_ProvisionedHeader_Configuration
Reg Addr   : 0x12001
Reg Formula: 0x12001
    Where  : 
Reg Desc   : 
The register provides data for configuration of 22bytes Header of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccrxhdr                                                                          0x12001
#define cAf6Reg_upen_dccrxhdr_WidthVal                                                                      96

/*--------------------------------------
BitField Name: ETH_TYP
BitField Type: RW
BitField Desc: 16b value of Provisioned ETHERTYPE of DCC
BitField Bits: [70:55]
--------------------------------------*/
#define cAf6_upen_dccrxhdr_ETH_TYP_Mask_01                                                           cBit31_23
#define cAf6_upen_dccrxhdr_ETH_TYP_Shift_01                                                                 23
#define cAf6_upen_dccrxhdr_ETH_TYP_Mask_02                                                             cBit6_0
#define cAf6_upen_dccrxhdr_ETH_TYP_Shift_02                                                                  0

/*--------------------------------------
BitField Name: CVID
BitField Type: RW
BitField Desc: 12b value of Provisioned C-VLAN ID. This value is used to
compared with received CVLAN ID value.
BitField Bits: [54:43]
--------------------------------------*/
#define cAf6_upen_dccrxhdr_CVID_Mask                                                                 cBit22_11
#define cAf6_upen_dccrxhdr_CVID_Shift                                                                       11

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: 43b MSB of Provisioned MAC DA value. This value is used to
compared with received MAC_DA[47:05] value. If a match is confirmed,
MAC_DA[04:00] is used to represent channelID value before mapping.
BitField Bits: [42:00]
--------------------------------------*/
#define cAf6_upen_dccrxhdr_MAC_DA_Mask_01                                                             cBit31_0
#define cAf6_upen_dccrxhdr_MAC_DA_Shift_01                                                                   0
#define cAf6_upen_dccrxhdr_MAC_DA_Mask_02                                                             cBit10_0
#define cAf6_upen_dccrxhdr_MAC_DA_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_MAC_Check_Enable_Configuration
Reg Addr   : 0x12002
Reg Formula: 0x12002
    Where  : 
Reg Desc   : 
The register provides configuration that enable base MAC DA check

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccrxmacenb                                                                       0x12002

/*--------------------------------------
BitField Name: MAC_check_enable
BitField Type: RW
BitField Desc: Enable checking of received MAC DA compare to globally
provisioned Base MAC.On mismatch, frame is discarded.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dccrxmacenb_MAC_check_enable_Mask                                                   cBit31_0
#define cAf6_upen_dccrxmacenb_MAC_check_enable_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_CVLAN_Check_Enable_Configuration
Reg Addr   : 0x12003
Reg Formula: 0x12003
    Where  : 
Reg Desc   : 
The register provides configuration that enable base CVLAN Check

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccrxcvlenb                                                                       0x12003

/*--------------------------------------
BitField Name: CVL_check_enable
BitField Type: RW
BitField Desc: Enable checking of received CVLAN ID compare to globally
provisioned CVLAN ID.On mismatch, frame is discarded.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dccrxcvlenb_CVL_check_enable_Mask                                                   cBit31_0
#define cAf6_upen_dccrxcvlenb_CVL_check_enable_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : DDC_RX_Channel_Mapping
Reg Addr   : 0x12200 - 0x1221F
Reg Formula: 0x12200 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
The register provides channel mapping from received MAC DA to internal channelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dccdec_Base                                                                       0x12200
#define cAf6Reg_upen_dccdec(channelid)                                                   (0x12200+(channelid))
#define cAf6Reg_upen_dccdec_WidthVal                                                                        32

/*--------------------------------------
BitField Name: Channel_enable
BitField Type: RW
BitField Desc: Enable Local Channel Identifier. Rx packet is discarded if enable
is not set
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_dccdec_Channel_enable_Mask                                                             cBit5
#define cAf6_upen_dccdec_Channel_enable_Shift                                                                5

/*--------------------------------------
BitField Name: Mapping_ChannelID
BitField Type: RW
BitField Desc: Local ChannelID that is mapped from received bit[4:0] of MAC DA
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_upen_dccdec_Mapping_ChannelID_Mask                                                        cBit4_0
#define cAf6_upen_dccdec_Mapping_ChannelID_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Provisioned_APS_KByte_Value_Per_Channel
Reg Addr   : 0x21200  - 0x2120F
Reg Formula: 0x21200 + $channelid
    Where  : 
           + $channelid(0-15): Channel ID
Reg Desc   : 
The register provides configuration for Kbyte value to overwrite

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12pro_Base                                                                       0x21200
#define cAf6Reg_upen_k12pro(channelid)                                                   (0x21200+(channelid))

/*--------------------------------------
BitField Name: CHANNELID_APS
BitField Type: RW
BitField Desc: 32b of ChannelID APS value to overwrite This Kbyte value is used
to overwrite Kbyte value get from RX-OCN when enable [31:24]: K1 byte value
[23:16]: K2 byte value [15:08]: D1(EK1&EK2) (extend K) byte value [07:00]: set
to 0x0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_k12pro_CHANNELID_APS_Mask                                                           cBit31_0
#define cAf6_upen_k12pro_CHANNELID_APS_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Enable Transmit Validated KByte Change Per ChannelID Configuration
Reg Addr   : 0x21001
Reg Formula: 0x21001
    Where  : 
Reg Desc   : 
The register configures channelID for provisioned APS packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgenacid                                                                         0x21001
#define cAf6Reg_upen_cfgenacid_WidthVal                                                                     32

/*--------------------------------------
BitField Name: ChannelID_Enable
BitField Type: RW
BitField Desc: Enable provisioned ChannelID. Bit[15:0] represents channelID
15->0 This channelID bitmap is used to allow transmission of newly validated K
byte change.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_cfgenacid_ChannelID_Enable_Mask                                                     cBit15_0
#define cAf6_upen_cfgenacid_ChannelID_Enable_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Timer_TX_Provisioned_APS_Packet
Reg Addr   : 0x21002
Reg Formula: 0x21002
    Where  : 
Reg Desc   : 
The register configures timer for transmiting provisioned APS packets of configured channelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgtim                                                                            0x21002

/*--------------------------------------
BitField Name: Timer_Ena
BitField Type: RW
BitField Desc: Enable Timer for TX provisoned APS packet
BitField Bits: [31]
--------------------------------------*/
#define cAf6_upen_cfgtim_Timer_Ena_Mask                                                                 cBit31
#define cAf6_upen_cfgtim_Timer_Ena_Shift                                                                    31

/*--------------------------------------
BitField Name: Timer_Val
BitField Type: RW
BitField Desc: Timer value for TX provisoned APS packet. This value is the
number of clock counter represent the time interval For ex: to set an interval
256us with the clock 155Mz The clock counter is: 256x10^3/(10^3/155) = 256*155 =
39680 Timer range from 125us to 8ms. This timer is used only when bit 31 (Timer
Ena) is set Each time the timer reach configurated value, an APS packet will be
transmit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_upen_cfgtim_Timer_Val_Mask                                                               cBit23_0
#define cAf6_upen_cfgtim_Timer_Val_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Trigger_TX_Provisioned_APS_Packet
Reg Addr   : 0x21003
Reg Formula: 0x21003
    Where  : 
Reg Desc   : 
The register configures channelID for provisioned APS packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfgcid                                                                            0x21003
#define cAf6Reg_upen_cfgcid_WidthVal                                                                        32

/*--------------------------------------
BitField Name: SW_Trig
BitField Type: RW
BitField Desc: SW trigger transmission of Provisioned APS packet Write 0, then
write 1 to trigger
BitField Bits: [0:0]
--------------------------------------*/
#define cAf6_upen_cfgcid_SW_Trig_Mask                                                                    cBit0
#define cAf6_upen_cfgcid_SW_Trig_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Enable KByte Overwrite Per ChannelID Configuration
Reg Addr   : 0x21004
Reg Formula: 0x21004
    Where  : 
Reg Desc   : 
The register configures channelID for provisioned APS packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_upen_cfgovwcid                                                                    0x21004
#define cAf6Reg_upen_upen_cfgovwcid_WidthVal                                                                32

/*--------------------------------------
BitField Name: ChannelID_Enable
BitField Type: RW
BitField Desc: Enable overwrite Kbyte value of channel Bit[15:0] represents
channelID 15->0 This channelID bitmap is used to allow overwrite of channel's
Kbyte. When this bit is set. Kbyte will get value from configurated Provisioned
Kbyte instead of Kbyte received from RX-OCN
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_upen_upen_cfgovwcid_ChannelID_Enable_Mask                                                cBit15_0
#define cAf6_upen_upen_cfgovwcid_ChannelID_Enable_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : APS_TX_Header_Mac_DA
Reg Addr   : 0x21006
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration of TX APS Packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_hdrmda                                                                        0x21006
#define cAf6Reg_upen_cfg_hdrmda_WidthVal                                                                    64

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: MAC DA value of TX APS Packet
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_cfg_hdrmda_MAC_DA_Mask_01                                                           cBit31_0
#define cAf6_upen_cfg_hdrmda_MAC_DA_Shift_01                                                                 0
#define cAf6_upen_cfg_hdrmda_MAC_DA_Mask_02                                                           cBit15_0
#define cAf6_upen_cfg_hdrmda_MAC_DA_Shift_02                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : APS_TX_Header_Mac_SA
Reg Addr   : 0x21007
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration of TX APS Packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_hdrmsa                                                                        0x21007
#define cAf6Reg_upen_cfg_hdrmsa_WidthVal                                                                    64

/*--------------------------------------
BitField Name: MAC_SA
BitField Type: RW
BitField Desc: MAC SA value of TX APS Packet
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_cfg_hdrmsa_MAC_SA_Mask_01                                                           cBit31_0
#define cAf6_upen_cfg_hdrmsa_MAC_SA_Shift_01                                                                 0
#define cAf6_upen_cfg_hdrmsa_MAC_SA_Mask_02                                                           cBit15_0
#define cAf6_upen_cfg_hdrmsa_MAC_SA_Shift_02                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : APS_TX_Header_VTL
Reg Addr   : 0x21008
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration of 18bytes Header of each channel ID receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_hdrvtl                                                                        0x21008
#define cAf6Reg_upen_cfg_hdrvtl_WidthVal                                                                    64

/*--------------------------------------
BitField Name: VLAN
BitField Type: RW
BitField Desc: 16b
BitField Bits: [47:32]
--------------------------------------*/
#define cAf6_upen_cfg_hdrvtl_VLAN_Mask                                                                cBit15_0
#define cAf6_upen_cfg_hdrvtl_VLAN_Shift                                                                      0

/*--------------------------------------
BitField Name: ETHTYPE
BitField Type: RW
BitField Desc: 16b Ethernet Type field
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_cfg_hdrvtl_ETHTYPE_Mask                                                            cBit31_16
#define cAf6_upen_cfg_hdrvtl_ETHTYPE_Shift                                                                  16

/*--------------------------------------
BitField Name: APSTYPE
BitField Type: RW
BitField Desc: 8b Type field
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_cfg_hdrvtl_APSTYPE_Mask                                                             cBit15_8
#define cAf6_upen_cfg_hdrvtl_APSTYPE_Shift                                                                   8

/*--------------------------------------
BitField Name: VERSION
BitField Type: RW
BitField Desc: 8b Version Field
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_cfg_hdrvtl_VERSION_Mask                                                              cBit7_0
#define cAf6_upen_cfg_hdrvtl_VERSION_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x21100  - 0x2110F
Reg Formula: 0x21100 + $portID
    Where  : 
           + $portID(0-15): Port ID
Reg Desc   : 
The register provides channel mapping from portID to ChannelID configuraton

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cid2pid_Base                                                                      0x21100
#define cAf6Reg_upen_cid2pid(portID)                                                        (0x21100+(portID))
#define cAf6Reg_upen_cid2pid_WidthVal                                                                       32

/*--------------------------------------
BitField Name: CHANNELID_VAL
BitField Type: RW
BitField Desc: 16b value of ChannelID of PortID
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_cid2pid_CHANNELID_VAL_Mask                                                          cBit15_0
#define cAf6_upen_cid2pid_CHANNELID_VAL_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : 
Reg Addr   : 0x21009
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides the initialization Kbyte value of each channelID
To init the content of each channelID, write bit31 = 0 first, then
write bit[31] = 1 and the channelId as well of Kbyte value of that
channelID. For example: to Init data FF for channel 0.
Step1: wr 0x21009 0
Step2: wr 0x21009 800000FF

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cfg_gencid                                                                        0x21009

/*--------------------------------------
BitField Name: Init_Enable
BitField Type: RW
BitField Desc: Enable ChannelID Data Initializtion
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_upen_cfg_gencid_Init_Enable_Mask                                                           cBit31
#define cAf6_upen_cfg_gencid_Init_Enable_Shift                                                              31

/*--------------------------------------
BitField Name: Init_ChannelId
BitField Type: RW
BitField Desc: ChannelID value
BitField Bits: [30:24]
--------------------------------------*/
#define cAf6_upen_cfg_gencid_Init_ChannelId_Mask                                                     cBit30_24
#define cAf6_upen_cfg_gencid_Init_ChannelId_Shift                                                           24

/*--------------------------------------
BitField Name: Init_KbyteVal
BitField Type: RW
BitField Desc: Default 24b value of Kbyte
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_cfg_gencid_Init_KbyteVal_Mask                                                       cBit23_0
#define cAf6_upen_cfg_gencid_Init_KbyteVal_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Header_MAC_DA_Configuration
Reg Addr   : 0x22001
Reg Formula: 0x22001
    Where  : 
Reg Desc   : 
The register provides MAC DA value of provisioned APS port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_macda                                                                       0x22001
#define cAf6Reg_upen_k12rx_macda_WidthVal                                                                   64

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: Provisioned MAC DA value
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_k12rx_macda_MAC_DA_Mask_01                                                          cBit31_0
#define cAf6_upen_k12rx_macda_MAC_DA_Shift_01                                                                0
#define cAf6_upen_k12rx_macda_MAC_DA_Mask_02                                                          cBit15_0
#define cAf6_upen_k12rx_macda_MAC_DA_Shift_02                                                                0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Header_MAC_SA_Configuration
Reg Addr   : 0x22002
Reg Formula: 0x22002
    Where  : 
Reg Desc   : 
The register provides MAC SA value of provisioned APS port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_macsa                                                                       0x22002
#define cAf6Reg_upen_k12rx_macsa_WidthVal                                                                   64

/*--------------------------------------
BitField Name: MAC_SA
BitField Type: RW
BitField Desc: Provisioned MAC SA value
BitField Bits: [47:00]
--------------------------------------*/
#define cAf6_upen_k12rx_macsa_MAC_SA_Mask_01                                                          cBit31_0
#define cAf6_upen_k12rx_macsa_MAC_SA_Shift_01                                                                0
#define cAf6_upen_k12rx_macsa_MAC_SA_Mask_02                                                          cBit15_0
#define cAf6_upen_k12rx_macsa_MAC_SA_Shift_02                                                                0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Header_EVT_Configuration
Reg Addr   : 0x22003
Reg Formula: 0x22003
    Where  : 
Reg Desc   : 
The register provides EVT(Ethernet Type, Version, APS Type) value of provisioned APS port

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_evt                                                                         0x22003

/*--------------------------------------
BitField Name: ETHTYP
BitField Type: RW
BitField Desc: Provisioned Ethernet Type
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_k12rx_evt_ETHTYP_Mask                                                              cBit31_16
#define cAf6_upen_k12rx_evt_ETHTYP_Shift                                                                    16

/*--------------------------------------
BitField Name: VER
BitField Type: RW
BitField Desc: Provisioned Version Number
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_k12rx_evt_VER_Mask                                                                  cBit15_8
#define cAf6_upen_k12rx_evt_VER_Shift                                                                        8

/*--------------------------------------
BitField Name: TYPE
BitField Type: RW
BitField Desc: Provisioned APS Type
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_k12rx_evt_TYPE_Mask                                                                  cBit7_0
#define cAf6_upen_k12rx_evt_TYPE_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_ChannelID_Configuration
Reg Addr   : 0x22100 - 0x2210F
Reg Formula: 0x22100 + $PortID
    Where  : 
           + $PortID(0-15): PortID value
Reg Desc   : 
The register provides ChannelID configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_chcfg_Base                                                                  0x22100
#define cAf6Reg_upen_k12rx_chcfg(PortID)                                                    (0x22100+(PortID))
#define cAf6Reg_upen_k12rx_chcfg_WidthVal                                                                   32

/*--------------------------------------
BitField Name: ChannelID
BitField Type: RW
BitField Desc: 12b represent for Channel ID
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_upen_k12rx_chcfg_ChannelID_Mask                                                          cBit11_0
#define cAf6_upen_k12rx_chcfg_ChannelID_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_ChannelID_Mapping_Configuration
Reg Addr   : 0x22400 - 0x2240F
Reg Formula: 0x22400 + $ChannelID
    Where  : 
           + $ChannelID(0-15): ChannelID value
Reg Desc   : 
The register provides ChannelID configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_chmap_Base                                                                  0x22400
#define cAf6Reg_upen_k12rx_chmap(ChannelID)                                              (0x22400+(ChannelID))
#define cAf6Reg_upen_k12rx_chmap_WidthVal                                                                   32

/*--------------------------------------
BitField Name: PortID
BitField Type: RW
BitField Desc: PortID value map from received Channel ID
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_upen_k12rx_chmap_PortID_Mask                                                              cBit3_0
#define cAf6_upen_k12rx_chmap_PortID_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Alarm_Sticky
Reg Addr   : 0x22010
Reg Formula: 0x22010
    Where  : 
Reg Desc   : 
The register provides Receive APS Frame Alarm sticky

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_alarm                                                                       0x22010
#define cAf6Reg_upen_k12rx_alarm_WidthVal                                                                   32

/*--------------------------------------
BitField Name: per_channel_mismat
BitField Type: W1C
BitField Desc: Received CHANNEL ID value of APS frame different from configured
value. This is a per channel status. Bit 23 -> 08 represent for channel 15 -> 0
respectively
BitField Bits: [23:08]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_per_channel_mismat_Mask                                                 cBit23_8
#define cAf6_upen_k12rx_alarm_per_channel_mismat_Shift                                                       8

/*--------------------------------------
BitField Name: Frame_miss
BitField Type: W1C
BitField Desc: APS FRAME missed When watchdog timer is enable. This bit is set
if no frame is received in pre-defined interval
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_Frame_miss_Mask                                                            cBit7
#define cAf6_upen_k12rx_alarm_Frame_miss_Shift                                                               7

/*--------------------------------------
BitField Name: channel_miss
BitField Type: W1C
BitField Desc: CHANNEL mismatch. Received CHANNELID value different from
configed
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_channel_miss_Mask                                                          cBit6
#define cAf6_upen_k12rx_alarm_channel_miss_Shift                                                             6

/*--------------------------------------
BitField Name: real_len_miss
BitField Type: W1C
BitField Desc: ACTUAL LENGH mismatch. acket LENGTH value different 0x60
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_real_len_miss_Mask                                                         cBit5
#define cAf6_upen_k12rx_alarm_real_len_miss_Shift                                                            5

/*--------------------------------------
BitField Name: len_miss
BitField Type: W1C
BitField Desc: LENGH mismatch. Received LENGTH value different 0x60
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_len_miss_Mask                                                              cBit4
#define cAf6_upen_k12rx_alarm_len_miss_Shift                                                                 4

/*--------------------------------------
BitField Name: ver_miss
BitField Type: W1C
BitField Desc: VERSION  mismatch. Received VERSION value different from configed
VER
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_ver_miss_Mask                                                              cBit3
#define cAf6_upen_k12rx_alarm_ver_miss_Shift                                                                 3

/*--------------------------------------
BitField Name: type_miss
BitField Type: W1C
BitField Desc: TYPE mismatch. Received TYPE value different from configed TYPE
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_type_miss_Mask                                                             cBit2
#define cAf6_upen_k12rx_alarm_type_miss_Shift                                                                2

/*--------------------------------------
BitField Name: ethtype_miss
BitField Type: W1C
BitField Desc: ETHERNET TYPE mismatch. Received ETHTYPE value different from
configed E
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_ethtype_miss_Mask                                                          cBit1
#define cAf6_upen_k12rx_alarm_ethtype_miss_Shift                                                             1

/*--------------------------------------
BitField Name: mac_da_miss
BitField Type: W1C
BitField Desc: DA mismatch. Received DA value different from configed DA
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_k12rx_alarm_mac_da_miss_Mask                                                           cBit0
#define cAf6_upen_k12rx_alarm_mac_da_miss_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_WatchDog_Timer
Reg Addr   : 0x22008
Reg Formula: 0x22008
    Where  : 
Reg Desc   : 
The register provides WatchDog Timer configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_watdog                                                                      0x22008

/*--------------------------------------
BitField Name: Enable
BitField Type: RW
BitField Desc: Enable Watchdog Timer
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_upen_k12rx_watdog_Enable_Mask                                                              cBit31
#define cAf6_upen_k12rx_watdog_Enable_Shift                                                                 31

/*--------------------------------------
BitField Name: Timer_Val
BitField Type: RW
BitField Desc: Watch Dog Timer value. This value is the number of clock counter
represent the expected time interval For ex: to set an interval 256us with the
clock 155Mz The clock counter is: 256x10^3/(10^3/155) = 256*155 = 39680 This
value will be write to this field. Timer range from 256us to 16ms. If in this
timer window, no frames received, alarm will be set
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_k12rx_watdog_Timer_Val_Mask                                                         cBit23_0
#define cAf6_upen_k12rx_watdog_Timer_Val_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Pkt_Length_Alarm_Sticky
Reg Addr   : 0x11004
Reg Formula: 0x11004
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stkerr_pktlen                                                                     0x11004

/*--------------------------------------
BitField Name: dcc_overize_err
BitField Type: W1C
BitField Desc: HDLC Length Oversize Error. Alarm per channel Received HDLC Frame
from OCN has frame length over maximum allowed length (1536bytes) Bit[63] ->
Bit[32]: channel ID 31 ->0
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_stkerr_pktlen_dcc_overize_err_Mask                                                  cBit31_0
#define cAf6_upen_stkerr_pktlen_dcc_overize_err_Shift                                                        0

/*--------------------------------------
BitField Name: dcc_undsize_err
BitField Type: W1C
BitField Desc: HDLC Length Undersize Error. Alarm per channel Received HDLC
Frame from OCN has frame length below minimum allowed length (2bytes) Bit[31] ->
Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_stkerr_pktlen_dcc_undsize_err_Mask                                                  cBit31_0
#define cAf6_upen_stkerr_pktlen_dcc_undsize_err_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Pkt_Error_And_Buffer_Full_Alarm_Sticky
Reg Addr   : 0x11005
Reg Formula: 0x11005
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC CRC Error and Buffer Full (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stkerr_crcbuf                                                                     0x11005

/*--------------------------------------
BitField Name: dcc_crc_err
BitField Type: W1C
BitField Desc: HDLC Packet has CRC error (OCN to ETH direction) . Alarm per
channel Bit[63] -> Bit[32]: channel ID 31 ->0
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_stkerr_crcbuf_dcc_crc_err_Mask                                                      cBit31_0
#define cAf6_upen_stkerr_crcbuf_dcc_crc_err_Shift                                                            0

/*--------------------------------------
BitField Name: dcc_buffull_err
BitField Type: W1C
BitField Desc: HDLC Packet buffer full (OCN to ETH direction) . Alarm per
channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[31]
-> Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_stkerr_crcbuf_dcc_buffull_err_Mask                                                  cBit31_0
#define cAf6_upen_stkerr_crcbuf_dcc_buffull_err_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : DCC_ETH2OCN_Alarm_Sticky
Reg Addr   : 0x12008
Reg Formula: 0x12008
    Where  : 
Reg Desc   : 
The register provides Alarms of ETH2OCN Direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_stkerr_rx_eth2ocn                                                                 0x12008
#define cAf6Reg_upen_stkerr_rx_eth2ocn_WidthVal                                                             96

/*--------------------------------------
BitField Name: dcc_eth2ocn_buffull_err
BitField Type: W1C
BitField Desc: Packet buffer full (ETH to OCN direction) . Alarm per channel
Buffer of Ethernet Frame has been full. Some frames will be dropped Bit[68] ->
Bit[37]: channel ID 31 ->0
BitField Bits: [68:37]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_eth2ocn_buffull_err_Mask_01                                   cBit31_5
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_eth2ocn_buffull_err_Shift_01                                         5
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_eth2ocn_buffull_err_Mask_02                                    cBit4_0
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_eth2ocn_buffull_err_Shift_02                                         0

/*--------------------------------------
BitField Name: dcc_rxeth_maxlenerr
BitField Type: W1C
BitField Desc: Received DCC packet from SGMII port has violated maximum packet's
length error
BitField Bits: [36:36]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_rxeth_maxlenerr_Mask                                             cBit4
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_rxeth_maxlenerr_Shift                                                4

/*--------------------------------------
BitField Name: dcc_rxeth_crcerr
BitField Type: W1C
BitField Desc: Received DCC packet from SGMII port has CRC error
BitField Bits: [35:35]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_rxeth_crcerr_Mask                                                cBit3
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_rxeth_crcerr_Shift                                                   3

/*--------------------------------------
BitField Name: dcc_channel_disable
BitField Type: W1C
BitField Desc: DCC Local Channel Identifier mapping is disable Bit[34] ->
Bit[03] indicate channel 31-> 00.
BitField Bits: [34:03]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_channel_disable_Mask_01                                       cBit31_3
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_channel_disable_Shift_01                                             3
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_channel_disable_Mask_02                                        cBit2_0
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_channel_disable_Shift_02                                             0

/*--------------------------------------
BitField Name: dcc_cvlid_mismat
BitField Type: W1C
BitField Desc: Received 12b CVLAN ID value of DCC frame different from global
provisioned CVID
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_cvlid_mismat_Mask                                                cBit2
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_cvlid_mismat_Shift                                                   2

/*--------------------------------------
BitField Name: dcc_macda_mismat
BitField Type: W1C
BitField Desc: Received 43b MAC DA value of DCC frame different from global
provisioned DA
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_macda_mismat_Mask                                                cBit1
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_macda_mismat_Shift                                                   1

/*--------------------------------------
BitField Name: dcc_ethtp_mismat
BitField Type: W1C
BitField Desc: Received Ethernet Type of DCC frame different from global
provisioned value
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_ethtp_mismat_Mask                                                cBit0
#define cAf6_upen_stkerr_rx_eth2ocn_dcc_ethtp_mismat_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Min_Pkt_Length_Current Status
Reg Addr   : 0x11008
Reg Formula: 0x11008
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_undsz                                                                      0x11008

/*--------------------------------------
BitField Name: dcc_ocn2eth_undsz_err_cursta
BitField Type: RO
BitField Desc: HDLC Length Undersize Error. Status per channel Received HDLC
Frame from OCN has frame length below minimum allowed length (2bytes) Bit[31] ->
Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_curerr_undsz_dcc_ocn2eth_undsz_err_cursta_Mask                                      cBit31_0
#define cAf6_upen_curerr_undsz_dcc_ocn2eth_undsz_err_cursta_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Max_Pkt_Length_Current_Status
Reg Addr   : 0x11009
Reg Formula: 0x11009
    Where  : 
Reg Desc   : 
The register provides Alarm Status Related to HDLC Length Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_ovrsz                                                                      0x11009

/*--------------------------------------
BitField Name: cc_ocn2eth_ovrsz_err_cursta
BitField Type: RO
BitField Desc: HDLC Length Oversize Error. Status per channel Received HDLC
Frame from OCN has frame length over maximum allowed length (1536bytes) Bit[31]
-> Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_curerr_ovrsz_cc_ocn2eth_ovrsz_err_cursta_Mask                                       cBit31_0
#define cAf6_upen_curerr_ovrsz_cc_ocn2eth_ovrsz_err_cursta_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Pkt_CRC_Error_Alarm_Status
Reg Addr   : 0x1100A
Reg Formula: 0x1100A
    Where  : 
Reg Desc   : 
The register provides Alarm Status Related to HDLC CRC Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_crcbuf                                                                     0x1100A

/*--------------------------------------
BitField Name: dcc_ocn2eth_crc_err_cursta
BitField Type: RO
BitField Desc: HDLC Packet has CRC error (OCN to ETH direction) . Alarm per
channel Bit[31] -> Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_curerr_crcbuf_dcc_ocn2eth_crc_err_cursta_Mask                                       cBit31_0
#define cAf6_upen_curerr_crcbuf_dcc_ocn2eth_crc_err_cursta_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Buffer_Full_Alarm_Current_Status
Reg Addr   : 0x1100B
Reg Formula: 0x1100B
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC Buffer Full (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_bufept                                                                     0x1100B

/*--------------------------------------
BitField Name: dcc_ocn2eth_bufful_err_cursta
BitField Type: RO
BitField Desc: HDLC Packet buffer full (OCN to ETH direction) . Alarm per
channel Buffer for HDLC Frame has been full. Some frames will be dropped Bit[31]
-> Bit[0]: channel ID 31 ->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_curerr_bufept_dcc_ocn2eth_bufful_err_cursta_Mask                                    cBit31_0
#define cAf6_upen_curerr_bufept_dcc_ocn2eth_bufful_err_cursta_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCC OCN2ETH Current Status of Global Alarm
Reg Addr   : 0x1100E
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides current status of global alarm of DCC, OCN2ETH direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_curerr_glb                                                                        0x1100E
#define cAf6Reg_upen_curerr_glb_WidthVal                                                                    32

/*--------------------------------------
BitField Name: mux_dcc_ocn2eth_cursta_ful_err
BitField Type: RO
BitField Desc: Current Status of  "OCN2ETH: One or more channels has buffer full
error"
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_ful_err_Mask                                         cBit3
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_ful_err_Shift                                            3

/*--------------------------------------
BitField Name: mux_dcc_ocn2eth_cursta_crc_err
BitField Type: RO
BitField Desc: Current Status of  "OCN2ETH: One or more channels has CRC error
packet "
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_crc_err_Mask                                         cBit2
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_crc_err_Shift                                            2

/*--------------------------------------
BitField Name: mux_dcc_ocn2eth_cursta_undsz_len
BitField Type: RO
BitField Desc: Current Status of  "OCN2ETH: One or more channels has minimum
packet length violation"
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_undsz_len_Mask                                       cBit1
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_undsz_len_Shift                                          1

/*--------------------------------------
BitField Name: mux_dcc_ocn2eth_cursta_ovrsz_len
BitField Type: RO
BitField Desc: Current Status of  "OCN2ETH: One or more channels has maximum
packet length violation "
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_ovrsz_len_Mask                                       cBit0
#define cAf6_upen_curerr_glb_mux_dcc_ocn2eth_cursta_ovrsz_len_Shift                                          0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Alarm " Local Channel Mapping Is Disable" Current Status of Channel 31 to 0
Reg Addr   : 0x12004
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable interrupt of DCC events ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cur_rxdcc0                                                                        0x12004

/*--------------------------------------
BitField Name: dcc_eth2ocn_chandis_cursta_31_0
BitField Type: RO
BitField Desc: Current Status of Alarm "DCC Local Channel Identifier mapping is
disable" of channel 0 to 31 Bit[00] -> Bit[31] indicate channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cur_rxdcc0_dcc_eth2ocn_chandis_cursta_31_0_Mask                                     cBit31_0
#define cAf6_upen_cur_rxdcc0_dcc_eth2ocn_chandis_cursta_31_0_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : DCC ETH2OCN Direction Curren Status of Buffer Full Channel 31 to 0
Reg Addr   : 0x12006
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides current alarm status of DCC event ETH2OCN direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_cur_rxdcc2                                                                        0x12006

/*--------------------------------------
BitField Name: dcc_eth2ocn_bufful_cursta_31_0
BitField Type: RO
BitField Desc: Current status of "DCC packet buffer for ETH2OCN direction was
fulled, some packets will be lost." channel 0 to 31 Bit[00] -> Bit[31] indicate
status of channel 0 -> 31.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_cur_rxdcc2_dcc_eth2ocn_bufful_cursta_31_0_Mask                                      cBit31_0
#define cAf6_upen_cur_rxdcc2_dcc_eth2ocn_bufful_cursta_31_0_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Trig_En_Cap
Reg Addr   : 0x22006
Reg Formula: 0x22006
    Where  : 
Reg Desc   : 
The register provides WatchDog Timer configuration

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_trig_encap                                                                  0x22006
#define cAf6Reg_upen_k12rx_trig_encap_WidthVal                                                              32

/*--------------------------------------
BitField Name: Enable
BitField Type: RW
BitField Desc: Trigger Enable Capturing Ethernet Header - Write '0' first, then
write '1' to enable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_k12rx_trig_encap_Enable_Mask                                                           cBit0
#define cAf6_upen_k12rx_trig_encap_Enable_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Packet_Header_Cap_Reg0
Reg Addr   : 0x22200 - 0x2220C
Reg Formula: 0x22200 + 3*$pktnum
    Where  : 
           + $pktnum(0-4): Number of packet capture
Reg Desc   : 
The register provides captured header data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_cap_reg0_Base                                                               0x22200
#define cAf6Reg_upen_k12rx_cap_reg0(pktnum)                                               (0x22200+3*(pktnum))

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RO
BitField Desc: Captured MAC DA value
BitField Bits: [63:16]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg0_MAC_DA_Mask_01                                                      cBit31_16
#define cAf6_upen_k12rx_cap_reg0_MAC_DA_Shift_01                                                            16
#define cAf6_upen_k12rx_cap_reg0_MAC_DA_Mask_02                                                       cBit31_0
#define cAf6_upen_k12rx_cap_reg0_MAC_DA_Shift_02                                                             0

/*--------------------------------------
BitField Name: MAC_SA
BitField Type: RO
BitField Desc: 16b MSB of Captured MAC SA value
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg0_MAC_SA_Mask                                                          cBit15_0
#define cAf6_upen_k12rx_cap_reg0_MAC_SA_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Packet_Header_Cap_Reg1
Reg Addr   : 0x22201 - 0x2220D
Reg Formula: 0x22201 + 3*$pktnum
    Where  : 
           + $pktnum(0-4): Number of packet capture
Reg Desc   : 
The register provides captured header data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_cap_reg1_Base                                                               0x22201
#define cAf6Reg_upen_k12rx_cap_reg1(pktnum)                                               (0x22201+3*(pktnum))

/*--------------------------------------
BitField Name: MAC_SA
BitField Type: RO
BitField Desc: 32b MSB of Captured MAC SA value
BitField Bits: [63:32]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg1_MAC_SA_Mask                                                          cBit31_0
#define cAf6_upen_k12rx_cap_reg1_MAC_SA_Shift                                                                0

/*--------------------------------------
BitField Name: VLAN
BitField Type: RO
BitField Desc: Captured VLAN value:
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg1_VLAN_Mask                                                            cBit31_0
#define cAf6_upen_k12rx_cap_reg1_VLAN_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : APS_RX_Packet_Header_Cap_Reg2
Reg Addr   : 0x22202 - 0x2220E
Reg Formula: 0x22202 + 3*$pktnum
    Where  : 
           + $pktnum(0-4): Number of packet capture
Reg Desc   : 
The register provides captured header data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12rx_cap_reg2_Base                                                               0x22202
#define cAf6Reg_upen_k12rx_cap_reg2(pktnum)                                               (0x22202+3*(pktnum))

/*--------------------------------------
BitField Name: ETH_TYP
BitField Type: RO
BitField Desc: 16b value of Ethernet Type
BitField Bits: [63:48]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg2_ETH_TYP_Mask                                                        cBit31_16
#define cAf6_upen_k12rx_cap_reg2_ETH_TYP_Shift                                                              16

/*--------------------------------------
BitField Name: VER
BitField Type: RO
BitField Desc: 8b Version
BitField Bits: [47:40]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg2_VER_Mask                                                             cBit15_8
#define cAf6_upen_k12rx_cap_reg2_VER_Shift                                                                   8

/*--------------------------------------
BitField Name: TYP
BitField Type: RO
BitField Desc: 8b APS Type
BitField Bits: [39:32]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg2_TYP_Mask                                                              cBit7_0
#define cAf6_upen_k12rx_cap_reg2_TYP_Shift                                                                   0

/*--------------------------------------
BitField Name: LENGTH
BitField Type: RO
BitField Desc: 16b packet length
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_k12rx_cap_reg2_LENGTH_Mask                                                         cBit31_16
#define cAf6_upen_k12rx_cap_reg2_LENGTH_Shift                                                               16


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Unknow_From_SGMII_Port0
Reg Addr   : 0x22021
Reg Formula: 0x22021
    Where  : 
Reg Desc   : 
Counter of number of Unknow Packet (not APS packet) receive from SGMII port 0

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12_glbcnt_rxeop                                                                  0x22021

/*--------------------------------------
BitField Name: glb_sgm2ocn_aps_counter
BitField Type: WC
BitField Desc: Counter of receive packet from SGMII port 0 has ETH_TYPE of KByte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12_glbcnt_rxeop_glb_sgm2ocn_aps_counter_Mask                                       cBit31_0
#define cAf6_upen_k12_glbcnt_rxeop_glb_sgm2ocn_aps_counter_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Unknow_From_SGMII_Port0
Reg Addr   : 0x22024
Reg Formula: 0x22024
    Where  : 
Reg Desc   : 
Counter of number of Unknow Packet (not APS packet) receive from SGMII port 0

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12_glbcnt_sfail                                                                  0x22024

/*--------------------------------------
BitField Name: glb_sgm2ocn_unk_counter
BitField Type: WC
BitField Desc: Counter of unknow receive packet from SGMII port 0 (not KByte nor
DCC packet)
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12_glbcnt_sfail_glb_sgm2ocn_unk_counter_Mask                                       cBit31_0
#define cAf6_upen_k12_glbcnt_sfail_glb_sgm2ocn_unk_counter_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Rx_APS_Passed_From_SGMII_Port0
Reg Addr   : 0x22025
Reg Formula: 0x22025
    Where  : 
Reg Desc   : 
Counter of total number of detected APS Packet receive from SGMII port 0

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12_glbcnt_sok                                                                    0x22025

/*--------------------------------------
BitField Name: glb_sgm2ocn_sok_counter
BitField Type: WC
BitField Desc: Counter of detected APS packet and passed classification from
SGMII port 0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12_glbcnt_sok_glb_sgm2ocn_sok_counter_Mask                                         cBit31_0
#define cAf6_upen_k12_glbcnt_sok_glb_sgm2ocn_sok_counter_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Rx_APS_Byte_Passed_From_SGMII_Port0
Reg Addr   : 0x22023
Reg Formula: 0x22023
    Where  : 
Reg Desc   : 
Counter of total number of detected APS Packet receive from SGMII port 0

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12_glbcnt_sok_byte                                                               0x22023

/*--------------------------------------
BitField Name: glb_sgm2ocn_sok_byte_counter
BitField Type: WC
BitField Desc: Counter bytes of detected Kbyte packet passed classification from
SGMII port 0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12_glbcnt_sok_byte_glb_sgm2ocn_sok_byte_counter_Mask                                cBit31_0
#define cAf6_upen_k12_glbcnt_sok_byte_glb_sgm2ocn_sok_byte_counter_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Discarded_APS_From_SGMII_Port0
Reg Addr   : 0x22026
Reg Formula: 0x22026
    Where  : 
Reg Desc   : 
Counter of number of Unknow Packet (not APS packet) receive from SGMII port 0

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12_glbcnt_serr                                                                   0x22026

/*--------------------------------------
BitField Name: glb_sgm2ocn_serr_counter
BitField Type: WC
BitField Desc: Counter of discarded APS packets received from SGMII port 0
Ethernet Type of packet is detect as Kbyte packet but the packet has fail other
conditions to extract Kbyte value in packet
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12_glbcnt_serr_glb_sgm2ocn_serr_counter_Mask                                       cBit31_0
#define cAf6_upen_k12_glbcnt_serr_glb_sgm2ocn_serr_counter_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_APS_From_SGMII_Port0_Per_Chan
Reg Addr   : 0x22300 - 0x2230F
Reg Formula: 0x22300 + $channelID
    Where  : 
           + $channelID(0-15): APS channelID
Reg Desc   : 
Counter of number of detected APS Packet per channel receive from SGMII port 0 per channelID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12_sokpkt_pcid_Base                                                              0x22300
#define cAf6Reg_upen_k12_sokpkt_pcid(channelID)                                          (0x22300+(channelID))

/*--------------------------------------
BitField Name: pcid_sgm2ocn_sok_counter
BitField Type: WC
BitField Desc: Counter of APS packets receive from SGMII port 0 per channelID
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12_sokpkt_pcid_pcid_sgm2ocn_sok_counter_Mask                                       cBit31_0
#define cAf6_upen_k12_sokpkt_pcid_pcid_sgm2ocn_sok_counter_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_APS_BYTE_From_RxOcn_Per_Chan
Reg Addr   : 0x21330 - 0x2133F
Reg Formula: 0x21330 + $channelID
    Where  : 
           + $channelID(0-15): APS channelID
Reg Desc   : 
Counter number of BYTE of APS packet receive from RX-OCN Per Channel

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_byt2k12_pcid_Base                                                                 0x21330
#define cAf6Reg_upen_byt2k12_pcid(channelID)                                             (0x21330+(channelID))

/*--------------------------------------
BitField Name: byte_counter
BitField Type: WC
BitField Desc: Counter number of BYTE of APS packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_byt2k12_pcid_byte_counter_Mask                                                      cBit31_0
#define cAf6_upen_byt2k12_pcid_byte_counter_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Total_Rx_APS_Packet_From_RxOcn
Reg Addr   : 0x21360
Reg Formula: 
    Where  : 
Reg Desc   : 
Counter Total number of RX APS packet receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_glbsop2k12                                                                        0x21360

/*--------------------------------------
BitField Name: rx_aps_pkt_cnt
BitField Type: WC
BitField Desc: Counter number of APS packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_glbsop2k12_rx_aps_pkt_cnt_Mask                                                      cBit31_0
#define cAf6_upen_glbsop2k12_rx_aps_pkt_cnt_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_APS_BYTE_From_RxOcn_Per_Chan
Reg Addr   : 0x21361
Reg Formula: 
    Where  : 
Reg Desc   : 
Counter number of Total BYTE of APS packet receive from RX-OCN

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_glbbyt2k12                                                                        0x21361

/*--------------------------------------
BitField Name: rx_aps_byte_counter
BitField Type: WC
BitField Desc: Counter number of BYTE of APS packets receive from RX-OCN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_glbbyt2k12_rx_aps_byte_counter_Mask                                                 cBit31_0
#define cAf6_upen_glbbyt2k12_rx_aps_byte_counter_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_APS_SOP_To_SGMII
Reg Addr   : 0x21350
Reg Formula: 
    Where  : 
Reg Desc   : 
Counter number of SOP of TX APS packet to SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12sop2sgm_pcid                                                                   0x21350

/*--------------------------------------
BitField Name: sop_counter
BitField Type: WC
BitField Desc: Counter number of SOP of TX APS packets to SGMII
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12sop2sgm_pcid_sop_counter_Mask                                                    cBit31_0
#define cAf6_upen_k12sop2sgm_pcid_sop_counter_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_APS_EOP_To_SGMII
Reg Addr   : 0x21351
Reg Formula: 
    Where  : 
Reg Desc   : 
Counter number of EOP of TX APS packet to SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12eop2sgm_pcid                                                                   0x21351

/*--------------------------------------
BitField Name: eop_counter
BitField Type: WC
BitField Desc: Counter number of EOP of TX APS packets to SGMII
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12eop2sgm_pcid_eop_counter_Mask                                                    cBit31_0
#define cAf6_upen_k12eop2sgm_pcid_eop_counter_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_APS_BYTE_To_SGMII
Reg Addr   : 0x21352
Reg Formula: 
    Where  : 
Reg Desc   : 
Counter number of BYTE of TX APS packet to SGMII

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12byt2sgm_pcid                                                                   0x21352

/*--------------------------------------
BitField Name: byte_counter
BitField Type: WC
BitField Desc: Counter number of BYTE of TX APS packets to SGMII
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_k12byt2sgm_pcid_byte_counter_Mask                                                   cBit31_0
#define cAf6_upen_k12byt2sgm_pcid_byte_counter_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Eop_From_SGMII_Port
Reg Addr   : 0x12021
Reg Formula: 0x12021
    Where  : 
Reg Desc   : 
Counter of number of EOP receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxeop                                                               0x12021

/*--------------------------------------
BitField Name: eop_counter
BitField Type: WC
BitField Desc: Counter of EOP receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxeop_eop_counter_Mask                                                cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxeop_eop_counter_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Err_From_SGMII_Port
Reg Addr   : 0x12022
Reg Formula: 0x12022
    Where  : 
Reg Desc   : 
Counter of number of ERR receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxerr                                                               0x12022

/*--------------------------------------
BitField Name: err_counter
BitField Type: WC
BitField Desc: Counter of ERR receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxerr_err_counter_Mask                                                cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxerr_err_counter_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Byte_From_SGMII_Port
Reg Addr   : 0x12023
Reg Formula: 0x12023
    Where  : 
Reg Desc   : 
Counter of number of BYTE receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxbyt                                                               0x12023

/*--------------------------------------
BitField Name: byte_counter
BitField Type: WC
BitField Desc: Counter of BYTE receive from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxbyt_byte_counter_Mask                                               cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxbyt_byte_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Unknow_From_SGMII_Port
Reg Addr   : 0x12024
Reg Formula: 0x12024
    Where  : 
Reg Desc   : 
Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxfail                                                              0x12024

/*--------------------------------------
BitField Name: unk_counter
BitField Type: WC
BitField Desc: Counter of unknow receive packet from SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxfail_unk_counter_Mask                                               cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxfail_unk_counter_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Passed_From_SGMII_Port
Reg Addr   : 0x12031
Reg Formula: 0x12031
    Where  : 
Reg Desc   : 
Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxpass                                                              0x12031

/*--------------------------------------
BitField Name: pass_counter
BitField Type: WC
BitField Desc: Counter of passed received DCC packet from SGMII port 1 .Packet
is detected as DCC packet and classify into channel successfully
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxpass_pass_counter_Mask                                              cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxpass_pass_counter_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Rx_Discarded_From_SGMII_Port
Reg Addr   : 0x12033
Reg Formula: 0x12033
    Where  : 
Reg Desc   : 
Counter of number of Unknow Packet (not DCC packet) receive from SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmrxdisc                                                              0x12033

/*--------------------------------------
BitField Name: disc_counter
BitField Type: WC
BitField Desc: Counter of discarded received DCC packet from SGMII port 1
.Packet is detected as DCC packet but fail other conditions to classify into
channel
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmrxdisc_disc_counter_Mask                                              cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmrxdisc_disc_counter_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Eop_To_SGMII_Port1
Reg Addr   : 0x11501
Reg Formula: 0x11501
    Where  : 
Reg Desc   : 
Counter of number of EOP transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxeop                                                               0x11501

/*--------------------------------------
BitField Name: sgm_txglb_eop_counter
BitField Type: WC
BitField Desc: Counter of EOP transmit to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxeop_sgm_txglb_eop_counter_Mask                                      cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxeop_sgm_txglb_eop_counter_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Err_To_SGMII_Port1
Reg Addr   : 0x11502
Reg Formula: 0x11502
    Where  : 
Reg Desc   : 
Counter of number of ERR transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxerr                                                               0x11502

/*--------------------------------------
BitField Name: sgm_txglb_err_counter
BitField Type: WC
BitField Desc: Counter of ERR  transmit to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxerr_sgm_txglb_err_counter_Mask                                      cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxerr_sgm_txglb_err_counter_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Tx_Byte_To_SGMII_Port1
Reg Addr   : 0x11503
Reg Formula: 0x11503
    Where  : 
Reg Desc   : 
Counter of number of BYTE transmit to SGMII port 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_glbcnt_sgmtxbyt                                                               0x11503

/*--------------------------------------
BitField Name: sgm_txglb_byte_counter
BitField Type: WC
BitField Desc: Counter of TX BYTE to SGMII port 1
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_glbcnt_sgmtxbyt_sgm_txglb_byte_counter_Mask                                     cBit31_0
#define cAf6_upen_dcc_glbcnt_sgmtxbyt_sgm_txglb_byte_counter_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : Counter_Packet_And_Byte_Per_Channel
Reg Addr   : 0x13000 - 0x13FFF
Reg Formula: 0x13000 + $cnt_type*32 + $channelID
    Where  : 
           + $cnt_type(1-14): counter type
           + $channelID(0-47): channel value
Reg Desc   : 
Counter of DCC at different point represent by cnt_type value:
{1} : counter RX bytes from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction
{2} : counter RX good packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction
{3} : counter RX error packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction
{4} : counter RX lost packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction
{5} : counter TX bytes from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction
{6} : counter TX packets from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction
{7} : counter TX error packet from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction
{8} : counter RX bytes from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction
{9} : counter RX good packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction
{10}: counter RX error packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction
{11}: counter RX lost packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction
{12}: counter TX bytes from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction
{13}: counter TX good packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction
{14}: counter TX error packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cnt_Base                                                                      0x13000
#define cAf6Reg_upen_dcc_cnt(cnttype, channelID)                            (0x13000+(cnttype)*32+(channelID))

/*--------------------------------------
BitField Name: dcc_counter
BitField Type: WC
BitField Desc: Counter of DCC packet and byte
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_dcc_cnt_dcc_counter_Mask                                                            cBit31_0
#define cAf6_upen_dcc_cnt_dcc_counter_Shift                                                                  0

#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_byte       1
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_good_pkt   2
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_error_pkt  3
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_lost_pkt   4
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_BUF2SGM_byte       5
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_BUF2SGM_pkt        6
#define cAf6Reg_upen_dcc_cnt_type_TDM2PSN_BUF2SGM_error_pkt  7
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGM2BUF_byte       8
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGM2BUF_good_pkt   9
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGM2BUF_error_pkt  10
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGMI2BUF_lost_pkt  11
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_BUF2ENC_byte       12
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_BUF2ENC_good_pkt   13
#define cAf6Reg_upen_dcc_cnt_type_PSN2TDM_BUF2ENC_error_pkt  14

/*--------------------------------------------------------------------------
Register Full Name: Counter_Rx_Drop_Packet_Per_Channel
RTL Instant Name  : af6ces10grtldcc_rx__upen_dispkt_pcid
Address: 0x12100-0x1211F
Formula: Address +  ChannelID
Where: {$ChannelID(0-31): HDLC Channel ID}
Description: Counter of number of discard packet per channel due too full
Width: 32
Field: [Bit:Bit] %%  Name        %% Description                                  %% Type     %% SW_Reset %% HW_Reset
Field: [31:0]    %% eth2ocn_disc_pkt_cnt     %% Counter of discarded packet due too buffer full      %% WC   %% 0x0      %% 0x0
--------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtldcc_rx__upen_dispkt_pcid_Base                                                 0x12100

/*------------------------------------------------------------------------------
Reg Name   : CONFIG HDLC LO DEC
Reg Addr   : 0x04000 - 0x0401F
Reg Formula: 0x04000+ $CID
    Where  : 
           + $CID (0-31) : Channel ID
Reg Desc   : 
config HDLC ID 0-31
HDL_PATH: iaf6cci0012_lodec_core.ihdlc_cfg.imem113x.ram.ram[$CID]

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_locfg_Base                                                                   0x04000
#define cAf6Reg_upen_hdlc_locfg(CID)                                                           (0x04000+(CID))
#define cAf6Reg_upen_hdlc_locfg_WidthVal                                                                    32

/*--------------------------------------
BitField Name: cfg_scren
BitField Type: R/W
BitField Desc: config to enable scramble, (1) is enable, (0) is disable
BitField Bits: [4]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_scren_Mask                                                              cBit4
#define cAf6_upen_hdlc_locfg_cfg_scren_Shift                                                                 4

/*--------------------------------------
BitField Name: cfg_bitstuff
BitField Type: R/W
BitField Desc: config to select bit stuff or byte sutff, (1) is bit stuff, (0)
is byte stuff
BitField Bits: [2]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_Mask                                                           cBit2
#define cAf6_upen_hdlc_locfg_cfg_bitstuff_Shift                                                              2

/*--------------------------------------
BitField Name: cfg_fcsmsb
BitField Type: R/W
BitField Desc: config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB
BitField Bits: [1]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_Mask                                                             cBit1
#define cAf6_upen_hdlc_locfg_cfg_fcsmsb_Shift                                                                1

/*--------------------------------------
BitField Name: cfg_fcsmode
BitField Type: R/W
BitField Desc: config to calculate FCS 32 or FCS 16, (1) is FCS 32, (0) is FCS
16
BitField Bits: [0]
--------------------------------------*/
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_Mask                                                            cBit0
#define cAf6_upen_hdlc_locfg_cfg_fcsmode_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : CONFIG HDLC BIT ORDER LO DEC
Reg Addr   : 0x04407
Reg Formula: 
    Where  : 
Reg Desc   : 
config Bit Order

------------------------------------------------------------------------------*/
#define cAf6Reg_af6cci0012_lodec_core__icfg_fcslsb                                                     0x04407

/*--------------------------------------
BitField Name: cfg_lsbfirst
BitField Type: R/W
BitField Desc: config to receive LSB first. Bit[31]->[0] equivalent to channel
31 -> 0 (1) is LSB first, (0) is default MSB
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_af6cci0012_lodec_core__icfg_fcslsb_cfg_lsbfirst_Mask                                     cBit31_0
#define cAf6_af6cci0012_lodec_core__icfg_fcslsb_cfg_lsbfirst_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Data_LSB_First
Reg Addr   : 0x30008
Reg Formula: 
    Where  : 
Reg Desc   : 
config HDLC Data LSB first

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_data_lsb_first                                                           0x30008

/*--------------------------------------
BitField Name: encap_lsbfirst
BitField Type: R/W
BitField Desc: config to transmit LSB first per channel Bit[31] -> [0]: channel
31 -> 0 (1) is LSB first, (0) is default MSB
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_data_lsb_first_encap_lsbfirst_Mask                                         cBit31_0
#define cAf6_upen_hdlc_enc_data_lsb_first_encap_lsbfirst_Shift                                               0


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Control Reg 1
Reg Addr   : 0x38000 - 0x3801F
Reg Formula: 0x38000+ $CID
    Where  : 
           + $CID (0-31) : Channel ID
Reg Desc   : 
config HDLC Encode Control Register 1

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_ctrl_reg1_Base                                                           0x38000
#define cAf6Reg_upen_hdlc_enc_ctrl_reg1(CID)                                                   (0x38000+(CID))

/*--------------------------------------
BitField Name: encap_screnb
BitField Type: R/W
BitField Desc: Enable Scamble (1) Enable      , (0) disable
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_screnb_Mask                                                   cBit9
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_screnb_Shift                                                      9

/*--------------------------------------
BitField Name: encap_idlemod
BitField Type: R/W
BitField Desc: This bit is only used in Bit Stuffing mode Used to configured
IDLE Mode When it is active, the ENC engine will insert '1' pattern when the ENC
is idle. Otherwise the ENC will insert FLAG '7E' pattern (1) Enable            ,
(0) Disabe
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_idlemod_Mask                                                  cBit7
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_idlemod_Shift                                                     7

/*--------------------------------------
BitField Name: encap_sabimod
BitField Type: R/W
BitField Desc: Sabi/Protocol Field Mode (1) Field has 2 bytes , (0) field has 1
byte
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabimod_Mask                                                  cBit6
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabimod_Shift                                                     6

/*--------------------------------------
BitField Name: encap_sabiins
BitField Type: R/W
BitField Desc: Sabi/Protocol Field Insert Enable (1) Enable Insert     , (0)
Disable Insert
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabiins_Mask                                                  cBit5
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_sabiins_Shift                                                     5

/*--------------------------------------
BitField Name: encap_ctrlins
BitField Type: R/W
BitField Desc: Address/Control Field Insert Enable (1) Enable Insert     , (0)
Disable Insert
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_ctrlins_Mask                                                  cBit4
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_ctrlins_Shift                                                     4

/*--------------------------------------
BitField Name: encap_fcsmod
BitField Type: R/W
BitField Desc: FCS Select Mode (1) 32b FCS           , (0) 16b FCS
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsmod_Mask                                                   cBit3
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsmod_Shift                                                      3

/*--------------------------------------
BitField Name: encap_fcsins
BitField Type: R/W
BitField Desc: FCS Insert Enable (1) Enable Insert     , (0) Disable Insert
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsins_Mask                                                   cBit2
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsins_Shift                                                      2

/*--------------------------------------
BitField Name: encap_flgmod
BitField Type: R/W
BitField Desc: Flag Mode (1) Minimum 2 Flag    , (0) Minimum 1 Flag
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_flgmod_Mask                                                   cBit1
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_flgmod_Shift                                                      1

/*--------------------------------------
BitField Name: encap_stfmod
BitField Type: R/W
BitField Desc: Stuffing Mode (1) Bit Stuffing      , (0) Byte Stuffing
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_stfmod_Mask                                                   cBit0
#define cAf6_upen_hdlc_enc_ctrl_reg1_encap_stfmod_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Control Reg 2
Reg Addr   : 0x39000 - 0x3901F
Reg Formula: 0x39000+ $CID
    Where  : 
           + $CID (0-31) : Channel ID
Reg Desc   : 
config HDLC Encode Control Register 2 Byte Stuff Mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_ctrl_reg2_Base                                                           0x39000
#define cAf6Reg_upen_hdlc_enc_ctrl_reg2(CID)                                                   (0x39000+(CID))

/*--------------------------------------
BitField Name: encap_addrval
BitField Type: R/W
BitField Desc: Address Field
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_addrval_Mask                                              cBit31_24
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_addrval_Shift                                                    24

/*--------------------------------------
BitField Name: encap_ctlrval
BitField Type: R/W
BitField Desc: Control Field
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_ctlrval_Mask                                              cBit23_16
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_ctlrval_Shift                                                    16

/*--------------------------------------
BitField Name: encap_sapival0
BitField Type: R/W
BitField Desc: SAPI/PROTOCOL Field 1st byte
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival0_Mask                                              cBit15_8
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival0_Shift                                                    8

/*--------------------------------------
BitField Name: encap_sapival1
BitField Type: R/W
BitField Desc: SAPI/PROTOCOL Field 2nd byte
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival1_Mask                                               cBit7_0
#define cAf6_upen_hdlc_enc_ctrl_reg2_encap_sapival1_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Control Reg 3
Reg Addr   : 0x30006
Reg Formula: 
    Where  : 
Reg Desc   : 
config HDLC Encode Control Register 3 config FCS calculation MSB bit first

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_ctrl_reg3                                                                0x30006

/*--------------------------------------
BitField Name: encap_fcsmsb
BitField Type: R/W
BitField Desc: config to calculate FCS from MSB or LSB, (1) is MSB, (0) is LSB
Bit 31->0: channel 31->0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg3_encap_fcsmsb_Mask                                                cBit31_0
#define cAf6_upen_hdlc_enc_ctrl_reg3_encap_fcsmsb_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : HDLC Encode Control Reg 4
Reg Addr   : 0x30007
Reg Formula: 
    Where  : 
Reg Desc   : 
config HDLC Encode Control Register 3 config swap final FCS value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_ctrl_reg4                                                                0x30007

/*--------------------------------------
BitField Name: encap_swapfcs
BitField Type: R/W
BitField Desc: config to swap FCS bit. Per channel (1) enable swap , (0):
disable Bit 31->0: channel 31->0
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_hdlc_enc_ctrl_reg4_encap_swapfcs_Mask                                               cBit31_0
#define cAf6_upen_hdlc_enc_ctrl_reg4_encap_swapfcs_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Enable Packet Capture
Reg Addr   : 0x0A000
Reg Formula: 
    Where  : 
Reg Desc   : 
config enable capture ethernet packet.

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_trig_encap                                                                        0x0A000
#define cAf6Reg_upen_trig_encap_WidthVal                                                                    32

/*--------------------------------------
BitField Name: ram_trig_fls
BitField Type: R/W
BitField Desc: Trigger flush packets captured RAM Write '0' first, then write
'1' to trigger flush RAM process
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_trig_encap_ram_trig_fls_Mask                                                           cBit3
#define cAf6_upen_trig_encap_ram_trig_fls_Shift                                                              3

/*--------------------------------------
BitField Name: enb_cap_dec
BitField Type: R/W
BitField Desc: Enable capture packets from HDLC Encap/Decapsulation. Write '0'
first, then write '1' to trigger capture process
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_trig_encap_enb_cap_dec_Mask                                                            cBit2
#define cAf6_upen_trig_encap_enb_cap_dec_Shift                                                               2


/*------------------------------------------------------------------------------
Reg Name   : Captured Packet Data
Reg Addr   : 0x08000 - 0x080FF
Reg Formula: 0x08000 + $loc
    Where  : 
           + $loc(0-255) : data location
Reg Desc   : 
Captured packet data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_pktcap_Base                                                                       0x08000
#define cAf6Reg_upen_pktcap(loc)                                                               (0x08000+(loc))
#define cAf6Reg_upen_pktcap_WidthVal                                                                        96

/*--------------------------------------
BitField Name: cap_sop
BitField Type: RO
BitField Desc: Start of packet
BitField Bits: [69:69]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_sop_Mask                                                                    cBit5
#define cAf6_upen_pktcap_cap_sop_Shift                                                                       5

/*--------------------------------------
BitField Name: cap_eop
BitField Type: RO
BitField Desc: End   of packet
BitField Bits: [68:68]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_eop_Mask                                                                    cBit4
#define cAf6_upen_pktcap_cap_eop_Shift                                                                       4

/*--------------------------------------
BitField Name: cap_err
BitField Type: RO
BitField Desc: Error of packet
BitField Bits: [67:67]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_err_Mask                                                                    cBit3
#define cAf6_upen_pktcap_cap_err_Shift                                                                       3

/*--------------------------------------
BitField Name: cap_nob
BitField Type: RO
BitField Desc: Number of valid bytes in 8-bytes data captured
BitField Bits: [66:64]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_nob_Mask                                                                  cBit2_0
#define cAf6_upen_pktcap_cap_nob_Shift                                                                       0

/*--------------------------------------
BitField Name: cap_dat
BitField Type: RO
BitField Desc: packet data captured
BitField Bits: [63:00]
--------------------------------------*/
#define cAf6_upen_pktcap_cap_dat_Mask_01                                                              cBit31_0
#define cAf6_upen_pktcap_cap_dat_Shift_01                                                                    0
#define cAf6_upen_pktcap_cap_dat_Mask_02                                                              cBit31_0
#define cAf6_upen_pktcap_cap_dat_Shift_02                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : APS_Acc_Received_Kbyte_Value_Per_Channel
Reg Addr   : 0x22800 - 0x228FF
Reg Formula: 0x22800 + $channelid
    Where  : 
           + $channelid(0-16): Channel ID
Reg Desc   : 
The register provides received Kbyte of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_k12acc_Base                                                                       0x22800
#define cAf6Reg_upen_k12acc(channelid)                                                   (0x22800+(channelid))
#define cAf6Reg_upen_k12acc_WidthVal                                                                        32

/*--------------------------------------
BitField Name: Kbyte_Value
BitField Type: RO
BitField Desc: Received Kbyte value of each channel [23:16]: K1 Byte [15:08]: K2
Byte [07:00]: Extend Kbyte
BitField Bits: [23:00]
--------------------------------------*/
#define cAf6_upen_k12acc_Kbyte_Value_Mask                                                             cBit23_0
#define cAf6_upen_k12acc_Kbyte_Value_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Sdh_Req_Interval_Configuration
Reg Addr   : 0x00003
Reg Formula: 
    Where  : 
Reg Desc   : 
The register is used to configure time interval to generate fake SDH request signal

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_genmon_reqint                                                                     0x00003
#define cAf6Reg_upen_genmon_reqint_WidthVal                                                                 32

/*--------------------------------------
BitField Name: Timer_Enable
BitField Type: RW
BitField Desc: Enable generation of fake request SDH signal
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_genmon_reqint_Timer_Enable_Mask                                                       cBit28
#define cAf6_upen_genmon_reqint_Timer_Enable_Shift                                                          28

/*--------------------------------------
BitField Name: Timer_Value
BitField Type: RW
BitField Desc: Counter of number of clk 155MHz between fake request SDH signal.
This counter is used make a delay interval between 2 consecutive SDH requests
generated by DCC GENERATOR. For example: to create a delay of 125us between SDH
request signals, with a clock 155Mz, the value of counter to be configed to this
field is (125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_upen_genmon_reqint_Timer_Value_Mask                                                      cBit27_0
#define cAf6_upen_genmon_reqint_Timer_Value_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Header_Per_Channel
Reg Addr   : 0x0C100 - 0x0C120
Reg Formula: 0x0C100 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
The register provides data for configuration of VID and MAC DA Header of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_hdr_Base                                                          0x0C100
#define cAf6Reg_upen_dcc_cfg_testgen_hdr(channelid)                                      (0x0C100+(channelid))
#define cAf6Reg_upen_dcc_cfg_testgen_hdr_WidthVal                                                           32

/*--------------------------------------
BitField Name: HDR_VID
BitField Type: RW
BitField Desc: 12b of VID value in header of Generated Packet
BitField Bits: [19:08]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_hdr_HDR_VID_Mask                                                    cBit19_8
#define cAf6_upen_dcc_cfg_testgen_hdr_HDR_VID_Shift                                                          8

/*--------------------------------------
BitField Name: MAC_DA
BitField Type: RW
BitField Desc: Bit [7:0] of MAC DA value [47:0] of generated Packet
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_hdr_MAC_DA_Mask                                                      cBit7_0
#define cAf6_upen_dcc_cfg_testgen_hdr_MAC_DA_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Mode_Per_Channel
Reg Addr   : 0x0C200 - 0x0C220
Reg Formula: 0x0C200 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
The register provides data for configuration of generation mode of each channel ID

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_mod_Base                                                          0x0C200
#define cAf6Reg_upen_dcc_cfg_testgen_mod(channelid)                                      (0x0C200+(channelid))

/*--------------------------------------
BitField Name: gen_payload_mod
BitField Type: RW
BitField Desc: Modes of generated packet payload:
BitField Bits: [31:30]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_mod_gen_payload_mod_Mask                                           cBit31_30
#define cAf6_upen_dcc_cfg_testgen_mod_gen_payload_mod_Shift                                                 30

/*--------------------------------------
BitField Name: gen_len_mode
BitField Type: RW
BitField Desc: Modes of generated packet length :
BitField Bits: [29:28]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_mod_gen_len_mode_Mask                                              cBit29_28
#define cAf6_upen_dcc_cfg_testgen_mod_gen_len_mode_Shift                                                    28

/*--------------------------------------
BitField Name: gen_fix_pattern
BitField Type: RW
BitField Desc: 8b fix pattern payload if mode "fix payload" is used
BitField Bits: [27:20]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_mod_gen_fix_pattern_Mask                                           cBit27_20
#define cAf6_upen_dcc_cfg_testgen_mod_gen_fix_pattern_Shift                                                 20

/*--------------------------------------
BitField Name: gen_number_of_packet
BitField Type: RW
BitField Desc: Number of packets the GEN generates for each channelID in gen
burst mode
BitField Bits: [19:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_mod_gen_number_of_packet_Mask                                       cBit19_0
#define cAf6_upen_dcc_cfg_testgen_mod_gen_number_of_packet_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Global_Gen_Enable_Channel
Reg Addr   : 0x0C000
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration to enable Channel to generate DCC packets

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_enacid                                                            0x0C000

/*--------------------------------------
BitField Name: Channel_enable
BitField Type: RW
BitField Desc: Enable Channel to generate DCC packets. Bit[31:0] <-> Channel
31->0
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_enacid_Channel_enable_Mask                                          cBit31_0
#define cAf6_upen_dcc_cfg_testgen_enacid_Channel_enable_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Global_Gen_Mode
Reg Addr   : 0x0C001
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides global configuration of GEN mode

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_glb_gen_mode                                                      0x0C001

/*--------------------------------------
BitField Name: gen_force_err_len
BitField Type: RW
BitField Desc: Create wrong packet's length fiedl generated packets
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_len_Mask                                    cBit6
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_len_Shift                                       6

/*--------------------------------------
BitField Name: gen_force_err_fcs
BitField Type: RW
BitField Desc: Create wrong FCS field in generated packets
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_fcs_Mask                                    cBit5
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_fcs_Shift                                       5

/*--------------------------------------
BitField Name: gen_force_err_dat
BitField Type: RW
BitField Desc: Create wrong data value in generated packets
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_dat_Mask                                    cBit4
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_dat_Shift                                       4

/*--------------------------------------
BitField Name: gen_force_err_seq
BitField Type: RW
BitField Desc: Create wrong Sequence field in generated packets
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_seq_Mask                                    cBit3
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_seq_Shift                                       3

/*--------------------------------------
BitField Name: gen_force_err_vcg
BitField Type: RW
BitField Desc: Create wrong VCG field in generated packets
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_vcg_Mask                                    cBit2
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_vcg_Shift                                       2

/*--------------------------------------
BitField Name: gen_burst_mod
BitField Type: RW
BitField Desc: Gen each Channel a number of packet as configured in "Gen mode
per channel" resgister
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_burst_mod_Mask                                        cBit1
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_burst_mod_Shift                                           1

/*--------------------------------------
BitField Name: gen_conti_mod
BitField Type: RW
BitField Desc: Gen packet forever without stopping
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_conti_mod_Mask                                        cBit0
#define cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_conti_mod_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Global_Packet_Length
Reg Addr   : 0x0C002
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration min length max length of generated Packets

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_glb_length                                                        0x0C002

/*--------------------------------------
BitField Name: gen_max_length
BitField Type: RW
BitField Desc: Maximum length of generated packet
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_length_gen_max_length_Mask                                     cBit31_16
#define cAf6_upen_dcc_cfg_testgen_glb_length_gen_max_length_Shift                                           16

/*--------------------------------------
BitField Name: gen_min_length
BitField Type: RW
BitField Desc: Minimum length of generated packet, also used for fix length
packets in case fix length mode is used
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_length_gen_min_length_Mask                                      cBit15_0
#define cAf6_upen_dcc_cfg_testgen_glb_length_gen_min_length_Shift                                            0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Config_Test_Gen_Global_Gen_Interval
Reg Addr   : 0x0C003
Reg Formula: 
    Where  : 
Reg Desc   : 
The register provides configuration for packet generating interval

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_dcc_cfg_testgen_glb_gen_interval                                                  0x0C003
#define cAf6Reg_upen_dcc_cfg_testgen_glb_gen_interval_WidthVal                                              32

/*--------------------------------------
BitField Name: Gen_Intv_Enable
BitField Type: RW
BitField Desc: Enable using this interval value
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Enable_Mask                                  cBit28
#define cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Enable_Shift                                      28

/*--------------------------------------
BitField Name: Gen_Intv_Value
BitField Type: RW
BitField Desc: Counter of number of clk 155MHz between packet generation. This
counter is used make a delay interval between 2 consecutive packet generation.
For example: to create a delay of 125us between 2 packet generating, with a
clock 155Mz, the value of counter to be configed to this field is
(125*10^3)ns/(10^3/155)ns = 125*155 = 19375 = 0x4BAF
BitField Bits: [27:00]
--------------------------------------*/
#define cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Value_Mask                                cBit27_0
#define cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Value_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Good_Packet_Counter
Reg Addr   : 0x0E100 - 0x0E120
Reg Formula: 0x0E100 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
Counter of receive good packet

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_godpkt_Base                                                                   0x0E100
#define cAf6Reg_upen_mon_godpkt(channelid)                                               (0x0E100+(channelid))

/*--------------------------------------
BitField Name: dcc_test_mon_good_cnt
BitField Type: WC
BitField Desc: Counter of Receive Good Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_godpkt_dcc_test_mon_good_cnt_Mask                                               cBit31_0
#define cAf6_upen_mon_godpkt_dcc_test_mon_good_cnt_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Error_Data_Packet_Counter
Reg Addr   : 0x0E200 - 0x0E220
Reg Formula: 0x0E200 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
Counter of received packets has error data

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_errpkt_Base                                                                   0x0E200
#define cAf6Reg_upen_mon_errpkt(channelid)                                               (0x0E200+(channelid))

/*--------------------------------------
BitField Name: dcc_test_mon_errdat_cnt
BitField Type: WC
BitField Desc: Counter of Receive Error Data Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_errpkt_dcc_test_mon_errdat_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_errpkt_dcc_test_mon_errdat_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Error_VCG_Packet_Counter
Reg Addr   : 0x0E300 - 0x0E320
Reg Formula: 0x0E300 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
Counter of received packet has wrong VCG value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_errvcg_Base                                                                   0x0E300
#define cAf6Reg_upen_mon_errvcg(channelid)                                               (0x0E300+(channelid))

/*--------------------------------------
BitField Name: dcc_test_mon_errvcg_cnt
BitField Type: WC
BitField Desc: Counter of Receive Error VCG Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_errvcg_dcc_test_mon_errvcg_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_errvcg_dcc_test_mon_errvcg_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Error_SEQ_Packet_Counter
Reg Addr   : 0x0E400 - 0x0E420
Reg Formula: 0x0E400 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
Counter of received packet has wrong Sequence value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_errseq_Base                                                                   0x0E400
#define cAf6Reg_upen_mon_errseq(channelid)                                               (0x0E400+(channelid))

/*--------------------------------------
BitField Name: dcc_test_mon_errseq_cnt
BitField Type: WC
BitField Desc: Counter of Receive Error Sequence Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_errseq_dcc_test_mon_errseq_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_errseq_dcc_test_mon_errseq_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Error_FCS_Packet_Counter
Reg Addr   : 0x0E500 - 0x0E520
Reg Formula: 0x0E500 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
Counter of received packet has wrong FCS value

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_errfcs_Base                                                                   0x0E500
#define cAf6Reg_upen_mon_errfcs(channelid)                                               (0x0E500+(channelid))

/*--------------------------------------
BitField Name: dcc_test_mon_errfcs_cnt
BitField Type: WC
BitField Desc: Counter of Receive Error FCS Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_errfcs_dcc_test_mon_errfcs_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_errfcs_dcc_test_mon_errfcs_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DDC_Test_Mon_Abort_VCG_Packet_Counter
Reg Addr   : 0x0E600 - 0x0E620
Reg Formula: 0x0E600 + $channelid
    Where  : 
           + $channelid(0-31): Channel ID
Reg Desc   : 
Counter of received packet has been abbort due to wrong length information

------------------------------------------------------------------------------*/
#define cAf6Reg_upen_mon_abrpkt_Base                                                                   0x0E600
#define cAf6Reg_upen_mon_abrpkt(channelid)                                               (0x0E600+(channelid))

/*--------------------------------------
BitField Name: dcc_test_mon_abrpkt_cnt
BitField Type: WC
BitField Desc: Counter of Abbort Packet from TEST GEN
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_upen_mon_abrpkt_dcc_test_mon_abrpkt_cnt_Mask                                             cBit31_0
#define cAf6_upen_mon_abrpkt_dcc_test_mon_abrpkt_cnt_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : DCC_OCN2ETH_Debug_Stk_Reg1
Reg Addr   : 0x10008
Reg Formula: 0x10008
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtl_dccpro__upen_txbuf_stk_err                                                0x10008

/*--------------------------------------
BitField Name: ffq_ful
BitField Type: W1C
BitField Desc: Fifo contain packet information is full, some packets will be
lossed
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_af6ces10grtl_dccpro__upen_txbuf_stk_err_ffq_ful_Mask                                        cBit7
#define cAf6_af6ces10grtl_dccpro__upen_txbuf_stk_err_ffq_ful_Shift                                           7


/*------------------------------------------------------------------------------
Reg Name   : DCC_ETH2OCN_Debug_Stk_Reg1
Reg Addr   : 0x10009
Reg Formula: 0x10009
    Where  : 
Reg Desc   : 
The register provides Alarm Related to HDLC Length Error (OCN2ETH Direction)

------------------------------------------------------------------------------*/
#define cAf6Reg_af6ces10grtl_dccpro__upen_rxbuf_stk_err1                                               0x10009

/*--------------------------------------
BitField Name: rambuf_ful
BitField Type: W1C
BitField Desc: Buffer contain data per channelID is full
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_af6ces10grtl_dccpro__upen_rxbuf_stk_err1_rambuf_ful_Mask                                    cBit2
#define cAf6_af6ces10grtl_dccpro__upen_rxbuf_stk_err1_rambuf_ful_Shift                                       2

/*--------------------------------------
BitField Name: ffinf_ful
BitField Type: W1C
BitField Desc: Fifo contain packet information is full, some packets will be
lossed
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_af6ces10grtl_dccpro__upen_rxbuf_stk_err1_ffinf_ful_Mask                                     cBit1
#define cAf6_af6ces10grtl_dccpro__upen_rxbuf_stk_err1_ffinf_ful_Shift                                        1

#endif /* _THA60290021DCCKBYTEREG_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW Module
 * 
 * File        : Tha60290021KbytePwInternal.h
 * 
 * Created Date: Sep 4, 2016
 *
 * Description : Tha60290021 KbytePW Internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021KBYTEPWINTERNAL_H_
#define _THA60290021KBYTEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/pw/AtPwInternal.h"
#include "../../../default/util/ThaBitMask.h"
#include "AtPwCounters.h"
#include "AtPwKbyteChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021PwKByte * Tha60290021PwKByte;

typedef struct tTha60290021PwKByteMethods
    {
    uint32 (*InterruptEnableRegister)(Tha60290021PwKByte self);
    uint32 (*InterruptStatusRegister)(Tha60290021PwKByte self);
    uint32 (*CurrentStatusRegister)(Tha60290021PwKByte self);
    }tTha60290021PwKByteMethods;

typedef struct tTha60290021PwKByte
    {
    tAtPw super;
    const tTha60290021PwKByteMethods * methods;

    /* Private data */
    AtEthPort port;
    AtPwCounters counters;
    AtPwKbyteChannel *kbyteChannels;
    }tTha60290021PwKByte;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPw Tha60290021PwKbyteObjectInit(AtPw self, uint32 pwId, AtModulePw module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021KBYTEPWINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60290021PwActivatorInternal.h
 * 
 * Created Date: Jul 15, 2019
 *
 * Description : 60290021 PW activator representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021PWACTIVATORINTERNAL_H_
#define _THA60290021PWACTIVATORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/pw/activator/Tha60210011PwActivatorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021PwActivator
    {
    tTha60210011PwActivator super;
    }tTha60290021PwActivator;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwActivator Tha60290021PwActivatorObjectInit(ThaPwActivator self, AtModulePw pwModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021PWACTIVATORINTERNAL_H_ */


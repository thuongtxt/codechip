/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW Module
 *
 * File        : Tha60290021PwHdlc.c
 *
 * Created Date: Sep 4, 2016
 *
 * Description : Tha60290021 HdlcPW Implement code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPwCounters.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/pw/counters/AtPwCountersInternal.h"
#include "../../../../generic/common/AtEthVlanTagInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/util/ThaUtil.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../encap/Tha60290021HdlcChannel.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "../man/Tha60290021Device.h"
#include "Tha60290021PwHdlcInternal.h"
#include "Tha60290021ModulePw.h"
#include "Tha60290021DccKbyteReg.h"


/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290021PwHdlc)self)

/* DA*/
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_5_Mask                                                       cBit31_24
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_5_Shift                                                             24
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_4_Mask                                                       cBit23_16
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_4_Shift                                                             16
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_3_Mask                                                        cBit15_8
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_3_Shift                                                              8
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_2_Mask                                                         cBit7_0
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_2_Shift                                                              0
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_1_Mask                                                       cBit31_24
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_1_Shift                                                             24
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_0_Mask                                                       cBit23_16
#define cAf6_upen_dcctxhdr_reg0_MAC_DA_0_Shift                                                             16

/* SA*/
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_5_Mask                                                        cBit15_8
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_5_Shift                                                              8
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_4_Mask                                                         cBit7_0
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_4_Shift                                                              0

#define cAf6_upen_dcctxhdr_reg0_MAC_SA_3_Mask                                                       cBit31_24
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_3_Shift                                                             24
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_2_Mask                                                       cBit23_16
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_2_Shift                                                             16
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_1_Mask                                                        cBit15_8
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_1_Shift                                                              8
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_0_Mask                                                         cBit7_0
#define cAf6_upen_dcctxhdr_reg0_MAC_SA_0_Shift                                                              0

/* Expected DA_MAC */
#define cAf6_upen_dccrxhdr_MAC_DA_5_Mask                                                            cBit15_8
#define cAf6_upen_dccrxhdr_MAC_DA_5_Shift                                                                  8
#define cAf6_upen_dccrxhdr_MAC_DA_4_Mask                                                             cBit7_0
#define cAf6_upen_dccrxhdr_MAC_DA_4_Shift                                                                  0
#define cAf6_upen_dccrxhdr_MAC_DA_3_Mask                                                           cBit31_24
#define cAf6_upen_dccrxhdr_MAC_DA_3_Shift                                                                 24
#define cAf6_upen_dccrxhdr_MAC_DA_2_Mask                                                           cBit23_16
#define cAf6_upen_dccrxhdr_MAC_DA_2_Shift                                                                 16
#define cAf6_upen_dccrxhdr_MAC_DA_1_Mask                                                            cBit15_8
#define cAf6_upen_dccrxhdr_MAC_DA_1_Shift                                                                  8
#define cAf6_upen_dccrxhdr_MAC_DA_0_Mask                                                             cBit7_0
#define cAf6_upen_dccrxhdr_MAC_DA_0_Shift                                                                  0

/*--------------------------- Macros -----------------------------------------*/
#define mAddressWithLocalAddress(self, localAddress) AddressWithLocalAddress((AtPw)self, localAddress)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021PwHdlcMethods m_methods;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtChannelMethods        m_AtChannelOverride;
static tAtPwMethods             m_AtPwOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtPwMethods      *m_AtPwMethods      = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(AtPw self)
    {
    return AtModuleDeviceGet((AtModule) AtChannelModuleGet((AtChannel) self));
    }

static ThaModuleOcn OcnModule(AtPw self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(DeviceGet(self), cThaModuleOcn);
    }

static AtIpCore Core(AtPw self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static uint32 DccChannelId(AtPw self)
    {
    AtChannel circuit = AtPwBoundCircuitGet(self);
    return AtChannelIdGet(circuit);
    }

static uint32 DccLayerChannelOffset(eAtSdhLineDccLayer layer)
    {
    return (layer & cAtSdhLineDccLayerSection) ? 0 : 1;
    }

static uint32 DccPwIdGet(AtHdlcChannel hdlcChannel)
    {
    AtSdhLine line = Tha60290021HdlcDccChannelLineGet(hdlcChannel);
    eAtSdhLineDccLayer layer = Tha60290021HdlcDccChannelLayerGet(hdlcChannel);
    return (uint32) (AtChannelIdGet((AtChannel) line) * 2UL + DccLayerChannelOffset(layer));
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "dcc_pw";
    }

static eAtModulePwRet EthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    eAtModulePwRet ret = AtPwEthDestMacSet(self, destMac);
    ret |= AtPwEthVlanSet(self, cVlan, sVlan);
    return ret;
    }

static uint32 AddressWithLocalAddress(AtPw self, uint32 localAddress)
    {
    return localAddress + ThaModuleOcnSohOverEthBaseAddress(OcnModule(self));
    }

static uint32 DccTxHeaderRegAddress(Tha60290021PwHdlc self)
    {
    return AddressWithLocalAddress((AtPw)self, cAf6Reg_upen_dcctxhdr_reg0_Base + DccChannelId((AtPw)self) * 4UL);
    }

static uint32 DccTxHeaderReg0Address(AtPw self)
    {
    return DccTxHeaderRegAddress((Tha60290021PwHdlc)self);
    }

static uint32 DccTxHeaderReg1Base(AtPw self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_upen_dcctxhdr_reg1_Base + DccChannelId(self) * 4UL);
    }

static uint16 LongReadOnCore(Tha60290021PwHdlc self, uint32 regAddr, uint32 *longRegVal, uint16 bufferLen)
    {
    AtIpCore core = Core((AtPw)self);
    ThaModuleOcn ocnModule = OcnModule((AtPw)self);
    return ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, bufferLen, core);
    }

static uint16 LongWriteOnCore(Tha60290021PwHdlc self, uint32 regAddr, uint32 *longRegVal, uint16 bufferLen)
    {
    AtIpCore core = Core((AtPw)self);
    ThaModuleOcn ocnModule = OcnModule((AtPw)self);
    return ThaModuleOcnSohOverEthLongWriteOnCore(ocnModule, regAddr, longRegVal, bufferLen, core);
    }

static eAtModulePwRet HwEthDestMacSet(AtPw self, uint8 *destMac)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = DccTxHeaderReg0Address(self);

    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    mRegFieldSet(longRegVal[0], cAf6_upen_dcctxhdr_reg0_MAC_DA_0_, destMac[5]);
    mRegFieldSet(longRegVal[0], cAf6_upen_dcctxhdr_reg0_MAC_DA_1_, destMac[4]);

    mRegFieldSet(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_DA_2_, destMac[3]);
    mRegFieldSet(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_DA_3_, destMac[2]);
    mRegFieldSet(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_DA_4_, destMac[1]);
    mRegFieldSet(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_DA_5_, destMac[0]);
    Tha60290021PwHdlcLongWriteOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);

    return cAtOk;
    }

static eAtModulePwRet EthDestMacSet(AtPw self, uint8 *destMac)
    {
    if (destMac == NULL)
        return cAtErrorNullPointer;

    return HwEthDestMacSet(self, destMac);
    }

static eAtModulePwRet HwEthDestMacGet(AtPw self, uint8 *destMac)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = DccTxHeaderReg0Address(self);

    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    destMac[5] = (uint8)mRegField(longRegVal[0], cAf6_upen_dcctxhdr_reg0_MAC_DA_0_);
    destMac[4] = (uint8)mRegField(longRegVal[0], cAf6_upen_dcctxhdr_reg0_MAC_DA_1_);
    destMac[3] = (uint8)mRegField(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_DA_2_);
    destMac[2] = (uint8)mRegField(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_DA_3_);
    destMac[1] = (uint8)mRegField(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_DA_4_);
    destMac[0] = (uint8)mRegField(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_DA_5_);

    return cAtOk;
    }

static eAtModulePwRet EthDestMacGet(AtPw self, uint8 *destMac)
    {
    if (destMac == NULL)
        return cAtErrorNullPointer;

    return HwEthDestMacGet(self, destMac);
    }

static eAtModulePwRet TxSrcMacSet(AtPw self, uint8 *srcMac)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = DccTxHeaderReg0Address(self);

    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    mRegFieldSet(longRegVal[0], cAf6_upen_dcctxhdr_reg0_MAC_SA_4_, srcMac[1]);
    mRegFieldSet(longRegVal[0], cAf6_upen_dcctxhdr_reg0_MAC_SA_5_, srcMac[0]);
    Tha60290021PwHdlcLongWriteOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);

    regAddr = DccTxHeaderReg1Base(self);
    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    mRegFieldSet(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_SA_0_, srcMac[5]);
    mRegFieldSet(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_SA_1_, srcMac[4]);
    mRegFieldSet(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_SA_2_, srcMac[3]);
    mRegFieldSet(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_SA_3_, srcMac[2]);
    Tha60290021PwHdlcLongWriteOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);

    return cAtOk;
    }

static eAtModulePwRet EthSrcMacSet(AtPw self, uint8 *srcMac)
    {
    if (srcMac == NULL)
        return cAtErrorNullPointer;
    return TxSrcMacSet(self, srcMac);
    }

static eAtModulePwRet HwEthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = DccTxHeaderReg0Address(self);

    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    srcMac[1] = (uint8)mRegField(longRegVal[0], cAf6_upen_dcctxhdr_reg0_MAC_SA_4_);
    srcMac[0] = (uint8)mRegField(longRegVal[0], cAf6_upen_dcctxhdr_reg0_MAC_SA_5_);

    regAddr = DccTxHeaderReg1Base(self);
    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    srcMac[2] = (uint8)mRegField(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_SA_3_);
    srcMac[3] = (uint8)mRegField(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_SA_2_);
    srcMac[4] = (uint8)mRegField(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_SA_1_);
    srcMac[5] = (uint8)mRegField(longRegVal[1], cAf6_upen_dcctxhdr_reg0_MAC_SA_0_);

    return cAtOk;
    }

static eAtModulePwRet EthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    if (srcMac)
        return HwEthSrcMacGet(self, srcMac);
    return cAtErrorNullPointer;
    }

static eAtModulePwRet HwEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = DccTxHeaderReg1Base(self);

    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    mRegFieldSet(longRegVal[0], cAf6_upen_dcctxhdr_reg1_VLAN_, AtEthVlanTagToUint32(cVlan));
    Tha60290021PwHdlcLongWriteOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);

    return cAtOk;
    }

static eAtModulePwRet EthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    /* Not Support S-VLAN */
    if (sVlan)
        return cAtErrorModeNotSupport;

    if (!cVlan)
        return cAtErrorModeNotSupport;

    return HwEthVlanSet(self, cVlan);
    }

static tAtEthVlanTag* HwEthVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = DccTxHeaderReg1Base(self);

    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    return AtEthVlanTagFromUint32(mRegField(longRegVal[0], cAf6_upen_dcctxhdr_reg1_VLAN_), cVlan);
    }

static tAtEthVlanTag* EthCVlanGet(AtPw self, tAtEthVlanTag *cVlan)
    {
    if (cVlan == NULL)
        return NULL;

    return HwEthVlanGet(self, cVlan);
    }

static tAtEthVlanTag* EthSVlanGet(AtPw self, tAtEthVlanTag *sVlan)
    {
    AtUnused(self);
    AtUnused(sVlan);
    return NULL;
    }

static const char *ToString(AtObject self)
    {
    static char description[64];
    AtSnprintf(description, sizeof(description) - 1,  "%s", AtChannelTypeString((AtChannel)self) );
    return description;
    }

static eAtModulePwRet EthPortSet(AtPw self, AtEthPort ethPort)
    {
    if (ethPort == NULL)
        return cAtErrorNotApplicable;

    if (ethPort != Tha6029PwDccDefaultSgmiiEthPort(mThis(self)))
        return cAtErrorNotApplicable;

    mThis(self)->port = ethPort;
    return cAtOk;
    }

static AtEthPort EthPortGet(AtPw self)
    {
    return mThis(self)->port;
    }

static AtChannel BoundCircuitGet(AtPw self)
    {
    return mThis(self)->circuit;
    }

static eAtModulePwRet CircuitBind(AtPw self, AtChannel circuit)
    {
    return (AtPwBoundCircuitGet(self) != circuit) ? cAtErrorNotApplicable : cAtOk;
    }

static eAtModulePwRet CircuitUnbind(AtPw self)
    {
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtRet HwDccPwEthTypeSet(Tha60290021PwHdlc self, uint16 ethType)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = AddressWithLocalAddress((AtPw)self, cAf6Reg_upen_dccrxhdr_Base);

    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    AtUtilLongFieldSet(longRegVal, 1, ethType,
                       cAf6_upen_dccrxhdr_ETH_TYP_01_Mask,
                       cAf6_upen_dccrxhdr_ETH_TYP_01_Shift,
                       cAf6_upen_dccrxhdr_ETH_TYP_02_Mask,
                       cAf6_upen_dccrxhdr_ETH_TYP_02_Shift);
    Tha60290021PwHdlcLongWriteOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);

    return cAtOk;
    }

static eAtRet DccPwEthTypeSet(Tha60290021PwHdlc self, uint16 ethType)
    {
    if (ethType == cThaDccPwEthTypeDefault)
        return HwDccPwEthTypeSet(self, ethType);

    return cAtErrorNotApplicable;
    }

static uint16 DccPwEthTypeGet(Tha60290021PwHdlc self)
	{
    AtUnused(self);
    return cThaDccPwEthTypeDefault;
	}

static eAtRet DccPwTypeSet(Tha60290021PwHdlc self, uint8 type)
	{
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = DccTxHeaderReg1Base((AtPw)self);

    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    mRegFieldSet(longRegVal[0], cAf6_upen_dcctxhdr_reg1_TYPE_, type);
    Tha60290021PwHdlcLongWriteOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    return cAtOk;
	}

static uint8 DccPwTypeGet(Tha60290021PwHdlc self)
	{
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = DccTxHeaderReg1Base((AtPw)self);

    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    return (uint8) mRegField(longRegVal[0], cAf6_upen_dcctxhdr_reg1_TYPE_);
	}

static eAtRet DccPwVersionSet(Tha60290021PwHdlc self, uint8 version)
	{
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = DccTxHeaderReg1Base((AtPw)self);

    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    mRegFieldSet(longRegVal[0], cAf6_upen_dcctxhdr_reg1_VERSION_, version);
    Tha60290021PwHdlcLongWriteOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    return cAtOk;
	}

static uint8 DccPwVersionGet(Tha60290021PwHdlc self)
	{
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = DccTxHeaderReg1Base((AtPw)self);

    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longRegVal, cThaLongRegMaxSize);
    return (uint8) mRegField(longRegVal[0], cAf6_upen_dcctxhdr_reg1_VERSION_);
	}

static uint32 DccRxMacCheckRegister(AtPw self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_upen_dccrxmacenb_Base);
    }

static uint32 MacCheckEnableMask(AtPw self)
    {
    return (cBit0 << DccChannelId(self));
    }

static uint32 MacCheckEnableShift(AtPw self)
    {
    return DccChannelId(self);
    }

static eAtRet DccPwMacCheckEnable(Tha60290021PwHdlc self, eBool enable)
    {
    AtPw pw = (AtPw) self;
    uint32 regAddr = DccRxMacCheckRegister(pw);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mFieldIns(&regVal, MacCheckEnableMask(pw), MacCheckEnableShift(pw), mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eBool DccPwMacCheckIsEnabled(Tha60290021PwHdlc self)
    {
    AtPw pw = (AtPw) self;
    uint32 regAddr = DccRxMacCheckRegister(pw);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    return (regVal & MacCheckEnableMask(pw)) ? cAtTrue : cAtFalse;
    }

static uint32 DccRxCVlanCheckRegister(AtPw self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_upen_dccrxcvlenb_Base);
    }

static eAtRet DccPwCVlanCheckEnable(Tha60290021PwHdlc self, eBool enable)
    {
    AtPw pw = (AtPw) self;
    uint32 regAddr = DccRxCVlanCheckRegister(pw);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mFieldIns(&regVal, MacCheckEnableMask(pw), MacCheckEnableShift(pw), mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eBool DccPwCVlanCheckIsEnabled(Tha60290021PwHdlc self)
    {
    AtPw pw = (AtPw) self;
    uint32 regAddr = DccRxCVlanCheckRegister(pw);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    return (regVal & MacCheckEnableMask(pw)) ? cAtTrue : cAtFalse;
    }

static AtEthPort DefaultSgmiiEthPort(Tha60290021PwHdlc self)
    {
    AtModuleEth ethModule = (AtModuleEth) AtDeviceModuleGet(AtChannelDeviceGet((AtChannel) self), cAtModuleEth);
    return Tha60290021ModuleEthSgmiiDccKbytePortGet(ethModule);
    }

static uint32 UpenDccdecAddress(Tha60290021PwHdlc self, uint8 label)
    {
    uint32 regBase = ThaModuleOcnUpenDccdecBase(OcnModule((AtPw)self));
    return mAddressWithLocalAddress(self, regBase + label);
    }

static eAtRet HwDccPwLabelChannelSet(Tha60290021PwHdlc self, uint8 label, uint32 channelId)
    {
    uint32 regAddr = UpenDccdecAddress(self, label);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_dccdec_Mapping_ChannelID_, channelId);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static uint32 HwDccPwLabelChannelGet(Tha60290021PwHdlc self, uint8 label)
    {
    uint32 regAddr = UpenDccdecAddress(self, label);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    return mRegField(regVal, cAf6_upen_dccdec_Mapping_ChannelID_);
    }

static uint8 LabelMaxValue(Tha60290021PwHdlc self)
    {
    AtUnused(self);
    return cBit4_0;
    }

static eBool IsLabelUsedByHdlcChannel(Tha60290021PwHdlc pwForNewLabel, AtHdlcChannel hdlcChannel, uint8 newLabel)
    {
    if (hdlcChannel)
        {
        Tha60290021PwHdlc pw = (Tha60290021PwHdlc)AtChannelBoundPwGet((AtChannel)hdlcChannel);
        uint8 cfgLabel = Tha6029PwDccPwExpectedLabelGet((Tha60290021PwHdlc)pw);
        eBool isTxEnabled = AtChannelTxIsEnabled((AtChannel)hdlcChannel);
        if (pw && (pw != pwForNewLabel) && (cfgLabel == newLabel) && isTxEnabled)
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool IsLabelUsedByLine(Tha60290021PwHdlc self, AtSdhLine line, uint8 label)
    {
    AtHdlcChannel sectionHdlc = AtSdhLineDccChannelGet(line, cAtSdhLineDccLayerSection);
    AtHdlcChannel lineHdlc    = AtSdhLineDccChannelGet(line, cAtSdhLineDccLayerLine);

    return IsLabelUsedByHdlcChannel(self, sectionHdlc, label) || IsLabelUsedByHdlcChannel(self, lineHdlc, label);
    }

static eBool IsExpectedLabelIsUsed(Tha60290021PwHdlc self, uint8 label)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    uint8 numLines = Tha60290021ModuleSdhNumUseableFaceplateLines(sdhModule);
    uint8 line_i;
    eBool ret = cAtFalse;

    for (line_i = 0; line_i < numLines; line_i++)
        {
        AtSdhLine line = Tha60290021ModuleSdhFaceplateLineGet(sdhModule, line_i);
        if (line)
            ret = ret || Tha6029DccPwIsExpectedLabelUsedByLine(self, line, label);
        }

    return ret;
    }

static eAtRet DccPwExpectedLabelSet(Tha60290021PwHdlc self, uint8 label)
    {
    eAtRet ret = cAtOk;
    AtChannel hdlc;

    if (label > mMethodsGet(self)->LabelMaxValue(self))
        return cAtErrorOutOfRangParm;

    if (Tha6029PwDccPwExpectedLabelGet(self) == label)
        return cAtOk;

    if (Tha60290021DccPwIsExpectedLabelIsUsed(self, label))
        return cAtErrorResourceBusy;

    /* Need to disable current lookup */
    hdlc = AtPwBoundCircuitGet((AtPw)self);
    if (self->expectedLabel != cInvalidUint8)
        ret |= Tha60290021HdlcChannelTxDccHwEnable((Tha60290021HdlcChannel)hdlc, cAtFalse);

    ret = Tha60290021PwHdlcHwDccPwLabelChannelSet(self, label, AtChannelIdGet((AtChannel)self));
    if (ret == cAtOk)
        self->expectedLabel = label;

    /* And enable lookup */
    if (AtChannelTxIsEnabled(hdlc))
        ret |= Tha60290021HdlcChannelTxDccHwEnable((Tha60290021HdlcChannel)hdlc, cAtTrue);

    return ret;
    }

static uint8 DccPwExpectedLabelGet(Tha60290021PwHdlc self)
    {
    return self->expectedLabel;
    }

static uint32 CounterBaseAddress(Tha60290021PwHdlc self, uint16 counterType, eBool r2c)
    {
    AtUnused(self);
    AtUnused(r2c);

    switch (counterType)
        {
        case cAtPwCounterTypeTxPackets:      return cAf6Reg_upen_dcceop2sgm_pcid_Base;
        case cAtPwCounterTypeTxPayloadBytes: return cAf6Reg_upen_dccbyt2sgm_pcid_Base;
        case cAtPwCounterTypeRxPackets:      return cAf6Reg_upen_dcc_sokpkt_pcid_Base;
        case cAtPwCounterTypeRxPayloadBytes: return cAf6Reg_upen_dcc_sokbyt_pcid_Base;
        default:                             return cInvalidUint32;
        }
    }

static uint32 HwCounterRead2Clear(AtChannel self, uint32 counterBaseAddress, eBool r2c)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, counterBaseAddress + AtChannelIdGet(self));
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    if (r2c)
        mChannelHwWrite(self, regAddr, 0x0, cAtModuleSdh);

    return regVal;
    }

static uint32 CounterReadToClear(AtChannel self, uint16 counterType, eBool r2c)
    {
    uint32 counterAddress = mMethodsGet(mThis(self))->CounterBaseAddress(mThis(self), counterType, r2c);
    if (counterAddress == cInvalidUint32)
        return 0x0;

    return HwCounterRead2Clear(self, counterAddress, r2c);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (AtChannelCounterIsSupported(self, counterType))
        return CounterReadToClear(self, counterType, cAtFalse);
    return 0x0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (AtChannelCounterIsSupported(self, counterType))
        return CounterReadToClear(self, counterType, cAtTrue);
    return 0x0;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    uint32 counterAddress = mMethodsGet(mThis(self))->CounterBaseAddress(mThis(self), counterType, cAtTrue);
    if (counterAddress == cInvalidUint32)
        return cAtFalse;

    return cAtTrue;
    }

static AtPwCounters CountersGet(AtChannel self)
    {
    if (mThis(self)->counters == NULL)
        mThis(self)->counters = AtPwCountersNew();

    return mThis(self)->counters;
    }

static eAtRet AllCountersReadToClear(AtChannel self, void *pAllCounters, eBool clear)
    {
    AtPwCounters counters = CountersGet(self);

    if (counters == NULL)
        return cAtOk;

    AtPwCountersInit(counters);
    AtPwCountersTxPacketsSet(counters, CounterReadToClear(self, cAtPwCounterTypeTxPackets, clear));
    AtPwCountersTxPayloadBytesSet(counters, CounterReadToClear(self, cAtPwCounterTypeTxPayloadBytes, clear));
    AtPwCountersRxPacketsSet(counters, CounterReadToClear(self, cAtPwCounterTypeRxPackets, clear));
    AtPwCountersRxPayloadBytesSet(counters, CounterReadToClear(self, cAtPwCounterTypeRxPayloadBytes, clear));
    AtPwCountersRxDiscardedPacketsSet(counters, CounterReadToClear(self, cAtPwCounterTypeRxDiscardedPackets, clear));
    AtPwCountersRxLostPacketsSet(counters, CounterReadToClear(self, cAtPwCounterTypeRxLostPackets, clear));

    if (pAllCounters)
        *((AtPwCounters *)pAllCounters) = counters;

    return cAtOk;
    }

static eAtRet AllCountersGet(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtFalse);
    }

static eAtRet AllCountersClear(AtChannel self, void *pAllCounters)
    {
    return AllCountersReadToClear(self, pAllCounters, cAtTrue);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return cTha6029DccPwAlarmTypePktDiscards;
    }

static uint32 ChannelShift(uint32 channelId)
    {
    return (channelId);
    }

static uint32 ChannelMask(uint32 channelId)
    {
    return (cBit0 << channelId);
    }

static uint32 InterruptEnableAddress(AtChannel self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_upen_rxdcc_intenb_r0_Base);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    if (defectMask & cTha6029DccPwAlarmTypePktDiscards)
        {
        uint32 channelId = DccChannelId((AtPw) self);
        uint32 regAddr = InterruptEnableAddress(self);
        uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
        mFieldIns(&regVal,
                  ChannelMask(channelId),
                  ChannelShift(channelId),
                  (enableMask & cTha6029DccPwAlarmTypePktDiscards) ? 1 : 0);
        mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
        }

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 channelId = DccChannelId((AtPw) self);
    uint32 regAddr = InterruptEnableAddress(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 alarmMask = 0;

    if (regVal & ChannelMask(channelId))
        alarmMask |= cTha6029DccPwAlarmTypePktDiscards;

    return alarmMask;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    return AtChannelInterruptMaskGet(self) ? cAtTrue : cAtFalse;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    uint32 intrMask = AtChannelInterruptMaskGet(self);
    uint32 changedAlarms = AtChannelDefectInterruptClear(self);
    uint32 currentStatus = AtChannelDefectGet(self);

    AtUnused(offset);

    AtChannelAllAlarmListenersCall(self, (changedAlarms & intrMask), (currentStatus & intrMask));
    return currentStatus;
    }

static eBool IsSimulation(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceIsSimulated(device);
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_upen_dcc_rx_glb_sta_Base);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 fieldVal;
    uint32 channelId = DccChannelId((AtPw) self);
    uint32 mask = cBit0 << channelId;

    Tha60290021PwHdlcLongReadOnCore(mThis(self), regAddr, longReg, cThaLongRegMaxSize);

    fieldVal = mLongField(longReg, 0, cAf6_upen_dcc_rx_glb_sta_sta_dcc_channel_disable_);
    if (fieldVal & mask)
        return cTha6029DccPwAlarmTypePktDiscards;

    return 0;
    }

static uint32 HwInterruptGet(AtChannel self, eBool r2c)
    {
    uint32 channelId = DccChannelId((AtPw) self);
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_upen_int_rxdcc0_stt_Base);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 alarmMask = 0;

    if (regVal & ChannelMask(channelId))
        {
        alarmMask |= cTha6029DccPwAlarmTypePktDiscards;
        /* Clear Interrupt Status */
        if (r2c)
            mChannelHwWrite(self, regAddr, IsSimulation(self) ? 0 : ChannelMask(channelId), cAtModuleSdh);
        }

    return alarmMask;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return HwInterruptGet(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return HwInterruptGet(self, cAtTrue);
    }

static eAtModulePwRet PsnSet(AtPw self, AtPwPsn psn)
    {
    AtUnused(psn);
    AtUnused(self);
    return cAtErrorModeNotSupport;
    }

static eAtRet Init(AtChannel self)
    {
    AtChannelBindToPseudowire(mThis(self)->circuit, (AtPw) self);
    Tha6029PwDccPwEthTypeSet(mThis(self), cThaDccPwEthTypeDefault);
    Tha6029PwDccMacCheckEnable(mThis(self), cAtFalse);
    Tha6029PwDccCVlanCheckEnable(mThis(self), cAtFalse);
    Tha6029PwDccPwVersionSet(mThis(self), 0x1);
    Tha6029PwDccPwTypeSet(mThis(self), 0x41);
    AtChannelInterruptMaskSet(self, AtChannelSupportedInterruptMasks(self), 0);

    return AtPwEthPortSet((AtPw)self, Tha6029PwDccDefaultSgmiiEthPort(mThis(self)));
    }

static void Delete(AtObject self)
    {
    AtChannelBindToPseudowire(mThis(self)->circuit, NULL);
    AtObjectDelete((AtObject)(mThis(self)->counters));
    mThis(self)->counters = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290021PwHdlc object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(circuit);
    mEncodeObjectDescription(port);
    mEncodeUInt(expectedLabel);
    mEncodeNone(counters);
    }

static eAtModulePwRet LopsSetThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(numPackets);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet LopsClearThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(numPackets);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet PayloadSizeSet(AtPw self, uint16 sizeInBytes)
    {
    AtUnused(sizeInBytes);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet PrioritySet(AtPw self, uint8 priority)
    {
    AtUnused(priority);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet ReorderingEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferSizeSet(AtPw self, uint32 microseconds)
    {
    AtUnused(microseconds);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferSizeInPacketSet(AtPw self, uint16 numPackets)
    {
    AtUnused(numPackets);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferDelaySet(AtPw self, uint32 microseconds)
    {
    AtUnused(microseconds);
    AtUnused(self);

    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferDelayInPacketSet(AtPw self, uint16 numPackets)
    {
    AtUnused(numPackets);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);

    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpTxPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    AtUnused(payloadType);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpExpectedPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    AtUnused(payloadType);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpPayloadTypeCompare(AtPw self, eBool enableCompare)
    {
    AtUnused(enableCompare);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpPayloadTypeSet(AtPw self, uint8 payloadType)
    {
    AtUnused(payloadType);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpTxSsrcSet(AtPw self, uint32 ssrc)
    {
    AtUnused(ssrc);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpExpectedSsrcSet(AtPw self, uint32 ssrc)
    {
    AtUnused(ssrc);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpSsrcCompare(AtPw self, eBool enableCompare)
    {
    AtUnused(enableCompare);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpSsrcSet(AtPw self, uint32 ssrc)
    {
    AtUnused(ssrc);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RtpTimeStampModeSet(AtPw self, eAtPwRtpTimeStampMode timeStampMode)
    {
    AtUnused(timeStampMode);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet SuppressEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwAutoTxLBitEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwAutoRxLBitEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwAutoRBitEnable(AtPw self, eBool enable)
    {
    AtUnused(enable);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwSequenceModeSet(AtPw self, eAtPwCwSequenceMode sequenceMode)
    {
    AtUnused(sequenceMode);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwLengthModeSet(AtPw self, eAtPwCwLengthMode lengthMode)
    {
    AtUnused(lengthMode);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet CwPktReplaceModeSet(AtPw self, eAtPwPktReplaceMode pktReplaceMode)
    {
    AtUnused(pktReplaceMode);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet EthExpectedCVlanSet(AtPw self, tAtEthVlanTag *cVlan)
    {
    AtUnused(cVlan);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet EthExpectedSVlanSet(AtPw self, tAtEthVlanTag *sVlan)
    {
    AtUnused(sVlan);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_us, uint32 jitterDelay_us, uint16 payloadSize)
    {
    AtUnused(payloadSize);
    AtUnused(jitterDelay_us);
    AtUnused(jitterBufferSize_us);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferInPacketAndPayloadSizeSet(AtPw self, uint32 jitterBufferSize_pkt, uint32 jitterDelay_pkt, uint16 payloadSize)
    {
    AtUnused(payloadSize);
    AtUnused(jitterDelay_pkt);
    AtUnused(jitterBufferSize_pkt);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet IdleCodeSet(AtPw self, uint8 idleCode)
    {
    AtUnused(idleCode);
    AtUnused(self);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet MissingPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet ExcessivePacketLossRateDefectThresholdSet(AtPw self, uint32 numPackets, uint32 timeInSecond)
    {
    AtUnused(self);
    AtUnused(numPackets);
    AtUnused(timeInSecond);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet StrayPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet MalformedPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet RemotePacketLossDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferOverrunDefectThresholdSet(AtPw self, uint32 numOverrunEvent)
    {
    AtUnused(self);
    AtUnused(numOverrunEvent);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet JitterBufferUnderrunDefectThresholdSet(AtPw self, uint32 numUnderrunEvent)
    {
    AtUnused(self);
    AtUnused(numUnderrunEvent);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet MisConnectionDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorNotApplicable;
    }

static AtPwGroup ApsGroupGet(AtPw self)
    {
    AtUnused(self);
    return NULL;
    }

static AtPwGroup HsGroupGet(AtPw self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtModulePwRet BackupPsnSet(AtPw self, AtPwPsn backupPsn)
    {
    AtUnused(self);
    AtUnused(backupPsn);
    return cAtErrorNotApplicable;
    }

static AtPwPsn BackupPsnGet(AtPw self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtModulePwRet BackupEthHeaderSet(AtPw self, uint8 *destMac, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    AtUnused(self);
    AtUnused(destMac);
    AtUnused(cVlan);
    AtUnused(sVlan);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet BackupEthDestMacSet(AtPw self, uint8 *destMac)
    {
    AtUnused(self);
    AtUnused(destMac);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet BackupEthSrcMacSet(AtPw self, uint8 *srcMac)
    {

    AtUnused(self);
    AtUnused(srcMac);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet BackupEthSrcMacGet(AtPw self, uint8 *srcMac)
    {
    AtUnused(self);
    AtUnused(srcMac);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet BackupEthDestMacGet(AtPw self, uint8 *destMac)
    {
    AtUnused(self);
    AtUnused(destMac);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet BackupEthVlanSet(AtPw self, const tAtEthVlanTag *cVlan, const tAtEthVlanTag *sVlan)
    {
    AtUnused(self);
    AtUnused(cVlan);
    AtUnused(sVlan);
    return cAtErrorNotApplicable;
    }

static eAtRet EthPortQueueSet(AtPw self, uint8 queueId)
    {
    AtUnused(self);
    AtUnused(queueId);
    return cAtErrorNotApplicable;
    }

static eAtRet PtchInsertionModeSet(AtPw self, eAtPtchMode insertMode)
    {
    AtUnused(self);
    AtUnused(insertMode);
    return cAtErrorNotApplicable;
    }

static eAtModulePwRet TxActiveForce(AtPw self, eBool active)
    {
    AtUnused(self);
    AtUnused(active);
    return cAtErrorNotApplicable;
    }

static eBool CwLengthModeIsSupported(AtPw self, eAtPwCwLengthMode lengthMode)
    {
    AtUnused(self);
    AtUnused(lengthMode);
    return cAtFalse;
    }

static eBool CwAutoRxLBitCanEnable(AtPw self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtFalse;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return enable;
    }

static eBool HasInterruptV2(Tha60290021PwHdlc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void MethodsInit(AtPwHdlc self)
    {
	Tha60290021PwHdlc dccPw = (Tha60290021PwHdlc)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, DccPwEthTypeSet);
        mMethodOverride(m_methods, DccPwEthTypeGet);
        mMethodOverride(m_methods, DccPwTypeSet);
        mMethodOverride(m_methods, DccPwTypeGet);
        mMethodOverride(m_methods, DccPwVersionSet);
        mMethodOverride(m_methods, DccPwVersionGet);
        mMethodOverride(m_methods, DccPwMacCheckEnable);
        mMethodOverride(m_methods, DccPwMacCheckIsEnabled);
        mMethodOverride(m_methods, DccPwCVlanCheckEnable);
        mMethodOverride(m_methods, DccPwCVlanCheckIsEnabled);
        mMethodOverride(m_methods, DefaultSgmiiEthPort);
        mMethodOverride(m_methods, DccPwExpectedLabelSet);
        mMethodOverride(m_methods, DccPwExpectedLabelGet);
        mMethodOverride(m_methods, LabelMaxValue);
        mMethodOverride(m_methods, IsExpectedLabelIsUsed);
        mMethodOverride(m_methods, LongReadOnCore);
        mMethodOverride(m_methods, LongWriteOnCore);
        mMethodOverride(m_methods, DccTxHeaderRegAddress);
        mMethodOverride(m_methods, HwDccPwLabelChannelSet);
        mMethodOverride(m_methods, HwDccPwLabelChannelGet);
        mMethodOverride(m_methods, CounterBaseAddress);
        mMethodOverride(m_methods, HasInterruptV2);
        }

    mMethodsSet(dccPw, &m_methods);
    }

static void OverrideAtPw(AtPwHdlc self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, EthHeaderSet);
        mMethodOverride(m_AtPwOverride, EthDestMacSet);
        mMethodOverride(m_AtPwOverride, EthDestMacGet);
        mMethodOverride(m_AtPwOverride, EthSrcMacSet);
        mMethodOverride(m_AtPwOverride, EthSrcMacGet);
        mMethodOverride(m_AtPwOverride, EthVlanSet);
        mMethodOverride(m_AtPwOverride, EthCVlanGet);
        mMethodOverride(m_AtPwOverride, EthSVlanGet);
        mMethodOverride(m_AtPwOverride, BoundCircuitGet);
        mMethodOverride(m_AtPwOverride, CircuitBind);
        mMethodOverride(m_AtPwOverride, CircuitUnbind);
        mMethodOverride(m_AtPwOverride, EthPortSet);
        mMethodOverride(m_AtPwOverride, EthPortGet);
        mMethodOverride(m_AtPwOverride, PsnSet);
        mMethodOverride(m_AtPwOverride, LopsSetThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdSet);
        mMethodOverride(m_AtPwOverride, PayloadSizeSet);
        mMethodOverride(m_AtPwOverride, PrioritySet);
        mMethodOverride(m_AtPwOverride, ReorderingEnable);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeSet);
        mMethodOverride(m_AtPwOverride, JitterBufferSizeInPacketSet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelaySet);
        mMethodOverride(m_AtPwOverride, JitterBufferDelayInPacketSet);
        mMethodOverride(m_AtPwOverride, RtpEnable);
        mMethodOverride(m_AtPwOverride, RtpTxPayloadTypeSet);
        mMethodOverride(m_AtPwOverride, RtpExpectedPayloadTypeSet);
        mMethodOverride(m_AtPwOverride, RtpPayloadTypeCompare);
        mMethodOverride(m_AtPwOverride, RtpPayloadTypeSet);
        mMethodOverride(m_AtPwOverride, RtpTxSsrcSet);
        mMethodOverride(m_AtPwOverride, RtpExpectedSsrcSet);
        mMethodOverride(m_AtPwOverride, RtpSsrcCompare);
        mMethodOverride(m_AtPwOverride, RtpSsrcSet);
        mMethodOverride(m_AtPwOverride, RtpTimeStampModeSet);
        mMethodOverride(m_AtPwOverride, SuppressEnable);
        mMethodOverride(m_AtPwOverride, CwEnable);
        mMethodOverride(m_AtPwOverride, CwAutoTxLBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitEnable);
        mMethodOverride(m_AtPwOverride, CwAutoRBitEnable);
        mMethodOverride(m_AtPwOverride, CwSequenceModeSet);
        mMethodOverride(m_AtPwOverride, CwLengthModeSet);
        mMethodOverride(m_AtPwOverride, CwPktReplaceModeSet);
        mMethodOverride(m_AtPwOverride, EthExpectedCVlanSet);
        mMethodOverride(m_AtPwOverride, EthExpectedSVlanSet);
        mMethodOverride(m_AtPwOverride, JitterBufferAndPayloadSizeSet);
        mMethodOverride(m_AtPwOverride, JitterBufferInPacketAndPayloadSizeSet);
        mMethodOverride(m_AtPwOverride, IdleCodeSet);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, ExcessivePacketLossRateDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, StrayPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MalformedPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, RemotePacketLossDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, JitterBufferOverrunDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, JitterBufferUnderrunDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MisConnectionDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, ApsGroupGet);
        mMethodOverride(m_AtPwOverride, HsGroupGet);
        mMethodOverride(m_AtPwOverride, BackupPsnSet);
        mMethodOverride(m_AtPwOverride, BackupPsnGet);
        mMethodOverride(m_AtPwOverride, BackupEthHeaderSet);
        mMethodOverride(m_AtPwOverride, BackupEthDestMacSet);
        mMethodOverride(m_AtPwOverride, BackupEthSrcMacSet);
        mMethodOverride(m_AtPwOverride, BackupEthSrcMacGet);
        mMethodOverride(m_AtPwOverride, BackupEthDestMacGet);
        mMethodOverride(m_AtPwOverride, BackupEthVlanSet);
        mMethodOverride(m_AtPwOverride, EthPortQueueSet);
        mMethodOverride(m_AtPwOverride, PtchInsertionModeSet);
        mMethodOverride(m_AtPwOverride, TxActiveForce);
        mMethodOverride(m_AtPwOverride, CwLengthModeIsSupported);
        mMethodOverride(m_AtPwOverride, CwAutoRxLBitCanEnable);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideAtChannel(AtPwHdlc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, AllCountersGet);
        mMethodOverride(m_AtChannelOverride, AllCountersClear);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtPwHdlc self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtPwHdlc self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtPw(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PwHdlc);
    }

AtPwHdlc Tha60290021PwDccObjectInit(AtPwHdlc self, AtHdlcChannel dcc, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPwHdlcObjectInit(self, DccPwIdGet(dcc), module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->circuit = (AtChannel) dcc;
    mThis(self)->expectedLabel = cInvalidUint8;

    return self;
    }

AtPwHdlc Tha60290021PwDccNew(AtHdlcChannel dcc, AtModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwHdlc newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021PwDccObjectInit(newChannel, dcc, module);
    }

eAtRet Tha6029PwDccPwEthTypeSet(Tha60290021PwHdlc self, uint16 ethType)
    {
    if (self)
        return mMethodsGet(self)->DccPwEthTypeSet(self, ethType);
    return cAtErrorNullPointer;
    }

uint16 Tha6029PwDccPwEthTypeGet(Tha60290021PwHdlc self)
    {
    if (self)
        return mMethodsGet(self)->DccPwEthTypeGet(self);
    return 0;
    }

eAtRet Tha6029PwDccPwTypeSet(Tha60290021PwHdlc self, uint8 type)
    {
    if (self)
        return mMethodsGet(self)->DccPwTypeSet(self, type);
    return cAtErrorNullPointer;
    }

uint8 Tha6029PwDccPwTypeGet(Tha60290021PwHdlc self)
    {
    if (self)
        return mMethodsGet(self)->DccPwTypeGet(self);
    return 0;
    }

eAtRet Tha6029PwDccPwVersionSet(Tha60290021PwHdlc self, uint8 version)
    {
    if (self)
        return mMethodsGet(self)->DccPwVersionSet(self, version);
    return cAtErrorNullPointer;
    }

uint8 Tha6029PwDccPwVersionGet(Tha60290021PwHdlc self)
    {
    if (self)
        return mMethodsGet(self)->DccPwVersionGet(self);
    return 0;
    }

eAtRet Tha6029PwDccMacCheckEnable(Tha60290021PwHdlc self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->DccPwMacCheckEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha6029PwDccMacCheckIsEnabled(Tha60290021PwHdlc self)
    {
    if (self)
        return mMethodsGet(self)->DccPwMacCheckIsEnabled(self);
    return cAtFalse;
    }

eAtRet Tha6029PwDccCVlanCheckEnable(Tha60290021PwHdlc self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->DccPwCVlanCheckEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool Tha6029PwDccCVlanCheckIsEnabled(Tha60290021PwHdlc self)
    {
    if (self)
        return mMethodsGet(self)->DccPwCVlanCheckIsEnabled(self);
    return cAtFalse;
    }

AtEthPort Tha6029PwDccDefaultSgmiiEthPort(Tha60290021PwHdlc self)
    {
    if (self)
        return mMethodsGet(self)->DefaultSgmiiEthPort(self);
    return NULL;
    }

eAtRet Tha6029PwDccPwExpectedLabelSet(Tha60290021PwHdlc self, uint8 label)
    {
    if (self)
        return mMethodsGet(self)->DccPwExpectedLabelSet(self, label);
    return cAtErrorNullPointer;
    }

uint8 Tha6029PwDccPwExpectedLabelGet(Tha60290021PwHdlc self)
    {
    if (self)
        return mMethodsGet(self)->DccPwExpectedLabelGet(self);
    return cInvalidUint8;
    }

uint32 Tha60290021PwHdlcDccTxHeaderRegAddress(Tha60290021PwHdlc self)
    {
    if (self)
        return mMethodsGet(self)->DccTxHeaderRegAddress(self);
    return cInvalidUint32;
    }

uint16 Tha60290021PwHdlcLongReadOnCore(Tha60290021PwHdlc self, uint32 regAddr, uint32 *longRegVal, uint16 bufferLen)
    {
    if (self)
        return mMethodsGet(self)->LongReadOnCore(self, regAddr, longRegVal, bufferLen);
    return 0;
    }

uint16 Tha60290021PwHdlcLongWriteOnCore(Tha60290021PwHdlc self, uint32 regAddr, uint32 *longRegVal, uint16 bufferLen)
    {
    if (self)
        return mMethodsGet(self)->LongWriteOnCore(self, regAddr, longRegVal, bufferLen);
    return 0;
    }

uint32 Tha60290021DccChannelId(AtPw self)
    {
    return DccChannelId(self);
    }

uint32 Tha60290021DccAddressWithLocalAddress(AtPw self, uint32 localAddress)
    {
    return AddressWithLocalAddress(self, localAddress);
    }

eAtRet Tha60290021PwHdlcHwDccPwLabelChannelSet(Tha60290021PwHdlc self, uint8 label, uint32 channelId)
    {
    if (self)
        return mMethodsGet(self)->HwDccPwLabelChannelSet(self, label, channelId);
    return cAtErrorNullPointer;
    }

uint32 Tha60290021PwHdlcHwDccPwLabelChannelGet(Tha60290021PwHdlc self, uint8 label)
    {
    if (self)
        return mMethodsGet(self)->HwDccPwLabelChannelGet(self, label);
    return cInvalidUint32;
    }

eBool Tha60290021DccPwIsExpectedLabelIsUsed(Tha60290021PwHdlc self, uint8 label)
    {
    if (self)
        return mMethodsGet(self)->IsExpectedLabelIsUsed(self, label);

    return cAtFalse;
    }

eBool Tha6029DccPwIsExpectedLabelUsedByLine(Tha60290021PwHdlc pwForNewLabel, AtSdhLine line, uint8 newLabel)
    {
    return IsLabelUsedByLine(pwForNewLabel, line, newLabel);
    }

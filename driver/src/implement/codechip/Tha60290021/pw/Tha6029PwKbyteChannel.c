/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha6029KbyteChannel.c
 *
 * Created Date: Jun 8, 2018
 *
 * Description : APS/K-byte channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "Tha60290021PwKbyteInternal.h"
#include "Tha60290021ModulePw.h"
#include "Tha60290021DccKbyteReg.h"
#include "Tha6029PwKbyteChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha6029PwKbyteChannel)self)

#define cAf6_upen_k12pro_K1Val_Mask        cBit31_24
#define cAf6_upen_k12pro_K1Val_Shift              24
#define cAf6_upen_k12pro_K2Val_Mask        cBit23_16
#define cAf6_upen_k12pro_K2Val_Shift              16
#define cAf6_upen_k12pro_EK1Val_Mask          cBit13
#define cAf6_upen_k12pro_EK1Val_Shift             13
#define cAf6_upen_k12pro_EK2Val_Mask          cBit12
#define cAf6_upen_k12pro_EK2Val_Shift             12

#define cAf6_upen_k12acc_K1Val_Mask        cBit23_16
#define cAf6_upen_k12acc_K1Val_Shift              16
#define cAf6_upen_k12acc_K2Val_Mask         cBit15_8
#define cAf6_upen_k12acc_K2Val_Shift               8
#define cAf6_upen_k12acc_EK1Val_Mask           cBit5
#define cAf6_upen_k12acc_EK1Val_Shift              5
#define cAf6_upen_k12acc_EK2Val_Mask           cBit4
#define cAf6_upen_k12acc_EK2Val_Shift              4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6029PwKbyteChannelMethods m_methods;

/* Override */
static tAtChannelMethods        m_AtChannelOverride;
static tAtPwKbyteChannelMethods m_AtPwKbyteChannelOverride;

/* Save super implementation */
static const tAtChannelMethods          *m_AtChannelMethods = NULL;
static const tAtPwKbyteChannelMethods   *m_AtPwKbyteChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static uint32 EgressPortIdGet(AtPwKbyteChannel self);

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn OcnModule(AtPwKbyteChannel self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);
    }

static AtModuleSdh SdhModule(AtChannel self)
    {
    return (AtModuleSdh) AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModuleSdh);
    }

static uint32 BaseAddress(AtPwKbyteChannel self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self));
    }

static eAtRet LineMappingDefault(AtChannel self)
    {
    AtSdhLine faceplateLine = Tha60290021ModuleSdhFaceplateLineGet(SdhModule(self), (uint8)AtChannelIdGet(self));
    return AtPwKbyteChannelSdhLineSet((AtPwKbyteChannel)self, faceplateLine);
    }

static eAtRet HwInit(AtChannel self)
    {
    uint32 regAddr = cAf6Reg_upen_cfg_gencid_Base + BaseAddress((AtPwKbyteChannel)self);
    uint32 regVal = cAf6_upen_cfg_gencid_Init_Enable_Mask;
    static const uint32 cKByteRstVal = 0;
    uint32 channelId = AtChannelIdGet(self);

    mChannelHwWrite(self, regAddr, 0, cAtModulePw);
    AtOsalUSleep(5000);
    mRegFieldSet(regVal, cAf6_upen_cfg_gencid_Init_ChannelId_, channelId);
    mRegFieldSet(regVal, cAf6_upen_cfg_gencid_Init_KbyteVal_, cKByteRstVal);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);

    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    AtPwKbyteChannel channel = (AtPwKbyteChannel)self;

    ret  = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    ret |= LineMappingDefault(self);
    ret |= AtPwKbyteChannelTxLabelSet(channel, (uint16)AtChannelIdGet(self));
    ret |= AtPwKbyteChannelExpectedLabelSet(channel, (uint16)AtChannelIdGet(self));
    ret |= AtPwKbyteChannelAlarmSuppressionEnable(channel, cAtTrue);
    ret |= HwInit(self);

    return ret;
    }

static eAtRet HwMappingLineSet(AtPwKbyteChannel self, uint8 lineId)
    {
    uint32 regAddr, regVal;
    uint32 channelId = AtChannelIdGet((AtChannel)self);

    /* OCN2ETH, addressed by input port ID which contains the value of output K-byte channel ID */
    regAddr = cAf6Reg_upen_cid2pid_Base + BaseAddress(self) + lineId;
    regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    mRegFieldSet(regVal, cAf6_upen_cid2pid_channelId_, channelId);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);

    /* ETH2OCN, addressed by input K-byte channel ID which contains the value of output port ID */
    regAddr = cAf6Reg_upen_k12rx_chmap_Base + BaseAddress(self) + channelId;
    regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    mRegFieldSet(regVal, cAf6_upen_k12rx_chmap_PortID_, lineId);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);

    return cAtOk;
    }

static eAtRet SdhLineSet(AtPwKbyteChannel self, AtSdhLine line)
    {
    uint32 defaultId = AtChannelIdGet((AtChannel)self);

    if (line)
        HwMappingLineSet(self, (uint8)AtChannelIdGet((AtChannel)line));
    else
        HwMappingLineSet(self, (uint8)defaultId);

    return m_AtPwKbyteChannelMethods->SdhLineSet(self, line);
    }

static eAtRet HwChannelEnable(AtPwKbyteChannel self, eBool enable, uint32 regAddr)
    {
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    uint32 channelId = AtChannelIdGet((AtChannel)self);
    uint32 enableMask = cBit0 << channelId;
    uint32 enableShift = channelId;
    mRegFieldSet(regVal, enable, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);
    return cAtOk;
    }

static eBool HwChannelIsEnabled(AtPwKbyteChannel self, uint32 regAddr)
    {
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    uint32 channelId = AtChannelIdGet((AtChannel)self);
    uint32 enableMask = cBit0 << channelId;
    return (regVal & enableMask) ? cAtTrue : cAtFalse;
    }

static eAtRet ValidationEnable(AtPwKbyteChannel self, eBool enable)
    {
    return HwChannelEnable(self, enable, BaseAddress(self) + cAf6Reg_upen_cfgenacid_Base);
    }

static eBool ValidationIsEnabled(AtPwKbyteChannel self)
    {
    return HwChannelIsEnabled(self, BaseAddress(self) + cAf6Reg_upen_cfgenacid_Base);
    }

static eAtRet AlarmSuppressionEnable(AtPwKbyteChannel self, eBool enable)
    {
    return Tha60290021ModuleOcnFaceplateLineKbyteTxAlarmSuppress(OcnModule(self), EgressPortIdGet(self), enable);
    }

static eBool AlarmSuppressionIsEnabled(AtPwKbyteChannel self)
    {
    return Tha60290021ModuleOcnFaceplateLineKbyteTxAlarmIsSuppressed(OcnModule(self), EgressPortIdGet(self));
    }

static eAtRet OverrideEnable(AtPwKbyteChannel self, eBool enable)
    {
    return HwChannelEnable(self, enable, BaseAddress(self) + cAf6Reg_upen_upen_cfgovwcid_Base);
    }

static eBool OverrideIsEnabled(AtPwKbyteChannel self)
    {
    return HwChannelIsEnabled(self, BaseAddress(self) + cAf6Reg_upen_upen_cfgovwcid_Base);
    }

static uint32 DefaultOffset(AtPwKbyteChannel self)
    {
    return AtChannelIdGet((AtChannel)self);
    }

static eAtRet OverrideValueSet(AtPwKbyteChannel self, uint32 valueFieldMask, uint32 valueFieldShift, uint8 value)
    {
    uint32 regAddr = cAf6Reg_upen_k12pro_Base + BaseAddress(self) + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    mRegFieldSet(regVal, valueField, value);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);

    return cAtOk;
    }

static uint8 OverrideValueGet(AtPwKbyteChannel self, uint32 valueFieldMask, uint32 valueFieldShift)
    {
    uint32 regAddr = cAf6Reg_upen_k12pro_Base + BaseAddress(self) + DefaultOffset(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    return (uint8) mRegField(regVal, valueField);
    }

static eAtRet TxK1Set(AtPwKbyteChannel self, uint8 value)
    {
    return OverrideValueSet(self, cAf6_upen_k12pro_K1Val_Mask, cAf6_upen_k12pro_K1Val_Shift, value);
    }

static uint8 TxK1Get(AtPwKbyteChannel self)
    {
    return OverrideValueGet(self, cAf6_upen_k12pro_K1Val_Mask, cAf6_upen_k12pro_K1Val_Shift);
    }

static eAtRet TxK2Set(AtPwKbyteChannel self, uint8 value)
    {
    return OverrideValueSet(self, cAf6_upen_k12pro_K2Val_Mask, cAf6_upen_k12pro_K2Val_Shift, value);
    }

static uint8 TxK2Get(AtPwKbyteChannel self)
    {
    return OverrideValueGet(self, cAf6_upen_k12pro_K2Val_Mask, cAf6_upen_k12pro_K2Val_Shift);
    }

static eBool EKnIsValid(uint8 value)
    {
    return (value <= 1) ? cAtTrue : cAtFalse;
    }

static eAtRet TxEK1Set(AtPwKbyteChannel self, uint8 value)
    {
    if (!EKnIsValid(value))
        return cAtErrorOutOfRangParm;

    return OverrideValueSet(self, cAf6_upen_k12pro_EK1Val_Mask, cAf6_upen_k12pro_EK1Val_Shift, value);
    }

static uint8 TxEK1Get(AtPwKbyteChannel self)
    {
    return OverrideValueGet(self, cAf6_upen_k12pro_EK1Val_Mask, cAf6_upen_k12pro_EK1Val_Shift);
    }

static eAtRet TxEK2Set(AtPwKbyteChannel self, uint8 value)
    {
    if (!EKnIsValid(value))
        return cAtErrorOutOfRangParm;

    return OverrideValueSet(self, cAf6_upen_k12pro_EK2Val_Mask, cAf6_upen_k12pro_EK2Val_Shift, value);
    }

static uint8 TxEK2Get(AtPwKbyteChannel self)
    {
    return OverrideValueGet(self, cAf6_upen_k12pro_EK2Val_Mask, cAf6_upen_k12pro_EK2Val_Shift);
    }

static uint8 RxValueGet(AtPwKbyteChannel self, uint32 valueFieldMask, uint32 valueFieldShift)
    {
    uint32 regAddr = cAf6Reg_upen_k12acc_Base + BaseAddress(self) +
                     mMethodsGet(mThis(self))->ChannelRxOffset(mThis(self));
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    return (uint8) mRegField(regVal, valueField);
    }

static uint8 RxK1Get(AtPwKbyteChannel self)
    {
    return RxValueGet(self, cAf6_upen_k12acc_K1Val_Mask, cAf6_upen_k12acc_K1Val_Shift);
    }

static uint8 RxK2Get(AtPwKbyteChannel self)
    {
    return RxValueGet(self, cAf6_upen_k12acc_K2Val_Mask, cAf6_upen_k12acc_K2Val_Shift);
    }

static uint8 RxEK1Get(AtPwKbyteChannel self)
    {
    return RxValueGet(self, cAf6_upen_k12acc_EK1Val_Mask, cAf6_upen_k12acc_EK1Val_Shift);
    }

static uint8 RxEK2Get(AtPwKbyteChannel self)
    {
    return RxValueGet(self, cAf6_upen_k12acc_EK2Val_Mask, cAf6_upen_k12acc_EK2Val_Shift);
    }

static eBool LabelValueIsValid(uint16 value)
    {
    return (value <= cBit11_0) ? cAtTrue : cAtFalse;
    }

static uint32 EgressPortIdGet(AtPwKbyteChannel self)
    {
    uint32 regVal, regAddr;
    regAddr = cAf6Reg_upen_k12rx_chmap_Base + BaseAddress(self) + DefaultOffset(self);
    regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    return mRegField(regVal, cAf6_upen_k12rx_chmap_PortID_);
    }

static eAtRet ExpectedLabelSet(AtPwKbyteChannel self, uint16 value)
    {
    uint32 regVal, regAddr;

    if (!LabelValueIsValid(value))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_upen_k12rx_chcfg_Base + BaseAddress(self) + EgressPortIdGet(self);
    regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    mRegFieldSet(regVal, cAf6_upen_k12rx_chcfg_ChannelID_, value);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);

    return cAtOk;
    }

static uint16 ExpectedLabelGet(AtPwKbyteChannel self)
    {
    uint32 regVal, regAddr;
    regAddr = cAf6Reg_upen_k12rx_chcfg_Base + BaseAddress(self) + EgressPortIdGet(self);
    regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    return (uint16) mRegField(regVal, cAf6_upen_k12rx_chcfg_ChannelID_);
    }

static uint32 IngressPortIdGet(AtPwKbyteChannel self)
    {
    uint32 regVal, regAddr;
    uint32 egressPort = EgressPortIdGet(self);
    uint32 channelId;

    regAddr = cAf6Reg_upen_cid2pid_Base + BaseAddress(self) + egressPort;
    regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    channelId = mRegField(regVal, cAf6_upen_cid2pid_channelId_);

    /* Only support symmetric mapping (1 Faceplate: 1 K-byte channel) */
    if (AtChannelIdGet((AtChannel)self) == channelId)
        return egressPort;

    return cInvalidUint32;
    }

static uint16 TxLabelGet(AtPwKbyteChannel self)
    {
    uint32 regVal, regAddr;
    regAddr = cAf6Reg_upen_cid2pid_Base + BaseAddress(self) + IngressPortIdGet(self);
    regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    return (uint16) mRegField(regVal, cAf6_upen_cid2pid_channel12b_);
    }

static eAtRet TxLabelSet(AtPwKbyteChannel self, uint16 value)
    {
    uint32 regVal, regAddr;

    if (!LabelValueIsValid(value))
        return cAtErrorOutOfRangParm;

    regAddr = cAf6Reg_upen_cid2pid_Base + BaseAddress(self) + IngressPortIdGet(self);
    regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    mRegFieldSet(regVal, cAf6_upen_cid2pid_channel12b_, value);
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);

    return cAtOk;
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return cAtPwKbyteChannelAlarmChannelMismatched;
    }

static uint32 InterruptEnableRegister(AtChannel self)
    {
    AtPw pw = AtPwKbyteChannelKbytePwGet((AtPwKbyteChannel)self);
    return mMethodsGet((Tha60290021PwKByte)pw)->InterruptEnableRegister((Tha60290021PwKByte)pw) + BaseAddress((AtPwKbyteChannel)self);
    }

static uint32 InterruptStatusRegister(AtChannel self)
    {
    AtPw pw = AtPwKbyteChannelKbytePwGet((AtPwKbyteChannel)self);
    return mMethodsGet((Tha60290021PwKByte)pw)->InterruptStatusRegister((Tha60290021PwKByte)pw) + BaseAddress((AtPwKbyteChannel)self);
    }

static uint32 CurrentStatusRegister(AtChannel self)
    {
    AtPw pw = AtPwKbyteChannelKbytePwGet((AtPwKbyteChannel)self);
    return mMethodsGet((Tha60290021PwKByte)pw)->CurrentStatusRegister((Tha60290021PwKByte)pw) + BaseAddress((AtPwKbyteChannel)self);
    }

static uint32 ChannelAlarmMask(AtChannel self)
    {
    return cBit8 << AtChannelIdGet(self);
    }

static eAtRet HwInterruptEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr = InterruptEnableRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    uint32 channelMask = ChannelAlarmMask(self);
    if (enable)
        regVal |= channelMask;
    else
        regVal &= ~channelMask;
    mChannelHwWrite(self, regAddr, regVal, cAtModulePw);
    return cAtOk;
    }

static eBool HwInterruptIsEnabled(AtChannel self)
    {
    uint32 regAddr = InterruptEnableRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    uint32 channelMask = ChannelAlarmMask(self);
    return (regVal & channelMask) ? cAtTrue : cAtFalse;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    if (defectMask & cAtPwKbyteChannelAlarmChannelMismatched)
        return HwInterruptEnable(self, (enableMask & cAtPwKbyteChannelAlarmChannelMismatched) ? cAtTrue : cAtFalse);
    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    if (HwInterruptIsEnabled(self))
        return cAtPwKbyteChannelAlarmChannelMismatched;
    return 0;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    uint32 intrAddr = InterruptStatusRegister(self);
    uint32 intrStatus = mChannelHwRead(self, intrAddr, cAtModulePw);
    uint32 intrMask = mChannelHwRead(self, InterruptEnableRegister(self), cAtModulePw);
    uint32 currentStatus = mChannelHwRead(self, CurrentStatusRegister(self), cAtModulePw);
    uint32 channelMask = ChannelAlarmMask(self);
    uint32 changedDefects = 0;
    uint32 defects = 0;
    AtUnused(offset);

    intrStatus = (intrStatus & intrMask & channelMask);
    if (intrStatus == 0)
        return 0;

    /* Clear interrupt */
    mChannelHwWrite(self, intrAddr, intrStatus, cAtModulePw);

    if (intrStatus)
        {
        changedDefects |= cAtPwKbyteChannelAlarmChannelMismatched;
        if (currentStatus & channelMask)
            defects |= cAtPwKbyteChannelAlarmChannelMismatched;
        }

    AtChannelAllAlarmListenersCall(self, changedDefects, defects);

    return 0;
    }

static uint32 DefectRead2Clear(AtChannel self, eBool r2c)
    {
    uint32 regAddr = InterruptStatusRegister(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModulePw);
    uint32 channelMask = ChannelAlarmMask(self);
    uint32 alarmMask = 0;

    if (regVal & channelMask)
        alarmMask |= cAtPwKbyteChannelAlarmChannelMismatched;

    /* Clear Interrupt Status */
    if (alarmMask && r2c)
        mChannelHwWrite(self, regAddr, channelMask, cAtModulePw);

    return alarmMask;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    return DefectRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    return DefectRead2Clear(self, cAtTrue);
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regAddr = CurrentStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModulePw);
    return (regVal & ChannelAlarmMask(self)) ? cAtPwKbyteChannelAlarmChannelMismatched : 0;
    }

static uint32 ChannelRxOffset(Tha6029PwKbyteChannel self)
    {
    return (uint32)(AtChannelIdGet((AtChannel)self) * 16UL);
    }

static void MethodsInit(AtPwKbyteChannel self)
    {
    Tha6029PwKbyteChannel channel = (Tha6029PwKbyteChannel)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, ChannelRxOffset);
        }

    mMethodsSet(channel, &m_methods);
    }

static void OverrideAtPwKbyteChannel(AtPwKbyteChannel self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwKbyteChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwKbyteChannelOverride, mMethodsGet(self), sizeof(m_AtPwKbyteChannelOverride));

        /* Setup methods */
        mMethodOverride(m_AtPwKbyteChannelOverride, SdhLineSet);
        mMethodOverride(m_AtPwKbyteChannelOverride, ValidationEnable);
        mMethodOverride(m_AtPwKbyteChannelOverride, ValidationIsEnabled);
        mMethodOverride(m_AtPwKbyteChannelOverride, AlarmSuppressionEnable);
        mMethodOverride(m_AtPwKbyteChannelOverride, AlarmSuppressionIsEnabled);
        mMethodOverride(m_AtPwKbyteChannelOverride, OverrideEnable);
        mMethodOverride(m_AtPwKbyteChannelOverride, OverrideIsEnabled);
        mMethodOverride(m_AtPwKbyteChannelOverride, TxK1Set);
        mMethodOverride(m_AtPwKbyteChannelOverride, TxK1Get);
        mMethodOverride(m_AtPwKbyteChannelOverride, TxK2Set);
        mMethodOverride(m_AtPwKbyteChannelOverride, TxK2Get);
        mMethodOverride(m_AtPwKbyteChannelOverride, TxEK1Set);
        mMethodOverride(m_AtPwKbyteChannelOverride, TxEK1Get);
        mMethodOverride(m_AtPwKbyteChannelOverride, TxEK2Set);
        mMethodOverride(m_AtPwKbyteChannelOverride, TxEK2Get);
        mMethodOverride(m_AtPwKbyteChannelOverride, RxK1Get);
        mMethodOverride(m_AtPwKbyteChannelOverride, RxK2Get);
        mMethodOverride(m_AtPwKbyteChannelOverride, RxEK1Get);
        mMethodOverride(m_AtPwKbyteChannelOverride, RxEK2Get);
        mMethodOverride(m_AtPwKbyteChannelOverride, ExpectedLabelSet);
        mMethodOverride(m_AtPwKbyteChannelOverride, ExpectedLabelGet);
        mMethodOverride(m_AtPwKbyteChannelOverride, TxLabelSet);
        mMethodOverride(m_AtPwKbyteChannelOverride, TxLabelGet);
        }

    mMethodsSet(self, &m_AtPwKbyteChannelOverride);
    }

static void OverrideAtChannel(AtPwKbyteChannel self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);

        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPwKbyteChannel self)
    {
    OverrideAtPwKbyteChannel(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029PwKbyteChannel);
    }

AtPwKbyteChannel Tha6029PwKbyteChannelObjectInit(AtPwKbyteChannel self, uint32 channelId, AtPw pw, AtModulePw module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor should be called first */
    if (AtPwKbyteChannelObjectInit(self, channelId, pw, module) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPwKbyteChannel Tha6029PwKbyteChannelNew(uint32 channelId, AtPw pw, AtModulePw module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPwKbyteChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return Tha6029PwKbyteChannelObjectInit(newChannel, channelId, pw, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290021PwActivator.c
 *
 * Created Date: Feb 14, 2017
 *
 * Description : PW activator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhVcInternal.h"
#include "../sdh/Tha6029SdhLineSideAuVc.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../xc/Tha60290021ModuleXc.h"
#include "Tha60290021ModulePw.h"
#include "Tha60290021PwActivatorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaPwActivatorMethods        m_ThaPwActivatorOverride;

/* Save super implementation */
static const tThaPwActivatorMethods *m_ThaPwActivatorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleXc XcModule(ThaPwActivator self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ThaPwActivatorModuleGet(self));
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static eBool ShouldRedirectPwCircuitBinding(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    eTha60290021XcHideMode xcHideMode = Tha60290021ModuleXcHideModeGet(XcModule(self));

    if (xcHideMode == cTha60290021XcHideModeNone)
        return cAtFalse;

    if (AtPwTypeGet((AtPw)adapter) != cAtPwTypeCEP)
        return cAtFalse;

    if (AtSdhVcIsHoVc((AtSdhVc)circuit))
        return Tha60290021ModuleSdhIsLineSideVc((AtSdhChannel)circuit);

    return cAtFalse;
    }

static AtChannel HideVc(AtChannel lineSideVc)
    {
    return (AtChannel)AtSdhChannelHideChannelGet((AtSdhChannel)lineSideVc);
    }

static eAtModulePwRet PwCircuitBind(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    if (ShouldRedirectPwCircuitBinding(self, adapter, circuit))
        return m_ThaPwActivatorMethods->PwCircuitBind(self, adapter, HideVc(circuit));

    return m_ThaPwActivatorMethods->PwCircuitBind(self, adapter, circuit);
    }

static eAtModulePwRet PwCircuitUnbind(ThaPwActivator self, ThaPwAdapter adapter)
    {
    return m_ThaPwActivatorMethods->PwCircuitUnbind(self, adapter);
    }

static void OverrideThaPwActivator(ThaPwActivator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwActivatorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwActivatorOverride, mMethodsGet(self), sizeof(m_ThaPwActivatorOverride));

        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitBind);
        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitUnbind);
        }

    mMethodsSet(self, &m_ThaPwActivatorOverride);
    }

static void Override(ThaPwActivator self)
    {
    OverrideThaPwActivator(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PwActivator);
    }

ThaPwActivator Tha60290021PwActivatorObjectInit(ThaPwActivator self, AtModulePw pwModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011PwDynamicActivatorObjectInit(self, pwModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwActivator Tha60290021PwActivatorNew(AtModulePw pwModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwActivator newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021PwActivatorObjectInit(newActivator, pwModule);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha6029KbyteDccInterruptManagerInternal.h
 * 
 * Created Date: Jan 19, 2018
 *
 * Description : Kbyte/DCC PW interrupt management.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029KBYTEDCCINTERRUPTMANAGERINTERNAL_H_
#define _THA6029KBYTEDCCINTERRUPTMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/man/intrcontroller/ThaInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029KbyteDccInterruptManager
    {
    tThaInterruptManager super;
    }tTha6029KbyteDccInterruptManager;

/*--------------------------- Forward declarations ---------------------------*/
AtInterruptManager Tha6029KbyteDccInterruptManagerObjectInit(AtInterruptManager self, AtModule module);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA6029KBYTEDCCINTERRUPTMANAGERINTERNAL_H_ */


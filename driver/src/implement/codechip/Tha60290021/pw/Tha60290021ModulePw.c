/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha60290021ModulePw.c
 *
 * Created Date: Jul 12, 2016
 *
 * Description : PW  management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/xc/AtCrossConnectInternal.h"
#include "../../../../generic/sdh/AtSdhVcInternal.h"
#include "../man/Tha60290021Device.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../sdh/Tha6029SdhLineSideAuVc.h"
#include "../eth/Tha60290021FaceplateSerdesEthPortReg.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "../pwe/Tha60290021ModulePwe.h"
#include "../common/Tha602900xxCommon.h"
#include "../xc/Tha60290021ModuleXc.h"
#include "Tha60290021ModulePwInternal.h"
#include "Tha60290021DccKbyteReg.h"
#include "Tha6029KbyteDccInterruptManager.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290021ModulePw)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021ModulePwMethods m_methods;

/* Override */
static tAtObjectMethods            m_AtObjectOverride;
static tAtModuleMethods            m_AtModuleOverride;
static tAtModulePwMethods          m_AtModulePwOverride;
static tThaModulePwMethods         m_ThaModulePwOverride;
static tTha60210011ModulePwMethods m_Tha60210011ModulePwOverride;
static tTha60150011ModulePwMethods m_Tha60150011ModulePwOverride;

/* Save super implementation */
static const tAtObjectMethods    *m_AtObjectMethods    = NULL;
static const tAtModuleMethods    *m_AtModuleMethods    = NULL;
static const tAtModulePwMethods  *m_AtModulePwMethods  = NULL;
static const tThaModulePwMethods *m_ThaModulePwMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(ThaModulePw self)
    {
    return AtModuleDeviceGet((AtModule)self);
    }

static ThaVersionReader VersionReader(ThaModulePw self)
    {
    return ThaDeviceVersionReader(Device(self));
    }

static eBool IsSimulated(ThaModulePw self)
    {
    return AtDeviceIsSimulated(Device(self));
    }

static eBool SubportVlanHwIsReady(Tha60290021ModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;
    ThaVersionReader versionReader = VersionReader(pwModule);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x1, 0);

    if (IsSimulated(pwModule))
    	return cAtTrue;

    if (hwVersion >= startVersion)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 TxSubportBaseAddress(void)
	{
	return Tha60290021ModuleEthBypassBaseAddress();
	}

static uint32 TxSubportVlanAddress(uint32 vlanIndex)
	{
	return (vlanIndex == 0) ? cAf6Reg_upen_cfgpwvlan1_Base : cAf6Reg_upen_cfgpwvlan2_Base;
	}

static uint32 ExpectedMask(uint32 vlanIndex)
	{
	return (vlanIndex == 0) ? cAf6_upen_cfgrmtpid_pw_out_cfgtpid_pw1_Mask : cAf6_upen_cfgrmtpid_pw_out_cfgtpid_pw2_Mask;
	}

static uint32 ExpectedShift(uint32 vlanIndex)
	{
	return (vlanIndex == 0) ? cAf6_upen_cfgrmtpid_pw_out_cfgtpid_pw1_Shift : cAf6_upen_cfgrmtpid_pw_out_cfgtpid_pw2_Shift;
	}

static eAtRet TxSubportVlanSet(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *vlan)
    {
    uint32 regAddr = TxSubportVlanAddress(subPortVlanIndex) + TxSubportBaseAddress();
    mModuleHwWrite(self, regAddr, ThaPktUtilVlanToUint32(vlan));
    return cAtOk;
    }

static eAtRet TxSubportVlanGet(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *vlan)
    {
	uint32 regVal = mModuleHwRead(self, TxSubportVlanAddress(subPortVlanIndex) + TxSubportBaseAddress());
    ThaPktUtilRegUint32ToVlan(regVal, vlan);
    return cAtOk;
    }

static eAtRet ExpectedSubportVlanSet(ThaModulePw self, uint32 subPortVlanIndex, const tAtVlan *expectedVlan)
    {
	uint32 regAddr, regVal;
    uint32 hwVlan = ThaPktUtilVlanToUint32(expectedVlan);

	regAddr = cAf6Reg_upen_cfgrmtpid_pw_Base + TxSubportBaseAddress();
    regVal = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, ExpectedMask(subPortVlanIndex), ExpectedShift(subPortVlanIndex), ((hwVlan & cBit31_16) >> 16));
    mModuleHwWrite(self, regAddr, regVal);

	regAddr = cAf6Reg_upen_cfg_pwvlan_Base  + TxSubportBaseAddress();
    regVal = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, ExpectedMask(subPortVlanIndex), ExpectedShift(subPortVlanIndex), (hwVlan & cBit15_0));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet ExpectedSubportVlanGet(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *expectedVlan)
    {
    uint32 regVal, hwLoVlan, hwHoVlan;

    regVal = mModuleHwRead(self, cAf6Reg_upen_cfgrmtpid_pw_Base  + TxSubportBaseAddress());
    mFieldGet(regVal, ExpectedMask(subPortVlanIndex), ExpectedShift(subPortVlanIndex), uint32, &hwHoVlan);

    regVal = mModuleHwRead(self, cAf6Reg_upen_cfg_pwvlan_Base  + TxSubportBaseAddress());
    mFieldGet(regVal, ExpectedMask(subPortVlanIndex), ExpectedShift(subPortVlanIndex), uint32, &hwLoVlan);

    ThaPktUtilRegUint32ToVlan((hwLoVlan| (hwHoVlan << 16)), expectedVlan);
    return cAtOk;
    }

static uint32 NumSubPortVlans(ThaModulePw self)
    {
    AtUnused(self);
    return 2UL;
    }

static uint32 TxPwSubportAddress(ThaModulePw self)
	{
	AtUnused(self);
	return (uint32) (cAf6Reg_upen_cfgpwvlaninsen_Base + TxSubportBaseAddress());
	}

static uint32 ExpectPwSubportAddress(ThaModulePw self)
    {
    AtUnused(self);
    return (uint32) (cAf6Reg_upen_enbtpid_Base + TxSubportBaseAddress());
    }

static eAtRet SubPortVlanCheckingEnable(ThaModulePw self, eBool enabled)
    {
    uint32 regAddr =  ExpectPwSubportAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_enbtpid_out_enbtpid_pw_, enabled ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool SubPortVlanCheckingIsEnabled(ThaModulePw self)
    {
    uint32 regAddr =  ExpectPwSubportAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_upen_enbtpid_out_enbtpid_pw_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet HwPwSubPortVlanGlobalIndexSet(ThaModulePw self, uint32 subPortVlanIndex)
    {
    uint32 regAddr = TxPwSubportAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_upen_cfgpwvlaninsen_PwVlanSel_, subPortVlanIndex);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 HwPwSubPortVlanGlobalIndexGet(ThaModulePw self)
    {
    uint32 regVal = mModuleHwRead(self, TxPwSubportAddress(self));
    return mRegField(regVal, cAf6_upen_cfgpwvlaninsen_PwVlanSel_);
    }

static eBool CanSelectSubPortVlanPerPw(Tha60290021ModulePw self)
    {
    ThaVersionReader versionReader = VersionReader((ThaModulePw)self);
    uint32 startSupportedVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x5, 0x0);
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (version >= startSupportedVersion) ? cAtTrue : cAtFalse;
    }

static ThaModulePwe PweModule(ThaModulePw self)
    {
    return (ThaModulePwe)AtDeviceModuleGet(Device(self), cThaModulePwe);
    }

static eAtRet TxPwSubPortVlanIndexSet(ThaModulePw self, AtPw adapter, uint32 subPortVlanIndex)
    {
    if (mMethodsGet(mThis(self))->CanSelectSubPortVlanPerPw(mThis(self)) && adapter)
        return Tha60290021ModulePwePwSubPortVlanIndexSet(PweModule(self), adapter, subPortVlanIndex);

    return HwPwSubPortVlanGlobalIndexSet(self, subPortVlanIndex);
    }

static uint32 TxPwSubPortVlanIndexGet(ThaModulePw self, AtPw adapter)
    {
    if (mMethodsGet(mThis(self))->CanSelectSubPortVlanPerPw(mThis(self)) && adapter)
        return Tha60290021ModulePwePwSubPortVlanIndexGet(PweModule(self), adapter);

    return HwPwSubPortVlanGlobalIndexGet(self);
    }

static uint32 MaxApsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 MaxHsGroupsGet(AtModulePw self)
    {
    AtUnused(self);
    return 0;
    }

static AtPwHdlc HdlcPwObjectCreate(AtModulePw self, AtHdlcChannel hdlcChannel)
    {
    return Tha60290021PwDccNew(hdlcChannel, self);
    }

static eAtRet DeleteHdlcPwObject(AtModulePw self, AtPw pw)
    {
    return Tha602900xxModulePwDccHdlcPwObjectDelete(self, pw);
    }

static eAtRet DeleteKbytePw(AtModulePw self)
    {
    if (mThis(self)->kBytePw == NULL)
        return cAtOk;

    AtObjectDelete((AtObject)mThis(self)->kBytePw);
    mThis(self)->kBytePw = NULL;
    return cAtOk;
    }

static AtPw KBytePwObjectCreate(Tha60290021ModulePw self, uint32 pwId)
    {
    AtUnused(pwId);
    return Tha60290021PwKbyteNew(0, (AtModulePw)self);
    }

static AtPw PwKBytePwGet(ThaModulePw self)
    {
    if (mThis(self)->kBytePw == NULL)
        {
        AtPw pw = mMethodsGet(mThis(self))->KBytePwObjectCreate(mThis(self), 0);
        if (pw == NULL)
            return NULL;

        /* Try to Init */
        if (AtChannelInit((AtChannel) pw) != cAtOk)
            AtModuleLog((AtModule) self, cAtLogLevelCritical, AtSourceLocation, "Cannot Init KbytePw \r\n");

        mThis(self)->kBytePw = pw;
        }

    return mThis(self)->kBytePw;
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret;
    ret = DeleteKbytePw((AtModulePw) self);
    if (ret != cAtOk)
        return ret;

    return m_AtModuleMethods->Setup(self);
    }

static eAtRet SubPortVlanDefaultSet(AtModule self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;
    eAtRet ret = cAtOk;

    if (Tha60290021DeviceIsRunningOnOtherPlatform(AtModuleDeviceGet(self)))
        return cAtOk;

    if (!Tha60290021ModulePwSubportVlanHwIsReady(pwModule))
        return cAtOk;

    ret |= ThaModulePwSubPortVlanCheckingEnable(pwModule, cAtTrue);
    ret |= Tha602900xxSubPortVlanDefaultTpidSet(pwModule);
    ret |= ThaModulePwSubPortVlanInsertionEnable(pwModule, cAtTrue);

    if (!mMethodsGet(mThis(self))->CanSelectSubPortVlanPerPw(mThis(self)))
        {
        ret |= ThaModulePwSubPortVlanInsertionEnable(pwModule, cAtTrue);
        ret |= ThaModulePwTxPwSubPortVlanIndexSet(pwModule, NULL, 0); /* For global subport VLAN */
        }

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return SubPortVlanDefaultSet(self);
    }

static void Delete(AtObject self)
    {
    DeleteKbytePw((AtModulePw) self);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290021ModulePw* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(kBytePw);
    mEncodeObjectDescription(hoVcToRestoreXc);
    }

static eBool DebugCountersModuleIsSupported(ThaModulePw self, uint32 module)
    {
    if (module == cThaModulePwPmc)
        return cAtTrue;

    return m_ThaModulePwMethods->DebugCountersModuleIsSupported(self, module);
    }

static eAtRet ShowPrivateData(AtModule self)
    {
    AtChannel channel = mThis(self)->hoVcToRestoreXc;

    if (channel == NULL)
        return cAtOk;

    AtPrintc(cSevCritical, "* PW removing may not happen properly, pending on: %s.%s\r\n",
             AtChannelTypeString(channel),
             AtChannelIdString(channel));

    return cAtOk;
    }

static eAtRet Debug(AtModule self)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtModuleMethods->Debug(self);
    ret |= ThaModulePwSubPortVlanDebug((ThaModulePw)self);
    ret |= ShowPrivateData(self);

    return ret;
    }

static ThaPwActivator PwActivatorCreate(ThaModulePw self)
    {
    return Tha60290021PwActivatorNew((AtModulePw)self);
    }

static eBool CanBindEthPort(ThaModulePw self, AtPw pw, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(pw);

    if (port == NULL)
        return cAtTrue;

    return Tha60290021ModuleEthIs40GInternalPort(port);
    }

static AtModuleXc XcModule(ThaModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static eBool XcHidingShouldRedirect(ThaModulePw self, ThaPwAdapter adapter, AtChannel circuit)
    {
    if (Tha60290021ModuleXcHideModeGet(XcModule(self)) == cTha60290021XcHideModeNone)
        return cAtFalse;

    if (AtPwTypeGet((AtPw)adapter) != cAtPwTypeCEP)
        return cAtFalse;

    if (circuit == NULL)
        return cAtFalse;

    return Tha60290021ModuleSdhIsLineSideVc((AtSdhChannel)circuit);
    }

static AtChannel RedirectedCircuit(AtChannel circuit)
    {
    return (AtChannel)AtSdhChannelHideChannelGet((AtSdhChannel)circuit);
    }

static AtChannel ReferenceCircuitToConfigureHardware(ThaModulePw self, ThaPwAdapter adapter, AtChannel circuit)
    {
    if (XcHidingShouldRedirect(self, adapter, circuit))
        return RedirectedCircuit(circuit);

    return m_ThaModulePwMethods->ReferenceCircuitToConfigureHardware(self, adapter, circuit);
    }

static AtSdhChannel SdhChannelHoVcGet(AtSdhChannel sdhChannel)
    {
    AtSdhChannel currentNode = sdhChannel;

    while (currentNode)
        {
        if (AtSdhVcIsHoVc((AtSdhVc)currentNode))
            return currentNode;

        currentNode = AtSdhChannelParentChannelGet(currentNode);
        }

    return NULL;
    }

static AtPdhChannel PdhChannelRootGet(AtPdhChannel pdhChannel)
    {
    AtPdhChannel parent;

    if (pdhChannel == NULL)
        return NULL;

    parent = AtPdhChannelParentChannelGet(pdhChannel);
    if (parent == NULL)
        return pdhChannel;

    while (AtPdhChannelParentChannelGet(parent))
        parent = AtPdhChannelParentChannelGet(parent);

    return parent;
    }

static AtSdhChannel PdhChannelHoVcGet(AtPdhChannel pdhChannel)
    {
    AtPdhChannel root = PdhChannelRootGet(pdhChannel);

    if (root)
        return SdhChannelHoVcGet((AtSdhChannel)AtPdhChannelVcGet(root));

    return NULL;
    }

static AtSdhChannel HoVcFind(AtPw adapter)
    {
    AtChannel circuit = AtPwBoundCircuitGet(adapter);
    uint32 pwType;

    if (circuit == NULL)
        return NULL;

    pwType = AtPwTypeGet(adapter);
    switch (pwType)
        {
        case cAtPwTypeSAToP:
            return PdhChannelHoVcGet((AtPdhChannel)circuit);
        case cAtPwTypeCESoP:
            return PdhChannelHoVcGet((AtPdhChannel)AtPdhNxDS0De1Get((AtPdhNxDS0)circuit));
        case cAtPwTypeCEP:
            return SdhChannelHoVcGet((AtSdhChannel)circuit);

        default:
            return NULL;
        }
    }

static AtCrossConnect HoXc(ThaModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleXc xcModule = (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    return AtModuleXcVcCrossConnectGet(xcModule);
    }

static eAtRet MakeValidXcIfNeed(ThaModulePw self, AtPw adapter)
    {
    ThaPwAdapter pwAdapter = (ThaPwAdapter)adapter;
    AtSdhChannel hoVc;
    AtCrossConnect xc;
    eAtRet ret;

    if (ThaPwAdapterIsLogicPw(pwAdapter))
        return cAtOk;

    hoVc = HoVcFind(adapter);

    /* When there is no source connection at XC block, no valid come to MAP/DEMAP
     * and this make PW hardware resources are not destroyed properly. The below
     * code is to make a local loopback before  */
    if (!Tha60290021ModuleSdhIsTerminatedVc(hoVc))
        return cAtOk;

    xc = HoXc(self);
    if (AtCrossConnectHwIsConnected(xc, NULL, (AtChannel)hoVc))
        return cAtOk;

    ret = AtChannelLoopbackSet((AtChannel)hoVc, cAtLoopbackModeLocal);
    if (ret == cAtOk)
        {
        mThis(self)->hoVcToRestoreXc = (AtChannel)hoVc;
        return cAtOk;
        }

    AtChannelLog((AtChannel)adapter, cAtLogLevelCritical, AtSourceLocation,
                 "Cannot make temporary loopback on %s.%s, ret = %s\r\n",
                 AtChannelTypeString((AtChannel)hoVc),
                 AtChannelIdString((AtChannel)hoVc),
                 AtRet2String(ret));
    return ret;
    }

static eAtRet RestoreXcIfNeed(ThaModulePw self, AtPw adapter)
    {
    eAtRet ret;
    AtChannel vc = mThis(self)->hoVcToRestoreXc;

    if (vc == NULL)
        return cAtOk;

    ret = AtChannelLoopbackSet(vc, cAtLoopbackModeRelease);
    if (ret == cAtOk)
        {
        mThis(self)->hoVcToRestoreXc = NULL;
        return ret;
        }

    AtChannelLog((AtChannel)adapter, cAtLogLevelCritical, AtSourceLocation,
                 "Cannot restore XC of %s.%s, ret = %s\r\n",
                 AtChannelTypeString((AtChannel)vc),
                 AtChannelIdString((AtChannel)vc),
                 AtRet2String(ret));
    return ret;
    }

static void PwDisablePrepare(ThaModulePw self, AtPw adapter)
    {
    MakeValidXcIfNeed(self, adapter);
    m_ThaModulePwMethods->PwDisablePrepare(self, adapter);
    }

static void PwDisableFinish(ThaModulePw self, AtPw adapter)
    {
    m_ThaModulePwMethods->PwDisableFinish(self, adapter);
    RestoreXcIfNeed(self, adapter);
    }

static eBool SubportVlansSupported(ThaModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool TxActiveForceSupported(ThaModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool RtpFrequencyLargerThan16BitsIsSupported(Tha60150011ModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionHas1344PwsPerSts24Slice(Tha60210011ModulePw self)
    {
    /* This is not applied for this product */
    AtUnused(self);
    return cInvalidUint32;
    }

static eBool ShouldHandleCircuitAisForcingOnEnabling(ThaModulePw self)
    {
    AtUnused(self);
    return cTha602900xxShouldHandleCircuitAisForcingOnEnabling;
    }

static eBool ShouldMaskDefectAndFailureOnPwDisabling(ThaModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtInterruptManager InterruptManagerCreate(AtModule self, uint32 managerIndex)
    {
    AtUnused(managerIndex);
    if (Tha60290021ModulePwDccKbyteInterruptIsSupported(mThis(self)))
        return Tha6029KbyteDccInterruptManagerNew(self);
    return NULL;
    }

static uint32 NumInterruptManagers(AtModule self)
    {
    if (Tha60290021ModulePwDccKbyteInterruptIsSupported(mThis(self)))
        return 1;
    return 0;
    }

static eBool DccKbyteInterruptIsSupported(Tha60290021ModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtPwKbyteChannel KByteChannelObjectCreate(Tha60290021ModulePw self, uint32 channelId, AtPw pw)
    {
    return Tha6029PwKbyteChannelNew(channelId, pw, (AtModulePw)self);
    }

static eBool DefaultCESoPPayloadSizeInBytes(AtModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideTha60210011ModulePw(AtModulePw self)
    {
    Tha60210011ModulePw module = (Tha60210011ModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePwOverride, mMethodsGet(module), sizeof(m_Tha60210011ModulePwOverride));

        mMethodOverride(m_Tha60210011ModulePwOverride, StartVersionHas1344PwsPerSts24Slice);
        }

    mMethodsSet(module, &m_Tha60210011ModulePwOverride);
    }

static void OverrideTha60150011ModulePw(AtModulePw self)
    {
    Tha60150011ModulePw module = (Tha60150011ModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011ModulePwOverride, mMethodsGet(module), sizeof(m_Tha60150011ModulePwOverride));

        mMethodOverride(m_Tha60150011ModulePwOverride, RtpFrequencyLargerThan16BitsIsSupported);
        }

    mMethodsSet(module, &m_Tha60150011ModulePwOverride);
    }

static void OverrideAtObject(AtModulePw self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePwMethods = mMethodsGet(pwModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, m_ThaModulePwMethods, sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, PwKBytePwGet);
        mMethodOverride(m_ThaModulePwOverride, NumSubPortVlans);
        mMethodOverride(m_ThaModulePwOverride, ExpectedSubportVlanSet);
        mMethodOverride(m_ThaModulePwOverride, ExpectedSubportVlanGet);
        mMethodOverride(m_ThaModulePwOverride, TxSubportVlanGet);
        mMethodOverride(m_ThaModulePwOverride, TxSubportVlanSet);
        mMethodOverride(m_ThaModulePwOverride, SubPortVlanCheckingEnable);
        mMethodOverride(m_ThaModulePwOverride, SubPortVlanCheckingIsEnabled);
        mMethodOverride(m_ThaModulePwOverride, TxPwSubPortVlanIndexSet);
        mMethodOverride(m_ThaModulePwOverride, TxPwSubPortVlanIndexGet);
        mMethodOverride(m_ThaModulePwOverride, DebugCountersModuleIsSupported);
        mMethodOverride(m_ThaModulePwOverride, PwActivatorCreate);
        mMethodOverride(m_ThaModulePwOverride, CanBindEthPort);
        mMethodOverride(m_ThaModulePwOverride, ReferenceCircuitToConfigureHardware);
        mMethodOverride(m_ThaModulePwOverride, PwDisablePrepare);
        mMethodOverride(m_ThaModulePwOverride, PwDisableFinish);
        mMethodOverride(m_ThaModulePwOverride, SubportVlansSupported);
        mMethodOverride(m_ThaModulePwOverride, TxActiveForceSupported);
        mMethodOverride(m_ThaModulePwOverride, ShouldHandleCircuitAisForcingOnEnabling);
        mMethodOverride(m_ThaModulePwOverride, ShouldMaskDefectAndFailureOnPwDisabling);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePwMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, m_AtModulePwMethods, sizeof(tAtModulePwMethods));

        mMethodOverride(m_AtModulePwOverride, MaxApsGroupsGet);
        mMethodOverride(m_AtModulePwOverride, MaxHsGroupsGet);
        mMethodOverride(m_AtModulePwOverride, HdlcPwObjectCreate);
        mMethodOverride(m_AtModulePwOverride, DeleteHdlcPwObject);
        mMethodOverride(m_AtModulePwOverride, DefaultCESoPPayloadSizeInBytes);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void OverrideAtModule(AtModulePw self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, InterruptManagerCreate);
        mMethodOverride(m_AtModuleOverride, NumInterruptManagers);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModulePw(self);
    OverrideThaModulePw(self);
    OverrideTha60150011ModulePw(self);
    OverrideTha60210011ModulePw(self);
    }

static void MethodsInit(Tha60290021ModulePw self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SubportVlanHwIsReady);
        mMethodOverride(m_methods, CanSelectSubPortVlanPerPw);
        mMethodOverride(m_methods, KBytePwObjectCreate);
        mMethodOverride(m_methods, DccKbyteInterruptIsSupported);
        mMethodOverride(m_methods, KByteChannelObjectCreate);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModulePw);
    }

AtModulePw Tha60290021ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePwObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60290021ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModulePwObjectInit(newModule, device);
    }

eBool Tha60290021ModulePwSubportVlanHwIsReady(ThaModulePw self)
    {
	if (self)
		return mMethodsGet(mThis(self))->SubportVlanHwIsReady(mThis(self));

    return cAtFalse;
    }

eBool Tha60290021ModulePwDccKbyteInterruptIsSupported(Tha60290021ModulePw self)
    {
    if (self)
        return mMethodsGet(self)->DccKbyteInterruptIsSupported(self);
    return cAtFalse;
    }

AtPwKbyteChannel Tha60290021ModulePwKByteChannelObjectCreate(Tha60290021ModulePw self, uint32 channelId, AtPw pw)
    {
    if (self)
        return mMethodsGet(self)->KByteChannelObjectCreate(self, channelId, pw);
    return NULL;
    }



/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW Module
 * 
 * File        : Tha60290021ModulePw.h
 * 
 * Created Date: Sep 4, 2016
 *
 * Description : Tha60290021 Module PW header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPW_H_
#define _THA60290021MODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pw/headercontrollers/ThaPwHeaderController.h"
#include "AtPwHdlc.h"
#include "AtPwKbyteChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/* Default ETH Type */
#define cThaDccPwEthTypeDefault 0x880B

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021PwHdlc * Tha60290021PwHdlc;

typedef struct tTha60290021ModulePw * Tha60290021ModulePw;

typedef enum eTha6029DccPwAlarmType
    {
    cTha6029DccPwAlarmTypePktDiscards  = cBit0  /* Rx packet discard if corresponding enable is not set, with discard flag per channel. */
    }eTha6029DccPwAlarmType;

typedef enum eTha6029KbyteApsPwAlarmType
    {
    cTha6029KbyteApsPwAlarmTypeDaMacMisMatch       = cBit0,   /**< DA mismatch */
    cTha6029KbyteApsPwAlarmTypeEthTypeMisMatch     = cBit1,   /**< ETH Type mismatch */
    cTha6029KbyteApsPwAlarmTypeVerMisMatch         = cBit2,   /**< Version mismatch */
    cTha6029KbyteApsPwAlarmTypeTypeMisMatch        = cBit3,   /**< Type mismatch */
    cTha6029KbyteApsPwAlarmTypeLengthFieldMisMatch = cBit4,   /**< Length field mismatch */
    cTha6029KbyteApsPwAlarmTypeLengthCountMisMatch = cBit5,   /**< Number of actual bytes are mismatched */
    cTha6029KbyteApsPwAlarmTypeChannelMisMatch     = cBit6,   /**< Channel mismatch */
    cTha6029KbyteApsPwAlarmTypeWatchdogTimer       = cBit7,   /**< Channel mismatch */
    cTha6029KbyteApsPwAlarmTypeAll                 = cBit7_0  /**< Channel mismatch */
    }eTha6029KbyteApsPwAlarmType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* KByte PW */
AtPw Tha60290021PwKbyteNew(uint32 pwId,  AtModulePw module);
AtPwKbyteChannel Tha6029PwKbyteChannelNew(uint32 channelId, AtPw pw, AtModulePw module);

/* PW SubPortVlan */
eBool Tha60290021ModulePwSubportVlanHwIsReady(ThaModulePw self);

/* K-byte channels */
AtPwKbyteChannel Tha60290021ModulePwKByteChannelObjectCreate(Tha60290021ModulePw self, uint32 channelId, AtPw pw);
uint32 Tha60290021PwKbyteNumKbyteChannelsGet(AtPw self);
AtPwKbyteChannel Tha60290021PwKbyteChannelGet(AtPw self, uint32 channelId);

/* With PW */
eAtRet Tha6029PwKbyteIntervalSet(AtPw self, uint32 intervalInUs);
uint32 Tha6029PwKbyteIntervalGet(AtPw self);
eAtRet Tha6029PwKbyteTrigger(AtPw self);
eAtRet Tha6029PwKbyteWatchdogSet(AtPw self, uint32 timerInUs);
uint32 Tha6029PwKbyteWatchdogGet(AtPw self);

/* DCC PW */
AtPwHdlc Tha60290021PwDccNew(AtHdlcChannel dcc, AtModulePw module);
AtPwHdlc Tha60290021PwDccV2New(AtHdlcChannel dcc, AtModulePw module);
eAtRet Tha6029PwDccPwEthTypeSet(Tha60290021PwHdlc self, uint16 ethType);
uint16 Tha6029PwDccPwEthTypeGet(Tha60290021PwHdlc self);
eAtRet Tha6029PwDccPwTypeSet(Tha60290021PwHdlc self, uint8 type);
uint8 Tha6029PwDccPwTypeGet(Tha60290021PwHdlc self);
eAtRet Tha6029PwDccPwVersionSet(Tha60290021PwHdlc self, uint8 version);
uint8 Tha6029PwDccPwVersionGet(Tha60290021PwHdlc self);
eAtRet Tha6029PwDccMacCheckEnable(Tha60290021PwHdlc self, eBool enable);
eBool Tha6029PwDccMacCheckIsEnabled(Tha60290021PwHdlc self);
eAtRet Tha6029PwDccCVlanCheckEnable(Tha60290021PwHdlc self, eBool enable);
eBool Tha6029PwDccCVlanCheckIsEnabled(Tha60290021PwHdlc self);
eAtRet Tha6029PwDccPwExpectedLabelSet(Tha60290021PwHdlc self, uint8 label);
uint8 Tha6029PwDccPwExpectedLabelGet(Tha60290021PwHdlc self);
uint32 Tha60290021PwHdlcDccTxHeaderRegAddress(Tha60290021PwHdlc self);
uint16 Tha60290021PwHdlcLongReadOnCore(Tha60290021PwHdlc self, uint32 regAddr, uint32 *longRegVal, uint16 bufferLen);
uint16 Tha60290021PwHdlcLongWriteOnCore(Tha60290021PwHdlc self, uint32 regAddr, uint32 *longRegVal, uint16 bufferLen);
uint32 Tha60290021DccChannelId(AtPw self);
uint32 Tha60290021DccAddressWithLocalAddress(AtPw self, uint32 localAddress);
eAtRet Tha60290021PwHdlcHwDccPwLabelChannelSet(Tha60290021PwHdlc self, uint8 label, uint32 channelId);
uint32 Tha60290021PwHdlcHwDccPwLabelChannelGet(Tha60290021PwHdlc self, uint8 label);
eBool Tha60290021DccPwIsExpectedLabelIsUsed(Tha60290021PwHdlc self, uint8 label);
eBool Tha6029DccPwIsExpectedLabelUsedByLine(Tha60290021PwHdlc pwForNewLabel, AtSdhLine line, uint8 newLabel);

AtEthPort Tha6029PwDccDefaultSgmiiEthPort(Tha60290021PwHdlc self);
ThaPwHeaderController Tha60290021PwHeaderControllerNew(ThaPwAdapter adapter);

/* KByte PW  to build Header */
eAtRet Tha6029PwKbytePwTxEthTypeSet(AtPw self, uint16 ethType);
uint16 Tha6029PwKbyteTxEthTypeGet(AtPw self);
eAtRet Tha6029PwKbytePwExpectedEthTypeSet(AtPw self, uint16 ethType);
uint16 Tha6029PwKbytePwExpectedEthTypeGet(AtPw self);
eAtRet Tha6029PwKbyteExpectedTypeSet(AtPw self, uint8 type);
eAtRet Tha6029PwKbyteTxTypeSet(AtPw self, uint8 type);
uint8 Tha6029PwKbyteTxTypeGet(AtPw self);
eAtRet Tha6029PwKbyteTxVersionSet(AtPw self, uint8 version);
uint8 Tha6029PwKbyteTxVersionGet(AtPw self);
eAtRet Tha6029PwKbyteExpectedVersionSet(AtPw self, uint8 version);

uint32 Tha60290021PwKByteBaseAddress(AtPw self);
eBool Tha60290021ModulePwDccKbyteInterruptIsSupported(Tha60290021ModulePw self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPW_H_ */


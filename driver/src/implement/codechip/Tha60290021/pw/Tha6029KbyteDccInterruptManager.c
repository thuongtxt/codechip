/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha6029KbyteDccInterruptManager.c
 *
 * Created Date: Apr 26, 2017
 *
 * Description : MRO Kbyte/DCC interrupt manager.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "Tha60290021DccKbyteV2Reg.h"
#include "Tha6029KbyteDccInterruptManager.h"
#include "Tha6029KbyteDccInterruptManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_global_interrupt_mask_enable_DCCKIntEnable_Mask        cBit19

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptManagerMethods  m_AtInterruptManagerOverride;
static tThaInterruptManagerMethods m_ThaInterruptManagerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePw PwModule(AtInterruptManager self)
    {
    return (ThaModulePw)AtInterruptManagerModuleGet(self);
    }

static ThaModuleOcn OcnModule(AtInterruptManager self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)PwModule(self)), cThaModuleOcn);
    }

static AtModuleSdh SdhModule(AtInterruptManager self)
    {
    return (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)PwModule(self)), cAtModuleSdh);
    }

static AtModuleEth EthModule(AtInterruptManager self)
    {
    return (AtModuleEth)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)PwModule(self)), cAtModuleEth);
    }

static void EthKByteInterruptProcess(AtInterruptManager self)
    {
    AtChannel pw = (AtChannel)ThaModulePwKBytePwGet(PwModule(self));
    if (pw)
        AtChannelInterruptProcess(pw, 0);
    }

static void EthDccInterruptProcess(AtInterruptManager self)
    {
    AtChannel ethPort = (AtChannel)ThaModuleEthDccEthPortGet((ThaModuleEth)EthModule(self));
    if (ethPort)
        AtChannelInterruptProcess(ethPort, 0);
    }

static AtHdlcChannel HdlcChannelGet(AtModuleSdh module, uint32 channelId)
    {
    static const uint32 dccLayers[] = {cAtSdhLineDccLayerSection, cAtSdhLineDccLayerLine};
    AtSdhLine line = AtModuleSdhLineGet(module, (uint8)(channelId / 2));
    return AtSdhLineDccChannelGet(line, dccLayers[(uint32)(channelId % 2)]);
    }

static void HdlcDccInterruptProcess(AtInterruptManager self, AtHal hal)
    {
    const uint32 baseAddress = ThaInterruptManagerBaseAddress((ThaInterruptManager)self);
    AtModuleSdh sdhModule = SdhModule(self);
    uint32 groupIntr = AtHalRead(hal, baseAddress + cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_sta);
    uint32 groupIntrEn = AtHalRead(hal, baseAddress + cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_en);
    uint8 group_i, channel_i;

    groupIntr &= groupIntrEn;
    for (group_i = 0; group_i < 2; group_i++)
        {
        if (groupIntr & cIteratorMask(group_i))
            {
            uint32 group32Intr = AtHalRead(hal, (uint32)(baseAddress + cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_or_sta_Base + group_i));

            for (channel_i = 0; channel_i < 32; channel_i++)
                {
                if (group32Intr & cIteratorMask(channel_i))
                    {
                    uint32 channelId = (uint32)(group_i * 32 + channel_i);
                    AtChannel channel = (AtChannel)HdlcChannelGet(sdhModule, channelId);
                    AtChannel pw;

                    if (channel == NULL)
                        {
                        AtModuleLog((AtModule)PwModule(self), cAtLogLevelCritical,
                                    AtSourceLocation, "NULL DCC/HDLC channel (%d) trapped by interrupt process\r\n", channelId);
                        continue;
                        }

                    /* HDLC channel */
                    AtChannelInterruptProcess(channel, channelId);

                    /* HDLC PW channel */
                    pw = (AtChannel)AtChannelBoundPwGet((AtChannel)channel);
                    if (pw)
                        AtChannelInterruptProcess(pw, 0);
                    }
                }
            }
        }
    }

static void InterruptProcess(AtInterruptManager self, uint32 glbIntr, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    const uint32 baseAddress = ThaInterruptManagerBaseAddress((ThaInterruptManager)self);
    uint32 intrEnable = AtHalRead(hal, baseAddress + cAf6Reg_upen_glb_intenb);
    uint32 intrStatus = AtHalRead(hal, baseAddress + cAf6Reg_upen_glbint_stt);
    AtUnused(glbIntr);

    intrStatus &= intrEnable;

    /* ETH Kbyte classifying */
    if (intrStatus & cAf6_upen_glbint_stt_kbyte_interrupt_Mask)
        EthKByteInterruptProcess(self);

    /* ETH DCC classifying */
    if (intrStatus & cAf6_upen_glbint_stt_dcc_interrupt_Mask)
        EthDccInterruptProcess(self);

    /* HDLC/DCC channels */
    if (intrStatus & cAf6_upen_glbint_stt_hdlc_interrupt_Mask)
        HdlcDccInterruptProcess(self, hal);
    }

static eBool HasInterrupt(AtInterruptManager self, uint32 glbIntr, AtHal hal)
    {
    AtUnused(self);
    AtUnused(hal);
    return (glbIntr & cAf6_global_interrupt_mask_enable_DCCKIntEnable_Mask) ? cAtTrue : cAtFalse;
    }

static uint32 BaseAddress(ThaInterruptManager self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule((AtInterruptManager)self));
    }

static void InterruptHwEnable(ThaInterruptManager self, eBool enable, AtIpCore ipCore)
    {
    AtHal hal = AtIpCoreHalGet(ipCore);
    uint32 baseAddress = ThaInterruptManagerBaseAddress((ThaInterruptManager)self);

    if (hal == NULL)
        return;

    /* Global Kbyte/DCC */
    AtHalWrite(hal, baseAddress + cAf6Reg_upen_glb_intenb, (enable) ? 0x7 : 0);
    AtHalWrite(hal, baseAddress + cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pgrp_or_en, (enable) ? 0x3 : 0x0);
    }

static void OverrideAtInterruptManager(AtInterruptManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptManagerOverride, mMethodsGet(self), sizeof(m_AtInterruptManagerOverride));

        mMethodOverride(m_AtInterruptManagerOverride, InterruptProcess);
        mMethodOverride(m_AtInterruptManagerOverride, HasInterrupt);
        }

    mMethodsSet(self, &m_AtInterruptManagerOverride);
    }

static void OverrideThaInterruptManager(AtInterruptManager self)
    {
    ThaInterruptManager manager = (ThaInterruptManager)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaInterruptManagerOverride, mMethodsGet(manager), sizeof(m_ThaInterruptManagerOverride));

        mMethodOverride(m_ThaInterruptManagerOverride, BaseAddress);
        mMethodOverride(m_ThaInterruptManagerOverride, InterruptHwEnable);
        }

    mMethodsSet(manager, &m_ThaInterruptManagerOverride);
    }

static void Override(AtInterruptManager self)
    {
    OverrideAtInterruptManager(self);
    OverrideThaInterruptManager(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029KbyteDccInterruptManager);
    }

AtInterruptManager Tha6029KbyteDccInterruptManagerObjectInit(AtInterruptManager self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaInterruptManagerObjectInit(self, module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptManager Tha6029KbyteDccInterruptManagerNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptManager newManager = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newManager == NULL)
        return NULL;

    /* Construct it */
    return Tha6029KbyteDccInterruptManagerObjectInit(newManager, module);
    }

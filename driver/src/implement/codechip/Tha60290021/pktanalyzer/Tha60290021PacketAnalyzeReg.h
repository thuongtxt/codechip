/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha60290021PacketAnalyzeReg.h
 * 
 * Created Date: Nov 25, 2016
 *
 * Description : Packet analyzer register definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021PACKETANALYZEREG_H_
#define _THA60290021PACKETANALYZEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

/*------------------------------------------------------------------------------
Reg Name   : Mac 40G dump direction control
Reg Addr   : 0x0000804
Reg Formula: 0x0000804
    Where  :
Reg Desc   :
This register configures direction to dump packet
------------------------------------------------------------------------------*/
#define cAf6Reg_ramdumtypecfg                                         								 0x0000804
#define cAf6Reg_ramdumtypecfg_WidthVal                                                                      32

/*--------------------------------------
BitField Name: DirectionDump
BitField Type: RW
BitField Desc: Tx or Rx MAC direction 1: Dump Tx MAC40G 0: Dump Rx MAC40G
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ramdumtypecfg_DirectionDump_Mask                                                            cBit0
#define cAf6_ramdumtypecfg_DirectionDump_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Mac 40G dump data status
Reg Addr   : 0x000000 - 0x0003FF #The address format for these registers is 0x000000 + entry
Reg Formula: 0x000000 +  entry
    Where  :
           + $entry(0-1023): Data entry
Reg Desc   :
This register shows data value to be dump, first entry always contain SOP

------------------------------------------------------------------------------*/
#define cAf6Reg_ramdumpdata                                                                           0x000000
#define cAf6Reg_ramdumpdata_WidthVal                                                                        96

/*--------------------------------------
BitField Name: DumpSop
BitField Type: RO
BitField Desc: Start of packet indication
BitField Bits: [68]
--------------------------------------*/
#define cAf6_ramdumpdata_DumpSop_Mask                                                                    cBit4
#define cAf6_ramdumpdata_DumpSop_Shift                                                                       4

/*--------------------------------------
BitField Name: DumpEoP
BitField Type: RO
BitField Desc: End of packet indication
BitField Bits: [67]
--------------------------------------*/
#define cAf6_ramdumpdata_DumpEoP_Mask                                                                    cBit3
#define cAf6_ramdumpdata_DumpEoP_Shift                                                                       3

/*--------------------------------------
BitField Name: DumpNob
BitField Type: RW
BitField Desc: Number of byte indication 0 means 1 byte
BitField Bits: [66:64]
--------------------------------------*/
#define cAf6_ramdumpdata_DumpNob_Mask                                                                  cBit2_0
#define cAf6_ramdumpdata_DumpNob_Shift                                                                       0

/*--------------------------------------
BitField Name: DumpData
BitField Type: RW
BitField Desc: Data value Bit63_56 is MSB
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_ramdumpdata_DumpData_Mask_01                                                             cBit31_0
#define cAf6_ramdumpdata_DumpData_Shift_01                                                                   0
#define cAf6_ramdumpdata_DumpData_Mask_02                                                             cBit31_0
#define cAf6_ramdumpdata_DumpData_Shift_02                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Mac 40G dump Hold Register Status
Reg Addr   : 0x000800 - 0x000001
Reg Formula: 0x000800 +  HID
    Where  :
           + $HID(0-1): Hold ID
Reg Desc   :
This register using for hold remain that more than 32bits

------------------------------------------------------------------------------*/
#define cAf6Reg_dump_hold_status                                                                      0x000800

/*--------------------------------------
BitField Name: DumpHoldStatus
BitField Type: RW
BitField Desc: Hold 32bits
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_dump_hold_status_DumpHoldStatus_Mask                                                     cBit31_0
#define cAf6_dump_hold_status_DumpHoldStatus_Shift                                                           0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021PACKETANALYZEREG_H_ */


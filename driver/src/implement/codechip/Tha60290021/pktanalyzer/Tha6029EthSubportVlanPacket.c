/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : Tha6029EthSubportVlanPacket.c
 *
 * Created Date: Nov 25, 2016
 *
 * Description : ETH Packet with sub port VLAN
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "Tha6029EthSubportVlanPacketInternal.h"
#include "Tha6029EthSubportVlanPacket.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6029EthSubportVlanPacket)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtEthPacketMethods m_AtEthPacketOverride;

/* Super implementation */
static const tAtObjectMethods    *m_AtObjectMethods    = NULL;
static const tAtEthPacketMethods *m_AtEthPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void DisplaySubportVlanTag(tAtVlanTag *vlanTag, uint8 level)
    {
    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Subport VLAN: TPID = 0x%04X, PCP = %d, CFI = %d, VLAN-ID = %x\r\n",
    		                vlanTag->tpid, vlanTag->pcp, vlanTag->cfi, vlanTag->vlanId);
    }

static void DisplaySubPortVlan(AtEthPacket self, uint16 offset, uint8 level)
	{
	tAtVlanTag vlanTag, *pVlanTag;

	pVlanTag = mMethodsGet(self)->VlanAtOffset(self, offset, &vlanTag);
	if (pVlanTag == NULL)
		return;

	DisplaySubportVlanTag(pVlanTag, level);
	}

static eBool HasCrc(AtEthPacket self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void DisplayVlans(AtEthPacket self, uint8 level)
    {
    AtEthPacket packet = (AtEthPacket)self;

    if (mThis(self)->withSubportVlan)
        {
        uint8 preambleLen = AtEthPacketPreambleLen(packet);
        DisplaySubPortVlan(packet, (uint16)(preambleLen + cMacLength * 2), level);
        }

    m_AtEthPacketMethods->DisplayVlans(self, level);
    }

static uint16 FirstVlanOffset(AtEthPacket self)
	{
	if (mThis(self)->withSubportVlan)
		return (uint16)(m_AtEthPacketMethods->FirstVlanOffset(self) + cSubportVlanByteNum);

	return m_AtEthPacketMethods->FirstVlanOffset(self);
	}

static uint32 SubportVlanOffset(AtEthPacket self)
    {
    return (uint32)(AtEthPacketPreambleLen(self) + cMacLength * 2);
    }

static eBool ShouldCheckVlanTpid(AtEthPacket self, uint32 offset)
    {
    if (mThis(self)->withSubportVlan && (offset == SubportVlanOffset(self)))
        return cAtFalse;

    return m_AtEthPacketMethods->ShouldCheckVlanTpid(self, offset);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha6029EthSubportVlanPacket object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(withSubportVlan);
    }

static void OverrideAtObject(AtPacket self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtEthPacket(AtPacket self)
    {
    AtEthPacket ethPacket = (AtEthPacket)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPacketMethods = mMethodsGet(ethPacket);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPacketOverride, m_AtEthPacketMethods, sizeof(m_AtEthPacketOverride));

        mMethodOverride(m_AtEthPacketOverride, FirstVlanOffset);
        mMethodOverride(m_AtEthPacketOverride, HasCrc);
        mMethodOverride(m_AtEthPacketOverride, DisplayVlans);
        mMethodOverride(m_AtEthPacketOverride, ShouldCheckVlanTpid);
        }

    mMethodsSet(ethPacket, &m_AtEthPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtObject(self);
    OverrideAtEthPacket(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029EthSubportVlanPacket);
    }

AtPacket Tha6029EthSubportVlanPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eBool withSubportVlan)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEthPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->withSubportVlan = withSubportVlan;

    return self;
    }

AtPacket Tha6029EthSubportVlanPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eBool withSubportVlan)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6029EthSubportVlanPacketObjectInit(newPacket, dataBuffer, length, cacheMode, withSubportVlan);
    }

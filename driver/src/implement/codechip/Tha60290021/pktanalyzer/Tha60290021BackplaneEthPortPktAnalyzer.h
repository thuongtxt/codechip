/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha60290021BackplaneEthPortPktAnalyzer.h
 * 
 * Created Date: Nov 25, 2016
 *
 * Description : Packet analyzer for 40G port
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021BACKPLANEETHPORTPKTANALYZER_H_
#define _THA60290021BACKPLANEETHPORTPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/pktanalyzer/ThaStmPwProductEthPortPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021BackplaneEthPortPktAnalyzer * Tha60290021BackplaneEthPortPktAnalyzer;

typedef struct tTha60290021BackplaneEthPortPktAnalyzer
	{
	tThaStmPwProductEthPortPktAnalyzer super;
	}tTha60290021BackplaneEthPortPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer Tha60290021BackplaneEthPortPktAnalyzerNew(AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021BACKPLANEETHPORTPKTANALYZER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha60290021EthPortSgmiiPktAnalyzerInternal.h
 * 
 * Created Date: Oct 14, 2016
 *
 * Description : SGMII packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021ETHPORTSGMIIPKTANALYZERINTERNAL_H_
#define _THA60290021ETHPORTSGMIIPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h"
#include "atclib.h"
#include "../../../default/pktanalyzer/ThaEthPortPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021EthPortSgmiiPktAnalyzer * Tha60290021EthPortSgmiiPktAnalyzer;

typedef struct tTha60290021EthPortSgmiiPktAnalyzerMethods
    {
    uint32 (*BufferPointerResetMask)(AtPktAnalyzer self);
    uint32 (*BufferPointerResetShift)(AtPktAnalyzer self);
    }tTha60290021EthPortSgmiiPktAnalyzerMethods;

typedef struct tTha60290021EthPortSgmiiPktAnalyzer
    {
    tThaEthPortPktAnalyzerV2 super;
    const tTha60290021EthPortSgmiiPktAnalyzerMethods * methods;

    /* Private data */
    uint8 pktType;
    }tTha60290021EthPortSgmiiPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer Tha60290021EthPortSgmiiPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021ETHPORTSGMIIPKTANALYZERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha6029EthPacket.c
 *
 * Created Date: Oct 15, 2016
 *
 * Description : DCC Ethernet packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/pktanalyzer/AtEthPacketInternal.h"
#include "packet/Tha6029OverheadPacket.h"
#include "Tha6029SgmiiEthPacket.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha6029SgmiiEthPacket *)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6029SgmiiEthPacket
    {
    tAtEthPacket super;

    /* Private data */
    AtPktAnalyzer analyzer;
    uint16 dccEthType;
    uint16 kbyteEthType;
    }tTha6029SgmiiEthPacket;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods    m_AtObjectOverride;
static tAtPacketMethods    m_AtPacketOverride;
static tAtEthPacketMethods m_AtEthPacketOverride;

/* Super implementation */
static const tAtObjectMethods    *m_AtObjectMethods    = NULL;
static const tAtPacketMethods    *m_AtPacketMethods    = NULL;
static const tAtEthPacketMethods *m_AtEthPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha6029SgmiiEthPacket* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(dccEthType);
    mEncodeUInt(kbyteEthType);
    mEncodeObjectDescription(analyzer);
    }

static eBool HasCrc(AtEthPacket self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool IsDccPacket(AtPacket self, uint16 ethType)
    {
    AtUnused(self);
    return (ethType == mThis(self)->dccEthType) ? cAtTrue : cAtFalse;
    }

static eBool IsKbytePacket(AtPacket self, uint16 ethType)
    {
    AtUnused(self);
    return (ethType == mThis(self)->kbyteEthType) ? cAtTrue : cAtFalse;
    }

static AtPacket PayloadPacketCreate(AtPacket self)
    {
    uint16 ethType = AtEthPacketEthTypeLength((AtEthPacket)self);
    uint32 numBytes;
    uint8 *dataBuffer = AtPacketPayloadBuffer(self, &numBytes);

    if (IsDccPacket(self, ethType))
        return Tha6029OverheadPacketNew(dataBuffer, numBytes, cAtPacketCacheModeNoCache);

    if (IsKbytePacket(self, ethType))
        return Tha6029KBytePacketNew(dataBuffer, numBytes, cAtPacketCacheModeNoCache);

    return NULL;
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
    AtPacket payload = PayloadPacketCreate(self);

    if (payload == NULL)
        {
        m_AtPacketMethods->DisplayPayload(self, level);
        return;
        }

    AtPacketPrintSpaces(level);
    AtPrintc(cSevNormal, "* Payload: \r\n");
    AtPacketDisplayAtLevel(payload, (uint8)(level + 1));
    AtObjectDelete((AtObject)payload);
    }

static uint8 LabelFromMac(uint8 *mac)
    {
    return mac[5] & cBit4_0;
    }

static void DisplayMacContent(AtEthPacket self, const char *name, uint8 *mac)
    {
    if (IsKbytePacket((AtPacket)self, AtEthPacketEthTypeLength(self)))
        {
        m_AtEthPacketMethods->DisplayMacContent(self, name, mac);
        return;
        }

    /* Only display label for DCC packets */
    AtPacketDisplayBufferInHex((AtPacket)self, mac, cMacLength, ".");
    if (AtStrcmp(name, sAtEthPacketFieldDMAC) == 0)
        AtPrintf(" (Label: 0x%x)", LabelFromMac(mac));
    }

static eAtRet DccEthTypeSet(AtPacket self, uint16 ethType)
    {
    mThis(self)->dccEthType = ethType;
    return cAtOk;
    }

static eAtRet KbyteEthTypeSet(AtPacket self, uint16 ethType)
    {
    mThis(self)->kbyteEthType = ethType;
    return cAtOk;
    }

static uint8 DisplayPreamble(AtEthPacket self, uint8 level)
    {
    AtUnused(self);
    AtUnused(level);
    return 0;
    }

static uint8 PreambleLen(AtEthPacket self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideAtEthPacket(AtPacket self)
    {
    AtEthPacket packet = (AtEthPacket)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEthPacketMethods = mMethodsGet(packet);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEthPacketOverride, m_AtEthPacketMethods, sizeof(m_AtEthPacketOverride));

        mMethodOverride(m_AtEthPacketOverride, HasCrc);
        mMethodOverride(m_AtEthPacketOverride, DisplayMacContent);
        mMethodOverride(m_AtEthPacketOverride, DisplayPreamble);
        mMethodOverride(m_AtEthPacketOverride, PreambleLen);
        }

    mMethodsSet(packet, &m_AtEthPacketOverride);
    }

static void OverrideAtObject(AtPacket self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPacket(AtPacket self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        }

    mMethodsSet(self, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtObject(self);
    OverrideAtPacket(self);
    OverrideAtEthPacket(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SgmiiEthPacket);
    }

static AtPacket ObjectInit(AtPacket self, AtPktAnalyzer analyzer, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEthPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->analyzer = analyzer;
    mThis(self)->dccEthType = 0x880B;
    mThis(self)->kbyteEthType = 0x88B6;

    return self;
    }

AtPacket Tha6029SgmiiEthPacketNew(AtPktAnalyzer analyzer, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newPacket, analyzer, dataBuffer, length, cacheMode);
    }

eAtRet Tha6029SgmiiEthPacketDccEthTypeSet(AtPacket self, uint16 ethType)
    {
    if (self)
        return DccEthTypeSet(self, ethType);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha6029SgmiiEthPacketKbyteEthTypeSet(AtPacket self, uint16 ethType)
    {
    if (self)
        return KbyteEthTypeSet(self, ethType);
    return cAtErrorObjectNotExist;
    }

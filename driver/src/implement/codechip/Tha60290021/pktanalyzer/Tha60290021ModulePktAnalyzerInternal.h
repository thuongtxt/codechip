/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PktAnalyzer
 * 
 * File        : Tha60290021ModulePktAnalyzerInternal.h
 * 
 * Created Date: Jan 4, 2018
 *
 * Description : Internal data for the PktAnalyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPKTANALYZERINTERNAL_H_
#define _THA60290021MODULEPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/pktanalyzer/Tha60210011ModulePktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModulePktAnalyzer
    {
    tTha60210011ModulePktAnalyzer super;
    }tTha60290021ModulePktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePktAnalyzer Tha60290021ModulePktAnalyzerObjectInit(AtModulePktAnalyzer self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPKTANALYZERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : Tha60290021BackplaneEthPortPktAnalyzer.c
 *
 * Created Date: Nov 25, 2016
 *
 * Description : Packet analyzer for 40G port
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/pktanalyzer/AtPacketUtil.h"
#include "../../../default/pktanalyzer/ThaPktAnalyzerInternal.h"
#include "../pw/Tha60290021ModulePw.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "Tha60290021BackplaneEthPortPktAnalyzer.h"
#include "Tha60290021PacketAnalyzeReg.h"
#include "Tha6029EthSubportVlanPacket.h"

/*--------------------------- Define -----------------------------------------*/
#define cThaBufferMaxOffset 0x3FFUL

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods  m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(AtPktAnalyzer self)
    {
    AtChannel channel = AtPktAnalyzerChannelGet(self);
    return AtChannelDeviceGet(channel);
    }

static ThaModulePw ModulePw(AtPktAnalyzer self)
    {
    return (ThaModulePw) AtDeviceModuleGet(Device(self), cAtModulePw);
    }

static AtModuleEth ModuleEth(AtPktAnalyzer self)
    {
    return (AtModuleEth) AtDeviceModuleGet(Device(self), cAtModuleEth);
    }

static uint8 MetaDwordIndex(ThaPktAnalyzer self)
	{
	AtUnused(self);
	return 2;
	}

static uint32 StartAddress(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return 0x80000;
    }

static uint32 PartOffset(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return 0;
    }

static eBool SkipLastInCompletePacket(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 EthPortHwDumpMode(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
	AtUnused(self);
	return (pktDumpMode == cThaPktAnalyzerDumpGbeTx) ? 1 : 0;
    }

static uint32 EthPortDumpModeHw2Sw(ThaPktAnalyzer self, uint32 pktDumpMode)
    {
	AtUnused(self);
    return (pktDumpMode == 0x1) ? cThaPktAnalyzerDumpGbeTx : cThaPktAnalyzerDumpGbeRx;
    }

static eBool ShouldRecapture(ThaPktAnalyzer self, uint8 newPktDumpMode, uint8 currentDumpMode)
    {
	AtUnused(self);
	if ((currentDumpMode != newPktDumpMode) && (currentDumpMode != cThaPktAnalyzerDumpUnknown))
		return cAtTrue;

	return cAtFalse;
    }

static void Recapture(AtPktAnalyzer self)
    {
	AtUnused(self);
    }

static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    uint32 baseAddress = ThaPktAnalyzerBaseAddress(self);
    ThaPktAnalyzerWrite(self, cAf6Reg_ramdumtypecfg + baseAddress, EthPortHwDumpMode(self, pktDumpMode));
    }

static uint32 DumpModeGet(ThaPktAnalyzer self)
    {
    uint32 regVal = ThaPktAnalyzerRead(self, cAf6Reg_ramdumtypecfg + ThaPktAnalyzerBaseAddress(self));
    uint32 pktDumpMode = mRegField(regVal, cAf6_ramdumtypecfg_DirectionDump_);

    return EthPortDumpModeHw2Sw(self, pktDumpMode);
    }

static uint32 FirstAddressOfBuffer(ThaPktAnalyzer self)
    {
    return (ThaPktAnalyzerBaseAddress(self) + cAf6Reg_ramdumpdata);
    }

static uint32 LastAddressOfBuffer(ThaPktAnalyzer self)
    {
    return (FirstAddressOfBuffer(self) + cThaBufferMaxOffset);
    }

static void LongRead(ThaPktAnalyzer self, uint32 address, uint32 *longValue)
    {
    AtDevice device = AtChannelDeviceGet(AtPktAnalyzerChannelGet((AtPktAnalyzer)self));
    AtModule module = AtDeviceModuleGet(device, cAtModulePktAnalyzer);
    AtIpCore core = AtDeviceIpCoreGet(device, AtModuleDefaultCoreGet(module));
    mModuleHwLongRead(module, address, longValue, cThaLongRegMaxSize, core);
    }

static eBool IsStartOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    uint32 longVal[cThaLongRegMaxSize];
	AtUnused(data);
    LongRead(self, address, longVal);
    return (longVal[mMetaDwordIndex(self)] & cAf6_ramdumpdata_DumpSop_Mask) ? cAtTrue : cAtFalse;
    }

static eBool IsEndOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    uint32 longVal[cThaLongRegMaxSize];
	AtUnused(data);
    LongRead(self, address, longVal);
    return (longVal[mMetaDwordIndex(self)] & cAf6_ramdumpdata_DumpEoP_Mask) ? cAtTrue : cAtFalse;
    }

static uint8 EntryRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
	LongRead(self, address, data);
    return (uint8)(mRegField(data[mMetaDwordIndex(self)], cAf6_ramdumpdata_DumpNob_) + 1);
    }

static eBool IsPwSubPortVlanTpId(AtPktAnalyzer self, uint16 tpid, eAtRet (*SubportVlanGet)(ThaModulePw self, uint32 subPortVlanIndex, tAtVlan *vlan))
    {
    uint32 vlan_i;
    ThaModulePw pwModule = ModulePw(self);

    for (vlan_i = 0; vlan_i < ThaModulePwNumSubPortVlans(pwModule); vlan_i++)
        {
        tAtVlan vlan;
        eAtRet ret = SubportVlanGet(pwModule, vlan_i, &vlan);
        if ((ret == cAtOk) && (vlan.tpid == tpid))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool IsEthPassThroughSubPortVlanTpId(AtPktAnalyzer self, uint16 tpid,
                                             uint16 (*SubPortVlanTransmitTpidGet)(AtModuleEth self, uint8 tpidIndex))
    {
    uint8 tpid_i;
    AtModuleEth ethModule = ModuleEth(self);

    for (tpid_i = 0; tpid_i < Tha60290021ModuleEthNumSubPortVlanTpids(ethModule); tpid_i++)
        {
        if (SubPortVlanTransmitTpidGet(ethModule, tpid_i) == tpid)
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool IsTxSubPortVlanTpId(AtPktAnalyzer self, uint16 tpid)
    {
    if (IsPwSubPortVlanTpId(self, tpid, ThaModulePwTxSubportVlanGet))
        return cAtTrue;
    return IsEthPassThroughSubPortVlanTpId(self, tpid, Tha60290021ModuleEthSubPortVlanTransmitTpidGet);
    }

static eBool IsRxSubPortVlanTpId(AtPktAnalyzer self, uint16 tpid)
    {
    if (IsPwSubPortVlanTpId(self, tpid, ThaModulePwExpectedSubportVlanGet))
        return cAtTrue;
    return IsEthPassThroughSubPortVlanTpId(self, tpid, Tha60290021ModuleEthSubPortVlanExpectedTpidGet);
    }

static eBool HasSubPortVlan(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    uint16 firstVlanOffset = AtPktUtilEthFirstVlanOffset(data, length);
    uint16 tpid;

    if (firstVlanOffset == cInvalidUint16)
        return cAtFalse;

    tpid = (uint16)((data[firstVlanOffset] << 8) | data[firstVlanOffset + 1]);
    if (direction == cAtPktAnalyzerDirectionRx)
        return IsRxSubPortVlanTpId(self, tpid);

    if (direction == cAtPktAnalyzerDirectionTx)
        return IsTxSubPortVlanTpId(self, tpid);

    return cAtFalse;
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
	{
    eBool withSubportVlan = HasSubPortVlan(self, data, length, direction);
    AtUnused(self);
    AtUnused(direction);
    return Tha6029EthSubportVlanPacketNew(data, length, cAtPacketCacheModeCacheData, withSubportVlan);
	}

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, Recapture);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, mMethodsGet(analyzer), sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, StartAddress);
        mMethodOverride(m_ThaPktAnalyzerOverride, SkipLastInCompletePacket);
        mMethodOverride(m_ThaPktAnalyzerOverride, PartOffset);
        mMethodOverride(m_ThaPktAnalyzerOverride, EthPortHwDumpMode);
		mMethodOverride(m_ThaPktAnalyzerOverride, EthPortDumpModeHw2Sw);
        mMethodOverride(m_ThaPktAnalyzerOverride, ShouldRecapture);
        mMethodOverride(m_ThaPktAnalyzerOverride, FirstAddressOfBuffer);
        mMethodOverride(m_ThaPktAnalyzerOverride, LastAddressOfBuffer);
        mMethodOverride(m_ThaPktAnalyzerOverride, IsStartOfPacket);
        mMethodOverride(m_ThaPktAnalyzerOverride, IsEndOfPacket);
        mMethodOverride(m_ThaPktAnalyzerOverride, EntryRead);
        mMethodOverride(m_ThaPktAnalyzerOverride, MetaDwordIndex);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeGet);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }
    
static void Override(AtPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021BackplaneEthPortPktAnalyzer);
    }

static AtPktAnalyzer ObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductEthPortPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60290021BackplaneEthPortPktAnalyzerNew(AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPktAnalyzer, port);
    }

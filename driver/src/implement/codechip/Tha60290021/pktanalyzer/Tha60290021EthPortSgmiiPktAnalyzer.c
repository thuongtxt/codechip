/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60290021EthPortSgmiiPktAnalyzer.c
 *
 * Created Date: Oct 11, 2016
 *
 * Description : SGMII Port Analyzer
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/pktanalyzer/ThaEthPortPktAnalyzerInternal.h"
#include "../pw/Tha60290021DccKbyteReg.h"
#include "../pw/Tha60290021ModulePw.h"
#include "../man/Tha60290021Device.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "Tha60290021EthPortSgmiiPktAnalyzerInternal.h"
#include "Tha6029SgmiiEthPacket.h"
#include "Tha60290021ModulePktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumDataLocation 255

#define cDumpInterfaceDcc   0
#define cDumpInterfaceKbyte 1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021EthPortSgmiiPktAnalyzer)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021EthPortSgmiiPktAnalyzerMethods m_methods;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtPktAnalyzerMethods  m_AtPktAnalyzerOverride;
static tThaPktAnalyzerMethods m_ThaPktAnalyzerOverride;


/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;
static const tAtPktAnalyzerMethods  *m_AtPktAnalyzerMethods  = NULL;
static const tThaPktAnalyzerMethods *m_ThaPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice DeviceGet(AtPktAnalyzer self)
    {
    return AtChannelDeviceGet(AtPktAnalyzerChannelGet(self));
    }

static ThaModuleOcn OcnModule(AtPktAnalyzer self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(DeviceGet(self), cThaModuleOcn);
    }

static AtIpCore Core(AtPktAnalyzer self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static eBool IsCaptureKbyteApsPacket(AtPktAnalyzer self)
    {
    return (mThis(self)->pktType ==  cAtPktAnalyzerSohPktTypeKbyteAps) ? cAtTrue: cAtFalse;
    }

static eBool IsCaptureDccPacket(AtPktAnalyzer self)
    {
    return (mThis(self)->pktType ==  cAtPktAnalyzerSohPktTypeDcc) ? cAtTrue: cAtFalse;
    }

static uint32 BufferPointerResetMask(AtPktAnalyzer self)
    {
    if (IsCaptureDccPacket(self))
        return cAf6_upen_trig_encap_enb_cap_dcc_Mask;

    if (IsCaptureKbyteApsPacket(self))
        return cAf6_upen_trig_encap_enb_cap_aps_Mask;

    return 0;
    }

static uint32 BufferPointerResetShift(AtPktAnalyzer self)
    {
    if (IsCaptureDccPacket(self))
        return cAf6_upen_trig_encap_enb_cap_dcc_Shift;

    if (IsCaptureKbyteApsPacket(self))
        return cAf6_upen_trig_encap_enb_cap_aps_Shift;

    return 0;
    }

static uint32 AddressWithLocalAddress(AtPktAnalyzer self, uint32 localAddress)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self)) + localAddress;
    }

static uint32 BufferControlAddress(AtPktAnalyzer self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_upen_trig_encap_Base);
    }

static void HwBufferFlush(AtPktAnalyzer self)
    {
    AtChannel channel = AtPktAnalyzerChannelGet(self);
    uint32 regAddr = BufferControlAddress(self);
    mChannelHwWrite(channel, regAddr, 0, cAtModuleSdh);
    mChannelHwWrite(channel, regAddr, cAf6_upen_trig_encap_ram_trig_fls_Mask, cAtModuleSdh);
    mChannelHwWrite(channel, regAddr, 0, cAtModuleSdh);
    }

static eBool PacketPointerReset(AtPktAnalyzer self)
    {
    AtChannel channel;
    uint32 regVal, regAddr;
    uint32 resetMask = mMethodsGet(mThis(self))->BufferPointerResetMask(self);
    uint32 resetShift = mMethodsGet(mThis(self))->BufferPointerResetShift(self);

    if (resetMask == 0)
        return cAtFalse;

    channel = AtPktAnalyzerChannelGet(self);
    regAddr = BufferControlAddress(self);
    regVal =  mChannelHwRead(channel, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, reset, 0);
    mChannelHwWrite(channel, regAddr, regVal, cAtModuleSdh);

    mRegFieldSet(regVal, reset, 1);
    mChannelHwWrite(channel, regAddr, regVal, cAtModuleSdh);

    /* Give HW a moment */
    AtOsalUSleep(100000);

    /* Success-indication */
    return cAtTrue;
    }

static AtModulePw PwModule(AtPktAnalyzer self)
    {
    AtDevice device = AtChannelDeviceGet(AtPktAnalyzerChannelGet(self));
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
    AtPacket newPacket = Tha6029SgmiiEthPacketNew(self, data, length, cAtPacketCacheModeCacheData);
    AtPw pw;

    AtUnused(direction);

    pw = ThaModulePwKBytePwGet((ThaModulePw)PwModule(self));
    if (pw)
        Tha6029SgmiiEthPacketKbyteEthTypeSet(newPacket, Tha6029PwKbytePwExpectedEthTypeGet(pw));

    Tha6029SgmiiEthPacketDccEthTypeSet(newPacket, cThaDccPwEthTypeDefault);

    return newPacket;
    }

static eBool IsValidPacketType(uint8 pktType)
    {
    switch(pktType)
        {
        case cAtPktAnalyzerSohPktTypeDcc:      return cAtTrue;
        case cAtPktAnalyzerSohPktTypeKbyteAps: return cAtTrue;
        default: return cAtFalse;
        }
    }

static eAtRet PacketTypeSet(AtPktAnalyzer self, uint8 pktType)
    {
    if (!IsValidPacketType(pktType))
        return cAtErrorModeNotSupport;

    mThis(self)->pktType = pktType;
    return cAtOk;
    }

static void MethodsInit(AtPktAnalyzer self)
    {
    Tha60290021EthPortSgmiiPktAnalyzer sgmiiPktAnalyzer = (Tha60290021EthPortSgmiiPktAnalyzer)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, BufferPointerResetMask);
        mMethodOverride(m_methods, BufferPointerResetShift);
        }

    mMethodsSet(sgmiiPktAnalyzer, &m_methods);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290021EthPortSgmiiPktAnalyzer object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(pktType);
    }

static uint16 LongRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    ThaModuleOcn ocnModule = OcnModule((AtPktAnalyzer)self);
    AtIpCore core = Core((AtPktAnalyzer)self);
    return mModuleHwLongRead(ocnModule, address, data, cThaLongRegMaxSize, core);
    }

static eBool GetIndication(ThaPktAnalyzer self, uint32 address, uint32 *data, uint32 mask)
    {
    AtUnused(address);
    return (data[mMetaDwordIndex(self)] & mask) ? cAtTrue : cAtFalse;
    }

static eBool IsStartOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    return GetIndication(self, address, data, cAf6_upen_pktcap_cap_sop_Mask);
    }

static eBool IsEndOfPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    return GetIndication(self, address, data, cAf6_upen_pktcap_cap_eop_Mask);
    }

static eBool IsErrorPacket(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    return GetIndication(self, address, data, cAf6_upen_pktcap_cap_err_Mask);
    }

static uint8 MetaDwordIndex(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return 2;
    }

static uint8 EntryRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    AtUnused(address);
    return (uint8)(mRegField(data[mMetaDwordIndex(self)], cAf6_upen_pktcap_cap_nob_) + 1);
    }

static void Flush(AtPktAnalyzer self)
    {
    HwBufferFlush(self);
    PacketPointerReset(self);
    }

static void AllRxPacketsRead(ThaPktAnalyzer self)
    {
    AtPktAnalyzerPacketFlush((AtPktAnalyzer)self); /* As auto flush is disabled */
    m_ThaPktAnalyzerMethods->AllRxPacketsRead(self);
    Flush((AtPktAnalyzer)self); /* For next analyzing */
    }

static uint32 Read(AtPktAnalyzer self, uint32 regAddr, uint32 moduleId)
    {
    AtChannel channel = AtPktAnalyzerChannelGet(self);
    return mChannelHwRead(channel, regAddr, moduleId);
    }

static void Write(AtPktAnalyzer self, uint32 regAddr, uint32 regVal, uint32 moduleId)
    {
    AtChannel channel = AtPktAnalyzerChannelGet(self);
    mChannelHwWrite(channel, regAddr, regVal, moduleId);
    }

static void DumpTxInterfaceSet(ThaPktAnalyzer self, uint32 interface)
    {
    uint32 regAddr = AddressWithLocalAddress((AtPktAnalyzer)self, cAf6Reg_upen_loopen_Base);
    uint32 regVal = Read((AtPktAnalyzer)self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_upen_loopen_sgmii_cap_tx_DCC_KByte_, interface);
    Write((AtPktAnalyzer)self, regAddr, regVal, cAtModuleEth);
    }

static uint32 DumpTxInterfaceGet(ThaPktAnalyzer self)
    {
    uint32 regAddr = AddressWithLocalAddress((AtPktAnalyzer)self, cAf6Reg_upen_loopen_Base);
    uint32 regVal = Read((AtPktAnalyzer)self, regAddr, cAtModuleEth);
    return mRegField(regVal, cAf6_upen_loopen_sgmii_cap_tx_DCC_KByte_);
    }

static eBool IsSimulated(ThaPktAnalyzer self)
    {
    AtDevice device = AtChannelDeviceGet(AtPktAnalyzerChannelGet((AtPktAnalyzer)self));
    return AtDeviceIsSimulated(device);
    }

static void AllTxPacketsRead(ThaPktAnalyzer self)
    {
    uint32 otherInterface;

    AtPktAnalyzerPacketFlush((AtPktAnalyzer)self); /* As auto flush is disabled */

    /* Just continue analyzing current interface */
    m_ThaPktAnalyzerMethods->AllTxPacketsRead(self);

    /* Switch to other interface */
    otherInterface = (DumpTxInterfaceGet(self) == cDumpInterfaceKbyte) ? cDumpInterfaceDcc : cDumpInterfaceKbyte;
    DumpTxInterfaceSet(self, otherInterface);
    Flush((AtPktAnalyzer)self);

    /* Give HW a moment to have new captured packets */
    if (!IsSimulated(self))
        AtOsalSleep(1);

    /* Then analyze */
    m_ThaPktAnalyzerMethods->AllTxPacketsRead(self);
    }

static eBool DumpModeIsSupported(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    AtUnused(self);
    if ((pktDumpMode == cThaPktAnalyzerDumpGbeRx) ||
        (pktDumpMode == cThaPktAnalyzerDumpGbeTx))
        return cAtTrue;
    return cAtFalse;
    }

static void DumpModeSet(ThaPktAnalyzer self, eThaPktAnalyzerPktDumpMode pktDumpMode)
    {
    uint32 regAddr = AddressWithLocalAddress((AtPktAnalyzer)self, cAf6Reg_upen_loopen_Base);
    uint32 regVal = Read((AtPktAnalyzer)self, regAddr, cAtModuleEth);
    uint32 hwDumpMode = (pktDumpMode == cThaPktAnalyzerDumpGbeRx) ? 0 : 1;
    mRegFieldSet(regVal, cAf6_upen_loopen_sgmii_cap_select_, hwDumpMode);
    Write((AtPktAnalyzer)self, regAddr, regVal, cAtModuleEth);
    }

static uint32 FirstAddressOfBuffer(ThaPktAnalyzer self)
    {
    return AddressWithLocalAddress((AtPktAnalyzer)self, cAf6Reg_upen_pktcap_Base);
    }

static uint32 LastAddressOfBuffer(ThaPktAnalyzer self)
    {
    return FirstAddressOfBuffer(self) + cNumDataLocation;
    }

static uint16 EntryInfoRead(ThaPktAnalyzer self, uint32 address, uint32 *data)
    {
    return ThaPktAnalyzerLongRead(self, address, data);
    }

static void Init(AtPktAnalyzer self)
    {
    m_AtPktAnalyzerMethods->Init(self);
    PacketPointerReset(self);
    }

static eBool AutoFlushPackets(ThaPktAnalyzer self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaPktAnalyzer(AtPktAnalyzer self)
    {
    ThaPktAnalyzer analyzer = (ThaPktAnalyzer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPktAnalyzerMethods = mMethodsGet(analyzer);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPktAnalyzerOverride, m_ThaPktAnalyzerMethods, sizeof(m_ThaPktAnalyzerOverride));

        mMethodOverride(m_ThaPktAnalyzerOverride, IsStartOfPacket);
        mMethodOverride(m_ThaPktAnalyzerOverride, IsEndOfPacket);
        mMethodOverride(m_ThaPktAnalyzerOverride, IsErrorPacket);
        mMethodOverride(m_ThaPktAnalyzerOverride, MetaDwordIndex);
        mMethodOverride(m_ThaPktAnalyzerOverride, EntryRead);
        mMethodOverride(m_ThaPktAnalyzerOverride, LongRead);
        mMethodOverride(m_ThaPktAnalyzerOverride, AllRxPacketsRead);
        mMethodOverride(m_ThaPktAnalyzerOverride, AllTxPacketsRead);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeIsSupported);
        mMethodOverride(m_ThaPktAnalyzerOverride, DumpModeSet);
        mMethodOverride(m_ThaPktAnalyzerOverride, FirstAddressOfBuffer);
        mMethodOverride(m_ThaPktAnalyzerOverride, LastAddressOfBuffer);
        mMethodOverride(m_ThaPktAnalyzerOverride, EntryInfoRead);
        mMethodOverride(m_ThaPktAnalyzerOverride, AutoFlushPackets);
        }

    mMethodsSet(analyzer, &m_ThaPktAnalyzerOverride);
    }

static void OverrideAtObject(AtPktAnalyzer self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, m_AtPktAnalyzerMethods, sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        mMethodOverride(m_AtPktAnalyzerOverride, PacketTypeSet);
        mMethodOverride(m_AtPktAnalyzerOverride, Init);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtObject(self);
    OverrideAtPktAnalyzer(self);
    OverrideThaPktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021EthPortSgmiiPktAnalyzer);
    }

AtPktAnalyzer Tha60290021EthPortSgmiiPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortPktAnalyzerV2ObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60290021EthPortSgmiiPktAnalyzerNew(AtEthPort port)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021EthPortSgmiiPktAnalyzerObjectInit(newPktAnalyzer, port);
    }

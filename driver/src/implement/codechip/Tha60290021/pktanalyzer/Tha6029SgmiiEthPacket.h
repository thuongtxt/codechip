/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha6029EthPacket.h
 * 
 * Created Date: Oct 15, 2016
 *
 * Description : ETH common packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029ETHPACKET_H_
#define _THA6029ETHPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket Tha6029SgmiiEthPacketNew(AtPktAnalyzer analyzer, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);

eAtRet Tha6029SgmiiEthPacketDccEthTypeSet(AtPacket self, uint16 ethType);
eAtRet Tha6029SgmiiEthPacketKbyteEthTypeSet(AtPacket self, uint16 ethType);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029ETHPACKET_H_ */


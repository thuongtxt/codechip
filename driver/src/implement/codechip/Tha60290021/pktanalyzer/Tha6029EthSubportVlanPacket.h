/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha6029EthSubportVlanPacket.h
 * 
 * Created Date: Nov 29, 2016
 *
 * Description : Subporg VLAN ETH packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029ETHSUBPORTVLANPACKET_H_
#define _THA6029ETHSUBPORTVLANPACKET_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPacket.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket Tha6029EthSubportVlanPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eBool withSubportVlan);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029ETHSUBPORTVLANPACKET_H_ */


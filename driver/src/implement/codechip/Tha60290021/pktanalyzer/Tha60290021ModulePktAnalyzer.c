/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet Analyzer
 *
 * File        : Tha60290021ModulePktAnalyzer.c
 *
 * Created Date: Oct 11, 2016
 *
 * Description : Packet Analyzer of 60290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../eth/Tha60290021ModuleEth.h"
#include "Tha60290021BackplaneEthPortPktAnalyzer.h"
#include "Tha60290021ModulePktAnalyzer.h"
#include "Tha60290021ModulePktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePktAnalyzerMethods m_AtModulePktAnalyzerOverride;
static tAtModuleMethods            m_AtModuleOverride;

/* Save super implementation */
static const tAtModuleMethods            *m_AtModuleMethods            = NULL;
static const tAtModulePktAnalyzerMethods *m_AtModulePktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPktAnalyzer EthPortPktAnalyzerCreate(AtModulePktAnalyzer self, AtEthPort port)
    {
	AtUnused(self);
    if (Tha60290021EthPortIsSgmiiPort(port))
        return Tha60290021EthPortSgmiiPktAnalyzerNew(port);

    return Tha60290021BackplaneEthPortPktAnalyzerNew(port);
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    static uint32 holdRegisters[] = {0x80800, 0x80801};
	AtUnused(self);

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AtModuleHoldRegistersGet(self, &numHoldRegisters);

    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    if (mInRange(localAddress, 0x80000, 0x80801))
        return cAtTrue;

    return m_AtModuleMethods->HasRegister(self, localAddress);
    }

static void OverrideAtModule(AtModulePktAnalyzer self)
    {
	AtModule atModule = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(atModule);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(atModule), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        }

    mMethodsSet(atModule, &m_AtModuleOverride);
    }

static void OverrideAtModulePktAnalyzer(AtModulePktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtModulePktAnalyzerOverride));

        mMethodOverride(m_AtModulePktAnalyzerOverride, EthPortPktAnalyzerCreate);
        }

    mMethodsSet(self, &m_AtModulePktAnalyzerOverride);
    }

static void Override(AtModulePktAnalyzer self)
    {
	OverrideAtModule(self);
    OverrideAtModulePktAnalyzer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModulePktAnalyzer);
    }

AtModulePktAnalyzer Tha60290021ModulePktAnalyzerObjectInit(AtModulePktAnalyzer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModulePktAnalyzerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePktAnalyzer Tha60290021ModulePktAnalyzerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePktAnalyzer newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModulePktAnalyzerObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha60290021ModulePktAnalyzer.h
 * 
 * Created Date: Sep 7, 2017
 *
 * Description : Packet analyzer module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPKTANALYZER_H_
#define _THA60290021MODULEPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pktanalyzer/ThaPktAnalyzer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPacketReader Tha60290021EthPortSgmiiPacketReaderNew(AtPktAnalyzer self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPKTANALYZER_H_ */


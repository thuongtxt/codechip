/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : Tha6029KBytePacket.c
 *
 * Created Date: Sep 7, 2017
 *
 * Description : KByte packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6029OverheadPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumBytesPerChannel 6

#define cChannelIdLabelMask   cBit15_4
#define cChannelIdLabelShift  4
#define cChannelIdPortIdMask  cBit3_0
#define cChannelIdPortIdShift 0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6029KBytePacket
    {
    tTha6029OverheadPacket super;
    }tTha6029KBytePacket;

typedef struct tChannelInfo
    {
    uint16 label;
    uint8 portId;
    uint8 k1;
    uint8 k2;
    uint8 d1;
    }tChannelInfo;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Save super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tChannelInfo *ParseInfo(uint8 *dataBuffer, uint32 numBytes, tChannelInfo *channelInfo)
    {
    uint32 channelId;

    if (numBytes < cNumBytesPerChannel)
        return NULL;

    channelId = (uint16)(dataBuffer[0] << 8 | dataBuffer[1]);
    channelInfo->label = (uint16)mRegField(channelId, cChannelIdLabel);
    channelInfo->portId = mRegField(channelId, cChannelIdPortId);
    channelInfo->k1 = dataBuffer[2];
    channelInfo->k2 = dataBuffer[3];
    channelInfo->d1 = dataBuffer[4];

    return channelInfo;
    }

static void DisplayInfo(AtPacket self, uint32 channelIndex, const tChannelInfo *info, uint8 level)
    {
    AtUnused(self);
    AtPacketPrintSpaces(level);
    AtPrintf("* [%2u] ChannelId[15:4] = 0x%03x, ChannelId[3:0] = 0x%x: K1 = 0x%02x, K2 = 0x%02x, D1 = 0x%02x\r\n",
             channelIndex, info->label, info->portId, info->k1, info->k2, info->d1);
    }

static void DisplayPayload(AtPacket self, uint8 level)
    {
    uint32 remainingBytes;
    uint8 *dataBuffer = AtPacketPayloadBuffer(self, &remainingBytes);
    uint32 channelIndex = 0;

    AtUnused(self);

    while (cAtTrue)
        {
        tChannelInfo info;
        tChannelInfo *pInfo = ParseInfo(dataBuffer, remainingBytes, &info);

        if (pInfo == NULL)
            break;

        DisplayInfo(self, channelIndex, pInfo, level);

        /* Next channel */
        dataBuffer = dataBuffer + cNumBytesPerChannel;
        remainingBytes = remainingBytes - cNumBytesPerChannel;
        channelIndex = channelIndex + 1;
        }
    }

static void OverrideAtPacket(AtPacket self)
    {
    AtPacket packet = (AtPacket)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(packet);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, DisplayPayload);
        }

    mMethodsSet(packet, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029KBytePacket);
    }

static AtPacket ObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029OverheadPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPacket Tha6029KBytePacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPayload = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPayload == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newPayload, dataBuffer, length, cacheMode);
    }

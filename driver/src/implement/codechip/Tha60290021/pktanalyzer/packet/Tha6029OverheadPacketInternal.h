/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha6029OverheadPacketInternal.h
 * 
 * Created Date: Sep 7, 2017
 *
 * Description : Abstract overhead packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029OVERHEADPACKETINTERNAL_H_
#define _THA6029OVERHEADPACKETINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../generic/pktanalyzer/AtPacketInternal.h"
#include "Tha6029OverheadPacket.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029OverheadPacket
    {
    tAtPacket super;
    }tTha6029OverheadPacket;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket Tha6029OverheadPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029OVERHEADPACKETINTERNAL_H_ */


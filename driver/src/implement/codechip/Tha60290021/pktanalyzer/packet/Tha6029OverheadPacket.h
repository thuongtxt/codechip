/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha6029OverheadPacket.h
 * 
 * Created Date: Sep 7, 2017
 *
 * Description : Abstract payload
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029OVERHEAD_H_
#define _THA6029OVERHEAD_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket Tha6029OverheadPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);
AtPacket Tha6029KBytePacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029OVERHEAD_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : Tha6029OverheadPacket.c
 *
 * Created Date: Sep 7, 2017
 *
 * Description : Abstract overhead packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6029OverheadPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPacketMethods m_AtPacketOverride;

/* Save super implementation */
static const tAtPacketMethods *m_AtPacketMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 HeaderLength(AtPacket self)
    {
    AtUnused(self);
    return 4;
    }

static void DisplayHeader(AtPacket self, uint8 level)
    {
    uint32 length;
    uint32 numBytes;
    uint8 *dataBuffer = AtPacketDataBuffer(self, &numBytes);

    if (numBytes < mMethodsGet(self)->HeaderLength(self))
        {
        AtPrintc(cSevWarning, "Cannot display header\r\n");
        return;
        }

    AtPacketPrintHexAttribute(self, "Version", *dataBuffer, level);
    dataBuffer = dataBuffer + 1;

    AtPacketPrintHexAttribute(self, "Type", *dataBuffer, level);
    dataBuffer = dataBuffer + 1;

    length = (uint32)(dataBuffer[0] << 8 | dataBuffer[1]);
    AtPacketPrintAttribute(self, "Length", length, level);
    }

static void OverrideAtPacket(AtPacket self)
    {
    AtPacket packet = (AtPacket)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPacketMethods = mMethodsGet(packet);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPacketOverride, m_AtPacketMethods, sizeof(m_AtPacketOverride));

        mMethodOverride(m_AtPacketOverride, HeaderLength);
        mMethodOverride(m_AtPacketOverride, DisplayHeader);
        }

    mMethodsSet(packet, &m_AtPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideAtPacket(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029OverheadPacket);
    }

AtPacket Tha6029OverheadPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPacket Tha6029OverheadPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPayload = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPayload == NULL)
        return NULL;

    /* Construct it */
    return Tha6029OverheadPacketObjectInit(newPayload, dataBuffer, length, cacheMode);
    }

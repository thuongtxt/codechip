/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet Analyzer
 * 
 * File        : Tha6029EthSubportVlanPacketInternal.h
 * 
 * Created Date: Dec 9, 2016
 *
 * Description : Internal interface of Eth Subport Vlan Packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029ETHSUBPORTVLANPACKETINTERNAL_H_
#define _THA6029ETHSUBPORTVLANPACKETINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/pktanalyzer/AtEthPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cSubportVlanByteNum     4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029EthSubportVlanPacket * Tha6029EthSubportVlanPacket;

typedef struct tTha6029EthSubportVlanPacket
    {
    tAtEthPacket super;

    /* Private data */
    eBool withSubportVlan;
    }tTha6029EthSubportVlanPacket;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket Tha6029EthSubportVlanPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode, eBool withSubportVlan);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029ETHSUBPORTVLANPACKETINTERNAL_H_ */

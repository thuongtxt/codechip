/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : Tha60290021ClockExtractor.c
 *
 * Created Date: Oct 6, 2016
 *
 * Description : Tha60290021ClockExtractor implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290021ClockExtractorInternal.h"
#include "../man/Tha60290021DeviceReg.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha60290021ClockExtractor)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021ClockExtractorMethods m_methods;
static tAtClockExtractorMethods  m_AtClockExtractorOverride;
static tThaClockExtractorMethods m_ThaClockExtractorOverride;

/* Supper implementations */
static const tAtClockExtractorMethods  *m_AtClockExtractorMethods  = NULL;
static const tThaClockExtractorMethods *m_ThaClockExtractorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(void)
    {
    return cTopBaseAddress;
    }

static eBool IsSonetSerdes(AtSerdesController serdes)
    {
    if (serdes)
        {
        eAtSerdesMode serdesMode = AtSerdesControllerOverSamplingMode(serdes);

        if ((serdesMode == cAtSerdesModeStm0)  ||
            (serdesMode == cAtSerdesModeStm1)  ||
            (serdesMode == cAtSerdesModeStm4)  ||
            (serdesMode == cAtSerdesModeStm16) ||
            (serdesMode == cAtSerdesModeStm64))
            return cAtTrue;

        return cAtFalse;
        }

    return cAtFalse;
    }

static eBool Is1GeSerdes(AtSerdesController serdes)
    {
    if (serdes)
        {
        eAtSerdesMode serdesMode = AtSerdesControllerOverSamplingMode(serdes);

        if (serdesMode == cAtSerdesModeEth1G)
            return cAtTrue;

        return cAtFalse;
        }

    return cAtFalse;
    }

static eBool Is100MSerdes(AtSerdesController serdes)
    {
    if (serdes)
        {
        eAtSerdesMode serdesMode = AtSerdesControllerOverSamplingMode(serdes);

        if (serdesMode == cAtSerdesModeEth100M)
            return cAtTrue;

        return cAtFalse;
        }

    return cAtFalse;
    }

static eBool IsTenGeSerdes(AtSerdesController serdes)
    {
    if (serdes)
        {
        eAtSerdesMode serdesMode = AtSerdesControllerOverSamplingMode(serdes);

        if (serdesMode == cAtSerdesModeEth10G)
            return cAtTrue;

        return cAtFalse;
        }

    return cAtFalse;
    }

static uint8 SerdesModeSw2Hw(AtSerdesController serdes)
    {
    eAtSerdesMode serdesMode = AtSerdesControllerOverSamplingMode(serdes);

    if ((serdesMode == cAtSerdesModeEth1G) ||
        (serdesMode == cAtSerdesModeEth100M))
        return 0x1;

    if ((serdesMode == cAtSerdesModeStm16) || (serdesMode == cAtSerdesModeStm64))
        return 0x2;

    if (serdesMode == cAtSerdesModeEth10G)
        return 0x3;

    if (serdesMode == cAtSerdesModeStm4)
        return 0;

    return 0x0;
    }

static uint8 LineModeSw2Hw(AtSdhLine line)
    {
    eAtSdhLineRate lineRate = AtSdhLineRateGet(line);

    if ((lineRate == cAtSdhLineRateStm16) || (lineRate == cAtSdhLineRateStm64))
        return 0x2;

    return 0x0;
    }

static eAtRet HwRateSet(AtClockExtractor self, uint32 portId, uint8 hwRateValue)
    {
    uint32 regAddr = cAf6Reg_o_control8_Base + BaseAddress();
    uint32 regVal = AtClockExtractorRead(self, regAddr);
    mFieldIns(&regVal, cAf6_o_control8_cfgrate_Mask(portId), cAf6_o_control8_cfgrate_Shift(portId), hwRateValue);
    AtClockExtractorWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool CanExtractSerdesClock(ThaClockExtractor self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return cAtTrue;
    }

static eBool SonetSerdesSquelchingIsSupported(ThaClockExtractor self, AtSerdesController serdes)
    {
    ThaModuleClock module = (ThaModuleClock)AtClockExtractorModuleGet((AtClockExtractor)self);
    if (ThaModuleClockSquelchingIsSupported(module) && IsSonetSerdes(serdes))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModuleClockRet HwSerdesClockExtract(ThaClockExtractor self, AtSerdesController serdes)
    {
    uint32 regAddr = cAf6Reg_o_control7_Base + BaseAddress();
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor) self);
    uint32 regVal = AtClockExtractorRead((AtClockExtractor)self, regAddr);
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    uint32 diagEnForSquelching = mMethodsGet(mThis(self))->HwClockExtractShouldUseDiag(mThis(self), serdes) ? 1 : 0;

    if (extractorId == 1) /* Need to write this field on extractor 2 */
        mRegFieldSet(regVal, cAf6_o_control7_cfgref2mod_, 0);

    mFieldIns(&regVal, cAf6_o_control7_cfgrefpid_Mask(extractorId), cAf6_o_control7_cfgrefpid_Shift(extractorId), serdesId);
    mFieldIns(&regVal, cAf6_o_control7_cfgdiagen_Mask(serdesId), cAf6_o_control7_cfgdiagen_Shift(serdesId), diagEnForSquelching);
    AtClockExtractorWrite((AtClockExtractor)self, regAddr, regVal);

    return HwRateSet((AtClockExtractor)self, serdesId, SerdesModeSw2Hw(serdes));
    }

static eAtModuleClockRet HwSdhLineClockExtract(ThaClockExtractor self, AtSdhLine line)
    {
    uint32 regAddr, regVal;
    uint32 extractorId = AtClockExtractorIdGet((AtClockExtractor) self);
    uint32 lineId = AtChannelIdGet((AtChannel)line);

    if (extractorId != 1)
        return cAtErrorModeNotSupport;

    regAddr = cAf6Reg_o_control7_Base + BaseAddress();
    regVal = AtClockExtractorRead((AtClockExtractor)self, regAddr);
    mRegFieldSet(regVal, cAf6_o_control7_cfgref2mod_, 1);
    mFieldIns(&regVal, cAf6_o_control7_cfgrefpid_Mask(extractorId), cAf6_o_control7_cfgrefpid_Shift(extractorId), lineId);
    mFieldIns(&regVal, cAf6_o_control7_cfgdiagen_Mask(lineId), cAf6_o_control7_cfgdiagen_Shift(lineId), 0);
    AtClockExtractorWrite((AtClockExtractor)self, regAddr, regVal);

    return HwRateSet((AtClockExtractor)self, lineId, LineModeSw2Hw(line));
    }


static eAtModuleClockRet Enable(AtClockExtractor self, eBool enable)
    {
    uint8 hwDisableVal = (enable == cAtFalse) ? 1 : 0;
    uint32 address = cAf6Reg_o_control7_Base + BaseAddress();
    uint32 regVal  = AtClockExtractorRead(self, address);
    uint8 extractorId = AtClockExtractorIdGet(self);

    if (extractorId == 0)
        mRegFieldSet(regVal, cAf6_o_control7_ref1disable_, hwDisableVal);
    else if (extractorId == 1)
        mRegFieldSet(regVal, cAf6_o_control7_ref2disable_, hwDisableVal);
    else
        return cAtErrorOutOfRangParm;

    AtClockExtractorWrite(self, address, regVal);

    return cAtOk;
    }

static eBool IsEnabled(AtClockExtractor self)
    {
    uint32 address = cAf6Reg_o_control7_Base + BaseAddress();
    uint32 regVal  = AtClockExtractorRead(self, address);
    uint8 extractorId = AtClockExtractorIdGet(self);

    if (extractorId == 0)
        return mRegField(regVal, cAf6_o_control7_ref1disable_) == 0 ? cAtTrue : cAtFalse;
    if (extractorId == 1)
        return mRegField(regVal, cAf6_o_control7_ref2disable_) == 0 ? cAtTrue : cAtFalse;

    return cAtFalse;
    }


static void SerdesModeDidChange(AtSerdesController serdes, eAtSerdesMode mode, void* userData)
    {
    ThaClockExtractor self = (ThaClockExtractor)userData;
    AtUnused(mode);
    ThaClockExtractorHwSerdesClockExtract(self, serdes);
    }

static tAtSerdesControllerPropertyListener* SerdesListener(void)
    {
    static tAtSerdesControllerPropertyListener* pListener = NULL;
    static tAtSerdesControllerPropertyListener listener;

    if (pListener)
        return pListener;

    AtOsalMemInit(&listener, 0, sizeof(listener));
    listener.ModeDidChange = SerdesModeDidChange;
    pListener = &listener;

    return pListener;
    }

static eAtRet SerdesListenerAdd(AtClockExtractor self, AtSerdesController serdes)
    {
    return AtSerdesControllerPropertyListenerAdd(serdes, SerdesListener(), self);
    }

static eAtRet SerdesListenerRemove(AtClockExtractor self, AtSerdesController serdes)
    {
    return AtSerdesControllerPropertyListenerRemove(serdes, SerdesListener(), self);
    }

static eAtRet ExtractSourceReset(ThaClockExtractor self)
    {
    AtSerdesController serdes = AtClockExtractorSerdesGet((AtClockExtractor)self);
    if (serdes)
        SerdesListenerRemove((AtClockExtractor)self, serdes);

    return m_ThaClockExtractorMethods->ExtractSourceReset(self);
    }

static eAtModuleClockRet SerdesClockExtract(AtClockExtractor self, AtSerdesController serdes)
    {
    eAtRet ret = cAtOk;

    if (AtClockExtractorSerdesGet(self) == serdes)
        return cAtOk;

    ret = m_AtClockExtractorMethods->SerdesClockExtract(self, serdes);
    if (ret == cAtOk)
        ret |= SerdesListenerAdd(self, serdes);

    return ret;
    }

static eBool HwClockExtractShouldUseDiag(Tha60290021ClockExtractor self, AtSerdesController serdes)
    {
    return SonetSerdesSquelchingIsSupported((ThaClockExtractor)self, serdes) ? cAtFalse : cAtTrue;
    }

static void MethodsInit(Tha60290021ClockExtractor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HwClockExtractShouldUseDiag);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtClockExtractor(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtClockExtractorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockExtractorOverride, m_AtClockExtractorMethods, sizeof(m_AtClockExtractorOverride));

        mMethodOverride(m_AtClockExtractorOverride, SerdesClockExtract);
        mMethodOverride(m_AtClockExtractorOverride, Enable);
        mMethodOverride(m_AtClockExtractorOverride, IsEnabled);
        }

    mMethodsSet(self, &m_AtClockExtractorOverride);
    }

static void OverrideThaClockExtractor(AtClockExtractor self)
    {
    ThaClockExtractor extractor = (ThaClockExtractor) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClockExtractorMethods = mMethodsGet(extractor);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClockExtractorOverride, m_ThaClockExtractorMethods, sizeof(m_ThaClockExtractorOverride));

        mMethodOverride(m_ThaClockExtractorOverride, CanExtractSerdesClock);
        mMethodOverride(m_ThaClockExtractorOverride, HwSerdesClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, HwSdhLineClockExtract);
        mMethodOverride(m_ThaClockExtractorOverride, ExtractSourceReset);
        }

    mMethodsSet(extractor, &m_ThaClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtClockExtractor(self);
    OverrideThaClockExtractor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ClockExtractor);
    }

AtClockExtractor Tha60290021ClockExtractorObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtClockExtractor Tha60290021ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ClockExtractorObjectInit(newExtractor, clockModule, extractorId);
    }

eBool Tha60290021ClockExtractorIsSonetSerdes(AtSerdesController serdes)
    {
    return IsSonetSerdes(serdes);
    }

eBool Tha60290021ClockExtractorIsTenGeSerdes(AtSerdesController serdes)
    {
    return IsTenGeSerdes(serdes);
    }

eBool Tha60290021ClockExtractorIs1GeSerdes(AtSerdesController serdes)
    {
    return Is1GeSerdes(serdes);
    }

eBool Tha60290021ClockExtractorIs100MSerdes(AtSerdesController serdes)
    {
    return Is100MSerdes(serdes);
    }

eBool Tha60290021ClockExtractorHwClockExtractShouldUseDiag(Tha60290021ClockExtractor self, AtSerdesController serdes)
    {
    if (self)
        return mMethodsGet(mThis(self))->HwClockExtractShouldUseDiag(self, serdes);
    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60290021EthPortClockMonitor.h
 * 
 * Created Date: Aug 8, 2017
 *
 * Description : Ethernet port clock monitor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021ETHPORTCLOCKMONITOR_H_
#define _THA60290021ETHPORTCLOCKMONITOR_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtClockMonitor Tha60290021EthPortClockMonitorNew(AtModuleClock clockModule, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021ETHPORTCLOCKMONITOR_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60290021ClockExtactorInternal.h
 * 
 * Created Date: Feb 27, 2018
 *
 * Description : clock extractor controller implementation for 60290021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021CLOCKEXTACTORINTERNAL_H_
#define _THA60290021CLOCKEXTACTORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../Tha60210011/clock/Tha60210011ClockExtractorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ClockExtractor *Tha60290021ClockExtractor;

typedef struct tTha60290021ClockExtractorMethods
    {
    eBool (*HwClockExtractShouldUseDiag)(Tha60290021ClockExtractor self, AtSerdesController serdes);
    }tTha60290021ClockExtractorMethods;

typedef struct tTha60290021ClockExtractor
    {
    tTha60210011ClockExtractor super;
    const tTha60290021ClockExtractorMethods *methods;
    }tTha60290021ClockExtractor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtClockExtractor Tha60290021ClockExtractorObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId);
eBool Tha60290021ClockExtractorIsSonetSerdes(AtSerdesController serdes);
eBool Tha60290021ClockExtractorIsTenGeSerdes(AtSerdesController serdes);
eBool Tha60290021ClockExtractorIs1GeSerdes(AtSerdesController serdes);
eBool Tha60290021ClockExtractorIs100MSerdes(AtSerdesController serdes);
eBool Tha60290021ClockExtractorHwClockExtractShouldUseDiag(Tha60290021ClockExtractor self, AtSerdesController serdes);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021CLOCKEXTACTORINTERNAL_H_ */


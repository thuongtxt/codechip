/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60290021ModuleClockInternal.h
 * 
 * Created Date: Feb 27, 2018
 *
 * Description : implementation for the 60290021 module clock
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULECLOCKINTERNAL_H_
#define _THA60290021MODULECLOCKINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/clock/Tha60210051ModuleClockInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModuleClock
    {
    tTha60210051ModuleClock super;
    }tTha60290021ModuleClock;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleClock Tha60290021ModuleClockObjectInit(AtModuleClock self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULECLOCKINTERNAL_H_ */


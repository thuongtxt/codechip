/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : Tha60290021EthPortClockMonitor.c
 *
 * Created Date: Aug 7, 2017
 *
 * Description : Eth Port Clock Monitor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/clock/AtClockMonitorInternal.h"
#include "../eth/Tha60290021FaceplateSerdesEthPort.h"
#include "../eth/Tha60290021Eth10gReg.h"
#include "../physical/Tha6029FaceplateSerdesControllerReg.h"
#include "../physical/Tha6029FaceplateSerdesController.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../man/Tha60290021Device.h"
#include "Tha60290021EthPortClockMonitor.h"
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define cHwUnit 156

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021EthPortClockMonitor
    {
    tAtClockMonitor super;
    }tTha60290021EthPortClockMonitor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtClockMonitorMethods  m_AtClockMonitorOverride;

/* Save super implementation */
static const tAtClockMonitorMethods  *m_AtClockMonitorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CurrentPpmGet(AtClockMonitor self)
    {
    AtEthPort port = (AtEthPort)AtClockMonitorMonitoredObjectGet(self);
    AtSerdesController serdes = AtEthPortSerdesController(port);
    uint32 localPortId = Tha6029FaceplateSerdesControllerLocalPortIdInGroup(serdes);
    uint32 regAddr = Tha6029FaceplateSerdesControllerAddressWithLocalAddress(serdes, cAf6Reg_RX_Monitor_PPM_Mon_Value_Base);
    uint32 regValue = mChannelHwRead(port, regAddr, cAtModuleEth);
    uint32 fieldMask  = cAf6_RX_Monitor_PPM_Mon_Value_ppmval_pid0_Mask << (8 * localPortId);
    uint32 fieldShift = cAf6_RX_Monitor_PPM_Mon_Value_ppmval_pid0_Shift + (8 * localPortId);
    return mRegField(regValue, field);
    }

static uint32 TopAddressWithLocalAddress(AtClockMonitor self, uint32 localAddress)
    {
    AtDevice device = AtClockMonitorDeviceGet(self);
    return Tha60290021DeviceTopRegisterWithLocalAddress(device, localAddress);
    }

static uint32 MaxValueWithMaskAndShift(uint32 mask, uint32 shift)
    {
    return (mask >> shift) / cHwUnit;
    }

static eAtRet LossOfClockThresholdSet(AtClockMonitor self, uint32 value)
    {
    AtModuleClock clockModule = AtClockMonitorModuleGet(self);
    uint32 regAddr = TopAddressWithLocalAddress(self, cAf6Reg_o_control13_Base);
    uint32 regVal = mModuleHwRead(clockModule, regAddr);
    mRegFieldSet(regVal, cAf6_o_control13_Lost_clock_thres_10g_, (cHwUnit * value));
    mModuleHwWrite(clockModule, regAddr, regVal);
    return cAtOk;
    }

static uint32 LossOfClockThresholdGet(AtClockMonitor self)
    {
    AtModuleClock clockModule = AtClockMonitorModuleGet(self);
    uint32 regAddr = TopAddressWithLocalAddress(self, cAf6Reg_o_control13_Base);
    uint32 regVal = mModuleHwRead(clockModule, regAddr);
    return mRegField(regVal, cAf6_o_control13_Lost_clock_thres_10g_) / cHwUnit;
    }

static eAtRet FrequencyOutofRangeThresholdSet(AtClockMonitor self, uint32 value)
    {
    AtModuleClock clockModule = AtClockMonitorModuleGet(self);
    uint32 regAddr = TopAddressWithLocalAddress(self, cAf6Reg_o_control14_Base);
    uint32 regVal = mModuleHwRead(clockModule, regAddr);
    mRegFieldSet(regVal, cAf6_o_control14_Out_of_freq_thres_10g_, (cHwUnit * value));
    mModuleHwWrite(clockModule, regAddr, regVal);
    return cAtOk;
    }

static uint32 FrequencyOutofRangeThresholdGet(AtClockMonitor self)
    {
    AtModuleClock clockModule = AtClockMonitorModuleGet(self);
    uint32 regAddr = TopAddressWithLocalAddress(self, cAf6Reg_o_control14_Base);
    uint32 regValue = mModuleHwRead(clockModule, regAddr);
    return mRegField(regValue, cAf6_o_control14_Out_of_freq_thres_10g_) / cHwUnit;
    }

static eAtRet ThresholdSet(AtClockMonitor self, eAtClockMonitorThreshold threshold, uint32 value)
    {
    if (threshold == cAtClockMonitorThresholdLossOfClock)
        return LossOfClockThresholdSet(self, value);

    if (threshold == cAtClockMonitorThresholdFrequencyOutOfRange)
        return FrequencyOutofRangeThresholdSet(self, value);

    return cAtErrorModeNotSupport;
    }

static uint32 ThresholdGet(AtClockMonitor self, eAtClockMonitorThreshold threshold)
    {
    switch (threshold)
        {
        case cAtClockMonitorThresholdLossOfClock:
            return LossOfClockThresholdGet(self);

        case cAtClockMonitorThresholdFrequencyOutOfRange:
            return FrequencyOutofRangeThresholdGet(self);

        default:
            return 0;
        }
    }

static eBool ThresholdIsSupported(AtClockMonitor self, eAtClockMonitorThreshold threshold)
    {
    AtUnused(self);

    if ((threshold == cAtClockMonitorThresholdLossOfClock) ||
        (threshold == cAtClockMonitorThresholdFrequencyOutOfRange))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 AlarmHw2Sw(uint32 hwVlaues)
    {
    uint32 alarms = 0;

    if (hwVlaues & cAf6_eth10g_link_fault_Cur_Sta_Eth10gClockOutRangeCur_Mask)
        alarms |= cAtClockMonitorFrequencyOutOfRange;

    if (hwVlaues & cAf6_eth10g_link_fault_Cur_Sta_Eth10gClockLossCur_Mask)
        alarms |= cAtClockMonitorAlarmLossOfClock;

    return alarms;
    }

static uint32 AlarmGet(AtClockMonitor self)
    {
    AtEthPort port = (AtEthPort)AtClockMonitorMonitoredObjectGet(self);
    uint32 regAddr = Tha60290021FaceplateEthPortTenGeAlarmAddressWithLocalAddress(port, cAf6Reg_eth10g_link_fault_Cur_Sta_Base);
    uint32 regValue = mChannelHwRead(port, regAddr, cAtModuleEth);
    return AlarmHw2Sw(regValue);
    }

static uint32 AlarmHistoryRead2Clear(AtEthPort port, eBool read2Clear)
    {
    uint32 regAddr = Tha60290021FaceplateEthPortTenGeAlarmAddressWithLocalAddress(port, cAf6Reg_eth10g_link_fault_sticky_Base);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    uint32 alarms = AlarmHw2Sw(regVal);

    if (read2Clear)
        mChannelHwWrite(port, regAddr, regVal, cAtModuleEth);

    return alarms;
    }

static uint32 AlarmHistoryGet(AtClockMonitor self)
    {
    AtEthPort port = (AtEthPort)AtClockMonitorMonitoredObjectGet(self);
    return AlarmHistoryRead2Clear(port, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtClockMonitor self)
    {
    AtEthPort port = (AtEthPort)AtClockMonitorMonitoredObjectGet(self);
    return AlarmHistoryRead2Clear(port, cAtTrue);
    }

static eAtRet InterruptMaskSet(AtClockMonitor self, uint32 defectMask, uint32 enableMask)
    {
    AtEthPort port = (AtEthPort)AtClockMonitorMonitoredObjectGet(self);
    uint32 regAddr = Tha60290021FaceplateEthPortTenGeAlarmAddressWithLocalAddress(port, cAf6Reg_eth10g_link_fault_int_enb_Base);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    uint32 enabled;

    if (defectMask & cAtClockMonitorFrequencyOutOfRange)
        {
        enabled = (enableMask & cAtClockMonitorFrequencyOutOfRange) ? 1 : 0;
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gClockOutRangeEnb_, enabled);
        }

    if (defectMask & cAtClockMonitorAlarmLossOfClock)
        {
        enabled = (enableMask & cAtClockMonitorAlarmLossOfClock) ? 1 : 0;
        mRegFieldSet(regVal, cAf6_eth10g_link_fault_int_enb_Eth10gClockLossEnb_, enabled);
        }

    mChannelHwWrite(port, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtClockMonitor self)
    {
    AtEthPort port = (AtEthPort)AtClockMonitorMonitoredObjectGet(self);
    uint32 regAddr = Tha60290021FaceplateEthPortTenGeAlarmAddressWithLocalAddress(port, cAf6Reg_eth10g_link_fault_int_enb_Base);
    uint32 regVal = mChannelHwRead(port, regAddr, cAtModuleEth);
    return AlarmHw2Sw(regVal);
    }

static eBool InterruptMaskIsSupported(AtClockMonitor self, uint32 defectMask)
    {
    AtUnused(self);

    if (defectMask & cAtClockMonitorAlarmLossOfClock)
        return cAtTrue;

    if (defectMask & cAtClockMonitorFrequencyOutOfRange)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 ThresholdMax(AtClockMonitor self, eAtClockMonitorThreshold threshold)
    {
    AtUnused(self);

    switch (threshold)
        {
        case cAtClockMonitorThresholdLossOfClock:
            return MaxValueWithMaskAndShift(cAf6_o_control13_Lost_clock_thres_10g_Mask, cAf6_o_control13_Lost_clock_thres_10g_Shift);

        case cAtClockMonitorThresholdFrequencyOutOfRange:
            return MaxValueWithMaskAndShift(cAf6_o_control14_Out_of_freq_thres_10g_Mask, cAf6_o_control14_Out_of_freq_thres_10g_Shift);

        default:
            return 0;
        }
    }

static eAtRet DefaultThresholdSet(AtClockMonitor self)
    {
    eAtRet ret = cAtOk;

    ret |= AtClockMonitorThresholdSet(self, cAtClockMonitorThresholdFrequencyOutOfRange, 100);
    ret |= AtClockMonitorThresholdSet(self, cAtClockMonitorThresholdLossOfClock, 150);

    return ret;
    }

static eAtRet Init(AtClockMonitor self)
    {
    eAtRet ret = m_AtClockMonitorMethods->Init(self);

    if (ret != cAtOk)
        return ret;

    return DefaultThresholdSet(self);
    }

static void OverrideAtClockMonitor(AtClockMonitor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtClockMonitorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockMonitorOverride, mMethodsGet(self), sizeof(m_AtClockMonitorOverride));

        mMethodOverride(m_AtClockMonitorOverride, CurrentPpmGet);
        mMethodOverride(m_AtClockMonitorOverride, ThresholdGet);
        mMethodOverride(m_AtClockMonitorOverride, ThresholdSet);
        mMethodOverride(m_AtClockMonitorOverride, ThresholdIsSupported);
        mMethodOverride(m_AtClockMonitorOverride, AlarmGet);
        mMethodOverride(m_AtClockMonitorOverride, AlarmHistoryGet);
        mMethodOverride(m_AtClockMonitorOverride, AlarmHistoryClear);
        mMethodOverride(m_AtClockMonitorOverride, InterruptMaskSet);
        mMethodOverride(m_AtClockMonitorOverride, InterruptMaskGet);
        mMethodOverride(m_AtClockMonitorOverride, InterruptMaskIsSupported);
        mMethodOverride(m_AtClockMonitorOverride, ThresholdMax);
        mMethodOverride(m_AtClockMonitorOverride, Init);
        }

    mMethodsSet(self, &m_AtClockMonitorOverride);
    }

static void Override(AtClockMonitor self)
    {
    OverrideAtClockMonitor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021EthPortClockMonitor);
    }

static AtClockMonitor ObjectInit(AtClockMonitor self, AtModuleClock clockModule, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtClockMonitorObjectInit(self, clockModule, (AtObject)port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtClockMonitor Tha60290021EthPortClockMonitorNew(AtModuleClock clockModule, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockMonitor newMonitor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    if (newMonitor == NULL)
        return NULL;

    return ObjectInit(newMonitor, clockModule, port);
    }

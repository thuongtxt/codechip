/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha6029InterruptPin.c
 *
 * Created Date: Oct 1, 2016
 *
 * Description : Interrupt PIN
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "../../../../generic/man/AtInterruptPinInternal.h"
#include "../man/Tha60290021DeviceReg.h"
#include "Tha6029InterruptPin.h"
#include "Tha6029InterruptDiagReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6029InterruptPin
    {
    tAtInterruptPin super;
    }tTha6029InterruptPin;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtInterruptPinMethods m_AtInterruptPinOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TriggerModeMask(AtInterruptPin self)
    {
    return cAf6_pcfg_mode_out_mode0_Mask << AtInterruptPinIdGet(self);
    }

static uint32 TriggerModeShift(AtInterruptPin self)
    {
    return AtInterruptPinIdGet(self);
    }

static uint32 TriggerMask(AtInterruptPin self)
    {
    return cAf6_pcfg_glb_out_glb0_Mask << AtInterruptPinIdGet(self);
    }

static uint32 TriggerShift(AtInterruptPin self)
    {
    return AtInterruptPinIdGet(self);
    }

static uint32 TriggerModeSw2Hw(eAtTriggerMode triggerMode)
    {
    switch (triggerMode)
        {
        case cAtTriggerModeEdge     : return 0;
        case cAtTriggerModeLevelHigh: return 1;
        case cAtTriggerModeLevelLow:
        case cAtTriggerModeUnknown:
        default:
            return cInvalidUint32;
        }
    }

static eAtTriggerMode TriggerModeHw2Sw(uint32 hwMode)
    {
    switch (hwMode)
        {
        case 0: return cAtTriggerModeEdge;
        case 1: return cAtTriggerModeLevelHigh;
        default:
            return cAtTriggerModeUnknown;
        }
    }

static uint32 AddressWithLocalAddress(AtInterruptPin self, uint32 localAddress)
    {
    AtUnused(self);
    return cInterruptPinBaseAdress + localAddress;
    }

static eAtRet HwTriggerModeSet(AtInterruptPin self, uint32 hwTriggerMode)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_pcfg_mode_Base);
    uint32 regVal = AtInterruptPinRead(self, regAddr);
    uint32 modeMask = TriggerModeMask(self);
    uint32 modeShift = TriggerModeShift(self);
    mRegFieldSet(regVal, mode, hwTriggerMode);
    AtInterruptPinWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet TriggerModeSet(AtInterruptPin self, eAtTriggerMode triggerMode)
    {
    uint32 hwMode = TriggerModeSw2Hw(triggerMode);
    if (hwMode == cInvalidUint32)
        return cAtErrorModeNotSupport;

    return HwTriggerModeSet(self, hwMode);
    }

static eAtTriggerMode TriggerModeGet(AtInterruptPin self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_pcfg_mode_Base);
    uint32 regVal = AtInterruptPinRead(self, regAddr);
    uint32 modeMask = TriggerModeMask(self);
    uint32 modeShift = TriggerModeShift(self);
    return TriggerModeHw2Sw(mRegField(regVal, mode));
    }

static eAtRet Trigger(AtInterruptPin self, eBool triggered)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_pcfg_glb_Base);
    uint32 regVal;
    uint32 triggerMask = TriggerMask(self);
    uint32 triggerShift = TriggerShift(self);
    uint32 startValue = triggered ? 0 : 1;
    uint32 stopValue = triggered ? 1 : 0;

    regVal = AtInterruptPinRead(self, regAddr);
    mRegFieldSet(regVal, trigger, startValue);
    AtInterruptPinWrite(self, regAddr, regVal);

    mRegFieldSet(regVal, trigger, stopValue);
    AtInterruptPinWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool IsTriggered(AtInterruptPin self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_pcfg_glb_Base);
    uint32 regVal = AtInterruptPinRead(self, regAddr);
    return (regVal & TriggerMask(self)) ? cAtTrue : cAtFalse;
    }

static eBool IsAsserted(AtInterruptPin self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_stickyx_int_Base);
    uint32 regVal = AtInterruptPinRead(self, regAddr);
    uint32 assertedMask = TriggerMask(self);
    AtInterruptPinWrite(self, regAddr, assertedMask);
    return (regVal & assertedMask) ? cAtTrue : cAtFalse;
    }

static void OverrideAtInterruptPin(AtInterruptPin self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtInterruptPinOverride, mMethodsGet(self), sizeof(m_AtInterruptPinOverride));

        mMethodOverride(m_AtInterruptPinOverride, TriggerModeSet);
        mMethodOverride(m_AtInterruptPinOverride, TriggerModeGet);
        mMethodOverride(m_AtInterruptPinOverride, Trigger);
        mMethodOverride(m_AtInterruptPinOverride, IsTriggered);
        mMethodOverride(m_AtInterruptPinOverride, IsAsserted);
        }

    mMethodsSet(self, &m_AtInterruptPinOverride);
    }

static void Override(AtInterruptPin self)
    {
    OverrideAtInterruptPin(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029InterruptPin);
    }

static AtInterruptPin ObjectInit(AtInterruptPin self, AtDevice device, uint32 pinId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtInterruptPinObjectInit(self, device, pinId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtInterruptPin Tha6029InterruptPinNew(AtDevice device, uint32 pinId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtInterruptPin newPin = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newPin, device, pinId);
    }

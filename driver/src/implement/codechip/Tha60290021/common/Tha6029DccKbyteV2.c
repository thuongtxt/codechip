/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : Tha6029DccKbyteV2.c
 *
 * Created Date: Jun 1, 2018
 *
 * Description : Common function for DCC/KByte over SGMII
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../pw/Tha60290021DccKbyteV2Reg.h"
#include "Tha6029DccKbyte.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_af6ces10grtl_dccpro__upen_cfg_maxlen 0x10004

#define cAf6_ETH2OCN_maxlen_Mask  cBit11_0
#define cAf6_ETH2OCN_maxlen_Shift 0
#define cAf6_OCN2ETH_maxlen_Mask   cBit23_12
#define cAf6_OCN2ETH_maxlen_Shift  12

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return (cTha6029EthAlarmTypeDccCVlanMisMatch    |
            cTha6029EthAlarmTypeDccDaMacMisMatch    |
            cAtEthPortAlarmOversized                |
            cAtEthPortAlarmFcsError                 |
            cTha6029EthAlarmTypeEthTypeMisMatch);
    }
    
static ThaModuleOcn OcnModule(AtChannel self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModuleOcn);
    }

static uint32 BaseAddress(AtChannel self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self));
    }

static uint32 InterruptEnableRegister(AtChannel self)
    {
    return BaseAddress(self) + cAf6Reg_upen_rxdcc_intenb_r1;
    }

static eAtRet HwInterruptEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_af6ces10grtldcck_corepi__upen_rxdcc_or_enb;
    mChannelHwWrite(self, regAddr, enable ? cBit0 : 0, cAtModuleEth);
    return cAtOk;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = InterruptEnableRegister(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 mask;

    mask = cTha6029EthAlarmTypeEthTypeMisMatch;
    if (defectMask & mask)
        mRegFieldSet(regVal, cAf6_upen_rxdcc_intenb_r1_enb_inb_dcc_eth2ocn_type_mismat_, (enableMask & mask) ? 1 : 0);

    mask = cTha6029EthAlarmTypeDccDaMacMisMatch;
    if (defectMask & mask)
        mRegFieldSet(regVal, cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_macda_mismat_, (enableMask & mask) ? 1 : 0);

    mask = cTha6029EthAlarmTypeDccCVlanMisMatch;
    if (defectMask & mask)
        mRegFieldSet(regVal, cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_cvlid_mismat_, (enableMask & mask) ? 1 : 0);

    mask = cAtEthPortAlarmOversized;
    if (defectMask & mask)
        mRegFieldSet(regVal, cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_ovrsize_len_, (enableMask & mask) ? 1 : 0);

    mask = cAtEthPortAlarmFcsError;
    if (defectMask & mask)
        mRegFieldSet(regVal, cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_crc_error_, (enableMask & mask) ? 1 : 0);

    mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return HwInterruptEnable(self, (regVal) ? cAtTrue : cAtFalse);
    }

static uint32 DefectHw2Sw(uint32 regVal)
    {
    uint32 alarmMask = 0;

    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_inb_dcc_eth2ocn_type_mismat_Mask)
        alarmMask |= cTha6029EthAlarmTypeEthTypeMisMatch;

    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_macda_mismat_Mask)
        alarmMask |= cTha6029EthAlarmTypeDccDaMacMisMatch;

    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_cvlid_mismat_Mask)
        alarmMask |= cTha6029EthAlarmTypeDccCVlanMisMatch;

    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_ovrsize_len_Mask)
        alarmMask |= cAtEthPortAlarmOversized;

    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_eth2ocn_crc_error_Mask)
        alarmMask |= cAtEthPortAlarmFcsError;

    return alarmMask;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regAddr = InterruptEnableRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return DefectHw2Sw(regVal);
    }

static uint32 InterruptStatusRegister(AtChannel self)
    {
    return BaseAddress(self) + cAf6Reg_upen_int_rxdcc1_stt;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    uint32 baseAddress = BaseAddress(self);
    uint32 intrAddress = baseAddress + cAf6Reg_upen_int_rxdcc1_stt;
    uint32 intrStatus = mChannelHwRead(self, intrAddress, cAtModulePw);
    uint32 intrMask = mChannelHwRead(self, baseAddress + cAf6Reg_upen_rxdcc_intenb_r1, cAtModulePw);
    uint32 currentStatus = mChannelHwRead(self, baseAddress + cAf6Reg_af6ces10grtldcc_rx__upen_glb_sta_cur_glb, cAtModulePw);
    uint32 changedDefects = 0;
    uint32 defects = 0;
    AtUnused(offset);

    intrStatus &= intrMask;
    mChannelHwWrite(self, intrAddress, intrStatus, cAtModulePw);

    if (intrStatus & cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_type_mismat_Mask)
        {
        changedDefects |= cTha6029EthAlarmTypeEthTypeMisMatch;
        if (currentStatus & cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_type_mismat_Mask)
            defects |= cTha6029EthAlarmTypeEthTypeMisMatch;
        }

    if (intrStatus & cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_macda_mismat_Mask)
        {
        changedDefects |= cTha6029EthAlarmTypeDccDaMacMisMatch;
        if (currentStatus & cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_macda_mismat_Mask)
            defects |= cTha6029EthAlarmTypeDccDaMacMisMatch;
        }

    if (intrStatus & cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_cvlid_mismat_Mask)
        {
        changedDefects |= cTha6029EthAlarmTypeDccCVlanMisMatch;
        if (currentStatus & cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_cvlid_mismat_Mask)
            defects |= cTha6029EthAlarmTypeDccCVlanMisMatch;
        }

    if (intrStatus & cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_ovrsize_len_Mask)
        {
        changedDefects |= cAtEthPortAlarmOversized;
        if (currentStatus & cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_ovrsize_len_Mask)
            defects |= cAtEthPortAlarmOversized;
        }

    if (intrStatus & cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_crc_error_Mask)
        {
        changedDefects |= cAtEthPortAlarmFcsError;
        if (currentStatus & cAf6_upen_int_rxdcc1_stt_dcc_eth2ocn_crc_error_Mask)
            defects |= cAtEthPortAlarmFcsError;
        }

    AtChannelAllAlarmListenersCall(self, changedDefects, defects);
    return 0;
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regAddr = BaseAddress(self) + cAf6Reg_af6ces10grtldcc_rx__upen_glb_sta_cur_glb;
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleEth);
    return DefectHw2Sw(regVal);
    }

static uint32 DefectHistoryRead2Clear(AtChannel self, eBool r2c)
    {
    uint32 regAddr = InterruptStatusRegister(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleEth);
    uint32 alarmMask = DefectHw2Sw(regVal);

    /* Clear Interrupt Status */
    if (r2c)
        mChannelHwWrite(self, regAddr, regVal, cAtModuleEth);

    return alarmMask;
    }

static uint32 CfgMaxLengthAddress(ThaModuleEth self)
    {
    ThaModuleOcn moduleOcn = (ThaModuleOcn)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleOcn);
    uint32 baseAddress = ThaModuleOcnSohOverEthBaseAddress(moduleOcn);
    return baseAddress + cAf6Reg_af6ces10grtl_dccpro__upen_cfg_maxlen;
    }

static eBool MaxLengInRange(uint32 maxLength)
    {
    if (maxLength <= cAf6_ETH2OCN_maxlen_Mask)
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet TxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength)
    {
    uint32 address = CfgMaxLengthAddress(self);
    uint32 regVal = mModuleHwRead(self, address);

    if (!MaxLengInRange(maxLength))
        return cAtErrorOutOfRangParm;

    mRegFieldSet(regVal, cAf6_OCN2ETH_maxlen_, maxLength);
    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static uint32 TxDccPacketMaxLengthGet(ThaModuleEth self)
    {
    uint32 address = CfgMaxLengthAddress(self);
    uint32 regVal = mModuleHwRead(self, address);
    uint32 maxLen = mRegField(regVal, cAf6_OCN2ETH_maxlen_);

    return maxLen;
    }

static eAtRet RxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength)
    {
    uint32 address = CfgMaxLengthAddress(self);
    uint32 regVal = mModuleHwRead(self, address);

    if (!MaxLengInRange(maxLength))
        return cAtErrorOutOfRangParm;

    mRegFieldSet(regVal, cAf6_ETH2OCN_maxlen_, maxLength);
    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static uint32 RxDccPacketMaxLengthGet(ThaModuleEth self)
    {
    uint32 address = CfgMaxLengthAddress(self);
    uint32 regVal = mModuleHwRead(self, address);
    uint32 maxLen = mRegField(regVal, cAf6_ETH2OCN_maxlen_);

    return maxLen;
    }

uint32 Tha6029DccKByteV2SgmiiSupportedInterruptMasks(AtChannel self)
    {
    if (self)
        return SupportedInterruptMasks(self);
    return 0;
    }

eAtRet Tha6029DccKByteV2SgmiiInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    if (self)
        return InterruptMaskSet(self, defectMask, enableMask);
    return cAtErrorNullPointer;
    }

uint32 Tha6029DccKByteV2SgmiiInterruptMaskGet(AtChannel self)
    {
    if (self)
        return InterruptMaskGet(self);
    return 0;
    }

uint32 Tha6029DccKByteV2SgmiiInterruptProcess(AtChannel self, uint32 offset)
    {
    if (self)
        return InterruptProcess(self, offset);
    return 0;
    }

uint32 Tha6029DccKByteV2SgmiiDefectHistoryRead2Clear(AtChannel self, eBool r2c)
    {
    if (self)
        return DefectHistoryRead2Clear(self, r2c);
    return 0;
    }

uint32 Tha6029DccKByteV2SgmiiDefectGet(AtChannel self)
    {
    if (self)
        return DefectGet(self);
    return 0;
    }

eAtRet Tha6029ModuleEthTxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength)
    {
    if (self)
        return TxDccPacketMaxLengthSet(self, maxLength);
    return cAtErrorNullPointer;
    }

uint32 Tha6029ModuleEthTxDccPacketMaxLengthGet(ThaModuleEth self)
    {
    if (self)
        return TxDccPacketMaxLengthGet(self);
    return 0;
    }

eAtRet Tha6029ModuleEthRxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength)
    {
    if (self)
        return RxDccPacketMaxLengthSet(self, maxLength);
    return cAtErrorNullPointer;
    }

uint32 Tha6029ModuleEthRxDccPacketMaxLengthGet(ThaModuleEth self)
    {
    if (self)
        return RxDccPacketMaxLengthGet(self);
    return 0;
    }


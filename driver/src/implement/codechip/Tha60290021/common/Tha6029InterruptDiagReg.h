/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_Interrupt_Diag_H_
#define _AF6_REG_AF6CNC0021_Interrupt_Diag_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Configure Raise mode for Interrupt Pin
Reg Addr   : 0x0
Reg Formula: Base_0x0 + 0x0
    Where  : 
Reg Desc   : 
This is Configure Raise mode for Interrupt Pin  1- 2

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_mode_Base                                                                             0x0

/*--------------------------------------
BitField Name: out_mode1
BitField Type: RW
BitField Desc: Diagnostic Mode for Interrupt 2 1: Interrupt Level ( Raise when
enable signal high ) 0: State change (Raise on pos-edge/neg-edge of config
enable signal)
BitField Bits: [01]
--------------------------------------*/
#define cAf6_pcfg_mode_out_mode1_Mask                                                                  cBit1
#define cAf6_pcfg_mode_out_mode1_Shift                                                                     1

/*--------------------------------------
BitField Name: out_mode0
BitField Type: RW
BitField Desc: Diagnostic Mode for Interrupt 1 1: Interrupt Level ( Raise when
enable signal high ) 0: State change  (Raise on pos-edge/neg-edge of config
enable signal)
BitField Bits: [00]
--------------------------------------*/
#define cAf6_pcfg_mode_out_mode0_Mask                                                                  cBit0
#define cAf6_pcfg_mode_out_mode0_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Enable Diagnostic Interrupt
Reg Addr   : 0x1
Reg Formula: Base_0x1 + 0x1
    Where  : 
Reg Desc   : 
This is Enable Diagnostic Interrupt 1-2

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glb_Base                                                                              0x1

/*--------------------------------------
BitField Name: out_glb1
BitField Type: RW
BitField Desc: Trige 0->1 to start Enable Diagnostic for Interrupt 2 0->1 :
Active Interrupt 1->0 : De-active Interrupt
BitField Bits: [01]
--------------------------------------*/
#define cAf6_pcfg_glb_out_glb1_Mask                                                                    cBit1
#define cAf6_pcfg_glb_out_glb1_Shift                                                                       1

/*--------------------------------------
BitField Name: out_glb0
BitField Type: RW
BitField Desc: Trige 0->1 to start Diagnostic for Interrupt 1 0->1 : Active
Interrupt 1->0 : De-active Interrupt
BitField Bits: [00]
--------------------------------------*/
#define cAf6_pcfg_glb_out_glb0_Mask                                                                    cBit0
#define cAf6_pcfg_glb_out_glb0_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Status of Diagnostic Interrupt
Reg Addr   : 0x2
Reg Formula: Base_0x2 + 0x2
    Where  : 
Reg Desc   : 
This is Status of Diagnostic Interrupt 1-2

------------------------------------------------------------------------------*/
#define cAf6Reg_stickyx_int_Base                                                                           0x2

/*--------------------------------------
BitField Name: oint_out
BitField Type: W1C
BitField Desc: Interrupt has transition Enabled/ Disable , bit per sub port,
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_stickyx_int_oint_out_Mask                                                                 cBit1_0
#define cAf6_stickyx_int_oint_out_Shift                                                                      0

#endif /* _AF6_REG_AF6CNC0021_Interrupt_Diag_H_ */

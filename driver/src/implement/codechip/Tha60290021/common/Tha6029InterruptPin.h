/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60290021InterruptPin.h
 * 
 * Created Date: Oct 1, 2016
 *
 * Description : Interrupt PIN
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021INTERRUPTPIN_H_
#define _THA60290021INTERRUPTPIN_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtInterruptPin Tha6029InterruptPinNew(AtDevice device, uint32 pinId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021INTERRUPTPIN_H_ */


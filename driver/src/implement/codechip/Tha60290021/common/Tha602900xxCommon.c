/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : Tha602900xxCommon.c
 *
 * Created Date: Mar 10, 2017
 *
 * Description : Util functions for 602900xx products
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha602900xxCommon.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet DefaultVlanWithTpidSet(ThaModulePw self, uint8 vlanIndex, uint16 tpid)
    {
    tAtVlan vlan;
    eAtRet ret = cAtOk;

    AtOsalMemInit(&vlan, 0, sizeof(vlan));

    vlan.tpid = tpid;
    ret |= ThaModulePwTxSubportVlanSet(self, vlanIndex, &vlan);
    ret |= ThaModulePwExpectedSubportVlanSet(self, vlanIndex, &vlan);

    return ret;
    }

eAtRet Tha602900xxSubPortVlanDefaultTpidSet(ThaModulePw self)
    {
    eAtRet ret = cAtOk;

    ret |= DefaultVlanWithTpidSet(self, 0, 0x9000);
    ret |= DefaultVlanWithTpidSet(self, 1, 0x9100);

    return ret;
    }

eBool Tha602900xxDe1LocalPayloadIsSupported(AtChannel self, uint8 loopbackMode)
    {
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)self);
    eBool isUnframed = AtPdhDe1IsUnframeMode(frameType);
    eBool isBoundPw  = (AtChannelBoundPwGet(self) != NULL) ? cAtTrue : cAtFalse;
    AtUnused(loopbackMode);

    if (isUnframed || isBoundPw)
        return cAtFalse;

    return cAtTrue;
    }

eBool Tha602900xxPdhDe1ShouldPreserveService(ThaPdhDe1 self)
    {
    AtUnused(self);
    return cAtTrue;/* This allow to change frame type eventhough de1 has nxds0 */
    }

eAtSurPeriodExpireMethod Tha602900xxModuleSurDefaultExpireMethod(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtSurPeriodExpireMethodAuto;
    }

eBool Tha602900xxNeedClearanceNotifyLogic(void)
    {
    return cAtFalse;
    }

eAtSurTickSource Tha602900xxModuleSurDefaultTickSource(ThaModuleHardSur self)
    {
    AtUnused(self);
    return cAtSurTickSourceExternal;
    }

eBool Tha602900xxDeviceAccessible(AtDevice self)
    {
    return AtDeviceWarmRestoreIsStarted(self) ? cAtFalse : cAtTrue;
    }

eBool Tha602900xxSdhLineShouldHideLofWhenLos(AtSdhLine self)
    {
    /* This is required by application to have their audit system works. */
    AtUnused(self);
    return cAtFalse;
    }

eBool Tha602900xxFlexiblePsnSide(ThaModulePrbs self)
    {
    AtUnused(self);
    return cAtTrue;
    }

eBool Tha602900xxModulePrbsChannelizedPrbsAllowed(AtModulePrbs self)
    {
    /* FIXME: need to turn this on to support channelized BERT, now it is buggy */
    AtUnused(self);
    return cAtFalse;
    }

eBool Tha602900xxShouldMaskFailuresWhenNonAutonomousFailures(AtModuleSur self)
    {
    AtUnused(self);
    return cAtTrue;
    }

eBool Tha602900xxShouldRestoreDatabaseOnWarmRestoreStop(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

eAtRet Tha602900xxModulePwDccHdlcPwObjectDelete(AtModulePw self, AtPw pw)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (pw == NULL)
        return cAtOk;

    AtChannelInterruptMaskSet((AtChannel)pw, AtChannelInterruptMaskGet((AtChannel)pw), 0);
    AtChannelBoundPwSet(AtPwBoundCircuitGet(pw), NULL);
    AtObjectDelete((AtObject)pw);

    return cAtOk;
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Common
 *
 * File        : Tha6029DccKbyte.c
 *
 * Created Date: Jan 24, 2017
 *
 * Description : Common function for DCC/KByte over SGMII
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../pw/Tha60290021DccKbyteReg.h"
#include "Tha6029DccKbyte.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mOcnModule(self) OcnModule((ThaEthPort)self)
#define mCore(self) Core((ThaEthPort)self)
#define mAddressWithLocalAddress(self, localAddress) AddressWithLocalAddress((AtChannel)self, localAddress)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return (cTha6029EthAlarmTypeDccCVlanMisMatch    |
            cTha6029EthAlarmTypeDccDaMacMisMatch    |
            cTha6029EthAlarmTypeDccLenOverSize      |
            cTha6029EthAlarmTypeFcsError);
    }
    
static AtDevice DeviceGet(ThaEthPort self)
    {
    return AtChannelDeviceGet((AtChannel) self);
    }

static ThaModuleOcn OcnModule(ThaEthPort self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(DeviceGet(self), cThaModuleOcn);
    }

static AtIpCore Core(ThaEthPort self)
    {
    return AtDeviceIpCoreGet(DeviceGet(self), 0);
    }

static uint32 AddressWithLocalAddress(AtChannel port, uint32 localAddress)
    {
    ThaModuleOcn ocnModule = mOcnModule(port);
    return localAddress + ThaModuleOcnSohOverEthBaseAddress(ocnModule);
    }

static eAtRet ExpectedDestMacSet(ThaEthPort self, const uint8 *mac, uint32 macMsbMask, uint32 macMsbShift, void (*MacDwordsSw2Hw)(uint32 macLsbVal, uint32 macMsbVal, uint32 *hwMacLsb, uint32 *hwMacMsb))
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaModuleOcn ocnModule = mOcnModule(self);
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_upen_dccrxhdr_Base);
    AtIpCore core = mCore(self);
    uint32 macLsbVal = (uint32)(mac[5] + (mac[4] << 8) + (mac[3] << 16) + (mac[2] << 24));
    uint32 macMsbVal = (uint32)(mac[1] + (mac[0] << 8));
    uint32 hwMacLsb = 0;
    uint32 hwMacMsb = 0;

    MacDwordsSw2Hw(macLsbVal, macMsbVal, &hwMacLsb, &hwMacMsb);
    if (ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core) == 0)
        return cAtErrorModeNotSupport;

    longRegVal[0] = hwMacLsb;
	mFieldIns(&longRegVal[1],
			macMsbMask,
			macMsbShift,
			hwMacMsb);

    ThaModuleOcnSohOverEthLongWriteOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    return cAtOk;
    }

static eAtRet ExpectedDestMacGet(ThaEthPort self, uint8 *mac, void (*MacDwordsHw2Sw)(uint32 *longRegVal, uint32 *macLsb, uint32 *macMsb))
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaModuleOcn ocnModule = mOcnModule(self);
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_upen_dccrxhdr_Base);
    AtIpCore core = mCore(self);
    uint32 macMsb = 0;
    uint32 macLsb = 0;

    if (ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core) == 0)
        return cAtErrorModeNotSupport;

    MacDwordsHw2Sw(longRegVal, &macLsb, &macMsb);

    /* Convert to SW MAC */
    mFieldGet(macLsb, cBit7_0,   0,  uint8, &mac[5]);
    mFieldGet(macLsb, cBit15_8,  8,  uint8, &mac[4]);
    mFieldGet(macLsb, cBit23_16, 16, uint8, &mac[3]);
    mFieldGet(macLsb, cBit31_24, 24, uint8, &mac[2]);
    mFieldGet(macMsb, cBit7_0,   0,  uint8, &mac[1]);
    mFieldGet(macMsb, cBit15_8,  8,  uint8, &mac[0]);

    return cAtOk;
    }

static uint32 InterruptEnableRegister(AtChannel self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_upen_rxdcc_intenb_r1_Base);
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = InterruptEnableRegister(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 mask;

    mask = cTha6029EthAlarmTypeDccDaMacMisMatch;
    if (defectMask & mask)
        mRegFieldSet(regVal, cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_macda_mismat_, (enableMask & mask) ? 1 : 0);

    mask = cTha6029EthAlarmTypeDccCVlanMisMatch;
    if (defectMask & mask)
        mRegFieldSet(regVal, cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_cvlid_mismat_, (enableMask & mask) ? 1 : 0);

    mask = cTha6029EthAlarmTypeDccLenOverSize;
    if (defectMask & mask)
        mRegFieldSet(regVal, cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_sgm_ovrsize_len_, (enableMask & mask) ? 1 : 0);

    mask = cTha6029EthAlarmTypeFcsError;
    if (defectMask & mask)
        mRegFieldSet(regVal, cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_sgm_crc_error_, (enableMask & mask) ? 1 : 0);

    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 regAddr = InterruptEnableRegister(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 alarmMask;

    alarmMask = 0;
    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_macda_mismat_Mask)
        alarmMask |= cTha6029EthAlarmTypeDccDaMacMisMatch;

    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_cvlid_mismat_Mask)
        alarmMask |= cTha6029EthAlarmTypeDccCVlanMisMatch;

    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_sgm_ovrsize_len_Mask)
        alarmMask |= cTha6029EthAlarmTypeDccLenOverSize;

    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_sgm_crc_error_Mask)
        alarmMask |= cTha6029EthAlarmTypeFcsError;

    return alarmMask;
    }

static uint32 InterruptStatusRegister(AtChannel self)
    {
    return mAddressWithLocalAddress(self, cAf6Reg_upen_int_rxdcc1_stt_Base);
    }

static eBool IsSimulation(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceIsSimulated(device);
    }

static uint32 DefectGet(AtChannel self)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_upen_dcc_rx_glb_sta_Base);
    uint32 longReg[cThaLongRegMaxSize];
    uint32 alarmMask = 0;

    ThaModuleOcnSohOverEthLongReadOnCore(OcnModule((ThaEthPort)self), regAddr, longReg, cThaLongRegMaxSize, mCore(self));

    if (longReg[0] & cAf6_upen_dcc_rx_glb_sta_sta_dcc_ethtp_mismat_Mask)
        alarmMask |= cTha6029EthAlarmTypeEthTypeMisMatch;

    if (longReg[0] & cAf6_upen_dcc_rx_glb_sta_sta_dcc_macda_mismat_Mask)
        alarmMask |= cTha6029EthAlarmTypeDccDaMacMisMatch;

    if (longReg[0] & cAf6_upen_dcc_rx_glb_sta_sta_dcc_cvlid_mismat_Mask)
        alarmMask |= cTha6029EthAlarmTypeDccCVlanMisMatch;

    if (longReg[1] & cAf6_upen_dcc_rx_glb_sta_sta_dcc_rxeth_maxlenerr_Mask)
        alarmMask |= cTha6029EthAlarmTypeDccLenOverSize;

    return alarmMask;
    }

static uint32 DefectHistoryRead2Clear(AtChannel self, eBool r2c)
    {
    uint32 regAddr = InterruptStatusRegister(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 alarmMask = 0;
    uint32 wrMask = 0;

    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_macda_mismat_Mask)
        {
        wrMask |= cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_macda_mismat_Mask;
        alarmMask |= cTha6029EthAlarmTypeDccDaMacMisMatch;
        }

    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_cvlid_mismat_Mask)
        {
        wrMask |= cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_cvlid_mismat_Mask;
        alarmMask |= cTha6029EthAlarmTypeDccCVlanMisMatch;
        }

    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_sgm_ovrsize_len_Mask)
        {
        wrMask |= cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_sgm_ovrsize_len_Mask;
        alarmMask |= cTha6029EthAlarmTypeDccLenOverSize;
        }

    if (regVal & cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_sgm_crc_error_Mask)
        {
        wrMask |= cAf6_upen_rxdcc_intenb_r1_enb_int_dcc_sgm_crc_error_Mask;
        alarmMask |= cTha6029EthAlarmTypeFcsError;
        }

    /* Clear Interrupt Status */
    if (wrMask && r2c)
        mChannelHwWrite(self, regAddr, IsSimulation(self) ? 0 : wrMask, cAtModuleSdh);

    return alarmMask;
    }

static eAtRet ExpectedCVlanSet(AtEthPort self, uint16 vlanId, uint32 hwVlanIdMask, uint32 hwVlanIdShift)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaModuleOcn ocnModule = mOcnModule(self);
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_upen_dccrxhdr_Base);
    AtIpCore core = mCore(self);
    ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[1], hwVlanId, vlanId);
    ThaModuleOcnSohOverEthLongWriteOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    return cAtOk;
    }

static uint16 ExpectedCVlanGet(AtEthPort self, uint32 hwVlanIdMask, uint32 hwVlanIdShift)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    ThaModuleOcn ocnModule = mOcnModule(self);
    uint32 regAddr = mAddressWithLocalAddress(self, cAf6Reg_upen_dccrxhdr_Base);
    AtIpCore core = mCore(self);
    ThaModuleOcnSohOverEthLongReadOnCore(ocnModule, regAddr, longRegVal, cThaLongRegMaxSize, core);
    return (uint16) mRegField(longRegVal[1], hwVlanId);
    }

static eBool IsDccCounter(AtChannel self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cTha602SgmiiPortCounterTypeDccRxFrames              : return cAtTrue;
        case cTha602SgmiiPortCounterTypeDccRxBytes               : return cAtTrue;
        case cTha602SgmiiPortCounterTypeDccRxErrors              : return cAtTrue;
        case cTha602SgmiiPortCounterTypeDccRxUnrecognizedFrames  : return cAtTrue;
        case cTha602SgmiiPortCounterTypeDccTxFrames              : return cAtTrue;
        case cTha602SgmiiPortCounterTypeDccTxBytes               : return cAtTrue;
        case cTha602SgmiiPortCounterTypeDccTxErrors              : return cAtTrue;
        case cTha602SgmiiPortCounterTypeDccRxGoodFrames          : return cAtTrue;
        case cTha602SgmiiPortCounterTypeDccRxDiscardFrames       : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eBool IsKByteCounter(AtChannel self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cTha602SgmiiPortCounterTypeKByteTxFrames            : return cAtTrue;
        case cTha602SgmiiPortCounterTypeKByteTxBytes             : return cAtTrue;
        case cTha602SgmiiPortCounterTypeKByteRxFrames            : return cAtTrue;
        case cTha602SgmiiPortCounterTypeKbyteRxUnrecognizedFrames: return cAtTrue;
        case cTha602SgmiiPortCounterTypeKbyteRxGoodFrames        : return cAtTrue;
        case cTha602SgmiiPortCounterTypeKbyteRxDiscardFrames     : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint32 DccCounterBaseAddress(AtChannel self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cTha602SgmiiPortCounterTypeDccRxFrames:               return cAf6Reg_upen_dcc_glbcnt_sgmrxeop_Base;
        case cTha602SgmiiPortCounterTypeDccRxBytes:                return cAf6Reg_upen_dcc_glbcnt_sgmrxbyt_Base;
        case cTha602SgmiiPortCounterTypeDccRxErrors:               return cAf6Reg_upen_dcc_glbcnt_sgmrxerr_Base;
        case cTha602SgmiiPortCounterTypeDccRxUnrecognizedFrames:   return cAf6Reg_upen_dcc_glbcnt_sgmrxfail_Base;
        case cTha602SgmiiPortCounterTypeDccTxFrames:               return cAf6Reg_upen_dcc_glbcnt_sgmtxeop_Base;
        case cTha602SgmiiPortCounterTypeDccTxBytes:                return cAf6Reg_upen_dcc_glbcnt_sgmtxbyt_Base;
        case cTha602SgmiiPortCounterTypeDccTxErrors:               return cAf6Reg_upen_dcc_glbcnt_sgmtxerr_Base;
        default:                                                   return cInvalidUint32;
        }
    }

static uint32 DccCounterV2BaseAddress(AtChannel self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cTha602SgmiiPortCounterTypeDccRxFrames:
        case cTha602SgmiiPortCounterTypeDccRxBytes:
        case cTha602SgmiiPortCounterTypeDccRxErrors:
        case cTha602SgmiiPortCounterTypeDccRxUnrecognizedFrames:
            return DccCounterBaseAddress(self, counterType);
        case cTha602SgmiiPortCounterTypeDccTxFrames:               return cAf6Reg_upen_dcc_glbcnt_sgmtxeop_V2_Base;
        case cTha602SgmiiPortCounterTypeDccTxBytes:                return cAf6Reg_upen_dcc_glbcnt_sgmtxbyt_V2_Base;
        case cTha602SgmiiPortCounterTypeDccTxErrors:               return cAf6Reg_upen_dcc_glbcnt_sgmtxerr_V2_Base;
        default:                                                   return cInvalidUint32;
        }
    }

static uint32 KByteCounterBaseAddress(AtChannel self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cTha602SgmiiPortCounterTypeKByteTxFrames:             return cAf6Reg_upen_k12eop2sgm_pcid_Base;
        case cTha602SgmiiPortCounterTypeKByteTxBytes:              return cAf6Reg_upen_k12byt2sgm_pcid_Base;
        case cTha602SgmiiPortCounterTypeKByteRxFrames:             return cAf6Reg_upen_k12_glbcnt_sok_Base;
        case cTha602SgmiiPortCounterTypeKbyteRxUnrecognizedFrames: return cAf6Reg_upen_k12_glbcnt_sfail_Base;
        default:                                                   return cInvalidUint32;
        }
    }

static uint32 CounterBaseAddress(AtChannel self, uint16 counterType)
    {
    if (IsDccCounter(self, counterType))
        return DccCounterBaseAddress(self, counterType);

    if (IsKByteCounter(self, counterType) && ThaModuleOcnKByteOverEthIsSupported(OcnModule((ThaEthPort)self)))
        return KByteCounterBaseAddress(self, counterType);

    return cInvalidUint32;
    }

static uint32 PdhDccCounterV3BaseAddress(AtChannel self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cTha602SgmiiPortCounterTypeDccRxFrames:
        case cTha602SgmiiPortCounterTypeDccRxBytes:
        case cTha602SgmiiPortCounterTypeDccRxErrors:
        case cTha602SgmiiPortCounterTypeDccRxUnrecognizedFrames:
        case cTha602SgmiiPortCounterTypeDccTxFrames:
        case cTha602SgmiiPortCounterTypeDccTxBytes:
        case cTha602SgmiiPortCounterTypeDccTxErrors:
            return DccCounterV2BaseAddress(self, counterType);
        case cTha602SgmiiPortCounterTypeDccRxGoodFrames:             return cAf6Reg_upen_dcc_glbcnt_sgmrxpass_Base;
        case cTha602SgmiiPortCounterTypeDccRxDiscardFrames:        return cAf6Reg_upen_dcc_glbcnt_sgmrxdisc_Base;
        default:                                                   return cInvalidUint32;
        }
    }

static uint32 MroDccCounterV3BaseAddress(AtChannel self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cTha602SgmiiPortCounterTypeDccRxFrames:
        case cTha602SgmiiPortCounterTypeDccRxBytes:
        case cTha602SgmiiPortCounterTypeDccRxErrors:
        case cTha602SgmiiPortCounterTypeDccRxUnrecognizedFrames:
        case cTha602SgmiiPortCounterTypeDccTxFrames:
        case cTha602SgmiiPortCounterTypeDccTxBytes:
        case cTha602SgmiiPortCounterTypeDccTxErrors:
            return DccCounterBaseAddress(self, counterType);
        case cTha602SgmiiPortCounterTypeDccRxGoodFrames:             return cAf6Reg_upen_dcc_glbcnt_sgmrxpass_Base;
        case cTha602SgmiiPortCounterTypeDccRxDiscardFrames:        return cAf6Reg_upen_dcc_glbcnt_sgmrxdisc_Base;
        default:                                                   return cInvalidUint32;
        }
    }

static uint32 KByteCounterV3BaseAddress(AtChannel self, uint16 counterType)
    {
    AtUnused(self);

    switch (counterType)
        {
        case cTha602SgmiiPortCounterTypeKByteTxFrames:
        case cTha602SgmiiPortCounterTypeKByteTxBytes:
        case cTha602SgmiiPortCounterTypeKbyteRxUnrecognizedFrames:
            return KByteCounterBaseAddress(self, counterType);
        case cTha602SgmiiPortCounterTypeKByteRxFrames:             return cAf6Reg_upen_k12_glbcnt_sok_matched_etype_Base;
        case cTha602SgmiiPortCounterTypeKbyteRxGoodFrames:           return cAf6Reg_upen_k12_glbcnt_sok_V3_Base;
        case cTha602SgmiiPortCounterTypeKbyteRxDiscardFrames:      return cAf6Reg_upen_k12_glbcnt_serr_V3_Base;
        default:                                                   return cInvalidUint32;
        }
    }

static uint32 MroCounterV3BaseAddress(AtChannel self, uint16 counterType)
    {
    if (IsDccCounter(self, counterType))
        return MroDccCounterV3BaseAddress(self, counterType);

    if (IsKByteCounter(self, counterType) && ThaModuleOcnKByteOverEthIsSupported(OcnModule((ThaEthPort)self)))
        return KByteCounterV3BaseAddress(self, counterType);

    return cInvalidUint32;
    }

static uint32 PdhCounterV3BaseAddress(AtChannel self, uint16 counterType)
    {
    if (IsDccCounter(self, counterType))
        return PdhDccCounterV3BaseAddress(self, counterType);

    if (IsKByteCounter(self, counterType) && ThaModuleOcnKByteOverEthIsSupported(OcnModule((ThaEthPort)self)))
        return KByteCounterV3BaseAddress(self, counterType);

    return cInvalidUint32;
    }

static uint32 HwCounterRead2Clear(AtChannel self, uint32 counterBaseAddress, eBool r2c)
    {
    uint32 regAddr = mAddressWithLocalAddress(self, counterBaseAddress);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    if (r2c)
        mChannelHwWrite(self, regAddr, 0x0, cAtModuleSdh);
    return regVal;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (ThaEthPortCounterLocalAddress((ThaEthPort)self, counterType) == cInvalidUint32)
        return cAtFalse;
    return cAtTrue;
    }

static uint32 CounterRead2Clear(AtChannel self, uint16 counterType, eBool r2c)
    {
    uint32 counterAddress = ThaEthPortCounterLocalAddress((ThaEthPort)self, counterType);
    if (counterAddress == cInvalidUint32)
        return 0x0;
    return HwCounterRead2Clear(self, counterAddress, r2c);
    }

static eBool ExpectedCVlanIsSupported(AtEthPort self)
    {
    AtUnused(self);
    return cAtTrue;
    }

eBool Tha6029DccKByteSgmiiExpectedCVlanIsSupported(AtEthPort self)
    {
    if (self)
        return ExpectedCVlanIsSupported(self);
    return cAtFalse;
    }

eAtRet Tha6029DccKByteSgmiiExpectedCVlanSet(AtEthPort self, uint16 vlanId, uint32 hwVlanIdMask, uint32 hwVlanIdShift)
    {
    if (self)
        return ExpectedCVlanSet(self, vlanId, hwVlanIdMask, hwVlanIdShift);
    return cAtErrorNullPointer;
    }

uint16 Tha6029DccKByteSgmiiExpectedCVlanGet(AtEthPort self, uint32 hwVlanIdMask, uint32 hwVlanIdShift)
    {
    if (self)
        return ExpectedCVlanGet(self, hwVlanIdMask, hwVlanIdShift);
    return cAtErrorNullPointer;
    }

eAtRet Tha6029DccKByteSgmiiExpectedDestMacSet(ThaEthPort self, const uint8 *mac, uint32 macMsbMask, uint32 macMsbShift, void (*MacDwordsSw2Hw)(uint32 macLsbVal, uint32 macMsbVal, uint32 *hwMacLsb, uint32 *hwMacMsb))
    {
    if (self)
        return ExpectedDestMacSet(self, mac, macMsbMask, macMsbShift, MacDwordsSw2Hw);
    return cAtErrorNullPointer;
    }

eAtRet Tha6029DccKByteSgmiiExpectedDestMacGet(ThaEthPort self, uint8 *mac, void (*MacDwordsHw2Sw)(uint32 *longRegVal, uint32 *macLsb, uint32 *macMsb))
    {
    if (self)
        return ExpectedDestMacGet(self, mac, MacDwordsHw2Sw);
    return cAtErrorNullPointer;
    }

uint32 Tha6029DccKByteSgmiiSupportedInterruptMasks(AtChannel self)
    {
    if (self)
        return SupportedInterruptMasks(self);
    return 0;
    }

eAtRet Tha6029DccKByteSgmiiInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    if (self)
        return InterruptMaskSet(self, defectMask, enableMask);
    return cAtErrorNullPointer;
    }

uint32 Tha6029DccKByteSgmiiInterruptMaskGet(AtChannel self)
    {
    if (self)
        return InterruptMaskGet(self);
    return 0;
    }

uint32 Tha6029DccKByteSgmiiDefectHistoryRead2Clear(AtChannel self, eBool r2c)
    {
    if (self)
        return DefectHistoryRead2Clear(self, r2c);
    return 0;
    }

uint32 Tha6029DccKByteSgmiiDefectGet(AtChannel self)
    {
    if (self)
        return DefectGet(self);
    return 0;
    }

eBool Tha6029DccKByteSgmiiCounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (self)
        return CounterIsSupported(self, counterType);
    return cAtFalse;
    }

uint32 Tha6029DccKByteSgmiiCounterRead2Clear(AtChannel self, uint16 counterType, eBool r2c)
    {
    if (self)
        return CounterRead2Clear(self, counterType, r2c);
    return 0;
    }

eBool Tha6029DccKByteSgmiiIsDccKByteCounter(AtChannel self, uint16 counterType)
    {
    if (self)
        return (IsDccCounter(self, counterType) || IsKByteCounter(self, counterType)) ? cAtTrue : cAtFalse;
    return cAtFalse;
    }

uint32 Tha6029DccKbyteEthPortCounterV1BaseAddress(ThaEthPort self, uint16 counterType)
    {
    return CounterBaseAddress((AtChannel)self, counterType);
    }

uint32 Tha6029DccKbyteEthPortCounterV2BaseAddress(ThaEthPort self, uint16 counterType)
    {
    uint32 address = DccCounterV2BaseAddress((AtChannel)self, counterType);
    if (address == cInvalidUint32)
        return CounterBaseAddress((AtChannel)self, counterType);
    return address;
    }

uint32 Tha6029MroDccKbyteEthPortCounterV3BaseAddress(ThaEthPort self, uint16 counterType)
    {
    return MroCounterV3BaseAddress((AtChannel)self, counterType);
    }

uint32 Tha6029PdhDccKbyteEthPortCounterV3BaseAddress(ThaEthPort self, uint16 counterType)
    {
    return PdhCounterV3BaseAddress((AtChannel)self, counterType);
    }


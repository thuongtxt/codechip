/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device Management
 *
 * File        : Tha6029CommonDevice.c
 *
 * Created Date: Sep 21, 2016
 *
 * Description : Ciena Common Device Management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtDevice.h"
#include "Tha6029CommonDevice.h"
#include "Tha6029InterruptPin.h"
#include "commacro.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6TopGlobalBaseAddress		0xF45000UL
#define cAf6Reg_Device_JtagId_Offset	0x0UL
#define cAf6Reg_Device_Version_Offset	0x1UL
#define cAf6Reg_Device_Year_Offset		0x2UL
#define cAf6Reg_Device_Week_Offset		0x3UL
#define cAf6Reg_Device_BuildId_Offset	0x4UL
#define cAf6Reg_Device_TimeStamp_Offset	0x8UL
#define cAf6Reg_Device_Type_Offset		0x10UL
#define cAf6Reg_Device_Family_Offset	0x14UL
#define cAf6Reg_Device_Arch_Offset		0x18UL
#define cAf6Reg_Device_Author_Offset	0x1CUL

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(void)
	{
	return cAf6TopGlobalBaseAddress;
	}

static uint32 Read(AtDevice self, uint32 offset)
	{
	AtHal hal = AtDeviceIpCoreHalGet(self, 0);
	return AtHalRead(hal, BaseAddress() + offset);
	}

static eBool IsLittleEndian(void)
    {
    uint32 word = (uint32)'a';
    char *string = (char*)(&word);
    if (string[0] == 'a')
        return cAtTrue;
    return cAtFalse;
    }

static uint32 ReverseBytes(uint32 value)
    {
    return (((value & cBit7_0)   << 24) |
            ((value & cBit15_8)  << 8)  |
            ((value & cBit23_16) >> 8)  |
            ((value & cBit31_24) >> 24));
    }

static uint32 DeviceJtagId(AtDevice self)
    {
    return Read(self, cAf6Reg_Device_JtagId_Offset);
    }

static uint32 DeviceVersion(AtDevice self)
    {
    return Read(self, cAf6Reg_Device_Version_Offset);
    }

static uint32 FpgaYear(AtDevice self)
    {
    return Read(self, cAf6Reg_Device_Year_Offset);
    }

static uint32 FpgaWeek(AtDevice self)
    {
    return Read(self, cAf6Reg_Device_Week_Offset);
    }

static uint32 FpgaBuildId(AtDevice self)
    {
    return Read(self, cAf6Reg_Device_BuildId_Offset);
    }

static char *StringRead(AtDevice self, char *buffer, uint32 bufferLen, uint32 localAddress, uint32 numWords)
    {
    uint32 word_i;
    uint32 *regs = (void *)buffer;
    uint32 maxWords = bufferLen / sizeof(uint32);

    if (maxWords > numWords)
        maxWords = numWords;

    for (word_i = 0; word_i < maxWords ; word_i++)
        {
        regs[word_i] = Read(self, localAddress + word_i);
        if (IsLittleEndian())
            regs[word_i] = ReverseBytes(regs[word_i]);
        }

    return buffer;
    }

static char *BuildTimeStamp(AtDevice self, char *buffer, uint32 bufferLen)
    {
    return StringRead(self, buffer, bufferLen, cAf6Reg_Device_TimeStamp_Offset, 8);
    }

static char *DeviceType(AtDevice self, char *buffer, uint32 bufferLen)
    {
    return StringRead(self, buffer, bufferLen, cAf6Reg_Device_Type_Offset, 4);
    }

static char *DeviceFamily(AtDevice self, char *buffer, uint32 bufferLen)
    {
    return StringRead(self, buffer, bufferLen, cAf6Reg_Device_Family_Offset, 4);
    }

static char *DeviceArch(AtDevice self, char *buffer, uint32 bufferLen)
    {
    return StringRead(self, buffer, bufferLen, cAf6Reg_Device_Arch_Offset, 4);
    }

static char *DeviceAuthor(AtDevice self, char *buffer, uint32 bufferLen)
    {
    return StringRead(self, buffer, bufferLen, cAf6Reg_Device_Author_Offset, 4);
    }

uint32 Tha6029CommonDeviceJtagId(AtDevice self)
	{
	if (self)
	    return DeviceJtagId(self);
	return cInvalidUint32;
	}

uint32 Tha6029CommonDeviceVersion(AtDevice self)
	{
	if (self)
	    return DeviceVersion(self);
	return cInvalidUint32;
	}

uint32 Tha6029CommonDeviceFpgaYear(AtDevice self)
	{
    if (self)
        return FpgaYear(self);
    return cInvalidUint32;
	}

uint32 Tha6029CommonDeviceFpgaWeek(AtDevice self)
    {
    if (self)
        return FpgaWeek(self);
    return cInvalidUint32;
    }

uint32 Tha6029CommonDeviceFpgaBuildId(AtDevice self)
    {
    if (self)
        return FpgaBuildId(self);
    return cInvalidUint32;
    }

char *Tha6029CommonBuildTimeStamp(AtDevice self, char *buffer, uint32 bufferLen)
    {
    if (self)
        return BuildTimeStamp(self, buffer, bufferLen);
    return NULL;
    }

char *Tha6029CommonDeviceType(AtDevice self, char *buffer, uint32 bufferLen)
	{
    if (self)
        return DeviceType(self, buffer, bufferLen);
    return NULL;
	}

char *Tha6029CommonDeviceFamily(AtDevice self, char *buffer, uint32 bufferLen)
    {
    if (self)
        return DeviceFamily(self, buffer, bufferLen);
    return NULL;
	}

char *Tha6029CommonDeviceArch(AtDevice self, char *buffer, uint32 bufferLen)
    {
    if (self)
        return DeviceArch(self, buffer, bufferLen);
    return NULL;
	}

char *Tha6029CommonDeviceAuthor(AtDevice self, char *buffer, uint32 bufferLen)
    {
    if (self)
        return DeviceAuthor(self, buffer, bufferLen);
    return NULL;
	}

uint32 Tha6029DeviceNumInterruptPins(AtDevice self)
    {
    AtUnused(self);
    return 2;
    }

AtInterruptPin Tha6029DeviceInterruptPinObjectCreate(AtDevice self, uint32 pinId)
    {
    return Tha6029InterruptPinNew(self, pinId);
    }

eAtTriggerMode Tha6029DeviceInterruptPinDefaultTriggerMode(AtDevice self, AtInterruptPin pin)
    {
    AtUnused(self);
    AtUnused(pin);
    return cAtTriggerModeLevelHigh;
    }

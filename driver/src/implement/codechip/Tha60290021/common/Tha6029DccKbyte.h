/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : Tha6029DccKbyte.h
 * 
 * Created Date: Jan 24, 2017
 *
 * Description : Common functions for DCC/KByte over SGMII
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029DCCKBYTE_H_
#define _THA6029DCCKBYTE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/eth/ThaEthPort.h"
#include "../../../default/eth/ThaModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eTha6029SgmiiPortAlarmType
    {
    cTha6029EthAlarmTypeDccDaMacMisMatch            = cBit30,  /*  Received DA value of APS frame different from configured DA */
    cTha6029EthAlarmTypeDccCVlanMisMatch            = cBit29,  /*  Received 12b CVLAN ID value of DCC frame different from global provisioned CVID  */
    cTha6029EthAlarmTypeDccLenOverSize              = cAtEthPortAlarmOversized,  /*  Received DCC Length is over-size */
    cTha6029EthAlarmTypeFcsError                    = cAtEthPortAlarmFcsError,  /*  Received packet from SGMII port has FCS error */
    cTha6029EthAlarmTypeEthTypeMisMatch             = cBit28   /*  ETH Type mismatch */
    }eTha6029SgmiiPortAlarmType;
    
typedef enum eTha602SgmiiPortCounterType
    {
    cTha602SgmiiPortCounterTypeDccTxFrames   = cBit15,
	cTha602SgmiiPortCounterTypeDccTxBytes,
	cTha602SgmiiPortCounterTypeDccTxErrors,
    cTha602SgmiiPortCounterTypeDccRxFrames,
	cTha602SgmiiPortCounterTypeDccRxBytes,
	cTha602SgmiiPortCounterTypeDccRxErrors,
    cTha602SgmiiPortCounterTypeKByteTxFrames,
    cTha602SgmiiPortCounterTypeKByteTxBytes,
    cTha602SgmiiPortCounterTypeKByteRxFrames,
    cTha602SgmiiPortCounterTypeDccRxUnrecognizedFrames,
    cTha602SgmiiPortCounterTypeKbyteRxUnrecognizedFrames,
    cTha602SgmiiPortCounterTypeDccRxGoodFrames,
    cTha602SgmiiPortCounterTypeDccRxDiscardFrames,
    cTha602SgmiiPortCounterTypeKbyteRxGoodFrames,
    cTha602SgmiiPortCounterTypeKbyteRxDiscardFrames
    }eTha602SgmiiPortCounterType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha6029DccKByteSgmiiExpectedDestMacSet(ThaEthPort self, const uint8 *mac, uint32 macMsbMask, uint32 macMsbShift, void (*MacDwordsSw2Hw)(uint32 macLsbVal, uint32 macMsbVal, uint32 *hwMacLsb, uint32 *hwMacMsb));
eAtRet Tha6029DccKByteSgmiiExpectedDestMacGet(ThaEthPort self, uint8 *mac, void (*MacDwordsHw2Sw)(uint32 *longRegVal, uint32 *macLsb, uint32 *macMsb));
uint32 Tha6029DccKByteSgmiiSupportedInterruptMasks(AtChannel self);
eAtRet Tha6029DccKByteSgmiiInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask);
uint32 Tha6029DccKByteSgmiiInterruptMaskGet(AtChannel self);
uint32 Tha6029DccKByteSgmiiDefectHistoryRead2Clear(AtChannel self, eBool r2c);
uint32 Tha6029DccKByteSgmiiDefectGet(AtChannel self);
eBool Tha6029DccKByteSgmiiExpectedCVlanIsSupported(AtEthPort self);
eAtRet Tha6029DccKByteSgmiiExpectedCVlanSet(AtEthPort self, uint16 vlanId, uint32 hwVlanIdMask, uint32 hwVlanIdShift);
uint16 Tha6029DccKByteSgmiiExpectedCVlanGet(AtEthPort self, uint32 hwVlanIdMask, uint32 hwVlanIdShift);
eBool Tha6029DccKByteSgmiiCounterIsSupported(AtChannel self, uint16 counterType);
uint32 Tha6029DccKByteSgmiiCounterRead2Clear(AtChannel self, uint16 counterType, eBool r2c);
eBool Tha6029DccKByteSgmiiIsDccKByteCounter(AtChannel self, uint16 counterType);
uint32 Tha6029DccKbyteEthPortCounterV1BaseAddress(ThaEthPort self, uint16 counterType);
uint32 Tha6029DccKbyteEthPortCounterV2BaseAddress(ThaEthPort self, uint16 counterType);
uint32 Tha6029MroDccKbyteEthPortCounterV3BaseAddress(ThaEthPort self, uint16 counterType);
uint32 Tha6029PdhDccKbyteEthPortCounterV3BaseAddress(ThaEthPort self, uint16 counterType);

/* Version 2 */
uint32 Tha6029DccKByteV2SgmiiSupportedInterruptMasks(AtChannel self);
eAtRet Tha6029DccKByteV2SgmiiInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask);
uint32 Tha6029DccKByteV2SgmiiInterruptMaskGet(AtChannel self);
uint32 Tha6029DccKByteV2SgmiiInterruptProcess(AtChannel self, uint32 offset);
uint32 Tha6029DccKByteV2SgmiiDefectHistoryRead2Clear(AtChannel self, eBool r2c);
uint32 Tha6029DccKByteV2SgmiiDefectGet(AtChannel self);

eAtRet Tha6029ModuleEthTxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength);
uint32 Tha6029ModuleEthTxDccPacketMaxLengthGet(ThaModuleEth self);
eAtRet Tha6029ModuleEthRxDccPacketMaxLengthSet(ThaModuleEth self, uint32 maxLength);
uint32 Tha6029ModuleEthRxDccPacketMaxLengthGet(ThaModuleEth self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029DCCKBYTE_H_ */


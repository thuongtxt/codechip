/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device Management
 * 
 * File        : Tha6029CommonDevice.h
 * 
 * Created Date: Sep 21, 2016
 *
 * Description : Ciena Common Device Management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029COMMONDEVICE_H_
#define _THA6029COMMONDEVICE_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha6029CommonDeviceJtagId(AtDevice self);
uint32 Tha6029CommonDeviceVersion(AtDevice self);
uint32 Tha6029CommonDeviceFpgaYear(AtDevice self);
uint32 Tha6029CommonDeviceFpgaWeek(AtDevice self);
uint32 Tha6029CommonDeviceFpgaBuildId(AtDevice self);
char *Tha6029CommonBuildTimeStamp(AtDevice self, char *buffer, uint32 bufferLen);
char *Tha6029CommonDeviceType(AtDevice self, char *buffer, uint32 bufferLen);
char *Tha6029CommonDeviceFamily(AtDevice self, char *buffer, uint32 bufferLen);
char *Tha6029CommonDeviceArch(AtDevice self, char *buffer, uint32 bufferLen);
char *Tha6029CommonDeviceAuthor(AtDevice self, char *buffer, uint32 bufferLen);

uint32 Tha6029DeviceNumInterruptPins(AtDevice self);
AtInterruptPin Tha6029DeviceInterruptPinObjectCreate(AtDevice self, uint32 pinId);
eAtTriggerMode Tha6029DeviceInterruptPinDefaultTriggerMode(AtDevice self, AtInterruptPin pin);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029COMMONDEVICE_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Common
 * 
 * File        : Tha602900xxCommon.h
 * 
 * Created Date: Mar 10, 2017
 *
 * Description : Util functions for 602900xx products
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA602900XXCOMMON_H_
#define _THA602900XXCOMMON_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSur.h"
#include "../../../default/pdh/ThaPdhDe1.h"
#include "../../../default/sur/hard/ThaModuleHardSur.h"
#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/prbs/ThaModulePrbs.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cTha602900xxShouldHandleCircuitAisForcingOnEnabling cAtTrue

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eTha602900xxEthPortCounterTickMode
    {
    cTha602900xxEthPortCounterTickModeUnknown,
    cTha602900xxEthPortCounterTickModeDisable, /* HW counter logic is disabled */
    cTha602900xxEthPortCounterTickModeAuto,
    cTha602900xxEthPortCounterTickModeManual
    }eTha602900xxEthPortCounterTickMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha602900xxSubPortVlanDefaultTpidSet(ThaModulePw self);
eBool Tha602900xxDe1LocalPayloadIsSupported(AtChannel self, uint8 loopbackMode);
eBool Tha602900xxPdhDe1ShouldPreserveService(ThaPdhDe1 self);
eBool Tha602900xxModulePrbsDe3ChannelizedPrbsIsSupported(ThaModulePrbs self);
eBool Tha602900xxNeedClearanceNotifyLogic(void);

eAtRet Tha602900xxSubPortVlanDefaultTpidSet(ThaModulePw self);

eBool Tha602900xxPdhDe1ShouldPreserveService(ThaPdhDe1 self);

eAtSurPeriodExpireMethod Tha602900xxModuleSurDefaultExpireMethod(ThaModuleHardSur self);
eAtSurTickSource Tha602900xxModuleSurDefaultTickSource(ThaModuleHardSur self);

eBool Tha602900xxDeviceAccessible(AtDevice self);
eBool Tha602900xxSdhLineShouldHideLofWhenLos(AtSdhLine self);
eBool Tha602900xxFlexiblePsnSide(ThaModulePrbs self);
eBool Tha602900xxShouldMaskFailuresWhenNonAutonomousFailures(AtModuleSur self);
eBool Tha602900xxShouldRestoreDatabaseOnWarmRestoreStop(AtDevice self);

eAtModuleEthRet Tha602900xxSerdesBackplaneCounterTickModeSet(AtEthPort self, eTha602900xxEthPortCounterTickMode mode);
eTha602900xxEthPortCounterTickMode Tha602900xxSerdesBackplaneCounterTickModeGet(AtEthPort self);
eAtModuleEthRet Tha602900xxEthPortCounterTickModeSet(AtEthPort port, eTha602900xxEthPortCounterTickMode mode);
eTha602900xxEthPortCounterTickMode Tha602900xxEthPortCounterTickModeGet(AtEthPort port);
const char *Tha602900xxEthPortCounterTickMode2String(eTha602900xxEthPortCounterTickMode mode);

eBool Tha602900xxModulePrbsChannelizedPrbsAllowed(AtModulePrbs self);
eBool Tha602900xxUseSwPsnGenMode(ThaModulePrbs self);

eAtRet Tha602900xxModulePwDccHdlcPwObjectDelete(AtModulePw self, AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA602900XXCOMMON_H_ */


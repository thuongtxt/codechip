/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha6029SerdesTunerInternal.h
 * 
 * Created Date: Oct 7, 2016
 *
 * Description : SERDES tuner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SERDESTUNERINTERNAL_H_
#define _THA6029SERDESTUNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtTimer.h"

#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../../generic/man/AtDeviceInternal.h"
#include "Tha6029SerdesTuner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SerdesTunerMethods
    {
    uint32 (*RawPrbsSyncChangeMask)(Tha6029SerdesTuner self, uint32 localId);
    uint32 (*RawPrbsSyncMask)(Tha6029SerdesTuner self, uint32 localId);
    uint32 (*DefaultDrpBaseAddress)(Tha6029SerdesTuner self);
    AtDrp (*DrpObjectCreate)(Tha6029SerdesTuner self, uint32 baseAddress, AtHal hal);
    uint32 (*LocalId)(Tha6029SerdesTuner self);
    uint32 (*AbsoluteOffset)(Tha6029SerdesTuner self);
    eBool (*UseQpll)(Tha6029SerdesTuner self);
    eAtRet (*EyeScanReset)(Tha6029SerdesTuner self, uint32 laneId);

    /* Registers */
    uint32 (*TxPowerDownMask)(Tha6029SerdesTuner self);
    uint32 (*TxPowerDownShift)(Tha6029SerdesTuner self);
    }tTha6029SerdesTunerMethods;

typedef struct tTha6029SerdesTuner
    {
    tAtObject super;
    const tTha6029SerdesTunerMethods *methods;

    /* Private data */
    AtSerdesController serdes;
    AtTimer prbsGatingTimer;

    /* As hardware does not support enabling, but use mode 0 for disabling PRBS.
     * So, need to cache this mode */
    eAtPrbsMode mode;
    eBool enabled;
    }tTha6029SerdesTuner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha6029SerdesTuner Tha6029SerdesTunerObjectInit(Tha6029SerdesTuner self, AtSerdesController serdes);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SERDESTUNERINTERNAL_H_ */


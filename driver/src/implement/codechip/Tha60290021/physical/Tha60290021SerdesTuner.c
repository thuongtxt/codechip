/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290021SerdesTuner.c
 *
 * Created Date: Oct 22, 2016
 *
 * Description : SERDES tuner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "Tha60290021SerdesTunerInternal.h"
#include "Tha6029SerdesTunningReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021SerdesTuner)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021SerdesTunerMethods m_methods;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tTha6029SerdesTunerMethods m_Tha6029SerdesTunerOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DrpLocalBaseAddress(Tha60290021SerdesTuner self)
    {
    AtUnused(self);
    return cAf6Reg_SERDES_DRP_PORT_Base;
    }

static uint32 LocalId(Tha6029SerdesTuner self)
    {
    return mThis(self)->localId;
    }

static uint32 BaseAddress(Tha6029SerdesTuner self)
    {
    return AtSerdesControllerBaseAddress(self->serdes);
    }

static uint32 GroupOffset(Tha6029SerdesTuner self)
    {
    return mThis(self)->groupId * 0x2000UL;
    }

static uint32 Offset(Tha6029SerdesTuner self)
    {
    return GroupOffset(self) + (mMethodsGet(self)->LocalId(self) * 0x400UL);
    }

static uint32 GroupAbsoluteOffset(Tha6029SerdesTuner self)
    {
    return BaseAddress(self) + GroupOffset(self);
    }

static uint32 DefaultDrpBaseAddress(Tha6029SerdesTuner self)
    {
    return BaseAddress(self) + mMethodsGet(mThis(self))->DrpLocalBaseAddress(mThis(self)) + Offset(self);
    }

static uint32 AbsoluteOffset(Tha6029SerdesTuner self)
    {
    return GroupAbsoluteOffset(self);
    }

static eBool UseQpll(Tha6029SerdesTuner self)
    {
    /* TODO: It is better to move this overridden to MATE sub class */
    AtUnused(self);
    return cAtTrue;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290021SerdesTuner object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(groupId);
    mEncodeUInt(localId);
    }

static void OverrideAtObject(Tha6029SerdesTuner self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideTha6029SerdesTuner(Tha6029SerdesTuner self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SerdesTunerOverride, mMethodsGet(self), sizeof(m_Tha6029SerdesTunerOverride));

        mMethodOverride(m_Tha6029SerdesTunerOverride, DefaultDrpBaseAddress);
        mMethodOverride(m_Tha6029SerdesTunerOverride, LocalId);
        mMethodOverride(m_Tha6029SerdesTunerOverride, AbsoluteOffset);
        mMethodOverride(m_Tha6029SerdesTunerOverride, UseQpll);
        }

    mMethodsSet(self, &m_Tha6029SerdesTunerOverride);
    }

static void Override(Tha6029SerdesTuner self)
    {
    OverrideAtObject(self);
    OverrideTha6029SerdesTuner(self);
    }

static void MethodsInit(Tha60290021SerdesTuner self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DrpLocalBaseAddress);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SerdesTuner);
    }

Tha6029SerdesTuner Tha60290021SerdesTunerObjectInit(Tha6029SerdesTuner self, AtSerdesController serdes, uint32 groupId, uint32 localPortId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesTunerObjectInit(self, serdes) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    mThis(self)->groupId = groupId;
    mThis(self)->localId = localPortId;

    return self;
    }

Tha6029SerdesTuner Tha60290021SerdesTunerNew(AtSerdesController serdes, uint32 groupId, uint32 localPortId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha6029SerdesTuner newTuner = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60290021SerdesTunerObjectInit(newTuner, serdes, groupId, localPortId);
    }


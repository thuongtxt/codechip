/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60290021SerdesTunerInternal.h
 * 
 * Created Date: Oct 22, 2016
 *
 * Description : SERDES tuner
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021SERDESTUNERINTERNAL_H_
#define _THA60290021SERDESTUNERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha6029SerdesTunerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021SerdesTuner * Tha60290021SerdesTuner;

typedef struct tTha60290021SerdesTunerMethods
    {
    uint32 (*DrpLocalBaseAddress)(Tha60290021SerdesTuner self);
    }tTha60290021SerdesTunerMethods;

typedef struct tTha60290021SerdesTuner
    {
    tTha6029SerdesTuner super;
    const tTha60290021SerdesTunerMethods *methods;

    /* Private data */
    uint32 groupId;
    uint32 localId;
    }tTha60290021SerdesTuner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha6029SerdesTuner Tha60290021SerdesTunerObjectInit(Tha6029SerdesTuner self, AtSerdesController serdes, uint32 groupId, uint32 localPortId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021SERDESTUNERINTERNAL_H_ */


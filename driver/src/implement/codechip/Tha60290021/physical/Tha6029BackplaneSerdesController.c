/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Serdes controller
 *
 * File        : Tha6029BackplaneSerdesController.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Backplane Serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModulePtp.h"
#include "../../../default/ptp/ThaPtpPort.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../man/Tha60290021Device.h"
#include "../prbs/Tha6029SerdesPrbsEngine.h"
#include "Tha6029BackplaneSerdesControllerInternal.h"
#include "Tha6029Physical.h"
#include "Tha6029SerdesEth40GReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cResetTriggerMask  cBit0
#define cResetTriggerShift 0
#define cResetDoneMask     cBit16
#define cResetDoneShift    16

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6029BackplaneSerdesController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods           = NULL;
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(AtSerdesController self)
    {
    return AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self));
    }

static AtModuleEth ModuleEth(AtSerdesController self)
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(self), cAtModuleEth);
    }

static uint32 HwIdGet(AtSerdesController self)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    return (uint32) (Tha60290021SerdesManagerBackplaneSerdesLocalId(manager, AtSerdesControllerIdGet(self)));
    }

static uint32 BaseAddress(AtSerdesController self)
    {
    if (AtSerdesControllerHwIdGet(self) == 0)
        return cSerdesEth40GFirstPortBaseAdress;

    return cSerdesEth40GSecondPortBaseAdress;
    }

static eAtSerdesMode DefaultMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeEth40G;
    }

static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return (mode == cAtSerdesModeEth40G) ? cAtTrue : cAtFalse;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    if (AtSerdesControllerModeIsSupported(self, mode) == cAtFalse)
        return cAtErrorModeNotSupport;

    if (AtSerdesControllerPhysicalPortGet(self) == NULL)
        AtSerdesControllerPhysicalPortSet(self, (AtChannel)AtModuleEthPortGet(ModuleEth(self), 0));

    return cAtOk;
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeEth40G;
    }

static eBool PowerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet PowerDown(AtSerdesController self, eBool powerDown)
    {
    AtUnused(self);
    return powerDown ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool PowerIsDown(AtSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return (loopbackMode != cAtLoopbackModeDual);
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    eAtRet ret = cAtOk;
    uint32 lane_i, numLanes = AtSerdesControllerNumLanes(self);

    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        AtSerdesController lane = AtSerdesControllerLaneGet(self, lane_i);
        ret |= AtSerdesControllerLoopbackEnable(lane, loopbackMode, enable);
        }

    /* XILINX guideline that SERDES reset needs to be done on loopback operation.
     * Only reset SERDES when it is powerup, otherwise, timeout error code will be returned */
    if (!AtSerdesControllerPowerIsDown(self))
        ret |= AtSerdesControllerReset(self);

    return ret;
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    uint32 lane_i, numLanes = AtSerdesControllerNumLanes(self);
    AtSerdesController lane = AtSerdesControllerLaneGet(self, 0);
    eBool enabled = AtSerdesControllerLoopbackIsEnabled(lane, loopbackMode);

    /* Make sure that other lanes also have the same loopback mode */
    for (lane_i = 1; lane_i < numLanes; lane_i++)
        {
        lane = AtSerdesControllerLaneGet(self, lane_i);
        if (mBoolToBin(AtSerdesControllerLoopbackIsEnabled(lane, loopbackMode)) != mBoolToBin(enabled))
            return cAtFalse;
        }

    return enabled;
    }

static eBool IsSimulated(AtSerdesController self)
    {
    AtDevice device = AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self));
    return AtDeviceIsSimulated(device);
    }

static uint32 RegisterWithLocalAddress(AtSerdesController self, uint32 localAddress)
    {
    return AtSerdesControllerBaseAddress(self) + localAddress;
    }

static eBool HwResetDone(AtSerdesController self, uint32 localAddress)
    {
    uint32 regAddr = RegisterWithLocalAddress(self, localAddress);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    return (regVal & cResetDoneMask) ? cAtTrue : cAtFalse;
    }

static eAtRet ResetTrigger(AtSerdesController self, uint32 localAddress)
    {
    uint32 regAddr = RegisterWithLocalAddress(self, localAddress);
    uint32 regVal;

    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cResetDone, 1);
    mRegFieldSet(regVal, cResetTrigger, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    mRegFieldSet(regVal, cResetTrigger, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 TimeoutMs(AtSerdesController self)
    {
    AtDevice device = Device(self);
    if (AtDeviceTestbenchIsEnabled(device))
        return AtDeviceTestbenchDefaultTimeoutMs();
    return 500;
    }

static eAtRet ResetWithLocalAddress(AtSerdesController self, uint32 localAddress, const char *direction)
    {
    uint32 timeoutMs = TimeoutMs(self);
    uint32 elapseTimeMs = 0;
    tAtOsalCurTime startTime, curTime;
    eAtRet ret = cAtOk;

    /* Trigger reset */
    ResetTrigger(self, localAddress);

    /* Till hardware done */
    AtOsalCurTimeGet(&startTime);
    while (elapseTimeMs < timeoutMs)
        {
        if (HwResetDone(self, localAddress))
            return cAtOk;

        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);

        if (IsSimulated(self))
            return cAtOk;
        }

    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation,
                            "[%s] Reset backplane SERDES %d fail with ret = %s\r\n",
                            direction, AtSerdesControllerIdGet(self) + 1, AtRet2String(ret));

    return cAtErrorOperationTimeout;
    }

static eAtRet Reset(AtSerdesController self)
    {
    eAtRet ret = cAtOk;

    if (AtDeviceWarmRestoreIsStarted(AtSerdesControllerDeviceGet(self)))
        return cAtOk;

    ret |= ResetWithLocalAddress(self, cAf6Reg_ETH_40G_TX_Reset_Base, "TX");
    if (!IsSimulated(self))
        AtOsalUSleep(1000);
    ret |= ResetWithLocalAddress(self, cAf6Reg_ETH_49G_RX_Reset_Base, "RX");

    return ret;
    }

static eAtRet RxReset(AtSerdesController self)
    {
    if (AtDeviceWarmRestoreIsStarted(AtSerdesControllerDeviceGet(self)))
        return cAtOk;

    return ResetWithLocalAddress(self, cAf6Reg_ETH_49G_RX_Reset_Base, "RX");
    }

static eAtRet TxReset(AtSerdesController self)
    {
    if (AtDeviceWarmRestoreIsStarted(AtSerdesControllerDeviceGet(self)))
        return cAtOk;

    return ResetWithLocalAddress(self, cAf6Reg_ETH_40G_TX_Reset_Base, "TX");
    }

static uint32 QPllStatusRegister(AtSerdesController self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_ETH_40G_QLL_Status_Base);
    }

static uint32 QpllLockStatusMask(AtSerdesController self)
    {
    /* There are two QPLLs, at a time, only one of them is used and this is
     * depend on the way IP is used. For this project, only QPLL0 is used */
    AtUnused(self);
    return cAf6_ETH_40G_QLL_Status_QPLL0_Lock_Mask;
    }

static uint32 QpllLockStickyMask(AtSerdesController self)
    {
    /* There are two QPLLs, at a time, only one of them is used and this is
     * depend on the way IP is used. For this project, only QPLL0 is used */
    AtUnused(self);
    return cAf6_ETH_40G_QLL_Status_QPLL0_Lock_change_Mask;
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    uint32 regAddr = QPllStatusRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 qpll = regVal & QpllLockStatusMask(self);

    return qpll ? cAtTrue : cAtFalse;
    }

static eBool PllChanged(AtSerdesController self)
    {
    uint32 regAddr = QPllStatusRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 qpll = regVal & QpllLockStickyMask(self);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return qpll ? cAtTrue : cAtFalse;
    }

static uint32 ParamMask(AtSerdesController self)
    {
    AtUnused(self);
    return cBit4_0;
    }

static uint32 ParamShift(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet PhysicalParamSetWithBaseRegister(AtSerdesController self, uint32 localAddress, uint32 value)
    {
    uint32 regAddr    = RegisterWithLocalAddress(self, localAddress);
    uint32 regVal     = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 paramMask  = ParamMask(self);
    uint32 paramShift = ParamShift(self);

    mRegFieldSet(regVal, param, value);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 PhysicalParamGetWithBaseRegister(AtSerdesController self, uint32 localAddress)
    {
    uint32 regAddr    = RegisterWithLocalAddress(self, localAddress);
    uint32 regVal     = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 paramMask  = ParamMask(self);
    uint32 paramShift = ParamShift(self);
    return mRegField(regVal, param);
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    if (!AtSerdesControllerPhysicalParamValueIsInRange(self, param, value))
        return cAtErrorOutOfRangParm;

    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor:
            return PhysicalParamSetWithBaseRegister(self, cAf6Reg_ETH_40G_TXPRECURSOR_Base, value);
        case cAtSerdesParamTxPostCursor:
            return PhysicalParamSetWithBaseRegister(self, cAf6Reg_ETH_40G_TXPOSTCURSOR_Base, value);
        case cAtSerdesParamTxDiffCtrl:
            return PhysicalParamSetWithBaseRegister(self, cAf6Reg_ETH_40G_TXDIFFCTRL_Base, value);

        default:
            return cAtErrorModeNotSupport;
        }
    }

static uint32 PhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor:
            return PhysicalParamGetWithBaseRegister(self, cAf6Reg_ETH_40G_TXPRECURSOR_Base);
        case cAtSerdesParamTxPostCursor:
            return PhysicalParamGetWithBaseRegister(self, cAf6Reg_ETH_40G_TXPOSTCURSOR_Base);
        case cAtSerdesParamTxDiffCtrl:
            return PhysicalParamGetWithBaseRegister(self, cAf6Reg_ETH_40G_TXDIFFCTRL_Base);
        default:
            return cInvalidUint32;
        }
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
    AtUnused(self);

    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor : return cAtTrue;
        case cAtSerdesParamTxPostCursor: return cAtTrue;
        case cAtSerdesParamTxDiffCtrl  : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eBool PhysicalParamValueIsInRange(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    uint32 maxValue = ParamMask(self) >> ParamShift(self);
    AtUnused(param);
    return (value <= maxValue) ? cAtTrue : cAtFalse;
    }

static eAtSerdesEqualizerMode DefaultEqualizerMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesEqualizerModeDfe;
    }

static uint32 EqualizerModeRegister(AtSerdesController self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_ETH_40G_LPMDFE_Mode_Base);
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    uint32 regAddr, regVal;

    regAddr = EqualizerModeRegister(self);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ETH_40G_LPMDFE_Mode_lpmdfe_mode_, (mode == cAtSerdesEqualizerModeLpm) ? 1 : 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* Need to toggle reset */
    regAddr = RegisterWithLocalAddress(self, cAf6Reg_ETH_40G_LPMDFE_Reset_Base);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ETH_40G_LPMDFE_Reset_lpmdfe_reset_, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ETH_40G_LPMDFE_Reset_lpmdfe_reset_, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_ETH_40G_LPMDFE_Reset_lpmdfe_reset_, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    uint32 regAddr = EqualizerModeRegister(self);
    uint32 regVal  = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_ETH_40G_LPMDFE_Mode_lpmdfe_mode_Mask) ? cAtSerdesEqualizerModeLpm : cAtSerdesEqualizerModeDfe;
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtUnused(self);

    if ((mode == cAtSerdesEqualizerModeDfe) ||
        (mode == cAtSerdesEqualizerModeLpm))
        return cAtTrue;

    return cAtFalse;
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanControlTimingMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 NumLanes(AtSerdesController self)
    {
    AtUnused(self);
    return 4;
    }

static AtSerdesController LaneObjectCreate(AtSerdesController self, uint32 laneIdx)
    {
    AtChannel port = AtSerdesControllerPhysicalPortGet(self);
    AtSerdesController parent = AtSerdesControllerParentGet(self);
    return Tha6029BacklaneSerdesLaneNew(AtSerdesControllerManagerGet(parent), port, laneIdx);
    }

static uint32 LocalId(AtSerdesController self)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    return Tha60290021SerdesManagerBackplaneSerdesLocalId(manager, AtSerdesControllerIdGet(self));
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    uint32 localId = LocalId((AtSerdesController)self);
    AtSnprintf(description, sizeof(description) - 1, "backplane_serdes.%d", localId + 1);
    return description;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    return Tha60290021BackplaneSerdesPrbsEngineNew(self);
    }

static AtChannel PhysicalPortGet(AtSerdesController self)
    {
    AtDevice device = AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self));
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    if (ethModule)
    	return (AtChannel)Tha60290021ModuleEth40GMacPortGet(ethModule, LocalId(self));
    return NULL;
    }

static eBool IsControllable(AtSerdesController self)
    {
    return Tha60290021DeviceIsRunningOnOtherPlatform(AtSerdesControllerDeviceGet(self)) ? cAtFalse : cAtTrue;
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    Tha60290021SerdesManager manager = (Tha60290021SerdesManager)AtSerdesControllerManagerGet(self);
    return Tha60290021SerdesManagerEyeScanControllerCreate(manager, self, drpBaseAddress);
    }

static const char *IdString(AtSerdesController self)
    {
    static char idString[32];
    AtSerdesController parent = AtSerdesControllerParentGet(self);
    AtSnprintf(idString, sizeof(idString), "%u.%u", AtSerdesControllerIdGet(parent) + 1, AtSerdesControllerIdGet(self) + 1);
    return idString;
    }

static eAtRet EyeScanReset(AtSerdesController self, uint32 laneId)
    {
    AtUnused(self);
    AtUnused(laneId);
    return cAtOk;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, ModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultMode);
        mMethodOverride(m_AtSerdesControllerOverride, PowerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, PowerDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerIsDown);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, PllChanged);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamGet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamValueIsInRange);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultEqualizerMode);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, CanControlTimingMode);
        mMethodOverride(m_AtSerdesControllerOverride, NumLanes);
        mMethodOverride(m_AtSerdesControllerOverride, LaneObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalPortGet);
        mMethodOverride(m_AtSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, HwIdGet);
        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);
        mMethodOverride(m_AtSerdesControllerOverride, IdString);
        mMethodOverride(m_AtSerdesControllerOverride, RxReset);
        mMethodOverride(m_AtSerdesControllerOverride, TxReset);

        /* EyeScan */
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanReset);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029BackplaneSerdesController);
    }

AtSerdesController Tha6029BackplaneSerdesControllerObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesControllerObjectInit(self, physicalPort, serdesId, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha6029BackplaneSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6029BackplaneSerdesControllerObjectInit(newModule, manager, physicalPort, serdesId);
    }

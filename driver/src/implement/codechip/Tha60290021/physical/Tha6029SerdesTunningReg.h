/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive
 * Technologies. The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      :
 *
 * File        :
 *
 * Created Date:
 *
 * Description : This file contain all constance definitions of  block.
 *
 * Notes       : None
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_Xilinx_Serdes_Turning_Configuration_RD_H_
#define _AF6_REG_AF6CNC0021_Xilinx_Serdes_Turning_Configuration_RD_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : SERDES DRP PORT
Reg Addr   : 0x1000-0x7FFF
Reg Formula: 0x1000+$G*0x2000+$P*0x400+$DRP
    Where  : 
           + $G(0-1) : Group of Ports
           + $P(0-1) : Sub Port ID
           + $DRP(0-1023) : DRP address, see UG578
Reg Desc   : 
Read/Write DRP address of SERDES, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_DRP_PORT_Base                                                                    0x1000

/*--------------------------------------
BitField Name: drp_rw
BitField Type: R/W
BitField Desc: DRP read/write value
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_SERDES_DRP_PORT_drp_rw_Mask                                                               cBit9_0
#define cAf6_SERDES_DRP_PORT_drp_rw_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : SERDES LoopBack
Reg Addr   : 0x0002-0x6002
Reg Formula: 0x0002+$G*0x2000
    Where  : 
           + $G(0-1) : Group of Ports
Reg Desc   : 
Configurate LoopBack, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_LoopBack_Base                                                                    0x0002

/*--------------------------------------
BitField Name: lpback_subport3
BitField Type: R/W
BitField Desc: Loopback subport 3
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_SERDES_LoopBack_lpback_subport3_Mask                                                    cBit15_12
#define cAf6_SERDES_LoopBack_lpback_subport3_Shift                                                          12

/*--------------------------------------
BitField Name: lpback_subport2
BitField Type: R/W
BitField Desc: Loopback subport 2
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_SERDES_LoopBack_lpback_subport2_Mask                                                     cBit11_8
#define cAf6_SERDES_LoopBack_lpback_subport2_Shift                                                           8

/*--------------------------------------
BitField Name: lpback_subport1
BitField Type: R/W
BitField Desc: Loopback subport 1
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_SERDES_LoopBack_lpback_subport1_Mask                                                      cBit7_4
#define cAf6_SERDES_LoopBack_lpback_subport1_Shift                                                           4

/*--------------------------------------
BitField Name: lpback_subport0
BitField Type: R/W
BitField Desc: Loopback subport 0
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_SERDES_LoopBack_lpback_subport0_Mask                                                      cBit3_0
#define cAf6_SERDES_LoopBack_lpback_subport0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : SERDES POWER DOWN
Reg Addr   : 0x0003-0x6003
Reg Formula: 0x0003+$G*0x2000
    Where  : 
           + $G(0-1) : Group of Ports
Reg Desc   : 
Configurate Power Down , there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_POWER_DOWN_Base                                                                  0x0003

/*--------------------------------------
BitField Name: TXPD3
BitField Type: R/W
BitField Desc: Power Down subport 3
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXPD3_Mask                                                            cBit15_14
#define cAf6_SERDES_POWER_DOWN_TXPD3_Shift                                                                  14

/*--------------------------------------
BitField Name: TXPD2
BitField Type: R/W
BitField Desc: Power Down subport 2
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXPD2_Mask                                                            cBit13_12
#define cAf6_SERDES_POWER_DOWN_TXPD2_Shift                                                                  12

/*--------------------------------------
BitField Name: TXPD1
BitField Type: R/W
BitField Desc: Power Down subport 1
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXPD1_Mask                                                            cBit11_10
#define cAf6_SERDES_POWER_DOWN_TXPD1_Shift                                                                  10

/*--------------------------------------
BitField Name: TXPD0
BitField Type: R/W
BitField Desc: Power Down subport 0
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXPD0_Mask                                                              cBit9_8
#define cAf6_SERDES_POWER_DOWN_TXPD0_Shift                                                                   8

/*--------------------------------------
BitField Name: RXPD3
BitField Type: R/W
BitField Desc: Power Down subport 3
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_RXPD3_Mask                                                              cBit7_6
#define cAf6_SERDES_POWER_DOWN_RXPD3_Shift                                                                   6

/*--------------------------------------
BitField Name: RXPD2
BitField Type: R/W
BitField Desc: Power Down subport 2
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_RXPD2_Mask                                                              cBit5_4
#define cAf6_SERDES_POWER_DOWN_RXPD2_Shift                                                                   4

/*--------------------------------------
BitField Name: RXPD1
BitField Type: R/W
BitField Desc: Power Down subport 1
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_RXPD1_Mask                                                              cBit3_2
#define cAf6_SERDES_POWER_DOWN_RXPD1_Shift                                                                   2

/*--------------------------------------
BitField Name: RXPD0
BitField Type: R/W
BitField Desc: Power Down subport 0
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_RXPD0_Mask                                                              cBit1_0
#define cAf6_SERDES_POWER_DOWN_RXPD0_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : SERDES PLL Status
Reg Addr   : 0x000B-0x600B
Reg Formula: 0x000B+$G*0x2000
    Where  : 
           + $G(0-1) : Group of Ports
Reg Desc   : 
QPLL/CPLL status, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_PLL_Status_Base                                                                  0x000B

/*--------------------------------------
BitField Name: QPLL1_Lock_change
BitField Type: W1C
BitField Desc: QPLL1 has transition lock/unlock, Group 0-1
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_change_Mask                                                   cBit29
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_change_Shift                                                      29

/*--------------------------------------
BitField Name: QPLL0_Lock_change
BitField Type: W1C
BitField Desc: QPLL0 has transition lock/unlock, Group 0-1
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_change_Mask                                                   cBit28
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_change_Shift                                                      28

/*--------------------------------------
BitField Name: QPLL1_Lock
BitField Type: R_O
BitField Desc: QPLL0 is Locked, Group 0-1
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_Mask                                                          cBit25
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_Shift                                                             25

/*--------------------------------------
BitField Name: QPLL0_Lock
BitField Type: R_O
BitField Desc: QPLL0 is Locked, Group 0-1
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_Mask                                                          cBit24
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_Shift                                                             24

/*--------------------------------------
BitField Name: CPLL_Lock_Change
BitField Type: W1C
BitField Desc: CPLL has transition lock/unlock, bit per sub port,
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_CPLL_Lock_Change_Mask                                                 cBit15_12
#define cAf6_SERDES_PLL_Status_CPLL_Lock_Change_Shift                                                       12

/*--------------------------------------
BitField Name: CPLL_Lock
BitField Type: R_O
BitField Desc: CPLL is Locked, bit per sub port,
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_CPLL_Lock_Mask                                                          cBit3_0
#define cAf6_SERDES_PLL_Status_CPLL_Lock_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TX Reset
Reg Addr   : 0x000C-0x600C
Reg Formula: 0x000C+$G*0x2000
    Where  : 
           + $G(0-1) : Group of Ports
Reg Desc   : 
Reset TX SERDES, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TX_Reset_Base                                                                    0x000C

/*--------------------------------------
BitField Name: txrst_done
BitField Type: W1C
BitField Desc: TX Reset Done, bit per sub port
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_SERDES_TX_Reset_txrst_done_Mask                                                         cBit19_16
#define cAf6_SERDES_TX_Reset_txrst_done_Shift                                                               16

/*--------------------------------------
BitField Name: txrst_trig
BitField Type: R/W
BitField Desc: Should reset TX_PMA SERDES about 300-500 ns , bit per sub port
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_SERDES_TX_Reset_txrst_trig_Mask                                                           cBit3_0
#define cAf6_SERDES_TX_Reset_txrst_trig_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : SERDES RX Reset
Reg Addr   : 0x000D-0x600D
Reg Formula: 0x000D+$G*0x2000
    Where  : 
           + $G(0-1) : Group of Ports
Reg Desc   : 
Reset RX SERDES, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_RX_Reset_Base                                                                    0x000D

/*--------------------------------------
BitField Name: rxrst_done
BitField Type: W1C
BitField Desc: RX Reset Done, bit per sub port
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_SERDES_RX_Reset_rxrst_done_Mask                                                         cBit19_16
#define cAf6_SERDES_RX_Reset_rxrst_done_Shift                                                               16

/*--------------------------------------
BitField Name: rxrst_trig
BitField Type: R/W
BitField Desc: Should reset reset RX_PMA SERDES about 300-500 ns , bit per sub
port
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_SERDES_RX_Reset_rxrst_trig_Mask                                                           cBit3_0
#define cAf6_SERDES_RX_Reset_rxrst_trig_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : SERDES LPMDFE Mode
Reg Addr   : 0x000E-0x600E
Reg Formula: 0x000E+$G*0x2000
    Where  : 
           + $G(0-1) : Group of Ports
Reg Desc   : 
Configure LPM/DFE mode , there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_LPMDFE_Mode_Base                                                                 0x000E

/*--------------------------------------
BitField Name: lpmdfe_mode
BitField Type: R/W
BitField Desc: bit per sub port
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_Mask                                                       cBit3_0
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : SERDES LPMDFE Reset
Reg Addr   : 0x000F-0x600F
Reg Formula: 0x000F+$G*0x2000
    Where  : 
           + $G(0-1) : Group of Ports
Reg Desc   : 
Reset LPM/DFE , there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_LPMDFE_Reset_Base                                                                0x000F

/*--------------------------------------
BitField Name: lpmdfe_reset
BitField Type: R/W
BitField Desc: bit per sub port, Must be toggled after switching between modes
to initialize adaptation
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_Mask                                                     cBit3_0
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TXDIFFCTRL
Reg Addr   : 0x0010-0x6010
Reg Formula: 0x0010+$G*0x2000
    Where  : 
           + $G(0-1) : Group of Ports
Reg Desc   : 
Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TXDIFFCTRL_Base                                                                  0x0010

/*--------------------------------------
BitField Name: TXDIFFCTRL_subport3
BitField Type: R/W
BitField Desc:
BitField Bits: [19:15]
--------------------------------------*/
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport3_Mask                                              cBit19_15
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport3_Shift                                                    15

/*--------------------------------------
BitField Name: TXDIFFCTRL_subport2
BitField Type: R/W
BitField Desc:
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport2_Mask                                              cBit14_10
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport2_Shift                                                    10

/*--------------------------------------
BitField Name: TXDIFFCTRL_subport1
BitField Type: R/W
BitField Desc:
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport1_Mask                                                cBit9_5
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport1_Shift                                                     5

/*--------------------------------------
BitField Name: TXDIFFCTRL_subport0
BitField Type: R/W
BitField Desc:
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_Mask                                                cBit4_0
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TXPOSTCURSOR
Reg Addr   : 0x0011-0x6011
Reg Formula: 0x0011+$G*0x2000
    Where  : 
           + $G(0-1) : Group of Ports
Reg Desc   : 
Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TXPOSTCURSOR_Base                                                                0x0011

/*--------------------------------------
BitField Name: TXPOSTCURSOR_subport3
BitField Type: R/W
BitField Desc:
BitField Bits: [19:15]
--------------------------------------*/
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport3_Mask                                          cBit19_15
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport3_Shift                                                15

/*--------------------------------------
BitField Name: TXPOSTCURSOR_subport2
BitField Type: R/W
BitField Desc:
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport2_Mask                                          cBit14_10
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport2_Shift                                                10

/*--------------------------------------
BitField Name: TXPOSTCURSOR_subport1
BitField Type: R/W
BitField Desc:
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_Mask                                            cBit9_5
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_Shift                                                 5

/*--------------------------------------
BitField Name: TXPOSTCURSOR_subport0
BitField Type: R/W
BitField Desc:
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Mask                                            cBit4_0
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TXPRECURSOR
Reg Addr   : 0x0012-0x6012
Reg Formula: 0x0012+$G*0x2000
    Where  : 
           + $G(0-1) : Group of Ports
Reg Desc   : 
Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TXPRECURSOR_Base                                                                 0x0012

/*--------------------------------------
BitField Name: TXPRECURSOR_subport3
BitField Type: R/W
BitField Desc:
BitField Bits: [19:15]
--------------------------------------*/
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport3_Mask                                            cBit19_15
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport3_Shift                                                  15

/*--------------------------------------
BitField Name: TXPRECURSOR_subport2
BitField Type: R/W
BitField Desc:
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport2_Mask                                            cBit14_10
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport2_Shift                                                  10

/*--------------------------------------
BitField Name: TXPRECURSOR_subport1
BitField Type: R/W
BitField Desc:
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport1_Mask                                              cBit9_5
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport1_Shift                                                   5

/*--------------------------------------
BitField Name: TXPRECURSOR_subport0
BitField Type: R/W
BitField Desc:
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_Mask                                              cBit4_0
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : SERDES PRBS generator test pattern control
Reg Addr   : 0x0020-0x6020
Reg Formula: 0x0020+$G*0x2000
    Where  :
           + $G(0-1) : Group of Ports
Reg Desc   :
Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_prbsctr_pen_Base                                                                        0x0020

/*--------------------------------------
BitField Name: PRBSSEL_subport3
BitField Type: R/W
BitField Desc: Transmitter PRBS generator and Receiver PRBS checker test pattern
control.After changing patterns, perform a reset of the RX (GTRXRESET,
RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the
RX pattern checker can attempt to reestablish the link acquired. No checking is
done for non-PRBS patterns.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_prbsctr_pen_PRBSSEL_subport3_Mask                                                       cBit15_12
#define cAf6_prbsctr_pen_PRBSSEL_subport3_Shift                                                             12

/*--------------------------------------
BitField Name: PRBSSEL_subport2
BitField Type: R/W
BitField Desc: Transmitter PRBS generator and Receiver PRBS checker test pattern
control.After changing patterns, perform a reset of the RX (GTRXRESET,
RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the
RX pattern checker can attempt to reestablish the link acquired. No checking is
done for non-PRBS patterns.
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_prbsctr_pen_PRBSSEL_subport2_Mask                                                        cBit11_8
#define cAf6_prbsctr_pen_PRBSSEL_subport2_Shift                                                              8

/*--------------------------------------
BitField Name: PRBSSEL_subport1
BitField Type: R/W
BitField Desc: Transmitter PRBS generator and Receiver PRBS checker test pattern
control.After changing patterns, perform a reset of the RX (GTRXRESET,
RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the
RX pattern checker can attempt to reestablish the link acquired. No checking is
done for non-PRBS patterns.
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_prbsctr_pen_PRBSSEL_subport1_Mask                                                         cBit7_4
#define cAf6_prbsctr_pen_PRBSSEL_subport1_Shift                                                              4

/*--------------------------------------
BitField Name: PRBSSEL_subport0
BitField Type: R/W
BitField Desc: Transmitter PRBS generator and Receiver PRBS checker test pattern
control.After changing patterns, perform a reset of the RX (GTRXRESET,
RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the
RX pattern checker can attempt to reestablish the link acquired. No checking is
done for non-PRBS patterns.
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_prbsctr_pen_PRBSSEL_subport0_Mask                                                         cBit3_0
#define cAf6_prbsctr_pen_PRBSSEL_subport0_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : SERDES prbsctr_pen
Reg Addr   : 0x0021-0x6021
Reg Formula: 0x0021+$G*0x2000
    Where  :
           + $G(0-1) : Group of Ports
Reg Desc   :
Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is 2 group (0-1), each group has 2 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_prbsctr_pen_Base                                                                 0x0021

/*--------------------------------------
BitField Name: RXPRBSCNTRESET_subport3
BitField Type: R/W
BitField Desc: Resets the PRBS error counter.
BitField Bits: [07]
--------------------------------------*/
#define cAf6_SERDES_prbsctr_pen_RXPRBSCNTRESET_subport3_Mask                                             cBit7
#define cAf6_SERDES_prbsctr_pen_RXPRBSCNTRESET_subport3_Shift                                                7

/*--------------------------------------
BitField Name: RXPRBSCNTRESET_subport2
BitField Type: R/W
BitField Desc: Resets the PRBS error counter.
BitField Bits: [06]
--------------------------------------*/
#define cAf6_SERDES_prbsctr_pen_RXPRBSCNTRESET_subport2_Mask                                             cBit6
#define cAf6_SERDES_prbsctr_pen_RXPRBSCNTRESET_subport2_Shift                                                6

/*--------------------------------------
BitField Name: RXPRBSCNTRESET_subport1
BitField Type: R/W
BitField Desc: Resets the PRBS error counter.
BitField Bits: [05]
--------------------------------------*/
#define cAf6_SERDES_prbsctr_pen_RXPRBSCNTRESET_subport1_Mask                                             cBit5
#define cAf6_SERDES_prbsctr_pen_RXPRBSCNTRESET_subport1_Shift                                                5

/*--------------------------------------
BitField Name: RXPRBSCNTRESET_subport0
BitField Type: R/W
BitField Desc: Resets the PRBS error counter.
BitField Bits: [04]
--------------------------------------*/
#define cAf6_SERDES_prbsctr_pen_RXPRBSCNTRESET_subport0_Mask                                             cBit4
#define cAf6_SERDES_prbsctr_pen_RXPRBSCNTRESET_subport0_Shift                                                4

/*--------------------------------------
BitField Name: TXPRBSFORCEERR_subport3
BitField Type: R/W
BitField Desc: When this port is driven High, errors are forced in the PRBS
transmitter. While this port is asserted, the output data pattern contains
errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA
BitField Bits: [03]
--------------------------------------*/
#define cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport3_Mask                                             cBit3
#define cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport3_Shift                                                3

/*--------------------------------------
BitField Name: TXPRBSFORCEERR_subport2
BitField Type: R/W
BitField Desc: When this port is driven High, errors are forced in the PRBS
transmitter. While this port is asserted, the output data pattern contains
errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA
BitField Bits: [02]
--------------------------------------*/
#define cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport2_Mask                                             cBit2
#define cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport2_Shift                                                2

/*--------------------------------------
BitField Name: TXPRBSFORCEERR_subport1
BitField Type: R/W
BitField Desc: When this port is driven High, errors are forced in the PRBS
transmitter. While this port is asserted, the output data pattern contains
errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA
BitField Bits: [01]
--------------------------------------*/
#define cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport1_Mask                                             cBit1
#define cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport1_Shift                                                1

/*--------------------------------------
BitField Name: TXPRBSFORCEERR_subport0
BitField Type: R/W
BitField Desc: When this port is driven High, errors are forced in the PRBS
transmitter. While this port is asserted, the output data pattern contains
errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA
BitField Bits: [00]
--------------------------------------*/
#define cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport0_Mask                                             cBit0
#define cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport0_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : SERDES PRBS Diag Status
Reg Addr   : 0x0022-0x6022
Reg Formula: 0x0022+$G*0x2000
    Where  :
           + $G(0-1) : Group of Ports
Reg Desc   :
QPLL/CPLL status, there is 2 group (0-1), each group has 2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_PRBS_Status_Base                                                                 0x0022

/*--------------------------------------
BitField Name: RXPRBSLOCKED
BitField Type: R_O
BitField Desc: Output to indicate that the RX PRBS checker has been error free
after reset. Once asserted High, RXPRBSLOCKED does not deassert until reset of
the RX pattern checker via a reset of the RX (GTRXRESET, RXPMARESET, or
RXPCSRESET in sequential mode) or a reset of the PRBS error counter
(RXPRBSCNTRESET), bit per sub port,
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_SERDES_PRBS_Status_RXPRBSLOCKED_Mask                                                    cBit15_12
#define cAf6_SERDES_PRBS_Status_RXPRBSLOCKED_Shift                                                          12

/*--------------------------------------
BitField Name: RXPRBSERR
BitField Type: R_O
BitField Desc: This non-sticky status output indicates that PRBS errors have
occurred. , bit per sub port,
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_SERDES_PRBS_Status_RXPRBSERR_Mask                                                        cBit11_8
#define cAf6_SERDES_PRBS_Status_RXPRBSERR_Shift                                                              8

/*--------------------------------------
BitField Name: RXPRBSLOCKED_Change
BitField Type: W1C
BitField Desc: This sticky status output indicates that PRBS not locked, bit per
sub port,
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_SERDES_PRBS_Status_RXPRBSLOCKED_Change_Mask                                               cBit7_4
#define cAf6_SERDES_PRBS_Status_RXPRBSLOCKED_Change_Shift                                                    4

/*--------------------------------------
BitField Name: RXPRBSERR_Change
BitField Type: W1C
BitField Desc: This sticky status output indicates that PRBS errors have
occurred. , bit per sub port,
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_SERDES_PRBS_Status_RXPRBSERR_Change_Mask                                                  cBit3_0
#define cAf6_SERDES_PRBS_Status_RXPRBSERR_Change_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : SERDES PRBS RX_PRBS_ERR_CNT RO
Reg Addr   : 0x23 - 0x26
Reg Formula: 0x23+$G*0x2000
    Where  :
           + $G(0-1) : Group of Ports
Reg Desc   :
This is counter RX PRBS error RO for Port 1-4 ( 0x23 :Port 1 ....0x26 Port 4)

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_PRBS_RX_PRBS_ERR_CNT_RO_Base                                                       0x23

/*--------------------------------------
BitField Name: RX_PRBS_ERR_CNT_RO
BitField Type: RO
BitField Desc: PRBS error counter.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_SERDES_PRBS_RX_PRBS_ERR_CNT_RO_RX_PRBS_ERR_CNT_RO_Mask                                   cBit31_0
#define cAf6_SERDES_PRBS_RX_PRBS_ERR_CNT_RO_RX_PRBS_ERR_CNT_RO_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : SERDES PRBS RX_PRBS_ERR_CNT R2C
Reg Addr   : 0x33 - 0x36
Reg Formula: 0x33+$G*0x2000
    Where  :
           + $G(0-1) : Group of Ports
Reg Desc   :
This is counter RX PRBS error R2C for Port 1-4 ( 0x23 :Port 1 ....0x26 Port 4)

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_PRBS_RX_PRBS_ERR_CNT_R2C_Base                                                      0x33

/*--------------------------------------
BitField Name: RX_PRBS_ERR_CNT_R2C
BitField Type: R2C
BitField Desc: PRBS error counter.
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_SERDES_PRBS_RX_PRBS_ERR_CNT_R2C_RX_PRBS_ERR_CNT_R2C_Mask                                 cBit31_0
#define cAf6_SERDES_PRBS_RX_PRBS_ERR_CNT_R2C_RX_PRBS_ERR_CNT_R2C_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : Mate serdeses Gatetime for PRBS Raw Diagnostic
Reg Addr   : 0x37
Reg Formula: 0x37+$G*0x2000
    Where  :
           + $G(0-1) : Group of Ports
Reg Desc   :
This is Mate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 4

------------------------------------------------------------------------------*/
#define cAf6Reg_gatetime_ctr_Base                                                                         0x37

/*--------------------------------------
BitField Name: start_diag4
BitField Type: RW
BitField Desc: Config start Diagnostic trigger 0 to 1 for Start auto run with
Gatetime Configuration
BitField Bits: [0]
--------------------------------------*/
#define cAf6_gatetime_ctr_start_diag4_Mask                                                               cBit0
#define cAf6_gatetime_ctr_start_diag4_Shift                                                                  0

/*--------------------------------------
BitField Name: start_diag3
BitField Type: RW
BitField Desc: Config start Diagnostic trigger 0 to 1 for Start auto run with
Gatetime Configuration
BitField Bits: [0]
--------------------------------------*/
#define cAf6_gatetime_ctr_start_diag3_Mask                                                               cBit0
#define cAf6_gatetime_ctr_start_diag3_Shift                                                                  0

/*--------------------------------------
BitField Name: start_diag2
BitField Type: RW
BitField Desc: Config start Diagnostic trigger 0 to 1 for Start auto run with
Gatetime Configuration
BitField Bits: [0]
--------------------------------------*/
#define cAf6_gatetime_ctr_start_diag2_Mask                                                               cBit0
#define cAf6_gatetime_ctr_start_diag2_Shift                                                                  0

/*--------------------------------------
BitField Name: start_diag1
BitField Type: RW
BitField Desc: Config start Diagnostic trigger 0 to 1 for Start auto run with
Gatetime Configuration
BitField Bits: [0]
--------------------------------------*/
#define cAf6_gatetime_ctr_start_diag1_Mask                                                               cBit0
#define cAf6_gatetime_ctr_start_diag1_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : gatetime config value
Reg Addr   : 0x38 - 0x3b
Reg Formula: 0x38+$G*0x2000
    Where  :
           + $G(0-1) : Group of Ports
Reg Desc   :
This is Mate serdeses Gatetime for PRBS Raw Diagnostic port 1 to port 4

------------------------------------------------------------------------------*/
#define cAf6Reg_gatetime_cfg_Base                                                                         0x38

/*--------------------------------------
BitField Name: time_cfg
BitField Type: RW
BitField Desc: Gatetime Configuration 1-86400 second
BitField Bits: [16:00]
--------------------------------------*/
#define cAf6_gatetime_cfg_time_cfg_Mask                                                               cBit16_0
#define cAf6_gatetime_cfg_time_cfg_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Gatetime Current
Reg Addr   : 0x3c - 0x3f
Reg Formula: 0x3a+$G*0x2000
    Where  :
           + $G(0-1) : Group of Ports
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_Gatetime_current_Base                                                                     0x3c

/*--------------------------------------
BitField Name: status_gatetime_diag
BitField Type: RO
BitField Desc: Status Gatetime diagnostic port 16 to port1 bit per port
1:Running 0:Done-Ready
BitField Bits: [17]
--------------------------------------*/
#define cAf6_Gatetime_current_status_gatetime_diag_Mask                                                 cBit17
#define cAf6_Gatetime_current_status_gatetime_diag_Shift                                                    17

/*--------------------------------------
BitField Name: currert_gatetime_diag
BitField Type: RO
BitField Desc: Current running time of Gatetime diagnostic
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_Gatetime_current_currert_gatetime_diag_Mask                                              cBit16_0
#define cAf6_Gatetime_current_currert_gatetime_diag_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Eyescan Reset
Reg Addr   : 0x40
Reg Formula: 0x40+$G*0x2000
    Where  : 
           + $G(0-1) : Group of Ports
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_eyescan_reset_Base                                                                        0x40

/*--------------------------------------
BitField Name: eyescanrst_trigger
BitField Type: R/W
BitField Desc: change 0 to 1 to trigger an eyescan reset, bit per sub port,
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_eyescan_reset_eyescanrst_trigger_Mask                                                     cBit7_4
#define cAf6_eyescan_reset_eyescanrst_trigger_Shift                                                          4

/*--------------------------------------
BitField Name: eyescanrst
BitField Type: R/W
BitField Desc: set 1 to reset the eyescan reset, bit per sub port
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_eyescan_reset_eyescanrst_Mask                                                             cBit3_0
#define cAf6_eyescan_reset_eyescanrst_Shift                                                                  0

#endif /* _AF6_REG_AF6CNC0021_Xilinx_Serdes_Turning_Configuration_RD_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Phyical
 *
 * File        : Tha6029SerdesTuning.c
 *
 * Created Date: Sep 25, 2016
 *
 * Description : SERDES tuning common logic
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../prbs/Tha60290021ModulePrbs.h"
#include "Tha6029SerdesTunerInternal.h"
#include "Tha6029SerdesTunningReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cPowerDownValue 3

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) (Tha6029SerdesTuner)self

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6029SerdesTunerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;

/* Save super implementations */
static const tAtObjectMethods *m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 Read(Tha6029SerdesTuner self, uint32 address, eAtModule moduleId)
    {
    return AtSerdesControllerRead(self->serdes, address, moduleId);
    }

static void Write(Tha6029SerdesTuner self, uint32 address, uint32 value, eAtModule moduleId)
    {
    AtSerdesControllerWrite(self->serdes, address, value, moduleId);
    }

static AtDevice Device(Tha6029SerdesTuner self)
    {
    return AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self->serdes));
    }
 
static uint32 DefaultDrpBaseAddress(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return cInvalidUint32;
 	}

static AtDrp DrpObjectCreate(Tha6029SerdesTuner self, uint32 baseAddress, AtHal hal)
    {
    AtUnused(self);
    return AtDrpNew(baseAddress, hal);
    }

static AtDrp DrpCreateWithBaseAddress(Tha6029SerdesTuner self, uint32 baseAddress)
    {
    AtHal hal = AtDeviceIpCoreHalGet(Device(self), 0);
    AtDrp drp = mMethodsGet(self)->DrpObjectCreate(self, baseAddress, hal);
    AtDrpNeedSelectPortSet(drp, cAtFalse);
    return drp;
    }

static eBool CanLoopback(Tha6029SerdesTuner self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);

    switch (loopbackMode)
        {
        case cAtLoopbackModeRelease:
        case cAtLoopbackModeLocal:
        case cAtLoopbackModeRemote:
            return cAtTrue;

        case cAtLoopbackModeDual:
        default :
            return cAtFalse;
        }
    }

static uint32 RemoteLoopbackShift(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 LocalLoopbackShift(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 AbsoluteOffset(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 RegisterWithLocalAddress(Tha6029SerdesTuner self, uint32 localAddress)
    {
    return localAddress + mMethodsGet(self)->AbsoluteOffset(self);
    }

static uint32 LoopbackRegister(Tha6029SerdesTuner self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_SERDES_LoopBack_Base);
    }

static uint32 LocalId(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 LoopbackModeMask(Tha6029SerdesTuner self)
    {
    return cBit3_0 << (mMethodsGet(self)->LocalId(self) * 4);
    }

static uint32 LoopbackModeShift(Tha6029SerdesTuner self)
    {
    return (uint32) (mMethodsGet(self)->LocalId(self) * 4);
    }

static uint32 LocalLoopbackMask(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return cBit1;
    }

static uint32 RemoteLoopbackMask(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return cBit2;
    }

static eAtRet LoopbackSet(Tha6029SerdesTuner self, eAtLoopbackMode loopback)
    {
    uint32 regAddr = LoopbackRegister(self);
    uint32 regVal = Read(self, regAddr, cAtModuleSdh);
    uint32 loopbackModeMask = LoopbackModeMask(self);
    uint32 loopbackModeShift = LoopbackModeShift(self);
    uint32 loopbackVal = mRegField(regVal, loopbackMode);
    uint32 localLoopbackMask  = LocalLoopbackMask(self);
    uint32 localLoopbackShift  = LocalLoopbackShift(self);
    uint32 remoteLoopbackMask  = RemoteLoopbackMask(self);
    uint32 remoteLoopbackShift = RemoteLoopbackShift(self);

    mRegFieldSet(loopbackVal, localLoopback, 0);
    mRegFieldSet(loopbackVal, remoteLoopback, 0);

    switch ((uint32)loopback)
        {
        case cAtLoopbackModeLocal:
            mRegFieldSet(loopbackVal, localLoopback, 1);
            break;

        case cAtLoopbackModeRemote:
            mRegFieldSet(loopbackVal, remoteLoopback, 1);
            break;

        case cAtLoopbackModeRelease:
            break;

        case cAtLoopbackModeDual:
        default:
            return cAtErrorModeNotSupport;
        }

    mRegFieldSet(regVal, loopbackMode, loopbackVal);
    Write(self, regAddr, regVal, cAtModuleSdh);

    /* XILINX guideline that SERDES reset needs to be done on loopback operation */
    return Tha6029SerdesTunerReset(self);
    }

static eAtLoopbackMode LoopbackGet(Tha6029SerdesTuner self)
    {
    uint32 regAddr = LoopbackRegister(self);
    uint32 regVal = Read(self, regAddr, cAtModuleSdh);
    uint32 loopbackModeMask = LoopbackModeMask(self);
    uint32 loopbackModeShift = LoopbackModeShift(self);
    uint32 loopbackVal = mRegField(regVal, loopbackMode);

    if (loopbackVal & LocalLoopbackMask(self))
        return cAtLoopbackModeLocal;

    if (loopbackVal & RemoteLoopbackMask(self))
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

static uint32 EqualizerResetRegister(Tha6029SerdesTuner self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_SERDES_LPMDFE_Reset_Base);
    }

static uint32 EqualizerModeRegister(Tha6029SerdesTuner self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_SERDES_LPMDFE_Mode_Base);
    }

static eAtRet EqualizerModeSet(Tha6029SerdesTuner self, eAtSerdesEqualizerMode mode)
    {
    uint32 regAddr, regVal;
    uint32 portMask = cBit0 << mMethodsGet(self)->LocalId(self);
    uint32 portShift = mMethodsGet(self)->LocalId(self);

    /* Change the mode */
    regAddr = EqualizerModeRegister(self);
    regVal = Read(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, port, (mode == cAtSerdesEqualizerModeLpm) ? 1 : 0);
    Write(self, regAddr, regVal, cAtModuleSdh);

    /* Need to toggle reset */
    regAddr = EqualizerResetRegister(self);
    regVal = Read(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, port, 0);
    Write(self, regAddr, regVal, cAtModuleSdh);
    mRegFieldSet(regVal, port, 1);
    Write(self, regAddr, regVal, cAtModuleSdh);
    mRegFieldSet(regVal, port, 0);
    Write(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtSerdesEqualizerMode EqualizerModeGet(Tha6029SerdesTuner self)
    {
    uint32 regAddr = EqualizerModeRegister(self);
    uint32 regVal  = Read(self, regAddr, cAtModuleSdh);
    uint32 portMask = cBit0 << mMethodsGet(self)->LocalId(self);
    return (regVal & portMask) ? cAtSerdesEqualizerModeLpm : cAtSerdesEqualizerModeDfe;
    }

static uint32 ParamMask(Tha6029SerdesTuner self)
    {
    return cBit4_0 << (mMethodsGet(self)->LocalId(self) * 5);
    }

static uint32 ParamShift(Tha6029SerdesTuner self)
    {
    return (uint32) (mMethodsGet(self)->LocalId(self) * 5);
    }

static eAtRet PhysicalParamSetWithRegister(Tha6029SerdesTuner self, uint32 regAddress, uint32 value)
    {
    uint32 regVal     = Read(self, regAddress, cAtModuleSdh);
    uint32 paramMask  = ParamMask(self);
    uint32 paramShift = ParamShift(self);

    mRegFieldSet(regVal, param, value);
    Write(self, regAddress, regVal, cAtModuleSdh);

    return cAtOk;
    }

static uint32 PhysicalParamGetWithRegister(Tha6029SerdesTuner self, uint32 regAddress)
    {
    uint32 regVal     = Read(self, regAddress, cAtModuleSdh);
    uint32 paramMask  = ParamMask(self);
    uint32 paramShift = ParamShift(self);
    return mRegField(regVal, param);
    }

static uint32 TxDiffCtrlRegister(Tha6029SerdesTuner self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_SERDES_TXDIFFCTRL_Base);
    }

static eAtRet TxDiffCtrlSet(Tha6029SerdesTuner self, uint32 value)
    {
    return PhysicalParamSetWithRegister(self, TxDiffCtrlRegister(self), value);
    }

static uint32 TxDiffCtrlGet (Tha6029SerdesTuner self)
    {
    return PhysicalParamGetWithRegister(self, TxDiffCtrlRegister(self));
    }

static uint32 TxPreCursorRegister(Tha6029SerdesTuner self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_SERDES_TXPRECURSOR_Base);
    }

static eAtRet TxPreCursorSet(Tha6029SerdesTuner self, uint32 value)
    {
    return PhysicalParamSetWithRegister(self, TxPreCursorRegister(self), value);
    }

static uint32 TxPreCursorGet(Tha6029SerdesTuner self)
    {
    return PhysicalParamGetWithRegister(self, TxPreCursorRegister(self));
    }

static uint32 TxPostCursorRegister(Tha6029SerdesTuner self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_SERDES_TXPOSTCURSOR_Base);
    }

static eAtRet TxPostCursorSet(Tha6029SerdesTuner self, uint32 value)
    {
    return PhysicalParamSetWithRegister(self, TxPostCursorRegister(self), value);
    }

static uint32 TxPostCursorGet(Tha6029SerdesTuner self)
    {
    return PhysicalParamGetWithRegister(self, TxPostCursorRegister(self));
    }

static eAtRet PhysicalParamSet(Tha6029SerdesTuner self, eAtSerdesParam param, uint32 value)
    {
    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor:
            return TxPreCursorSet(self, value);
        case cAtSerdesParamTxPostCursor:
            return TxPostCursorSet(self, value);
        case cAtSerdesParamTxDiffCtrl:
            return TxDiffCtrlSet(self, value);

        default :
            return cAtErrorModeNotSupport;
        }

    return cAtOk;
    }

static uint32 PhysicalParamGet(Tha6029SerdesTuner self, eAtSerdesParam param)
    {
    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor:
            return TxPreCursorGet(self);
        case cAtSerdesParamTxPostCursor:
            return TxPostCursorGet(self);
        case cAtSerdesParamTxDiffCtrl:
            return TxDiffCtrlGet(self);
        default :
            return 0;
        }
    return 0;
    }

static eBool PhysicalParamIsSupported(Tha6029SerdesTuner self, eAtSerdesParam param)
    {
    AtUnused(self);

    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor:
        case cAtSerdesParamTxPostCursor:
        case cAtSerdesParamTxDiffCtrl:
            return cAtTrue;

        case cAtSerdesParamVod:
        case cAtSerdesParamPreEmphasisPreTap:
        case cAtSerdesParamPreEmphasisFirstPostTap:
        case cAtSerdesParamPreEmphasisSecondPostTap:
        case cAtSerdesParamRxEqualizationDcGain:
        case cAtSerdesParamRxEqualizationControl:
        default :
            return cAtFalse;
        }
    }

static uint32 PllStatusRegister(Tha6029SerdesTuner self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_SERDES_PLL_Status_Base);
    }

static eBool UseQpll(Tha6029SerdesTuner self)
    {
    /* Sub class must know */
    AtUnused(self);
    return cAtFalse;
    }

static uint32 QpllLockStatusMask(Tha6029SerdesTuner self)
    {
    /* There are two QPLLs, at a time, only one of them is used and this is
     * depend on the way IP is used. For this project, only QPLL0 is used */
    AtUnused(self);
    return cAf6_SERDES_PLL_Status_QPLL0_Lock_Mask;
    }

static uint32 QpllLockStickyMask(Tha6029SerdesTuner self)
    {
    /* There are two QPLLs, at a time, only one of them is used and this is
     * depend on the way IP is used. For this project, only QPLL0 is used */
    AtUnused(self);
    return cAf6_SERDES_PLL_Status_QPLL0_Lock_change_Mask;
    }

static uint32 CpllStatusMask(Tha6029SerdesTuner self)
    {
    return cBit0 << mMethodsGet(self)->LocalId(self);
    }

static uint32 PllStatusMask(Tha6029SerdesTuner self)
    {
    if (mMethodsGet(self)->UseQpll(self))
        return QpllLockStatusMask(self);
    return CpllStatusMask(self);
    }

static eBool PllIsLocked(Tha6029SerdesTuner self)
    {
    uint32 regAddr = PllStatusRegister(self);
    uint32 regVal = Read(self, regAddr, cAtModuleSdh);
    uint32 pllMask = PllStatusMask(self);
    return (regVal & pllMask) ? cAtTrue : cAtFalse;
    }

static uint32 CpllStickyMask(Tha6029SerdesTuner self)
    {
    return cBit12 << mMethodsGet(self)->LocalId(self);
    }

static uint32 PllStickyMask(Tha6029SerdesTuner self)
    {
    if (mMethodsGet(self)->UseQpll(self))
        return QpllLockStickyMask(self);
    return CpllStickyMask(self);
    }

static eBool PllChanged(Tha6029SerdesTuner self)
    {
    uint32 regAddr = PllStatusRegister(self);
    uint32 regVal  = Read(self, regAddr, cAtModuleSdh);
    uint32 pllLockChange  = regVal & PllStickyMask(self);
    Write(self, regAddr, regVal, cAtModuleSdh);
    return pllLockChange ? cAtTrue : cAtFalse;
    }

static uint32 PowerDownRegister(Tha6029SerdesTuner self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_SERDES_POWER_DOWN_Base);
    }

static uint32 RxPowerDownMask(Tha6029SerdesTuner self)
    {
    return cAf6_SERDES_POWER_DOWN_RXPD0_Mask << (mMethodsGet(self)->LocalId(self) * 2);
    }

static uint32 RxPowerDownShift(Tha6029SerdesTuner self)
    {
    return (uint32) (mMethodsGet(self)->LocalId(self) * 2);
    }

static uint32 TxPowerDownMask(Tha6029SerdesTuner self)
    {
    return cAf6_SERDES_POWER_DOWN_TXPD0_Mask << (mMethodsGet(self)->LocalId(self) * 2);
    }

static uint32 TxPowerDownShift(Tha6029SerdesTuner self)
    {
    return (uint32)((mMethodsGet(self)->LocalId(self) * 2) + 8);
    }

static eAtRet PowerDown(Tha6029SerdesTuner self, eBool powerDown)
    {
    uint32 regAddr = PowerDownRegister(self);
    uint32 regVal = Read(self, regAddr, cAtModuleSdh);
    uint32 powerDownMask, powerDownShift;
    uint32 powerDownValue = powerDown ? cPowerDownValue : 0;
    eAtRet ret = cAtOk;

    powerDownMask  = RxPowerDownMask(self);
    powerDownShift = RxPowerDownShift(self);
    mRegFieldSet(regVal, powerDown, powerDownValue);

    powerDownMask  = mMethodsGet(self)->TxPowerDownMask(self);
    powerDownShift = mMethodsGet(self)->TxPowerDownShift(self);
    mRegFieldSet(regVal, powerDown, powerDownValue);

    Write(self, regAddr, regVal, cAtModuleSdh);

    /* XILINX recommended power up should include reset */
    if (!powerDown)
        ret = AtSerdesControllerReset(Tha6029SerdesTunerSerdesGet(self));

    return ret;
    }

static eBool PowerIsDown(Tha6029SerdesTuner self)
    {
    uint32 regAddr = PowerDownRegister(self);
    uint32 regVal = Read(self, regAddr, cAtModuleSdh);
    uint32 powerDownMask = RxPowerDownMask(self);
    uint32 powerDownShift = RxPowerDownShift(self);
    return (mRegField(regVal, powerDown) == cPowerDownValue) ? cAtTrue : cAtFalse;
    }

static uint32 ResetMask(Tha6029SerdesTuner self)
    {
    return cBit0 << mMethodsGet(self)->LocalId(self);
    }

static uint32 ResetShift(Tha6029SerdesTuner self)
    {
    return mMethodsGet(self)->LocalId(self);
    }

static uint32 ResetDoneMask(Tha6029SerdesTuner self)
    {
    return cBit16 << mMethodsGet(self)->LocalId(self);
    }

static uint32 ResetDoneShift(Tha6029SerdesTuner self)
    {
    return mMethodsGet(self)->LocalId(self) + 16;
    }

static uint32 TimeoutMs(Tha6029SerdesTuner self)
    {
    if (AtDeviceTestbenchIsEnabled(Device(self)))
        return AtDeviceTestbenchDefaultTimeoutMs();
    return 1000;
    }

static eBool IsSimulated(Tha6029SerdesTuner self)
    {
    return AtDeviceIsSimulated(AtSerdesControllerDeviceGet(self->serdes));
    }

static eAtRet ResetTrigger(Tha6029SerdesTuner self, uint32 localAddress)
    {
    uint32 regAddr = RegisterWithLocalAddress(self, localAddress);
    uint32 regVal;
    uint32 resetMask = ResetMask(self);
    uint32 resetShift = ResetShift(self);
    uint32 resetDoneMask = ResetDoneMask(self);
    uint32 resetDoneShift = ResetDoneShift(self);

    /* Toggle reset */
    regVal = Read(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, reset, 0);
    mRegFieldSet(regVal, resetDone, 1); /* Clear sticky */
    Write(self, regAddr, regVal, cAtModuleEth);
    mRegFieldSet(regVal, reset, 1);
    Write(self, regAddr, regVal, cAtModuleEth);

    /* Give hardware a moment then disable reset */
    if (!IsSimulated(self))
        AtOsalUSleep(100000);
    mRegFieldSet(regVal, reset, 0);
    Write(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool ResetDone(Tha6029SerdesTuner self, uint32 localAddress)
    {
    uint32 regAddr = RegisterWithLocalAddress(self, localAddress);
    uint32 regVal = Read(self, regAddr, cAtModuleEth);
    return (regVal & ResetDoneMask(self)) ? cAtTrue : cAtFalse;
    }

static eAtRet ResetWithLocalAddress(Tha6029SerdesTuner self, uint32 localAddress, const char *direction)
    {
    tAtOsalCurTime startTime, curTime;
    uint32 timeoutMs = TimeoutMs(self);
    uint32 elapseTimeMs = 0;
    eBool isSimulated = AtDeviceIsSimulated(Device(self));

    ResetTrigger(self, localAddress);

    /* Make sure that hardware finish reseting */
    AtOsalCurTimeGet(&startTime);
    while (elapseTimeMs < timeoutMs)
        {
        if (ResetDone(self, localAddress))
            return cAtOk;

        /* Retry */
        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);

        /* Never timeout on simulation */
        if (isSimulated)
            return cAtOk;
        }

    /* Timeout */
    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation,
                            "[%s] SERDES %s reset timeout\r\n",
                            direction,
                            AtObjectToString((AtObject)self->serdes));

    return cAtErrorSerdesResetTimeout;
    }

static eAtRet Reset(Tha6029SerdesTuner self)
    {
    eAtRet ret = cAtOk;

    if (AtDeviceWarmRestoreIsStarted(Device(self)))
        return cAtOk;

    ret |= ResetWithLocalAddress(self, cAf6Reg_SERDES_TX_Reset_Base, "TX");
    if (!IsSimulated(self))
        AtOsalUSleep(1000);
    ret |= ResetWithLocalAddress(self, cAf6Reg_SERDES_RX_Reset_Base, "RX");

    return ret;
    }

static eAtRet RxReset(Tha6029SerdesTuner self)
    {
    if (AtDeviceWarmRestoreIsStarted(Device(self)))
        return cAtOk;

    return ResetWithLocalAddress(self, cAf6Reg_SERDES_RX_Reset_Base, "RX");
    }

static eAtRet TxReset(Tha6029SerdesTuner self)
    {
    if (AtDeviceWarmRestoreIsStarted(Device(self)))
        return cAtOk;

    return ResetWithLocalAddress(self, cAf6Reg_SERDES_TX_Reset_Base, "TX");
    }

static uint32 RawPrbsModeControlRegister(Tha6029SerdesTuner self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_prbsctr_pen_Base);
    }

static uint32 RawPrbsControlRegister(Tha6029SerdesTuner self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_SERDES_prbsctr_pen_Base);
    }

static uint32 RawPrbsModeMask(Tha6029SerdesTuner self)
    {
    return cAf6_prbsctr_pen_PRBSSEL_subport0_Mask << (mMethodsGet(self)->LocalId(self) * 4);
    }

static uint32 RawPrbsModeShift(Tha6029SerdesTuner self)
    {
    return mMethodsGet(self)->LocalId(self) * 4;
    }

static uint32 RawPrbsModeSw2Hw(eAtPrbsMode mode)
    {
    switch ((uint32)mode)
        {
        case cAtPrbsModePrbs7 : return 1;
        case cAtPrbsModePrbs9 : return 2;
        case cAtPrbsModePrbs15: return 3;
        case cAtPrbsModePrbs23: return 4;
        case cAtPrbsModePrbs31: return 5;
        default:
            return cInvalidUint32;
        }
    }

static eAtPrbsMode RawPrbsModeHw2Sw(uint32 hwMode)
    {
    switch (hwMode)
        {
        case 1: return cAtPrbsModePrbs7;
        case 2: return cAtPrbsModePrbs9;
        case 3: return cAtPrbsModePrbs15;
        case 4: return cAtPrbsModePrbs23;
        case 5: return cAtPrbsModePrbs31;
        default:
            return cAtPrbsModeInvalid;
        }
    }

static eBool RawPrbsModeIsSupported(Tha6029SerdesTuner self, eAtPrbsMode mode)
    {
    AtUnused(self);
    return (RawPrbsModeSw2Hw(mode) == cInvalidUint32) ? cAtFalse : cAtTrue;
    }

static eAtRet RawPrbsHwModeSet(Tha6029SerdesTuner self, uint32 hwMode)
    {
    uint32 regAddr = RawPrbsModeControlRegister(self);
    uint32 regVal = Read(self, regAddr, cAtModuleEth);
    uint32 modeMask = RawPrbsModeMask(self);
    uint32 modeShift = RawPrbsModeShift(self);
    mRegFieldSet(regVal, mode, hwMode);
    Write(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 RawPrbsHwModeGet(Tha6029SerdesTuner self)
    {
    uint32 regAddr = RawPrbsModeControlRegister(self);
    uint32 regVal = Read(self, regAddr, cAtModuleEth);
    uint32 modeMask = RawPrbsModeMask(self);
    uint32 modeShift = RawPrbsModeShift(self);
    return mRegField(regVal, mode);
    }

static eAtRet RawPrbsModeSet(Tha6029SerdesTuner self, eAtPrbsMode mode)
    {
    eAtRet ret = RawPrbsHwModeSet(self, RawPrbsModeSw2Hw(mode));
    if (ret == cAtOk)
        self->mode = mode;
    return ret;
    }

static eAtPrbsMode RawPrbsModeGet(Tha6029SerdesTuner self)
    {
    if (self->enabled)
        return RawPrbsModeHw2Sw(RawPrbsHwModeGet(self));
    return self->mode;
    }

static eAtPrbsMode RawPrbsDefaultMode(void)
    {
    return cAtPrbsModePrbs31;
    }

static eAtRet RawPrbsEnable(Tha6029SerdesTuner self, eBool enabled)
    {
    self->enabled = enabled;

    /* When enable, need to apply valid mode */
    if (enabled)
        {
        if (self->mode == cAtPrbsModeInvalid)
            self->mode = RawPrbsDefaultMode();
        return RawPrbsHwModeSet(self, RawPrbsModeSw2Hw(self->mode));
        }

    /* Use mode 0 for disabling */
    return RawPrbsHwModeSet(self, 0);
    }

static eBool RawPrbsIsEnabled(Tha6029SerdesTuner self)
    {
    uint32 hwMode = RawPrbsHwModeGet(self);
    return (hwMode == 0) ? cAtFalse : cAtTrue;
    }

static uint32 RawPrbsErrorForceMask(Tha6029SerdesTuner self)
    {
    return cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport0_Mask << mMethodsGet(self)->LocalId(self);
    }

static uint32 RawPrbsErrorForceShift(Tha6029SerdesTuner self)
    {
    return mMethodsGet(self)->LocalId(self);
    }

static eAtRet RawPrbsErrorForce(Tha6029SerdesTuner self, eBool forced)
    {
    uint32 regAddr = RawPrbsControlRegister(self);
    uint32 regVal = Read(self, regAddr, cAtModuleEth);
    uint32 forceMask = RawPrbsErrorForceMask(self);
    uint32 forceShift = RawPrbsErrorForceShift(self);
    mRegFieldSet(regVal, force, forced ? 1 : 0);
    Write(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool RawPrbsErrorIsForced(Tha6029SerdesTuner self)
    {
    uint32 regAddr = RawPrbsControlRegister(self);
    uint32 regVal = Read(self, regAddr, cAtModuleEth);
    return (regVal & RawPrbsErrorForceMask(self)) ? cAtTrue : cAtFalse;
    }

static uint32 RawPrbsErrorCounterRead2Clear(Tha6029SerdesTuner self, eBool r2c)
    {
    uint32 localAddress = r2c ? cAf6Reg_SERDES_PRBS_RX_PRBS_ERR_CNT_R2C_Base : cAf6Reg_SERDES_PRBS_RX_PRBS_ERR_CNT_RO_Base;
    uint32 regAddr = RegisterWithLocalAddress(self, localAddress) + mMethodsGet(self)->LocalId(self);
    return Read(self, regAddr, cAtModuleEth);
    }

static uint32 RawPrbsStatusRegister(Tha6029SerdesTuner self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_SERDES_PRBS_Status_Base);
    }

static uint32 RawPrbsSyncChangeMask(Tha6029SerdesTuner self, uint32 localId)
    {
    AtUnused(self);
    return cBit4 << localId;
    }

static eAtPrbsEngineAlarmType RawPrbsAlarmHistoryRead2Clear(Tha6029SerdesTuner self, eBool r2c)
    {
    uint32 regAddr = RawPrbsStatusRegister(self);
    uint32 regVal = Read(self, regAddr, cAtModuleEth);
    uint32 errorChangeMask = cBit0 << mMethodsGet(self)->LocalId(self);
    uint32 syncChangeMask  = mMethodsGet(self)->RawPrbsSyncChangeMask(self, mMethodsGet(self)->LocalId(self));
    uint32 alarms = 0;

    if (r2c)
        Write(self, regAddr, errorChangeMask | syncChangeMask, cAtModuleEth);

    if (regVal & errorChangeMask)
        alarms |= cAtPrbsEngineAlarmTypeError;
    if (regVal & syncChangeMask)
        alarms |= cAtPrbsEngineAlarmTypeLossSync;

    return alarms;
    }

static uint32 RawPrbsSyncMask(Tha6029SerdesTuner self, uint32 localId)
    {
    AtUnused(self);
    return cBit12 << localId;
    }

static eAtPrbsEngineAlarmType RawPrbsAlarmGet(Tha6029SerdesTuner self)
    {
    uint32 regAddr = RawPrbsStatusRegister(self);
    uint32 regVal = Read(self, regAddr, cAtModuleEth);
    uint32 errorMask = cBit8 << mMethodsGet(self)->LocalId(self);
    uint32 syncMask = mMethodsGet(self)->RawPrbsSyncMask(self, mMethodsGet(self)->LocalId(self));
    uint32 alarms = 0;

    if (regVal & errorMask)
        alarms |= cAtPrbsEngineAlarmTypeError;
    if ((regVal & syncMask) == 0)
        alarms |= cAtPrbsEngineAlarmTypeLossSync;

    return alarms;
    }

static uint32 EyeScanResetRegister(Tha6029SerdesTuner self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_eyescan_reset_Base);
    }

static eAtRet EyeScanResetDone(Tha6029SerdesTuner self)
    {
    uint32 elapseTimeMs = 0;
    tAtOsalCurTime startTime, curTime;
    uint32 cTimeoutMs = 500;

    AtOsalCurTimeGet(&startTime);
    while (elapseTimeMs < cTimeoutMs)
        {
        if (ResetDone(self, cAf6Reg_SERDES_RX_Reset_Base))
            return cAtOk;

        if (IsSimulated(self))
            return cAtOk;

        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);
        }

    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation,
                            "[%s] Reset SERDES EyeScan timeout\r\n",
                            AtObjectToString((AtObject)self));

    return cAtErrorSerdesResetTimeout;
    }

static eAtRet EyeScanResetTrigger(Tha6029SerdesTuner self, uint32 laneId)
    {
    uint32 regAddr;
    uint32 regVal;
    uint32 resetMask = ResetMask(self);
    uint32 resetShift = ResetShift(self);
    uint32 resetDoneMask = ResetDoneMask(self);
    uint32 resetDoneShift = ResetDoneShift(self);
    AtUnused(laneId);

    regAddr = RegisterWithLocalAddress(self, cAf6Reg_SERDES_RX_Reset_Base);
    regVal = Read(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, resetDone, 1); /* Clear sticky */
    Write(self, regAddr, regVal, cAtModuleSdh);

    /* Toggle reset */
    regAddr = EyeScanResetRegister(self);
    regVal = Read(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, reset, 0);
    Write(self, regAddr, regVal, cAtModuleSdh);
    mRegFieldSet(regVal, reset, 1);
    Write(self, regAddr, regVal, cAtModuleSdh);

    /* Give hardware a moment then disable reset */
    if (!IsSimulated(self))
        AtOsalUSleep(100000);
    mRegFieldSet(regVal, reset, 0);
    Write(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet EyeScanReset(Tha6029SerdesTuner self, uint32 laneId)
    {
    eAtRet ret = cAtOk;
    ret |= EyeScanResetTrigger(self, laneId);
    ret |= EyeScanResetDone(self);
    return ret;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha6029SerdesTuner object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjectDescription(serdes);

    mEncodeUInt(mode);
    mEncodeUInt(enabled);
    mEncodeObjectDescription(prbsGatingTimer);
    }

static void OverrideAtObject(Tha6029SerdesTuner self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(Tha6029SerdesTuner self)
    {
    OverrideAtObject(self);
    }

static void MethodsInit(Tha6029SerdesTuner self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, RawPrbsSyncChangeMask);
        mMethodOverride(m_methods, RawPrbsSyncMask);
        mMethodOverride(m_methods, DefaultDrpBaseAddress);
        mMethodOverride(m_methods, DrpObjectCreate);
        mMethodOverride(m_methods, LocalId);
        mMethodOverride(m_methods, AbsoluteOffset);
        mMethodOverride(m_methods, TxPowerDownMask);
        mMethodOverride(m_methods, TxPowerDownShift);
        mMethodOverride(m_methods, UseQpll);
        mMethodOverride(m_methods, EyeScanReset);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SerdesTuner);
    }

Tha6029SerdesTuner Tha6029SerdesTunerObjectInit(Tha6029SerdesTuner self, AtSerdesController serdes)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtObjectInit((AtObject)self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    self->serdes = serdes;

    return self;
    }

Tha6029SerdesTuner Tha6029SerdesTunerNew(AtSerdesController serdes)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha6029SerdesTuner newTuner = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6029SerdesTunerObjectInit(newTuner, serdes);
    }

AtDrp Tha6029SerdesTunerDrpCreate(Tha6029SerdesTuner self)
    {
    if (self)
        return Tha6029SerdesTunerDrpCreateWithBaseAddress(self, mMethodsGet(self)->DefaultDrpBaseAddress(self));
    return NULL;
    }

AtDrp Tha6029SerdesTunerDrpCreateWithBaseAddress(Tha6029SerdesTuner self, uint32 drpBaseAddress)
    {
    if (self)
        return DrpCreateWithBaseAddress(self, drpBaseAddress);
    return NULL;
    }

eBool Tha6029SerdesTunerCanLoopback(Tha6029SerdesTuner self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    if (self)
        return CanLoopback(self, loopbackMode, enable);
    return cAtFalse;
    }

eAtRet Tha6029SerdesTunerLoopbackSet(Tha6029SerdesTuner self, eAtLoopbackMode loopback)
    {
    if (self)
        return LoopbackSet(self, loopback);
    return cAtErrorNullPointer;
    }

eAtLoopbackMode Tha6029SerdesTunerLoopbackGet(Tha6029SerdesTuner self)
    {
    if (self)
        return LoopbackGet(self);
    return cAtLoopbackModeRelease;
    }

eAtRet Tha6029SerdesTunerEqualizerModeSet(Tha6029SerdesTuner self, eAtSerdesEqualizerMode mode)
    {
    if (self)
        return EqualizerModeSet(self, mode);
    return cAtErrorNullPointer;
    }

eAtSerdesEqualizerMode Tha6029SerdesTunerEqualizerModeGet(Tha6029SerdesTuner self)
    {
    if (self)
        return EqualizerModeGet(self);
    return cAtSerdesEqualizerModeUnknown;
    }

eBool Tha6029SerdesTunerEqualizerModeIsSupported(Tha6029SerdesTuner self, eAtSerdesEqualizerMode mode)
    {
    AtUnused(self);

    switch (mode)
        {
        case cAtSerdesEqualizerModeDfe: return cAtTrue;
        case cAtSerdesEqualizerModeLpm: return cAtTrue;
        case cAtSerdesEqualizerModeUnknown:
        default:
            return cAtFalse;
        }
    }

eBool Tha6029SerdesTunerEqualizerCanControl(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return cAtTrue;
    }

eAtRet Tha6029SerdesTunerPhysicalParamSet(Tha6029SerdesTuner self, eAtSerdesParam param, uint32 value)
    {
    if (Tha6029SerdesTunerPhysicalParamValueIsInRange(self, param, value))
        return PhysicalParamSet(self, param, value);

    return cAtErrorOutOfRangParm;
    }

uint32 Tha6029SerdesTunerPhysicalParamGet(Tha6029SerdesTuner self, eAtSerdesParam param)
    {
    if (self)
        return PhysicalParamGet(self, param);
    return 0;
    }

eBool Tha6029SerdesTunerPhysicalParamIsSupported(Tha6029SerdesTuner self, eAtSerdesParam param)
    {
    if (self)
        return PhysicalParamIsSupported(self, param);
    return cAtFalse;
    }

eBool Tha6029SerdesTunerPhysicalParamValueIsInRange(Tha6029SerdesTuner self, eAtSerdesParam param, uint32 value)
    {
    AtUnused(self);

    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor:
        case cAtSerdesParamTxPostCursor:
        case cAtSerdesParamTxDiffCtrl:
            return (value <= 0x1f) ? cAtTrue : cAtFalse;

        case cAtSerdesParamVod:
        case cAtSerdesParamPreEmphasisPreTap:
        case cAtSerdesParamPreEmphasisFirstPostTap:
        case cAtSerdesParamPreEmphasisSecondPostTap:
        case cAtSerdesParamRxEqualizationDcGain:
        case cAtSerdesParamRxEqualizationControl:
        default :
            return cAtFalse;
        }
    }

eBool Tha6029SerdesTunerPllIsLocked(Tha6029SerdesTuner self)
    {
    if (self)
        return PllIsLocked(self);
    return cAtFalse;
    }

eBool Tha6029SerdesTunerPllChanged(Tha6029SerdesTuner self)
    {
    if (self)
        return PllChanged(self);
    return cAtFalse;
    }

eBool Tha6029SerdesTunerPowerCanControl(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return cAtTrue;
    }

eAtRet Tha6029SerdesTunerPowerDown(Tha6029SerdesTuner self, eBool powerDown)
    {
    if (self)
        return PowerDown(self, powerDown);
    return cAtErrorNullPointer;
    }

eBool Tha6029SerdesTunerPowerIsDown(Tha6029SerdesTuner self)
    {
    if (self)
        return PowerIsDown(self);
    return cAtFalse;
    }

eAtRet Tha6029SerdesTunerReset(Tha6029SerdesTuner self)
    {
    if (self)
        return Reset(self);
    return cAtErrorNullPointer;
    }

eAtRet Tha6029SerdesTunerRxReset(Tha6029SerdesTuner self)
    {
    if (self)
        return RxReset(self);
    return cAtErrorNullPointer;
    }

eAtRet Tha6029SerdesTunerTxReset(Tha6029SerdesTuner self)
    {
    if (self)
        return TxReset(self);
    return cAtErrorNullPointer;
    }

eAtRet Tha6029SerdesTunerRawPrbsModeSet(Tha6029SerdesTuner self, eAtPrbsMode mode)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (Tha6029SerdesTunerRawPrbsModeIsSupported(self, mode))
        return RawPrbsModeSet(self, mode);

    return cAtErrorModeNotSupport;
    }

eAtPrbsMode Tha6029SerdesTunerRawPrbsModeGet(Tha6029SerdesTuner self)
    {
    if (self)
        return RawPrbsModeGet(self);
    return cAtPrbsModeInvalid;
    }

eBool Tha6029SerdesTunerRawPrbsModeIsSupported(Tha6029SerdesTuner self, eAtPrbsMode mode)
    {
    if (self)
        return RawPrbsModeIsSupported(self, mode);
    return cAtFalse;
    }

eAtRet Tha6029SerdesTunerRawPrbsEnable(Tha6029SerdesTuner self, eBool enabled)
    {
    if (self)
        return RawPrbsEnable(self, enabled);
    return cAtErrorNullPointer;
    }

eBool Tha6029SerdesTunerRawPrbsIsEnabled(Tha6029SerdesTuner self)
    {
    if (self)
        return RawPrbsIsEnabled(self);
    return cAtFalse;
    }

eAtRet Tha6029SerdesTunerRawPrbsErrorForce(Tha6029SerdesTuner self, eBool forced)
    {
    if (self)
        return RawPrbsErrorForce(self, forced);
    return cAtErrorNullPointer;
    }

eBool Tha6029SerdesTunerRawPrbsErrorIsForced(Tha6029SerdesTuner self)
    {
    if (self)
        return RawPrbsErrorIsForced(self);
    return cAtFalse;
    }

uint32 Tha6029SerdesTunerRawPrbsErrorCounterRead2Clear(Tha6029SerdesTuner self, eBool r2c)
    {
    if (self)
        return RawPrbsErrorCounterRead2Clear(self, r2c);
    return 0;
    }

eAtPrbsEngineAlarmType Tha6029SerdesTunerRawPrbsAlarmHistoryRead2Clear(Tha6029SerdesTuner self, eBool r2c)
    {
    if (self)
        return RawPrbsAlarmHistoryRead2Clear(self, r2c);
    return cAtPrbsEngineAlarmTypeNone;
    }

eAtPrbsEngineAlarmType Tha6029SerdesTunerRawPrbsAlarmGet(Tha6029SerdesTuner self)
    {
    if (self)
        return RawPrbsAlarmGet(self);
    return cAtPrbsEngineAlarmTypeNone;
    }

AtSerdesController Tha6029SerdesTunerSerdesGet(Tha6029SerdesTuner self)
    {
    return self ? self->serdes : NULL;
    }

uint32 Tha6029SerdesTunerRegisterWithLocalAddress(Tha6029SerdesTuner self, uint32 localAddress)
    {
    if (self)
        return RegisterWithLocalAddress(self, localAddress);
    return cInvalidUint32;
    }

uint32 Tha6029SerdesTunerLocalId(Tha6029SerdesTuner self)
    {
    if (self)
        return mMethodsGet(self)->LocalId(self);
    return cInvalidUint32;
    }

uint32 Tha6029SerdesTunerRead(Tha6029SerdesTuner self, uint32 address, eAtModule moduleId)
    {
    if (self)
        return Read(self, address, moduleId);
    return cInvalidUint32;
    }

void Tha6029SerdesTunerWrite(Tha6029SerdesTuner self, uint32 address, uint32 value, eAtModule moduleId)
    {
    if (self)
        Write(self, address, value, moduleId);
    }

eBool Tha6029SerdesTunerIsSimulated(Tha6029SerdesTuner self)
    {
    if (self)
        return IsSimulated(self);
    return cAtFalse;
    }

eAtRet Tha6029SerdesTunerEyeScanReset(Tha6029SerdesTuner self, uint32 laneId)
    {
    if (self)
        return mMethodsGet(self)->EyeScanReset(self, laneId);
    return cAtErrorNullPointer;
    }

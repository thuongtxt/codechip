/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60290021PowerSupplySensorInternal.h
 * 
 * Created Date: Aug 15, 2019
 *
 * Description : Internal data and method for the power supply sensor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021POWERSUPPLYSENSORINTERNAL_H_
#define _THA60290021POWERSUPPLYSENSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "../../../default/physical/ThaPowerSupplySensorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021PowerSupplySensor
    {
    tThaPowerSupplySensor super;
    }tTha60290021PowerSupplySensor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPowerSupplySensor Tha60290021PowerSupplySensorObjectInit(AtPowerSupplySensor self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021POWERSUPPLYSENSORINTERNAL_H_ */


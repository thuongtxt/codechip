/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290021EyeScanController.c
 *
 * Created Date: Aug 3, 2017
 *
 * Description : Eyescan controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtEyeScanControllerInternal.h"
#include "Tha60290021SerdesManager.h"
#include "Tha6029Physical.h"

/*--------------------------- Define -----------------------------------------*/
#define cHorizontalOffsetQuarter 128
#define cHorizontalOffsetHex     512

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021EyeScanController
    {
    tAtEyeScanControllerGtyE4 super;
    }tTha60290021EyeScanController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtEyeScanControllerMethods m_AtEyeScanControllerOverride;

/* Save super implementation */
static const tAtEyeScanControllerMethods *m_AtEyeScanControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldAutoReset(AtEyeScanController self)
    {
    /* Let reseting be controlled from the outside - the serdes itself */
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldHaveDefaultEqualizer(AtEyeScanController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint16 Stm1MaxHorizontalOffsetGet(AtSerdesController serdes)
    {
    eAtSerdesMode oversamplingMode = AtSerdesControllerOverSamplingMode(serdes);

    if (oversamplingMode == cAtSerdesModeStm4)
        return cHorizontalOffsetHex;

    if (oversamplingMode == cAtSerdesModeStm16)
        return cHorizontalOffsetQuarter;

    AtAssert(0);
    return cHorizontalOffsetQuarter;
    }

static uint16 MultiRateMaxHorizontalOffsetGet(AtSerdesController serdes, AtEyeScanLane lane)
    {
    uint32 serdesMode = AtSerdesControllerModeGet(serdes);

    switch (serdesMode)
        {
        case cAtSerdesModeStm0: /* STM0 is over-sampling from OC-12 */
        case cAtSerdesModeStm4:
            return cHorizontalOffsetHex; /* Hex */

        case cAtSerdesModeStm1:
            return Stm1MaxHorizontalOffsetGet(serdes);

        case cAtSerdesModeEth100M:
            return Stm1MaxHorizontalOffsetGet(serdes);

        case cAtSerdesModeEth1G:
        case cAtSerdesModeStm16:
            return cHorizontalOffsetQuarter;

        case cAtSerdesModeStm64:
        case cAtSerdesModeEth10G:
            return 32; /* Full */

        default:
            AtEyeScanLaneLog(lane, cAtLogLevelCritical, AtSourceLocation, "Cannot determine RxOutDiv\r\n");
            return cInvalidUint16;
        }
    }

static uint16 MaxHorizontalOffsetGet(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtSerdesController serdes = AtEyeScanControllerSerdesControllerGet(self);
    AtSerdesManager manager = AtSerdesControllerManagerGet(serdes);
    uint32 serdesId = AtSerdesControllerIdGet(serdes);

    /* Normally, MAX horizontal offset shall be detected via RXOUTDIV, but for
     * Faceplate SERDES, RXOUTDIV is always zero as they are controlled via
     * signal pins. */

    if (Tha60290021SerdesManagerIsFaceplateSerdes(manager, serdesId))
        return MultiRateMaxHorizontalOffsetGet(serdes, lane);

    return m_AtEyeScanControllerMethods->MaxHorizontalOffsetGet(self, lane);
    }

static uint8 MultirateDefaultVsRange(AtEyeScanController self, AtEyeScanLane lane)
    {
    return m_AtEyeScanControllerMethods->DefaultVsRange(self, lane);
    }

static uint8 DefaultVsRange(AtEyeScanController self, AtEyeScanLane lane)
    {
    AtSerdesController serdes = AtEyeScanControllerSerdesControllerGet(self);
    AtSerdesManager manager = AtSerdesControllerManagerGet(serdes);
    uint32 serdesId = AtSerdesControllerIdGet(serdes);

    if (Tha60290021SerdesManagerIsFaceplateSerdes(manager, serdesId))
        return MultirateDefaultVsRange(self, lane);

    return m_AtEyeScanControllerMethods->DefaultVsRange(self, lane);
    }

static eAtRet Init(AtEyeScanController self)
    {
    return m_AtEyeScanControllerMethods->Init(self);
    }

static void OverrideAtEyeScanController(AtEyeScanController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEyeScanControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtEyeScanControllerOverride, m_AtEyeScanControllerMethods, sizeof(m_AtEyeScanControllerOverride));

        mMethodOverride(m_AtEyeScanControllerOverride, ShouldAutoReset);
        mMethodOverride(m_AtEyeScanControllerOverride, ShouldHaveDefaultEqualizer);
        mMethodOverride(m_AtEyeScanControllerOverride, MaxHorizontalOffsetGet);
        mMethodOverride(m_AtEyeScanControllerOverride, Init);
        mMethodOverride(m_AtEyeScanControllerOverride, DefaultVsRange);
        }

    mMethodsSet(self, &m_AtEyeScanControllerOverride);
    }

static void Override(AtEyeScanController  self)
    {
    OverrideAtEyeScanController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021EyeScanController);
    }

static AtEyeScanController ObjectInit(AtEyeScanController self, AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtEyeScanControllerGtyE4ObjectInit(self, serdesController, drpBaseAddress) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEyeScanController Tha60290021EyeScanControllerNew(AtSerdesController serdesController, uint32 drpBaseAddress)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEyeScanController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newController, serdesController, drpBaseAddress);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha6029FaceplateSerdesController.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Faceplate SERDES controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleEth.h"
#include "AtModuleSdh.h"
#include "AtModulePtp.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../../generic/physical/AtEyeScanLaneInternal.h"
#include "../../../default/ptp/ThaPtpPort.h"
#include "../../Tha60210011/sdh/Tha60210011ModuleSdh.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "../eth/Tha60290021FaceplateSerdesEthPort.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../prbs/Tha6029SerdesPrbsEngine.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../man/Tha60290021Device.h"
#include "Tha60290021SerdesManagerInternal.h"
#include "Tha6029SerdesControllerInternal.h"
#include "Tha6029FaceplateSerdesControllerReg.h"
#include "Tha6029Physical.h"
#include "Tha6029FaceplateSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cPcsLoopback 8
#define cPmaLoopback 4

#define RX_CM_TRIM_DRP_ADDRESS 0x61
#define RX_CM_TRIM_Mask        cBit5_2
#define RX_CM_TRIM_Shift       2
#define RX_CM_TRIM_Max         (RX_CM_TRIM_Mask >> RX_CM_TRIM_Shift)

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6029FaceplateSerdesController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6029FaceplateSerdesControllerMethods m_methods;

/* Override */
static tAtObjectMethods                 m_AtObjectOverride;
static tAtSerdesControllerMethods       m_AtSerdesControllerOverride;
static tTha6029SerdesControllerMethods  m_Tha6029SerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods           = NULL;
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static eAtRet TimingModeHwSet(AtSerdesController self, eAtSerdesTimingMode timingMode);

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtSerdesController self)
    {
    AtUnused(self);
    return cSerdesOcnGbeBaseAdress;
    }

static uint32 NumSubPortsInGroup(AtSerdesController self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 HwIdGet(AtSerdesController self)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    return (uint32) (Tha60290021SerdesManagerFaceplateSerdesLocalId(manager, AtSerdesControllerIdGet(self)));
    }

static uint32 GroupOf4SerdesIn16SerdesGroupId(AtSerdesController self)
    {
    return ((AtSerdesControllerHwIdGet(self) % 16 ) / NumSubPortsInGroup(self));
    }

static uint32 GroupOf16SerdesId(AtSerdesController self)
    {
    return (AtSerdesControllerHwIdGet(self) / 16);
    }

static uint32 LocalPortIdInGroup(AtSerdesController self)
    {
    return (AtSerdesControllerHwIdGet(self) % NumSubPortsInGroup(self));
    }

static uint32 GroupOffset(AtSerdesController self)
    {
    uint32 group4SerdesId = GroupOf4SerdesIn16SerdesGroupId(self);
    uint32 group16SerdesId = GroupOf16SerdesId(self);
    uint32 offset = 0;

    if (group16SerdesId == 0) /*the first 16 faceplate serdes*/
        offset = (uint32)(group4SerdesId * 0x2000);
    else if (group16SerdesId == 1)
        offset = (uint32)((group4SerdesId * 0x2000) + 0x10000); /* the second 16 faceplate serdes */
    else
        offset = cInvalidUint32;

    return offset;
    }

static uint32 AddressWithLocalAddress(AtSerdesController self, uint32 localAddress)
    {
    return AtSerdesControllerBaseAddress((AtSerdesController)self) + localAddress + GroupOffset(self);
    }

static uint32 TxResetRegister(AtSerdesController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_OC192_MUX_OC48_TX_Reset_Base);
    }

static uint32 RxResetRegister(AtSerdesController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_OC192_MUX_OC48_RX_Reset_Base);
    }

static eBool IsSimulated(AtSerdesController self)
    {
    AtDevice device = AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self));
    return AtDeviceIsSimulated(device);
    }

static eBool HwDone(AtSerdesController self, uint32 regAddr, uint32 doneBitMask, uint32 timeoutMs)
    {
    uint32 elapseTimeMs = 0;
    tAtOsalCurTime startTime, curTime;

    if (AtDeviceWarmRestoreIsStarted(AtSerdesControllerDeviceGet(self)))
        return cAtTrue;

    AtOsalCurTimeGet(&startTime);
    while (elapseTimeMs < timeoutMs)
        {
        uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
        if (regVal & doneBitMask)
            return cAtTrue;

        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);

        if (IsSimulated(self))
            return cAtTrue;
        }

    return cAtFalse;
    }

static uint32 TimeoutMs(AtSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet(self);

    if (AtDeviceTestbenchIsEnabled(device))
        return AtDeviceTestbenchDefaultTimeoutMs();

    return 500;
    }

static uint32 ResetDoneMask(AtSerdesController self)
    {
    return cBit16 << LocalPortIdInGroup(self);
    }

static uint32 ResetDoneShift(AtSerdesController self)
    {
    return LocalPortIdInGroup(self) + 16;
    }

static eAtRet ResetTrigger(AtSerdesController self, uint32 regAddr)
    {
    uint32 localId = LocalPortIdInGroup(self);
    uint32 regVal  = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 triggerMask  = cBit0 << localId;
    uint32 triggerShift = localId;
    uint32 doneMask     = ResetDoneMask(self);
    uint32 doneShift    = ResetDoneShift(self);

    mRegFieldSet(regVal, done, 1);
    mRegFieldSet(regVal, trigger, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    mRegFieldSet(regVal, trigger, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool HwResetDone(AtSerdesController self, uint32 regAddr)
    {
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    return (regVal & ResetDoneMask(self)) ? cAtTrue : cAtFalse;
    }

static eAtRet ResetWithAddress(AtSerdesController self, uint32 regAddr, const char *direction)
    {
    uint32 elapseTimeMs = 0;
    tAtOsalCurTime startTime, curTime;
    uint32 cTimeoutMs = TimeoutMs(self);

    ResetTrigger(self, regAddr);

    AtOsalCurTimeGet(&startTime);
    while (elapseTimeMs < cTimeoutMs)
        {
        if (HwResetDone(self, regAddr))
            return cAtOk;

        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);

        if (IsSimulated(self))
            return cAtOk;
        }

    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation,
                            "[%s] Reset SERDES %s timeout\r\n",
                            direction, AtObjectToString((AtObject)self));

    return cAtErrorSerdesResetTimeout;
    }

static eAtRet Reset(AtSerdesController self)
    {
    eAtRet ret = cAtOk;

    if (AtDeviceWarmRestoreIsStarted(AtSerdesControllerDeviceGet(self)))
        return cAtOk;

    ret |= ResetWithAddress(self, TxResetRegister(self), "TX");
    if (!IsSimulated(self))
        AtOsalUSleep(1000);
    ret |= ResetWithAddress(self, RxResetRegister(self), "RX");

    return ret;
    }

static eAtRet RxReset(AtSerdesController self)
    {
    if (AtDeviceWarmRestoreIsStarted(AtSerdesControllerDeviceGet(self)))
        return cAtOk;

    return ResetWithAddress(self, RxResetRegister(self), "RX");
    }

static eAtRet TxReset(AtSerdesController self)
    {
    if (AtDeviceWarmRestoreIsStarted(AtSerdesControllerDeviceGet(self)))
        return cAtOk;

    return ResetWithAddress(self, TxResetRegister(self), "TX");
    }

static ThaVersionReader VersionReader(AtSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet(self);
    return ThaDeviceVersionReader(device);
    }

static eBool Stm1SamplingFromStm4(Tha6029FaceplateSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionHasNewParams(Tha6029FaceplateSerdesController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x2, 0x1845);
    }

static eAtSerdesMode OverSamplingModeForSerdesMode(AtSerdesController self, eAtSerdesMode mode)
    {
    if (mode == cAtSerdesModeStm0)
        return cAtSerdesModeStm4;

    if (mode == cAtSerdesModeStm1)
        return mMethodsGet(mThis(self))->Stm1SamplingFromStm4(mThis(self)) ? cAtSerdesModeStm4 : cAtSerdesModeStm16;

    if (mode == cAtSerdesModeEth100M)
        return cAtSerdesModeStm4;

    return mode;
    }

static uint8 Stm1ModeSw2Hw(AtSerdesController self)
    {
    eAtSerdesMode samplingMode = OverSamplingModeForSerdesMode(self, cAtSerdesModeStm1);

    if (samplingMode == cAtSerdesModeStm4)
        return cTha6029FaceplateSerdesHwModeOc12;

    return cTha6029FaceplateSerdesHwModeOc48;
    }

static eAtSerdesMode OverSamplingMode(AtSerdesController self)
    {
    eAtSerdesMode mode = AtSerdesControllerModeGet(self);
    return OverSamplingModeForSerdesMode(self, mode);
    }

static uint8 ModeSw2Hw(AtSerdesController self, eAtSerdesMode mode)
    {
    switch (mode)
        {
        case cAtSerdesModeStm64   : return cTha6029FaceplateSerdesHwModeOc192;
        case cAtSerdesModeStm16   : return cTha6029FaceplateSerdesHwModeOc48;
        case cAtSerdesModeStm4    : return cTha6029FaceplateSerdesHwModeOc12;

        /* Regard STM-4/1/0, hardware is working on over sampling and require
         * following modes:
         * - STM-1: need to specify STM16 mode
         * - STM-0: need to specify STM4 mode
         */
        case cAtSerdesModeStm1    : return Stm1ModeSw2Hw(self);
        case cAtSerdesModeStm0    : return cTha6029FaceplateSerdesHwModeOc12;

        case cAtSerdesModeEth10G  : return cTha6029FaceplateSerdesHwMode10G;
        case cAtSerdesModeEth1G   : return cTha6029FaceplateSerdesHwMode1G;
        case cAtSerdesModeEth100M : return cTha6029FaceplateSerdesHwMode100Fx;

        case cAtSerdesModeEth2500M:
        case cAtSerdesModeEth40G:
        case cAtSerdesModeOcn:
        case cAtSerdesModeGe:
        case cAtSerdesModeUnknown:
        case cAtSerdesModeEth10M  :
        default :
            return cInvalidUint8;
        }
    }

static eAtSerdesMode ModeHw2Sw(uint32 mode)
    {
    switch (mode)
        {
        case 0: return cAtSerdesModeStm64;
        case 1: return cAtSerdesModeStm16;
        case 2: return cAtSerdesModeStm4;
        case 4: return cAtSerdesModeEth10G;
        case 5: return cAtSerdesModeEth1G;
        case 6: return cAtSerdesModeEth100M;

        default :
            return cInvalidUint8;
        }
    }

static eBool IsLowRateMode(eAtSerdesMode mode)
    {
    switch ((uint32)mode)
        {
        case cAtSerdesModeStm0   : return cAtTrue;
        case cAtSerdesModeStm1   : return cAtTrue;
        case cAtSerdesModeStm4   : return cAtTrue;
        case cAtSerdesModeStm16  : return cAtTrue;
        case cAtSerdesModeEth10M : return cAtTrue;
        case cAtSerdesModeEth100M: return cAtTrue;

        default:
            return cAtFalse;
        }
    }

static uint32 ChangeModeRegister(AtSerdesController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_OC192_MUX_OC48_Change_Mode_Base);
    }

static eBool ModeApplied(AtSerdesController self)
    {
    return HwDone(self, ChangeModeRegister(self), cAf6_OC192_MUX_OC48_Change_Mode_chg_done_Mask, TimeoutMs(self));
    }

static uint32 CurrentModeMask(AtSerdesController self)
    {
    return cAf6_OC192_MUX_OC48_Current_Mode_cur_mode_subport0_Mask << (LocalPortIdInGroup(self) * 4);
    }

static uint32 CurrentModeShift(AtSerdesController self)
    {
    return LocalPortIdInGroup(self) * 4;
    }

static uint32 CurrentModeRegister(AtSerdesController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_OC192_MUX_OC48_Current_Mode_Base);
    }

static eAtRet SimulateCurrentModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    uint32 regAddr = CurrentModeRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 modeMask = CurrentModeMask(self);
    uint32 modeShift = CurrentModeShift(self);

    mRegFieldSet(regVal, mode, ModeSw2Hw(self, mode));
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
	{
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
	return Tha60290021SerdesManagerFacePlateLineSerdesModeIsSupported(manager, self, mode);
	}

static eAtRet PrbsStop(AtSerdesController self)
    {
    AtPrbsEngine prbsEngine = AtSerdesControllerPrbsEngine(self);

    if (prbsEngine)
        return AtPrbsEngineEnable(prbsEngine, cAtFalse);

    return cAtOk;
    }

static eBool UseDifferentRemoteLoopbackFor10G(Tha6029FaceplateSerdesController self)
    {
    ThaVersionReader versionReader = VersionReader((AtSerdesController)self);
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x5, 0x00);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool SerdesHasNewParams(AtSerdesController self)
    {
    return mMethodsGet(mThis(self))->HasNewParams(mThis(self));
    }

static eBool HasNewParams(Tha6029FaceplateSerdesController self)
    {
    ThaVersionReader versionReader = VersionReader((AtSerdesController)self);
    uint32 startVersionHasThis = StartVersionHasNewParams(self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eAtSerdesTimingMode DefaultTimingMode(AtSerdesController self)
    {
    uint32 serdesMode;

    if (!AtSerdesControllerCanControlTimingMode(self))
        return cAtSerdesTimingModeAuto;

    serdesMode = AtSerdesControllerModeGet(self);
    switch (serdesMode)
        {
        case cAtSerdesModeStm1:
            return SerdesHasNewParams(self) ? cAtSerdesTimingModeLockToRef : cAtSerdesTimingModeLockToData;
        case cAtSerdesModeStm0:
            return cAtSerdesTimingModeLockToRef;
        case cAtSerdesModeEth100M:
            return cAtSerdesTimingModeLockToRef;
        default:
            return cAtSerdesTimingModeLockToData;
        }
    }

static eAtRet TimingDefaultSet(AtSerdesController self)
    {
    eAtSerdesTimingMode timingMode;

    timingMode = DefaultTimingMode(self);
    if (timingMode == cAtSerdesTimingModeAuto)
        return cAtOk;

    return AtSerdesControllerTimingModeSet(self, timingMode);
    }

static eBool EyeScanIsEnabled(AtSerdesController self)
    {
    AtEyeScanController eyescanController = AtSerdesControllerEyeScanControllerGet(self);
    uint8 lane_i, numLanes;

    if (eyescanController == NULL)
        return cAtFalse;

    numLanes = AtEyeScanControllerNumLanes(eyescanController);
    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        AtEyeScanLane lane = AtEyeScanControllerLaneGet(eyescanController, lane_i);
        if (AtEyeScanLaneEyeScanIsEnabled(lane))
            return cAtTrue;
        }

    return cAtFalse;
    }

static eAtRet EyeScanEnable(AtSerdesController self, eBool enabled)
    {
    AtEyeScanController eyescanController = AtSerdesControllerEyeScanControllerGet(self);
    uint8 lane_i, numLanes;

    if (eyescanController == NULL)
        return cAtOk;

    numLanes = AtEyeScanControllerNumLanes(eyescanController);
    for (lane_i = 0; lane_i < numLanes; lane_i++)
        {
        AtEyeScanLane lane = AtEyeScanControllerLaneGet(eyescanController, lane_i);
        AtEyeScanLaneEyeScanEnable(lane, enabled);
        }

    return cAtOk;
    }

static eAtRet RxAfeTrimSet(AtSerdesController self, uint32 value)
    {
    AtDrp drp = AtSerdesControllerDrp(self);
    uint32 regAddr = RX_CM_TRIM_DRP_ADDRESS;
    uint32 regVal = AtDrpRead(drp, regAddr);
    mRegFieldSet(regVal, RX_CM_TRIM_, value);
    AtDrpWrite(drp, regAddr, regVal);
    return cAtOk;
    }

static uint32 RxAfeTrimGet(AtSerdesController self)
    {
    AtDrp drp = AtSerdesControllerDrp(self);
    uint32 regAddr = RX_CM_TRIM_DRP_ADDRESS;
    uint32 regVal = AtDrpRead(drp, regAddr);
    return mRegField(regVal, RX_CM_TRIM_);
    }

static uint32 RxAfeDefaultTrimValue(eAtSerdesMode mode)
    {
    /* Ref: ug578-ultrascale-gty-transceivers_v1.2.pdf, page 181 */
    if ((mode == cAtSerdesModeStm1) ||
        (mode == cAtSerdesModeStm0) ||
        (mode == cAtSerdesModeStm4) ||
        (mode == cAtSerdesModeEth100M))
        return 0x3; /* 330 mV */

    return 0xA; /* 800 mV */
    }

static const char *RxAfeTrimValue2String(uint32 value)
    {
    static char valueString[16];

    if (value == 0x3) return "330mV";
    if (value == 0xA) return "800mV";

    AtSnprintf(valueString, sizeof(valueString), "0x%x", value);
    return valueString;
    }

static eAtRet RxAfeTrimDefaultSet(AtSerdesController self, eAtSerdesMode newMode)
    {
    return RxAfeTrimSet(self, RxAfeDefaultTrimValue(newMode));
    }

static uint32 StartVersionShouldNotTouchAfeTrim(Tha6029FaceplateSerdesController self)
    {
    /* TODO: still waiting for customer feedback. Debugging is still in progress */
    AtUnused(self);
    return cInvalidUint32;
    }

static eBool ShouldTouchAfeTrim(AtSerdesController self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesControllerDeviceGet(self);
    ThaVersionReader versionReader = ThaDeviceVersionReader((AtDevice)device);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersionShouldNotTouch = mMethodsGet(mThis(self))->StartVersionShouldNotTouchAfeTrim(mThis(self));

    if (currentVersion >= startVersionShouldNotTouch)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 DefaultExcessiveErrorRatioUpperThreshold(eAtSerdesMode mode)
    {
    if (mode == cAtSerdesModeEth10G)
        return 6;
    return 12;
    }

static uint32 DefaultExcessiveErrorRatioLowerThreshold(eAtSerdesMode mode)
    {
    if (mode == cAtSerdesModeEth10G)
        return 1;
    if (mode == cAtSerdesModeEth1G)
        return 8;
    return 1;
    }

static eAtRet ExcessiveErrorRatioThresholdDefaultSet(AtSerdesController self, eAtSerdesMode newMode)
    {
    AtEthPort port;
    eAtRet ret = cAtOk;

    if (!AtSerdesControllerModeIsEthernet(newMode))
        return cAtOk;

    if (LocalPortIdInGroup(self) != 0)
        return cAtOk;

    port = (AtEthPort)AtSerdesControllerPhysicalPortGet(self);
    if (!Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioIsSupported(port))
        return cAtOk;

    ret |= Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioUpperThresholdSet(port, DefaultExcessiveErrorRatioUpperThreshold(newMode));
    ret |= Tha60290021FaceplateSerdesEthPortExcessiveErrorRatioLowerThresholdSet(port, DefaultExcessiveErrorRatioLowerThreshold(newMode));

    return ret;
    }

static uint32 Stm1PhysicalParamDefaultValue(eAtSerdesParam param)
    {
    switch ((uint32)param)
        {
        case cAtSerdesParamRXLPMHFOVRDEN  : return 1;
        case cAtSerdesParamRXLPMLFKLOVRDEN: return 1;
        case cAtSerdesParamRXLPMOSOVRDEN  : return 1;
        case cAtSerdesParamRXLPMGCOVRDEN  : return 1;
        case cAtSerdesParamRXOSOVRDEN     : return 1;

        /* Not necessary */
        default:
            return cInvalidUint32;
        }
    }

static uint32 Stm4PhysicalParamDefaultValue(eAtSerdesParam param)
    {
    switch ((uint32)param)
        {
        case cAtSerdesParamRXLPMOSOVRDEN  : return 1;
        case cAtSerdesParamRXLPMHFOVRDEN  : return 0;
        case cAtSerdesParamRXLPMLFKLOVRDEN: return 0;
        case cAtSerdesParamRXLPMGCOVRDEN  : return 0;
        case cAtSerdesParamRXOSOVRDEN     : return 0;

        /* Not necessary */
        default:
            return cInvalidUint32;
        }
    }

static uint32 Stm16PhysicalParamDefaultValue(eAtSerdesParam param)
    {
    return Stm4PhysicalParamDefaultValue(param);
    }

static uint32 OneGePhysicalParamDefaultValue(eAtSerdesParam param)
    {
    return Stm4PhysicalParamDefaultValue(param);
    }

static uint32 TenGePhysicalParamDefaultValue(eAtSerdesParam param)
    {
    switch ((uint32)param)
        {
        case cAtSerdesParamRXLPMOSOVRDEN  : return 0;
        case cAtSerdesParamRXLPMHFOVRDEN  : return 0;
        case cAtSerdesParamRXLPMLFKLOVRDEN: return 0;
        case cAtSerdesParamRXLPMGCOVRDEN  : return 0;
        case cAtSerdesParamRXOSOVRDEN     : return 0;

        /* Not necessary */
        default:
            return cInvalidUint32;
        }
    }

static uint32 Stm64PhysicalParamDefaultValue(eAtSerdesParam param)
    {
    return TenGePhysicalParamDefaultValue(param);
    }

static uint32 BaseFxPhysicalParamDefaultValue(eAtSerdesParam param)
    {
    switch ((uint32)param)
        {
        case cAtSerdesParamRXLPMHFOVRDEN  : return 1;
        case cAtSerdesParamRXLPMLFKLOVRDEN: return 1;
        case cAtSerdesParamRXLPMOSOVRDEN  : return 1;
        case cAtSerdesParamRXLPMGCOVRDEN  : return 1;
        case cAtSerdesParamRXOSOVRDEN     : return 1;

        /* Not necessary */
        default:
            return cInvalidUint32;
        }
    }

static uint32 PhysicalParamDefaultValue(AtSerdesController self, eAtSerdesParam param)
    {
    eAtSerdesMode mode = AtSerdesControllerModeGet(self);

    switch ((uint32)mode)
        {
        case cAtSerdesModeStm0   : return Stm1PhysicalParamDefaultValue(param);
        case cAtSerdesModeStm1   : return Stm1PhysicalParamDefaultValue(param);
        case cAtSerdesModeStm4   : return Stm4PhysicalParamDefaultValue(param);
        case cAtSerdesModeStm16  : return Stm16PhysicalParamDefaultValue(param);
        case cAtSerdesModeStm64  : return Stm64PhysicalParamDefaultValue(param);
        case cAtSerdesModeEth1G  : return OneGePhysicalParamDefaultValue(param);
        case cAtSerdesModeEth10G : return TenGePhysicalParamDefaultValue(param);
        case cAtSerdesModeEth100M: return BaseFxPhysicalParamDefaultValue(param);

        default:
            AtDriverLogWithFileLine(AtDriverSharedDriverGet(),
                                    cAtLogLevelCritical, AtSourceLocation,
                                    "Default value for param %d of SERDES %s has not been determined\r\n",
                                    param, AtObjectToString((AtObject)self));
            return cInvalidUint32;
        }
    }

static eAtRet PhysicalParamDefaultSet(AtSerdesController self)
    {
    eAtSerdesParam newParams[] = {cAtSerdesParamRXLPMOSOVRDEN,
                                  cAtSerdesParamRXLPMHFOVRDEN,
                                  cAtSerdesParamRXLPMLFKLOVRDEN,
                                  cAtSerdesParamRXLPMGCOVRDEN,
                                  cAtSerdesParamRXOSOVRDEN};
    uint32 param_i;
    eAtRet ret = cAtOk;

    for (param_i = 0; param_i < mCount(newParams); param_i++)
        {
        eAtSerdesParam param = newParams[param_i];
        uint32 defaultValue;

        /* Better do not touch */
        defaultValue = PhysicalParamDefaultValue(self, param);
        if (defaultValue == cInvalidUint32)
            continue;

        if (AtSerdesControllerPhysicalParamIsSupported(self, param))
            ret |= AtSerdesControllerPhysicalParamSet(self, param, defaultValue);
        }

    ret |= AtSerdesControllerPhysicalParamSet(self, cAtSerdesParamRXLPM_OS_CFG1, 0x8002);

    return ret;
    }

static uint32 DefaultAutoResetPpmThresholdValue(AtSerdesController self)
    {
    AtUnused(self);
    return 152;
    }

static eBool IsSerdesEth(eAtSerdesMode mode)
    {
    if ((mode == cAtSerdesModeEth100M) || (mode == cAtSerdesModeEth1G) || (mode == cAtSerdesModeEth10G))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet PtpCfgDefaultOnSerdesMode(AtSerdesController self, eAtSerdesMode mode)
    {
    AtPtpPort ptpPort = NULL;
    eAtRet ret = cAtOk;
    AtModulePtp module = (AtModulePtp)AtDeviceModuleGet(AtSerdesControllerDeviceGet(self), cAtModulePtp);

    if (module && IsSerdesEth(mode))
        {
        AtEthPort ethPort = (AtEthPort)AtSerdesControllerPhysicalPortGet(self);
        ptpPort = AtModulePtpPortGet(module, AtChannelIdGet((AtChannel)ethPort));
        ret |= ThaPtpPortDelayAdjustDefaultSet((ThaPtpPort)ptpPort, mode);
        }

    return ret;
    }

static eAtRet DefaultSet(AtSerdesController self, eAtSerdesMode newMode)
    {
    eAtRet ret = cAtOk;
    eAtSerdesEqualizerMode defaultMode;

    ret |= TimingDefaultSet(self);

    if (ShouldTouchAfeTrim(self))
        ret |= RxAfeTrimDefaultSet(self, newMode);

    defaultMode = mMethodsGet(self)->DefaultEqualizerMode(self);
    if (AtSerdesControllerEqualizerModeGet(self) != defaultMode)
        ret |= AtSerdesControllerEqualizerModeSet(self, defaultMode);
    ret |= ExcessiveErrorRatioThresholdDefaultSet(self, newMode);

    if (SerdesHasNewParams(self))
        ret |= PhysicalParamDefaultSet(self);
    ret |= AtSerdesControllerAutoResetPpmThresholdSet(self, DefaultAutoResetPpmThresholdValue(self));

    ret |= PtpCfgDefaultOnSerdesMode(self, newMode);
    return ret;
    }

static eBool ShouldDisableInterruptMaskWithNewMode(AtSerdesController self, eAtSerdesMode newMode)
    {
    if (mBoolToBin(AtSerdesControllerModeIsEthernet(newMode)) != mBoolToBin(AtSerdesControllerModeIsEthernet(mThis(self)->mode)))
        return cAtTrue;

    if (((newMode == cAtSerdesModeEth10G) ? 1 : 0) != ((mThis(self)->mode == cAtSerdesModeEth10G) ? 1 : 0))
        return cAtTrue;

    return cAtFalse;
    }

static eBool DataMaskOnResetConfigurable(Tha6029FaceplateSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DataMaskingOnResetRegister(AtSerdesController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_RX_Auto_Reset_Masking_Data_Base);
    }

static uint32 DataMaskingOnRetsetRegField_Mask(AtSerdesController self)
    {
    uint32 mask = cAf6_RX_Auto_Reset_Masking_Data_auto_rst_disdat_Mask << LocalPortIdInGroup(self);
    return mask;
    }

static uint32 DataMaskingOnRetsetRegField_Shift(AtSerdesController self)
    {
    uint32 shift = cAf6_RX_Auto_Reset_Masking_Data_auto_rst_disdat_Shift + LocalPortIdInGroup(self);
    return shift;
    }

static eAtRet EthSerdesDataMaskingOnResetEnable(Tha6029FaceplateSerdesController self, eBool enable)
    {
    AtSerdesController serdes = (AtSerdesController)self;
    uint32 address = DataMaskingOnResetRegister(serdes);
    uint32 regVal = AtSerdesControllerRead(serdes, address, cAtModuleEth);
    uint32 field_Mask = DataMaskingOnRetsetRegField_Mask(serdes);
    uint32 field_Shift = DataMaskingOnRetsetRegField_Shift(serdes);

    mRegFieldSet(regVal, field_, (enable ? 1 : 0));
    AtSerdesControllerWrite(serdes, address, regVal, cAtModuleEth);
    return cAtOk;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    uint32 regAddr = ChangeModeRegister(self);
    uint32 regVal;
    uint8 hwMode = ModeSw2Hw(self, mode);
    eAtLoopbackMode currentLoopbackMode;
    eBool modeChanged;

    if (hwMode == cInvalidUint8)
        return cAtErrorModeNotSupport;

    if (!AtSerdesControllerModeIsSupported(self, mode))
    	return cAtErrorModeNotSupport;

    if (ShouldDisableInterruptMaskWithNewMode(self, mode))
        {
        AtChannel phyPort = AtSerdesControllerPhysicalPortGet(self);
        AtChannelInterruptMaskSet(phyPort, AtChannelSupportedInterruptMasks(phyPort), 0);
        }

    currentLoopbackMode = AtSerdesControllerLoopbackGet(self);

    /* Build request */
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_OC192_MUX_OC48_Change_Mode_chg_done_, 1); /* To clear sticky */
    mRegFieldSet(regVal, cAf6_OC192_MUX_OC48_Change_Mode_chg_mode_, hwMode);
    mRegFieldSet(regVal, cAf6_OC192_MUX_OC48_Change_Mode_chg_port_, LocalPortIdInGroup(self));

    /* Trigger */
    mRegFieldSet(regVal, cAf6_OC192_MUX_OC48_Change_Mode_chg_trig_, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_OC192_MUX_OC48_Change_Mode_chg_trig_, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* Timeout wait for hardware done */
    if (!ModeApplied(self))
        return cAtErrorOperationTimeout;

    /* Hardware return current mode in different registers, so the following code
     * is to help simulation mode works properly */
    if (IsSimulated(self))
        SimulateCurrentModeSet(self, mode);

    modeChanged = (mode != mThis(self)->mode) ? cAtTrue : cAtFalse;
    mThis(self)->mode = mode;

    /* Eyescan need to be re-enabled (if it has been enabled before) */
    if (AtSerdesControllerIsControllable(self))
        {
        if (EyeScanIsEnabled(self) && modeChanged)
            {
            EyeScanEnable(self, cAtFalse);
            EyeScanEnable(self, cAtTrue);
            }
        }

    /* Set masking data before reset */
    if (mMethodsGet(mThis(self))->DataMaskOnResetConfigurable(mThis(self)))
        {
        eBool enable = (eBool)(mode == cAtSerdesModeEth10G ? 0 : 1);
        EthSerdesDataMaskingOnResetEnable(mThis(self), enable);
        }

    AtSerdesControllerReset(self);
    Tha60290021FacePlateSerdesControllerPrbsHwModeSet(self, Tha60290021FacePlateSerdesControllerPrbsDefaultMode(self));
    PrbsStop(self);

    /* 10G and other modes use different remote loopback, so need to update */
    if (mMethodsGet(mThis(self))->UseDifferentRemoteLoopbackFor10G(mThis(self)) && (currentLoopbackMode == cAtLoopbackModeRemote))
        AtSerdesControllerLoopbackSet(self, currentLoopbackMode);

    return DefaultSet(self, mode);
    }

static eTha6029FaceplateSerdesHwMode HwModeGet(AtSerdesController self)
    {
    uint32 regAddr = CurrentModeRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 modeMask = CurrentModeMask(self);
    uint32 modeShift = CurrentModeShift(self);
    return mRegField(regVal, mode);
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    eAtSerdesMode mode;

    if (AtDeviceInAccessible(AtSerdesControllerDeviceGet(self)))
        return mThis(self)->mode;

    /* Mode that has 1:1 mapping with HW */
    mode = ModeHw2Sw(Tha6029FaceplateSerdesControllerHwModeGet(self));
    if (mode == mThis(self)->mode)
        return mode;

    /* Low rate mode, need to make sure that both HW and SW match this */
    if (IsLowRateMode(mode) && IsLowRateMode(mThis(self)->mode))
        return mThis(self)->mode;

    return cAtSerdesModeUnknown;
    }

static eAtSerdesMode DefaultMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeStm16;
    }

static float SpeedInGbps(AtSerdesController self)
    {
    AtUnused(self);
    switch (mThis(self)->mode)
        {
        case cAtSerdesModeStm64   : return (float)(4 * 2.488);
        case cAtSerdesModeStm16   : return (float)(2.488);
        case cAtSerdesModeStm4    : return (float)(0.622);
        case cAtSerdesModeStm1    : return (float)(0.155);
        case cAtSerdesModeStm0    : return (float)(0.54);
        case cAtSerdesModeEth10G  : return (float)(4 * 3.125);
        case cAtSerdesModeEth1G   : return (float)(1.25);
        case cAtSerdesModeEth100M : return (float)(0.125);
        case cAtSerdesModeEth10M  : return (float)(0.0125);

        case cAtSerdesModeEth2500M:return (float)(3.125);
        case cAtSerdesModeEth40G:  return (float)(16 * 3.125);
        case cAtSerdesModeOcn:
        case cAtSerdesModeGe:
        case cAtSerdesModeUnknown:
        default :
            return 0.0;
        }
    }

static uint32 PllStatusRegister(AtSerdesController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_OC192_MUX_OC48_PLL_Status_Base);
    }

static eBool PllIsLocked(AtSerdesController self)
    {
    uint32 regAddr = PllStatusRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 localId = LocalPortIdInGroup(self);
    uint32 cpllLock  = regVal & (cBit0 << localId);
    uint32 qpll0Lock = regVal & cAf6_OC192_MUX_OC48_PLL_Status_QPLL0_Lock_Mask;
    uint32 qpll1Lock = regVal & cAf6_OC192_MUX_OC48_PLL_Status_QPLL1_Lock_Mask;

    return (cpllLock && qpll0Lock && qpll1Lock) ? cAtTrue : cAtFalse;
    }

static eBool PllChanged(AtSerdesController self)
    {
    uint32 regAddr = PllStatusRegister(self);
    uint32 regVal  = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 localId = LocalPortIdInGroup(self);
    uint32 cpllLockChange  = regVal & (cBit12 << localId);
    uint32 qpll0LockChange = regVal & cAf6_OC192_MUX_OC48_PLL_Status_QPLL0_Lock_change_Mask;
    uint32 qpll1LockChange = regVal & cAf6_OC192_MUX_OC48_PLL_Status_QPLL1_Lock_change_Mask;
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    return (cpllLockChange && qpll0LockChange && qpll1LockChange) ? cAtTrue : cAtFalse;
    }

static uint32 SerdesModeToPllLockMask(AtSerdesController self, uint32 cpllLockMask, uint32 qpll0LockMask, uint32 qpll1LockMask)
    {
    eAtSerdesMode mode = AtSerdesControllerModeGet(self);
    uint32 pllLockMask = 0;

    if (mode == cAtSerdesModeEth10G)
        pllLockMask = qpll0LockMask;
    else if (mode == cAtSerdesModeStm0 || mode == cAtSerdesModeStm1 || mode == cAtSerdesModeStm4 || mode == cAtSerdesModeStm16 || mode == cAtSerdesModeStm64)
        pllLockMask = qpll1LockMask;
    else
        pllLockMask = cpllLockMask;

    return pllLockMask;
    }

static eBool PllIsLockedWithSerdesMode(AtSerdesController self)
    {
    uint32 regAddr = PllStatusRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 localId = LocalPortIdInGroup(self);
    uint32 cpllLockMask  = (cBit0 << localId);
    uint32 qpll0LockMask = cAf6_OC192_MUX_OC48_PLL_Status_QPLL0_Lock_Mask;
    uint32 qpll1LockMask = cAf6_OC192_MUX_OC48_PLL_Status_QPLL1_Lock_Mask;
    uint32 pllLockMask = SerdesModeToPllLockMask(self, cpllLockMask, qpll0LockMask, qpll1LockMask);
    uint32 pllLock = regVal & pllLockMask;

    return (pllLock) ? cAtTrue : cAtFalse;
    }

static eBool PllChangedWithSerdesMode(AtSerdesController self)
    {
    uint32 regAddr = PllStatusRegister(self);
    uint32 regVal  = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 localId = LocalPortIdInGroup(self);
    uint32 cpllLockChangeMask  = (cBit12 << localId);
    uint32 qpll0LockChangeMask = cAf6_OC192_MUX_OC48_PLL_Status_QPLL0_Lock_change_Mask;
    uint32 qpll1LockChangeMask = cAf6_OC192_MUX_OC48_PLL_Status_QPLL1_Lock_change_Mask;
    uint32 pllLockChangeMask = SerdesModeToPllLockMask(self, cpllLockChangeMask, qpll0LockChangeMask, qpll1LockChangeMask);
    uint32 pllLockChange = regVal & pllLockChangeMask;

    AtSerdesControllerWrite(self, regAddr, pllLockChange, cAtModuleEth);
    return (pllLockChange) ? cAtTrue : cAtFalse;
    }

static uint32 ParamMask(AtSerdesController self)
    {
    return cBit4_0 << (LocalPortIdInGroup(self) * 5);
    }

static uint32 ParamShift(AtSerdesController self)
    {
    return LocalPortIdInGroup(self) * 5;
    }

static eAtRet PhysicalParamSetWithBaseRegister(AtSerdesController self, uint32 localAddress, uint32 value)
    {
    uint32 regAddr    = AddressWithLocalAddress(self, localAddress);
    uint32 regVal     = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 paramMask  = ParamMask(self);
    uint32 paramShift = ParamShift(self);

    mRegFieldSet(regVal, param, value);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static uint32 PhysicalParamGetWithBaseRegister(AtSerdesController self, uint32 localAddress)
    {
    uint32 regAddr    = AddressWithLocalAddress(self, localAddress);
    uint32 regVal     = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 paramMask  = ParamMask(self);
    uint32 paramShift = ParamShift(self);
    return mRegField(regVal, param);
    }

static eAtRet TxDiffCtrlSet(AtSerdesController self, uint32 value)
    {
    return PhysicalParamSetWithBaseRegister(self, cAf6Reg_OC192_MUX_OC48_TXDIFFCTRL_Base, value);
    }

static uint32 TxDiffCtrlGet (AtSerdesController self)
    {
    return PhysicalParamGetWithBaseRegister(self, cAf6Reg_OC192_MUX_OC48_TXDIFFCTRL_Base);
    }

static eAtRet TxPreCursorSet(AtSerdesController self, uint32 value)
    {
    return PhysicalParamSetWithBaseRegister(self, cAf6Reg_OC192_MUX_OC48_TXPRECURSOR_Base, value);
    }

static uint32 TxPreCursorGet(AtSerdesController self)
    {
    return PhysicalParamGetWithBaseRegister(self, cAf6Reg_OC192_MUX_OC48_TXPRECURSOR_Base);
    }

static eAtRet TxPostCursorSet(AtSerdesController self, uint32 value)
    {
    return PhysicalParamSetWithBaseRegister(self, cAf6Reg_OC192_MUX_OC48_TXPOSTCURSOR_Base, value);
    }

static uint32 TxPostCursorGet(AtSerdesController self)
    {
    return PhysicalParamGetWithBaseRegister(self, cAf6Reg_OC192_MUX_OC48_TXPOSTCURSOR_Base);
    }

static uint32 NewParamAddressBase(AtSerdesController self, eAtSerdesParam param)
    {
    if (!SerdesHasNewParams(self))
        return cInvalidUint32;

    AtUnused(self);

    switch ((uint32)param)
        {
        case cAtSerdesParamRXLPMHFOVRDEN  : return cAf6Reg_RX_RXLPMHFOVRDEN_Control_Base;
        case cAtSerdesParamRXLPMLFKLOVRDEN: return cAf6Reg_RX_RXLPMLFKLOVRDEN_Control_Base;
        case cAtSerdesParamRXOSOVRDEN     : return cAf6Reg_RX_RXOSOVRDEN_Control_Base;
        case cAtSerdesParamRXLPMOSHOLD    : return cAf6Reg_RX_RXLPMOSHOLD_Control_Base;
        case cAtSerdesParamRXLPMOSOVRDEN  : return cAf6Reg_RX_RXLPMOSOVRDEN_Control_Base;
        case cAtSerdesParamRXLPM_OS_CFG1  : return cAf6Reg_RX_RXLPM_OS_CFG1_Control_Base;
        case cAtSerdesParamRXLPMGCHOLD    : return cAf6Reg_RX_RXLPMGCHOLD_Control_Base;
        case cAtSerdesParamRXLPMGCOVRDEN  : return cAf6Reg_RX_RXLPMGCOVRDEN_CONTROL_Base;
        case cAtSerdesParamRXLPM_GC_CFG   : return cAf6Reg_RX_RXLPM_GC_CFG_Control_Base;
        default:
            return cInvalidUint32;
        }
    }

static uint32 DrpPortOffset(AtSerdesController self)
    {
    return LocalPortIdInGroup(self) * 0x400;
    }

static eAtRet NewBoolParamSet(AtSerdesController self, uint32 addressBase, uint32 value)
    {
    uint32 regAddr = AddressWithLocalAddress(self, addressBase);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 fieldShift, fieldMask;
    fieldShift = LocalPortIdInGroup(self);
    fieldMask = cBit0 << fieldShift;
    mRegFieldSet(regVal, field, value);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static uint32 NewBoolParamGet(AtSerdesController self, uint32 addressBase)
    {
    uint32 regAddr = AddressWithLocalAddress(self, addressBase);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 fieldShift, fieldMask;
    fieldShift = LocalPortIdInGroup(self);
    fieldMask = cBit0 << fieldShift;
    return mRegField(regVal, field);
    }

static eAtRet NewUint16ParamSet(AtSerdesController self, uint32 addressBase, uint32 value)
    {
    uint32 regAddr = AddressWithLocalAddress(self, addressBase) + DrpPortOffset(self);
    AtSerdesControllerWrite(self, regAddr, value, cAtModuleEth);
    return cAtOk;
    }

static uint32 NewUint16ParamGet(AtSerdesController self, uint32 addressBase)
    {
    uint32 regAddr = AddressWithLocalAddress(self, addressBase) + DrpPortOffset(self);
    return AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    }

static eAtRet NewParamSet(AtSerdesController self, eAtSerdesParam param, uint32 addressBase, uint32 value)
    {
    switch ((uint32)param)
        {
        case cAtSerdesParamRXLPMHFOVRDEN  : return NewBoolParamSet(self, addressBase, value);
        case cAtSerdesParamRXLPMLFKLOVRDEN: return NewBoolParamSet(self, addressBase, value);
        case cAtSerdesParamRXOSOVRDEN     : return NewBoolParamSet(self, addressBase, value);
        case cAtSerdesParamRXLPMOSHOLD    : return NewBoolParamSet(self, addressBase, value);
        case cAtSerdesParamRXLPMOSOVRDEN  : return NewBoolParamSet(self, addressBase, value);
        case cAtSerdesParamRXLPMGCHOLD    : return NewBoolParamSet(self, addressBase, value);
        case cAtSerdesParamRXLPMGCOVRDEN  : return NewBoolParamSet(self, addressBase, value);
        case cAtSerdesParamRXLPM_OS_CFG1  : return NewUint16ParamSet(self, addressBase, value);
        case cAtSerdesParamRXLPM_GC_CFG   : return NewUint16ParamSet(self, addressBase, value);
        default:
            return cAtErrorModeNotSupport;
        }
    }

static uint32 NewParamGet(AtSerdesController self, eAtSerdesParam param, uint32 addressBase)
    {
    switch ((uint32)param)
        {
        case cAtSerdesParamRXLPMHFOVRDEN  : return NewBoolParamGet(self, addressBase);
        case cAtSerdesParamRXLPMLFKLOVRDEN: return NewBoolParamGet(self, addressBase);
        case cAtSerdesParamRXOSOVRDEN     : return NewBoolParamGet(self, addressBase);
        case cAtSerdesParamRXLPMOSHOLD    : return NewBoolParamGet(self, addressBase);
        case cAtSerdesParamRXLPMOSOVRDEN  : return NewBoolParamGet(self, addressBase);
        case cAtSerdesParamRXLPMGCHOLD    : return NewBoolParamGet(self, addressBase);
        case cAtSerdesParamRXLPMGCOVRDEN  : return NewBoolParamGet(self, addressBase);
        case cAtSerdesParamRXLPM_OS_CFG1  : return NewUint16ParamGet(self, addressBase);
        case cAtSerdesParamRXLPM_GC_CFG   : return NewUint16ParamGet(self, addressBase);
        default:
            return cInvalidUint32;
        }
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    uint32 newParamAddressBase;

    if (!AtSerdesControllerPhysicalParamValueIsInRange(self, param, value))
        return cAtErrorOutOfRangParm;

    /* New XILINX params */
    newParamAddressBase = NewParamAddressBase(self, param);
    if (newParamAddressBase != cInvalidUint32)
        return NewParamSet(self, param, newParamAddressBase, value);

    /* Standard VLANs */
    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor:
            return TxPreCursorSet(self, value);
        case cAtSerdesParamTxPostCursor:
            return TxPostCursorSet(self, value);
        case cAtSerdesParamTxDiffCtrl:
            return TxDiffCtrlSet(self, value);

        default :
            return cAtErrorModeNotSupport;
        }
    return cAtOk;
    }

static uint32 PhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    uint32 newParamAddressBase = NewParamAddressBase(self, param);

    if (newParamAddressBase != cInvalidUint32)
        return NewParamGet(self, param, newParamAddressBase);

    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor:
            return TxPreCursorGet(self);
        case cAtSerdesParamTxPostCursor:
            return TxPostCursorGet(self);
        case cAtSerdesParamTxDiffCtrl:
            return TxDiffCtrlGet(self);
        default :
            return 0;
        }

    return 0;
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
    uint32 newParamAddressBase = NewParamAddressBase(self, param);

    if (newParamAddressBase != cInvalidUint32)
        return cAtTrue;

    switch ((uint32)param)
        {
        case cAtSerdesParamTxPreCursor:
        case cAtSerdesParamTxPostCursor:
        case cAtSerdesParamTxDiffCtrl:
            return cAtTrue;

        case cAtSerdesParamVod:
        case cAtSerdesParamPreEmphasisPreTap:
        case cAtSerdesParamPreEmphasisFirstPostTap:
        case cAtSerdesParamPreEmphasisSecondPostTap:
        case cAtSerdesParamRxEqualizationDcGain:
        case cAtSerdesParamRxEqualizationControl:
        default :
            return cAtFalse;
        }
    }

static eBool PhysicalParamValueIsInRange(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    AtUnused(self);

    switch (param)
        {
        case cAtSerdesParamTxPreCursor:
        case cAtSerdesParamTxPostCursor:
        case cAtSerdesParamTxDiffCtrl:
            return (value <= 0x1f) ? cAtTrue : cAtFalse;

        /* New param */
        case cAtSerdesParamRXLPMHFOVRDEN  : return (value <= 1) ? cAtTrue : cAtFalse;
        case cAtSerdesParamRXLPMLFKLOVRDEN: return (value <= 1) ? cAtTrue : cAtFalse;
        case cAtSerdesParamRXOSOVRDEN     : return (value <= 1) ? cAtTrue : cAtFalse;
        case cAtSerdesParamRXLPMOSHOLD    : return (value <= 1) ? cAtTrue : cAtFalse;
        case cAtSerdesParamRXLPMOSOVRDEN  : return (value <= 1) ? cAtTrue : cAtFalse;
        case cAtSerdesParamRXLPMGCHOLD    : return (value <= 1) ? cAtTrue : cAtFalse;
        case cAtSerdesParamRXLPMGCOVRDEN  : return (value <= 1) ? cAtTrue : cAtFalse;
        case cAtSerdesParamRXLPM_OS_CFG1  : return (value <= cBit15_0) ? cAtTrue : cAtFalse;
        case cAtSerdesParamRXLPM_GC_CFG   : return (value <= cBit15_0) ? cAtTrue : cAtFalse;

        /* Not support params */
        case cAtSerdesParamVod:
        case cAtSerdesParamPreEmphasisPreTap:
        case cAtSerdesParamPreEmphasisFirstPostTap:
        case cAtSerdesParamPreEmphasisSecondPostTap:
        case cAtSerdesParamRxEqualizationDcGain:
        case cAtSerdesParamRxEqualizationControl:
        default :
            return cAtFalse;
        }
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    switch (loopbackMode)
        {
        case cAtLoopbackModeRelease:
        case cAtLoopbackModeLocal:
        case cAtLoopbackModeRemote:
        case cAtLoopbackModeDual:
            return cAtTrue;
        default :
            return cAtFalse;
        }
    }

static uint32 LoopbackRegister(AtSerdesController self)
    {

    return AddressWithLocalAddress(self, cAf6Reg_OC192_MUX_OC48_LoopBack_Base);
    }

static uint32 LoopbackModeMask(AtSerdesController self)
    {
    return cAf6_OC192_MUX_OC48_LoopBack_lpback_subport0_Mask << (LocalPortIdInGroup(self) * 4);
    }

static uint32 LoopbackModeShift(AtSerdesController self)
    {
    return LocalPortIdInGroup(self) * 4;
    }

static uint32 DefaultLoopbackLayer(AtSerdesController self)
    {
    AtUnused(self);
    return cPmaLoopback;
    }

static eBool Vu9PPmaFarEndLoopbackSupported(AtSerdesController self)
    {
    ThaVersionReader versionReader = VersionReader((AtSerdesController)self);
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x7, 0x00);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool Vu13PPmaFarEndLoopbackSupported(AtSerdesController self)
    {
    ThaVersionReader versionReader = VersionReader((AtSerdesController)self);
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x4, 0x1, 0x00);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool PmaFarEndLoopbackSupported(Tha6029FaceplateSerdesController self)
    {
    AtSerdesController serdes = (AtSerdesController)self;
    AtDevice device = AtSerdesControllerDeviceGet(serdes);

    /* New loopback hardware logic has not been available on this platform yet */
    if (Tha60290021DeviceIsRunningOnVu13P(device))
        return Vu13PPmaFarEndLoopbackSupported(serdes);

    return Vu9PPmaFarEndLoopbackSupported(serdes);
    }

static uint32 RemoteLoopbackHwMode(AtSerdesController self)
    {
    if (mMethodsGet(mThis(self))->PmaFarEndLoopbackSupported(mThis(self)))
        return DefaultLoopbackLayer(self);

    if (AtSerdesControllerModeGet(self) == cAtSerdesModeEth10G)
        return 8;

    if (mMethodsGet(mThis(self))->UseDifferentRemoteLoopbackFor10G(mThis(self)))
        return 6;

    return 8;
    }

static uint32 LoopbackModeSw2Hw(AtSerdesController self, eAtLoopbackMode loopback)
    {
    AtUnused(self);

    switch (loopback)
        {
        case cAtLoopbackModeRelease: return 0;
        case cAtLoopbackModeLocal  : return 2;
        case cAtLoopbackModeRemote : return RemoteLoopbackHwMode(self);

        case cAtLoopbackModeDual:
        default:
            return cInvalidUint32;
        }
    }

static eAtLoopbackMode LoopbackModeHw2Sw(AtSerdesController self, uint32 loopback)
    {
    eBool isTenGe;

    if (loopback == 2)
        return cAtLoopbackModeLocal;

    /* Only recent version can support PMA loopback */
    if (mMethodsGet(mThis(self))->PmaFarEndLoopbackSupported(mThis(self)) && (loopback == cPmaLoopback))
        return cAtLoopbackModeRemote;

    /* Previous versions support PCS loopback */
    isTenGe = (AtSerdesControllerModeGet(self) == cAtSerdesModeEth10G) ? cAtTrue : cAtFalse;
    if (loopback == 8)
        {
        if (!isTenGe && mMethodsGet(mThis(self))->UseDifferentRemoteLoopbackFor10G(mThis(self)))
            {
            AtDriverLog(AtDriverSharedDriverGet(),
                        cAtLogLevelWarning, "Remote loopback mode (%d) should only be applied for 10G mode\r\n",
                        loopback);
            }

        return cAtLoopbackModeRemote;
        }

    /* Also regard to optimizing, other modes different from 10G, will used this
     * mode for remote loopback */
    if (loopback == 6)
        {
        if (isTenGe && mMethodsGet(mThis(self))->UseDifferentRemoteLoopbackFor10G(mThis(self)))
            {
            AtDriverLog(AtDriverSharedDriverGet(),
                        cAtLogLevelWarning, "Remote loopback mode (%d) should not be applied for 10G mode\r\n",
                        loopback);
            }

        return cAtLoopbackModeRemote;
        }

    return cAtLoopbackModeRelease;
    }

static uint32 GearBoxAddress(AtSerdesController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_Async_GearBox_Enable_of_10Ge_mode_Base);
    }

static eAtRet GearBoxEnable(AtSerdesController self, eBool enabled)
    {
    uint32 regAddr = GearBoxAddress(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, cAf6_Async_GearBox_Enable_of_10Ge_mode_async_gearbix_enb_, enabled ? 1 : 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool GearBoxIsEnabled(AtSerdesController self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_Async_GearBox_Enable_of_10Ge_mode_Base);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    return (regVal & cAf6_Async_GearBox_Enable_of_10Ge_mode_async_gearbix_enb_Mask) ? cAtTrue : cAtFalse;
    }

static eBool ShouldChangeTimingModeWhenLoopback(AtSerdesController self)
    {
    eAtSerdesMode serdesMode = AtSerdesControllerModeGet(self);

    if ((serdesMode == cAtSerdesModeStm0) || (serdesMode == cAtSerdesModeEth100M))
        return cAtTrue;

    /* From HW version that has new parameters, then lock2ref is used on STM-1
     * interface */
    if (serdesMode == cAtSerdesModeStm1)
        return SerdesHasNewParams(self) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static eBool ShouldChangeGearBoxFor10GLoopback(Tha6029FaceplateSerdesController self, eAtLoopbackMode newLoopback)
    {
    AtUnused(self);
    AtUnused(newLoopback);
    return cAtTrue;
    }

static eAtRet LoopbackSet(AtSerdesController self, eAtLoopbackMode loopback)
    {
    uint32 regAddr = LoopbackRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 loopbackModeMask = LoopbackModeMask(self);
    uint32 loopbackModeShift = LoopbackModeShift(self);
    uint32 hwLoopbackMode;
    eAtSerdesMode serdesMode;
    eBool shouldChangeGearBoxFor10GLoopback = mMethodsGet(mThis(self))->ShouldChangeGearBoxFor10GLoopback(mThis(self), loopback);

    hwLoopbackMode = LoopbackModeSw2Hw(self, loopback);
    if (hwLoopbackMode == cInvalidUint32)
        return cAtErrorModeNotSupport;

    mRegFieldSet(regVal, loopbackMode, hwLoopbackMode);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* Gear box needs to be controlled properly */
    serdesMode = AtSerdesControllerModeGet(self);
    if ((serdesMode == cAtSerdesModeEth10G) && mMethodsGet(mThis(self))->PmaFarEndLoopbackSupported(mThis(self))
        && shouldChangeGearBoxFor10GLoopback)
        {
        eBool gearBoxEnabled = (loopback == cAtLoopbackModeRemote) ? cAtFalse : cAtTrue;
        GearBoxEnable(self, gearBoxEnabled);
        }

    /* If it is required to change SERDES timing mode on remote loopback, then
     * we need to make sure that application explicit setting will not be overridden
     * when another loopback mode is made */
    if (ShouldChangeTimingModeWhenLoopback(self))
        {
        if (loopback == cAtLoopbackModeRemote)
            TimingModeHwSet(self, cAtSerdesTimingModeLockToData);
        else
            TimingModeHwSet(self, mThis(self)->timingMode);
        }

    /* XILINX guideline that SERDES reset needs to be done on loopback operation.
     * Only do this when SERDES is power up, otherwise, timeout error code will
     * be returned */
    if (!AtSerdesControllerPowerIsDown(self))
        return AtSerdesControllerReset(self);

    return cAtOk;
    }

static eAtLoopbackMode LoopbackGet(AtSerdesController self)
    {
    uint32 regAddr = LoopbackRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 loopbackModeMask = LoopbackModeMask(self);
    uint32 loopbackModeShift = LoopbackModeShift(self);
    uint32 hwLoopbackMode = mRegField(regVal, loopbackMode);

    return LoopbackModeHw2Sw(self, hwLoopbackMode);
    }

static eAtSerdesEqualizerMode DefaultEqualizerMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesEqualizerModeLpm;
    }

static uint32 EqualizerModeRegister(AtSerdesController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_OC192_MUX_OC48_LPMDFE_Mode_Base);
    }

static uint32 EqualizerResetRegister(AtSerdesController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_OC192_MUX_OC48_LPMDFE_Reset_Base);
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    uint32 regAddr, regVal;
    uint32 localId = LocalPortIdInGroup(self);
    uint32 portMask = cBit0 << localId;
    uint32 portShift = localId;

    /* Change the mode */
    regAddr = EqualizerModeRegister(self);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, port, (mode == cAtSerdesEqualizerModeLpm) ? 1 : 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* Need to toggle reset */
    regAddr = EqualizerResetRegister(self);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, port, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    mRegFieldSet(regVal, port, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    mRegFieldSet(regVal, port, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    uint32 regAddr = EqualizerModeRegister(self);
    uint32 regVal  = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 localId = LocalPortIdInGroup(self);
    uint32 portMask = cBit0 << localId;
    return (regVal & portMask) ? cAtSerdesEqualizerModeLpm : cAtSerdesEqualizerModeDfe;
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    AtUnused(self);

    switch (mode)
        {
        case cAtSerdesEqualizerModeDfe: return cAtTrue;
        case cAtSerdesEqualizerModeLpm: return cAtTrue;
        case cAtSerdesEqualizerModeUnknown:
        default:
            return cAtFalse;
        }
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PowerCanControl(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 PowerDownRegister(AtSerdesController self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_SERDES_POWER_DOWN_Base);
    }

static uint32 RxPowerDownShift(AtSerdesController self)
    {
    return LocalPortIdInGroup(self) * 2UL;
    }

static uint32 RxPowerDownMask(AtSerdesController self)
    {
    AtUnused(self);
    return cAf6_SERDES_POWER_DOWN_RXPD0_Mask << RxPowerDownShift(self);
    }

static uint32 TxPowerDownMask(AtSerdesController self)
    {
    AtUnused(self);
    return cAf6_SERDES_POWER_DOWN_TXPD0_Mask << (LocalPortIdInGroup(self) * 2UL);
    }

static uint32 TxPowerDownShift(AtSerdesController self)
    {
    AtUnused(self);
    return (LocalPortIdInGroup(self) * 2UL) + 8;
    }

static uint32 TxElecIdleMask(AtSerdesController self)
    {
    return cBit16 << LocalPortIdInGroup(self);
    }

static uint32 TxElecIdleShift(AtSerdesController self)
    {
    return 16 + LocalPortIdInGroup(self);
    }

static eAtRet PowerDown(AtSerdesController self, eBool powerDown)
    {
    uint32 regAddr = PowerDownRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 fieldMask, fieldShift;
    uint32 powerDownValue = powerDown ? 3 : 0;
    eAtRet ret = cAtOk;

    /* RX */
    fieldMask = RxPowerDownMask(self);
    fieldShift = RxPowerDownShift(self);
    mRegFieldSet(regVal, field, powerDownValue);

    /* TX */
    fieldMask = TxPowerDownMask(self);
    fieldShift = TxPowerDownShift(self);
    mRegFieldSet(regVal, field, powerDownValue);

    /* Electric IDEL */
    fieldMask = TxElecIdleMask(self);
    fieldShift = TxElecIdleShift(self);
    mRegFieldSet(regVal, field, powerDown ? 1 : 0);

    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* XILINX recommended power up should include reset */
    if (!powerDown)
        ret = AtSerdesControllerReset(self);

    return ret;
    }

static eBool PowerIsDown(AtSerdesController self)
    {
    uint32 regAddr = PowerDownRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    return (regVal & RxPowerDownMask(self)) ? cAtTrue : cAtFalse;
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    AtSerdesController controller = (AtSerdesController)self;
    AtSerdesManager manager = AtSerdesControllerManagerGet(controller);
    uint8 localId = (uint8)(AtSerdesControllerIdGet(controller) - Tha60290021SerdesManagerFaceplateSerdesStartId(manager));
    AtSnprintf(description, sizeof(description) - 1, "faceplate_serdes.%d", localId + 1);
    return description;
    }

static uint32 DrpBaseAddress(AtSerdesController self)
    {
    uint32 portOffset = DrpPortOffset(self);
    uint32 baseAddress = AddressWithLocalAddress(self, cAf6Reg_OC192_MUX_OC48_DRP_Base);
    return baseAddress + portOffset;
    }

static AtDrp DrpObjectCreate(AtSerdesController self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self)), 0);
    AtDrp drp = AtDrpNew(DrpBaseAddress(self), hal);
    AtDrpNeedSelectPortSet(drp, cAtFalse);
    return drp;
    }

static AtPrbsEngine RawPrbsEngineObjectCreate(Tha6029FaceplateSerdesController self)
    {
    return Tha60290021FacePlateSerdesPrbsEngineNew((AtSerdesController)self);
    }

static AtPrbsEngine XfiPrbsEngineObjectCreate(Tha6029FaceplateSerdesController self, uint32 baseAddress)
    {
    return Tha60290021XfiSerdesPrbsEngineNew((AtSerdesController)self, baseAddress);
    }

static void PrbsEnginesSetupIfNeed(AtSerdesController self)
    {
    if (mThis(self)->xfiSerdesPrbs == NULL)
        {
        if (AtSerdesControllerModeGet(self) == cAtSerdesModeEth10G)
            {
            uint32 baseAddress = Tha60290021SerdesManagerXfiDiagnosticBaseAddress(self);
            mThis(self)->xfiSerdesPrbs = mMethodsGet(mThis(self))->XfiPrbsEngineObjectCreate(mThis(self), baseAddress);
            AtPrbsEngineInit(mThis(self)->xfiSerdesPrbs);
            }
        }

    if (mThis(self)->rawSerdesPrbs == NULL)
        {
        mThis(self)->rawSerdesPrbs = mMethodsGet(mThis(self))->RawPrbsEngineObjectCreate(mThis(self));
        AtPrbsEngineInit(mThis(self)->rawSerdesPrbs);
        }
    }

static AtPrbsEngine PrbsEngine(AtSerdesController self)
    {
    PrbsEnginesSetupIfNeed(self);

    if (mMethodsGet(mThis(self))->EthFramedPrbsIsRemoved(mThis(self)))
        return mThis(self)->rawSerdesPrbs;

    if (AtSerdesControllerModeGet(self) == cAtSerdesModeEth10G)
        return mThis(self)->xfiSerdesPrbs;

    /* Other modes can just only use raw PRBS engine */
    return mThis(self)->rawSerdesPrbs;
    }

static AtModule ModuleGet(AtSerdesController self, eAtModule moduleId)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    AtDevice device = AtSerdesManagerDeviceGet(manager);
    return AtDeviceModuleGet(device, moduleId);
    }

static uint32 Read(AtSerdesController self, uint32 address, eAtModule moduleId)
    {
    AtModule module = ModuleGet(self, moduleId);

    if (module)
        return mModuleHwRead(module, address);

    return cInvalidUint32;
    }

static void Write(AtSerdesController self, uint32 address, uint32 value, eAtModule moduleId)
    {
    AtModule module = ModuleGet(self, moduleId);
    if (module)
        mModuleHwWrite(module, address, value);
    }

static AtChannel PhysicalPortGet(AtSerdesController self)
    {
    eAtSerdesMode serdesMode = AtSerdesControllerModeGet(self);
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    uint8 localId = (uint8)Tha60290021SerdesManagerFaceplateSerdesLocalId(manager, AtSerdesControllerIdGet(self));

    if (AtSerdesControllerModeIsOcn(serdesMode))
        {
        AtModuleSdh sdhModule = (AtModuleSdh)ModuleGet(self, cAtModuleSdh);
        return (AtChannel)Tha60290021ModuleSdhFaceplateLineGet(sdhModule, localId);
        }

    if (AtSerdesControllerModeIsEthernet(serdesMode))
        {
        AtModuleEth ethModule = (AtModuleEth)ModuleGet(self, cAtModuleEth);
        return (AtChannel)Tha60290021ModuleEthFaceplatePortGet(ethModule, localId);
        }

    return m_AtSerdesControllerMethods->PhysicalPortGet(self);
    }

static void DeletePrbsEngine(AtPrbsEngine *engine)
    {
    AtObjectDelete((AtObject)*engine);
    *engine = NULL;
    }

static void Delete(AtObject self)
    {
    DeletePrbsEngine(&mThis(self)->xfiSerdesPrbs);
    DeletePrbsEngine(&mThis(self)->rawSerdesPrbs);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha6029FaceplateSerdesController* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(mode);

    (void)AtSerdesControllerPrbsEngine((AtSerdesController)self);
    if (AtSerdesControllerModeGet((AtSerdesController)self) == cAtSerdesModeEth10G)
        mEncodeObject(xfiSerdesPrbs);
    else
        AtCoderEncodeString(encoder, "N/A", "xfiSerdesPrbs");

    mEncodeObject(rawSerdesPrbs);
    mEncodeUInt(timingMode);
    }

static eBool IsControllable(AtSerdesController self)
    {
    if (Tha60290021DeviceIsRunningOnOtherPlatform(AtSerdesControllerDeviceGet(self)))
        return cAtFalse;

    return AtSerdesManagerSerdesIsControllable(AtSerdesControllerManagerGet(self), AtSerdesControllerIdGet(self));
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 EyeScanDrpBaseAddress(AtSerdesController self)
    {
    return DrpBaseAddress(self);
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    Tha60290021SerdesManager manager = (Tha60290021SerdesManager)AtSerdesControllerManagerGet(self);
    return Tha60290021SerdesManagerEyeScanControllerCreate(manager, self, drpBaseAddress);
    }

static uint32 EyeScanResetRegAddress(Tha6029SerdesController self)
    {
    AtUnused(self);
    return cAf6Reg_OC192_MUX_OC48_EYESCAN_Reset_Base;
    }

static uint32 EyeScanResetRegister(AtSerdesController self)
    {
    uint32 localAddress = mMethodsGet((Tha6029SerdesController)self)->EyeScanResetRegAddress((Tha6029SerdesController)self);
    return AddressWithLocalAddress(self, localAddress);
    }

static eAtRet EyeScanResetDone(AtSerdesController self)
    {
    uint32 regAddr = RxResetRegister(self);
    uint32 elapseTimeMs = 0;
    tAtOsalCurTime startTime, curTime;
    uint32 cTimeoutMs = 500;

    AtOsalCurTimeGet(&startTime);
    while (elapseTimeMs < cTimeoutMs)
        {
        if (HwResetDone(self, regAddr))
            return cAtOk;

        AtOsalCurTimeGet(&curTime);
        elapseTimeMs = mTimeIntervalInMsGet(startTime, curTime);

        if (IsSimulated(self))
            return cAtOk;
        }

    AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation,
                            "[%s] Reset SERDES EyeScan timeout\r\n",
                            AtObjectToString((AtObject)self));

    return cAtErrorSerdesResetTimeout;
    }

static eAtRet EyeScanResetTrigger(AtSerdesController self, uint32 laneId)
    {
    uint32 regAddr, regVal;
    uint32 localId = LocalPortIdInGroup(self);
    uint32 localPortMask = (cBit4 << localId);
    uint32 localPortShift = localId + 4;
    uint32 doneMask = ResetDoneMask(self);
    uint32 doneShift = ResetDoneShift(self);
    AtUnused(laneId);

    /* Clear done bit */
    regAddr = RxResetRegister(self);
    regVal  = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, done, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    /* Write 0, then write 1 to trigger */
    regAddr = EyeScanResetRegister(self);
    regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    mRegFieldSet(regVal, localPort, 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    mRegFieldSet(regVal, localPort, 1);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet EyeScanReset(AtSerdesController self, uint32 laneId)
    {
    Tha6029SerdesController serdes = (Tha6029SerdesController)self;

    if (mMethodsGet(serdes)->HasEyeScanReset(serdes))
        {
        eAtRet ret = cAtOk;
        ret |= EyeScanResetTrigger(self, laneId);
        ret |= EyeScanResetDone(self);
        return ret;
        }

    return cAtOk;
    }

static uint32 NumberPortInGroup(void)
    {
    return 8;
    }

static uint32 SubPortInGroup(AtSerdesController self)
    {
    return AtSerdesControllerHwIdGet(self) % NumberPortInGroup();
    }

static uint32 PrbsControlAddress(AtSerdesController self)
    {
    if ((AtSerdesControllerHwIdGet(self) / NumberPortInGroup()) == 0)
        return cAf6Reg_o_control4_Base + cTopBaseAddress;
    return cAf6Reg_o_control6_Base + cTopBaseAddress;
    }

static uint32 ModeShift(AtSerdesController self)
    {
    return SubPortInGroup(self) * 4;
    }

static uint32 ModeMask(AtSerdesController self)
    {
    return cBit3_0 << (SubPortInGroup(self) * 4);
    }

static eAtModulePrbsRet PrbsHwModeSet(AtSerdesController self, uint32 mode)
    {
    uint32 regAddr = PrbsControlAddress(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 modeFieldMask  = ModeMask(self);
    uint32 modeFieldShift = ModeShift(self);
    mRegFieldSet(regVal, modeField, mode);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtModulePrbsRet PrbsHwSerdesModeSet(Tha6029FaceplateSerdesController self, uint32 mode)
    {
    return PrbsHwModeSet((AtSerdesController)self, mode);
    }

static uint32 PrbsHwModeGet(AtSerdesController self)
    {
    uint32 regAddr = PrbsControlAddress(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 modeMask  = ModeMask(self);
    uint32 modeShift = ModeShift(self);
    return mRegField(regVal, mode);
    }

static uint32 PrbsDefaultMode(AtSerdesController serdes)
    {
    uint32 serdesMode = AtSerdesControllerModeGet(serdes);

    switch (serdesMode)
        {
        case cAtSerdesModeStm64:   return 0;
        case cAtSerdesModeEth10G:  return 1;

        case cAtSerdesModeStm0:    return 2;
        case cAtSerdesModeStm1:    return 2;
        case cAtSerdesModeStm4:    return 2;
        case cAtSerdesModeStm16:   return 2;

        case cAtSerdesModeEth1G:   return 3;
        case cAtSerdesModeEth100M: return 4;
        case cAtSerdesModeEth10M:  return 4;

        default: return 0;
        }
    }

static uint32 DisparityForceShift(AtSerdesController self)
    {
    return LocalPortIdInGroup(self) * 2;
    }

static uint32 DisparityForceMask(AtSerdesController self)
    {
    return cAf6_Force_Running_Disparity_Error_Fdisp0_pid0_Mask << DisparityForceShift(self);
    }

static eAtRet DisparityForce(AtSerdesController self, eBool forced)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_Force_Running_Disparity_Error_Base);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 disparityForceMask = DisparityForceMask(self);
    uint32 disparityForceShift = DisparityForceShift(self);
    mRegFieldSet(regVal, disparityForce, forced ? 3 : 0);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eBool DisparityIsForced(AtSerdesController self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_Force_Running_Disparity_Error_Base);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 disparityForceMask = DisparityForceMask(self);
    uint32 disparityForceShift = DisparityForceShift(self);
    uint32 mode = mRegField(regVal, disparityForce);
    return (mode == 3) ? cAtTrue : cAtFalse;
    }

static eBool CanResetWhenPowerDown(AtSerdesController self)
    {
    /* Reset will be timeout when SERDES is power down */
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanControlTimingMode(AtSerdesController self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x0, 0x0);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader(self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 RXCdrLockToReferenceLock2refShift(AtSerdesController self)
    {
    return LocalPortIdInGroup(self);
    }

static uint32 RXCdrLockToReferenceLock2refMask(AtSerdesController self)
    {
    return cAf6_RX_CDR_Lock_to_Reference_lock2ref_pid0_Mask << RXCdrLockToReferenceLock2refShift(self);
    }

static eBool TimingModeIsSupported(AtSerdesController self, eAtSerdesTimingMode timingMode)
    {
    AtUnused(self);
    if ((timingMode == cAtSerdesTimingModeLockToData) ||
        (timingMode == cAtSerdesTimingModeLockToRef))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet TimingModeHwSet(AtSerdesController self, eAtSerdesTimingMode timingMode)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_RX_CDR_Lock_to_Reference_Base);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 timingModeMask = RXCdrLockToReferenceLock2refMask(self);
    uint32 timingModeShift = RXCdrLockToReferenceLock2refShift(self);
    uint32 hwMode = (timingMode == cAtSerdesTimingModeLockToRef) ? 1 : 0;

    mRegFieldSet(regVal, timingMode, hwMode);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);

    return cAtOk;
    }

static eAtRet TimingModeSet(AtSerdesController self, eAtSerdesTimingMode timingMode)
    {
    eAtRet ret;

    if (!TimingModeIsSupported(self, timingMode))
        return cAtErrorModeNotSupport;

    ret = TimingModeHwSet(self, timingMode);
    if (ret == cAtOk)
        mThis(self)->timingMode = timingMode;

    return ret;
    }

static eAtSerdesTimingMode TimingModeGet(AtSerdesController self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_RX_CDR_Lock_to_Reference_Base);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 timingModeMask = RXCdrLockToReferenceLock2refMask(self);
    return (regVal & timingModeMask) ? cAtSerdesTimingModeLockToRef : cAtSerdesTimingModeLockToData;
    }

static void GearBoxDebug(AtSerdesController self)
    {
    eBool enabled = GearBoxIsEnabled(self);
    AtPrintc(cSevNormal, "    * Async GearBox (0x%08x[%d]): %s\r\n", GearBoxAddress(self), cAf6_Async_GearBox_Enable_of_10Ge_mode_async_gearbix_enb_Shift, enabled ? "enabled" : "disabled");
    }

static uint32 LosPpmThresholdShift(AtSerdesController self)
    {
    return LocalPortIdInGroup(self) * 8;
    }

static uint32 LosPpmThresholdMask(AtSerdesController self)
    {
    return cBit7_0 << LosPpmThresholdShift(self);
    }

static uint32 PpmGetWithLocalAddress(AtSerdesController self, uint32 localAddress)
    {
    uint32 regAddr = AddressWithLocalAddress(self, localAddress);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 fieldShift = LosPpmThresholdShift(self);
    uint32 fieldMask = LosPpmThresholdMask(self);
    return mRegField(regVal, field);
    }

static uint32 CurrentPpm(AtSerdesController self)
    {
    return PpmGetWithLocalAddress(self, cAf6Reg_RX_Monitor_PPM_Mon_Value_Base);
    }

static void Debug(AtSerdesController self)
    {
    m_AtSerdesControllerMethods->Debug(self);
    GearBoxDebug(self);
    AtPrintc(cSevNormal, "    * Current PPM: %u (ppm)\r\n", CurrentPpm(self));
    AtPrintc(cSevNormal, "    * RX_CM_TRIM: %s\r\n", RxAfeTrimValue2String(RxAfeTrimGet(self)));
    }

static eBool EthFramedPrbsIsRemoved(Tha6029FaceplateSerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasAutoResetPpmThreshold(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 AutoResetPpmThresholdGet(AtSerdesController self)
    {
    return PpmGetWithLocalAddress(self, cAf6Reg_RX_Monitor_PPM_Ref_Value_Base);
    }

static uint32 AutoResetPpmThresholdMax(AtSerdesController self)
    {
    return LosPpmThresholdMask(self) >> LosPpmThresholdShift(self);
    }

static eAtRet AutoResetPpmThresholdSet(AtSerdesController self, uint32 ppmThreshold)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_RX_Monitor_PPM_Ref_Value_Base);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleEth);
    uint32 fieldShift = LosPpmThresholdShift(self);
    uint32 fieldMask = LosPpmThresholdMask(self);
    mRegFieldSet(regVal, field, ppmThreshold);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleEth);
    return cAtOk;
    }

static eBool HasEyeScanReset(Tha6029SerdesController self)
    {
    Tha60290021SerdesManager manager = (Tha60290021SerdesManager)AtSerdesControllerManagerGet((AtSerdesController)self);
    return Tha60290021SerdesManagerSerdesEyeScanResetIsSupported(manager);
    }

static void OverrideTha6029SerdesController(AtSerdesController self)
    {
    Tha6029SerdesController controller = (Tha6029SerdesController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6029SerdesControllerOverride));

        mMethodOverride(m_Tha6029SerdesControllerOverride, HasEyeScanReset);
        mMethodOverride(m_Tha6029SerdesControllerOverride, EyeScanResetRegAddress);
        }

    mMethodsSet(controller, &m_Tha6029SerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, CanResetWhenPowerDown);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        mMethodOverride(m_AtSerdesControllerOverride, ModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultMode);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, PllChanged);
        mMethodOverride(m_AtSerdesControllerOverride, PowerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, PowerDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerIsDown);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamGet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamValueIsInRange);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackSet);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackGet);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultEqualizerMode);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, DrpObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngine);
        mMethodOverride(m_AtSerdesControllerOverride, Read);
        mMethodOverride(m_AtSerdesControllerOverride, Write);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalPortGet);
        mMethodOverride(m_AtSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, HwIdGet);
        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);
        mMethodOverride(m_AtSerdesControllerOverride, TimingModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, TimingModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, CanControlTimingMode);
        mMethodOverride(m_AtSerdesControllerOverride, Debug);
        mMethodOverride(m_AtSerdesControllerOverride, OverSamplingMode);
        mMethodOverride(m_AtSerdesControllerOverride, RxReset);
        mMethodOverride(m_AtSerdesControllerOverride, TxReset);

        /* EyeScan */
        mMethodOverride(m_AtSerdesControllerOverride, SpeedInGbps);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanDrpBaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanReset);

        /* Auto reset on PPM crossing */
        mMethodOverride(m_AtSerdesControllerOverride, HasAutoResetPpmThreshold);
        mMethodOverride(m_AtSerdesControllerOverride, AutoResetPpmThresholdGet);
        mMethodOverride(m_AtSerdesControllerOverride, AutoResetPpmThresholdSet);
        mMethodOverride(m_AtSerdesControllerOverride, AutoResetPpmThresholdMax);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    OverrideTha6029SerdesController(self);
    }

static void MethodsInit(Tha6029FaceplateSerdesController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, UseDifferentRemoteLoopbackFor10G);
        mMethodOverride(m_methods, RawPrbsEngineObjectCreate);
        mMethodOverride(m_methods, PmaFarEndLoopbackSupported);
        mMethodOverride(m_methods, EthFramedPrbsIsRemoved);
        mMethodOverride(m_methods, Stm1SamplingFromStm4);
        mMethodOverride(m_methods, StartVersionShouldNotTouchAfeTrim);
        mMethodOverride(m_methods, HasNewParams);
        mMethodOverride(m_methods, DataMaskOnResetConfigurable);
        mMethodOverride(m_methods, PrbsHwSerdesModeSet);
        mMethodOverride(m_methods, XfiPrbsEngineObjectCreate);
        mMethodOverride(m_methods, ShouldChangeGearBoxFor10GLoopback);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029FaceplateSerdesController);
    }

AtSerdesController Tha6029FaceplateSerdesControllerObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesControllerObjectInit(self, physicalPort, serdesId, manager) == NULL)
       return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha6029FaceplateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6029FaceplateSerdesControllerObjectInit(newModule, manager, physicalPort, serdesId);
    }

eAtModulePrbsRet Tha60290021FacePlateSerdesControllerPrbsHwModeSet(AtSerdesController serdes, uint32 mode)
    {
    if (serdes)
        return mMethodsGet(mThis(serdes))->PrbsHwSerdesModeSet(mThis(serdes), mode);
    return cAtErrorNullPointer;
    }

uint32 Tha60290021FacePlateSerdesControllerPrbsHwModeGet(AtSerdesController serdes)
    {
    if (serdes)
        return PrbsHwModeGet(serdes);
    return cInvalidUint32;
    }

uint32 Tha60290021FacePlateSerdesControllerPrbsDefaultMode(AtSerdesController self)
    {
    if (self)
        return PrbsDefaultMode(self);
    return 0;
    }

eTha6029FaceplateSerdesHwMode Tha6029FaceplateSerdesControllerHwModeGet(AtSerdesController self)
    {
    if (self)
        return HwModeGet(self);
    return cTha6029FaceplateSerdesHwModeInvalid;
    }

eAtRet Tha6029FaceplateSerdesControllerDisparityForce(AtSerdesController self, eBool forced)
    {
    if (self)
        return DisparityForce(self, forced);
    return cAtErrorNullPointer;
    }

eBool Tha6029FaceplateSerdesControllerDisparityIsForced(AtSerdesController self)
    {
    if (self)
        return DisparityIsForced(self);
    return cAtFalse;
    }

uint32 Tha6029FaceplateSerdesControllerLocalPortIdInGroup(AtSerdesController self)
    {
    if (self)
        return LocalPortIdInGroup(self);
    return cInvalidUint32;
    }

uint32 Tha6029FaceplateSerdesControllerAddressWithLocalAddress(AtSerdesController self, uint32 localAddress)
    {
    if (self)
        return AddressWithLocalAddress(self, localAddress);
    return cInvalidUint32;
    }

eBool Tha6029FaceplateSerdesPllIsLockedWithSerdesMode(AtSerdesController self)
    {
    if (self)
        return PllIsLockedWithSerdesMode(self);
    return cAtFalse;
    }

eBool Tha6029FaceplateSerdesPllChangedWithSerdesMode(AtSerdesController self)
    {
    if (self)
        return PllChangedWithSerdesMode(self);
    return cAtFalse;
    }

uint32 Tha6029FaceplateSerdesGearBoxEnable(AtSerdesController self, eBool enable)
    {
    return GearBoxEnable(self, enable);
    }

void Tha6029FaceplateSerdesXfiPrbsEngineSet(AtSerdesController self, AtPrbsEngine prbs)
    {
    mThis(self)->xfiSerdesPrbs = prbs;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PHYSICAL
 * 
 * File        : Tha6029SerdesController.h
 * 
 * Created Date: Jun 26, 2018
 *
 * Description : 6029xx SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SERDESCONTROLLER_H_
#define _THA6029SERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha6029SerdesTuner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SerdesController *Tha6029SerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha6029SerdesTuner Tha6029SerdesControllerSerdesTunerGet(Tha6029SerdesController self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SERDESCONTROLLER_H_ */


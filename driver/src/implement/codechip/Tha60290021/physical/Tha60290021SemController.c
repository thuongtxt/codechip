/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60290021SemController.c
 *
 * Created Date: Apr 27, 2017
 *
 * Description : SEM controller implementation for the product 60290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290021SemControllerInternal.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "Tha60290021SemReg.h"
#include "Tha60290021SemController.h"

/*--------------------------- Define -----------------------------------------*/
#define cTha_sem_cur_heartbeat_Mask    cBit24
#define cTha_sem_cur_heartbeat_Shift   24
#define cTha_sem_cur_heartbeat1_Mask   cBit25
#define cTha_sem_cur_heartbeat1_Shift  25
#define cTha_sem_cur_heartbeat2_Mask   cBit26
#define cTha_sem_cur_heartbeat2_Shift  26

#define cTha_sem_cur_act_Mask          cBit16
#define cTha_sem_cur_act_Shift         16
#define cTha_sem_cur_act1_Mask         cBit17
#define cTha_sem_cur_act1_Shift        17
#define cTha_sem_cur_act2_Mask         cBit18
#define cTha_sem_cur_act2_Shift        18


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSemControllerMethods m_ThaSemControllerOverride;
static tAtSemControllerMethods  m_AtSemControllerOverride;

/* Super implementations */
static const tAtSemControllerMethods  *m_AtSemControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaSemController, _sem_cur_heartbeat_)
mDefineMaskShift(ThaSemController, _sem_cur_act_)

static uint32 DefaultOffset(ThaSemController self)
    {
    AtUnused(self);
    return 0xF40000;
    }

static uint8 NumSlr(AtSemController self)
    {
    AtUnused(self);
    return 3;
    }

static uint32 FrameSize(AtSemController self)
    {
    AtUnused(self);
    return 93;/* 101 word for K7Series, 123 for ulstrascale, 93 for ultrascale+, 81 for virtex6 */
    }

static uint32 MaxLfa(AtSemController self)
    {
    AtUnused(self);
    return 0x0000DD19; /* XCVU9P: 18 bit LFA */
    }

static uint32 SupportedAlarms(AtSemController self)
    {
    AtUnused(self);
    return cAtSemControllerAlarmActive
         | cAtSemControllerAlarmInitialization
         | cAtSemControllerAlarmObservation
         | cAtSemControllerAlarmCorrection
         | cAtSemControllerAlarmClassification
         | cAtSemControllerAlarmInjection
         | cAtSemControllerAlarmIdle
         | cAtSemControllerAlarmFatalError
         | cAtSemControllerAlarmErrorEssential
         | cAtSemControllerAlarmErrorCorrectable
         | cAtSemControllerAlarmErrorNoncorrectable
         | cAtSemControllerAlarmHeartbeat
         | cAtSemControllerAlarmHeartbeatSlr1
         | cAtSemControllerAlarmHeartbeatSlr2
         | cAtSemControllerAlarmActiveSlr1
         | cAtSemControllerAlarmActiveSlr2;
    }

static uint32 HeartBeatMaxTimeInUs(ThaSemController self)
    {
    AtUnused(self);
    return 1000000; /* 1s */
    }

static eAtRet AddressCommandString(ThaSemController self, char *addressString, uint32 numChar, uint32 lfa, uint32 dwordIdxInLfa, uint32 bitIdxInDword)
    {
    char token[16];
    AtUart uart = AtSemControllerUartGet((AtSemController)self);
    uint32 value32bit = 0;

    AtUnused(dwordIdxInLfa);
    AtUnused(bitIdxInDword);
    /* Build command */
    if (numChar < (sizeof(token) + 2))
        return cAtErrorInvlParm;

    AtOsalMemInit(addressString, 0, numChar);
    AtStrcat(addressString, "C00");

    /* 1100 0000 0000 ss11 1111 1111 1111 1111 wwww wwwb bbbb
     * ss is Hardware slr number (2-bit). Valid range: 0..2
     * Linear frame address (17-bit). Valid range: 0..Max Frame - 2
     * Word address (7-bit). Valid range: 0..122
     * Bit address (5-bit). Valid range: 0..31 */
    AtOsalMemInit(token, 0, sizeof(token));
    mFieldIns(&value32bit, cBit4_0, 0, 0);
    mFieldIns(&value32bit, cBit11_5, 5, 46);
    mFieldIns(&value32bit, cBit29_12, 12, lfa);
    mFieldIns(&value32bit, cBit31_30, 30, AtUartSuperLogicRegionIdGet(uart));
    AtSprintf(token, "%08X", value32bit);
    AtStrcat(addressString, token);

    return cAtOk;
    }

static const char* DeviceDescription(AtSemController self)
    {
    AtUnused(self);
    return "XCVU9P";
    }

static void OverrideThaSemController(AtSemController self)
    {
    ThaSemController sem = (ThaSemController)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSemControllerOverride, mMethodsGet(sem), sizeof(m_ThaSemControllerOverride));

        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_heartbeat_)
        mBitFieldOverride(ThaSemController, m_ThaSemControllerOverride, _sem_cur_act_)
        mMethodOverride(m_ThaSemControllerOverride, DefaultOffset);
        mMethodOverride(m_ThaSemControllerOverride, HeartBeatMaxTimeInUs);
        mMethodOverride(m_ThaSemControllerOverride, AddressCommandString);
        }

    mMethodsSet(sem, &m_ThaSemControllerOverride);
    }

static void OverrideAtSemController(AtSemController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSemControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSemControllerOverride, m_AtSemControllerMethods, sizeof(m_AtSemControllerOverride));

        mMethodOverride(m_AtSemControllerOverride, SupportedAlarms);
        mMethodOverride(m_AtSemControllerOverride, FrameSize);
        mMethodOverride(m_AtSemControllerOverride, MaxLfa);
        mMethodOverride(m_AtSemControllerOverride, NumSlr);
        mMethodOverride(m_AtSemControllerOverride, DeviceDescription);
        }

    mMethodsSet(self, &m_AtSemControllerOverride);
    }

static void Override(AtSemController self)
    {
    OverrideThaSemController(self);
    OverrideAtSemController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SemController);
    }

AtSemController Tha60290021SemControllerObjectInit(AtSemController self, AtDevice device, uint32 semId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011SemControllerObjectInit(self, device, semId) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSemController Tha60290021SemControllerNew(AtDevice device, uint32 semId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSemController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (controller == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021SemControllerObjectInit(controller, device, semId);
    }

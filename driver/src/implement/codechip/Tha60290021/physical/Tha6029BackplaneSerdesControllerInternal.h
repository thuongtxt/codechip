/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEVICE
 * 
 * File        : Tha6020SerdesManagerInternal.h
 * 
 * Created Date: Jul 11, 2016
 *
 * Description : Serdes Management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029BACKPLANESERDESCONTROLLERINTERNAL_H_
#define _THA6029BACKPLANESERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../../include/eth/AtModuleEth.h"
#include "../../../../generic/man/AtModuleInternal.h"
#include "Tha60290021SerdesManagerInternal.h"
#include "Tha6029FaceplateSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029BackplaneSerdesController * Tha6029BackplaneSerdesController;

typedef struct tTha6029BackplaneSerdesController
    {
    tTha6029SerdesController super;
    }tTha6029BackplaneSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha6029BackplaneBaseAddress(Tha6029BackplaneSerdesController self);
AtSerdesController Tha60290011BacklaneSerdesLaneNew(AtSerdesManager manager, AtSerdesController backplane, AtChannel physicalPort, uint32 serdesId);
AtSerdesController Tha6029BackplaneSerdesControllerObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029BACKPLANESERDESCONTROLLERINTERNAL_H_ */

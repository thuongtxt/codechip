/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PHYSICAL
 *
 * File        : Tha60290021PowerSupplySensor.c
 *
 * Created Date: Oct 4, 2016
 *
 * Description : Tha60290021PowerSupplySensor implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/Tha60290021DeviceReg.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "Tha60290021PowerSupplySensorInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSensorMethods             m_AtSensorOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHal Hal(AtSensor self)
    {
    return AtDeviceIpCoreHalGet(AtSensorDeviceGet(self), 0);
    }

static uint32 AlarmHistoryRead2Clear(AtSensor self, eBool r2c)
    {
    uint32 regAddr = cAf6Reg_i_sticky0_Base + cTopBaseAddress;
    uint32 regVal = AtHalRead(Hal(self), regAddr);
    uint32 alarmHistoryMask = 0;

    if (regVal & cAf6_i_sticky0_vbram_alarm_out_Mask)
        alarmHistoryMask |= cAtPowerSupplySensorAlarmOfVccBram;
    if (regVal & cAf6_i_sticky0_vccaux_alarm_out_Mask)
        alarmHistoryMask |= cAtPowerSupplySensorAlarmOfVccAux;
    if (regVal & cAf6_i_sticky0_vccint_alarm_out_Mask)
        alarmHistoryMask |= cAtPowerSupplySensorAlarmOfVccInt;

    if (r2c)
        {
        uint32 cleanedMask = cAf6_i_sticky0_vbram_alarm_out_Mask | cAf6_i_sticky0_vccaux_alarm_out_Mask | cAf6_i_sticky0_vccint_alarm_out_Mask;
        AtHalWrite(Hal(self), regAddr, cleanedMask);
        }

    return alarmHistoryMask;
    }

static uint32 AlarmHistoryGet(AtSensor self)
    {
    return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 AlarmHistoryClear(AtSensor self)
    {
    return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static void OverrideAtSensor(AtPowerSupplySensor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSensorOverride, mMethodsGet((AtSensor)self), sizeof(m_AtSensorOverride));

        mMethodOverride(m_AtSensorOverride, AlarmHistoryGet);
        mMethodOverride(m_AtSensorOverride, AlarmHistoryClear);
        }

    mMethodsSet(((AtSensor)self), &m_AtSensorOverride);
    }

static void Override(AtPowerSupplySensor self)
    {
    OverrideAtSensor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PowerSupplySensor);
    }

AtPowerSupplySensor Tha60290021PowerSupplySensorObjectInit(AtPowerSupplySensor self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPowerSupplySensorObjectInit(self, device) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPowerSupplySensor Tha60290021PowerSupplySensorNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPowerSupplySensor newSensor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSensor == NULL)
        return NULL;

    return Tha60290021PowerSupplySensorObjectInit(newSensor, device);
    }

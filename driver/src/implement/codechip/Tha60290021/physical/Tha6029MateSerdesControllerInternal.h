/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
<<<<<<< HEAD
 * Module      : physical
=======
 * Module      : Physical
>>>>>>> origin/product/anna_sdk.2.3_eyescan
 * 
 * File        : Tha6029MateSerdesControllerInternal.h
 * 
 * Created Date: Jan 8, 2019
 *
 * Description : Mate SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029MATESERDESCONTROLLERINTERNAL_H_
#define _THA6029MATESERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha6029SerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029MateSerdesController
    {
    tTha6029SerdesController super;
    }tTha6029MateSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha6029MateSerdesControllerObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029MATESERDESCONTROLLERINTERNAL_H_ */


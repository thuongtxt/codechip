/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha6029SerdesTuner.h
 * 
 * Created Date: Sep 25, 2016
 *
 * Description : SERDES tuning common logic
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SERDESTUNING_H_
#define _THA6029SERDESTUNING_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SerdesTuner * Tha6029SerdesTuner;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha6029SerdesTuner Tha6029SerdesTunerNew(AtSerdesController serdes);
Tha6029SerdesTuner Tha6029SerdesTunerSgmiiNew(AtSerdesController serdes, uint32 groupId, uint32 localPortId);
Tha6029SerdesTuner Tha60290021SerdesTunerNew(AtSerdesController serdes, uint32 groupId, uint32 localPortId);

AtDrp Tha6029SerdesTunerDrpCreate(Tha6029SerdesTuner self);
AtDrp Tha6029SerdesTunerDrpCreateWithBaseAddress(Tha6029SerdesTuner self, uint32 drpBaseAddress);

/* Common */
AtSerdesController Tha6029SerdesTunerSerdesGet(Tha6029SerdesTuner self);
eAtRet Tha6029SerdesTunerReset(Tha6029SerdesTuner self);
eAtRet Tha6029SerdesTunerRxReset(Tha6029SerdesTuner self);
eAtRet Tha6029SerdesTunerTxReset(Tha6029SerdesTuner self);

/* Loopback */
eBool Tha6029SerdesTunerCanLoopback(Tha6029SerdesTuner self, eAtLoopbackMode loopbackMode, eBool enable);
eAtRet Tha6029SerdesTunerLoopbackSet(Tha6029SerdesTuner self, eAtLoopbackMode loopback);
eAtLoopbackMode Tha6029SerdesTunerLoopbackGet(Tha6029SerdesTuner self);

/* Equalizer */
eAtRet Tha6029SerdesTunerEqualizerModeSet(Tha6029SerdesTuner self, eAtSerdesEqualizerMode mode);
eAtSerdesEqualizerMode Tha6029SerdesTunerEqualizerModeGet(Tha6029SerdesTuner self);
eBool Tha6029SerdesTunerEqualizerModeIsSupported(Tha6029SerdesTuner self, eAtSerdesEqualizerMode mode);
eBool Tha6029SerdesTunerEqualizerCanControl(Tha6029SerdesTuner self);

/* Physical parameters */
eAtRet Tha6029SerdesTunerPhysicalParamSet(Tha6029SerdesTuner self, eAtSerdesParam param, uint32 value);
uint32 Tha6029SerdesTunerPhysicalParamGet(Tha6029SerdesTuner self, eAtSerdesParam param);
eBool Tha6029SerdesTunerPhysicalParamIsSupported(Tha6029SerdesTuner self, eAtSerdesParam param);
eBool Tha6029SerdesTunerPhysicalParamValueIsInRange(Tha6029SerdesTuner self, eAtSerdesParam param, uint32 value);

/* PLL */
eBool Tha6029SerdesTunerPllIsLocked(Tha6029SerdesTuner self);
eBool Tha6029SerdesTunerPllChanged(Tha6029SerdesTuner self);

/* Power control */
eBool Tha6029SerdesTunerPowerCanControl(Tha6029SerdesTuner self);
eAtRet Tha6029SerdesTunerPowerDown(Tha6029SerdesTuner self, eBool powerDown);
eBool Tha6029SerdesTunerPowerIsDown(Tha6029SerdesTuner self);

/* Reset */
eAtRet Tha6029SerdesTunerEyeScanReset(Tha6029SerdesTuner self, uint32 laneId);

/* Raw PRBS */
eAtRet Tha6029SerdesTunerRawPrbsModeSet(Tha6029SerdesTuner self, eAtPrbsMode mode);
eAtPrbsMode Tha6029SerdesTunerRawPrbsModeGet(Tha6029SerdesTuner self);
eBool Tha6029SerdesTunerRawPrbsModeIsSupported(Tha6029SerdesTuner self, eAtPrbsMode mode);
eAtRet Tha6029SerdesTunerRawPrbsEnable(Tha6029SerdesTuner self, eBool enabled);
eBool Tha6029SerdesTunerRawPrbsIsEnabled(Tha6029SerdesTuner self);
eAtRet Tha6029SerdesTunerRawPrbsErrorForce(Tha6029SerdesTuner self, eBool forced);
eBool Tha6029SerdesTunerRawPrbsErrorIsForced(Tha6029SerdesTuner self);
uint32 Tha6029SerdesTunerRawPrbsErrorCounterRead2Clear(Tha6029SerdesTuner self, eBool r2c);
eAtPrbsEngineAlarmType Tha6029SerdesTunerRawPrbsAlarmHistoryRead2Clear(Tha6029SerdesTuner self, eBool r2c);
eAtPrbsEngineAlarmType Tha6029SerdesTunerRawPrbsAlarmGet(Tha6029SerdesTuner self);

uint32 Tha6029SerdesTunerRegisterWithLocalAddress(Tha6029SerdesTuner self, uint32 localAddress);
uint32 Tha6029SerdesTunerLocalId(Tha6029SerdesTuner self);
uint32 Tha6029SerdesTunerRead(Tha6029SerdesTuner self, uint32 address, eAtModule moduleId);
void Tha6029SerdesTunerWrite(Tha6029SerdesTuner self, uint32 address, uint32 value, eAtModule moduleId);
eBool Tha6029SerdesTunerIsSimulated(Tha6029SerdesTuner self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SERDESTUNING_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha6029MateSerdesController.h
 * 
 * Created Date: Oct 7, 2016
 *
 * Description : MATE SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029MATESERDESCONTROLLER_H_
#define _THA6029MATESERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPhysicalClasses.h"
#include "Tha6029SerdesTuner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
Tha6029SerdesTuner Tha6029MateSerdesTuner(AtSerdesController self);
uint8 Tha6029MateSerdesControllerLocalPortIdInGroup(AtSerdesController self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029MATESERDESCONTROLLER_H_ */


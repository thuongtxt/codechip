/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEVICE
 * 
 * File        : Tha6020SerdesManagerInternal.h
 * 
 * Created Date: Jul 11, 2016
 *
 * Description : Serdes Management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021SERDESMANAGERINTERNAL_H_
#define _THA60290021SERDESMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha6029SerdesManagerInternal.h"
#include "Tha60290021SerdesManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021SerdesManagerMethods
    {
    eBool (*PortsCanBeReduced)(Tha60290021SerdesManager self);
    AtSerdesController (*FaceplateSerdesControllerObjectCreate)(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId);
    AtSerdesController (*SgmiiSerdesControllerObjectCreate)(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId);
    AtSerdesController (*MateSerdesControllerObjectCreate)(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId);
    AtEyeScanController (*EyeScanControllerCreate)(Tha60290021SerdesManager self, AtSerdesController serdes, uint32 drpBaseAddress);
    eBool (*SpareSgmiiSerdesAreRemoved)(Tha60290021SerdesManager self);
    uint32 (*NumFaceplateSerdes)(AtSerdesManager self);
    uint32 (*NumMateSerdes)(AtSerdesManager self);
    uint32 (*NumSgmiiSerdes)(AtSerdesManager self);
    uint32 (*NumBackplaneSerdes)(AtSerdesManager self);
    uint8 (*FaceplateSerdesStartId)(AtSerdesManager self);
    uint8 (*FaceplateSerdesEndId)(AtSerdesManager self);
    uint8 (*MateSerdesStartId)(AtSerdesManager self);
    uint8 (*MateSerdesEndId)(AtSerdesManager self);
    uint8 (*SgmiiSerdesStartId)(AtSerdesManager self);
    uint8 (*SgmiiSerdesEndId)(AtSerdesManager self);
    uint8 (*BackplaneSerdesStartId)(AtSerdesManager self);
    uint8 (*BackplaneSerdesEndId)(AtSerdesManager self);
    uint32 (*TenG_FaceplateSerdes_XfiDiagnosticBaseAddress)(AtSerdesManager self, AtSerdesController serdes);
    eBool (*SerdesEyeScanResetIsSupported)(Tha60290021SerdesManager self);
    }tTha60290021SerdesManagerMethods;

typedef struct tTha60290021SerdesManager
    {
    tTha6029SerdesManager super;
    const tTha60290021SerdesManagerMethods *methods;
    }tTha60290021SerdesManager;
	
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager Tha60290021SerdesManagerObjectInit(AtSerdesManager self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021SERDESMANAGERINTERNAL_H_ */

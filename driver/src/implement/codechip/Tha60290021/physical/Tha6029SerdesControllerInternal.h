/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEVICE
 * 
 * File        : Tha6029SerdesControllerInternal.h
 * 
 * Created Date: Jul 11, 2016
 *
 * Description : Serdes Management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SERDESCONTROLLERINTERNAL_H_
#define _THA6029SERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "Tha6029SerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SerdesControllerMethods
    {
    Tha6029SerdesTuner (*SerdesTunerObjectCreate)(Tha6029SerdesController self);
    eBool (*HasEyeScanReset)(Tha6029SerdesController self);
    uint32 (*EyeScanResetRegAddress)(Tha6029SerdesController self);
    }tTha6029SerdesControllerMethods;

typedef struct tTha6029SerdesController
    {
    tAtSerdesController super;
    const tTha6029SerdesControllerMethods *methods;

    /* Private data */
    Tha6029SerdesTuner tuner;
    }tTha6029SerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha6029SerdesControllerObjectInit(AtSerdesController self, AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SERDESCONTROLLERINTERNAL_H_ */

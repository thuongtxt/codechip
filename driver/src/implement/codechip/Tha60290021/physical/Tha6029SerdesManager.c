/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device
 *
 * File        : Tha6029SerdesManager.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : Serdes Management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6029SerdesManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6029SerdesManagerMethods m_methods;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet FaceplateGroupSet(Tha6029SerdesManager self, uint8 groupId)
    {
    AtUnused(self);
    AtUnused(groupId);
    return cAtErrorModeNotSupport;
    }

static uint8 FaceplateGroupGet(Tha6029SerdesManager self)
    {
    AtUnused(self);
    return cInvalidUint8;
    }

static void MethodsInit(Tha6029SerdesManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, FaceplateGroupSet);
        mMethodOverride(m_methods, FaceplateGroupGet);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SerdesManager);
    }

AtSerdesManager Tha6029SerdesManagerObjectInit(AtSerdesManager self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSerdesManagerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit((Tha6029SerdesManager)self);
    m_methodsInit = 1;

    return self;
    }

eAtRet Tha6029SerdesManagerFaceplateGroupSet(Tha6029SerdesManager self, uint8 groupId)
    {
    if (self)
        return mMethodsGet(self)->FaceplateGroupSet(self, groupId);
    return cAtErrorNullPointer;
    }

uint8 Tha6029SerdesManagerFaceplateGroupGet(Tha6029SerdesManager self)
    {
    if (self)
        return mMethodsGet(self)->FaceplateGroupGet(self);
    return cInvalidUint8;
    }


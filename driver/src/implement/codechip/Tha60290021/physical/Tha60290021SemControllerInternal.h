/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60290021SemControllerInternal.h
 * 
 * Created Date: Aug 31, 2018
 *
 * Description : 60290021 SEM controller internal data
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021SEMCONTROLLERINTERNAL_H_
#define _THA60290021SEMCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/physical/Tha60210011SemControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021SemController
    {
    tTha60210011SemController super;
    }tTha60290021SemController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSemController Tha60290021SemControllerObjectInit(AtSemController self, AtDevice device, uint32 semId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021SEMCONTROLLERINTERNAL_H_ */


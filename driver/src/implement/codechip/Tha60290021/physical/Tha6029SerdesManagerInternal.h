/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha6029SerdesManagerInternal.h
 * 
 * Created Date: Oct 1, 2016
 *
 * Description : Abstract SERDES manager for 6029xxxx products
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SERDESMANAGERINTERNAL_H_
#define _THA6029SERDESMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/physical/AtSerdesManagerInternal.h"
#include "Tha6029SerdesManager.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SerdesManagerMethods
    {
    eAtRet (*FaceplateGroupSet)(Tha6029SerdesManager self, uint8 groupId);
    uint8 (*FaceplateGroupGet)(Tha6029SerdesManager self);
    }tTha6029SerdesManagerMethods;

typedef struct tTha6029SerdesManager
    {
    tAtSerdesManager super;
    const tTha6029SerdesManagerMethods *methods;
    }tTha6029SerdesManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager Tha6029SerdesManagerObjectInit(AtSerdesManager self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SERDESMANAGERINTERNAL_H_ */


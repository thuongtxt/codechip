/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha6029SgmiiSerdesController.c
 *
 * Created Date: Sep 22, 2016
 *
 * Description : Tha6029 Sgmii Serdes Controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "../../Tha60290011/prbs/Tha60290011SgmiiSerdesPrbsEngine.h"
#include "../../Tha60210021/man/Tha6021DebugPrint.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "../eth/Tha6029SerdesSgmiiDiagReg.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../man/Tha60290021Device.h"
#include "../prbs/Tha6029SerdesPrbsEngine.h"
#include "Tha60290021SerdesManager.h"
#include "Tha60290021SerdesManagerInternal.h"
#include "Tha6029Physical.h"
#include "Tha6029SgmiiSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6029SgmiiSerdesController *)self)

#define cMdioRegRegister(page, address) page, address

#define cMdioRegControlRegister           cMdioRegRegister(1, 0)
#define cMdioRegControlResetMask          cBit15
#define cMdioRegControlResetShift         15
#define cMdioRegControlIsolatedMask       cBit10
#define cMdioRegControlIsolatedShift      10
#define cMdioRegControlLocalLoopbackMask  cBit14
#define cMdioRegControlLocalLoopbackShift 14

#define cMdioRegStatusRegister             cMdioRegRegister(1, 1)
#define cMdioRegStatusLinkStatusMask       cBit2

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6029SgmiiSerdesControllerMethods m_methods;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods           = NULL;
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(AtSerdesController self)
    {
    return AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self));
    }

static AtModuleEth ModuleEth(AtSerdesController self)
    {
    return (AtModuleEth)AtDeviceModuleGet(Device(self), cAtModuleEth);
    }

static uint32 HwIdGet(AtSerdesController self)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    return Tha60290021SerdesManagerSgmiiSerdesLocalId(manager, AtSerdesControllerIdGet(self));
    }

static uint8 LocalIdGet(AtSerdesController self)
    {
    return (uint8) AtSerdesControllerHwIdGet(self);
    }

static uint32 BaseAddress(AtSerdesController self)
    {
    return Tha60290021SerdesManagerSgmiiDiagnosticBaseAddress(self);
    }

static eAtSerdesMode SupportedMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeEth1G;
    }

static float SpeedInGbps(AtSerdesController self)
    {
    AtUnused(self);
    return (float)(1.25);
    }

static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    return (mode == SupportedMode(self)) ? cAtTrue : cAtFalse;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    return (mode == SupportedMode(self)) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    return SupportedMode(self);
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    AtSerdesController serdes = (AtSerdesController)self;
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    Tha60290021SerdesManagerSgmiiDescription(AtSerdesControllerManagerGet(serdes), serdesId, description, sizeof(description));
    return description;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->tuner);
    mThis(self)->tuner = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha6029SgmiiSerdesController* object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(tuner);
    mEncodeUInt(mdioLocalLoopbackEnabled);
    }

static ThaVersionReader VersionReader(AtSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet(self);
    return ThaDeviceVersionReader(device);
    }

static eBool UseEngineOfPdhProduct(Tha6029SgmiiSerdesController self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x7, 0x0);
    uint32 version = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtSerdesController)self));
    return (version >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool HasDiagnosticLogic(AtSerdesController self)
    {
    Tha60290021ModuleEth ethModule = (Tha60290021ModuleEth)ModuleEth(self);
    if (ethModule)
    	return Tha60290021ModuleEthHasSgmiiDiagnosticLogic(ethModule);
    return cAtFalse;
    }

static eBool HasFramePrbsEngine(AtSerdesController self)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    uint32 localId = LocalIdGet(self);

    /* This is the second SGMII SERDES controller used for DCC and Kbyte traffic.
     * Other SGMIIs are not used */
    if (localId == Tha60290021SerdesManagerSgmiiSerdesLocalDccId(manager))
        return cAtTrue;

    return cAtFalse;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    eBool onlyHasFramePrbs;

    onlyHasFramePrbs = HasDiagnosticLogic(self) ? cAtFalse : cAtTrue;
    if (onlyHasFramePrbs)
        return HasFramePrbsEngine(self) ? Tha60290021SgmiiFramePrbsEngineNew(self) : NULL;

    if (mMethodsGet(mThis(self))->UseEngineOfPdhProduct(mThis(self)))
        {
        uint32 localId = AtSerdesControllerHwIdGet(self);
        AtPrbsEngine engine = Tha60290011SgmiiSerdesPrbsEngineWithHwIdNew(self, localId);
        Tha60290011SgmiiSerdesPrbsEngineGatingLogicEnabled(engine, cAtTrue);
        return engine;
        }

    return Tha6029SgmiiSerdesPrbsEngineNew(self);
    }

static AtChannel PhysicalPortGet(AtSerdesController self)
    {
    uint8 localId = LocalIdGet(self);
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    AtModuleEth ethModule = ModuleEth(self);
    /*
     * SERDES 0: KByte     --> unused and better mapped to Ethernet port 18 which is reserved
     * SERDES 1: KByte/DCC --> assigned to Ethernet port 17 committed in PG document
     * --> ETH Ports are swapped compare with beginning. This is bad thing. But
     * is better to keep this to avoid impact to application
     */
    if (ethModule)
    	{
		if (localId == Tha60290021SerdesManagerSgmiiSerdesLocalKByteId(manager)) /* ID 0 */
			return (AtChannel)Tha60290021ModuleEthSgmiiDccPortGet(ethModule);         /* Port 18 */

		if (localId == Tha60290021SerdesManagerSgmiiSerdesLocalDccId(manager))
			return (AtChannel)Tha60290021ModuleEthSgmiiDccKbytePortGet(ethModule);

		return (AtChannel)Tha60290021ModuleEthSgmiiPortGet(ethModule, localId);
    	}
    return NULL;
    }

static uint8 TunerGroupId(AtSerdesController self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 TunerLocalPortId(AtSerdesController self)
    {
    uint32 localId = LocalIdGet(self);
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);

    if (localId == Tha60290021SerdesManagerSgmiiSerdesLocalKByteId(manager))
        return 0;
    if (localId == Tha60290021SerdesManagerSgmiiSerdesLocalDccId(manager))
        return 0;

    return (uint8)Tha60290021SerdesManagerSgmiiSerdesSpareLocalId(manager, AtSerdesControllerIdGet(self));
    }

static Tha6029SerdesTuner Tuner(AtSerdesController self)
    {
    if (mThis(self)->tuner == NULL)
        mThis(self)->tuner = Tha6029SerdesTunerSgmiiNew(self, TunerGroupId(self), TunerLocalPortId(self));
    return mThis(self)->tuner;
    }

static AtDrp DrpObjectCreate(AtSerdesController self)
    {
    return Tha6029SerdesTunerDrpCreate(Tuner(self));
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 EyeScanDrpBaseAddress(AtSerdesController self)
    {
    return AtDrpBaseAddress(AtSerdesControllerDrp(self));
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    Tha60290021SerdesManager manager = (Tha60290021SerdesManager)AtSerdesControllerManagerGet(self);
    return Tha60290021SerdesManagerEyeScanControllerCreate(manager, self, drpBaseAddress);
    }

static AtMdio Mdio(AtSerdesController self)
    {
    AtSerdesManager serdesManager = AtSerdesControllerManagerGet(self);
    AtDevice device = AtSerdesManagerDeviceGet(serdesManager);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    if (ethModule)
    	return AtModuleEthSerdesMdio(ethModule, self);
    return NULL;
    }

static eAtRet MdioLocalLoopbackEnable(AtSerdesController self, eBool enabled)
    {
    mThis(self)->mdioLocalLoopbackEnabled = enabled;

    /* Only do when accessible */
    if (AtDeviceAccessible(Device(self)))
        {
        AtMdio mdio = Mdio(self);
        uint32 regVal = AtMdioRead(mdio, cMdioRegControlRegister);
        mRegFieldSet(regVal, cMdioRegControlLocalLoopback, (enabled ? 1 : 0));
        return AtMdioWrite(mdio, cMdioRegControlRegister, regVal);
        }

    return cAtOk;
    }

static eBool MdioLocalLoopbackIsEnabled(AtSerdesController self)
    {
    if (AtDeviceIsSimulated(Device(self)) || AtDeviceInAccessible(Device(self)))
        return mThis(self)->mdioLocalLoopbackEnabled;
    else
        {
        AtMdio mdio = Mdio(self);
        uint32 regVal = AtMdioRead(mdio, cMdioRegControlRegister);
        return (regVal & cMdioRegControlLocalLoopbackMask) ? cAtTrue : cAtFalse;
        }
    }

static eBool ShouldUseMdioLocalLoopback(Tha6029SgmiiSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    return Tha6029SerdesTunerCanLoopback(Tuner(self), loopbackMode, enable);
    }

static eAtRet LoopbackSet(AtSerdesController self, eAtLoopbackMode loopback)
    {
    /* Recent versions need to use MDIO loopback */
    if (mMethodsGet(mThis(self))->ShouldUseMdioLocalLoopback(mThis(self)))
        {
        if (loopback == cAtLoopbackModeLocal)
            return MdioLocalLoopbackEnable(self, cAtTrue);

        /* Need to release MDIO loopback for other mode */
        MdioLocalLoopbackEnable(self, cAtFalse);
        }

    return Tha6029SerdesTunerLoopbackSet(Tuner(self), loopback);
    }

static eAtLoopbackMode LoopbackGet(AtSerdesController self)
    {
    /* MDIO may be in local loopback state */
    if (mMethodsGet(mThis(self))->ShouldUseMdioLocalLoopback(mThis(self)) &&
        MdioLocalLoopbackIsEnabled(self))
        return cAtLoopbackModeLocal;

    /* Just ask the tuner */
    return Tha6029SerdesTunerLoopbackGet(Tuner(self));
    }

static eAtSerdesEqualizerMode DefaultEqualizerMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesEqualizerModeDfe;
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    return Tha6029SerdesTunerEqualizerModeSet(Tuner(self), mode);
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    return Tha6029SerdesTunerEqualizerModeGet(Tuner(self));
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    return Tha6029SerdesTunerEqualizerModeIsSupported(Tuner(self), mode);
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    return Tha6029SerdesTunerEqualizerCanControl(Tuner(self));
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    return Tha6029SerdesTunerPhysicalParamSet(Tuner(self), param, value);
    }

static uint32 PhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    return Tha6029SerdesTunerPhysicalParamGet(Tuner(self), param);
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
    return Tha6029SerdesTunerPhysicalParamIsSupported(Tuner(self), param);
    }

static eBool PhysicalParamValueIsInRange(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    return Tha6029SerdesTunerPhysicalParamValueIsInRange(Tuner(self), param, value);
    }

static eBool PllIsLocked (AtSerdesController self)
    {
    return Tha6029SerdesTunerPllIsLocked(Tuner(self));
    }

static eBool PllChanged (AtSerdesController self)
    {
    return Tha6029SerdesTunerPllChanged(Tuner(self));
    }

static eBool PowerCanControl(AtSerdesController self)
    {
    return Tha6029SerdesTunerPowerCanControl(Tuner(self));
    }

static eAtRet ElectricallyIsolatePHYFromGMII(AtSerdesController self, eBool isolated)
    {
    AtMdio mdio = AtSerdesControllerMdio(self);
    uint32 regValue = AtMdioRead(mdio, cMdioRegControlRegister);
    mRegFieldSet(regValue, cMdioRegControlIsolated, isolated ? 1 : 0);
    return AtMdioWrite(mdio, cMdioRegControlRegister, regValue);
    }

static eAtRet PrbsDisable(AtSerdesController self)
    {
    AtPrbsEngine engine;
    eAtRet ret = cAtOk;

    engine = AtSerdesControllerPrbsEngine(self);
    if (engine == NULL)
        return cAtOk;

    if (!AtPrbsEngineIsEnabled(engine))
        return cAtOk;

    /* Disable and wait a moment for hardware to flush all of frames */
    ret = AtPrbsEngineEnable(engine, cAtFalse);
    if (ret == cAtOk)
        AtOsalUSleep(10000);

    return ret;
    }

static eAtRet PcsReset(AtSerdesController self)
    {
    tAtOsalCurTime curTime, startTime;
    const uint32 cResetTimeoutInMs = 2000;
    uint32 elapse = 0;
    AtMdio mdio = Mdio(self);
    uint32 regVal;

    if (AtSerdesControllerInAccessible(self))
        return cAtOk;

    /* Make reset */
    regVal = AtMdioRead(mdio, cMdioRegControlRegister);
    regVal |= cMdioRegControlResetMask;
    AtMdioWrite(mdio, cMdioRegControlRegister, regVal);

    /* Wait for reset done */
    AtOsalCurTimeGet(&startTime);
    while (elapse < cResetTimeoutInMs)
        {
        regVal = AtMdioRead(mdio, cMdioRegControlRegister);
        if ((regVal & cMdioRegControlResetMask) == 0)
            return cAtOk;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorSerdesResetTimeout;
    }

static eAtRet Reset(AtSerdesController self)
    {
    eAtRet ret = cAtOk;

    ret |= PrbsDisable(self);
    if (HasDiagnosticLogic(self))
    ret |= Tha6029SerdesTunerReset(Tuner(self));
    ret |= PcsReset(self);

    if (!AtDeviceWarmRestoreIsStarted(AtSerdesControllerDeviceGet(self)))
        ret |= ElectricallyIsolatePHYFromGMII(self, cAtFalse);

    return ret;
    }

static eAtRet PowerDown(AtSerdesController self, eBool powerDown)
    {
    eAtRet ret = cAtOk;

    if (powerDown)
        ret |= PrbsDisable(self);

    ret |= Tha6029SerdesTunerPowerDown(Tuner(self), powerDown);

    return ret;
    }

static eBool PowerIsDown(AtSerdesController self)
    {
    return Tha6029SerdesTunerPowerIsDown(Tuner(self));
    }

static eAtRet Init(AtSerdesController self)
    {
    AtPrbsEngine engine = AtSerdesControllerPrbsEngine(self);
    if (engine)
        return AtPrbsEngineInit(engine);
    return cAtOk;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    return (enable) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    return enable;
    }

static eAtRet InterruptMaskSet(AtSerdesController self, uint32 alarmMask, uint32 enableMask)
    {
    AtUnused(self);
    AtUnused(alarmMask);
    AtUnused(enableMask);
    return (alarmMask == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint32 InterruptMaskGet(AtSerdesController self)
    {
    AtUnused(self);
    return 0x0;
    }

static uint32 SupportedInterruptMask(AtSerdesController self)
    {
    AtUnused(self);
    return 0x0;
    }

static eBool IsControllable(AtSerdesController self)
    {
    AtDevice device = AtSerdesControllerDeviceGet(self);
    AtSerdesManager manager;

    if (Tha60290021DeviceIsRunningOnOtherPlatform(device))
        return cAtFalse;

    manager = AtSerdesControllerManagerGet(self);
    return AtSerdesManagerSerdesIsControllable(manager, AtSerdesControllerIdGet(self));
    }

static eAtSerdesLinkStatus LinkStatus(AtSerdesController self)
    {
    AtMdio mdio = Mdio(self);
    uint32 regVal = AtMdioRead(mdio, cMdioRegStatusRegister);
    return (regVal & cMdioRegStatusLinkStatusMask) ? cAtSerdesLinkStatusUp : cAtSerdesLinkStatusDown;
    }

static void DebugPrintRegName(const char* title, uint32 page, uint32 address, uint32 value)
    {
    Tha6021DebugPrintStart(NULL);
    AtPrintc(cSevInfo, "\r\n%s: (%d.%02d = 0x%08x)\r\n", title, page, address, value);
    return;
    }

static void Debug(AtSerdesController self)
    {
    AtMdio mdio = Mdio(self);
    uint32 regVal;
    eBool twoColumnDisplayed = Tha6021DebugPrintTwoCollumnsNeeded();
    eBool autoNegEnabled;

    Tha6021DebugPrintNeedTwoCollumnsEnable(cAtFalse);
    regVal = AtMdioRead(mdio, cMdioRegControlRegister);
    DebugPrintRegName("Control Register", cMdioRegControlRegister, regVal);
    Tha6021DebugPrintErrorBitWithLabel("Reset", regVal, cBit15, "Core Reset", "Normal Operation");
    autoNegEnabled = (regVal & cBit12) ? cAtTrue : cAtFalse;
    Tha6021DebugPrintBitWithLabelAndColor("Auto-Negotiation Enable", regVal, cBit12, "enabled", "disabled", cSevWarning, cSevNormal);
    Tha6021DebugPrintErrorBitWithLabel("Power Down", regVal, cBit11, "Power down", "Normal Operation");
    Tha6021DebugPrintBitWithLabelAndColor("Isolate", regVal, cBit10, "Electrically Isolate PHY from GMII", "Normal Operation", cSevNormal, cSevWarning);
    Tha6021DebugPrintBitWithLabelAndColor("Restart Auto-Negotiation", regVal, cBit9, "Restart Auto-Negotiation Process", "Normal Operation", cSevNormal, cSevWarning);
    Tha6021DebugPrintBitWithLabelAndColor("Unidirectional", regVal, cBit5, "enabled", "disabled", cSevWarning, cSevNormal);
    Tha6021DebugPrintStop(NULL);

    regVal = AtMdioRead(mdio, cMdioRegStatusRegister);
    DebugPrintRegName("Status Register", cMdioRegStatusRegister, regVal);
    if (autoNegEnabled)
        Tha6021DebugPrintBitWithLabelAndColor("Auto-Negotiation Complete", regVal, cBit5, "completed", "not-completed", cSevInfo, cSevWarning);
    Tha6021DebugPrintErrorBitWithLabel("Remote Fault", regVal, cBit4, "detected", "no");
    Tha6021DebugPrintOkBitWithLabel("Link Status", regVal, cBit2, "up", "down");
    Tha6021DebugPrintStop(NULL);

    Tha6021DebugPrintNeedTwoCollumnsEnable(twoColumnDisplayed);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, LinkStatus);
        mMethodOverride(m_AtSerdesControllerOverride, Debug);

        mMethodOverride(m_AtSerdesControllerOverride, InterruptMaskSet);
        mMethodOverride(m_AtSerdesControllerOverride, InterruptMaskGet);
        mMethodOverride(m_AtSerdesControllerOverride, SupportedInterruptMask);

        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);

        mMethodOverride(m_AtSerdesControllerOverride, ModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);

        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalPortGet);

        mMethodOverride(m_AtSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, DrpObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackSet);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackGet);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultEqualizerMode);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerCanControl);

        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamGet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamValueIsInRange);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, PllChanged);

        mMethodOverride(m_AtSerdesControllerOverride, PowerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, PowerDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerIsDown);
        mMethodOverride(m_AtSerdesControllerOverride, HwIdGet);
        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);

        /* EyeScan */
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanDrpBaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, SpeedInGbps);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        mMethodOverride(m_AtSerdesControllerOverride, Mdio);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    }

static void MethodsInit(Tha6029SgmiiSerdesController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, UseEngineOfPdhProduct);
        mMethodOverride(m_methods, ShouldUseMdioLocalLoopback);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SgmiiSerdesController);
    }

AtSerdesController Tha6029SgmiiSerdesControllerObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSerdesControllerObjectInitWithManager(self, physicalPort, serdesId, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha6029SgmiiSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6029SgmiiSerdesControllerObjectInit(newModule, manager, physicalPort, serdesId);
    }

eAtRet Tha6029SgmiiElectricallyIsolatePHYFromGMII(AtSerdesController self, eBool isolated)
    {
    if (self == NULL)
    return cAtErrorNullPointer;

    if (AtSerdesControllerAccessible(self))
        return ElectricallyIsolatePHYFromGMII(self, isolated);

    return cAtOk;
    }

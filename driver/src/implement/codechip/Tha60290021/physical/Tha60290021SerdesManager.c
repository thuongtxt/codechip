/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device
 *
 * File        : Tha60290021SerdesManager.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : Serdes Management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210051/physical/Tha60210051Physical.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../eth/Tha60290021ModuleEth.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../man/Tha60290021Device.h"
#include "Tha60290021SerdesManager.h"
#include "Tha60290021SerdesManagerInternal.h"
#include "Tha6029FaceplateSerdesControllerInternal.h"
#include "Tha6029Physical.h"

/*--------------------------- Define -----------------------------------------*/
#define cSgmiiNumSparePorts         2
#define cSgmiiLocalPortDcc          cSgmiiLocalPortKByte + 1
#define cSgmiiLocalPortSpartStartId cSgmiiLocalPortDcc + 1
#define cSgmiiNumPorts              4

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021SerdesManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021SerdesManagerMethods m_methods;

/* Override */
static tAtSerdesManagerMethods      m_AtSerdesManagerOverride;
static tTha6029SerdesManagerMethods m_Tha6029SerdesManagerOverride;

/* To save super implementation */
static const tAtSerdesManagerMethods     *m_AtSerdesManagerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumFaceplateSerdes(AtSerdesManager self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesManagerDeviceGet(self);

    return Tha60290021DeviceNumFaceplateSerdes(device);
    }

static uint32 NumMateSerdes(AtSerdesManager self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesManagerDeviceGet(self);

    return Tha60290021DeviceNumMateSerdes(device);
    }

static uint32 NumBackplaneSerdes(AtSerdesManager self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesManagerDeviceGet(self);

    return Tha60290021DeviceNumBackplaneSerdes(device);
    }

static uint32 NumSgmiiSerdes(AtSerdesManager self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesManagerDeviceGet(self);

    return Tha60290021DeviceNumSgmiiSerdes(device);
    }

static uint8 FaceplateSerdesStartId(AtSerdesManager self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesManagerDeviceGet(self);

    return Tha60290021DeviceFaceplateSerdesStartId(device);
    }

static uint8 FaceplateSerdesEndId(AtSerdesManager self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesManagerDeviceGet(self);

    return Tha60290021DeviceFaceplateSerdesEndId(device);
    }

static uint8 MateSerdesStartId(AtSerdesManager self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesManagerDeviceGet(self);

    return Tha60290021DeviceMateSerdesStartId(device);
    }

static uint8 MateSerdesEndId(AtSerdesManager self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesManagerDeviceGet(self);

    return Tha60290021DeviceMateSerdesEndId(device);
    }

static uint8 BackplaneSerdesStartId(AtSerdesManager self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesManagerDeviceGet(self);

    return Tha60290021DeviceBackplaneSerdesStartId(device);
    }

static uint8 BackplaneSerdesEndId(AtSerdesManager self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesManagerDeviceGet(self);

    return Tha60290021DeviceBackplaneSerdesEndId(device);
    }

static uint8 SgmiiSerdesStartId(AtSerdesManager self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesManagerDeviceGet(self);

    return Tha60290021DeviceSgmiiSerdesStartId(device);
    }

static uint8 SgmiiSerdesEndId(AtSerdesManager self)
    {
    Tha60290021Device device = (Tha60290021Device)AtSerdesManagerDeviceGet(self);

    return Tha60290021DeviceSgmiiSerdesEndId(device);
    }

static eBool IsFaceplateSerdes(AtSerdesManager self, uint32 serdesId)
    {
    uint8 minId = mMethodsGet(mThis(self))->FaceplateSerdesStartId(self);
    uint8  maxId = mMethodsGet(mThis(self))->FaceplateSerdesEndId(self);

    return ((serdesId >= minId) && (serdesId <= maxId)) ? cAtTrue : cAtFalse;
    }

static eBool IsMateSerdes(AtSerdesManager self, uint32 serdesId)
    {
    uint8 minId = mMethodsGet(mThis(self))->MateSerdesStartId(self);
    uint8  maxId = mMethodsGet(mThis(self))->MateSerdesEndId(self);

    return ((serdesId >= minId) && (serdesId <= maxId)) ? cAtTrue : cAtFalse;
    }

static eBool IsBackplaneSerdes(AtSerdesManager self, uint32 serdesId)
    {
    uint8 minId = mMethodsGet(mThis(self))->BackplaneSerdesStartId(self);
    uint8  maxId = mMethodsGet(mThis(self))->BackplaneSerdesEndId(self);

    return ((serdesId >= minId) && (serdesId <= maxId)) ? cAtTrue : cAtFalse;
    }

static eBool IsSgmiiSerdes(AtSerdesManager self, uint32 serdesId)
    {
    uint8 minId = mMethodsGet(mThis(self))->SgmiiSerdesStartId(self);
    uint8  maxId = mMethodsGet(mThis(self))->SgmiiSerdesEndId(self);

    return ((serdesId >= minId) && (serdesId <= maxId)) ? cAtTrue : cAtFalse;
    }

static AtSerdesController MateSerdesControllerObjectCreate(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId)
    {
    AtDevice device = AtSerdesManagerDeviceGet((AtSerdesManager)self);

    if (Tha60290021DeviceIsRunningOnOtherPlatform(device))
        {
        AtSerdesController newSerdes = Tha60210051Tfi5SerdesControllerNew((AtSdhLine)physicalPort, serdesId);
        AtSerdesControllerManagerSet(newSerdes, (AtSerdesManager)self);
        AtSerdesControllerHwIdSet(newSerdes, Tha60290021SerdesManagerMateSerdesLocalId((AtSerdesManager)self, serdesId));
        return newSerdes;
        }

    return Tha6029MateSerdesControllerNew((AtSerdesManager)self, physicalPort, serdesId);
    }

static AtSerdesController FaceplateSerdesControllerObjectCreate(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId)
    {
    return Tha6029FaceplateSerdesControllerNew((AtSerdesManager)self, physicalPort, serdesId);
    }

static AtSerdesController SgmiiSerdesControllerObjectCreate(Tha60290021SerdesManager self, AtChannel physicalPort, uint32 serdesId)
    {
    return Tha6029SgmiiSerdesControllerNew((AtSerdesManager)self, physicalPort, serdesId);
    }

static  AtSerdesController SerdesControllerObjectCreate (AtSerdesManager self, uint32 serdesId)
    {
    if (IsFaceplateSerdes(self, serdesId))
        return mMethodsGet(mThis(self))->FaceplateSerdesControllerObjectCreate(mThis(self), NULL, serdesId);

    if (IsMateSerdes(self, serdesId))
        return mMethodsGet(mThis(self))->MateSerdesControllerObjectCreate(mThis(self), NULL, serdesId);

    if (IsBackplaneSerdes(self, serdesId))
        return Tha6029BackplaneSerdesControllerNew(self, NULL, serdesId);

    if (IsSgmiiSerdes(self, serdesId))
        return mMethodsGet(mThis(self))->SgmiiSerdesControllerObjectCreate(mThis(self), NULL, serdesId);

    return NULL;
    }

static uint32 NumSerdesControllers(AtSerdesManager self)
    {
    uint32 numFateplateSerdes = mMethodsGet(mThis(self))->NumFaceplateSerdes(self);
    uint32 numMateSerdes = mMethodsGet(mThis(self))->NumMateSerdes(self);
    uint32 numSgmiiSerdes = mMethodsGet(mThis(self))->NumSgmiiSerdes(self);
    uint32 numBackplaneSerdes = mMethodsGet(mThis(self))->NumBackplaneSerdes(self);
    return  numFateplateSerdes + numMateSerdes + numSgmiiSerdes + numBackplaneSerdes;
    }

static AtSerdesController BackplaneSerdes(AtSerdesManager self, uint8 localIndex)
    {
    uint8 minId = mMethodsGet(mThis(self))->BackplaneSerdesStartId(self);
    uint32 serdesId = localIndex + (uint32)minId;
    return AtSerdesManagerSerdesControllerGet(self, serdesId);
    }

static eBool IsFaceplateSerdesRegister(AtSerdesManager self, uint32 address)
    {
    AtUnused(self);

    if (mInRange(address, 0xFA0000, 0xFA07FF) ||
        mInRange(address, 0xFAA000, 0xFAA7FF) ||
        mInRange(address, 0xFAA800, 0xFAAFFF) ||
        mInRange(address, 0xFAB000, 0xFAB7FF) ||
        mInRange(address, 0xFAB800, 0xFABFFF) ||
        mInRange(address, 0xFA2000, 0xFA27FF) ||
        mInRange(address, 0xFA6000, 0xFA67FF) ||
        mInRange(address, 0xF60000, 0xF61FFF))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsBackplaneSerdesRegister(AtSerdesManager self, uint32 address)
    {
    AtUnused(self);
    if (mInRange(address, 0xF80000, 0xF8FFFF) ||
        mInRange(address, 0xF90000, 0xF9FFFF))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsMateSerdesRegister(AtSerdesManager self, uint32 address)
    {
    AtUnused(self);
    if (mInRange(address, 0xF70000, 0xF71FFF) ||
        mInRange(address, 0xF72000, 0xF73FFF) ||
        mInRange(address, 0xF74000, 0xF75FFF) ||
        mInRange(address, 0xF76000, 0xF77FFF))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsSgmiiSerdesRegister(AtSerdesManager self, uint32 address)
    {
    AtUnused(self);
    if (mInRange(address, 0xF50000, 0xF50FFF) ||
        mInRange(address, 0xF51000, 0xF51FFF) ||
        mInRange(address, 0xF52000, 0xF52FFF) ||
        mInRange(address, 0xF53000, 0xF53FFF))
        return cAtTrue;

    return cAtFalse;
    }

static eBool CanAccessRegister(AtSerdesManager self, uint32 serdesId, uint32 address, eAtModule moduleId)
    {
    AtUnused(moduleId);
    AtUnused(serdesId);

    if (IsFaceplateSerdesRegister(self, address))
        return cAtTrue;
    if (IsBackplaneSerdesRegister(self, address))
        return cAtTrue;
    if (IsMateSerdesRegister(self, address))
        return cAtTrue;

    return IsSgmiiSerdesRegister(self, address);
    }

static uint32 BackplaneSerdesLocalId(AtSerdesManager self, uint32 serdesId)
    {
    return serdesId - Tha60290021SerdesManagerBackplaneSerdesStartId(self);
    }

static uint32 MateSerdesLocalId(AtSerdesManager self, uint32 serdesId)
    {
    return serdesId - Tha60290021SerdesManagerMateSerdesStartId(self);
    }

static uint32 FaceplateSerdesLocalId(AtSerdesManager self, uint32 serdesId)
    {
    return serdesId - Tha60290021SerdesManagerFaceplateSerdesStartId(self);
    }

static uint32 SgmiiSerdesLocalId(AtSerdesManager self, uint32 serdesId)
    {
    return serdesId - Tha60290021SerdesManagerSgmiiSerdesStartId(self);
    }

static AtIpCore IpCore(AtSerdesManager self)
    {
    return AtDeviceIpCoreGet(AtSerdesManagerDeviceGet(self), 0);
    }

static uint32 TopAddressWithLocalAddress(uint32 localAddress)
    {
    return cTopBaseAddress + localAddress;
    }

static eAtRet HwFaceplateGroupSet(Tha6029SerdesManager self, uint8 groupId)
    {
    AtIpCore core = IpCore((AtSerdesManager)self);
    uint32 regAddr = TopAddressWithLocalAddress(cAf6Reg_o_control0_Base);
    uint32 regVal = AtIpCoreRead(core, regAddr);
    mRegFieldSet(regVal, cAf6_o_control0_multirate_sel_, groupId);
    AtIpCoreWrite(core, regAddr, regVal);
    return cAtOk;
    }

static eBool FaceplateGroupIsValid(Tha6029SerdesManager self, uint8 groupId)
    {
    AtUnused(self);
    return (groupId < 2) ? cAtTrue : cAtFalse;
    }

static eAtRet FaceplateGroupSet(Tha6029SerdesManager self, uint8 groupId)
    {
    if (FaceplateGroupIsValid(self, groupId))
        return HwFaceplateGroupSet(self, groupId);
    return cAtErrorInvlParm;
    }

static uint8 FaceplateGroupGet(Tha6029SerdesManager self)
    {
    AtIpCore core = IpCore((AtSerdesManager)self);
    uint32 regAddr = TopAddressWithLocalAddress(cAf6Reg_o_control0_Base);
    uint32 regVal = AtIpCoreRead(core, regAddr);
    return (uint8)mRegField(regVal, cAf6_o_control0_multirate_sel_);
    }

static eBool FacePlateLineModeIsSupported(AtSerdesManager self, AtSerdesController serdes, eAtSerdesMode mode)
    {
    uint32 portId = AtSerdesControllerHwIdGet(serdes);

    AtUnused(self);

    if ((mode == cAtSerdesModeEth10G) || (mode == cAtSerdesModeStm64))
        {
        if ((portId == 0) || (portId == 8))
            return cAtTrue;

        return cAtFalse;
        }

    switch ((uint32) mode)
        {
        case cAtSerdesModeStm16   : return cAtTrue;
        case cAtSerdesModeStm4    : return cAtTrue;
        case cAtSerdesModeStm0    : return cAtTrue;
        case cAtSerdesModeStm1    : return cAtTrue;
        case cAtSerdesModeEth100M : return cAtTrue;
        case cAtSerdesModeEth1G   : return cAtTrue;

        case cAtSerdesModeEth2500M: return cAtFalse;
        case cAtSerdesModeEth40G  : return cAtFalse;
        case cAtSerdesModeOcn     : return cAtFalse;
        case cAtSerdesModeGe      : return cAtFalse;
        case cAtSerdesModeEth10M  : return cAtFalse;
        case cAtSerdesModeUnknown : return cAtFalse;

        default:
            return cAtFalse;
        }
    }

static AtSerdesController FacePlateSerdesGet(AtSerdesManager self, uint8 localId)
    {
    uint32 serdesId = (uint32)(Tha60290021SerdesManagerFaceplateSerdesStartId(self) + localId);
    return AtSerdesManagerSerdesControllerGet(self, serdesId);
    }

static uint32 NumFaceplateSerdesPerPart(AtSerdesManager self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 FaceplateSerdesPart(AtSerdesManager self, uint32 serdesId)
    {
    return serdesId / NumFaceplateSerdesPerPart(self);
    }

static eBool PortsCanBeReduced(Tha60290021SerdesManager self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SerdesSecondPartIsRemoved(AtSerdesManager self)
    {
    AtDevice device = AtSerdesManagerDeviceGet(self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersionRemoveLast8FaceplateSerdes = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x2, 0x0);

    if (hwVersion >= startVersionRemoveLast8FaceplateSerdes)
        return cAtTrue;

    return cAtFalse;
    }

static eBool SerdesPartIsRemoved(AtSerdesManager self, uint32 serdesPart)
    {
    if (mMethodsGet(mThis(self))->PortsCanBeReduced(mThis(self)))
        {
        eBool belongToSecondPart = (serdesPart == 1) ? cAtTrue : cAtFalse;
        if (belongToSecondPart && SerdesSecondPartIsRemoved(self))
            return cAtTrue;
        }

    return cAtFalse;
    }

static uint32 NumMateSerdesPerPart(AtSerdesManager self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 MateSerdesPart(AtSerdesManager self, uint32 serdesId)
    {
    return MateSerdesLocalId(self, serdesId) / NumMateSerdesPerPart(self);
    }

static eBool SpareSgmiiSerdesAreRemoved(Tha60290021SerdesManager self)
    {
    AtDevice device = AtSerdesManagerDeviceGet((AtSerdesManager)self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersionRemoveSpareSgmii = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x5, 0x0);

    if (hwVersion >= startVersionRemoveSpareSgmii)
        return cAtTrue;

    return cAtFalse;
    }

static eBool SgmiiSerdesIsRemoved(AtSerdesManager self, uint32 serdesId)
    {
    uint32 localId = Tha60290021SerdesManagerSgmiiSerdesLocalId(self, serdesId);

    /* DCC and K-Byte SGMIIs will not be removed */
    if ((localId == Tha60290021SerdesManagerSgmiiSerdesLocalKByteId(self)) ||
        (localId == Tha60290021SerdesManagerSgmiiSerdesLocalDccId(self)))
        return cAtFalse;

    /* Only spare SGMIIs are removed from version 1.5 */
    return mMethodsGet(mThis(self))->SpareSgmiiSerdesAreRemoved(mThis(self));
    }

static eBool SerdesIsControllable(AtSerdesManager self, uint32 serdesId)
    {
    if (IsFaceplateSerdes(self, serdesId) && SerdesPartIsRemoved(self, FaceplateSerdesPart(self, serdesId)))
        return cAtFalse;

    if (IsMateSerdes(self, serdesId) && SerdesPartIsRemoved(self, MateSerdesPart(self, serdesId)))
        return cAtFalse;

    if (IsSgmiiSerdes(self, serdesId) && SgmiiSerdesIsRemoved(self, serdesId))
        return cAtFalse;

    return m_AtSerdesManagerMethods->SerdesIsControllable(self, serdesId);
    }

static uint32 TenG_FaceplateSerdes_XfiDiagnosticBaseAddress(AtSerdesManager self, AtSerdesController serdes)
    {
    uint32 portId = AtSerdesControllerHwIdGet(serdes);
    AtUnused(self);

    if (portId == 0)
        return cSerdesXfi0DiagBaseAdress;

    if (portId == 8)
        return cSerdesXfi8DiagBaseAdress;

    return cInvalidUint32;
    }

static AtEyeScanController EyeScanControllerCreate(Tha60290021SerdesManager self, AtSerdesController serdes, uint32 drpBaseAddress)
    {
    AtUnused(self);
    return Tha60290021EyeScanControllerNew(serdes, drpBaseAddress);
    }

static eBool SerdesEyeScanResetIsSupported(Tha60290021SerdesManager self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideTha6029SerdesManager(AtSerdesManager self)
    {
    Tha6029SerdesManager manager = (Tha6029SerdesManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SerdesManagerOverride, mMethodsGet(manager), sizeof(m_Tha6029SerdesManagerOverride));

        mMethodOverride(m_Tha6029SerdesManagerOverride, FaceplateGroupSet);
        mMethodOverride(m_Tha6029SerdesManagerOverride, FaceplateGroupGet);
        }

    mMethodsSet(manager, &m_Tha6029SerdesManagerOverride);
    }

static void OverrideAtSerdesManager(AtSerdesManager self)
    {
    AtSerdesManager object = (AtSerdesManager)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesManagerMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesManagerOverride, m_AtSerdesManagerMethods, sizeof(m_AtSerdesManagerOverride));

        mMethodOverride(m_AtSerdesManagerOverride, NumSerdesControllers);
        mMethodOverride(m_AtSerdesManagerOverride, SerdesControllerObjectCreate);
        mMethodOverride(m_AtSerdesManagerOverride, CanAccessRegister);
        mMethodOverride(m_AtSerdesManagerOverride, SerdesIsControllable);
        }

    mMethodsSet(object, &m_AtSerdesManagerOverride);
    }

static void Override(AtSerdesManager self)
    {
    OverrideAtSerdesManager(self);
    OverrideTha6029SerdesManager(self);
    }

static void MethodsInit(Tha60290021SerdesManager self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, PortsCanBeReduced);
        mMethodOverride(m_methods, FaceplateSerdesControllerObjectCreate);
        mMethodOverride(m_methods, SgmiiSerdesControllerObjectCreate);
        mMethodOverride(m_methods, MateSerdesControllerObjectCreate);
        mMethodOverride(m_methods, EyeScanControllerCreate);
        mMethodOverride(m_methods, SpareSgmiiSerdesAreRemoved);
        mMethodOverride(m_methods, NumFaceplateSerdes);
        mMethodOverride(m_methods, NumMateSerdes);
        mMethodOverride(m_methods, NumSgmiiSerdes);
        mMethodOverride(m_methods, NumBackplaneSerdes);
        mMethodOverride(m_methods, FaceplateSerdesStartId);
        mMethodOverride(m_methods, FaceplateSerdesEndId);
        mMethodOverride(m_methods, MateSerdesStartId);
        mMethodOverride(m_methods, MateSerdesEndId);
        mMethodOverride(m_methods, SgmiiSerdesStartId);
        mMethodOverride(m_methods, SgmiiSerdesEndId);
        mMethodOverride(m_methods, BackplaneSerdesStartId);
        mMethodOverride(m_methods, BackplaneSerdesEndId);
        mMethodOverride(m_methods, TenG_FaceplateSerdes_XfiDiagnosticBaseAddress);
        mMethodOverride(m_methods, SerdesEyeScanResetIsSupported);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SerdesManager);
    }

AtSerdesManager Tha60290021SerdesManagerObjectInit(AtSerdesManager self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesManagerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtSerdesManager Tha60290021SerdesManagerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesManager newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021SerdesManagerObjectInit(newModule, device);
    }

AtSerdesController Tha60290021SerdesManagerBackplaneSerdesWithLocalIndex(AtSerdesManager self, uint8 localIndex)
    {
    if (self)
        return BackplaneSerdes(self, localIndex);
    return NULL;
    }

uint8 Tha60290021SerdesManagerFaceplateSerdesStartId(AtSerdesManager self)
    {
    if (self)
        return mMethodsGet(mThis(self))->FaceplateSerdesStartId(self);
    return 0xFF;
    }

uint8 Tha60290021SerdesManagerMateSerdesStartId(AtSerdesManager self)
    {
    if (self)
        return mMethodsGet(mThis(self))->MateSerdesStartId(self);
    return 0xFF;
    }

uint8 Tha60290021SerdesManagerBackplaneSerdesStartId(AtSerdesManager self)
    {
    if (self)
        return mMethodsGet(mThis(self))->BackplaneSerdesStartId(self);
    return 0xFF;
    }

uint8 Tha60290021SerdesManagerSgmiiSerdesStartId(AtSerdesManager self)
    {
    if (self)
        return mMethodsGet(mThis(self))->SgmiiSerdesStartId(self);
    return 0xFF;
    }

AtSerdesController Tha60290021SerdesManagerMateSerdesController(AtSerdesManager self, uint32 localId)
    {
    return AtSerdesManagerSerdesControllerGet(self, localId + Tha60290021SerdesManagerMateSerdesStartId(self));
    }

uint32 Tha60290021SerdesManagerBackplaneSerdesLocalId(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return BackplaneSerdesLocalId(self, serdesId);
    return cInvalidUint32;
    }

uint32 Tha60290021SerdesManagerMateSerdesLocalId(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return MateSerdesLocalId(self, serdesId);
    return cInvalidUint32;
    }

uint32 Tha60290021SerdesManagerFaceplateSerdesLocalId(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return FaceplateSerdesLocalId(self, serdesId);
    return cInvalidUint32;
    }

uint32 Tha60290021SerdesManagerSgmiiSerdesLocalId(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return SgmiiSerdesLocalId(self, serdesId);
    return cInvalidUint32;
    }

uint32 Tha60290021SerdesManagerSgmiiSerdesLocalKByteId(AtSerdesManager self)
    {
    AtUnused(self);
    return 0;
    }

uint32 Tha60290021SerdesManagerSgmiiSerdesLocalDccId(AtSerdesManager self)
    {
    return Tha60290021SerdesManagerSgmiiSerdesLocalKByteId(self) + 1;
    }

uint32 Tha60290021SerdesManagerSgmiiSerdesSpareLocalId(AtSerdesManager self, uint32 serdesId)
    {
    uint32 localId = Tha60290021SerdesManagerSgmiiSerdesLocalId(self, serdesId);
    uint32 startLocalId = Tha60290021SerdesManagerSgmiiSerdesLocalDccId(self) + 1;
    return (localId - startLocalId);
    }

eBool Tha60290021SerdesManagerSerdesIsSgmii(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return IsSgmiiSerdes(self, serdesId);
    return cAtFalse;
    }

eBool Tha60290021SerdesManagerSerdesIsBackplane(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return IsBackplaneSerdes(self, serdesId);
    return cAtFalse;
    }

AtSerdesController Tha60290021SerdesManagerBackplaneSerdesController(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        {
        uint32 localId = BackplaneSerdesLocalId(self, serdesId);
        return Tha60290021SerdesManagerBackplaneSerdesWithLocalIndex(self, (uint8)localId);
        }

    return NULL;
    }

uint32 Tha60290021SerdesManagerXfiDiagnosticBaseAddress(AtSerdesController serdes)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(serdes);
    return mMethodsGet(mThis(manager))->TenG_FaceplateSerdes_XfiDiagnosticBaseAddress(manager, serdes);
    }

uint32 Tha60290021SerdesManagerSgmiiDiagnosticBaseAddress(AtSerdesController serdes)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(serdes);
    uint32 serdesId = AtSerdesControllerIdGet(serdes);
    uint32 localId =  Tha60290021SerdesManagerSgmiiSerdesLocalId(manager, serdesId);

    if (localId == Tha60290021SerdesManagerSgmiiSerdesLocalKByteId(manager))
        return 0xF50000;

    if (localId == Tha60290021SerdesManagerSgmiiSerdesLocalDccId(manager))
        return 0xF51000;

    return 0xF52000;
    }

char *Tha60290021SerdesManagerSgmiiDescription(AtSerdesManager self, uint32 serdesId, char *buffer, uint32 bufferSize)
    {
    uint32 localPortId = serdesId - SgmiiSerdesStartId(self);

    if (localPortId == Tha60290021SerdesManagerSgmiiSerdesLocalKByteId(self))
        {
        AtSnprintf(buffer, bufferSize, "sgmii_kbyte_reserved");
        return buffer;
        }

    if (localPortId == Tha60290021SerdesManagerSgmiiSerdesLocalDccId(self))
        {
        AtSnprintf(buffer, bufferSize, "sgmii_kbyte_dcc");
        return buffer;
        }

    AtSnprintf(buffer, bufferSize, "sgmii_spare.%d", Tha60290021SerdesManagerSgmiiSerdesSpareLocalId(self, serdesId) + 1);
    return buffer;
    }

AtSerdesController Tha60290021SerdesManagerSgmiiSerdesControllerGet(AtSerdesManager self, uint32 localId)
    {
    uint32 flatId = SgmiiSerdesStartId(self) + localId;
    return AtSerdesManagerSerdesControllerGet(self, flatId);
    }

uint32 Tha60290021SerdesManagerNumSgmiiSerdes(AtSerdesManager self)
    {
    return mMethodsGet(mThis(self))->NumSgmiiSerdes(self);
    }

uint32 Tha60290021SerdesManagerNumFaceplateSerdes(AtSerdesManager self)
    {
    return mMethodsGet(mThis(self))->NumFaceplateSerdes(self);
    }

uint32 Tha60290021SerdesManagerNumMateSerdes(AtSerdesManager self)
    {
    return mMethodsGet(mThis(self))->NumMateSerdes(self);
    }

eBool Tha60290021SerdesManagerFacePlateLineSerdesModeIsSupported(AtSerdesManager self, AtSerdesController serdes, eAtSerdesMode mode)
    {
    if (self)
        return FacePlateLineModeIsSupported(self, serdes, mode);
    return cAtFalse;
    }

AtSerdesController Tha60290021SerdesManagerFacePlateSerdesGet(AtSerdesManager self, uint8 localId)
    {
    if (self)
        return FacePlateSerdesGet(self, localId);
    return NULL;
    }

uint32 Tha60290021SerdesManagerFaceplateSerdesPart(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return FaceplateSerdesPart(self, serdesId);
    return cInvalidUint32;
    }

eBool Tha60290021SerdesManagerIsFaceplateSerdes(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return IsFaceplateSerdes(self, serdesId);
    return cAtFalse;
    }

eBool Tha60290021SerdesManagerIsMateSerdes(AtSerdesManager self, uint32 serdesId)
    {
    if (self)
        return IsMateSerdes(self, serdesId);
    return cAtFalse;
    }

eBool Tha60290021SerdesManagerSerdesEyeScanResetIsSupported(Tha60290021SerdesManager self)
    {
    if (self)
        return mMethodsGet(self)->SerdesEyeScanResetIsSupported(self);
    return cAtFalse;
    }

AtEyeScanController Tha60290021SerdesManagerEyeScanControllerCreate(Tha60290021SerdesManager self, AtSerdesController serdes, uint32 drpBaseAddress)
    {
    if (self)
        return mMethodsGet(self)->EyeScanControllerCreate(self, serdes, drpBaseAddress);
    return NULL;
    }

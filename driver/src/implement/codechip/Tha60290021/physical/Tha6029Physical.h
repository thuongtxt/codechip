/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha6029Physical.h
 * 
 * Created Date: Sep 10, 2016
 *
 * Description : Physical declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029PHYSICAL_H_
#define _THA6029PHYSICAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSerdesController.h"
#include "AtUart.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eTha6029FaceplateSerdesHwMode
    {
    cTha6029FaceplateSerdesHwModeOc192   = 0,
    cTha6029FaceplateSerdesHwModeOc48    = 1,
    cTha6029FaceplateSerdesHwModeOc12    = 2,
    cTha6029FaceplateSerdesHwMode10G     = 4,
    cTha6029FaceplateSerdesHwMode1G      = 5,
    cTha6029FaceplateSerdesHwMode100Fx   = 6,
    cTha6029FaceplateSerdesHwModeInvalid = cInvalidUint8
    }eTha6029FaceplateSerdesHwMode;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtUart Tha6029DiagUartNew(AtDevice device);

AtSerdesController Tha6029BackplaneSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId);
AtSerdesController Tha6029FaceplateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId);
AtSerdesController Tha6029MateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId);
AtSerdesController Tha6029BacklaneSerdesLaneNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId);
AtSerdesController Tha6029SgmiiSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId);

AtEyeScanController Tha60290021EyeScanControllerNew(AtSerdesController serdesController, uint32 drpBaseAddress);

eAtModulePrbsRet Tha60290021FacePlateSerdesControllerPrbsHwModeSet(AtSerdesController serdes, uint32 mode);
uint32 Tha60290021FacePlateSerdesControllerPrbsHwModeGet(AtSerdesController serdes);
uint32 Tha60290021FacePlateSerdesControllerPrbsDefaultMode(AtSerdesController self);
eTha6029FaceplateSerdesHwMode Tha6029FaceplateSerdesControllerHwModeGet(AtSerdesController self);

eAtRet Tha6029FaceplateSerdesControllerDisparityForce(AtSerdesController self, eBool forced);
eBool Tha6029FaceplateSerdesControllerDisparityIsForced(AtSerdesController self);

eAtRet Tha6029SgmiiElectricallyIsolatePHYFromGMII(AtSerdesController self, eBool isolated);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029PHYSICAL_H_ */


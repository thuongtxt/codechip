/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEVICE
 * 
 * File        : Tha6020SerdesManager.h
 * 
 * Created Date: Jul 11, 2016
 *
 * Description : Serdes Management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021SERDESMANAGER_H_
#define _THA60290021SERDESMANAGER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtCommon.h"
#include "AtSerdesManager.h"
#include "AtSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021SerdesManager * Tha60290021SerdesManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesManager Tha60290021SerdesManagerNew(AtDevice device);

AtSerdesController Tha60290021SerdesManagerBackplaneSerdesWithLocalIndex(AtSerdesManager self, uint8 localIndex);
uint32 Tha60290021SerdesManagerBackplaneSerdesLocalId(AtSerdesManager self, uint32 serdesId);
eBool Tha60290021SerdesManagerSerdesIsBackplane(AtSerdesManager self, uint32 serdesId);
AtSerdesController Tha60290021SerdesManagerBackplaneSerdesController(AtSerdesManager self, uint32 serdesId);

AtSerdesController Tha60290021SerdesManagerMateSerdesController(AtSerdesManager self, uint32 localId);
uint32 Tha60290021SerdesManagerMateSerdesLocalId(AtSerdesManager self, uint32 serdesId);
uint32 Tha60290021SerdesManagerNumMateSerdes(AtSerdesManager self);

uint32 Tha60290021SerdesManagerFaceplateSerdesLocalId(AtSerdesManager self, uint32 serdesId);
uint32 Tha60290021SerdesManagerNumFaceplateSerdes(AtSerdesManager self);

uint32 Tha60290021SerdesManagerSgmiiSerdesLocalId(AtSerdesManager self, uint32 serdesId);
eBool Tha60290021SerdesManagerSerdesIsSgmii(AtSerdesManager self, uint32 serdesId);
char *Tha60290021SerdesManagerSgmiiDescription(AtSerdesManager self, uint32 serdesId, char *buffer, uint32 bufferSize);
uint32 Tha60290021SerdesManagerNumSgmiiSerdes(AtSerdesManager self);
uint32 Tha60290021SerdesManagerSgmiiSerdesLocalKByteId(AtSerdesManager self);
uint32 Tha60290021SerdesManagerSgmiiSerdesLocalDccId(AtSerdesManager self);
uint32 Tha60290021SerdesManagerSgmiiSerdesSpareLocalId(AtSerdesManager self, uint32 serdesId);
uint32 Tha60290021SerdesManagerSgmiiDiagnosticBaseAddress(AtSerdesController serdes);
AtSerdesController Tha60290021SerdesManagerSgmiiSerdesControllerGet(AtSerdesManager self, uint32 localId);

uint8 Tha60290021SerdesManagerFaceplateSerdesStartId(AtSerdesManager self);
uint8 Tha60290021SerdesManagerMateSerdesStartId(AtSerdesManager self);
uint8 Tha60290021SerdesManagerBackplaneSerdesStartId(AtSerdesManager self);
uint8 Tha60290021SerdesManagerSgmiiSerdesStartId(AtSerdesManager self);

uint32 Tha60290021SerdesManagerXfiDiagnosticBaseAddress(AtSerdesController serdes);
eBool Tha60290021SerdesManagerFacePlateLineSerdesModeIsSupported(AtSerdesManager self, AtSerdesController serdes, eAtSerdesMode mode);
AtSerdesController Tha60290021SerdesManagerFacePlateSerdesGet(AtSerdesManager self, uint8 localId);
uint32 Tha60290021SerdesManagerFaceplateSerdesPart(AtSerdesManager self, uint32 serdesId);
eBool Tha60290021SerdesManagerIsFaceplateSerdes(AtSerdesManager self, uint32 serdesId);
eBool Tha60290021SerdesManagerIsMateSerdes(AtSerdesManager self, uint32 serdesId);

/* Eyescan */
AtEyeScanController Tha60290021SerdesManagerEyeScanControllerCreate(Tha60290021SerdesManager self, AtSerdesController serdes, uint32 drpBaseAddress);
eBool Tha60290021SerdesManagerSerdesEyeScanResetIsSupported(Tha60290021SerdesManager self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021SERDESMANAGER_H_ */

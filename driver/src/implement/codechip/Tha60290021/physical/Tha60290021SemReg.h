/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_RD_SEM_H_
#define _AF6_REG_AF6CNC0021_RD_SEM_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : SEU PCI Configuration 0
Reg Addr   : 0x000-0x200
Reg Formula: 0x000+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
Control Command Interface bus of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_cmd_ctrl_Base                                                                        0x000

/*--------------------------------------
BitField Name: cmd_ctrl_done
BitField Type: W1C
BitField Desc: Command Interface status
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_sem_cmd_ctrl_cmd_ctrl_done_Mask                                                            cBit10
#define cAf6_sem_cmd_ctrl_cmd_ctrl_done_Shift                                                               10

/*--------------------------------------
BitField Name: cmd_ctrl_busy
BitField Type: R_O
BitField Desc: Command Interface status
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_sem_cmd_ctrl_cmd_ctrl_busy_Mask                                                             cBit9
#define cAf6_sem_cmd_ctrl_cmd_ctrl_busy_Shift                                                                9

/*--------------------------------------
BitField Name: SEU_INJECT_STROBE
BitField Type: R/W
BitField Desc: Trigger 1->0 to start command
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_sem_cmd_ctrl_SEU_INJECT_STROBE_Mask                                                         cBit8
#define cAf6_sem_cmd_ctrl_SEU_INJECT_STROBE_Shift                                                            8

/*--------------------------------------
BitField Name: OP
BitField Type: R/W
BitField Desc: Command type, other values are unused
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_sem_cmd_ctrl_OP_Mask                                                                      cBit7_4
#define cAf6_sem_cmd_ctrl_OP_Shift                                                                           4


/*------------------------------------------------------------------------------
Reg Name   : SEU PCI Configuration 1
Reg Addr   : 0x001-0x201
Reg Formula: 0x001+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
Command Interface bus value part0 of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_cmd_val0_Base                                                                        0x001

/*--------------------------------------
BitField Name: sem_cmd_slr
BitField Type: R/W
BitField Desc: Hardware slr number (2-bit),valid range [0:2]
BitField Bits: [30:29]
--------------------------------------*/
#define cAf6_sem_cmd_val0_sem_cmd_slr_Mask                                                           cBit30_29
#define cAf6_sem_cmd_val0_sem_cmd_slr_Shift                                                                 29

/*--------------------------------------
BitField Name: sem_cmd_wradr
BitField Type: R/W
BitField Desc: Word Address  :  7bit, valid range [0:122]
BitField Bits: [11:05]
--------------------------------------*/
#define cAf6_sem_cmd_val0_sem_cmd_wradr_Mask                                                          cBit11_5
#define cAf6_sem_cmd_val0_sem_cmd_wradr_Shift                                                                5


/*------------------------------------------------------------------------------
Reg Name   : SEU Alarm
Reg Addr   : 0x002-0x202
Reg Formula: 0x002+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
SEM controller sticky state of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_ctrl_stk_sta_Base                                                                    0x002

/*--------------------------------------
BitField Name: SLR2_status_heartbeat
BitField Type: W1C
BitField Desc: SLR2 status_heartbeat : Set 1 indicate to SEM IP got heartbeat
event
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_SLR2_status_heartbeat_Mask                                                cBit26
#define cAf6_sem_ctrl_stk_sta_SLR2_status_heartbeat_Shift                                                   26

/*--------------------------------------
BitField Name: SLR1_status_heartbeat
BitField Type: W1C
BitField Desc: SLR1 status_heartbeat : Set 1 indicate to SEM IP got heartbeat
event
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_SLR1_status_heartbeat_Mask                                                cBit25
#define cAf6_sem_ctrl_stk_sta_SLR1_status_heartbeat_Shift                                                   25

/*--------------------------------------
BitField Name: SLR0_status_heartbeat
BitField Type: W1C
BitField Desc: SLR0 status_heartbeat : Set 1 indicate to SEM IP got heartbeat
event
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_SLR0_status_heartbeat_Mask                                                cBit24
#define cAf6_sem_ctrl_stk_sta_SLR0_status_heartbeat_Shift                                                   24

/*--------------------------------------
BitField Name: SLR2_Sem_Act
BitField Type: W1C
BitField Desc: SLR2 Sem_Act : Set 1 indicate to SEM IP got Active State event
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_SLR2_Sem_Act_Mask                                                         cBit18
#define cAf6_sem_ctrl_stk_sta_SLR2_Sem_Act_Shift                                                            18

/*--------------------------------------
BitField Name: SLR1_Sem_Act
BitField Type: W1C
BitField Desc: SLR1 Sem_Act : Set 1 indicate to SEM IP got Active State event
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_SLR1_Sem_Act_Mask                                                         cBit17
#define cAf6_sem_ctrl_stk_sta_SLR1_Sem_Act_Shift                                                            17

/*--------------------------------------
BitField Name: SLR0_Sem_Act
BitField Type: W1C
BitField Desc: SLR0 Sem_Act : Set 1 indicate to SEM IP got Active State event
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_SLR0_Sem_Act_Mask                                                         cBit16
#define cAf6_sem_ctrl_stk_sta_SLR0_Sem_Act_Shift                                                            16

/*--------------------------------------
BitField Name: status_uncorrectable
BitField Type: W1C
BitField Desc: status_uncorrectable : Set 1 indicate to SEM IP got uncorrectable
event
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_status_uncorrectable_Mask                                                 cBit11
#define cAf6_sem_ctrl_stk_sta_status_uncorrectable_Shift                                                    11

/*--------------------------------------
BitField Name: status_correctable
BitField Type: W1C
BitField Desc: status_correctable : Set 1 indicate to SEM IP got correctable
event
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_status_correctable_Mask                                                   cBit10
#define cAf6_sem_ctrl_stk_sta_status_correctable_Shift                                                      10

/*--------------------------------------
BitField Name: status_essential
BitField Type: W1C
BitField Desc: status_essential : Set 1 indicate to SEM IP got essential event
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_status_essential_Mask                                                      cBit8
#define cAf6_sem_ctrl_stk_sta_status_essential_Shift                                                         8

/*--------------------------------------
BitField Name: status_idle
BitField Type: W1C
BitField Desc: status_idle : Set 1 indicate to SEM IP got IDLE State event
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_status_idle_Mask                                                           cBit6
#define cAf6_sem_ctrl_stk_sta_status_idle_Shift                                                              6

/*--------------------------------------
BitField Name: status_initialization
BitField Type: W1C
BitField Desc: status_initialization : Set 1 indicate to SEM IP got
initialization State event
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_status_initialization_Mask                                                 cBit5
#define cAf6_sem_ctrl_stk_sta_status_initialization_Shift                                                    5

/*--------------------------------------
BitField Name: status_injection
BitField Type: W1C
BitField Desc: status_injection : Set 1 indicate to SEM IP got injection State
event
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_status_injection_Mask                                                      cBit4
#define cAf6_sem_ctrl_stk_sta_status_injection_Shift                                                         4

/*--------------------------------------
BitField Name: status_classification
BitField Type: W1C
BitField Desc: status_classification : Set 1 indicate to SEM IP got
classification State event
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_status_classification_Mask                                                 cBit3
#define cAf6_sem_ctrl_stk_sta_status_classification_Shift                                                    3

/*--------------------------------------
BitField Name: Fatal_Sem_err
BitField Type: W1C
BitField Desc: Fatal_Sem_Err : Set 1 indicate to SEM IP has fatal error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_Fatal_Sem_err_Mask                                                         cBit2
#define cAf6_sem_ctrl_stk_sta_Fatal_Sem_err_Shift                                                            2

/*--------------------------------------
BitField Name: status_observation
BitField Type: W1C
BitField Desc: status_observation : Set 1 indicate to SEM IP got observation
State event
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_status_observation_Mask                                                    cBit1
#define cAf6_sem_ctrl_stk_sta_status_observation_Shift                                                       1

/*--------------------------------------
BitField Name: status_correction
BitField Type: W1C
BitField Desc: status_correction : Set 1 indicate to SEM IP got Correction State
event
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_sem_ctrl_stk_sta_status_correction_Mask                                                     cBit0
#define cAf6_sem_ctrl_stk_sta_status_correction_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : SEU PCI Status
Reg Addr   : 0x003-0x203
Reg Formula: 0x003+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
SEM controller current state of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_ctrl_cur_sta_Base                                                                    0x003

/*--------------------------------------
BitField Name: SLR2_Sem_Act
BitField Type: R_O
BitField Desc: SLR2 Sem_Act: Set 1 indicate  status FSM of SEM IP is active
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_sem_ctrl_cur_sta_SLR2_Sem_Act_Mask                                                         cBit18
#define cAf6_sem_ctrl_cur_sta_SLR2_Sem_Act_Shift                                                            18

/*--------------------------------------
BitField Name: SLR1_Sem_Act
BitField Type: R_O
BitField Desc: SLR1 Sem_Act: Set 1 indicate  status FSM of SEM IP is active
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_sem_ctrl_cur_sta_SLR1_Sem_Act_Mask                                                         cBit17
#define cAf6_sem_ctrl_cur_sta_SLR1_Sem_Act_Shift                                                            17

/*--------------------------------------
BitField Name: SLR0_Sem_Act
BitField Type: R_O
BitField Desc: SLR0 Sem_Act: Set 1 indicate  status FSM of SEM IP is active
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_sem_ctrl_cur_sta_SLR0_Sem_Act_Mask                                                         cBit16
#define cAf6_sem_ctrl_cur_sta_SLR0_Sem_Act_Shift                                                            16

/*--------------------------------------
BitField Name: UnCor_St
BitField Type: R_O
BitField Desc: UnCor_St: Set 1 indicate error uncorrectable
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_sem_ctrl_cur_sta_UnCor_St_Mask                                                             cBit11
#define cAf6_sem_ctrl_cur_sta_UnCor_St_Shift                                                                11

/*--------------------------------------
BitField Name: status_idle
BitField Type: R_O
BitField Desc: status_idle: Set 1 indicate status FSM of SEM IP is IDLE
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_sem_ctrl_cur_sta_status_idle_Mask                                                           cBit6
#define cAf6_sem_ctrl_cur_sta_status_idle_Shift                                                              6

/*--------------------------------------
BitField Name: status_Initialization
BitField Type: R_O
BitField Desc: status_Initialization: Set 1 indicate status FSM of SEM IP is
INIITIALIZATION
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_sem_ctrl_cur_sta_status_Initialization_Mask                                                 cBit5
#define cAf6_sem_ctrl_cur_sta_status_Initialization_Shift                                                    5

/*--------------------------------------
BitField Name: status_injection
BitField Type: R_O
BitField Desc: status_injection: Set 1 indicate status FSM of SEM IP is
injection
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_sem_ctrl_cur_sta_status_injection_Mask                                                      cBit4
#define cAf6_sem_ctrl_cur_sta_status_injection_Shift                                                         4

/*--------------------------------------
BitField Name: status_classification
BitField Type: R_O
BitField Desc: status_classification: Set 1 indicate status FSM of SEM IP is
Classification
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_sem_ctrl_cur_sta_status_classification_Mask                                                 cBit3
#define cAf6_sem_ctrl_cur_sta_status_classification_Shift                                                    3

/*--------------------------------------
BitField Name: fatal_sem_err
BitField Type: R_O
BitField Desc: fatal_sem_err: Set 1 indicate status FSM of SEM IP is Falal Error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_sem_ctrl_cur_sta_fatal_sem_err_Mask                                                         cBit2
#define cAf6_sem_ctrl_cur_sta_fatal_sem_err_Shift                                                            2

/*--------------------------------------
BitField Name: status_observation
BitField Type: R_O
BitField Desc: status_observation: Set 1 indicate status FSM of SEM IP is
OBSERVATION
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_sem_ctrl_cur_sta_status_observation_Mask                                                    cBit1
#define cAf6_sem_ctrl_cur_sta_status_observation_Shift                                                       1

/*--------------------------------------
BitField Name: status_correction
BitField Type: R_O
BitField Desc: status_correction: Set 1 indicate status FSM of SEM IP is
Correction
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_sem_ctrl_cur_sta_status_correction_Mask                                                     cBit0
#define cAf6_sem_ctrl_cur_sta_status_correction_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : SEU Interrupt enable
Reg Addr   : 0x004-0x204
Reg Formula: 0x004+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
SEM controller enable interrupt of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_int_enb_Base                                                                         0x004

/*--------------------------------------
BitField Name: slr2_status_heartbeat
BitField Type: R/W
BitField Desc: Set 1 to SLR2 enable heartbeat interrupt
BitField Bits: [26:26]
--------------------------------------*/
#define cAf6_sem_int_enb_slr2_status_heartbeat_Mask                                                     cBit26
#define cAf6_sem_int_enb_slr2_status_heartbeat_Shift                                                        26

/*--------------------------------------
BitField Name: slr1_status_heartbeat
BitField Type: R/W
BitField Desc: Set 1 to SLR1 enable heartbeat interrupt
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_sem_int_enb_slr1_status_heartbeat_Mask                                                     cBit25
#define cAf6_sem_int_enb_slr1_status_heartbeat_Shift                                                        25

/*--------------------------------------
BitField Name: slr0_status_heartbeat
BitField Type: R/W
BitField Desc: Set 1 to SLR0 enable heartbeat interrupt
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_sem_int_enb_slr0_status_heartbeat_Mask                                                     cBit24
#define cAf6_sem_int_enb_slr0_status_heartbeat_Shift                                                        24

/*--------------------------------------
BitField Name: status_uncorrectable
BitField Type: R/W
BitField Desc: Set 1 to enable uncorrectable interrupt
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_sem_int_enb_status_uncorrectable_Mask                                                      cBit11
#define cAf6_sem_int_enb_status_uncorrectable_Shift                                                         11

/*--------------------------------------
BitField Name: status_correctable
BitField Type: R/W
BitField Desc: Set 1 to enable correctable interrupt
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_sem_int_enb_status_correctable_Mask                                                        cBit10
#define cAf6_sem_int_enb_status_correctable_Shift                                                           10

/*--------------------------------------
BitField Name: status_essential
BitField Type: R/W
BitField Desc: Set 1 to enable essential interrupt
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_sem_int_enb_status_essential_Mask                                                           cBit8
#define cAf6_sem_int_enb_status_essential_Shift                                                              8

/*--------------------------------------
BitField Name: status_idle
BitField Type: R/W
BitField Desc: Set 1 to enable idle interrupt
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_sem_int_enb_status_idle_Mask                                                                cBit6
#define cAf6_sem_int_enb_status_idle_Shift                                                                   6

/*--------------------------------------
BitField Name: status_initialization
BitField Type: R/W
BitField Desc: Set 1 to enable initialization interrupt
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_sem_int_enb_status_initialization_Mask                                                      cBit5
#define cAf6_sem_int_enb_status_initialization_Shift                                                         5

/*--------------------------------------
BitField Name: status_injection
BitField Type: R/W
BitField Desc: Set 1 to enable injection interrupt
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_sem_int_enb_status_injection_Mask                                                           cBit4
#define cAf6_sem_int_enb_status_injection_Shift                                                              4

/*--------------------------------------
BitField Name: status_classification
BitField Type: R/W
BitField Desc: Set 1 to enable classification interrupt
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_sem_int_enb_status_classification_Mask                                                      cBit3
#define cAf6_sem_int_enb_status_classification_Shift                                                         3

/*--------------------------------------
BitField Name: Fatal_Sem_err
BitField Type: R/W
BitField Desc: Set 1 to enable fatal interrupt
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_sem_int_enb_Fatal_Sem_err_Mask                                                              cBit2
#define cAf6_sem_int_enb_Fatal_Sem_err_Shift                                                                 2

/*--------------------------------------
BitField Name: status_observation
BitField Type: R/W
BitField Desc: Set 1 to enable observation interrupt
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_sem_int_enb_status_observation_Mask                                                         cBit1
#define cAf6_sem_int_enb_status_observation_Shift                                                            1

/*--------------------------------------
BitField Name: CorSt
BitField Type: R/W
BitField Desc: Set 1 to enable correction interrupt
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_sem_int_enb_CorSt_Mask                                                                      cBit0
#define cAf6_sem_int_enb_CorSt_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : SEU Essential Counter R2C
Reg Addr   : 0x008-0x208
Reg Formula: 0x008+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
SEU Essential Counter R2C of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_essen_cnt_r2c_Base                                                                   0x008

/*--------------------------------------
BitField Name: Sem_Essential_Cnt_r2c
BitField Type: R2C
BitField Desc: Sem_Essential_Cnt: Counter Essential of SEM IP
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_sem_essen_cnt_r2c_Sem_Essential_Cnt_r2c_Mask                                              cBit7_0
#define cAf6_sem_essen_cnt_r2c_Sem_Essential_Cnt_r2c_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : SEU Essential Counter RO
Reg Addr   : 0x009-0x209
Reg Formula: 0x009+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
SEU Essential Counter RO of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_essen_cnt_ro_Base                                                                    0x009

/*--------------------------------------
BitField Name: Sem_Essential_Cnt_ro
BitField Type: RO
BitField Desc: Sem_Essential_Cnt: Counter Essential of SEM IP
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_sem_essen_cnt_ro_Sem_Essential_Cnt_ro_Mask                                                cBit7_0
#define cAf6_sem_essen_cnt_ro_Sem_Essential_Cnt_ro_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : SEU Uncorrectable Counter R2C
Reg Addr   : 0x00A-0x20A
Reg Formula: 0x00A+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
SEU Uncorrectable Counter R2C of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_uncorr_cnt_r2c_Base                                                                  0x00A

/*--------------------------------------
BitField Name: Sem_UnCorrectable_Cnt_r2c
BitField Type: R2C
BitField Desc: Sem_UnCorrectable_Cnt: Counter UnCorrectable of SEM IP
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_sem_uncorr_cnt_r2c_Sem_UnCorrectable_Cnt_r2c_Mask                                         cBit7_0
#define cAf6_sem_uncorr_cnt_r2c_Sem_UnCorrectable_Cnt_r2c_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : SEU Uncorrectable Counter RO
Reg Addr   : 0x00B-0x20B
Reg Formula: 0x00B+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
SEU Uncorrectable Counter RO of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_uncorr_cnt_ro_Base                                                                   0x00B

/*--------------------------------------
BitField Name: Sem_UnCorrectable_Cnt_ro
BitField Type: RO
BitField Desc: Sem_UnCorrectable_Cnt: Counter UnCorrectable of SEM IP
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_sem_uncorr_cnt_ro_Sem_UnCorrectable_Cnt_ro_Mask                                           cBit7_0
#define cAf6_sem_uncorr_cnt_ro_Sem_UnCorrectable_Cnt_ro_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : SEU Correctable Counter R2C
Reg Addr   : 0x00C-0x20C
Reg Formula: 0x00C+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
SEU Correctable Counter R2C of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_corr_cnt_r2c_Base                                                                    0x00C

/*--------------------------------------
BitField Name: Sem_Correctable_Cnt_r2c
BitField Type: R2C
BitField Desc: Sem_Correctable_Cnt: Counter Correctable of SEM IP
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_sem_corr_cnt_r2c_Sem_Correctable_Cnt_r2c_Mask                                             cBit7_0
#define cAf6_sem_corr_cnt_r2c_Sem_Correctable_Cnt_r2c_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : SEU Correctable Counter RO
Reg Addr   : 0x00D-0x20D
Reg Formula: 0x00D+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
SEU Correctable Counter RO of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_corr_cnt_ro_Base                                                                     0x00D

/*--------------------------------------
BitField Name: Sem_Correctable_Cnt_ro
BitField Type: RO
BitField Desc: Sem_Correctable_Cnt: Counter Correctable of SEM IP
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_sem_corr_cnt_ro_Sem_Correctable_Cnt_ro_Mask                                               cBit7_0
#define cAf6_sem_corr_cnt_ro_Sem_Correctable_Cnt_ro_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : SEM SLRID
Reg Addr   : 0x010-0x210
Reg Formula: 0x010+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
Read HW SLR of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_slrid_Base                                                                           0x010

/*--------------------------------------
BitField Name: slrid
BitField Type: R_O
BitField Desc: HW SLR ID
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_sem_slrid_slrid_Mask                                                                      cBit1_0
#define cAf6_sem_slrid_slrid_Shift                                                                           0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Status FiFo TX Side
Reg Addr   : 0x011-0x211
Reg Formula: 0x011+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
Monitor Interface bus at TX side of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_mon_sta_Base                                                                         0x011

/*--------------------------------------
BitField Name: mon_tx_ctrl_trig
BitField Type: R/W
BitField Desc: Trigger 0->1 to start reseting FiFo
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_sem_mon_sta_mon_tx_ctrl_trig_Mask                                                          cBit31
#define cAf6_sem_mon_sta_mon_tx_ctrl_trig_Shift                                                             31

/*--------------------------------------
BitField Name: mon_tx_eful_stk
BitField Type: W1C
BitField Desc: Sticky Commnon TX fifo is early full
BitField Bits: [23:23]
--------------------------------------*/
#define cAf6_sem_mon_sta_mon_tx_eful_stk_Mask                                                           cBit23
#define cAf6_sem_mon_sta_mon_tx_eful_stk_Shift                                                              23

/*--------------------------------------
BitField Name: mon_tx_full_stk
BitField Type: W1C
BitField Desc: Sticky Common TX fifo is full
BitField Bits: [22:22]
--------------------------------------*/
#define cAf6_sem_mon_sta_mon_tx_full_stk_Mask                                                           cBit22
#define cAf6_sem_mon_sta_mon_tx_full_stk_Shift                                                              22

/*--------------------------------------
BitField Name: mon_tx_ept_stk
BitField Type: W1C
BitField Desc: Sticky Common TX fifo is empty
BitField Bits: [21:21]
--------------------------------------*/
#define cAf6_sem_mon_sta_mon_tx_ept_stk_Mask                                                            cBit21
#define cAf6_sem_mon_sta_mon_tx_ept_stk_Shift                                                               21

/*--------------------------------------
BitField Name: mon_tx_nept_stk
BitField Type: W1C
BitField Desc: Sticky Common TX fifo is not empty
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_sem_mon_sta_mon_tx_nept_stk_Mask                                                           cBit20
#define cAf6_sem_mon_sta_mon_tx_nept_stk_Shift                                                              20

/*--------------------------------------
BitField Name: mon_tx_eful_cur
BitField Type: R_O
BitField Desc: Current Commnon TX fifo is early full
BitField Bits: [19:19]
--------------------------------------*/
#define cAf6_sem_mon_sta_mon_tx_eful_cur_Mask                                                           cBit19
#define cAf6_sem_mon_sta_mon_tx_eful_cur_Shift                                                              19

/*--------------------------------------
BitField Name: mon_tx_full_cur
BitField Type: R_O
BitField Desc: Current Common TX fifo is full
BitField Bits: [18:18]
--------------------------------------*/
#define cAf6_sem_mon_sta_mon_tx_full_cur_Mask                                                           cBit18
#define cAf6_sem_mon_sta_mon_tx_full_cur_Shift                                                              18

/*--------------------------------------
BitField Name: mon_tx_ept_cur
BitField Type: R_O
BitField Desc: Current Common TX fifo is empty
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_sem_mon_sta_mon_tx_ept_cur_Mask                                                            cBit17
#define cAf6_sem_mon_sta_mon_tx_ept_cur_Shift                                                               17

/*--------------------------------------
BitField Name: mon_tx_nept_cur
BitField Type: R_O
BitField Desc: Current Common TX fifo is not empty
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_sem_mon_sta_mon_tx_nept_cur_Mask                                                           cBit16
#define cAf6_sem_mon_sta_mon_tx_nept_cur_Shift                                                              16

/*--------------------------------------
BitField Name: mon_tx_len_cur
BitField Type: R_O
BitField Desc: Current length of Common fifo
BitField Bits: [11:00]
--------------------------------------*/
#define cAf6_sem_mon_sta_mon_tx_len_cur_Mask                                                          cBit11_0
#define cAf6_sem_mon_sta_mon_tx_len_cur_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Value FiFo TX Side
Reg Addr   : 0x012-0x212
Reg Formula: 0x012+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
Monitor Interface bus at TX side of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_mon_val_Base                                                                         0x012

/*--------------------------------------
BitField Name: mon_tx_val
BitField Type: R_O
BitField Desc: Status of SEM controller, ASCII format, you have to monitor
status of FiFo TX to get valid value
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_sem_mon_val_mon_tx_val_Mask                                                               cBit7_0
#define cAf6_sem_mon_val_mon_tx_val_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Control RX Side
Reg Addr   : 0x013-0x213
Reg Formula: 0x013+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
Monitor Interface bus at RX side of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_mon_ctrl_Base                                                                        0x013

/*--------------------------------------
BitField Name: mon_rx_ctrl_done
BitField Type: W1C
BitField Desc: Trigger is done
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_sem_mon_ctrl_mon_rx_ctrl_done_Mask                                                          cBit9
#define cAf6_sem_mon_ctrl_mon_rx_ctrl_done_Shift                                                             9

/*--------------------------------------
BitField Name: mon_rx_ctrl_trig
BitField Type: R/W
BitField Desc: Trigger 1->0 to HW start sending command to SEM controller via
Monitor interface
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_sem_mon_ctrl_mon_rx_ctrl_trig_Mask                                                          cBit8
#define cAf6_sem_mon_ctrl_mon_rx_ctrl_trig_Shift                                                             8

/*--------------------------------------
BitField Name: mon_rx_ctrl_len
BitField Type: R/W
BitField Desc: Command length
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_sem_mon_ctrl_mon_rx_ctrl_len_Mask                                                         cBit3_0
#define cAf6_sem_mon_ctrl_mon_rx_ctrl_len_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Command Value Part0 RX Side
Reg Addr   : 0x014-0x214
Reg Formula: 0x014+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
Monitor Interface bus at RX side of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_mon_cmd_part0_Base                                                                   0x014

/*--------------------------------------
BitField Name: mon_rx_cmd_val0
BitField Type: R/W
BitField Desc: Command value part0, this is MSB, ASCII format
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_sem_mon_cmd_part0_mon_rx_cmd_val0_Mask                                                   cBit31_0
#define cAf6_sem_mon_cmd_part0_mon_rx_cmd_val0_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Command Value Part1 RX Side
Reg Addr   : 0x015-0x215
Reg Formula: 0x015+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
Monitor Interface bus at RX side of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_mon_cmd_part1_Base                                                                   0x015

/*--------------------------------------
BitField Name: mon_rx_cmd_val1
BitField Type: R/W
BitField Desc: Command value part1, it is followed Part0, ASCII format
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_sem_mon_cmd_part1_mon_rx_cmd_val1_Mask                                                   cBit31_0
#define cAf6_sem_mon_cmd_part1_mon_rx_cmd_val1_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Command Value Part2 RX Side
Reg Addr   : 0x016-0x216
Reg Formula: 0x016+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
Monitor Interface bus at RX side of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_mon_cmd_part2_Base                                                                   0x016

/*--------------------------------------
BitField Name: mon_rx_cmd_val2
BitField Type: R/W
BitField Desc: Command value part2, it is followed Part1, ASCII format
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_sem_mon_cmd_part2_mon_rx_cmd_val2_Mask                                                   cBit31_0
#define cAf6_sem_mon_cmd_part2_mon_rx_cmd_val2_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : SEM Monitor Interface Command Value Part3 RX Side
Reg Addr   : 0x017-0x217
Reg Formula: 0x017+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
Monitor Interface bus at RX side of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_mon_cmd_part3_Base                                                                   0x017

/*--------------------------------------
BitField Name: mon_rx_cmd_val3
BitField Type: R/W
BitField Desc: Command value part3, it is followed Part2, this is LSB part,
ASCII format
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_sem_mon_cmd_part3_mon_rx_cmd_val3_Mask                                                   cBit31_0
#define cAf6_sem_mon_cmd_part3_mon_rx_cmd_val3_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : SEM Heartbeat Status
Reg Addr   : 0x018-0x218
Reg Formula: 0x018+$slrid*0x100
    Where  : 
           + $slrid(0) : Hardware SLR
Reg Desc   : 
Heartbeat status of each SLR

------------------------------------------------------------------------------*/
#define cAf6Reg_sem_hrtb_sta_Base                                                                        0x018

/*--------------------------------------
BitField Name: slr2_hrtb_err_obsv
BitField Type: W1C
BitField Desc: SLR2 Heartbeat is observation error
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_sem_hrtb_sta_slr2_hrtb_err_obsv_Mask                                                       cBit10
#define cAf6_sem_hrtb_sta_slr2_hrtb_err_obsv_Shift                                                          10

/*--------------------------------------
BitField Name: slr2_hrtb_err_diag
BitField Type: W1C
BitField Desc: SLR2 Heartbeat is diagnostic error
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_sem_hrtb_sta_slr2_hrtb_err_diag_Mask                                                        cBit9
#define cAf6_sem_hrtb_sta_slr2_hrtb_err_diag_Shift                                                           9

/*--------------------------------------
BitField Name: slr2_hrtb_err_dete
BitField Type: W1C
BitField Desc: SLR2 Heartbeat is detect-only error
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_sem_hrtb_sta_slr2_hrtb_err_dete_Mask                                                        cBit8
#define cAf6_sem_hrtb_sta_slr2_hrtb_err_dete_Shift                                                           8

/*--------------------------------------
BitField Name: slr1_hrtb_err_obsv
BitField Type: W1C
BitField Desc: SLR1 Heartbeat is observation error
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_sem_hrtb_sta_slr1_hrtb_err_obsv_Mask                                                        cBit6
#define cAf6_sem_hrtb_sta_slr1_hrtb_err_obsv_Shift                                                           6

/*--------------------------------------
BitField Name: slr1_hrtb_err_diag
BitField Type: W1C
BitField Desc: SLR1 Heartbeat is diagnostic error
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_sem_hrtb_sta_slr1_hrtb_err_diag_Mask                                                        cBit5
#define cAf6_sem_hrtb_sta_slr1_hrtb_err_diag_Shift                                                           5

/*--------------------------------------
BitField Name: slr1_hrtb_err_dete
BitField Type: W1C
BitField Desc: SLR1 Heartbeat is detect-only error
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_sem_hrtb_sta_slr1_hrtb_err_dete_Mask                                                        cBit4
#define cAf6_sem_hrtb_sta_slr1_hrtb_err_dete_Shift                                                           4

/*--------------------------------------
BitField Name: slr0_hrtb_err_obsv
BitField Type: W1C
BitField Desc: SLR0 Heartbeat is observation error
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_sem_hrtb_sta_slr0_hrtb_err_obsv_Mask                                                        cBit2
#define cAf6_sem_hrtb_sta_slr0_hrtb_err_obsv_Shift                                                           2

/*--------------------------------------
BitField Name: slr0_hrtb_err_diag
BitField Type: W1C
BitField Desc: SLR0 Heartbeat is diagnostic error
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_sem_hrtb_sta_slr0_hrtb_err_diag_Mask                                                        cBit1
#define cAf6_sem_hrtb_sta_slr0_hrtb_err_diag_Shift                                                           1

/*--------------------------------------
BitField Name: slr0_hrtb_err_dete
BitField Type: W1C
BitField Desc: SLR0 Heartbeat is detect-only error
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_sem_hrtb_sta_slr0_hrtb_err_dete_Mask                                                        cBit0
#define cAf6_sem_hrtb_sta_slr0_hrtb_err_dete_Shift                                                           0

#endif /* _AF6_REG_AF6CNC0021_RD_SEM_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PHYSICAL
 *
 * File        : Tha60290021ThermalSensor.c
 *
 * Created Date: Oct 3, 2016
 *
 * Description : Tha60290021ThermalSensor implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/physical/ThaThermalSensorInternal.h"
#include "../../../../generic/man/AtDriverInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021ThermalSensor
    {
    tThaThermalSensor super;
    }tTha60290021ThermalSensor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSensorMethods         m_AtSensorOverride;
static tThaThermalSensorMethods m_ThaThermalSensorOverride;

/* Save super implementation */
static const tAtSensorMethods *m_AtSensorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(ThaThermalSensor self)
    {
    AtUnused(self);
    return 0xF44000;
    }

static eAtRet Init(AtSensor self)
    {
    AtThermalSensor sensor = (AtThermalSensor)self;
    eAtRet ret;

    ret = m_AtSensorMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* Note, below default settings are made on several boards and these thresholds
     * should be explicitly controlled by application. */
    ret |= AtThermalSensorAlarmSetThresholdSet(sensor, 90);
    ret |= AtThermalSensorAlarmClearThresholdSet(sensor, 49);

    return ret;
    }

static void OverrideAtSensor(AtThermalSensor self)
    {
    AtSensor sensor = (AtSensor)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSensorMethods = mMethodsGet(sensor);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSensorOverride, m_AtSensorMethods, sizeof(m_AtSensorOverride));

        mMethodOverride(m_AtSensorOverride, Init);
        }

    mMethodsSet(sensor, &m_AtSensorOverride);
    }

static void OverrideThaThermalSensor(AtThermalSensor self)
    {
    ThaThermalSensor sensor = (ThaThermalSensor)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_ThaThermalSensorOverride, 0, sizeof(m_ThaThermalSensorOverride));

        /* Setup methods */
        mMethodOverride(m_ThaThermalSensorOverride, BaseAddress);
        }

    mMethodsSet(sensor, &m_ThaThermalSensorOverride);
    }

static void Override(AtThermalSensor self)
    {
    OverrideAtSensor(self);
    OverrideThaThermalSensor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ThermalSensor);
    }

static AtThermalSensor ObjectInit(AtThermalSensor self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaThermalSensorObjectInit(self, device) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtThermalSensor Tha60290021ThermalSensorNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtThermalSensor newSensor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newSensor == NULL)
        return NULL;

    return ObjectInit(newSensor, device);
    }

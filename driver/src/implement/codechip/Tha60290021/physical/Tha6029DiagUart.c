/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha6029DiagUart.c
 *
 * Created Date: Sep 25, 2016
 *
 * Description : UART
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtUartInternal.h"
#include "../../../../generic/man/AtIpCoreInternal.h"
#include "../man/Tha60290021DeviceReg.h"
#include "Tha6029Physical.h"

/*--------------------------- Define -----------------------------------------*/
#define cUartOneByteLength 1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6029DiagUart
    {
    tAtUart super;
    }tTha6029DiagUart;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtUartMethods m_AtUartOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 RegisterWithLocalAddress(AtUart self, uint32 localAddress)
    {
    AtUnused(self);
    return localAddress + cTopBaseAddress;
    }

static eAtRet HwUartSend(AtUart self, const char* buffer, uint32 bufferSize)
    {
    AtIpCore core = AtDeviceIpCoreGet(AtUartDeviceGet(self), 0);
    uint32 regAddr = RegisterWithLocalAddress(self, cAf6Reg_o_control0_Base);
    uint32 regVal = AtIpCoreRead(core, regAddr);
    AtUnused(bufferSize);

    mRegFieldSet(regVal, cAf6_o_control0_o_control0_,  (uint8)buffer[0]);
    AtIpCoreWrite(core, regAddr, regVal);
    return cAtOk;
    }

static uint32 HwUartReceive(AtUart self, char* buffer, uint32 bufferSize)
    {
    AtIpCore core = AtDeviceIpCoreGet(AtUartDeviceGet(self), 0);
    uint32 regVal = AtIpCoreRead(core, RegisterWithLocalAddress(self, cAf6Reg_c_uart_Base));
    AtUnused(bufferSize);

    buffer[0] = (char)(regVal & cBit7_0);
    return cUartOneByteLength;
    }

static eAtRet Transmit(AtUart self, const char *buffer, uint32 bufferSize)
    {
    if (!buffer)
        return cAtErrorNullPointer;

    if (bufferSize == cUartOneByteLength)
        return HwUartSend(self, buffer, bufferSize);

    return cAtErrorOutOfRangParm;
    }

static uint32 Receive(AtUart self, char *buffer, uint32 bufferSize)
    {
    if (!buffer)
        return cAtErrorNullPointer;

    if (bufferSize < cUartOneByteLength)
        return 0;

    return HwUartReceive(self, buffer, bufferSize);
    }

static void OverrideAtUart(AtUart self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtUartOverride, mMethodsGet(self), sizeof(m_AtUartOverride));

        mMethodOverride(m_AtUartOverride, Transmit);
        mMethodOverride(m_AtUartOverride, Receive);
        }

    mMethodsSet(self, &m_AtUartOverride);
    }

static void Override(AtUart self)
    {
    OverrideAtUart(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029DiagUart);
    }

static AtUart ObjectInit(AtUart self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtUartObjectInit(self, device, 0) == NULL)
        return NULL;

    /* Setup methods */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtUart Tha6029DiagUartNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtUart controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (controller == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(controller, device);
    }

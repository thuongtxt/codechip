/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_ETH40G_RD_H_
#define _AF6_REG_AF6CNC0021_ETH40G_RD_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G DRP
Reg Addr   : 0x1000-0x1FFF
Reg Formula: 0x1000+$P*0x400+$DRP
    Where  :
           + $P(0-3) : Lane ID
           + $DRP(0-1023) : DRP address, see UG578
Reg Desc   :
Read/Write DRP address of SERDES

------------------------------------------------------------------------------*/
#define cAf6Reg_OETH_40G_DRP_Base                                                                       0x1000

/*--------------------------------------
BitField Name: drp_rw
BitField Type: R/W
BitField Desc: DRP read/write value
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_OETH_40G_DRP_drp_rw_Mask                                                                  cBit9_0
#define cAf6_OETH_40G_DRP_drp_rw_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G LoopBack
Reg Addr   : 0x0002
Reg Formula:
    Where  :
Reg Desc   :
Configurate LoopBack

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_LoopBack_Base                                                                   0x0002

/*--------------------------------------
BitField Name: lpback_lane3
BitField Type: R/W
BitField Desc: Loopback lane3
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ETH_40G_LoopBack_lpback_lane3_Mask                                                      cBit15_12
#define cAf6_ETH_40G_LoopBack_lpback_lane3_Shift                                                            12

/*--------------------------------------
BitField Name: lpback_lane2
BitField Type: R/W
BitField Desc: Loopback lane2
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_ETH_40G_LoopBack_lpback_lane2_Mask                                                       cBit11_8
#define cAf6_ETH_40G_LoopBack_lpback_lane2_Shift                                                             8

/*--------------------------------------
BitField Name: lpback_lane1
BitField Type: R/W
BitField Desc: Loopback lane1
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_ETH_40G_LoopBack_lpback_lane1_Mask                                                        cBit7_4
#define cAf6_ETH_40G_LoopBack_lpback_lane1_Shift                                                             4

/*--------------------------------------
BitField Name: lpback_lane0
BitField Type: R/W
BitField Desc: Loopback lane0
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ETH_40G_LoopBack_lpback_lane0_Mask                                                        cBit3_0
#define cAf6_ETH_40G_LoopBack_lpback_lane0_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G QLL Status
Reg Addr   : 0x000B
Reg Formula:
    Where  :
Reg Desc   :
QPLL status

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_QLL_Status_Base                                                                 0x000B

/*--------------------------------------
BitField Name: QPLL1_Lock_change
BitField Type: W1C
BitField Desc: QPLL1 has transition lock/unlock, Group 0-3
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_ETH_40G_QLL_Status_QPLL1_Lock_change_Mask                                                  cBit29
#define cAf6_ETH_40G_QLL_Status_QPLL1_Lock_change_Shift                                                     29

/*--------------------------------------
BitField Name: QPLL0_Lock_change
BitField Type: W1C
BitField Desc: QPLL0 has transition lock/unlock, Group 0-3
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_ETH_40G_QLL_Status_QPLL0_Lock_change_Mask                                                  cBit28
#define cAf6_ETH_40G_QLL_Status_QPLL0_Lock_change_Shift                                                     28

/*--------------------------------------
BitField Name: QPLL1_Lock
BitField Type: R_O
BitField Desc: QPLL0 is Locked, Group 0-3
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_ETH_40G_QLL_Status_QPLL1_Lock_Mask                                                         cBit25
#define cAf6_ETH_40G_QLL_Status_QPLL1_Lock_Shift                                                            25

/*--------------------------------------
BitField Name: QPLL0_Lock
BitField Type: R_O
BitField Desc: QPLL0 is Locked, Group 0-3
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_ETH_40G_QLL_Status_QPLL0_Lock_Mask                                                         cBit24
#define cAf6_ETH_40G_QLL_Status_QPLL0_Lock_Shift                                                            24


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G TX Reset
Reg Addr   : 0x000C
Reg Formula:
    Where  :
Reg Desc   :
Reset TX SERDES

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_TX_Reset_Base                                                                   0x000C

/*--------------------------------------
BitField Name: txrst_done
BitField Type: W1C
BitField Desc: TX Reset Done
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_ETH_40G_TX_Reset_txrst_done_Mask                                                           cBit16
#define cAf6_ETH_40G_TX_Reset_txrst_done_Shift                                                              16

/*--------------------------------------
BitField Name: txrst_trig
BitField Type: R/W
BitField Desc: Trige 0->1 to start reset TX SERDES
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_40G_TX_Reset_txrst_trig_Mask                                                            cBit0
#define cAf6_ETH_40G_TX_Reset_txrst_trig_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G RX Reset
Reg Addr   : 0x000D
Reg Formula:
    Where  :
Reg Desc   :
Reset RX SERDES

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_49G_RX_Reset_Base                                                                   0x000D

/*--------------------------------------
BitField Name: rxrst_done
BitField Type: W1C
BitField Desc: RX Reset Done
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_ETH_49G_RX_Reset_rxrst_done_Mask                                                           cBit16
#define cAf6_ETH_49G_RX_Reset_rxrst_done_Shift                                                              16

/*--------------------------------------
BitField Name: rxrst_trig
BitField Type: R/W
BitField Desc: Trige 0->1 to start reset RX SERDES
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_49G_RX_Reset_rxrst_trig_Mask                                                            cBit0
#define cAf6_ETH_49G_RX_Reset_rxrst_trig_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G LPMDFE Mode
Reg Addr   : 0x000E
Reg Formula:
    Where  :
Reg Desc   :
Configure LPM/DFE mode

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_LPMDFE_Mode_Base                                                                0x000E

/*--------------------------------------
BitField Name: lpmdfe_mode
BitField Type: R/W
BitField Desc: LPM/DFE mode
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_40G_LPMDFE_Mode_lpmdfe_mode_Mask                                                        cBit0
#define cAf6_ETH_40G_LPMDFE_Mode_lpmdfe_mode_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G LPMDFE Reset
Reg Addr   : 0x000F
Reg Formula:
    Where  :
Reg Desc   :
Reset LPM/DFE

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_LPMDFE_Reset_Base                                                               0x000F

/*--------------------------------------
BitField Name: lpmdfe_reset
BitField Type: R/W
BitField Desc: LPM/DFE reset
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_40G_LPMDFE_Reset_lpmdfe_reset_Mask                                                      cBit0
#define cAf6_ETH_40G_LPMDFE_Reset_lpmdfe_reset_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G TXDIFFCTRL
Reg Addr   : 0x0010
Reg Formula:
    Where  :
Reg Desc   :
Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_TXDIFFCTRL_Base                                                                 0x0010

/*--------------------------------------
BitField Name: TXDIFFCTRL
BitField Type: R/W
BitField Desc: TXDIFFCTRL
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_ETH_40G_TXDIFFCTRL_TXDIFFCTRL_Mask                                                        cBit4_0
#define cAf6_ETH_40G_TXDIFFCTRL_TXDIFFCTRL_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G TXPOSTCURSOR
Reg Addr   : 0x0011
Reg Formula:
    Where  :
Reg Desc   :
Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_TXPOSTCURSOR_Base                                                               0x0011

/*--------------------------------------
BitField Name: TXPOSTCURSOR
BitField Type: R/W
BitField Desc: TXPOSTCURSOR
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_ETH_40G_TXPOSTCURSOR_TXPOSTCURSOR_Mask                                                    cBit4_0
#define cAf6_ETH_40G_TXPOSTCURSOR_TXPOSTCURSOR_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G TXPRECURSOR
Reg Addr   : 0x0012
Reg Formula:
    Where  :
Reg Desc   :
Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_TXPRECURSOR_Base                                                                0x0012

/*--------------------------------------
BitField Name: TXPRECURSOR
BitField Type: R/W
BitField Desc: TXPRECURSOR
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_ETH_40G_TXPRECURSOR_TXPRECURSOR_Mask                                                      cBit4_0
#define cAf6_ETH_40G_TXPRECURSOR_TXPRECURSOR_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Ctrl FCS
Reg Addr   : 0x0080
Reg Formula:
    Where  :
Reg Desc   :
configure FCS mode

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Ctrl_FCS_Base                                                                   0x0080

/*--------------------------------------
BitField Name: txfcs_ignore
BitField Type: R/W
BitField Desc: TX ignore check FCS when txfcs_ins is low
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_40G_Ctrl_FCS_txfcs_ignore_Mask                                                          cBit5
#define cAf6_ETH_40G_Ctrl_FCS_txfcs_ignore_Shift                                                             5

/*--------------------------------------
BitField Name: txfcs_ins
BitField Type: R/W
BitField Desc: TX inserts 4bytes FCS
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_40G_Ctrl_FCS_txfcs_ins_Mask                                                             cBit4
#define cAf6_ETH_40G_Ctrl_FCS_txfcs_ins_Shift                                                                4

/*--------------------------------------
BitField Name: rxfcs_ignore
BitField Type: R/W
BitField Desc: RX ignore check FCS
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_40G_Ctrl_FCS_rxfcs_ignore_Mask                                                          cBit1
#define cAf6_ETH_40G_Ctrl_FCS_rxfcs_ignore_Shift                                                             1

/*--------------------------------------
BitField Name: rxfcs_rmv
BitField Type: R/W
BitField Desc: RX remove 4bytes FCS
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_40G_Ctrl_FCS_rxfcs_rmv_Mask                                                             cBit0
#define cAf6_ETH_40G_Ctrl_FCS_rxfcs_rmv_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G AutoNeg
Reg Addr   : 0x0081
Reg Formula:
    Where  :
Reg Desc   :
configure Auto-Neg

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_AutoNeg_Base                                                                    0x0081

/*--------------------------------------
BitField Name: fec_rx_enb
BitField Type: R/W
BitField Desc: FEC RX enable
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_ETH_40G_AutoNeg_fec_rx_enb_Mask                                                            cBit29
#define cAf6_ETH_40G_AutoNeg_fec_rx_enb_Shift                                                               29

/*--------------------------------------
BitField Name: fec_tx_enb
BitField Type: R/W
BitField Desc: FEC TX enable
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_ETH_40G_AutoNeg_fec_tx_enb_Mask                                                            cBit28
#define cAf6_ETH_40G_AutoNeg_fec_tx_enb_Shift                                                               28

/*--------------------------------------
BitField Name: an_pseudo_sel
BitField Type: R/W
BitField Desc: Selects the polynomial generator for the bit 49 random bit
generator
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_ETH_40G_AutoNeg_an_pseudo_sel_Mask                                                         cBit24
#define cAf6_ETH_40G_AutoNeg_an_pseudo_sel_Shift                                                            24

/*--------------------------------------
BitField Name: an_nonce_seed
BitField Type: R/W
BitField Desc: 8-bit seed to initialize the nonce field polynomial generator.
Non-zero.
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_ETH_40G_AutoNeg_an_nonce_seed_Mask                                                      cBit23_16
#define cAf6_ETH_40G_AutoNeg_an_nonce_seed_Shift                                                            16

/*--------------------------------------
BitField Name: an_lt_sta
BitField Type: R/W
BitField Desc: Link Control outputs from the auto-negotiationcontroller for the
various Ethernet protocols.
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_ETH_40G_AutoNeg_an_lt_sta_Mask                                                            cBit9_8
#define cAf6_ETH_40G_AutoNeg_an_lt_sta_Shift                                                                 8

/*--------------------------------------
BitField Name: lt_restart
BitField Type: R/W
BitField Desc: This signal triggers a restart of link training regardless of the
current state.
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_40G_AutoNeg_lt_restart_Mask                                                             cBit5
#define cAf6_ETH_40G_AutoNeg_lt_restart_Shift                                                                5

/*--------------------------------------
BitField Name: lt_enb
BitField Type: R/W
BitField Desc: Enables link training. When link training is disabled, all PCS
lanes function in mission mode.
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_40G_AutoNeg_lt_enb_Mask                                                                 cBit4
#define cAf6_ETH_40G_AutoNeg_lt_enb_Shift                                                                    4

/*--------------------------------------
BitField Name: an_restart
BitField Type: R/W
BitField Desc: This input is used to trigger a restart of the auto-negotiation,
regardless of what state the circuit is currently in.
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_40G_AutoNeg_an_restart_Mask                                                             cBit2
#define cAf6_ETH_40G_AutoNeg_an_restart_Shift                                                                2

/*--------------------------------------
BitField Name: an_bypass
BitField Type: R/W
BitField Desc: Input to disable auto-negotiation and bypass the auto-negotiation
function. If this input is asserted, auto-negotiation is turned off, but the PCS
is connected to the output to allow operation.
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_40G_AutoNeg_an_bypass_Mask                                                              cBit1
#define cAf6_ETH_40G_AutoNeg_an_bypass_Shift                                                                 1

/*--------------------------------------
BitField Name: an_enb
BitField Type: R/W
BitField Desc: Enable signal for auto-negotiation
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_40G_AutoNeg_an_enb_Mask                                                                 cBit0
#define cAf6_ETH_40G_AutoNeg_an_enb_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Diag Ctrl0
Reg Addr   : 0x0020
Reg Formula:
    Where  :
Reg Desc   :
Diagnostic control 0

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Diag_ctrl0_Base                                                                 0x0020

/*--------------------------------------
BitField Name: diag_err
BitField Type: W1C
BitField Desc: Error detection
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl0_diag_err_Mask                                                           cBit24
#define cAf6_ETH_40G_Diag_ctrl0_diag_err_Shift                                                              24

/*--------------------------------------
BitField Name: diag_ferr
BitField Type: R/W
BitField Desc: Enable force error data of diagnostic packet
BitField Bits: [20:20]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl0_diag_ferr_Mask                                                          cBit20
#define cAf6_ETH_40G_Diag_ctrl0_diag_ferr_Shift                                                             20

/*--------------------------------------
BitField Name: diag_datmod
BitField Type: R/W
BitField Desc: payload mod of ethernet frame
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl0_diag_datmod_Mask                                                       cBit5_4
#define cAf6_ETH_40G_Diag_ctrl0_diag_datmod_Shift                                                            4

/*--------------------------------------
BitField Name: diag_enb
BitField Type: R/W
BitField Desc: enable diagnostic block
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl0_diag_enb_Mask                                                            cBit0
#define cAf6_ETH_40G_Diag_ctrl0_diag_enb_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Diag Ctrl1
Reg Addr   : 0x0021
Reg Formula:
    Where  :
Reg Desc   :
Diagnostic control 1

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Diag_ctrl1_Base                                                                 0x0021

/*--------------------------------------
BitField Name: diag_lenmax
BitField Type: R/W
BitField Desc: Maximum length of diagnostic packet, count from 0, min value is
63
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl1_diag_lenmax_Mask                                                     cBit31_16
#define cAf6_ETH_40G_Diag_ctrl1_diag_lenmax_Shift                                                           16

/*--------------------------------------
BitField Name: diag_lenmin
BitField Type: R/W
BitField Desc: Minimum length of diagnostic packet, count from 0, min value is
63
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl1_diag_lenmin_Mask                                                      cBit15_0
#define cAf6_ETH_40G_Diag_ctrl1_diag_lenmin_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Diag Ctrl2
Reg Addr   : 0x0022
Reg Formula:
    Where  :
Reg Desc   :
Diagnostic control 2

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Diag_ctrl2_Base                                                                 0x0022

/*--------------------------------------
BitField Name: diag_dalsb
BitField Type: R/W
BitField Desc: 32bit-LSB DA of diagnostic packet
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl2_diag_dalsb_Mask                                                       cBit31_0
#define cAf6_ETH_40G_Diag_ctrl2_diag_dalsb_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Diag Ctrl3
Reg Addr   : 0x0023
Reg Formula:
    Where  :
Reg Desc   :
Diagnostic control 3

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Diag_ctrl3_Base                                                                 0x0023

/*--------------------------------------
BitField Name: diag_salsb
BitField Type: R/W
BitField Desc: 32bit-LSB SA of diagnostic packet
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl3_diag_salsb_Mask                                                       cBit31_0
#define cAf6_ETH_40G_Diag_ctrl3_diag_salsb_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Diag Ctrl4
Reg Addr   : 0x0024
Reg Formula:
    Where  :
Reg Desc   :
Diagnostic control 4

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Diag_ctrl4_Base                                                                 0x0024

/*--------------------------------------
BitField Name: diag_damsb
BitField Type: R/W
BitField Desc: 16bit-MSB DA of diagnostic packet
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl4_diag_damsb_Mask                                                      cBit31_16
#define cAf6_ETH_40G_Diag_ctrl4_diag_damsb_Shift                                                            16

/*--------------------------------------
BitField Name: diag_samsb
BitField Type: R/W
BitField Desc: 16bit-MSB SA of diagnostic packet
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl4_diag_samsb_Mask                                                       cBit15_0
#define cAf6_ETH_40G_Diag_ctrl4_diag_samsb_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Diag Ctrl6
Reg Addr   : 0x0026
Reg Formula:
    Where  :
Reg Desc   :
Diagnostic control 7

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Diag_ctrl6_Base                                                                 0x0026

/*--------------------------------------
BitField Name: enb_type
BitField Type: RW
BitField Desc: Config enable insert type from CPU, (1) is enable, (0) is disable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl6_enb_type_Mask                                                           cBit17
#define cAf6_ETH_40G_Diag_ctrl6_enb_type_Shift                                                              17

/*--------------------------------------
BitField Name: type_cfg
BitField Type: RW
BitField Desc: value type that is configured
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl6_type_cfg_Mask                                                         cBit16_0
#define cAf6_ETH_40G_Diag_ctrl6_type_cfg_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Diag Ctrl7
Reg Addr   : 0x0027
Reg Formula:
    Where  :
Reg Desc   :
Diagnostic control 7

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Diag_ctrl7_Base                                                                 0x0027

/*--------------------------------------
BitField Name: Unsed
BitField Type: R/W
BitField Desc: Unsed
BitField Bits: [31:19]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl7_Unsed_Mask                                                           cBit31_19
#define cAf6_ETH_40G_Diag_ctrl7_Unsed_Shift                                                                 19

/*--------------------------------------
BitField Name: status_gatetime_diag
BitField Type: RO
BitField Desc: Status Gatetime diagnostic 1:Running 0:Done-Ready
BitField Bits: [18]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl7_status_gatetime_diag_Mask                                               cBit18
#define cAf6_ETH_40G_Diag_ctrl7_status_gatetime_diag_Shift                                                  18

/*--------------------------------------
BitField Name: start_gatetime_diag
BitField Type: RW
BitField Desc: Config start Diagnostic trigger 0 to 1 for Start auto run with
Gatetime Configuration
BitField Bits: [17]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl7_start_gatetime_diag_Mask                                                cBit17
#define cAf6_ETH_40G_Diag_ctrl7_start_gatetime_diag_Shift                                                   17

/*--------------------------------------
BitField Name: time_cfg
BitField Type: RW
BitField Desc: Gatetime Configuration 1-86400 second
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_ctrl7_time_cfg_Mask                                                         cBit16_0
#define cAf6_ETH_40G_Diag_ctrl7_time_cfg_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Gatetime Current
Reg Addr   : 0x28
Reg Formula:
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_Gatetime_current_Base                                                                     0x28

/*--------------------------------------
BitField Name: Unsed
BitField Type: R/W
BitField Desc: Unsed
BitField Bits: [31:17]
--------------------------------------*/
#define cAf6_Gatetime_current_Unsed_Mask                                                             cBit31_17
#define cAf6_Gatetime_current_Unsed_Shift                                                                   17

/*--------------------------------------
BitField Name: currert_gatetime_diag
BitField Type: RO
BitField Desc: Current running time of Gatetime diagnostic
BitField Bits: [16:0]
--------------------------------------*/
#define cAf6_Gatetime_current_currert_gatetime_diag_Mask                                              cBit16_0
#define cAf6_Gatetime_current_currert_gatetime_diag_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Diag Sta0
Reg Addr   : 0x0040
Reg Formula:
    Where  :
Reg Desc   :
Diagnostic Sta0

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Diag_Sta0_Base                                                                  0x0040

/*--------------------------------------
BitField Name: diag_txmis_sop
BitField Type: W1C
BitField Desc: Packet miss SOP
BitField Bits: [07:07]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_Sta0_diag_txmis_sop_Mask                                                       cBit7
#define cAf6_ETH_40G_Diag_Sta0_diag_txmis_sop_Shift                                                          7

/*--------------------------------------
BitField Name: diag_txmis_eop
BitField Type: W1C
BitField Desc: Packet miss EOP
BitField Bits: [06:06]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_Sta0_diag_txmis_eop_Mask                                                       cBit6
#define cAf6_ETH_40G_Diag_Sta0_diag_txmis_eop_Shift                                                          6

/*--------------------------------------
BitField Name: diag_txsop_eop
BitField Type: W1C
BitField Desc: Short packet, length is less than 16bytes
BitField Bits: [05:05]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_Sta0_diag_txsop_eop_Mask                                                       cBit5
#define cAf6_ETH_40G_Diag_Sta0_diag_txsop_eop_Shift                                                          5

/*--------------------------------------
BitField Name: diag_txwff_ful
BitField Type: W1C
BitField Desc: TX-Fifo is full
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_Sta0_diag_txwff_ful_Mask                                                       cBit4
#define cAf6_ETH_40G_Diag_Sta0_diag_txwff_ful_Shift                                                          4

/*--------------------------------------
BitField Name: diag_rxmis_sop
BitField Type: W1C
BitField Desc: Packet miss SOP
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_Sta0_diag_rxmis_sop_Mask                                                       cBit3
#define cAf6_ETH_40G_Diag_Sta0_diag_rxmis_sop_Shift                                                          3

/*--------------------------------------
BitField Name: diag_rxmis_eop
BitField Type: W1C
BitField Desc: Packet miss EOP
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_Sta0_diag_rxmis_eop_Mask                                                       cBit2
#define cAf6_ETH_40G_Diag_Sta0_diag_rxmis_eop_Shift                                                          2

/*--------------------------------------
BitField Name: diag_rxsop_eop
BitField Type: W1C
BitField Desc: Short packet, length is less than 16bytes  t
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_Sta0_diag_rxsop_eop_Mask                                                       cBit1
#define cAf6_ETH_40G_Diag_Sta0_diag_rxsop_eop_Shift                                                          1

/*--------------------------------------
BitField Name: diag_rxwff_ful
BitField Type: W1C
BitField Desc: RX-Fifo is full
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_Sta0_diag_rxwff_ful_Mask                                                       cBit0
#define cAf6_ETH_40G_Diag_Sta0_diag_rxwff_ful_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Diag TXPKT
Reg Addr   : 0x0042
Reg Formula:
    Where  :
Reg Desc   :
Diagnostic TX packet counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Diag_TXPKT_Base                                                                 0x0042

/*--------------------------------------
BitField Name: diag_txpkt
BitField Type: R2C
BitField Desc: TX packet counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_TXPKT_diag_txpkt_Mask                                                       cBit31_0
#define cAf6_ETH_40G_Diag_TXPKT_diag_txpkt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Diag TXNOB
Reg Addr   : 0x0043
Reg Formula:
    Where  :
Reg Desc   :
Diagnostic TX number of bytes counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Diag_TXNOB_Base                                                                 0x0043

/*--------------------------------------
BitField Name: diag_txnob
BitField Type: R2C
BitField Desc: TX number of byte counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_TXNOB_diag_txnob_Mask                                                       cBit31_0
#define cAf6_ETH_40G_Diag_TXNOB_diag_txnob_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Diag RXPKT
Reg Addr   : 0x0044
Reg Formula:
    Where  :
Reg Desc   :
Diagnostic RX packet counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Diag_RXPKT_Base                                                                 0x0044

/*--------------------------------------
BitField Name: diag_rxpkt
BitField Type: R2C
BitField Desc: RX packet counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_RXPKT_diag_rxpkt_Mask                                                       cBit31_0
#define cAf6_ETH_40G_Diag_RXPKT_diag_rxpkt_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Diag RXNOB
Reg Addr   : 0x0045
Reg Formula:
    Where  :
Reg Desc   :
Diagnostic RX number of bytes counter

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Diag_RXNOB_Base                                                                 0x0045

/*--------------------------------------
BitField Name: diag_rxnob
BitField Type: R2C
BitField Desc: RX number of byte counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_40G_Diag_RXNOB_diag_rxnob_Mask                                                       cBit31_0
#define cAf6_ETH_40G_Diag_RXNOB_diag_rxnob_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G TX CFG
Reg Addr   : 0x2021
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_TX_CFG_Base                                                                      0x2021

/*--------------------------------------
BitField Name: cfg_txen
BitField Type: RW
BitField Desc: enable transmit side,
BitField Bits: [04:04]
--------------------------------------*/
#define cAf6_ETH_40G_TX_CFG_cfg_txen_Mask                                                                cBit4
#define cAf6_ETH_40G_TX_CFG_cfg_txen_Shift                                                                   4

/*--------------------------------------
BitField Name: ipg_cfg
BitField Type: RW
BitField Desc: configure Tx IPG
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ETH_40G_TX_CFG_ipg_cfg_Mask                                                               cBit3_0
#define cAf6_ETH_40G_TX_CFG_ipg_cfg_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G RX CFG
Reg Addr   : 0x20C2
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_RX_CFG_Base                                                                     0x20C2

/*--------------------------------------
BitField Name: cfg_rxen
BitField Type: RW
BitField Desc: enable receive side,
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_ETH_40G_RX_CFG_cfg_rxen_Mask                                                               cBit24
#define cAf6_ETH_40G_RX_CFG_cfg_rxen_Shift                                                                  24

/*--------------------------------------
BitField Name: cfg_maxlen
BitField Type: RW
BitField Desc: configure Rx MTU, max len packet receive
BitField Bits: [22:08]
--------------------------------------*/
#define cAf6_ETH_40G_RX_CFG_cfg_maxlen_Mask                                                           cBit22_8
#define cAf6_ETH_40G_RX_CFG_cfg_maxlen_Shift                                                                 8
#define cAf6_ETH_40G_RX_CFG_cfg_maxlen_Max                                                            16004

/*--------------------------------------
BitField Name: cfg_minlen
BitField Type: configure Rx MTU, min len packet receive
BitField Desc:
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_ETH_40G_RX_CFG_cfg_minlen_Mask                                                            cBit7_0
#define cAf6_ETH_40G_RX_CFG_cfg_minlen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G FEC_AN STICKY
Reg Addr   : 0x2102
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 40G -pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 44-45)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_FEC_AN_STICKY                                                                   0x2102

/*--------------------------------------
BitField Name: stkfec_inc_cant_correct_count
BitField Type: W1C
BitField Desc: stkfec_inc_cant_correct_count, per lane
BitField Bits: [13:10]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_STICKY_stkfec_inc_cant_correct_count_Mask                                cBit13_10
#define cAf6_ETH_40G_FEC_AN_STICKY_stkfec_inc_cant_correct_count_Shift                                      10

/*--------------------------------------
BitField Name: stkfec_inc_correct_count
BitField Type: W1C
BitField Desc: stkfec_inc_correct_count , per lane
BitField Bits: [09:06]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_STICKY_stkfec_inc_correct_count_Mask                                       cBit9_6
#define cAf6_ETH_40G_FEC_AN_STICKY_stkfec_inc_correct_count_Shift                                            6

/*--------------------------------------
BitField Name: stkfec_lock_error
BitField Type: W1C
BitField Desc: stkfec_lock_error, per lane
BitField Bits: [05:02]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_STICKY_stkfec_lock_error_Mask                                              cBit5_2
#define cAf6_ETH_40G_FEC_AN_STICKY_stkfec_lock_error_Shift                                                   2

/*--------------------------------------
BitField Name: stkan_autoneg_complete
BitField Type: W1C
BitField Desc: stkan_autoneg_complete
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_STICKY_stkan_autoneg_complete_Mask                                           cBit1
#define cAf6_ETH_40G_FEC_AN_STICKY_stkan_autoneg_complete_Shift                                              1

/*--------------------------------------
BitField Name: stkan_parallel_detection_fault
BitField Type: W1C
BitField Desc: stkan_parallel_detection_fault
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_STICKY_stkan_parallel_detection_fault_Mask                                   cBit0
#define cAf6_ETH_40G_FEC_AN_STICKY_stkan_parallel_detection_fault_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G TX STICKY
Reg Addr   : 0x2103
Reg Formula:
    Where  :
Reg Desc   :
ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_TX_STICKY_Old                                                                   0x2020
#define cAf6Reg_ETH_40G_TX_STICKY_New                                                                   0x2103

/*--------------------------------------
BitField Name: Tx_underflow_err
BitField Type: W1C
BitField Desc: not support with xilinx IP- reserve
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_ETH_40G_TX_STICKY_Tx_underflow_err_Mask                                                    cBit17
#define cAf6_ETH_40G_TX_STICKY_Tx_underflow_err_Shift                                                       17

/*--------------------------------------
BitField Name: stktx_local_fault
BitField Type: W1C
BitField Desc: A value of 1 indicates the transmit encoder state machine is in
the TX_INIT state
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_ETH_40G_TX_STICKY_stktx_local_fault_Mask                                                   cBit16
#define cAf6_ETH_40G_TX_STICKY_stktx_local_fault_Shift                                                      16

/*--------------------------------------
BitField Name: stklt_signal_detect
BitField Type: W1C
BitField Desc: This signal indicates when the respective link training state
machine has entered the SEND_DATA state, in which normal PCS operation can
resume, per lane
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ETH_40G_TX_STICKY_stklt_signal_detect_Mask                                              cBit15_12
#define cAf6_ETH_40G_TX_STICKY_stklt_signal_detect_Shift                                                    12

/*--------------------------------------
BitField Name: stklt_training
BitField Type: W1C
BitField Desc: This signal indicates when the respective link training state
machine is performing link training, per lane
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_ETH_40G_TX_STICKY_stklt_training_Mask                                                    cBit11_8
#define cAf6_ETH_40G_TX_STICKY_stklt_training_Shift                                                          8

/*--------------------------------------
BitField Name: stklt_training_fail
BitField Type: W1C
BitField Desc: This signal is asserted during link training if the corresponding
link training state machine detects a time-out during the training period, per
lane
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_ETH_40G_TX_STICKY_stklt_training_fail_Mask                                                cBit7_4
#define cAf6_ETH_40G_TX_STICKY_stklt_training_fail_Shift                                                     4

/*--------------------------------------
BitField Name: stklt_frame_lock
BitField Type: W1C
BitField Desc: When link training has begun, these signals are asserted, per
lane
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ETH_40G_TX_STICKY_stklt_frame_lock_Mask                                                   cBit3_0
#define cAf6_ETH_40G_TX_STICKY_stklt_frame_lock_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G RX STICKY
Reg Addr   : 0x2104
Reg Formula:
    Where  :
Reg Desc   :
ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_RX_STICKY_Old                                                                   0x20C0
#define cAf6Reg_ETH_40G_RX_STICKY_New                                                                   0x2104

/*--------------------------------------
BitField Name: stkrx_local_fault
BitField Type: W1C
BitField Desc: stkrx_local_fault
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_local_fault_Mask                                                   cBit31
#define cAf6_ETH_40G_RX_STICKY_stkrx_local_fault_Shift                                                      31

/*--------------------------------------
BitField Name: stkrx_remote_fault
BitField Type: W1C
BitField Desc: stkrx_remote_fault
BitField Bits: [30:00]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_remote_fault_Mask                                                  cBit30
#define cAf6_ETH_40G_RX_STICKY_stkrx_remote_fault_Shift                                                     30

/*--------------------------------------
BitField Name: stkrx_internal_local_fault
BitField Type: W1C
BitField Desc: stkrx_internal_local_fault
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_internal_local_fault_Mask                                          cBit29
#define cAf6_ETH_40G_RX_STICKY_stkrx_internal_local_fault_Shift                                             29

/*--------------------------------------
BitField Name: stkrx_received_local_fault
BitField Type: W1C
BitField Desc: stkrx_received_local_fault
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_received_local_fault_Mask                                          cBit28
#define cAf6_ETH_40G_RX_STICKY_stkrx_received_local_fault_Shift                                             28

/*--------------------------------------
BitField Name: stkrx_framing_err
BitField Type: W1C
BitField Desc: stk_framing_err, per lane
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_framing_err_Mask                                                cBit27_24
#define cAf6_ETH_40G_RX_STICKY_stkrx_framing_err_Shift                                                      24

/*--------------------------------------
BitField Name: stkrx_synced_err
BitField Type: W1C
BitField Desc: stk_synced_err, per lane
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_synced_err_Mask                                                 cBit23_20
#define cAf6_ETH_40G_RX_STICKY_stkrx_synced_err_Shift                                                       20

/*--------------------------------------
BitField Name: stkrx_mf_len_err
BitField Type: W1C
BitField Desc: stk_mf_len_err, per lane
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_mf_len_err_Mask                                                 cBit19_16
#define cAf6_ETH_40G_RX_STICKY_stkrx_mf_len_err_Shift                                                       16

/*--------------------------------------
BitField Name: stkrx_mf_repeat_err
BitField Type: W1C
BitField Desc: stk_mf_repeat_err,  per lane
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_mf_repeat_err_Mask                                              cBit15_12
#define cAf6_ETH_40G_RX_STICKY_stkrx_mf_repeat_err_Shift                                                    12

/*--------------------------------------
BitField Name: stkrx_aligned_err
BitField Type: W1C
BitField Desc: stk_aligned_err_
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_aligned_err_Mask                                                   cBit11
#define cAf6_ETH_40G_RX_STICKY_stkrx_aligned_err_Shift                                                      11

/*--------------------------------------
BitField Name: stkrx_misaligned
BitField Type: W1C
BitField Desc: stk misaligned
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_misaligned_Mask                                                    cBit10
#define cAf6_ETH_40G_RX_STICKY_stkrx_misaligned_Shift                                                       10

/*--------------------------------------
BitField Name: stkrx_truncated
BitField Type: W1C
BitField Desc: stk truncated
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_truncated_Mask                                                      cBit9
#define cAf6_ETH_40G_RX_STICKY_stkrx_truncated_Shift                                                         9

/*--------------------------------------
BitField Name: stkrx_hi_ber
BitField Type: W1C
BitField Desc: stk stkrx_hi_ber
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_hi_ber_Mask                                                         cBit8
#define cAf6_ETH_40G_RX_STICKY_stkrx_hi_ber_Shift                                                            8

/*--------------------------------------
BitField Name: stkrx_bip_err
BitField Type: W1C
BitField Desc: stk these signal are asserted  per lane
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_bip_err_Mask                                                      cBit7_4
#define cAf6_ETH_40G_RX_STICKY_stkrx_bip_err_Shift                                                           4

/*--------------------------------------
BitField Name: stkrx_mf_err
BitField Type: W1C
BitField Desc: stk these signals are asserted, per lane
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ETH_40G_RX_STICKY_stkrx_mf_err_Mask                                                       cBit3_0
#define cAf6_ETH_40G_RX_STICKY_stkrx_mf_err_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G FEC_AN INTTERUPT ENABLE
Reg Addr   : 0x2105
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 40G -pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 44-45)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_FEC_AN_INTEN                                                                    0x2105

/*--------------------------------------
BitField Name: int_en_fec_inc_cant_correct_count
BitField Type: W1C
BitField Desc: enable interrupt fec_inc_cant_correct_count, per lane,
BitField Bits: [13:10]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_INTEN_int_en_fec_inc_cant_correct_count_Mask                               cBit13_10
#define cAf6_ETH_40G_FEC_AN_INTEN_int_en_fec_inc_cant_correct_count_Shift                                      10

/*--------------------------------------
BitField Name: int_en_fec_inc_correct_count
BitField Type: W1C
BitField Desc: enable interrupt fec_inc_correct_count , per lane,
BitField Bits: [09:06]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_INTEN_int_en_fec_inc_correct_count_Mask                                    cBit9_6
#define cAf6_ETH_40G_FEC_AN_INTEN_int_en_fec_inc_correct_count_Shift                                         6

/*--------------------------------------
BitField Name: int_en_fec_lock_error
BitField Type: W1C
BitField Desc: enable interrupt fec_lock_error, per lane,
BitField Bits: [05:02]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_INTEN_int_en_fec_lock_error_Mask                                           cBit5_2
#define cAf6_ETH_40G_FEC_AN_INTEN_int_en_fec_lock_error_Shift                                                2

/*--------------------------------------
BitField Name: int_en_an_autoneg_complete
BitField Type: W1C
BitField Desc: enable interrupt an_autoneg_complete,
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_INTEN_int_en_an_autoneg_complete_Mask                                        cBit1
#define cAf6_ETH_40G_FEC_AN_INTEN_int_en_an_autoneg_complete_Shift                                           1

/*--------------------------------------
BitField Name: int_en_an_parallel_detection_fault
BitField Type: W1C
BitField Desc: enable interrupt an_parallel_detection_fault,
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_INTEN_int_en_an_parallel_detection_fault_Mask                                   cBit0
#define cAf6_ETH_40G_FEC_AN_INTEN_int_en_an_parallel_detection_fault_Shift                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G TX INTTERUPT ENABLE
Reg Addr   : 0x2106
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_TX_INTEN                                                                        0x2106

/*--------------------------------------
BitField Name: int_en_tx_local_fault
BitField Type: W1C
BitField Desc: enable interrupt TX_INIT,
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_ETH_40G_TX_INTEN_int_en_tx_local_fault_Mask                                                cBit16
#define cAf6_ETH_40G_TX_INTEN_int_en_tx_local_fault_Shift                                                   16

/*--------------------------------------
BitField Name: int_en_lt_signal_detect
BitField Type: W1C
BitField Desc: enable interrupt link training state machine has entered the
SEND_DATA state, in which normal PCS operation can resume, per lane,
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ETH_40G_TX_INTEN_int_en_lt_signal_detect_Mask                                           cBit15_12
#define cAf6_ETH_40G_TX_INTEN_int_en_lt_signal_detect_Shift                                                 12

/*--------------------------------------
BitField Name: int_en_lt_training
BitField Type: W1C
BitField Desc: enable interrupt link training state machine is performing link
training, per lane,
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_ETH_40G_TX_INTEN_int_en_lt_training_Mask                                                 cBit11_8
#define cAf6_ETH_40G_TX_INTEN_int_en_lt_training_Shift                                                       8

/*--------------------------------------
BitField Name: int_en_lt_training_fail
BitField Type: W1C
BitField Desc: enable interrupt link training state machine detects a time-out
during the training period, per lane,
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_ETH_40G_TX_INTEN_int_en_lt_training_fail_Mask                                             cBit7_4
#define cAf6_ETH_40G_TX_INTEN_int_en_lt_training_fail_Shift                                                  4

/*--------------------------------------
BitField Name: int_en_lt_frame_lock
BitField Type: W1C
BitField Desc: enable interrupt link training has begun, per lane,
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ETH_40G_TX_INTEN_int_en_lt_frame_lock_Mask                                                cBit3_0
#define cAf6_ETH_40G_TX_INTEN_int_en_lt_frame_lock_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G RX INTTERUPT ENABLE
Reg Addr   : 0x2107
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_RX_INTEN                                                                        0x2107

/*--------------------------------------
BitField Name: int_en_rx_local_fault
BitField Type: W1C
BitField Desc: enable interrupt rx_local_fault,
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_local_fault_Mask                                                cBit31
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_local_fault_Shift                                                   31

/*--------------------------------------
BitField Name: int_en_rx_remote_fault
BitField Type: W1C
BitField Desc: enable interrupt rx_remote_fault,
BitField Bits: [30:00]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_remote_fault_Mask                                               cBit30
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_remote_fault_Shift                                                  30

/*--------------------------------------
BitField Name: int_en_rx_internal_local_fault
BitField Type: W1C
BitField Desc: enable interrupt rx_internal_local_fault,
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_internal_local_fault_Mask                                       cBit29
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_internal_local_fault_Shift                                          29

/*--------------------------------------
BitField Name: int_en_rx_received_local_fault
BitField Type: W1C
BitField Desc: enable interrupt rx_received_local_fault,
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_received_local_fault_Mask                                       cBit28
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_received_local_fault_Shift                                          28

/*--------------------------------------
BitField Name: int_en_rx_framing_err
BitField Type: W1C
BitField Desc: enable interrupt framing_err, per lane,
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_framing_err_Mask                                             cBit27_24
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_framing_err_Shift                                                   24

/*--------------------------------------
BitField Name: int_en_rx_synced_err
BitField Type: W1C
BitField Desc: enable interrupt synced_err, per lane,
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_synced_err_Mask                                              cBit23_20
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_synced_err_Shift                                                    20

/*--------------------------------------
BitField Name: int_en_rx_mf_len_err
BitField Type: W1C
BitField Desc: enable interrupt mf_len_err, per lane ,
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_mf_len_err_Mask                                              cBit19_16
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_mf_len_err_Shift                                                    16

/*--------------------------------------
BitField Name: int_en_rx_mf_repeat_err
BitField Type: W1C
BitField Desc: enable interrupt mf_repeat_err,  per lane,
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_mf_repeat_err_Mask                                           cBit15_12
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_mf_repeat_err_Shift                                                 12

/*--------------------------------------
BitField Name: int_en_rx_aligned_err
BitField Type: W1C
BitField Desc: enable interrupt aligned_err,
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_aligned_err_Mask                                                cBit11
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_aligned_err_Shift                                                   11

/*--------------------------------------
BitField Name: int_en_rx_misaligned
BitField Type: W1C
BitField Desc: enable interrupt misaligned,
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_misaligned_Mask                                                 cBit10
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_misaligned_Shift                                                    10

/*--------------------------------------
BitField Name: int_en_rx_truncated
BitField Type: W1C
BitField Desc: enable interrupt truncated,
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_truncated_Mask                                                   cBit9
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_truncated_Shift                                                      9

/*--------------------------------------
BitField Name: int_en_rx_hi_ber
BitField Type: W1C
BitField Desc: enable interrupt rx_hi_ber,
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_hi_ber_Mask                                                      cBit8
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_hi_ber_Shift                                                         8

/*--------------------------------------
BitField Name: int_en_rx_bip_err
BitField Type: W1C
BitField Desc: enable interrupt rx_bip_err,    per lane,
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_bip_err_Mask                                                   cBit7_4
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_bip_err_Shift                                                        4

/*--------------------------------------
BitField Name: int_en_rx_mf_err
BitField Type: W1C
BitField Desc: enable interrupt rx_mf_err,  per lane,
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_mf_err_Mask                                                    cBit3_0
#define cAf6_ETH_40G_RX_INTEN_int_en_rx_mf_err_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G FEC_AN ALARM
Reg Addr   : 0x2108
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 40G -pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 44-45)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_FEC_AN_ALARM                                                                    0x2108

/*--------------------------------------
BitField Name: stat_fec_inc_cant_correct_count
BitField Type: W1C
BitField Desc: stat_fec_inc_cant_correct_count, per lane
BitField Bits: [13:10]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_ALARM_stat_fec_inc_cant_correct_count_Mask                                cBit13_10
#define cAf6_ETH_40G_FEC_AN_ALARM_stat_fec_inc_cant_correct_count_Shift                                      10

/*--------------------------------------
BitField Name: stat_fec_inc_correct_count
BitField Type: W1C
BitField Desc: stat_fec_inc_correct_count , per lane
BitField Bits: [09:06]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_ALARM_stat_fec_inc_correct_count_Mask                                      cBit9_6
#define cAf6_ETH_40G_FEC_AN_ALARM_stat_fec_inc_correct_count_Shift                                           6

/*--------------------------------------
BitField Name: stat_fec_lock_error
BitField Type: W1C
BitField Desc: stat_fec_lock_error, per lane
BitField Bits: [05:02]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_ALARM_stat_fec_lock_error_Mask                                             cBit5_2
#define cAf6_ETH_40G_FEC_AN_ALARM_stat_fec_lock_error_Shift                                                  2

/*--------------------------------------
BitField Name: stat_an_autoneg_complete
BitField Type: W1C
BitField Desc: stat_an_autoneg_complete
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_ALARM_stat_an_autoneg_complete_Mask                                          cBit1
#define cAf6_ETH_40G_FEC_AN_ALARM_stat_an_autoneg_complete_Shift                                             1

/*--------------------------------------
BitField Name: stat_an_parallel_detection_fault
BitField Type: W1C
BitField Desc: stat_an_parallel_detection_fault
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_AN_ALARM_stat_an_parallel_detection_fault_Mask                                  cBit0
#define cAf6_ETH_40G_FEC_AN_ALARM_stat_an_parallel_detection_fault_Shift                                     0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G TX ALARM
Reg Addr   : 0x2109
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_TX_ALARM                                                                        0x2109

/*--------------------------------------
BitField Name: Tx_underflow_err
BitField Type: W1C
BitField Desc: not support with xilinx IP- reserve
BitField Bits: [17:17]
--------------------------------------*/
#define cAf6_ETH_40G_TX_ALARM_Tx_underflow_err_Mask                                                     cBit17
#define cAf6_ETH_40G_TX_ALARM_Tx_underflow_err_Shift                                                        17

/*--------------------------------------
BitField Name: stat_tx_local_fault
BitField Type: W1C
BitField Desc: A value of 1 indicates the transmit encoder state machine is in
the TX_INIT state
BitField Bits: [16:16]
--------------------------------------*/
#define cAf6_ETH_40G_TX_ALARM_stat_tx_local_fault_Mask                                                  cBit16
#define cAf6_ETH_40G_TX_ALARM_stat_tx_local_fault_Shift                                                     16

/*--------------------------------------
BitField Name: stat_lt_signal_detect
BitField Type: W1C
BitField Desc: This signal indicates when the respective link training state
machine has entered the SEND_DATA state, in which normal PCS operation can
resume, per lane
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ETH_40G_TX_ALARM_stat_lt_signal_detect_Mask                                             cBit15_12
#define cAf6_ETH_40G_TX_ALARM_stat_lt_signal_detect_Shift                                                   12

/*--------------------------------------
BitField Name: stat_lt_training
BitField Type: W1C
BitField Desc: This signal indicates when the respective link training state
machine is performing link training, per lane
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_ETH_40G_TX_ALARM_stat_lt_training_Mask                                                   cBit11_8
#define cAf6_ETH_40G_TX_ALARM_stat_lt_training_Shift                                                         8

/*--------------------------------------
BitField Name: stat_lt_training_fail
BitField Type: W1C
BitField Desc: This signal is asserted during link training if the corresponding
link training state machine detects a time-out during the training period, per
lane
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_ETH_40G_TX_ALARM_stat_lt_training_fail_Mask                                               cBit7_4
#define cAf6_ETH_40G_TX_ALARM_stat_lt_training_fail_Shift                                                    4

/*--------------------------------------
BitField Name: stat_lt_frame_lock
BitField Type: W1C
BitField Desc: When link training has begun, these signals are asserted, per
lane
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ETH_40G_TX_ALARM_stat_lt_frame_lock_Mask                                                  cBit3_0
#define cAf6_ETH_40G_TX_ALARM_stat_lt_frame_lock_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G RX ALARM
Reg Addr   : 0x210A
Reg Formula: 
    Where  : 
Reg Desc   : 
ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 57-64)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_RX_ALARM                                                                        0x210A

/*--------------------------------------
BitField Name: stat_rx_local_fault
BitField Type: W1C
BitField Desc: stat_rx_local_fault
BitField Bits: [31:31]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_local_fault_Mask                                                  cBit31
#define cAf6_ETH_40G_RX_ALARM_stat_rx_local_fault_Shift                                                     31

/*--------------------------------------
BitField Name: stat_rx_remote_fault
BitField Type: W1C
BitField Desc: stat_rx_remote_fault
BitField Bits: [30:00]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_remote_fault_Mask                                                 cBit30
#define cAf6_ETH_40G_RX_ALARM_stat_rx_remote_fault_Shift                                                    30

/*--------------------------------------
BitField Name: stat_rx_internal_local_fault
BitField Type: W1C
BitField Desc: stat_rx_internal_local_fault
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_internal_local_fault_Mask                                         cBit29
#define cAf6_ETH_40G_RX_ALARM_stat_rx_internal_local_fault_Shift                                            29

/*--------------------------------------
BitField Name: stat_rx_received_local_fault
BitField Type: W1C
BitField Desc: stat_rx_received_local_fault
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_received_local_fault_Mask                                         cBit28
#define cAf6_ETH_40G_RX_ALARM_stat_rx_received_local_fault_Shift                                            28

/*--------------------------------------
BitField Name: stat_rx_framing_err
BitField Type: W1C
BitField Desc: stat_framing_err, per lane
BitField Bits: [27:24]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_framing_err_Mask                                               cBit27_24
#define cAf6_ETH_40G_RX_ALARM_stat_rx_framing_err_Shift                                                     24

/*--------------------------------------
BitField Name: stat_rx_synced_err
BitField Type: W1C
BitField Desc: synced_err, per lane
BitField Bits: [23:20]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_synced_err_Mask                                                cBit23_20
#define cAf6_ETH_40G_RX_ALARM_stat_rx_synced_err_Shift                                                      20

/*--------------------------------------
BitField Name: stat_rx_mf_len_err
BitField Type: W1C
BitField Desc: mf_len_err, per lane
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_mf_len_err_Mask                                                cBit19_16
#define cAf6_ETH_40G_RX_ALARM_stat_rx_mf_len_err_Shift                                                      16

/*--------------------------------------
BitField Name: stat_rx_mf_repeat_err
BitField Type: W1C
BitField Desc: mf_repeat_err,  per lane
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_mf_repeat_err_Mask                                             cBit15_12
#define cAf6_ETH_40G_RX_ALARM_stat_rx_mf_repeat_err_Shift                                                   12

/*--------------------------------------
BitField Name: stat_rx_aligned_err
BitField Type: W1C
BitField Desc: aligned_err_
BitField Bits: [11:11]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_aligned_err_Mask                                                  cBit11
#define cAf6_ETH_40G_RX_ALARM_stat_rx_aligned_err_Shift                                                     11

/*--------------------------------------
BitField Name: stat_rx_misaligned
BitField Type: W1C
BitField Desc: misaligned
BitField Bits: [10:10]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_misaligned_Mask                                                   cBit10
#define cAf6_ETH_40G_RX_ALARM_stat_rx_misaligned_Shift                                                      10

/*--------------------------------------
BitField Name: stat_rx_truncated
BitField Type: W1C
BitField Desc: truncated
BitField Bits: [09:09]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_truncated_Mask                                                     cBit9
#define cAf6_ETH_40G_RX_ALARM_stat_rx_truncated_Shift                                                        9

/*--------------------------------------
BitField Name: stat_rx_hi_ber
BitField Type: W1C
BitField Desc: stat_rx_hi_ber
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_hi_ber_Mask                                                        cBit8
#define cAf6_ETH_40G_RX_ALARM_stat_rx_hi_ber_Shift                                                           8

/*--------------------------------------
BitField Name: stat_rx_bip_err
BitField Type: W1C
BitField Desc: these signal are asserted  per lane
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_bip_err_Mask                                                     cBit7_4
#define cAf6_ETH_40G_RX_ALARM_stat_rx_bip_err_Shift                                                          4

/*--------------------------------------
BitField Name: stat_rx_mf_err
BitField Type: W1C
BitField Desc: these signals are asserted, per lane
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ETH_40G_RX_ALARM_stat_rx_mf_err_Mask                                                      cBit3_0
#define cAf6_ETH_40G_RX_ALARM_stat_rx_mf_err_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G FEC STICKY (Obsolete)
Reg Addr   : 0x20C1
Reg Formula:
    Where  :
Reg Desc   :
ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 46-47)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_FEC_STICKY_Base                                                                 0x20C1

/*--------------------------------------
BitField Name: stat_fec_inc_cant_correct_count
BitField Type: W1C
BitField Desc: stat_fec_inc_cant_correct_count, per lane
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_STICKY_stat_fec_inc_cant_correct_count_Mask                                  cBit11_8
#define cAf6_ETH_40G_FEC_STICKY_stat_fec_inc_cant_correct_count_Shift                                        8

/*--------------------------------------
BitField Name: stat_fec_inc_correct_count
BitField Type: W1C
BitField Desc: stat_fec_inc_correct_count , per lane
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_STICKY_stat_fec_inc_correct_count_Mask                                        cBit7_4
#define cAf6_ETH_40G_FEC_STICKY_stat_fec_inc_correct_count_Shift                                             4

/*--------------------------------------
BitField Name: stat_fec_lock_error
BitField Type: W1C
BitField Desc: stat_fec_lock_error, per lane
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_ETH_40G_FEC_STICKY_stat_fec_lock_error_Mask                                               cBit3_0
#define cAf6_ETH_40G_FEC_STICKY_stat_fec_lock_error_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G CFG GLBEN
Reg Addr   : 0x2100
Reg Formula:
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_CFG_GLBEN_Base                                                                  0x2100

/*--------------------------------------
BitField Name: cfg_sel_tick
BitField Type: RW
BitField Desc: configure select pm_tick from CPU configure (tick_reg) or from
signal pm_tick, (1) from signal pm_tick, (0) from CPU
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_ETH_40G_CFG_GLBEN_cfg_sel_tick_Mask                                                         cBit1
#define cAf6_ETH_40G_CFG_GLBEN_cfg_sel_tick_Shift                                                            1

/*--------------------------------------
BitField Name: cfg_enable_cnt
BitField Type: RW
BitField Desc: configure enbale counter, (1) is enable, (0) is disable
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_40G_CFG_GLBEN_cfg_enable_cnt_Mask                                                       cBit0
#define cAf6_ETH_40G_CFG_GLBEN_cfg_enable_cnt_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G CFG TICK REG
Reg Addr   : 0x2101
Reg Formula:
    Where  :
Reg Desc   :


------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_CFG_TICK_REG_Base                                                               0x2101
#define cAf6Reg_ETH_40G_CFG_TICK_REG_WidthVal                                                               32

/*--------------------------------------
BitField Name: tick_reg
BitField Type: RW
BitField Desc: write value "1" for tick, auto low (value "0")
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_ETH_40G_CFG_TICK_REG_tick_reg_Mask                                                          cBit0
#define cAf6_ETH_40G_CFG_TICK_REG_tick_reg_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Statistics TX COUNTER
Reg Addr   : 0x2000 - 0x2019
Reg Formula:
    Where  :
Reg Desc   :
ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 69-70/table 2-22)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Statistics_TX_COUNTER_Base                                                      0x2000

/*--------------------------------------
BitField Name: cnt_tx_val
BitField Type: RW
BitField Desc: value of resgister Statistics Tx Counter
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_40G_Statistics_TX_COUNTER_cnt_tx_val_Mask                                            cBit31_0
#define cAf6_ETH_40G_Statistics_TX_COUNTER_cnt_tx_val_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : ETH 40G Statistics RX Counters
Reg Addr   : 0x2080 - 0x20AC
Reg Formula:
    Where  :
Reg Desc   :
AXI4 Statistics Counters ETH 40G - pg211-50g-ethernet (40G/50G High Speed Ethernet Subsystem v1.0 page 71-75/table 2-22)

------------------------------------------------------------------------------*/
#define cAf6Reg_ETH_40G_Statistics_RX_Counters_Base                                                     0x2080

/*--------------------------------------
BitField Name: cnt_rx_val
BitField Type: RW
BitField Desc: value of resgister Statistics Rx Counters
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_ETH_40G_Statistics_RX_Counters_cnt_rx_val_Mask                                           cBit31_0
#define cAf6_ETH_40G_Statistics_RX_Counters_cnt_rx_val_Shift                                                 0

#endif /* _AF6_REG_AF6CNC0021_ETH40G_RD_H_ */

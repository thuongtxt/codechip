/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD_H_
#define _AF6_REG_AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 Change Mode
Reg Addr   : 0x0080-0x6080
Reg Formula: 0x0080+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
Configurate Port mode, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_Change_Mode_Base                                                         0x0080

/*--------------------------------------
BitField Name: chg_done
BitField Type: W1C
BitField Desc: Change mode has done
BitField Bits: [12:12]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_Change_Mode_chg_done_Mask                                                   cBit12
#define cAf6_OC192_MUX_OC48_Change_Mode_chg_done_Shift                                                      12

/*--------------------------------------
BitField Name: chg_trig
BitField Type: R/W
BitField Desc: Trigger 0->1 to start changing mode
BitField Bits: [08:08]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_Change_Mode_chg_trig_Mask                                                    cBit8
#define cAf6_OC192_MUX_OC48_Change_Mode_chg_trig_Shift                                                       8

/*--------------------------------------
BitField Name: chg_mode
BitField Type: R/W
BitField Desc: Port mode
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_Change_Mode_chg_mode_Mask                                                  cBit7_4
#define cAf6_OC192_MUX_OC48_Change_Mode_chg_mode_Shift                                                       4

/*--------------------------------------
BitField Name: chg_port
BitField Type: R/W
BitField Desc: Sub Port ID
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_Change_Mode_chg_port_Mask                                                  cBit3_0
#define cAf6_OC192_MUX_OC48_Change_Mode_chg_port_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 Current Mode
Reg Addr   : 0x0090-0x6090
Reg Formula: 0x0090+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
Current Port mode, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_Current_Mode_Base                                                        0x0090

/*--------------------------------------
BitField Name: cur_mode_subport3
BitField Type: R_O
BitField Desc: Current mode subport3, Group 0 => Port3, Group 1 => Port 7, Group
2 => Port 11, Group 3 => Port 15
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_Current_Mode_cur_mode_subport3_Mask                                      cBit15_12
#define cAf6_OC192_MUX_OC48_Current_Mode_cur_mode_subport3_Shift                                            12

/*--------------------------------------
BitField Name: cur_mode_subport2
BitField Type: R_O
BitField Desc: Current mode subport2, Group 0 => Port2, Group 1 => Port 6, Group
2 => Port 10, Group 3 => Port 14
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_Current_Mode_cur_mode_subport2_Mask                                       cBit11_8
#define cAf6_OC192_MUX_OC48_Current_Mode_cur_mode_subport2_Shift                                             8

/*--------------------------------------
BitField Name: cur_mode_subport1
BitField Type: R_O
BitField Desc: Current mode subport1, Group 0 => Port1, Group 1 => Port 5, Group
2 => Port 9, Group 3 => Port 13
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_Current_Mode_cur_mode_subport1_Mask                                        cBit7_4
#define cAf6_OC192_MUX_OC48_Current_Mode_cur_mode_subport1_Shift                                             4

/*--------------------------------------
BitField Name: cur_mode_subport0
BitField Type: R_O
BitField Desc: Current mode subport0, Group 0 => Port0, Group 1 => Port 4, Group
2 => Port 8, Group 3 => Port 12
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_Current_Mode_cur_mode_subport0_Mask                                        cBit3_0
#define cAf6_OC192_MUX_OC48_Current_Mode_cur_mode_subport0_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 DRP
Reg Addr   : 0x1000-0x7FFF
Reg Formula: 0x1000+$G*0x2000+$P*0x400+$DRP
    Where  : 
           + $G(0-3) : Group of Ports
           + $P(0-3) : Sub Port ID
           + $DRP(0-1023) : DRP address, see UG578
Reg Desc   : 
Read/Write DRP address of SERDES, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_DRP_Base                                                                 0x1000

/*--------------------------------------
BitField Name: drp_rw
BitField Type: R/W
BitField Desc: DRP read/write value
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_DRP_drp_rw_Mask                                                            cBit9_0
#define cAf6_OC192_MUX_OC48_DRP_drp_rw_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 LoopBack
Reg Addr   : 0x0002-0x6002
Reg Formula: 0x0002+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
Configurate LoopBack, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_LoopBack_Base                                                            0x0002

/*--------------------------------------
BitField Name: lpback_subport3
BitField Type: R/W
BitField Desc: Loopback subport 3, Group 0 => Port3, Group 1 => Port 7, Group 2
=> Port 11, Group 3 => Port 15
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_LoopBack_lpback_subport3_Mask                                            cBit15_12
#define cAf6_OC192_MUX_OC48_LoopBack_lpback_subport3_Shift                                                  12

/*--------------------------------------
BitField Name: lpback_subport2
BitField Type: R/W
BitField Desc: Loopback subport 2, Group 0 => Port2, Group 1 => Port 6, Group 2
=> Port 10, Group 3 => Port 14
BitField Bits: [11:08]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_LoopBack_lpback_subport2_Mask                                             cBit11_8
#define cAf6_OC192_MUX_OC48_LoopBack_lpback_subport2_Shift                                                   8

/*--------------------------------------
BitField Name: lpback_subport1
BitField Type: R/W
BitField Desc: Loopback subport 1, Group 0 => Port1, Group 1 => Port 5, Group 2
=> Port 9, Group 3 => Port 13
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_LoopBack_lpback_subport1_Mask                                              cBit7_4
#define cAf6_OC192_MUX_OC48_LoopBack_lpback_subport1_Shift                                                   4

/*--------------------------------------
BitField Name: lpback_subport0
BitField Type: R/W
BitField Desc: Loopback subport 0, Group 0 => Port0, Group 1 => Port 4, Group 2
=> Port 8, Group 3 => Port 12
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_LoopBack_lpback_subport0_Mask                                              cBit3_0
#define cAf6_OC192_MUX_OC48_LoopBack_lpback_subport0_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Async GearBox Enable of 10Ge mode
Reg Addr   : 0x107C-0x7C7C
Reg Formula: 0x107C+$G*0x2000
    Where  : 
           + $G(0) : Group of Ports
Reg Desc   : 
Configurate Enable/Disable Async GearBox of 10Ge mode, Async GearBox has to be disable when using remote loopback far-end PMA

------------------------------------------------------------------------------*/
#define cAf6Reg_Async_GearBox_Enable_of_10Ge_mode_Base                                                  0x107C

/*--------------------------------------
BitField Name: async_gearbix_enb
BitField Type: R/W
BitField Desc: Eanble/disable async gearbox of 10Ge mode
BitField Bits: [13:13]
--------------------------------------*/
#define cAf6_Async_GearBox_Enable_of_10Ge_mode_async_gearbix_enb_Mask                                   cBit13
#define cAf6_Async_GearBox_Enable_of_10Ge_mode_async_gearbix_enb_Shift                                      13


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 PLL Status
Reg Addr   : 0x000B-0x600B
Reg Formula: 0x000B+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
QPLL/CPLL status, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_PLL_Status_Base                                                          0x000B

/*--------------------------------------
BitField Name: QPLL1_Lock_change
BitField Type: W1C
BitField Desc: QPLL1 has transition lock/unlock, Group 0-3
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_PLL_Status_QPLL1_Lock_change_Mask                                           cBit29
#define cAf6_OC192_MUX_OC48_PLL_Status_QPLL1_Lock_change_Shift                                              29

/*--------------------------------------
BitField Name: QPLL0_Lock_change
BitField Type: W1C
BitField Desc: QPLL0 has transition lock/unlock, Group 0-3
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_PLL_Status_QPLL0_Lock_change_Mask                                           cBit28
#define cAf6_OC192_MUX_OC48_PLL_Status_QPLL0_Lock_change_Shift                                              28

/*--------------------------------------
BitField Name: QPLL1_Lock
BitField Type: R_O
BitField Desc: QPLL0 is Locked, Group 0-3
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_PLL_Status_QPLL1_Lock_Mask                                                  cBit25
#define cAf6_OC192_MUX_OC48_PLL_Status_QPLL1_Lock_Shift                                                     25

/*--------------------------------------
BitField Name: QPLL0_Lock
BitField Type: R_O
BitField Desc: QPLL0 is Locked, Group 0-3
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_PLL_Status_QPLL0_Lock_Mask                                                  cBit24
#define cAf6_OC192_MUX_OC48_PLL_Status_QPLL0_Lock_Shift                                                     24

/*--------------------------------------
BitField Name: CPLL_Lock_Change
BitField Type: W1C
BitField Desc: CPLL has transition lock/unlock, bit per sub port, Group 0 =>
Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_PLL_Status_CPLL_Lock_Change_Mask                                         cBit15_12
#define cAf6_OC192_MUX_OC48_PLL_Status_CPLL_Lock_Change_Shift                                               12

/*--------------------------------------
BitField Name: CPLL_Lock
BitField Type: R_O
BitField Desc: CPLL is Locked, bit per sub port, Group 0 => Port0-3, Group 1 =>
Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_PLL_Status_CPLL_Lock_Mask                                                  cBit3_0
#define cAf6_OC192_MUX_OC48_PLL_Status_CPLL_Lock_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 TX Reset
Reg Addr   : 0x000C-0x600C
Reg Formula: 0x000C+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
Reset TX SERDES, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_TX_Reset_Base                                                            0x000C

/*--------------------------------------
BitField Name: txrst_done
BitField Type: W1C
BitField Desc: TX Reset Done, bit per sub port, Group 0 => Port0-3, Group 1 =>
Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TX_Reset_txrst_done_Mask                                                 cBit19_16
#define cAf6_OC192_MUX_OC48_TX_Reset_txrst_done_Shift                                                       16

/*--------------------------------------
BitField Name: txrst_trig
BitField Type: R/W
BitField Desc: Trige 0->1 to start reset TX SERDES, bit per sub port, Group 0 =>
Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TX_Reset_txrst_trig_Mask                                                   cBit3_0
#define cAf6_OC192_MUX_OC48_TX_Reset_txrst_trig_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 RX Reset
Reg Addr   : 0x000D-0x600D
Reg Formula: 0x000D+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
Reset RX SERDES, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_RX_Reset_Base                                                            0x000D

/*--------------------------------------
BitField Name: rxrst_done
BitField Type: W1C
BitField Desc: RX Reset Done, bit per sub port, Group 0 => Port0-3, Group 1 =>
Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_RX_Reset_rxrst_done_Mask                                                 cBit19_16
#define cAf6_OC192_MUX_OC48_RX_Reset_rxrst_done_Shift                                                       16

/*--------------------------------------
BitField Name: rxrst_trig
BitField Type: R/W
BitField Desc: Trige 0->1 to start reset RX SERDES, bit per sub port, Group 0 =>
Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_RX_Reset_rxrst_trig_Mask                                                   cBit3_0
#define cAf6_OC192_MUX_OC48_RX_Reset_rxrst_trig_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 LPMDFE Mode
Reg Addr   : 0x000E-0x600E
Reg Formula: 0x000E+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
Configure LPM/DFE mode , there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_LPMDFE_Mode_Base                                                         0x000E

/*--------------------------------------
BitField Name: lpmdfe_mode
BitField Type: R/W
BitField Desc: bit per sub port, Group 0 => Port0-3, Group 1 => Port 4-7, ,
Group 2 => Port 8-11, Group 3 => Port 12-15
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_LPMDFE_Mode_lpmdfe_mode_Mask                                               cBit3_0
#define cAf6_OC192_MUX_OC48_LPMDFE_Mode_lpmdfe_mode_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 LPMDFE Reset
Reg Addr   : 0x000F-0x600F
Reg Formula: 0x000F+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
Reset LPM/DFE , there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_LPMDFE_Reset_Base                                                        0x000F

/*--------------------------------------
BitField Name: lpmdfe_reset
BitField Type: R/W
BitField Desc: bit per sub port, Must be toggled after switching between modes
to initialize adaptation, Group 0 => Port0-3, Group 1 => Port 4-7, , Group 2 =>
Port 8-11, Group 3 => Port 12-15
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_LPMDFE_Reset_lpmdfe_reset_Mask                                             cBit3_0
#define cAf6_OC192_MUX_OC48_LPMDFE_Reset_lpmdfe_reset_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 TXDIFFCTRL
Reg Addr   : 0x0010-0x6010
Reg Formula: 0x0010+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_TXDIFFCTRL_Base                                                          0x0010

/*--------------------------------------
BitField Name: TXDIFFCTRL_subport3
BitField Type: R/W
BitField Desc: Group 3 => Port 12-15
BitField Bits: [19:15]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TXDIFFCTRL_TXDIFFCTRL_subport3_Mask                                      cBit19_15
#define cAf6_OC192_MUX_OC48_TXDIFFCTRL_TXDIFFCTRL_subport3_Shift                                            15

/*--------------------------------------
BitField Name: TXDIFFCTRL_subport2
BitField Type: R/W
BitField Desc: Group 2 => Port 8-11
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TXDIFFCTRL_TXDIFFCTRL_subport2_Mask                                      cBit14_10
#define cAf6_OC192_MUX_OC48_TXDIFFCTRL_TXDIFFCTRL_subport2_Shift                                            10

/*--------------------------------------
BitField Name: TXDIFFCTRL_subport1
BitField Type: R/W
BitField Desc: Group 1 => Port 4-7
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TXDIFFCTRL_TXDIFFCTRL_subport1_Mask                                        cBit9_5
#define cAf6_OC192_MUX_OC48_TXDIFFCTRL_TXDIFFCTRL_subport1_Shift                                             5

/*--------------------------------------
BitField Name: TXDIFFCTRL_subport0
BitField Type: R/W
BitField Desc: Group 0 => Port 0-3
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TXDIFFCTRL_TXDIFFCTRL_subport0_Mask                                        cBit4_0
#define cAf6_OC192_MUX_OC48_TXDIFFCTRL_TXDIFFCTRL_subport0_Shift                                             0


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 TXPOSTCURSOR
Reg Addr   : 0x0011-0x6011
Reg Formula: 0x0011+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_TXPOSTCURSOR_Base                                                        0x0011

/*--------------------------------------
BitField Name: TXPOSTCURSOR_subport3
BitField Type: R/W
BitField Desc: Group 3 => Port 12-15
BitField Bits: [19:15]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TXPOSTCURSOR_TXPOSTCURSOR_subport3_Mask                                  cBit19_15
#define cAf6_OC192_MUX_OC48_TXPOSTCURSOR_TXPOSTCURSOR_subport3_Shift                                        15

/*--------------------------------------
BitField Name: TXPOSTCURSOR_subport2
BitField Type: R/W
BitField Desc: Group 2 => Port 8-11
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TXPOSTCURSOR_TXPOSTCURSOR_subport2_Mask                                  cBit14_10
#define cAf6_OC192_MUX_OC48_TXPOSTCURSOR_TXPOSTCURSOR_subport2_Shift                                        10

/*--------------------------------------
BitField Name: TXPOSTCURSOR_subport1
BitField Type: R/W
BitField Desc: Group 1 => Port 4-7
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TXPOSTCURSOR_TXPOSTCURSOR_subport1_Mask                                    cBit9_5
#define cAf6_OC192_MUX_OC48_TXPOSTCURSOR_TXPOSTCURSOR_subport1_Shift                                         5

/*--------------------------------------
BitField Name: TXPOSTCURSOR_subport0
BitField Type: R/W
BitField Desc: Group 0 => Port 0-3
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Mask                                    cBit4_0
#define cAf6_OC192_MUX_OC48_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 TXPRECURSOR
Reg Addr   : 0x0012-0x6012
Reg Formula: 0x0012+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_TXPRECURSOR_Base                                                         0x0012

/*--------------------------------------
BitField Name: TXPRECURSOR_subport3
BitField Type: R/W
BitField Desc: Group 3 => Port 12-15
BitField Bits: [19:15]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TXPRECURSOR_TXPRECURSOR_subport3_Mask                                    cBit19_15
#define cAf6_OC192_MUX_OC48_TXPRECURSOR_TXPRECURSOR_subport3_Shift                                          15

/*--------------------------------------
BitField Name: TXPRECURSOR_subport2
BitField Type: R/W
BitField Desc: Group 2 => Port 8-11
BitField Bits: [14:10]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TXPRECURSOR_TXPRECURSOR_subport2_Mask                                    cBit14_10
#define cAf6_OC192_MUX_OC48_TXPRECURSOR_TXPRECURSOR_subport2_Shift                                          10

/*--------------------------------------
BitField Name: TXPRECURSOR_subport1
BitField Type: R/W
BitField Desc: Group 1 => Port 4-7
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TXPRECURSOR_TXPRECURSOR_subport1_Mask                                      cBit9_5
#define cAf6_OC192_MUX_OC48_TXPRECURSOR_TXPRECURSOR_subport1_Shift                                           5

/*--------------------------------------
BitField Name: TXPRECURSOR_subport0
BitField Type: R/W
BitField Desc: Group 0 => Port 0-3
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_TXPRECURSOR_TXPRECURSOR_subport0_Mask                                      cBit4_0
#define cAf6_OC192_MUX_OC48_TXPRECURSOR_TXPRECURSOR_subport0_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : SERDES POWER DOWN
Reg Addr   : 0x0014-0x6014
Reg Formula: 0x0014+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
TX/RX power down control, see "UG578 for more detail, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_POWER_DOWN_Base                                                                  0x0014

/*--------------------------------------
BitField Name: TXELECIDLE
BitField Type: R/W
BitField Desc: TXELECIDLE must be strapped to TXPD[1] and TXPD[0] of Port per
bit for per port
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXELECIDLE_Mask                                                       cBit19_16
#define cAf6_SERDES_POWER_DOWN_TXELECIDLE_Shift                                                             16

/*--------------------------------------
BitField Name: TXPD3
BitField Type: R/W
BitField Desc: Power Down subport 3
BitField Bits: [15:14]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXPD3_Mask                                                            cBit15_14
#define cAf6_SERDES_POWER_DOWN_TXPD3_Shift                                                                  14

/*--------------------------------------
BitField Name: TXPD2
BitField Type: R/W
BitField Desc: Power Down subport 2
BitField Bits: [13:12]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXPD2_Mask                                                            cBit13_12
#define cAf6_SERDES_POWER_DOWN_TXPD2_Shift                                                                  12

/*--------------------------------------
BitField Name: TXPD1
BitField Type: R/W
BitField Desc: Power Down subport 1
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXPD1_Mask                                                            cBit11_10
#define cAf6_SERDES_POWER_DOWN_TXPD1_Shift                                                                  10

/*--------------------------------------
BitField Name: TXPD0
BitField Type: R/W
BitField Desc: Power Down subport 0
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXPD0_Mask                                                              cBit9_8
#define cAf6_SERDES_POWER_DOWN_TXPD0_Shift                                                                   8

/*--------------------------------------
BitField Name: RXPD3
BitField Type: R/W
BitField Desc: Power Down subport 3
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_RXPD3_Mask                                                              cBit7_6
#define cAf6_SERDES_POWER_DOWN_RXPD3_Shift                                                                   6

/*--------------------------------------
BitField Name: RXPD2
BitField Type: R/W
BitField Desc: Power Down subport 2
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_RXPD2_Mask                                                              cBit5_4
#define cAf6_SERDES_POWER_DOWN_RXPD2_Shift                                                                   4

/*--------------------------------------
BitField Name: RXPD1
BitField Type: R/W
BitField Desc: Power Down subport 1
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_RXPD1_Mask                                                              cBit3_2
#define cAf6_SERDES_POWER_DOWN_RXPD1_Shift                                                                   2

/*--------------------------------------
BitField Name: RXPD0
BitField Type: R/W
BitField Desc: Power Down subport 0
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_RXPD0_Mask                                                              cBit1_0
#define cAf6_SERDES_POWER_DOWN_RXPD0_Shift                                                                   0

/*------------------------------------------------------------------------------
Reg Name   : Force Running Disparity Error
Reg Addr   : 0x0015-0x6015
Reg Formula: 0x0015+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
Force running disparity error, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_Force_Running_Disparity_Error_Base                                                      0x0015

/*--------------------------------------
BitField Name: Fdisp0_pid3
BitField Type: R/W
BitField Desc: Force running disparity error for port 3
BitField Bits: [07:06]
--------------------------------------*/
#define cAf6_Force_Running_Disparity_Error_Fdisp0_pid3_Mask                                            cBit7_6
#define cAf6_Force_Running_Disparity_Error_Fdisp0_pid3_Shift                                                 6

/*--------------------------------------
BitField Name: Fdisp0_pid2
BitField Type: R/W
BitField Desc: Force running disparity error for port 2
BitField Bits: [05:04]
--------------------------------------*/
#define cAf6_Force_Running_Disparity_Error_Fdisp0_pid2_Mask                                            cBit5_4
#define cAf6_Force_Running_Disparity_Error_Fdisp0_pid2_Shift                                                 4

/*--------------------------------------
BitField Name: Fdisp0_pid1
BitField Type: R/W
BitField Desc: Force running disparity error for port 1
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_Force_Running_Disparity_Error_Fdisp0_pid1_Mask                                            cBit3_2
#define cAf6_Force_Running_Disparity_Error_Fdisp0_pid1_Shift                                                 2

/*--------------------------------------
BitField Name: Fdisp0_pid0
BitField Type: R/W
BitField Desc: Force running disparity error for port 0
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_Force_Running_Disparity_Error_Fdisp0_pid0_Mask                                            cBit1_0
#define cAf6_Force_Running_Disparity_Error_Fdisp0_pid0_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : RX CDR Lock to Reference
Reg Addr   : 0x0017-0x6017
Reg Formula: 0x0017+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
RX CDR Lock to Reference, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_CDR_Lock_to_Reference_Base                                                           0x0017

/*--------------------------------------
BitField Name: lock2ref_pid3
BitField Type: R/W
BitField Desc: RX CDR Lock to Reference for port 3
BitField Bits: [03:03]
--------------------------------------*/
#define cAf6_RX_CDR_Lock_to_Reference_lock2ref_pid3_Mask                                                 cBit3
#define cAf6_RX_CDR_Lock_to_Reference_lock2ref_pid3_Shift                                                    3

/*--------------------------------------
BitField Name: lock2ref_pid2
BitField Type: R/W
BitField Desc: RX CDR Lock to Reference for port 2
BitField Bits: [02:02]
--------------------------------------*/
#define cAf6_RX_CDR_Lock_to_Reference_lock2ref_pid2_Mask                                                 cBit2
#define cAf6_RX_CDR_Lock_to_Reference_lock2ref_pid2_Shift                                                    2

/*--------------------------------------
BitField Name: lock2ref_pid1
BitField Type: R/W
BitField Desc: RX CDR Lock to Reference for port 1
BitField Bits: [01:01]
--------------------------------------*/
#define cAf6_RX_CDR_Lock_to_Reference_lock2ref_pid1_Mask                                                 cBit1
#define cAf6_RX_CDR_Lock_to_Reference_lock2ref_pid1_Shift                                                    1

/*--------------------------------------
BitField Name: lock2ref_pid0
BitField Type: R/W
BitField Desc: RX CDR Lock to Reference for port 0
BitField Bits: [00:00]
--------------------------------------*/
#define cAf6_RX_CDR_Lock_to_Reference_lock2ref_pid0_Mask                                                 cBit0
#define cAf6_RX_CDR_Lock_to_Reference_lock2ref_pid0_Shift                                                    0

/*------------------------------------------------------------------------------
Reg Name   : RX Monitor PPM Ref Value
Reg Addr   : 0x001C-0x601C
Reg Formula: 0x001C+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
RX Monitor PPM Ref Value, this reg is used to reset RX side base PPM difference between TX & RX, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_Monitor_PPM_Ref_Value_Base                                                           0x001C

/*--------------------------------------
BitField Name: ppmref_pid3
BitField Type: R/W
BitField Desc: RX Monitor PPM Ref Value for port 3
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_RX_Monitor_PPM_Ref_Value_ppmref_pid3_Mask                                               cBit31_24
#define cAf6_RX_Monitor_PPM_Ref_Value_ppmref_pid3_Shift                                                     24

/*--------------------------------------
BitField Name: ppmref_pid2
BitField Type: R/W
BitField Desc: RX Monitor PPM Ref Value for port 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_RX_Monitor_PPM_Ref_Value_ppmref_pid2_Mask                                               cBit23_16
#define cAf6_RX_Monitor_PPM_Ref_Value_ppmref_pid2_Shift                                                     16

/*--------------------------------------
BitField Name: ppmref_pid1
BitField Type: R/W
BitField Desc: RX Monitor PPM Ref Value for port 1
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_RX_Monitor_PPM_Ref_Value_ppmref_pid1_Mask                                                cBit15_8
#define cAf6_RX_Monitor_PPM_Ref_Value_ppmref_pid1_Shift                                                      8

/*--------------------------------------
BitField Name: ppmref_pid0
BitField Type: R/W
BitField Desc: RX Monitor PPM Ref Value for port 0
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_RX_Monitor_PPM_Ref_Value_ppmref_pid0_Mask                                                 cBit7_0
#define cAf6_RX_Monitor_PPM_Ref_Value_ppmref_pid0_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : RX Monitor PPM Mon Value
Reg Addr   : 0x001D-0x601D
Reg Formula: 0x001D+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
RX Monitor PPM Value, this reg is used to reset RX side base PPM difference between TX & RX, it will be compared to reg "RX Monitor PPM Ref Value", there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_Monitor_PPM_Mon_Value_Base                                                           0x001D

/*--------------------------------------
BitField Name: ppmval_pid3
BitField Type: R_O
BitField Desc: RX Monitor PPM Mon Value for port 3
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_RX_Monitor_PPM_Mon_Value_ppmval_pid3_Mask                                               cBit31_24
#define cAf6_RX_Monitor_PPM_Mon_Value_ppmval_pid3_Shift                                                     24

/*--------------------------------------
BitField Name: ppmval_pid2
BitField Type: R_O
BitField Desc: RX Monitor PPM Mon Value for port 2
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_RX_Monitor_PPM_Mon_Value_ppmval_pid2_Mask                                               cBit23_16
#define cAf6_RX_Monitor_PPM_Mon_Value_ppmval_pid2_Shift                                                     16

/*--------------------------------------
BitField Name: ppmval_pid1
BitField Type: R_O
BitField Desc: RX Monitor PPM Mon Value for port 1
BitField Bits: [15:08]
--------------------------------------*/
#define cAf6_RX_Monitor_PPM_Mon_Value_ppmval_pid1_Mask                                                cBit15_8
#define cAf6_RX_Monitor_PPM_Mon_Value_ppmval_pid1_Shift                                                      8

/*--------------------------------------
BitField Name: ppmval_pid0
BitField Type: R_O
BitField Desc: RX Monitor PPM Mon Value for port 0
BitField Bits: [07:00]
--------------------------------------*/
#define cAf6_RX_Monitor_PPM_Mon_Value_ppmval_pid0_Mask                                                 cBit7_0
#define cAf6_RX_Monitor_PPM_Mon_Value_ppmval_pid0_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : RX Monitor PPM Control
Reg Addr   : 0x001E-0x601E
Reg Formula: 0x001E+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
RX Monitor PPM Control, this reg is used to reset RX side base PPM difference between TX & RX, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_Monitor_PPM_Control_Base                                                             0x001E

/*--------------------------------------
BitField Name: ppmctrl_err
BitField Type: W1C
BitField Desc: RX Monitor PPM gets an error, bit per port of each group, bit0 is
port0, bit3 is port3
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_RX_Monitor_PPM_Control_ppmctrl_err_Mask                                                 cBit19_16
#define cAf6_RX_Monitor_PPM_Control_ppmctrl_err_Shift                                                       16

/*--------------------------------------
BitField Name: ppmctrl_enb
BitField Type: R/W
BitField Desc: Enable monitor PPM difference, bit per port of each group, bit0
is port0, bit3 is port3
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_RX_Monitor_PPM_Control_ppmctrl_enb_Mask                                                   cBit3_0
#define cAf6_RX_Monitor_PPM_Control_ppmctrl_enb_Shift                                                        0

/*-----------------------------------------------------------------------------------
Register Full Name: RX Auto Reset Masking Data
RTL Instant Name  : RX_Auto_Reset_Masking_Data
Address      :  0x001F-0x601F
Address is relative address, will be sum with Base_Address for each function block
Formula      : Address+($G*0x2000)
Where        : {$G(0-3) : Group of Ports}
Description  : RX Monitor PPM Control, this reg is used to reset RX side base PPM difference between TX & RX, there are 4 groups (0-3), each group has 4 sub ports
Width        : 32
 Field : [Bit:Bit] %% Name           %% Description                                  %% Type %% Reset %% Default
 Fieldx: [31:04]   %% Unused         %% Unused                                       %% R_O  %% 0x0   %% 0x0
 Fieldx: [03:00]   %% auto_rst_disdat%% Masking RX DATA is 0 when auto-reset, bit per port of each group, bit0 is port0, bit3 is port3
                                      {1} : enable masking
------------------------------------------------------------------------------------------*/
#define cAf6Reg_RX_Auto_Reset_Masking_Data_Base                                                     0x001F
#define cAf6_RX_Auto_Reset_Masking_Data_auto_rst_disdat_Mask                                        cBit0
#define cAf6_RX_Auto_Reset_Masking_Data_auto_rst_disdat_Shift                                        0

/*------------------------------------------------------------------------------
Reg Name   : RX RXLPMHFOVRDEN Control
Reg Addr   : 0x0018-0x6018
Reg Formula: 0x0018+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
RX RXLPMHFOVRDEN Control is used for lock-to-ref mode, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_RXLPMHFOVRDEN_Control_Base                                                           0x0018

/*--------------------------------------
BitField Name: rxlpmhfovrden
BitField Type: R/W
BitField Desc: RX RXLPMHFOVRDEN Control, bit per port of each group, bit0 is
port0, bit3 is port3
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_RX_RXLPMHFOVRDEN_Control_rxlpmhfovrden_Mask                                               cBit3_0
#define cAf6_RX_RXLPMHFOVRDEN_Control_rxlpmhfovrden_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RX RXLPMLFKLOVRDEN Control
Reg Addr   : 0x0019-0x6019
Reg Formula: 0x0019+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
RX RXLPMLFKLOVRDEN Control is used for lock-to-ref mode, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_RXLPMLFKLOVRDEN_Control_Base                                                         0x0019

/*--------------------------------------
BitField Name: rxlpmlfklovrden
BitField Type: R/W
BitField Desc: RX RXLPMLFKLOVRDEN Control, bit per port of each group, bit0 is
port0, bit3 is port3
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_RX_RXLPMLFKLOVRDEN_Control_rxlpmlfklovrden_Mask                                           cBit3_0
#define cAf6_RX_RXLPMLFKLOVRDEN_Control_rxlpmlfklovrden_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : RX RXOSOVRDEN Control
Reg Addr   : 0x001A-0x601A
Reg Formula: 0x001A+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
RX RXOSOVRDEN Control is used for lock-to-ref mode, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_RXOSOVRDEN_Control_Base                                                              0x001A

/*--------------------------------------
BitField Name: rxosovrden
BitField Type: R/W
BitField Desc: RX RXOSOVRDEN Control, bit per port of each group, bit0 is port0,
bit3 is port3
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_RX_RXOSOVRDEN_Control_rxosovrden_Mask                                                     cBit3_0
#define cAf6_RX_RXOSOVRDEN_Control_rxosovrden_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : RX RXLPMOSHOLD Control
Reg Addr   : 0x0030-0x6030
Reg Formula: 0x0030+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
RX RXLPMOSHOLD Control, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_RXLPMOSHOLD_Control_Base                                                             0x0030

/*--------------------------------------
BitField Name: rxlpmoshold
BitField Type: R/W
BitField Desc: RX RXLPMOSHOLD Control, bit per port of each group, bit0 is
port0, bit3 is port3, refer to UG578 for usage
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_RX_RXLPMOSHOLD_Control_rxlpmoshold_Mask                                                   cBit3_0
#define cAf6_RX_RXLPMOSHOLD_Control_rxlpmoshold_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : RX RXLPMOSOVRDEN Control
Reg Addr   : 0x0031-0x6031
Reg Formula: 0x0031+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
RX RXLPMOSOVRDEN Control, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_RXLPMOSOVRDEN_Control_Base                                                           0x0031

/*--------------------------------------
BitField Name: rxlpmosovrden
BitField Type: R/W
BitField Desc: RX RXLPMOSOVRDEN Control, bit per port of each group, bit0 is
port0, bit3 is port3, refer to UG578 for usage
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_RX_RXLPMOSOVRDEN_Control_rxlpmosovrden_Mask                                               cBit3_0
#define cAf6_RX_RXLPMOSOVRDEN_Control_rxlpmosovrden_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RX RXLPM_OS_CFG1 Control
Reg Addr   : 0x1038-0x7C38
Reg Formula: 0x1038+$G*0x2000+$P*400
    Where  : 
           + $G(0-3) : Group of Ports,$P(0-3) : Port of each group
Reg Desc   : 
RX RXLPM_OS_CFG1 Control, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_RXLPM_OS_CFG1_Control_Base                                                           0x1038

/*--------------------------------------
BitField Name: rxlpm_os_cfg1
BitField Type: R/W
BitField Desc: RX RXLPM_OS_CFG1 Control, refer to UG578 for usage
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_RX_RXLPM_OS_CFG1_Control_rxlpm_os_cfg1_Mask                                              cBit15_0
#define cAf6_RX_RXLPM_OS_CFG1_Control_rxlpm_os_cfg1_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : RX RXLPMGCHOLD Control
Reg Addr   : 0x0032-0x6032
Reg Formula: 0x0032+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
RX RXLPMGCHOLD Control, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_RXLPMGCHOLD_Control_Base                                                             0x0032

/*--------------------------------------
BitField Name: rxlpmgchold
BitField Type: R/W
BitField Desc: RX RXLPMGCHOLD Control, bit per port of each group, bit0 is
port0, bit3 is port3, refer to UG578 for usage
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_RX_RXLPMGCHOLD_Control_rxlpmgchold_Mask                                                   cBit3_0
#define cAf6_RX_RXLPMGCHOLD_Control_rxlpmgchold_Shift                                                        0


/*------------------------------------------------------------------------------
Reg Name   : RX RXLPMGCOVRDEN Control
Reg Addr   : 0x0033-0x6033
Reg Formula: 0x0033+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
RX RXLPMGCOVRDEN Control, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_RXLPMGCOVRDEN_CONTROL_Base                                                           0x0033

/*--------------------------------------
BitField Name: rxlpmgcovrden
BitField Type: R/W
BitField Desc: RX RXLPMGCOVRDEN Control, bit per port of each group, bit0 is
port0, bit3 is port3, refer to UG578 for usage
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_RX_RXLPMGCOVRDEN_CONTROL_rxlpmgcovrden_Mask                                               cBit3_0
#define cAf6_RX_RXLPMGCOVRDEN_CONTROL_rxlpmgcovrden_Shift                                                    0


/*------------------------------------------------------------------------------
Reg Name   : OC192 MUX OC48 EYESCAN Reset
Reg Addr   : 0x0034-0x6034
Reg Formula: 0x0034+$G*0x2000
    Where  : 
           + $G(0-3) : Group of Ports
Reg Desc   : 
Reset EYESCAN circuit , there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_OC192_MUX_OC48_EYESCAN_Reset_Base                                                       0x0034
#define cAf6Reg_OC192_MUX_OC48_EYESCAN_Reset(G)                                            (0x0034+(G)*0x2000)

/*--------------------------------------
BitField Name: eyescanrst_trig
BitField Type: R/W
BitField Desc: Trige 0->1 to start reset EYESCAN RESET, bit per sub port, Group
0 => Port0-3, Group 1 => Port 4-7, Group 2 => Port 8-11, Group 3 => Port 12-15
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_EYESCAN_Reset_eyescanrst_trig_Mask                                         cBit7_4
#define cAf6_OC192_MUX_OC48_EYESCAN_Reset_eyescanrst_trig_Shift                                              4

/*--------------------------------------
BitField Name: eyescan_reset
BitField Type: R/W
BitField Desc: bit per sub port. Group 0 => Port0-3, Group 1 => Port 4-7, ,
Group 2 => Port 8-11, Group 3 => Port 12-15
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_OC192_MUX_OC48_EYESCAN_Reset_eyescan_reset_Mask                                           cBit3_0
#define cAf6_OC192_MUX_OC48_EYESCAN_Reset_eyescan_reset_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : RX RXLPM_GC_CFG Control
Reg Addr   : 0x1039-0x7C39
Reg Formula: 0x1039+$G*0x2000+$P*400
    Where  : 
           + $G(0-3) : Group of Ports,$P(0-3) : Port of each group
Reg Desc   : 
RX RXLPM_GC_CFG Control, there are 4 groups (0-3), each group has 4 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_RX_RXLPM_GC_CFG_Control_Base                                                            0x1039

/*--------------------------------------
BitField Name: rxlpm_gc_cfg
BitField Type: R/W
BitField Desc: RX RXLPM_GC_CFG Control, refer to UG578 for usage
BitField Bits: [15:00]
--------------------------------------*/
#define cAf6_RX_RXLPM_GC_CFG_Control_rxlpm_gc_cfg_Mask                                                cBit15_0
#define cAf6_RX_RXLPM_GC_CFG_Control_rxlpm_gc_cfg_Shift                                                      0

#endif /* _AF6_REG_AF6CNC0021_OC192_MUX_OC48_16ch_wrap_RD_H_ */

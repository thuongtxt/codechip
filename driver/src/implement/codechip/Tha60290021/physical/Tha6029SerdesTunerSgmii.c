/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha6029SerdesTunerSgmii.c
 *
 * Created Date: Oct 7, 2016
 *
 * Description : SGMII SERDES tuner
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290021SerdesTunerInternal.h"
#include "Tha6029SgmiiSerdesTurningReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6029SerdesTunerSgmii
    {
    tTha60290021SerdesTuner super;
    }tTha6029SerdesTunerSgmii;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha6029SerdesTunerMethods     m_Tha6029SerdesTunerOverride;
static tTha60290021SerdesTunerMethods m_Tha60290021SerdesTunerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DrpLocalBaseAddress(Tha60290021SerdesTuner self)
    {
    AtUnused(self);
    return cAf6Reg_SERDES_DRP_PORT_Base;
    }

static uint32 RawPrbsSyncChangeMask(Tha6029SerdesTuner self, uint32 localId)
    {
    AtUnused(self);
    return cBit2 << localId;
    }

static uint32 RawPrbsSyncMask(Tha6029SerdesTuner self, uint32 localId)
    {
    AtUnused(self);
    return cBit10 << localId;
    }

static eBool UseQpll(Tha6029SerdesTuner self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideTha60290021SerdesTuner(Tha6029SerdesTuner self)
    {
    Tha60290021SerdesTuner tuner = (Tha60290021SerdesTuner)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SerdesTunerOverride, mMethodsGet(tuner), sizeof(m_Tha60290021SerdesTunerOverride));

        mMethodOverride(m_Tha60290021SerdesTunerOverride, DrpLocalBaseAddress);
        }

    mMethodsSet(tuner, &m_Tha60290021SerdesTunerOverride);
    }

static void OverrideTha6029SerdesTuner(Tha6029SerdesTuner self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SerdesTunerOverride, mMethodsGet(self), sizeof(m_Tha6029SerdesTunerOverride));

        mMethodOverride(m_Tha6029SerdesTunerOverride, RawPrbsSyncChangeMask);
        mMethodOverride(m_Tha6029SerdesTunerOverride, RawPrbsSyncMask);
        mMethodOverride(m_Tha6029SerdesTunerOverride, UseQpll);
        }

    mMethodsSet(self, &m_Tha6029SerdesTunerOverride);
    }

static void Override(Tha6029SerdesTuner self)
    {
    OverrideTha6029SerdesTuner(self);
    OverrideTha60290021SerdesTuner(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SerdesTunerSgmii);
    }

static Tha6029SerdesTuner ObjectInit(Tha6029SerdesTuner self, AtSerdesController serdes, uint32 groupId, uint32 localPortId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SerdesTunerObjectInit(self, serdes, groupId, localPortId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha6029SerdesTuner Tha6029SerdesTunerSgmiiNew(AtSerdesController serdes, uint32 groupId, uint32 localPortId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    Tha6029SerdesTuner newTuner = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newTuner, serdes, groupId, localPortId);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Serdes controller
 *
 * File        : Tha6029MateSerdesController.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Mate Serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtModuleSdh.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210011/sdh/Tha60210011ModuleSdh.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../man/Tha60290021DeviceReg.h"
#include "../man/Tha60290021Device.h"
#include "../prbs/Tha6029SerdesPrbsEngine.h"
#include "Tha60290021SerdesManagerInternal.h"
#include "Tha6029Physical.h"
#include "Tha6029SerdesTunningReg.h"
#include "Tha6029MateSerdesController.h"
#include "Tha6029MateSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6029MateSerdesController *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                 m_AtObjectOverride;
static tAtSerdesControllerMethods       m_AtSerdesControllerOverride;
static tTha6029SerdesControllerMethods  m_Tha6029SerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods = NULL;
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(AtSerdesController self)
    {
    return AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(self));
    }

static AtModuleSdh ModuleSdh(AtSerdesController self)
    {
    return (AtModuleSdh)AtDeviceModuleGet(Device(self), cAtModuleSdh);
    }

static uint8 NumSubPortsInGroup(AtSerdesController self)
    {
    AtUnused(self);
    return 4;
    }

static uint8 LocalIdGet(AtSerdesController self)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    return (uint8)(AtSerdesControllerIdGet(self) - Tha60290021SerdesManagerMateSerdesStartId(manager));
    }

static uint8 GroupId(AtSerdesController self)
    {
    return (uint8) (LocalIdGet(self) / NumSubPortsInGroup(self));
    }

static uint8 LocalPortIdInGroup(AtSerdesController self)
    {
    return (uint8) (LocalIdGet(self) % NumSubPortsInGroup(self));
    }

static uint32 BaseAddress(AtSerdesController self)
    {
    AtUnused(self);
    return cSerdesMateG0ConfigureBaseAdress;
    }

static Tha6029SerdesTuner Tuner(AtSerdesController self)
    {
    return Tha6029SerdesControllerSerdesTunerGet((Tha6029SerdesController)self);
    }

static Tha6029SerdesTuner SerdesTunerObjectCreate(Tha6029SerdesController self)
    {
    return Tha60290021SerdesTunerNew((AtSerdesController)self, GroupId((AtSerdesController)self), LocalPortIdInGroup((AtSerdesController)self));
    }

static AtDrp DrpObjectCreate(AtSerdesController self)
    {
    return Tha6029SerdesTunerDrpCreate(Tuner(self));
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtEyeScanController EyeScanControllerObjectCreate(AtSerdesController self, uint32 drpBaseAddress)
    {
    Tha60290021SerdesManager manager = (Tha60290021SerdesManager)AtSerdesControllerManagerGet(self);
    return Tha60290021SerdesManagerEyeScanControllerCreate(manager, self, drpBaseAddress);
    }

static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    return Tha6029SerdesTunerCanLoopback(Tuner(self), loopbackMode, enable);
    }

static eAtRet LoopbackSet(AtSerdesController self, eAtLoopbackMode loopback)
    {
    return Tha6029SerdesTunerLoopbackSet(Tuner(self), loopback);
    }

static eAtLoopbackMode LoopbackGet(AtSerdesController self)
    {
    return Tha6029SerdesTunerLoopbackGet(Tuner(self));
    }

static eAtSerdesEqualizerMode DefaultEqualizerMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesEqualizerModeDfe;
    }

static eAtRet EqualizerModeSet(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    return Tha6029SerdesTunerEqualizerModeSet(Tuner(self), mode);
    }

static eAtSerdesEqualizerMode EqualizerModeGet(AtSerdesController self)
    {
    return Tha6029SerdesTunerEqualizerModeGet(Tuner(self));
    }

static eBool EqualizerModeIsSupported(AtSerdesController self, eAtSerdesEqualizerMode mode)
    {
    return Tha6029SerdesTunerEqualizerModeIsSupported(Tuner(self), mode);
    }

static eBool EqualizerCanControl(AtSerdesController self)
    {
    return Tha6029SerdesTunerEqualizerCanControl(Tuner(self));
    }

static eAtRet PhysicalParamSet(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    return Tha6029SerdesTunerPhysicalParamSet(Tuner(self), param, value);
    }

static uint32 PhysicalParamGet(AtSerdesController self, eAtSerdesParam param)
    {
    return Tha6029SerdesTunerPhysicalParamGet(Tuner(self), param);
    }

static eBool PhysicalParamIsSupported(AtSerdesController self, eAtSerdesParam param)
    {
    return Tha6029SerdesTunerPhysicalParamIsSupported(Tuner(self), param);
    }

static eBool PhysicalParamValueIsInRange(AtSerdesController self, eAtSerdesParam param, uint32 value)
    {
    return Tha6029SerdesTunerPhysicalParamValueIsInRange(Tuner(self), param, value);
    }

static eBool PllIsLocked (AtSerdesController self)
    {
    return Tha6029SerdesTunerPllIsLocked(Tuner(self));
    }

static eBool PllChanged (AtSerdesController self)
    {
    return Tha6029SerdesTunerPllChanged(Tuner(self));
    }

static eBool PowerCanControl(AtSerdesController self)
    {
    return Tha6029SerdesTunerPowerCanControl(Tuner(self));
    }

static eAtRet PowerDown(AtSerdesController self, eBool powerDown)
    {
    return Tha6029SerdesTunerPowerDown(Tuner(self), powerDown);
    }

static eBool PowerIsDown(AtSerdesController self)
    {
    return Tha6029SerdesTunerPowerIsDown(Tuner(self));
    }

static eAtRet Reset(AtSerdesController self)
    {
    return Tha6029SerdesTunerReset(Tuner(self));
    }

static eAtRet RxReset(AtSerdesController self)
    {
    return Tha6029SerdesTunerRxReset(Tuner(self));
    }

static eAtRet TxReset(AtSerdesController self)
    {
    return Tha6029SerdesTunerTxReset(Tuner(self));
    }

static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return (mode == cAtSerdesModeStm16) ? cAtTrue : cAtFalse;
    }

static eAtSerdesMode DefaultMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeStm16;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    if (ModeIsSupported(self, mode) == cAtFalse)
        return cAtErrorModeNotSupport;

    if (AtSerdesControllerPhysicalPortGet(self) == NULL)
        AtSerdesControllerPhysicalPortSet(self, (AtChannel)AtModuleSdhLineGet(ModuleSdh(self), (uint8)AtSerdesControllerIdGet(self)));

    return cAtOk;
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeStm16;
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    uint8 localId = LocalIdGet((AtSerdesController)self);
    AtSnprintf(description, sizeof(description) - 1, "mate_serdes.%d", localId + 1);
    return description;
    }

static AtPrbsEngine PrbsEngineCreate(AtSerdesController self)
    {
    return Tha60290021MateSerdesPrbsEngineNew(self);
    }

static AtChannel PhysicalPortGet(AtSerdesController self)
    {
    AtSerdesManager manager = AtSerdesControllerManagerGet(self);
    uint32 localId = Tha60290021SerdesManagerMateSerdesLocalId(manager, AtSerdesControllerIdGet(self));
    return (AtChannel)Tha60290021ModuleSdhMateLineGet(ModuleSdh(self), (uint8)localId);
    }

static eBool IsControllable(AtSerdesController self)
    {
    if (Tha60290021DeviceIsRunningOnOtherPlatform(AtSerdesControllerDeviceGet(self)))
        return cAtFalse;

    return AtSerdesManagerSerdesIsControllable(AtSerdesControllerManagerGet(self), AtSerdesControllerIdGet(self));
    }

static float SpeedInGbps(AtSerdesController self)
    {
    AtUnused(self);
    return (float)(2.488);
    }

static eBool HasEyeScanReset(Tha6029SerdesController self)
    {
    Tha60290021SerdesManager manager = (Tha60290021SerdesManager)AtSerdesControllerManagerGet((AtSerdesController)self);
    return Tha60290021SerdesManagerSerdesEyeScanResetIsSupported(manager);
    }

static void OverrideTha6029SerdesController(AtSerdesController self)
    {
    Tha6029SerdesController controller = (Tha6029SerdesController)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SerdesControllerOverride, mMethodsGet(controller), sizeof(m_Tha6029SerdesControllerOverride));

        mMethodOverride(m_Tha6029SerdesControllerOverride, SerdesTunerObjectCreate);
        mMethodOverride(m_Tha6029SerdesControllerOverride, HasEyeScanReset);
        }

    mMethodsSet(controller, &m_Tha6029SerdesControllerOverride);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, ModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultMode);

        mMethodOverride(m_AtSerdesControllerOverride, PrbsEngineCreate);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalPortGet);

        mMethodOverride(m_AtSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, DrpObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackSet);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackGet);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultEqualizerMode);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EqualizerCanControl);

        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamSet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamGet);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalParamValueIsInRange);
        mMethodOverride(m_AtSerdesControllerOverride, PllIsLocked);
        mMethodOverride(m_AtSerdesControllerOverride, PllChanged);

        mMethodOverride(m_AtSerdesControllerOverride, PowerCanControl);
        mMethodOverride(m_AtSerdesControllerOverride, PowerDown);
        mMethodOverride(m_AtSerdesControllerOverride, PowerIsDown);
        mMethodOverride(m_AtSerdesControllerOverride, IsControllable);
 
        /* EyeScan */
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanControllerObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, SpeedInGbps);

        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        mMethodOverride(m_AtSerdesControllerOverride, RxReset);
        mMethodOverride(m_AtSerdesControllerOverride, TxReset);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    OverrideTha6029SerdesController(self);
    }

/* Construct Current Object */
AtSerdesController Tha6029MateSerdesControllerObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha6029MateSerdesController));

    /* Super constructor */
    if (Tha6029SerdesControllerObjectInit(self, physicalPort, serdesId, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha6029MateSerdesControllerNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha6029MateSerdesController));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6029MateSerdesControllerObjectInit(newModule, manager, physicalPort, serdesId);
    }

Tha6029SerdesTuner Tha6029MateSerdesTuner(AtSerdesController self)
    {
    if (self)
        return Tuner(self);
    return NULL;
    }

uint8 Tha6029MateSerdesControllerLocalPortIdInGroup(AtSerdesController self)
    {
    if (self)
        return LocalPortIdInGroup(self);
    return cInvalidUint8;
    }

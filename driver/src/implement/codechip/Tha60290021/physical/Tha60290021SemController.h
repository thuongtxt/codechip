/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60290021SemController.h
 * 
 * Created Date: Apr 27, 2017
 *
 * Description : SEM controller interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021SEMCONTROLLER_H_
#define _THA60290021SEMCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSemController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSemController Tha60290021SemControllerNew(AtDevice device, uint32 semId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021SEMCONTROLLER_H_ */


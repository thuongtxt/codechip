/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEVICE
 * 
 * File        : Tha6029SerdesControllerInternal.h
 * 
 * Created Date: Jul 11, 2016
 *
 * Description : Serdes Management
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029FACEPLATESERDESCONTROLLERINTERNAL_H_
#define _THA6029FACEPLATESERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha6029SerdesControllerInternal.h"
#include "Tha6029FaceplateSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cMaxLoopbackMode    3

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha6029FaceplateSerdesControllerMethods
    {
    eBool (*UseDifferentRemoteLoopbackFor10G)(Tha6029FaceplateSerdesController self);
    AtPrbsEngine (*RawPrbsEngineObjectCreate)(Tha6029FaceplateSerdesController self);
    eBool (*PmaFarEndLoopbackSupported)(Tha6029FaceplateSerdesController self);
    eBool (*EthFramedPrbsIsRemoved)(Tha6029FaceplateSerdesController self);
    eBool (*Stm1SamplingFromStm4)(Tha6029FaceplateSerdesController self);
    uint32 (*StartVersionShouldNotTouchAfeTrim)(Tha6029FaceplateSerdesController self);
    eBool (*HasNewParams)(Tha6029FaceplateSerdesController self);
    eBool (*DataMaskOnResetConfigurable)(Tha6029FaceplateSerdesController self);
    eAtModulePrbsRet (*PrbsHwSerdesModeSet)(Tha6029FaceplateSerdesController self, uint32 mode);
    AtPrbsEngine (*XfiPrbsEngineObjectCreate)(Tha6029FaceplateSerdesController self, uint32 baseAddress);
    eBool (*ShouldChangeGearBoxFor10GLoopback)(Tha6029FaceplateSerdesController self, eAtLoopbackMode newLoopback);
    }tTha6029FaceplateSerdesControllerMethods;

typedef struct tTha6029FaceplateSerdesController
    {
    tTha6029SerdesController super;
    const tTha6029FaceplateSerdesControllerMethods *methods;

    /* Private data */
    eAtSerdesMode mode;
    AtPrbsEngine rawSerdesPrbs;
    AtPrbsEngine xfiSerdesPrbs;
    eAtSerdesTimingMode timingMode;
    }tTha6029FaceplateSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha6029FaceplateSerdesControllerObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029FACEPLATESERDESCONTROLLERINTERNAL_H_ */

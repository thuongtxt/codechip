/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device
 *
 * File        : Tha6029SerdesController.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : Serdes Management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/eth/AtModuleEthInternal.h"
#include "Tha6029FaceplateSerdesControllerInternal.h"
#include "Tha6029SerdesControllerInternal.h"
#include "Tha6029SerdesTuner.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6029SerdesController)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6029SerdesControllerMethods m_methods;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods           = NULL;
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanEnable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    return enable;
    }

static eAtRet Enable(AtSerdesController self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool IsEnabled(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    if (enable)
        return AtSerdesControllerLoopbackSet(self, loopbackMode);

    /* Current loopback mode is not active */
    if (AtSerdesControllerLoopbackGet(self) != loopbackMode)
        return cAtOk;

    if ((loopbackMode == cAtLoopbackModeLocal) || (loopbackMode == cAtLoopbackModeRemote))
        return AtSerdesControllerLoopbackSet(self, cAtLoopbackModeRelease);

    return cAtOk;
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    return (AtSerdesControllerLoopbackGet(self) == loopbackMode) ? cAtTrue : cAtFalse;
    }

static eAtRet Init(AtSerdesController self)
    {
    eAtRet ret;

    ret = m_AtSerdesControllerMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    AtSerdesControllerLoopbackSet(self, cAtLoopbackModeRelease);

    return cAtOk;
    }

static uint32 EyeScanDrpBaseAddress(AtSerdesController self)
    {
    return AtDrpBaseAddress(AtSerdesControllerDrp(self));
    }

static AtMdio Mdio(AtSerdesController self)
    {
    AtChannel channel = AtSerdesControllerPhysicalPortGet(self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    return AtModuleEthSerdesMdio(ethModule, self);
    }

static uint32 SupportedInterruptMask(AtSerdesController self)
    {
    /* Everything is moved to port layer for easier control */
    AtUnused(self);
    return 0;
    }

static eAtRet InterruptMaskSet(AtSerdesController self, uint32 alarmMask, uint32 enableMask)
    {
    /* Apply disabling mask should be fine */
    if ((alarmMask & AtSerdesControllerSupportedInterruptMask(self)) == 0)
        return ((enableMask & alarmMask) == 0) ? cAtOk : cAtErrorModeNotSupport;

    return cAtOk;
    }

static Tha6029SerdesTuner SerdesTunerObjectCreate(Tha6029SerdesController self)
    {
    AtUnused(self);
    return NULL;
    }

static Tha6029SerdesTuner SerdesTunerGet(Tha6029SerdesController self)
    {
    if (self->tuner == NULL)
        self->tuner = mMethodsGet(self)->SerdesTunerObjectCreate(self);
    return self->tuner;
    }

static void Delete(AtObject self)
    {
    AtObjectDelete((AtObject)mThis(self)->tuner);
    mThis(self)->tuner = NULL;
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha6029SerdesController object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObject(tuner);
    }

static eAtRet EyeScanReset(AtSerdesController self, uint32 laneId)
    {
    Tha6029SerdesController serdes = (Tha6029SerdesController)self;

    if (mMethodsGet(serdes)->HasEyeScanReset(serdes))
        {
        Tha6029SerdesTuner tuner = Tha6029SerdesControllerSerdesTunerGet((Tha6029SerdesController)self);
        return Tha6029SerdesTunerEyeScanReset(tuner, laneId);
        }

    return cAtOk;
    }

static eBool HasEyeScanReset(Tha6029SerdesController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 EyeScanResetRegAddress(Tha6029SerdesController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, Init);
        mMethodOverride(m_AtSerdesControllerOverride, CanEnable);
        mMethodOverride(m_AtSerdesControllerOverride, Enable);
        mMethodOverride(m_AtSerdesControllerOverride, IsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanDrpBaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, Mdio);
        mMethodOverride(m_AtSerdesControllerOverride, SupportedInterruptMask);
        mMethodOverride(m_AtSerdesControllerOverride, InterruptMaskSet);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanReset);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    OverrideAtObject(self);
    }

static void MethodsInit(Tha6029SerdesController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, SerdesTunerObjectCreate);
        mMethodOverride(m_methods, HasEyeScanReset);
        mMethodOverride(m_methods, EyeScanResetRegAddress);
        }

    mMethodsSet(self, &m_methods);
    }

AtSerdesController Tha6029SerdesControllerObjectInit(AtSerdesController self, AtChannel physicalPort, uint32 serdesId, AtSerdesManager manager)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha6029SerdesController));

    /* Super constructor */
    if (AtSerdesControllerObjectInitWithManager(self, physicalPort, serdesId, manager) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

Tha6029SerdesTuner Tha6029SerdesControllerSerdesTunerGet(Tha6029SerdesController self)
    {
    if (self)
        return SerdesTunerGet(self);
    return NULL;
    }

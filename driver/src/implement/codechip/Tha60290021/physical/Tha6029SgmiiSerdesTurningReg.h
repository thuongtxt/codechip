/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2010 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date:                                                                
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII_H_
#define _AF6_REG_AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : SERDES DRP PORT
Reg Addr   : 0x800-0xFFF
Reg Formula: 0x800+$P*0x400+$DRP
    Where  : 
           + $P(0-1) : Sub Port ID
           + $DRP(0-1023) : DRP address, see UG578
Reg Desc   : 
Read/Write DRP address of SERDES,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_DRP_PORT_Base                                                                     0x800

/*--------------------------------------
BitField Name: drp_rw
BitField Type: R/W
BitField Desc: DRP read/write value
BitField Bits: [09:00]
--------------------------------------*/
#define cAf6_SERDES_DRP_PORT_drp_rw_Mask                                                               cBit9_0
#define cAf6_SERDES_DRP_PORT_drp_rw_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : SERDES LoopBack
Reg Addr   : 0x02
Reg Formula: 
    Where  : 
Reg Desc   : 
Configurate LoopBack,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_LoopBack_Base                                                                      0x02

/*--------------------------------------
BitField Name: lpback_subport1
BitField Type: R/W
BitField Desc: Loopback subport 1
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_SERDES_LoopBack_lpback_subport1_Mask                                                      cBit7_4
#define cAf6_SERDES_LoopBack_lpback_subport1_Shift                                                           4

/*--------------------------------------
BitField Name: lpback_subport0
BitField Type: R/W
BitField Desc: Loopback subport 0
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_SERDES_LoopBack_lpback_subport0_Mask                                                      cBit3_0
#define cAf6_SERDES_LoopBack_lpback_subport0_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : SERDES POWER DOWN
Reg Addr   : 0x03
Reg Formula: 
    Where  : 
Reg Desc   : 
Configurate Power Down ,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_POWER_DOWN_Base                                                                    0x03

/*--------------------------------------
BitField Name: TXPD1
BitField Type: R/W
BitField Desc: Power Down subport 1
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXPD1_Mask                                                            cBit11_10
#define cAf6_SERDES_POWER_DOWN_TXPD1_Shift                                                                  10

/*--------------------------------------
BitField Name: TXPD0
BitField Type: R/W
BitField Desc: Power Down subport 0
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_TXPD0_Mask                                                              cBit9_8
#define cAf6_SERDES_POWER_DOWN_TXPD0_Shift                                                                   8

/*--------------------------------------
BitField Name: RXPD1
BitField Type: R/W
BitField Desc: Power Down subport 1
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_RXPD1_Mask                                                              cBit3_2
#define cAf6_SERDES_POWER_DOWN_RXPD1_Shift                                                                   2

/*--------------------------------------
BitField Name: RXPD0
BitField Type: R/W
BitField Desc: Power Down subport 0
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_POWER_DOWN_RXPD0_Mask                                                              cBit1_0
#define cAf6_SERDES_POWER_DOWN_RXPD0_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : SERDES PLL Status
Reg Addr   : 0x0B
Reg Formula: 
    Where  : 
Reg Desc   : 
QPLL/CPLL status,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_PLL_Status_Base                                                                    0x0B

/*--------------------------------------
BitField Name: QPLL1_Lock_change
BitField Type: W1C
BitField Desc: QPLL1 has transition lock/unlock, Group 0-1
BitField Bits: [29:29]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_change_Mask                                                   cBit29
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_change_Shift                                                      29

/*--------------------------------------
BitField Name: QPLL0_Lock_change
BitField Type: W1C
BitField Desc: QPLL0 has transition lock/unlock, Group 0-1
BitField Bits: [28:28]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_change_Mask                                                   cBit28
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_change_Shift                                                      28

/*--------------------------------------
BitField Name: QPLL1_Lock
BitField Type: R_O
BitField Desc: QPLL0 is Locked, Group 0-1
BitField Bits: [25:25]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_Mask                                                          cBit25
#define cAf6_SERDES_PLL_Status_QPLL1_Lock_Shift                                                             25

/*--------------------------------------
BitField Name: QPLL0_Lock
BitField Type: R_O
BitField Desc: QPLL0 is Locked, Group 0-1
BitField Bits: [24:24]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_Mask                                                          cBit24
#define cAf6_SERDES_PLL_Status_QPLL0_Lock_Shift                                                             24

/*--------------------------------------
BitField Name: CPLL_Lock_Change
BitField Type: W1C
BitField Desc: CPLL has transition lock/unlock, bit per sub port,
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_CPLL_Lock_Change_Mask                                                 cBit15_12
#define cAf6_SERDES_PLL_Status_CPLL_Lock_Change_Shift                                                       12

/*--------------------------------------
BitField Name: CPLL_Lock
BitField Type: R_O
BitField Desc: CPLL is Locked, bit per sub port,
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_SERDES_PLL_Status_CPLL_Lock_Mask                                                          cBit3_0
#define cAf6_SERDES_PLL_Status_CPLL_Lock_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TX Reset
Reg Addr   : 0x0C
Reg Formula: 
    Where  : 
Reg Desc   : 
Reset TX SERDES,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TX_Reset_Base                                                                      0x0C

/*--------------------------------------
BitField Name: txrst_done
BitField Type: W1C
BitField Desc: TX Reset Done, bit per sub port
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_SERDES_TX_Reset_txrst_done_Mask                                                         cBit17_16
#define cAf6_SERDES_TX_Reset_txrst_done_Shift                                                               16

/*--------------------------------------
BitField Name: txrst_trig
BitField Type: R/W
BitField Desc: Should reset TX_PMA SERDES about 300-500 ns , bit per sub port
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_TX_Reset_txrst_trig_Mask                                                           cBit1_0
#define cAf6_SERDES_TX_Reset_txrst_trig_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : SERDES RX Reset
Reg Addr   : 0x0D
Reg Formula: 
    Where  : 
Reg Desc   : 
Reset RX SERDES,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_RX_Reset_Base                                                                      0x0D

/*--------------------------------------
BitField Name: rxrst_done
BitField Type: W1C
BitField Desc: RX Reset Done, bit per sub port
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_SERDES_RX_Reset_rxrst_done_Mask                                                         cBit17_16
#define cAf6_SERDES_RX_Reset_rxrst_done_Shift                                                               16

/*--------------------------------------
BitField Name: rxrst_trig
BitField Type: R/W
BitField Desc: Should reset reset RX_PMA SERDES about 300-500 ns , bit per sub
port
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_RX_Reset_rxrst_trig_Mask                                                           cBit1_0
#define cAf6_SERDES_RX_Reset_rxrst_trig_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : SERDES LPMDFE Mode
Reg Addr   : 0x0E
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure LPM/DFE mode ,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_LPMDFE_Mode_Base                                                                   0x0E

/*--------------------------------------
BitField Name: lpmdfe_mode
BitField Type: R/W
BitField Desc: bit per sub port
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_Mask                                                       cBit1_0
#define cAf6_SERDES_LPMDFE_Mode_lpmdfe_mode_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : SERDES LPMDFE Reset
Reg Addr   : 0x0F
Reg Formula: 
    Where  : 
Reg Desc   : 
Reset LPM/DFE ,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_LPMDFE_Reset_Base                                                                  0x0F

/*--------------------------------------
BitField Name: lpmdfe_reset
BitField Type: R/W
BitField Desc: bit per sub port, Must be toggled after switching between modes
to initialize adaptation
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_Mask                                                     cBit1_0
#define cAf6_SERDES_LPMDFE_Reset_lpmdfe_reset_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TXDIFFCTRL
Reg Addr   : 0x10
Reg Formula: 
    Where  : 
Reg Desc   : 
Driver Swing Control, see "Table 3-35: TX Configurable Driver Ports" page 158 of UG578 for more detail, there is  2 sub ports Group 0 => Port0-3, Group 1 => Port 4-7

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TXDIFFCTRL_Base                                                                    0x10

/*--------------------------------------
BitField Name: TXDIFFCTRL_subport1
BitField Type: R/W
BitField Desc:
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport1_Mask                                                cBit9_5
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport1_Shift                                                     5

/*--------------------------------------
BitField Name: TXDIFFCTRL_subport0
BitField Type: R/W
BitField Desc:
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_Mask                                                cBit4_0
#define cAf6_SERDES_TXDIFFCTRL_TXDIFFCTRL_subport0_Shift                                                     0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TXPOSTCURSOR
Reg Addr   : 0x11
Reg Formula: 
    Where  : 
Reg Desc   : 
Transmitter post-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 160 of UG578 for more detail, there is  2 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TXPOSTCURSOR_Base                                                                  0x11

/*--------------------------------------
BitField Name: TXPOSTCURSOR_subport1
BitField Type: R/W
BitField Desc:
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_Mask                                            cBit9_5
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport1_Shift                                                 5

/*--------------------------------------
BitField Name: TXPOSTCURSOR_subport0
BitField Type: R/W
BitField Desc:
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Mask                                            cBit4_0
#define cAf6_SERDES_TXPOSTCURSOR_TXPOSTCURSOR_subport0_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : SERDES TXPRECURSOR
Reg Addr   : 0x12
Reg Formula: 
    Where  : 
Reg Desc   : 
Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is  2 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_TXPRECURSOR_Base                                                                   0x12

/*--------------------------------------
BitField Name: TXPRECURSOR_subport1
BitField Type: R/W
BitField Desc:
BitField Bits: [09:05]
--------------------------------------*/
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport1_Mask                                              cBit9_5
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport1_Shift                                                   5

/*--------------------------------------
BitField Name: TXPRECURSOR_subport0
BitField Type: R/W
BitField Desc:
BitField Bits: [04:00]
--------------------------------------*/
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_Mask                                              cBit4_0
#define cAf6_SERDES_TXPRECURSOR_TXPRECURSOR_subport0_Shift                                                   0


/*------------------------------------------------------------------------------
Reg Name   : SERDES PRBS generator test pattern control
Reg Addr   : 0x20
Reg Formula: 
    Where  : 
Reg Desc   : 
Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is  2 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_prbsctr_pen_Base                                                                          0x20

/*--------------------------------------
BitField Name: PRBSSEL_subport1
BitField Type: R/W
BitField Desc: Transmitter PRBS generator and Receiver PRBS checker test pattern
control.After changing patterns, perform a reset of the RX (GTRXRESET,
RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the
RX pattern checker can attempt to reestablish the link acquired. No checking is
done for non-PRBS patterns.
BitField Bits: [07:04]
--------------------------------------*/
#define cAf6_prbsctr_pen_PRBSSEL_subport1_Mask                                                         cBit7_4
#define cAf6_prbsctr_pen_PRBSSEL_subport1_Shift                                                              4

/*--------------------------------------
BitField Name: PRBSSEL_subport0
BitField Type: R/W
BitField Desc: Transmitter PRBS generator and Receiver PRBS checker test pattern
control.After changing patterns, perform a reset of the RX (GTRXRESET,
RXPMARESET) or a reset of the PRBS error counter (RXPRBSCNTRESET) such that the
RX pattern checker can attempt to reestablish the link acquired. No checking is
done for non-PRBS patterns.
BitField Bits: [03:00]
--------------------------------------*/
#define cAf6_prbsctr_pen_PRBSSEL_subport0_Mask                                                         cBit3_0
#define cAf6_prbsctr_pen_PRBSSEL_subport0_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : SERDES prbsctr_pen
Reg Addr   : 0x21
Reg Formula: 
    Where  : 
Reg Desc   : 
Transmitter pre-cursor TX pre-emphasis control, see "Table 3-35: TX Configurable Driver Ports" page 161 of UG578 for more detail, there is  2 sub ports

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_prbsctr_pen_Base                                                                   0x21

/*--------------------------------------
BitField Name: RXPRBSCNTRESET_subport1
BitField Type: R/W
BitField Desc: Resets the PRBS error counter.
BitField Bits: [05]
--------------------------------------*/
#define cAf6_SERDES_prbsctr_pen_RXPRBSCNTRESET_subport1_Mask                                             cBit5
#define cAf6_SERDES_prbsctr_pen_RXPRBSCNTRESET_subport1_Shift                                                5

/*--------------------------------------
BitField Name: RXPRBSCNTRESET_subport0
BitField Type: R/W
BitField Desc: Resets the PRBS error counter.
BitField Bits: [04]
--------------------------------------*/
#define cAf6_SERDES_prbsctr_pen_RXPRBSCNTRESET_subport0_Mask                                             cBit4
#define cAf6_SERDES_prbsctr_pen_RXPRBSCNTRESET_subport0_Shift                                                4

/*--------------------------------------
BitField Name: TXPRBSFORCEERR_subport1
BitField Type: R/W
BitField Desc: When this port is driven High, errors are forced in the PRBS
transmitter. While this port is asserted, the output data pattern contains
errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA
BitField Bits: [01]
--------------------------------------*/
#define cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport1_Mask                                             cBit1
#define cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport1_Shift                                                1

/*--------------------------------------
BitField Name: TXPRBSFORCEERR_subport0
BitField Type: R/W
BitField Desc: When this port is driven High, errors are forced in the PRBS
transmitter. While this port is asserted, the output data pattern contains
errors. When PRBSSEL_subportx is set to 0, this port does not affect TXDATA
BitField Bits: [00]
--------------------------------------*/
#define cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport0_Mask                                             cBit0
#define cAf6_SERDES_prbsctr_pen_TXPRBSFORCEERR_subport0_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : SERDES PRBS Diag Status
Reg Addr   : 0x22
Reg Formula: 
    Where  : 
Reg Desc   : 
QPLL/CPLL status,

------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_PRBS_Status_Base                                                                   0x22

/*--------------------------------------
BitField Name: RXPRBSLOCKED
BitField Type: R_O
BitField Desc: Output to indicate that the RX PRBS checker has been error free
after reset. Once asserted High, RXPRBSLOCKED does not deassert until reset of
the RX pattern checker via a reset of the RX (GTRXRESET, RXPMARESET, or
RXPCSRESET in sequential mode) or a reset of the PRBS error counter
(RXPRBSCNTRESET), bit per sub port,
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_SERDES_PRBS_Status_RXPRBSLOCKED_Mask                                                    cBit11_10
#define cAf6_SERDES_PRBS_Status_RXPRBSLOCKED_Shift                                                          10

/*--------------------------------------
BitField Name: RXPRBSERR
BitField Type: R_O
BitField Desc: This non-sticky status output indicates that PRBS errors have
occurred. , bit per sub port,
BitField Bits: [09:08]
--------------------------------------*/
#define cAf6_SERDES_PRBS_Status_RXPRBSERR_Mask                                                         cBit9_8
#define cAf6_SERDES_PRBS_Status_RXPRBSERR_Shift                                                              8

/*--------------------------------------
BitField Name: RXPRBSLOCKED_Change
BitField Type: W1C
BitField Desc: This sticky status output indicates that PRBS not locked, bit per
sub port,
BitField Bits: [03:02]
--------------------------------------*/
#define cAf6_SERDES_PRBS_Status_RXPRBSLOCKED_Change_Mask                                               cBit3_2
#define cAf6_SERDES_PRBS_Status_RXPRBSLOCKED_Change_Shift                                                    2

/*--------------------------------------
BitField Name: RXPRBSERR_Change
BitField Type: W1C
BitField Desc: This sticky status output indicates that PRBS errors have
occurred. , bit per sub port,
BitField Bits: [01:00]
--------------------------------------*/
#define cAf6_SERDES_PRBS_Status_RXPRBSERR_Change_Mask                                                  cBit1_0
#define cAf6_SERDES_PRBS_Status_RXPRBSERR_Change_Shift                                                       0


/*------------------------------------------------------------------------------
Reg Name   : SERDES PRBS RX_PRBS_ERR_CNT
Reg Addr   : 0x25E - 0x25F (DRP Address)
Reg Formula: 
    Where  : 
Reg Desc   : 


------------------------------------------------------------------------------*/
#define cAf6Reg_SERDES_PRBS_RX_PRBS_ERR_CNT_Base                                                         0x25E

/*--------------------------------------
BitField Name: RX_PRBS_ERR_CNT
BitField Type: R/W
BitField Desc: PRBS error counter. This counter can be reset by asserting
RXPRBSCNTRESET. When a single bit error in incoming data occurs, this counter
increments by 1. Single bit errors are counted thus when multiple bit errors
occur in incoming data. The counter increments by the actual number of bit
errors. Counting begins after config mode and prbs syn . The counter saturates
at 32'hFFFFFFFF. This error counter can only be accessed via the DRP interface.
Because the DRP only outputs 16 bits of data per operation, two DRP transactions
must be completed to read out the complete 32-bit value. To properly read out
the error counter, read out the lower 16 bits at address 0x25E first, followed
by the upper 16 bits at address 0x25F
BitField Bits: [31:00]
--------------------------------------*/
#define cAf6_SERDES_PRBS_RX_PRBS_ERR_CNT_RX_PRBS_ERR_CNT_Mask                                         cBit31_0
#define cAf6_SERDES_PRBS_RX_PRBS_ERR_CNT_RX_PRBS_ERR_CNT_Shift                                               0

#endif /* _AF6_REG_AF6CNC0021_Xilinx_Serdes_Turning_2xSGMII_H_ */

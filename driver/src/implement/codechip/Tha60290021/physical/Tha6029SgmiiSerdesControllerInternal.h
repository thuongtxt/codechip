/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha6029SgmiiSerdesControllerInternal.h
 * 
 * Created Date: Apr 25, 2017
 *
 * Description : SGMII SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SGMIISERDESCONTROLLERINTERNAL_H_
#define _THA6029SGMIISERDESCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "Tha6029SerdesTuner.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SgmiiSerdesController * Tha6029SgmiiSerdesController;

typedef struct tTha6029SgmiiSerdesControllerMethods
    {
    eBool (*UseEngineOfPdhProduct)(Tha6029SgmiiSerdesController self);
    eBool (*ShouldUseMdioLocalLoopback)(Tha6029SgmiiSerdesController self);
    }tTha6029SgmiiSerdesControllerMethods;

typedef struct tTha6029SgmiiSerdesController
    {
    tAtSerdesController super;
    const tTha6029SgmiiSerdesControllerMethods *methods;

    /* Private data */
    Tha6029SerdesTuner tuner;
    eBool mdioLocalLoopbackEnabled; /* Only for simulation */
    }tTha6029SgmiiSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSerdesController Tha6029SgmiiSerdesControllerObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SGMIISERDESCONTROLLERINTERNAL_H_ */


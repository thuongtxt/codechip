/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Serdes controller
 *
 * File        : Tha6029BacklaneSerdesLane.c
 *
 * Created Date: Jul 22, 2016
 *
 * Description : Lane Serdes controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6029BackplaneSerdesControllerInternal.h"
#include "Tha6029Physical.h"
#include "Tha6029SerdesEth40GReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6029LaneSerdesController *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6029BacklaneSerdesLane
    {
    tTha6029SerdesController super;
    }tTha6029BacklaneSerdesLane;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/* Save super implementation */
static const tAtSerdesControllerMethods *m_AtSerdesControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 BaseAddress(AtSerdesController self)
    {
    AtSerdesController parent = AtSerdesControllerParentGet(self);
    return AtSerdesControllerBaseAddress(parent);
    }

static uint32 RegisterWithLocalAddress(AtSerdesController self, uint32 localAddress)
    {
    return AtSerdesControllerBaseAddress(self) + localAddress;
    }

static uint32 LoopbackRegister(AtSerdesController self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_ETH_40G_LoopBack_Base);
    }

static uint32 LoopbackModeMask(AtSerdesController self)
    {
    return cBit3_0 << (AtSerdesControllerHwIdGet(self) * 4);
    }

static uint32 LoopbackModeShift(AtSerdesController self)
    {
    return AtSerdesControllerHwIdGet(self) * 4;
    }

static uint32 LocalLoopbackMask(AtSerdesController self)
    {
    AtUnused(self);
    return cBit1;
    }

static uint32 LocalLoopbackShift(AtSerdesController self)
    {
    AtUnused(self);
    return 1;
    }

static uint32 RemoteLoopbackMask(AtSerdesController self)
    {
    AtUnused(self);
    return cBit2;
    }

static uint32 RemoteLoopbackShift(AtSerdesController self)
    {
    AtUnused(self);
    return 2;
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    uint32 regAddr = LoopbackRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleSdh);
    uint32 loopbackModeMask = LoopbackModeMask(self);
    uint32 loopbackModeShift = LoopbackModeShift(self);
    uint32 loopbackVal = mRegField(regVal, loopbackMode);
    uint32 localLoopbackMask  = LocalLoopbackMask(self);
    uint32 localLoopbackShift  = LocalLoopbackShift(self);
    uint32 remoteLoopbackMask  = RemoteLoopbackMask(self);
    uint32 remoteLoopbackShift = RemoteLoopbackShift(self);

    switch ((uint32)loopbackMode)
        {
        case cAtLoopbackModeLocal:
            mRegFieldSet(loopbackVal, localLoopback, enable ? 1 : 0);
            break;

        case cAtLoopbackModeRemote:
            mRegFieldSet(loopbackVal, remoteLoopback, enable ? 1 : 0);
            break;

        case cAtLoopbackModeDual:
            mRegFieldSet(loopbackVal, localLoopback, enable ? 1 : 0);
            mRegFieldSet(loopbackVal, remoteLoopback, enable ? 1 : 0);
            break;

        case cAtLoopbackModeRelease:
            if (enable)
                {
                mRegFieldSet(loopbackVal, localLoopback, 0);
                mRegFieldSet(loopbackVal, remoteLoopback, 0);
                }
            break;

        default:
            return cAtErrorModeNotSupport;
        }

    mRegFieldSet(regVal, loopbackMode, loopbackVal);
    AtSerdesControllerWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    uint32 regAddr = LoopbackRegister(self);
    uint32 regVal = AtSerdesControllerRead(self, regAddr, cAtModuleSdh);
    uint32 loopbackModeMask = LoopbackModeMask(self);
    uint32 loopbackModeShift = LoopbackModeShift(self);
    uint32 loopbackVal = mRegField(regVal, loopbackMode);
    uint32 localLoopback = loopbackVal & LocalLoopbackMask(self);
    uint32 remoteLoopback = loopbackVal & RemoteLoopbackMask(self);

    switch ((uint32)loopbackMode)
        {
        case cAtLoopbackModeLocal:
            return localLoopback ? cAtTrue : cAtFalse;

        case cAtLoopbackModeRemote:
            return remoteLoopback ? cAtTrue : cAtFalse;

        case cAtLoopbackModeDual:
            return (remoteLoopback && localLoopback) ? cAtTrue : cAtFalse;

        default:
            return cAtFalse;
        }
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    AtSerdesController serdes = (AtSerdesController)self;
    AtSerdesController parent = AtSerdesControllerParentGet(serdes);
    AtSnprintf(description,
               sizeof(description) - 1, "serdes_lane.%d.%d",
               AtSerdesControllerIdGet(parent) + 1,
               AtSerdesControllerIdGet(serdes) + 1);
    return description;
    }

static uint32 DrpBaseAddress(AtSerdesController self)
    {
    uint32 localAddress = cAf6Reg_OETH_40G_DRP_Base + (AtSerdesControllerIdGet(self) * 0x400);
    return RegisterWithLocalAddress(self, localAddress);
    }

static AtDrp DrpObjectCreate(AtSerdesController self)
    {
    AtSerdesController parent = AtSerdesControllerParentGet(self);
    AtDevice device = AtSerdesManagerDeviceGet(AtSerdesControllerManagerGet(parent));
    AtHal hal = AtDeviceIpCoreHalGet(device, 0);
    return AtDrpNew(DrpBaseAddress(self), hal);
    }

static eBool ModeIsSupported(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return (mode == cAtSerdesModeEth10G) ? cAtTrue : cAtFalse;
    }

static eAtRet ModeSet(AtSerdesController self, eAtSerdesMode mode)
    {
    AtUnused(self);
    return (mode == cAtSerdesModeEth10G) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtSerdesMode ModeGet(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeEth10G;
    }

static AtChannel PhysicalPortGet(AtSerdesController self)
    {
    AtSerdesController parent = AtSerdesControllerParentGet(self);
    return AtSerdesControllerPhysicalPortGet(parent);
    }

static eAtSerdesMode DefaultMode(AtSerdesController self)
    {
    AtUnused(self);
    return cAtSerdesModeEth10G;
    }

static float SpeedInGbps(AtSerdesController self)
    {
    AtUnused(self);
    return (float) (4 * 3.125);
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSerdesControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, m_AtSerdesControllerMethods, sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, BaseAddress);
        mMethodOverride(m_AtSerdesControllerOverride, DrpObjectCreate);
        mMethodOverride(m_AtSerdesControllerOverride, ModeSet);
        mMethodOverride(m_AtSerdesControllerOverride, ModeGet);
        mMethodOverride(m_AtSerdesControllerOverride, DefaultMode);
        mMethodOverride(m_AtSerdesControllerOverride, ModeIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, PhysicalPortGet);
        mMethodOverride(m_AtSerdesControllerOverride, SpeedInGbps);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void OverrideAtObject(AtSerdesController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, mMethodsGet(object), sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtObject(self);
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029BacklaneSerdesLane);
    }

static AtSerdesController ObjectInit(AtSerdesController self, AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SerdesControllerObjectInit(self, physicalPort, serdesId, manager) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha6029BacklaneSerdesLaneNew(AtSerdesManager manager, AtChannel physicalPort, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, manager, physicalPort, serdesId);
    }

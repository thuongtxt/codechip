/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60290021HdlcChannel.c
 *
 * Created Date: Sep 3, 2016
 *
 * Description : HDLC channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../pw/Tha60290021DccKbyteReg.h"
#include "../pw/Tha60290021ModulePw.h"
#include "../man/Tha60290021Device.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "../encap/Tha6029DccHdlcPktGenerator.h"
#include "Tha60290021HdlcChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cTha60290021HdlcMtu 1536
#define mThis(self) ((Tha60290021HdlcChannel)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021HdlcChannelMethods m_methods;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;
static tAtEncapChannelMethods m_AtEncapChannelOverride;
static tAtHdlcChannelMethods  m_AtHdlcChannelOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;
static const tAtChannelMethods      *m_AtChannelMethods      = NULL;
static const tAtEncapChannelMethods *m_AtEncapChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ChannelOffSet(Tha60290021HdlcChannel self)
    {
    return AtChannelIdGet((AtChannel) self);
    }

static ThaModuleOcn OcnModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (ThaModuleOcn) AtDeviceModuleGet(device, cThaModuleOcn);
    }

static uint32 LineIdGet(AtChannel self)
    {
    AtSdhLine line = Tha60290021HdlcDccChannelLineGet((AtHdlcChannel)self);
    return AtChannelIdGet((AtChannel) line);
    }

static uint32 BaseAddress(AtHdlcChannel self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule((AtChannel)self));
    }

static uint8 IdlePatternSw2Hw(uint8 idlePattern)
    {
    return (idlePattern == 0x7E) ? 0 : 1;
    }

static uint8 IdlePatternHw2Sw(uint8 idlePattern)
    {
    return (idlePattern == 0) ? 0x7E: 0xFF;
    }

static uint32 RegisterWithLocalAddress(AtHdlcChannel self, uint32 localAddress)
    {
    uint32 offset = mMethodsGet(mThis(self))->ChannelOffSet(mThis(self));
    return BaseAddress(self) + localAddress + offset;
    }

static uint32 EncapContrlReg(AtHdlcChannel self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_upen_hdlc_enc_ctrl_reg1_Base);
    }

static eAtRet DecapContrlReg(AtHdlcChannel self)
    {
    return RegisterWithLocalAddress(self, cAf6Reg_upen_hdlc_locfg_Base);
    }

static eAtRet HwIdlePatternSet(AtHdlcChannel self, uint8 idlePattern)
    {
    uint32 regAddr = EncapContrlReg(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_hdlc_enc_ctrl_reg1_encap_idlemod_, IdlePatternSw2Hw(idlePattern));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eBool SupportedIdlePattern(AtHdlcChannel self, uint8 idle)
    {
    AtUnused(self);

    if ((idle == 0xFF) || (idle == 0x7E))
        return cAtTrue;

    return cAtFalse;
    }

static eAtModuleEncapRet IdlePatternSet(AtHdlcChannel self, uint8 idle)
    {
    if (SupportedIdlePattern(self, idle))
        return HwIdlePatternSet(self, idle);

    return cAtErrorModeNotSupport;
    }

static uint8 IdlePatternGet(AtHdlcChannel self)
    {
    uint32 regAddr = EncapContrlReg(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    return IdlePatternHw2Sw((uint8) mRegField(regVal, cAf6_upen_hdlc_enc_ctrl_reg1_encap_idlemod_));
    }

static eAtRet FlagSet(AtHdlcChannel self, uint8 flag)
    {
    if (AtHdlcChannelFlagIsSupported(self, flag))
        return cAtOk;

    return cAtErrorModeNotSupport;
    }

static uint8 FlagGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return 0x7E;
    }

static eBool FlagIsSupported(AtHdlcChannel self, uint8 flag)
    {
    AtUnused(self);

    if (flag == 0x7E)
        return cAtTrue;

    return cAtFalse;
    }

static uint32 FcsSw2Hw(uint32 fcsMode)
    {
    switch (fcsMode)
        {
        case cAtHdlcFcsModeFcs16  : return 0;
        case cAtHdlcFcsModeFcs32  : return 1;
        default:                    return 0;
        }
    }

static eAtRet EncapFcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    uint32 regAddr = EncapContrlReg(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsmod_, FcsSw2Hw(fcsMode));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eAtRet DecapFcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    uint32 regAddr = DecapContrlReg(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_hdlc_locfg_cfg_fcsmode_, FcsSw2Hw(fcsMode));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet FcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    eAtRet ret = cAtOk;

    if (!AtHdlcChannelFcsModeIsSupported(self, fcsMode))
        return cAtErrorModeNotSupport;

    ret |= EncapFcsModeSet(self, fcsMode);
    ret |= DecapFcsModeSet(self, fcsMode);

    return ret;
    }

static eAtHdlcFcsMode FcsHw2Sw(uint32 fcsMode)
    {
    return (fcsMode == 0) ? cAtHdlcFcsModeFcs16 : cAtHdlcFcsModeFcs32;
    }

static eAtHdlcFcsMode FcsModeGet(AtHdlcChannel self)
    {
    uint32 regAddr = EncapContrlReg(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    return FcsHw2Sw((uint8) mRegField(regVal, cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsmod_));
    }

static eBool FcsModeIsSupported(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    AtUnused(self);

    switch (fcsMode)
        {
        case cAtHdlcFcsModeFcs16  : return cAtTrue;

        /* Although this is in RD, but the RX direction only expect FCS16. This
         * is to say, FCS-32 will not be supported */
        case cAtHdlcFcsModeFcs32  : return cAtFalse;

        case cAtHdlcFcsModeNoFcs  : return cAtFalse;
        case cAtHdlcFcsModeUnknown: return cAtFalse;
        default:                    return cAtFalse;
        }
    }

static eAtRet MtuSet(AtHdlcChannel self, uint32 mtu)
    {
    AtUnused(self);
    return (mtu == cTha60290021HdlcMtu) ? cAtOk : cAtErrorOutOfRangParm;
    }

static uint32 MtuGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return cTha60290021HdlcMtu;
    }

static uint32 StuffModeSw2Hw(uint32 stuffMode)
    {
    switch (stuffMode)
        {
        case cAtHdlcStuffBit   : return 1;
        case cAtHdlcStuffByte  : return 0;
        default:                 return 1;
        }
    }

static eAtHdlcStuffMode StuffModeHw2Sw(uint32 stuffMode)
    {
    return (stuffMode == 0) ? cAtHdlcStuffByte : cAtHdlcStuffBit;
    }

static eAtRet StuffHwModeSet(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    uint32 regAddr, regVal;
    uint32 stuffHwMode = StuffModeSw2Hw(stuffMode);

    regAddr = EncapContrlReg(self);
    regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_hdlc_enc_ctrl_reg1_encap_stfmod_, stuffHwMode);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    regAddr = DecapContrlReg(self);
    regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_hdlc_locfg_cfg_bitstuff_, stuffHwMode);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet StuffModeSet(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    if (AtHdlcChannelStuffModeIsSupported(self, stuffMode))
        return StuffHwModeSet(self, stuffMode);

    return cAtErrorModeNotSupport;
    }

static eAtHdlcStuffMode StuffModeGet(AtHdlcChannel self)
    {
    uint32 regVal =  mChannelHwRead(self, EncapContrlReg(self), cAtModuleSdh);
    return StuffModeHw2Sw((uint32) mRegField(regVal, cAf6_upen_hdlc_enc_ctrl_reg1_encap_stfmod_));
    }

static eBool StuffModeIsSupported(AtHdlcChannel self, eAtHdlcStuffMode stuffMode)
    {
    AtUnused(self);

    switch (stuffMode)
        {
        case cAtHdlcStuffBit   : return cAtTrue;
        case cAtHdlcStuffByte  : return cAtTrue;
        default:                 return cAtFalse;
        }
    }

static eAtRet EncapScrambleEnable(AtEncapChannel self, eBool enable)
    {
    uint32 regAddr = EncapContrlReg((AtHdlcChannel) self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_hdlc_enc_ctrl_reg1_encap_screnb_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet DecapScrambleEnable(AtEncapChannel self, eBool enable)
    {
    uint32 regAddr = DecapContrlReg((AtHdlcChannel) self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_hdlc_locfg_cfg_scren_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet ScrambleEnable(AtEncapChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= EncapScrambleEnable(self, enable);
    ret |= DecapScrambleEnable(self, enable);

    return ret;
    }

static eBool EncapScrambleIsEnabled(AtEncapChannel self)
    {
    uint32 regAddr = EncapContrlReg((AtHdlcChannel) self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    return mBinToBool(mRegField(regVal, cAf6_upen_hdlc_enc_ctrl_reg1_encap_screnb_));
    }

static eBool ScrambleIsEnabled(AtEncapChannel self)
    {
    return EncapScrambleIsEnabled(self);
    }

static uint8 NumFlagSw2Hw(uint8 numFlag)
    {
    return (uint8)(numFlag - 1);
    }

static uint8 NumFlagHw2Sw(uint8 numFlag)
    {
    return (uint8)(numFlag + 1);
    }

static eAtRet EncapNumFlagsSet(AtHdlcChannel self, uint8 numFlag)
    {
    uint32 regAddr = EncapContrlReg(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_hdlc_enc_ctrl_reg1_encap_flgmod_, NumFlagSw2Hw(numFlag));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet NumFlagsSet(AtHdlcChannel self, uint8 numFlag)
    {
    if ((numFlag > 2) || (numFlag == 0))
        return cAtErrorOutOfRangParm;

    return EncapNumFlagsSet(self, numFlag);
    }

static uint8 NumFlagsGet(AtHdlcChannel self)
    {
    uint32 regAddr = EncapContrlReg(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    return NumFlagHw2Sw((uint8) mRegField(regVal, cAf6_upen_hdlc_enc_ctrl_reg1_encap_flgmod_));
    }

static eAtRet EncapFrameBitOrderSet(Tha60290021HdlcChannel self, eAtBitOrder order)
    {
    uint32 regAddr, regVal;

    /* LSB as default */
    regAddr = BaseAddress((AtHdlcChannel)self) + cAf6Reg_upen_hdlc_enc_master_ctrl_Base;
    regVal  =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_hdlc_enc_master_ctrl_encap_lsbfirst_, (order == cAtBitOrderLsb) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet EncapDefault(AtHdlcChannel self)
    {
    uint32 regAddr, regVal;

    regAddr = EncapContrlReg(self);
    regVal  =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_hdlc_enc_ctrl_reg1_encap_flgmod_, 0); /* at least 1 flag inserted */
    mRegFieldSet(regVal, cAf6_upen_hdlc_enc_ctrl_reg1_encap_fcsins_, 1); /* enable to insert FCS */
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet DecapFrameBitOrderSet(Tha60290021HdlcChannel self, eAtBitOrder order)
    {
    uint32 regAddr = BaseAddress((AtHdlcChannel)self) + cAf6Reg_upen_hdlc_loglbcfg_Base;
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_hdlc_loglbcfg_cfg_lsbfirst_, (order == cAtBitOrderLsb) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eAtRet DecapDefault(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static const char *TypeString(AtChannel self)
    {
    AtUnused(self);
    return "hdlc_dcc";
    }

static const char *LayerString(eAtSdhLineDccLayer layer)
    {
    static char tempString[16];
    static char layerString[32];
    AtOsal osal = AtSharedDriverOsalGet();
    eBool hasPrint = cAtFalse;

    mMethodsGet(osal)->MemInit(osal, layerString, 0, sizeof (layerString));

    if (layer & cAtSdhLineDccLayerSection)
        {
        AtSprintf(tempString, "%s", "section");
        AtStrcat(layerString, tempString);
        hasPrint = cAtTrue;
        }

    if (layer & cAtSdhLineDccLayerLine)
        {
        if (hasPrint)
            AtStrcat(layerString, "|");
        AtSprintf(tempString, "%s", "line");
        AtStrcat(layerString, tempString);
        }

    if (layer & cAtSdhLineDccLayerAll)
        {
        if (hasPrint)
            AtStrcat(layerString, "|");
        AtSprintf(tempString, "%s", "all");
        AtStrcat(layerString, tempString);
        }

    return layerString;
    }

static const char *IdString(AtChannel self)
    {
    static char idString[64];
    AtSdhLine line = Tha60290021HdlcDccChannelLineGet((AtHdlcChannel) self);
    eAtSdhLineDccLayer layer = Tha60290021HdlcDccChannelLayerGet((AtHdlcChannel) self);

    AtSprintf(idString, "%u.%s", AtChannelIdGet((AtChannel) line) + 1, LayerString(layer));

    return idString;
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    if (AtChannelBoundPwGet(self) != NULL)
        return cAtErrorChannelBusy;

    if (pseudowire == NULL)
        return cAtErrorNotExist;

    return cAtOk;
    }

static eBool ShouldDeletePwOnCleanup(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool HasPacketCounterDuringFifoFull(Tha60290021HdlcChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Delete(AtObject self)
    {
    AtChannelBoundPwObjectDelete((AtChannel)self);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290021HdlcChannel object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(layers);
    mEncodeObjectDescription(pseudowire);
    mEncodeUInt(txEnabled);
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = cAtOk;
    AtHdlcChannel hdlcChannel = (AtHdlcChannel)self;

    ret |= AtHdlcChannelFlagSet(hdlcChannel, 0x7E);
    ret |= AtHdlcChannelIdlePatternSet(hdlcChannel, 0x7E);
    ret |= AtHdlcChannelNumFlagsSet(hdlcChannel, 1);
    ret |= AtHdlcChannelFcsModeSet(hdlcChannel, cAtHdlcFcsModeFcs16);
    ret |= AtHdlcChannelStuffModeSet(hdlcChannel, cAtHdlcStuffBit);
    ret |= EncapDefault(hdlcChannel);
    ret |= DecapDefault(hdlcChannel);

    /* Disable all interrupt mask */
    ret |= AtChannelInterruptMaskSet(self, AtChannelSupportedInterruptMasks(self), 0);

    /* Let application explicitly enable */
    ret |= AtChannelEnable(self, cAtFalse);
    ret |= Tha6029DccHdlcPktGenChannelEnable(hdlcChannel, cAtFalse);

    ret |= AtHdlcChannelFcsCalculationModeSet(hdlcChannel, cAtHdlcFcsCalculationModeLsb);
    ret |= AtEncapChannelScrambleEnable((AtEncapChannel)self, cAtFalse);

    return ret;
    }

static uint32 CounterBaseAddressByHwType(Tha60290021HdlcChannel self, uint32 hwType)
    {
    AtUnused(self);
    return cAf6Reg_upen_dcc_cnt_V2_Base + (hwType * 32);
    }

static uint32 CounterBaseAddress(Tha60290021HdlcChannel self, uint16 counterType, eBool r2c)
    {
    AtUnused(self);
    AtUnused(r2c);

    switch (counterType)
        {
        case cAtHdlcChannelCounterTypeTxPackets:      return cAf6Reg_upen_eop2ocn_pcid_Base;
        case cAtHdlcChannelCounterTypeTxBytes:        return cAf6Reg_upen_byt2ocn_pcid_Base;
        case cAtHdlcChannelCounterTypeTxDiscardFrames:return cAf6Reg_upen_ful2ocn_pcid_Base;

        case cAtHdlcChannelCounterTypeRxPackets:      return cAf6Reg_upen_eop2dcc_pcid_Base;
        case cAtHdlcChannelCounterTypeRxBytes:        return cAf6Reg_upen_byt2dcc_pcid_Base;
        case cAtHdlcChannelCounterTypeRxFcsErrors:    return cAf6Reg_upen_err2dcc_pcid_Base;
        case cAtHdlcChannelCounterTypeRxDiscardFrames:return cAf6Reg_upen_errfull_pcid_Base;

        default:                                      return cInvalidUint32;
        }
    }

static uint32 HwCounterRead2Clear(AtChannel self, uint32 counterBaseAddress, eBool r2c)
    {
    uint32 Offset = mMethodsGet(mThis(self))->ChannelOffSet(mThis(self));
    uint32 regAddr = BaseAddress((AtHdlcChannel) self) + counterBaseAddress + Offset;
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    if (r2c)
        mChannelHwWrite(self, regAddr, 0x0, cAtModuleSdh);

    return regVal;
    }

static uint32 CounterReadGet(AtChannel self, uint16 counterType, eBool r2c)
    {
    uint32 counterAddress = mMethodsGet(mThis(self))->CounterBaseAddress(mThis(self), counterType, r2c);
    if (counterAddress == cInvalidUint32)
        return 0x0;

    return HwCounterRead2Clear(self, counterAddress, r2c);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (AtChannelCounterIsSupported(self, counterType))
        return CounterReadGet(self, counterType, cAtFalse);
    return 0x0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (AtChannelCounterIsSupported(self, counterType))
        return CounterReadGet(self, counterType, cAtTrue);

    return 0x0;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (CounterBaseAddress((Tha60290021HdlcChannel)self, counterType, cAtTrue) == cInvalidUint32)
        return cAtFalse;
    return cAtTrue;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    eAtRet ret = AtChannelTxEnable(self, enable);
    ret |= AtChannelRxEnable(self, enable);
    return ret;
    }

static eBool IsEnabled(AtChannel self)
    {
    return AtChannelRxIsEnabled(self);
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static uint32 UpenDccdecAddress(Tha60290021HdlcChannel self)
    {
    AtPw pw;
    uint32 localAddr;
    AtChannel channel = (AtChannel)self;

    pw = AtChannelBoundPwGet(channel);
    if (pw == NULL)
        return cInvalidUint32;

    localAddr = ThaModuleOcnUpenDccdecBase(OcnModule(channel)) + Tha6029PwDccPwExpectedLabelGet((Tha60290021PwHdlc)pw);
    return localAddr + BaseAddress((AtHdlcChannel)self);
    }

static eAtRet TxDccHwEnable(Tha60290021HdlcChannel self, eBool enable)
    {
    uint32 regAddr = UpenDccdecAddress(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_dccdec_Channel_enable_, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eBool IsValidLabel(AtPw pw)
    {
    uint8 label = Tha6029PwDccPwExpectedLabelGet((Tha60290021PwHdlc)pw);

    if (label == cInvalidUint8)
        return cAtFalse;
    return cAtTrue;
    }

static eAtRet TxDccEnable(AtChannel self, eBool enable)
    {
    AtPw pw = AtChannelBoundPwGet(self);

    mThis(self)->txEnabled = enable;

    if (pw && IsValidLabel(pw))
        return mMethodsGet(mThis(self))->TxDccHwEnable(mThis(self), enable);

    return cAtOk;
    }

static eBool TxDccIsEnabled(AtChannel self)
    {
    return mThis(self)->txEnabled;
    }

static eBool HasTohBusEnabling(Tha60290021HdlcChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet TxLineInsertionHandle(AtChannel self, eBool enable)
    {
    AtSdhLine line = Tha60290021HdlcDccChannelLineGet((AtHdlcChannel)self);
    AtHdlcChannel theOtherDcc = NULL;
    eAtSdhLineDccLayer layer = Tha60290021HdlcDccChannelLayerGet((AtHdlcChannel) self);

    if (layer == cAtSdhLineDccLayerSection)
        theOtherDcc = AtSdhLineDccChannelGet(line, cAtSdhLineDccLayerLine);
    else if (layer == cAtSdhLineDccLayerLine)
        theOtherDcc = AtSdhLineDccChannelGet(line, cAtSdhLineDccLayerSection);

    if (theOtherDcc && AtChannelTxIsEnabled((AtChannel)theOtherDcc))
        return cAtOk;

    return Tha60290021ModuleOcnFaceplateLineTxDccInsByOhBusEnable(OcnModule(self), LineIdGet(self), enable);
    }

static eAtRet TxHwOcnDccInsertionEnable(Tha60290021HdlcChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret = TxLineInsertionHandle((AtChannel)self, enable);
    return ret;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    eAtRet ret = cAtOk;

    if (mMethodsGet(mThis(self))->HasTohBusEnabling(mThis(self)))
        ret |= mMethodsGet(mThis(self))->TxHwOcnDccInsertionEnable(mThis(self), enable);

    ret |= TxDccEnable(self, enable);

    return ret;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    return TxDccIsEnabled(self);
    }

static eBool TxCanEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static uint32 EnableMask(AtChannel self)
    {
    return cBit0 << AtChannelIdGet(self);
    }

static uint32 EnableShift(AtChannel self)
    {
    return AtChannelIdGet(self);
    }

static uint32 RxDccEnableRegister(AtHdlcChannel self)
    {
    return BaseAddress(self) + cAf6Reg_upen_dcctx_enacid_Base;
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    uint32 regAddr = RxDccEnableRegister((AtHdlcChannel) self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mFieldIns(&regVal, EnableMask(self), EnableShift(self), mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    uint32 regAddr = RxDccEnableRegister((AtHdlcChannel) self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    return (regVal & EnableMask(self)) ? cAtTrue : cAtFalse;
    }

static eBool RxCanEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static eBool IsSimulation(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceIsSimulated(device);
    }

static uint32 IntStatusRegisterBase(uint32 defectMask)
    {
    switch (defectMask)
        {
        case cAtHdlcChannelAlarmTypeFcsError:           return cAf6Reg_crc_error_status_Base;
        case cAtHdlcChannelAlarmTypeUndersize:          return cAf6Reg_hdlc_undsize_len_status_Base;
        case cAtHdlcChannelAlarmTypeOversize:           return cAf6Reg_hdlc_ovrsize_len_status_Base;
        case cAtHdlcChannelAlarmTypeDecapBufferFull:    return cAf6Reg_ocn2eth_blk_ept_status_Base;
        case cAtHdlcChannelAlarmTypeEncapBufferFull:    return cAf6Reg_eth2ocn_blk_ept_status_Base;
        default: return cInvalidUint32;
        }
    }

static eBool InterruptStickyByRegisterBase(AtChannel self, uint32 regAddrBase, eBool clear)
    {
    uint32 regAddr = regAddrBase + BaseAddress((AtHdlcChannel) self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 mask = EnableMask(self);
    eBool hasSticky = (regVal & mask) ? cAtTrue : cAtFalse;
    if (hasSticky & clear)
        mChannelHwWrite(self, regAddr, IsSimulation(self) ? 0 : mask, cAtModuleSdh);

    return hasSticky;
    }

static uint32 AlarmHistoryRead2Clear(AtChannel self, eBool clear)
    {
    uint32 swAlarm = 0;

    if (InterruptStickyByRegisterBase(self, IntStatusRegisterBase(cAtHdlcChannelAlarmTypeFcsError), clear))
        swAlarm |= cAtHdlcChannelAlarmTypeFcsError;

    if (InterruptStickyByRegisterBase(self, IntStatusRegisterBase(cAtHdlcChannelAlarmTypeUndersize), clear))
        swAlarm |= cAtHdlcChannelAlarmTypeUndersize;

    if (InterruptStickyByRegisterBase(self, IntStatusRegisterBase(cAtHdlcChannelAlarmTypeOversize), clear))
        swAlarm |= cAtHdlcChannelAlarmTypeOversize;

    if (InterruptStickyByRegisterBase(self, IntStatusRegisterBase(cAtHdlcChannelAlarmTypeDecapBufferFull), clear))
        swAlarm |= cAtHdlcChannelAlarmTypeDecapBufferFull;

    if (InterruptStickyByRegisterBase(self, IntStatusRegisterBase(cAtHdlcChannelAlarmTypeEncapBufferFull), clear))
        swAlarm |= cAtHdlcChannelAlarmTypeEncapBufferFull;

    return swAlarm;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
	return AlarmHistoryRead2Clear(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
	return AlarmHistoryRead2Clear(self, cAtTrue);
    }

static eBool AlarmIsSupported(AtChannel self, uint32 alarmType)
	{
	AtUnused(self);
	switch (alarmType)
		{
		case cAtHdlcChannelAlarmTypeFcsError        :return cAtTrue;
		case cAtHdlcChannelAlarmTypeUndersize       :return cAtTrue;
		case cAtHdlcChannelAlarmTypeOversize        :return cAtTrue;
		case cAtHdlcChannelAlarmTypeDecapBufferFull :return cAtTrue;
		case cAtHdlcChannelAlarmTypeEncapBufferFull :return cAtTrue;
		default:return cAtFalse;
		}
	}

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return (cAtHdlcChannelAlarmTypeFcsError         |
            cAtHdlcChannelAlarmTypeUndersize        |
            cAtHdlcChannelAlarmTypeOversize         |
            cAtHdlcChannelAlarmTypeDecapBufferFull  |
            cAtHdlcChannelAlarmTypeEncapBufferFull);
    }

static eAtRet InterruptEnableSetByRegisterBase(AtChannel self, uint32 regAddrBase, eBool enable)
    {
    uint32 regAddr = regAddrBase + BaseAddress((AtHdlcChannel) self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mFieldIns(&regVal, EnableMask(self), EnableShift(self), mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static uint32 InterruptEnableRegisterBase(uint32 defectMask)
    {
    switch (defectMask)
        {
        case cAtHdlcChannelAlarmTypeFcsError:           return cAf6Reg_upen_txdcc_intenb_crc_Base;
        case cAtHdlcChannelAlarmTypeUndersize:          return cAf6Reg_upen_txdcc_intenb_undsize_Base;
        case cAtHdlcChannelAlarmTypeOversize:           return cAf6Reg_upen_txdcc_intenb_ovrsize_Base;
        case cAtHdlcChannelAlarmTypeDecapBufferFull:    return cAf6Reg_upen_txdcc_intenb_ocn2eth_blk_ept_Base;
        case cAtHdlcChannelAlarmTypeEncapBufferFull:    return cAf6Reg_upen_rxdcc_intenb_eth2ocn_blk_ept_Base;
        default: return cInvalidUint32;
        }
    }

static eAtRet InterruptEnableWithMask(AtChannel self, uint32 defectMask, uint32 enableMask, uint32 mask)
    {
    if (defectMask & mask)
        {
        eBool enable = (enableMask & mask) ? cAtTrue : cAtFalse;
        uint32 regAddrBase = InterruptEnableRegisterBase(mask);

        /* Only Access with valid Register Base */
        if (regAddrBase != cInvalidUint32)
        	return InterruptEnableSetByRegisterBase(self, InterruptEnableRegisterBase(mask), enable);
        }

    return cAtOk;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    eAtRet ret = cAtOk;
    uint32 notSupportedMask = ~AtChannelSupportedInterruptMasks(self);

    if (notSupportedMask & defectMask & enableMask)
        return cAtErrorModeNotSupport;

    ret |= InterruptEnableWithMask(self, defectMask, enableMask, cAtHdlcChannelAlarmTypeFcsError);
    ret |= InterruptEnableWithMask(self, defectMask, enableMask, cAtHdlcChannelAlarmTypeUndersize);
    ret |= InterruptEnableWithMask(self, defectMask, enableMask, cAtHdlcChannelAlarmTypeOversize);
    ret |= InterruptEnableWithMask(self, defectMask, enableMask, cAtHdlcChannelAlarmTypeDecapBufferFull);
    ret |= InterruptEnableWithMask(self, defectMask, enableMask, cAtHdlcChannelAlarmTypeEncapBufferFull);

    return ret;
    }

static eBool InterruptIsEnabledByRegisterBase(AtChannel self, uint32 regAddrBase)
    {
    uint32 regAddr = regAddrBase + BaseAddress((AtHdlcChannel) self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    return (regVal & EnableMask(self)) ? cAtTrue : cAtFalse;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    uint32 alarmMask = 0;
    uint32 mask;

    mask = cAtHdlcChannelAlarmTypeFcsError;
    if (InterruptIsEnabledByRegisterBase(self, InterruptEnableRegisterBase(mask)))
        alarmMask |= mask;

    mask = cAtHdlcChannelAlarmTypeUndersize;
    if (InterruptIsEnabledByRegisterBase(self, InterruptEnableRegisterBase(mask)))
        alarmMask |= mask;

    mask = cAtHdlcChannelAlarmTypeOversize;
    if (InterruptIsEnabledByRegisterBase(self, InterruptEnableRegisterBase(mask)))
        alarmMask |= mask;

    mask = cAtHdlcChannelAlarmTypeDecapBufferFull;
    if (InterruptIsEnabledByRegisterBase(self, InterruptEnableRegisterBase(mask)))
        alarmMask |= mask;

    mask = cAtHdlcChannelAlarmTypeEncapBufferFull;
    if (InterruptIsEnabledByRegisterBase(self, InterruptEnableRegisterBase(mask)))
        alarmMask |= mask;

    return alarmMask;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    return AtChannelInterruptMaskGet(self) ? cAtTrue : cAtFalse;
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    uint32 intrMask = AtChannelInterruptMaskGet(self);
    uint32 changedAlarms = AtChannelDefectInterruptClear(self);
    uint32 currentStatus = AtChannelDefectInterruptGet(self);

    AtUnused(offset);
    AtChannelAllAlarmListenersCall(self, (changedAlarms & intrMask), (currentStatus & intrMask));

    return changedAlarms;
    }

static eAtHdlcFrameType FrameTypeGet(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAtHdlcFrmLapd;
    }

static eAtRet FrameTypeSet(AtHdlcChannel self, uint8 frameType)
    {
    AtUnused(self);
    return (frameType == cAtHdlcFrmLapd) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    eAtRet ret;

    ret = m_AtChannelMethods->BindToPseudowire(self, pseudowire);
    if (ret != cAtOk)
        return ret;

    /* Activate enabling cached before binding */
    if (pseudowire)
        ret |= mMethodsGet(mThis(self))->TxDccHwEnable(mThis(self), mThis(self)->txEnabled);
    else
        ret |= mMethodsGet(mThis(self))->TxDccHwEnable(mThis(self), cAtFalse);

    return ret;
    }

static eAtRet FcsCalculationModeSet(AtHdlcChannel self, eAtHdlcFcsCalculationMode fcsCalculationMode)
    {
    uint32 regAddr = DecapContrlReg(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_upen_hdlc_locfg_cfg_fcsmsb_, (fcsCalculationMode == cAtHdlcFcsCalculationModeMsb) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    /* Need to change LSB/MSB of hdlc frame also */
    mMethodsGet(mThis(self))->EncapFrameBitOrderSet(mThis(self), (fcsCalculationMode == cAtHdlcFcsCalculationModeMsb) ? cAtBitOrderMsb : cAtBitOrderLsb);
    mMethodsGet(mThis(self))->DecapFrameBitOrderSet(mThis(self), (fcsCalculationMode == cAtHdlcFcsCalculationModeMsb) ? cAtBitOrderMsb : cAtBitOrderLsb);
    return cAtOk;
    }

static eAtHdlcFcsCalculationMode FcsCalculationModeGet(AtHdlcChannel self)
    {
    uint32 regAddr = DecapContrlReg(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    return (regVal & cAf6_upen_hdlc_locfg_cfg_fcsmsb_Mask) ? cAtHdlcFcsCalculationModeMsb : cAtHdlcFcsCalculationModeLsb;
    }

static uint32 PktGeneratorChannelEnableMask(AtChannel self)
    {
    return cBit0 << AtChannelIdGet(self);
    }

static uint32 PktGeneratorChannelEnableShift(AtChannel self)
    {
    return AtChannelIdGet(self);
    }

static uint32 UpenDccCfgTestgenEnacid(ThaModuleOcn self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(self) + cAf6Reg_upen_dcc_cfg_testgen_enacid_Base;
    }

static eAtRet PktGeneratorChannelEnable(ThaModuleOcn self, AtChannel hdlc, eBool enable)
    {
    uint32 regAddr = UpenDccCfgTestgenEnacid(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, PktGeneratorChannelEnableMask(hdlc), PktGeneratorChannelEnableShift(hdlc), mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool PktGeneratorChannelIsEnabled(ThaModuleOcn self, AtChannel hdlc)
    {
    uint32 regAddr = UpenDccCfgTestgenEnacid(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & PktGeneratorChannelEnableMask(hdlc)) ? cAtTrue : cAtFalse;
    }

static eAtRet PktGeneratorEnable(Tha60290021HdlcChannel self, eBool enable)
    {
    return PktGeneratorChannelEnable(OcnModule((AtChannel)self), (AtChannel)self, enable);
    }

static eBool  PktGeneratorIsEnabled(Tha60290021HdlcChannel self)
    {
    return PktGeneratorChannelIsEnabled(OcnModule((AtChannel)self), (AtChannel)self);
    }

static eBool HasInterruptV2(Tha60290021HdlcChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtObject(AtHdlcChannel self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtHdlcChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, TypeString);
        mMethodOverride(m_AtChannelOverride, IdString);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxCanEnable);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxCanEnable);
        mMethodOverride(m_AtChannelOverride, ShouldDeletePwOnCleanup);

        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, AlarmIsSupported);

        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtEncapChannel(AtHdlcChannel self)
    {
    AtEncapChannel encapChannel = (AtEncapChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtEncapChannelMethods = mMethodsGet(encapChannel) ;
        mMethodsGet(osal)->MemCpy(osal, &m_AtEncapChannelOverride, m_AtEncapChannelMethods, sizeof(m_AtEncapChannelOverride));

        mMethodOverride(m_AtEncapChannelOverride, ScrambleEnable);
        mMethodOverride(m_AtEncapChannelOverride, ScrambleIsEnabled);
        }

    mMethodsSet(encapChannel, &m_AtEncapChannelOverride);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, mMethodsGet(self), sizeof(m_AtHdlcChannelOverride));

        mMethodOverride(m_AtHdlcChannelOverride, FlagSet);
        mMethodOverride(m_AtHdlcChannelOverride, FlagGet);
        mMethodOverride(m_AtHdlcChannelOverride, FlagIsSupported);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeIsSupported);
        mMethodOverride(m_AtHdlcChannelOverride, FcsCalculationModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsCalculationModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, MtuSet);
        mMethodOverride(m_AtHdlcChannelOverride, MtuGet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeGet);
        mMethodOverride(m_AtHdlcChannelOverride, StuffModeIsSupported);
        mMethodOverride(m_AtHdlcChannelOverride, NumFlagsSet);
        mMethodOverride(m_AtHdlcChannelOverride, NumFlagsGet);
        mMethodOverride(m_AtHdlcChannelOverride, FrameTypeSet);
        mMethodOverride(m_AtHdlcChannelOverride, FrameTypeGet);
        mMethodOverride(m_AtHdlcChannelOverride, IdlePatternSet);
        mMethodOverride(m_AtHdlcChannelOverride, IdlePatternGet);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void MethodsInit(AtHdlcChannel self)
    {
    Tha60290021HdlcChannel dcc = (Tha60290021HdlcChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ChannelOffSet);
        mMethodOverride(m_methods, CounterBaseAddress);
        mMethodOverride(m_methods, HasTohBusEnabling);
        mMethodOverride(m_methods, TxHwOcnDccInsertionEnable);
        mMethodOverride(m_methods, TxDccHwEnable);
        mMethodOverride(m_methods, CounterBaseAddressByHwType);
        mMethodOverride(m_methods, PktGeneratorEnable);
        mMethodOverride(m_methods, PktGeneratorIsEnabled);
        mMethodOverride(m_methods, EncapFrameBitOrderSet);
        mMethodOverride(m_methods, DecapFrameBitOrderSet);

        /* Interrupt */
        mMethodOverride(m_methods, HasInterruptV2);

        /* Debug */
        mMethodOverride(m_methods, HasPacketCounterDuringFifoFull);
        }

    mMethodsSet(dcc, &m_methods);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtHdlcChannel(self);
    OverrideAtEncapChannel(self);
    OverrideAtChannel(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021HdlcChannel);
    }

AtHdlcChannel Tha60290021HdlcChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtHdlcChannelObjectInit((AtHdlcChannel) self, channelId, cAtHdlcFrmUnknown, (AtModule) module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    /* Private data */
    AtEncapChannelPhysicalChannelSet((AtEncapChannel)self, (AtChannel)line);
    mThis(self)->layers = layers;

    return self;
    }

AtHdlcChannel Tha60290021HdlcChannelNew(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021HdlcChannelObjectInit(newChannel, channelId, line, layers, module);
    }

AtSdhLine Tha60290021HdlcDccChannelLineGet(AtHdlcChannel self)
    {
    if (self)
        return (AtSdhLine) AtEncapChannelBoundPhyGet((AtEncapChannel) self);

    return NULL;
    }

eAtSdhLineDccLayer Tha60290021HdlcDccChannelLayerGet(AtHdlcChannel self)
    {
    if (self)
        return mThis(self)->layers;

    return cAtSdhLineDccLayerUnknown;
    }

uint32 Tha60290021HdlcChannelUpenDccdecAddress(Tha60290021HdlcChannel self)
    {
    if (self)
        return UpenDccdecAddress(self);
    return cInvalidUint32;
    }

eAtRet Tha60290021HdlcChannelTxDccHwEnable(Tha60290021HdlcChannel self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->TxDccHwEnable(self, enable);
    return cAtErrorObjectNotExist;
    }

uint32 Tha60290011HdlcChannelFcsSw2Hw(uint32 fcsMode)
    {
    return FcsSw2Hw(fcsMode);
    }

uint32 Tha60290021HdlcChannelHwCounterRead2Clear(AtChannel self, uint32 counterBaseAddress, eBool r2c)
    {
    return HwCounterRead2Clear(self, counterBaseAddress, r2c);
    }

uint32 Tha60290021HdlcChannelCounterBaseAddressByHwType(Tha60290021HdlcChannel self, uint32 hwType)
    {
    if (self)
        return mMethodsGet(self)->CounterBaseAddressByHwType(self, hwType);
    return 0;
    }

eAtRet Tha60290021HdlcPktGeneratorEnable(Tha60290021HdlcChannel self, eBool enable)
    {
    if (self)
        return mMethodsGet(self)->PktGeneratorEnable(self, enable);
    return cAtErrorNullPointer;
    }

eBool  Tha60290021HdlcPktGeneratorIsEnabled(Tha60290021HdlcChannel self)
    {
    if (self)
        return mMethodsGet(self)->PktGeneratorIsEnabled(self);
    return cAtFalse;
    }

eAtRet Tha60290021HdlcChannelFcsModePrivateSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    return FcsModeSet(self, fcsMode);
    }

eAtRet Tha60290021HdlcV3EncapFrameBitOrderSet(Tha60290021HdlcChannel self, eAtBitOrder order)
    {
    return EncapFrameBitOrderSet(self, order);
    }

eAtRet Tha60290021HdlcV3DecapFrameBitOrderSet(Tha60290021HdlcChannel self, eAtBitOrder order)
    {
    return DecapFrameBitOrderSet(self, order);
    }

eAtRet Tha60290021HdlcChannelTxRsMsDccInsHandle(Tha60290021HdlcChannel self, eBool enable)
    {
    eAtSdhLineDccLayer layer = Tha60290021HdlcDccChannelLayerGet((AtHdlcChannel) self);
    eAtRet ret = cAtOk;
    AtChannel hdlcChannel = (AtChannel)self;

    if (layer == cAtSdhLineDccLayerSection)
        return ThaModuleOcnLineTxRsDccInsByOhBusEnable(OcnModule(hdlcChannel), LineIdGet(hdlcChannel), enable);

    if (layer == cAtSdhLineDccLayerLine)
        return ThaModuleOcnLineTxMsDccInsByOhBusEnable(OcnModule(hdlcChannel), LineIdGet(hdlcChannel), enable);

    if (layer == cAtSdhLineDccLayerAll)
        {
        ret = ThaModuleOcnLineTxRsDccInsByOhBusEnable(OcnModule(hdlcChannel), LineIdGet(hdlcChannel), enable);
        ret |= ThaModuleOcnLineTxMsDccInsByOhBusEnable(OcnModule(hdlcChannel), LineIdGet(hdlcChannel), enable);
        return ret;
        }

    return cAtErrorModeNotSupport;
    }

eBool Tha60290021HdlcChannelHasPacketCounterDuringFifoFull(Tha60290021HdlcChannel self)
    {
    if (self)
        return mMethodsGet(self)->HasPacketCounterDuringFifoFull(self);
    return cAtFalse;
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60290021HdlcChannelV2.c
 *
 * Created Date: Dec 12, 2017
 *
 * Description : DCC HDLC Version2 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../pw/Tha60290021ModulePw.h"
#include "../pw/Tha60290021DccKbyteV2Reg.h"
#include "Tha60290021HdlcChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods              m_AtChannelOverride;
static tAtHdlcChannelMethods          m_AtHdlcChannelOverride;
static tTha60290021HdlcChannelMethods m_Tha60290021HdlcChannelOverride;

/* Save super implementation */
static const tAtChannelMethods     *m_AtChannelMethods     = NULL;
static const tAtHdlcChannelMethods *m_AtHdlcChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn OcnModule(AtHdlcChannel self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);
    }

static uint32 BaseAddress(AtHdlcChannel self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self));
    }

static uint32 GlobalRegisterWithLocalAddress(AtHdlcChannel self, uint32 localAddress)
    {
    return BaseAddress(self) + localAddress;
    }

static uint32 FcsModeMask(AtChannel self)
    {
    return cBit0 << AtChannelIdGet(self);
    }

static uint32 FcsModeShift(AtChannel self)
    {
    return AtChannelIdGet(self);
    }

static eAtRet DecapFcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    uint32 regAddr, regVal;
    uint32 hwFcsMode_Mask = FcsModeMask((AtChannel)self);
    uint32 hwFcsMode_Shift = FcsModeShift((AtChannel)self);

    regAddr = GlobalRegisterWithLocalAddress(self, cAf6Reg_upen_dcctx_fcsrem);
    regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);

    mRegFieldSet(regVal, hwFcsMode_, Tha60290011HdlcChannelFcsSw2Hw(fcsMode));
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet FcsModeSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    eAtRet ret = cAtOk;

    ret |= m_AtHdlcChannelMethods->FcsModeSet(self, fcsMode);
    if (ret == cAtOk)
        ret |= DecapFcsModeSet(self, fcsMode);

    return ret;
    }

static uint32 CounterV2BaseAddressByHwType(Tha60290021HdlcChannel self, uint32 hwType)
    {
    return Tha60290021HdlcChannelCounterBaseAddressByHwType(self, hwType);
    }

static uint32 CounterBaseAddress(Tha60290021HdlcChannel self, uint16 counterType, eBool r2c)
    {
    AtUnused(r2c);

    switch (counterType)
        {
        case cAtHdlcChannelCounterTypeTxGoodPackets:      return CounterV2BaseAddressByHwType(self, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_BUF2ENC_good_pkt);
        case cAtHdlcChannelCounterTypeTxGoodBytes:        return CounterV2BaseAddressByHwType(self, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_BUF2ENC_byte);

        case cAtHdlcChannelCounterTypeRxGoodPackets:      return CounterV2BaseAddressByHwType(self, cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_good_pkt);
        case cAtHdlcChannelCounterTypeRxGoodBytes:        return CounterV2BaseAddressByHwType(self, cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_byte);
        case cAtHdlcChannelCounterTypeRxFcsErrors:        return CounterV2BaseAddressByHwType(self, cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_error_pkt);

        default:                                          return cInvalidUint32;
        }
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    if (CounterBaseAddress((Tha60290021HdlcChannel)self, counterType, cAtTrue) == cInvalidUint32)
        return cAtFalse;
    return cAtTrue;
    }

static void DebugEth2OcnFifo(AtChannel self)
    {
    uint32 regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, cAf6Reg_af6ces10grtldcc_rx__upen_dispkt_pcid_Base, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "ETH2OCN Counter Drop Packet Due Too Buffer Full Per Channel", regVal);
    }

static eAtRet Debug(AtChannel self)
    {
    uint32 regAddr, regVal;

    AtPrintc(cSevDebug, "Debug information of the channel: %s\r\n", AtObjectToString((AtObject)self));
    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_byte);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter RX bytes from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction        ", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_good_pkt);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter RX good packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction ", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_error_pkt);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter RX error packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_TDM2PSN_DEC2BUF_lost_pkt);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter RX lost packets from HDLC DECAP to DCC TX BUFFER (DEC2BUF), TDM2PSN direction ", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_TDM2PSN_BUF2SGM_byte);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter TX bytes from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction        ", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_TDM2PSN_BUF2SGM_pkt);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter TX packets from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction      ", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_TDM2PSN_BUF2SGM_error_pkt);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter TX error packet from DCC TX BUFFER to SGMII port (BUF2SGM), TDM2PSN direction ", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGM2BUF_byte);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter RX bytes from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction        ", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGM2BUF_good_pkt);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter RX good packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction ", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGM2BUF_error_pkt);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter RX error packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_SGMI2BUF_lost_pkt);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter RX lost packets from SGMII port to DCC RX BUFFER (SGM2BUF), PSN2TDM direction ", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_BUF2ENC_byte);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter TX bytes from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction        ", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_BUF2ENC_good_pkt);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter TX good packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction ", regVal);

    regAddr = CounterV2BaseAddressByHwType((Tha60290021HdlcChannel)self, cAf6Reg_upen_dcc_cnt_type_PSN2TDM_BUF2ENC_error_pkt);
    regVal = Tha60290021HdlcChannelHwCounterRead2Clear(self, regAddr, cAtTrue);
    AtPrintc(cSevDebug, "%s: %d\r\n", "counter TX error packets from DCC RX BUFFER to HDLC ENCAP (BUF2ENC), PSN2TDM direction", regVal);

    if (Tha60290021HdlcChannelHasPacketCounterDuringFifoFull((Tha60290021HdlcChannel)self))
        DebugEth2OcnFifo(self);

    return cAtOk;
    }

static eBool FcsModeIsSupported(AtHdlcChannel self, eAtHdlcFcsMode fcsMode)
    {
    AtUnused(self);

    switch (fcsMode)
        {
        case cAtHdlcFcsModeFcs16  : return cAtTrue;
        case cAtHdlcFcsModeFcs32  : return cAtTrue;

        case cAtHdlcFcsModeNoFcs  : return cAtFalse;
        case cAtHdlcFcsModeUnknown: return cAtFalse;
        default:                    return cAtFalse;
        }
    }

static eBool ShouldUseInterruptV2(AtChannel self)
    {
    return mMethodsGet((Tha60290021HdlcChannel)self)->HasInterruptV2((Tha60290021HdlcChannel)self);
    }

static uint32 DefaultOffset(AtChannel self)
    {
    return BaseAddress((AtHdlcChannel)self) + mMethodsGet((Tha60290021HdlcChannel)self)->ChannelOffSet((Tha60290021HdlcChannel)self);
    }

static uint32 CurrentStatusRegister(AtChannel self)
    {
    return cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_cur_sta_Base + DefaultOffset(self);
    }

static uint32 DefectHw2Sw(AtChannel self, uint32 regVal)
    {
    uint32 swAlarm = 0;
    AtUnused(self);

    if (regVal & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_ocn2eth_dcc_erro_und_Mask)
        swAlarm |= cAtHdlcChannelAlarmTypeUndersize;
    if (regVal & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_ocn2eth_dcc_erro_ovr_Mask)
        swAlarm |= cAtHdlcChannelAlarmTypeOversize;
    if (regVal & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_ocn2eth_dcc_erro_fcs_Mask)
        swAlarm |= cAtHdlcChannelAlarmTypeFcsError;
    if (regVal & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_ocn2eth_dcc_buff_ful_Mask)
        swAlarm |= cAtHdlcChannelAlarmTypeDecapBufferFull;
    if (regVal & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_eth2ocn_dcc_buff_ful_Mask)
        swAlarm |= cAtHdlcChannelAlarmTypeEncapBufferFull;

    return swAlarm;
    }

static uint32 HwDefectGet(AtChannel self)
    {
    uint32 regAddr = CurrentStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return DefectHw2Sw(self, regVal);
    }

static uint32 DefectGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return HwDefectGet(self);

    return m_AtChannelMethods->DefectGet(self);
    }

static uint32 InterruptStatusRegister(AtChannel self)
    {
    return cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_Base + DefaultOffset(self);
    }

static uint32 DefectHistoryRead2Clear(AtChannel self, eBool clear)
    {
    static const uint32 cDccPwHwDefect = cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int__pcid_int_sta_sta_int_eth2ocn_dcc_chan_dis_Mask;
    uint32 regAddr = InterruptStatusRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 swAlarm = DefectHw2Sw(self, regVal);
    if (clear)
        mChannelHwWrite(self, regAddr, (regVal & ~cDccPwHwDefect), cThaModuleOcn);
    return swAlarm;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return DefectHistoryRead2Clear(self, cAtFalse);

    return m_AtChannelMethods->DefectHistoryGet(self);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return DefectHistoryRead2Clear(self, cAtTrue);

    return m_AtChannelMethods->DefectHistoryClear(self);
    }

static uint32 InterruptMaskRegister(AtChannel self)
    {
    return cAf6Reg_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_Base + DefaultOffset(self);
    }

static eAtRet HwInterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    uint32 regAddr = InterruptMaskRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (defectMask & cAtHdlcChannelAlarmTypeUndersize)
        mRegFieldSet(regVal, cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_und_,
                     (enableMask & cAtHdlcChannelAlarmTypeUndersize) ? 1 : 0);

    if (defectMask & cAtHdlcChannelAlarmTypeOversize)
        mRegFieldSet(regVal, cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_ovr_,
                     (enableMask & cAtHdlcChannelAlarmTypeOversize) ? 1 : 0);

    if (defectMask & cAtHdlcChannelAlarmTypeFcsError)
        mRegFieldSet(regVal, cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_fcs_,
                     (enableMask & cAtHdlcChannelAlarmTypeFcsError) ? 1 : 0);

    if (defectMask & cAtHdlcChannelAlarmTypeDecapBufferFull)
        mRegFieldSet(regVal, cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_buff_ful_,
                     (enableMask & cAtHdlcChannelAlarmTypeDecapBufferFull) ? 1 : 0);

    if (defectMask & cAtHdlcChannelAlarmTypeEncapBufferFull)
        mRegFieldSet(regVal, cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_eth2ocn_dcc_buff_ful_,
                     (enableMask & cAtHdlcChannelAlarmTypeEncapBufferFull) ? 1 : 0);

    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    if (ShouldUseInterruptV2(self))
        return HwInterruptMaskSet(self, defectMask, enableMask);

    return m_AtChannelMethods->InterruptMaskSet(self, defectMask, enableMask);
    }

static uint32 HwInterruptMaskGet(AtChannel self)
    {
    uint32 regAddr = InterruptMaskRegister(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return DefectHw2Sw(self, regVal);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    if (ShouldUseInterruptV2(self))
        return HwInterruptMaskGet(self);

    return m_AtChannelMethods->InterruptMaskGet(self);
    }

static uint32 InterruptProcess(AtChannel self, uint32 offset)
    {
    uint32 intrAddress = InterruptStatusRegister(self);
    uint32 intrStatus = mChannelHwRead(self, intrAddress, cThaModuleOcn);
    uint32 intrMask = mChannelHwRead(self, InterruptMaskRegister(self), cThaModuleOcn);
    uint32 currentStatus = mChannelHwRead(self, CurrentStatusRegister(self), cThaModuleOcn);
    uint32 changedDefects = 0;
    uint32 defects = 0;
    uint32 intr2Clear = 0;
    AtUnused(offset);

    intrStatus &= intrMask;

    if (intrStatus & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_und_Mask)
        {
        changedDefects |= cAtHdlcChannelAlarmTypeUndersize;
        if (currentStatus & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_und_Mask)
            defects |= cAtHdlcChannelAlarmTypeUndersize;

        intr2Clear |= cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_und_Mask;
        }

    if (intrStatus & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_ovr_Mask)
        {
        changedDefects |= cAtHdlcChannelAlarmTypeOversize;
        if (currentStatus & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_ovr_Mask)
            defects |= cAtHdlcChannelAlarmTypeOversize;

        intr2Clear |= cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_ovr_Mask;
        }

    if (intrStatus & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_fcs_Mask)
        {
        changedDefects |= cAtHdlcChannelAlarmTypeFcsError;
        if (currentStatus & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_fcs_Mask)
            defects |= cAtHdlcChannelAlarmTypeFcsError;

        intr2Clear |= cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_erro_fcs_Mask;
        }

    if (intrStatus & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_buff_ful_Mask)
        {
        changedDefects |= cAtHdlcChannelAlarmTypeDecapBufferFull;
        if (currentStatus & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_buff_ful_Mask)
            defects |= cAtHdlcChannelAlarmTypeDecapBufferFull;

        intr2Clear |= cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_ocn2eth_dcc_buff_ful_Mask;
        }

    if (intrStatus & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_eth2ocn_dcc_buff_ful_Mask)
        {
        changedDefects |= cAtHdlcChannelAlarmTypeEncapBufferFull;
        if (currentStatus & cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_eth2ocn_dcc_buff_ful_Mask)
            defects |= cAtHdlcChannelAlarmTypeEncapBufferFull;

        intr2Clear |= cAf6_af6ces10grtldcck_corepi__rtl_hdlc_int_pcid_int_en_enb_int_eth2ocn_dcc_buff_ful_Mask;
        }

    if (intr2Clear)
        mChannelHwWrite(self, intrAddress, intr2Clear, cThaModuleOcn);

    AtChannelAllAlarmListenersCall(self, changedDefects, defects);
    return 0;
    }

static eBool HasInterruptV2(Tha60290021HdlcChannel self)
    {
    Tha60290021ModulePw pwModule = (Tha60290021ModulePw)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cAtModulePw);
    return Tha60290021ModulePwDccKbyteInterruptIsSupported(pwModule);
    }

static void OverrideAtChannel(AtHdlcChannel self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, InterruptProcess);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, m_AtHdlcChannelMethods, sizeof(m_AtHdlcChannelOverride));

        mMethodOverride(m_AtHdlcChannelOverride, FcsModeSet);
        mMethodOverride(m_AtHdlcChannelOverride, FcsModeIsSupported);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void OverrideTha60290021HdlcChannel(AtHdlcChannel self)
    {
    Tha60290021HdlcChannel dcc = (Tha60290021HdlcChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021HdlcChannelOverride, mMethodsGet(dcc), sizeof(tTha60290021HdlcChannelMethods));

        mMethodOverride(m_Tha60290021HdlcChannelOverride, CounterBaseAddress);
        mMethodOverride(m_Tha60290021HdlcChannelOverride, HasInterruptV2);
        }

    mMethodsSet(dcc, &m_Tha60290021HdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideTha60290021HdlcChannel(self);
    OverrideAtHdlcChannel(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021HdlcChannelV2);
    }

AtHdlcChannel Tha60290021HdlcChannelV2ObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021HdlcChannelObjectInit(self, channelId, line, layers, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel Tha60290021HdlcChannelV2New(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021HdlcChannelV2ObjectInit(newChannel, channelId, line, layers, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60290021HdlcChannelV3.c
 *
 * Created Date: Mar 2, 2018
 *
 * Description : Implement HDLC channel version 3 which support Tx HDLC MSB
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../pw/Tha60290021DccKbyteReg.h"
#include "Tha60290021HdlcChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_upen_hdlc_enc_ctrl_reg3 0x30006
#define cAf6Reg_upen_hdlc_enc_ctrl_reg4 0x30007

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHdlcChannelMethods          m_AtHdlcChannelOverride;

/* Save super implementation */
static const tAtHdlcChannelMethods *m_AtHdlcChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn OcnModule(AtHdlcChannel self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);
    }

static uint32 BaseAddress(AtHdlcChannel self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self));
    }

static uint32 GlobalRegisterWithLocalAddress(AtHdlcChannel self, uint32 localAddress)
    {
    return BaseAddress(self) + localAddress;
    }

static uint32 ChannelBitMask(AtChannel self)
    {
    return cBit0 << AtChannelIdGet(self);
    }

static uint32 ChannelBitShift(AtChannel self)
    {
    return AtChannelIdGet(self);
    }

static uint32 TxHdlcFcsCalculation_InputDataBitOrderRegAddress(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAf6Reg_upen_hdlc_enc_ctrl_reg3;
    }

static uint32 TxHdlcFcsTransmit_BitOrderRegAddress(AtHdlcChannel self)
    {
    AtUnused(self);
    return cAf6Reg_upen_hdlc_enc_ctrl_reg4;
    }

static eAtRet TxHdlcFcsCalculationModeSet(AtHdlcChannel self, eAtHdlcFcsCalculationMode fcsCalculationMode)
    {
    uint32 regAddr = GlobalRegisterWithLocalAddress(self, TxHdlcFcsCalculation_InputDataBitOrderRegAddress(self));
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 channelBit_Mask = ChannelBitMask((AtChannel)self);
    uint32 channelBit_Shift = ChannelBitShift((AtChannel)self);

    mRegFieldSet(regVal, channelBit_, (fcsCalculationMode == cAtHdlcFcsCalculationModeMsb) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    regAddr = GlobalRegisterWithLocalAddress(self, TxHdlcFcsTransmit_BitOrderRegAddress(self));
    regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    mRegFieldSet(regVal, channelBit_, (fcsCalculationMode == cAtHdlcFcsCalculationModeMsb) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);

    return cAtOk;
    }

static eAtRet FcsCalculationModeSet(AtHdlcChannel self, eAtHdlcFcsCalculationMode fcsCalculationMode)
    {
    eAtRet ret = m_AtHdlcChannelMethods->FcsCalculationModeSet(self, fcsCalculationMode);

    if (ret != cAtOk)
        return ret;

    return TxHdlcFcsCalculationModeSet(self, fcsCalculationMode);
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, m_AtHdlcChannelMethods, sizeof(m_AtHdlcChannelOverride));

        mMethodOverride(m_AtHdlcChannelOverride, FcsCalculationModeSet);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtHdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021HdlcChannelV3);
    }

AtHdlcChannel Tha60290021HdlcChannelV3ObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021HdlcChannelV2ObjectInit(self, channelId, line, layers, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel Tha60290021HdlcChannelV3New(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021HdlcChannelV3ObjectInit(newChannel, channelId, line, layers, module);
    }

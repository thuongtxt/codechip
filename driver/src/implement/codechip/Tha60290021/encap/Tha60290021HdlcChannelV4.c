/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ENCAP
 *
 * File        : Tha60290021HdlcChannelV4.c
 *
 * Created Date: Apr 3, 2018
 *
 * Description : Implement hdlc frame transmission per hdlc channel
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../pw/Tha60290021DccKbyteReg.h"
#include "Tha60290021HdlcChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_upen_hdlc_dec_data_lsb_first      0x4407
#define cAf6Reg_upen_hdlc_enc_data_lsb_first      0x30008


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021HdlcChannelMethods m_Tha60290021HdlcChannelOverride;

/* Save super implementation */
static const tTha60290021HdlcChannelMethods *m_Tha60290021HdlcChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn OcnModule(AtHdlcChannel self)
    {
    return (ThaModuleOcn) AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)self), cThaModuleOcn);
    }

static uint32 BaseAddress(AtHdlcChannel self)
    {
    return ThaModuleOcnSohOverEthBaseAddress(OcnModule(self));
    }

static uint32 ChannelBitMask(AtChannel self)
    {
    return cBit0 << AtChannelIdGet(self);
    }

static uint32 ChannelBitShift(AtChannel self)
    {
    return AtChannelIdGet(self);
    }

static uint32 EncapBitOrderTransmissionRegAddress(Tha60290021HdlcChannel self)
    {
    uint32 address = BaseAddress((AtHdlcChannel)self) + cAf6Reg_upen_hdlc_enc_data_lsb_first;
    return address;
    }

static uint32 DecapBitOrderTransmissionRegAddress(Tha60290021HdlcChannel self)
    {
    uint32 address = BaseAddress((AtHdlcChannel)self) + cAf6Reg_upen_hdlc_dec_data_lsb_first;
    return address;
    }

static eAtRet EncapFrameBitOrderSet(Tha60290021HdlcChannel self, eAtBitOrder order)
    {
    uint32 regAddr = EncapBitOrderTransmissionRegAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 channelBit_Mask = ChannelBitMask((AtChannel)self);
    uint32 channelBit_Shift = ChannelBitShift((AtChannel)self);

    mRegFieldSet(regVal, channelBit_, (order == cAtBitOrderLsb) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eAtRet DecapFrameBitOrderSet(Tha60290021HdlcChannel self, eAtBitOrder order)
    {
    uint32 regAddr = DecapBitOrderTransmissionRegAddress(self);
    uint32 regVal =  mChannelHwRead(self, regAddr, cAtModuleSdh);
    uint32 channelBit_Mask = ChannelBitMask((AtChannel)self);
    uint32 channelBit_Shift = ChannelBitShift((AtChannel)self);

    mRegFieldSet(regVal, channelBit_, (order == cAtBitOrderLsb) ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cAtModuleSdh);
    return cAtOk;
    }

static void OverrideTha60290021HdlcChannel(AtHdlcChannel self)
    {
    Tha60290021HdlcChannel dcc = (Tha60290021HdlcChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021HdlcChannelMethods = mMethodsGet(dcc);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021HdlcChannelOverride, m_Tha60290021HdlcChannelMethods, sizeof(tTha60290021HdlcChannelMethods));

        mMethodOverride(m_Tha60290021HdlcChannelOverride, EncapFrameBitOrderSet);
        mMethodOverride(m_Tha60290021HdlcChannelOverride, DecapFrameBitOrderSet);
        }

    mMethodsSet(dcc, &m_Tha60290021HdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideTha60290021HdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021HdlcChannelV4);
    }

AtHdlcChannel Tha60290021HdlcChannelV4ObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021HdlcChannelV3ObjectInit(self, channelId, line, layers, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel Tha60290021HdlcChannelV4New(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021HdlcChannelV4ObjectInit(newChannel, channelId, line, layers, module);
    }

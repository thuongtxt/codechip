/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha6029HdlcChannelGenerator.h
 * 
 * Created Date: Apr 6, 2017
 *
 * Description : DCC HDLC Packet Generator
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029HDLCCHANNELGENERATOR_H_
#define _THA6029HDLCCHANNELGENERATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha60290021HdlcChannelInternal.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/
#define cAtHdlcChannelCounterTypeRxPacketsDataError (cAtHdlcChannelCounterTypeEnd + 1)
#define cAtHdlcChannelCounterTypeRxPacketsSeqError  (cAtHdlcChannelCounterTypeEnd + 2)
#define cAtHdlcChannelCounterTypeRxChannelIdError    (cAtHdlcChannelCounterTypeEnd + 3)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eAtGeneratorMode
    {
    cAtGeneratorModeInvalid,   /* Invalid mode */
    cAtGeneratorModeOneshot,   /* Oneshot mode */
    cAtGeneratorModeContinuous /* Continuous mode */
    }eAtGeneratorMode;

typedef enum eAtLengthMode
    {
    cAtLengthModeUnknown,       /* Unknown */
    cAtLengthModeFix,           /* Fixed with length was configured       */
    cAtLengthModeIncrease,      /* Increase from [minlength ..maxlength]  */
    cAtLengthModeRandom         /* Random inrange [minlength ..maxlength] */
    }eAtLengthMode;

typedef enum eAtPktErrorType
    {
    cAtPktErrorTypeNone,
    cAtPktErrorTypeLength     = cBit0,
    cAtPktErrorTypeFcs        = cBit1,
    cAtPktErrorTypePayload    = cBit2
    }eAtPktErrorType;

typedef enum eAtPktLoopbackType
    {
    cAtPktLoopbackTypeNone,
    cAtPktLoopbackTypeDccSgmiiLoop     = cBit0,
    cAtPktLoopbackTypeDccHdlcLoop      = cBit1,
    cAtPktLoopbackTypeKbyteSgmiiLoop   = cBit2,
    cAtPktLoopbackTypeKbyteSdhLoop     = cBit3,
    cAtPktLoopbackTypeDccBufferLoop    = cBit5
    }eAtPktLoopbackType;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Global for all channels */
eAtRet Tha6029DccHdlcPktGenInit(AtDevice self);
eAtRet Tha6029DccHdlcPktGenEnable(AtDevice self, eBool enable);
eBool Tha6029DccHdlcPktGenIsEnabled(AtDevice self);
eAtRet Tha6029DccHdlcPktMonEnable(AtDevice self, eBool enable);
eBool Tha6029DccHdlcPktMonIsEnabled(AtDevice self);
eAtRet Tha6029DccHdlcPktGenTimerIntervalSet(AtDevice self, uint32 interval);
uint32 Tha6029DccHdlcPktGenTimerIntervalGet(AtDevice self);
eAtRet Tha6029DccHdlcPktMonTimerIntervalSet(AtDevice self, uint32 interval);
uint32 Tha6029DccHdlcPktMonTimerIntervalGet(AtDevice self);

eAtRet Tha6029DccHdlcPktGenModeSet(AtDevice self, eAtGeneratorMode mode);
eAtGeneratorMode Tha6029DccHdlcPktGenModeGet(AtDevice self);

eAtRet Tha6029DccHdlcPktGenErrorForce(AtDevice self, uint32 errorMask);
eAtRet Tha6029DccHdlcPktGenErrorUnForce(AtDevice self, uint32 errorMask);
uint32 Tha6029DccHdlcPktGenForcedErrorGet(AtDevice self);

eAtRet Tha6029DccHdlcPktGenMaxLengthSet(AtDevice self, uint32 length);
uint32 Tha6029DccHdlcPktGenMaxLengthGet(AtDevice self);
eAtRet Tha6029DccHdlcPktGenMinLengthSet(AtDevice self, uint32 length);
uint32 Tha6029DccHdlcPktGenMinLengthGet(AtDevice self);

/* Per Channels */
eAtRet Tha6029DccHdlcPktGenChannelEnable(AtHdlcChannel self, eBool enable);
eBool Tha6029DccHdlcPktGenChannelIsEnabled(AtHdlcChannel self);
eAtRet Tha6029DccHdlcPktGenChannelHeaderVlanIdSet(AtHdlcChannel self, uint16 vlanId);
uint16 Tha6029DccHdlcPktGenChannelHeaderVlanIdGet(AtHdlcChannel self);
eAtRet Tha6029DccHdlcPktGenChannelHeaderDestMacLsbSet(AtHdlcChannel self, uint8 value);
uint8 Tha6029DccHdlcPktGenChannelHeaderDestMacLsbGet(AtHdlcChannel self);
eAtRet Tha6029DccHdlcPktGenChannelLengthModeSet(AtHdlcChannel self, eAtLengthMode mode);
eAtLengthMode Tha6029DccHdlcPktGenChannelLengthModeGet(AtHdlcChannel self);
eAtRet Tha6029DccHdlcPktGenChannelPayloadPatternSet(AtHdlcChannel self, eAtPrbsMode pattern);
eAtPrbsMode Tha6029DccHdlcPktGenChannelPayloadPatternGet(AtHdlcChannel self);
eAtRet Tha6029DccHdlcPktGenChannelFixPatternSet(AtHdlcChannel self, uint32 value);
uint32 Tha6029DccHdlcPktGenChannelFixPatternGet(AtHdlcChannel self);
eAtRet Tha6029DccHdlcPktGenChannelNumPktSet(AtHdlcChannel self, uint32 numPkts);
uint32 Tha6029DccHdlcPktGenChannelNumPktGet(AtHdlcChannel self);
uint32 Tha6029DccHdlcPktGenChannelCounterGet(AtHdlcChannel self, uint32 counterType, eBool r2c);
eAtRet Tha6029DccHdlcSoELoopbackTypeSet(AtDevice self, uint32 loopBackType, eBool enable);
uint32 Tha6029DccHdlcSoELoopbackTypeGet(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029HDLCCHANNELGENERATOR_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : Tha60290021HdlcChanel.h
 * 
 * Created Date: Sep 3, 2016
 *
 * Description : HDLC channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021HDLCCHANEL_H_
#define _THA60290021HDLCCHANEL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhLine.h"
#include "AtHdlcChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021HdlcChannel * Tha60290021HdlcChannel;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtHdlcChannel Tha60290021HdlcChannelNew(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);
AtHdlcChannel Tha60290021HdlcChannelV2New(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);
AtHdlcChannel Tha60290021HdlcChannelV3New(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);
AtHdlcChannel Tha60290021HdlcChannelV4New(uint32 channelId,  AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);

/* Utility */
AtSdhLine Tha60290021HdlcDccChannelLineGet(AtHdlcChannel self);
eAtSdhLineDccLayer Tha60290021HdlcDccChannelLayerGet(AtHdlcChannel self);
uint32 Tha60290021HdlcChannelUpenDccdecAddress(Tha60290021HdlcChannel self);
eAtRet Tha60290021HdlcChannelTxDccHwEnable(Tha60290021HdlcChannel self, eBool enable);
uint32 Tha60290011HdlcChannelFcsSw2Hw(uint32 fcsMode);
uint32 Tha60290021HdlcChannelHwCounterRead2Clear(AtChannel self, uint32 counterBaseAddress, eBool r2c);
uint32 Tha60290021HdlcChannelCounterBaseAddressByHwType(Tha60290021HdlcChannel self, uint32 hwType);
eAtRet Tha60290021HdlcPktGeneratorEnable(Tha60290021HdlcChannel self, eBool enable);
eBool  Tha60290021HdlcPktGeneratorIsEnabled(Tha60290021HdlcChannel self);
eAtRet Tha60290021HdlcChannelFcsModePrivateSet(AtHdlcChannel self, eAtHdlcFcsMode fcsMode);
eBool Tha60290021HdlcChannelHasPacketCounterDuringFifoFull(Tha60290021HdlcChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021HDLCCHANEL_H_ */


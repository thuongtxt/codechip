/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha6029DccHdlcPktGenerator.c
 *
 * Created Date: Apr 6, 2017
 *
 * Description : DCC HDLC Packet Generator .
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6029DccHdlcPktGenerator.h"

#include "../../../default/man/ThaDeviceInternal.h"
#include "../pw/Tha60290021DccKbyteReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSupportCheck(device, returnValue)                                     \
    {                                                                          \
    ocnModule = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);        \
    if (!ThaModuleOcnSohOverEthIsSupported(ocnModule))                          \
        return returnValue;                                                    \
    }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 AddressWithLocalAddress(ThaModuleOcn self, uint32 localAddress)
    {
    return localAddress + ThaModuleOcnSohOverEthBaseAddress(self);
    }

static uint32 UpenGenmonReqint(ThaModuleOcn self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_upen_genmon_reqint_Base);
    }

static eAtRet MonitorEnable(ThaModuleOcn self, eBool enable)
    {
    uint32 regAddr = UpenGenmonReqint(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_genmon_reqint_Timer_Enable_, mBoolToBin(enable));
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool MonitorIsEnabled(ThaModuleOcn self)
    {
    uint32 regAddr = UpenGenmonReqint(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mBinToBool(mRegField(regVal, cAf6_upen_genmon_reqint_Timer_Enable_));
    }

static eAtRet MonitorTimerHwIntervalSet(ThaModuleOcn self, uint32 timerValue)
    {
    uint32 regAddr = UpenGenmonReqint(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cAf6_upen_genmon_reqint_Timer_Enable_, timerValue ? 1 : 0);
    mRegFieldSet(regVal, cAf6_upen_genmon_reqint_Timer_Value_, timerValue);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet MonitorTimerIntervalSet(ThaModuleOcn self, uint32 timerValue)
    {
	if (timerValue <= cAf6_upen_genmon_reqint_Timer_Value_Mask)
		return MonitorTimerHwIntervalSet(self, timerValue);
	return cAtErrorOutOfRangParm;
    }

static uint32 MonitorTimerIntervalGet(ThaModuleOcn self)
    {
    uint32 regAddr = UpenGenmonReqint(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_upen_genmon_reqint_Timer_Value_);
    }

static uint32 UpenDccCfgTestgenGlbGenIntervalAddress(ThaModuleOcn self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_upen_dcc_cfg_testgen_glb_gen_interval_Base);
    }

static uint32 UpenLoopenAddress(ThaModuleOcn self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_upen_loopen_Base);
    }

static eAtRet GeneratorEnable(ThaModuleOcn self, eBool enable)
    {
    uint32 regAddr = UpenLoopenAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_loopen_genmon_loop_en_, enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool GeneratorIsEnabled(ThaModuleOcn self)
    {
    uint32 regAddr = UpenLoopenAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_upen_loopen_genmon_loop_en_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet GeneratorTimerHwIntervalSet(ThaModuleOcn self, uint32 timerValue)
    {
    uint32 regAddr = UpenDccCfgTestgenGlbGenIntervalAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Enable_, timerValue ? 1 : 0);
    mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Value_, timerValue);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet GeneratorTimerIntervalSet(ThaModuleOcn self, uint32 timerValue)
    {
	if (timerValue <= cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Value_Mask)
	    return GeneratorTimerHwIntervalSet(self, timerValue);
	return cAtErrorOutOfRangParm;
    }

static uint32 GeneratorTimerIntervalGet(ThaModuleOcn self)
    {
    uint32 regAddr = UpenDccCfgTestgenGlbGenIntervalAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_upen_dcc_cfg_testgen_glb_gen_interval_Gen_Intv_Value_);
    }

static uint32 UpenDccCfgTestgenGlbGenModeAddress(ThaModuleOcn self)
    {
    return cAf6Reg_upen_dcc_cfg_testgen_glb_gen_mode_Base + ThaModuleOcnSohOverEthBaseAddress(self);
    }

static eAtRet GeneratorModeSet(ThaModuleOcn self, eAtGeneratorMode mode)
    {
    static const uint32 cTriggerWaitMs = 500; /* Mili-Second */
    uint32 regAddr = UpenDccCfgTestgenGlbGenModeAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mModuleHwWrite(self, regAddr, 0x0);
    AtOsalUSleep(cTriggerWaitMs);
    mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_burst_mod_, mBoolToBin(mode == cAtGeneratorModeOneshot));
    mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_conti_mod_, mBoolToBin(mode == cAtGeneratorModeContinuous));
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtGeneratorMode GeneratorModeGet(ThaModuleOcn self)
    {
    uint32 regAddr = UpenDccCfgTestgenGlbGenModeAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    if (regVal & cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_conti_mod_Mask)
        return cAtGeneratorModeContinuous;

    if (regVal & cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_burst_mod_Mask)
        return cAtGeneratorModeOneshot;

    return cAtGeneratorModeInvalid;
    }

static eAtRet GeneratorForceError(ThaModuleOcn self, uint32 errorMask, eBool enable)
    {
    uint32 regAddr = UpenDccCfgTestgenGlbGenModeAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);

    if (errorMask & cAtPktErrorTypeLength)
        mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_len_, mBoolToBin(enable));

    if (errorMask & cAtPktErrorTypeFcs)
        mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_fcs_, mBoolToBin(enable));

    if (errorMask & cAtPktErrorTypePayload)
        mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_dat_, mBoolToBin(enable));

    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 GeneratorForcedError(ThaModuleOcn self)
    {
    uint32 regAddr = UpenDccCfgTestgenGlbGenModeAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 forceErrMask = 0;

    if (regVal & cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_len_Mask)
        forceErrMask |= cAtPktErrorTypeLength;

    if (regVal & cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_fcs_Mask)
        forceErrMask |= cAtPktErrorTypeFcs;

    if (regVal & cAf6_upen_dcc_cfg_testgen_glb_gen_mode_gen_force_err_dat_Mask)
        forceErrMask |= cAtPktErrorTypePayload;

    return forceErrMask;
    }

static eBool LengthIsValid(uint32 length)
    {
    return ((length > 1) && (length <= cBit15_0)) ? cAtTrue: cAtFalse;
    }

static uint32 UpenDccCfgTestgenGlbLengthAddress(ThaModuleOcn self)
    {
    return AddressWithLocalAddress(self, cAf6Reg_upen_dcc_cfg_testgen_glb_length_Base);
    }

static eAtRet GeneratorMaxLengthSet(ThaModuleOcn self, uint32 length)
    {
    uint32 regAddr = UpenDccCfgTestgenGlbLengthAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 maxValue = cAf6_upen_dcc_cfg_testgen_glb_length_gen_max_length_Mask >> cAf6_upen_dcc_cfg_testgen_glb_length_gen_max_length_Shift;

    if (length > maxValue)
        return cAtErrorOutOfRangParm;

    mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_glb_length_gen_max_length_, length);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet GeneratorMinLengthSet(ThaModuleOcn self, uint32 length)
    {
    uint32 regAddr = UpenDccCfgTestgenGlbLengthAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 maxValue = cAf6_upen_dcc_cfg_testgen_glb_length_gen_min_length_Mask >> cAf6_upen_dcc_cfg_testgen_glb_length_gen_min_length_Shift;

    if (length > maxValue)
        return cAtErrorOutOfRangParm;

    mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_glb_length_gen_min_length_, length);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 GeneratorMaxLengthGet(ThaModuleOcn self)
    {
    uint32 regAddr = UpenDccCfgTestgenGlbLengthAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_upen_dcc_cfg_testgen_glb_length_gen_max_length_);
    }

static uint32 GeneratorMinLengthGet(ThaModuleOcn self)
    {
    uint32 regAddr = UpenDccCfgTestgenGlbLengthAddress(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_upen_dcc_cfg_testgen_glb_length_gen_min_length_);
    }

static eBool VlanIdIsInRange(uint32 value)
    {
    return (value > cBit11_0) ? cAtFalse : cAtTrue;
    }

static uint32 ChannelAddressWithLocalAddress(ThaModuleOcn self, AtChannel hdlc, uint32 localAddress)
    {
    return localAddress + ThaModuleOcnSohOverEthBaseAddress(self) + AtChannelIdGet(hdlc);
    }

static eAtRet ChannelHeaderVlanIdSet(ThaModuleOcn self, AtChannel hdlc, uint16 vlanId)
    {
    uint32 regAddr = ChannelAddressWithLocalAddress(self, hdlc, cAf6Reg_upen_dcc_cfg_testgen_hdr_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_hdr_HDR_VID_, vlanId);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint16 ChannelHeaderVlanIdGet(ThaModuleOcn self, AtChannel hdlc)
    {
    uint32 regAddr = ChannelAddressWithLocalAddress(self, hdlc, cAf6Reg_upen_dcc_cfg_testgen_hdr_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (uint16) mRegField(regVal, cAf6_upen_dcc_cfg_testgen_hdr_HDR_VID_);
    }

static eAtRet ChannelHeaderDestMacLsbSet(ThaModuleOcn self, AtChannel hdlc, uint8 value)
    {
    uint32 regAddr = ChannelAddressWithLocalAddress(self, hdlc, cAf6Reg_upen_dcc_cfg_testgen_hdr_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_hdr_MAC_DA_, value);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint8 ChannelHeaderDestMacLsbGet(ThaModuleOcn self, AtChannel hdlc)
    {
    uint32 regAddr = ChannelAddressWithLocalAddress(self, hdlc, cAf6Reg_upen_dcc_cfg_testgen_hdr_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (uint8) mRegField(regVal, cAf6_upen_dcc_cfg_testgen_hdr_MAC_DA_);
    }

static eBool LengthModeIsSupported(eAtLengthMode mode)
    {
    switch ((uint32) mode)
        {
        case cAtLengthModeFix                   : return cAtTrue;
        case cAtLengthModeIncrease              : return cAtTrue;
        case cAtLengthModeRandom                : return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 LengthModeSw2Hw(eAtLengthMode mode)
    {
    switch ((uint32) mode)
        {
        case cAtLengthModeFix                  : return 0;
        case cAtLengthModeIncrease             : return 1;
        case cAtLengthModeRandom               : return 2;
        default: return cAtFalse;
        }
    }

static eAtLengthMode LengthModeHw2Sw(uint32 mode)
    {
    switch (mode)
        {
        case 0  :   return cAtLengthModeFix;
        case 1  :   return cAtLengthModeIncrease;
        case 2  :   return cAtLengthModeRandom;
        default:    return cAtLengthModeUnknown;
        }
    }

static eAtRet ChannelLengthModeSet(ThaModuleOcn self, AtChannel hdlc, uint32 mode)
    {
    uint32 regAddr = ChannelAddressWithLocalAddress(self, hdlc, cAf6Reg_upen_dcc_cfg_testgen_mod_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_mod_gen_len_mode_, mode);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 ChannelLengthModeGet(ThaModuleOcn self, AtChannel hdlc)
    {
    uint32 regAddr = ChannelAddressWithLocalAddress(self, hdlc, cAf6Reg_upen_dcc_cfg_testgen_mod_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_upen_dcc_cfg_testgen_mod_gen_len_mode_);
    }

static eBool PatternIsSupported(eAtPrbsMode pattern)
    {
    switch ((uint32) pattern)
        {
        case cAtPrbsModePrbsSeq                 : return cAtTrue;
        case cAtPrbsModePrbsFixedPattern1Byte   : return cAtTrue;
        default: return cAtFalse;
        }
    }

static uint32 PatternSw2Hw(eAtPrbsMode pattern)
    {
    switch ((uint32) pattern)
        {
        case cAtPrbsModePrbsSeq                 : return 0;
        case cAtPrbsModePrbsFixedPattern1Byte   : return 2;
        default: return cAtFalse;
        }
    }

static eAtPrbsMode PatternHw2Sw(uint32 pattern)
    {
    switch (pattern)
        {
        case 0  :   return cAtPrbsModePrbsSeq;
        case 2  :   return cAtPrbsModePrbsFixedPattern1Byte;
        default:    return cAtPrbsModeInvalid;
        }
    }

static eAtRet ChannelPayloadPatternSet(ThaModuleOcn self, AtChannel hdlc, uint32 pattern)
    {
    uint32 regAddr = ChannelAddressWithLocalAddress(self, hdlc, cAf6Reg_upen_dcc_cfg_testgen_mod_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_mod_gen_payload_mod_, pattern);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 ChannelPayloadPatternGet(ThaModuleOcn self, AtChannel hdlc)
    {
    uint32 regAddr = ChannelAddressWithLocalAddress(self, hdlc, cAf6Reg_upen_dcc_cfg_testgen_mod_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_upen_dcc_cfg_testgen_mod_gen_payload_mod_);
    }

static eBool FixPatternIsInRange(uint32 value)
    {
    return (value > cBit7_0) ? cAtFalse : cAtTrue;
    }

static eAtRet ChannelFixPatternSet(ThaModuleOcn self, AtChannel hdlc, uint32 pattern)
    {
    uint32 regAddr = ChannelAddressWithLocalAddress(self, hdlc, cAf6Reg_upen_dcc_cfg_testgen_mod_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_mod_gen_fix_pattern_, pattern);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 ChannelFixPatternGet(ThaModuleOcn self, AtChannel hdlc)
    {
    uint32 regAddr = ChannelAddressWithLocalAddress(self, hdlc, cAf6Reg_upen_dcc_cfg_testgen_mod_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_upen_dcc_cfg_testgen_mod_gen_fix_pattern_);
    }

static eBool NumPktIsInRange(uint32 value)
    {
    return (value > cBit19_0) ? cAtFalse : cAtTrue;
    }

static eAtRet ChannelNumPktSet(ThaModuleOcn self, AtChannel hdlc, uint32 pattern)
    {
    uint32 regAddr = ChannelAddressWithLocalAddress(self, hdlc, cAf6Reg_upen_dcc_cfg_testgen_mod_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_dcc_cfg_testgen_mod_gen_number_of_packet_, pattern);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 ChannelNumPktGet(ThaModuleOcn self, AtChannel hdlc)
    {
    uint32 regAddr = ChannelAddressWithLocalAddress(self, hdlc, cAf6Reg_upen_dcc_cfg_testgen_mod_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return mRegField(regVal, cAf6_upen_dcc_cfg_testgen_mod_gen_number_of_packet_);
    }

static uint32 CounterAddressBase(uint32 counterType)
    {
    switch (counterType)
        {
        case cAtHdlcChannelCounterTypeRxGoodPackets:        return cAf6Reg_upen_mon_godpkt_Base;
        case cAtHdlcChannelCounterTypeRxFcsErrors:          return cAf6Reg_upen_mon_errfcs_Base;
        case cAtHdlcChannelCounterTypeRxAborts:             return cAf6Reg_upen_mon_abrpkt_Base;
        case cAtHdlcChannelCounterTypeRxPacketsDataError:   return cAf6Reg_upen_mon_errpkt_Base;
        case cAtHdlcChannelCounterTypeRxPacketsSeqError:    return cAf6Reg_upen_mon_errseq_Base;
        case cAtHdlcChannelCounterTypeRxChannelIdError:     return cAf6Reg_upen_mon_errvcg_Base;

        default: return cInvalidUint32;
        }
    }

static uint32 ChannelCounterGet(ThaModuleOcn self, AtChannel hdlc, uint32 counterType, eBool r2c)
    {
	uint32 counterVal = 0;
    uint32 addressBase = CounterAddressBase(counterType);
    uint32 regAddr;

    if (addressBase == cInvalidUint32)
        return 0;

    regAddr = ChannelAddressWithLocalAddress(self, hdlc, addressBase);
    counterVal = mModuleHwRead(self, regAddr);
    if (r2c)
    	mModuleHwWrite(self, regAddr, 0x0);

    return counterVal;
    }

static eAtRet PktGenInit(ThaModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    eAtRet ret = cAtOk;

    if (!ThaModuleOcnSohOverEthIsSupported(self))
        return cAtOk;

    ret |= Tha6029DccHdlcSoELoopbackTypeSet(device, cAtPktLoopbackTypeNone, cAtTrue);
    ret |= Tha6029DccHdlcPktMonEnable(device, cAtFalse);
    ret |= Tha6029DccHdlcPktGenEnable(device, cAtFalse);
    ret |= Tha6029DccHdlcPktGenModeSet(device, cAtGeneratorModeInvalid);

    return ret;
    }

static eAtRet SoELoopbackTypeSet(AtDevice self, uint32 loopBackType, eBool enable)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(self, cThaModuleOcn);
    uint32 regAddr = UpenLoopenAddress(ocnModule);
    uint32 regVal = mModuleHwRead(ocnModule, regAddr);

    if (loopBackType == cAtPktLoopbackTypeNone)
        {
        mRegFieldSet(regVal, cAf6_upen_loopen_dsgm_loop_en_, 0);
        mRegFieldSet(regVal, cAf6_upen_loopen_hdlc_loop_en_, 0);
        mRegFieldSet(regVal, cAf6_upen_loopen_ksgm_loop_en_, 0);
        mRegFieldSet(regVal, cAf6_upen_loopen_ksdh_loop_en_, 0);
        mRegFieldSet(regVal, cAf6_upen_loopen_dcc_buffer_loop_en_, 0);
        mModuleHwWrite(ocnModule, regAddr, regVal);
        return cAtOk;
        }

    if (loopBackType & cAtPktLoopbackTypeDccSgmiiLoop)
        mRegFieldSet(regVal, cAf6_upen_loopen_dsgm_loop_en_, enable ? 1 : 0);

    if (loopBackType & cAtPktLoopbackTypeDccHdlcLoop)
        mRegFieldSet(regVal, cAf6_upen_loopen_hdlc_loop_en_, enable ? 1 : 0);

    if (loopBackType & cAtPktLoopbackTypeKbyteSgmiiLoop)
        mRegFieldSet(regVal, cAf6_upen_loopen_ksgm_loop_en_, enable ? 1 : 0);

    if (loopBackType & cAtPktLoopbackTypeKbyteSdhLoop)
        mRegFieldSet(regVal, cAf6_upen_loopen_ksdh_loop_en_, enable ? 1 : 0);

    if (loopBackType & cAtPktLoopbackTypeDccBufferLoop)
        mRegFieldSet(regVal, cAf6_upen_loopen_dcc_buffer_loop_en_, enable ? 1 : 0);

    mModuleHwWrite(ocnModule, regAddr, regVal);

    return cAtOk;
    }

static uint32 SoELoopbackTypeGet(AtDevice self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(self, cThaModuleOcn);
    uint32 loopBackType = cAtPktLoopbackTypeNone;
    uint32 regAddr = UpenLoopenAddress(ocnModule);
    uint32 regVal = mModuleHwRead(ocnModule, regAddr);

    if (regVal & cAf6_upen_loopen_dsgm_loop_en_Mask)
        loopBackType |= cAtPktLoopbackTypeDccSgmiiLoop;

    if (regVal & cAf6_upen_loopen_hdlc_loop_en_Mask)
        loopBackType |= cAtPktLoopbackTypeDccHdlcLoop;

    if (regVal & cAf6_upen_loopen_ksgm_loop_en_Mask)
        loopBackType |= cAtPktLoopbackTypeKbyteSgmiiLoop;

    if (regVal & cAf6_upen_loopen_ksdh_loop_en_Mask)
        loopBackType |= cAtPktLoopbackTypeKbyteSdhLoop;

    if (regVal & cAf6_upen_loopen_dcc_buffer_loop_en_Mask)
        loopBackType |= cAtPktLoopbackTypeDccBufferLoop;

    return loopBackType;
    }

eAtRet Tha6029DccHdlcPktGenEnable(AtDevice self, eBool enable)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtErrorModeNotSupport)
    return GeneratorEnable(ocnModule, enable);
    }

eBool Tha6029DccHdlcPktGenIsEnabled(AtDevice self)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtFalse)
    return GeneratorIsEnabled(ocnModule);
    }

eAtRet Tha6029DccHdlcPktGenTimerIntervalSet(AtDevice self, uint32 interval)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtErrorModeNotSupport)
    return GeneratorTimerIntervalSet(ocnModule, interval);
    }

uint32 Tha6029DccHdlcPktGenTimerIntervalGet(AtDevice self)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, 0)
    return GeneratorTimerIntervalGet(ocnModule);
    }

eAtRet Tha6029DccHdlcPktMonEnable(AtDevice self, eBool enable)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtErrorModeNotSupport)
    return MonitorEnable(ocnModule, enable);
    }

eBool Tha6029DccHdlcPktMonIsEnabled(AtDevice self)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtFalse)
    return MonitorIsEnabled(ocnModule);
    }

eAtRet Tha6029DccHdlcPktMonTimerIntervalSet(AtDevice self, uint32 interval)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtErrorModeNotSupport)
    return MonitorTimerIntervalSet(ocnModule, interval);
    }

uint32 Tha6029DccHdlcPktMonTimerIntervalGet(AtDevice self)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, 0)
    return MonitorTimerIntervalGet(ocnModule);
    }

eAtRet Tha6029DccHdlcPktGenModeSet(AtDevice self, eAtGeneratorMode mode)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtErrorModeNotSupport)
    return GeneratorModeSet(ocnModule, mode);
    }

eAtGeneratorMode Tha6029DccHdlcPktGenModeGet(AtDevice self)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtGeneratorModeInvalid)
    return GeneratorModeGet(ocnModule);
    }

eAtRet Tha6029DccHdlcPktGenErrorForce(AtDevice self, uint32 errorMask)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtErrorModeNotSupport)
    return GeneratorForceError(ocnModule, errorMask, cAtTrue);
    }

eAtRet Tha6029DccHdlcPktGenErrorUnForce(AtDevice self, uint32 errorMask)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtErrorModeNotSupport)
    return GeneratorForceError(ocnModule, errorMask, cAtFalse);
    }

uint32 Tha6029DccHdlcPktGenForcedErrorGet(AtDevice self)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, 0)
    return GeneratorForcedError(ocnModule);
    }

eAtRet Tha6029DccHdlcPktGenMinLengthSet(AtDevice self, uint32 length)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtErrorModeNotSupport)

    if (!LengthIsValid(length))
        return cAtErrorInvlParm;

    if (length > GeneratorMaxLengthGet(ocnModule))
        return cAtErrorInvlParm;

    return GeneratorMinLengthSet(ocnModule, length);
    }

uint32 Tha6029DccHdlcPktGenMinLengthGet(AtDevice self)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, 0)
    return GeneratorMinLengthGet(ocnModule);
    }

eAtRet Tha6029DccHdlcPktGenMaxLengthSet(AtDevice self, uint32 length)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtErrorModeNotSupport)

    if (!LengthIsValid(length))
        return cAtErrorInvlParm;

    if (length < GeneratorMinLengthGet(ocnModule))
        return cAtErrorInvlParm;

    return GeneratorMaxLengthSet(ocnModule, length);
    }

uint32 Tha6029DccHdlcPktGenMaxLengthGet(AtDevice self)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, 0)
    return GeneratorMaxLengthGet(ocnModule);
    }

eAtRet Tha6029DccHdlcPktGenChannelEnable(AtHdlcChannel self, eBool enable)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, cAtErrorModeNotSupport)

    return Tha60290021HdlcPktGeneratorEnable((Tha60290021HdlcChannel)self, enable);
    }

eBool Tha6029DccHdlcPktGenChannelIsEnabled(AtHdlcChannel self)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, cAtFalse)

    return Tha60290021HdlcPktGeneratorIsEnabled((Tha60290021HdlcChannel)self);
    }

eAtRet Tha6029DccHdlcPktGenChannelHeaderVlanIdSet(AtHdlcChannel self, uint16 vlanId)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, cAtErrorModeNotSupport)

    if (!VlanIdIsInRange(vlanId))
        return cAtErrorOutOfRangParm;

    return ChannelHeaderVlanIdSet(ocnModule, (AtChannel)self, vlanId);
    }

uint16 Tha6029DccHdlcPktGenChannelHeaderVlanIdGet(AtHdlcChannel self)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, 0)
    return ChannelHeaderVlanIdGet(ocnModule, (AtChannel)self);
    }

eAtRet Tha6029DccHdlcPktGenChannelHeaderDestMacLsbSet(AtHdlcChannel self, uint8 value)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, cAtErrorModeNotSupport)
    return ChannelHeaderDestMacLsbSet(ocnModule, (AtChannel)self, value);
    }

uint8 Tha6029DccHdlcPktGenChannelHeaderDestMacLsbGet(AtHdlcChannel self)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, 0)
    return ChannelHeaderDestMacLsbGet(ocnModule, (AtChannel)self);
    }

eAtRet Tha6029DccHdlcPktGenChannelLengthModeSet(AtHdlcChannel self, eAtLengthMode mode)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, cAtErrorModeNotSupport)
    if (!LengthModeIsSupported(mode))
        return cAtErrorModeNotSupport;

    return ChannelLengthModeSet(ocnModule, (AtChannel)self, LengthModeSw2Hw(mode));
    }

eAtLengthMode Tha6029DccHdlcPktGenChannelLengthModeGet(AtHdlcChannel self)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, cAtErrorModeNotSupport)
    return LengthModeHw2Sw(ChannelLengthModeGet(ocnModule, (AtChannel)self));
    }

eAtRet Tha6029DccHdlcPktGenChannelPayloadPatternSet(AtHdlcChannel self, eAtPrbsMode pattern)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, cAtErrorModeNotSupport)

    if (!PatternIsSupported(pattern))
        return cAtErrorModeNotSupport;

    return ChannelPayloadPatternSet(ocnModule, (AtChannel)self, PatternSw2Hw(pattern));
    }

eAtPrbsMode Tha6029DccHdlcPktGenChannelPayloadPatternGet(AtHdlcChannel self)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, cAtPrbsModeInvalid)

    return PatternHw2Sw(ChannelPayloadPatternGet(ocnModule, (AtChannel)self));
    }

eAtRet Tha6029DccHdlcPktGenChannelFixPatternSet(AtHdlcChannel self, uint32 value)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, cAtErrorModeNotSupport)

    if (!FixPatternIsInRange(value))
        return cAtErrorOutOfRangParm;

    return ChannelFixPatternSet(ocnModule, (AtChannel)self, value);
    }

uint32 Tha6029DccHdlcPktGenChannelFixPatternGet(AtHdlcChannel self)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, cAtErrorModeNotSupport)

    return ChannelFixPatternGet(ocnModule, (AtChannel)self);
    }

eAtRet Tha6029DccHdlcPktGenChannelNumPktSet(AtHdlcChannel self, uint32 numPkts)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, cAtErrorModeNotSupport)

    if (!NumPktIsInRange(numPkts))
        return cAtErrorOutOfRangParm;

    return ChannelNumPktSet(ocnModule, (AtChannel)self, numPkts);
    }

uint32 Tha6029DccHdlcPktGenChannelNumPktGet(AtHdlcChannel self)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, cAtErrorModeNotSupport)

    return ChannelNumPktGet(ocnModule, (AtChannel)self);
    }

eAtRet Tha6029DccHdlcPktGenInit(AtDevice self)
    {
	ThaModuleOcn ocnModule;
	mSupportCheck(self, cAtErrorModeNotSupport)
	return PktGenInit(ocnModule);
    }

uint32 Tha6029DccHdlcPktGenChannelCounterGet(AtHdlcChannel self, uint32 counterType, eBool r2c)
    {
    ThaModuleOcn ocnModule;
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    mSupportCheck(device, 0)

    return ChannelCounterGet(ocnModule, (AtChannel)self, counterType, r2c);
    }

eAtRet Tha6029DccHdlcSoELoopbackTypeSet(AtDevice self, uint32 loopBackType, eBool enable)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, cAtErrorModeNotSupport)
    return SoELoopbackTypeSet(self, loopBackType, enable);
    }

uint32 Tha6029DccHdlcSoELoopbackTypeGet(AtDevice self)
    {
    ThaModuleOcn ocnModule;
    mSupportCheck(self, 0x0)
    return SoELoopbackTypeGet(self);
    }


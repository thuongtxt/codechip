/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Encap
 * 
 * File        : Tha60290021HdlcChanelInternal.h
 * 
 * Created Date: Sep 3, 2016
 *
 * Description : HDLC channel
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021HDLCCHANELINTERNAL_H_
#define _THA60290021HDLCCHANELINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhLine.h"
#include "Tha60290021HdlcChannel.h"
#include "AtPw.h"
#include "../../../../generic/encap/AtHdlcChannelInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021HdlcChannelMethods
    {
    uint32 (*ChannelOffSet)(Tha60290021HdlcChannel self);
    uint32 (*CounterBaseAddress)(Tha60290021HdlcChannel self, uint16 counterType, eBool r2c);
    eBool (*HasTohBusEnabling)(Tha60290021HdlcChannel self);
    eAtRet (*TxHwOcnDccInsertionEnable)(Tha60290021HdlcChannel self, eBool enable);
    eAtRet (*TxDccHwEnable)(Tha60290021HdlcChannel self, eBool enable);
    uint32 (*CounterBaseAddressByHwType)(Tha60290021HdlcChannel self, uint32 hwType);
    eAtRet (*PktGeneratorEnable)(Tha60290021HdlcChannel self, eBool enable);
    eBool  (*PktGeneratorIsEnabled)(Tha60290021HdlcChannel self);
    eAtRet (*EncapFrameBitOrderSet)(Tha60290021HdlcChannel self, eAtBitOrder order);
    eAtRet (*DecapFrameBitOrderSet)(Tha60290021HdlcChannel self, eAtBitOrder order);

    /* Interrupt */
    eBool (*HasInterruptV2)(Tha60290021HdlcChannel self);

    /* Debug */
    eBool (*HasPacketCounterDuringFifoFull)(Tha60290021HdlcChannel self);
    }tTha60290021HdlcChannelMethods;

typedef struct tTha60290021HdlcChannel
    {
    tAtHdlcChannel super;
    const tTha60290021HdlcChannelMethods * methods;

    /* Private data*/
    eAtSdhLineDccLayer layers;
    AtPw pseudowire;
    eBool txEnabled; /* To cache enabling when PW has not been created */
    }tTha60290021HdlcChannel;

typedef struct tTha60290021HdlcChannelV2
    {
    tTha60290021HdlcChannel super;
    }tTha60290021HdlcChannelV2;

typedef struct tTha60290021HdlcChannelV3
    {
    tTha60290021HdlcChannelV2 super;
    }tTha60290021HdlcChannelV3;

typedef struct tTha60290021HdlcChannelV4
    {
    tTha60290021HdlcChannelV3 super;
    }tTha60290021HdlcChannelV4;

typedef struct tTha60290021HdlcChannelV5
    {
    tTha60290021HdlcChannelV4 super;
    }tTha60290021HdlcChannelV5;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60290021HdlcChannelTxRsMsDccInsHandle(Tha60290021HdlcChannel self, eBool enable);
eAtRet Tha60290021HdlcV3EncapFrameBitOrderSet(Tha60290021HdlcChannel self, eAtBitOrder order);
eAtRet Tha60290021HdlcV3DecapFrameBitOrderSet(Tha60290021HdlcChannel self, eAtBitOrder order);
AtHdlcChannel Tha60290021HdlcChannelObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);
AtHdlcChannel Tha60290021HdlcChannelV2ObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);
AtHdlcChannel Tha60290021HdlcChannelV3ObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);
AtHdlcChannel Tha60290021HdlcChannelV4ObjectInit(AtHdlcChannel self, uint32 channelId, AtSdhLine line, eAtSdhLineDccLayer layers, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021HDLCCHANELINTERNAL_H_ */


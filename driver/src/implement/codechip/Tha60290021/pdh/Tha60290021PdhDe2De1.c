/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290021PdhDe2De1.c
 *
 * Created Date: Dec 9, 2016
 *
 * Description : Concrete class for PDH De1
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"
#include "../../../default/man/ThaDevice.h"
#include "../common/Tha602900xxCommon.h"
#include "Tha60290021ModulePdh.h"
#include "Tha60290021PdhDe2De1Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tThaPdhDe1Methods m_ThaPdhDe1Override;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule PmcModule(AtChannel self)
	{
	return AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
	}

static uint32 CounterGet(AtChannel self, uint16 counterType)
	{
	if (ThaTdmChannelShouldGetCounterFromPmc(self))
		return ThaModulePmcSdhVcDe1CounterGet(PmcModule(self), (AtPdhChannel)self, counterType, cAtFalse);

	return m_AtChannelMethods->CounterGet(self, counterType);
	}

static uint32 CounterClear(AtChannel self, uint16 counterType)
	{
	if (ThaTdmChannelShouldGetCounterFromPmc(self))
		return ThaModulePmcSdhVcDe1CounterGet(PmcModule(self), (AtPdhChannel)self, counterType, cAtTrue);

	return m_AtChannelMethods->CounterClear(self, counterType);
	}

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (loopbackMode == cAtPdhLoopbackModeLocalPayload)
        return Tha602900xxDe1LocalPayloadIsSupported(self, loopbackMode);

    return m_AtChannelMethods->LoopbackIsSupported(self, loopbackMode);
    }

static eBool ShouldPreserveService(ThaPdhDe1 self)
    {
    return Tha602900xxPdhDe1ShouldPreserveService(self);
    }

static void OverrideThaPdhDe1(AtPdhDe1 self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe1Override, mMethodsGet(de1), sizeof(m_ThaPdhDe1Override));

        mMethodOverride(m_ThaPdhDe1Override, ShouldPreserveService);
        }

    mMethodsSet(de1, &m_ThaPdhDe1Override);
    }

static void OverrideAtChannel(AtPdhDe1 self)
	{
	AtChannel channel = (AtChannel)self;

	if (!m_methodsInit)
		{
		AtOsal osal = AtSharedDriverOsalGet();
		m_AtChannelMethods = mMethodsGet(channel);
		mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

		mMethodOverride(m_AtChannelOverride, CounterGet);
		mMethodOverride(m_AtChannelOverride, CounterClear);
		mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
		}

	mMethodsSet(channel, &m_AtChannelOverride);
	}

static void Override(AtPdhDe1 self)
	{
	OverrideAtChannel(self);
	OverrideThaPdhDe1(self);
	}

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PdhDe2De1);
    }

AtPdhDe1 Tha60290021PdhDe2De1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60210031PdhDe2De1ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe1 Tha60290021PdhDe2De1New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe1 newDe1 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe1 == NULL)
        return NULL;

    /* construct it */
    return Tha60290021PdhDe2De1ObjectInit(newDe1, channelId, module);
    }

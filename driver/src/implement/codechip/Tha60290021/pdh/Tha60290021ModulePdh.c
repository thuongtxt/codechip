/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290021ModulePdh.c
 *
 * Created Date: Jul 12, 2016
 *
 * Description : PDH management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../man/Tha60290021Device.h"
#include "../common/Tha602900xxCommon.h"
#include "Tha60290021ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleMethods             m_AtModuleOverride;
static tAtModulePdhMethods          m_AtModulePdhOverride;
static tThaModulePdhMethods         m_ThaModulePdhOverride;
static tTha60210031ModulePdhMethods m_Tha60210031ModulePdhOverride;

/* Save super implementation */
static const tAtModuleMethods      *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool DatalinkFcsBitOrderIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static eBool MdlIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool ForcedAisCorrectionIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionSupportAlarmForwardingStatus(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionFixDs3AisInterrupt(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionSupportDe3Disabling(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionSupportAisDownStream(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionSupportDe3Ssm(ThaModulePdh self)
    {
    AtUnused(self);
    /* TODO: HW Not support yet */
    return cInvalidUint32;
    }

static AtPdhDe1 VcDe1Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
	return Tha60290021PdhVcDe1New((AtSdhVc)sdhVc, self);
    }

static AtPdhDe3 VcDe3Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    uint8 slice, hwSts;
    ThaSdhChannel2HwMasterStsId(sdhVc, cAtModulePdh, &slice, &hwSts);
    return Tha60290021PdhVcDe3New(hwSts, self);
    }

static AtPdhDe1 De2De1Create(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id)
	{
	AtUnused(de2);
	return Tha60290021PdhDe2De1New(de1Id, self);
	}

static uint32 StartVersionSupportDe3FramedSatop(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static eBool De3FramedSatopIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool De3AisSelectablePatternIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    const char *ramName = AtInternalRamName(ram);

    if (ramName == NULL)
        return m_AtModuleMethods->InternalRamIsReserved(self, ram);

    if (AtStrstr(ramName, sTha60210011ModulePdhRamPDHVTAsyncMapControl) != NULL)
        return cAtTrue;

    return m_AtModuleMethods->InternalRamIsReserved(self, ram);
    }

static eBool NeedClearanceNotifyLogic(AtModulePdh self)
    {
    AtUnused(self);
    return Tha602900xxNeedClearanceNotifyLogic();
    }

static uint32 *HoldRegistersGet(AtModule self, uint16 *numberOfHoldRegisters)
    {
    uint32 baseAddress = ThaModulePdhBaseAddress((ThaModulePdh)self);
    static uint32 holdRegisters[] = {0x400, 0x401, 0x402};

    holdRegisters[0] = baseAddress + 0x400;
    holdRegisters[1] = baseAddress + 0x401;
    holdRegisters[2] = baseAddress + 0x402;

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static AtLongRegisterAccess LongRegisterAccessCreate(AtModule self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = AtModuleHoldRegistersGet(self, &numHoldRegisters);
    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static eBool ShouldRejectTxChangeWhenChannelBoundPw(ThaModulePdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        mMethodOverride(m_AtModuleOverride, HoldRegistersGet);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccessCreate);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(self), sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, VcDe1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe3Create);
        mMethodOverride(m_AtModulePdhOverride, De2De1Create);
        mMethodOverride(m_AtModulePdhOverride, NeedClearanceNotifyLogic);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(module), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        mMethodOverride(m_ThaModulePdhOverride, MdlIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, ForcedAisCorrectionIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAlarmForwardingStatus);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionFixDs3AisInterrupt);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3Disabling);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportAisDownStream);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3Ssm);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportDe3FramedSatop);
        mMethodOverride(m_ThaModulePdhOverride, De3FramedSatopIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, De3AisSelectablePatternIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, ShouldRejectTxChangeWhenChannelBoundPw);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void OverrideTha60210031ModulePdh(AtModulePdh self)
    {
    Tha60210031ModulePdh pdhModule = (Tha60210031ModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_Tha60210031ModulePdhOverride));

        mMethodOverride(m_Tha60210031ModulePdhOverride, DatalinkFcsBitOrderIsSupported);
        }

    mMethodsSet(pdhModule, &m_Tha60210031ModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModule(self);
	OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    OverrideTha60210031ModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModulePdh);
    }

AtModulePdh Tha60290021ModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60290021ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModulePdhObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290021PdhDe2De1Internal.h
 * 
 * Created Date: Apr 23, 2017
 *
 * Description : DE1
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021PDHDE2DE1INTERNAL_H_
#define _THA60290021PDHDE2DE1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhDe2De1Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021PdhDe2De1
    {
    tTha60210031PdhDe2De1 super;
    }tTha60290021PdhDe2De1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 Tha60290021PdhDe2De1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021PDHDE2DE1INTERNAL_H_ */


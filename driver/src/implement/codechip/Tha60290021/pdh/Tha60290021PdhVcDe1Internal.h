/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290021PdhVcDe1Internal.h
 * 
 * Created Date: Apr 23, 2017
 *
 * Description : DE1
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021PDHVCDE1INTERNAL_H_
#define _THA60290021PDHVCDE1INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhVcDe1Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021PdhVcDe1
    {
    tTha60210031PdhVcDe1 super;
    }tTha60290021PdhVcDe1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 Tha60290021PdhVcDe1ObjectInit(AtPdhDe1 self, AtSdhVc vc1x, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021PDHVCDE1INTERNAL_H_ */


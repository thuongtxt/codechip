/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290021PdhVcDe3Internal.h
 * 
 * Created Date: May 30, 2017
 *
 * Description : VC PDH DE3 implementation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021PDHVCDE3INTERNAL_H_
#define _THA60290021PDHVCDE3INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhDe3Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021PdhVcDe3
    {
    tTha60210031PdhDe3 super;
    }tTha60290021PdhVcDe3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe3 Tha60290021PdhVcDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021PDHVCDE3INTERNAL_H_ */


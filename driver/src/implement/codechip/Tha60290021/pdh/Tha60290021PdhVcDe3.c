/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290021PdhVcDe3.c
 *
 * Created Date: Nov 18, 2016
 *
 * Description : VC PDH DE3 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290021/xc/Tha60290021ModuleXc.h"
#include "Tha60290021ModulePdh.h"
#include "Tha60290021PdhVcDe3Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods         m_AtChannelOverride;
static tThaPdhDe3Methods         m_ThaPdhDe3Override;
static tAtPdhDe3Methods          m_AtPdhDe3Override;
static tTha60210031PdhDe3Methods m_Tha60210031PdhDe3Override;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtPdhDe3Methods  *m_AtPdhDe3Methods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule PmcModule(AtChannel self)
	{
	return AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
	}

static uint32 CounterGet(AtChannel self, uint16 counterType)
	{
	if (ThaTdmChannelShouldGetCounterFromPmc(self))
		return ThaModulePmcSdhVcDe3CounterGet(PmcModule(self), (AtPdhChannel)self, counterType, cAtFalse);

	return m_AtChannelMethods->CounterGet(self, counterType);
	}

static uint32 CounterClear(AtChannel self, uint16 counterType)
	{
	if (ThaTdmChannelShouldGetCounterFromPmc(self))
		return ThaModulePmcSdhVcDe3CounterGet(PmcModule(self), (AtPdhChannel)self, counterType, cAtTrue);

	return m_AtChannelMethods->CounterClear(self, counterType);
	}

static eBool IsManagedByModulePdh(ThaPdhDe3 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtModuleXc XcModule(AtPdhDe3 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static AtSdhChannel XcHidingVcForIdString(AtSdhChannel vc)
    {
    AtSdhChannel parent = AtSdhChannelParentChannelGet(vc);

    if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeAu3)
        {
        AtSdhChannel source = (AtSdhChannel)AtChannelSourceGet((AtChannel)vc);
        if (source)
            return source;
        }

    return vc;
    }

static const char *VcIdString(AtPdhDe3 self, AtSdhVc vc)
    {
    if (Tha60290021ModuleXcHideModeGet(XcModule(self)) == cTha60290021XcHideModeNone)
        return m_AtPdhDe3Methods->VcIdString(self, vc);

    return AtChannelIdString((AtChannel)XcHidingVcForIdString((AtSdhChannel)vc));
    }

static eBool BpvCounterIsSupported(Tha60210031PdhDe3 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideTha60210031PdhDe3(AtPdhDe3 self)
    {
    Tha60210031PdhDe3 channel = (Tha60210031PdhDe3)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031PdhDe3Override, mMethodsGet(channel), sizeof(tTha60210031PdhDe3Methods));

        mMethodOverride(m_Tha60210031PdhDe3Override, BpvCounterIsSupported);
        }

    mMethodsSet(channel, &m_Tha60210031PdhDe3Override);
    }

static void OverrideAtPdhDe3(AtPdhDe3 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhDe3Methods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhDe3Override, m_AtPdhDe3Methods, sizeof(m_AtPdhDe3Override));

        mMethodOverride(m_AtPdhDe3Override, VcIdString);
        }

    mMethodsSet(self, &m_AtPdhDe3Override);
    }

static void OverrideThaPdhDe3(AtPdhDe3 self)
    {
    ThaPdhDe3 channel = (ThaPdhDe3)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3Override, mMethodsGet(channel), sizeof(m_ThaPdhDe3Override));

        mMethodOverride(m_ThaPdhDe3Override, IsManagedByModulePdh);
        }

    mMethodsSet(channel, &m_ThaPdhDe3Override);
    }

static void OverrideAtChannel(AtPdhDe3 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtChannel(self);
    OverrideAtPdhDe3(self);
    OverrideThaPdhDe3(self);
    OverrideTha60210031PdhDe3(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021PdhVcDe3);
    }

AtPdhDe3 Tha60290021PdhVcDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60210031PdhDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe3 Tha60290021PdhVcDe3New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return Tha60290021PdhVcDe3ObjectInit(newDe3, channelId, module);
    }

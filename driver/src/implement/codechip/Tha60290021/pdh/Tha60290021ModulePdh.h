/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60290021ModulePdh.h
 * 
 * Created Date: Nov 18, 2016
 *
 * Description : Internal PDH module declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPDH_H_
#define _THA60290021MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 Tha60290021PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module);
AtPdhDe3 Tha60290021PdhVcDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha60290021PdhDe2De1New(uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPDH_H_ */


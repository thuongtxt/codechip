/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60290021PohReg.h
 * 
 * Created Date: Sep 8, 2016
 *
 * Description : Tha60290021PohReg declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021POHREG_H_
#define _THA60290021POHREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
Reg Name   : POH CPE Global Control
Reg Addr   : 0x01_0002
Reg Formula:
    Where  :
Reg Desc   :
This register is used to control whether receive bytes from OCN block .

------------------------------------------------------------------------------*/
#define cAf6Reg_pcfg_glbcpectr_Base                                                                   0x010002
#define cAf6Reg_pcfg_glbcpectr                                                                        0x010002
#define cAf6Reg_pcfg_glbcpectr_WidthVal                                                                     32
#define cAf6Reg_pcfg_glbcpectr_WriteMask                                                                   0x0

/*--------------------------------------
BitField Name: cpevtline5en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 5
BitField Bits: [29]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline5en_Bit_Start                                                          29
#define cAf6_pcfg_glbcpectr_cpevtline5en_Bit_End                                                            29
#define cAf6_pcfg_glbcpectr_cpevtline5en_Mask                                                           cBit29
#define cAf6_pcfg_glbcpectr_cpevtline5en_Shift                                                              29
#define cAf6_pcfg_glbcpectr_cpevtline5en_MaxVal                                                            0x1
#define cAf6_pcfg_glbcpectr_cpevtline5en_MinVal                                                            0x0
#define cAf6_pcfg_glbcpectr_cpevtline5en_RstVal                                                            0x0

/*--------------------------------------
BitField Name: cpevtline4en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 4
BitField Bits: [28]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline4en_Bit_Start                                                          28
#define cAf6_pcfg_glbcpectr_cpevtline4en_Bit_End                                                            28
#define cAf6_pcfg_glbcpectr_cpevtline4en_Mask                                                           cBit28
#define cAf6_pcfg_glbcpectr_cpevtline4en_Shift                                                              28
#define cAf6_pcfg_glbcpectr_cpevtline4en_MaxVal                                                            0x1
#define cAf6_pcfg_glbcpectr_cpevtline4en_MinVal                                                            0x0
#define cAf6_pcfg_glbcpectr_cpevtline4en_RstVal                                                            0x0

/*--------------------------------------
BitField Name: cpevtline3en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 3
BitField Bits: [27]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline3en_Bit_Start                                                          27
#define cAf6_pcfg_glbcpectr_cpevtline3en_Bit_End                                                            27
#define cAf6_pcfg_glbcpectr_cpevtline3en_Mask                                                           cBit27
#define cAf6_pcfg_glbcpectr_cpevtline3en_Shift                                                              27
#define cAf6_pcfg_glbcpectr_cpevtline3en_MaxVal                                                            0x1
#define cAf6_pcfg_glbcpectr_cpevtline3en_MinVal                                                            0x0
#define cAf6_pcfg_glbcpectr_cpevtline3en_RstVal                                                            0x1

/*--------------------------------------
BitField Name: cpevtline2en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 2
BitField Bits: [26]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline2en_Bit_Start                                                          26
#define cAf6_pcfg_glbcpectr_cpevtline2en_Bit_End                                                            26
#define cAf6_pcfg_glbcpectr_cpevtline2en_Mask                                                           cBit26
#define cAf6_pcfg_glbcpectr_cpevtline2en_Shift                                                              26
#define cAf6_pcfg_glbcpectr_cpevtline2en_MaxVal                                                            0x1
#define cAf6_pcfg_glbcpectr_cpevtline2en_MinVal                                                            0x0
#define cAf6_pcfg_glbcpectr_cpevtline2en_RstVal                                                            0x1

/*--------------------------------------
BitField Name: cpevtline1en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 1
BitField Bits: [25]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline1en_Bit_Start                                                          25
#define cAf6_pcfg_glbcpectr_cpevtline1en_Bit_End                                                            25
#define cAf6_pcfg_glbcpectr_cpevtline1en_Mask                                                           cBit25
#define cAf6_pcfg_glbcpectr_cpevtline1en_Shift                                                              25
#define cAf6_pcfg_glbcpectr_cpevtline1en_MaxVal                                                            0x1
#define cAf6_pcfg_glbcpectr_cpevtline1en_MinVal                                                            0x0
#define cAf6_pcfg_glbcpectr_cpevtline1en_RstVal                                                            0x1

/*--------------------------------------
BitField Name: cpevtline0en
BitField Type: RW
BitField Desc: Enable recevive bytes for VTs Line 0
BitField Bits: [24]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpevtline0en_Bit_Start                                                          24
#define cAf6_pcfg_glbcpectr_cpevtline0en_Bit_End                                                            24
#define cAf6_pcfg_glbcpectr_cpevtline0en_Mask                                                           cBit24
#define cAf6_pcfg_glbcpectr_cpevtline0en_Shift                                                              24
#define cAf6_pcfg_glbcpectr_cpevtline0en_MaxVal                                                            0x1
#define cAf6_pcfg_glbcpectr_cpevtline0en_MinVal                                                            0x0
#define cAf6_pcfg_glbcpectr_cpevtline0en_RstVal                                                            0x1

/*--------------------------------------
BitField Name: cpestsline7en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 7
BitField Bits: [23]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline7en_Bit_Start                                                         23
#define cAf6_pcfg_glbcpectr_cpestsline7en_Bit_End                                                           23
#define cAf6_pcfg_glbcpectr_cpestsline7en_Mask                                                          cBit23
#define cAf6_pcfg_glbcpectr_cpestsline7en_Shift                                                             23
#define cAf6_pcfg_glbcpectr_cpestsline7en_MaxVal                                                           0x1
#define cAf6_pcfg_glbcpectr_cpestsline7en_MinVal                                                           0x0
#define cAf6_pcfg_glbcpectr_cpestsline7en_RstVal                                                           0x0

/*--------------------------------------
BitField Name: cpestsline6en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 6
BitField Bits: [22]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline6en_Bit_Start                                                         22
#define cAf6_pcfg_glbcpectr_cpestsline6en_Bit_End                                                           22
#define cAf6_pcfg_glbcpectr_cpestsline6en_Mask                                                          cBit22
#define cAf6_pcfg_glbcpectr_cpestsline6en_Shift                                                             22
#define cAf6_pcfg_glbcpectr_cpestsline6en_MaxVal                                                           0x1
#define cAf6_pcfg_glbcpectr_cpestsline6en_MinVal                                                           0x0
#define cAf6_pcfg_glbcpectr_cpestsline6en_RstVal                                                           0x0

/*--------------------------------------
BitField Name: cpestsline5en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 5
BitField Bits: [21]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline5en_Bit_Start                                                         21
#define cAf6_pcfg_glbcpectr_cpestsline5en_Bit_End                                                           21
#define cAf6_pcfg_glbcpectr_cpestsline5en_Mask                                                          cBit21
#define cAf6_pcfg_glbcpectr_cpestsline5en_Shift                                                             21
#define cAf6_pcfg_glbcpectr_cpestsline5en_MaxVal                                                           0x1
#define cAf6_pcfg_glbcpectr_cpestsline5en_MinVal                                                           0x0
#define cAf6_pcfg_glbcpectr_cpestsline5en_RstVal                                                           0x0

/*--------------------------------------
BitField Name: cpestsline4en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 4
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline4en_Bit_Start                                                         20
#define cAf6_pcfg_glbcpectr_cpestsline4en_Bit_End                                                           20
#define cAf6_pcfg_glbcpectr_cpestsline4en_Mask                                                          cBit20
#define cAf6_pcfg_glbcpectr_cpestsline4en_Shift                                                             20
#define cAf6_pcfg_glbcpectr_cpestsline4en_MaxVal                                                           0x1
#define cAf6_pcfg_glbcpectr_cpestsline4en_MinVal                                                           0x0
#define cAf6_pcfg_glbcpectr_cpestsline4en_RstVal                                                           0x0

/*--------------------------------------
BitField Name: cpestsline3en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 3
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline3en_Bit_Start                                                         19
#define cAf6_pcfg_glbcpectr_cpestsline3en_Bit_End                                                           19
#define cAf6_pcfg_glbcpectr_cpestsline3en_Mask                                                          cBit19
#define cAf6_pcfg_glbcpectr_cpestsline3en_Shift                                                             19
#define cAf6_pcfg_glbcpectr_cpestsline3en_MaxVal                                                           0x1
#define cAf6_pcfg_glbcpectr_cpestsline3en_MinVal                                                           0x0
#define cAf6_pcfg_glbcpectr_cpestsline3en_RstVal                                                           0x1

/*--------------------------------------
BitField Name: cpestsline2en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 2
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline2en_Bit_Start                                                         18
#define cAf6_pcfg_glbcpectr_cpestsline2en_Bit_End                                                           18
#define cAf6_pcfg_glbcpectr_cpestsline2en_Mask                                                          cBit18
#define cAf6_pcfg_glbcpectr_cpestsline2en_Shift                                                             18
#define cAf6_pcfg_glbcpectr_cpestsline2en_MaxVal                                                           0x1
#define cAf6_pcfg_glbcpectr_cpestsline2en_MinVal                                                           0x0
#define cAf6_pcfg_glbcpectr_cpestsline2en_RstVal                                                           0x1

/*--------------------------------------
BitField Name: cpestsline1en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 1
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline1en_Bit_Start                                                         17
#define cAf6_pcfg_glbcpectr_cpestsline1en_Bit_End                                                           17
#define cAf6_pcfg_glbcpectr_cpestsline1en_Mask                                                          cBit17
#define cAf6_pcfg_glbcpectr_cpestsline1en_Shift                                                             17
#define cAf6_pcfg_glbcpectr_cpestsline1en_MaxVal                                                           0x1
#define cAf6_pcfg_glbcpectr_cpestsline1en_MinVal                                                           0x0
#define cAf6_pcfg_glbcpectr_cpestsline1en_RstVal                                                           0x1

/*--------------------------------------
BitField Name: cpestsline0en
BitField Type: RW
BitField Desc: Enable recevive bytes for STSs Line 0
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pcfg_glbcpectr_cpestsline0en_Bit_Start                                                         16
#define cAf6_pcfg_glbcpectr_cpestsline0en_Bit_End                                                           16
#define cAf6_pcfg_glbcpectr_cpestsline0en_Mask                                                          cBit16
#define cAf6_pcfg_glbcpectr_cpestsline0en_Shift                                                             16
#define cAf6_pcfg_glbcpectr_cpestsline0en_MaxVal                                                           0x1
#define cAf6_pcfg_glbcpectr_cpestsline0en_MinVal                                                           0x0
#define cAf6_pcfg_glbcpectr_cpestsline0en_RstVal                                                           0x1


/*------------------------------------------------------------------------------
Reg Name   : POH CPE STS/TU3 Control Register
Reg Addr   : 0x02_A000
Reg Formula: 0x02_A000 + $sliceid*96 + $stsid*2 + $tu3en
    Where  :
           + $sliceid(0-4): Slice Identification, Slice 4 for Line
           + $stsid(0-47): STS Identification
           + $tu3en(0-1): Tu3enable, 0: STS, 1:Tu3
Reg Desc   :
This register is used to configure the POH Hi-order Path Monitoring.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohcpestsctr_Base                                                                     0x02A000
#define cAf6Reg_pohcpestsctr(sliceid, stsid, tu3en)                   (0x02A000+(sliceid)*96+(stsid)*2+(tu3en))
#define cAf6Reg_pohcpestsctr_WidthVal                                                                       32
#define cAf6Reg_pohcpestsctr_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: MonEnb
BitField Type: RW
BitField Desc: Alarm monitor enable
BitField Bits: [20]
--------------------------------------*/
#define cAf6_pohcpestsctr_MonEnb_Bit_Start                                                                  20
#define cAf6_pohcpestsctr_MonEnb_Bit_End                                                                    20
#define cAf6_pohcpestsctr_MonEnb_Mask                                                                   cBit20
#define cAf6_pohcpestsctr_MonEnb_Shift                                                                      20
#define cAf6_pohcpestsctr_MonEnb_MaxVal                                                                    0x1
#define cAf6_pohcpestsctr_MonEnb_MinVal                                                                    0x0
#define cAf6_pohcpestsctr_MonEnb_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: PlmEnb
BitField Type: RW
BitField Desc: PLM enable
BitField Bits: [19]
--------------------------------------*/
#define cAf6_pohcpestsctr_PlmEnb_Bit_Start                                                                  19
#define cAf6_pohcpestsctr_PlmEnb_Bit_End                                                                    19
#define cAf6_pohcpestsctr_PlmEnb_Mask                                                                   cBit19
#define cAf6_pohcpestsctr_PlmEnb_Shift                                                                      19
#define cAf6_pohcpestsctr_PlmEnb_MaxVal                                                                    0x1
#define cAf6_pohcpestsctr_PlmEnb_MinVal                                                                    0x0
#define cAf6_pohcpestsctr_PlmEnb_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: VcaisDstren
BitField Type: RW
BitField Desc: VcaisDstren
BitField Bits: [18]
--------------------------------------*/
#define cAf6_pohcpestsctr_VcaisDstren_Bit_Start                                                             18
#define cAf6_pohcpestsctr_VcaisDstren_Bit_End                                                               18
#define cAf6_pohcpestsctr_VcaisDstren_Mask                                                              cBit18
#define cAf6_pohcpestsctr_VcaisDstren_Shift                                                                 18
#define cAf6_pohcpestsctr_VcaisDstren_MaxVal                                                               0x1
#define cAf6_pohcpestsctr_VcaisDstren_MinVal                                                               0x0
#define cAf6_pohcpestsctr_VcaisDstren_RstVal                                                               0x0

/*--------------------------------------
BitField Name: PlmDstren
BitField Type: RW
BitField Desc: PlmDstren
BitField Bits: [17]
--------------------------------------*/
#define cAf6_pohcpestsctr_PlmDstren_Bit_Start                                                               17
#define cAf6_pohcpestsctr_PlmDstren_Bit_End                                                                 17
#define cAf6_pohcpestsctr_PlmDstren_Mask                                                                cBit17
#define cAf6_pohcpestsctr_PlmDstren_Shift                                                                   17
#define cAf6_pohcpestsctr_PlmDstren_MaxVal                                                                 0x1
#define cAf6_pohcpestsctr_PlmDstren_MinVal                                                                 0x0
#define cAf6_pohcpestsctr_PlmDstren_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: UneqDstren
BitField Type: RW
BitField Desc: UneqDstren
BitField Bits: [16]
--------------------------------------*/
#define cAf6_pohcpestsctr_UneqDstren_Bit_Start                                                              16
#define cAf6_pohcpestsctr_UneqDstren_Bit_End                                                                16
#define cAf6_pohcpestsctr_UneqDstren_Mask                                                               cBit16
#define cAf6_pohcpestsctr_UneqDstren_Shift                                                                  16
#define cAf6_pohcpestsctr_UneqDstren_MaxVal                                                                0x1
#define cAf6_pohcpestsctr_UneqDstren_MinVal                                                                0x0
#define cAf6_pohcpestsctr_UneqDstren_RstVal                                                                0x0

/*--------------------------------------
BitField Name: TimDstren
BitField Type: RW
BitField Desc: TimDstren
BitField Bits: [15]
--------------------------------------*/
#define cAf6_pohcpestsctr_TimDstren_Bit_Start                                                               15
#define cAf6_pohcpestsctr_TimDstren_Bit_End                                                                 15
#define cAf6_pohcpestsctr_TimDstren_Mask                                                                cBit15
#define cAf6_pohcpestsctr_TimDstren_Shift                                                                   15
#define cAf6_pohcpestsctr_TimDstren_MaxVal                                                                 0x1
#define cAf6_pohcpestsctr_TimDstren_MinVal                                                                 0x0
#define cAf6_pohcpestsctr_TimDstren_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: Sdhmode
BitField Type: RW
BitField Desc: SDH mode
BitField Bits: [14]
--------------------------------------*/
#define cAf6_pohcpestsctr_Sdhmode_Bit_Start                                                                 14
#define cAf6_pohcpestsctr_Sdhmode_Bit_End                                                                   14
#define cAf6_pohcpestsctr_Sdhmode_Mask                                                                  cBit14
#define cAf6_pohcpestsctr_Sdhmode_Shift                                                                     14
#define cAf6_pohcpestsctr_Sdhmode_MaxVal                                                                   0x1
#define cAf6_pohcpestsctr_Sdhmode_MinVal                                                                   0x0
#define cAf6_pohcpestsctr_Sdhmode_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: Blkmden
BitField Type: RW
BitField Desc: Block mode BIP
BitField Bits: [13]
--------------------------------------*/
#define cAf6_pohcpestsctr_Blkmden_Bit_Start                                                                 13
#define cAf6_pohcpestsctr_Blkmden_Bit_End                                                                   13
#define cAf6_pohcpestsctr_Blkmden_Mask                                                                  cBit13
#define cAf6_pohcpestsctr_Blkmden_Shift                                                                     13
#define cAf6_pohcpestsctr_Blkmden_MaxVal                                                                   0x1
#define cAf6_pohcpestsctr_Blkmden_MinVal                                                                   0x0
#define cAf6_pohcpestsctr_Blkmden_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ERDIenb
BitField Type: RW
BitField Desc: Enable E-RDI
BitField Bits: [12]
--------------------------------------*/
#define cAf6_pohcpestsctr_ERDIenb_Bit_Start                                                                 12
#define cAf6_pohcpestsctr_ERDIenb_Bit_End                                                                   12
#define cAf6_pohcpestsctr_ERDIenb_Mask                                                                  cBit12
#define cAf6_pohcpestsctr_ERDIenb_Shift                                                                     12
#define cAf6_pohcpestsctr_ERDIenb_MaxVal                                                                   0x1
#define cAf6_pohcpestsctr_ERDIenb_MinVal                                                                   0x0
#define cAf6_pohcpestsctr_ERDIenb_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: PslExp
BitField Type: RW
BitField Desc: C2 Expected Path Signal Lable Value
BitField Bits: [11:4]
--------------------------------------*/
#define cAf6_pohcpestsctr_PslExp_Bit_Start                                                                   4
#define cAf6_pohcpestsctr_PslExp_Bit_End                                                                    11
#define cAf6_pohcpestsctr_PslExp_Mask                                                                 cBit11_4
#define cAf6_pohcpestsctr_PslExp_Shift                                                                       4
#define cAf6_pohcpestsctr_PslExp_MaxVal                                                                   0xff
#define cAf6_pohcpestsctr_PslExp_MinVal                                                                    0x0
#define cAf6_pohcpestsctr_PslExp_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: TimEnb
BitField Type: RW
BitField Desc: Enable Monitor TIM
BitField Bits: [3]
--------------------------------------*/
#define cAf6_pohcpestsctr_TimEnb_Bit_Start                                                                   3
#define cAf6_pohcpestsctr_TimEnb_Bit_End                                                                     3
#define cAf6_pohcpestsctr_TimEnb_Mask                                                                    cBit3
#define cAf6_pohcpestsctr_TimEnb_Shift                                                                       3
#define cAf6_pohcpestsctr_TimEnb_MaxVal                                                                    0x1
#define cAf6_pohcpestsctr_TimEnb_MinVal                                                                    0x0
#define cAf6_pohcpestsctr_TimEnb_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: Reiblkmden
BitField Type: RW
BitField Desc: Block mode REI
BitField Bits: [2]
--------------------------------------*/
#define cAf6_pohcpestsctr_Reiblkmden_Bit_Start                                                               2
#define cAf6_pohcpestsctr_Reiblkmden_Bit_End                                                                 2
#define cAf6_pohcpestsctr_Reiblkmden_Mask                                                                cBit2
#define cAf6_pohcpestsctr_Reiblkmden_Shift                                                                   2
#define cAf6_pohcpestsctr_Reiblkmden_MaxVal                                                                0x1
#define cAf6_pohcpestsctr_Reiblkmden_MinVal                                                                0x0
#define cAf6_pohcpestsctr_Reiblkmden_RstVal                                                                0x0

/*--------------------------------------
BitField Name: J1mode
BitField Type: RW
BitField Desc: 0: 1Byte 1:16Byte 2:64byte 3:Floating
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_pohcpestsctr_J1mode_Bit_Start                                                                   0
#define cAf6_pohcpestsctr_J1mode_Bit_End                                                                     1
#define cAf6_pohcpestsctr_J1mode_Mask                                                                  cBit1_0
#define cAf6_pohcpestsctr_J1mode_Shift                                                                       0
#define cAf6_pohcpestsctr_J1mode_MaxVal                                                                    0x3
#define cAf6_pohcpestsctr_J1mode_MinVal                                                                    0x0
#define cAf6_pohcpestsctr_J1mode_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 STS Expected Message buffer
Reg Addr   : 0x0B_0000
Reg Formula: 0x0B_0000 + $sliceid*384 + $stsid*8 + $msgid
    Where  :
           + $sliceid(0-4): Slice Identification, Slice 4 for Line
           + $stsid(0-47): STS Identification
           + $msgid(0-7): Message ID
Reg Desc   :
The J1 Expected Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstsexp_Base                                                                     0x0B0000
#define cAf6Reg_pohmsgstsexp(sliceid, stsid, msgid)                   (0x0B0000+(sliceid)*384+(stsid)*8+(msgid))
#define cAf6Reg_pohmsgstsexp_WidthVal                                                                       64
#define cAf6Reg_pohmsgstsexp_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: J1ExpMsg
BitField Type: RW
BitField Desc: J1 Expected Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstsexp_J1ExpMsg_Bit_Start                                                                 0
#define cAf6_pohmsgstsexp_J1ExpMsg_Bit_End                                                                  63
#define cAf6_pohmsgstsexp_J1ExpMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstsexp_J1ExpMsg_Shift_01                                                                  0
#define cAf6_pohmsgstsexp_J1ExpMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstsexp_J1ExpMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH CPE J1 STS Current Message buffer
Reg Addr   : 0x0B_1000
Reg Formula: 0x0B_1000 + $sliceid*384 + $stsid*8 + $msgid
    Where  :
           + $sliceid(0-4): Slice Identification, Slice 4 for Line
           + $stsid(0-47): STS Identification
           + $msgid(0-7): Message ID
Reg Desc   :
The J1 Current Message Buffer.

------------------------------------------------------------------------------*/
#define cAf6Reg_pohmsgstscur_Base                                                                     0x0B1000
#define cAf6Reg_pohmsgstscur(sliceid, stsid, msgid)                   (0x0B1000+(sliceid)*384+(stsid)*8+(msgid))
#define cAf6Reg_pohmsgstscur_WidthVal                                                                       64
#define cAf6Reg_pohmsgstscur_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: J1CurMsg
BitField Type: RW
BitField Desc: J1 Current Message
BitField Bits: [63:0]
--------------------------------------*/
#define cAf6_pohmsgstscur_J1CurMsg_Bit_Start                                                                 0
#define cAf6_pohmsgstscur_J1CurMsg_Bit_End                                                                  63
#define cAf6_pohmsgstscur_J1CurMsg_Mask_01                                                            cBit31_0
#define cAf6_pohmsgstscur_J1CurMsg_Shift_01                                                                  0
#define cAf6_pohmsgstscur_J1CurMsg_Mask_02                                                            cBit31_0
#define cAf6_pohmsgstscur_J1CurMsg_Shift_02                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Control STS/TU3
Reg Addr   : 0x06_2007
Reg Formula: 0x06_2007 + $STS*128 + $OCID*8
    Where  :
           + $STS(0-47)  : STS
           + $OCID(0-7)  : Line ID, Line 4-7 channel 2 for DE3, Line 4-7 channel 1 for Sonet Line
Reg Desc   :
This register is used to enable and set threshold SD SF .

------------------------------------------------------------------------------*/
#define cAf6Reg_imemrwpctrl2_Base                                                                     0x062007
#define cAf6Reg_imemrwpctrl2(STS, OCID)                                          (0x062007+(STS)*128+(OCID)*8)
#define cAf6Reg_imemrwpctrl2_WidthVal                                                                       64
#define cAf6Reg_imemrwpctrl2_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: tcatrsh2
BitField Type: RW
BitField Desc: TCA threshold raise channel 2
BitField Bits: [39:37]
--------------------------------------*/
#define cAf6_imemrwpctrl2_tcatrsh2_Bit_Start                                                                37
#define cAf6_imemrwpctrl2_tcatrsh2_Bit_End                                                                  39
#define cAf6_imemrwpctrl2_tcatrsh2_Mask                                                                cBit7_5
#define cAf6_imemrwpctrl2_tcatrsh2_Shift                                                                     5
#define cAf6_imemrwpctrl2_tcatrsh2_MaxVal                                                                  0x0
#define cAf6_imemrwpctrl2_tcatrsh2_MinVal                                                                  0x0
#define cAf6_imemrwpctrl2_tcatrsh2_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: tcatrsh1
BitField Type: RW
BitField Desc: TCA threshold raise channel 1
BitField Bits: [36:34]
--------------------------------------*/
#define cAf6_imemrwpctrl2_tcatrsh1_Bit_Start                                                                34
#define cAf6_imemrwpctrl2_tcatrsh1_Bit_End                                                                  36
#define cAf6_imemrwpctrl2_tcatrsh1_Mask                                                                cBit4_2
#define cAf6_imemrwpctrl2_tcatrsh1_Shift                                                                     2
#define cAf6_imemrwpctrl2_tcatrsh1_MaxVal                                                                  0x0
#define cAf6_imemrwpctrl2_tcatrsh1_MinVal                                                                  0x0
#define cAf6_imemrwpctrl2_tcatrsh1_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: rate2
BitField Type: RW
BitField Desc: STS Rate 0-63 type
BitField Bits: [27:21]
--------------------------------------*/
#define cAf6_imemrwpctrl2_rate2_Bit_Start                                                                   21
#define cAf6_imemrwpctrl2_rate2_Bit_End                                                                     27
#define cAf6_imemrwpctrl2_rate2_Mask                                                                 cBit27_21
#define cAf6_imemrwpctrl2_rate2_Shift                                                                       21
#define cAf6_imemrwpctrl2_rate2_MaxVal                                                                    0x7f
#define cAf6_imemrwpctrl2_rate2_MinVal                                                                     0x0
#define cAf6_imemrwpctrl2_rate2_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: sftrsh2
BitField Type: RW
BitField Desc: SF threshold raise channel 1
BitField Bits: [20:18]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sftrsh2_Bit_Start                                                                 18
#define cAf6_imemrwpctrl2_sftrsh2_Bit_End                                                                   20
#define cAf6_imemrwpctrl2_sftrsh2_Mask                                                               cBit20_18
#define cAf6_imemrwpctrl2_sftrsh2_Shift                                                                     18
#define cAf6_imemrwpctrl2_sftrsh2_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl2_sftrsh2_MinVal                                                                   0x0
#define cAf6_imemrwpctrl2_sftrsh2_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: sdtrsh2
BitField Type: RW
BitField Desc: SD threshold raise channel 1
BitField Bits: [17:15]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sdtrsh2_Bit_Start                                                                 15
#define cAf6_imemrwpctrl2_sdtrsh2_Bit_End                                                                   17
#define cAf6_imemrwpctrl2_sdtrsh2_Mask                                                               cBit17_15
#define cAf6_imemrwpctrl2_sdtrsh2_Shift                                                                     15
#define cAf6_imemrwpctrl2_sdtrsh2_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl2_sdtrsh2_MinVal                                                                   0x0
#define cAf6_imemrwpctrl2_sdtrsh2_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ena2
BitField Type: RW
BitField Desc: Enable channel 1
BitField Bits: [14]
--------------------------------------*/
#define cAf6_imemrwpctrl2_ena2_Bit_Start                                                                    14
#define cAf6_imemrwpctrl2_ena2_Bit_End                                                                      14
#define cAf6_imemrwpctrl2_ena2_Mask                                                                     cBit14
#define cAf6_imemrwpctrl2_ena2_Shift                                                                        14
#define cAf6_imemrwpctrl2_ena2_MaxVal                                                                      0x1
#define cAf6_imemrwpctrl2_ena2_MinVal                                                                      0x0
#define cAf6_imemrwpctrl2_ena2_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: rate1
BitField Type: RW
BitField Desc: STS Rate 0-63 type
BitField Bits: [13:7]
--------------------------------------*/
#define cAf6_imemrwpctrl2_rate1_Bit_Start                                                                    7
#define cAf6_imemrwpctrl2_rate1_Bit_End                                                                     13
#define cAf6_imemrwpctrl2_rate1_Mask                                                                  cBit13_7
#define cAf6_imemrwpctrl2_rate1_Shift                                                                        7
#define cAf6_imemrwpctrl2_rate1_MaxVal                                                                    0x7f
#define cAf6_imemrwpctrl2_rate1_MinVal                                                                     0x0
#define cAf6_imemrwpctrl2_rate1_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: sftrsh1
BitField Type: RW
BitField Desc: SF threshold raise channel 0
BitField Bits: [6:4]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sftrsh1_Bit_Start                                                                  4
#define cAf6_imemrwpctrl2_sftrsh1_Bit_End                                                                    6
#define cAf6_imemrwpctrl2_sftrsh1_Mask                                                                 cBit6_4
#define cAf6_imemrwpctrl2_sftrsh1_Shift                                                                      4
#define cAf6_imemrwpctrl2_sftrsh1_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl2_sftrsh1_MinVal                                                                   0x0
#define cAf6_imemrwpctrl2_sftrsh1_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: sdtrsh1
BitField Type: RW
BitField Desc: SD threshold raise channel 0
BitField Bits: [3:1]
--------------------------------------*/
#define cAf6_imemrwpctrl2_sdtrsh1_Bit_Start                                                                  1
#define cAf6_imemrwpctrl2_sdtrsh1_Bit_End                                                                    3
#define cAf6_imemrwpctrl2_sdtrsh1_Mask                                                                 cBit3_1
#define cAf6_imemrwpctrl2_sdtrsh1_Shift                                                                      1
#define cAf6_imemrwpctrl2_sdtrsh1_MaxVal                                                                   0x7
#define cAf6_imemrwpctrl2_sdtrsh1_MinVal                                                                   0x0
#define cAf6_imemrwpctrl2_sdtrsh1_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: ena1
BitField Type: RW
BitField Desc: Enable channel 0
BitField Bits: [0]
--------------------------------------*/
#define cAf6_imemrwpctrl2_ena1_Bit_Start                                                                     0
#define cAf6_imemrwpctrl2_ena1_Bit_End                                                                       0
#define cAf6_imemrwpctrl2_ena1_Mask                                                                      cBit0
#define cAf6_imemrwpctrl2_ena1_Shift                                                                         0
#define cAf6_imemrwpctrl2_ena1_MaxVal                                                                      0x1
#define cAf6_imemrwpctrl2_ena1_MinVal                                                                      0x0
#define cAf6_imemrwpctrl2_ena1_RstVal                                                                      0x0


/*------------------------------------------------------------------------------
Reg Name   : POH BER Report STS/TU3
Reg Addr   : 0x06_C000
Reg Formula: 0x06_C000 + $STS*16 + $OCID*2 + $TU3TYPE
    Where  :
           + $STS(0-47)  : STS
           + $OCID(0-7)     : Line ID
           + $TU3TYPE(0-1)  : Type TU3:1, STS:0
Reg Desc   :
This register is used to get current BER rate . BER DE3 used with OCID 4-7, TU3TYPE = 1, BER Line used with OCID 4-7, TU3TYPE = 0.

------------------------------------------------------------------------------*/
#define cAf6Reg_ramberrateststu3_Base                                                                 0x06C000
#define cAf6Reg_ramberrateststu3(STS, OCID, TU3TYPE)                    (0x06C000+(STS)*16+(OCID)*2+(TU3TYPE))
#define cAf6Reg_ramberrateststu3_WidthVal                                                                   32
#define cAf6Reg_ramberrateststu3_WriteMask                                                                 0x0

/*--------------------------------------
BitField Name: hwsta
BitField Type: RW
BitField Desc: Hardware status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_ramberrateststu3_hwsta_Bit_Start                                                                3
#define cAf6_ramberrateststu3_hwsta_Bit_End                                                                  3
#define cAf6_ramberrateststu3_hwsta_Mask                                                                 cBit3
#define cAf6_ramberrateststu3_hwsta_Shift                                                                    3
#define cAf6_ramberrateststu3_hwsta_MaxVal                                                                 0x1
#define cAf6_ramberrateststu3_hwsta_MinVal                                                                 0x0
#define cAf6_ramberrateststu3_hwsta_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: rate
BitField Type: RW
BitField Desc: BER rate
BitField Bits: [2:0]
--------------------------------------*/
#define cAf6_ramberrateststu3_rate_Bit_Start                                                                 0
#define cAf6_ramberrateststu3_rate_Bit_End                                                                   2
#define cAf6_ramberrateststu3_rate_Mask                                                                cBit2_0
#define cAf6_ramberrateststu3_rate_Shift                                                                     0
#define cAf6_ramberrateststu3_rate_MaxVal                                                                  0x7
#define cAf6_ramberrateststu3_rate_MinVal                                                                  0x0
#define cAf6_ramberrateststu3_rate_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Counter Report STS
Reg Addr   : 0x0C_A000
Reg Formula: 0x0C_A000 + $STS + $OCID*48
    Where  :
           + $STS(0-47)  : STS
           + $OCID(0-4)     : Line ID, Line ID 4 for B2 counter
Reg Desc   :
This register is used to get POH Counter, Rx SDH pointer increase, decrease counter.

------------------------------------------------------------------------------*/
#define cAf6Reg_ipm_cnthi_Base                                                                        0x0CA000
#define cAf6Reg_ipm_cnthi(STS, OCID)                                                (0x0CA000+(STS)+(OCID)*48)
#define cAf6Reg_ipm_cnthi_WidthVal                                                                          32
#define cAf6Reg_ipm_cnthi_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: reicnt
BitField Type: RC
BitField Desc: REI counter
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_ipm_cnthi_reicnt_Bit_Start                                                                     16
#define cAf6_ipm_cnthi_reicnt_Bit_End                                                                       31
#define cAf6_ipm_cnthi_reicnt_Mask                                                                   cBit31_16
#define cAf6_ipm_cnthi_reicnt_Shift                                                                         16
#define cAf6_ipm_cnthi_reicnt_MaxVal                                                                    0xffff
#define cAf6_ipm_cnthi_reicnt_MinVal                                                                       0x0
#define cAf6_ipm_cnthi_reicnt_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: bipcnt
BitField Type: RC
BitField Desc: BIP counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_ipm_cnthi_bipcnt_Bit_Start                                                                      0
#define cAf6_ipm_cnthi_bipcnt_Bit_End                                                                       15
#define cAf6_ipm_cnthi_bipcnt_Mask                                                                    cBit15_0
#define cAf6_ipm_cnthi_bipcnt_Shift                                                                          0
#define cAf6_ipm_cnthi_bipcnt_MaxVal                                                                    0xffff
#define cAf6_ipm_cnthi_bipcnt_MinVal                                                                       0x0
#define cAf6_ipm_cnthi_bipcnt_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Mask Report STS
Reg Addr   : 0x0D_0000
Reg Formula: 0x0D_0000 + $STS + $OCID*128
    Where  :
           + $STS(0-23)  : STS
           + $OCID(0-8) : Line ID, ID 0,2,4,6,8 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   :
This register is used to get POH alarm mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_mskhi_Base                                                                        0x0D0000
#define cAf6Reg_alm_mskhi(STS, OCID)                                               (0x0D0000+(STS)+(OCID)*128)
#define cAf6Reg_alm_mskhi_WidthVal                                                                          32
#define cAf6Reg_alm_mskhi_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: jnstachgmsk
BitField Type: RW
BitField Desc: jnstachg mask
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_mskhi_jnstachgmsk_Bit_Start                                                                13
#define cAf6_alm_mskhi_jnstachgmsk_Bit_End                                                                  13
#define cAf6_alm_mskhi_jnstachgmsk_Mask                                                                 cBit13
#define cAf6_alm_mskhi_jnstachgmsk_Shift                                                                    13
#define cAf6_alm_mskhi_jnstachgmsk_MaxVal                                                                  0x1
#define cAf6_alm_mskhi_jnstachgmsk_MinVal                                                                  0x0
#define cAf6_alm_mskhi_jnstachgmsk_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: pslstachgmsk
BitField Type: RW
BitField Desc: pslstachg mask
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_mskhi_pslstachgmsk_Bit_Start                                                               12
#define cAf6_alm_mskhi_pslstachgmsk_Bit_End                                                                 12
#define cAf6_alm_mskhi_pslstachgmsk_Mask                                                                cBit12
#define cAf6_alm_mskhi_pslstachgmsk_Shift                                                                   12
#define cAf6_alm_mskhi_pslstachgmsk_MaxVal                                                                 0x1
#define cAf6_alm_mskhi_pslstachgmsk_MinVal                                                                 0x0
#define cAf6_alm_mskhi_pslstachgmsk_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: bersdmsk
BitField Type: RW
BitField Desc: bersd mask
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_mskhi_bersdmsk_Bit_Start                                                                   11
#define cAf6_alm_mskhi_bersdmsk_Bit_End                                                                     11
#define cAf6_alm_mskhi_bersdmsk_Mask                                                                    cBit11
#define cAf6_alm_mskhi_bersdmsk_Shift                                                                       11
#define cAf6_alm_mskhi_bersdmsk_MaxVal                                                                     0x1
#define cAf6_alm_mskhi_bersdmsk_MinVal                                                                     0x0
#define cAf6_alm_mskhi_bersdmsk_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: bersfmsk
BitField Type: RW
BitField Desc: bersf  mask
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_mskhi_bersfmsk_Bit_Start                                                                   10
#define cAf6_alm_mskhi_bersfmsk_Bit_End                                                                     10
#define cAf6_alm_mskhi_bersfmsk_Mask                                                                    cBit10
#define cAf6_alm_mskhi_bersfmsk_Shift                                                                       10
#define cAf6_alm_mskhi_bersfmsk_MaxVal                                                                     0x1
#define cAf6_alm_mskhi_bersfmsk_MinVal                                                                     0x0
#define cAf6_alm_mskhi_bersfmsk_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: erdimsk
BitField Type: RW
BitField Desc: erdis/rdi mask
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_mskhi_erdimsk_Bit_Start                                                                     9
#define cAf6_alm_mskhi_erdimsk_Bit_End                                                                       9
#define cAf6_alm_mskhi_erdimsk_Mask                                                                      cBit9
#define cAf6_alm_mskhi_erdimsk_Shift                                                                         9
#define cAf6_alm_mskhi_erdimsk_MaxVal                                                                      0x1
#define cAf6_alm_mskhi_erdimsk_MinVal                                                                      0x0
#define cAf6_alm_mskhi_erdimsk_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: bertcamsk
BitField Type: RW
BitField Desc: bertca mask
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_mskhi_bertcamsk_Bit_Start                                                                   8
#define cAf6_alm_mskhi_bertcamsk_Bit_End                                                                     8
#define cAf6_alm_mskhi_bertcamsk_Mask                                                                    cBit8
#define cAf6_alm_mskhi_bertcamsk_Shift                                                                       8
#define cAf6_alm_mskhi_bertcamsk_MaxVal                                                                    0x1
#define cAf6_alm_mskhi_bertcamsk_MinVal                                                                    0x0
#define cAf6_alm_mskhi_bertcamsk_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: erdicmsk
BitField Type: RW
BitField Desc: erdicmsk  mask
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_mskhi_erdicmsk_Bit_Start                                                                    7
#define cAf6_alm_mskhi_erdicmsk_Bit_End                                                                      7
#define cAf6_alm_mskhi_erdicmsk_Mask                                                                     cBit7
#define cAf6_alm_mskhi_erdicmsk_Shift                                                                        7
#define cAf6_alm_mskhi_erdicmsk_MaxVal                                                                     0x1
#define cAf6_alm_mskhi_erdicmsk_MinVal                                                                     0x0
#define cAf6_alm_mskhi_erdicmsk_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: erdipmsk
BitField Type: RW
BitField Desc: erdipmsk  mask
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_mskhi_erdipmsk_Bit_Start                                                                    6
#define cAf6_alm_mskhi_erdipmsk_Bit_End                                                                      6
#define cAf6_alm_mskhi_erdipmsk_Mask                                                                     cBit6
#define cAf6_alm_mskhi_erdipmsk_Shift                                                                        6
#define cAf6_alm_mskhi_erdipmsk_MaxVal                                                                     0x1
#define cAf6_alm_mskhi_erdipmsk_MinVal                                                                     0x0
#define cAf6_alm_mskhi_erdipmsk_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: rfimsk
BitField Type: RW
BitField Desc: rfi/lom mask
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_mskhi_rfimsk_Bit_Start                                                                      5
#define cAf6_alm_mskhi_rfimsk_Bit_End                                                                        5
#define cAf6_alm_mskhi_rfimsk_Mask                                                                       cBit5
#define cAf6_alm_mskhi_rfimsk_Shift                                                                          5
#define cAf6_alm_mskhi_rfimsk_MaxVal                                                                       0x1
#define cAf6_alm_mskhi_rfimsk_MinVal                                                                       0x0
#define cAf6_alm_mskhi_rfimsk_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: timmsk
BitField Type: RW
BitField Desc: tim mask
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_mskhi_timmsk_Bit_Start                                                                      4
#define cAf6_alm_mskhi_timmsk_Bit_End                                                                        4
#define cAf6_alm_mskhi_timmsk_Mask                                                                       cBit4
#define cAf6_alm_mskhi_timmsk_Shift                                                                          4
#define cAf6_alm_mskhi_timmsk_MaxVal                                                                       0x1
#define cAf6_alm_mskhi_timmsk_MinVal                                                                       0x0
#define cAf6_alm_mskhi_timmsk_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: uneqmsk
BitField Type: RW
BitField Desc: uneq mask
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_mskhi_uneqmsk_Bit_Start                                                                     3
#define cAf6_alm_mskhi_uneqmsk_Bit_End                                                                       3
#define cAf6_alm_mskhi_uneqmsk_Mask                                                                      cBit3
#define cAf6_alm_mskhi_uneqmsk_Shift                                                                         3
#define cAf6_alm_mskhi_uneqmsk_MaxVal                                                                      0x1
#define cAf6_alm_mskhi_uneqmsk_MinVal                                                                      0x0
#define cAf6_alm_mskhi_uneqmsk_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: plmmsk
BitField Type: RW
BitField Desc: plm mask
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_mskhi_plmmsk_Bit_Start                                                                      2
#define cAf6_alm_mskhi_plmmsk_Bit_End                                                                        2
#define cAf6_alm_mskhi_plmmsk_Mask                                                                       cBit2
#define cAf6_alm_mskhi_plmmsk_Shift                                                                          2
#define cAf6_alm_mskhi_plmmsk_MaxVal                                                                       0x1
#define cAf6_alm_mskhi_plmmsk_MinVal                                                                       0x0
#define cAf6_alm_mskhi_plmmsk_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: aismsk
BitField Type: RW
BitField Desc: ais mask
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_mskhi_aismsk_Bit_Start                                                                      1
#define cAf6_alm_mskhi_aismsk_Bit_End                                                                        1
#define cAf6_alm_mskhi_aismsk_Mask                                                                       cBit1
#define cAf6_alm_mskhi_aismsk_Shift                                                                          1
#define cAf6_alm_mskhi_aismsk_MaxVal                                                                       0x1
#define cAf6_alm_mskhi_aismsk_MinVal                                                                       0x0
#define cAf6_alm_mskhi_aismsk_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: lopmsk
BitField Type: RW
BitField Desc: lop mask
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_mskhi_lopmsk_Bit_Start                                                                      0
#define cAf6_alm_mskhi_lopmsk_Bit_End                                                                        0
#define cAf6_alm_mskhi_lopmsk_Mask                                                                       cBit0
#define cAf6_alm_mskhi_lopmsk_Shift                                                                          0
#define cAf6_alm_mskhi_lopmsk_MaxVal                                                                       0x1
#define cAf6_alm_mskhi_lopmsk_MinVal                                                                       0x0
#define cAf6_alm_mskhi_lopmsk_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Alarm Status Report STS
Reg Addr   : 0x0D_0040
Reg Formula: 0x0D_0040 + $STS + $OCID*128
    Where  :
           + $STS(0-23)  : STS
           + $OCID(0-8) : Line ID, ID 0,2,4,6,8 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   :
This register is used to get POH alarm status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_stahi_Base                                                                        0x0D0040
#define cAf6Reg_alm_stahi(STS, OCID)                                               (0x0D0040+(STS)+(OCID)*128)
#define cAf6Reg_alm_stahi_WidthVal                                                                          32
#define cAf6Reg_alm_stahi_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: bersdsta
BitField Type: W1C
BitField Desc: bersd  status
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_stahi_bersdsta_Bit_Start                                                                   11
#define cAf6_alm_stahi_bersdsta_Bit_End                                                                     11
#define cAf6_alm_stahi_bersdsta_Mask                                                                    cBit11
#define cAf6_alm_stahi_bersdsta_Shift                                                                       11
#define cAf6_alm_stahi_bersdsta_MaxVal                                                                     0x1
#define cAf6_alm_stahi_bersdsta_MinVal                                                                     0x0
#define cAf6_alm_stahi_bersdsta_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: bersfsta
BitField Type: W1C
BitField Desc: bersf  status
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_stahi_bersfsta_Bit_Start                                                                   10
#define cAf6_alm_stahi_bersfsta_Bit_End                                                                     10
#define cAf6_alm_stahi_bersfsta_Mask                                                                    cBit10
#define cAf6_alm_stahi_bersfsta_Shift                                                                       10
#define cAf6_alm_stahi_bersfsta_MaxVal                                                                     0x1
#define cAf6_alm_stahi_bersfsta_MinVal                                                                     0x0
#define cAf6_alm_stahi_bersfsta_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: erdista
BitField Type: RO
BitField Desc: erdis/rdi  status
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_stahi_erdista_Bit_Start                                                                     9
#define cAf6_alm_stahi_erdista_Bit_End                                                                       9
#define cAf6_alm_stahi_erdista_Mask                                                                      cBit9
#define cAf6_alm_stahi_erdista_Shift                                                                         9
#define cAf6_alm_stahi_erdista_MaxVal                                                                      0x1
#define cAf6_alm_stahi_erdista_MinVal                                                                      0x0
#define cAf6_alm_stahi_erdista_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: bertcasta
BitField Type: RO
BitField Desc: bertca status
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_stahi_bertcasta_Bit_Start                                                                   8
#define cAf6_alm_stahi_bertcasta_Bit_End                                                                     8
#define cAf6_alm_stahi_bertcasta_Mask                                                                    cBit8
#define cAf6_alm_stahi_bertcasta_Shift                                                                       8
#define cAf6_alm_stahi_bertcasta_MaxVal                                                                    0x1
#define cAf6_alm_stahi_bertcasta_MinVal                                                                    0x0
#define cAf6_alm_stahi_bertcasta_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: erdicsta
BitField Type: RO
BitField Desc: erdic status
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_stahi_erdicsta_Bit_Start                                                                    7
#define cAf6_alm_stahi_erdicsta_Bit_End                                                                      7
#define cAf6_alm_stahi_erdicsta_Mask                                                                     cBit7
#define cAf6_alm_stahi_erdicsta_Shift                                                                        7
#define cAf6_alm_stahi_erdicsta_MaxVal                                                                     0x1
#define cAf6_alm_stahi_erdicsta_MinVal                                                                     0x0
#define cAf6_alm_stahi_erdicsta_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: erdipsta
BitField Type: RO
BitField Desc: erdip stable status
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_stahi_erdipsta_Bit_Start                                                                    6
#define cAf6_alm_stahi_erdipsta_Bit_End                                                                      6
#define cAf6_alm_stahi_erdipsta_Mask                                                                     cBit6
#define cAf6_alm_stahi_erdipsta_Shift                                                                        6
#define cAf6_alm_stahi_erdipsta_MaxVal                                                                     0x1
#define cAf6_alm_stahi_erdipsta_MinVal                                                                     0x0
#define cAf6_alm_stahi_erdipsta_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: rfista
BitField Type: RO
BitField Desc: rfi/lom status
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_stahi_rfista_Bit_Start                                                                      5
#define cAf6_alm_stahi_rfista_Bit_End                                                                        5
#define cAf6_alm_stahi_rfista_Mask                                                                       cBit5
#define cAf6_alm_stahi_rfista_Shift                                                                          5
#define cAf6_alm_stahi_rfista_MaxVal                                                                       0x1
#define cAf6_alm_stahi_rfista_MinVal                                                                       0x0
#define cAf6_alm_stahi_rfista_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: timsta
BitField Type: RO
BitField Desc: tim status
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_stahi_timsta_Bit_Start                                                                      4
#define cAf6_alm_stahi_timsta_Bit_End                                                                        4
#define cAf6_alm_stahi_timsta_Mask                                                                       cBit4
#define cAf6_alm_stahi_timsta_Shift                                                                          4
#define cAf6_alm_stahi_timsta_MaxVal                                                                       0x1
#define cAf6_alm_stahi_timsta_MinVal                                                                       0x0
#define cAf6_alm_stahi_timsta_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: uneqsta
BitField Type: RO
BitField Desc: uneq status
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_stahi_uneqsta_Bit_Start                                                                     3
#define cAf6_alm_stahi_uneqsta_Bit_End                                                                       3
#define cAf6_alm_stahi_uneqsta_Mask                                                                      cBit3
#define cAf6_alm_stahi_uneqsta_Shift                                                                         3
#define cAf6_alm_stahi_uneqsta_MaxVal                                                                      0x1
#define cAf6_alm_stahi_uneqsta_MinVal                                                                      0x0
#define cAf6_alm_stahi_uneqsta_RstVal                                                                      0x0

/*--------------------------------------
BitField Name: plmsta
BitField Type: RO
BitField Desc: plm status
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_stahi_plmsta_Bit_Start                                                                      2
#define cAf6_alm_stahi_plmsta_Bit_End                                                                        2
#define cAf6_alm_stahi_plmsta_Mask                                                                       cBit2
#define cAf6_alm_stahi_plmsta_Shift                                                                          2
#define cAf6_alm_stahi_plmsta_MaxVal                                                                       0x1
#define cAf6_alm_stahi_plmsta_MinVal                                                                       0x0
#define cAf6_alm_stahi_plmsta_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: aissta
BitField Type: RO
BitField Desc: ais status
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_stahi_aissta_Bit_Start                                                                      1
#define cAf6_alm_stahi_aissta_Bit_End                                                                        1
#define cAf6_alm_stahi_aissta_Mask                                                                       cBit1
#define cAf6_alm_stahi_aissta_Shift                                                                          1
#define cAf6_alm_stahi_aissta_MaxVal                                                                       0x1
#define cAf6_alm_stahi_aissta_MinVal                                                                       0x0
#define cAf6_alm_stahi_aissta_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: lopsta
BitField Type: RO
BitField Desc: lop status
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_stahi_lopsta_Bit_Start                                                                      0
#define cAf6_alm_stahi_lopsta_Bit_End                                                                        0
#define cAf6_alm_stahi_lopsta_Mask                                                                       cBit0
#define cAf6_alm_stahi_lopsta_Shift                                                                          0
#define cAf6_alm_stahi_lopsta_MaxVal                                                                       0x1
#define cAf6_alm_stahi_lopsta_MinVal                                                                       0x0
#define cAf6_alm_stahi_lopsta_RstVal                                                                       0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Status Report STS
Reg Addr   : 0x0D_0020
Reg Formula: 0x0D_0020 + $STS + $OCID*128
    Where  :
           + $STS(0-23)  : STS
           + $OCID(0-8) : Line ID, ID 0,2,4,6,8 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   :
This register is used to get POH alarm change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_chghi_Base                                                                        0x0D0020
#define cAf6Reg_alm_chghi(STS, OCID)                                               (0x0D0020+(STS)+(OCID)*128)
#define cAf6Reg_alm_chghi_WidthVal                                                                          32
#define cAf6Reg_alm_chghi_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: jnstachg
BitField Type: RW
BitField Desc: jn message change
BitField Bits: [13]
--------------------------------------*/
#define cAf6_alm_chghi_jnstachg_Bit_Start                                                                   13
#define cAf6_alm_chghi_jnstachg_Bit_End                                                                     13
#define cAf6_alm_chghi_jnstachg_Mask                                                                    cBit13
#define cAf6_alm_chghi_jnstachg_Shift                                                                       13
#define cAf6_alm_chghi_jnstachg_MaxVal                                                                     0x1
#define cAf6_alm_chghi_jnstachg_MinVal                                                                     0x0
#define cAf6_alm_chghi_jnstachg_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: pslstachg
BitField Type: RW
BitField Desc: psl byte change
BitField Bits: [12]
--------------------------------------*/
#define cAf6_alm_chghi_pslstachg_Bit_Start                                                                  12
#define cAf6_alm_chghi_pslstachg_Bit_End                                                                    12
#define cAf6_alm_chghi_pslstachg_Mask                                                                   cBit12
#define cAf6_alm_chghi_pslstachg_Shift                                                                      12
#define cAf6_alm_chghi_pslstachg_MaxVal                                                                    0x1
#define cAf6_alm_chghi_pslstachg_MinVal                                                                    0x0
#define cAf6_alm_chghi_pslstachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: bersdstachg
BitField Type: W1C
BitField Desc: bersd stable status change
BitField Bits: [11]
--------------------------------------*/
#define cAf6_alm_chghi_bersdstachg_Bit_Start                                                                11
#define cAf6_alm_chghi_bersdstachg_Bit_End                                                                  11
#define cAf6_alm_chghi_bersdstachg_Mask                                                                 cBit11
#define cAf6_alm_chghi_bersdstachg_Shift                                                                    11
#define cAf6_alm_chghi_bersdstachg_MaxVal                                                                  0x1
#define cAf6_alm_chghi_bersdstachg_MinVal                                                                  0x0
#define cAf6_alm_chghi_bersdstachg_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: bersfstachg
BitField Type: W1C
BitField Desc: bersf stable status change
BitField Bits: [10]
--------------------------------------*/
#define cAf6_alm_chghi_bersfstachg_Bit_Start                                                                10
#define cAf6_alm_chghi_bersfstachg_Bit_End                                                                  10
#define cAf6_alm_chghi_bersfstachg_Mask                                                                 cBit10
#define cAf6_alm_chghi_bersfstachg_Shift                                                                    10
#define cAf6_alm_chghi_bersfstachg_MaxVal                                                                  0x1
#define cAf6_alm_chghi_bersfstachg_MinVal                                                                  0x0
#define cAf6_alm_chghi_bersfstachg_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: erdistachg
BitField Type: W1C
BitField Desc: erdis/rdi status change
BitField Bits: [9]
--------------------------------------*/
#define cAf6_alm_chghi_erdistachg_Bit_Start                                                                  9
#define cAf6_alm_chghi_erdistachg_Bit_End                                                                    9
#define cAf6_alm_chghi_erdistachg_Mask                                                                   cBit9
#define cAf6_alm_chghi_erdistachg_Shift                                                                      9
#define cAf6_alm_chghi_erdistachg_MaxVal                                                                   0x1
#define cAf6_alm_chghi_erdistachg_MinVal                                                                   0x0
#define cAf6_alm_chghi_erdistachg_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: bertcastachg
BitField Type: W1C
BitField Desc: bertca status change
BitField Bits: [8]
--------------------------------------*/
#define cAf6_alm_chghi_bertcastachg_Bit_Start                                                                8
#define cAf6_alm_chghi_bertcastachg_Bit_End                                                                  8
#define cAf6_alm_chghi_bertcastachg_Mask                                                                 cBit8
#define cAf6_alm_chghi_bertcastachg_Shift                                                                    8
#define cAf6_alm_chghi_bertcastachg_MaxVal                                                                 0x1
#define cAf6_alm_chghi_bertcastachg_MinVal                                                                 0x0
#define cAf6_alm_chghi_bertcastachg_RstVal                                                                 0x0

/*--------------------------------------
BitField Name: erdicstachg
BitField Type: W1C
BitField Desc: erdic status change
BitField Bits: [7]
--------------------------------------*/
#define cAf6_alm_chghi_erdicstachg_Bit_Start                                                                 7
#define cAf6_alm_chghi_erdicstachg_Bit_End                                                                   7
#define cAf6_alm_chghi_erdicstachg_Mask                                                                  cBit7
#define cAf6_alm_chghi_erdicstachg_Shift                                                                     7
#define cAf6_alm_chghi_erdicstachg_MaxVal                                                                  0x1
#define cAf6_alm_chghi_erdicstachg_MinVal                                                                  0x0
#define cAf6_alm_chghi_erdicstachg_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: erdipstachg
BitField Type: W1C
BitField Desc: erdip status change
BitField Bits: [6]
--------------------------------------*/
#define cAf6_alm_chghi_erdipstachg_Bit_Start                                                                 6
#define cAf6_alm_chghi_erdipstachg_Bit_End                                                                   6
#define cAf6_alm_chghi_erdipstachg_Mask                                                                  cBit6
#define cAf6_alm_chghi_erdipstachg_Shift                                                                     6
#define cAf6_alm_chghi_erdipstachg_MaxVal                                                                  0x1
#define cAf6_alm_chghi_erdipstachg_MinVal                                                                  0x0
#define cAf6_alm_chghi_erdipstachg_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: rfistachg
BitField Type: W1C
BitField Desc: rfi/lom status change
BitField Bits: [5]
--------------------------------------*/
#define cAf6_alm_chghi_rfistachg_Bit_Start                                                                   5
#define cAf6_alm_chghi_rfistachg_Bit_End                                                                     5
#define cAf6_alm_chghi_rfistachg_Mask                                                                    cBit5
#define cAf6_alm_chghi_rfistachg_Shift                                                                       5
#define cAf6_alm_chghi_rfistachg_MaxVal                                                                    0x1
#define cAf6_alm_chghi_rfistachg_MinVal                                                                    0x0
#define cAf6_alm_chghi_rfistachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: timstachg
BitField Type: W1C
BitField Desc: tim status change
BitField Bits: [4]
--------------------------------------*/
#define cAf6_alm_chghi_timstachg_Bit_Start                                                                   4
#define cAf6_alm_chghi_timstachg_Bit_End                                                                     4
#define cAf6_alm_chghi_timstachg_Mask                                                                    cBit4
#define cAf6_alm_chghi_timstachg_Shift                                                                       4
#define cAf6_alm_chghi_timstachg_MaxVal                                                                    0x1
#define cAf6_alm_chghi_timstachg_MinVal                                                                    0x0
#define cAf6_alm_chghi_timstachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: uneqstachg
BitField Type: W1C
BitField Desc: uneq status change
BitField Bits: [3]
--------------------------------------*/
#define cAf6_alm_chghi_uneqstachg_Bit_Start                                                                  3
#define cAf6_alm_chghi_uneqstachg_Bit_End                                                                    3
#define cAf6_alm_chghi_uneqstachg_Mask                                                                   cBit3
#define cAf6_alm_chghi_uneqstachg_Shift                                                                      3
#define cAf6_alm_chghi_uneqstachg_MaxVal                                                                   0x1
#define cAf6_alm_chghi_uneqstachg_MinVal                                                                   0x0
#define cAf6_alm_chghi_uneqstachg_RstVal                                                                   0x0

/*--------------------------------------
BitField Name: plmstachg
BitField Type: W1C
BitField Desc: plm status change
BitField Bits: [2]
--------------------------------------*/
#define cAf6_alm_chghi_plmstachg_Bit_Start                                                                   2
#define cAf6_alm_chghi_plmstachg_Bit_End                                                                     2
#define cAf6_alm_chghi_plmstachg_Mask                                                                    cBit2
#define cAf6_alm_chghi_plmstachg_Shift                                                                       2
#define cAf6_alm_chghi_plmstachg_MaxVal                                                                    0x1
#define cAf6_alm_chghi_plmstachg_MinVal                                                                    0x0
#define cAf6_alm_chghi_plmstachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: aisstachg
BitField Type: W1C
BitField Desc: ais status change
BitField Bits: [1]
--------------------------------------*/
#define cAf6_alm_chghi_aisstachg_Bit_Start                                                                   1
#define cAf6_alm_chghi_aisstachg_Bit_End                                                                     1
#define cAf6_alm_chghi_aisstachg_Mask                                                                    cBit1
#define cAf6_alm_chghi_aisstachg_Shift                                                                       1
#define cAf6_alm_chghi_aisstachg_MaxVal                                                                    0x1
#define cAf6_alm_chghi_aisstachg_MinVal                                                                    0x0
#define cAf6_alm_chghi_aisstachg_RstVal                                                                    0x0

/*--------------------------------------
BitField Name: lopstachg
BitField Type: W1C
BitField Desc: lop status change
BitField Bits: [0]
--------------------------------------*/
#define cAf6_alm_chghi_lopstachg_Bit_Start                                                                   0
#define cAf6_alm_chghi_lopstachg_Bit_End                                                                     0
#define cAf6_alm_chghi_lopstachg_Mask                                                                    cBit0
#define cAf6_alm_chghi_lopstachg_Shift                                                                       0
#define cAf6_alm_chghi_lopstachg_MaxVal                                                                    0x1
#define cAf6_alm_chghi_lopstachg_MinVal                                                                    0x0
#define cAf6_alm_chghi_lopstachg_RstVal                                                                    0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Status Report STS
Reg Addr   : 0x0D_007F
Reg Formula: 0x0D_007F + $OCID*128
    Where  :
           + $OCID(0-8) : Line ID, ID 0,2,4,6,8 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   :
This register is used to get POH alarm global change status report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbchghi_Base                                                                     0x0D007F
#define cAf6Reg_alm_glbchghi(OCID)                                                       (0x0D007F+(OCID)*128)
#define cAf6Reg_alm_glbchghi_WidthVal                                                                       32
#define cAf6Reg_alm_glbchghi_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: glbstachg
BitField Type: RO
BitField Desc: global status change bit
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbchghi_glbstachg_Bit_Start                                                                0
#define cAf6_alm_glbchghi_glbstachg_Bit_End                                                                 23
#define cAf6_alm_glbchghi_glbstachg_Mask                                                              cBit23_0
#define cAf6_alm_glbchghi_glbstachg_Shift                                                                    0
#define cAf6_alm_glbchghi_glbstachg_MaxVal                                                            0xffffff
#define cAf6_alm_glbchghi_glbstachg_MinVal                                                                 0x0
#define cAf6_alm_glbchghi_glbstachg_RstVal                                                                 0x0


/*------------------------------------------------------------------------------
Reg Name   : POH Interrupt Global Mask Report STS
Reg Addr   : 0x0D_007E
Reg Formula: 0x0D_007E + $OCID*128
    Where  :
           + $OCID(0-8) : Line ID, ID 0,2,4,6,8 : for STS 0,2,4..46, ID 1,3,5,7 for STS 1,3,5..47
Reg Desc   :
This register is used to get POH alarm global mask report.

------------------------------------------------------------------------------*/
#define cAf6Reg_alm_glbmskhi_Base                                                                     0x0D007E
#define cAf6Reg_alm_glbmskhi(OCID)                                                       (0x0D007E+(OCID)*128)
#define cAf6Reg_alm_glbmskhi_WidthVal                                                                       32
#define cAf6Reg_alm_glbmskhi_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: glbmsk
BitField Type: RW
BitField Desc: global mask
BitField Bits: [23:0]
--------------------------------------*/
#define cAf6_alm_glbmskhi_glbmsk_Bit_Start                                                                   0
#define cAf6_alm_glbmskhi_glbmsk_Bit_End                                                                    23
#define cAf6_alm_glbmskhi_glbmsk_Mask                                                                 cBit23_0
#define cAf6_alm_glbmskhi_glbmsk_Shift                                                                       0
#define cAf6_alm_glbmskhi_glbmsk_MaxVal                                                               0xffffff
#define cAf6_alm_glbmskhi_glbmsk_MinVal                                                                    0x0
#define cAf6_alm_glbmskhi_glbmsk_RstVal                                                                    0x0

/*------------------------------------------------------------------------------
Reg Name   : POH Hi-order Path Over Head Grabber
Reg Addr   : 0x02_4000
Reg Formula: 0x02_4000 + $stsid * 8 + $sliceid
    Where  :
           + $sliceid(0-4): Slice Identification, Slice 4 for Line
           + $stsid(0-47): STS Identification
Reg Desc   :
This register is used to grabber Hi-Order Path Overhead

------------------------------------------------------------------------------*/
#define cAf6Reg_pohstspohgrb_Base                                                                     0x024000
#define cAf6Reg_pohstspohgrb(sliceid, stsid)                                    (0x024000+(stsid)*8+(sliceid))
#define cAf6Reg_pohstspohgrb_WidthVal                                                                       96
#define cAf6Reg_pohstspohgrb_WriteMask                                                                     0x0

/*--------------------------------------
BitField Name: hlais
BitField Type: RO
BitField Desc: High-level AIS from OCN
BitField Bits: [67]
--------------------------------------*/
#define cAf6_pohstspohgrb_hlais_Bit_Start                                                                   67
#define cAf6_pohstspohgrb_hlais_Bit_End                                                                     67
#define cAf6_pohstspohgrb_hlais_Mask                                                                     cBit3
#define cAf6_pohstspohgrb_hlais_Shift                                                                        3
#define cAf6_pohstspohgrb_hlais_MaxVal                                                                     0x0
#define cAf6_pohstspohgrb_hlais_MinVal                                                                     0x0
#define cAf6_pohstspohgrb_hlais_RstVal                                                                     0x0

/*--------------------------------------
BitField Name: lom
BitField Type: RO
BitField Desc: LOM  from OCN
BitField Bits: [66]
--------------------------------------*/
#define cAf6_pohstspohgrb_lom_Bit_Start                                                                     66
#define cAf6_pohstspohgrb_lom_Bit_End                                                                       66
#define cAf6_pohstspohgrb_lom_Mask                                                                       cBit2
#define cAf6_pohstspohgrb_lom_Shift                                                                          2
#define cAf6_pohstspohgrb_lom_MaxVal                                                                       0x0
#define cAf6_pohstspohgrb_lom_MinVal                                                                       0x0
#define cAf6_pohstspohgrb_lom_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: lop
BitField Type: RO
BitField Desc: LOP from OCN
BitField Bits: [65]
--------------------------------------*/
#define cAf6_pohstspohgrb_lop_Bit_Start                                                                     65
#define cAf6_pohstspohgrb_lop_Bit_End                                                                       65
#define cAf6_pohstspohgrb_lop_Mask                                                                       cBit1
#define cAf6_pohstspohgrb_lop_Shift                                                                          1
#define cAf6_pohstspohgrb_lop_MaxVal                                                                       0x0
#define cAf6_pohstspohgrb_lop_MinVal                                                                       0x0
#define cAf6_pohstspohgrb_lop_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: ais
BitField Type: RO
BitField Desc: AIS from OCN
BitField Bits: [64]
--------------------------------------*/
#define cAf6_pohstspohgrb_ais_Bit_Start                                                                     64
#define cAf6_pohstspohgrb_ais_Bit_End                                                                       64
#define cAf6_pohstspohgrb_ais_Mask                                                                       cBit0
#define cAf6_pohstspohgrb_ais_Shift                                                                          0
#define cAf6_pohstspohgrb_ais_MaxVal                                                                       0x0
#define cAf6_pohstspohgrb_ais_MinVal                                                                       0x0
#define cAf6_pohstspohgrb_ais_RstVal                                                                       0x0

/*--------------------------------------
BitField Name: K3
BitField Type: RO
BitField Desc: K3 byte
BitField Bits: [63:56]
--------------------------------------*/
#define cAf6_pohstspohgrb_K3_Bit_Start                                                                      56
#define cAf6_pohstspohgrb_K3_Bit_End                                                                        63
#define cAf6_pohstspohgrb_K3_Mask                                                                    cBit31_24
#define cAf6_pohstspohgrb_K3_Shift                                                                          24
#define cAf6_pohstspohgrb_K3_MaxVal                                                                        0x0
#define cAf6_pohstspohgrb_K3_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_K3_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: F3
BitField Type: RO
BitField Desc: F3 byte
BitField Bits: [55:48]
--------------------------------------*/
#define cAf6_pohstspohgrb_F3_Bit_Start                                                                      48
#define cAf6_pohstspohgrb_F3_Bit_End                                                                        55
#define cAf6_pohstspohgrb_F3_Mask                                                                    cBit23_16
#define cAf6_pohstspohgrb_F3_Shift                                                                          16
#define cAf6_pohstspohgrb_F3_MaxVal                                                                        0x0
#define cAf6_pohstspohgrb_F3_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_F3_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: H4
BitField Type: RO
BitField Desc: H4 byte
BitField Bits: [47:40]
--------------------------------------*/
#define cAf6_pohstspohgrb_H4_Bit_Start                                                                      40
#define cAf6_pohstspohgrb_H4_Bit_End                                                                        47
#define cAf6_pohstspohgrb_H4_Mask                                                                     cBit15_8
#define cAf6_pohstspohgrb_H4_Shift                                                                           8
#define cAf6_pohstspohgrb_H4_MaxVal                                                                        0x0
#define cAf6_pohstspohgrb_H4_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_H4_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: F2
BitField Type: RO
BitField Desc: F2 byte
BitField Bits: [39:32]
--------------------------------------*/
#define cAf6_pohstspohgrb_F2_Bit_Start                                                                      32
#define cAf6_pohstspohgrb_F2_Bit_End                                                                        39
#define cAf6_pohstspohgrb_F2_Mask                                                                      cBit7_0
#define cAf6_pohstspohgrb_F2_Shift                                                                           0
#define cAf6_pohstspohgrb_F2_MaxVal                                                                        0x0
#define cAf6_pohstspohgrb_F2_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_F2_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: G1
BitField Type: RO
BitField Desc: G1 byte
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_pohstspohgrb_G1_Bit_Start                                                                      24
#define cAf6_pohstspohgrb_G1_Bit_End                                                                        31
#define cAf6_pohstspohgrb_G1_Mask                                                                    cBit31_24
#define cAf6_pohstspohgrb_G1_Shift                                                                          24
#define cAf6_pohstspohgrb_G1_MaxVal                                                                       0xff
#define cAf6_pohstspohgrb_G1_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_G1_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: C2
BitField Type: RO
BitField Desc: C2 byte
BitField Bits: [23:16]
--------------------------------------*/
#define cAf6_pohstspohgrb_C2_Bit_Start                                                                      16
#define cAf6_pohstspohgrb_C2_Bit_End                                                                        23
#define cAf6_pohstspohgrb_C2_Mask                                                                    cBit23_16
#define cAf6_pohstspohgrb_C2_Shift                                                                          16
#define cAf6_pohstspohgrb_C2_MaxVal                                                                       0xff
#define cAf6_pohstspohgrb_C2_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_C2_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: N1
BitField Type: RO
BitField Desc: N1 byte
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_pohstspohgrb_N1_Bit_Start                                                                       8
#define cAf6_pohstspohgrb_N1_Bit_End                                                                        15
#define cAf6_pohstspohgrb_N1_Mask                                                                     cBit15_8
#define cAf6_pohstspohgrb_N1_Shift                                                                           8
#define cAf6_pohstspohgrb_N1_MaxVal                                                                       0xff
#define cAf6_pohstspohgrb_N1_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_N1_RstVal                                                                        0x0

/*--------------------------------------
BitField Name: J1
BitField Type: RO
BitField Desc: J1 byte
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_pohstspohgrb_J1_Bit_Start                                                                       0
#define cAf6_pohstspohgrb_J1_Bit_End                                                                         7
#define cAf6_pohstspohgrb_J1_Mask                                                                      cBit7_0
#define cAf6_pohstspohgrb_J1_Shift                                                                           0
#define cAf6_pohstspohgrb_J1_MaxVal                                                                       0xff
#define cAf6_pohstspohgrb_J1_MinVal                                                                        0x0
#define cAf6_pohstspohgrb_J1_RstVal                                                                        0x0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021POHREG_H_ */


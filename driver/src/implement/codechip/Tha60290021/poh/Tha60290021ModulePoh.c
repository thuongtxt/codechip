/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290021ModulePoh.c
 *
 * Created Date: Jul 12, 2016
 *
 * Description : POH  management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/Tha60290021Device.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "Tha60290021ModulePohInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021ModulePoh)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021ModulePohMethods m_methods;

/* Override */
static tThaModulePohMethods         m_ThaModulePohOverride;
static tTha60210011ModulePohMethods m_Tha60210011ModulePohOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaTtiProcessor FaceplateLineTtiProcessorObjectCreate(Tha60290021ModulePoh self, AtSdhLine line)
    {
    AtUnused(self);
    return Tha60290021FaceplateLineTtiProcessorNew((AtSdhChannel)line);
    }

static ThaTtiProcessor LineTtiProcessorCreate(ThaModulePoh self, AtSdhLine line)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);

    if (Tha60290021ModuleSdhLineIsFaceplate(sdhModule, (uint8)AtChannelIdGet((AtChannel)line)))
        return mMethodsGet(mThis(self))->FaceplateLineTtiProcessorObjectCreate(mThis(self), line);

    return NULL;
    }

static eBool PslTtiInterruptIsSupported(ThaModulePoh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool SoftStickyIsNeeded(ThaModulePoh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportBlockModeCounters(Tha60210011ModulePoh self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionSupportPohMonitorEnabling(ThaModulePoh self)
    {
    mVersionReset(self);
    }

static uint32 StartVersionSupportSdSfInterrupt(Tha60210011ModulePoh self)
    {
    mVersionReset(self);
    }

static uint32 JnRequests(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return 0x0F1F7;
    }

static eBool UpsrIsSupported(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool PohInterruptIsSupported(Tha60210011ModulePoh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideTha60210011ModulePoh(AtModule self)
    {
    Tha60210011ModulePoh pohModule = (Tha60210011ModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModulePohOverride, mMethodsGet(pohModule), sizeof(m_Tha60210011ModulePohOverride));

        mMethodOverride(m_Tha60210011ModulePohOverride, PslTtiInterruptIsSupported);
        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportBlockModeCounters);
        mMethodOverride(m_Tha60210011ModulePohOverride, StartVersionSupportSdSfInterrupt);
        mMethodOverride(m_Tha60210011ModulePohOverride, JnRequests);
        mMethodOverride(m_Tha60210011ModulePohOverride, UpsrIsSupported);
        mMethodOverride(m_Tha60210011ModulePohOverride, PohInterruptIsSupported);
        }

    mMethodsSet(pohModule, &m_Tha60210011ModulePohOverride);
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, LineTtiProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, SoftStickyIsNeeded);
        mMethodOverride(m_ThaModulePohOverride, StartVersionSupportPohMonitorEnabling);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePoh(self);
    OverrideTha60210011ModulePoh(self);
    }

static void MethodsInit(Tha60290021ModulePoh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, FaceplateLineTtiProcessorObjectCreate);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModulePoh);
    }

AtModule Tha60290021ModulePohObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModulePohObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290021ModulePohNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModulePohObjectInit(newModule, device);
    }

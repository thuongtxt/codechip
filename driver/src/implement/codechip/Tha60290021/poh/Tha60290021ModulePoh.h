/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60290021ModulePoh.h
 * 
 * Created Date: Sep 3, 2016
 *
 * Description : POH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPOH_H_
#define _THA60290021MODULEPOH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/poh/ThaTtiProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaTtiProcessor Tha60290021FaceplateLineTtiProcessorNew(AtSdhChannel sdhChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPOH_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60290021FaceplateLineTtiProcessorInternal.h
 * 
 * Created Date: Apr 30, 2017
 *
 * Description : Line TTI processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021FACEPLATELINETTIPROCESSORINTERNAL_H_
#define _THA60290021FACEPLATELINETTIPROCESSORINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/poh/v2/ThaSdhLineTtiProcessorV2Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021FaceplateLineTtiProcessor * Tha60290021FaceplateLineTtiProcessor;

typedef struct tTha60290021FaceplateLineTtiProcessorMethods
    {
    uint8 (*GrabberSliceId)(Tha60290021FaceplateLineTtiProcessor self);
    uint8 (*BufferSliceId)(Tha60290021FaceplateLineTtiProcessor self);
    }tTha60290021FaceplateLineTtiProcessorMethods;

typedef struct tTha60290021FaceplateLineTtiProcessor
    {
    tThaSdhLineTtiProcessorV2 super;
    const tTha60290021FaceplateLineTtiProcessorMethods *methods;
    }tTha60290021FaceplateLineTtiProcessor;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaTtiProcessor Tha60290021FaceplateLineTtiProcessorObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021FACEPLATELINETTIPROCESSORINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60290021ModulePohInternal.h
 * 
 * Created Date: Apr 29, 2017
 *
 * Description : POH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEPOHINTERNAL_H_
#define _THA60290021MODULEPOHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/poh/Tha60210051ModulePohInternal.h"
#include "Tha60290021ModulePoh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModulePoh * Tha60290021ModulePoh;

typedef struct tTha60290021ModulePohMethods
    {
    ThaTtiProcessor (*FaceplateLineTtiProcessorObjectCreate)(Tha60290021ModulePoh self, AtSdhLine line);
    }tTha60290021ModulePohMethods;

typedef struct tTha60290021ModulePoh
    {
    tTha60210051ModulePoh super;
    const tTha60290021ModulePohMethods *methods;
    }tTha60290021ModulePoh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290021ModulePohObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEPOHINTERNAL_H_ */


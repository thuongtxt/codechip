/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60290021FaceplateLineTtiProcessor.c
 *
 * Created Date: Sep 2, 2016
 *
 * Description : SDH Line TTI processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/sdh/Tha60210031SdhLine.h"
#include "../ocn/Tha60290021ModuleOcnReg.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "Tha60290021ModulePoh.h"
#include "Tha60290021PohReg.h"
#include "Tha60290021FaceplateLineTtiProcessorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6_pohcpestssta_JnBadFrame_Mask      cBit24

/*--------------------------- Macros -----------------------------------------*/
#define mThaTtiProcessorRead(self, regAddr)           ThaTtiProcessorRead((ThaTtiProcessor)self, regAddr, cThaModulePoh)
#define mThaTtiProcessorWrite(self, regAddr, regVal)  ThaTtiProcessorWrite((ThaTtiProcessor)self, regAddr, regVal, cThaModulePoh)
#define mPohBaseAddress(self)                         PohBaseAddress((ThaTtiProcessor)self)
#define mThis(self) ((Tha60290021FaceplateLineTtiProcessor)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021FaceplateLineTtiProcessorMethods m_methods;

/* Override */
static tThaSdhLineTtiProcessorV2Methods m_ThaSdhLineTtiProcessorV2Override;
static tThaTtiProcessorMethods m_ThaTtiProcessorOverride;

/* Super implementation */
static const tThaTtiProcessorMethods *m_ThaTtiProcessorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePoh PohModule(ThaTtiProcessor self)
    {
    AtChannel channel = (AtChannel)ThaTtiProcessorChannelGet(self);
    return (ThaModulePoh)AtDeviceModuleGet(AtChannelDeviceGet(channel), cThaModulePoh);
    }

static uint32 TxJ0MessageBufferRegAddress(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return cAf6Reg_tfmj0insctl_Base;
    }

static uint32 OcnJ0TxMsgBufDefaultOffset(ThaSdhLineTtiProcessorV2 self, uint8 bufId)
    {
    AtChannel line = (AtChannel)ThaTtiProcessorChannelGet((ThaTtiProcessor)self);
    uint32 lineOffset = Tha60210031SdhLineChannelOffset((Tha60210031SdhLine)line);
    return lineOffset + bufId + ThaTtiProcessorPartOffset((ThaTtiProcessor)self);
    }

static uint8 SliceId(void)
    {
    /* Always use slice 4 for SDH Line TTI */
    return 4;
    }

static uint8 HwLineId(ThaTtiProcessor self)
    {
    return (uint8)AtChannelHwIdGet((AtChannel)ThaTtiProcessorChannelGet(self));
    }

static uint32 PohBaseAddress(ThaTtiProcessor self)
    {
    return Tha60210011ModulePohBaseAddress(PohModule(self));
    }

static uint32 DefaultOffset(ThaSdhLineTtiProcessorV2 self)
    {
    uint8 sliceId = mMethodsGet(mThis(self))->BufferSliceId(mThis(self));
    return (uint32)((96 * sliceId) + (2 * HwLineId((ThaTtiProcessor)self))) + ThaTtiProcessorPartOffset((ThaTtiProcessor)self) + mPohBaseAddress(self);
    }

static uint32 ExpTtiBufferRegOffset(ThaSdhLineTtiProcessorV2 self)
    {
    uint8 sliceId = mMethodsGet(mThis(self))->BufferSliceId(mThis(self));
    return (uint32)((384 * sliceId)  + (8 * HwLineId((ThaTtiProcessor)self))) + ThaTtiProcessorPartOffset((ThaTtiProcessor)self) + mPohBaseAddress(self);
    }

static uint32 PohJ0GrabberOffset(ThaSdhLineTtiProcessorV2 self)
    {
    ThaTtiProcessor processor = (ThaTtiProcessor)self;
    AtSdhChannel line = ThaTtiProcessorChannelGet(processor);
    uint8 hwSts = HwLineId(processor);
    uint8 sliceId = mMethodsGet(mThis(self))->GrabberSliceId(mThis(self));
    uint32 offset = Tha60210011Reg_pohstspohgrb(PohModule(processor), line, sliceId, hwSts);
    return offset + ThaTtiProcessorPartOffset(processor);
    }

static uint32 PohJ0MonitorControl(ThaTtiProcessor self)
    {
    ThaSdhLineTtiProcessorV2 processor = (ThaSdhLineTtiProcessorV2)self;
    return mMethodsGet(processor)->PohJ0Control(processor) +
           mMethodsGet(processor)->DefaultOffset(processor);
    }

static uint32 PohJ0Control(ThaSdhLineTtiProcessorV2 self)
    {
    return ThaModulePohCpestsctrBase(PohModule((ThaTtiProcessor)self));
    }

static eAtRet MonitorEnable(ThaTtiProcessor self, eBool enable)
    {
    uint32 regAddr = PohJ0MonitorControl(self);
    uint32 regVal  = mThaTtiProcessorRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_pohcpestsctr_TimEnb_, enable ? 1 : 0);
    mThaTtiProcessorWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool MonitorIsEnabled(ThaTtiProcessor self)
    {
    uint32 regAddr = PohJ0MonitorControl(self);
    uint32 regVal  = mThaTtiProcessorRead(self, regAddr);

    return (regVal & cAf6_pohcpestsctr_TimEnb_Mask) ? cAtTrue : cAtFalse;
    }

static eAtRet TimAffectingEnable(ThaTtiProcessor self, eBool enable)
    {
    uint32 regAddr = PohJ0MonitorControl(self);
    uint32 regVal = mThaTtiProcessorRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_pohcpestsctr_TimDstren_, enable ? 1 : 0);
    mThaTtiProcessorWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool TimAffectingIsEnabled(ThaTtiProcessor self)
    {
    uint32 regAddr = PohJ0MonitorControl(self);
    uint32 regVal  = mThaTtiProcessorRead(self, regAddr);

    return (regVal & cAf6_pohcpestsctr_TimDstren_Mask) ? cAtTrue : cAtFalse;
    }

static eAtSdhTtiMode RxTtiModeGet(ThaSdhLineTtiProcessorV2 self)
    {
    uint32 regAddr = PohJ0MonitorControl((ThaTtiProcessor)self);
    uint32 regVal = mThaTtiProcessorRead(self, regAddr);

    return ThaTtiProcessorTtiModeHw2Sw(mRegField(regVal, cAf6_pohcpestsctr_J1mode_));
    }

static eAtRet RxTtiModeSet(ThaSdhLineTtiProcessorV2 self, eAtSdhTtiMode ttiMode)
    {
    uint32 regAddr = PohJ0MonitorControl((ThaTtiProcessor)self);
    uint32 regVal = mThaTtiProcessorRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_pohcpestsctr_J1mode_, ThaTtiProcessorTtiModeSw2Hw(ttiMode));
    mThaTtiProcessorWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet ExpectedTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    uint32 regAddr;
    ThaSdhLineTtiProcessorV2 processor = (ThaSdhLineTtiProcessorV2)self;
    AtChannel channel = (AtChannel)ThaTtiProcessorChannelGet(self);

    tti->mode = RxTtiModeGet(processor);

    regAddr = mMethodsGet(processor)->PohExpectedTtiAddressGet(processor) +
              mMethodsGet(processor)->ExpTtiBufferRegOffset(processor);
    return ThaTtiProcessorLongRead(tti, regAddr, channel);
    }

static eAtRet ExpectedTtiSet(ThaTtiProcessor self, const tAtSdhTti *tti)
    {
    uint32 regAddr;
    ThaSdhLineTtiProcessorV2 processor = (ThaSdhLineTtiProcessorV2)self;
    AtChannel channel = (AtChannel)ThaTtiProcessorChannelGet(self);

    RxTtiModeSet(processor, tti->mode);

    regAddr = mMethodsGet(processor)->PohExpectedTtiAddressGet(processor) +
              mMethodsGet(processor)->ExpTtiBufferRegOffset(processor);
    return ThaTtiProcessorLongWrite(tti, regAddr, channel);
    }

static uint32 PohJ0StatusOffset(ThaSdhLineTtiProcessorV2 self)
    {
    uint8 sliceId = mMethodsGet(mThis(self))->BufferSliceId(mThis(self));
    return (uint32)((48 * sliceId) + HwLineId((ThaTtiProcessor)self)) + mPohBaseAddress(self);
    }

static uint32 PohJ0Status(ThaSdhLineTtiProcessorV2 self)
    {
    return ThaModulePohCpeStsStatus(PohModule((ThaTtiProcessor)self));
    }

static uint32 PohJ0StatusRegAddr(ThaTtiProcessor self)
    {
    ThaSdhLineTtiProcessorV2 processor = (ThaSdhLineTtiProcessorV2)self;
    return mMethodsGet(processor)->PohJ0Status(processor) +
           mMethodsGet(processor)->PohJ0StatusOffset(processor);
    }

static eAtModuleSdhRet RxTtiModeCheck(ThaTtiProcessor self)
    {
    uint32 regAddr, regVal;

    if (!Tha60210011ModulePohShouldCheckTtiOofStatus((Tha60210011ModulePoh)PohModule(self)))
        return cAtOk;

    regAddr = PohJ0StatusRegAddr(self);
    regVal = ThaTtiProcessorRead(self, regAddr, cThaModulePoh);
    if (regVal & cAf6_pohcpestssta_JnBadFrame_Mask)
        return cAtModuleSdhErrorTtiModeMismatch;

    return cAtOk;
    }

static eAtRet RxTtiGet(ThaTtiProcessor self, tAtSdhTti *tti)
    {
    eAtRet ret;
    uint32 regAddr;
    uint32 regVal[cThaLongRegMaxSize];
    AtChannel channel = (AtChannel)ThaTtiProcessorChannelGet(self);
    ThaSdhLineTtiProcessorV2 ttiProcessor = (ThaSdhLineTtiProcessorV2)self;
    AtOsal osal = AtSharedDriverOsalGet();

    mMethodsGet(osal)->MemInit(osal, tti->message, 0, cAtSdhChannelMaxTtiLength);
    tti->mode = RxTtiModeGet(ttiProcessor);

    if (ThaTtiProcessorHasAlarmForwarding(self))
        return cAtOk;

    if (tti->mode == cAtSdhTtiMode1Byte)
        {
        regAddr = mMethodsGet(ttiProcessor)->PohJ0Grabber(ttiProcessor) +
                  mMethodsGet(ttiProcessor)->PohJ0GrabberOffset(ttiProcessor);
        mChannelHwLongRead(channel, regAddr, regVal, cThaLongRegMaxSize, cThaModulePoh);
        tti->message[0] = (uint8)mRegField(regVal[0], cAf6_pohstspohgrb_J1_);
        return cAtOk;
        }

    ret = RxTtiModeCheck(self);
    if (ret != cAtOk)
        return ret;

    regAddr = mMethodsGet(ttiProcessor)->PohCapturedTtiAddressGet(ttiProcessor) +
              mMethodsGet(ttiProcessor)->ExpTtiBufferRegOffset(ttiProcessor);
    return ThaTtiProcessorLongRead(tti, regAddr, channel);
    }

static uint32 PohExpectedTtiAddressGet(ThaSdhLineTtiProcessorV2 self)
    {
    return ThaModulePohMsgstsexpBase(PohModule((ThaTtiProcessor)self));
    }

static uint32 PohCapturedTtiAddressGet(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return ThaModulePohMsgstscurBase(PohModule((ThaTtiProcessor)self));
    }

static eAtRet DefaultSet(ThaTtiProcessor self)
    {
    uint32 regAddr = PohJ0MonitorControl(self);
    uint32 regVal  = mThaTtiProcessorRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_pohcpestsctr_MonEnb_, 1);
    mThaTtiProcessorWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint8 GrabberSliceId(Tha60290021FaceplateLineTtiProcessor self)
    {
    AtUnused(self);
    return SliceId();
    }

static uint8 BufferSliceId(Tha60290021FaceplateLineTtiProcessor self)
    {
    AtUnused(self);
    return SliceId();
    }

static eBool HasAlarmForwarding(ThaTtiProcessor self)
    {
    AtChannel channel = (AtChannel)ThaTtiProcessorChannelGet(self);
    ThaSdhLineTtiProcessorV2 ttiProcessor = (ThaSdhLineTtiProcessorV2)self;
    uint32 regVal[cThaLongRegMaxSize];
    uint32 regAddr = mMethodsGet(ttiProcessor)->PohJ0Grabber(ttiProcessor) +
                     mMethodsGet(ttiProcessor)->PohJ0GrabberOffset(ttiProcessor);
    mChannelHwLongRead(channel, regAddr, regVal, cThaLongRegMaxSize, cThaModulePoh);
    return mRegField(regVal[2], cAf6_pohstspohgrb_ais_) ? cAtTrue : cAtFalse;
    }

static uint32 PohJ0Grabber(ThaSdhLineTtiProcessorV2 self)
    {
    AtChannel channel = (AtChannel)ThaTtiProcessorChannelGet((ThaTtiProcessor)self);
    return ThaModulePohstspohgrbBase(PohModule((ThaTtiProcessor)self), (AtSdhChannel)channel);
    }

static void OverrideThaSdhLineTtiProcessorV2(ThaTtiProcessor self)
    {
    ThaSdhLineTtiProcessorV2 processor = (ThaSdhLineTtiProcessorV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineTtiProcessorV2Override, mMethodsGet(processor), sizeof(m_ThaSdhLineTtiProcessorV2Override));

        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, TxJ0MessageBufferRegAddress);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, OcnJ0TxMsgBufDefaultOffset);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohCapturedTtiAddressGet);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohExpectedTtiAddressGet);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, ExpTtiBufferRegOffset);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0Control);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, DefaultOffset);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0Status);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0StatusOffset);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0Grabber);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0GrabberOffset);
        }

    mMethodsSet(processor, &m_ThaSdhLineTtiProcessorV2Override);
    }

static void OverrideThaTtiProcessor(ThaTtiProcessor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaTtiProcessorMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaTtiProcessorOverride, m_ThaTtiProcessorMethods, sizeof(m_ThaTtiProcessorOverride));

        mMethodOverride(m_ThaTtiProcessorOverride, MonitorEnable);
        mMethodOverride(m_ThaTtiProcessorOverride, MonitorIsEnabled);
        mMethodOverride(m_ThaTtiProcessorOverride, TimAffectingEnable);
        mMethodOverride(m_ThaTtiProcessorOverride, TimAffectingIsEnabled);
        mMethodOverride(m_ThaTtiProcessorOverride, ExpectedTtiSet);
        mMethodOverride(m_ThaTtiProcessorOverride, ExpectedTtiGet);
        mMethodOverride(m_ThaTtiProcessorOverride, RxTtiGet);
        mMethodOverride(m_ThaTtiProcessorOverride, DefaultSet);
        mMethodOverride(m_ThaTtiProcessorOverride, HasAlarmForwarding);
        }

    mMethodsSet(self, &m_ThaTtiProcessorOverride);
    }

static void Override(ThaTtiProcessor self)
    {
    OverrideThaSdhLineTtiProcessorV2(self);
    OverrideThaTtiProcessor(self);
    }

static void MethodsInit(Tha60290021FaceplateLineTtiProcessor self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, GrabberSliceId);
        mMethodOverride(m_methods, BufferSliceId);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021FaceplateLineTtiProcessor);
    }

ThaTtiProcessor Tha60290021FaceplateLineTtiProcessorObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineTtiProcessorV2ObjectInit(self, sdhChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaTtiProcessor Tha60290021FaceplateLineTtiProcessorNew(AtSdhChannel sdhChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaTtiProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021FaceplateLineTtiProcessorObjectInit(newProcessor, sdhChannel);
    }

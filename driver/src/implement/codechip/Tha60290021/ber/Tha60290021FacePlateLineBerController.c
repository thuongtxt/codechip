/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60290021FacePlateLineBerController.c
 *
 * Created Date: Sep 8, 2016
 *
 * Description : Tha60290021FacePlateLineBerController implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290021ModuleBer.h"
#include "../../Tha60210031/ber/Tha60210031SdhLineBerControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021FacePlateLineBerController
    {
    tTha60210031SdhLineBerController super;
    }tTha60290021FacePlateLineBerController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60210011SdhAuVcBerControllerMethods m_Tha60210011SdhAuVcBerControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60210011ModuleBer BerModule(Tha60210011SdhAuVcBerController self)
    {
    return (Tha60210011ModuleBer)AtBerControllerModuleGet((AtBerController)self);
    }

static AtSdhLine Line(Tha60210011SdhAuVcBerController self)
    {
    return (AtSdhLine)AtBerControllerMonitoredChannel((AtBerController)self);
    }

static eAtRet HwSts(Tha60210011SdhAuVcBerController self, uint8 *slice, uint8 *hwSts)
    {
    return Tha60210011ModuleBerLinePohHwId(BerModule(self), Line(self), slice, hwSts);
    }

static uint32 CurrentBerOffset(Tha60210011SdhAuVcBerController self)
    {
    uint8 slice, hwSts;
    HwSts(self, &slice, &hwSts);
    return Tha60210011ModuleBerLineCurrentBerOffset(BerModule(self), Line(self), slice, hwSts);
    }

static uint32 BerControlOffset(Tha60210011SdhAuVcBerController self)
    {
    uint8 slice, hwSts;
    HwSts(self, &slice, &hwSts);
    return Tha60210011ModuleBerLineBerControlOffset(BerModule(self), Line(self), slice, hwSts);
    }

static uint32 HwChannelTypeValue(AtBerController self)
    {
    AtChannel line = AtBerControllerMonitoredChannel((AtBerController)self);
    eAtSdhLineRate rate = AtSdhLineRateGet((AtSdhLine) line);

    switch ((uint32) rate)
        {
        case cAtSdhLineRateStm0 : return cOc1Threshold1Rate;
        case cAtSdhLineRateStm1 : return cOc3Threshold1Rate;
        case cAtSdhLineRateStm4 : return cOc12Threshold1Rate;
        case cAtSdhLineRateStm16: return cOc48Threshold1Rate;
        case cAtSdhLineRateStm64: return cOc192Threshold1Rate;
        default:
            return 0;
        }
    }

static void OverrideTha60210011SdhAuVcBerController(AtBerController self)
    {
    Tha60210011SdhAuVcBerController auvcController = (Tha60210011SdhAuVcBerController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011SdhAuVcBerControllerOverride, mMethodsGet(auvcController), sizeof(m_Tha60210011SdhAuVcBerControllerOverride));

        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, CurrentBerOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, BerControlOffset);
        mMethodOverride(m_Tha60210011SdhAuVcBerControllerOverride, HwChannelTypeValue);
        }

    mMethodsSet(auvcController, &m_Tha60210011SdhAuVcBerControllerOverride);
    }

static void Override(AtBerController self)
    {
    OverrideTha60210011SdhAuVcBerController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021FacePlateLineBerController);
    }

static AtBerController ObjectInit(AtBerController self, uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031SdhLineBerControllerObjectInit(self, controllerId, channel, berModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtBerController Tha60290021FacePlateLineBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtBerController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, controllerId, channel, berModule);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60290021ModuleBerInternal.h
 * 
 * Created Date: Jul 12, 2017
 *
 * Description : BER module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEBERINTERNAL_H_
#define _THA60290021MODULEBERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/ber/Tha60210051ModuleBerInternal.h"
#include "Tha60290021ModuleBer.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModuleBer
    {
    tTha60210051ModuleBer super;
    }tTha60290021ModuleBer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleBer Tha60290021ModuleBerObjectInit(AtModuleBer self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEBERINTERNAL_H_ */


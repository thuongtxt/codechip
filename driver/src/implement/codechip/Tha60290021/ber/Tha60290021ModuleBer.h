/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : BER
 * 
 * File        : Tha60290021ModuleBer.h
 * 
 * Created Date: Sep 8, 2016
 *
 * Description : Tha60290021ModuleBer declarations
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEBER_H_
#define _THA60290021MODULEBER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtBerController.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtBerController Tha60290021FacePlateLineBerControllerNew(uint32 controllerId, AtChannel channel, AtModuleBer berModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEBER_H_ */


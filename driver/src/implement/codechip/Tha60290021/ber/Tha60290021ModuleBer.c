/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : BER
 *
 * File        : Tha60290021ModuleBer.c
 *
 * Created Date: Jul 12, 2016
 *
 * Description : BER  management
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../man/Tha60290021Device.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "Tha60290021ModuleBerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cLineOc1Threshold1 0x1E0071

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleBerMethods           m_AtModuleBerOverride;
static tTha60210011ModuleBerMethods  m_Tha60210011ModuleBerOverride;

/* Save super implementation */
static const tTha60210011ModuleBerMethods *m_Tha60210011ModuleBerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtBerController SdhLineMsBerControlerCreate(AtModuleBer self, AtChannel line)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cAtModuleSdh);
    uint8 lineId = (uint8)AtChannelIdGet(line);

    if (Tha60290021ModuleSdhLineIsFaceplate(sdhModule, lineId))
        return Tha60290021FacePlateLineBerControllerNew(lineId, line, self);

    return NULL;
    }

static AtBerController SdhLineMsBerControlerObjectCreate(AtModuleBer self, uint32 controllerId, AtChannel line)
    {
    return Tha60290021FacePlateLineBerControllerNew(controllerId, line, self);
    }

static void BerOc3ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc3Threshold1Rate),     cOc3Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate),     cOc3Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 1, cOc3Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 2, cOc3Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 3, cOc3Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 4, cOc3Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 5, cOc3Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 6, cOc3Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 7, cOc3Threshold2_7);
    }

static void BerOc3ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerOc3ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc3Threshold1Rate),     0x2DB0166);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate),     0xEC0076);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 1, 0xEC0076);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 2, 0x7B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 3, 0x98C0578);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 4, 0xA40059A);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 5, 0xA5205A5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 6, 0xA5405A6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc3Threshold2Rate) + 7, 0xA5405A6);
    }

static void BerOc12ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc12Threshold1Rate),     cOc12Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate),     cOc12Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 1, cOc12Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 2, cOc12Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 3, cOc12Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 4, cOc12Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 5, cOc12Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 6, cOc12Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 7, cOc12Threshold2_7);
    }

static void BerOc12ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerOc12ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc12Threshold1Rate),     0xBB405CB);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate),     0x3FC0202);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 1, 0x4040202);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 2, 0x212);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 3, 0x271615CC);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 4, 0x29F615EE);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 5, 0x2A421617);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 6, 0x2A4A161B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc12Threshold2Rate) + 7, 0x2A4C161B);
    }

static void BerOc48ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc48Threshold1Rate),     cOc48Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate),     cOc48Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 1, cOc48Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 2, cOc48Threshold2_2) ;
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 3, cOc48Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 4, cOc48Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 5, cOc48Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 6, cOc48Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 7, cOc48Threshold2_7);
    }

static void BerOc48ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerOc48ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc48Threshold1Rate),     0x2F601792);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate),     0x108E0857);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 1, 0x10B00858);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 2, 0x896);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 3, 0x9E1C50D4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 4, 0xA9BC56C5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 5, 0xAAF65765);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 6, 0xAB165776);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc48Threshold2Rate) + 7, 0xAB1A5777);
    }

static AtIpCore Core(Tha60210011ModuleBer self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    uint8 coreId = AtModuleDefaultCoreGet((AtModule)self);
    return AtDeviceIpCoreGet(device, coreId);
    }

static void BerOc192ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    AtIpCore core = Core(self);
    uint32 longRegVal[cThaLongRegMaxSize];

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc192Threshold1Rate),     0x122D5F13);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 0, 0x437221F7);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 1, 0x43FC21FE);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 2, 0x22EF    );

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
    longRegVal[0] = 0x7BFE22ED;
    longRegVal[1] = 0x2;
    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 3, longRegVal, cThaLongRegMaxSize, core);

    /* Note, still keed the longRegVal[1] to be 2 for the rest of long write operations */
    longRegVal[0] = 0xAAC2232E;
    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 4, longRegVal, cThaLongRegMaxSize, core);

    longRegVal[0] = 0xAFB02334;
    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 5, longRegVal, cThaLongRegMaxSize, core);

    longRegVal[0] = 0xB02E2335;
    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 6, longRegVal, cThaLongRegMaxSize, core);

    longRegVal[0] = 0xB03A2335;
    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 7, longRegVal, cThaLongRegMaxSize, core);
    }

static void BerOc192ThresholdInit(Tha60210011ModuleBer self)
    {
    AtIpCore core = Core(self);
    uint32 longRegVal[cThaLongRegMaxSize];

    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerOc192ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cOc192Threshold1Rate),     0xBE9D5F13);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 0, 0x437221F7);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 1, 0x43FC21FE);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 2, 0x22EF);

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
    longRegVal[0] = 0x7BFF418A;
    longRegVal[1] = 0x2;
    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 3, longRegVal, cThaLongRegMaxSize, core);

    /* Note, still keed the longRegVal[1] to be 2 for the rest of long write operations */
    longRegVal[0] = 0xAAC35BA8;
    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 4, longRegVal, cThaLongRegMaxSize, core);

    longRegVal[0] = 0xAFB15BAB;
    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 5, longRegVal, cThaLongRegMaxSize, core);

    longRegVal[0] = 0xB02F5BEB;
    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 6, longRegVal, cThaLongRegMaxSize, core);

    longRegVal[0] = 0xB03B5BF2;
    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cOc192Threshold2Rate) + 7, longRegVal, cThaLongRegMaxSize, core);
    }

static void BerSts192cThresholdInit(Tha60210011ModuleBer self)
    {
    AtIpCore core = Core(self);
    uint32 longRegVal[cThaLongRegMaxSize];

    if (!mMethodsGet(self)->HasStandardClearTime(self))
        return;

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cSts192Threshold1Rate),     0x15A91);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts192Threshold2Rate) + 0, 0x14E61CB2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts192Threshold2Rate) + 1, 0x4176212D);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts192Threshold2Rate) + 2, 0x12B);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts192Threshold2Rate) + 3, 0x16020AEF);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts192Threshold2Rate) + 4, 0xD4541DBB);

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
    longRegVal[0] = 0x450021E9;
    longRegVal[1] = 0x2;

    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts192Threshold2Rate) + 5, longRegVal, cThaLongRegMaxSize, core);

    longRegVal[0] = 0x96B6225F;
    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts192Threshold2Rate) + 6, longRegVal, cThaLongRegMaxSize, core);

    longRegVal[0] = 0x9FB8226B;
    mModuleHwLongWrite(self, Tha60210011RegThreshold2AddressByRate(self, cSts192Threshold2Rate) + 7, longRegVal, cThaLongRegMaxSize, core);
    }

static void BerLineEc1ThresholdStandardClearTimeInit(Tha60210011ModuleBer self)
    {
    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cLineEc1Threshold1Rate),     cLineOc1Threshold1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate),     cLineEC1Threshold2_0);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 1, cLineEC1Threshold2_1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 2, cLineEC1Threshold2_2);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 3, cLineEC1Threshold2_3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 4, cLineEC1Threshold2_4);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 5, cLineEC1Threshold2_5);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 6, cLineEC1Threshold2_6);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 7, cLineEC1Threshold2_7);
    }

static void BerLineEc1ThresholdInit(Tha60210011ModuleBer self)
    {
    if (mMethodsGet(self)->HasStandardClearTime(self))
        {
        BerLineEc1ThresholdStandardClearTimeInit(self);
        return;
        }

    mModuleHwWrite(self, Tha60210011RegThreshold1AddressByRate(self, cLineEc1Threshold1Rate),     0x1D671);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate),     0x440022);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 1, 0x440022);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 2, 0x24);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 3, 0x31201D3);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 4, 0x34C01ED);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 5, 0x35201F1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 6, 0x35401F1);
    mModuleHwWrite(self, Tha60210011RegThreshold2AddressByRate(self, cLineEC1Threshold2Rate) + 7, 0x35401F1);
    }

static void DefaultSet(Tha60210011ModuleBer self)
    {
    m_Tha60210011ModuleBerMethods->DefaultSet(self);
    BerLineEc1ThresholdInit(self);
    BerOc3ThresholdInit(self);
    BerOc12ThresholdInit(self);
    BerOc48ThresholdInit(self);
    BerOc192ThresholdInit(self);
    BerSts192cThresholdInit(self);
    }

static eAtRet LinePohHwId(Tha60210011ModuleBer self, AtSdhLine line, uint8 *slice, uint8 *hwSts)
    {
    AtUnused(self);
    *hwSts = (uint8)AtChannelHwIdGet((AtChannel)line);
    *slice = 4;
    return cAtOk;
    }

static uint32 LineBerControlOffset(Tha60210011ModuleBer self, AtSdhLine line, uint8 slice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(line);
    return 128UL * hwSts + 8UL * slice;
    }

static uint32 LineCurrentBerOffset(Tha60210011ModuleBer self, AtSdhLine line, uint8 slice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(line);
    return 16UL * hwSts + 2UL * slice;
    }

static void OverrideTha60210011ModuleBer(AtModuleBer self)
    {
    Tha60210011ModuleBer module = (Tha60210011ModuleBer)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleBerMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleBerOverride, m_Tha60210011ModuleBerMethods, sizeof(m_Tha60210011ModuleBerOverride));

        mMethodOverride(m_Tha60210011ModuleBerOverride, DefaultSet);
        mMethodOverride(m_Tha60210011ModuleBerOverride, LinePohHwId);
        mMethodOverride(m_Tha60210011ModuleBerOverride, LineBerControlOffset);
        mMethodOverride(m_Tha60210011ModuleBerOverride, LineCurrentBerOffset);
        }

    mMethodsSet(module, &m_Tha60210011ModuleBerOverride);
    }

static void OverrideAtModuleBer(AtModuleBer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleBerOverride, mMethodsGet(self), sizeof(m_AtModuleBerOverride));

        mMethodOverride(m_AtModuleBerOverride, SdhLineMsBerControlerCreate);
        mMethodOverride(m_AtModuleBerOverride, SdhLineMsBerControlerObjectCreate);
        }

    mMethodsSet(self, &m_AtModuleBerOverride);
    }

static void Override(AtModuleBer self)
    {
    OverrideAtModuleBer(self);
    OverrideTha60210011ModuleBer(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModuleBer);
    }

AtModuleBer Tha60290021ModuleBerObjectInit(AtModuleBer self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModuleBerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleBer Tha60290021ModuleBerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleBer newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModuleBerObjectInit(newModule, device);
    }

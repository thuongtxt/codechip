/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha6029OcnSerdesDiag.h
 *
 * Created Date: Sep 21, 2016
 *
 * Description : Diagnostic
 *
 * Notes       :
 *----------------------------------------------------------------------------*/
#ifndef _AF6_REG_AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG_H_
#define _AF6_REG_AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer Control
Reg Addr   : 0x00000
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Rx Framer

------------------------------------------------------------------------------*/
#define cAf6_diag_glbrfm_reg_RxFrm_Base                                                                   0x00000

/*--------------------------------------
BitField Name: RxFrmBadFrmThresh
BitField Type: RW
BitField Desc: Threshold for A1A2 missing counter, that is used to change state
from FRAMED to HUNT.
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_diag_glbrfm_reg_RxFrmBadFrmThresh_Mask                                                       cBit14_12
#define cAf6_diag_glbrfm_reg_RxFrmBadFrmThresh_Shift                                                             12
#define cAf6_diag_glbrfm_reg_RxFrmBadFrmThresh_RstVal                                                           0x4

/*--------------------------------------
BitField Name: RxFrmDescrEn
BitField Type: RW
BitField Desc: Enable/disable de-scrambling of the Rx coming data stream for 4
lines. 1: Enable 0: Disable
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_diag_glbrfm_reg_RxFrmDescrEn_Mask                                                             cBit11_8
#define cAf6_diag_glbrfm_reg_RxFrmDescrEn_Shift                                                                   8
#define cAf6_diag_glbrfm_reg_RxFrmDescrEn_RstVal                                                                0xf
#define cAf6_diag_glbtfm_reg_RxFrmDesScrEnMask(line)                                                   (cBit8 << (line))
#define cAf6_diag_glbtfm_reg_RxFrmDesScrEnshift(line)                                                  (8 + (line))
/*--------------------------------------
BitField Name: RxFrmStmOcnMode
BitField Type: RW
BitField Desc: STM rate mode for Rx of  4 lines, each line use 2 bits.Bits[1:0]
for line 0 0: OC48 1: OC12 2: OC3
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_diag_glbrfm_reg_RxFrmStmOcnMode_Mask                                                           cBit7_0
#define cAf6_diag_glbrfm_reg_RxFrmStmOcnMode_Shift                                                                0
#define cAf6_diag_glbrfm_reg_RxFrmStmOcnMode_RstVal                                                             0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOS Detecting Control 1
Reg Addr   : 0x00006
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Rx Framer LOS detecting 1

------------------------------------------------------------------------------*/
#define cAf6_diag_glbclkmon_reg_Base                                                                     0x00006

/*--------------------------------------
BitField Name: RxFrmLosDetMod
BitField Type: RW
BitField Desc: Detected LOS mode. 1: the LOS declare when all zero or all one
detected 0: the LOS declare when all zero detected
BitField Bits: [18]
--------------------------------------*/
#define cAf6_diag_glbclkmon_reg_RxFrmLosDetMod_Mask                                                          cBit18
#define cAf6_diag_glbclkmon_reg_RxFrmLosDetMod_Shift                                                             18
#define cAf6_diag_glbclkmon_reg_RxFrmLosDetMod_RstVal                                                           0x1

/*--------------------------------------
BitField Name: RxFrmLosDetDis
BitField Type: RW
BitField Desc: Disable detect LOS. 1: Disable 0: Enable
BitField Bits: [17]
--------------------------------------*/
#define cAf6_diag_glbclkmon_reg_RxFrmLosDetDis_Mask                                                          cBit17
#define cAf6_diag_glbclkmon_reg_RxFrmLosDetDis_Shift                                                             17
#define cAf6_diag_glbclkmon_reg_RxFrmLosDetDis_RstVal                                                           0x0

/*--------------------------------------
BitField Name: RxFrmClkMonDis
BitField Type: RW
BitField Desc: Disable to generate LOS to Rx Framer if detecting error on Rx
line clock. 1: Disable 0: Enable
BitField Bits: [16]
--------------------------------------*/
#define cAf6_diag_glbclkmon_reg_RxFrmClkMonDis_Mask                                                          cBit16
#define cAf6_diag_glbclkmon_reg_RxFrmClkMonDis_Shift                                                             16
#define cAf6_diag_glbclkmon_reg_RxFrmClkMonDis_RstVal                                                           0x0

/*--------------------------------------
BitField Name: RxFrmClkMonThr
BitField Type: RW
BitField Desc: Threshold to generate LOS to Rx Framer if detecting error on Rx
line clock.(Threshold = PPM * 155.52/8 for STM16 and STM1). Default 0x3cc ~
50ppm for STM16 and STM1. With STM4 must config value that is this default value
divide by 4.
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_diag_glbclkmon_reg_RxFrmClkMonThr_Mask                                                        cBit15_0
#define cAf6_diag_glbclkmon_reg_RxFrmClkMonThr_Shift                                                              0
#define cAf6_diag_glbclkmon_reg_RxFrmClkMonThr_RstVal                                                         0x3cc


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOS Detecting Control 2
Reg Addr   : 0x00007
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Rx Framer LOS detecting 2

------------------------------------------------------------------------------*/
#define cAf6_diag_glbdetlos_pen_Base                                                                     0x00007

/*--------------------------------------
BitField Name: RxFrmLosClr2Thr
BitField Type: RW
BitField Desc: Configure the period of time during which there is no period of
consecutive data without transition, used for reset no transition counter. The
recommended value is 0x30 (~2.5ms) for STM16 and STM1. With STM4 must config
value that is this default value divide by 4.
BitField Bits: [31:24]
--------------------------------------*/
#define cAf6_diag_glbdetlos_pen_RxFrmLosClr2Thr_Mask                                                      cBit31_24
#define cAf6_diag_glbdetlos_pen_RxFrmLosClr2Thr_Shift                                                            24
#define cAf6_diag_glbdetlos_pen_RxFrmLosClr2Thr_RstVal                                                         0x30

/*--------------------------------------
BitField Name: RxFrmLosSetThr
BitField Type: RW
BitField Desc: Configure the value that define the period of time in which there
is no transition detected from incoming data from SERDES. The recommended value
is 0x3cc (~50ms) for STM16 and STM1. With STM4 must config value that is this
default value divide by 4.
BitField Bits: [23:12]
--------------------------------------*/
#define cAf6_diag_glbdetlos_pen_RxFrmLosSetThr_Mask                                                       cBit23_12
#define cAf6_diag_glbdetlos_pen_RxFrmLosSetThr_Shift                                                             12
#define cAf6_diag_glbdetlos_pen_RxFrmLosSetThr_RstVal                                                         0x3cc

/*--------------------------------------
BitField Name: RxFrmLosClr1Thr
BitField Type: RW
BitField Desc: Configure the period of time during which there is no period of
consecutive data without transition, used for counting at INFRM to change state
to OOF The recommended value is 0x3cc (~50ms) for STM16 and STM1. With STM4 must
config value that is this default value divide by 4.
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_diag_glbdetlos_pen_RxFrmLosClr1Thr_Mask                                                       cBit11_0
#define cAf6_diag_glbdetlos_pen_RxFrmLosClr1Thr_Shift                                                             0
#define cAf6_diag_glbdetlos_pen_RxFrmLosClr1Thr_RstVal                                                        0x3cc


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Rx Framer LOF Threshold
Reg Addr   : 0x00008
Reg Formula: 
    Where  : 
Reg Desc   : 
Configure thresholds for LOF detection at receive SONET/SDH framer,There are two thresholds

------------------------------------------------------------------------------*/
#define cAf6_diag_glblofthr_reg_Base                                                                     0x00008

/*--------------------------------------
BitField Name: RxFrmLofDetMod
BitField Type: RW
BitField Desc: Detected LOF mode.Set 1 to clear OOF counter when state into
INFRAMED
BitField Bits: [16]
--------------------------------------*/
#define cAf6_diag_glblofthr_reg_RxFrmLofDetMod_Mask                                                          cBit16
#define cAf6_diag_glblofthr_reg_RxFrmLofDetMod_Shift                                                             16
#define cAf6_diag_glblofthr_reg_RxFrmLofDetMod_RstVal                                                           0x1

/*--------------------------------------
BitField Name: RxFrmLofSetThr
BitField Type: RW
BitField Desc: Configure the OOF time counter threshold for entering LOF state.
Resolution of this threshold is one frame.
BitField Bits: [15:8]
--------------------------------------*/
#define cAf6_diag_glblofthr_reg_RxFrmLofSetThr_Mask                                                        cBit15_8
#define cAf6_diag_glblofthr_reg_RxFrmLofSetThr_Shift                                                              8
#define cAf6_diag_glblofthr_reg_RxFrmLofSetThr_RstVal                                                          0x18

/*--------------------------------------
BitField Name: RxFrmLofClrThr
BitField Type: RW
BitField Desc: Configure the In-frame time counter threshold for exiting LOF
state. Resolution of this threshold is one frame.
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_diag_glblofthr_reg_RxFrmLofClrThr_Mask                                                         cBit7_0
#define cAf6_diag_glblofthr_reg_RxFrmLofClrThr_Shift                                                              0
#define cAf6_diag_glblofthr_reg_RxFrmLofClrThr_RstVal                                                          0x18


/*------------------------------------------------------------------------------
Reg Name   : OCN Global Tx Framer Control
Reg Addr   : 0x00001
Reg Formula: 
    Where  : 
Reg Desc   : 
This is the global configuration register for the Tx Framer

------------------------------------------------------------------------------*/
#define cAf6_diag_glbtfm_reg_Base                                                                        0x00001

/*--------------------------------------
BitField Name: TxLineOofFrc
BitField Type: RW
BitField Desc: Enable/disable force OOF for 4 tx lines. Bit[0] for line 0.
BitField Bits: [19:16]
--------------------------------------*/
#define cAf6_diag_glbtfm_reg_TxLineOofFrc_Mask                                                            cBit19_16
#define cAf6_diag_glbtfm_reg_TxLineOofFrc_Shift                                                                  16
#define cAf6_diag_glbtfm_reg_TxLineOofFrc_RstVal                                                                0x0

#define cAf6_diag_glbtfm_reg_TxLineOofFrcMask(line)                                                            (cBit16 << (line))
#define cAf6_diag_glbtfm_reg_TxLineOofFrcshift(line)                                                           (16 + (line))

/*--------------------------------------
BitField Name: TxLineB1ErrFrc
BitField Type: RW
BitField Desc: Enable/disable force B1 error for 4 tx lines. Bit[0] for line 0.
BitField Bits: [15:12]
--------------------------------------*/
#define cAf6_diag_glbtfm_reg_TxLineB1ErrFrc_Mask                                                          cBit15_12
#define cAf6_diag_glbtfm_reg_TxLineB1ErrFrc_Shift                                                                12
#define cAf6_diag_glbtfm_reg_TxLineB1ErrFrc_RstVal                                                              0x0

#define cAf6_diag_glbtfm_reg_TxLineB1ErrFrcMask(line)                                                   (cBit12 << (line))
#define cAf6_diag_glbtfm_reg_TxLineB1ErrFrcshift(line)                                                  (12 + (line))
/*--------------------------------------
BitField Name: TxFrmScrEn
BitField Type: RW
BitField Desc: Enable/disable scrambling for 4 tx lines. Bit[0] for line 0. 1:
Enable 0: Disable
BitField Bits: [11:8]
--------------------------------------*/
#define cAf6_diag_glbtfm_reg_TxFrmScrEn_Mask                                                               cBit11_8
#define cAf6_diag_glbtfm_reg_TxFrmScrEn_Shift                                                                     8
#define cAf6_diag_glbtfm_reg_TxFrmScrEn_RstVal                                                                  0xf

#define cAf6_diag_glbtfm_reg_TxFrmScrEnMask(line)                                                   (cBit8 << (line))
#define cAf6_diag_glbtfm_reg_TxFrmScrEnshift(line)                                                  (8 + (line))
/*--------------------------------------
BitField Name: TxFrmStmOcnMode
BitField Type: RW
BitField Desc: STM rate mode for Tx of 4 lines, each line use 2 bits.Bits[1:0]
for line 0 0: OC48 1: OC12 2: OC3
BitField Bits: [7:0]
--------------------------------------*/
#define cAf6_diag_glbtfm_reg_TxFrmStmOcnMode_Mask                                                           cBit7_0
#define cAf6_diag_glbtfm_reg_TxFrmStmOcnMode_Shift                                                                0
#define cAf6_diag_glbtfm_reg_TxFrmStmOcnMode_RstVal                                                             0x0

#define cAf6_diag_glbtfm_reg_TxFrmStmOcnModeMask(line)                                                   (cBit1_0 << ((line)*2))
#define cAf6_diag_glbtfm_reg_TxFrmStmOcnModeshift(line)                                                  ((line)*2)

/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer Status
Reg Addr   : 0x00010 - 0x00040
Reg Formula: 0x00010 + 16*LineId
    Where  : 
           + $LineId(0-3)
Reg Desc   : 
Rx Framer status

------------------------------------------------------------------------------*/
#define cAf6_diag_rxfrmsta_Base                                                                          0x00010
#define cAf6_diag_rxfrmsta(LineId)                                                         (0x00010+16*(LineId))

/*--------------------------------------
BitField Name: OofCurStatus
BitField Type: RW
BitField Desc: OOF current status in the related line. 1: OOF state 0: Not OOF
state.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_diag_rxfrmsta_OofCurStatus_Mask                                                                  cBit2
#define cAf6_diag_rxfrmsta_OofCurStatus_Shift                                                                     2
#define cAf6_diag_rxfrmsta_OofCurStatus_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: LofCurStatus
BitField Type: RW
BitField Desc: LOF current status in the related line. 1: LOF state 0: Not LOF
state.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_diag_rxfrmsta_LofCurStatus_Mask                                                                  cBit1
#define cAf6_diag_rxfrmsta_LofCurStatus_Shift                                                                     1
#define cAf6_diag_rxfrmsta_LofCurStatus_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: LosCurStatus
BitField Type: RW
BitField Desc: LOS current status in the related line. 1: LOS state 0: Not LOS
state.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_diag_rxfrmsta_LosCurStatus_Mask                                                                  cBit0
#define cAf6_diag_rxfrmsta_LosCurStatus_Shift                                                                     0
#define cAf6_diag_rxfrmsta_LosCurStatus_RstVal                                                                  0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer Sticky
Reg Addr   : 0x00011 - 0x00041
Reg Formula: 0x00011 + 16*LineId
    Where  : 
           + $LineId(0-3)
Reg Desc   : 
Rx Framer sticky

------------------------------------------------------------------------------*/
#define cAf6_diag_rxfrmstk_Base                                                                          0x00011
#define cAf6_diag_rxfrmstk(LineId)                                                         (0x00011+16*(LineId))

/*--------------------------------------
BitField Name: Oc3AlgErrStk
BitField Type: W1C
BitField Desc: Set 1 while any error detected at OC3 Aligner engine.
BitField Bits: [4]
--------------------------------------*/
#define cAf6_diag_rxfrmstk_Oc3AlgErrStk_Mask                                                                  cBit4
#define cAf6_diag_rxfrmstk_Oc3AlgErrStk_Shift                                                                     4
#define cAf6_diag_rxfrmstk_Oc3AlgErrStk_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: ClkMonErrStk
BitField Type: W1C
BitField Desc: Set 1 while any error detected at Error clock monitor engine.
BitField Bits: [3]
--------------------------------------*/
#define cAf6_diag_rxfrmstk_ClkMonErrStk_Mask                                                                  cBit3
#define cAf6_diag_rxfrmstk_ClkMonErrStk_Shift                                                                     3
#define cAf6_diag_rxfrmstk_ClkMonErrStk_RstVal                                                                  0x0

/*--------------------------------------
BitField Name: OofStateChgStk
BitField Type: W1C
BitField Desc: Set 1 while OOF state change event happens in the related line.
BitField Bits: [2]
--------------------------------------*/
#define cAf6_diag_rxfrmstk_OofStateChgStk_Mask                                                                cBit2
#define cAf6_diag_rxfrmstk_OofStateChgStk_Shift                                                                   2
#define cAf6_diag_rxfrmstk_OofStateChgStk_RstVal                                                                0x0

/*--------------------------------------
BitField Name: LofStateChgStk
BitField Type: W1C
BitField Desc: Set 1 while LOF state change event happens in the related line.
BitField Bits: [1]
--------------------------------------*/
#define cAf6_diag_rxfrmstk_LofStateChgStk_Mask                                                                cBit1
#define cAf6_diag_rxfrmstk_LofStateChgStk_Shift                                                                   1
#define cAf6_diag_rxfrmstk_LofStateChgStk_RstVal                                                                0x0

/*--------------------------------------
BitField Name: LosStateChgStk
BitField Type: W1C
BitField Desc: Set 1 while LOS state change event happens in the related line.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_diag_rxfrmstk_LosStateChgStk_Mask                                                                cBit0
#define cAf6_diag_rxfrmstk_LosStateChgStk_Shift                                                                   0
#define cAf6_diag_rxfrmstk_LosStateChgStk_RstVal                                                                0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer B1 error counter read only
Reg Addr   : 0x00012 - 0x00042
Reg Formula: 0x00012 + 16*LineId
    Where  : 
           + $LineId(0-3)
Reg Desc   : 
Rx Framer B1 error counter read only

------------------------------------------------------------------------------*/
#define cAf6_diag_rxfrmb1cntro_Base                                                                      0x00012
#define cAf6_diag_rxfrmb1cntro(LineId)                                                     (0x00012+16*(LineId))

/*--------------------------------------
BitField Name: RxFrmB1ErrRo
BitField Type: RO
BitField Desc: Number of B1 error - read only
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_diag_rxfrmb1cntro_RxFrmB1ErrRo_Mask                                                           cBit31_0
#define cAf6_diag_rxfrmb1cntro_RxFrmB1ErrRo_Shift                                                                 0
#define cAf6_diag_rxfrmb1cntro_RxFrmB1ErrRo_RstVal                                                              0x0


/*------------------------------------------------------------------------------
Reg Name   : OCN Rx Framer B1 error counter read to clear
Reg Addr   : 0x00013 - 0x00043
Reg Formula: 0x00013 + 16*LineId
    Where  : 
           + $LineId(0-3)
Reg Desc   : 
Rx Framer B1 error counter read to clear

------------------------------------------------------------------------------*/
#define cAf6_diag_rxfrmb1cntr2c_Base                                                                     0x00013
#define cAf6_diag_rxfrmb1cntr2c(LineId)                                                    (0x00013+16*(LineId))
#define cAf6_diag_rxfrmb1cntr2c_WidthVal                                                                      32
#define cAf6_diag_rxfrmb1cntr2c_WriteMask                                                                    0x0

/*--------------------------------------
BitField Name: RxFrmB1ErrR2C
BitField Type: RC
BitField Desc: Number of B1 error - read to clear
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_diag_rxfrmb1cntr2c_RxFrmB1ErrR2C_Mask                                                         cBit31_0
#define cAf6_diag_rxfrmb1cntr2c_RxFrmB1ErrR2C_Shift                                                               0
#define cAf6_diag_rxfrmb1cntr2c_RxFrmB1ErrR2C_RstVal                                                            0x0

#endif /* _AF6_REG_AF6CNC0011_OC192_OC48_OC12_OC3_RD_DIAG_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60290021ModuleOcn.h
 * 
 * Created Date: Jul 12, 2016
 *
 * Description : OCN module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULEOCNINTERNAL_H_
#define _THA60290021MODULEOCNINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/sdh/ThaSdhLine.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcnInternal.h"
#include "Tha60290021ModuleOcnDiag.h"
#include "Tha60290021ModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mFaceplateLineLocalInGroup(faceplateLineId) ((faceplateLineId) % 8)

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModuleOcnMethods
    {
    uint32 (*NumFaceplateSlices)(Tha60290021ModuleOcn self);
    uint32 (*MateXcStartLineId)(Tha60290021ModuleOcn self);
    uint32 (*FaceplateXcStartLineId)(Tha60290021ModuleOcn self);
    uint32 (*TerminatedXcStartLineId)(Tha60290021ModuleOcn self);
    eBool (*HasOverheadCpuControl)(Tha60290021ModuleOcn self);
    eBool (*HasRoleStatus)(Tha60290021ModuleOcn self);
    uint32 (*NumXcLines)(Tha60290021ModuleOcn self);
    eBool (*HasDiagnosticIp)(Tha60290021ModuleOcn self);

    /* Faceplate */
    uint32 (*FaceplateGroupOffset)(Tha60290021ModuleOcn self, uint32 groupId);
    uint32 (*FaceplateLineDefaultOffset)(Tha60290021ModuleOcn self, uint32 faceplateLineId);
    uint32 (*FaceplateLineTxDefaultOffset)(Tha60290021ModuleOcn self, uint32 faceplateLineId);
    eAtRet (*FaceplateStsMappingDefaultSet)(Tha60290021ModuleOcn self);

    /* ID conversion */
    eAtRet (*FaceplateStsIdSw2Hw)(Tha60290021ModuleOcn self, AtSdhChannel sdhChannel, uint8 swSts, uint8* sliceId, uint8* hwStsInSlice);

    /* STS-192c */
    eBool (*Sts192cIsSupported)(Tha60290021ModuleOcn self);

    /* Registers */
    uint32 (*TohMonitoringChannelControlRegAddr)(Tha60290021ModuleOcn self);
    uint32 (*TxFramerPerChannelControlRegAddr)(Tha60290021ModuleOcn self);
    uint32 (*FaceplateLineRxOverheadByteRegAddr)(Tha60290021ModuleOcn self, uint32 overheadByte);

    /* Z1/Z2 bytes */
    uint8 (*FaceplateLineRxZnRegValue2Position)(Tha60290021ModuleOcn self, uint32 regVal);
    uint32 (*FaceplateLineRxZnPosition2RegValue)(Tha60290021ModuleOcn self, uint8 sts1);
    }tTha60290021ModuleOcnMethods;

typedef struct tTha60290021ModuleOcn
    {
    tTha60210011ModuleOcn super;
    const tTha60290021ModuleOcnMethods *methods;

    /* Private data */
    eTha6029LineSide pohSide;
    }tTha60290021ModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290021ModuleOcnObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULEOCNINTERNAL_H_ */


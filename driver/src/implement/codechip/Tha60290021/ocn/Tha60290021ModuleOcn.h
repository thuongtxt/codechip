/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60290021ModuleOcn.h
 * 
 * Created Date: Jul 12, 2016
 *
 * Description : OCN module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _ATSDK_THA60290021MODULEOCN_H_
#define _ATSDK_THA60290021MODULEOCN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/sdh/ThaSdhLine.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "Tha60290021ModuleOcnDiag.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef enum eTha6029LineSide
    {
    cTha6029LineSideUnknown,
    cTha6029LineSideFaceplate,
    cTha6029LineSideMate
    }eTha6029LineSide;

typedef struct tTha60290021ModuleOcn * Tha60290021ModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290021ModuleOcnNew(AtDevice device);

const char **Tha60290021ModuleOcnAllInternalRamsDescription(AtModule self, uint32 *numRams);

eBool Tha60290021ModuleOcnHasDiagnosticIp(Tha60290021ModuleOcn self);
void Tha60290021ModuleOcnHwFlush(AtModule self);
uint32 Tha60290021ModuleOcnNumFaceplateSlices(Tha60290021ModuleOcn self);
uint8 Tha60290021ModuleOcnFaceplateSliceLocalId(ThaModuleOcn self, uint8 sliceId);

eAtRet Tha60290021ModuleOcnFaceplateLineRateSet(ThaModuleOcn self, uint32 lineId, eAtSdhLineRate rate);
eAtRet Tha60290021ModuleOcnFaceplateOc192Enable(ThaModuleOcn self, uint32 lineId, eBool enabled);
eAtRet Tha60290021ModuleOcnFaceplateLineScrambleEnable(ThaModuleOcn self, uint32 lineId, eBool enabled);
eBool Tha60290021ModuleOcnFaceplateLineScrambleIsEnabled(ThaModuleOcn self, uint32 lineId);
eBool Tha60290021ModuleOcnSts192cIsSupported(Tha60290021ModuleOcn self);

/* Face-plate line enabling */
eAtRet Tha60290021ModuleOcnFaceplateLineEnable(ThaModuleOcn self, uint32 lineId, eBool enabled);
eBool Tha60290021ModuleOcnFaceplateLineIsEnabled(ThaModuleOcn self, uint32 lineId);
eAtRet Tha60290021ModuleOcnFaceplateLineRxEnable(ThaModuleOcn self, uint32 lineId, eBool enabled);
eBool Tha60290021ModuleOcnFaceplateLineRxIsEnabled(ThaModuleOcn self, uint32 lineId);
eAtRet Tha60290021ModuleOcnFaceplateLineTxEnable(ThaModuleOcn self, uint32 lineId, eBool enabled);
eBool Tha60290021ModuleOcnFaceplateLineTxIsEnabled(ThaModuleOcn self, uint32 lineId);

/* Faceplate STS mapping */
eAtRet Tha60290021ModuleOcnFaceplateStsAssign(ThaModuleOcn self, uint32 faceplateLineId, uint32 sts48Id, uint32 sts12Id, uint32 sts3Id);

/* Tx DCC Insertion */
eAtRet Tha60290021ModuleOcnFaceplateLineTxDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable);
eBool Tha60290021ModuleOcnFaceplateLineTxDccInsByOhBusIsEnabled(ThaModuleOcn self, uint32 lineId);

/* K-Byte */
eAtRet Tha60290021ModuleOcnFaceplateLineTxExtensionKbyteEnable(ThaModuleOcn self, uint32 lineId, eBool enable);
eAtRet Tha60290021ModuleOcnFaceplateLineRxExtensionKbyteEnable(ThaModuleOcn self, uint32 lineId, eBool enable);
eBool Tha60290021ModuleOcnFaceplateLineTxExtensionKbyteIsEnabled(ThaModuleOcn self, uint32 lineId);
eBool Tha60290021ModuleOcnFaceplateLineRxExtensionKbyteIsEnabled(ThaModuleOcn self, uint32 lineId);
eAtRet Tha60290021ModuleOcnFaceplateLineKbyteTxAlarmSuppress(ThaModuleOcn self, uint32 lineId, eBool enable);
eBool Tha60290021ModuleOcnFaceplateLineKbyteTxAlarmIsSuppressed(ThaModuleOcn self, uint32 lineId);
eAtRet Tha60290021ModuleOcnFaceplateLineKbyteRxAllOnesMark(ThaModuleOcn self, uint32 lineId, eBool enable);
eThaSdhLineKByteSource Tha60290021ModuleOcnFaceplateLineTxKbyteSourceGet(ThaModuleOcn self, uint32 lineId);
eAtRet Tha60290021ModuleOcnFaceplateLineTxKbyteSourceSet(ThaModuleOcn self, uint32 lineId, eThaSdhLineKByteSource kbyteSource);

eAtRet Tha60290021ModuleOcnFaceplateLineTxKbyteExtensionPostionSet(ThaModuleOcn self, uint32 lineId, eThaSdhLineExtendedKBytePosition extKbytePos);
eThaSdhLineExtendedKBytePosition Tha60290021ModuleOcnFaceplateLineTxKbyteExtensionPostionGet(ThaModuleOcn self, uint32 lineId);
eAtRet Tha6029ModuleOcnSohOverEthInit(ThaModuleOcn self);

eAtRet Tha60290021ModuleOcnFaceplateLineTxB1Force(ThaModuleOcn self, uint32 lineId, eBool enable);
eBool Tha60290021ModuleOcnFaceplateLineTxB1IsForced(ThaModuleOcn self, uint32 lineId);
eAtRet Tha60290021ModuleOcnFaceplateLineTxOofForce(ThaModuleOcn self, uint32 lineId, eBool enable);
eBool Tha60290021ModuleOcnFaceplateLineTxOofIsForced(ThaModuleOcn self, uint32 lineId);

eAtModuleSdhRet Tha60290021ModuleOcnFaceplateLineTxOverheadByteSet(ThaModuleOcn self, uint32 lineId, uint32 overheadByte, uint8 value);
uint8 Tha60290021ModuleOcnFaceplateLineTxOverheadByteGet(ThaModuleOcn self, uint32 lineId, uint32 overheadByte);
uint8 Tha60290021ModuleOcnFaceplateLineRxOverheadByteGet(ThaModuleOcn self, uint32 lineId, uint32 overheadByte);
eBool Tha60290021ModuleOcnFacelateLineTxOverheadByteIsSupported(ThaModuleOcn self, uint32 faceplateLineId, uint32 overheadByte);
eBool Tha60290021ModuleOcnFacelateLineRxOverheadByteIsSupported(ThaModuleOcn self, uint32 faceplateLineId, uint32 overheadByte);
eAtRet Tha60290021ModuleOcnFaceplateLineRxZ1PositionSet(ThaModuleOcn self, uint32 lineId, uint8 sts1);
uint8 Tha60290021ModuleOcnFaceplateLineRxZ1PositionGet(ThaModuleOcn self, uint32 lineId);
eAtRet Tha60290021ModuleOcnFaceplateLineRxZ2PositionSet(ThaModuleOcn self, uint32 lineId, uint8 sts1);
uint8 Tha60290021ModuleOcnFaceplateLineRxZ2PositionGet(ThaModuleOcn self, uint32 lineId);

eAtRet Tha60290021ModuleOcnPohProcessorSideSet(ThaModuleOcn self, eTha6029LineSide side);
eTha6029LineSide Tha60290021ModuleOcnPohProcessorSideGet(ThaModuleOcn self);
eAtRet Tha60290021ModuleOcnLineRateExtraSet(ThaModuleOcn self, uint32 lineId, eAtSdhLineRate rate);

uint32 Tha60290021ModuleOcnFaceplateLineLocalId(ThaModuleOcn self, uint32 lineId);
uint32 Tha60290021ModuleOcnFaceplateEnableMask(ThaModuleOcn self, uint32 lineId);

/* AIS fordwarding */
eAtRet Tha60290021ModuleOcnFaceplateLineAisFordwardingEnable(ThaModuleOcn self, uint32 lineId, uint32 alarmType, eBool enable);
eBool Tha60290021ModuleOcnFaceplateLineAisFordwardingIsEnabled(ThaModuleOcn self, uint32 lineId, uint32 alarmType);

/* RDI backward */
eAtRet Tha60290021ModuleOcnFaceplateLineRdiEnable(ThaModuleOcn self, uint32 lineId, uint32 alarmType, eBool enable);
eBool Tha60290021ModuleOcnFaceplateLineRdiIsEnabled(ThaModuleOcn self, uint32 lineId, uint32 alarmType);

/* Registers */
uint32 Tha60290021ModuleOcnFaceplateLineGroupOffset(ThaModuleOcn self, uint32 faceplateLineId);
uint32 Tha60290021ModuleOcnNumFaceplateLineGroups(ThaModuleOcn self);
uint32 Tha60290021ModuleOcnNumFaceplateLinesPerGroup(ThaModuleOcn self);
uint32 Tha60290021ModuleOcnFaceplateGroupOffset(ThaModuleOcn self, uint32 groupId);
uint32 Tha60290021ModuleOcnFaceplateChannelGroupOffset(ThaModuleOcn self, AtSdhChannel channel);
uint32 Tha60290021ModuleOcnFaceplateLineGroup(ThaModuleOcn self, uint32 faceplateLineId);
uint32 Tha60290021ModuleOcnFaceplateLineAddressWithLocalAddress(ThaModuleOcn self, uint32 faceplateLineId, uint32 localAddress);
uint32 Tha60290021ModuleOcnFaceplateLineTxDefaultOffset(ThaModuleOcn self, uint32 faceplateLineId);

uint32 Tha60290021ModuleOcnFaceplateGlobalRxFramerControl(ThaModuleOcn self, uint32 lineId);
uint32 Tha60290021ModuleOcnFaceplateGlobalTxFramerControl(ThaModuleOcn self, uint32 lineId);
uint32 Tha60290021ModuleOcnTohMonitoringChannelControlRegAddr(ThaModuleOcn self);
uint32 Tha60290021ModuleOcnTxFramerPerChannelControlRegAddr(ThaModuleOcn self);

/* OCN Kbyte trigger to Kbyte DCC processor to send packet to SGMII */
eAtRet Tha60290021ModuleOcnFacePlateKbyteOcnToKbyteDccProcessorTrigger(ThaModuleOcn self, uint32 facePlateLineId);

#ifdef __cplusplus
}
#endif
#endif /* _ATSDK_THA60290021MODULEOCN_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha60290021ModuleOcnSohOverEth.c
 *
 * Created Date: Nov 9, 2016
 *
 * Description : SOH over ETH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtModuleInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/ocn/ThaModuleOcnInternal.h"
#include "../../Tha60210031/sdh/Tha60210031SdhLine.h"
#include "../pw/Tha60290021DccKbyteReg.h"
#include "Tha60290021ModuleOcn.h"
#include "Tha60290021ModuleOcnReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 AddressWithLocalAddress(ThaModuleOcn self, uint32 localAddress)
    {
    return ThaModuleOcnSohOverEthBaseAddress(self) + localAddress;
    }

static eAtRet AllDccsDisable(ThaModuleOcn self)
    {
    mModuleHwWrite(self, AddressWithLocalAddress(self, cAf6Reg_upen_dcctx_enacid_Base), 0x0);
    return cAtOk;
    }

static uint32 NumbDccHdlcChannels(ThaModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModuleSdh sdhModue = (ThaModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    return ThaModuleSdhMaxNumDccHdlcChannels(sdhModue);
    }

static eAtRet DccMappingInit(ThaModuleOcn self)
    {
    uint32 label;
    uint32 regBase = AddressWithLocalAddress(self, ThaModuleOcnUpenDccdecBase(self));
    uint32 maxChannels = NumbDccHdlcChannels(self);

    /* Default Mapping, but let channels be disabled */
    for (label = 0; label < maxChannels; label++)
        mModuleHwWrite(self, label + regBase, label);

    return cAtOk;
    }

static eAtRet DccBufferLoopbackEnable(ThaModuleOcn self, eBool enabled)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_upen_loopen_Base);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_upen_loopen_dcc_buffer_loop_en_, enabled ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet SohOverEthInit(ThaModuleOcn self)
    {
    eAtRet ret = cAtOk;

    ret |= AllDccsDisable(self);
    ret |= DccMappingInit(self);

    if (mMethodsGet(self)->NeedIssolateDcc(self))
        ret |= DccBufferLoopbackEnable(self, cAtTrue);

    return ret;
    }

static uint32 FaceplateTxDccEnableMask(ThaModuleOcn self, uint32 lineId)
    {
    return cBit8 << Tha60290021ModuleOcnFaceplateLineLocalId(self, lineId);
    }

static uint32 FaceplateTxDccEnableShift(ThaModuleOcn self, uint32 lineId)
    {
    return (Tha60290021ModuleOcnFaceplateLineLocalId(self, lineId) + 8);
    }

static eBool FaceplateLineTxDccIsEnabledCheckWithRegister(ThaModuleOcn self, uint32 lineId, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & FaceplateTxDccEnableMask(self, lineId)) ? cAtTrue : cAtFalse;
    }

static uint32 FaceplateTxInsControlRegister(ThaModuleOcn self, uint32 lineId)
    {
    return Tha60290021ModuleOcnFaceplateLineAddressWithLocalAddress(self, lineId, cAf6Reg_glbrdiinsthr_reg_Base);
    }

static eAtRet FaceplateLineTxDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    uint32 regAddr = FaceplateTxInsControlRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal,
              FaceplateTxDccEnableMask(self, lineId),
              FaceplateTxDccEnableShift(self, lineId),
              enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool FaceplateLineTxDccInsByOhBusIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateTxInsControlRegister(self, lineId);
    return FaceplateLineTxDccIsEnabledCheckWithRegister(self, lineId, regAddr);
    }

static uint32 BlbtxtohbusctlShiftWithLocalBit(ThaModuleOcn self, uint32 lineId, uint32 localBit)
    {
    AtUnused(self);
    return (Tha60290021ModuleOcnFaceplateLineLocalId(self, lineId) * 4) + localBit;
    }

static uint32 BlbtxtohbusctlMaskWithLocalBit(ThaModuleOcn self, uint32 lineId, uint32 localBit)
    {
    uint32 localId = Tha60290021ModuleOcnFaceplateLineLocalId(self, lineId);
    uint32 shift = BlbtxtohbusctlShiftWithLocalBit(self, localId, localBit);
    return cBit0 << shift;
    }

static uint32 FaceplateKbyteExtensionEnableShift(ThaModuleOcn self, uint32 lineId)
    {
    return BlbtxtohbusctlShiftWithLocalBit(self, lineId, 1);
    }

static uint32 FaceplateKbyteExtensionEnableMask(ThaModuleOcn self, uint32 lineId)
    {
    return BlbtxtohbusctlMaskWithLocalBit(self, lineId, 1);
    }

static uint32 FaceplateKbyteExtensionPostionShift(ThaModuleOcn self, uint32 lineId)
    {
    return BlbtxtohbusctlShiftWithLocalBit(self, lineId, 0);
    }

static uint32 FaceplateKbyteExtensionPostionMask(ThaModuleOcn self, uint32 lineId)
    {
    return BlbtxtohbusctlMaskWithLocalBit(self, lineId, 0);
    }

static eAtRet FaceplateLineKbyteExtensionEnableWithRegister(ThaModuleOcn self, uint32 lineId, eBool enable, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal,
              FaceplateKbyteExtensionEnableMask(self, lineId),
              FaceplateKbyteExtensionEnableShift(self, lineId),
              enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool FaceplateLineKbyteExtensionIsEnabledWithRegister(ThaModuleOcn self, uint32 lineId, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & FaceplateKbyteExtensionEnableMask(self, lineId)) ? cAtTrue : cAtFalse;
    }

static uint32 FaceplateTxKbyteExtensionRegister(ThaModuleOcn self, uint32 lineId)
    {
    return Tha60290021ModuleOcnFaceplateLineAddressWithLocalAddress(self, lineId, cAf6Reg_glbtxtohbusctl_reg_Base);
    }

static uint32 FaceplateRxKbyteExtensionRegister(ThaModuleOcn self, uint32 lineId)
    {
    return Tha60290021ModuleOcnFaceplateLineAddressWithLocalAddress(self, lineId, cAf6Reg_glbrxtohbusctl_reg_Base);
    }

static uint32 ExtendKbytePositionSw2Hw(uint32 extKbytePos)
    {
    switch (extKbytePos)
        {
        case cThaSdhLineExtendedKBytePositionD1Sts4:  return 1;
        case cThaSdhLineExtendedKBytePositionD1Sts10: return 0;
        default: return cInvalidUint32;
        }
    }

static uint32 ExtendKbytePositionHw2Sw(uint32 extKbytePos)
    {
    switch (extKbytePos)
        {
        case 0:  return cThaSdhLineExtendedKBytePositionD1Sts10;
        case 1:  return cThaSdhLineExtendedKBytePositionD1Sts4;
        default: return cThaSdhLineExtendedKBytePositionInvalid;
        }
    }

static eAtRet FaceplateLineTxKbyteExtensionPostionSetWithRegister(ThaModuleOcn self,
                                                                  uint32 lineId,
                                                                  uint32 extKbytePos,
                                                                  uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal,
              FaceplateKbyteExtensionPostionMask(self, lineId),
              FaceplateKbyteExtensionPostionShift(self, lineId),
              extKbytePos);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 FaceplateLineTxKbyteExtensionGetWithRegister(ThaModuleOcn self, uint32 lineId, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 extKbytePos;
    mFieldGet(regVal,
              FaceplateKbyteExtensionPostionMask(self, lineId),
              FaceplateKbyteExtensionPostionShift(self, lineId),
              uint32,
              &extKbytePos);
    return extKbytePos;
    }

static eAtRet FaceplateLineTxKbyteExtensionPostionSet(ThaModuleOcn self, uint32 lineId, uint32 extKbytePos)
    {
    uint32 regAddr = FaceplateTxKbyteExtensionRegister(self, lineId);
    uint32 hwExtKbytePos = ExtendKbytePositionSw2Hw(extKbytePos);
    if (hwExtKbytePos == cInvalidUint32)
        return cAtErrorInvlParm;

    return FaceplateLineTxKbyteExtensionPostionSetWithRegister(self, lineId, hwExtKbytePos, regAddr);
    }

static uint32 FaceplateLineTxKbyteExtensionPostionGet(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateTxKbyteExtensionRegister(self, lineId);
    uint32 hwExtKbytePos = FaceplateLineTxKbyteExtensionGetWithRegister(self, lineId, regAddr);
    return ExtendKbytePositionHw2Sw(hwExtKbytePos);
    }

static uint32 FaceplateKbyteTxAlarmSuppressShift(ThaModuleOcn self, uint32 lineId)
    {
    return BlbtxtohbusctlShiftWithLocalBit(self, lineId, 2);
    }

static uint32 FaceplateKbyteTxAlarmSuppressMask(ThaModuleOcn self, uint32 lineId)
    {
    return BlbtxtohbusctlMaskWithLocalBit(self, lineId, 2);
    }

static uint32 FaceplateKbyteRxAllOnesMarkShift(ThaModuleOcn self, uint32 lineId)
    {
    return (Tha60290021ModuleOcnFaceplateLineLocalId(self, lineId) * 4) + 2;
    }

static uint32 FaceplateKbyteRxAllOnesMarkMask(ThaModuleOcn self, uint32 lineId)
    {
    uint32 shift = FaceplateKbyteRxAllOnesMarkShift(self, lineId);
    return cBit1 << shift;
    }

static eAtRet FaceplateLineKbyteRxAllOnesMarkWithRegister(ThaModuleOcn self, uint32 lineId, eBool enable, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal,
              FaceplateKbyteRxAllOnesMarkMask(self, lineId),
              FaceplateKbyteRxAllOnesMarkShift(self, lineId),
              enable ? 0 : 1);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet FaceplateLineTxExtensionKbyteEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    uint32 regAddr = FaceplateTxKbyteExtensionRegister(self, lineId);
    return FaceplateLineKbyteExtensionEnableWithRegister(self, lineId, enable, regAddr);
    }

static eBool FaceplateLineTxExtensionKbyteIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateTxKbyteExtensionRegister(self, lineId);
    return FaceplateLineKbyteExtensionIsEnabledWithRegister(self, lineId, regAddr);
    }

static eAtRet FaceplateLineRxExtensionKbyteEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    uint32 regAddr = FaceplateRxKbyteExtensionRegister(self, lineId);
    return FaceplateLineKbyteExtensionEnableWithRegister(self, lineId, enable, regAddr);
    }

static eBool FaceplateLineRxExtensionKbyteIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateRxKbyteExtensionRegister(self, lineId);
    return FaceplateLineKbyteExtensionIsEnabledWithRegister(self, lineId, regAddr);
    }

static eAtRet FaceplateLineKbyteTxAlarmSuppress(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    uint32 regAddr = FaceplateTxKbyteExtensionRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal,
              FaceplateKbyteTxAlarmSuppressMask(self, lineId),
              FaceplateKbyteTxAlarmSuppressShift(self, lineId),
              enable ? 0 : 1);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool FaceplateLineKbyteTxAlarmIsSuppressed(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateTxKbyteExtensionRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & FaceplateKbyteTxAlarmSuppressMask(self, lineId)) ? cAtFalse : cAtTrue;
    }

static eAtRet FaceplateLineKbyteRxAllOnesMark(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    uint32 regAddr = FaceplateRxKbyteExtensionRegister(self, lineId);
    return FaceplateLineKbyteRxAllOnesMarkWithRegister(self, lineId, enable, regAddr);
    }

static eAtModuleSdhRet TxFramerPerChannelControlRegAddr(ThaModuleOcn self, uint32 lineId)
    {
    uint32 address = Tha60290021ModuleOcnTxFramerPerChannelControlRegAddr(self);
    uint32 offset = Tha60290021ModuleOcnFaceplateLineTxDefaultOffset(self, lineId);
    return address + offset;
    }

static eAtModuleSdhRet TxKbyteSourceSet(ThaModuleOcn self, uint32 lineId, eThaSdhLineKByteSource kbyteSource)
    {
    uint32 regAddr = TxFramerPerChannelControlRegAddr(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_tfmregctl_OCNTxApsDis_, (kbyteSource == cThaSdhLineKByteSourceCpu) ? 0 : 1);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eThaSdhLineKByteSource TxKbyteSourceGet(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = TxFramerPerChannelControlRegAddr(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 txAps = mRegField(regVal, cAf6_tfmregctl_OCNTxApsDis_);
    return (txAps == 0) ? cThaSdhLineKByteSourceCpu : cThaSdhLineKByteSourceEth;
    }

eThaSdhLineExtendedKBytePosition Tha60290021ModuleOcnFaceplateLineTxKbyteExtensionPostionGet(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineTxKbyteExtensionPostionGet(self, lineId);
    return cThaSdhLineExtendedKBytePositionInvalid;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineTxKbyteExtensionPostionSet(ThaModuleOcn self, uint32 lineId, eThaSdhLineExtendedKBytePosition extKbytePos)
    {
    if (self)
        return FaceplateLineTxKbyteExtensionPostionSet(self, lineId, extKbytePos);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineTxExtensionKbyteEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateLineTxExtensionKbyteEnable(self, lineId, enable);

    return cAtErrorObjectNotExist;
    }

eBool Tha60290021ModuleOcnFaceplateLineTxExtensionKbyteIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineTxExtensionKbyteIsEnabled(self, lineId);

    return cAtFalse;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineRxExtensionKbyteEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateLineRxExtensionKbyteEnable(self, lineId, enable);
    return cAtErrorObjectNotExist;
    }

eBool Tha60290021ModuleOcnFaceplateLineRxExtensionKbyteIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineRxExtensionKbyteIsEnabled(self, lineId);

    return cAtFalse;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineTxDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateLineTxDccInsByOhBusEnable(self, lineId, enable);
    return cAtErrorObjectNotExist;
    }

eBool Tha60290021ModuleOcnFaceplateLineTxDccInsByOhBusIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineTxDccInsByOhBusIsEnabled(self, lineId);
    return cAtFalse;
    }

eAtRet Tha6029ModuleOcnSohOverEthInit(ThaModuleOcn self)
    {
    if (self)
        return SohOverEthInit(self);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineKbyteTxAlarmSuppress(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateLineKbyteTxAlarmSuppress(self, lineId, enable);
    return cAtErrorObjectNotExist;
    }

eBool Tha60290021ModuleOcnFaceplateLineKbyteTxAlarmIsSuppressed(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineKbyteTxAlarmIsSuppressed(self, lineId);
    return cAtFalse;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineKbyteRxAllOnesMark(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateLineKbyteRxAllOnesMark(self, lineId, enable);

    return cAtErrorObjectNotExist;
    }

eThaSdhLineKByteSource Tha60290021ModuleOcnFaceplateLineTxKbyteSourceGet(ThaModuleOcn self, uint32 lineId)
    {
	if (self)
		return TxKbyteSourceGet(self, lineId);

    return cThaSdhLineKByteSourceUnknown;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineTxKbyteSourceSet(ThaModuleOcn self, uint32 lineId, eThaSdhLineKByteSource kbyteSource)
    {
	if (self)
		return TxKbyteSourceSet(self, lineId, kbyteSource);
    return cAtErrorModeNotSupport;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60290021ModuleOcnDiag.h
 * 
 * Created Date: Sep 24, 2016
 *
 * Description : OCN diagnostic
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _OCN_THA60290021MODULEOCNDIAG_H_
#define _OCN_THA60290021MODULEOCNDIAG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtModuleSdhRet Tha60290021ModuleOcnDiagLineRateSet(AtSdhLine self, eAtSdhLineRate rate);
eAtSdhLineRate Tha60290021ModuleOcnDiagLineRateGet(AtSdhLine self);
eAtRet Tha60290021ModuleOcnDiagLineDefaultSet(AtSdhLine self);
uint32 Tha60290021ModuleOcnDiagLineAlarmGet(AtSdhLine self);
uint32 Tha60290021ModuleOcnDiagLineAlarmHistoryRead2Clear(AtSdhLine self, eBool r2c);
uint32 Tha60290021ModuleOcnDiagLineB1CounterRead2Clear(AtSdhLine self, eBool read2Clear);
eAtRet Tha60290021ModuleOcnDiagLineTxAlarmForce(AtSdhLine self, uint32 alarmType, eBool force);
uint32 Tha60290021ModuleOcnDiagLineTxForcedAlarmGet(AtSdhLine self);
eAtRet Tha60290021ModuleOcnDiagLineB1Force(AtSdhLine self, eBool force);
eBool Tha60290021ModuleOcnDiagLineB1IsForced(AtSdhLine self);
eAtRet Tha60290021ModuleOcnDiagLineScrambleEnable(AtSdhLine self, eBool enable);
eBool Tha60290021ModuleOcnDiagLineScrambleIsEnabled(AtSdhLine self);
#ifdef __cplusplus
}
#endif
#endif /* _OCN_THA60290021MODULEOCNDIAG_H_ */


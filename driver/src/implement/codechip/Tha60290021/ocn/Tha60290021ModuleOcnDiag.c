/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290021ModuleOcnDiag.c
 *
 * Created Date: Sep 24, 2016
 *
 * Description : Diagnostic for SDH Lines
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "commacro.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/sdh/ThaSdhLine.h"
#include "../../../default/man/ThaDevice.h"
#include "../ocn/Tha6029OcnDiagReg.h"
#include "../man/Tha60290021Device.h"
#include "Tha60290021ModuleOcnDiag.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mLocalIdInGroup(line) LocalIdInGroup((AtChannel)line)
#define mBaseAddress(line) BaseAddress((AtChannel)line)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumLinesPerGroup(AtChannel self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 LocalIdInGroup(AtChannel self)
    {
    uint32 localId = ThaModuleSdhLineLocalId((AtSdhLine)self);
    return (localId % NumLinesPerGroup(self));
    }

static uint32 BaseAddress(AtChannel self)
    {
    return ThaSdhLineDiagnosticBaseAddress((ThaSdhLine)self);
    }

static uint32 RateSw2Hw(eAtSdhLineRate rate)
    {
    switch ((uint32)rate)
        {
        case cAtSdhLineRateStm16 : return 0;
        case cAtSdhLineRateStm4  : return 1;
        case cAtSdhLineRateStm1  : return 2;
        case cAtSdhLineRateStm64 : return 3;
        default:
            return 0xff;
        }
    }

static eAtSdhLineRate RateHw2Sw(uint32 hwRate)
    {
    switch (hwRate)
        {
        case 0: return cAtSdhLineRateStm16;
        case 1: return cAtSdhLineRateStm4;
        case 2: return cAtSdhLineRateStm1;
        case 3: return cAtSdhLineRateStm64;
        default:
            return cAtSdhLineRateInvalid;
        }
    }

static eAtModuleSdhRet RateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    uint32 lineId = mLocalIdInGroup(self);
    uint32 regAddr = cAf6_diag_glbtfm_reg_Base + mBaseAddress(self);
    uint32 regVal=  mChannelHwRead(self, regAddr, cThaModuleOcn);

    mFieldIns(&regVal,
              cAf6_diag_glbtfm_reg_TxFrmStmOcnModeMask(lineId),
              cAf6_diag_glbtfm_reg_TxFrmStmOcnModeshift(lineId),
              RateSw2Hw(rate));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    /* Apply same configure for Rx */
    regAddr = cAf6_diag_glbrfm_reg_RxFrm_Base + mBaseAddress(self);
    regVal=  mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal,
              cAf6_diag_glbtfm_reg_TxFrmStmOcnModeMask(lineId),
              cAf6_diag_glbtfm_reg_TxFrmStmOcnModeshift(lineId),
              RateSw2Hw(rate));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eAtSdhLineRate RateGet(AtSdhLine self)
    {
    uint32 lineId = mLocalIdInGroup(self);
    uint32 regAddr = cAf6_diag_glbtfm_reg_Base + mBaseAddress(self);
    uint32 regVal=  mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 hwRate = cInvalidUint32;

    mFieldGet(regVal,
              cAf6_diag_glbtfm_reg_TxFrmStmOcnModeMask(lineId),
              cAf6_diag_glbtfm_reg_TxFrmStmOcnModeshift(lineId),
              uint32,
              &hwRate);

    return RateHw2Sw(hwRate);
    }

static eAtRet ThresholdDefaultSet(AtSdhLine self)
    {
    uint32 regAddr, regVal;
    regAddr = cAf6_diag_glbrfm_reg_RxFrm_Base + mBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_diag_glbrfm_reg_RxFrmBadFrmThresh_, cAf6_diag_glbrfm_reg_RxFrmBadFrmThresh_RstVal);
    mRegFieldSet(regVal, cAf6_diag_glbrfm_reg_RxFrmDescrEn_, cAf6_diag_glbrfm_reg_RxFrmDescrEn_RstVal);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    regAddr = cAf6_diag_glbclkmon_reg_Base + mBaseAddress(self);
    mRegFieldSet(regVal, cAf6_diag_glbclkmon_reg_RxFrmLosDetMod_, 0);
    mRegFieldSet(regVal, cAf6_diag_glbclkmon_reg_RxFrmLosDetDis_, 0);
    mRegFieldSet(regVal, cAf6_diag_glbclkmon_reg_RxFrmClkMonThr_, cAf6_diag_glbclkmon_reg_RxFrmClkMonThr_RstVal);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    regAddr = cAf6_diag_glbdetlos_pen_Base + mBaseAddress(self);
    mRegFieldSet(regVal, cAf6_diag_glbdetlos_pen_RxFrmLosClr2Thr_, cAf6_diag_glbdetlos_pen_RxFrmLosClr2Thr_RstVal);
    mRegFieldSet(regVal, cAf6_diag_glbdetlos_pen_RxFrmLosSetThr_, cAf6_diag_glbdetlos_pen_RxFrmLosSetThr_RstVal);
    mRegFieldSet(regVal, cAf6_diag_glbdetlos_pen_RxFrmLosClr1Thr_, cAf6_diag_glbdetlos_pen_RxFrmLosClr1Thr_RstVal);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    regAddr = cAf6_diag_glblofthr_reg_Base + mBaseAddress(self);
    mRegFieldSet(regVal, cAf6_diag_glblofthr_reg_RxFrmLofDetMod_, cAf6_diag_glblofthr_reg_RxFrmLofDetMod_RstVal);
    mRegFieldSet(regVal, cAf6_diag_glblofthr_reg_RxFrmLofSetThr_, cAf6_diag_glblofthr_reg_RxFrmLofSetThr_RstVal);
    mRegFieldSet(regVal, cAf6_diag_glblofthr_reg_RxFrmLofClrThr_, cAf6_diag_glblofthr_reg_RxFrmLofClrThr_RstVal);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eAtRet TxScrambleEnable(AtSdhLine self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 lineId = mLocalIdInGroup(self);
    regAddr = cAf6_diag_glbtfm_reg_Base + mBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal,
              cAf6_diag_glbtfm_reg_TxFrmScrEnMask(lineId),
              cAf6_diag_glbtfm_reg_TxFrmScrEnshift(lineId),
              mBoolToBin(enable));

    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eAtRet RxScrambleEnable(AtSdhLine self, eBool enable)
    {
    uint32 regAddr, regVal;
    uint32 lineId = mLocalIdInGroup(self);
    regAddr = cAf6_diag_glbrfm_reg_RxFrm_Base + mBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal,
              cAf6_diag_glbtfm_reg_RxFrmDesScrEnMask(lineId),
              cAf6_diag_glbtfm_reg_RxFrmDesScrEnshift(lineId),
              mBoolToBin(enable));

    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eAtRet ScrambleEnable(AtSdhLine self, eBool enable)
    {
    eAtRet ret = TxScrambleEnable(self, enable);
    ret |= RxScrambleEnable(self, enable);
    return ret;
    }

static eBool TxScrambleIsEnabled(AtSdhLine self)
    {
    uint32 regAddr, regVal;
    uint32 lineId = mLocalIdInGroup(self);
    regAddr = cAf6_diag_glbtfm_reg_Base + mBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    if (regVal & cAf6_diag_glbtfm_reg_TxFrmScrEnMask(lineId))
        return cAtTrue;
    return cAtFalse;
    }

static eBool ScrambleIsEnabled(AtSdhLine self)
    {
    return TxScrambleIsEnabled(self);
    }

static eAtRet TxUnforceAll(AtSdhLine self)
    {
    uint32 regAddr, regVal;
    uint32 lineId = mLocalIdInGroup(self);
    regAddr = cAf6_diag_glbtfm_reg_Base + mBaseAddress(self);
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal,
              cAf6_diag_glbtfm_reg_TxLineOofFrcMask(lineId),
              cAf6_diag_glbtfm_reg_TxLineOofFrcshift(lineId),
              0);

    mFieldIns(&regVal,
              cAf6_diag_glbtfm_reg_TxLineB1ErrFrcMask(lineId),
              cAf6_diag_glbtfm_reg_TxLineB1ErrFrcshift(lineId),
              0);

    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eAtRet DefaultSet(AtSdhLine self)
    {
    eAtRet ret = ThresholdDefaultSet(self);
    ret |= ScrambleEnable(self, cAtTrue);
    ret |= TxUnforceAll(self);
    return ret;
    }

static uint32 Hw2SwAlarm(uint32 regVal)
    {
    uint32 alarm = 0;

    if (regVal & cAf6_diag_rxfrmsta_LosCurStatus_Mask)
        alarm |= cAtSdhLineAlarmLos;

    if (regVal & cAf6_diag_rxfrmsta_LofCurStatus_Mask)
        alarm |= cAtSdhLineAlarmLof;

    if (regVal & cAf6_diag_rxfrmsta_OofCurStatus_Mask)
        alarm |= cAtSdhLineAlarmOof;

    return alarm;
    }

static uint32 AlarmGet(AtSdhLine self)
    {
    uint32 lineId = mLocalIdInGroup(self);
    uint32 regAddr = cAf6_diag_rxfrmsta(lineId) + mBaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return Hw2SwAlarm(regVal);
    }

static uint32 AlarmHistoryRead2Clear(AtSdhLine self, eBool r2c)
    {
    uint32 lineId = mLocalIdInGroup(self);
    uint32 regAddr = cAf6_diag_rxfrmstk(lineId) + mBaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 hwAlarm = regVal & cBit2_0;
    if (r2c && (hwAlarm))
        mChannelHwWrite(self, regAddr, hwAlarm, cThaModuleOcn);

    return Hw2SwAlarm(hwAlarm);
    }

static uint32 B1CounterRead2Clear(AtSdhLine self, eBool read2Clear)
    {
    uint32 lineId = mLocalIdInGroup(self);
    uint32 regAddr = mBaseAddress(self);
    if (read2Clear)
        regAddr += cAf6_diag_rxfrmb1cntr2c(lineId);
    else
        regAddr += cAf6_diag_rxfrmb1cntro(lineId);

    return mChannelHwRead(self, regAddr, cThaModuleOcn);
    }

static eAtRet TxAlarmForce(AtSdhLine self, uint32 alarmType, eBool force)
    {
    uint32 regVal, regAddr, lineId;

    if (alarmType != cAtSdhLineAlarmOof)
        return cAtErrorModeNotSupport;

    lineId = mLocalIdInGroup(self);
    regAddr = cAf6_diag_glbtfm_reg_Base + mBaseAddress(self);
    regVal=  mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal,
              cAf6_diag_glbtfm_reg_TxLineOofFrcMask(lineId),
              cAf6_diag_glbtfm_reg_TxLineOofFrcshift(lineId),
              mBoolToBin(force));

    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static uint32 TxForcedAlarmGet(AtSdhLine self)
    {
    uint32 lineId = mLocalIdInGroup(self);
    uint32 regAddr = cAf6_diag_glbtfm_reg_Base + mBaseAddress(self);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);

    if (regVal & cAf6_diag_glbtfm_reg_TxLineOofFrcMask(lineId))
        return cAtSdhLineAlarmOof;

    return 0;
    }

static eAtRet B1Force(AtSdhLine self, eBool force)
    {
    uint32 lineId = mLocalIdInGroup(self);
    uint32 regAddr = cAf6_diag_glbtfm_reg_Base + mBaseAddress(self);
    uint32 regVal=  mChannelHwRead(self, regAddr, cThaModuleOcn);

    mFieldIns(&regVal,
              cAf6_diag_glbtfm_reg_TxLineB1ErrFrcMask(lineId),
              cAf6_diag_glbtfm_reg_TxLineB1ErrFrcshift(lineId),
              mBoolToBin(force));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool B1IsForced(AtSdhLine self)
    {
    uint32 lineId = mLocalIdInGroup(self);
    uint32 regAddr = cAf6_diag_glbtfm_reg_Base + mBaseAddress(self);
    uint32 regVal=  mChannelHwRead(self, regAddr, cThaModuleOcn);
    return (regVal & cAf6_diag_glbtfm_reg_TxLineB1ErrFrcMask(lineId)) ? cAtTrue : cAtFalse;
    }

static eBool RunningOnOtherPlatform(AtSdhLine self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return Tha60290021DeviceIsRunningOnOtherPlatform(device);
    }

eAtModuleSdhRet Tha60290021ModuleOcnDiagLineRateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (RunningOnOtherPlatform(self))
        return cAtOk;

    return RateSet(self, rate);
    }

eAtSdhLineRate Tha60290021ModuleOcnDiagLineRateGet(AtSdhLine self)
    {
    if (self == NULL)
        return cAtSdhLineRateInvalid;

    if (RunningOnOtherPlatform(self))
        return cAtSdhLineRateInvalid;

    return RateGet(self);
    }

eAtRet Tha60290021ModuleOcnDiagLineDefaultSet(AtSdhLine self)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (RunningOnOtherPlatform(self))
        return cAtOk;

    return DefaultSet(self);
    }

uint32 Tha60290021ModuleOcnDiagLineAlarmGet(AtSdhLine self)
    {
    if (self)
        return AlarmGet(self);
    return 0;
    }

uint32 Tha60290021ModuleOcnDiagLineAlarmHistoryRead2Clear(AtSdhLine self, eBool r2c)
    {
    if (self)
        return AlarmHistoryRead2Clear(self, r2c);
    return 0;
    }

uint32 Tha60290021ModuleOcnDiagLineB1CounterRead2Clear(AtSdhLine self, eBool read2Clear)
    {
    if (self)
        return B1CounterRead2Clear(self, read2Clear);
    return 0;
    }

eAtRet Tha60290021ModuleOcnDiagLineTxAlarmForce(AtSdhLine self, uint32 alarmType, eBool force)
    {
    if (self)
        return TxAlarmForce(self, alarmType, force);
    return cAtErrorNullPointer;
    }

uint32 Tha60290021ModuleOcnDiagLineTxForcedAlarmGet(AtSdhLine self)
    {
    if (self)
        return TxForcedAlarmGet(self);
    return 0;
    }

eAtRet Tha60290021ModuleOcnDiagLineB1Force(AtSdhLine self, eBool force)
    {
    if (self)
        return B1Force(self, force);
    return cAtErrorNullPointer;
    }

eBool Tha60290021ModuleOcnDiagLineB1IsForced(AtSdhLine self)
    {
    if (self)
        return B1IsForced(self);
    return cAtFalse;
    }

eAtRet Tha60290021ModuleOcnDiagLineScrambleEnable(AtSdhLine self, eBool enable)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (RunningOnOtherPlatform(self))
        return cAtOk;

    return ScrambleEnable(self, enable);
    }

eBool Tha60290021ModuleOcnDiagLineScrambleIsEnabled(AtSdhLine self)
    {
    if (self)
        return ScrambleIsEnabled(self);
    return cAtFalse;
    }

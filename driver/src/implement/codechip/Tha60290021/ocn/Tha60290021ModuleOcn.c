/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha60290021ModuleOcn.c
 *
 * Created Date: Jul 12, 2016
 *
 * Description : OCN source code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../default/xc/ThaModuleXc.h"
#include "../../../default/xc/ThaVcCrossConnect.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../man/Tha60290021Device.h"
#include "../pw/Tha60290021DccKbyteReg.h"
#include "../ram/Tha60290021ModuleRam.h"
#include "Tha60290021ModuleOcnInternal.h"
#include "Tha60290021ModuleOcnReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cPohSideSelectMask  cBit3
#define cPohSideSelectShift 3
#define cNumSts1InOc48Slice 48

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021ModuleOcn)self)
#define mFaceplateLinePartInSubGroup(faceplateLineId)  ((faceplateLineId) / 4)
#define mFaceplateLineLocalInSubGroup(faceplateLineId) ((faceplateLineId) % 4)
#define mVersionReader(self) VersionReader((AtModule)self)

/*--------------------------- Local typedefs ---------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021ModuleOcnMethods m_methods;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleOcnMethods         m_ThaModuleOcnOverride;
static tTha60210011ModuleOcnMethods m_Tha60210011ModuleOcnOverride;

/* Supper implementation */
static const tAtObjectMethods             *m_AtObjectMethods             = NULL;
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;
static const tThaModuleOcnMethods         *m_ThaModuleOcnMethods         = NULL;
static const tTha60210011ModuleOcnMethods *m_Tha60210011ModuleOcnMethods = NULL;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaVersionReader VersionReader(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return ThaDeviceVersionReader(device);
    }

static eBool SohOverEthIsSupported(ThaModuleOcn self)
    {
    return ThaDeviceSupportFeatureFromVersion(AtModuleDeviceGet((AtModule)self), 0x2, 0x2, 0);
    }

static uint32 SohOverEthBaseAddress(ThaModuleOcn self)
    {
    AtUnused(self);
    return 0x1EC0000;
    }

static eBool IsSohRegister(ThaModuleOcn self, uint32 localAddress)
    {
    AtUnused(self);
    if (mInRange(localAddress, 0x1EC0000, 0x1EFFFFF))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 *SohOverEthHoldRegistersGet(ThaModuleOcn self, uint16 *numberOfHoldRegisters)
    {
    uint32 baseAddress = 0x0;
    static uint32 holdRegisters[] = {0x0, 0x0};

    baseAddress = ThaModuleOcnSohOverEthBaseAddress(self);
    holdRegisters[0] = baseAddress + 0xA;
    holdRegisters[1] = baseAddress + 0xB;

    if (numberOfHoldRegisters)
        *numberOfHoldRegisters = mCount(holdRegisters);

    return holdRegisters;
    }

static AtLongRegisterAccess SohOverEthLongRegisterAccessCreate(ThaModuleOcn self)
    {
    uint16 numHoldRegisters;
    uint32 *holdRegisters = ThaModuleOcnSohOverEthHoldRegistersGet(self, &numHoldRegisters);

    return AtDefaultLongRegisterAccessNew(holdRegisters, numHoldRegisters, holdRegisters, numHoldRegisters);
    }

static eAtRet SohOverEthInit(ThaModuleOcn self)
    {
    return Tha6029ModuleOcnSohOverEthInit(self);
    }

static uint32 UpenDccdecBase(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_upen_dccdec_Base;
    }

static uint32 FaceplateSlicePointerShift(ThaModuleOcn self, uint32 faceplateLineId)
    {
    AtUnused(self);
    return (mFaceplateLineLocalInSubGroup(faceplateLineId) * 8);
    }

static uint32 FaceplateSlicePointerMask(ThaModuleOcn self, uint32 faceplateLineId)
    {
    return cBit5_0 << FaceplateSlicePointerShift(self, faceplateLineId);
    }

static uint32 FaceplateEnableMask(ThaModuleOcn self, uint32 faceplateLineId)
    {
    AtUnused(self);
    return cBit6 << (mFaceplateLineLocalInSubGroup(faceplateLineId) * 8);
    }

static uint32 FaceplateEnableShift(ThaModuleOcn self, uint32 faceplateLineId)
    {
    AtUnused(self);
    return (mFaceplateLineLocalInSubGroup(faceplateLineId) * 8) + 6;
    }

static uint32 NumFaceplateLinesPerGroup(ThaModuleOcn self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 FaceplateLineGroup(ThaModuleOcn self, uint32 faceplateLineId)
    {
    return faceplateLineId / NumFaceplateLinesPerGroup(self);
    }

static uint32 FaceplateGroupOffset(Tha60290021ModuleOcn self, uint32 groupId)
    {
    AtUnused(self);
    return groupId * 65536;
    }

static uint32 FaceplateLineGroupOffset(ThaModuleOcn self, uint32 faceplatePortId)
    {
    return mMethodsGet(mThis(self))->FaceplateGroupOffset(mThis(self), FaceplateLineGroup(self, faceplatePortId));
    }

static uint32 FaceplateLineAddressWithLocalAddress(ThaModuleOcn self, uint32 faceplateLineId, uint32 localAddress)
    {
    uint32 baseAddress = ThaModuleOcnBaseAddress(self);
    uint32 groupOffset = mMethodsGet(mThis(self))->FaceplateGroupOffset(mThis(self), FaceplateLineGroup(self, faceplateLineId));
    return baseAddress + groupOffset + localAddress;
    }

static AtModuleSdh SdhModule(ThaModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static uint32 FaceplateLineLocalId(ThaModuleOcn self, uint32 lineId)
    {
    return ThaModuleSdhLineIdLocalId((ThaModuleSdh)SdhModule(self), (uint8)lineId) % NumFaceplateLinesPerGroup(self);
    }

static uint32 FaceplateLineDefaultOffset(Tha60290021ModuleOcn self, uint32 faceplateLineId)
    {
    uint32 baseAddress = ThaModuleOcnBaseAddress((ThaModuleOcn)self);
    uint32 groupOffset = FaceplateLineGroupOffset((ThaModuleOcn)self, faceplateLineId);
    return baseAddress + groupOffset + FaceplateLineLocalId((ThaModuleOcn)self, faceplateLineId);
    }

static uint32 TohMonitoringChannelControlRegAddr(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tohramctl_Base;
    }

static uint32 FaceplateOCN_TOH_Monitoring_Per_Line_Control_Register(ThaModuleOcn self, uint32 faceplateLineId)
    {
    Tha60290021ModuleOcn moduleOcn = (Tha60290021ModuleOcn)self;
    return mMethodsGet(moduleOcn)->TohMonitoringChannelControlRegAddr(moduleOcn) +
           mMethodsGet(moduleOcn)->FaceplateLineDefaultOffset(moduleOcn, faceplateLineId);
    }

static eAtRet KbyteOcnToKbyteDccProcessorTrigger(ThaModuleOcn self, uint32 faceplateLineId)
    {
    uint32 address = FaceplateOCN_TOH_Monitoring_Per_Line_Control_Register(self, faceplateLineId);
    uint32 regVal = mModuleHwRead(self, address);
    static const uint32 cTriggerTime = 500; /*500us*/

    mRegFieldSet(regVal, cAf6_tohramctl_RxKByteTransEn_, 0);
    mModuleHwWrite(self, address, regVal);
    mRegFieldSet(regVal, cAf6_tohramctl_RxKByteTransEn_, 1);
    mModuleHwWrite(self, address, regVal);
    AtOsalUSleep(cTriggerTime);
    mRegFieldSet(regVal, cAf6_tohramctl_RxKByteTransEn_, 0);
    mModuleHwWrite(self, address, regVal);
    return cAtOk;
    }

static uint32 FaceplateRxLocatedSlicePointerSelectionRegister(ThaModuleOcn self, uint32 faceplateLineId)
    {
    uint32 localIdInGroup = FaceplateLineLocalId(self, faceplateLineId);

    if (mFaceplateLinePartInSubGroup(localIdInGroup) == 0)
        return FaceplateLineAddressWithLocalAddress(self, faceplateLineId, cAf6Reg_glbrxslcppsel1_reg_Base);

    return FaceplateLineAddressWithLocalAddress(self, faceplateLineId, cAf6Reg_glbrxslcppsel2_reg_Base);
    }

static uint32 FaceplateTxLocatedSlicePointerSelectionRegister(ThaModuleOcn self, uint32 faceplateLineId)
    {
    uint32 localIdInGroup = FaceplateLineLocalId(self, faceplateLineId);

    if (mFaceplateLinePartInSubGroup(localIdInGroup) == 0)
        return FaceplateLineAddressWithLocalAddress(self, faceplateLineId, cAf6Reg_glbtxslcppsel1_reg_Base);

    return FaceplateLineAddressWithLocalAddress(self, faceplateLineId, cAf6Reg_glbtxslcppsel2_reg_Base);
    }

static eAtRet FaceplateStsAssignWithRegister(ThaModuleOcn self, uint32 faceplateLineId, uint32 sts48Id, uint32 sts12Id, uint32 sts3Id, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 fieldVal = sts3Id | (sts12Id << 2) | (sts48Id << 4);
    mFieldIns(&regVal,
              FaceplateSlicePointerMask(self, faceplateLineId),
              FaceplateSlicePointerShift(self, faceplateLineId),
              fieldVal);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet FaceplateStsAssign(ThaModuleOcn self, uint32 faceplateLineId, uint32 sts48Id, uint32 sts12Id, uint32 sts3Id)
    {
    eAtRet ret = cAtOk;

    ret |= FaceplateStsAssignWithRegister(self, faceplateLineId,
                                          sts48Id, sts12Id, sts3Id,
                                          FaceplateRxLocatedSlicePointerSelectionRegister(self, faceplateLineId));
    ret |= FaceplateStsAssignWithRegister(self, faceplateLineId,
                                          sts48Id, sts12Id, sts3Id,
                                          FaceplateTxLocatedSlicePointerSelectionRegister(self, faceplateLineId));

    return ret;
    }

static uint32 NumFaceplateLineGroups(ThaModuleOcn self)
    {
    AtModuleSdh sdhModule = SdhModule(self);
    return Tha60290021ModuleSdhNumUseableFaceplateLines(sdhModule) / NumFaceplateLinesPerGroup(self);
    }

static eAtRet DefaultFaceplateStsAssign(ThaModuleOcn self)
    {
    eAtRet ret = cAtOk;
    uint32 faceplateId = 0;
    uint32 portGroup_i;

    /* See sts_assignment.xlsx, sheet "Rate setting" */
    for (portGroup_i = 0; portGroup_i < NumFaceplateLineGroups(self); portGroup_i++)
        {
        ret |= FaceplateStsAssign(self, faceplateId++, 0, 0, 0);
        ret |= FaceplateStsAssign(self, faceplateId++, 0, 1, 0);
        ret |= FaceplateStsAssign(self, faceplateId++, 1, 0, 0);
        ret |= FaceplateStsAssign(self, faceplateId++, 1, 1, 0);
        ret |= FaceplateStsAssign(self, faceplateId++, 2, 0, 0);
        ret |= FaceplateStsAssign(self, faceplateId++, 2, 1, 0);
        ret |= FaceplateStsAssign(self, faceplateId++, 3, 0, 0);
        ret |= FaceplateStsAssign(self, faceplateId++, 3, 1, 0);
        }

    return ret;
    }

static eAtRet FaceplateStsMappingDefaultSet(Tha60290021ModuleOcn self)
    {
    return DefaultFaceplateStsAssign((ThaModuleOcn)self);
    }

static eAtRet PlatformSetup(ThaModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (Tha60290021DeviceIsRunningOnOtherPlatform(device))
        return Tha60290021ModuleOcnPohProcessorSideSet(self, cTha6029LineSideMate);

    return Tha60290021ModuleOcnPohProcessorSideSet(self, cTha6029LineSideFaceplate);
    }

static eAtRet HwTohThresholdDefaultSet(ThaModuleOcn self, uint32 groupId)
    {
    static const uint8 cNotMaskingTohStableMonitoring = 1;
    uint32 baseAddress = ThaModuleOcnBaseAddress(self) + FaceplateGroupOffset(mThis(self), groupId);
    uint32 regAddr = ThaModuleOcnTohGlobalK1StableMonitoringThresholdControlRegAddr(self) + baseAddress;
    uint32 regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbk1stbthr_reg_TohK1StbThr2_, 3);
    mRegFieldSet(regVal, cAf6_tohglbk1stbthr_reg_TohK1StbThr1_, 4);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = ThaModuleOcnTohGlobalK2StableMonitoringThresholdControlRegAddr(self) + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbk2stbthr_reg_TohK2StbThr2_, 3);
    mRegFieldSet(regVal, cAf6_tohglbk2stbthr_reg_TohK2StbThr1_, 4);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = ThaModuleOcnTohGlobalS1StableMonitoringThresholdControlRegAddr(self) + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbs1stbthr_reg_TohS1StbThr2_, 5);
    mRegFieldSet(regVal, cAf6_tohglbs1stbthr_reg_TohS1StbThr1_, 5);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglbrdidetthr_reg_Base + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbrdidetthr_reg_TohRdiDetThr2_, 0x4);
    mRegFieldSet(regVal, cAf6_tohglbrdidetthr_reg_TohRdiDetThr1_, 0x9);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglbaisdetthr_reg_Base + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbaisdetthr_reg_TohAisDetThr2_, 3);
    mRegFieldSet(regVal, cAf6_tohglbaisdetthr_reg_TohAisDetThr1_, 4);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglbk1smpthr_reg_Base + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbk1smpthr_reg_TohK1SmpThr2_, 7);
    mRegFieldSet(regVal, cAf6_tohglbk1smpthr_reg_TohK1SmpThr1_, 7);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglberrcntmod_reg_Base + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglberrcntmod_reg_TohReiErrCntMod_, 1);
    mRegFieldSet(regVal, cAf6_tohglberrcntmod_reg_TohB2ErrCntMod_, 1);
    mRegFieldSet(regVal, cAf6_tohglberrcntmod_reg_TohB1ErrCntMod_, 1);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = cAf6Reg_tohglbaffen_reg_Base + baseAddress;
    regVal = 0;
    mRegFieldSet(regVal, cAf6_tohglbaffen_reg_TohAisAffStbMon_, cNotMaskingTohStableMonitoring);
    mRegFieldSet(regVal, cAf6_tohglbaffen_reg_TohAisAffRdilMon_, 0);
    mRegFieldSet(regVal, cAf6_tohglbaffen_reg_TohAisAffAislMon_, 0);
    mRegFieldSet(regVal, cAf6_tohglbaffen_reg_TohAisAffErrCnt_, 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet TohThresholdDefaultSet(ThaModuleOcn self)
    {
    eAtRet ret = cAtOk;
    uint32 groupId;

    for (groupId = 0; groupId < NumFaceplateLineGroups(self); groupId++)
        ret |= HwTohThresholdDefaultSet(self, groupId);

    return ret;
    }

static eAtRet DefaultSet(ThaModuleOcn self)
    {
    eAtRet ret;

    ret = m_ThaModuleOcnMethods->DefaultSet(self);
    if (ret != cAtOk)
        return ret;

    ret |= mMethodsGet(mThis(self))->FaceplateStsMappingDefaultSet(mThis(self));
    ret |= PlatformSetup(self);
    ret |= mMethodsGet(self)->TohThresholdDefaultSet(self);

    return ret;
    }

static eAtRet FaceplateLineEnableWithRegister(ThaModuleOcn self, uint32 lineId, eBool enable, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal,
              FaceplateEnableMask(self, lineId),
              FaceplateEnableShift(self, lineId),
              enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool FaceplateLineIsEnabledCheckWithRegister(ThaModuleOcn self, uint32 lineId, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & FaceplateEnableMask(self, lineId)) ? cAtTrue : cAtFalse;
    }

static eAtRet FaceplateLineRxEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    uint32 regAddr = FaceplateRxLocatedSlicePointerSelectionRegister(self, lineId);
    return FaceplateLineEnableWithRegister(self, lineId, enable, regAddr);
    }

static eBool FaceplateLineRxIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateRxLocatedSlicePointerSelectionRegister(self, lineId);
    return FaceplateLineIsEnabledCheckWithRegister(self, lineId, regAddr);
    }

static eAtRet FaceplateLineTxEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    uint32 regAddr = FaceplateTxLocatedSlicePointerSelectionRegister(self, lineId);
    return FaceplateLineEnableWithRegister(self, lineId, enable, regAddr);
    }

static eBool FaceplateLineTxIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateTxLocatedSlicePointerSelectionRegister(self, lineId);
    return FaceplateLineIsEnabledCheckWithRegister(self, lineId, regAddr);
    }

static eAtRet FaceplateLineEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    eAtRet ret = cAtOk;

    ret |= FaceplateLineRxEnable(self, lineId, enable);
    ret |= FaceplateLineTxEnable(self, lineId, enable);

    return ret;
    }

static uint8 RateSw2Hw(eAtSdhLineRate rate)
    {
    switch (rate)
        {
        case cAtSdhLineRateStm0 : return 3;
        case cAtSdhLineRateStm1 : return 2;
        case cAtSdhLineRateStm4 : return 1;
        case cAtSdhLineRateStm16: return 0;

        /* Impossible, but... */
        case cAtSdhLineRateStm64:
        case cAtSdhLineRateInvalid:
        default:
            return 0;
        }
    }

static uint32 FrmOc192EnbMask(void)
    {
    return cAf6_glbtfm_reg_TxFrmOc192Enb_Mask;
    }

static uint32 FrmOc192EnbShift(void)
    {
    return cAf6_glbtfm_reg_TxFrmOc192Enb_Shift;
    }

static uint32 RateMask(ThaModuleOcn self, uint32 lineId)
    {
    uint32 localId = FaceplateLineLocalId(self, lineId);
    return cBit1_0 << (localId * 2);
    }

static uint32 RateShift(ThaModuleOcn self, uint32 lineId)
    {
    uint32 localId = FaceplateLineLocalId(self, lineId);
    return (localId * 2);
    }

static uint32 FaceplateGlbrfmAddress(ThaModuleOcn self, uint32 lineId)
    {
    return FaceplateLineAddressWithLocalAddress(self, lineId, cAf6Reg_glbrfm_reg_Base);
    }

static uint32 FaceplateGlbtfmAddress(ThaModuleOcn self, uint32 lineId)
    {
    return FaceplateLineAddressWithLocalAddress(self, lineId, cAf6Reg_glbtfm_reg_Base);
    }

static eAtRet FaceplateOc192Enable(ThaModuleOcn self, uint32 lineId, eBool enabled)
    {
    uint32 regAddr, regVal;
    uint8 hwEnabled = enabled ? 1 : 0;
    uint32 mask = FrmOc192EnbMask();
    uint32 shift = FrmOc192EnbShift();
    uint32 allRate;
    AtUnused(lineId);

    regAddr = FaceplateGlbrfmAddress(self, lineId);
    regVal  = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, mask, shift, hwEnabled);
    mModuleHwWrite(self, regAddr, regVal);
    allRate = regVal & cAf6_glbrfm_reg_RxFrmStmOcnMode_Mask;
    regAddr = FaceplateGlbtfmAddress(self, lineId);
    if (enabled)
        {
        /* FIXEDME: Add default for OC-192 */
        mModuleHwWrite(self, regAddr, 0x1010000);
        }
    else
        {
        regVal  = mModuleHwRead(self, regAddr);
        mFieldIns(&regVal, mask, shift, hwEnabled);
        mFieldIns(&regVal, cAf6_glbrfm_reg_RxFrmStmOcnMode_Mask, cAf6_glbrfm_reg_RxFrmStmOcnMode_Shift, allRate);
        mModuleHwWrite(self, regAddr, regVal);
        }
    return cAtOk;
    }

static eAtRet FaceplateLineBelowStm64RateSet(ThaModuleOcn self, uint32 lineId, eAtSdhLineRate rate)
    {
    uint32 regAddr, regVal;
    uint32 mask = RateMask(self, lineId);
    uint32 shift = RateShift(self, lineId);
    uint32 hwRate = RateSw2Hw(rate);

    regAddr = FaceplateGlbtfmAddress(self, lineId);
    regVal  = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, mask, shift, hwRate);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = FaceplateGlbrfmAddress(self, lineId);
    regVal  = mModuleHwRead(self, regAddr);
    mFieldIns(&regVal, mask, shift, hwRate);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet FaceplateLineRateSet(ThaModuleOcn self, uint32 lineId, eAtSdhLineRate rate)
    {
    if (rate == cAtSdhLineRateStm64)
        return Tha60290021ModuleOcnFaceplateOc192Enable(self, lineId, cAtTrue);

    if (FaceplateLineLocalId(self, lineId) == 0)
        Tha60290021ModuleOcnFaceplateOc192Enable(self, lineId, cAtFalse);

    return FaceplateLineBelowStm64RateSet(self, lineId, rate);
    }

static eBool IsFaceplateChannel(AtSdhChannel channel)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)channel);
    return Tha60290021ModuleSdhLineIsFaceplate(sdhModule, AtSdhChannelLineGet(channel));
    }

static eBool IsMateChannel(AtSdhChannel channel)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)channel);
    return Tha60290021ModuleSdhLineIsMate(sdhModule, AtSdhChannelLineGet(channel));
    }

static uint32 OcnStsPointerInterpreterPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (channel == NULL)
        return m_Tha60210011ModuleOcnMethods->OcnStsPointerInterpreterPerChannelControlRegAddr(self, channel);

    if (IsFaceplateChannel(channel))
        return cAf6Reg_spiramctl_Base;

    if (IsMateChannel(channel))
        return cAf6Reg_tfi5spiramctl_Base;

    return m_Tha60210011ModuleOcnMethods->OcnStsPointerInterpreterPerChannelControlRegAddr(self, channel);
    }

static uint32 OcnStsPointerGeneratorPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (IsFaceplateChannel(channel))
        return cAf6Reg_spgramctl_Base;

    if (IsMateChannel(channel))
        return cAf6Reg_tfi5spgramctl_Base;

    return m_Tha60210011ModuleOcnMethods->OcnStsPointerGeneratorPerChannelControlRegAddr(self, channel);
    }

static eBool IsSts12c(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhChannel);
    if ((vcType == cAtSdhChannelTypeVc4_4c) || (vcType == cAtSdhChannelTypeAu4_4c))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsSts48c(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType vcType = AtSdhChannelTypeGet(sdhChannel);
    if ((vcType == cAtSdhChannelTypeVc4_16c) || (vcType == cAtSdhChannelTypeAu4_16c))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 GroupIdSts64(uint8 swStsId)
    {
    return (uint8) (swStsId / 64);
    }

static uint8 GroupIdSts16(uint8 swStsId)
    {
    return (uint8) (swStsId / 16)%4;
    }

static uint8 LocalIdSts16(uint8 swStsId)
    {
    return (uint8) (swStsId % 16);
    }

static uint8 Stm64Sw2HwId(uint8 swStsId)
    {
    return (uint8) (((swStsId % 3) * 64) + (swStsId / 3));
    }

static uint8 Stm64Aug1StsId(uint8 swStsId)
    {
    uint8 hwId = Stm64Sw2HwId(swStsId);
    return (uint8)(LocalIdSts16(hwId) + (GroupIdSts64(hwId) * 16));
    }

static uint8 Stm64Aug1SliceId(uint8 swStsId)
    {
    uint8 hwId = Stm64Sw2HwId(swStsId);
    return (uint8) (GroupIdSts16(hwId) % 4);
    }

static eAtRet Stm64Sts12cStsIdSw2HwGet(uint8 swSts,
                                       uint8* sliceId,
                                       uint8 *hwStsInSlice)
    {
    uint8 local48 = (uint8) (swSts % 48);
    uint8 group12 = (uint8) (local48 / 12);
    uint8 local12 = (uint8) (local48 % 12);
    uint8 group4 = (uint8) (local12 / 4);
    uint8 local4 = (uint8) (local12 % 4);

    *hwStsInSlice = (uint8) (local4 + (group4 * 16) + (group12 * 4));
    *sliceId = (uint8) (swSts / 48);

    return cAtOk;
    }

static eAtRet Stm64Sts48cStsIdSw2HwGet(uint8 swSts,
                                       uint8* sliceId,
                                       uint8 *hwStsInSlice)
    {
    *sliceId = (uint8) (swSts/48);
    *hwStsInSlice = (uint8) (swSts%48);
    return cAtOk;
    }

static eAtRet Stm64FaceplateChannelStsIdSw2HwGet(ThaModuleOcn self,
                                            AtSdhChannel sdhChannel,
                                            uint8 swSts,
                                            uint8* sliceId,
                                            uint8 *hwStsInSlice)
    {
    AtUnused(self);

    if (IsSts12c(sdhChannel))
        return Stm64Sts12cStsIdSw2HwGet(swSts, sliceId, hwStsInSlice);

    if (IsSts48c(sdhChannel))
        return Stm64Sts48cStsIdSw2HwGet(swSts, sliceId, hwStsInSlice);

    if (sliceId)
        *sliceId = Stm64Aug1SliceId(swSts);

    if (hwStsInSlice)
        *hwStsInSlice = Stm64Aug1StsId(swSts);

    return cAtOk;
    }

static uint8 NumFaceplateSlicesPerGroup(ThaModuleOcn self)
    {
    AtUnused(self);
    return 4;
    }

static uint8 FaceplateSliceLocalId(ThaModuleOcn self, uint8 sliceId)
    {
    return (uint8)(sliceId % NumFaceplateSlicesPerGroup(self));
    }

static eAtRet FaceplateChannelStsIdSw2HwGet(ThaModuleOcn self, AtSdhChannel sdhChannel, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(sdhChannel);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    uint32 lineId = AtChannelIdGet((AtChannel)line);
    uint32 sts48Id = lineId / 2, sts12Id = 0, sts3Id = 0, sts1Id = 0;

    AtUnused(self);

    /* Refer sts_assignment.xlsx for multiplexing to have the following converting */
    if (rate == cAtSdhLineRateStm64)
        return Stm64FaceplateChannelStsIdSw2HwGet(self, sdhChannel, swSts, sliceId, hwStsInSlice);

    switch (rate)
        {
        case cAtSdhLineRateStm0:
        case cAtSdhLineRateStm1:
        case cAtSdhLineRateStm4:
            sts12Id = (lineId % 2);
            sts3Id  = (swSts / 3); /* 0 0 0*/
            sts1Id  = (swSts % 3); /* 0 1 2*/
            break;

        case cAtSdhLineRateStm16:
            sts12Id = swSts / 12;
            sts3Id  = (swSts % 12) / 3;
            sts1Id  = (swSts % 3);
            break;

        case cAtSdhLineRateStm64:
        case cAtSdhLineRateInvalid:
        default:
            AtChannelLog((AtChannel)sdhChannel, cAtLogLevelCritical, AtSourceLocation,
                         "Cannot convert hardware STS because of unknown line rate: %d\r\n",
                         rate);
            return cAtErrorInvlParm;
        }

    if (sliceId)
        *sliceId = (uint8)sts48Id;

    if (hwStsInSlice)
        {
        if (IsSts12c(sdhChannel) && (rate == cAtSdhLineRateStm4))
            {
            sts1Id  = (swSts % 12); /* 0 1 2 3*/
            *hwStsInSlice = (uint8) ((sts1Id * 4) + sts12Id);/* 0 4 8 12*/
            }
        else /* Normal Case */
            {
            *hwStsInSlice = (uint8)((sts1Id * 16) + (sts3Id * 4) + sts12Id);
            }
        }

    return cAtOk;
    }

static eBool IsBelongToStm16Line(AtSdhChannel self)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(self);
    return (AtSdhLineRateGet(line) == cAtSdhLineRateStm16);
    }

static eAtRet ChannelStsIdSw2HwGetOnStm16(ThaModuleOcn self, AtSdhChannel sdhChannel, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    eAtRet ret = m_ThaModuleOcnMethods->StsIdSw2Hw(self, sdhChannel, cThaModuleOcn, swSts, sliceId, hwStsInSlice);
    if (ret != cAtOk)
        return ret;

    *sliceId = (uint8) (*sliceId) / 2;
    return ret;
    }

static uint8 FaceplateGroupStartSlice(ThaModuleOcn self, uint32 faceplateLineId)
    {
    return (uint8)(NumFaceplateSlicesPerGroup(self) * FaceplateLineGroup(self, faceplateLineId));
    }

static eAtRet FaceplateHwStsNewGet(ThaModuleOcn self, AtSdhChannel sdhChannel, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    uint32 localSts48, localSts24, localSts12;
    uint32 sts48Id = 0, sts12Id = 0, sts3Id = 0, sts1Id = 0;
    AtSdhLine line = AtSdhChannelLineObjectGet(sdhChannel);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    uint32 lineId = AtChannelIdGet((AtChannel)line);

    if (IsSts48c(sdhChannel))
        {
        if (rate == cAtSdhLineRateStm64)
            {
            eAtRet ret = Stm64Sts48cStsIdSw2HwGet(swSts, sliceId, hwStsInSlice);
            *sliceId = (uint8)(*sliceId + FaceplateGroupStartSlice(self, lineId));
            return ret;
            }

        /* For another rate */
        *sliceId = (uint8) (lineId/2);
        *hwStsInSlice = (uint8) (swSts%48);

        return cAtOk;
        }

    localSts48 = (swSts % 48);
    localSts24 = (localSts48 % 24);
    localSts12 = (localSts24 % 12);

    AtUnused(self);
    switch (rate)
        {
        case cAtSdhLineRateStm0:
        case cAtSdhLineRateStm1:
        case cAtSdhLineRateStm4:
            sts48Id = lineId / 2;
            sts12Id = (lineId % 2);
            sts3Id  = (localSts12 / 3);
            sts1Id  = (localSts12 % 3);
            break;

        case cAtSdhLineRateStm16:
            sts48Id = lineId / 2;
            sts12Id = (localSts48 / 12);
            sts3Id  = (localSts12 / 3);
            sts1Id  = (localSts12 % 3);
            break;

        case cAtSdhLineRateStm64:
            sts48Id = (uint8)((swSts / 48) + FaceplateGroupStartSlice(self, lineId));
            sts12Id = (localSts48 / 12);
            sts3Id  = (localSts12 / 3);
            sts1Id  = (localSts12 % 3);
            break;

        case cAtSdhLineRateInvalid:
        default:
            AtChannelLog((AtChannel)sdhChannel, cAtLogLevelCritical, AtSourceLocation,
                         "Cannot convert hardware STS because of unknown line rate: %d\r\n",
                         rate);
            return cAtErrorInvlParm;
        }

    if (sliceId)
        *sliceId = (uint8)sts48Id;

    if (hwStsInSlice)
        {
        if (IsSts12c(sdhChannel))
            {
        	if (rate == cAtSdhLineRateStm4)
        		*hwStsInSlice = (uint8) ((localSts12 % 4) + ((localSts12 / 4) * 16) + ((localSts48 / 12) * 4) + (sts12Id * 4));
        	else
        		*hwStsInSlice = (uint8) ((localSts12 % 4) + ((localSts12 / 4) * 16) + ((localSts48 / 12) * 4));
            }
        else
            {
            *hwStsInSlice = (uint8)((sts1Id * 16) + (sts3Id) + (sts12Id * 4));
            }
        }

    return cAtOk;
    }

static eAtRet FaceplateHwStsOldGet(ThaModuleOcn self, AtSdhChannel sdhChannel, uint8 swSts, uint8* sliceId, uint8* hwStsInSlice)
    {
    if (IsBelongToStm16Line(sdhChannel))
        return ChannelStsIdSw2HwGetOnStm16(self, sdhChannel, swSts, sliceId, hwStsInSlice);

    return FaceplateChannelStsIdSw2HwGet((ThaModuleOcn) self, sdhChannel, swSts, sliceId, hwStsInSlice);
    }

static eAtRet FaceplateHwStsGet(ThaModuleOcn self, AtSdhChannel sdhChannel, uint8 swSts, uint8* sliceId, uint8* hwStsInSlice)
    {
    if (Tha60290021ModuleSdhUseNewStsIdConvert((AtModuleSdh) AtChannelModuleGet((AtChannel) sdhChannel)))
        return FaceplateHwStsNewGet(self, sdhChannel, swSts, sliceId, hwStsInSlice);

    return FaceplateHwStsOldGet(self, sdhChannel, swSts, sliceId, hwStsInSlice);
    }

static eAtRet FaceplateStsIdSw2Hw(Tha60290021ModuleOcn self, AtSdhChannel sdhChannel, uint8 swSts, uint8* sliceId, uint8* hwStsInSlice)
    {
    return FaceplateHwStsGet((ThaModuleOcn)self, sdhChannel, swSts, sliceId, hwStsInSlice);
    }

static eAtRet StsIdSw2Hw(ThaModuleOcn self, AtSdhChannel sdhChannel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8* hwStsInSlice)
    {
    if (IsFaceplateChannel(sdhChannel))
        return mMethodsGet(mThis(self))->FaceplateStsIdSw2Hw(mThis(self), sdhChannel, swSts, sliceId, hwStsInSlice);

    return m_ThaModuleOcnMethods->StsIdSw2Hw(self, sdhChannel, phyModule, swSts, sliceId, hwStsInSlice);
    }

static uint8 HwSliceInApsModule(AtSdhChannel channel, uint8 sliceId)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)channel);
    if (Tha60290021ModuleSdhLineIsMate(sdhModule, AtSdhChannelLineGet(channel)))
        return (uint8)(sliceId + 8);

    if (Tha60290021ModuleSdhLineIsTerminated(sdhModule, AtSdhChannelLineGet(channel)))
        return cInvalidSlice;

    return sliceId;
    }

static uint32 MateXcStartLineId(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 FaceplateXcStartLineId(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 TerminatedXcStartLineId(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 HwSliceInXcModule(Tha60290021ModuleOcn self, AtSdhChannel channel, uint32 sliceId)
    {
    uint32 lineId = AtSdhChannelLineGet(channel);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)channel);

    if (Tha60290021ModuleSdhLineIsMate(sdhModule, (uint8)lineId))
        return sliceId + mMethodsGet(self)->MateXcStartLineId(self);

    if (Tha60290021ModuleSdhLineIsFaceplate(sdhModule, (uint8)lineId))
        return sliceId + mMethodsGet(self)->FaceplateXcStartLineId(self);

    return sliceId + mMethodsGet(self)->TerminatedXcStartLineId(self);
    }

static eAtRet ChannelStsIdSw2HwGet(ThaModuleOcn self, AtSdhChannel channel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    eAtRet ret = m_ThaModuleOcnMethods->ChannelStsIdSw2HwGet(self, channel, phyModule, swSts, sliceId, hwStsInSlice);
    if (ret != cAtOk)
        return ret;

    if (phyModule == cAtModuleAps)
        *sliceId = HwSliceInApsModule(channel, *sliceId);

    if (phyModule == cAtModuleXc)
        *sliceId = (uint8)HwSliceInXcModule(mThis(self), channel, *sliceId);

    return cAtOk;
    }

static uint32 Tfi5GlbtfmBase(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tfi5glbtfm_reg_Base;
    }

static uint32 Tfi5GlbrfmBase(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tfi5glbrfm_reg_Base;
    }

static uint32 GlbtpgBase(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_glbtpg_reg_Base;
    }

static uint32 GlbvpiBase(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_glbvpi_reg_Base;
    }

static uint32 OcnRxHoMapConcatenateReg(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_rxhomapramctl_Base;
    }

static uint32 RxStsPayloadControl(ThaModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_demramctl_Base;
    }

static uint32 TxStsMultiplexingControl(ThaModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_pgdemramctl_Base;
    }

static uint32 OcnVtTuPointerGeneratorPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_vpgramctl_Base;
    }

static uint32 OcnVtTuPointerInterpreterPerChannelControlRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_vpiramctl_Base;
    }

static uint32 OcnVtTuPointerInterpreterPerChannelInterruptRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_upvtchstkram_Base;
    }

static uint32 OcnVtTuPointerInterpreterPerChannelAlarmStatusRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_upvtchstaram_Base;
    }

static uint32 OcnVtTuPointerGeneratorPerChannelInterruptRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_vtpgstkram_Base;
    }

static uint32 OcnStsPointerInterpreterPerChannelInterruptRegAddr(Tha60210011ModuleOcn self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);

    if (IsMateChannel(sdhChannel))
        return cAf6Reg_tfi5upstschstkram_Base;

    if (IsFaceplateChannel(sdhChannel))
        return cAf6Reg_upstschstkram_Base;

    AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Unknown channel side\r\n");
    return cInvalidUint32;
    }

static uint32 OcnStsPointerInterpreterPerChannelAlarmStatusRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);

    if (IsMateChannel(channel))
        return cAf6Reg_tfi5upstschstaram_Base;

    if (IsFaceplateChannel(channel))
        return cAf6Reg_upstschstaram_Base;

    AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Unknown channel side\r\n");
    return cInvalidUint32;
    }

static uint32 OcnStsPointerGeneratorPerChannelInterruptRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);

    if (IsMateChannel(channel))
        return cAf6Reg_tfi5stspgstkram_Base;
    if (IsFaceplateChannel(channel))
        return cAf6Reg_stspgstkram_Base;

    AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Unknown channel side\r\n");
    return cInvalidUint32;
    }

static uint32 GlbspiBase(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_glbspi_reg_Base;
    }

static uint32 TohGlobalK1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tohglbk1stbthr_reg_Base;
    }

static uint32 TohGlobalK2StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tohglbk2stbthr_reg_Base;
    }

static uint32 TohGlobalS1StableMonitoringThresholdControlRegAddr(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tohglbs1stbthr_reg_Base;
    }

static eBool ChannelCanBeTerminated(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)channel);
    AtUnused(self);
    return Tha60290021ModuleSdhLineIsTerminated(sdhModule, AtSdhChannelLineGet(channel));
    }

static eBool ChannelHasPointerProcessor(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(channel);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)channel);
    AtUnused(self);
    return Tha60290021ModuleSdhLineIsTerminated(sdhModule, (uint8)AtChannelIdGet((AtChannel)line)) ? cAtFalse : cAtTrue;
    }

static uint32 RxSxcControlRegAddr(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_sxcramctl0_Base;
    }

static uint32 TxSxcControlRegAddr(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_sxcramctl0_Base;
    }

static uint32 FaceplateLineScrambleMask(ThaModuleOcn self, uint32 lineId)
    {
    return cBit16 << FaceplateLineLocalId(self, lineId);
    }

static uint32 FaceplateLineScrambleShift(ThaModuleOcn self, uint32 lineId)
    {
    return (FaceplateLineLocalId(self, lineId) + 16);
    }

static eAtRet FaceplateLineScrambleEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    uint32 regAddr, regVal;
    uint8 hwEnabled = enable ? 1 : 0;
    uint32 scrambleMask = FaceplateLineScrambleMask(self, lineId);
    uint32 scrambleShift = FaceplateLineScrambleShift(self, lineId);

    regAddr = FaceplateGlbtfmAddress(self, lineId);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, scramble, hwEnabled);
    mModuleHwWrite(self, regAddr, regVal);

    regAddr = FaceplateGlbrfmAddress(self, lineId);
    regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, scramble, hwEnabled);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool FaceplateLineScrambleIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr, regVal;
    uint32 scrambleMask = FaceplateLineScrambleMask(self, lineId);
    uint32 rxScramble, txScramble;

    regAddr = FaceplateGlbtfmAddress(self, lineId);
    regVal  = mModuleHwRead(self, regAddr);
    txScramble = regVal & scrambleMask;

    regAddr = FaceplateGlbrfmAddress(self, lineId);
    regVal  = mModuleHwRead(self, regAddr);
    rxScramble = regVal & scrambleMask;

    if (txScramble != rxScramble)
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Line %d has different TX/RX scramble(TX = %d, RX = %d)\r\n",
                    lineId + 1, txScramble, rxScramble);
        }

    return txScramble ? cAtTrue : cAtFalse;
    }

static uint32 PohProcessorSideRegister(ThaModuleOcn self)
    {
    return cAf6Reg_glbloop_reg_Base + ThaModuleOcnBaseAddress(self);
    }

static eTha6029LineSide HwPohProcessorSideGet(ThaModuleOcn self)
    {
    uint32 regAddr = PohProcessorSideRegister(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 fieldVal = mRegField(regVal, cPohSideSelect);
    return (fieldVal == 1) ? cTha6029LineSideMate : cTha6029LineSideFaceplate;
    }

static eAtRet HwPohProcessorSideSet(ThaModuleOcn self, eTha6029LineSide side)
    {
    uint32 regAddr = PohProcessorSideRegister(self);
    uint32 regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cPohSideSelect, (side == cTha6029LineSideMate) ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eAtRet PohProcessorSideSet(ThaModuleOcn self, eTha6029LineSide side)
    {
    eAtRet ret = HwPohProcessorSideSet(self, side);
    mThis(self)->pohSide = side;
    return ret;
    }

static eTha6029LineSide PohProcessorSideGet(ThaModuleOcn self)
    {
    if (mThis(self)->pohSide == cTha6029LineSideUnknown)
        mThis(self)->pohSide = HwPohProcessorSideGet(self);
    return mThis(self)->pohSide;
    }

static const char *PohSideString(AtModule self)
    {
    if (Tha60290021ModuleOcnPohProcessorSideGet((ThaModuleOcn)self) == cTha6029LineSideFaceplate)
        return "faceplate";
    return "mate";
    }

static uint32 NumFaceplateSlices(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 4;
    }

static eAtRet ShowConcateDebugInformation(Tha60210011ModuleOcn self)
    {
    uint32 numLines = Tha60290021ModuleSdhNumUseableMateLines(SdhModule((ThaModuleOcn)self));
    static const uint32 cIndentLevel = 1;

    AtPrintc(cSevInfo, "\r\n");
    AtPrintc(cSevInfo, "* MATE concatenation\r\n");
    Tha60210011ModuleOcnDebugConcateWithIndent((ThaModuleOcn)self, numLines, cAf6Reg_tfi5spiramctl_Base, cIndentLevel);

    AtPrintc(cSevInfo, "\r\n");
    AtPrintc(cSevInfo, "* Faceplate concatenation\r\n");
    return Tha60210011ModuleOcnDebugConcateWithIndent((ThaModuleOcn)self, mMethodsGet(mThis(self))->NumFaceplateSlices(mThis(self)), cAf6Reg_spiramctl_Base, cIndentLevel);
    }

static eAtRet Debug(AtModule self)
    {
    m_AtModuleMethods->Debug(self);

    AtPrintc(cSevNormal, "\r\n");
    AtPrintc(cSevNormal, "* POH processor side: %s\r\n", PohSideString(self));

    return cAtOk;
    }

static uint32 FaceplateB1ForceShift(ThaModuleOcn self, uint32 faceplateLineId)
    {
    AtUnused(self);
    return mFaceplateLineLocalInGroup(faceplateLineId);
    }

static uint32 FaceplateB1ForceMask(ThaModuleOcn self, uint32 faceplateLineId)
    {
    AtUnused(self);
    return cBit0 << mFaceplateLineLocalInGroup(faceplateLineId);
    }

static uint32 FaceplateOofForceShift(ThaModuleOcn self, uint32 faceplateLineId)
    {
    AtUnused(self);
    return mFaceplateLineLocalInGroup(faceplateLineId) + 8;
    }

static uint32 FaceplateOofForceMask(ThaModuleOcn self, uint32 faceplateLineId)
    {
    AtUnused(self);
    return cBit8 << mFaceplateLineLocalInGroup(faceplateLineId);
    }

static uint32 FaceplateTxB1ForceRegister(ThaModuleOcn self, uint32 faceplateLineId)
    {
    return FaceplateLineAddressWithLocalAddress(self, faceplateLineId, cAf6Reg_glbtfmfrc_reg_Base);
    }

static uint32 FaceplateTxOofForceRegister(ThaModuleOcn self, uint32 faceplateLineId)
    {
    return FaceplateLineAddressWithLocalAddress(self, faceplateLineId, cAf6Reg_glbtfmfrc_reg_Base);
    }

static uint32 FaceplateTxOverheadByteIndexSw2Hw(uint32 overheadByte)
    {
    switch (overheadByte)
        {
        case cAtSdhLineRsOverheadByteZ0: return 0;
        case cAtSdhLineMsOverheadByteZ1: return 0;
        case cAtSdhLineRsOverheadByteE1: return 1;
        case cAtSdhLineMsOverheadByteZ2: return 1;
        case cAtSdhLineRsOverheadByteF1: return 2;
        case cAtSdhLineMsOverheadByteE2: return 2;
        default:
            return 3;
        }
    }

static uint32 FaceplateTxOverheadByteMask(uint32 overheadByte)
    {
    uint32 overheadByteIndex = FaceplateTxOverheadByteIndexSw2Hw(overheadByte);
    return cBit7_0 << (overheadByteIndex * 8);
    }

static uint32 FaceplateTxOverheadByteShift(uint32 overheadByte)
    {
    uint32 overheadByteIndex = FaceplateTxOverheadByteIndexSw2Hw(overheadByte);
    return (overheadByteIndex * 8);
    }

static uint32 FaceplateLineOverheadByteAbsoluteOffset(ThaModuleOcn self, uint32 lineId)
    {
    return mMethodsGet(mThis(self))->FaceplateLineTxDefaultOffset(mThis(self), lineId);
    }

static uint32 FaceplateLineTxDefaultOffset(Tha60290021ModuleOcn self, uint32 lineId)
    {
    uint32 baseAddress = ThaModuleOcnBaseAddress((ThaModuleOcn)self);
    uint32 groupOffset = mMethodsGet(self)->FaceplateGroupOffset(self, FaceplateLineGroup((ThaModuleOcn)self, lineId));
    uint32 localLineId = mFaceplateLineLocalInGroup(lineId);
    return 256 * localLineId + groupOffset + baseAddress;
    }

static uint32 FaceplateLineRxOverheadByteAbsoluteOffset(ThaModuleOcn self, uint32 lineId)
    {
    return mMethodsGet(mThis(self))->FaceplateLineDefaultOffset(mThis(self), lineId);
    }

static uint32 XcLineOffset(ThaModuleOcn self, uint32 lineId)
    {
    AtUnused(self);
    return 256 * lineId;
    }

static uint32 FaceplateTxOverheadByteRegister(ThaModuleOcn self, uint32 faceplateLineId, uint32 overheadByte)
    {
    uint32 offset = FaceplateLineOverheadByteAbsoluteOffset(self, faceplateLineId);

    switch (overheadByte)
        {
        case cAtSdhLineRsOverheadByteZ0       : return cAf6Reg_tfmregctl1_Base + offset;
        case cAtSdhLineRsOverheadByteE1       : return cAf6Reg_tfmregctl1_Base + offset;
        case cAtSdhLineRsOverheadByteF1       : return cAf6Reg_tfmregctl1_Base + offset;
        case cAtSdhLineRsOverheadByteUndefined: return cAf6Reg_tfmregctl1_Base + offset;
        case cAtSdhLineMsOverheadByteE2       : return cAf6Reg_tfmregctl2_Base + offset;
        case cAtSdhLineMsOverheadByteZ1       : return cAf6Reg_tfmregctl2_Base + offset;
        case cAtSdhLineMsOverheadByteZ2       : return cAf6Reg_tfmregctl2_Base + offset;
        case cAtSdhLineMsOverheadByteUndefined: return cAf6Reg_tfmregctl2_Base + offset;
        default: return cInvalidUint32;
        }
    }

static eBool HasOverheadCpuControl(Tha60290021ModuleOcn self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x0, 0x00);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(mVersionReader(self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool FacelateLineTxOverheadByteIsSupported(ThaModuleOcn self, uint32 faceplateLineId, uint32 overheadByte)
    {
    if (!mMethodsGet(mThis(self))->HasOverheadCpuControl(mThis(self)))
        return cAtFalse;

    if (FaceplateTxOverheadByteRegister(self, faceplateLineId, overheadByte) == cInvalidUint32)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 FaceplateRxOverheadByteMask(uint32 overheadByte)
    {
    AtUnused(overheadByte);
    return cBit7_0;
    }

static uint32 FaceplateRxOverheadByteShift(uint32 overheadByte)
    {
    AtUnused(overheadByte);
    return 0;
    }

static uint32 FaceplateRxOverheadByteRegister(ThaModuleOcn self, uint32 faceplateLineId, uint32 overheadByte)
    {
    uint32 offset = FaceplateLineRxOverheadByteAbsoluteOffset(self, faceplateLineId);
    uint32 address = mMethodsGet(mThis(self))->FaceplateLineRxOverheadByteRegAddr(mThis(self), overheadByte);

    if (address == cInvalidUint32)
        return cInvalidUint32;

    return address + offset;
    }

static uint32 FaceplateLineRxOverheadByteRegAddr(Tha60290021ModuleOcn self, uint32 overheadByte)
    {
    AtUnused(self);
    switch (overheadByte)
        {
        case cAtSdhLineRsOverheadByteE1: return cAf6Reg_tohe1byte_Base;
        case cAtSdhLineRsOverheadByteF1: return cAf6Reg_tohf1byte_Base;
        case cAtSdhLineMsOverheadByteE2: return cAf6Reg_tohe2byte_Base;
        case cAtSdhLineMsOverheadByteZ1: return cAf6Reg_tohz1byte_Base;
        case cAtSdhLineMsOverheadByteZ2: return cAf6Reg_tohz2byte_Base;
        default: return cInvalidUint32;
        }
    }

static uint32 FaceplateRxZnPositionRegister(ThaModuleOcn self, uint32 faceplateLineId)
    {
    uint32 offset = FaceplateLineOverheadByteAbsoluteOffset(self, faceplateLineId);
    return cAf6Reg_rfmz1z2selctl_Base + offset;
    }

static eBool FacelateLineRxOverheadByteIsSupported(ThaModuleOcn self, uint32 faceplateLineId, uint32 overheadByte)
    {
    if (!mMethodsGet(mThis(self))->HasOverheadCpuControl(mThis(self)))
        return cAtFalse;

    if (FaceplateRxOverheadByteRegister(self, faceplateLineId, overheadByte) == cInvalidUint32)
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet FaceplateLineTxB1Force(ThaModuleOcn self, uint32 lineId, eBool enabled)
    {
    uint32 regAddr = FaceplateTxB1ForceRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal,
              FaceplateB1ForceMask(self, lineId),
              FaceplateB1ForceShift(self, lineId),
              enabled ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool FaceplateLineTxB1IsForced(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateTxB1ForceRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & FaceplateB1ForceMask(self, lineId)) ? cAtTrue : cAtFalse;
    }

static eAtRet FaceplateLineTxOofForce(ThaModuleOcn self, uint32 lineId, eBool enabled)
    {
    uint32 regAddr = FaceplateTxOofForceRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal,
              FaceplateOofForceMask(self, lineId),
              FaceplateOofForceShift(self, lineId),
              enabled ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool FaceplateLineTxOofIsForced(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateTxOofForceRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & FaceplateOofForceMask(self, lineId)) ? cAtTrue : cAtFalse;
    }

static eAtModuleSdhRet FaceplateLineTxOverheadByteSet(ThaModuleOcn self, uint32 lineId, uint32 overheadByte, uint8 value)
    {
    uint32 regAddr = FaceplateTxOverheadByteRegister(self, lineId, overheadByte);
    uint32 sohByte_Mask = FaceplateTxOverheadByteMask(overheadByte);
    uint32 sohByte_Shift = FaceplateTxOverheadByteShift(overheadByte);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, sohByte_, value);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint8 FaceplateLineTxOverheadByteGet(ThaModuleOcn self, uint32 lineId, uint32 overheadByte)
    {
    uint32 regAddr = FaceplateTxOverheadByteRegister(self, lineId, overheadByte);
    uint32 sohByte_Mask = FaceplateTxOverheadByteMask(overheadByte);
    uint32 sohByte_Shift = FaceplateTxOverheadByteShift(overheadByte);
    uint32 regVal = mModuleHwRead(self, regAddr);

    return (uint8)mRegField(regVal, sohByte_);
    }

static uint8 FaceplateLineRxOverheadByteGet(ThaModuleOcn self, uint32 lineId, uint32 overheadByte)
    {
    uint32 regAddr = FaceplateRxOverheadByteRegister(self, lineId, overheadByte);
    uint32 sohByteMask = FaceplateRxOverheadByteMask(overheadByte);
    uint32 sohByteShift = FaceplateRxOverheadByteShift(overheadByte);
    uint32 regVal = mModuleHwRead(self, regAddr);

    return (uint8)mRegField(regVal, sohByte);
    }

static eAtRet FaceplateLineRxZ1PositionSet(ThaModuleOcn self, uint32 lineId, uint8 sts1)
    {
    uint32 regAddr = FaceplateRxZnPositionRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 fieldVal = mRegField(regVal, cAf6_rfmz1z2selctl_OCNRxZ1MonStsSel_);
    uint8 currentSts1 = mMethodsGet(mThis(self))->FaceplateLineRxZnRegValue2Position(mThis(self), fieldVal);

    if (currentSts1 == sts1)
        return cAtOk;

    fieldVal = mMethodsGet(mThis(self))->FaceplateLineRxZnPosition2RegValue(mThis(self), sts1);
    mRegFieldSet(regVal, cAf6_rfmz1z2selctl_OCNRxZ1MonStsSel_, fieldVal);
    mModuleHwWrite(self, regAddr, regVal);

    /* Give hardware a short time to capture new value */
    AtOsalUSleep(1000);

    return cAtOk;
    }

static uint8 FaceplateLineRxZ1PositionGet(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateRxZnPositionRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 fieldVal = mRegField(regVal, cAf6_rfmz1z2selctl_OCNRxZ1MonStsSel_);

    return mMethodsGet(mThis(self))->FaceplateLineRxZnRegValue2Position(mThis(self), fieldVal);
    }

static eAtRet FaceplateLineRxZ2PositionSet(ThaModuleOcn self, uint32 lineId, uint8 sts1)
    {
    uint32 regAddr = FaceplateRxZnPositionRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 fieldVal = mRegField(regVal, cAf6_rfmz1z2selctl_OCNRxZ2MonStsSel_);
    uint8 currentSts1 = mMethodsGet(mThis(self))->FaceplateLineRxZnRegValue2Position(mThis(self), fieldVal);

    if (currentSts1 == sts1)
        return cAtOk;

    fieldVal = mMethodsGet(mThis(self))->FaceplateLineRxZnPosition2RegValue(mThis(self), sts1);
    mRegFieldSet(regVal, cAf6_rfmz1z2selctl_OCNRxZ2MonStsSel_, fieldVal);
    mModuleHwWrite(self, regAddr, regVal);

    /* Delay for HW applies cfg */
    AtOsalUSleep(1000);

    return cAtOk;
    }

static uint8 FaceplateLineRxZ2PositionGet(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateRxZnPositionRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 fieldVal = mRegField(regVal, cAf6_rfmz1z2selctl_OCNRxZ2MonStsSel_);

    return mMethodsGet(mThis(self))->FaceplateLineRxZnRegValue2Position(mThis(self), fieldVal);
    }

static uint8 StartSts1ToScanConcate(Tha60210011ModuleOcn self, AtSdhChannel aug)
    {
    uint32 slice;

    if (IsMateChannel(aug))
        return m_Tha60210011ModuleOcnMethods->StartSts1ToScanConcate(self, aug);

    slice = AtSdhChannelSts1Get(aug) / cNumSts1InOc48Slice;
    return (uint8)(slice * cNumSts1InOc48Slice);
    }

static uint8 NumStsToReleaseConcate(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    uint32 numSts = AtSdhChannelNumSts((AtSdhChannel)AtSdhChannelLineObjectGet(channel));
    AtUnused(self);
    return (uint8)mMin(numSts, cNumSts1InOc48Slice);
    }

static uint32 FaceplateExtraRateLocalId(uint32 faceplateLineId)
    {
    return (uint32) (faceplateLineId % 8);
    }

static uint32 FaceplateExtraRateGroupId(uint32 faceplateLineId)
    {
    return (uint32) (faceplateLineId / 8);
    }

static uint32 FaceplateExtraRateShift(ThaModuleOcn self, uint32 faceplateLineId)
    {
    AtUnused(self);
    return (FaceplateExtraRateLocalId(faceplateLineId) * 4);
    }

static uint32 FaceplateExtraRateMask(ThaModuleOcn self, uint32 faceplateLineId)
    {
    AtUnused(self);
    return cBit3_0 << (FaceplateExtraRateLocalId(faceplateLineId) * 4);
    }

static uint32 FaceplateExtraRateRegister(ThaModuleOcn self, uint32 faceplateLineId)
    {
    AtUnused(self);

    if (FaceplateExtraRateGroupId(faceplateLineId) == 0)
        return 0xF00048;

    return 0xF00049;
    }

static uint32 ExtraRateSw2Hw(uint32 swRate)
    {
    switch (swRate)
        {
        case cAtSdhLineRateStm0:  return 2;
        case cAtSdhLineRateStm1:  return 1;
        case cAtSdhLineRateStm4:  return 2;
        case cAtSdhLineRateStm16: return 1;
        case cAtSdhLineRateStm64: return 0;
        default: return 3;
        }
    }

static eAtRet LineRateExtraSet(ThaModuleOcn self, uint32 lineId, eAtSdhLineRate rate)
    {
    uint32 regAddr = FaceplateExtraRateRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint32 rateFieldMask = FaceplateExtraRateMask(self, lineId);
    uint32 rateFieldShift = FaceplateExtraRateShift(self, lineId);
    mRegFieldSet(regVal, rateField, ExtraRateSw2Hw(rate));
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool FaceplateLineIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    uint32 rxEnabled = FaceplateLineRxIsEnabled(self, lineId) ? 1 : 0;
    uint32 txEnabled = FaceplateLineTxIsEnabled(self, lineId) ? 1 : 0;

    if (rxEnabled == txEnabled)
        return rxEnabled ? cAtTrue : cAtFalse;

    AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Faceplate line %d, RX is %s but TX is %s\r\n",
                lineId + 1,
                rxEnabled ? "enabled" : "disabled",
                txEnabled ? "enabled" : "disabled");
    return (rxEnabled | txEnabled) ? cAtTrue : cAtFalse;
    }

static eBool HoPathPhyModuleIsDividedToOc24(Tha60210011ModuleOcn self, eAtModule phyModule)
    {
    AtUnused(self);
    if (phyModule == cAtModulePdh)
        return cAtTrue;
    return m_Tha60210011ModuleOcnMethods->HoPathPhyModuleIsDividedToOc24(self, phyModule);
    }

static uint32 FaceplateAisFordwardingRegister(ThaModuleOcn self, uint32 lineId)
    {
    return FaceplateLineAddressWithLocalAddress(self, lineId, cAf6Reg_glbrfmaisfwd_reg_Base);
    }

static uint32 FaceplateAisFordwardingShift(ThaModuleOcn self, uint32 lineId, uint8 offset)
    {
    AtUnused(self);
    return (Tha60290021ModuleOcnFaceplateLineLocalId(self, lineId) * 4) + offset;
    }

static uint32 FaceplateAisFordwardingMask(ThaModuleOcn self, uint32 lineId, uint8 offset)
    {
    uint32 shift = FaceplateAisFordwardingShift(self, lineId, offset);
    return cBit0 << shift;
    }

static uint8 AlarmTypeOffset(uint32 alarmType)
    {
    switch (alarmType)
        {
        case cAtSdhLineAlarmLos: return 0;
        case cAtSdhLineAlarmLof: return 1;
        case cAtSdhLineAlarmTim: return 2;
        case cAtSdhLineAlarmAis: return 3;
        default: return cInvalidUint8;
        }
    }

static eAtRet FaceplateAisFordwardingEnableWithRegister(ThaModuleOcn self, uint32 lineId, uint32 alarmType, eBool enable, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint8  offset = AlarmTypeOffset(alarmType);

    /* This Alarm Type is not applicable */
    if (offset == cInvalidUint8)
        return cAtErrorNotApplicable;

    mFieldIns(&regVal,FaceplateAisFordwardingMask(self, lineId, offset), FaceplateAisFordwardingShift(self, lineId, offset), enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static eBool FaceplateAisFordwardingIsEnabledWithRegister(ThaModuleOcn self, uint32 lineId, uint32 alarmType, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);
    uint8  offset = AlarmTypeOffset(alarmType);

    /* This Alarm Type is not applicable */
    if (offset == cInvalidUint8)
        return cAtTrue;

    if (regVal & FaceplateAisFordwardingMask(self, lineId, offset))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet FaceplateAisFordwardingEnable(ThaModuleOcn self, uint32 lineId, uint32 alarmType, eBool enable)
    {
    uint32 regAddr = FaceplateAisFordwardingRegister(self, lineId);
    return FaceplateAisFordwardingEnableWithRegister(self, lineId, alarmType, enable, regAddr);
    }

static eBool FaceplateAisFordwardingIsEnabled(ThaModuleOcn self, uint32 lineId, uint32 alarmType)
    {
    uint32 regAddr = FaceplateAisFordwardingRegister(self, lineId);
    return FaceplateAisFordwardingIsEnabledWithRegister(self, lineId, alarmType, regAddr);
    }

static uint32 FaceplateAutoRdiRegister(ThaModuleOcn self, uint32 lineId)
    {
    return FaceplateLineAddressWithLocalAddress(self, lineId, cAf6Reg_glbrfmrdibwd_reg_Base);
    }

static eAtRet FaceplateAutoRdiEnable(ThaModuleOcn self, uint32 lineId, uint32 alarmType, eBool enable)
    {
    uint32 regAddr = FaceplateAutoRdiRegister(self, lineId);
    return FaceplateAisFordwardingEnableWithRegister(self, lineId, alarmType, enable, regAddr);
    }

static eBool FaceplateAutoIsRdiEnabled(ThaModuleOcn self, uint32 lineId, uint32 alarmType)
    {
    uint32 regAddr = FaceplateAutoRdiRegister(self, lineId);
    return FaceplateAisFordwardingIsEnabledWithRegister(self, lineId, alarmType, regAddr);
    }

static const char **AllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    static const char * description[] =
        {
         "TFI-5 OCN Rx Bridge and Roll SXC Control 0",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 1",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 2",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 3",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 4",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 5",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 6",
         "TFI-5 OCN Rx Bridge and Roll SXC Control 7",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 0",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 1",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 2",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 3",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 4",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 5",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 6",
         "TFI-5 OCN Tx Bridge and Roll SXC Control 7",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 0",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 1",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 2",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 3",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 4",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 5",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 6",
         "TFI-5 OCN STS Pointer Interpreter Per Channel Control 7",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 0",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 1",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 2",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 3",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 4",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 5",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 6",
         "TFI-5 OCN STS Pointer Generator Per Channel Control 7", /*31*/

         "Line Side OCN Tx J0 Insertion Buffer 0",/*32*/
         "Line Side OCN Tx J0 Insertion Buffer 1",
         "Line Side OCN Tx J0 Insertion Buffer 2",
         "Line Side OCN Tx J0 Insertion Buffer 3",
         "Line Side OCN Tx J0 Insertion Buffer 4",
         "Line Side OCN Tx J0 Insertion Buffer 5",
         "Line Side OCN Tx J0 Insertion Buffer 6",
         "Line Side OCN Tx J0 Insertion Buffer 7",
         "Line Side OCN STS Pointer Interpreter Per Channel Control 0", /*40*/
         "Line Side OCN STS Pointer Interpreter Per Channel Control 1",
         "Line Side OCN STS Pointer Interpreter Per Channel Control 2",
         "Line Side OCN STS Pointer Interpreter Per Channel Control 3",
         "Line Side OCN STS Pointer Generator Per Channel Control 0",/*44*/
         "Line Side OCN STS Pointer Generator Per Channel Control 1",
         "Line Side OCN STS Pointer Generator Per Channel Control 2",
         "Line Side OCN STS Pointer Generator Per Channel Control 3", /*47*/
         "Line Side OCN TOH Monitoring Per Line Control", /*48*/

         "OCN RXPP Per STS payload Control 0",/*49*/
         "OCN RXPP Per STS payload Control 1",
         "OCN RXPP Per STS payload Control 2",
         "OCN RXPP Per STS payload Control 3",
         "OCN VTTU Pointer Interpreter Per Channel Control 0", /* 53 */
         "OCN VTTU Pointer Interpreter Per Channel Control 1",
         "OCN VTTU Pointer Interpreter Per Channel Control 2",
         "OCN VTTU Pointer Interpreter Per Channel Control 3",
         "OCN TXPP Per STS Multiplexing Control 0", /* 57 */
         "OCN TXPP Per STS Multiplexing Control 1",
         "OCN TXPP Per STS Multiplexing Control 2",
         "OCN TXPP Per STS Multiplexing Control 3",
         "OCN VTTU Pointer Generator Per Channel Control 0", /* 61 */
         "OCN VTTU Pointer Generator Per Channel Control 1",
         "OCN VTTU Pointer Generator Per Channel Control 2",
         "OCN VTTU Pointer Generator Per Channel Control 3",
         "OCN Rx High Order Map concatenate configuration 0", /* 65 */
         "OCN Rx High Order Map concatenate configuration 1",
         "OCN Rx High Order Map concatenate configuration 2",
         "OCN Rx High Order Map concatenate configuration 3", /*68*/

         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 0",/*69*/
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 1",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 2",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 3",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 4",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 5",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 6",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 7",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 8",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 9",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 10",
         "OCN SXC Control 1 - Config Page 0 and Page 1 of SXC 11", /* 80 */

         "OCN SXC Control 2 - Config Page 2 of SXC 0",/**81*/
         "OCN SXC Control 2 - Config Page 2 of SXC 1",
         "OCN SXC Control 2 - Config Page 2 of SXC 2",
         "OCN SXC Control 2 - Config Page 2 of SXC 3",
         "OCN SXC Control 2 - Config Page 2 of SXC 4",
         "OCN SXC Control 2 - Config Page 2 of SXC 5",
         "OCN SXC Control 2 - Config Page 2 of SXC 6",
         "OCN SXC Control 2 - Config Page 2 of SXC 7",
         "OCN SXC Control 2 - Config Page 2 of SXC 8",
         "OCN SXC Control 2 - Config Page 2 of SXC 9",
         "OCN SXC Control 2 - Config Page 2 of SXC 10",
         "OCN SXC Control 2 - Config Page 2 of SXC 11",/*92*/

         "OCN SXC Control 3 - Config APS 0",/*93*/
         "OCN SXC Control 3 - Config APS 1",
         "OCN SXC Control 3 - Config APS 2",
         "OCN SXC Control 3 - Config APS 3",
         "OCN SXC Control 3 - Config APS 4",
         "OCN SXC Control 3 - Config APS 5",
         "OCN SXC Control 3 - Config APS 6",
         "OCN SXC Control 3 - Config APS 7",
         "OCN SXC Control 3 - Config APS 8",
         "OCN SXC Control 3 - Config APS 9",
         "OCN SXC Control 3 - Config APS 10",
         "OCN SXC Control 3 - Config APS 11" /*104*/
        };
    AtUnused(self);

    if (numRams)
        *numRams = mCount(description);

    return description;
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    return Tha60290021InternalRamOcnNew(self, ramId, localRamId);
    }

static eAtRet RoleSet(AtModule self, eAtDeviceRole role)
    {
    AtUnused(self);
    AtUnused(role);
    return cAtOk;
    }

static eBool HasRoleStatus(Tha60290021ModuleOcn self)
    {
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x0, 0x0);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(mVersionReader(self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eAtDeviceRole HwRoleGet(ThaModuleOcn self)
    {
    uint32 regAddr = ThaModuleOcnBaseAddress(self) + cAf6Reg_glbfsm_reg_Base;
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & cAf6_glbfsm_reg_FsmValue_Mask) ? cAtDeviceRoleStandby : cAtDeviceRoleActive;
    }

static eAtDeviceRole RoleGet(AtModule self)
    {
    if (mMethodsGet(mThis(self))->HasRoleStatus(mThis(self)))
        return HwRoleGet((ThaModuleOcn)self);

    return m_AtModuleMethods->RoleGet(self);
    }

static eBool HasRole(AtModule self)
    {
    if (mMethodsGet(mThis(self))->HasRoleStatus(mThis(self)))
        return cAtTrue;

    return m_AtModuleMethods->HasRole(self);
    }

static eAtRet RoleInputEnable(AtModule self, eBool enable)
    {
    AtUnused(self);
    return (enable) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtRet RoleActiveForce(AtModule self, eBool forced)
    {
    AtUnused(self);
    return (forced) ? cAtErrorModeNotSupport : cAtOk;
    }

static uint32 AddressWithLocalAddress(AtModule self, uint32 localAddress)
    {
    return ThaModuleOcnBaseAddress((ThaModuleOcn)self) + localAddress;
    }

static ThaModuleXc XcModule(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return (ThaModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static void XcLineHwFlush(AtModule self, uint32 lineId)
    {
    uint32 sts1;
    ThaModuleOcn ocnModule = (ThaModuleOcn)self;
    uint32 lineOffset = XcLineOffset(ocnModule, lineId);
    uint32 baseAddress = ThaModuleOcnBaseAddress(ocnModule);
    ThaModuleXc xcModule = XcModule(self);
    uint32 disconnectSliceId = ThaModuleXcDisconnectSliceId(xcModule);
    ThaVcCrossConnect xc = (ThaVcCrossConnect)AtModuleXcVcCrossConnectGet((AtModuleXc)xcModule);

    for (sts1 = 0; sts1 < cNumSts1InOc48Slice;sts1++)
        {
        uint32 absoluteOffset = lineOffset + sts1 + baseAddress;
        uint32 regAddr, regVal;
        uint32 fieldMask, fieldShift;

        regAddr = cAf6Reg_sxcramctl0_Base + absoluteOffset;
        regVal = mModuleHwRead(self, regAddr);

        fieldMask = ThaVcCrossConnectSliceIdPageMask(xc, 1);
        fieldShift = ThaVcCrossConnectSliceIdPageShift(xc, 1);
        mRegFieldSet(regVal, field, disconnectSliceId);
        mRegFieldSet(regVal, cAf6_sxcramctl0_SxcStsIdPage1_, cAf6_sxcramctl0_SxcStsIdPage1_Mask >> cAf6_sxcramctl0_SxcStsIdPage1_Shift);

        fieldMask = ThaVcCrossConnectSliceIdPageMask(xc, 0);
        fieldShift = ThaVcCrossConnectSliceIdPageShift(xc, 0);
        mRegFieldSet(regVal, field, disconnectSliceId);
        mRegFieldSet(regVal, cAf6_sxcramctl0_SxcStsIdPage0_, cAf6_sxcramctl0_SxcStsIdPage0_Mask >> cAf6_sxcramctl0_SxcStsIdPage0_Shift);
        mModuleHwWrite(self, regAddr, regVal);

        regAddr = cAf6Reg_sxcramctl1_Base + absoluteOffset;
        regVal = mModuleHwRead(self, regAddr);
        fieldMask = ThaVcCrossConnectSliceIdPageMask(xc, 2);
        fieldShift = ThaVcCrossConnectSliceIdPageShift(xc, 2);
        mRegFieldSet(regVal, field, disconnectSliceId);
        mRegFieldSet(regVal, cAf6_sxcramctl1_SxcStsIdPage2_, cAf6_sxcramctl1_SxcStsIdPage2_Mask >> cAf6_sxcramctl1_SxcStsIdPage2_Shift);
        mModuleHwWrite(self, regAddr, regVal);

        regAddr = cAf6Reg_apsramctl_Base + absoluteOffset;
        regVal = 0;
        mRegFieldSet(regVal, cAf6_apsramctl_SelectorID_, cAf6_apsramctl_SelectorID_Mask >> cAf6_apsramctl_SelectorID_Shift);
        mModuleHwWrite(self, regAddr, regVal);
        }
    }

static void XcHwFlush(AtModule self)
    {
    uint32 lineId;
    uint32 numLines = mMethodsGet(mThis(self))->NumXcLines(mThis(self));

    for (lineId = 0; lineId < numLines; lineId++)
        XcLineHwFlush(self, lineId);
    }

static void ApsHwFlush(AtModule self)
    {
    mModuleHwWrite(self, AddressWithLocalAddress(self, cAf6Reg_glbapsgrppage_reg_Base), 0);
    }

static void HwFlush(AtModule self)
    {
    ApsHwFlush(self);
    XcHwFlush(self);
    }

static uint32 OcnVtTuPointerInterpreterPerChannelAdjustCountRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_adjcntperstkram_Base;
    }

static uint32 OcnVtTuPointerGeneratorPerChannelAdjustCountRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cAf6Reg_adjcntpgpervtram_Base;
    }

static uint32 FaceplateGroupOfChannel(ThaModuleOcn self, AtSdhChannel channel)
    {
    return FaceplateLineGroup(self, AtSdhChannelLineGet(channel));
    }

static uint32 FaceplateChannelGroupOffset(ThaModuleOcn self, AtSdhChannel channel)
    {
    return mMethodsGet(mThis(self))->FaceplateGroupOffset(mThis(self), FaceplateGroupOfChannel(self, channel));
    }

static uint32 HwStsDefaultOffset(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    eBool isFaceplate = IsFaceplateChannel(channel);
    uint8 faceplateLocalSlice = FaceplateSliceLocalId((ThaModuleOcn)self, hwSlice);
    uint8 sliceToAccess = (uint8)(isFaceplate ? faceplateLocalSlice : hwSlice);
    uint32 offset = m_Tha60210011ModuleOcnMethods->HwStsDefaultOffset(self, channel, sliceToAccess, hwSts);

    if (isFaceplate)
        offset = offset + FaceplateChannelGroupOffset((ThaModuleOcn)self, channel);

    return offset;
    }

static eAtRet PiStsGroupIdSet(ThaModuleOcn self, AtSdhChannel channel, uint8 sts1, uint32 groupId)
    {
    Tha60210011ModuleOcn ocnModule = (Tha60210011ModuleOcn)self;
    uint32 regAddr, regVal;

    regAddr = Tha60210011ModuleOcnStsPointerInterpreterPerChannelControl(ocnModule, channel) +
              ThaModuleOcnStsDefaultOffset(self, channel, sts1);
    regVal  = mModuleHwRead(self, regAddr);
    if (groupId != cInvalidUint32)
        {
        mRegFieldSet(regVal, cAf6_spiramctl_LinePassThrEnb_, 1);
        mRegFieldSet(regVal, cAf6_spiramctl_LinePassThrGrp_, groupId);
        }
    else
        {
        mRegFieldSet(regVal, cAf6_spiramctl_LinePassThrEnb_, 0);
        mRegFieldSet(regVal, cAf6_spiramctl_LinePassThrGrp_, 0);
        }

    mModuleHwWrite(self, regAddr, regVal);
    return cAtOk;
    }

static uint32 NumXcLines(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return 12;
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    if (ThaModuleOcnIsSohRegister((ThaModuleOcn)self, localAddress))
        return ThaModuleOcnSohOverEthLongRegisterAccess((ThaModuleOcn)self);
    return m_AtModuleMethods->LongRegisterAccess(self, localAddress);
    }

static uint8 FaceplateLineRxZnRegValue2Position(Tha60290021ModuleOcn self, uint32 regVal)
    {
    AtUnused(self);
    return (uint8)regVal;
    }

static uint32 FaceplateLineRxZnPosition2RegValue(Tha60290021ModuleOcn self, uint8 sts1)
    {
    AtUnused(self);
    return (uint32)sts1;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290021ModuleOcn object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeUInt(pohSide);
    }

static eBool HasDiagnosticIp(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 OcnVtTuPointerGeneratorNdfIntrMask(Tha60210011ModuleOcn self)
    {
    AtUnused(self);
    return cAf6_vtpgstkram_VtPgNdfIntr_Mask;
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    static const uint32 reserved[] = {4, 5, 6, 7, 12, 13, 14, 15, 20, 21, 22, 23, 28, 29, 30, 31};
    uint32 numReserved = mCount(reserved);
    uint32 localId = AtInternalRamLocalIdGet(ram);
    uint32 ram_i;

    AtUnused(self);

    for (ram_i = 0; ram_i < numReserved; ram_i++)
        {
        if (reserved[ram_i] == localId)
            return cAtTrue;
        }

    return cAtFalse;
    }

static eBool Sts192cIsSupported(Tha60290021ModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return AtDeviceIsSimulated(device);
    }

static eBool CanMovePathAisRxForcingPoint(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 FaceplateTxSeparatedSectionAndLineDccInsControlRegister(ThaModuleOcn self, uint32 lineId)
    {
    return Tha60290021ModuleOcnFaceplateLineAddressWithLocalAddress(self, lineId, cAf6Reg_glbtfmfrc_reg_Base);
    }

static uint32 FaceplateTxSectionDccEnableMask(ThaModuleOcn self, uint32 lineId)
    {
    uint32 localLineId = Tha60290021ModuleOcnFaceplateLineLocalId(self, lineId);
    return cAf6_tfi5glbtfm_reg_TxFrmOhBusDccEnb1_Section_Mask << (localLineId * 2);
    }

static uint32 FaceplateTxSectionDccEnableShift(ThaModuleOcn self, uint32 lineId)
    {
    uint32 localLineId = Tha60290021ModuleOcnFaceplateLineLocalId(self, lineId);
    return (cAf6_tfi5glbtfm_reg_TxFrmOhBusDccEnb1_Section_Shift + (localLineId * 2));
    }

static uint32 FaceplateTxLineDccEnableMask(ThaModuleOcn self, uint32 lineId)
    {
    uint32 localLineId = Tha60290021ModuleOcnFaceplateLineLocalId(self, lineId);
    return cAf6_tfi5glbtfm_reg_TxFrmOhBusDccEnb1_Line_Mask << (localLineId * 2);
    }

static uint32 FaceplateTxLineDccEnableShift(ThaModuleOcn self, uint32 lineId)
    {
    uint32 localLineId = Tha60290021ModuleOcnFaceplateLineLocalId(self, lineId);
    return (cAf6_tfi5glbtfm_reg_TxFrmOhBusDccEnb1_Line_Shift + (localLineId * 2));
    }

static eBool FaceplateLineTxSectionDccIsEnabledCheckWithRegister(ThaModuleOcn self, uint32 lineId, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & FaceplateTxSectionDccEnableMask(self, lineId)) ? cAtTrue : cAtFalse;
    }

static eBool FaceplateLineTxLineDccIsEnabledCheckWithRegister(ThaModuleOcn self, uint32 lineId, uint32 regAddr)
    {
    uint32 regVal = mModuleHwRead(self, regAddr);
    return (regVal & FaceplateTxLineDccEnableMask(self, lineId)) ? cAtTrue : cAtFalse;
    }

static eAtRet FaceplateLineTxSectionDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    uint32 regAddr = FaceplateTxSeparatedSectionAndLineDccInsControlRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal,
              FaceplateTxSectionDccEnableMask(self, lineId),
              FaceplateTxSectionDccEnableShift(self, lineId),
              enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool FaceplateLineTxSectionDccInsByOhBusIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateTxSeparatedSectionAndLineDccInsControlRegister(self, lineId);
    return FaceplateLineTxSectionDccIsEnabledCheckWithRegister(self, lineId, regAddr);
    }

static eAtRet FaceplateLineTxLineDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    uint32 regAddr = FaceplateTxSeparatedSectionAndLineDccInsControlRegister(self, lineId);
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal,
              FaceplateTxLineDccEnableMask(self, lineId),
              FaceplateTxLineDccEnableShift(self, lineId),
              enable ? 1 : 0);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eBool FaceplateLineTxLineDccInsByOhBusIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    uint32 regAddr = FaceplateTxSeparatedSectionAndLineDccInsControlRegister(self, lineId);
    return FaceplateLineTxLineDccIsEnabledCheckWithRegister(self, lineId, regAddr);
    }

static eAtRet LineTxRsDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    return FaceplateLineTxSectionDccInsByOhBusEnable(self, lineId, enable);
    }

static eBool  LineTxRsDccInsByOhBusEnabled(ThaModuleOcn self, uint32 lineId)
    {
    return FaceplateLineTxSectionDccInsByOhBusIsEnabled(self, lineId);
    }

static eAtRet LineTxMsDccInsByOhBusEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    return FaceplateLineTxLineDccInsByOhBusEnable(self, lineId, enable);
    }

static eBool  LineTxMsDccInsByOhBusEnabled(ThaModuleOcn self, uint32 lineId)
    {
    return FaceplateLineTxLineDccInsByOhBusIsEnabled(self, lineId);
    }

static uint32 OcnStsPointerInterpreterPerChannelAdjustCountRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (channel == NULL)
        return m_Tha60210011ModuleOcnMethods->OcnStsPointerInterpreterPerChannelAdjustCountRegAddr(self, channel);

    if (IsFaceplateChannel(channel))
        return cAf6Reg_adjcntperstsram_Base;

    if (IsMateChannel(channel))
        return cAf6Reg_tfi5adjcntperstsram_Base;

    return m_Tha60210011ModuleOcnMethods->OcnStsPointerInterpreterPerChannelAdjustCountRegAddr(self, channel);
    }

static uint32 OcnStsPointerGeneratorPerChannelAdjustCountRegAddr(Tha60210011ModuleOcn self, AtSdhChannel channel)
    {
    if (channel == NULL)
        return m_Tha60210011ModuleOcnMethods->OcnStsPointerGeneratorPerChannelAdjustCountRegAddr(self, channel);

    if (IsFaceplateChannel(channel))
        return cAf6Reg_adjcntpgperstsram_Base;

    if (IsMateChannel(channel))
        return cAf6Reg_tfi5adjcntpgperstsram_Base;

    return m_Tha60210011ModuleOcnMethods->OcnStsPointerGeneratorPerChannelAdjustCountRegAddr(self, channel);
    }

static uint32 LineDefaultOffset(ThaModuleOcn self, AtSdhChannel sdhLine)
    {
    uint32 faceplateLineId = AtChannelIdGet((AtChannel)sdhLine);
    return mMethodsGet(mThis(self))->FaceplateLineDefaultOffset(mThis(self), faceplateLineId);
    }

static uint32 LineTxDefaultOffset(ThaModuleOcn self, AtSdhChannel sdhLine)
    {
    uint32 faceplateLineId = AtChannelIdGet((AtChannel)sdhLine);
    return mMethodsGet(mThis(self))->FaceplateLineTxDefaultOffset(mThis(self), faceplateLineId);
    }

static uint32 TxFramerPerChannelControlRegAddr(Tha60290021ModuleOcn self)
    {
    AtUnused(self);
    return cAf6Reg_tfmregctl_Base;
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Debug);
        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, AllInternalRamsDescription);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        mMethodOverride(m_AtModuleOverride, RoleSet);
        mMethodOverride(m_AtModuleOverride, RoleGet);
        mMethodOverride(m_AtModuleOverride, HasRole);
        mMethodOverride(m_AtModuleOverride, RoleInputEnable);
        mMethodOverride(m_AtModuleOverride, RoleActiveForce);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static void OverrideTha60210011ModuleOcn(AtModule self)
    {
    Tha60210011ModuleOcn module = (Tha60210011ModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleOcnMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleOcnOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleOcnOverride));

        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerInterpreterPerChannelControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerGeneratorPerChannelControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, Tfi5GlbtfmBase);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, Tfi5GlbrfmBase);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, GlbtpgBase);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, GlbvpiBase);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnRxHoMapConcatenateReg);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerGeneratorPerChannelControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerInterpreterPerChannelControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerGeneratorPerChannelInterruptRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerInterpreterPerChannelAlarmStatusRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerInterpreterPerChannelInterruptRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerInterpreterPerChannelAlarmStatusRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerGeneratorPerChannelInterruptRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerGeneratorNdfIntrMask);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerInterpreterPerChannelInterruptRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, GlbspiBase);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, ChannelCanBeTerminated);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, ChannelHasPointerProcessor);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, RxSxcControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, TxSxcControlRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, ShowConcateDebugInformation);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, StartSts1ToScanConcate);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, NumStsToReleaseConcate);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, HoPathPhyModuleIsDividedToOc24);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerInterpreterPerChannelAdjustCountRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnVtTuPointerGeneratorPerChannelAdjustCountRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, HwStsDefaultOffset);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerInterpreterPerChannelAdjustCountRegAddr);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, OcnStsPointerGeneratorPerChannelAdjustCountRegAddr);
        }

    mMethodsSet(module, &m_Tha60210011ModuleOcnOverride);
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleOcnMethods = mMethodsGet(ocnModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, m_ThaModuleOcnMethods, sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, DefaultSet);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthIsSupported);
        mMethodOverride(m_ThaModuleOcnOverride, UpenDccdecBase);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthInit);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthBaseAddress);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthHoldRegistersGet);
        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthLongRegisterAccessCreate);
        mMethodOverride(m_ThaModuleOcnOverride, IsSohRegister);
        mMethodOverride(m_ThaModuleOcnOverride, StsIdSw2Hw);
        mMethodOverride(m_ThaModuleOcnOverride, ChannelStsIdSw2HwGet);
        mMethodOverride(m_ThaModuleOcnOverride, TohGlobalK1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_ThaModuleOcnOverride, TohGlobalK2StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_ThaModuleOcnOverride, TohGlobalS1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_ThaModuleOcnOverride, PiStsGroupIdSet);
        mMethodOverride(m_ThaModuleOcnOverride, RxStsPayloadControl);
        mMethodOverride(m_ThaModuleOcnOverride, TxStsMultiplexingControl);
        mMethodOverride(m_ThaModuleOcnOverride, CanMovePathAisRxForcingPoint);
        mMethodOverride(m_ThaModuleOcnOverride, LineTxRsDccInsByOhBusEnable);
        mMethodOverride(m_ThaModuleOcnOverride, LineTxRsDccInsByOhBusEnabled);
        mMethodOverride(m_ThaModuleOcnOverride, LineTxMsDccInsByOhBusEnable);
        mMethodOverride(m_ThaModuleOcnOverride, LineTxMsDccInsByOhBusEnabled);
        mMethodOverride(m_ThaModuleOcnOverride, LineDefaultOffset);
        mMethodOverride(m_ThaModuleOcnOverride, LineTxDefaultOffset);
        mMethodOverride(m_ThaModuleOcnOverride, TohThresholdDefaultSet);
        }

    mMethodsSet(ocnModule, &m_ThaModuleOcnOverride);
    }

static void MethodsInit(Tha60290021ModuleOcn self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, NumFaceplateSlices);
        mMethodOverride(m_methods, MateXcStartLineId);
        mMethodOverride(m_methods, FaceplateXcStartLineId);
        mMethodOverride(m_methods, TerminatedXcStartLineId);
        mMethodOverride(m_methods, HasOverheadCpuControl);
        mMethodOverride(m_methods, HasRoleStatus);
        mMethodOverride(m_methods, NumXcLines);
        mMethodOverride(m_methods, HasDiagnosticIp);
        mMethodOverride(m_methods, FaceplateLineRxZnRegValue2Position);
        mMethodOverride(m_methods, FaceplateLineRxZnPosition2RegValue);
        mMethodOverride(m_methods, Sts192cIsSupported);
        mMethodOverride(m_methods, FaceplateGroupOffset);
        mMethodOverride(m_methods, FaceplateLineDefaultOffset);
        mMethodOverride(m_methods, FaceplateLineTxDefaultOffset);
        mMethodOverride(m_methods, FaceplateStsMappingDefaultSet);
        mMethodOverride(m_methods, TohMonitoringChannelControlRegAddr);
        mMethodOverride(m_methods, TxFramerPerChannelControlRegAddr);
        mMethodOverride(m_methods, FaceplateLineRxOverheadByteRegAddr);
        mMethodOverride(m_methods, FaceplateStsIdSw2Hw);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModuleOcn(self);
    OverrideTha60210011ModuleOcn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModuleOcn);
    }

AtModule Tha60290021ModuleOcnObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    /* Private data */
    mThis(self)->pohSide = cTha6029LineSideUnknown;

    return self;
    }

AtModule Tha60290021ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha60290021ModuleOcnObjectInit(newModule, device);
    }

eAtRet Tha60290021ModuleOcnFaceplateLineRateSet(ThaModuleOcn self, uint32 lineId, eAtSdhLineRate rate)
    {
    if (self)
        return FaceplateLineRateSet(self, lineId, rate);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290021ModuleOcnFaceplateOc192Enable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateOc192Enable(self, lineId, enable);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateLineEnable(self, lineId, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60290021ModuleOcnFaceplateLineIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineIsEnabled(self, lineId);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineRxEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateLineRxEnable(self, lineId, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60290021ModuleOcnFaceplateLineRxIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineRxIsEnabled(self, lineId);
    return cAtFalse;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineTxEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateLineTxEnable(self, lineId, enable);
    return cAtErrorNullPointer;
    }

eBool Tha60290021ModuleOcnFaceplateLineTxIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineTxIsEnabled(self, lineId);
    return cAtFalse;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineScrambleEnable(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateLineScrambleEnable(self, lineId, enable);
    return cAtErrorObjectNotExist;
    }

eBool Tha60290021ModuleOcnFaceplateLineScrambleIsEnabled(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineScrambleIsEnabled(self, lineId);
    return cAtFalse;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineTxB1Force(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateLineTxB1Force(self, lineId, enable);

    return cAtErrorNullPointer;
    }

eBool Tha60290021ModuleOcnFaceplateLineTxB1IsForced(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineTxB1IsForced(self, lineId);
    return cAtFalse;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineTxOofForce(ThaModuleOcn self, uint32 lineId, eBool enable)
    {
    if (self)
        return FaceplateLineTxOofForce(self, lineId, enable);

    return cAtErrorNullPointer;
    }

eBool Tha60290021ModuleOcnFaceplateLineTxOofIsForced(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineTxOofIsForced(self, lineId);
    return cAtFalse;
    }

eAtModuleSdhRet Tha60290021ModuleOcnFaceplateLineTxOverheadByteSet(ThaModuleOcn self, uint32 lineId, uint32 overheadByte, uint8 value)
    {
    if (self)
        return FaceplateLineTxOverheadByteSet(self, lineId, overheadByte, value);
    return cAtErrorNullPointer;
    }

uint8 Tha60290021ModuleOcnFaceplateLineTxOverheadByteGet(ThaModuleOcn self, uint32 lineId, uint32 overheadByte)
    {
    if (self)
        return FaceplateLineTxOverheadByteGet(self, lineId, overheadByte);
    return cAtErrorNullPointer;
    }

uint8 Tha60290021ModuleOcnFaceplateLineRxOverheadByteGet(ThaModuleOcn self, uint32 lineId, uint32 overheadByte)
    {
    if (self)
        return FaceplateLineRxOverheadByteGet(self, lineId, overheadByte);
    return cAtErrorNullPointer;
    }

eBool Tha60290021ModuleOcnFacelateLineTxOverheadByteIsSupported(ThaModuleOcn self, uint32 faceplateLineId, uint32 overheadByte)
    {
    if (self)
        return FacelateLineTxOverheadByteIsSupported(self, faceplateLineId, overheadByte);
    return cAtFalse;
    }

eBool Tha60290021ModuleOcnFacelateLineRxOverheadByteIsSupported(ThaModuleOcn self, uint32 faceplateLineId, uint32 overheadByte)
    {
    if (self)
        return FacelateLineRxOverheadByteIsSupported(self, faceplateLineId, overheadByte);
    return cAtFalse;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineRxZ1PositionSet(ThaModuleOcn self, uint32 lineId, uint8 sts1)
    {
    if (self)
        return FaceplateLineRxZ1PositionSet(self, lineId, sts1);
    return cAtErrorNullPointer;
    }

uint8 Tha60290021ModuleOcnFaceplateLineRxZ1PositionGet(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineRxZ1PositionGet(self, lineId);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineRxZ2PositionSet(ThaModuleOcn self, uint32 lineId, uint8 sts1)
    {
    if (self)
        return FaceplateLineRxZ2PositionSet(self, lineId, sts1);
    return cAtErrorNullPointer;
    }

uint8 Tha60290021ModuleOcnFaceplateLineRxZ2PositionGet(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineRxZ2PositionGet(self, lineId);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290021ModuleOcnPohProcessorSideSet(ThaModuleOcn self, eTha6029LineSide side)
    {
    if (self)
        return PohProcessorSideSet(self, side);
    return cAtErrorNullPointer;
    }

eTha6029LineSide Tha60290021ModuleOcnPohProcessorSideGet(ThaModuleOcn self)
    {
    if (self)
        return PohProcessorSideGet(self);
    return cTha6029LineSideUnknown;
    }

uint32 Tha60290021ModuleOcnFaceplateLineLocalId(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateLineLocalId(self, lineId);
    return cInvalidUint32;
    }

uint32 Tha60290021ModuleOcnFaceplateEnableMask(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateEnableMask(self, lineId);
    return 0;
    }

eAtRet Tha60290021ModuleOcnLineRateExtraSet(ThaModuleOcn self, uint32 lineId, eAtSdhLineRate rate)
    {
    if (self)
        return LineRateExtraSet(self, lineId, rate);
    return cAtErrorNullPointer;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineAisFordwardingEnable(ThaModuleOcn self, uint32 lineId, uint32 alarmType, eBool enable)
    {
    if (self)
        return FaceplateAisFordwardingEnable(self, lineId, alarmType, enable);

    return cAtErrorObjectNotExist;
    }

eBool Tha60290021ModuleOcnFaceplateLineAisFordwardingIsEnabled(ThaModuleOcn self, uint32 lineId, uint32 alarmType)
    {
    if (self)
        return FaceplateAisFordwardingIsEnabled(self, lineId, alarmType);
    return cAtFalse;
    }

eAtRet Tha60290021ModuleOcnFaceplateLineRdiEnable(ThaModuleOcn self, uint32 lineId, uint32 alarmType, eBool enable)
    {
    if (self)
        return FaceplateAutoRdiEnable(self, lineId, alarmType, enable);

    return cAtErrorObjectNotExist;
    }

eBool Tha60290021ModuleOcnFaceplateLineRdiIsEnabled(ThaModuleOcn self, uint32 lineId, uint32 alarmType)
    {
    if (self)
        return FaceplateAutoIsRdiEnabled(self, lineId, alarmType);
    return cAtFalse;
    }

void Tha60290021ModuleOcnHwFlush(AtModule self)
    {
    if (self)
        HwFlush(self);
    }

uint32 Tha60290021ModuleOcnFaceplateLineGroupOffset(ThaModuleOcn self, uint32 faceplateLineId)
    {
    if (self)
        return FaceplateLineGroupOffset(self, faceplateLineId);
    return cInvalidUint32;
    }

uint32 Tha60290021ModuleOcnNumFaceplateLineGroups(ThaModuleOcn self)
    {
    if (self)
        return NumFaceplateLineGroups(self);
    return 0;
    }

uint32 Tha60290021ModuleOcnNumFaceplateLinesPerGroup(ThaModuleOcn self)
    {
    if (self)
        return NumFaceplateLinesPerGroup(self);
    return 0;
    }

uint32 Tha60290021ModuleOcnFaceplateGroupOffset(ThaModuleOcn self, uint32 groupId)
    {
    if (self)
        return mMethodsGet(mThis(self))->FaceplateGroupOffset(mThis(self), groupId);
    return cInvalidUint32;
    }

uint32 Tha60290021ModuleOcnNumFaceplateSlices(Tha60290021ModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->NumFaceplateSlices(self);
    return 0;
    }

uint8 Tha60290021ModuleOcnFaceplateSliceLocalId(ThaModuleOcn self, uint8 sliceId)
    {
    if (self)
        return FaceplateSliceLocalId(self, sliceId);
    return sliceId;
    }

uint32 Tha60290021ModuleOcnFaceplateChannelGroupOffset(ThaModuleOcn self, AtSdhChannel channel)
    {
    if (self)
        return FaceplateChannelGroupOffset(self, channel);
    return 0;
    }

uint32 Tha60290021ModuleOcnFaceplateLineGroup(ThaModuleOcn self, uint32 faceplateLineId)
    {
    if (self)
        return FaceplateLineGroup(self, faceplateLineId);
    return cInvalidUint32;
    }

uint32 Tha60290021ModuleOcnFaceplateLineAddressWithLocalAddress(ThaModuleOcn self, uint32 faceplateLineId, uint32 localAddress)
    {
    if (self)
        return FaceplateLineAddressWithLocalAddress(self, faceplateLineId, localAddress);
    return cInvalidUint32;
    }

uint32 Tha60290021ModuleOcnFaceplateGlobalRxFramerControl(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateGlbrfmAddress(self, lineId);
    return cInvalidUint32;
    }

uint32 Tha60290021ModuleOcnFaceplateGlobalTxFramerControl(ThaModuleOcn self, uint32 lineId)
    {
    if (self)
        return FaceplateGlbtfmAddress(self, lineId);
    return cInvalidUint32;
    }

eBool Tha60290021ModuleOcnSts192cIsSupported(Tha60290021ModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->Sts192cIsSupported(self);
    return cAtFalse;
    }

eBool Tha60290021ModuleOcnHasDiagnosticIp(Tha60290021ModuleOcn self)
    {
    if (self)
        return mMethodsGet(self)->HasDiagnosticIp(self);
    return cAtFalse;
    }

const char **Tha60290021ModuleOcnAllInternalRamsDescription(AtModule self, uint32 *numRams)
    {
    if (self)
        return AllInternalRamsDescription(self, numRams);
    return NULL;
    }

eAtRet Tha60290021ModuleOcnFacePlateKbyteOcnToKbyteDccProcessorTrigger(ThaModuleOcn self, uint32 facePlateLineId)
    {
    return KbyteOcnToKbyteDccProcessorTrigger(self, facePlateLineId);
    }

uint32 Tha60290021ModuleOcnTohMonitoringChannelControlRegAddr(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(mThis(self))->TohMonitoringChannelControlRegAddr(mThis(self));
    return cInvalidUint32;
    }

uint32 Tha60290021ModuleOcnTxFramerPerChannelControlRegAddr(ThaModuleOcn self)
    {
    if (self)
        return mMethodsGet(mThis(self))->TxFramerPerChannelControlRegAddr(mThis(self));
    return cInvalidUint32;
    }

uint32 Tha60290021ModuleOcnFaceplateLineTxDefaultOffset(ThaModuleOcn self, uint32 faceplateLineId)
    {
    if (self)
        return mMethodsGet(mThis(self))->FaceplateLineTxDefaultOffset(mThis(self), faceplateLineId);
    return cInvalidUint32;
    }

eAtRet Tha60290021ModuleOcnFaceplateStsAssign(ThaModuleOcn self, uint32 faceplateLineId, uint32 sts48Id, uint32 sts12Id, uint32 sts3Id)
    {
    if (self)
        return FaceplateStsAssign(self, faceplateLineId, sts48Id, sts12Id, sts3Id);
    return cAtErrorNullPointer;
    }

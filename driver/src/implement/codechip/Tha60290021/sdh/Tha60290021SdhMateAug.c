/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290021SdhMateAug.c
 *
 * Created Date: Oct 15, 2016
 *
 * Description : MATE AU
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60290021ModuleSdh.h"
#include "Tha60290021SdhAugInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSdhModule(self) SdhModule((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021SdhMateAug
    {
    tTha60290021SdhLineSideAug super;
    }tTha60290021SdhMateAug;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021SdhLineSideAugMethods m_Tha60290021SdhLineSideAugOverride;

/* Save super implementation */
static const tTha60290021SdhLineSideAugMethods *m_Tha60290021SdhLineSideAugMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh SdhModule(AtChannel self)
    {
    return (AtModuleSdh)AtChannelModuleGet(self);
    }

static AtSdhLine XcHidingLineSideSourcePairLine(Tha60290021SdhLineSideAug self, uint8 pairLocalId)
    {
    return Tha60290021ModuleSdhFaceplateLineGet(mSdhModule(self), pairLocalId);
    }

static AtSdhLine XcHidingLineSideDestPairLine(Tha60290021SdhLineSideAug self, uint8 pairLocalId)
    {
    return Tha60290021ModuleSdhFaceplateLineGet(mSdhModule(self), pairLocalId);
    }

static eBool XcHidingShouldApply(Tha60290021SdhLineSideAug self, eTha60290021XcHideMode hideMode)
    {
    if (hideMode == cTha60290021XcHideModeMate)
        return cAtTrue;

    return m_Tha60290021SdhLineSideAugMethods->XcHidingShouldApply(self, hideMode);
    }

static uint8 XcHidingLineSideSourcePairLocalLineId(Tha60290021SdhLineSideAug self)
    {
    /* Faceplate line 0, 4, 8, .. will be used for source connections connected
     * to MATE */
    AtSdhLine line = AtSdhChannelLineObjectGet((AtSdhChannel)self);
    uint8 localId = ThaModuleSdhLineLocalId(line);
    return (uint8)(localId * 4);
    }

static uint8 XcHidingLineSideDestPairLocalLineId(Tha60290021SdhLineSideAug self)
    {
    /* Faceplate line 2, 6, 10, .. will be used for source connections connected
     * to MATE */
    AtSdhLine line = AtSdhChannelLineObjectGet((AtSdhChannel)self);
    uint8 localId = ThaModuleSdhLineLocalId(line);
    return (uint8)((localId * 4) + 2);
    }

static void OverrideTha60290021SdhLineSideAug(AtSdhAug self)
    {
    Tha60290021SdhLineSideAug aug = (Tha60290021SdhLineSideAug)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021SdhLineSideAugMethods = mMethodsGet(aug);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SdhLineSideAugOverride, mMethodsGet(aug), sizeof(m_Tha60290021SdhLineSideAugOverride));

        mMethodOverride(m_Tha60290021SdhLineSideAugOverride, XcHidingLineSideSourcePairLine);
        mMethodOverride(m_Tha60290021SdhLineSideAugOverride, XcHidingLineSideSourcePairLocalLineId);
        mMethodOverride(m_Tha60290021SdhLineSideAugOverride, XcHidingLineSideDestPairLine);
        mMethodOverride(m_Tha60290021SdhLineSideAugOverride, XcHidingLineSideDestPairLocalLineId);
        mMethodOverride(m_Tha60290021SdhLineSideAugOverride, XcHidingShouldApply);
        }

    mMethodsSet(aug, &m_Tha60290021SdhLineSideAugOverride);
    }

static void Override(AtSdhAug self)
    {
    OverrideTha60290021SdhLineSideAug(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SdhMateAug);
    }

static AtSdhAug ObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SdhLineSideAugObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAug Tha60290021SdhMateAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAug newAug = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newAug == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newAug, channelId, channelType, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhMateLineAu.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : PWCodechip-6029 AU Mate line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../man/Tha60290021Device.h"
#include "../ocn/Tha60290021ModuleOcnReg.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "Tha6029SdhAuInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhPathMethods    m_AtSdhPathOverride;
static tTha6029SdhAuMethods m_Tha6029SdhAuOverride;

/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tTha6029SdhAuMethods *m_Tha6029SdhAuMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    if ((eThaPhyModule)moduleId == cThaModuleOcn)
        return Tha60290021IsOcnTfi5SideRegister(AtChannelDeviceGet(self), address);

    return cAtFalse;
    }

static eBool ShouldGetPointerAlarmFromOcnModule(Tha6029SdhAu self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);

    if (Tha60290021DeviceIsRunningOnOtherPlatform(device))
        return cAtTrue;

    /* If POH is there, path alarms are redirected to POH */
    if (Tha60290021ModuleOcnPohProcessorSideGet(ocnModule) == cTha6029LineSideMate)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 Stschstkram_Base(Tha6029SdhAu self)
    {
    AtUnused(self);
    return cAf6Reg_tfi5upstschstkram_Base;
    }

static uint32 Stschstaram_Base(Tha6029SdhAu self)
    {
    AtUnused(self);
    return cAf6Reg_tfi5upstschstaram_Base;
    }

static eBool HasPohProcessor(AtSdhPath self)
    {
    AtSdhPath vc = (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    return AtSdhPathHasPohProcessor(vc);
    }

static eAtRet SetSsDefaultValueToFalse(Tha6029SdhAu self)
    {
    AtSdhPath path = (AtSdhPath)self;
    AtSdhPathExpectedSsSet(path, 0);
    AtSdhPathSsCompareEnable(path, cAtFalse);
    AtSdhPathTxSsSet(path, 0);
    AtSdhPathSsInsertionEnable(path, cAtFalse);
    return cAtOk;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;



    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    /* MATE internal communication does not require SS bit functionality */
    ret |= mMethodsGet((Tha6029SdhAu)self)->SetSsDefaultValueToFalse((Tha6029SdhAu)self);

    return ret;
    }

static eBool SsBitCompareIsEnabledAsDefault(AtSdhPath self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideAtChannel(AtSdhAu self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhPath(AtSdhAu self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, HasPohProcessor);
        mMethodOverride(m_AtSdhPathOverride, SsBitCompareIsEnabledAsDefault);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideTha6029SdhAu(AtSdhAu self)
    {
    Tha6029SdhAu au = (Tha6029SdhAu)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6029SdhAuMethods = mMethodsGet(au);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SdhAuOverride, mMethodsGet(au), sizeof(m_Tha6029SdhAuOverride));

        mMethodOverride(m_Tha6029SdhAuOverride, ShouldGetPointerAlarmFromOcnModule);
        mMethodOverride(m_Tha6029SdhAuOverride, Stschstkram_Base);
        mMethodOverride(m_Tha6029SdhAuOverride, Stschstaram_Base);
        mMethodOverride(m_Tha6029SdhAuOverride, SetSsDefaultValueToFalse);
        }

    mMethodsSet(au, &m_Tha6029SdhAuOverride);
    }

static void Override(AtSdhAu self)
    {
    OverrideAtChannel(self);
	OverrideAtSdhPath(self);
    OverrideTha6029SdhAu(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhMateLineAu);
    }

AtSdhAu Tha6029SdhMateLineAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhAuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAu Tha6029SdhMateLineAuNew(uint32 auId, uint8 auType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAu self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha6029SdhMateLineAuObjectInit(self, auId, auType, module);
    }

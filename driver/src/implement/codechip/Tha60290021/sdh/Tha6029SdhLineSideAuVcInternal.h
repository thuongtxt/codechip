/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6029SdhLineSideAuVcInternal.h
 * 
 * Created Date: Oct 8, 2016
 *
 * Description : Lide side (MATE, Faceplate) AU's VC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SDHLINESIDEAUVCINTERNAL_H_
#define _THA6029SDHLINESIDEAUVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../xc/Tha60290021ModuleXc.h"
#include "Tha6029SdhAuVcInternal.h"
#include "Tha6029SdhLineSideAuVc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SdhLineSideAuVcMethods
    {
    eAtRet (*CanBindToPseudowireWithXcHideMode)(Tha6029SdhLineSideAuVc self, AtPw pseudowire, eTha60290021XcHideMode mode);
    eAtRet (*ConfigLookUpWithXcHideMode)(Tha6029SdhLineSideAuVc self);
    }tTha6029SdhLineSideAuVcMethods;

typedef struct tTha6029SdhLineSideAuVc
    {
    tTha6029SdhAuVc super;
    const tTha6029SdhLineSideAuVcMethods *methods;
    }tTha6029SdhLineSideAuVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha6029SdhLineSideAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SDHLINESIDEAUVCINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhTerminatedLineAuVc.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : PWCodechip-6029 AU VC Terminated line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/xc/AtCrossConnectInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "../man/Tha60290021Device.h"
#include "../ocn/Tha60290021ModuleOcnReg.h"
#include "../sdh/Tha60290021ModuleSdh.h"
#include "../prbs/Tha60290021ModulePrbs.h"
#include "Tha6029SdhTerminatedLineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_glbtxholosel2_reg_Old_Base 0xf0014

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self)         ((Tha6029SdhTerminatedLineAuVc)self)

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods         m_AtObjectOverride;
static tAtChannelMethods      	m_AtChannelOverride;
static tAtSdhChannelMethods 	m_AtSdhChannelOverride;
static tAtSdhPathMethods      	m_AtSdhPathOverride;
static tThaSdhVcMethods       	m_ThaSdhVcOverride;
static tThaSdhAuVcMethods       m_ThaSdhAuVcOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods     = NULL;
static const tAtChannelMethods 		*m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods 	*m_AtSdhChannelMethods = NULL;
static const tAtSdhPathMethods 		*m_AtSdhPathMethods    = NULL;
static const tThaSdhVcMethods       *m_ThaSdhVcMethods     = NULL;
static const tThaSdhAuVcMethods     *m_ThaSdhAuVcMethods   = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtDevice Device(AtSdhChannel self)
    {
	return AtChannelDeviceGet((AtChannel) self);
    }

static uint32 HoLoSelectionMask(uint8 hwSts)
    {
    return  cBit0 << (hwSts % 32);
    }

static uint32 HoLoSelectionShift(uint8 hwSts)
    {
    return hwSts % 32;
    }

static uint32 HoLoSelection2Base(AtSdhChannel self)
    {
	AtModuleSdh SdhModule = (AtModuleSdh) AtChannelModuleGet((AtChannel) self);
	return Tha60290021ModuleSdhUseNewStsIdConvert(SdhModule) ? cAf6Reg_glbtxholosel2_reg_Base : cAf6Reg_glbtxholosel2_reg_Old_Base;
    }

static uint32 GlbtxholoselRegister(AtSdhChannel self, uint8 sts1Id)
    {
    ThaModuleOcn ocnModule;
    uint32 baseAddress;
    uint32 localAddr;
    uint8 hwSlice, hwSts;
    eAtRet ret;

    ret = ThaSdhChannelHwStsGet(self, cThaModuleOcn, sts1Id, &hwSlice, &hwSts);
    if (ret != cAtOk)
        return cInvalidUint32;

    ocnModule = (ThaModuleOcn) AtDeviceModuleGet(Device(self), cThaModuleOcn);
    baseAddress = ThaModuleOcnBaseAddress(ocnModule);
    localAddr = (hwSts < 32) ? cAf6Reg_glbtxholosel1_reg_Base : HoLoSelection2Base(self);
    return localAddr + baseAddress + hwSlice;
    }

static eBool VcFromLoBus(AtSdhChannel self)
    {
    uint32 vcType = AtSdhChannelTypeGet(self);
    uint16 mapType = AtSdhChannelMapTypeGet(self);

    if (Tha60290021ModulePrbsShouldRedirectToHoBus(self))
        return cAtFalse;

    switch (vcType)
        {
        case cAtSdhChannelTypeVc3:
            if ((mapType == cAtSdhVcMapTypeVc3Map7xTug2s) ||
                (mapType == cAtSdhVcMapTypeVc3MapDe3))
                return cAtTrue;
            break;

        case cAtSdhChannelTypeVc4:
            if (mapType == cAtSdhVcMapTypeVc4Map3xTug3s)
                return cAtTrue;
            break;

        default:
            break;
        }

    return cAtFalse;
    }

static ThaModuleOcn OcnModule(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel) self);
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static eAtRet Sts1HoSelect(AtSdhChannel self, uint8 swSts1, eBool hoSelected)
    {
    eAtRet ret;
    uint8 hwSlice, hwSts;
    uint32 regVal;
    uint32 regAddr = GlbtxholoselRegister(self, swSts1);

    ret = ThaSdhChannelHwStsGet(self, cThaModuleOcn, swSts1, &hwSlice, &hwSts);
    if ((ret != cAtOk) || (regAddr == cInvalidUint32))
    	{
		AtDriverLogWithFileLine(AtDriverSharedDriverGet(), cAtLogLevelCritical, AtSourceLocation, "Can not convert to hardware Id \r\n");
    	return ret;
    	}

    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mFieldIns(&regVal, HoLoSelectionMask(hwSts), HoLoSelectionShift(hwSts), hoSelected ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    if (hoSelected)
        ret |= Tha60210011ModuleOcnLoLineHwSts1PayloadInit(OcnModule(self), self, hwSlice, hwSts);

    return cAtOk;
    }

static eAtRet HoSelect(AtSdhChannel self, eBool selectFromHo)
    {
    uint32 sts_i;
    eAtRet ret = cAtOk;
	uint32 numSts = AtSdhChannelNumSts(self);
	uint8 swSts1 = AtSdhChannelSts1Get(self);

    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 sts1 = (uint8)(swSts1 + sts_i);
        ret |= Sts1HoSelect(self, sts1, selectFromHo);
        }

    return ret;
    }


static eAtRet MapTypeUpdate(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret = 0;
    eBool selectFromHo = Tha6029SdhTerminatedLineAuVcFromLoBus((AtSdhChannel)self) ? cAtFalse : cAtTrue;

    AtUnused(mapType);

    ret |= Tha6029SdhTerminatedLineAuVcHoSelect(self, selectFromHo);
    ret |= AtSdhPathPohInsertionEnable((AtSdhPath)self, cAtTrue);
    ret |= ThaSdhAuVcLomMonitorEnable((ThaSdhAuVc)self, AtSdhVcTu1xChannelized((AtSdhVc)self));

    return ret;
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret;

    if (mapType == AtSdhChannelMapTypeGet(self))
    	return cAtOk;

    /* Let the super do first because mapping changing may relate with resource
     * management */
    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    ret = MapTypeUpdate(self, mapType);
    if (ret != cAtOk)
        return ret;

    return ret;
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return (enable == cAtTrue) ? cAtOk : cAtErrorNotApplicable;
    }

static eBool IsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return  enable;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return (enable == cAtTrue) ? cAtOk : cAtErrorNotApplicable;
    }

static eBool TxIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    AtUnused(self);
    return (enable == cAtTrue) ? cAtOk : cAtErrorNotApplicable;
    }

static eBool RxIsEnabled(AtChannel self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool RxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    return enabled;
    }

static eBool TxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    return enabled;
    }

static ThaPohProcessor PohProcessorCreate(ThaSdhVc self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool HasPointerProcessor(AtSdhPath self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    if ((eThaPhyModule)moduleId == cThaModuleOcn)
        return Tha60290021IsOcnTerminatedSideRegister(AtChannelDeviceGet(self), address);

    return m_AtChannelMethods->CanAccessRegister(self, moduleId, address);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
    AtUnused(self);
    return cAtSdhPathAlarmClockStateChange;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    ThaCdrController cdrController;

    if (!AtChannelInterruptMaskCanSet(self, defectMask, enableMask))
        return cAtErrorModeNotSupport;

    cdrController = ThaSdhVcCdrControllerGet((AtSdhVc)self);
    if (cdrController && (defectMask & cAtSdhPathAlarmClockStateChange))
        return ThaCdrControllerInterruptMaskSet(cdrController, defectMask, enableMask);

    return cAtOk;
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    ThaCdrController cdrController = ThaSdhVcCdrControllerGet((AtSdhVc)self);
    return cdrController ? ThaCdrControllerInterruptMaskGet(cdrController) : 0;
    }

static eAtRet PohInsertionEnable(AtSdhPath self, eBool enabled)
    {
    eAtRet ret = cAtOk;
    AtList dests;
    AtIterator iterator;
    AtChannel dest;

    dests = ThaSdhVcDestVcs((ThaSdhVc)self);
    iterator = AtListIteratorCreate(dests);
    while ((dest = (AtChannel)AtIteratorNext(iterator)) != NULL)
        {
        if (Tha60290021ModuleSdhIsLineSideVc((AtSdhChannel)dest))
            ret |= AtSdhPathPohInsertionEnable((AtSdhPath)dest, enabled);
        }
    AtObjectDelete((AtObject)iterator);

    return ret;
    }

static eBool PrbsIsActive(ThaSdhVc self)
    {
    AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel)self);
    if (engine == NULL)
        return cAtFalse;
    return AtPrbsEngineIsEnabled(engine);
    }

static eAtRet AlarmAffectingHandle(AtSdhChannel self, eAtRet (*AlarmAffectingFunc)(AtSdhChannel))
    {
    eAtRet ret = cAtOk;
    uint32 numSourceVcs = ThaSdhVcNumSourceVcs((ThaSdhVc)self);
    uint32 sourceIndex;

    for (sourceIndex = 0; sourceIndex < numSourceVcs; sourceIndex++)
        {
        ThaSdhVc sourceVc = ThaSdhVcSourceVcGet((ThaSdhVc)self, sourceIndex);
        if (sourceVc == NULL)
            continue;

        if (AtSdhPathHasPohProcessor((AtSdhPath)sourceVc))
            ret |= AlarmAffectingFunc((AtSdhChannel)sourceVc);
        }

    return ret;
    }

static eAtRet PwCepSet(ThaSdhVc self)
    {
    eAtRet ret = m_ThaSdhVcMethods->PwCepSet(self);
    if (ret != cAtOk)
        return ret;

    /* POH insertion/downstream must be handled properly */
    ret |= AtSdhPathPohInsertionEnable((AtSdhPath)self, PrbsIsActive(self) ? cAtTrue : cAtFalse);
    ret |= AlarmAffectingHandle((AtSdhChannel)self, AtSdhChannelAlarmAffectingDisable);

    return ret;
    }

static eAtRet MappingRestore(ThaSdhVc self)
    {
    eAtRet ret = m_ThaSdhVcMethods->MappingRestore(self);
    if (ret != cAtOk)
        return ret;

    /* POH insertion/downstream must be handled properly */
    ret |= AtSdhPathPohInsertionEnable((AtSdhPath)self, cAtTrue);
    ret |= AlarmAffectingHandle((AtSdhChannel)self, AtSdhChannelAlarmAffectingRestore);

    return ret;
    }

static eBool NeedConfigurePdh(ThaSdhAuVc self)
    {
    Tha60210011Device device = (Tha60210011Device)AtChannelDeviceGet((AtChannel)self);

    if (AtSdhChannelTypeGet((AtSdhChannel)self) == cAtSdhChannelTypeVc4_64c)
        return cAtFalse;

    if (Tha60210011DeviceHasHoBus(device))
        return m_ThaSdhAuVcMethods->NeedConfigurePdh(self);

    return cAtTrue;
    }

static eAtRet LomMonitorEnable(ThaSdhAuVc self, eBool enable)
    {
    eAtRet ret = cAtOk;
    uint32 numSourceVcs = ThaSdhVcNumSourceVcs((ThaSdhVc)self);
    uint32 sourceIndex;

    for (sourceIndex = 0; sourceIndex < numSourceVcs; sourceIndex++)
        {
        ThaSdhVc sourceVc = ThaSdhVcSourceVcGet((ThaSdhVc)self, sourceIndex);
        if ((sourceVc != NULL) && Tha60290021ModuleSdhIsLineSideVc((AtSdhChannel)sourceVc))
            ret |= ThaSdhAuVcLomMonitorEnable((ThaSdhAuVc)sourceVc, enable);
        }

    return ret;
    }

static eAtRet TxSquelch(AtChannel self, eBool squelched)
    {
    eAtRet ret = cAtOk;
    AtList dests;
    AtIterator iterator;
    AtChannel dest;

    mThis(self)->needSquelch = squelched;

    dests = ThaSdhVcDestVcs((ThaSdhVc)self);
    iterator = AtListIteratorCreate(dests);
    while ((dest = (AtChannel)AtIteratorNext(iterator)) != NULL)
        if (Tha60290021ModuleSdhIsLineSideVc((AtSdhChannel)dest))
            ret |= AtChannelTxSquelch(dest, squelched);

    AtObjectDelete((AtObject)iterator);

    return ret;
    }

static eAtRet TxSquelchingUpdate(AtSdhChannel self)
    {
    return AtChannelTxSquelch((AtChannel)self, mThis(self)->needSquelch);
    }

static eBool XcLoopbackModeIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (loopbackMode == cAtLoopbackModeRemote)
        return cAtFalse;

    if (loopbackMode == cAtLoopbackModeLocal)
        return cAtTrue;

    return m_AtChannelMethods->XcLoopbackModeIsSupported(self, loopbackMode);
    }

static eAtRet XcLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    if ((loopbackMode == cAtLoopbackModeLocal) || (loopbackMode == cAtLoopbackModeRelease))
        return m_AtChannelMethods->XcLoopbackSet(self, loopbackMode);

    return cAtErrorModeNotSupport;
    }

static uint8 XcLoopbackGet(AtChannel self)
    {
    AtCrossConnect xc = mMethodsGet(self)->XcEngine(self);

    if (AtCrossConnectHwIsConnected(xc, self, self))
        return cAtLoopbackModeLocal;

    return cAtLoopbackModeRelease;
    }

static eAtSdhChannelMode DefaultMode(AtSdhChannel self)
    {
    if (Tha60290021SdhChannelHidingXcIsActive(self))
        return Tha60290021SdhTerminatedChannelHidingXcDefaultMode(self);

    return m_AtSdhChannelMethods->DefaultMode(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha6029SdhTerminatedLineAuVc *object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeUInt(needSquelch);
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaSdhAuVc(AtSdhVc self)
    {
    ThaSdhAuVc sdhAuVc = (ThaSdhAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhAuVcMethods = mMethodsGet(sdhAuVc);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhAuVcOverride, m_ThaSdhAuVcMethods, sizeof(m_ThaSdhAuVcOverride));

        mMethodOverride(m_ThaSdhAuVcOverride, NeedConfigurePdh);
        mMethodOverride(m_ThaSdhAuVcOverride, LomMonitorEnable);
        }

    mMethodsSet(sdhAuVc, &m_ThaSdhAuVcOverride);
    }

static void OverrideAtSdhPath(AtSdhVc self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhPathMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, m_AtSdhPathMethods, sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, HasPointerProcessor);
        mMethodOverride(m_AtSdhPathOverride, PohInsertionEnable);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc thaVc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhVcMethods = mMethodsGet(thaVc);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, m_ThaSdhVcMethods, sizeof(m_ThaSdhVcOverride));

        mMethodOverride(m_ThaSdhVcOverride, PohProcessorCreate);
        mMethodOverride(m_ThaSdhVcOverride, PwCepSet);
        mMethodOverride(m_ThaSdhVcOverride, MappingRestore);
        }

    mMethodsSet(thaVc, &m_ThaSdhVcOverride);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, DefaultMode);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        mMethodOverride(m_AtChannelOverride, RxCanEnable);
        mMethodOverride(m_AtChannelOverride, TxCanEnable);
        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, TxSquelch);
        mMethodOverride(m_AtChannelOverride, XcLoopbackModeIsSupported);
        mMethodOverride(m_AtChannelOverride, XcLoopbackSet);
        mMethodOverride(m_AtChannelOverride, XcLoopbackGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    OverrideThaSdhVc(self);
    OverrideThaSdhAuVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhTerminatedLineAuVc);
    }

AtSdhVc Tha6029SdhTerminatedLineAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha6029SdhTerminatedLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha6029SdhTerminatedLineAuVcObjectInit(self, channelId, channelType, module);
    }
    
eAtRet Tha6029SdhTerminatedLineAuVcHoSelect(AtSdhChannel self, eBool selectFromHo)
    {
    if (self)
        return HoSelect(self, selectFromHo);
    return cAtErrorObjectNotExist;
    }

eBool Tha6029SdhTerminatedLineAuVcFromLoBus(AtSdhChannel self)
    {
    if (self)
        return VcFromLoBus(self);
    return cAtFalse;
    }

eAtRet Tha6029SdhTerminatedLineAuVcMapTypeUpdate(AtSdhChannel self, uint8 mapType)
    {
    if (self)
        return MapTypeUpdate(self, mapType);
    return cAtErrorObjectNotExist;
    }

eAtRet Tha6029SdhTerminatedLineAuVcTxSquelchingUpdate(AtSdhChannel self)
    {
    if (self)
        return TxSquelchingUpdate(self);
    return cAtErrorObjectNotExist;
    }

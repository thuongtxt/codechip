/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhFacePlateLineAuVc.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : PWCodechip-6029 AU VC FacePlate Line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/xc/ThaVcCrossConnect.h"
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "../man/Tha60290021Device.h"
#include "../ocn/Tha60290021ModuleOcnReg.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "Tha6029SdhFacePlateLineAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods               m_AtChannelOverride;
static tThaSdhVcMethods                m_ThaSdhVcOverride;
static tTha6029SdhLineSideAuVcMethods  m_Tha6029SdhLineSideAuVcOverride;

/* Super implementation */
static const tAtChannelMethods              *m_AtChannelMethods = NULL;
static const tThaSdhVcMethods               *m_ThaSdhVcMethods  = NULL;
static const tTha6029SdhLineSideAuVcMethods *m_Tha6029SdhLineSideAuVcMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn OcnModule(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static eBool HasPohProcessor(ThaSdhVc self)
    {
    if (Tha60290021ModuleOcnPohProcessorSideGet(OcnModule(self)) == cTha6029LineSideFaceplate)
        return cAtTrue;
    return cAtFalse;
    }

static ThaPohProcessor PohProcessorCreate(ThaSdhVc self)
    {
    if (Tha60290021ModuleOcnPohProcessorSideGet(OcnModule(self)) == cTha6029LineSideFaceplate)
        return m_ThaSdhVcMethods->PohProcessorCreate(self);

    return NULL;
    }

static ThaPohProcessor PohProcessorGet(ThaSdhVc self)
    {
    if (HasPohProcessor(self))
        return m_ThaSdhVcMethods->PohProcessorGet(self);
    return NULL;
    }

static eAtRet CanBindToPseudowireWithXcHideMode(Tha6029SdhLineSideAuVc self, AtPw pseudowire, eTha60290021XcHideMode mode)
    {
    if (mode == cTha60290021XcHideModeFaceplate)
        return cAtOk;

    return m_Tha6029SdhLineSideAuVcMethods->CanBindToPseudowireWithXcHideMode(self, pseudowire, mode);
    }

static uint32 OcnPJCounterOffset(ThaSdhVc self, eBool isPosAdj)
    {
    uint8 slice, hwStsInSlice, localSlice;
    uint8 adjMode = isPosAdj ? 0 : 1;
    ThaModuleOcn ocnModule = OcnModule((ThaSdhVc)self);
    uint32 sliceBase;

    ThaSdhChannel2HwMasterStsId((AtSdhChannel)self, cThaModuleOcn, &slice, &hwStsInSlice);
    sliceBase = Tha60290021ModuleOcnFaceplateChannelGroupOffset(ocnModule, (AtSdhChannel)self);
    localSlice = Tha60290021ModuleOcnFaceplateSliceLocalId(ocnModule, slice);

    return (uint32)(512UL * localSlice + 64UL * adjMode + hwStsInSlice + sliceBase + Tha60210011ModuleOcnBaseAddress(ocnModule));
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    uint32 moduleVal = moduleId;

    AtDevice device = AtChannelDeviceGet(self);

    if (moduleVal == cThaModuleOcn)
        return Tha60290021IsOcnFaceplateSideRegister(device, address);

    if (moduleVal == cThaModulePoh)
        return Tha60210011IsPohRegister(device, address);

    if (moduleVal == cAtModuleAps)
        return cAtTrue; /* TODO: Need to define APS register ranges and check */

    return cAtFalse;
    }

static void OverrideTha6029SdhLineSideAuVc(AtSdhVc self)
    {
    Tha6029SdhLineSideAuVc vc = (Tha6029SdhLineSideAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6029SdhLineSideAuVcMethods = mMethodsGet(vc);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SdhLineSideAuVcOverride, mMethodsGet(vc), sizeof(m_Tha6029SdhLineSideAuVcOverride));

        mMethodOverride(m_Tha6029SdhLineSideAuVcOverride, CanBindToPseudowireWithXcHideMode);
        }

    mMethodsSet(vc, &m_Tha6029SdhLineSideAuVcOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc thaVc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhVcMethods = mMethodsGet(thaVc);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, m_ThaSdhVcMethods, sizeof(m_ThaSdhVcOverride));

        mMethodOverride(m_ThaSdhVcOverride, PohProcessorCreate);
        mMethodOverride(m_ThaSdhVcOverride, PohProcessorGet);
        mMethodOverride(m_ThaSdhVcOverride, OcnPJCounterOffset);
        }

    mMethodsSet(thaVc, &m_ThaSdhVcOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtChannel(self);
    OverrideThaSdhVc(self);
    OverrideTha6029SdhLineSideAuVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhFacePlateLineAuVc);
    }

AtSdhVc Tha6029SdhFacePlateLineAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhLineSideAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha6029SdhFacePlateLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha6029SdhFacePlateLineAuVcObjectInit(self, channelId, channelType, module);
    }

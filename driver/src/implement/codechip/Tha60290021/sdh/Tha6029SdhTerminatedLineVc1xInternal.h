/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6029SdhTerminatedLineVc1xInternal.h
 * 
 * Created Date: Jul 25, 2017
 *
 * Description : Terminated VC-1x representation.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SDHTERMINATEDLINEVC1XINTERNAL_H_
#define _THA6029SDHTERMINATEDLINEVC1XINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/sdh/Tha60210051Tfi5LineVcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SdhTerminatedLineVc1x
    {
    tTha60210051Tfi5LineVc1x super;
    }tTha6029SdhTerminatedLineVc1x;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha6029SdhTerminatedLineVc1xObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SDHTERMINATEDLINEVC1XINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290021ModuleSdh.h
 * 
 * Created Date: Jul 8, 2016
 *
 * Description : PWCodechip-60290021 SDH module.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULESDHINTERNAL_H_
#define _THA60290021MODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/sdh/Tha60210051ModuleSdhInternal.h"
#include "Tha60290021ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021ModuleSdhMethods
    {
    uint8 (*NumUseableFaceplateLines)(Tha60290021ModuleSdh self);
    AtSdhLine (*FaceplateLineGet)(Tha60290021ModuleSdh self, uint8 localLineId);
    uint8 (*NumUseableMateLines)(Tha60290021ModuleSdh self);
    AtSdhLine (*MateLineGet)(Tha60290021ModuleSdh self, uint8 localLineId);
    uint8 (*NumUseableTerminatedLines)(Tha60290021ModuleSdh self);
    AtSdhLine (*TerminatedLineGet)(Tha60290021ModuleSdh self, uint8 localLineId);
    AtHdlcChannel (*DccChannelObjectCreate)(Tha60290021ModuleSdh self, uint32 dccId,  AtSdhLine line, eAtSdhLineDccLayer layers);
    eBool (*UseNewStsIdConvert)(Tha60290021ModuleSdh self);
    AtSdhLine (*FaceplateLineCreate)(Tha60290021ModuleSdh self, uint8 lineId);
    AtSdhLine (*MateLineCreate)(Tha60290021ModuleSdh self, uint8 lineId);
    AtSdhLine (*TerminatedLineCreate)(Tha60290021ModuleSdh self, uint8 lineId);
    AtSdhAu (*MateLineAuCreate)(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType);
    AtSdhAu (*FacePlateLineAuCreate)(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType);
    AtSdhVc (*FacePlateLineAuVcCreate)(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType);
    AtSdhVc (*TerminatedLineAuVcCreate)(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType);
    AtSdhAug (*FacePlateLineAugCreate)(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType);

    eBool (*FacePlateLineIsUsed)(Tha60290021ModuleSdh self, uint8 lineId);
    eAtRet (*FaceplateLineRateWillChange)(Tha60290021ModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate);
    eAtRet (*FaceplateLineRateDidChange)(Tha60290021ModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate);
    AtSdhLine (*HidingXcTerminatedChannelToFaceplateLineGet)(Tha60290021ModuleSdh self, AtSdhChannel channel);

    /* Interrupt helper */
    AtSdhPath (*FaceplateAuPathFromHwIdGet)(Tha60290021ModuleSdh self, uint8 slice48, uint8 stsInSlice);
    }tTha60290021ModuleSdhMethods;

typedef struct tTha60290021ModuleSdh
    {
    tTha60210051ModuleSdh super;
    const tTha60290021ModuleSdhMethods *methods;
    uint16 rate[cThaNumFacePlateLine];
    }tTha60290021ModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha60290021ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULESDHINTERNAL_H_ */


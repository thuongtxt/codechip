/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhLine.c
 *
 * Created Date: Jul 10, 2016
 *
 * Description : PWCodechip-6029 SDH line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6029SdhLineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods       m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtSdhChannelMethods       *m_AtSdhChannelMethods       = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtModuleSdhRet CounterModeSet(AtSdhChannel self, uint16 counterType, eAtSdhChannelCounterMode mode)
    {
    AtUnused(self);
    AtUnused(counterType);
    AtUnused(mode);
    return cAtOk;
    }

static void OverrideAtSdhChannel(AtSdhLine self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, CounterModeSet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhLine);
    }

AtSdhLine Tha6029SdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5LineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }


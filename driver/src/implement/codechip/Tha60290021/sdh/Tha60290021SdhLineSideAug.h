/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290021SdhLineSideAug.h
 * 
 * Created Date: Oct 15, 2016
 *
 * Description : SDH Line side AUG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021SDHLINESIDEAUG_H_
#define _THA60290021SDHLINESIDEAUG_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtSdhChannel.h"
#include "../xc/Tha60290021ModuleXc.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021SdhLineSideAug * Tha60290021SdhLineSideAug;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60290021SdhLineSideAugXcHidingShouldApply(AtSdhChannel self, eTha60290021XcHideMode hideMode);

AtSdhChannel Tha60290021SdhLineSideAugXcHdingLineSideSourcePairAug(AtSdhChannel self);
AtSdhChannel Tha60290021SdhLineSideAugXcHdingLineSideDestPairAug(AtSdhChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021SDHLINESIDEAUG_H_ */


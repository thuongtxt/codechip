/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhFacePlateLine.c
 *
 * Created Date: Jul 10, 2016
 *
 * Description : PWCodechip-6029 SDH Faceplate line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtModuleSdhInternal.h"
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../../default/pmc/ThaModulePmc.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "../man/Tha60290021Device.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "../ocn/Tha60290021ModuleOcnReg.h"
#include "../poh/Tha60290021PohReg.h"
#include "../physical/Tha60290021SerdesManager.h"
#include "../common/Tha602900xxCommon.h"
#include "../xc/Tha60290021ModuleXc.h"
#include "Tha60290021ModuleSdh.h"
#include "Tha6029SdhFacePlateLineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha6029SdhFacePlateLine*)self)

/*--------------------------- Macros -----------------------------------------*/
#define mOcnModule(self) OcnModule((AtChannel)self)
#define mGroupOffset(self) GroupOffset((AtChannel)self)

#define mAlarmPrint(alarmMask, mask, maskName)              \
    {                                                       \
    AtPrintc(cSevNormal, "    %s: ", maskName);             \
    AtPrintc((alarmMask & mask) ? cSevCritical : cSevInfo, "%s\r\n", (alarmMask & mask) ? "Set" : "Clear"); \
    }

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods           m_AtObjectOverride;
static tAtChannelMethods          m_AtChannelOverride;
static tAtSdhChannelMethods  	  m_AtSdhChannelOverride;
static tAtSdhLineMethods          m_AtSdhLineOverride;
static tThaSdhLineMethods         m_ThaSdhLineOverride;
static tTha60210031SdhLineMethods m_Tha60210031SdhLineOverride;

/* Save super implementation */
static const tAtObjectMethods           *m_AtObjectMethods           = NULL;
static const tAtChannelMethods          *m_AtChannelMethods          = NULL;
static const tAtSdhChannelMethods       *m_AtSdhChannelMethods       = NULL;
static const tAtSdhLineMethods          *m_AtSdhLineMethods          = NULL;
static const tThaSdhLineMethods         *m_ThaSdhLineMethods         = NULL;
static const tTha60210031SdhLineMethods *m_Tha60210031SdhLineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModuleOcn OcnModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static ThaModulePoh PohModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (ThaModulePoh)AtDeviceModuleGet(device, cThaModulePoh);
    }

static eBool DiagnosticEnabled(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceDiagnosticModeIsEnabled(device);
    }

static uint32 DefectGet(AtChannel self)
    {
    if (DiagnosticEnabled(self))
        return Tha60290021ModuleOcnDiagLineAlarmGet((AtSdhLine)self);

    return m_AtChannelMethods->DefectGet(self);
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (DiagnosticEnabled(self))
        return Tha60290021ModuleOcnDiagLineAlarmHistoryRead2Clear((AtSdhLine)self, cAtFalse);

    return m_AtChannelMethods->DefectHistoryGet(self);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (DiagnosticEnabled(self))
        return Tha60290021ModuleOcnDiagLineAlarmHistoryRead2Clear((AtSdhLine)self, cAtTrue);

    return m_AtChannelMethods->DefectHistoryClear(self);
    }

static AtModule PmcModule(AtChannel self)
	{
	return AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
	}

static uint32 DiagnosticCounterGet(AtChannel self, uint16 counterType)
    {
    if (counterType == cAtSdhLineCounterTypeB1)
        return Tha60290021ModuleOcnDiagLineB1CounterRead2Clear((AtSdhLine)self, cAtFalse);

    return 0x0;
    }

static uint32 DiagnosticCounterClear(AtChannel self, uint16 counterType)
    {
    if (counterType == cAtSdhLineCounterTypeB1)
        return Tha60290021ModuleOcnDiagLineB1CounterRead2Clear((AtSdhLine)self, cAtTrue);

    return 0x0;
    }

static uint32 CoreCounterGet(AtChannel self, uint16 counterType)
    {
    if (ThaSdhChannelShouldGetCounterFromPmc(self))
        return ThaModulePmcSdhLineCounterGet(PmcModule(self), (AtSdhChannel)self, counterType, cAtFalse);

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 CoreCounterClear(AtChannel self, uint16 counterType)
    {
    if (ThaSdhChannelShouldGetCounterFromPmc(self))
        return ThaModulePmcSdhLineCounterGet(PmcModule(self), (AtSdhChannel)self, counterType, cAtTrue);

    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (DiagnosticEnabled(self))
        return DiagnosticCounterGet(self, counterType);
    return CoreCounterGet(self, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (DiagnosticEnabled(self))
        return DiagnosticCounterClear(self, counterType);
    return CoreCounterClear(self, counterType);
    }

static uint32 BlockErrorCounterGet(AtSdhChannel self, uint16 counterType)
	{
	if (ThaSdhChannelShouldGetCounterFromPmc((AtChannel)self))
        return ThaModulePmcSdhLineBlockErrorCounterGet(PmcModule((AtChannel)self), self, counterType, cAtFalse);
	return m_AtSdhChannelMethods->BlockErrorCounterGet(self, counterType);
	}

static uint32 BlockErrorCounterClear(AtSdhChannel self, uint16 counterType)
	{
	if (ThaSdhChannelShouldGetCounterFromPmc((AtChannel)self))
        return ThaModulePmcSdhLineBlockErrorCounterGet(PmcModule((AtChannel)self), self, counterType, cAtTrue);
	return m_AtSdhChannelMethods->BlockErrorCounterClear(self, counterType);
	}

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (DiagnosticEnabled(self))
        return Tha60290021ModuleOcnDiagLineTxAlarmForce((AtSdhLine)self, alarmType, cAtTrue);

    if (alarmType == cAtSdhLineAlarmOof)
        return Tha60290021ModuleOcnFaceplateLineTxOofForce(OcnModule(self), AtChannelIdGet(self), cAtTrue);

    return m_AtChannelMethods->TxAlarmForce(self, alarmType);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (DiagnosticEnabled(self))
        return Tha60290021ModuleOcnDiagLineTxAlarmForce((AtSdhLine)self, alarmType, cAtFalse);

    if (alarmType == cAtSdhLineAlarmOof)
        return Tha60290021ModuleOcnFaceplateLineTxOofForce(OcnModule(self), AtChannelIdGet(self), cAtFalse);

    return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    if (DiagnosticEnabled(self))
        return Tha60290021ModuleOcnDiagLineTxForcedAlarmGet((AtSdhLine)self);

    if (Tha60290021ModuleOcnFaceplateLineTxOofIsForced(OcnModule(self), AtChannelIdGet(self)))
        return cAtSdhLineAlarmOof;

    return m_AtChannelMethods->TxForcedAlarmGet(self);
    }

static eBool RxAisForceIsSupported(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    uint32 startVersionHasThis = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x0, 0x00);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaDeviceVersionReader(device));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    if (RxAisForceIsSupported(self))
        return cAtSdhLineAlarmAis;

    return 0;
    }

static eAtRet HelperRxAisForce(AtChannel self, eBool forced)
    {
    Tha60210031SdhLine line = (Tha60210031SdhLine)self;
    uint32 regAddr = mMethodsGet(line)->TohMonitoringChannelControlRegAddr(line) +
                     mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    mRegFieldSet(regVal, cAf6_tohramctl_RxAisLFrc_, forced ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return cAtOk;
    }

static eBool HelperRxAisIsForced(AtChannel self)
    {
    Tha60210031SdhLine line = (Tha60210031SdhLine)self;
    uint32 regAddr = mMethodsGet(line)->TohMonitoringChannelControlRegAddr(line) +
                     mMethodsGet((ThaSdhLine)self)->DefaultOffset((ThaSdhLine)self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    return mRegField(regVal, cAf6_tohramctl_RxAisLFrc_) ? cAtTrue : cAtFalse;
    }

static eBool CanForceRxAlarm(AtChannel self, uint32 alarms)
    {
    return (AtChannelRxForcableAlarmsGet(self) & alarms) ? cAtTrue : cAtFalse;
    }

static eAtRet HelperRxAlarmForce(AtChannel self, uint32 alarmType, eBool forced, eAtRet (*superRxAlarmForce)(AtChannel self, uint32 alarmType))
    {
    if ((alarmType & cAtSdhLineAlarmAis) && CanForceRxAlarm(self, cAtSdhLineAlarmAis))
        {
        eAtRet ret = HelperRxAisForce(self, forced);
        if (ret != cAtOk)
            return ret;

        /* Let super deal with other alarms */
        alarmType = (alarmType & (uint32)(~cAtSdhLineAlarmAis));
        }

    return superRxAlarmForce(self, alarmType);
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return HelperRxAlarmForce(self, alarmType, cAtTrue, m_AtChannelMethods->RxAlarmForce);
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return HelperRxAlarmForce(self, alarmType, cAtFalse, m_AtChannelMethods->RxAlarmUnForce);
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    uint32 alarms = 0;

    if (CanForceRxAlarm(self, cAtSdhLineAlarmAis) && HelperRxAisIsForced(self))
        alarms |= cAtSdhLineAlarmAis;

    alarms |= m_AtChannelMethods->RxForcedAlarmGet(self);
    return alarms;
    }

static eBool OcnTxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    return Tha60290021ModuleOcnFacelateLineTxOverheadByteIsSupported(mOcnModule(self), AtChannelIdGet((AtChannel)self), overheadByte);
    }

static eBool TxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    if (OcnTxOverheadByteIsSupported(self, overheadByte))
        return cAtTrue;

    return m_AtSdhChannelMethods->TxOverheadByteIsSupported(self, overheadByte);
    }

static eBool OcnFacelateRxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    return Tha60290021ModuleOcnFacelateLineRxOverheadByteIsSupported(mOcnModule(self), AtChannelIdGet((AtChannel)self), overheadByte);
    }

static eBool RxOverheadByteIsSupported(AtSdhChannel self, uint32 overheadByte)
    {
    if (OcnFacelateRxOverheadByteIsSupported(self, overheadByte))
        return cAtTrue;

    return m_AtSdhChannelMethods->RxOverheadByteIsSupported(self, overheadByte);
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 value)
    {
    AtChannel channel = (AtChannel)self;
    if (OcnTxOverheadByteIsSupported(self, overheadByte))
        return Tha60290021ModuleOcnFaceplateLineTxOverheadByteSet(OcnModule(channel), AtChannelIdGet(channel), overheadByte, value);

    return m_AtSdhChannelMethods->TxOverheadByteSet(self, overheadByte, value);
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    AtChannel channel = (AtChannel)self;
    if (OcnTxOverheadByteIsSupported(self, overheadByte))
        return Tha60290021ModuleOcnFaceplateLineTxOverheadByteGet(OcnModule(channel), AtChannelIdGet(channel), overheadByte);

    return m_AtSdhChannelMethods->TxOverheadByteGet(self, overheadByte);
    }

static uint8 RxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    AtChannel channel = (AtChannel)self;

    if (OcnFacelateRxOverheadByteIsSupported(self, overheadByte))
        return Tha60290021ModuleOcnFaceplateLineRxOverheadByteGet(OcnModule(channel), AtChannelIdGet(channel), overheadByte);

    return m_AtSdhChannelMethods->RxOverheadByteGet(self, overheadByte);
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (DiagnosticEnabled(self))
        {
        if (errorType == cAtSdhLineErrorB1)
            return Tha60290021ModuleOcnDiagLineB1Force((AtSdhLine)self, cAtTrue);
        return cAtErrorModeNotSupport;
        }

    if (errorType == cAtSdhLineErrorB1)
        return Tha60290021ModuleOcnFaceplateLineTxB1Force(OcnModule(self), AtChannelIdGet(self), cAtTrue);

    return m_AtChannelMethods->TxErrorForce(self, errorType);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (DiagnosticEnabled(self))
        {
        if (errorType == cAtSdhLineErrorB1)
            return Tha60290021ModuleOcnDiagLineB1Force((AtSdhLine)self, cAtFalse);

        return cAtOk;
        }

    if (errorType == cAtSdhLineErrorB1)
        return Tha60290021ModuleOcnFaceplateLineTxB1Force(OcnModule(self), AtChannelIdGet(self), cAtFalse);

    return m_AtChannelMethods->TxErrorUnForce(self, errorType);
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    if (DiagnosticEnabled(self))
        {
        if (Tha60290021ModuleOcnDiagLineB1IsForced((AtSdhLine)self))
            return cAtSdhLineErrorB1;

        return cAtSdhLineErrorNone;
        }

    if (Tha60290021ModuleOcnFaceplateLineTxB1IsForced(OcnModule(self), AtChannelIdGet(self)))
        return cAtSdhLineErrorB1;

    return m_AtChannelMethods->TxForcedErrorGet(self);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (DiagnosticEnabled(self))
        return cAtSdhLineAlarmOof;

    return (m_AtChannelMethods->TxForcibleAlarmsGet(self) | cAtSdhLineAlarmOof);
    }

static eBool HasDiagnosticLogic(AtSdhLine self)
    {
    return Tha60290021ModuleOcnHasDiagnosticIp((Tha60290021ModuleOcn)mOcnModule(self));
    }

static eBool ShouldChangeRateOnly(AtSdhLine self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    if (AtDeviceDiagnosticModeIsEnabled(device) && AtDeviceTestbenchIsEnabled(device))
        return cAtTrue;
    return cAtFalse;
    }

static eBool ShouldAddExtraRateConfigure(AtSdhLine self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
	return Tha60290021DeviceIsRunningOnOtherPlatform(device);
    }

static eAtRet CachedRate(AtSdhLine self, eAtSdhLineRate newRate)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)self);
    return Tha60290021ModuleSdhFaceplateLineRateSet(moduleSdh, lineId, newRate);
    }

static eAtModuleSdhRet RateHwSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    if (HasDiagnosticLogic(self))
        return Tha60290021ModuleOcnDiagLineRateSet(self, rate);
    return  Tha60290021ModuleOcnFaceplateLineRateSet(mOcnModule(self), AtChannelIdGet((AtChannel)self), rate);
    }

static eAtModuleSdhRet RateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    eAtRet ret = cAtOk;

    if (ShouldChangeRateOnly(self))
        return RateHwSet(self, rate);

    if (AtChannelServiceIsRunning((AtChannel)self))
        return cAtErrorChannelBusy;

    if (AtSdhChannelIsJoiningAps((AtSdhChannel)self))
        return cAtErrorChannelBusy;

    AtSdhChannelSubChannelsInterruptDisable((AtSdhChannel)self, cAtTrue);

    if (Tha60290021SdhChannelNeedDestroyAllServices((AtSdhChannel)self))
        ret |= AtChannelAllServicesDestroy((AtChannel)self);

    /* Reset mapping to bring concatenation to default state */
    ret |= AtSdhChannelMappingReset((AtSdhChannel)self);
    if (ret != cAtOk)
        return ret;

    /* Configure hardware */
    ret = Tha60290021ModuleOcnFaceplateLineRateSet(mOcnModule(self), AtChannelIdGet((AtChannel)self), rate);
    if (ret != cAtOk)
        return ret;

    if (ShouldAddExtraRateConfigure(self))
        Tha60290021ModuleOcnLineRateExtraSet(mOcnModule(self), AtChannelIdGet((AtChannel)self), rate);

    /* Sync this rate to diagnostic module also */
    if (HasDiagnosticLogic(self))
        {
    ret = Tha60290021ModuleOcnDiagLineRateSet(self, rate);
    if (ret != cAtOk)
        return ret;
        }

    ret = CachedRate(self, rate);
    if (ret != cAtOk)
        return ret;

    /* Update database */
    return AtSdhLineSoftwareRateSet(self, rate);
    }

static void RateWillChange(AtSdhLine self, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    m_AtSdhLineMethods->RateWillChange(self, oldRate, newRate);
    Tha60290021ModuleSdhFaceplateLineRateWillChange(moduleSdh, (uint8)AtChannelIdGet((AtChannel)self), oldRate, newRate);
    }

static void RateDidChange(AtSdhLine self, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    m_AtSdhLineMethods->RateDidChange(self, oldRate, newRate);
    Tha60290021ModuleSdhFaceplateLineRateDidChange(moduleSdh, (uint8)AtChannelIdGet((AtChannel)self), oldRate, newRate);
    }

static eAtSdhLineRate RateGet(AtSdhLine self)
    {
    if (ShouldChangeRateOnly(self) && HasDiagnosticLogic(self))
        return Tha60290021ModuleOcnDiagLineRateGet(self);

    return Tha60290021ModuleSdhFaceplateLineRateGet((AtModuleSdh)AtChannelModuleGet((AtChannel)self), (uint8)AtChannelIdGet((AtChannel)self));
    }

static eBool RateIsSupported(AtSdhLine self, eAtSdhLineRate rate)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    return AtModuleSdhLineRateIsSupported(moduleSdh, self, rate);
    }

static eAtRet FaceplateLineCanChangeRateToStm64(AtSdhLine self)
    {
    static const uint8 cNumLinesPerGroup = 8;
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)self);
    uint8 line_i;

    /* If STM-64 is applied, all of other ports must be disabled */
    for (line_i = 1; line_i < cNumLinesPerGroup; line_i++)
        {
        AtSdhLine otherLine = AtModuleSdhLineGet(sdhModule, (uint8)(lineId + line_i));

        if (otherLine == NULL)
            continue;

        if (AtChannelServiceIsRunning((AtChannel)otherLine))
            return cAtErrorResourceBusy;

        if (AtSdhChannelIsJoiningAps((AtSdhChannel)otherLine))
            return cAtErrorResourceBusy;
        }

    return cAtOk;
    }

static eAtRet FaceplateLineCanChangeRateToStm16(AtSdhLine self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)self);
    AtSdhLine nextLine;

    /* If STM-16 is applied, the next line must be disabled */
    nextLine = AtModuleSdhLineGet(sdhModule, (uint8)(lineId + 1UL));

    if (nextLine == NULL)
        return cAtOk;

    if (AtChannelServiceIsRunning((AtChannel)nextLine))
        return cAtErrorResourceBusy;

    if (AtSdhChannelIsJoiningAps((AtSdhChannel)nextLine))
        return cAtErrorResourceBusy;

    return cAtOk;
    }

static eAtModuleSdhRet CanChangeRate(AtSdhLine self, eAtSdhLineRate rate)
    {
    if (rate == cAtSdhLineRateStm64)
        return FaceplateLineCanChangeRateToStm64(self);

    if (rate == cAtSdhLineRateStm16)
        return FaceplateLineCanChangeRateToStm16(self);

    return cAtOk;
    }

static eBool HasBerControllers(AtSdhLine self)
    {
    /* TODO: Has not known how hardware support this */
    AtUnused(self);
    return cAtFalse;
    }

static eBool SourceIsValid(eThaSdhLineKByteSource source)
	{
	switch (source)
		{
		case cThaSdhLineKByteSourceCpu    : return cAtTrue;
		case cThaSdhLineKByteSourceEth    : return cAtTrue;
		case cThaSdhLineKByteSourceUnknown: return cAtFalse;
		default: return cAtFalse;
		}
	}

static eAtRet KByteSourceSet(ThaSdhLine self, eThaSdhLineKByteSource source)
	{
	if (SourceIsValid(source))
	    return Tha60290021ModuleOcnFaceplateLineTxKbyteSourceSet(mOcnModule(self), AtChannelIdGet((AtChannel)self), source);
	return cAtErrorInvlParm;
	}

static eThaSdhLineKByteSource KByteSourceGet(ThaSdhLine self)
	{
    return Tha60290021ModuleOcnFaceplateLineTxKbyteSourceGet(mOcnModule(self), AtChannelIdGet((AtChannel)self));
	}

static eAtRet EK1TxSet(ThaSdhLine self, uint8 value)
	{
    AtUnused(self);
    return (value == 0) ? cAtOk : cAtErrorModeNotSupport;
	}

static eAtRet EK2TxSet(ThaSdhLine self, uint8 value)
	{
    AtUnused(self);
    AtUnused(value);
    return (value == 0) ? cAtOk : cAtErrorModeNotSupport;
	}

static uint8  EK1TxGet(ThaSdhLine self)
	{
    AtUnused(self);
    return 0x0;
	}

static uint8  EK2TxGet(ThaSdhLine self)
	{
    AtUnused(self);
    return 0x0;
	}

static eAtRet ExtendedKByteEnable(ThaSdhLine self, eBool enable)
    {
    ThaModuleOcn ocnModule = mOcnModule(self);
    uint32 lineId = AtChannelIdGet((AtChannel) self);
    eAtRet ret = cAtOk;

    ret |= Tha60290021ModuleOcnFaceplateLineTxExtensionKbyteEnable(ocnModule,lineId, enable);
    ret |= Tha60290021ModuleOcnFaceplateLineRxExtensionKbyteEnable(ocnModule,lineId, enable);

    return ret;
    }

static eBool ExtendedKByteIsEnabled(ThaSdhLine self)
    {
    return Tha60290021ModuleOcnFaceplateLineTxExtensionKbyteIsEnabled(mOcnModule(self), AtChannelIdGet((AtChannel)self));
    }

static eAtRet ExtendedKBytePositionSet(ThaSdhLine self, eThaSdhLineExtendedKBytePosition position)
    {
    return Tha60290021ModuleOcnFaceplateLineTxKbyteExtensionPostionSet(mOcnModule(self), AtChannelIdGet((AtChannel)self), position);
    }

static eThaSdhLineExtendedKBytePosition ExtendedKBytePositionGet(ThaSdhLine self)
    {
    return Tha60290021ModuleOcnFaceplateLineTxKbyteExtensionPostionGet(mOcnModule(self), AtChannelIdGet((AtChannel)self));
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    AtSnprintf(description, sizeof(description), "faceplate.%d", AtChannelIdGet((AtChannel)self) + 1);
    return description;
    }

static AtSerdesController SerdesController(AtSdhLine self)
    {
    AtChannel line = (AtChannel)self;
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(AtChannelDeviceGet(line));
    if (serdesManager)
        return AtSerdesManagerSerdesControllerGet(serdesManager, AtChannelIdGet(line));
    return NULL;
    }

static uint8 SliceId(void)
    {
    return 4;
    }

static uint8 HwStsId(AtChannel self)
    {
    return (uint8)AtChannelHwIdGet(self);
    }

static uint32 StatusReportOffset(AtChannel self)
    {
    return (uint32)(HwStsId(self) + (128 * SliceId())) + Tha60210011ModulePohBaseAddress(PohModule(self)) + (uint32)ThaModuleSdhPartOfChannel((AtSdhChannel)self);
    }

static uint32 DebugAlarmGet(AtChannel self)
    {
    uint32 alarmMask = 0;
    uint32 regAddr = cAf6Reg_alm_stahi_Base + StatusReportOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    if (regVal & cAf6_alm_stahi_timsta_Mask)
        alarmMask |= cAtSdhLineAlarmTim;

    return alarmMask;
    }

static uint32 DebugInterruptAlarmRead2Clear(AtChannel self, eBool r2c)
    {
    uint32 alarmMask = 0;
    uint32 regAddr = ThaModulePohAlmChghiBase(PohModule(self)) + StatusReportOffset(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);

    if (regVal & cAf6_alm_chghi_timstachg_Mask)
        alarmMask |= cAtSdhLineAlarmTim;

    if (r2c)
        mChannelHwWrite(self, regAddr, alarmMask, cThaModulePoh);

    return alarmMask;
    }

static void DebugAlarmPrint(uint32 alarmMask, const char* message)
    {
    AtPrintc(cSevInfo, "* %s:\r\n", message);
    AtPrintc(cSevNormal, "    %s: ", "TIM");
    AtPrintc((alarmMask & cAtSdhLineAlarmTim) ? cSevCritical : cSevInfo, "%s\r\n", (alarmMask & cAtSdhLineAlarmTim) ? "Set" : "Clear");
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);

    AtPrintc(cSevNormal, "\r\n");
    DebugAlarmPrint(DebugAlarmGet(self), "Current Status");
    DebugAlarmPrint(DebugInterruptAlarmRead2Clear(self, cAtTrue), "Interrupt");

    return cAtOk;
    }

static uint32 LocalIdInGroup(AtChannel self)
    {
    return Tha60290021ModuleOcnFaceplateLineLocalId(OcnModule(self), AtChannelIdGet(self));
    }

static uint32 InterruptEnableMask(AtChannel self)
    {
    return (cBit0 << LocalIdInGroup(self));
    }

static uint32 InterruptEnableShift(AtChannel self)
    {
    return LocalIdInGroup(self);
    }

static uint32 AddressWithLocalAddress(AtChannel self, uint32 localAddress)
    {
    ThaModuleOcn ocnModule = OcnModule(self);
    uint32 lineId = AtChannelIdGet(self);
    uint32 baseAddress = ThaModuleOcnBaseAddress(ocnModule);
    uint32 groupOffset = Tha60290021ModuleOcnFaceplateLineGroupOffset(ocnModule, lineId);
    return baseAddress + groupOffset + localAddress;
    }

static eAtRet HelperInterruptEnable(AtChannel self, eBool enabled)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_tohintperlineenctl_Base);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 interruptMask  = InterruptEnableMask(self);
    uint32 interruptShift = InterruptEnableShift(self);
    mRegFieldSet(regVal, interrupt, (enabled ? 1 : 0));
    mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
    return cAtOk;
    }

static eBool InterruptIsEnabled(AtChannel self)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_tohintperlineenctl_Base);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 interruptMask  = InterruptEnableMask(self);
    return (regVal & interruptMask) ? cAtTrue : cAtFalse;
    }

static eAtRet InterruptEnable(AtChannel self)
    {
    return HelperInterruptEnable(self, cAtTrue);
    }

static eAtRet InterruptDisable(AtChannel self)
    {
    return HelperInterruptEnable(self, cAtFalse);
    }

static uint32 GroupOffset(AtChannel self)
    {
    AtChannel line = (AtChannel)self;
    uint32 lineId = AtChannelIdGet(line);
    return Tha60290021ModuleOcnFaceplateLineGroupOffset(OcnModule(line), lineId);
    }

static uint32 ChannelOffset(Tha60210031SdhLine self)
    {
    return ThaModuleOcnLineTxDefaultOffset(mOcnModule(self), (AtSdhChannel)self);
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    uint32 moduleVal = moduleId;

    if ((moduleVal == cThaModuleOcn) ||
        (moduleVal == cThaModulePoh))
        return Tha60290021IsOcnFaceplateSideRegister(AtChannelDeviceGet(self), address);

    return cAtFalse;
    }

static eAtModuleSdhRet ScrambleEnable(AtSdhLine self, eBool enable)
    {
    if (DiagnosticEnabled((AtChannel)self))
        return Tha60290021ModuleOcnDiagLineScrambleEnable(self, enable);
    return Tha60290021ModuleOcnFaceplateLineScrambleEnable(mOcnModule(self), AtChannelIdGet((AtChannel)self), enable);
    }

static eBool ScrambleIsEnabled(AtSdhLine self)
    {
    if (DiagnosticEnabled((AtChannel)self))
        return Tha60290021ModuleOcnDiagLineScrambleIsEnabled(self);
    return Tha60290021ModuleOcnFaceplateLineScrambleIsEnabled(mOcnModule(self), AtChannelIdGet((AtChannel)self));
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    AtUnused(self);
    AtUnused(timingMode);
    AtUnused(timingSource);
    return cAtErrorModeNotSupport;
    }

static eAtTimingMode TimingModeGet(AtChannel self)
    {
    AtUnused(self);
    return cAtTimingModeUnknown;
    }

static eAtRet DefaultAlarmAutoRdiSet(AtChannel self)
    {
    eAtRet ret = cAtOk;

    ret |= AtSdhChannelAlarmAutoRdiEnable((AtSdhChannel)self, cAtSdhLineAlarmLos, cAtTrue);
    ret |= AtSdhChannelAlarmAutoRdiEnable((AtSdhChannel)self, cAtSdhLineAlarmLof, cAtTrue);
    ret |= AtSdhChannelAlarmAutoRdiEnable((AtSdhChannel)self, cAtSdhLineAlarmAis, cAtTrue);

    /* Disable TIM auto RDI-L as default. */
    ret |= AtSdhChannelAlarmAutoRdiEnable((AtSdhChannel)self, cAtSdhLineAlarmTim, cAtFalse);

    return ret;
    }

static eAtRet ZnDefaultSet(AtSdhLine self)
    {
    eAtRet ret = cAtOk;

    if (AtSdhLineZ1CanMonitor(self))
        ret |= AtSdhLineZ1MonitorPositionSet(self, 0);

    if (AtSdhLineZ2CanMonitor(self))
        ret |= AtSdhLineZ2MonitorPositionSet(self, 0);

    return ret;
    }

static eAtRet DefaultSet(AtChannel self)
    {
    AtSdhLine line = (AtSdhLine)self;
    ThaTtiProcessor tti = ThaSdhLineTtiProcessor(line);

    eAtRet ret = cAtOk;

    ret |= AtSdhLineScrambleEnable(line, cAtTrue);
    ret |= AtChannelEnable(self, cAtTrue);
    if (HasDiagnosticLogic(line))
    ret |= Tha60290021ModuleOcnDiagLineDefaultSet(line);
    ret |= ThaSdhLineExtendedKBytePositionSet((ThaSdhLine) self, cThaSdhLineExtendedKBytePositionD1Sts4);
    ret |= ThaTtiProcessorDefaultSet(tti);
	ret |= AtSdhChannelAutoRdiEnable((AtSdhChannel) self, cAtTrue);
	ret |= DefaultAlarmAutoRdiSet(self);

	ret |= ZnDefaultSet((AtSdhLine)self);
	ret |= ThaSdhLineKByteSourceSet((ThaSdhLine)self, cThaSdhLineKByteSourceCpu);

    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return DefaultSet(self);
    }

static eAtRet Enable(AtChannel self, eBool enable)
    {
    if (DiagnosticEnabled(self))
        return enable ? cAtOk : cAtErrorModeNotSupport;
    return Tha60290021ModuleOcnFaceplateLineEnable(OcnModule(self), AtChannelIdGet(self), enable);
    }

static eBool IsEnabled(AtChannel self)
    {
    if (DiagnosticEnabled(self))
        return cAtTrue;
    return Tha60290021ModuleOcnFaceplateLineIsEnabled(OcnModule(self), AtChannelIdGet(self));
    }

static eBool CanEnable(AtChannel self, eBool enable)
    {
    if (DiagnosticEnabled(self))
        return enable;

    AtUnused(self);
    AtUnused(enable);
    return cAtTrue;
    }

static eAtRet TxEnable(AtChannel self, eBool enable)
    {
    return Tha60290021ModuleOcnFaceplateLineTxEnable(OcnModule(self), AtChannelIdGet(self), enable);
    }

static eBool TxIsEnabled(AtChannel self)
    {
    return Tha60290021ModuleOcnFaceplateLineTxIsEnabled(OcnModule(self), AtChannelIdGet(self));
    }

static eAtRet RxEnable(AtChannel self, eBool enable)
    {
    return Tha60290021ModuleOcnFaceplateLineRxEnable(OcnModule(self), AtChannelIdGet(self), enable);
    }

static eBool RxIsEnabled(AtChannel self)
    {
    return Tha60290021ModuleOcnFaceplateLineRxIsEnabled(OcnModule(self), AtChannelIdGet(self));
    }

static eBool RxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtTrue;
    }

static eBool TxCanEnable(AtChannel self, eBool enabled)
    {
    AtUnused(self);
    AtUnused(enabled);
    return cAtTrue;
    }

static uint32 NumSubLinesInGroup(AtChannel self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 GroupId(AtChannel self)
    {
    return (AtChannelIdGet(self) / NumSubLinesInGroup(self));
    }

static uint32 Stm64DiagnosticBaseAddress(ThaSdhLine self)
    {
    AtUnused(self);
    return 0xFA0000UL;
    }

static AtSerdesManager SerdesManager(ThaSdhLine self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return AtDeviceSerdesManagerGet(device);
    }

static uint32 DiagnosticBaseAddress(ThaSdhLine self)
    {
    uint8 localId = ThaModuleSdhLineLocalId((AtSdhLine)self);
    AtSerdesController controller = Tha60290021SerdesManagerFacePlateSerdesGet(SerdesManager(self), localId);

    if (AtSerdesControllerModeGet(controller) == cAtSerdesModeStm64)
        return Stm64DiagnosticBaseAddress(self);

    return (0xFAA000UL + (GroupId((AtChannel)self) * 0x800UL));
    }

static eBool ShouldInitSerdesController(AtSdhLine self)
    {
    /* In this product, SERDES and Line are separate layers, should avoid side
     * affect */
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    AtSerdesController serdes = AtSdhLineSerdesController((AtSdhLine)self);
    if (serdes)
        return AtSerdesControllerLoopbackSet(serdes, loopbackMode);
    return m_AtChannelMethods->LoopbackSet(self, loopbackMode);
    }

static uint8 LoopbackGet(AtChannel self)
    {
    AtSerdesController serdes = AtSdhLineSerdesController((AtSdhLine)self);
    if (serdes)
        return AtSerdesControllerLoopbackGet(serdes);
    return m_AtChannelMethods->LoopbackGet(self);
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtSerdesController serdes = AtSdhLineSerdesController((AtSdhLine)self);

    if (loopbackMode == cAtLoopbackModeRelease)
        return cAtTrue;

    return AtSerdesControllerCanLoopback(serdes, loopbackMode, cAtTrue);
    }

static uint32 SupportedAlarmAffecting(AtSdhChannel self)
    {
    uint32 alarms = cAtSdhLineAlarmLos |
    				cAtSdhLineAlarmTim |
                    cAtSdhLineAlarmLof |
                    cAtSdhLineAlarmAis;
    AtUnused(self);
    return alarms;
    }

static eBool CanChangeAlarmAffecting(AtSdhChannel self, uint32 alarmType)
	{
	return (alarmType & SupportedAlarmAffecting(self)) ? cAtTrue : cAtFalse;
	}

static eAtModuleSdhRet HwAlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    AtChannel channel = (AtChannel)self;
    ThaModuleOcn ocn = OcnModule(channel);
    uint32 lineId = AtChannelIdGet(channel);
    eAtRet ret = cAtOk;

    ret |= Tha60290021ModuleOcnFaceplateLineAisFordwardingEnable(ocn, lineId, alarmType, enable);
    if (alarmType & cAtSdhLineAlarmTim)
        {
        ret |= Tha60290021ModuleOcnFaceplateLineRdiEnable(ocn, lineId, cAtSdhLineAlarmTim, enable);
        ret |= ThaTtiProcessorTimAffectingEnable(ThaSdhLineTtiProcessor((AtSdhLine)self), enable);
        }

    return ret;
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    if (alarmType & SupportedAlarmAffecting(self))
        return HwAlarmAffectingEnable(self, alarmType, enable);

    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    if (alarmType & SupportedAlarmAffecting(self))
    	return Tha60290021ModuleOcnFaceplateLineAisFordwardingIsEnabled(OcnModule((AtChannel)self), AtChannelIdGet((AtChannel)self), alarmType);

    return cAtFalse;
    }

static uint32 AlarmAffectingMaskGet(AtSdhChannel self)
    {
    uint32 alarms = 0;
    ThaModuleOcn ocnModule = OcnModule((AtChannel)self);
	uint8 lineId = (uint8) AtChannelIdGet((AtChannel) self);

    if (Tha60290021ModuleOcnFaceplateLineAisFordwardingIsEnabled(ocnModule, lineId, cAtSdhLineAlarmLos))
        alarms |= cAtSdhLineAlarmLos;

    if (Tha60290021ModuleOcnFaceplateLineAisFordwardingIsEnabled(ocnModule, lineId, cAtSdhLineAlarmLof))
        alarms |= cAtSdhLineAlarmLof;

    if (Tha60290021ModuleOcnFaceplateLineAisFordwardingIsEnabled(ocnModule, lineId, cAtSdhLineAlarmTim))
        alarms |= cAtSdhLineAlarmTim;

    if (Tha60290021ModuleOcnFaceplateLineAisFordwardingIsEnabled(ocnModule, lineId, cAtSdhLineAlarmAis))
        alarms |= cAtSdhLineAlarmAis;

    return alarms;
    }

static uint32 TxFramerPerChannelControlRegAddr(Tha60210031SdhLine self)
    {
    return Tha60290021ModuleOcnTxFramerPerChannelControlRegAddr(mOcnModule(self));
    }

static uint32 TohMonitoringChannelControlRegAddr(Tha60210031SdhLine self)
    {
    return Tha60290021ModuleOcnTohMonitoringChannelControlRegAddr(OcnModule((AtChannel)self));
    }

static uint32 B1CounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb1errr2ccnt_Base : cAf6Reg_tohb1errrocnt_Base;
    }

static uint32 B2CounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb2errr2ccnt_Base : cAf6Reg_tohb2errrocnt_Base;
    }

static uint32 ReiCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohreierrr2ccnt_Base : cAf6Reg_tohreierrrocnt_Base;
    }

static uint32 B1BlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb1blkerrr2ccnt_Base : cAf6Reg_tohb1blkerrrocnt_Base;
    }

static uint32 B2BlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohb2blkerrr2ccnt_Base : cAf6Reg_tohb2blkerrrocnt_Base;
    }

static uint32 ReiBlockCounterRegAddr(Tha60210031SdhLine self, eBool r2c)
    {
    AtUnused(self);
    return r2c ? cAf6Reg_tohreiblkerrr2ccnt_Base : cAf6Reg_tohreiblkerrrocnt_Base;
    }

static uint32 RxLineAlarmCurrentStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohcursta_Base;
    }

static uint32 RxLineAlarmInterruptEnableControlRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohintperalrenbctl_Base;
    }

static uint32 RxLineAlarmInterruptStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohintsta_Base;
    }

static uint32 TohS1MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohs1monsta_Base;
    }

static uint32 TohK1MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohk1monsta_Base;
    }

static uint32 TohK2MonitoringStatusRegAddr(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAf6Reg_tohk2monsta_Base;
    }

static eAtRet Z1MonitorPositionSet(AtSdhLine self, uint8 sts1)
    {
    AtChannel channel = (AtChannel)self;
    if (OcnFacelateRxOverheadByteIsSupported((AtSdhChannel)self, cAtSdhLineMsOverheadByteZ1))
        return Tha60290021ModuleOcnFaceplateLineRxZ1PositionSet(OcnModule(channel), AtChannelIdGet(channel), sts1);
    return cAtErrorModeNotSupport;
    }

static eBool Z1CanMonitor(AtSdhLine self)
    {
    return OcnFacelateRxOverheadByteIsSupported((AtSdhChannel)self, cAtSdhLineMsOverheadByteZ1);
    }

static uint8 Z1MonitorPositionGet(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;
    if (OcnFacelateRxOverheadByteIsSupported((AtSdhChannel)self, cAtSdhLineMsOverheadByteZ1))
        return Tha60290021ModuleOcnFaceplateLineRxZ1PositionGet(OcnModule(channel), AtChannelIdGet(channel));
    return cInvalidUint8;
    }

static eAtRet Z2MonitorPositionSet(AtSdhLine self, uint8 sts1)
    {
    AtChannel channel = (AtChannel)self;
    if (OcnFacelateRxOverheadByteIsSupported((AtSdhChannel)self, cAtSdhLineMsOverheadByteZ2))
        return Tha60290021ModuleOcnFaceplateLineRxZ2PositionSet(OcnModule(channel), AtChannelIdGet(channel), sts1);
    return cAtErrorModeNotSupport;
    }

static uint8 Z2MonitorPositionGet(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;
    if (OcnFacelateRxOverheadByteIsSupported((AtSdhChannel)self, cAtSdhLineMsOverheadByteZ2))
        return Tha60290021ModuleOcnFaceplateLineRxZ2PositionGet(OcnModule(channel), AtChannelIdGet(channel));
    return cInvalidUint8;
    }

static eBool Z2CanMonitor(AtSdhLine self)
    {
    return OcnFacelateRxOverheadByteIsSupported((AtSdhChannel)self, cAtSdhLineMsOverheadByteZ2);
    }

static uint32 TohGlobalS1StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    return m_Tha60210031SdhLineMethods->TohGlobalS1StableMonitoringThresholdControlRegAddr(self) + mGroupOffset(self);
    }

static uint32 TohGlobalK1StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    return m_Tha60210031SdhLineMethods->TohGlobalK1StableMonitoringThresholdControlRegAddr(self) + mGroupOffset(self);
    }

static uint32 TohGlobalK2StableMonitoringThresholdControlRegAddr(Tha60210031SdhLine self)
    {
    return m_Tha60210031SdhLineMethods->TohGlobalK2StableMonitoringThresholdControlRegAddr(self) + mGroupOffset(self);
    }

static uint32 DefaultOffset(ThaSdhLine self)
    {
    return ThaModuleOcnLineDefaultOffset(mOcnModule(self), (AtSdhChannel)self);
    }

static eAtModuleSdhRet AlarmAutoRdiEnable(AtSdhChannel self, uint32 alarms, eBool enable)
    {
    AtChannel channel = (AtChannel)self;
    ThaModuleOcn ocnModule = OcnModule(channel);
    uint8 lineId = (uint8) AtChannelIdGet(channel);
    eAtRet ret = cAtOk;

    if (alarms & cAtSdhLineAlarmLos)
        ret |= Tha60290021ModuleOcnFaceplateLineRdiEnable(ocnModule, lineId, cAtSdhLineAlarmLos, enable);

    if (alarms & cAtSdhLineAlarmLof)
        ret |= Tha60290021ModuleOcnFaceplateLineRdiEnable(ocnModule, lineId, cAtSdhLineAlarmLof, enable);

    if (alarms & cAtSdhLineAlarmAis)
        ret |= Tha60290021ModuleOcnFaceplateLineRdiEnable(ocnModule, lineId, cAtSdhLineAlarmAis, enable);

    if (alarms & cAtSdhLineAlarmTim)
        ret |= Tha60290021ModuleOcnFaceplateLineRdiEnable(ocnModule, lineId, cAtSdhLineAlarmTim, enable);

    return cAtOk;
    }

static eBool AlarmAutoRdiIsEnabled(AtSdhChannel self, uint32 alarms)
    {
    AtChannel channel = (AtChannel)self;
    ThaModuleOcn ocnModule = OcnModule(channel);
    uint8 lineId = (uint8) AtChannelIdGet(channel);

    if (alarms & cAtSdhLineAlarmLos)
        return Tha60290021ModuleOcnFaceplateLineRdiIsEnabled(ocnModule, lineId, cAtSdhLineAlarmLos);

    if (alarms & cAtSdhLineAlarmLof)
        return Tha60290021ModuleOcnFaceplateLineRdiIsEnabled(ocnModule, lineId, cAtSdhLineAlarmLof);

    if (alarms & cAtSdhLineAlarmAis)
        return Tha60290021ModuleOcnFaceplateLineRdiIsEnabled(ocnModule, lineId, cAtSdhLineAlarmAis);

    if (alarms & cAtSdhLineAlarmTim)
        return Tha60290021ModuleOcnFaceplateLineRdiIsEnabled(ocnModule, lineId, cAtSdhLineAlarmTim);

    return cAtFalse;
    }

static eBool ShouldHideLofWhenLos(Tha60210031SdhLine self)
    {
    return Tha602900xxSdhLineShouldHideLofWhenLos((AtSdhLine)self);
    }

static AtModuleSdh SdhModule(AtSdhLine self)
    {
    return (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    }

static uint8 LocalIdGet(AtSdhLine self)
    {
    return ThaModuleSdhLineIdLocalId((ThaModuleSdh)SdhModule(self), (uint8)AtChannelIdGet((AtChannel)self));
    }

static AtSdhLine HideLineGet(AtSdhLine self, uint8 sts1)
    {
    uint8 localId = LocalIdGet(self);
    uint8 firstTerminatedLocalId = (uint8)((localId / 8U) * 4U);

    /* If faceplate line is not STM64, two consecutive faceplate lines are mapped to one terminated line STM16. */
    if (AtSdhLineRateGet(self) != cAtSdhLineRateStm64)
        return Tha60290021ModuleSdhTerminatedLineGet(SdhModule(self), (uint8)(localId / 2));

    /* One faceplate line STM64 is mapped to four terminated lines STM16. */
    return Tha60290021ModuleSdhTerminatedLineGet(SdhModule(self), (uint8)(firstTerminatedLocalId + (sts1 / 48)));
    }

static eBool Ec1TxAisForceIsSupported(Tha60210031SdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideTha60210031SdhLine(AtSdhLine self)
    {
    Tha60210031SdhLine line = (Tha60210031SdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210031SdhLineMethods = mMethodsGet(line);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031SdhLineOverride, mMethodsGet(line), sizeof(m_Tha60210031SdhLineOverride));

        mMethodOverride(m_Tha60210031SdhLineOverride, ChannelOffset);
        mMethodOverride(m_Tha60210031SdhLineOverride, TxFramerPerChannelControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohMonitoringChannelControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B1CounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B2CounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, ReiCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B1BlockCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, B2BlockCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, ReiBlockCounterRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, RxLineAlarmCurrentStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, RxLineAlarmInterruptEnableControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, RxLineAlarmInterruptStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohS1MonitoringStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohK1MonitoringStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohK2MonitoringStatusRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohGlobalS1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohGlobalK1StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, TohGlobalK2StableMonitoringThresholdControlRegAddr);
        mMethodOverride(m_Tha60210031SdhLineOverride, ShouldHideLofWhenLos);
        mMethodOverride(m_Tha60210031SdhLineOverride, Ec1TxAisForceIsSupported);
        }

    mMethodsSet(line, &m_Tha60210031SdhLineOverride);
    }

static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhLineMethods = mMethodsGet(line);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, m_ThaSdhLineMethods, sizeof(m_ThaSdhLineOverride));

        mMethodOverride(m_ThaSdhLineOverride, ExtendedKByteEnable);
        mMethodOverride(m_ThaSdhLineOverride, ExtendedKByteIsEnabled);
        mMethodOverride(m_ThaSdhLineOverride, ExtendedKBytePositionSet);
        mMethodOverride(m_ThaSdhLineOverride, ExtendedKBytePositionGet);
        mMethodOverride(m_ThaSdhLineOverride, DiagnosticBaseAddress);
        mMethodOverride(m_ThaSdhLineOverride, KByteSourceSet);
        mMethodOverride(m_ThaSdhLineOverride, KByteSourceGet);
        mMethodOverride(m_ThaSdhLineOverride, EK1TxSet);
        mMethodOverride(m_ThaSdhLineOverride, EK2TxSet);
        mMethodOverride(m_ThaSdhLineOverride, EK1TxGet);
        mMethodOverride(m_ThaSdhLineOverride, EK2TxGet);
        mMethodOverride(m_ThaSdhLineOverride, DefaultOffset);
        }

    mMethodsSet(line, &m_ThaSdhLineOverride);
    }

static void OverrideAtObject(AtSdhLine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, mMethodsGet(self), sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, RateSet);
        mMethodOverride(m_AtSdhLineOverride, RateGet);
        mMethodOverride(m_AtSdhLineOverride, RateIsSupported);
        mMethodOverride(m_AtSdhLineOverride, CanChangeRate);
        mMethodOverride(m_AtSdhLineOverride, RateWillChange);
        mMethodOverride(m_AtSdhLineOverride, RateDidChange);
        mMethodOverride(m_AtSdhLineOverride, HasBerControllers);
        mMethodOverride(m_AtSdhLineOverride, SerdesController);
        mMethodOverride(m_AtSdhLineOverride, ScrambleEnable);
        mMethodOverride(m_AtSdhLineOverride, ScrambleIsEnabled);
        mMethodOverride(m_AtSdhLineOverride, ShouldInitSerdesController);
        mMethodOverride(m_AtSdhLineOverride, HideLineGet);

        /* Zn monitoring */
        mMethodOverride(m_AtSdhLineOverride, Z1MonitorPositionSet);
        mMethodOverride(m_AtSdhLineOverride, Z1MonitorPositionGet);
        mMethodOverride(m_AtSdhLineOverride, Z1CanMonitor);
        mMethodOverride(m_AtSdhLineOverride, Z2MonitorPositionSet);
        mMethodOverride(m_AtSdhLineOverride, Z2MonitorPositionGet);
        mMethodOverride(m_AtSdhLineOverride, Z2CanMonitor);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, InterruptEnable);
        mMethodOverride(m_AtChannelOverride, InterruptDisable);
        mMethodOverride(m_AtChannelOverride, InterruptIsEnabled);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        mMethodOverride(m_AtChannelOverride, TimingModeGet);
        mMethodOverride(m_AtChannelOverride, Enable);
        mMethodOverride(m_AtChannelOverride, IsEnabled);
        mMethodOverride(m_AtChannelOverride, CanEnable);
        mMethodOverride(m_AtChannelOverride, TxEnable);
        mMethodOverride(m_AtChannelOverride, TxIsEnabled);
        mMethodOverride(m_AtChannelOverride, TxCanEnable);
        mMethodOverride(m_AtChannelOverride, RxEnable);
        mMethodOverride(m_AtChannelOverride, RxIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxCanEnable);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhLine self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterGet);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterClear);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeAlarmAffecting);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteSet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteIsSupported);
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteIsSupported);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAutoRdiEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAutoRdiIsEnabled);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhLine(self);
    OverrideThaSdhLine(self);
    OverrideTha60210031SdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhFacePlateLine);
    }

AtSdhLine Tha6029SdhFacePlateLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031SdhLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha6029SdhFacePlateLineNew(uint32 lineId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6029SdhFacePlateLineObjectInit(newLine, lineId, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6029SdhAuVcInternal.h
 * 
 * Created Date: Jul 11, 2016
 *
 * Description : PWCodechip-6029 AU VC internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SDHFACEPLATELINEAUVCINTERNAL_H_
#define _THA6029SDHFACEPLATELINEAUVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "Tha6029SdhLineSideAuVcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SdhFacePlateLineAuVc
    {
    tTha6029SdhLineSideAuVc super;
    }tTha6029SdhFacePlateLineAuVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha6029SdhFacePlateLineAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SDHFACEPLATELINEAUVCINTERNAL_H_ */


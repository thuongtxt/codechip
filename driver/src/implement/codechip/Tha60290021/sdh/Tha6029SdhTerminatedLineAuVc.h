/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6029SdhTerminatedLineAuVc.h
 * 
 * Created Date: Oct 9, 2017
 *
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SDHTERMINATEDLINEAUVC_H_
#define _THA6029SDHTERMINATEDLINEAUVC_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SdhTerminatedLineAuVc *Tha6029SdhTerminatedLineAuVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha6029SdhTerminatedLineAuVcMapTypeUpdate(AtSdhChannel self, uint8 mapType);
eAtRet Tha6029SdhTerminatedLineAuVcHoSelect(AtSdhChannel self, eBool selectFromHo);
eBool Tha6029SdhTerminatedLineAuVcFromLoBus(AtSdhChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SDHTERMINATEDLINEAUVC_H_ */


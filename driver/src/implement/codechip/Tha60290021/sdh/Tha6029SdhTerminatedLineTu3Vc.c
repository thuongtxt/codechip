/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029TerminatedLineTu3Vc.c
 *
 * Created Date: Jul 13, 2017
 *
 * Description : SDH Tu3Vc
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"
#include "Tha6029SdhTerminatedLineTu3VcInternal.h"
#include "Tha60290021ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods      m_AtChannelOverride;
static tAtSdhChannelMethods   m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtChannelMethods     *m_AtChannelMethods   = NULL;
static const tAtSdhChannelMethods  *m_AtSdhChannelMethods   = NULL;

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule PmcModuleGet(AtChannel self)
    {
    return (AtModule)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (ThaSdhChannelShouldGetCounterFromPmc(self))
        return ThaModulePmcSdhTuVc1xCounterGet(PmcModuleGet(self), (AtSdhChannel)self, counterType, cAtFalse);

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (ThaSdhChannelShouldGetCounterFromPmc(self))
        return ThaModulePmcSdhTuVc1xCounterGet(PmcModuleGet(self), (AtSdhChannel)self, counterType, cAtTrue);

    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static eAtSdhChannelMode DefaultMode(AtSdhChannel self)
    {
    if (Tha60290021SdhChannelHidingXcIsActive(self))
        return Tha60290021SdhTerminatedChannelHidingXcDefaultMode(self);

    return m_AtSdhChannelMethods->DefaultMode(self);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, DefaultMode);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhTerminatedLineTu3Vc);
    }

AtSdhVc Tha6029SdhTerminatedLineTu3VcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5LineTu3VcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha6029SdhTerminatedLineTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    return Tha6029SdhTerminatedLineTu3VcObjectInit(self, channelId, channelType, module);
    }

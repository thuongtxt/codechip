/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6029SdhTerminatedLineTu3VcInternal.h
 * 
 * Created Date: Jul 13, 2017
 *
 * Description : PWCodechip-6029 AU VC internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef __THA6029SDHTERMINATEDLINETU3VCINTERNAL_H_
#define __THA6029SDHTERMINATEDLINETU3VCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineTu3VcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SdhTerminatedLineTu3Vc
    {
    tTha60210011Tfi5LineTu3Vc super;
    }tTha6029SdhTerminatedLineTu3Vc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha6029SdhTerminatedLineTu3VcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* __THA6029SDHTERMINATEDLINETU3VCINTERNAL_H_ */


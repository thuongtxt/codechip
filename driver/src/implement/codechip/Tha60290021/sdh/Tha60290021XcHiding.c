/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290021XcHiding.c
 *
 * Created Date: Dec 25, 2016
 *
 * Description : XC hiding
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../../generic/sdh/AtSdhVcInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "AtSdhLine.h"
#include "Tha60290021XcHiding.h"
#include "Tha60290021ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhChannel XcHidingPairVcFromAug(AtSdhChannel self, AtSdhChannel pairAug)
    {
    AtSdhChannel au = AtSdhChannelParentChannelGet(self);
    AtSdhChannel pairAu;

    if (pairAug == NULL)
        return NULL;

    pairAu = AtSdhChannelSubChannelGet(pairAug, (uint8)AtChannelIdGet((AtChannel)au));
    if (pairAu && (AtSdhChannelTypeGet(pairAu) == AtSdhChannelTypeGet(au)))
        return AtSdhChannelSubChannelGet(pairAu, 0);

    return NULL;
    }

static uint8 LineSideChannelToPairSts1IdBase(AtSdhChannel self)
    {
    return AtSdhChannelSts1Get(self) % 48;
    }

static uint8 LineSideChannelToPairSts1Offset(AtSdhChannel self)
    {
    AtSdhLine faceLine = AtSdhChannelLineObjectGet(self);

    /* First Line always with Zero offset, Not need to check */
    if (AtChannelIdGet((AtChannel) faceLine) % 2 == 0)
        return 0;

    switch (AtSdhLineRateGet(faceLine))
        {
        case cAtSdhLineRateStm0:    return 24;
        case cAtSdhLineRateStm1:    return 24;
        case cAtSdhLineRateStm4:    return 24;
        case cAtSdhLineRateStm16:   return 0;
        case cAtSdhLineRateStm64:   return 0;
        case cAtSdhLineRateInvalid: return 0;
        default: return 0;
        }
    }

static uint8 LineSideChannelToPairSts1(AtSdhChannel self)
    {
    return (uint8)(LineSideChannelToPairSts1IdBase(self) + LineSideChannelToPairSts1Offset(self));
    }

static uint8 LineSideChannelToTerminatedLineLocalId(AtModuleSdh self, AtSdhChannel channel)
    {
    return ThaModuleSdhLineIdLocalId((ThaModuleSdh)self, AtSdhChannelLineGet(channel)) / 2;
    }

uint8 Tha60290021XcHidingPairSts1Id(AtSdhChannel self)
    {
    if (self)
        return LineSideChannelToPairSts1IdBase(self);
    return cInvalidUint8;
    }

AtSdhChannel Tha60290021XcHidingLineSideXcHidingPairChannel(AtSdhChannel self, AtSdhLine pairLine)
    {
    uint8 pairSts1 = LineSideChannelToPairSts1(self);
    uint32 augType = AtSdhChannelTypeGet(self);

    switch (augType)
        {
        case cAtSdhChannelTypeAug16:
            return (AtSdhChannel)AtSdhLineAug16Get(pairLine, 0);

        case cAtSdhChannelTypeAug4:
            return (AtSdhChannel)AtSdhLineAug4Get(pairLine, pairSts1 / 12);

        case cAtSdhChannelTypeAug1:
            return (AtSdhChannel)AtSdhLineAug1Get(pairLine, pairSts1 / 3);

        case cAtSdhChannelTypeVc3:
            return (AtSdhChannel)AtSdhLineVc3Get(pairLine, pairSts1 / 3, 0);

        case cAtSdhChannelTypeAug64:
        default:
            return NULL;
        }
    }

uint8 Tha60290021XcHidingPairTerminatedLineLocalId(AtModuleSdh self, AtSdhChannel channel)
    {
    return LineSideChannelToTerminatedLineLocalId(self, channel);
    }

AtSdhChannel Tha60290021XcHidingPairVcFromAug(AtSdhChannel self, AtSdhChannel pairAug)
    {
    return XcHidingPairVcFromAug(self, pairAug);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290021SdhLineSideAug.c
 *
 * Created Date: Oct 15, 2016
 *
 * Description : Line side (faceplate/mate) AUGs
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhLineInternal.h"
#include "Tha60290021SdhAugInternal.h"
#include "Tha60290021ModuleSdh.h"
#include "Tha60290021XcHiding.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021SdhLineSideAug)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021SdhLineSideAugMethods m_methods;

/* Override */
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleXc XcModule(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static eTha60290021XcHideMode XcHideMode(AtSdhChannel self)
    {
    return Tha60290021ModuleXcHideModeGet(XcModule(self));
    }

static uint8 XcHidingLineSideSourcePairLocalLineId(Tha60290021SdhLineSideAug self)
    {
    AtUnused(self);
    return cInvalidUint8;
    }

static AtSdhLine XcHidingLineSideSourcePairLine(Tha60290021SdhLineSideAug self, uint8 pairLocalId)
    {
    AtUnused(self);
    AtUnused(pairLocalId);
    return NULL;
    }

static AtSdhLine XcHidingLineSideDestPairLine(Tha60290021SdhLineSideAug self, uint8 pairLocalId)
    {
    AtUnused(self);
    AtUnused(pairLocalId);
    return NULL;
    }

static uint8 XcHidingLineSideDestPairLocalLineId(Tha60290021SdhLineSideAug self)
    {
    AtUnused(self);
    return cInvalidUint8;
    }

static AtSdhChannel XcHidingPairAug(AtSdhChannel self, AtSdhLine pairLine)
    {
    return Tha60290021XcHidingLineSideXcHidingPairChannel(self, pairLine);
    }

static AtSdhChannel XcHidingLineSideSourcePairAug(AtSdhChannel self)
    {
    uint8 pairLocalId = mMethodsGet(mThis(self))->XcHidingLineSideSourcePairLocalLineId(mThis(self));
    AtSdhLine pairLine = mMethodsGet(mThis(self))->XcHidingLineSideSourcePairLine(mThis(self), pairLocalId);
    return XcHidingPairAug(self, pairLine);
    }

static AtSdhChannel XcHidingLineSideDestPairAug(AtSdhChannel self)
    {
    uint8 pairLocalId = mMethodsGet(mThis(self))->XcHidingLineSideDestPairLocalLineId(mThis(self));
    AtSdhLine pairLine = mMethodsGet(mThis(self))->XcHidingLineSideDestPairLine(mThis(self), pairLocalId);
    return XcHidingPairAug(self, pairLine);
    }

static eBool XcHidingShouldApply(Tha60290021SdhLineSideAug self, eTha60290021XcHideMode hideMode)
    {
    AtUnused(self);
    return (hideMode == cTha60290021XcHideModeDirect) ? cAtTrue : cAtFalse;
    }

static eAtModuleSdhRet XcHidingApplyMapTypeOnSourceDestPairAug(AtSdhChannel self, uint8 mapType)
    {
    eTha60290021XcHideMode hideMode = XcHideMode(self);
    AtSdhChannel pairAug = NULL;
    eAtRet ret = cAtOk;

    if (!mMethodsGet(mThis(self))->XcHidingShouldApply(mThis(self), hideMode))
        return cAtOk;

    if (hideMode != cTha60290021XcHideModeDirect)
        {
        pairAug = Tha60290021SdhLineSideAugXcHdingLineSideSourcePairAug(self);
        if (pairAug)
            ret |= mMethodsGet(pairAug)->MapTypeSet(pairAug, mapType);

        pairAug = Tha60290021SdhLineSideAugXcHdingLineSideDestPairAug(self);
        if (pairAug)
            ret |= mMethodsGet(pairAug)->MapTypeSet(pairAug, mapType);
        }

    return ret;
    }

static AtSdhChannel HideAug64Get(Tha60290021SdhLineSideAug self, uint8 mapType)
    {
    AtUnused(self);
    AtUnused(mapType);
    return NULL;
    }

static AtSdhChannel HideAugGet(AtSdhChannel self, uint8 mapType)
    {
    if (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeAug64)
        return mMethodsGet(mThis(self))->HideAug64Get(mThis(self), mapType);

    return AtSdhChannelHideChannelGet(self);
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel hideAug;

    if (AtSdhChannelMapTypeGet(self) == mapType)
        return cAtOk;

    if (Tha60290021SdhChannelNeedDestroyAllServices(self))
        ret |= AtChannelAllServicesDestroy((AtChannel)self);

    hideAug = HideAugGet(self, mapType);
    if (hideAug)
        ret |= mMethodsGet(hideAug)->MapTypeSet(hideAug, mapType);

    if (ret != cAtOk)
        return ret;

    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    /* For Faceplate/Mate hiding XC modes only */
    return XcHidingApplyMapTypeOnSourceDestPairAug(self, mapType);
    }

static AtSdhChannel AugGet(AtSdhLine line, uint8 channelType, uint8 sts1)
    {
    switch (channelType)
        {
        case cAtSdhChannelTypeAug64:
            return (AtSdhChannel)AtSdhLineAug64Get(line, 0);

        case cAtSdhChannelTypeAug16:
            return (AtSdhChannel)AtSdhLineAug16Get(line, sts1 / 48);

        case cAtSdhChannelTypeAug4:
            return (AtSdhChannel)AtSdhLineAug4Get(line, sts1 / 12);

        case cAtSdhChannelTypeAug1:
            return (AtSdhChannel)AtSdhLineAug1Get(line, sts1 / 3);

        default:
            return NULL;
        }
    }

static uint8 StartSts1InTerminatedLineGet(Tha60290021SdhLineSideAug self)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet((AtSdhChannel)self);

    /* Even line has zero offset */
    if (AtChannelIdGet((AtChannel) line) % 2 == 0)
        return 0;

    switch (AtSdhLineRateGet(line))
        {
        case cAtSdhLineRateStm0:    return 24;
        case cAtSdhLineRateStm1:    return 24;
        case cAtSdhLineRateStm4:    return 24;
        case cAtSdhLineRateStm16:   return 0;
        case cAtSdhLineRateStm64:   return 0;
        case cAtSdhLineRateInvalid: return 0;
        default: return 0;
        }
    }

static AtSdhChannel HideChannelGet(AtSdhChannel self)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(self);
    uint8 channelType = AtSdhChannelTypeGet(self);
    uint8 sts1 = AtSdhChannelSts1Get(self);
    AtSdhLine terminatedLine;
    uint8 startSts1;

    if (!Tha60290021SdhChannelHaveHideChannel(self))
        return NULL;

    terminatedLine = AtSdhLineHideLineGet(line, sts1);

    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm64)
        {
        if (AtSdhLineRateGet(terminatedLine) == cAtSdhLineRateStm16)
            return AugGet(terminatedLine, channelType, (uint8)(sts1 % 48));
        else
            return AugGet(terminatedLine, channelType, sts1);
        }

    /* Line-side is STM-16/STM-4/STM-1/STM-0 */
    startSts1 = mMethodsGet(mThis(self))->StartSts1InTerminatedLineGet(mThis(self));
    return AugGet(terminatedLine, channelType, (uint8)(startSts1 + sts1));
    }

static void OverrideAtSdhChannel(AtSdhAug self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, HideChannelGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void MethodsInit(Tha60290021SdhLineSideAug self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, XcHidingLineSideSourcePairLine);
        mMethodOverride(m_methods, XcHidingLineSideSourcePairLocalLineId);
        mMethodOverride(m_methods, XcHidingLineSideDestPairLine);
        mMethodOverride(m_methods, XcHidingLineSideDestPairLocalLineId);
        mMethodOverride(m_methods, XcHidingShouldApply);
        mMethodOverride(m_methods, HideAug64Get);
        mMethodOverride(m_methods, StartSts1InTerminatedLineGet);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtSdhAug self)
    {
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SdhLineSideAug);
    }

AtSdhAug Tha60290021SdhLineSideAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SdhAugObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

eBool Tha60290021SdhLineSideAugXcHidingShouldApply(AtSdhChannel self, eTha60290021XcHideMode hideMode)
    {
    if (self)
        return mMethodsGet(mThis(self))->XcHidingShouldApply(mThis(self), hideMode);
    return cAtFalse;
    }

AtSdhChannel Tha60290021SdhLineSideAugXcHdingLineSideSourcePairAug(AtSdhChannel self)
    {
    if (self)
        return XcHidingLineSideSourcePairAug(self);
    return NULL;
    }

AtSdhChannel Tha60290021SdhLineSideAugXcHdingLineSideDestPairAug(AtSdhChannel self)
    {
    if (self)
        return XcHidingLineSideDestPairAug(self);
    return NULL;
    }

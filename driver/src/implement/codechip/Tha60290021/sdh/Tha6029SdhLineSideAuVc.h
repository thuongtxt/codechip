/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6029SdhLineSideAuVc.h
 * 
 * Created Date: Feb 14, 2017
 *
 * Description : Line side AU VC
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SDHLINESIDEAUVC_H_
#define _THA6029SDHLINESIDEAUVC_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SdhLineSideAuVc * Tha6029SdhLineSideAuVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhChannel Tha6029SdhLineSideAuVcXcHidingPairVc(AtSdhChannel self, AtSdhChannel pairAug);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SDHLINESIDEAUVC_H_ */


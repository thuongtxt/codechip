/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290021XcHiding.h
 * 
 * Created Date: Dec 25, 2016
 *
 * Description : XC hiding
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021XCHIDING_H_
#define _THA60290021XCHIDING_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModuleSdh.h"
#include "AtSdhChannel.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint8 Tha60290021XcHidingPairSts1Id(AtSdhChannel self);
AtSdhChannel Tha60290021XcHidingLineSideXcHidingPairChannel(AtSdhChannel self, AtSdhLine pairLine);
uint8 Tha60290021XcHidingPairTerminatedLineLocalId(AtModuleSdh self, AtSdhChannel channel);
AtSdhChannel Tha60290021XcHidingPairVcFromAug(AtSdhChannel self, AtSdhChannel pairAug);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021XCHIDING_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6029SdhFacePlateLineInternal.h
 * 
 * Created Date: Aug 6, 2017
 *
 * Description : Faceplate line
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SDHFACEPLATELINEINTERNAL_H_
#define _THA6029SDHFACEPLATELINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/sdh/Tha60210031SdhLineInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SdhFacePlateLine
    {
    tTha60210031SdhLine super;
    }tTha6029SdhFacePlateLine;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhLine Tha6029SdhFacePlateLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SDHFACEPLATELINEINTERNAL_H_ */


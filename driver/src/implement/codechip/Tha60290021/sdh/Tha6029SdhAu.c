/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhAu.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : PWCodechip-6029 AU.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtCrossConnect.h"
#include "../../../default/man/ThaDevice.h"
#include "../ocn/Tha60290021ModuleOcnReg.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "Tha6029SdhAuInternal.h"
#include "Tha60290021ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6029SdhAu)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6029SdhAuMethods m_methods;

/* Override */
static tAtObjectMethods                   m_AtObjectOverride;
static tAtChannelMethods 	              m_AtChannelOverride;
static tAtSdhChannelMethods               m_AtSdhChannelOverride;
static tTha60210011Tfi5LineAuMethods      m_Tha60210011Tfi5LineOverride;

/* Save super implementation */
static const tAtObjectMethods     	         *m_AtObjectMethods              = NULL;
static const tAtChannelMethods 		         *m_AtChannelMethods             = NULL;
static const tAtSdhChannelMethods            *m_AtSdhChannelMethods          = NULL;
static const tTha60210011Tfi5LineAuMethods   *m_Tha60210011Tfi5LineAuMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool ShouldGetPointerAlarmFromOcnModule(Tha6029SdhAu self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 Stschstkram_Base(Tha6029SdhAu self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 Stschstaram_Base(Tha6029SdhAu self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 DefectHw2Sw(uint32 hwDefects)
    {
    uint32 defects = 0;

    if (hwDefects & cAf6_tfi5upstschstaram_Tfi5StsPiStsAisCurStatus_Mask)
        defects |= cAtSdhPathAlarmAis;
    if (hwDefects & cAf6_tfi5upstschstaram_Tfi5StsPiStsLopCurStatus_Mask)
        defects |= cAtSdhPathAlarmLop;

    return defects;
    }

static uint32 DefectRead2ClearWithAddress(AtChannel self, eBool r2c, uint32 regAddr)
    {
    uint32 offset = Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)self);
    uint32 regVal;

    if (offset == cBit31_0)
        return 0;

    regAddr = regAddr + offset;
    regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    if (r2c)
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);

    return DefectHw2Sw(regVal);
    }

static uint32 DefectGet(AtChannel self)
    {
    if (mMethodsGet(mThis(self))->ShouldGetPointerAlarmFromOcnModule(mThis(self)))
        return DefectRead2ClearWithAddress(self, cAtFalse, mMethodsGet(mThis(self))->Stschstaram_Base(mThis(self)));

    return m_AtChannelMethods->DefectGet(self);
    }

static uint32 DefectHistoryRead2Clear(AtChannel self, eBool r2c)
    {
    return DefectRead2ClearWithAddress(self, r2c, mMethodsGet(mThis(self))->Stschstkram_Base(mThis(self)));
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (mMethodsGet(mThis(self))->ShouldGetPointerAlarmFromOcnModule(mThis(self)))
        return DefectHistoryRead2Clear(self, cAtFalse);

    return m_AtChannelMethods->DefectHistoryGet(self);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (mMethodsGet(mThis(self))->ShouldGetPointerAlarmFromOcnModule(mThis(self)))
        return DefectHistoryRead2Clear(self, cAtTrue);

    return m_AtChannelMethods->DefectHistoryClear(self);
    }

static AtCrossConnect CrossConnect(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModuleXc xcModule = (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    return AtModuleXcVcCrossConnectGet(xcModule);
    }

static eAtRet XcDisConnect(AtSdhChannel self)
    {
	eAtRet ret = cAtOk;
	AtChannel dest =  (AtChannel)self;
    AtCrossConnect xc = CrossConnect(self);
    AtChannel source = AtCrossConnectSourceChannelGet(xc, dest);
    if (source == NULL)
    	return cAtOk;


    ret |= AtCrossConnectChannelDisconnect(xc, source, dest);
    if (AtCrossConnectSourceChannelGet(xc, source) == dest)
        ret |= AtCrossConnectChannelDisconnect(xc, dest, source);

    return ret;
    }

static void WillDelete(AtObject self)
	{
    AtSdhChannel channel = (AtSdhChannel)self;
    if (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeAu4_nc)
        {
    	AtSdhChannel vc = AtSdhChannelSubChannelGet(channel, 0);
    	if (vc)
    		XcDisConnect(vc);
        }
	}

static eBool CanConcate(Tha60210011Tfi5LineAu self, AtSdhChannel *slaveList, uint8 numSlaves)
    {
    AtSdhLine line;
    uint8 masterSlice48Id;
    uint8 slave_i;

    /* Call Supper to check different line Id */
    if (!m_Tha60210011Tfi5LineAuMethods->CanConcate(self, slaveList, numSlaves))
        return cAtFalse;

    /* With STM16/STM4, super logic already handles */
    line = AtSdhChannelLineObjectGet((AtSdhChannel) self);
    if (AtSdhLineRateGet(line) != cAtSdhLineRateStm64)
        return cAtTrue;

    /* Check base on Slice48Id */
    masterSlice48Id = (uint8)(AtSdhChannelSts1Get((AtSdhChannel)self) / 48);
    for (slave_i = 0; slave_i < numSlaves; slave_i++)
        {
        uint8 slaverSlice48Id = (uint8)(AtSdhChannelSts1Get(slaveList[slave_i]) / 48);
        if (slaverSlice48Id != masterSlice48Id)
            return cAtFalse;
        }

    return cAtTrue;
    }

static eAtRet Concate(AtSdhChannel self, AtSdhChannel *slaveList, uint8 numSlaves)
	{
	eAtRet ret = cAtOk;
	uint32 channel_i;
	AtSdhChannel slaves[192];
    AtSdhChannel master;

    if (!Tha60290021SdhChannelHaveHideChannel(self))
        return m_AtSdhChannelMethods->Concate(self, slaveList, numSlaves);

    master = AtSdhChannelHideChannelGet(self);
    for (channel_i = 0; channel_i < numSlaves; channel_i++)
        slaves[channel_i] = AtSdhChannelHideChannelGet(slaveList[channel_i]);

    if (master)
        ret |= m_AtSdhChannelMethods->Concate(master, slaves, numSlaves);

    ret |= m_AtSdhChannelMethods->Concate(self, slaveList, numSlaves);
    return ret;
    }

static eAtRet Deconcate(AtSdhChannel self)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel hideAu;

    if (!Tha60290021SdhChannelHaveHideChannel(self))
        return m_AtSdhChannelMethods->Deconcate(self);

    hideAu = AtSdhChannelHideChannelGet(self);
    if (hideAu)
        ret |= m_AtSdhChannelMethods->Deconcate(hideAu);

    ret |= m_AtSdhChannelMethods->Deconcate(self);
    return ret;
    }

static eBool IsTerminatedAu(AtSdhChannel self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    return Tha60290021ModuleSdhLineIsTerminated(sdhModule, AtSdhChannelLineGet(self));
    }

static eAtRet HideXcModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    AtSdhChannel hideAu;

    if (IsTerminatedAu(self))
        return cAtOk;

    hideAu = AtSdhChannelHideChannelGet(self);
    if (hideAu)
        return AtSdhChannelModeSet(hideAu, mode);

    return cAtOk;
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    eAtRet ret = m_AtSdhChannelMethods->ModeSet(self, mode);
    if (ret != cAtOk)
        return ret;

    return HideXcModeSet(self, mode);
    }

static void OverrideAtObject(AtSdhAu self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        mMethodOverride(m_AtObjectOverride, WillDelete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhAu self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhAu self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(sdhChannel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, Concate);
        mMethodOverride(m_AtSdhChannelOverride, Deconcate);
        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        }

    mMethodsSet(sdhChannel, &m_AtSdhChannelOverride);
    }

static void OverrideTha60210011Tfi5LineAu(AtSdhAu self)
    {
    Tha60210011Tfi5LineAu tfi5LineAu = (Tha60210011Tfi5LineAu)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011Tfi5LineAuMethods = mMethodsGet(tfi5LineAu);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011Tfi5LineOverride, m_Tha60210011Tfi5LineAuMethods, sizeof(m_Tha60210011Tfi5LineOverride));

        mMethodOverride(m_Tha60210011Tfi5LineOverride, CanConcate);
        }

    mMethodsSet(tfi5LineAu, &m_Tha60210011Tfi5LineOverride);
    }

static void MethodsInit(AtSdhAu self)
    {
    Tha6029SdhAu au = (Tha6029SdhAu)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ShouldGetPointerAlarmFromOcnModule);
        mMethodOverride(m_methods, Stschstkram_Base);
        mMethodOverride(m_methods, Stschstaram_Base);
        }

    mMethodsSet(au, &m_methods);
    }

static void Override(AtSdhAu self)
    {
	OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideTha60210011Tfi5LineAu(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhAu);
    }

AtSdhAu Tha6029SdhAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5LineAuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhAuVc.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : PWCodechip-6029 AU VC.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/xc/AtCrossConnectInternal.h"
#include "../../../default/aps/ThaModuleHardAps.h"
#include "../../../default/pmc/ThaModulePmc.h"
#include "../xc/Tha60290021ModuleXc.h"
#include "Tha6029SdhAuVcInternal.h"
#include "Tha60290021ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6029SdhAuVc)self)
#define mHasPohProcessor(self) AtSdhPathHasPohProcessor((AtSdhPath)self)
#define mHasPointerProcessor(self) AtSdhPathHasPointerProcessor((AtSdhPath)self)

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                m_AtObjectOverride;
static tAtChannelMethods               m_AtChannelOverride;
static tAtSdhChannelMethods            m_AtSdhChannelOverride;
static tAtSdhPathMethods               m_AtSdhPathOverride;
static tThaSdhVcMethods                m_ThaSdhVcOverride;
static tThaSdhAuVcMethods              m_ThaSdhAuVcOverride;

/* Save super implementation */
static const tAtObjectMethods                *m_AtObjectMethods                = NULL;
static const tAtChannelMethods               *m_AtChannelMethods               = NULL;
static const tAtSdhPathMethods               *m_AtSdhPathMethods               = NULL;
static const tAtSdhChannelMethods            *m_AtSdhChannelMethods            = NULL;
static const tThaSdhAuVcMethods              *m_ThaSdhAuVcMethods              = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static tAtSdhPathApsInfo* ApsInfoGet(AtSdhPath self)
    {
    return &(mThis(self)->apsInfo);
    }

static eAtRet LomMonitorEnable(ThaSdhAuVc self, eBool enable)
    {
    if (mHasPointerProcessor(self))
        return m_ThaSdhAuVcMethods->LomMonitorEnable(self, enable);
    return cAtOk;
    }

static eBool LomMonitorIsEnabled(ThaSdhAuVc self)
    {
    if (mHasPointerProcessor(self))
        return m_ThaSdhAuVcMethods->LomMonitorIsEnabled(self);
    return cAtFalse;
    }

static uint32 NumSourceVcs(ThaSdhVc self)
    {
    AtUnused(self);
    return 2;
    }

static ThaSdhVc *SourceVcsCreate(ThaSdhVc self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 numVcs = ThaSdhVcNumSourceVcs(self);
    ThaSdhVc *vcs = NULL;

    if (numVcs)
        vcs = mMethodsGet(osal)->MemAlloc(osal, sizeof(ThaSdhVc) * numVcs);

    if (vcs != NULL)
        mMethodsGet(osal)->MemInit(osal, vcs, 0, sizeof(ThaSdhVc) * numVcs);

    return vcs;
    }

static void SourceVcsDelete(Tha6029SdhAuVc self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    if (self->sourceVcs)
        mMethodsGet(osal)->MemFree(osal, self->sourceVcs);
    self->sourceVcs = NULL;
    }

static void SourceVcSet(ThaSdhVc self, uint32 vcIndex, ThaSdhVc sourceVc)
    {
    if (mThis(self)->sourceVcs == NULL)
        mThis(self)->sourceVcs = SourceVcsCreate(self);

    if (mThis(self)->sourceVcs)
        mThis(self)->sourceVcs[vcIndex] = sourceVc;
    }

static ThaSdhVc SourceVcGet(ThaSdhVc self, uint32 vcIndex)
    {
    if (mThis(self)->sourceVcs)
        return mThis(self)->sourceVcs[vcIndex];
    return NULL;
    }

static AtList DestVcs(ThaSdhVc self)
    {
    return mThis(self)->destVcs;
    }

static eAtModuleSdhRet TxPslSet(AtSdhPath self, uint8 psl)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhPathMethods->TxPslSet(self, psl);

    return cAtErrorNotApplicable;
    }

static eAtModuleSdhRet ExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhPathMethods->ExpectedPslSet(self, psl);

    return cAtErrorNotApplicable;
    }

static uint8 TxPslGet(AtSdhPath self)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhPathMethods->TxPslGet(self);
    return 0;
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhPathMethods->RxPslGet(self);
    return 0;
    }

static uint8 ExpectedPslGet(AtSdhPath self)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhPathMethods->ExpectedPslGet(self);
    return 0;
    }

static eAtModuleSdhRet TxSsSet(AtSdhPath self, uint8 value)
    {
    if (mHasPointerProcessor(self))
        return m_AtSdhPathMethods->TxSsSet(self, value);
    return cAtErrorNotApplicable;
    }

static eAtModuleSdhRet ExpectedSsSet(AtSdhPath self, uint8 value)
    {
    if (mHasPointerProcessor(self))
        return m_AtSdhPathMethods->ExpectedSsSet(self, value);
    return cAtErrorNotApplicable;
    }

static eAtModuleSdhRet SsCompareEnable(AtSdhPath self, eBool enable)
    {
    if (mHasPointerProcessor(self))
        return m_AtSdhPathMethods->SsCompareEnable(self, enable);
    return cAtErrorNotApplicable;
    }

static uint8 TxSsGet(AtSdhPath self)
    {
    if (mHasPointerProcessor(self))
        return m_AtSdhPathMethods->TxSsGet(self);
    return 0;
    }

static uint8 ExpectedSsGet(AtSdhPath self)
    {
    if (mHasPointerProcessor(self))
        return m_AtSdhPathMethods->ExpectedSsGet(self);
    return 0;
    }

static eBool SsCompareIsEnabled(AtSdhPath self)
    {
    if (mHasPointerProcessor(self))
        return m_AtSdhPathMethods->SsCompareIsEnabled(self);
    return cAtFalse;
    }

static eAtRet SsInsertionEnable(AtSdhPath self, eBool enable)
    {
    if (mHasPointerProcessor(self))
        return m_AtSdhPathMethods->SsInsertionEnable(self, enable);
    return cAtErrorNotApplicable;
    }

static eBool SsInsertionIsEnabled(AtSdhPath self)
    {
    if (mHasPointerProcessor(self))
        return m_AtSdhPathMethods->SsInsertionIsEnabled(self);
    return cAtFalse;
    }

static eAtModuleSdhRet ERdiEnable(AtSdhPath self, eBool enable)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhPathMethods->ERdiEnable(self, enable);
    return cAtErrorNotApplicable;
    }

static eAtModuleSdhRet PlmMonitorEnable(AtSdhPath self, eBool enable)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhPathMethods->PlmMonitorEnable(self, enable);
    return cAtErrorNotApplicable;
    }

static eBool ERdiIsEnabled(AtSdhPath self)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhPathMethods->ERdiIsEnabled(self);
    return cAtFalse;
    }

static eBool PlmMonitorIsEnabled(AtSdhPath self)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhPathMethods->PlmMonitorIsEnabled(self);
    return cAtFalse;
    }

static eAtRet PohMonitorEnable(AtSdhPath self, eBool enabled)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhPathMethods->PohMonitorEnable(self, enabled);
    return cAtErrorNotApplicable;
    }

static eBool PohMonitorIsEnabled(AtSdhPath self)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhPathMethods->PohMonitorIsEnabled(self);
    return cAtFalse;
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->TxTtiSet(self, tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->TxTtiGet(self, tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet RxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->RxTtiGet(self, tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet ExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->ExpectedTtiGet(self, tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet ExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->ExpectedTtiSet(self, tti);
    return cAtErrorModeNotSupport;
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->AlarmAffectingEnable(self, alarmType, enable);

    return cAtErrorNotApplicable;
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    if (mHasPohProcessor(self) || mHasPointerProcessor(self))
        return m_AtSdhChannelMethods->AlarmAffectingIsEnabled(self, alarmType);
    return cAtFalse;
    }

static eAtRet AutoRdiEnable(AtSdhChannel self, eBool enable)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->AutoRdiEnable(self, enable);
    return cAtErrorNotApplicable;
    }

static eBool AutoRdiIsEnabled(AtSdhChannel self)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->AutoRdiIsEnabled(self);
    return cAtFalse;
    }

static eBool CanChangeAlarmAffecting(AtSdhChannel self, uint32 alarmType)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->CanChangeAlarmAffecting(self, alarmType);
    return cAtFalse;
    }

static AtBerController BerControllerCreate(AtSdhChannel self)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->BerControllerCreate(self);
    return NULL;
    }

static eAtRet InterruptAndDefectGet(AtSdhChannel self, uint32 *changedDefects, uint32 *currentStatus, eBool read2Clear)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->InterruptAndDefectGet(self, changedDefects, currentStatus, read2Clear);
    return 0;
    }

static eAtRet InterruptAndAlarmGet(AtSdhChannel self, uint32 *changedAlarms, uint32 *currentStatus, eBool read2Clear)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->InterruptAndAlarmGet(self, changedAlarms, currentStatus, read2Clear);
    return 0;
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->TimMonitorEnable(self, enable);
    return cAtErrorNotApplicable;
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    if (mHasPohProcessor(self))
        return m_AtSdhChannelMethods->TimMonitorIsEnabled(self);
    return cAtFalse;
    }

static ThaModuleHardAps ApsModule(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleHardAps)AtDeviceModuleGet(device, cAtModuleAps);
    }

static eBool HoldOffTimerSupported(AtSdhChannel self)
    {
    return AtSdhPathUpsrIsApplicable((AtSdhPath)self);
    }

static eBool HoldOnTimerSupported(AtSdhChannel self)
    {
    return AtSdhPathUpsrIsApplicable((AtSdhPath)self);
    }

static eAtRet HoldOffTimerSet(AtSdhChannel self, uint32 timerInMs)
    {
    if (AtSdhPathUpsrIsApplicable((AtSdhPath)self))
        {
        ThaModuleHardAps apsModule = ApsModule(self);
        return ThaModuleHardApsPathHoldOffTimerSet(apsModule, (AtSdhPath)self, timerInMs);
        }

    return cAtErrorNotApplicable;
    }

static uint32 HoldOffTimerGet(AtSdhChannel self)
    {
    if (AtSdhPathUpsrIsApplicable((AtSdhPath)self))
        {
        ThaModuleHardAps apsModule = ApsModule(self);
        return ThaModuleHardApsPathHoldOffTimerGet(apsModule, (AtSdhPath)self);
        }

    return 0;
    }

static eAtRet HoldOnTimerSet(AtSdhChannel self, uint32 timerInMs)
    {
    if (AtSdhPathUpsrIsApplicable((AtSdhPath)self))
        {
        ThaModuleHardAps apsModule = ApsModule(self);
        return ThaModuleHardApsPathHoldOnTimerSet(apsModule, (AtSdhPath)self, timerInMs);
        }

    return cAtErrorNotApplicable;
    }

static uint32 HoldOnTimerGet(AtSdhChannel self)
    {
    if (AtSdhPathUpsrIsApplicable((AtSdhPath)self))
        {
        ThaModuleHardAps apsModule = ApsModule(self);
        return ThaModuleHardApsPathHoldOnTimerGet(apsModule, (AtSdhPath)self);
        }

    return 0;
    }

static eAtModuleSdhRet ApsSwitchingConditionSet(AtSdhPath self, uint32 defects, eAtApsSwitchingCondition condition)
    {
    if (AtSdhPathUpsrIsApplicable(self))
        return ThaModuleHardApsPathSwitchingConditionSet(ApsModule((AtSdhChannel)self), self, defects, condition);
    return cAtErrorNotApplicable;
    }

static eAtApsSwitchingCondition ApsSwitchingConditionGet(AtSdhPath self, uint32 defect)
    {
    if (AtSdhPathUpsrIsApplicable(self))
        return ThaModuleHardApsPathSwitchingConditionGet(ApsModule((AtSdhChannel)self), self, defect);
    return cAtApsSwitchingConditionNone;
    }

static eAtModuleSdhRet ApsForcedSwitchingConditionSet(AtSdhPath self, eAtApsSwitchingCondition condition)
    {
    if (AtSdhPathUpsrIsApplicable(self))
        return ThaModuleHardApsPathForcedSwitchingConditionSet(ApsModule((AtSdhChannel)self), self, condition);
    return cAtErrorNotApplicable;
    }

static eAtApsSwitchingCondition ApsForcedSwitchingConditionGet(AtSdhPath self)
    {
    if (AtSdhPathUpsrIsApplicable(self))
        return ThaModuleHardApsPathForcedSwitchingConditionGet(ApsModule((AtSdhChannel)self), self);
    return cAtApsSwitchingConditionInvalid;
    }

static AtChannel SourceVc(AtChannel self)
    {
    const uint8 cSourceVcIndexByDefaultXc = 0;
    return (AtChannel)ThaSdhVcSourceVcGet((ThaSdhVc)self, cSourceVcIndexByDefaultXc);
    }

static AtChannel BackupSourceVcGet(AtChannel self)
    {
    return mThis(self)->backupSourceVc;
    }

static void BackupSourceVcSet(AtChannel self, AtChannel backupSourceVc)
    {
    mThis(self)->backupSourceVc = backupSourceVc;
    }

static eAtRet XcReleaseLoopback(AtChannel self)
    {
    AtCrossConnect xc = mMethodsGet(self)->XcEngine(self);
    AtChannel bksource = BackupSourceVcGet(self);
    if (bksource)
        {
        BackupSourceVcSet(self, NULL);
        return AtCrossConnectHwConnect(xc, bksource, self);
        }

    return AtCrossConnectHwDisconnect(xc, self);
    }

static eAtRet XcLoopback(AtChannel self)
    {
    AtCrossConnect xc = mMethodsGet(self)->XcEngine(self);
    BackupSourceVcSet(self, SourceVc(self));
    return AtCrossConnectHwConnect(xc, self, self);
    }

static eAtRet XcLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    if ((loopbackMode == cAtLoopbackModeLocal) ||
        (loopbackMode == cAtLoopbackModeRemote))
        return XcLoopback(self);

    if (loopbackMode == cAtLoopbackModeRelease)
        return XcReleaseLoopback(self);

    return cAtErrorModeNotSupport;
    }

static eBool XcLoopbackModeIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    if (loopbackMode == cAtLoopbackModeRelease)
        return cAtTrue;

    if (loopbackMode == cAtLoopbackModeDual)
        return cAtFalse;

    /* Other loopback should let subclass to determine */
    return cAtFalse;
    }

static AtModule PmcModuleGet(AtChannel self)
	{
	return AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
	}

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (!ThaSdhChannelShouldGetCounterFromPmc(self))
        return m_AtChannelMethods->CounterGet(self, counterType);

    if (AtSdhPathHasPointerProcessor((AtSdhPath)self) && AtSdhPathCounterIsPointerAdjust(counterType))
        return ThaModulePmcSdhAuPointerCounterGet(PmcModuleGet(self), (AtSdhChannel)self, counterType, cAtFalse);

    return ThaModulePmcSdhAuVcPohCounterGet(PmcModuleGet(self), (AtSdhChannel)self, counterType, cAtFalse);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (!ThaSdhChannelShouldGetCounterFromPmc(self))
        return m_AtChannelMethods->CounterClear(self, counterType);

    if (AtSdhPathHasPointerProcessor((AtSdhPath)self) && AtSdhPathCounterIsPointerAdjust(counterType))
        return ThaModulePmcSdhAuPointerCounterGet(PmcModuleGet(self), (AtSdhChannel)self, counterType, cAtTrue);

    return ThaModulePmcSdhAuVcPohCounterGet(PmcModuleGet(self), (AtSdhChannel)self, counterType, cAtTrue);
    }

static void BoolAttributePrint(const char* attribute, eBool value)
    {
    AtPrintc(cSevInfo, "* %s\t: %s\r\n", attribute, value ? "Enabled" : "Disabled");
    }

static eAtRet Debug(AtChannel self)
    {
    m_AtChannelMethods->Debug(self);

    AtPrintc(cSevNormal, "\r\n");
    BoolAttributePrint("LOM Detection", ThaSdhAuVcLomMonitorIsEnabled((ThaSdhAuVc)self));
    BoolAttributePrint("POH Insertion", AtSdhPathPohInsertionIsEnabled((AtSdhPath)self));

    return cAtOk;
    }

static void SerializeApsInfo(AtObject self, AtCoder encoder)
    {
    tAtSdhPathApsInfo *object = (tAtSdhPathApsInfo *)self;

    mEncodeObjectDescription(apsEngine);
    mEncodeObjectDescriptionList(selectors);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha6029SdhAuVc object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    AtCoderEncodeObjectWithHandler(encoder, (AtObject)&(object->apsInfo), "apsInfo", SerializeApsInfo);

    /* This field is lazy created */
    if (mThis(self)->sourceVcs == NULL)
        mThis(self)->sourceVcs = SourceVcsCreate((ThaSdhVc)self);
    mEncodeObjectDescriptionArray(sourceVcs, ThaSdhVcNumSourceVcs((ThaSdhVc)self));

    mEncodeObjectDescriptionList(destVcs);
    mEncodeObjectDescription(backupSourceVc);
    }

static void Delete(AtObject self)
    {
    SourceVcsDelete(mThis(self));
    AtObjectDelete((AtObject)mThis(self)->destVcs);
    mThis(self)->destVcs = NULL;

    mThis(self)->backupSourceVc = NULL;
    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static AtModuleXc XcModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static AtCrossConnect Xc(AtSdhChannel self)
    {
    return AtModuleXcVcCrossConnectGet(XcModule((AtChannel)self));
    }

static eAtRet AllServicesDestroy(AtChannel self)
    {
    AtCrossConnect xc = Xc((AtSdhChannel)self);
    eAtRet ret = cAtOk;

    ret |= AtCrossConnectVcDisconnectAllConnections(xc, (AtSdhChannel)self);
    ret |= m_AtChannelMethods->AllServicesDestroy(self);

    return ret;
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, XcLoopbackSet);
        mMethodOverride(m_AtChannelOverride, XcLoopbackModeIsSupported);
        mMethodOverride(m_AtChannelOverride, CounterGet);
		mMethodOverride(m_AtChannelOverride, CounterClear);
		mMethodOverride(m_AtChannelOverride, AllServicesDestroy);
        mMethodOverride(m_AtChannelOverride, Debug);
        mMethodOverride(m_AtChannelOverride, AllServicesDestroy);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhPath(AtSdhVc self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhPathMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, m_AtSdhPathMethods, sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, TxPslSet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslGet);
        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslGet);
        mMethodOverride(m_AtSdhPathOverride, TxSsSet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedSsSet);
        mMethodOverride(m_AtSdhPathOverride, SsCompareEnable);
        mMethodOverride(m_AtSdhPathOverride, TxSsGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedSsGet);
        mMethodOverride(m_AtSdhPathOverride, SsCompareIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, SsInsertionEnable);
        mMethodOverride(m_AtSdhPathOverride, SsInsertionIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, ERdiEnable);
        mMethodOverride(m_AtSdhPathOverride, PlmMonitorEnable);
        mMethodOverride(m_AtSdhPathOverride, ERdiIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, PlmMonitorIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorEnable);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, ApsInfoGet);
        mMethodOverride(m_AtSdhPathOverride, ApsSwitchingConditionSet);
        mMethodOverride(m_AtSdhPathOverride, ApsSwitchingConditionGet);
        mMethodOverride(m_AtSdhPathOverride, ApsForcedSwitchingConditionSet);
        mMethodOverride(m_AtSdhPathOverride, ApsForcedSwitchingConditionGet);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc vc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, mMethodsGet(vc), sizeof(m_ThaSdhVcOverride));

        mMethodOverride(m_ThaSdhVcOverride, NumSourceVcs);
        mMethodOverride(m_ThaSdhVcOverride, SourceVcSet);
        mMethodOverride(m_ThaSdhVcOverride, SourceVcGet);
        mMethodOverride(m_ThaSdhVcOverride, DestVcs);
        }

    mMethodsSet(vc, &m_ThaSdhVcOverride);
    }

static void OverrideThaSdhAuVc(AtSdhVc self)
    {
    ThaSdhAuVc vc = (ThaSdhAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhAuVcMethods = mMethodsGet(vc);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhAuVcOverride, mMethodsGet(vc), sizeof(m_ThaSdhAuVcOverride));

        mMethodOverride(m_ThaSdhAuVcOverride, LomMonitorEnable);
        mMethodOverride(m_ThaSdhAuVcOverride, LomMonitorIsEnabled);
        }

    mMethodsSet(vc, &m_ThaSdhAuVcOverride);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, TxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, RxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AutoRdiEnable);
        mMethodOverride(m_AtSdhChannelOverride, AutoRdiIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeAlarmAffecting);
        mMethodOverride(m_AtSdhChannelOverride, BerControllerCreate);
        mMethodOverride(m_AtSdhChannelOverride, InterruptAndDefectGet);
        mMethodOverride(m_AtSdhChannelOverride, InterruptAndAlarmGet);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerSet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerGet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOnTimerSet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOnTimerGet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerSupported);
        mMethodOverride(m_AtSdhChannelOverride, HoldOnTimerSupported);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    OverrideThaSdhVc(self);
    OverrideThaSdhAuVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhAuVc);
    }

AtSdhVc Tha6029SdhAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051Tfi5LineAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->destVcs = AtListCreate(0);

    return self;
    }


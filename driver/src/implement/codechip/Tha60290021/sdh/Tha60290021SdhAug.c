/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : AUG
 *
 * File        : Tha60290021SdhAug.c
 *
 * Created Date: Aug 8, 2016
 *
 * Description : AUG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../ocn/Tha60290021ModuleOcn.h"
#include "Tha60290021ModuleSdh.h"
#include "Tha60290021SdhAugInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static Tha60290021ModuleOcn ModuleOcn(AtSdhChannel self)
    {
    return (Tha60290021ModuleOcn)ThaModuleOcnFromChannel((AtChannel)self);
    }

static eBool IsTerminatedChannel(AtSdhChannel self)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    return Tha60290021ModuleSdhLineIsTerminated(sdhModule, AtSdhChannelLineGet(self));
    }

static eBool Aug64MapTypeIsSupported(AtSdhChannel self, uint8 mapType)
    {
    if (mapType == cAtSdhAugMapTypeAug64MapVc4_64c)
        return Tha60290021ModuleOcnSts192cIsSupported(ModuleOcn(self));

    /* Terminated STM-64 lines only supports VC4-64c mapping. */
    if (IsTerminatedChannel(self))
        return cAtFalse;

    return m_AtSdhChannelMethods->MapTypeIsSupported(self, mapType);
    }

static eBool MapTypeIsSupported(AtSdhChannel self, uint8 mapType)
    {
    if (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeAug64)
        return Aug64MapTypeIsSupported(self, mapType);

    return m_AtSdhChannelMethods->MapTypeIsSupported(self, mapType);
    }

static eAtModuleSdhRet CanChangeMapping(AtSdhChannel self, uint8 mapType)
    {
    if (mapType == AtSdhChannelMapTypeGet(self))
        return cAtOk;

    /* Do not allow to change AUG mapping type if any belonged VC is being crossed. */
    if (AtSdhChannelHasCrossConnect(self))
        return cAtErrorChannelBusy;

    return cAtOk;
    }

static void OverrideAtSdhChannel(AtSdhAug self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, MapTypeIsSupported);
        mMethodOverride(m_AtSdhChannelOverride, CanChangeMapping);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhAug self)
    {
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SdhAug);
    }

AtSdhAug Tha60290021SdhAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5LineAugObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAug Tha60290021SdhAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAug newAug = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newAug == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021SdhAugObjectInit(newAug, channelId, channelType, module);
    }

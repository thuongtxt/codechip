/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290021ModuleSdh.c
 *
 * Created Date: Jul 8, 2016
 *
 * Description : PWCodechip-60290021 SDH module.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../Tha60210011/poh/Tha60210011ModulePoh.h"
#include "../encap/Tha60290021HdlcChannel.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "../ocn/Tha60290021ModuleOcnReg.h"
#include "../poh/Tha60290021PohReg.h"
#include "../man/Tha60290021Device.h"
#include "../common/Tha602900xxCommon.h"
#include "../prbs/Tha60290021ModulePrbs.h"
#include "../xc/Tha60290021ModuleXc.h"
#include "Tha60290021ModuleSdhInternal.h"
#include "Tha6029SdhLineInternal.h"
#include "Tha6029SdhAuInternal.h"
#include "Tha6029SdhAuVcInternal.h"
#include "Tha60290021XcHiding.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumLinesPerGroup      8
#define cIteratorMask(_idx)    (cBit0 << (_idx))

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290021ModuleSdh)self)
#define mDbRateGet(self, lineId) (Tha60290021ModuleSdhFaceplateLineRateGet(self, (uint8) lineId))

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290021ModuleSdhMethods m_methods;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleSdhMethods          m_AtModuleSdhOverride;
static tThaModuleSdhMethods         m_ThaModuleSdhOverride;
static tTha60210011ModuleSdhMethods m_Tha60210011ModuleSdhOverride;

/* Save super implementation */
static const tAtObjectMethods             *m_AtObjectMethods             = NULL;
static const tAtModuleSdhMethods          *m_AtModuleSdhMethods          = NULL;
static const  tThaModuleSdhMethods        *m_ThaModuleSdhMethods         = NULL;
static const tTha60210011ModuleSdhMethods *m_Tha60210011ModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool UseNewStsIdConvert(Tha60290021ModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule) self);
    ThaVersionReader versionReader = ThaDeviceVersionReader(device);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32 startVersion = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x1, 0x1, 0);
    if (hwVersion >= startVersion)
        return cAtTrue;

    return cAtFalse;
    }

static uint8 FaceplateLineStartId(AtModuleSdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 FaceplateLineStopId(AtModuleSdh self)
    {
    AtUnused(self);
    return 15;
    }

static uint8 FaceplateLineIdFirstStm64(AtModuleSdh self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 FaceplateLineIdSecondStm64(AtModuleSdh self)
    {
    AtUnused(self);
    return 8;
    }

static uint8 MateLineStartId(AtModuleSdh self)
    {
    AtUnused(self);
    return 16;
    }

static uint8 MateLineStopId(AtModuleSdh self)
    {
    AtUnused(self);
    return 23;
    }

static uint8 TerminatedLineStartId(AtModuleSdh self)
    {
    AtUnused(self);
    return 24;
    }

static uint8 TerminatedLineStopId(AtModuleSdh self)
    {
    AtUnused(self);
    return 31;
    }

static eBool IsFaceplateLine(AtModuleSdh self, uint8 lineId)
    {
    return mInRange(lineId, FaceplateLineStartId(self), FaceplateLineStopId(self));
    }

static eBool IsMateLine(AtModuleSdh self, uint8 lineId)
    {
    return mInRange(lineId, MateLineStartId(self), MateLineStopId(self));
    }

static eBool IsTerminatedLine(AtModuleSdh self, uint8 lineId)
    {
    return mInRange(lineId, TerminatedLineStartId(self), TerminatedLineStopId(self));
    }

static eBool IsFaceplateLineSupportStm64(AtModuleSdh self, uint8 lineId)
    {
    AtUnused(self);
    return ((lineId == FaceplateLineIdFirstStm64(self)) || (lineId == FaceplateLineIdSecondStm64(self)));
    }

static eBool IsFaceLineIdsFirstPartUnused(AtModuleSdh self, uint8 lineId)
    {
    if (lineId >= FaceplateLineIdSecondStm64(self))
        return cAtFalse;

    /* Configure line rate of line 0 to OC–192/STM–64 will make line 1 to 7 be unused */
    if (mDbRateGet(self, FaceplateLineIdFirstStm64(self)) == cAtSdhLineRateStm64)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsFaceLineIdsSecondPartUnused(AtModuleSdh self, uint8 lineId)
    {
    if (lineId <= FaceplateLineIdSecondStm64(self))
        return cAtFalse;

    /* Configure line rate of line 8 to OC–192/STM–64 will make line 9 to 15 be unused */
    if (mDbRateGet(self, FaceplateLineIdSecondStm64(self)) == cAtSdhLineRateStm64)
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsFaceLineOddIdsUnused(AtModuleSdh self, uint8 lineId)
    {
    if ((lineId % 2) == 0)
        return cAtFalse;

    /* Configure line rate of line 0,2,..,14 to OC–48/STM–16 will make line 1,3,..,15 be unused */
    if (mDbRateGet(self, (lineId - 1)) == cAtSdhLineRateStm16)
        return cAtTrue;

    return cAtFalse;
    }

static eBool FacePlateLineIsUsed(Tha60290021ModuleSdh self, uint8 lineId)
    {
    if (IsFaceplateLineSupportStm64((AtModuleSdh)self, lineId))
        return cAtTrue;

    if (IsFaceLineIdsFirstPartUnused((AtModuleSdh)self, lineId))
        return cAtFalse;

    if (IsFaceLineIdsSecondPartUnused((AtModuleSdh)self, lineId))
        return cAtFalse;

    if (IsFaceLineOddIdsUnused((AtModuleSdh)self, lineId))
        return cAtFalse;

    return cAtTrue;
    }

static eBool LineExist(AtModuleSdh self, uint8 lineId)
    {
    uint8 localLineId = ThaModuleSdhLineIdLocalId((ThaModuleSdh)self, lineId);

    if (IsMateLine(self, lineId))
        return (localLineId < mMethodsGet(mThis(self))->NumUseableMateLines(mThis(self))) ? cAtTrue : cAtFalse;

    if (IsTerminatedLine(self, lineId))
        return (localLineId < mMethodsGet(mThis(self))->NumUseableTerminatedLines(mThis(self))) ? cAtTrue : cAtFalse;

    if (!IsFaceplateLine(self, lineId))
        return cAtFalse;

    if (localLineId >= mMethodsGet(mThis(self))->NumUseableFaceplateLines(mThis(self)))
        return cAtFalse;

    return mMethodsGet(mThis(self))->FacePlateLineIsUsed(mThis(self), lineId);
    }

static uint8 MaxLinesGet(AtModuleSdh self)
    {
    return (uint8)(Tha60290021ModuleSdhNumFaceplateLines(self) +
                   Tha60290021ModuleSdhNumMateLines(self) +
                   Tha60290021ModuleSdhNumTerminatedLines(self));
    }

static eBool IsLineSupportStm16(AtModuleSdh self, uint8 lineId)
    {
    AtUnused(self);

    if (IsMateLine(self, lineId))
        return cAtTrue;

    if (IsTerminatedLine(self, lineId))
        return cAtTrue;

    if (IsFaceplateLine(self, lineId) && ((lineId % 2) == 0))
        return cAtTrue;

    return cAtFalse;
    }

static eBool ShouldNotTouchRateOfOtherLines(AtModuleSdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceTestbenchIsEnabled(device) && AtDeviceDiagnosticModeIsEnabled(device))
        return cAtTrue;
    return cAtFalse;
    }

static eAtSdhLineRate FaceplateLineRateGet(AtModuleSdh self, uint8 lineId)
    {
    if (lineId < cThaNumFacePlateLine)
        return mThis(self)->rate[lineId];
    return cAtSdhLineRateInvalid;
    }

static eAtRet FaceplateLineRateSet(AtModuleSdh self, uint8 lineId, eAtSdhLineRate rate)
    {
    if (lineId >= cThaNumFacePlateLine)
        return cAtErrorInvlParm;

    mThis(self)->rate[lineId] = rate;
    return cAtOk;
    }

static eAtRet FaceplateLineRateWillChangeToStm64(AtModuleSdh self, uint8 lineId)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    eAtRet ret = cAtOk;
    uint8 line_i;

    /* If STM-64 is applied, all of other ports must be disabled */
    for (line_i = 1; line_i < cNumLinesPerGroup; line_i++)
        {
        uint8 unusedLineId = (uint8)(lineId + line_i);
        ret |= Tha60290021ModuleOcnFaceplateLineEnable(ocnModule, unusedLineId, cAtFalse);
        ret |= AtModuleSdhLineUnuse(self, unusedLineId);
        }

    if (ret == cAtOk)
        return cAtOk;

    AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                "Set line rate to STM-64 fail with ret = %s\r\n",
                AtRet2String(ret));

    return ret;
    }

static eAtSdhChannelMode DefaultLineMode(void)
    {
    return cAtSdhChannelModeSdh;
    }

static eAtRet FaceplateLineRateWillChangeToStm16(AtModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    eAtRet ret = cAtOk;
    uint32 nextLineId;

    AtUnused(lineId);
    AtUnused(oldRate);

    /* If STM-16 is applied, the next line must be disabled */
    nextLineId = lineId + 1UL;
    ret |= Tha60290021ModuleOcnFaceplateLineEnable(ocnModule, nextLineId, cAtFalse);
    ret |= AtModuleSdhLineUnuse(self, (uint8)nextLineId);
    if (ret == cAtOk)
        {
        mThis(self)->rate[nextLineId] = cAtSdhLineRateInvalid;
    	return ret;
        }


    AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                "Set line rate to STM-64 fail with ret = %s\r\n",
                AtRet2String(ret));
    return ret;
    }

static eAtRet FaceplateLineRateDidChangeFromStm64(AtModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    eAtRet ret = cAtOk;
    uint32 line_i;

    /* When changing from STM-64 back to STM-16, it would be better to have
     * other ports enabled with STM-16 rate */
    if (newRate == cAtSdhLineRateStm16)
        {
        for (line_i = 2; line_i < cNumLinesPerGroup; line_i += 2)
            {
            AtSdhLine line = AtModuleSdhLineGet(self, (uint8)(line_i + lineId));
            ret |= AtSdhChannelModeSet((AtSdhChannel)line, DefaultLineMode());
            ret |= AtSdhLineRateSet(line, cAtSdhLineRateStm16);
            }
        }

    /* When changing to STM-4/STM-1/STM-0 from STM-64, it is better to have all
     * of ports have the same rate */
    else
        {
        AtUnused(oldRate);
        for (line_i = 1; line_i < cNumLinesPerGroup; line_i++)
            {
            AtSdhLine line = AtModuleSdhLineGet(self, (uint8)(line_i + lineId));
            ret |= AtSdhChannelModeSet((AtSdhChannel)line, DefaultLineMode());
            ret |= AtSdhLineRateSet(line, newRate);
            }
        }

    if (ret != cAtOk)
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Set line rate to %d fail with ret = %s\r\n", newRate,
                    AtRet2String(ret));
        }

    return ret;
    }

static eAtRet FaceplateLineRateDidChangeFromStm16(AtModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    eAtRet ret = cAtOk;
    uint32 nextLineId;
    AtSdhLine nextLine;

    /* When changing to STM-4/STM-1/STM-0 from STM-16, it is better to have the
     * next port have the same rate */
    nextLineId = lineId + 1UL;
    if (!LineExist(self, (uint8)nextLineId))
        return cAtOk;


    nextLine = AtModuleSdhLineGet(self, (uint8)nextLineId);
    /* To handle initializing time when STM-16 is set to default */
    if ((nextLine == NULL) && (oldRate == newRate))
        return cAtOk;

    ret |= AtSdhChannelModeSet((AtSdhChannel)nextLine, DefaultLineMode());
    ret |= AtSdhLineRateSet(nextLine, newRate);

    if (ret != cAtOk)
        {
        AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation,
                    "Set line rate to %d fail with ret = %s\r\n", newRate,
                    AtRet2String(ret));
        }

    return ret;
    }

static eAtRet FaceplateLineRateWillChange(Tha60290021ModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    if (newRate == cAtSdhLineRateStm64)
        return FaceplateLineRateWillChangeToStm64((AtModuleSdh)self, lineId);

    if (newRate == cAtSdhLineRateStm16)
        return FaceplateLineRateWillChangeToStm16((AtModuleSdh)self, lineId, oldRate);

    return cAtOk;
    }

static eAtRet FaceplateLineRateDidChange(Tha60290021ModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    if (oldRate == cAtSdhLineRateStm64)
        return FaceplateLineRateDidChangeFromStm64((AtModuleSdh)self, lineId, oldRate, newRate);

    if (oldRate == cAtSdhLineRateStm16)
        return FaceplateLineRateDidChangeFromStm16((AtModuleSdh)self, lineId, oldRate, newRate);

    return cAtOk;
    }

static eBool LineRateIsSupported(AtModuleSdh self, AtSdhLine line, eAtSdhLineRate rate)
    {
    uint8 lineId = (uint8)AtChannelIdGet((AtChannel)line);

    switch (rate)
        {
        case cAtSdhLineRateStm0:
        case cAtSdhLineRateStm1:
        case cAtSdhLineRateStm4:
            return IsFaceplateLine(self, lineId);
        case cAtSdhLineRateStm16:
            return (IsLineSupportStm16(self, lineId));
        case cAtSdhLineRateStm64:
            return (IsFaceplateLineSupportStm64(self, lineId));
        case cAtSdhLineRateInvalid:
        default:
            return cAtFalse;
        }
    }

static AtSdhLine LineGet (AtModuleSdh self, uint8 lineId)
    {
    if (LineExist(self, lineId) == cAtFalse)
        return NULL;

    return m_AtModuleSdhMethods->LineGet(self, lineId);
    }

static AtSdhLine FaceplateLineCreate(Tha60290021ModuleSdh self, uint8 lineId)
    {
    return Tha6029SdhFacePlateLineNew(lineId, (AtModuleSdh)self);
    }

static AtSdhLine TerminatedLineCreate(Tha60290021ModuleSdh self, uint8 lineId)
    {
    return Tha6029SdhTerminatedLineNew(lineId, (AtModuleSdh)self);
    }

static AtSdhLine MateLineCreate(Tha60290021ModuleSdh self, uint8 lineId)
    {
    return Tha6029SdhMateLineNew(lineId, (AtModuleSdh)self);
    }

static AtSdhLine LineCreate(AtModuleSdh self, uint8 lineId)
    {
    if (IsFaceplateLine(self, lineId))
        return mMethodsGet(mThis(self))->FaceplateLineCreate(mThis(self), lineId);

    if (IsMateLine(self, lineId))
        return mMethodsGet(mThis(self))->MateLineCreate(mThis(self), lineId);

    if (IsTerminatedLine(self, lineId))
        return mMethodsGet(mThis(self))->TerminatedLineCreate(mThis(self), lineId);

    return NULL;
    }

static AtSdhAu AuCreate(AtModuleSdh self, uint8 lineId, uint8 channelType, uint8 channelId)
    {
    if (IsFaceplateLine(self, lineId))
        return mMethodsGet(mThis(self))->FacePlateLineAuCreate(mThis(self), channelId, channelType);

    if (IsMateLine(self, lineId))
        return mMethodsGet(mThis(self))->MateLineAuCreate(mThis(self), channelId, channelType);

    if (IsTerminatedLine(self, lineId))
        return Tha6029SdhTerminatedLineAuNew(channelId, channelType, self);

    return NULL;
    }

static AtSdhVc VcCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3)
        return (AtSdhVc)m_AtModuleSdhMethods->ChannelCreate(self, lineId, parent, channelType, channelId);

    if (IsFaceplateLine(self, lineId))
        return mMethodsGet(mThis(self))->FacePlateLineAuVcCreate(mThis(self), channelId, channelType);

    if (IsMateLine(self, lineId))
        return Tha6029SdhMateLineAuVcNew(channelId, channelType, self);

    if (IsTerminatedLine(self, lineId))
        return mMethodsGet(mThis(self))->TerminatedLineAuVcCreate(mThis(self), channelId, channelType);

    return NULL;
    }

static AtSdhAug AugCreate(AtModuleSdh self, uint8 lineId, uint8 channelType, uint8 channelId)
    {
    if (IsMateLine(self, lineId))
        return Tha60290021SdhMateAugNew(channelId, channelType, self);
    if (IsFaceplateLine(self, lineId))
        return mMethodsGet(mThis(self))->FacePlateLineAugCreate(mThis(self), channelId, channelType);

    return Tha60290021SdhAugNew(channelId, channelType, self);
    }

static AtSdhChannel TugObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha60290021SdhTugNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel Vc1xObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha6029SdhTerminatedLineVc1xNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel Tu3VcObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha6029SdhTerminatedLineTu3VcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel TuObjectCreate(Tha60210011ModuleSdh self, uint8 channelType, uint8 channelId)
    {
    return (AtSdhChannel)Tha6029SdhTerminatedLineTuNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhChannel ChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    switch (channelType)
        {
        case cAtSdhChannelTypeLine:
            return (AtSdhChannel)LineCreate(self, channelId);

        /* Create AUG */
        case cAtSdhChannelTypeAug64:
        case cAtSdhChannelTypeAug16:
        case cAtSdhChannelTypeAug4:
        case cAtSdhChannelTypeAug1:
            return (AtSdhChannel)AugCreate(self, lineId, channelType, channelId);

        /* Create AU */
        case cAtSdhChannelTypeAu4_64c:
        case cAtSdhChannelTypeAu4_16c:
        case cAtSdhChannelTypeAu4_4c:
        case cAtSdhChannelTypeAu4:
        case cAtSdhChannelTypeAu3:
            return (AtSdhChannel)AuCreate(self, lineId, channelType, channelId);

        /* Create VC */
        case cAtSdhChannelTypeVc4_64c:
        case cAtSdhChannelTypeVc4_16c:
        case cAtSdhChannelTypeVc4_4c:
        case cAtSdhChannelTypeVc4:
        case cAtSdhChannelTypeVc3:
            return (AtSdhChannel)VcCreate(self, lineId, parent, channelType, channelId);

        default:
            return m_AtModuleSdhMethods->ChannelCreate(self, lineId, parent, channelType, channelId);
        }
    }

static AtSdhLine FaceplateLineGet(Tha60290021ModuleSdh self, uint8 localLineId)
    {
    return AtModuleSdhLineGet((AtModuleSdh)self, (uint8)(localLineId + FaceplateLineStartId((AtModuleSdh)self)));
    }

static AtSdhLine MateLineGet(Tha60290021ModuleSdh self, uint8 localLineId)
    {
    return AtModuleSdhLineGet((AtModuleSdh)self, (uint8)(localLineId + MateLineStartId((AtModuleSdh)self)));
    }

static AtSdhLine TerminatedLineGet(Tha60290021ModuleSdh self, uint8 localLineId)
    {
    return AtModuleSdhLineGet((AtModuleSdh)self, (uint8)(localLineId + TerminatedLineStartId((AtModuleSdh)self)));
    }

static eBool AutoCrossConnect(Tha60210011ModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 NumTfi5Lines(AtModuleSdh self)
    {
    return Tha60290021ModuleSdhNumMateLines(self);
    }

static uint32 MaxLineRate(AtModuleSdh self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm64;
    }

static eBool LineHasFramer(AtModuleSdh self, AtSdhLine line)
    {
    return IsFaceplateLine(self, (uint8)AtChannelIdGet((AtChannel)line));
    }

static eBool BerHardwareInterruptIsSupported(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 MaxNumDccHdlcChannels(ThaModuleSdh self)
    {
    AtUnused(self);
    return 32;
    }

static uint32 DccIdOffset(eAtSdhLineDccLayer layers)
    {
    return (layers & cAtSdhLineDccLayerSection) ? 0 : 1;
    }

static uint32 DccIdProvision(AtSdhLine self, eAtSdhLineDccLayer layers)
    {
    return (uint32)(AtChannelIdGet((AtChannel)self) * 2 + DccIdOffset(layers));
    }

static AtHdlcChannel DccChannelObjectCreate(Tha60290021ModuleSdh module, uint32 dccId,  AtSdhLine line, eAtSdhLineDccLayer layers)
	{
	return Tha60290021HdlcChannelNew(dccId, line, layers, (AtModuleSdh)module);
	}

static AtHdlcChannel DccChannelCreate(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layers)
    {
    uint32 dccId = DccIdProvision(line, layers);
    if (dccId >= ThaModuleSdhMaxNumDccHdlcChannels((ThaModuleSdh) self))
        return NULL;

    return mMethodsGet((Tha60290021ModuleSdh)self)->DccChannelObjectCreate((Tha60290021ModuleSdh)self, dccId, line, layers);
    }

static eBool DccAllIsSupported(AtModuleSdh self)
    {
    /* Just Support on simulation for more-testing */
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return AtDeviceIsSimulated(device);
    }

static eBool DccLayerIsSupported(AtModuleSdh self, AtSdhLine line, eAtSdhLineDccLayer layers)
    {
    AtUnused(self);
    AtUnused(line);

    switch ((uint32)layers)
        {
        case cAtSdhLineDccLayerSection  : return cAtTrue;
        case cAtSdhLineDccLayerLine   : return cAtTrue;
        case cAtSdhLineDccLayerAll      : return DccAllIsSupported(self);
        default                         : return cAtFalse;
        }
    }

static eBool LineIsMate(AtModuleSdh self, uint8 lineId)
    {
    return mInRange(lineId, MateLineStartId(self), MateLineStopId(self));
    }

static eBool LineIsTerminated(AtModuleSdh self, uint8 lineId)
    {
    uint8 startLineId = Tha60290021ModuleSdhTerminatedLineStartId(self);
    uint8 endLineId = TerminatedLineStopId(self);
    return mInRange(lineId, startLineId, endLineId);
    }

static uint8 LocalLineId(ThaModuleSdh self, uint32 lineId)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;

    if (Tha60290021ModuleSdhLineIsFaceplate(sdhModule, (uint8)lineId))
        return (uint8)lineId;

    if (Tha60290021ModuleSdhLineIsMate(sdhModule, (uint8)lineId))
        return (uint8)(lineId - MateLineStartId(sdhModule));

    /* It must be terminated line */
    return (uint8)(lineId - TerminatedLineStartId(sdhModule));
    }

static eBool LineCanBeUsed(AtModuleSdh self, uint8 lineId)
    {
    uint8 localId = ThaModuleSdhLineIdLocalId((ThaModuleSdh)self, lineId);

    if (Tha60290021ModuleSdhLineIsFaceplate(self, lineId))
        return (localId < mMethodsGet(mThis(self))->NumUseableFaceplateLines(mThis(self))) ? cAtTrue : cAtFalse;

    if (Tha60290021ModuleSdhLineIsMate(self, lineId))
        return (localId < mMethodsGet(mThis(self))->NumUseableMateLines(mThis(self))) ? cAtTrue : cAtFalse;

    if (Tha60290021ModuleSdhLineIsTerminated(self, lineId))
        return (localId < mMethodsGet(mThis(self))->NumUseableTerminatedLines(mThis(self))) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static uint8 NumFaceplateLines(AtModuleSdh self)
    {
    AtUnused(self);
    return cThaNumFacePlateLine;
    }

static uint8 NumUseableFaceplateLines(Tha60290021ModuleSdh self)
    {
    AtUnused(self);
    return 8;
    }

static uint8 NumMateLines(AtModuleSdh self)
    {
    AtUnused(self);
    return 8;
    }

static uint8 NumUseableMateLines(Tha60290021ModuleSdh self)
    {
    AtUnused(self);
    return 4;
    }

static uint8 NumTerminatedLines(AtModuleSdh self)
    {
    AtUnused(self);
    return 8;
    }

static uint8 NumUseableTerminatedLines(Tha60290021ModuleSdh self)
    {
    AtUnused(self);
    return 4;
    }

static uint8 MaxNumLoLines(Tha60210011ModuleSdh self)
    {
    return mMethodsGet(mThis(self))->NumUseableTerminatedLines(mThis(self));
    }

static uint8 MaxNumHoLines(Tha60210011ModuleSdh self)
    {
    return mMethodsGet(mThis(self))->NumUseableTerminatedLines(mThis(self));
    }

static eAtRet PohMonitorDefaultSet(Tha60210011ModuleSdh self)
    {
    uint32 regVal, regAddr;
    eAtRet ret;
    ThaModulePoh pohModule;

    ret = m_Tha60210011ModuleSdhMethods->PohMonitorDefaultSet(self);
    if (ret != cAtOk)
        return ret;

    /* As HW's implementation, STSs Line 4 will be used for all lines TTI processor.
     * So sliceId will be always 4, and stsId will be Faceplate local line ID when
     * programming for Faceplate line TTI processor */
    pohModule = (ThaModulePoh)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModulePoh);
    regAddr = cAf6Reg_pcfg_glbcpectr_Base + Tha60210011ModulePohBaseAddress(pohModule);;
    regVal = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cAf6_pcfg_glbcpectr_cpestsline4en_, 1);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static uint32 SerdesPrbsBaseAddress(Tha60210011ModuleSdh self, AtSerdesController serdes)
    {
    /* TODO: Update this when having RD */
    return m_Tha60210011ModuleSdhMethods->SerdesPrbsBaseAddress(self, serdes);
    }

static AtSdhLine LineFromLoLineStsGet(Tha60210011ModuleSdh self, uint8 loSlice, uint8 loHwStsInSlice, uint8 *stsInLine)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    AtSdhLine terminatedLine = Tha60290021ModuleSdhTerminatedLineGet(sdhModule, loSlice);
    if (stsInLine)
        *stsInLine = loHwStsInSlice;
    return terminatedLine;
    }

static eBool VcIsFromLoBus(AtModuleSdh self, AtSdhChannel vc)
    {
    uint8 lineId = AtSdhChannelLineGet(vc);

    if (!Tha60290021ModuleSdhLineIsTerminated(self, lineId))
        return cAtFalse;

    if (Tha60290021ModulePrbsShouldRedirectToHoBus(vc))
        return cAtFalse;

    switch (AtSdhChannelMapTypeGet(vc))
        {
        case cAtSdhVcMapTypeVc4Map3xTug3s: return cAtTrue;
        case cAtSdhVcMapTypeVc3Map7xTug2s: return cAtTrue;
        case cAtSdhVcMapTypeVc3MapDe3    : return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static ThaModuleOcn ModuleOcn(ThaModuleSdh self)
    {
    return (ThaModuleOcn)AtDeviceModuleGet(AtModuleDeviceGet((AtModule)self), cThaModuleOcn);
    }

static void LineInterruptProcess(ThaModuleSdh self, uint32 tohIntr, AtHal hal)
    {
    ThaModuleOcn ocnModule = ModuleOcn(self);
    uint32 group_i, numGroups = Tha60290021ModuleOcnNumFaceplateLineGroups(ocnModule);
    uint32 numLinesInGroup = Tha60290021ModuleOcnNumFaceplateLinesPerGroup(ocnModule);

    for (group_i = 0; group_i < numGroups; group_i++)
        {
        uint32 groupOffset = Tha60290021ModuleOcnFaceplateGroupOffset(ocnModule, group_i);
        uint32 baseAddress = ThaModuleOcnBaseAddress(ModuleOcn(self));
        uint32 intrLineVal = AtHalRead(hal, (uint32)cAf6Reg_tohintperline_Base + baseAddress + groupOffset);
        uint32 intrLineMask = AtHalRead(hal, (uint32)cAf6Reg_tohintperlineenctl_Base + baseAddress + groupOffset);
        uint8 localLineId;

        AtUnused(tohIntr);

        intrLineVal &= intrLineMask;
        for (localLineId = 0; localLineId < numLinesInGroup; localLineId++)
            {
            if (intrLineVal & cIteratorMask(localLineId))
                {
                AtSdhLine line = ThaModuleSdhLineFromHwIdGet(self, cAtModuleSdh, (uint8)group_i, localLineId);
                uint8 lineId = (uint8)AtChannelIdGet((AtChannel)self);
                AtSdhLineInterruptProcess(line, lineId, hal);
                }
            }
        }
    }

static AtSdhLine LineFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 localLineInSlice)
    {
    if (phyModule == cAtModuleSdh)
        {
        uint32 numLinesInGroup = Tha60290021ModuleOcnNumFaceplateLinesPerGroup(ModuleOcn(self));
        uint8 lineId = (uint8)(sliceId * numLinesInGroup + localLineInSlice);
        return AtModuleSdhLineGet((AtModuleSdh)self, lineId);
        }

    return m_ThaModuleSdhMethods->LineFromHwIdGet(self, phyModule, sliceId, localLineInSlice);
    }

static AtSdhPath FaceplateAuPathFromHwIdGet(Tha60290021ModuleSdh self, uint8 slice48, uint8 stsInSlice)
    {
    AtSdhLine faceplateLine;
    uint8 faceplateId;

    faceplateId = (uint8)((slice48 / 4) * 8); /* Two possible-STM64 line IDs are 0 and 8 */
    faceplateLine = AtModuleSdhLineGet((AtModuleSdh)self, faceplateId);
    if (faceplateLine == NULL)
        return NULL;

    if (AtSdhLineRateGet(faceplateLine) == cAtSdhLineRateStm64)
        return (AtSdhPath)Tha60290021SdhLineStm64AuPathFromHwStsGet(faceplateLine, slice48, stsInSlice);

    /* There are 8 possible-STM16 line IDs such as 0, 2, 4, 6, 8, 10, 12 and 14 */
    faceplateId = (uint8)(slice48 * 2);
    faceplateLine = AtModuleSdhLineGet((AtModuleSdh)self, faceplateId);
    if (faceplateLine == NULL)
            return NULL;

    if (AtSdhLineRateGet(faceplateLine) == cAtSdhLineRateStm16)
        return (AtSdhPath)Tha60210011ModuleSdhAuFromHwStsGet(faceplateLine, stsInSlice);
        else
            {
        uint8 sts12IndexInSts48 = (uint8)((stsInSlice % 16) / 4);
        uint8 sts1IndexInSts12;

            /* One STS-48 slice is multiplexed from four STS-12s, we only uses
             * the first two STS-12s */
        if (sts12IndexInSts48 >= 2)
                    return NULL;

        faceplateId = (uint8)(faceplateId + sts12IndexInSts48);
        faceplateLine = AtModuleSdhLineGet((AtModuleSdh)self, faceplateId);
        if (faceplateLine == NULL)
                    return NULL;

        /* STM-4/STM-1/STM-0 lookup */
        sts1IndexInSts12 = (uint8)((stsInSlice % 4) + (stsInSlice / 16) * 4);
        return (AtSdhPath)Tha60290021SdhLineSubStm16AuPathFromHwStsGet(faceplateLine, sts1IndexInSts12);
                }
            }

static AtSdhPath MateAuPathFromHwIdGet(ThaModuleSdh self, uint8 sliceId, uint8 stsId)
    {
    AtSdhLine mateLine = AtModuleSdhLineGet((AtModuleSdh)self, (uint8)(sliceId + MateLineStartId((AtModuleSdh)self)));
    if (mateLine == NULL)
        return NULL;

    return (AtSdhPath)Tha60210011ModuleSdhAuFromHwStsGet(mateLine, stsId);
    }

static AtSdhPath LineSideAuPathFromHwIdGet(ThaModuleSdh self, uint8 slice48, uint8 stsInSlice)
    {
    eTha6029LineSide side = Tha60290021ModuleOcnPohProcessorSideGet(ModuleOcn(self));

    if (side == cTha6029LineSideFaceplate)
        return mMethodsGet(mThis(self))->FaceplateAuPathFromHwIdGet(mThis(self), slice48, stsInSlice);

    if (side == cTha6029LineSideMate)
        return MateAuPathFromHwIdGet(self, slice48, stsInSlice);

    return NULL;
    }

static AtSdhPath AuPathFromHwIdGet(ThaModuleSdh self, eAtModule phyModule, uint8 sliceId, uint8 hwStsId)
    {
    uint8 slice48, stsInSlice;
    AtUnused(phyModule);

    ThaModuleSdhHwSts24ToHwSts48Get(self, sliceId, hwStsId, &slice48, &stsInSlice);

    return Tha60290021ModuleSdhLineSideAuPathFromHwIdGet(self, slice48, stsInSlice);
    }

static eBool IsTerminatedVc(AtSdhChannel vc)
    {
    uint8 lineId = AtSdhChannelLineGet(vc);
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet((AtChannel)vc);
    return Tha60290021ModuleSdhLineIsTerminated(sdhModule, lineId);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60290021ModuleSdh object = mThis(self);

    m_AtObjectMethods->Serialize(self, encoder);

    AtCoderEncodeUInt16Array(encoder, object->rate, cThaNumFacePlateLine, "rate");
    }

static eBool ShouldUpdateSerdesTimingWhenLineRateChange(ThaModuleSdh self)
    {
    /* Let SERDES layer and applications manage this */
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldCompareTuSsBit(AtModuleSdh self, AtSdhPath tu)
    {
    AtUnused(self);
    AtUnused(tu);
    return cAtTrue;
    }

static eBool NeedClearanceNotifyLogic(AtModuleSdh self)
    {
    AtUnused(self);
    return Tha602900xxNeedClearanceNotifyLogic();
    }

static AtSdhAu MateLineAuCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha6029SdhMateLineAuNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhAu FacePlateLineAuCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha6029SdhFacePlateLineAuNew(channelId, channelType, (AtModuleSdh)self);
    }

static eBool ShouldUpdateTimRdiBackwardWhenAisDownstreamChange(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtSdhVc FacePlateLineAuVcCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha6029SdhFacePlateLineAuVcNew(channelId, channelType, (AtModuleSdh)self);
    }

static AtSdhVc TerminatedLineAuVcCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha6029SdhTerminatedLineAuVcNew(channelId, channelType, (AtModuleSdh)self);
    }

static eBool ShouldUpdatePlmMonitorWhenExpectedPslChange(ThaModuleSdh self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 NumEc1LinesGet(AtModuleSdh self)
    {
    return Tha60290021ModuleSdhNumUseableFaceplateLines(self);
    }

static AtSdhAug FacePlateLineAugCreate(Tha60290021ModuleSdh self, uint32 channelId, uint8 channelType)
    {
    return Tha60290021SdhFaceplateAugNew(channelId, channelType, (AtModuleSdh)self);
    }

static eBool BitErrorCounterModeAsDefault(AtModuleSdh self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HidingXcIsActive(AtSdhChannel channel)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)channel);
    AtModuleXc xcModule =  (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    return (Tha60290021ModuleXcHideModeGet(xcModule) == cTha60290021XcHideModeDirect) ? cAtTrue : cAtFalse;
    }

static eBool NeedDestroyAllServices(AtSdhChannel channel)
    {
    return HidingXcIsActive(channel);
    }

static eBool HaveHideChannel(AtSdhChannel channel)
    {
    return HidingXcIsActive(channel);
    }

static AtSdhLine HidingXcTerminatedChannelToFaceplateLineGet(Tha60290021ModuleSdh self, AtSdhChannel channel)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;
    uint32 localLineId = ThaModuleSdhLineIdLocalId((ThaModuleSdh)sdhModule, AtSdhChannelLineGet(channel));
    uint32 flatSts1 = localLineId * 48U + AtSdhChannelSts1Get(channel);
    uint32 faceplateLineId;
    AtSdhLine line;

    /* Line 0 or 8 */
    faceplateLineId = (flatSts1 / 192) * 8;
    line = AtModuleSdhLineGet(sdhModule, (uint8)faceplateLineId);
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm64)
        return line;

    /* Line even ID */
    faceplateLineId = (flatSts1 / 48) * 2;
    line = AtModuleSdhLineGet(sdhModule, (uint8)faceplateLineId);
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm16)
        return line;

    /* Line even ID */
    faceplateLineId = ((flatSts1 / 48) * 2 + (flatSts1 % 24) / 12);
    line = AtModuleSdhLineGet(sdhModule, (uint8)faceplateLineId);
    return line;
    }

static void OverrideAtObject(AtModuleSdh self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, BerHardwareInterruptIsSupported);
        mMethodOverride(m_ThaModuleSdhOverride, LocalLineId);
        mMethodOverride(m_ThaModuleSdhOverride, LineInterruptProcess);
        mMethodOverride(m_ThaModuleSdhOverride, LineFromHwIdGet);
        mMethodOverride(m_ThaModuleSdhOverride, AuPathFromHwIdGet);
        mMethodOverride(m_ThaModuleSdhOverride, MaxNumDccHdlcChannels);
        mMethodOverride(m_ThaModuleSdhOverride, ShouldUpdateSerdesTimingWhenLineRateChange);
        mMethodOverride(m_ThaModuleSdhOverride, ShouldUpdateTimRdiBackwardWhenAisDownstreamChange);
        mMethodOverride(m_ThaModuleSdhOverride, ShouldUpdatePlmMonitorWhenExpectedPslChange);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, LineGet);
        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        mMethodOverride(m_AtModuleSdhOverride, NumTfi5Lines);
        mMethodOverride(m_AtModuleSdhOverride, ChannelCreate);
        mMethodOverride(m_AtModuleSdhOverride, LineRateIsSupported);
        mMethodOverride(m_AtModuleSdhOverride, MaxLineRate);
        mMethodOverride(m_AtModuleSdhOverride, LineHasFramer);
        mMethodOverride(m_AtModuleSdhOverride, DccChannelCreate);
        mMethodOverride(m_AtModuleSdhOverride, DccLayerIsSupported);
        mMethodOverride(m_AtModuleSdhOverride, LineCanBeUsed);
        mMethodOverride(m_AtModuleSdhOverride, ShouldCompareTuSsBit);
        mMethodOverride(m_AtModuleSdhOverride, NeedClearanceNotifyLogic);
        mMethodOverride(m_AtModuleSdhOverride, NumEc1LinesGet);
        mMethodOverride(m_AtModuleSdhOverride, BitErrorCounterModeAsDefault);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void OverrideTha60210011ModuleSdh(AtModuleSdh self)
    {
    Tha60210011ModuleSdh module = (Tha60210011ModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleSdhMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleSdhOverride, m_Tha60210011ModuleSdhMethods, sizeof(m_Tha60210011ModuleSdhOverride));

        mMethodOverride(m_Tha60210011ModuleSdhOverride, AutoCrossConnect);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, MaxNumLoLines);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, MaxNumHoLines);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, PohMonitorDefaultSet);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, SerdesPrbsBaseAddress);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, LineFromLoLineStsGet);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, TugObjectCreate);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, Tu3VcObjectCreate);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, TuObjectCreate);
        mMethodOverride(m_Tha60210011ModuleSdhOverride, Vc1xObjectCreate);
        }

    mMethodsSet(module, &m_Tha60210011ModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtObject(self);
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    OverrideTha60210011ModuleSdh(self);
    }

static void MethodsInit(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, NumUseableFaceplateLines);
        mMethodOverride(m_methods, FaceplateLineGet);
        mMethodOverride(m_methods, NumUseableMateLines);
        mMethodOverride(m_methods, MateLineGet);
        mMethodOverride(m_methods, NumUseableTerminatedLines);
        mMethodOverride(m_methods, TerminatedLineGet);
        mMethodOverride(m_methods, DccChannelObjectCreate);
        mMethodOverride(m_methods, UseNewStsIdConvert);
        mMethodOverride(m_methods, FaceplateLineCreate);
        mMethodOverride(m_methods, TerminatedLineCreate);
        mMethodOverride(m_methods, MateLineCreate);
        mMethodOverride(m_methods, MateLineAuCreate);
        mMethodOverride(m_methods, FacePlateLineAuCreate);
        mMethodOverride(m_methods, FacePlateLineAuVcCreate);
        mMethodOverride(m_methods, TerminatedLineAuVcCreate);
        mMethodOverride(m_methods, FacePlateLineAugCreate);
        mMethodOverride(m_methods, FacePlateLineIsUsed);
        mMethodOverride(m_methods, FaceplateLineRateWillChange);
        mMethodOverride(m_methods, FaceplateLineRateDidChange);
        mMethodOverride(m_methods, HidingXcTerminatedChannelToFaceplateLineGet);
        mMethodOverride(m_methods, FaceplateAuPathFromHwIdGet);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021ModuleSdh);
    }

AtModuleSdh Tha60290021ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60290021ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021ModuleSdhObjectInit(newModule, device);
    }

uint8 Tha60290021ModuleSdhNumFaceplateLines(AtModuleSdh self)
    {
    if (self)
        return NumFaceplateLines(self);
    return 0;
    }

AtSdhLine Tha60290021ModuleSdhFaceplateLineGet(AtModuleSdh self, uint8 localLineId)
    {
    if (self)
        return mMethodsGet(mThis(self))->FaceplateLineGet(mThis(self), localLineId);
    return NULL;
    }

uint8 Tha60290021ModuleSdhNumMateLines(AtModuleSdh self)
    {
    if (self)
        return NumMateLines(self);
    return 0;
    }

AtSdhLine Tha60290021ModuleSdhMateLineGet(AtModuleSdh self, uint8 localLineId)
    {
    if (self)
        return mMethodsGet(mThis(self))->MateLineGet(mThis(self), localLineId);
    return NULL;
    }

uint8 Tha60290021ModuleSdhNumTerminatedLines(AtModuleSdh self)
    {
    if (self)
        return NumTerminatedLines(self);
    return 0;
    }

AtSdhLine Tha60290021ModuleSdhTerminatedLineGet(AtModuleSdh self, uint8 localLineId)
    {
    if (self)
        return mMethodsGet(mThis(self))->TerminatedLineGet(mThis(self), localLineId);
    return 0;
    }

uint8 Tha60290021ModuleSdhTerminatedLineStartId(AtModuleSdh self)
    {
    if (self)
        return TerminatedLineStartId(self);
    return 0xFF;
    }

eBool Tha60290021ModuleSdhLineIsTerminated(AtModuleSdh self, uint8 lineId)
    {
    if (self)
        return LineIsTerminated(self, lineId);
    return cAtFalse;
    }

eBool Tha60290021ModuleSdhLineIsMate(AtModuleSdh self, uint8 lineId)
    {
    if (self)
        return LineIsMate(self, lineId);
    return cAtFalse;
    }

eBool Tha60290021ModuleSdhLineIsFaceplate(AtModuleSdh self, uint8 lineId)
    {
    if (self)
        return IsFaceplateLine(self, lineId);
    return cAtFalse;
    }

eAtRet Tha60290021ModuleSdhFaceplateLineRateWillChange(AtModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (ShouldNotTouchRateOfOtherLines(self))
        return cAtOk;

    return mMethodsGet(mThis(self))->FaceplateLineRateWillChange(mThis(self), lineId, oldRate, newRate);
    }

eAtRet Tha60290021ModuleSdhFaceplateLineRateDidChange(AtModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate)
    {
    if (self == NULL)
        return cAtErrorNullPointer;

    if (ShouldNotTouchRateOfOtherLines(self))
        return cAtOk;

    return mMethodsGet(mThis(self))->FaceplateLineRateDidChange(mThis(self), lineId, oldRate, newRate);
    }

uint8 Tha60290021ModuleSdhMateLineStartId(AtModuleSdh self)
    {
    if (self)
        return MateLineStartId(self);
    return cInvalidUint8;
    }

uint8 Tha60290021ModuleSdhNumUseableFaceplateLines(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->NumUseableFaceplateLines(mThis(self));
    return 0;
    }

uint8 Tha60290021ModuleSdhNumUseableMateLines(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->NumUseableMateLines(mThis(self));
    return 0;
    }

uint8 Tha60290021ModuleSdhNumUseableTerminatedLines(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->NumUseableTerminatedLines(mThis(self));
    return 0;
    }

eAtSdhLineRate Tha60290021ModuleSdhFaceplateLineRateGet(AtModuleSdh self, uint8 lineId)
    {
    if (self)
        return FaceplateLineRateGet(self, lineId);
    return cAtSdhLineRateInvalid;
    }

eAtRet Tha60290021ModuleSdhFaceplateLineRateSet(AtModuleSdh self, uint8 lineId, eAtSdhLineRate rate)
    {
    if (self)
        return FaceplateLineRateSet(self, lineId, rate);
    return cAtErrorNullPointer;
    }

eBool Tha60290021ModuleSdhVcIsFromLoBus(AtModuleSdh self, AtSdhChannel vc)
    {
    if (self)
        return VcIsFromLoBus(self, vc);
    return cAtFalse;
    }

AtSdhChannel Tha60290021SdhLineStm64AuPathFromHwStsGet(AtSdhLine facePlateLine, uint8 slice48, uint8 stsInSlice)
    {
    AtSdhChannel aug64 = AtSdhChannelSubChannelGet((AtSdhChannel)facePlateLine, 0);

    if (AtSdhChannelMapTypeGet(aug64) == cAtSdhAugMapTypeAug64MapVc4_64c)
        return AtSdhChannelSubChannelGet(aug64, 0);
    else
        {
        uint8 aug16Id = slice48 % 4;
        AtSdhChannel aug16 = AtSdhChannelSubChannelGet(aug64, aug16Id);

        if (AtSdhChannelMapTypeGet(aug16) == cAtSdhAugMapTypeAug16MapVc4_16c)
            return AtSdhChannelSubChannelGet(aug16, 0);
        else
            {
            uint8 aug4Id = (uint8)((stsInSlice % 16) / 4);
            AtSdhChannel aug4 = AtSdhChannelSubChannelGet(aug16, aug4Id);

            if (AtSdhChannelMapTypeGet(aug4) == cAtSdhAugMapTypeAug4MapVc4_4c)
                return AtSdhChannelSubChannelGet(aug4, 0);

            else if (AtSdhChannelMapTypeGet(aug4) == cAtSdhAugMapTypeAug4Map4xAug1s)
                {
                uint8 aug1Id = (uint8)((stsInSlice % 16) % 4);
                AtSdhChannel aug1 = AtSdhChannelSubChannelGet(aug4, aug1Id);

                if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1MapVc4)
                    return AtSdhChannelSubChannelGet(aug1, 0);

                if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1Map3xVc3s)
                    return AtSdhChannelSubChannelGet(aug1, (uint8)(stsInSlice / 16));
                }
            }
        }

    return NULL;
    }

AtSdhChannel Tha60290021SdhLineSubStm16AuPathFromHwStsGet(AtSdhLine facePlateLine, uint8 hwStsInSlice12)
    {
    eAtSdhLineRate rate = AtSdhLineRateGet(facePlateLine);

    if (rate == cAtSdhLineRateStm4)
        {
        AtSdhChannel aug4 = AtSdhChannelSubChannelGet((AtSdhChannel)facePlateLine, 0);
        if (AtSdhChannelMapTypeGet(aug4) == cAtSdhAugMapTypeAug4MapVc4_4c)
            return AtSdhChannelSubChannelGet(aug4, 0);
        else
            {
            AtSdhChannel aug1 = AtSdhChannelSubChannelGet(aug4, (uint8)(hwStsInSlice12 % 4));
            if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1MapVc4)
                return AtSdhChannelSubChannelGet(aug1, 0);
            else if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1Map3xVc3s)
                return AtSdhChannelSubChannelGet(aug1, (uint8)(hwStsInSlice12 / 4));
            }

        return NULL;
        }

    if (rate == cAtSdhLineRateStm1)
        {
        AtSdhChannel aug1 = AtSdhChannelSubChannelGet((AtSdhChannel)facePlateLine, 0);
        if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1MapVc4)
            return AtSdhChannelSubChannelGet(aug1, 0);
        else if (AtSdhChannelMapTypeGet(aug1) == cAtSdhAugMapTypeAug1Map3xVc3s)
            return AtSdhChannelSubChannelGet(aug1, (uint8)(hwStsInSlice12 / 4));

        return NULL;
        }

    if (rate == cAtSdhLineRateStm0)
        return AtSdhChannelSubChannelGet((AtSdhChannel)facePlateLine, 0);

    return NULL;
    }

AtSdhPath Tha60290021ModuleSdhLineSideAuPathFromHwIdGet(ThaModuleSdh self, uint8 slice48, uint8 hwStsInSlice)
    {
    if (self)
        return LineSideAuPathFromHwIdGet(self, slice48, hwStsInSlice);
    return NULL;
    }

eBool Tha60290021ModuleSdhUseNewStsIdConvert(AtModuleSdh self)
    {
    if (self)
        return mMethodsGet(mThis(self))->UseNewStsIdConvert(mThis(self));
    return cAtFalse;
    }

eBool Tha60290021ModuleSdhIsTerminatedVc(AtSdhChannel vc)
    {
    if (vc)
        return IsTerminatedVc(vc);
    return cAtFalse;
    }

eBool Tha60290021ModuleSdhIsLineSideVc(AtSdhChannel vc)
    {
    if (vc)
        return Tha60290021ModuleSdhIsTerminatedVc(vc) ? cAtFalse : cAtTrue;
    return cAtFalse;
    }

eBool Tha60290021SdhChannelNeedDestroyAllServices(AtSdhChannel self)
    {
    if (self)
        return NeedDestroyAllServices(self);
    return cAtFalse;
    }

eBool Tha60290021SdhChannelHaveHideChannel(AtSdhChannel self)
    {
    if (self)
        return HaveHideChannel(self);
    return cAtFalse;
    }

eAtSdhChannelMode Tha60290021SdhTerminatedChannelHidingXcDefaultMode(AtSdhChannel channel)
    {
    Tha60290021ModuleSdh sdhModule = (Tha60290021ModuleSdh)AtChannelModuleGet((AtChannel)channel);
    if (sdhModule)
        {
        AtSdhLine line = mMethodsGet(sdhModule)->HidingXcTerminatedChannelToFaceplateLineGet(sdhModule, channel);
        if (line)
            return AtSdhChannelModeGet((AtSdhChannel)line);

        AtChannelLog((AtChannel)channel, cSevCritical, AtSourceLocation, "Do not find the faceplate line\r\n");
        }

    return cAtSdhChannelModeSdh;
    }

eBool Tha60290021SdhChannelHidingXcIsActive(AtSdhChannel channel)
    {
    if (channel)
        return HidingXcIsActive(channel);
    return cAtFalse;
    }

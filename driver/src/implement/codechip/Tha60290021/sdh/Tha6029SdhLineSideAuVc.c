/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhLineSideAuVc.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : Lide side (MATE, Faceplate) AU's VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtChannel.h"
#include "../../../../generic/xc/AtCrossConnectInternal.h"
#include "../../../../generic/prbs/AtModulePrbsInternal.h"
#include "Tha60290021ModuleSdh.h"
#include "Tha60290021XcHiding.h"
#include "Tha60290021SdhLineSideAug.h"
#include "Tha6029SdhLineSideAuVcInternal.h"
#include "Tha6029SdhTerminatedLineAuVc.h"
#include "../man/Tha60290021Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6029SdhLineSideAuVc)self)

#define mClassAttributeGet(class, method, pairPathGetFunction)                 \
     do {                                                                      \
        class pairPath = (class)pairPathGetFunction((AtChannel)self);          \
        if (pairPath)                                                          \
            return class##method(pairPath);                                    \
        return m_##class##Methods->method(self);                               \
        }while (0)

#define mClassOneParamAttributeGet(class, method, value, pairPathGetFunction)  \
    do  {                                                                      \
        class pairPath = (class)pairPathGetFunction((AtChannel)self);          \
        if (pairPath)                                                          \
            return class##method(pairPath, value);                             \
        return m_##class##Methods->method(self, value);                        \
        }while(0)

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6029SdhLineSideAuVcMethods m_methods;

/* Override */
static tAtObjectMethods                m_AtObjectOverride;
static tAtChannelMethods               m_AtChannelOverride;
static tAtSdhChannelMethods            m_AtSdhChannelOverride;
static tAtSdhPathMethods               m_AtSdhPathOverride;
static tThaSdhVcMethods                m_ThaSdhVcOverride;
static tThaSdhAuVcMethods              m_ThaSdhAuVcOverride;

/* Super implementation */
static const tAtObjectMethods       *m_AtObjectMethods     = NULL;
static const tAtChannelMethods      *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods   *m_AtSdhChannelMethods = NULL;
static const tThaSdhVcMethods       *m_ThaSdhVcMethods     = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleXc XcModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static eBool XcIsVisible(AtChannel self)
    {
    return (Tha60290021ModuleXcHideModeGet(XcModule(self)) == cTha60290021XcHideModeNone) ? cAtTrue : cAtFalse;
    }

static eBool ServiceIsRunning(AtChannel self)
    {
    if (XcIsVisible(self) && AtChannelHasCrossConnect(self))
        return cAtTrue;

    return m_AtChannelMethods->ServiceIsRunning(self);
    }

static AtCrossConnect Xc(AtSdhChannel self)
    {
    return AtModuleXcVcCrossConnectGet(XcModule((AtChannel)self));
    }

static eTha60290021XcHideMode XcHidingMode(AtChannel self)
    {
    return Tha60290021ModuleXcHideModeGet(XcModule(self));
    }

static eAtRet CanBindToPseudowireWithXcHideMode(Tha6029SdhLineSideAuVc self, AtPw pseudowire, eTha60290021XcHideMode mode)
    {
    if (mode == cTha60290021XcHideModeDirect)
        return cAtOk;

    AtUnused(self);
    AtUnused(pseudowire);
    return cAtErrorModeNotSupport;
    }

static eAtRet ConfigLookUpWithXcHideMode(Tha6029SdhLineSideAuVc self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet CanBindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    /* Super already handles complex constrain checking, reuse it */
    eAtRet ret = m_AtChannelMethods->CanBindToPseudowire(self, pseudowire);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->CanBindToPseudowireWithXcHideMode(mThis(self), pseudowire, XcHidingMode(self));
    }

static AtChannel HideVc(AtChannel self)
    {
    return (AtChannel)AtSdhChannelHideChannelGet((AtSdhChannel)self);
    }

static eBool ShouldRedirectTimingConfiguration(AtChannel self)
    {
    if (XcHidingMode(self) == cTha60290021XcHideModeNone)
        return cAtFalse;

    /* Only redirect when having terminated source channel */
    return AtChannelSourceGet(self) ? cAtTrue : cAtFalse;
    }

static eBool TimingModeIsSupported(AtChannel self, eAtTimingMode timingMode)
    {
    if (ShouldRedirectTimingConfiguration(self))
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            return AtChannelTimingModeIsSupported(hideVc, timingMode);
        }

    return (timingMode == cAtTimingModeSys) ? cAtTrue : cAtFalse;
    }

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    if (ShouldRedirectTimingConfiguration(self))
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            return AtChannelTimingSet(hideVc, timingMode, timingSource);
        }

    return (timingMode == cAtTimingModeSys) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtTimingMode TimingModeGet(AtChannel self)
    {
    if (ShouldRedirectTimingConfiguration(self))
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            return AtChannelTimingModeGet(hideVc);
        }

    return cAtTimingModeSys;
    }

static AtChannel TimingSourceGet(AtChannel self)
    {
    if (ShouldRedirectTimingConfiguration(self))
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            return AtChannelTimingSourceGet(hideVc);
        }

    return NULL;
    }

static eAtClockState ClockStateGet(AtChannel self)
    {
    if (ShouldRedirectTimingConfiguration(self))
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            return AtChannelClockStateGet(hideVc);
        }

    return cAtClockStateNotApplicable;
    }

static ThaCdrController CdrControllerCreate(ThaSdhVc self)
    {
    AtUnused(self);
    return NULL;
    }

static ThaCdrController CdrControllerGet(ThaSdhVc self)
    {
    AtSdhVc terVc;

    /* TODO: still need to handle other XC hiding mode */
    if (XcHidingMode((AtChannel)self) != cTha60290021XcHideModeDirect)
        return NULL;

    /* Only redirect when having terminated source channel */
    terVc = (AtSdhVc) AtChannelSourceGet((AtChannel) self);
    if (terVc == NULL)
        return NULL;

    return ThaSdhVcCdrControllerGet(terVc);
    }

static ThaCdrController RedirectCdrController(ThaSdhVc self)
    {
    if (XcHidingMode((AtChannel)self) != cTha60290021XcHideModeDirect)
        return m_ThaSdhVcMethods->RedirectCdrController(self);

    return ThaSdhVcCdrControllerGet((AtSdhVc)self);
    }

static eBool UpsrIsApplicable(AtSdhPath self)
    {
    return AtDeviceModuleHwReady(AtChannelDeviceGet((AtChannel)self), cAtModuleAps);
    }

static eBool HasLoOrderMapping(ThaSdhVc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtSdhChannel XcHidingLineSideSourcePairVc(AtSdhChannel self)
    {
    AtSdhChannel aug = AtSdhVcAugGet(self);
    return Tha60290021XcHidingPairVcFromAug(self, Tha60290021SdhLineSideAugXcHdingLineSideSourcePairAug(aug));
    }

static AtSdhChannel XcHidingLineSideDestPairVc(AtSdhChannel self)
    {
    AtSdhChannel aug = AtSdhVcAugGet(self);
    return Tha60290021XcHidingPairVcFromAug(self, Tha60290021SdhLineSideAugXcHdingLineSideDestPairAug(aug));
    }

static eBool XcHidingShouldApply(AtSdhChannel self)
    {
    uint32 hideMode = XcHidingMode((AtChannel)self);
    AtSdhChannel aug = AtSdhVcAugGet(self);
    AtSdhLine line;

    if (aug)
        return Tha60290021SdhLineSideAugXcHidingShouldApply(aug, hideMode);

    line = AtSdhChannelLineObjectGet(self);
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm0)
        return (XcHidingMode((AtChannel)self) == cTha60290021XcHideModeDirect) ? cAtTrue : cAtFalse;

    return cAtFalse;
    }

static eAtModuleSdhRet XcHidingPairTerminatedVcConnectHelper(AtSdhChannel self,
                                                             AtSdhChannel pairTerminatedVc,
                                                             eTha60290021XcHideMode hideMode,
                                                             eAtModuleXcRet (*ConnectFunc)(AtCrossConnect self, AtChannel source, AtChannel dest))
    {
    AtCrossConnect xc = Xc(self);
    eAtRet ret = cAtOk;
    AtSdhChannel pairLineSideVc;

    if (hideMode == cTha60290021XcHideModeDirect)
        {
        ret |= ConnectFunc(xc, (AtChannel)self, (AtChannel)pairTerminatedVc);
        ret |= ConnectFunc(xc, (AtChannel)pairTerminatedVc, (AtChannel)self);
        return ret;
        }

    /* Other side is being looped */
    /* Make add connections */
    pairLineSideVc = XcHidingLineSideSourcePairVc(self);
    ret |= ConnectFunc(xc, (AtChannel)pairLineSideVc, (AtChannel)self);
    ret |= ConnectFunc(xc, (AtChannel)pairTerminatedVc, (AtChannel)pairLineSideVc);

    /* Make drop connections */
    pairLineSideVc = XcHidingLineSideDestPairVc(self);
    ret |= ConnectFunc(xc, (AtChannel)self, (AtChannel)pairLineSideVc);
    ret |= ConnectFunc(xc, (AtChannel)pairLineSideVc, (AtChannel)pairTerminatedVc);

    return ret;
    }

static eAtModuleSdhRet XcHidingConnectPairTerminatedVc(AtSdhChannel self, AtSdhChannel pairTerminatedVc, eTha60290021XcHideMode hideMode)
    {
    return XcHidingPairTerminatedVcConnectHelper(self, pairTerminatedVc, hideMode, AtCrossConnectChannelConnect);
    }

static uint8 XcHidingLineSideMapType(uint8 mapType)
    {
    if ((mapType == cAtSdhVcMapTypeVc3MapDe3) || (mapType == cAtSdhVcMapTypeVc3Map7xTug2s))
        return cAtSdhVcMapTypeVc3MapC3;

    if (mapType == cAtSdhVcMapTypeVc4Map3xTug3s)
        return cAtSdhVcMapTypeVc4MapC4;

    return mapType;
    }

static eAtModuleSdhRet XcHidingMapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret = cAtOk;
    AtSdhChannel hideVc = AtSdhChannelHideChannelGet(self);
    if (hideVc)
        {
        ret = mMethodsGet(hideVc)->MapTypeSet(hideVc, mapType);
        if (ret != cAtOk)
            return ret;
        }

    ret = m_AtSdhChannelMethods->MapTypeSet(self, XcHidingLineSideMapType(mapType));
    if (ret != cAtOk)
        return ret;

    return XcHidingConnectPairTerminatedVc(self, hideVc, XcHidingMode((AtChannel)self));
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret = cAtOk;
    if (XcHidingShouldApply(self))
        {
        ret |=  XcHidingMapTypeSet(self, mapType);
        ret |=  mMethodsGet(mThis(self))->ConfigLookUpWithXcHideMode(mThis(self));
        return ret;
        }

    return m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    }

static uint8 MapTypeGet(AtSdhChannel self)
    {
    if (XcHidingShouldApply(self))
        mClassAttributeGet(AtSdhChannel, MapTypeGet, HideVc);
    return m_AtSdhChannelMethods->MapTypeGet(self);
    }

static eBool MapTypeIsSupported(AtSdhChannel self, uint8 mapType)
    {
    if (XcHidingShouldApply(self))
        {
        AtSdhChannel hideVc = (AtSdhChannel)HideVc((AtChannel)self);
        if (hideVc)
            return AtSdhChannelMapTypeIsSupported(hideVc, mapType);
        }

    return m_AtSdhChannelMethods->MapTypeIsSupported(self, mapType);
    }

static AtChannel MapChannelGet(AtSdhChannel self)
    {
    if (XcHidingShouldApply(self))
        return (AtChannel)SdhVcPdhChannelGet((AtSdhVc)HideVc((AtChannel)self));

    return m_AtSdhChannelMethods->MapChannelGet(self);
    }

static AtPw BoundPseudowireGet(AtChannel self)
    {
    if (XcHidingShouldApply((AtSdhChannel)self))
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            return AtChannelBoundPwGet(hideVc);
       }

    return NULL;
    }

static eAtRet XcHidingTryConnectTerminatedVc(AtChannel self, AtChannel hideVc)
    {
    if (AtChannelSourceGet(self))
        return cAtOk;

    return XcHidingConnectPairTerminatedVc((AtSdhChannel)self, (AtSdhChannel)hideVc, XcHidingMode(self));
    }

static eAtRet BindToPseudowire(AtChannel self, AtPw pseudowire)
    {
    if (XcHidingShouldApply((AtSdhChannel)self))
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            {
            eAtRet ret;
            ret  = XcHidingTryConnectTerminatedVc(self, hideVc);
            ret |= AtChannelBindToPseudowire(hideVc, pseudowire);
            return ret;
            }
        }

    return cAtErrorModeNotSupport;
    }

static uint8 NumberOfSubChannelsGet(AtSdhChannel self)
    {
    if (XcHidingShouldApply(self))
        mClassAttributeGet(AtSdhChannel, NumberOfSubChannelsGet, HideVc);
    return m_AtSdhChannelMethods->NumberOfSubChannelsGet(self);
    }

static AtSdhChannel SubChannelGet(AtSdhChannel self, uint8 subChannelId)
    {
    if (XcHidingShouldApply(self))
        mClassOneParamAttributeGet(AtSdhChannel, SubChannelGet, subChannelId, HideVc);
    return m_AtSdhChannelMethods->SubChannelGet(self, subChannelId);
    }

static AtPrbsEngine XcHidingPrbsEngineGet(AtSdhChannel self)
    {
    AtSdhChannel pairTerVc = (AtSdhChannel)HideVc((AtChannel)self);
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModulePrbs prbsModule = (AtModulePrbs)AtDeviceModuleGet(device, cAtModulePrbs);

    if (pairTerVc == NULL)
        return NULL;

    if (AtModulePrbsAllChannelsSupported(prbsModule))
        return AtChannelPrbsEngineGet((AtChannel)pairTerVc);

    return ThaSdhVcCachedPrbsEngine((ThaSdhVc)pairTerVc);
    }

static AtPrbsEngine PrbsEngineGet(AtChannel self)
    {
    if (XcHidingShouldApply((AtSdhChannel)self))
        return XcHidingPrbsEngineGet((AtSdhChannel)self);

    return NULL;
    }

static void PrbsEngineSet(AtChannel self, AtPrbsEngine engine)
    {
    if (XcHidingShouldApply((AtSdhChannel)self))
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            {
            XcHidingTryConnectTerminatedVc(self, hideVc);
            AtChannelPrbsEngineSet(hideVc, engine);
            }
        }
    }

static void Delete(AtObject self)
    {
    AtPw pw = AtChannelBoundPwGet((AtChannel) self);
    if (pw)
        AtPwCircuitUnbind(pw);

    /* Should Remove PW before delete */
    m_AtObjectMethods->Delete(self);
    }

static eBool NeedConfigurePdh(ThaSdhAuVc self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet TrafficEnable(AtChannel self, eBool enable,
                            eAtRet (*TrafficEnableApi)(AtChannel self, eBool enable),
                            eAtRet (*TrafficEnableMethod)(AtChannel self, eBool enable))
    {
    if (XcHidingShouldApply((AtSdhChannel)self))
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            {
            eAtRet ret;
            ret  = XcHidingTryConnectTerminatedVc(self, hideVc);
            ret |= TrafficEnableApi(hideVc, enable);
            return ret;
            }
        }

    return TrafficEnableMethod(self, enable);
    }

static eBool TrafficIsEnabled(AtChannel self,
                              eBool (*TrafficIsEnabledApi)(AtChannel self),
                              eBool (*TrafficIsEnabledMethods)(AtChannel self))
    {
    if (XcHidingShouldApply((AtSdhChannel)self))
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            return TrafficIsEnabledApi(hideVc);
        }

    return TrafficIsEnabledMethods(self);
    }

static eAtRet TxTrafficEnable(AtChannel self, eBool enable)
    {
    return TrafficEnable(self, enable, AtChannelTxTrafficEnable, m_AtChannelMethods->TxTrafficEnable);
    }

static eBool TxTrafficIsEnabled(AtChannel self)
    {
    return TrafficIsEnabled(self, AtChannelTxTrafficIsEnabled, m_AtChannelMethods->TxTrafficIsEnabled);
    }

static eAtRet RxTrafficEnable(AtChannel self, eBool enable)
    {
    return TrafficEnable(self, enable, AtChannelRxTrafficEnable, m_AtChannelMethods->RxTrafficEnable);
    }

static eBool RxTrafficIsEnabled(AtChannel self)
    {
    return TrafficIsEnabled(self, AtChannelRxTrafficIsEnabled, m_AtChannelMethods->RxTrafficIsEnabled);
    }

static uint8 ActualMapTypeGet(AtSdhChannel self)
    {
    if (XcHidingMode((AtChannel)self) == cTha60290021XcHideModeNone)
        return m_AtSdhChannelMethods->ActualMapTypeGet(self);

    return SdhChannelMapTypeGet(self);
    }

static eAtRet MapFrameModeDefaultSet(ThaSdhAuVc self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet Vc4xPldSet(ThaSdhAuVc self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool NeedPohRestore(ThaSdhVc self)
    {
    eBool noSourceVcs = cAtTrue; /* Maybe no any sources, restore POH after release loopback. */
    uint32 numSourceVcs = ThaSdhVcNumSourceVcs((ThaSdhVc)self);
    uint32 sourceIndex;

    for (sourceIndex = 0; sourceIndex < numSourceVcs; sourceIndex++)
        {
        ThaSdhVc sourceVc = ThaSdhVcSourceVcGet((ThaSdhVc)self, sourceIndex);
        if (sourceVc)
            {
            noSourceVcs = cAtFalse;

            if (Tha60290021ModuleSdhIsLineSideVc((AtSdhChannel)sourceVc))
                continue;

            /* If any terminated source requires POH restore, just do it. */
            if (m_ThaSdhVcMethods->NeedPohRestore(sourceVc))
                return cAtTrue;
            }
        }

    return noSourceVcs;
    }

static eBool XcLoopbackModeIsSupported(AtChannel self, uint8 loopbackMode)
    {
    if (loopbackMode == cAtLoopbackModeRemote)
        return cAtTrue;

    if (loopbackMode == cAtLoopbackModeLocal)
        return (XcHidingMode(self) == cTha60290021XcHideModeDirect) ? cAtTrue : cAtFalse;

    return m_AtChannelMethods->XcLoopbackModeIsSupported(self, loopbackMode);
    }

static eAtRet XcLoopbackRelease(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->XcLoopbackSet(self, cAtLoopbackModeRelease);

    if (XcHidingMode(self) == cTha60290021XcHideModeDirect)
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            ret |= m_AtChannelMethods->XcLoopbackSet(hideVc, cAtLoopbackModeRelease);
        }

    return ret;
    }

static eAtRet XcLoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    eAtLoopbackMode oldloopbackMode = AtChannelLoopbackGet(self);

    switch (loopbackMode)
        {
        case cAtLoopbackModeRelease:
            if (XcHidingMode(self) == cTha60290021XcHideModeDirect)
               {
               AtChannel hideVc = HideVc(self);
               if (hideVc)
                   return XcLoopbackRelease(hideVc);
               }
            return XcLoopbackRelease(self);

        case cAtLoopbackModeRemote:
            if (oldloopbackMode != cAtLoopbackModeRelease)
                XcLoopbackRelease(self);/* only release when it was loopback before */
            return m_AtChannelMethods->XcLoopbackSet(self, loopbackMode);

        case cAtLoopbackModeLocal:
            if (XcHidingMode(self) == cTha60290021XcHideModeDirect)
                {
                AtChannel hideVc = NULL;
                if (oldloopbackMode != cAtLoopbackModeRelease)
                    XcLoopbackRelease(self);

                hideVc = HideVc(self);
                if (hideVc)
                    return m_AtChannelMethods->XcLoopbackSet(hideVc, loopbackMode);
                }
            break;

        default:
            break;
        }

    return cAtErrorModeNotSupport;
    }

static uint8 XcLoopbackGet(AtChannel self)
    {
    AtCrossConnect xc = mMethodsGet(self)->XcEngine(self);

    if (AtCrossConnectHwIsConnected(xc, self, self))
        return cAtLoopbackModeRemote;

    if (XcHidingMode(self) == cTha60290021XcHideModeDirect)
        {
        /* Ask hiding VC for local loopback */
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            return mMethodsGet(hideVc)->XcLoopbackGet(hideVc);
        }

    return cAtLoopbackModeRelease;
    }

static eAtRet AllServicesDestroy(AtChannel self)
    {
    eAtRet ret = cAtOk;
    AtChannel hideVc = HideVc(self);

    if (hideVc)
        ret |= m_AtChannelMethods->AllServicesDestroy(hideVc);

    ret |= m_AtChannelMethods->AllServicesDestroy(self);

    return ret;
    }

static eAtRet BindToEncapChannel(AtChannel self, AtEncapChannel encapChannel)
    {
    if (XcHidingShouldApply((AtSdhChannel)self))
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            {
            eAtRet ret;
            ret  = XcHidingTryConnectTerminatedVc(self, hideVc);
            ret |= AtChannelBindToEncapChannel(hideVc, encapChannel);
            return ret;
            }
        }

    return cAtErrorModeNotSupport;
    }

static eAtRet BindToConcateGroup(AtChannel self, AtConcateGroup group,
                                 eAtRet (*GroupBinder)(AtChannel, AtConcateGroup))
    {
    if (XcHidingShouldApply((AtSdhChannel)self))
        {
        AtChannel hideVc = HideVc(self);
        if (hideVc)
            {
            eAtRet ret;
            ret  = XcHidingTryConnectTerminatedVc(self, hideVc);
            ret |= GroupBinder(hideVc, group);
            return ret;
            }
        }

    return cAtErrorModeNotSupport;
    }

static eAtRet BindToSourceGroup(AtChannel self, AtConcateGroup group)
    {
    return BindToConcateGroup(self, group, AtChannelBindToSourceGroup);
    }

static eAtRet BindToSinkGroup(AtChannel self, AtConcateGroup group)
    {
    return BindToConcateGroup(self, group, AtChannelBindToSinkGroup);
    }

static void OverrideThaSdhAuVc(AtSdhVc self)
    {
    ThaSdhAuVc sdhAuVc = (ThaSdhAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhAuVcOverride, mMethodsGet(sdhAuVc), sizeof(m_ThaSdhAuVcOverride));

        mMethodOverride(m_ThaSdhAuVcOverride, NeedConfigurePdh);
        mMethodOverride(m_ThaSdhAuVcOverride, MapFrameModeDefaultSet);
        mMethodOverride(m_ThaSdhAuVcOverride, Vc4xPldSet);
        }

    mMethodsSet(sdhAuVc, &m_ThaSdhAuVcOverride);
    }

static void OverrideAtObject(AtSdhVc self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(AtSharedDriverOsalGet(), &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, MapTypeIsSupported);
        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        mMethodOverride(m_AtSdhChannelOverride, MapTypeGet);
        mMethodOverride(m_AtSdhChannelOverride, SubChannelGet);
        mMethodOverride(m_AtSdhChannelOverride, NumberOfSubChannelsGet);
        mMethodOverride(m_AtSdhChannelOverride, MapChannelGet);
        mMethodOverride(m_AtSdhChannelOverride, ActualMapTypeGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhPath(AtSdhVc self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, UpsrIsApplicable);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc thaVc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhVcMethods = mMethodsGet(thaVc);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, m_ThaSdhVcMethods, sizeof(m_ThaSdhVcOverride));

        mMethodOverride(m_ThaSdhVcOverride, CdrControllerGet);
        mMethodOverride(m_ThaSdhVcOverride, CdrControllerCreate);
        mMethodOverride(m_ThaSdhVcOverride, HasLoOrderMapping);
        mMethodOverride(m_ThaSdhVcOverride, RedirectCdrController);
        mMethodOverride(m_ThaSdhVcOverride, NeedPohRestore);
        }

    mMethodsSet(thaVc, &m_ThaSdhVcOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, BindToEncapChannel);
        mMethodOverride(m_AtChannelOverride, CanBindToPseudowire);
        mMethodOverride(m_AtChannelOverride, BindToPseudowire);
        mMethodOverride(m_AtChannelOverride, BoundPseudowireGet);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        mMethodOverride(m_AtChannelOverride, TimingModeGet);
        mMethodOverride(m_AtChannelOverride, TimingModeIsSupported);
        mMethodOverride(m_AtChannelOverride, TimingSourceGet);
        mMethodOverride(m_AtChannelOverride, ClockStateGet);
        mMethodOverride(m_AtChannelOverride, ServiceIsRunning);
        mMethodOverride(m_AtChannelOverride, PrbsEngineGet);
        mMethodOverride(m_AtChannelOverride, PrbsEngineSet);
        mMethodOverride(m_AtChannelOverride, TxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, TxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, RxTrafficEnable);
        mMethodOverride(m_AtChannelOverride, RxTrafficIsEnabled);
        mMethodOverride(m_AtChannelOverride, XcLoopbackModeIsSupported);
        mMethodOverride(m_AtChannelOverride, XcLoopbackSet);
        mMethodOverride(m_AtChannelOverride, XcLoopbackGet);
        mMethodOverride(m_AtChannelOverride, AllServicesDestroy);
        mMethodOverride(m_AtChannelOverride, BindToSourceGroup);
        mMethodOverride(m_AtChannelOverride, BindToSinkGroup);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void MethodsInit(Tha6029SdhLineSideAuVc self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, CanBindToPseudowireWithXcHideMode);
        mMethodOverride(m_methods, ConfigLookUpWithXcHideMode);
        }

    mMethodsSet(self, &m_methods);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    OverrideThaSdhVc(self);
    OverrideThaSdhAuVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhLineSideAuVc);
    }

AtSdhVc Tha6029SdhLineSideAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((Tha6029SdhLineSideAuVc)self);
    m_methodsInit = 1;

    return self;
    }

AtSdhChannel Tha6029SdhLineSideAuVcXcHidingPairVc(AtSdhChannel self, AtSdhChannel pairAug)
    {
    if (self)
        return Tha60290021XcHidingPairVcFromAug(self, pairAug);
    return NULL;
    }

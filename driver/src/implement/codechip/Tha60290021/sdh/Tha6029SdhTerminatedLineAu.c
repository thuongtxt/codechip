/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhTerminatedLineAu.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : PWCodechip-6029 AU Terminated line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/Tha60290021Device.h"
#include "Tha6029SdhAuInternal.h"
#include "Tha60290021ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6029SdhTerminatedLineAu
    {
    tTha6029SdhAu super;
    }tTha6029SdhTerminatedLineAu;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods             m_AtSdhChannelOverride;
static tAtSdhPathMethods                m_AtSdhPathOverride;
static tAtChannelMethods                m_AtChannelOverride;
static tTha60210011Tfi5LineAuMethods    m_Tha60210011Tfi5LineAuOverride;

/* Save superclass implementation */
static const tAtSdhChannelMethods      *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool HoldOffTimerSupported(AtSdhChannel self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet HoldOffTimerSet(AtSdhChannel self, uint32 timerInMs)
    {
    AtUnused(self);
    return (timerInMs == 0) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint32 HoldOffTimerGet(AtSdhChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleSdhRet TxSsSet(AtSdhPath self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorModeNotSupport;
    }

static uint8 TxSsGet(AtSdhPath self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleSdhRet ExpectedSsSet(AtSdhPath self, uint8 value)
    {
    AtUnused(self);
    AtUnused(value);
    return cAtErrorModeNotSupport;
    }

static uint8 ExpectedSsGet(AtSdhPath self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModuleSdhRet SsCompareEnable(AtSdhPath self, eBool enable)
    {
    AtUnused(self);
    return (enable == cAtFalse) ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool SsCompareIsEnabled(AtSdhPath self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet SsInsertionEnable(AtSdhPath self, eBool enable)
    {
    AtUnused(self);
    return enable ? cAtErrorModeNotSupport : cAtOk;
    }

static eBool SsInsertionIsEnabled(AtSdhPath self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasPointerProcessor(AtSdhPath self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasPohProcessor(AtSdhPath self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtErrorModeNotSupport;
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtErrorModeNotSupport;
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet RxAlarmForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtErrorModeNotSupport;
    }

static eAtRet RxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtErrorModeNotSupport;
    }

static uint32 RxForcedAlarmGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 RxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static void AlarmDisplay(Tha60210011Tfi5LineAu self)
    {
    AtUnused(self);
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    if ((eThaPhyModule)moduleId == cThaModuleOcn)
        return Tha60290021IsOcnTerminatedSideRegister(AtChannelDeviceGet(self), address);

    return cAtFalse;
    }

static uint32 DefectGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtSdhChannelMode DefaultMode(AtSdhChannel self)
    {
    if (Tha60290021SdhChannelHidingXcIsActive(self))
        return Tha60290021SdhTerminatedChannelHidingXcDefaultMode(self);

    return m_AtSdhChannelMethods->DefaultMode(self);
    }

static void OverrideAtSdhChannel(AtSdhAu self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(sdhChannel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(sdhChannel), sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerSet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerGet);
        mMethodOverride(m_AtSdhChannelOverride, HoldOffTimerSupported);
        mMethodOverride(m_AtSdhChannelOverride, DefaultMode);
        }

    mMethodsSet(sdhChannel, &m_AtSdhChannelOverride);
    }

static void OverrideAtSdhPath(AtSdhAu self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, TxSsGet);
        mMethodOverride(m_AtSdhPathOverride, TxSsSet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedSsSet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedSsGet);
        mMethodOverride(m_AtSdhPathOverride, SsCompareEnable);
        mMethodOverride(m_AtSdhPathOverride, SsCompareIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, SsInsertionEnable);
        mMethodOverride(m_AtSdhPathOverride, SsInsertionIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, HasPointerProcessor);
        mMethodOverride(m_AtSdhPathOverride, HasPohProcessor);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtChannel(AtSdhAu self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, RxAlarmForce);
        mMethodOverride(m_AtChannelOverride, RxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, RxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, RxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideTha60210011Tfi5LineAu(AtSdhAu self)
    {
    Tha60210011Tfi5LineAu au = (Tha60210011Tfi5LineAu)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011Tfi5LineAuOverride, mMethodsGet(au), sizeof(m_Tha60210011Tfi5LineAuOverride));

        mMethodOverride(m_Tha60210011Tfi5LineAuOverride, AlarmDisplay);
        }

    mMethodsSet(au, &m_Tha60210011Tfi5LineAuOverride);
    }

static void Override(AtSdhAu self)
    {
    OverrideAtSdhPath(self);
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    OverrideTha60210011Tfi5LineAu(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhTerminatedLineAu);
    }

static AtSdhAu ObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhAuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAu Tha6029SdhTerminatedLineAuNew(uint32 auId, uint8 auType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAu self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, auId, auType, module);
    }

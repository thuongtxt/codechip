/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290021SdhFaceplateAug.c
 *
 * Created Date: Oct 15, 2016
 *
 * Description : Faceplate AUG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "Tha60290021XcHiding.h"
#include "Tha60290021ModuleSdh.h"
#include "Tha60290021SdhAugInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mSdhModule(self) SdhModule((AtChannel)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tTha60290021SdhLineSideAugMethods m_Tha60290021SdhLineSideAugOverride;

/* Save super implementation */
static const tTha60290021SdhLineSideAugMethods *m_Tha60290021SdhLineSideAugMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleSdh SdhModule(AtChannel self)
    {
    return (AtModuleSdh)AtChannelModuleGet(self);
    }

static AtSdhLine XcHidingLineSideSourcePairLine(Tha60290021SdhLineSideAug self, uint8 pairLocalId)
    {
    return Tha60290021ModuleSdhMateLineGet(mSdhModule(self), pairLocalId);
    }

static eBool XcHidingShouldApply(Tha60290021SdhLineSideAug self, eTha60290021XcHideMode hideMode)
    {
    if (hideMode == cTha60290021XcHideModeFaceplate)
        return cAtTrue;

    return m_Tha60290021SdhLineSideAugMethods->XcHidingShouldApply(self, hideMode);
    }

static ThaModuleOcn OcnModule(Tha60290021SdhLineSideAug self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static uint8 XcHidingLineSideSourcePairLocalLineId(Tha60290021SdhLineSideAug self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;
    AtSdhLine line = AtSdhChannelLineObjectGet((AtSdhChannel)self);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    if ((AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeAug16) && (rate == cAtSdhLineRateStm64))
        {
        ThaModuleOcn ocnModule = OcnModule(self);
        uint32 groupId = Tha60290021ModuleOcnFaceplateLineGroup(ocnModule, AtChannelIdGet((AtChannel)line));
        static const uint8 cNumTerminatedLinesPerStm64 = 4;
        return (uint8)(AtChannelIdGet((AtChannel)self) + (groupId * cNumTerminatedLinesPerStm64));
        }

    return Tha60290021XcHidingPairTerminatedLineLocalId(mSdhModule(self), channel);
    }

static void OverrideTha60290021SdhLineSideAug(AtSdhAug self)
    {
    Tha60290021SdhLineSideAug aug = (Tha60290021SdhLineSideAug)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60290021SdhLineSideAugMethods = mMethodsGet(aug);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021SdhLineSideAugOverride, mMethodsGet(aug), sizeof(m_Tha60290021SdhLineSideAugOverride));

        mMethodOverride(m_Tha60290021SdhLineSideAugOverride, XcHidingLineSideSourcePairLine);
        mMethodOverride(m_Tha60290021SdhLineSideAugOverride, XcHidingShouldApply);
        mMethodOverride(m_Tha60290021SdhLineSideAugOverride, XcHidingLineSideSourcePairLocalLineId);
        }

    mMethodsSet(aug, &m_Tha60290021SdhLineSideAugOverride);
    }

static void Override(AtSdhAug self)
    {
    OverrideTha60290021SdhLineSideAug(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SdhFaceplateAug);
    }

AtSdhAug Tha60290021SdhFaceplateAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290021SdhLineSideAugObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAug Tha60290021SdhFaceplateAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAug newAug = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newAug == NULL)
        return NULL;

    /* Construct it */
    return Tha60290021SdhFaceplateAugObjectInit(newAug, channelId, channelType, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhTerminatedLineTu.c
 *
 * Created Date: Jul 31, 2018
 *
 * Description : Concrete class of PWCodechip-6029xxxx TU.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineTuInternal.h"
#include "Tha60290021ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6029SdhTerminatedLineTu
    {
    tTha60210011Tfi5LineTu super;
    }tTha6029SdhTerminatedLineTu;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtSdhChannelMethods  *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtSdhChannelMode DefaultMode(AtSdhChannel self)
    {
    if (Tha60290021SdhChannelHidingXcIsActive(self))
        return Tha60290021SdhTerminatedChannelHidingXcDefaultMode(self);

    return m_AtSdhChannelMethods->DefaultMode(self);
    }

static void OverrideAtSdhChannel(AtSdhTu self)
    {
    AtSdhChannel path = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(path), sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, DefaultMode);
        }

    mMethodsSet(path, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhTu self)
    {
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhTerminatedLineTu);
    }

static AtSdhTu ObjectInit(AtSdhTu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5LineTuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhTu Tha6029SdhTerminatedLineTuNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhTu self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, channelId, channelType, module);
    }

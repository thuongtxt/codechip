/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6029AuInternal.h
 * 
 * Created Date: Jul 11, 2016
 *
 * Description : PWCodechip-6029 AU internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SDHAUINTERNAL_H_
#define _THA6029SDHAUINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineAuInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SdhAu * Tha6029SdhAu;

typedef struct tTha6029SdhAuMethods
    {
    eBool (*ShouldGetPointerAlarmFromOcnModule)(Tha6029SdhAu self);
    uint32 (*Stschstkram_Base)(Tha6029SdhAu self);
    uint32 (*Stschstaram_Base)(Tha6029SdhAu self);
    eAtRet (*SetSsDefaultValueToFalse)(Tha6029SdhAu self);
    }tTha6029SdhAuMethods;

typedef struct tTha6029SdhAu
    {
    tTha60210011Tfi5LineAu super;
    const tTha6029SdhAuMethods *methods;
    }tTha6029SdhAu;

typedef struct tTha6029SdhMateLineAu
    {
    tTha6029SdhAu super;
    }tTha6029SdhMateLineAu;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAu Tha6029SdhAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhAu Tha6029SdhMateLineAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module);

AtSdhAu Tha6029SdhFacePlateLineAuNew(uint32 auId, uint8 auType, AtModuleSdh module);
AtSdhAu Tha6029SdhMateLineAuNew(uint32 auId, uint8 auType, AtModuleSdh module);
AtSdhAu Tha6029SdhTerminatedLineAuNew(uint32 auId, uint8 auType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SDHAUINTERNAL_H_ */


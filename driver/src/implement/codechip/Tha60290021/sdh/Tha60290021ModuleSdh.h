/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290021ModuleSdh.h
 * 
 * Created Date: Jul 8, 2016
 *
 * Description : PWCodechip-60290021 SDH module.
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021MODULESDH_H_
#define _THA60290021MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/sdh/Tha60210051ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
#define cThaNumFacePlateLine 16

typedef struct tTha60290021ModuleSdh * Tha60290021ModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint8 Tha60290021ModuleSdhNumFaceplateLines(AtModuleSdh self);
AtSdhLine Tha60290021ModuleSdhFaceplateLineGet(AtModuleSdh self, uint8 localLineId);
uint8 Tha60290021ModuleSdhNumMateLines(AtModuleSdh self);
AtSdhLine Tha60290021ModuleSdhMateLineGet(AtModuleSdh self, uint8 localLineId);
uint8 Tha60290021ModuleSdhNumTerminatedLines(AtModuleSdh self);
AtSdhLine Tha60290021ModuleSdhTerminatedLineGet(AtModuleSdh self, uint8 localLineId);

uint8 Tha60290021ModuleSdhNumUseableFaceplateLines(AtModuleSdh self);
uint8 Tha60290021ModuleSdhNumUseableMateLines(AtModuleSdh self);
uint8 Tha60290021ModuleSdhNumUseableTerminatedLines(AtModuleSdh self);

/* Get Rate for FacePlateLine */
eAtSdhLineRate Tha60290021ModuleSdhFaceplateLineRateGet(AtModuleSdh self, uint8 lineId);
eAtRet Tha60290021ModuleSdhFaceplateLineRateSet(AtModuleSdh self, uint8 lineId, eAtSdhLineRate rate);

/* Lines */
AtSdhLine Tha6029SdhFacePlateLineNew(uint32 lineId, AtModuleSdh module);
AtSdhLine Tha6029SdhMateLineNew(uint32 lineId, AtModuleSdh module);
AtSdhLine Tha6029SdhTerminatedLineNew(uint32 lineId, AtModuleSdh module);

/* Aug */
AtSdhAug Tha60290021SdhAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhAug Tha60290021SdhMateAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhAug Tha60290021SdhFaceplateAugNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/* TUG */
AtSdhTug Tha60290021SdhTugNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/* TU3VC */
AtSdhVc Tha6029SdhTerminatedLineTu3VcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/* VC1x */
AtSdhVc Tha6029SdhTerminatedLineVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/* TU */
AtSdhTu Tha6029SdhTerminatedLineTuNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

/* Util */
eBool Tha60290021ModuleSdhLineIsTerminated(AtModuleSdh self, uint8 lineId);
eBool Tha60290021ModuleSdhLineIsMate(AtModuleSdh self, uint8 lineId);
eBool Tha60290021ModuleSdhLineIsFaceplate(AtModuleSdh self, uint8 lineId);
uint8 Tha60290021ModuleSdhTerminatedLineStartId(AtModuleSdh self);
uint8 Tha60290021ModuleSdhMateLineStartId(AtModuleSdh self);
eAtRet Tha60290021ModuleSdhFaceplateLineRateWillChange(AtModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate);
eAtRet Tha60290021ModuleSdhFaceplateLineRateDidChange(AtModuleSdh self, uint8 lineId, eAtSdhLineRate oldRate, eAtSdhLineRate newRate);
eBool Tha60290021ModuleSdhVcIsFromLoBus(AtModuleSdh self, AtSdhChannel vc);
eBool Tha60290021ModuleSdhIsTerminatedVc(AtSdhChannel vc);
eBool Tha60290021ModuleSdhIsLineSideVc(AtSdhChannel vc);

AtSdhChannel Tha60290021SdhLineStm64AuPathFromHwStsGet(AtSdhLine facePlateLine, uint8 slice48, uint8 stsInSlice);
AtSdhChannel Tha60290021SdhLineSubStm16AuPathFromHwStsGet(AtSdhLine facePlateLine, uint8 hwStsInSlice12);
AtSdhPath Tha60290021ModuleSdhLineSideAuPathFromHwIdGet(ThaModuleSdh self, uint8 slice48, uint8 stsInSlice);

eBool Tha60290021ModuleSdhUseNewStsIdConvert(AtModuleSdh self);

/* Work with Hiding XC */
eBool Tha60290021SdhChannelNeedDestroyAllServices(AtSdhChannel channel);
eBool Tha60290021SdhChannelHaveHideChannel(AtSdhChannel channel);
eAtSdhChannelMode Tha60290021SdhTerminatedChannelHidingXcDefaultMode(AtSdhChannel channel);
eBool Tha60290021SdhChannelHidingXcIsActive(AtSdhChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021MODULESDH_H_ */


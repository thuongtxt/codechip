/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhTerminatedLine.c
 *
 * Created Date: Jul 10, 2016
 *
 * Description : PWCodechip-6029 SDH Terminated line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6029SdhLineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods m_AtChannelOverride;
static tAtSdhLineMethods m_AtSdhLineOverride;
static tAtObjectMethods  m_AtObjectOverride;

/* Save super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSerdesController SerdesController(AtSdhLine self)
    {
    AtUnused(self);
    return NULL;
    }

static AtPrbsEngine PrbsEngineGet(AtSdhLine self)
    {
    AtUnused(self);
    return NULL;
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    AtSnprintf(description, sizeof(description), "terminated.%d", AtChannelIdGet((AtChannel)self) + 1);
    return description;
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    return (loopbackMode == cAtLoopbackModeRelease) ? cAtTrue : cAtFalse;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    AtUnused(self);
    return (loopbackMode == cAtLoopbackModeRelease) ? cAtOk : cAtErrorModeNotSupport;
    }

static uint8 LoopbackGet(AtChannel self)
    {
    AtUnused(self);
    return cAtLoopbackModeRelease;
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    /* This is just to hold managed objects, it does not have any line hardware
     * configuration */
    AtUnused(self);
    AtUnused(moduleId);
    AtUnused(address);
    return cAtFalse;
    }

static uint32 DefectGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return 0;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return cAtFalse;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtErrorNotApplicable;
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtOk;
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    return alarmType ? cAtErrorNotApplicable : cAtOk;
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtOk;
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryGet(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryClear(AtChannel self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, AlarmGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtSdhLine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, mMethodsGet(self), sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, SerdesController);
        mMethodOverride(m_AtSdhLineOverride, PrbsEngineGet);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhTerminatedLine);
    }

AtSdhLine Tha6029SdhTerminatedLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha6029SdhTerminatedLineNew(uint32 lineId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6029SdhTerminatedLineObjectInit(newLine, lineId, module);
    }

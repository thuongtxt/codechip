/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6029SdhAuVcInternal.h
 * 
 * Created Date: Jul 11, 2016
 *
 * Description : PWCodechip-6029 AU VC internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SDHAUVCINTERNAL_H_
#define _THA6029SDHAUVCINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210051/sdh/Tha60210051Tfi5LineVcInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SdhAuVc* Tha6029SdhAuVc;

typedef struct tTha6029SdhAuVc
    {
    tTha60210051Tfi5LineAuVc super;

    /* Private data */
    tAtSdhPathApsInfo apsInfo;
    ThaSdhVc *sourceVcs;
    AtList destVcs;
    AtChannel backupSourceVc; /* Backup VC for restore after releasing loopback */
    }tTha6029SdhAuVc;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhVc Tha6029SdhAuVcObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module);

AtSdhVc Tha6029SdhFacePlateLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha6029SdhMateLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha6029SdhTerminatedLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6029SDHAUVCINTERNAL_H_ */


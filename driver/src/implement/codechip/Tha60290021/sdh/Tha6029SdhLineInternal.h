/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information
 * is prohibited except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhLineInternal.h
 *
 * Created Date: Jul 10, 2016
 *
 * Description : PWCodechip-6029 SDH line internal header.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6029SDHLINEINTERNAL_H_
#define _THA6029SDHLINEINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineInternal.h"
#include "Tha60290021ModuleSdh.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6029SdhLine * Tha6029SdhLine;

typedef struct tTha6029SdhLine
    {
    tTha60210011Tfi5Line super;
    }tTha6029SdhLine;

typedef struct tTha6029SdhTerminatedLine
    {
    tTha6029SdhLine super;
    }tTha6029SdhTerminatedLine;

typedef struct tTha6029SdhMateLine
    {
    tTha6029SdhLine super;
    }tTha6029SdhMateLine;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
AtSdhLine Tha6029SdhLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module);
AtSdhLine Tha6029SdhTerminatedLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module);
AtSdhLine Tha6029SdhMateLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module);

#ifdef __cplusplus
}
#endif

#endif /* _THA6029SDHLINEINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH Module
 *
 * File        : Tha6029SdhTerminatedLineVc1x.c
 *
 * Created Date: Nov 21, 2016
 *
 * Description : SDH Module for VC1x
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pmc/ThaModulePmc.h"
#include "Tha60290021ModuleSdh.h"
#include "Tha6029SdhTerminatedLineVc1xInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods      m_AtChannelOverride;
static tAtSdhChannelMethods   m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtChannelMethods     *m_AtChannelMethods = NULL;
static const tAtSdhChannelMethods  *m_AtSdhChannelMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule PmcModule(AtChannel self)
    {
    return (AtModule)AtDeviceModuleGet(AtChannelDeviceGet(self), cThaModulePmc);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (ThaSdhChannelShouldGetCounterFromPmc(self))
        return ThaModulePmcSdhTuVc1xCounterGet(PmcModule(self), (AtSdhChannel)self, counterType, cAtFalse);

    return m_AtChannelMethods->CounterGet(self, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (ThaSdhChannelShouldGetCounterFromPmc(self))
        return ThaModulePmcSdhTuVc1xCounterGet(PmcModule(self), (AtSdhChannel)self, counterType, cAtTrue);

    return m_AtChannelMethods->CounterClear(self, counterType);
    }

static eAtSdhChannelMode DefaultMode(AtSdhChannel self)
    {
    if (Tha60290021SdhChannelHidingXcIsActive(self))
        return Tha60290021SdhTerminatedChannelHidingXcDefaultMode(self);

    return m_AtSdhChannelMethods->DefaultMode(self);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));
        mMethodOverride(m_AtSdhChannelOverride, DefaultMode);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtSdhChannel(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhTerminatedLineVc1x);
    }

AtSdhVc Tha6029SdhTerminatedLineVc1xObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210051Tfi5LineVc1xObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha6029SdhTerminatedLineVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha6029SdhTerminatedLineVc1xObjectInit(self, channelId, channelType, module);
    }

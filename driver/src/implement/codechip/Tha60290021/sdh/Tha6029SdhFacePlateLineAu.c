/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhFacePlateLineAu.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : PWCodechip-6029 AU Faceplate line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhLineInternal.h"
#include "../man/Tha60290021Device.h"
#include "../ocn/Tha60290021ModuleOcnReg.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "Tha6029SdhFacePlateLineAuInternal.h"
#include "AtCrossConnect.h"
#include "Tha60290021ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods                 m_AtObjectOverride;
static tAtChannelMethods                m_AtChannelOverride;
static tAtSdhChannelMethods             m_AtSdhChannelOverride;
static tAtSdhPathMethods                m_AtSdhPathOverride;
static tTha6029SdhAuMethods             m_Tha6029SdhAuOverride;

/* Save super implementation */
static const tAtObjectMethods               *m_AtObjectMethods     = NULL;
static const tAtChannelMethods              *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods           *m_AtSdhChannelMethods = NULL;
static const tTha6029SdhAuMethods           *m_Tha6029SdhAuMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    if ((eThaPhyModule)moduleId == cThaModuleOcn)
        return Tha60290021IsOcnFaceplateSideRegister(AtChannelDeviceGet(self), address);

    return cAtFalse;
    }

static eBool ShouldGetPointerAlarmFromOcnModule(Tha6029SdhAu self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);

    /* If POH is there, path alarms are redirected to POH */
    if (Tha60290021ModuleOcnPohProcessorSideGet(ocnModule) == cTha6029LineSideFaceplate)
        return cAtFalse;

    return cAtTrue;
    }

static uint32 Stschstkram_Base(Tha6029SdhAu self)
    {
    AtUnused(self);
    return cAf6Reg_upstschstkram_Base;
    }

static uint32 Stschstaram_Base(Tha6029SdhAu self)
    {
    AtUnused(self);
    return cAf6Reg_upstschstaram_Base;
    }

static eBool HasPohProcessor(AtSdhPath self)
    {
    AtSdhPath vc = (AtSdhPath)AtSdhChannelSubChannelGet((AtSdhChannel)self, 0);
    return AtSdhPathHasPohProcessor(vc);
    }

static AtCrossConnect CrossConnect(AtSdhChannel self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    AtModuleXc xcModule = (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    return AtModuleXcVcCrossConnectGet(xcModule);
    }

static eAtRet XcDisConnect(AtSdhChannel self)
    {
    AtChannel dest =  (AtChannel)self;
    AtCrossConnect xc = CrossConnect(self);
    AtChannel source = AtCrossConnectSourceChannelGet(xc, dest);
    if (source)
        return AtCrossConnectChannelDisconnect(xc, source, dest);
    return cAtOk;
    }

static void WillDelete(AtObject self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    /* Should-Remove-Cross-Connect and terminate service
     * Need Consider move that logic to device init after Unbind all PWs */
    if (AtSdhChannelTypeGet(channel) == cAtSdhChannelTypeAu4_nc)
        XcDisConnect(AtSdhChannelSubChannelGet(channel, 0));
    }

static uint32 AddressWithLocalAddress(AtChannel self, uint32 localAddress)
    {
    uint32 offset = Tha60210011ModuleOcnStsDefaultOffset((AtSdhChannel)self);
    return localAddress + offset;
    }

static uint32 PiPointerEventRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_upstschstkram_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 events = 0;

    if (regVal & cAf6_upstschstkram_LineStsPiStsNewDetIntr_Mask)
        events |= cAtSdhPathAlarmNewPointer;

    if (regVal & cAf6_upstschstkram_LineStsPiStsNdfIntr_Mask)
        events |= cAtSdhPathAlarmPiNdf;

    if (read2Clear)
        {
        regVal = cAf6_upstschstkram_LineStsPiStsNewDetIntr_Mask |
                 cAf6_upstschstkram_LineStsPiStsNdfIntr_Mask;
        mChannelHwWrite(self, regAddr, regVal, cThaModuleOcn);
        }

    return events;
    }

static uint32 PgPointerEventRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 regAddr = AddressWithLocalAddress(self, cAf6Reg_stspgstkram_Base);
    uint32 regVal = mChannelHwRead(self, regAddr, cThaModuleOcn);
    uint32 events = 0;

    if (regVal & cAf6_stspgstkram_LineStsPgNdfIntr_Mask)
        events |= cAtSdhPathAlarmPgNdf;

    if (read2Clear)
        mChannelHwWrite(self, regAddr, cAf6_stspgstkram_LineStsPgNdfIntr_Mask, cThaModuleOcn);

    return events;
    }

static uint32 PointerEventRead2Clear(AtChannel self, eBool read2Clear)
    {
    uint32 piEvents = PiPointerEventRead2Clear(self, read2Clear);
    uint32 pgEvents = PgPointerEventRead2Clear(self, read2Clear);
    return piEvents | pgEvents;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    uint32 defects = 0;

    defects |= m_AtChannelMethods->DefectHistoryGet(self);
    defects |= PointerEventRead2Clear(self, cAtFalse);

    return defects;
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    uint32 defects = 0;

    defects |= m_AtChannelMethods->DefectHistoryClear(self);
    defects |= PointerEventRead2Clear(self, cAtTrue);

    return defects;
    }

static AtModuleSdh SdhModule(AtSdhChannel self)
    {
    return (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    }

static uint8 LocalIdGet(AtSdhLine line)
    {
    return ThaModuleSdhLineIdLocalId((ThaModuleSdh)SdhModule((AtSdhChannel)line), (uint8)AtChannelIdGet((AtChannel)line));
    }

static AtSdhChannel HideChannelGet(AtSdhChannel self)
    {
    AtSdhLine line = AtSdhChannelLineObjectGet(self);

    /* STM-0/AU-3 */
    if (AtSdhLineRateGet(line) == cAtSdhLineRateStm0)
        {
        AtSdhLine hideLine = AtSdhLineHideLineGet(line, 0);
        uint8 aug1Id = (uint8)((LocalIdGet(line) % 2) * 8); /* AUG-1 #0 and #8 of STM16 */

        return (AtSdhChannel)AtSdhLineAu3Get(hideLine, aug1Id, 0);
        }

    return m_AtSdhChannelMethods->HideChannelGet(self);
    }

static void OverrideAtChannel(AtSdhAu self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, mMethodsGet(channel), sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtSdhAu self)
    {
    AtObject object = (AtObject)self;


    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, WillDelete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideTha6029SdhAu(AtSdhAu self)
    {
    Tha6029SdhAu au = (Tha6029SdhAu)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6029SdhAuMethods = mMethodsGet(au);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SdhAuOverride, m_Tha6029SdhAuMethods, sizeof(m_Tha6029SdhAuOverride));

        mMethodOverride(m_Tha6029SdhAuOverride, ShouldGetPointerAlarmFromOcnModule);
        mMethodOverride(m_Tha6029SdhAuOverride, Stschstkram_Base);
        mMethodOverride(m_Tha6029SdhAuOverride, Stschstaram_Base);
        }

    mMethodsSet(au, &m_Tha6029SdhAuOverride);
    }

static void OverrideAtSdhPath(AtSdhAu self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, HasPohProcessor);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideAtSdhChannel(AtSdhAu self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, mMethodsGet(channel), sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, HideChannelGet);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhAu self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    OverrideTha6029SdhAu(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhFacePlateLineAu);
    }

AtSdhAu Tha6029SdhFacePlateLineAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhAuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAu Tha6029SdhFacePlateLineAuNew(uint32 auId, uint8 auType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAu self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return Tha6029SdhFacePlateLineAuObjectInit(self, auId, auType, module);
    }

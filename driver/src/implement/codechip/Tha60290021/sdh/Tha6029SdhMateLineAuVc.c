/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhMateLineAuVc.c
 *
 * Created Date: Jul 11, 2016
 *
 * Description : PWCodechip-6029 AU VC Mate line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/Tha60290021Device.h"
#include "../ocn/Tha60290021ModuleOcnReg.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "Tha6029SdhLineSideAuVcInternal.h"
#include "Tha60290021SdhLineSideAug.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mPohProcessor(self) mMethodsGet((ThaSdhVc)self)->PohProcessorGet((ThaSdhVc)self)
#define mXcModule(self) XcModule((AtChannel)self)
#define mIsMateXcHiding(self) ((XcHidingMode((AtChannel)self) == cTha60290021XcHideModeMate) ? cAtTrue : cAtFalse)

#define mClassOneParamAttributeGet(class, method, value, pairPathGetFunction)  \
    do                                                                         \
        {                                                                      \
        class pairPath = (class)pairPathGetFunction((AtSdhChannel)self);       \
        if (pairPath)                                                          \
            return class##method(pairPath, value);                             \
        return m_##class##Methods->method(self, value);                        \
        }while(0)

#define mClassAttributeGet(class, method, pairPathGetFunction)                 \
    do                                                                         \
        {                                                                      \
        class pairPath = (class)pairPathGetFunction((AtSdhChannel)self);       \
        if (pairPath)                                                          \
            return class##method(pairPath);                                    \
        return m_##class##Methods->method(self);                               \
        }while(0)

#define mClassAttributeSet(class, method, value, pairPathGetFunction)          \
    do                                                                         \
        {                                                                      \
        class pairPath = (class)pairPathGetFunction((AtSdhChannel)self);       \
        if (pairPath)                                                          \
            return class##method(pairPath, value);                             \
        return m_##class##Methods->method(self, value);                        \
        }while(0)

#define mClassTwoParamsAttributeSet(class, method, param, value, pairPathGetFunction) \
    do                                                                         \
        {                                                                      \
        class pairPath = (class)pairPathGetFunction((AtSdhChannel)self);       \
        if (pairPath)                                                          \
            return class##method(pairPath, param, value);                      \
        return m_##class##Methods->method(self, param, value);                 \
        }while(0)

#define mRxTwoParamsAttributeSet(class, method, param, value) mClassTwoParamsAttributeSet(class, method, param, value, XcHidingMonitoredChannel)
#define mRxOneParamAttributeGet(class, method, param) mClassOneParamAttributeGet(class, method, param, XcHidingMonitoredChannel)
#define mRxAttributeGet(class, method)         mClassAttributeGet(class, method, XcHidingMonitoredChannel)
#define mRxAttributeSet(class, method, value)  mClassAttributeSet(class, method, value, XcHidingMonitoredChannel)
#define mTxTwoParamsAttributeSet(class, method, param, value) mClassTwoParamsAttributeSet(class, method, param, value, XcHidingInsertedChannel)
#define mTxAttributeGet(class, method)         mClassAttributeGet(class, method, XcHidingInsertedChannel)
#define mTxAttributeSet(class, method, value)  mClassAttributeSet(class, method, value, XcHidingInsertedChannel)
#define mTxOneParamAttributeGet(class, method, param) mClassOneParamAttributeGet(class, method, param, XcHidingInsertedChannel)

/*--------------------------- Local Typedefs ---------------------------------*/
typedef struct tTha6029SdhMateLineAuVc
    {
    tTha6029SdhLineSideAuVc super;
    }tTha6029SdhMateLineAuVc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods               m_AtChannelOverride;
static tAtSdhChannelMethods            m_AtSdhChannelOverride;
static tAtSdhPathMethods               m_AtSdhPathOverride;
static tThaSdhVcMethods                m_ThaSdhVcOverride;
static tTha6029SdhLineSideAuVcMethods  m_Tha6029SdhLineSideAuVcOverride;

/* Super implementation */
static const tAtChannelMethods              *m_AtChannelMethods              = NULL;
static const tAtSdhChannelMethods           *m_AtSdhChannelMethods           = NULL;
static const tAtSdhPathMethods              *m_AtSdhPathMethods              = NULL;
static const tThaSdhVcMethods               *m_ThaSdhVcMethods               = NULL;
static const tTha6029SdhLineSideAuVcMethods *m_Tha6029SdhLineSideAuVcMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPohProcessor PohProcessorCreate(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);

    if (Tha60290021ModuleOcnPohProcessorSideGet(ocnModule) == cTha6029LineSideMate)
        return m_ThaSdhVcMethods->PohProcessorCreate(self);

    return NULL;
    }

static ThaModuleOcn OcnModule(ThaSdhVc self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (ThaModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static AtModuleXc XcModule(AtChannel self)
    {
    return (AtModuleXc)AtDeviceModuleGet(AtChannelDeviceGet(self), cAtModuleXc);
    }

static eTha60290021XcHideMode XcHidingMode(AtChannel self)
    {
    return Tha60290021ModuleXcHideModeGet(XcModule(self));
    }

static AtSdhChannel XcHidingLineSidePairSourceVc(AtSdhChannel self)
    {
    AtSdhChannel aug = AtSdhVcAugGet(self);
    AtSdhChannel pairAug = Tha60290021SdhLineSideAugXcHdingLineSideSourcePairAug(aug);
    return Tha6029SdhLineSideAuVcXcHidingPairVc(self, pairAug);
    }

static AtSdhChannel XcHidingLineSidePairDestVc(AtSdhChannel self)
    {
    AtSdhChannel aug = AtSdhVcAugGet(self);
    AtSdhChannel pairAug = Tha60290021SdhLineSideAugXcHdingLineSideDestPairAug(aug);
    return Tha6029SdhLineSideAuVcXcHidingPairVc(self, pairAug);
    }

static eBool HasPohProcessor(ThaSdhVc self)
    {
    if (Tha60290021ModuleOcnPohProcessorSideGet(OcnModule(self)) == cTha6029LineSideMate)
        return cAtTrue;
    return cAtFalse;
    }

static ThaPohProcessor PohProcessorGet(ThaSdhVc self)
    {
    if (HasPohProcessor(self))
        return m_ThaSdhVcMethods->PohProcessorGet(self);
    return NULL;
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    if ((eThaPhyModule)moduleId == cThaModuleOcn)
        return Tha60290021IsOcnTfi5SideRegister(AtChannelDeviceGet(self), address);

    return cAtFalse;
    }

static eAtRet CanBindToPseudowireWithXcHideMode(Tha6029SdhLineSideAuVc self, AtPw pseudowire, eTha60290021XcHideMode mode)
    {
    if (mode == cTha60290021XcHideModeMate)
        return cAtOk;

    return m_Tha6029SdhLineSideAuVcMethods->CanBindToPseudowireWithXcHideMode(self, pseudowire, mode);
    }

static AtSdhChannel XcHidingMonitoredChannel(AtSdhChannel self)
    {
    if (mIsMateXcHiding(self))
        return XcHidingLineSidePairDestVc(self);
    return NULL;
    }

static AtSdhChannel XcHidingInsertedChannel(AtSdhChannel self)
    {
    if (mIsMateXcHiding(self))
        return XcHidingLineSidePairSourceVc(self);
    return NULL;
    }

static uint8 RxPslGet(AtSdhPath self)
    {
    mRxAttributeGet(AtSdhPath, RxPslGet);
    }

static uint8 ExpectedPslGet(AtSdhPath self)
    {
    mRxAttributeGet(AtSdhPath, ExpectedPslGet);
    }

static eAtModuleSdhRet ExpectedPslSet(AtSdhPath self, uint8 psl)
    {
    mRxAttributeSet(AtSdhPath, ExpectedPslSet, psl);
    }

static eAtModuleSdhRet TxPslSet(AtSdhPath self, uint8 psl)
    {
    mTxAttributeSet(AtSdhPath, TxPslSet, psl);
    }

static uint8 TxPslGet(AtSdhPath self)
    {
    mTxAttributeGet(AtSdhPath, TxPslGet);
    }

static eAtModuleSdhRet ERdiEnable(AtSdhPath self, eBool enable)
    {
    mTxAttributeSet(AtSdhPath, ERdiEnable, enable);
    }

static eBool ERdiIsEnabled(AtSdhPath self)
    {
    mTxAttributeGet(AtSdhPath, ERdiIsEnabled);
    }

static eBool ERdiDatabaseIsEnabled(AtSdhPath self)
    {
    mTxAttributeGet(AtSdhPath, ERdiDatabaseIsEnabled);
    }

static eAtModuleSdhRet PlmMonitorEnable(AtSdhPath self, eBool enable)
    {
    mRxAttributeSet(AtSdhPath, PlmMonitorEnable, enable);
    }

static eBool PlmMonitorIsEnabled(AtSdhPath self)
    {
    mRxAttributeGet(AtSdhPath, PlmMonitorIsEnabled);
    }

static eAtRet PohMonitorEnable(AtSdhPath self, eBool enabled)
    {
    mRxAttributeSet(AtSdhPath, PohMonitorEnable, enabled);
    }

static eBool PohMonitorIsEnabled(AtSdhPath self)
    {
    mRxAttributeGet(AtSdhPath, PohMonitorIsEnabled);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    mRxOneParamAttributeGet(AtChannel, CounterGet, counterType);
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    mRxOneParamAttributeGet(AtChannel, CounterClear, counterType);
    }

static uint32 DefectGet(AtChannel self)
    {
    mRxAttributeGet(AtChannel, DefectGet);
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    mRxAttributeGet(AtChannel, DefectHistoryGet);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    mRxAttributeGet(AtChannel, DefectHistoryClear);
    }

static uint32 SupportedInterruptMasks(AtChannel self)
    {
	AtChannel pairPath = (AtChannel) XcHidingMonitoredChannel((AtSdhChannel) self);
	AtChannel channel = pairPath ? pairPath : self;
	uint32 mask = 0;
	ThaCdrController cdrController;

    cdrController = ThaSdhVcCdrControllerGet((AtSdhVc)channel);
    if (cdrController)
        mask |= cAtSdhPathAlarmClockStateChange;

    if (mPohProcessor(channel))
        {
        mask |= (cAtSdhPathAlarmAis   |
                 cAtSdhPathAlarmTim   |
                 cAtSdhPathAlarmPlm   |
                 cAtSdhPathAlarmUneq  |
                 cAtSdhPathAlarmRdi   |
                 cAtSdhPathAlarmErdiS |
                 cAtSdhPathAlarmErdiP |
                 cAtSdhPathAlarmErdiC |
                 cAtSdhPathAlarmRfi);
        }

    return mask;
    }

static eAtRet InterruptMaskSet(AtChannel self, uint32 defectMask, uint32 enableMask)
    {
    mRxTwoParamsAttributeSet(AtChannel, InterruptMaskSet, defectMask, enableMask);
    }

static uint32 InterruptMaskGet(AtChannel self)
    {
    mRxAttributeGet(AtChannel, InterruptMaskGet);
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    mTxAttributeSet(AtChannel, TxAlarmForce, alarmType);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    mTxAttributeSet(AtChannel, TxAlarmUnForce, alarmType);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    mTxAttributeGet(AtChannel, TxForcedAlarmGet);
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    mTxAttributeSet(AtChannel, TxErrorForce, errorType);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    mTxAttributeSet(AtChannel, TxErrorUnForce, errorType);
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    mTxAttributeGet(AtChannel, TxForcedErrorGet);
    }

static eAtModuleSdhRet TxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    mTxOneParamAttributeGet(AtSdhChannel, TxTtiGet, tti);
    }

static eAtModuleSdhRet TxTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    mTxAttributeSet(AtSdhChannel, TxTtiSet, tti);
    }

static eAtModuleSdhRet ExpectedTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    mRxOneParamAttributeGet(AtSdhChannel, ExpectedTtiGet, tti);
    }

static eAtModuleSdhRet ExpectedTtiSet(AtSdhChannel self, const tAtSdhTti *tti)
    {
    mRxAttributeSet(AtSdhChannel, ExpectedTtiSet, tti);
    }

static eAtModuleSdhRet RxTtiGet(AtSdhChannel self, tAtSdhTti *tti)
    {
    mRxOneParamAttributeGet(AtSdhChannel, RxTtiGet, tti);
    }

static eAtModuleSdhRet AlarmAffectingEnable(AtSdhChannel self, uint32 alarmType, eBool enable)
    {
    mRxTwoParamsAttributeSet(AtSdhChannel, AlarmAffectingEnable, alarmType, enable);
    }

static eBool AlarmAffectingIsEnabled(AtSdhChannel self, uint32 alarmType)
    {
    mRxOneParamAttributeGet(AtSdhChannel, AlarmAffectingIsEnabled, alarmType);
    }

static uint32 AlarmAffectingMaskGet(AtSdhChannel self)
    {
    mRxAttributeGet(AtSdhChannel, AlarmAffectingMaskGet);
    }

static eAtRet AutoRdiEnable(AtSdhChannel self, eBool enable)
    {
    mRxAttributeSet(AtSdhChannel, AutoRdiEnable, enable);
    }

static eBool AutoRdiIsEnabled(AtSdhChannel self)
    {
    mRxAttributeGet(AtSdhChannel, AutoRdiIsEnabled);
    }

static eAtModuleSdhRet TimMonitorEnable(AtSdhChannel self, eBool enable)
    {
    mRxAttributeSet(AtSdhChannel, TimMonitorEnable, enable);
    }

static eBool TimMonitorIsEnabled(AtSdhChannel self)
    {
    mRxAttributeGet(AtSdhChannel, TimMonitorIsEnabled);
    }

static uint32 BlockErrorCounterGet(AtSdhChannel self, uint16 counterType)
    {
    mRxOneParamAttributeGet(AtSdhChannel, BlockErrorCounterGet, counterType);
    }

static uint32 BlockErrorCounterClear(AtSdhChannel self, uint16 counterType)
    {
    mRxOneParamAttributeGet(AtSdhChannel, BlockErrorCounterClear, counterType);
    }

static eAtModuleSdhRet TxOverheadByteSet(AtSdhChannel self, uint32 overheadByte, uint8 overheadByteValue)
    {
    mTxTwoParamsAttributeSet(AtSdhChannel, TxOverheadByteSet, overheadByte, overheadByteValue);
    }

static uint8 TxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    mTxOneParamAttributeGet(AtSdhChannel, TxOverheadByteGet, overheadByte);
    }

static uint8 RxOverheadByteGet(AtSdhChannel self, uint32 overheadByte)
    {
    mRxOneParamAttributeGet(AtSdhChannel, RxOverheadByteGet, overheadByte);
    }

static eAtModuleSdhRet HideXcModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    AtSdhChannel pairPath;
    eAtRet ret = cAtOk;

    pairPath = XcHidingMonitoredChannel(self);
    if (pairPath)
        ret |= AtSdhChannelModeSet(pairPath, mode);

    pairPath = XcHidingInsertedChannel(self);
    if (pairPath)
        ret |= AtSdhChannelModeSet(pairPath, mode);

    return ret;
    }

static eAtModuleSdhRet ModeSet(AtSdhChannel self, eAtSdhChannelMode mode)
    {
    if (XcHidingMode((AtChannel)self) != cTha60290021XcHideModeNone)
        return HideXcModeSet(self, mode);

    return m_AtSdhChannelMethods->ModeSet(self, mode);
    }

static eAtModuleSdhRet TtiCompareEnable(AtSdhChannel self, eBool enable)
    {
    mRxAttributeSet(AtSdhChannel, TtiCompareEnable, enable);
    }

static eBool TtiCompareIsEnabled(AtSdhChannel self)
    {
    mRxAttributeGet(AtSdhChannel, TtiCompareIsEnabled);
    }

static eBool SsBitCompareIsEnabledAsDefault(AtSdhPath self)
    {
    return AtSdhPathSsBitCompareIsEnabledAsDefault((AtSdhPath)AtSdhChannelParentChannelGet((AtSdhChannel)self));
    }

static void OverrideAtSdhPath(AtSdhVc self)
    {
    AtSdhPath path = (AtSdhPath)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhPathMethods = mMethodsGet(path);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhPathOverride, mMethodsGet(path), sizeof(m_AtSdhPathOverride));

        mMethodOverride(m_AtSdhPathOverride, RxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslGet);
        mMethodOverride(m_AtSdhPathOverride, ExpectedPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslSet);
        mMethodOverride(m_AtSdhPathOverride, TxPslGet);
        mMethodOverride(m_AtSdhPathOverride, ERdiEnable);
        mMethodOverride(m_AtSdhPathOverride, ERdiIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, ERdiDatabaseIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, PlmMonitorEnable);
        mMethodOverride(m_AtSdhPathOverride, PlmMonitorIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorEnable);
        mMethodOverride(m_AtSdhPathOverride, PohMonitorIsEnabled);
        mMethodOverride(m_AtSdhPathOverride, SsBitCompareIsEnabledAsDefault);
        }

    mMethodsSet(path, &m_AtSdhPathOverride);
    }

static void OverrideTha6029SdhLineSideAuVc(AtSdhVc self)
    {
    Tha6029SdhLineSideAuVc vc = (Tha6029SdhLineSideAuVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6029SdhLineSideAuVcMethods = mMethodsGet(vc);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SdhLineSideAuVcOverride, mMethodsGet(vc), sizeof(m_Tha6029SdhLineSideAuVcOverride));

        mMethodOverride(m_Tha6029SdhLineSideAuVcOverride, CanBindToPseudowireWithXcHideMode);
        }

    mMethodsSet(vc, &m_Tha6029SdhLineSideAuVcOverride);
    }

static void OverrideThaSdhVc(AtSdhVc self)
    {
    ThaSdhVc thaVc = (ThaSdhVc)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhVcMethods = mMethodsGet(thaVc);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhVcOverride, m_ThaSdhVcMethods, sizeof(m_ThaSdhVcOverride));

        mMethodOverride(m_ThaSdhVcOverride, PohProcessorCreate);
        mMethodOverride(m_ThaSdhVcOverride, PohProcessorGet);
        }

    mMethodsSet(thaVc, &m_ThaSdhVcOverride);
    }

static void OverrideAtChannel(AtSdhVc self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, SupportedInterruptMasks);
        mMethodOverride(m_AtChannelOverride, InterruptMaskSet);
        mMethodOverride(m_AtChannelOverride, InterruptMaskGet);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtSdhChannel(AtSdhVc self)
    {
    AtSdhChannel channel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, TxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, TxTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, ExpectedTtiSet);
        mMethodOverride(m_AtSdhChannelOverride, RxTtiGet);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingEnable);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, AlarmAffectingMaskGet);
        mMethodOverride(m_AtSdhChannelOverride, AutoRdiEnable);
        mMethodOverride(m_AtSdhChannelOverride, AutoRdiIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorEnable);
        mMethodOverride(m_AtSdhChannelOverride, TimMonitorIsEnabled);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterGet);
        mMethodOverride(m_AtSdhChannelOverride, BlockErrorCounterClear);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteSet);
        mMethodOverride(m_AtSdhChannelOverride, TxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, RxOverheadByteGet);
        mMethodOverride(m_AtSdhChannelOverride, ModeSet);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareEnable);
        mMethodOverride(m_AtSdhChannelOverride, TtiCompareIsEnabled);
        }

    mMethodsSet(channel, &m_AtSdhChannelOverride);
    }

static void Override(AtSdhVc self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    OverrideAtSdhPath(self);
    OverrideThaSdhVc(self);
    OverrideTha6029SdhLineSideAuVc(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhMateLineAuVc);
    }

static AtSdhVc ObjectInit(AtSdhVc self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhLineSideAuVcObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhVc Tha6029SdhMateLineAuVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhVc self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, channelId, channelType, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha60290021SdhAugInternal.h
 * 
 * Created Date: Oct 15, 2016
 *
 * Description : AUG
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290021SDHAUGINTERNAL_H_
#define _THA60290021SDHAUGINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineAugInternal.h"
#include "../xc/Tha60290021ModuleXc.h"
#include "Tha60290021SdhLineSideAug.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290021SdhAug
    {
    tTha60210011Tfi5LineAug super;
    }tTha60290021SdhAug;

typedef struct tTha60290021SdhLineSideAugMethods
    {
    AtSdhLine (*XcHidingLineSideSourcePairLine)(Tha60290021SdhLineSideAug self, uint8 pairLocalId);
    uint8 (*XcHidingLineSideSourcePairLocalLineId)(Tha60290021SdhLineSideAug self);
    AtSdhLine (*XcHidingLineSideDestPairLine)(Tha60290021SdhLineSideAug self, uint8 pairLocalId);
    uint8 (*XcHidingLineSideDestPairLocalLineId)(Tha60290021SdhLineSideAug self);
    eBool (*XcHidingShouldApply)(Tha60290021SdhLineSideAug self, eTha60290021XcHideMode hideMode);

    AtSdhChannel (*HideAug64Get)(Tha60290021SdhLineSideAug self, uint8 mapType);
    uint8 (*StartSts1InTerminatedLineGet)(Tha60290021SdhLineSideAug self);
    }tTha60290021SdhLineSideAugMethods;

typedef struct tTha60290021SdhLineSideAug
    {
    tTha60290021SdhAug super;
    const tTha60290021SdhLineSideAugMethods *methods;
    }tTha60290021SdhLineSideAug;

typedef struct tTha60290021SdhFaceplateAug
    {
    tTha60290021SdhLineSideAug super;
    }tTha60290021SdhFaceplateAug;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAug Tha60290021SdhAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhAug Tha60290021SdhLineSideAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhAug Tha60290021SdhFaceplateAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290021SDHAUGINTERNAL_H_ */


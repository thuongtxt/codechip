/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290021SdhTug.c
 *
 * Created Date: Oct 16, 2016
 *
 * Description : TUG
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhVcInternal.h"
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineTugInternal.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../xc/Tha60290021ModuleXc.h"
#include "Tha60290021ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60290021SdhTug
    {
    tTha60210011Tfi5LineTug super;
    }tTha60290021SdhTug;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tAtSdhChannelMethods m_AtSdhChannelOverride;

/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtSdhChannelMethods *m_AtSdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModuleXc XcModule(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static eTha60290021XcHideMode XcHidingMode(AtChannel self)
    {
    return Tha60290021ModuleXcHideModeGet(XcModule(self));
    }

static eBool XcIsHiding(AtChannel self)
    {
    eTha60290021XcHideMode hideMode = XcHidingMode(self);
    if (hideMode == cTha60290021XcHideModeNone)
        return cAtFalse;
    return cAtTrue;
    }

static AtSdhChannel ParentVc(AtSdhChannel self)
    {
    AtSdhChannel parent = AtSdhChannelParentChannelGet(self);

    if (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTug3)
        parent = AtSdhChannelParentChannelGet(parent);

    if (AtSdhChannelIsVc(parent))
        return parent;

    /* This is impossible */
    return NULL;
    }

static AtSdhChannel XcHidingSourceVc(AtSdhChannel self)
    {
    AtCrossConnect xc = AtModuleXcVcCrossConnectGet(XcModule((AtChannel)self));
    AtSdhChannel parentVc = ParentVc(self);
    AtSdhChannel sourceVc;
    eTha60290021XcHideMode hideMode;

    if (parentVc == NULL)
        return NULL;

    sourceVc = (AtSdhChannel)AtCrossConnectSourceChannelGet(xc, (AtChannel)parentVc);
    if (sourceVc == NULL)
        return NULL;

    hideMode = XcHidingMode((AtChannel)self);
    if (hideMode == cTha60290021XcHideModeMate)
        sourceVc = (AtSdhChannel)AtCrossConnectSourceChannelGet(xc, (AtChannel)sourceVc);

    return sourceVc;
    }

static AtSdhChannel XcHidingParentForIdDescription(AtSdhChannel self)
    {
    AtSdhChannel sourceVc = XcHidingSourceVc(self);

    if (sourceVc == NULL)
        return NULL;

    if (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeTug3)
        return sourceVc;

    /* TUG-2 can have TUG-3 or VC-3 as its parent */
    if (AtSdhChannelTypeGet(sourceVc) == cAtSdhChannelTypeVc4)
        return AtSdhChannelParentChannelGet(self);

    return sourceVc;
    }

static const char *IdString(AtChannel self)
    {
    if (XcIsHiding(self))
        {
        AtSdhChannel parent = XcHidingParentForIdDescription((AtSdhChannel)self);
        if (parent)
            {
            static char idString[16];
            AtSprintf(idString, "%s.%u", AtChannelIdString((AtChannel)parent), AtChannelIdGet(self) + 1);
            return idString;
            }
        }

    return m_AtChannelMethods->IdString(self);
    }

static eAtModuleSdhRet MapTypeSet(AtSdhChannel self, uint8 mapType)
    {
    eAtRet ret;

    ret = m_AtSdhChannelMethods->MapTypeSet(self, mapType);
    if (ret != cAtOk)
        return ret;

    if (AtSdhChannelTypeGet(self) == cAtSdhChannelTypeTug3)
        {
        AtSdhChannel vc4 = AtSdhChannelParentChannelGet(self);
        ret |= ThaSdhAuVcLomMonitorEnable((ThaSdhAuVc)vc4, AtSdhVcTu1xChannelized((AtSdhVc)vc4));
        }

    return ret;
    }

static void OverrideAtSdhChannel(AtSdhTug self)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhChannelMethods = mMethodsGet(sdhChannel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhChannelOverride, m_AtSdhChannelMethods, sizeof(m_AtSdhChannelOverride));

        mMethodOverride(m_AtSdhChannelOverride, MapTypeSet);
        }

    mMethodsSet(sdhChannel, &m_AtSdhChannelOverride);
    }

static void OverrideAtChannel(AtSdhTug self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, IdString);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtSdhTug self)
    {
    OverrideAtChannel(self);
    OverrideAtSdhChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290021SdhTug);
    }

static AtSdhTug ObjectInit(AtSdhTug self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011Tfi5LineTugObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhTug Tha60290021SdhTugNew(uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhTug newTug = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newTug == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newTug, channelId, channelType, module);
    }

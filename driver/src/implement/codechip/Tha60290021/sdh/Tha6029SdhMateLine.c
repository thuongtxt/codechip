/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6029SdhMateLine.c
 *
 * Created Date: Jul 10, 2016
 *
 * Description : PWCodechip-6029 SDH Mate line.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/physical/AtSerdesControllerInternal.h"
#include "../../Tha60210011/man/Tha60210011Device.h"
#include "../man/Tha60290021Device.h"
#include "../ocn/Tha60290021ModuleOcnReg.h"
#include "../ocn/Tha60290021ModuleOcn.h"
#include "../physical/Tha60290021SerdesManager.h"
#include "Tha6029SdhLineInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6029SdhMateLine *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods           m_AtChannelOverride;
static tAtObjectMethods            m_AtObjectOverride;
static tAtSdhLineMethods           m_AtSdhLineOverride;
static tThaSdhLineMethods          m_ThaSdhLineOverride;
static tTha60210011Tfi5LineMethods m_Tha60210011Tfi5LineOverride;

/* Save super implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtSdhLineMethods *m_AtSdhLineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumSubLinesInGroup(AtChannel self)
    {
    AtUnused(self);
    return 4;
    }

static uint32 LocalId(AtChannel self)
    {
    return AtChannelIdGet(self) - Tha60290021ModuleSdhMateLineStartId((AtModuleSdh)AtChannelModuleGet(self));
    }

static uint32 GroupId(AtChannel self)
    {
    return (LocalId(self) / NumSubLinesInGroup(self));
    }

static uint32 DiagnosticBaseAddress(ThaSdhLine self)
    {
    return (0xF74000UL + GroupId((AtChannel)self) * 0x2000UL);
    }

static eBool IsInDiagnosticMode(AtChannel self)
    {
    AtDevice device = AtChannelDeviceGet(self);
    return AtDeviceDiagnosticModeIsEnabled(device);
    }

static uint32 DefectGet(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return Tha60290021ModuleOcnDiagLineAlarmGet((AtSdhLine)self);

    return m_AtChannelMethods->DefectGet(self);
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return Tha60290021ModuleOcnDiagLineAlarmHistoryRead2Clear((AtSdhLine)self, cAtFalse);

    return m_AtChannelMethods->DefectHistoryGet(self);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return Tha60290021ModuleOcnDiagLineAlarmHistoryRead2Clear((AtSdhLine)self, cAtTrue);

    return m_AtChannelMethods->DefectHistoryClear(self);
    }

static uint32 AlarmGet(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return Tha60290021ModuleOcnDiagLineAlarmGet((AtSdhLine)self);

    return m_AtChannelMethods->AlarmGet(self);
    }

static uint32 AlarmHistoryGet(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return Tha60290021ModuleOcnDiagLineAlarmHistoryRead2Clear((AtSdhLine)self, cAtFalse);

    return m_AtChannelMethods->AlarmHistoryGet(self);
    }

static uint32 AlarmHistoryClear(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return Tha60290021ModuleOcnDiagLineAlarmHistoryRead2Clear((AtSdhLine)self, cAtTrue);

    return m_AtChannelMethods->AlarmHistoryClear(self);
    }

static uint32 CounterGet(AtChannel self, uint16 counterType)
    {
    if (!IsInDiagnosticMode(self))
        return m_AtChannelMethods->CounterGet(self, counterType);

    if (counterType == cAtSdhLineCounterTypeB1)
        return Tha60290021ModuleOcnDiagLineB1CounterRead2Clear((AtSdhLine)self, cAtFalse);

    return 0x0;
    }

static uint32 CounterClear(AtChannel self, uint16 counterType)
    {
    if (!IsInDiagnosticMode(self))
        return m_AtChannelMethods->CounterClear(self, counterType);

    if (counterType == cAtSdhLineCounterTypeB1)
        return Tha60290021ModuleOcnDiagLineB1CounterRead2Clear((AtSdhLine)self, cAtTrue);

    return 0x0;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (IsInDiagnosticMode(self))
        return Tha60290021ModuleOcnDiagLineTxAlarmForce((AtSdhLine)self, alarmType, cAtTrue);

    return m_AtChannelMethods->TxAlarmForce(self, alarmType);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (IsInDiagnosticMode(self))
        return Tha60290021ModuleOcnDiagLineTxAlarmForce((AtSdhLine)self, alarmType, cAtFalse);

    return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return Tha60290021ModuleOcnDiagLineTxForcedAlarmGet((AtSdhLine)self);

    return m_AtChannelMethods->TxForcedAlarmGet(self);
    }

static eAtRet DiagTxErrorForce(AtChannel self, uint32 errorType)
    {
    if (errorType == cAtSdhLineErrorB1)
        return Tha60290021ModuleOcnDiagLineB1Force((AtSdhLine)self, cAtTrue);

    return cAtErrorModeNotSupport;
    }

static eAtRet DiagTxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (errorType == cAtSdhLineErrorB1)
        return Tha60290021ModuleOcnDiagLineB1Force((AtSdhLine)self, cAtFalse);

    return cAtOk;
    }

static uint32 DiagTxForcedErrorGet(AtChannel self)
    {
    if (Tha60290021ModuleOcnDiagLineB1IsForced((AtSdhLine)self))
        return cAtSdhLineErrorB1;

    return cAtSdhLineErrorNone;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (IsInDiagnosticMode(self))
        return DiagTxErrorForce(self, errorType);

    return m_AtChannelMethods->TxErrorForce(self, errorType);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (IsInDiagnosticMode(self))
        return DiagTxErrorUnForce(self, errorType);
    return m_AtChannelMethods->TxErrorUnForce(self, errorType);
    }

static uint32 TxForcedErrorGet(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return DiagTxForcedErrorGet(self);

    return m_AtChannelMethods->TxForcedErrorGet(self);
    }

static Tha60290021ModuleOcn OcnModule(AtSdhLine self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)self);
    return (Tha60290021ModuleOcn)AtDeviceModuleGet(device, cThaModuleOcn);
    }

static eAtModuleSdhRet RateSet(AtSdhLine self, eAtSdhLineRate rate)
    {
    eAtRet ret = m_AtSdhLineMethods->RateSet(self, rate);
    if (ret != cAtOk)
        return ret;

    if (Tha60290021ModuleOcnHasDiagnosticIp(OcnModule(self)))
    return Tha60290021ModuleOcnDiagLineRateSet((AtSdhLine)self, rate);

    return ret;
    }

static const char *ToString(AtObject self)
    {
    static char description[32];
    AtSnprintf(description, sizeof(description), "mate.%d", AtChannelIdGet((AtChannel)self) + 1);
    return description;
    }

static eAtRet LoopbackSet(AtChannel self, uint8 loopbackMode)
    {
    AtSerdesController serdes = AtSdhLineSerdesController((AtSdhLine)self);
    if (serdes)
        return AtSerdesControllerLoopbackSet(serdes, loopbackMode);
    return m_AtChannelMethods->LoopbackSet(self, loopbackMode);
    }

static uint8 LoopbackGet(AtChannel self)
    {
    AtSerdesController serdes = AtSdhLineSerdesController((AtSdhLine)self);
    if (serdes)
        return AtSerdesControllerLoopbackGet(serdes);
    return m_AtChannelMethods->LoopbackGet(self);
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtSerdesController serdes = AtSdhLineSerdesController((AtSdhLine)self);

    if (loopbackMode == cAtLoopbackModeRelease)
        return cAtTrue;

    return AtSerdesControllerCanLoopback(serdes, loopbackMode, cAtTrue);
    }

static eBool CanAccessRegister(AtChannel self, eAtModule moduleId, uint32 address)
    {
    AtDevice device = AtChannelDeviceGet(self);

    if ((eThaPhyModule)moduleId == cThaModuleOcn)
        return Tha60290021IsOcnTfi5SideRegister(device, address);

    if (moduleId == cAtModuleSdh)
        return Tha60210011IsOcnTfi5SerdesRegister(device, address);

    return cAtFalse;
    }

static AtSerdesController SerdesController(AtSdhLine self)
    {
    AtChannel line = (AtChannel)self;
    AtModuleSdh sdhModule = (AtModuleSdh)AtChannelModuleGet(line);
    AtSerdesManager serdesManager = AtDeviceSerdesManagerGet(AtChannelDeviceGet(line));
    uint32 localId = AtChannelIdGet(line) - Tha60290021ModuleSdhMateLineStartId(sdhModule);
    if (serdesManager)
        return Tha60290021SerdesManagerMateSerdesController(serdesManager, localId);
    return NULL;
    }

static eAtRet Init(AtChannel self)
    {
    if (IsInDiagnosticMode(self))
        return Tha60290021ModuleOcnDiagLineDefaultSet((AtSdhLine)self);

    return m_AtChannelMethods->Init(self);
    }

static eAtModuleSdhRet ScrambleEnable(AtSdhLine self, eBool enable)
    {
    return Tha60290021ModuleOcnDiagLineScrambleEnable(self, enable);
    }

static eBool ScrambleIsEnabled(AtSdhLine self)
    {
    return Tha60290021ModuleOcnDiagLineScrambleIsEnabled(self);
    }

static uint32 RxfrmstaBase(Tha60210011Tfi5Line self)
    {
    AtUnused(self);
    return cAf6Reg_tfi5rxfrmsta_Base;
    }

static uint32 RxfrmstkBase(Tha60210011Tfi5Line self)
    {
    AtUnused(self);
    return cAf6Reg_tfi5rxfrmstk_Base;
    }

static uint32 Rxfrmb1cntr2cBase(Tha60210011Tfi5Line self)
    {
    AtUnused(self);
    return cAf6Reg_tfi5rxfrmb1cntr2c_Base;
    }

static uint32 Rxfrmb1cntroBase(Tha60210011Tfi5Line self)
    {
    AtUnused(self);
    return cAf6Reg_tfi5rxfrmb1cntro_Base;
    }

static AtModuleSdh SdhModule(AtSdhLine self)
    {
    return (AtModuleSdh)AtChannelModuleGet((AtChannel)self);
    }

static uint8 LocalIdGet(AtSdhLine self)
    {
    return ThaModuleSdhLineIdLocalId((ThaModuleSdh)SdhModule(self), (uint8)AtChannelIdGet((AtChannel)self));
    }

static AtSdhLine HideLineGet(AtSdhLine self, uint8 sts1)
    {
    AtUnused(sts1);
    return Tha60290021ModuleSdhTerminatedLineGet(SdhModule(self), LocalIdGet(self));
    }

static void OverideTha60210011Tfi5Line(AtSdhLine self)
    {
    Tha60210011Tfi5Line line = (Tha60210011Tfi5Line)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011Tfi5LineOverride, mMethodsGet(line), sizeof(m_Tha60210011Tfi5LineOverride));

        mMethodOverride(m_Tha60210011Tfi5LineOverride, RxfrmstaBase);
        mMethodOverride(m_Tha60210011Tfi5LineOverride, RxfrmstkBase);
        mMethodOverride(m_Tha60210011Tfi5LineOverride, Rxfrmb1cntr2cBase);
        mMethodOverride(m_Tha60210011Tfi5LineOverride, Rxfrmb1cntroBase);
        }

    mMethodsSet(line, &m_Tha60210011Tfi5LineOverride);
    }

static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, mMethodsGet(line), sizeof(m_ThaSdhLineOverride));

        mMethodOverride(m_ThaSdhLineOverride, DiagnosticBaseAddress);
        }

    mMethodsSet(line, &m_ThaSdhLineOverride);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, mMethodsGet(self), sizeof(m_AtSdhLineOverride));

        mMethodOverride(m_AtSdhLineOverride, SerdesController);
        mMethodOverride(m_AtSdhLineOverride, RateSet);
        mMethodOverride(m_AtSdhLineOverride, ScrambleEnable);
        mMethodOverride(m_AtSdhLineOverride, ScrambleIsEnabled);
        mMethodOverride(m_AtSdhLineOverride, HideLineGet);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        mMethodOverride(m_AtChannelOverride, LoopbackSet);
        mMethodOverride(m_AtChannelOverride, LoopbackGet);
        mMethodOverride(m_AtChannelOverride, CanAccessRegister);
        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, CounterGet);
        mMethodOverride(m_AtChannelOverride, CounterClear);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcedErrorGet);
        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, AlarmGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryClear);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideAtObject(AtSdhLine self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtObject(self);
    OverrideAtChannel(self);
    OverrideAtSdhLine(self);
    OverrideThaSdhLine(self);
    OverideTha60210011Tfi5Line(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6029SdhMateLine);
    }

AtSdhLine Tha6029SdhMateLineObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029SdhLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha6029SdhMateLineNew(uint32 lineId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6029SdhMateLineObjectInit(newLine, lineId, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6A290022ModuleSdhInternal.h
 * 
 * Created Date: Jul 8, 2018
 *
 * Description : Module SDH internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A291022MODULESDHINTERNAL_H_
#define _THA6A291022MODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A290022/sdh/Tha6A290022ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A291022ModuleSdh * Tha6A291022ModuleSdh;
typedef struct tTha6A291022ModuleSdh
    {
	tTha6A290022ModuleSdh super;
    }tTha6A291022ModuleSdh;
/*--------------------------- Entries ----------------------------------------*/

AtModuleSdh Tha6A291022ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);
#ifdef __cplusplus
}
#endif
#endif /* _THA6A291022MODULESDHINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60290021ModuleSdh.c
 *
 * Created Date: March 13, 2018
 *
 * Description : PWCodechip-60290021 SDH module.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A291022ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290022ModuleSdh*)self)

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods          m_AtModuleSdhOverride;
static tTha60290021ModuleSdhMethods m_Tha60290021ModuleSdhOverride;

/* Save super implementation */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumUseableMateLines(Tha60290021ModuleSdh self)
    {
    AtUnused(self);
    return 4;
    }

static uint8 NumUseableTerminatedLines(Tha60290021ModuleSdh self)
    {
    AtUnused(self);
    return 4;
    }

static void OverrideTha60290021ModuleSdh(Tha60290021ModuleSdh self)
	{
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290021ModuleSdhOverride, mMethodsGet(self), sizeof(m_Tha60290021ModuleSdhOverride));

        mMethodOverride(m_Tha60290021ModuleSdhOverride, NumUseableTerminatedLines);
        mMethodOverride(m_Tha60290021ModuleSdhOverride, NumUseableMateLines);
        }

    mMethodsSet(self, &m_Tha60290021ModuleSdhOverride);
    }


static void OverrideAtModuleSdh(AtModuleSdh self)
	{
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, mMethodsGet(self), sizeof(m_AtModuleSdhOverride));
        /*mMethodOverride(m_AtModuleSdhOverride, NumTerminatedLines);*/
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }


static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideTha60290021ModuleSdh((Tha60290021ModuleSdh) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A291022ModuleSdh);
    }

AtModuleSdh Tha6A291022ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A290022ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha6A291022ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6A291022ModuleSdhObjectInit(newModule, device);
    }


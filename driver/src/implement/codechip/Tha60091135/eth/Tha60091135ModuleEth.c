/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60091135ModuleEth.c
 *
 * Created Date: Oct 27, 2014
 *
 * Description : Ethernet module of product 60031135
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60091132/eth/Tha60091132ModuleEthInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/eth/controller/ThaEthFlowHeaderProvider.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60091135ModuleEth
    {
    tTha60091132ModuleEth super;
    }tTha60091135ModuleEth;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleEthMethods m_ThaModuleEthOverride;
static tTha60091132ModuleEthMethods m_Tha60091132ModuleEthOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaEthFlowHeaderProvider FlowHeaderProviderCreate(ThaModuleEth self)
    {
    return Tha60091135EthFlowHeaderProviderNew((AtModuleEth)self);
    }

static uint32 StartVersionSupportHigigStmPortLookup(Tha60091132ModuleEth self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(0x0, 0x2, 0x0);
    }

static uint32 StartVersionIgnoreMpFlowHigigStmPortLookup(Tha60091132ModuleEth self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(0x0, 0x3, 0x0);
    }

static uint32 StartVersionHas10GDiscardCounter(ThaModuleEth self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(0x0, 0x3, 0x0);;
    }

static void OverrideTha60091132ModuleEth(AtModuleEth self)
    {
    Tha60091132ModuleEth ethModule = (Tha60091132ModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60091132ModuleEthOverride, mMethodsGet(ethModule), sizeof(m_Tha60091132ModuleEthOverride));

        mMethodOverride(m_Tha60091132ModuleEthOverride, StartVersionSupportHigigStmPortLookup);
        mMethodOverride(m_Tha60091132ModuleEthOverride, StartVersionIgnoreMpFlowHigigStmPortLookup);
        }

    mMethodsSet(ethModule, &m_Tha60091132ModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth thaModuleEth = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(thaModuleEth), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, FlowHeaderProviderCreate);
        mMethodOverride(m_ThaModuleEthOverride, StartVersionHas10GDiscardCounter);
        }

    mMethodsSet(thaModuleEth, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideThaModuleEth(self);
    OverrideTha60091132ModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60091135ModuleEth);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60091132ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60091135ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies.
 * The use, copying, transfer or disclosure of such information is prohibited 
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha61290011Device.c
 *
 * Created Date: Sep 28, 2016 
 *
 * Description : Tha61290011 Device implementations
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha61290011Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local Typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60210031DeviceMethods m_Tha60210031DeviceOverride;

/* Save Super Implementation */
static const tAtDeviceMethods  *m_AtDeviceMethods  = NULL;
static const tThaDeviceMethods *m_ThaDeviceMethods = NULL;

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumInterruptPins(AtDevice self)
    {
    AtUnused(self);
    return 0;
    }

static eBool SemControllerIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint8 NumPartsOfModule(ThaDevice self, uint32 moduleId)
    {
    AtUnused(self);
    AtUnused(moduleId);
    return 1;
    }

static uint32 PllClkSysStatBitMask(ThaDevice self)
    {
    return Tha60290011SuperDevicePllClkSysStatBitMaskGet(self);
    }

static uint32 DdrCurrentStatusRegister(Tha60210031Device self)
    {
    return Tha60290011SuperDeviceDdrCurrentStatusRegister((ThaDevice)self);
    }

static eBool DdrCalibCurrentStatusIsDone(Tha60210031Device self, uint32 regVal, uint32 mask)
    {
    return Tha60210031DeviceDdrCalibCurrentStatusIsDone(self, regVal, mask);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, SemControllerIsSupported);
        mMethodOverride(m_AtDeviceOverride, NumInterruptPins);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, PllClkSysStatBitMask);
        mMethodOverride(m_ThaDeviceOverride, NumPartsOfModule);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void OverrideTha60210031Device(AtDevice self)
    {
    Tha60210031Device device = (Tha60210031Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031DeviceOverride, mMethodsGet(device), sizeof(m_Tha60210031DeviceOverride));

        mMethodOverride(m_Tha60210031DeviceOverride, DdrCurrentStatusRegister);
        mMethodOverride(m_Tha60210031DeviceOverride, DdrCalibCurrentStatusIsDone);
        }

    mMethodsSet(device, &m_Tha60210031DeviceOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha61290011Device);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    OverrideTha60210031Device(self);
    }

AtDevice Tha61290011DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha61290011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha61290011DeviceObjectInit(newDevice, driver, productCode);
    }

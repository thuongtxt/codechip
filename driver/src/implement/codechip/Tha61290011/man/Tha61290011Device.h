/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device Management
 *
 * File        : Tha61290011DeviceInternal.h
 *
 * Created Date: Nov 16, 2016
 *
 * Description : Internal Interface of Product 24 DS3/E3/EC1 or 84 DS1/E1 in Kintex board
 *
 * Notes       :
 *----------------------------------------------------------------------------*/

#ifndef _THA61290011DEVICE_H_
#define _THA61290011DEVICE_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/man/Tha60290011DeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha61290011Device
    {
    tTha60290011Device super;
    }tTha61290011Device;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha61290011DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif

#endif /* _THA61290011DEVICE_H_ */

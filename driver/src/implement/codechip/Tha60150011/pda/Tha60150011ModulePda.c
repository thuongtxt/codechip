/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDA
 *
 * File        : Tha60150011ModulePda.c
 *
 * Created Date: Jun 25, 2014
 *
 * Description : PDA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/ThaModulePwV2Reg.h"
#include "../../../default/pw/ThaPwUtil.h"
#include "Tha60150011ModulePdaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cPDASliceMask   cBit10
#define cPDASliceShift  10
#define cPWReorPdvMask  cBit17_12
#define cPWReorPdvShift 12

#define cPwRxPldOctCnt(ro)          (0x521000UL + (ro) * 0x800UL)
#define cPwRxReorderDropPktCnt(ro)  (0x527000UL + (ro) * 0x800UL)
#define cPwRxReorderedPktCnt(ro)    (0x528000UL + (ro) * 0x800UL)
#define cPwRxLofsTransCnt(ro)       (0x522000UL + (ro) * 0x800UL)
#define cPwRxJitBufOverrunPktCnt(ro)(0x52A000UL + (ro) * 0x800UL)
#define cPwJitBufUnderrunEvtCnt(ro) (0x52B000UL + (ro) * 0x800UL)
#define cPwRxReorderLostPktCnt(ro)  (0x529000UL + (ro) * 0x800UL)

#define cPwLOPSPkMask  cBit24_16 /* bit 56:48 */
#define cPwLOPSPkShift 16

#define cSetLofsHeadMask  cBit1_0 /* Bit 33:32 */
#define cSetLofsHeadShift 0
#define cSetLofsTailMask  cBit31_25
#define cSetLofsTailShift 25

/*--------------------------- Macros -----------------------------------------*/
#define mModulePdaV2(self) ((ThaModulePdaV2)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60150011ModulePdaMethods m_methods;

/* Override */
static tThaModulePdaV2Methods m_ThaModulePdaV2Override;
static tThaModulePdaMethods   m_ThaModulePdaOverride;

/* Save super implementation */
static const tThaModulePdaMethods *m_ThaModulePdaMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedToCfgCircuitType(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 PdaFirstStsIdMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cBit9_5;
    }

static uint8 PdaFirstStsIdShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return 5;
    }

static eAtRet PwCircuitSliceSet(ThaModulePda self, AtPw pw, uint8 slice)
    {
    uint32 regAddr = mMethodsGet(self)->PWPdaTdmModeCtrl(self, pw) + mPwOffset(self, pw);
    uint32 regVal = mChannelHwRead(pw, regAddr, cThaModulePda);

    mRegFieldSet(regVal, cPDASlice, slice);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static uint32 PdaIdleCodeMask(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cBit21_14;
    }

static uint8 PdaIdleCodeShift(ThaModulePdaV2 self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 14;
    }

static uint32 PdaNumNxDs0Mask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cBit27_22;
    }

static uint8 PdaNumNxDs0Shift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return 22;
    }

static uint32 SdhChannelSliceOffset(ThaModulePdaV2 self, uint8 slice)
    {
    AtUnused(self);
    return slice * 0x1000UL;
    }

static uint32 PDATdmJitBufStat(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return 0x243800;
    }

static uint32 HotConfigureEngineRequestInfoPwIdMask(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cBit12_1;
    }

static uint8 HotConfigureEngineRequestInfoPwIdShift(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return 1;
    }

static eAtRet PwLopsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 address = mMethodsGet(mModulePdaV2(self))->PWPdaJitBufCtrl(mModulePdaV2(self)) + mPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mRegFieldSet(longRegVal[1], cPwLOPSPk, numPackets);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    return cAtOk;
    }

static uint32 PwLopsSetThresholdGet(ThaModulePda self, AtPw pw)
    {
    uint32 address = mMethodsGet(mModulePdaV2(self))->PWPdaJitBufCtrl(mModulePdaV2(self)) + mPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    return mRegField(longRegVal[1], cPwLOPSPk);
    }

static eBool CepFractionalIsSupported(ThaModulePdaV2 self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 MaxNumJitterBufferBlocks(ThaModulePda self)
    {
    AtUnused(self);
    return 262144;
    }

static uint32 JitterBufferBlockSizeInByte(ThaModulePda self)
    {
    AtUnused(self);
    return 256;
    }

static void JitterBufferRegShow(ThaModulePdaV2 self, AtPw pw)
    {
    ThaModulePdaLongRegDisplay(pw, "Pseudowire PDA Jitter Buffer Control", mMethodsGet(self)->PWPdaJitBufCtrl(self) + mPwOffset(self, pw));
    }

static void OverrideThaModulePdaV2(AtModule self)
    {
    ThaModulePdaV2 pdaModuleV2 = (ThaModulePdaV2)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaV2Override, mMethodsGet(pdaModuleV2), sizeof(m_ThaModulePdaV2Override));

        mMethodOverride(m_ThaModulePdaV2Override, NeedToCfgCircuitType);
        mMethodOverride(m_ThaModulePdaV2Override, SdhChannelSliceOffset);
        mMethodOverride(m_ThaModulePdaV2Override, PDATdmJitBufStat);
        mMethodOverride(m_ThaModulePdaV2Override, CepFractionalIsSupported);
        mMethodOverride(m_ThaModulePdaV2Override, JitterBufferRegShow);

        mBitFieldOverride(ThaModulePdaV2, m_ThaModulePdaV2Override, PdaFirstStsId)
        mBitFieldOverride(ThaModulePdaV2, m_ThaModulePdaV2Override, PdaIdleCode)
        mBitFieldOverride(ThaModulePdaV2, m_ThaModulePdaV2Override, PdaNumNxDs0)
        mBitFieldOverride(ThaModulePdaV2, m_ThaModulePdaV2Override, HotConfigureEngineRequestInfoPwId)
        }

    mMethodsSet(pdaModuleV2, &m_ThaModulePdaV2Override);
    }

static uint32 PktReplaceModeMask(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cBit12_11;
    }

static uint8 PktReplaceModeShift(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 11;
    }

/*
 * ReorderPdv is equal to PdvSizePkt if PdvSizePkt <= 32,
 * and is 32 otherwise
 */
static uint32 ReorderPdvCalculate(ThaModulePda self, AtPw pw)
    {
    uint32 address = mMethodsGet(mModulePdaV2(self))->PWPdaJitBufCtrl(mModulePdaV2(self)) +
                     mMethodsGet(self)->PwDefaultOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 pdvSizePkt;

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    pdvSizePkt = mRegField(longRegVal[cThaRegPWPdaJitBufCtrlPdvSizePkDwIndex], cThaRegPWPdaJitBufCtrlPdvSizePk);

    return ((pdvSizePkt > 32) ? 32 : pdvSizePkt);
    }

static eAtRet ReorderPdvSet(Tha60150011ModulePda self, AtPw pw)
    {
    uint32 reorderPdv, regVal, regAddr;

    /* Configure reorder PDV number packets */
    reorderPdv = ReorderPdvCalculate((ThaModulePda)self, pw);

    regAddr = cThaRegPWPdaReorderCtrl + mMethodsGet((ThaModulePda)self)->PwDefaultOffset((ThaModulePda)self, pw);
    regVal = mChannelHwRead(pw, regAddr, cThaModulePda);
    mRegFieldSet(regVal, cPWReorPdv, reorderPdv);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePda);

    return cAtOk;
    }

static eAtRet PwReorderingEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    eAtRet ret = m_ThaModulePdaMethods->PwReorderingEnable(self, pw, enable);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet((Tha60150011ModulePda)self)->ReorderPdvSet((Tha60150011ModulePda)self, pw);
    }

static ThaModulePw ModulePw(AtPw pw)
    {
    return (ThaModulePw)AtChannelModuleGet((AtChannel)pw);
    }

static eAtRet PwJitterBufferDelaySet(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    uint32 numPackets, delayInUsByPacket;
    eAtRet ret = m_ThaModulePdaMethods->PwJitterBufferDelaySet(self, pw, microseconds);
    if (ret != cAtOk)
        return ret;

    /* If product support HW storage to save the disparity, save it to use when getting */
    numPackets = ThaModulePdaCalculateNumPacketsByUs(self, pw, microseconds);
    delayInUsByPacket = ThaPwUtilTimeInUsForNumPackets(numPackets,
                                                       ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, AtPwBoundCircuitGet(pw)),
                                                       ThaPwAdapterPayloadSizeInBytes((ThaPwAdapter)pw));
    ThaModulePwBufferDelayDisparitySaveToHwStorage(ModulePw(pw), pw, (int)(microseconds - delayInUsByPacket));

    /* Update reorder PDV */
    return mMethodsGet((Tha60150011ModulePda)self)->ReorderPdvSet((Tha60150011ModulePda)self, pw);
    }

static eAtRet PwJitterBufferSizeSet(ThaModulePda self, AtPw pw, uint32 microseconds)
    {
    uint32 sizeInUsByPacket, numPackets;
    eAtRet ret = m_ThaModulePdaMethods->PwJitterBufferSizeSet(self, pw, microseconds);
    if (ret != cAtOk)
        return ret;

    /* If product support HW storage to save the disparity, save it to use when getting */
    numPackets = ThaModulePdaCalculateNumPacketsByUs(self, pw, microseconds);
    sizeInUsByPacket = ThaPwUtilTimeInUsForNumPackets(numPackets,
                                                      ThaPwAdapterDataRateInBytesPerMs((ThaPwAdapter)pw, AtPwBoundCircuitGet(pw)),
                                                      ThaPwAdapterPayloadSizeInBytes((ThaPwAdapter)pw));

    ThaModulePwBufferSizeDisparitySaveToHwStorage(ModulePw(pw), pw, (int)(microseconds - sizeInUsByPacket));

    return cAtOk;
    }

static uint32 PwJitterBufferSizeGet(ThaModulePda self, AtPw pw)
    {
    uint32 sizeInUsByPacket = m_ThaModulePdaMethods->PwJitterBufferSizeGet(self, pw);
    return (uint32)((int)sizeInUsByPacket + ThaModulePwBufferSizeDisparityGetFromHwStorage(ModulePw(pw), pw));
    }

static uint32 PwJitterBufferDelayGet(ThaModulePda self, AtPw pw)
    {
    uint32 delayInUsByPacket = m_ThaModulePdaMethods->PwJitterBufferDelayGet(self, pw);
    return (uint32)((int)delayInUsByPacket + ThaModulePwBufferDelayDisparityGetFromHwStorage(ModulePw(pw), pw));
    }

static uint32 PwRxOutOfSeqDropedPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxReorderDropPktCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxBytesGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxPldOctCnt((clear) ? 0 : 1) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxReorderedPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxReorderedPktCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxLopsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxLofsTransCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxJitBufOverrunGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxJitBufOverrunPktCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxJitBufUnderrunGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwJitBufUnderrunEvtCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwRxLostPacketsGet(ThaModulePda self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxReorderLostPktCnt(mRo(clear)) + mPwOffset(self, pw), cThaModulePwPmc);
    }

static eAtRet PwCepEbmEnable(ThaModulePda self, AtPw pw, eBool enable)
    {
    /* Implement this when CEP fractional is supported */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtOk;
    }

static uint16 CEPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 4095;
    }

static uint16 SAToPMaxPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 1500;
    }

static eAtRet PwLofsSetThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    uint32 address = mMethodsGet(mModulePdaV2(self))->PWPdaJitBufCtrl(mModulePdaV2(self)) + mPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    mRegFieldSet(longRegVal[1], cSetLofsHead, ((numPackets >> 7) & cBit1_0));
    mRegFieldSet(longRegVal[0], cSetLofsTail, numPackets & cBit6_0);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);

    return cAtOk;
    }

static uint32 PwLofsSetThresholdGet(ThaModulePda self, AtPw pw)
    {
    uint32 address = mMethodsGet(mModulePdaV2(self))->PWPdaJitBufCtrl(mModulePdaV2(self)) + mPwOffset(self, pw);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint8 setThres1;
    uint8 setThres2;

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cThaModulePda);
    setThres1 = (uint8)mRegField(longRegVal[1], cSetLofsHead);
    setThres2 = (uint8)mRegField(longRegVal[0], cSetLofsTail);

    return (((setThres1 & cBit1_0) << 7)) | ((setThres2 & cBit6_0));
    }

static eAtRet PwLofsClearThresholdSet(ThaModulePda self, AtPw pw, uint32 numPackets)
    {
    /* This product do not care LOFS clear threshold */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(numPackets);
    return cAtOk;
    }

static uint32 PwLofsClearThresholdGet(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0;
    }

static uint32 PwLopsThresholdMin(ThaModulePda self, AtPw adapter)
    {
    AtUnused(self);
    AtUnused(adapter);
    return 1;
    }

static uint32 PwLopsThresholdMax(ThaModulePda self, AtPw adapter)
    {
    AtUnused(self);
    AtUnused(adapter);
    return cPwLOPSPkMask >> cPwLOPSPkShift;
    }

static uint32 HwLimitNumPktForJitterBufferSize(ThaModulePda self, AtPw pw, uint32 payloadSize)
    {
    AtUnused(self);
    AtUnused(pw);
    AtUnused(payloadSize);
    return 511;
    }

static uint16 SAToPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 64;
    }

static uint16 CEPMinPayloadSize(ThaModulePda self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 70;
    }

static void OverrideThaModulePda(AtModule self)
    {
    ThaModulePda pdaModule = (ThaModulePda)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePdaMethods = mMethodsGet(pdaModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdaOverride, m_ThaModulePdaMethods, sizeof(m_ThaModulePdaOverride));

        mMethodOverride(m_ThaModulePdaOverride, PwCircuitSliceSet);
        mMethodOverride(m_ThaModulePdaOverride, PwReorderingEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferDelaySet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxBytesGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxOutOfSeqDropedPacketsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxReorderedPacketsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxLopsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxJitBufOverrunGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxJitBufUnderrunGet);
        mMethodOverride(m_ThaModulePdaOverride, PwRxLostPacketsGet);
        mMethodOverride(m_ThaModulePdaOverride, PwCepEbmEnable);
        mMethodOverride(m_ThaModulePdaOverride, PwLopsSetThresholdSet);
        mMethodOverride(m_ThaModulePdaOverride, PwLopsSetThresholdGet);
        mMethodOverride(m_ThaModulePdaOverride, CEPMaxPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsSetThresholdSet);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsSetThresholdGet);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsClearThresholdSet);
        mMethodOverride(m_ThaModulePdaOverride, PwLofsClearThresholdGet);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferSizeSet);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferSizeGet);
        mMethodOverride(m_ThaModulePdaOverride, PwJitterBufferDelayGet);
        mMethodOverride(m_ThaModulePdaOverride, SAToPMaxPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, HwLimitNumPktForJitterBufferSize);
        mMethodOverride(m_ThaModulePdaOverride, SAToPMinPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, CEPMinPayloadSize);
        mMethodOverride(m_ThaModulePdaOverride, PwLopsThresholdMin);
        mMethodOverride(m_ThaModulePdaOverride, PwLopsThresholdMax);
        mMethodOverride(m_ThaModulePdaOverride, MaxNumJitterBufferBlocks);
        mMethodOverride(m_ThaModulePdaOverride, JitterBufferBlockSizeInByte);
        mBitFieldOverride(ThaModulePda, m_ThaModulePdaOverride, PktReplaceMode)
        }

    mMethodsSet(pdaModule, &m_ThaModulePdaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePdaV2(self);
    OverrideThaModulePda(self);
    }

static void MethodsInit(AtModule self)
    {
    Tha60150011ModulePda module = (Tha60150011ModulePda)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, ReorderPdvSet);
        }

    mMethodsSet(module, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ModulePda);
    }

AtModule Tha60150011ModulePdaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModulePdaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60150011ModulePdaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModulePdaObjectInit(newModule, device);
    }


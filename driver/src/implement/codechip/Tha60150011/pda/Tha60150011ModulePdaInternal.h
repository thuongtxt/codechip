/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDA
 * 
 * File        : Tha60150011ModulePdaInternal.h
 * 
 * Created Date: May 7, 2015
 *
 * Description : 60150011 PDA internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011MODULEPDAINTERNAL_H_
#define _THA60150011MODULEPDAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/pda/ThaStmPwProductModulePda.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModulePda *Tha60150011ModulePda;

typedef struct tTha60150011ModulePdaMethods
    {
    eAtRet (*ReorderPdvSet)(Tha60150011ModulePda self, AtPw pw);
    }tTha60150011ModulePdaMethods;

typedef struct tTha60150011ModulePda
    {
    tThaStmPwProductModulePda super;
    const tTha60150011ModulePdaMethods *methods;
    }tTha60150011ModulePda;

/*--------------------------- Forward declarations ---------------------------*/
AtModule Tha60150011ModulePdaObjectInit(AtModule self, AtDevice device);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _THA60150011MODULEPDAINTERNAL_H_ */


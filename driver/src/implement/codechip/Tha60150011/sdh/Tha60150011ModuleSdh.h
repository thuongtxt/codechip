/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH module name
 * 
 * File        : Tha60150011ModuleSdh.h
 * 
 * Created Date: Mar 26, 2015
 *
 * Description : 60150011SDH module header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011MODULESDH_H_
#define _THA60150011MODULESDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60030081/sdh/Tha60030081ModuleSdh.h"
#include "../../../default/sdh/ThaSdhAugInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModuleSdh
    {
    tTha60030081ModuleSdh super;
    }tTha60150011ModuleSdh;

typedef struct tTha60150011SdhAug
    {
    tThaSdhAug super;
    }tTha60150011SdhAug;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSdh Tha60150011ModuleSdhObjectInit(AtModuleSdh self, AtDevice device);
AtModuleSdh Tha60150011ModuleSdhNew(AtDevice device);
AtSdhAug Tha60150011SdhAugObjectInit(AtSdhAug self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011MODULESDH_H_ */


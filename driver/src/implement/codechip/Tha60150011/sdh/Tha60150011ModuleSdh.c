/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60150011ModuleSdh.c
 *
 * Created Date: Jun 12, 2014
 *
 * Description : SDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../../default/physical/ThaSerdesController.h"
#include "Tha60150011ModuleSdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleSdhMethods m_ThaModuleSdhOverride;
static tAtModuleSdhMethods  m_AtModuleSdhOverride;

/* Save super implementation */
static const tThaModuleSdhMethods *m_ThaModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ModuleSdh);
    }

static AtSdhChannel ChannelCreateWithVersion(ThaModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId, uint8 version)
    {
    AtModuleSdh sdhModule = (AtModuleSdh)self;

    if (channelType == cAtSdhChannelTypeLine)
        return (AtSdhChannel)Tha60150011SdhLineNew(channelId, (AtModuleSdh)self, version);

    /* Create AUG */
    if ((channelType == cAtSdhChannelTypeAug1) ||
        (channelType == cAtSdhChannelTypeAug4) ||
        (channelType == cAtSdhChannelTypeAug16))
        return (AtSdhChannel)Tha60150011SdhAugNew(channelId, channelType, sdhModule);

    return m_ThaModuleSdhMethods->ChannelCreateWithVersion(self, lineId, parent, channelType, channelId, version);
    }

static uint32 MaxLineRate(AtModuleSdh self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm16;
    }

static uint8 MaxLinesGet(AtModuleSdh self)
    {
    AtUnused(self);
    return 1;
    }

static eAtSdhLineRate LineDefaultRate(ThaModuleSdh self, AtSdhLine line)
    {
    AtUnused(self);
    return ThaSdhLineHighRate((ThaSdhLine)line);
    }

static AtSerdesController SerdesControllerCreate(ThaModuleSdh self, AtSdhLine line, uint32 serdesId)
    {
    AtUnused(self);
    return Tha60150011SdhLineSerdesControllerNew(line, serdesId);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleSdhMethods = mMethodsGet(sdhModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, m_ThaModuleSdhMethods, sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, ChannelCreateWithVersion);
        mMethodOverride(m_ThaModuleSdhOverride, LineDefaultRate);
        mMethodOverride(m_ThaModuleSdhOverride, SerdesControllerCreate);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, mMethodsGet(self), sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, MaxLineRate);
        mMethodOverride(m_AtModuleSdhOverride, MaxLinesGet);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideThaModuleSdh(self);
    OverrideAtModuleSdh(self);
    }

AtModuleSdh Tha60150011ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60030081ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60150011ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModuleSdhObjectInit(newModule, device);
    }

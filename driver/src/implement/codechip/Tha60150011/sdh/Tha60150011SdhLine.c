/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60150011SdhLine.c
 *
 * Created Date: Jun 12, 2014
 *
 * Description : SDH Line
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/sdh/ThaSdhLineInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../physical/Tha60150011SdhLineSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60150011SdhLine
    {
    tThaSdhLine super;
    }tTha60150011SdhLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods  m_AtChannelOverride;
static tThaSdhLineMethods m_ThaSdhLineOverride;
static tAtSdhLineMethods  m_AtSdhLineOverride;

/* Super implementation */
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tAtSdhLineMethods  *m_AtSdhLineMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtSdhLineRate LowRate(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm4;
    }

static eAtSdhLineRate HighRate(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtSdhLineRateStm16;
    }

static eBool RateIsSupported(AtSdhLine self, eAtSdhLineRate rate)
    {
    AtUnused(self);

    /* One port stm16 only */
    if (rate == cAtSdhLineRateStm16)
        return cAtTrue;

    /* Not support for other cases */
    return cAtFalse;
    }

static eBool FpgaLoopOutIsApplicable(ThaSdhLine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret;
    AtSerdesController serdesController = AtSdhLineSerdesController((AtSdhLine)self);
    static const uint8 cSerdesTxDifferentialDefaultValue = 8;

    ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    Tha60150011SdhLineSerdesControllerTransmitDifferentialSet(serdesController, cSerdesTxDifferentialDefaultValue);
    return cAtOk;
    }

static eBool LoopbackIsSupported(AtChannel self, uint8 loopbackMode)
    {
    AtSerdesController serdesController;

    if (loopbackMode != cAtLoopbackModeRemote)
        return m_AtChannelMethods->LoopbackIsSupported(self, loopbackMode);

    serdesController = AtSdhLineSerdesController((AtSdhLine)self);
    return AtSerdesControllerCanLoopback(serdesController, loopbackMode, cAtTrue);
    }

static void OverrideAtChannel(AtSdhLine self)
    {
    AtChannel channel = (AtChannel)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, LoopbackIsSupported);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideThaSdhLine(AtSdhLine self)
    {
    ThaSdhLine line = (ThaSdhLine)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineOverride, mMethodsGet(line), sizeof(m_ThaSdhLineOverride));

        mMethodOverride(m_ThaSdhLineOverride, LowRate);
        mMethodOverride(m_ThaSdhLineOverride, HighRate);
        mMethodOverride(m_ThaSdhLineOverride, FpgaLoopOutIsApplicable);
        }

    mMethodsSet(line, &m_ThaSdhLineOverride);
    }

static void OverrideAtSdhLine(AtSdhLine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtSdhLineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtSdhLineOverride, m_AtSdhLineMethods, sizeof(m_AtSdhLineOverride));
        mMethodOverride(m_AtSdhLineOverride, RateIsSupported);
        }

    mMethodsSet(self, &m_AtSdhLineOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtChannel(self);
    OverrideThaSdhLine(self);
    OverrideAtSdhLine(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011SdhLine);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineObjectInit(self, channelId, module, version) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha60150011SdhLineNew(uint32 channelId, AtModuleSdh module, uint8 version)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLine, channelId, module, version);
    }

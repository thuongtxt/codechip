/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60150011ModulePoh.h
 * 
 * Created Date: Apr 30, 2015
 *
 * Description : POH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef THA60150011MODULEPOH_H_
#define THA60150011MODULEPOH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/poh/ThaModulePohInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModulePoh
    {
    tThaModulePohV2 super;
    }tTha60150011ModulePoh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60150011ModulePohObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* THA60150011MODULEPOH_H_ */

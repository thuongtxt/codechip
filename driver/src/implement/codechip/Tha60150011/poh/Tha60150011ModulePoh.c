/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60150011ModulePoh.c
 *
 * Created Date: Jun 14, 2014
 *
 * Description : POH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60150011ModulePoh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePohMethods   m_ThaModulePohOverride;
static tThaModulePohV2Methods m_ThaModulePohOverrideV2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 NumSts(ThaModulePohV2 self)
    {
    AtUnused(self);
    return 48;
    }

static ThaTtiProcessor LineTtiProcessorCreate(ThaModulePoh self, AtSdhLine line)
    {
    AtUnused(self);
    return Tha60150011SdhLineTtiProcessorNew((AtSdhChannel)line);
    }

static ThaPohProcessor AuVcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60150011AuVcPohProcessorNew(vc);
    }

static ThaPohProcessor Tu3VcPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60150011Tu3VcPohProcessorNew(vc);
    }

static ThaPohProcessor Vc1xPohProcessorCreate(ThaModulePoh self, AtSdhVc vc)
    {
    AtUnused(self);
    return Tha60150011Vc1xPohProcessorNew(vc);
    }

static uint32 ThaPohIndirectAddressMask(ThaModulePohV2 self)
    {
    AtUnused(self);
    return cBit16_0;
    }

static uint8  ThaPohIndirectAddressShift(ThaModulePohV2 self)
    {
    AtUnused(self);
    return 0;
    }

static void OverrideThaModulePoh(AtModule self)
    {
    ThaModulePoh pohModule = (ThaModulePoh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverride, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverride));

        mMethodOverride(m_ThaModulePohOverride, LineTtiProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, AuVcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Tu3VcPohProcessorCreate);
        mMethodOverride(m_ThaModulePohOverride, Vc1xPohProcessorCreate);
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverride);
    }

static void OverrideThaModulePohV2(AtModule self)
    {
    ThaModulePohV2 pohModule = (ThaModulePohV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePohOverrideV2, mMethodsGet(pohModule), sizeof(m_ThaModulePohOverrideV2));
        mMethodOverride(m_ThaModulePohOverrideV2, NumSts);
        mBitFieldOverride(ThaModulePohV2, m_ThaModulePohOverrideV2, ThaPohIndirectAddress)
        }

    mMethodsSet(pohModule, &m_ThaModulePohOverrideV2);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePoh(self);
    OverrideThaModulePohV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ModulePoh);
    }

AtModule Tha60150011ModulePohObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModulePohV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60150011ModulePohNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModulePohObjectInit(newModule, device);
    }


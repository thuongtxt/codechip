/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha60150011PohReg.h
 * 
 * Created Date: Aug 13, 2014
 *
 * Description : POH register
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60030041POHREG_H_
#define _THA60030041POHREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#define cPohCpeCtrl            0x090000
#define cPohCpePathOhGrabber1  0x092000
#define cPohCpeAlarmStatus     0x09C000
#define cPohRegAlarmIntrSticky 0x09A000
#define cPohCpeAlarmIntrEnable 0x098000
#define cPohCpeBipErrCnt       0x094000
#define cPohRegReiErrCnt       0x096000
#define cPohTtiCaptureBaseAddr 0x8000
#define cPohTtiTxBaseAddr      0x10000

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _THA60030041POHREG_H_ */


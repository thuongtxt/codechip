/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60150011SdhLineTtiProcessor.c
 *
 * Created Date: Jun 17, 2014
 *
 * Description : TTI processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/poh/v2/ThaSdhLineTtiProcessorV2Internal.h"
#include "../../../default/poh/ThaModulePohInternal.h"
#include "Tha60150011PohReg.h"

/*--------------------------- Define -----------------------------------------*/
#define VTMAX 28

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60150011SdhLineTtiProcessor
    {
    tThaSdhLineTtiProcessorV2 super;

    }tTha60150011SdhLineTtiProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaSdhLineTtiProcessorV2Methods m_ThaSdhLineTtiProcessorV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaModulePohV2 ModulePoh(ThaSdhLineTtiProcessorV2 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaTtiProcessorChannelGet((ThaTtiProcessor)self));
    return (ThaModulePohV2)AtDeviceModuleGet(device, cThaModulePoh);
    }

static uint8 StsMax(ThaSdhLineTtiProcessorV2 self)
    {
    return ThaModulePohV2NumSts(ModulePoh(self));
    }

static uint32 PohJ0StiMsgAlarmStatus(ThaSdhLineTtiProcessorV2 self)
    {
    uint32 STSMAX = StsMax(self);
    return cPohCpeAlarmStatus + (VTMAX * STSMAX) + (STSMAX * 2UL);
    }

static uint32 PohJ0StiMsgAlarmIntrSticky(ThaSdhLineTtiProcessorV2 self)
    {
    uint32 STSMAX = StsMax(self);
    return cPohRegAlarmIntrSticky + VTMAX * STSMAX + STSMAX * 2UL;
    }

static uint32 PohJ0StiMsgAlarmIntrEnable(ThaSdhLineTtiProcessorV2 self)
    {
    uint32 STSMAX = StsMax(self);
    return cPohCpeAlarmIntrEnable +  VTMAX * STSMAX + STSMAX * 2UL;
    }

static uint32 PohJ0Control(ThaSdhLineTtiProcessorV2 self)
    {
    uint32 STSMAX = StsMax(self);
    return cPohCpeCtrl + VTMAX * STSMAX + STSMAX * 2UL;
    }

static uint16 PohCapturedTtiBaseAddress(ThaSdhLineTtiProcessorV2 self)
    {
    AtUnused(self);
    return 0x8000;
    }

static void OverrideThaSdhLineTtiProcessorV2(ThaSdhLineTtiProcessorV2 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhLineTtiProcessorV2Override, mMethodsGet(self), sizeof(m_ThaSdhLineTtiProcessorV2Override));

        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0StiMsgAlarmStatus);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0StiMsgAlarmIntrSticky);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0StiMsgAlarmIntrEnable);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohJ0Control);
        mMethodOverride(m_ThaSdhLineTtiProcessorV2Override, PohCapturedTtiBaseAddress);
        }

    mMethodsSet(self, &m_ThaSdhLineTtiProcessorV2Override);
    }

static void Override(ThaSdhLineTtiProcessorV2 self)
    {
    OverrideThaSdhLineTtiProcessorV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011SdhLineTtiProcessor);
    }

static ThaTtiProcessor ObjectInit(ThaTtiProcessor self, AtSdhChannel sdhChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineTtiProcessorV2ObjectInit(self, sdhChannel) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaSdhLineTtiProcessorV2)self);
    m_methodsInit = 1;

    return self;
    }

ThaTtiProcessor Tha60150011SdhLineTtiProcessorNew(AtSdhChannel sdhChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaTtiProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, sdhChannel);
    }


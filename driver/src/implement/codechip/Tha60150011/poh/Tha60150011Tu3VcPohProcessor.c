/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60150011Tu3VcPohProcessor.c
 *
 * Created Date: Jun 17, 2014
 *
 * Description : TU-3's VC POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/poh/v2/ThaTu3VcPohProcessorV2Internal.h"
#include "../../../default/poh/ThaModulePohInternal.h"
#include "Tha60150011PohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60150011Tu3VcPohProcessor
    {
    tThaTu3VcPohProcessorV2 super;

    }tTha60150011Tu3VcPohProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaAuVcPohProcessorV2Methods    m_ThaAuVcPohProcessorV2Override;
static tThaDefaultPohProcessorV2Methods m_ThaDefaultPohProcessorV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 OverheadByteInsertBufferRegister(ThaAuVcPohProcessorV2 self, uint32 overheadByte)
    {
    AtUnused(self);
    switch (overheadByte)
        {
        case cAtSdhPathOverheadByteC2: return 0x08A200;
        case cAtSdhPathOverheadByteF2: return 0x08A400;
        case cAtSdhPathOverheadByteF3: return 0x08A600;
        case cAtSdhPathOverheadByteG1: return 0x08A300;
        case cAtSdhPathOverheadByteH4: return 0x08A500;
        case cAtSdhPathOverheadByteK3: return 0x08A700;
        case cAtSdhPathOverheadByteN1: return 0x08A100;

        default:
            return 0x0;
        }
    }

static uint32 PohCpeStsTu3Ctrl(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpeCtrl;
    }

static uint32 PohCpeStsTu3PathOhGrabber1(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpePathOhGrabber1;
    }

static ThaModulePohV2 ModulePoh(ThaAuVcPohProcessorV2 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    return (ThaModulePohV2)AtDeviceModuleGet(device, cThaModulePoh);
    }

static uint8 StsMax(ThaAuVcPohProcessorV2 self)
    {
    return ThaModulePohV2NumSts(ModulePoh(self));
    }

static uint32 PohCpeStsTu3PathOhGrabber2(ThaAuVcPohProcessorV2 self)
    {
    uint8 STSMAX = StsMax(self);
    static const uint8 LidMax = 4;

    return (cPohCpePathOhGrabber1 + STSMAX*2UL + LidMax);
    }

static uint32 PohCpeStsTu3AlarmStatus(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpeAlarmStatus;
    }

static uint32 PohCpeStsTu3AlarmIntrSticky(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohRegAlarmIntrSticky;
    }

static uint32 PohCpeStsTu3AlarmIntrEnable(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpeAlarmIntrEnable;
    }

static uint32 PohCpeStsTu3B3BipErrCnt(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpeBipErrCnt;
    }

static uint32 PohCpeStsTu3ReiPErrCnt(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohRegReiErrCnt;
    }

static uint32 PohCapturedTtiBaseAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohTtiCaptureBaseAddr;
    }

static uint32 PohTxTtiBaseAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohTtiTxBaseAddr;
    }

static uint32 RxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    uint8 stsInSlice, slice;

    /* Pointer adjust counters belong to module OCN */
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModuleOcn, &slice, &stsInSlice) == cAtOk)
        return (uint32)(slice << 13) + (uint32)(stsInSlice) + (read2Clear ? 0 : 0x800) + (isDecrease ? 1024 : 0) + mPohPartOffset(self);

    return 0;
    }

static uint32 TxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    /* Same as RX */
    return RxPointerAdjustCounterOffset(self, read2Clear, isDecrease);
    }

static void OverrideThaAuVcPohProcessorV2(ThaTu3VcPohProcessorV2 self)
    {
    ThaAuVcPohProcessorV2 auvcProcessor = (ThaAuVcPohProcessorV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAuVcPohProcessorV2Override,
                                  mMethodsGet(auvcProcessor),
                                  sizeof(m_ThaAuVcPohProcessorV2Override));

        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3Ctrl);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3PathOhGrabber1);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3PathOhGrabber2);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3AlarmStatus);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3AlarmIntrSticky);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3AlarmIntrEnable);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3B3BipErrCnt);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3ReiPErrCnt);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, OverheadByteInsertBufferRegister);
        }

    mMethodsSet(auvcProcessor, &m_ThaAuVcPohProcessorV2Override);
    }

static void OverrideThaDefaultPohProcessorV2(ThaTu3VcPohProcessorV2 self)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaDefaultPohProcessorV2Override,
                                  mMethodsGet(processor),
                                  sizeof(m_ThaDefaultPohProcessorV2Override));
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohCapturedTtiBaseAddress);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohTxTtiBaseAddress);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxPointerAdjustCounterOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, TxPointerAdjustCounterOffset);
        }

    mMethodsSet(processor, &m_ThaDefaultPohProcessorV2Override);
    }

static void Override(ThaTu3VcPohProcessorV2 self)
    {
    OverrideThaAuVcPohProcessorV2(self);
    OverrideThaDefaultPohProcessorV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011Tu3VcPohProcessor);
    }

static ThaPohProcessor ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaTu3VcPohProcessorV2ObjectInit((ThaTu3VcPohProcessorV2)self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaTu3VcPohProcessorV2)self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60150011Tu3VcPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, vc);
    }

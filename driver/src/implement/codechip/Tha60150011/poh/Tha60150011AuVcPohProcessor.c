/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60150011AuVcPohProcessor.c
 *
 * Created Date: Jun 17, 2014
 *
 * Description : AU's VC POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/poh/v2/ThaAuVcPohProcessorV2Internal.h"
#include "../../../default/poh/ThaModulePohInternal.h"
#include "Tha60150011PohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60150011AuVcPohProcessor
    {
    tThaAuVcPohProcessorV2 super;

    }tTha60150011AuVcPohProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaAuVcPohProcessorV2Methods    m_ThaAuVcPohProcessorV2Override;
static tThaDefaultPohProcessorV2Methods m_ThaDefaultPohProcessorV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PohCpeStsTu3Ctrl(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpeCtrl;
    }

static uint32 PohCpeStsTu3PathOhGrabber1(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpePathOhGrabber1;
    }

static uint32 PohCapturedTtiBaseAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohTtiCaptureBaseAddr;
    }

static uint32 PohTxTtiBaseAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohTtiTxBaseAddr;
    }

static ThaModulePohV2 ModulePoh(ThaAuVcPohProcessorV2 self)
    {
    AtDevice device = AtChannelDeviceGet((AtChannel)ThaPohProcessorVcGet((ThaPohProcessor)self));
    return (ThaModulePohV2)AtDeviceModuleGet(device, cThaModulePoh);
    }

static uint8 StsMax(ThaAuVcPohProcessorV2 self)
    {
    return ThaModulePohV2NumSts(ModulePoh(self));
    }

static uint32 PohCpeStsTu3PathOhGrabber2(ThaAuVcPohProcessorV2 self)
    {
    uint32 STSMAX = StsMax(self);
    static const uint8 LidMax = 4;

    return (cPohCpePathOhGrabber1 + STSMAX*2 + LidMax);
    }

static uint32 PohCpeStsTu3AlarmStatus(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpeAlarmStatus;
    }

static uint32 PohCpeStsTu3AlarmIntrSticky(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohRegAlarmIntrSticky;
    }

static uint32 PohCpeStsTu3AlarmIntrEnable(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpeAlarmIntrEnable;
    }

static uint32 PohCpeStsTu3B3BipErrCnt(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpeBipErrCnt;
    }

static uint32 PohCpeStsTu3ReiPErrCnt(ThaAuVcPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohRegReiErrCnt;
    }

static uint32 RxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    uint8 slice, stsInSlice;

    /* Pointer adjust counters belong to module OCN */
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModuleOcn, &slice, &stsInSlice) == cAtOk)
        return (uint32)(slice << 13 /* 8192 */) + stsInSlice + (read2Clear ? 0 : 0x40) + (isDecrease ? 32 : 0) + mPohPartOffset(self);

    return 0;
    }

static uint32 TxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    uint8 slice, stsInSlice;

    /* Pointer adjust counters belong to module OCN */
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModuleOcn, &slice, &stsInSlice) == cAtOk)
        return (uint32)(slice << 13 /* 8192 */) + (uint32)(stsInSlice << 5 /* 32 */) + (read2Clear ? 0 : 0x800) + (isDecrease ? 1024 : 0) + mPohPartOffset(self);

    return 0;
    }

static void OverrideThaAuVcPohProcessorV2(ThaAuVcPohProcessorV2 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAuVcPohProcessorV2Override,
                                  mMethodsGet(self),
                                  sizeof(m_ThaAuVcPohProcessorV2Override));

        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3Ctrl);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3PathOhGrabber1);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3PathOhGrabber2);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3AlarmStatus);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3AlarmIntrSticky);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3AlarmIntrEnable);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3B3BipErrCnt);
        mMethodOverride(m_ThaAuVcPohProcessorV2Override, PohCpeStsTu3ReiPErrCnt);
        }

    mMethodsSet(self, &m_ThaAuVcPohProcessorV2Override);
    }

static void OverrideThaDefaultPohProcessorV2(ThaAuVcPohProcessorV2 self)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaDefaultPohProcessorV2Override,
                                  mMethodsGet(processor),
                                  sizeof(m_ThaDefaultPohProcessorV2Override));
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohCapturedTtiBaseAddress);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohTxTtiBaseAddress);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, TxPointerAdjustCounterOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxPointerAdjustCounterOffset);
        }

    mMethodsSet(processor, &m_ThaDefaultPohProcessorV2Override);
    }

static void Override(ThaAuVcPohProcessorV2 self)
    {
    OverrideThaAuVcPohProcessorV2(self);
    OverrideThaDefaultPohProcessorV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011AuVcPohProcessor);
    }

static ThaPohProcessor ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaAuVcPohProcessorV2ObjectInit((ThaAuVcPohProcessorV2)self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaAuVcPohProcessorV2)self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60150011AuVcPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, vc);
    }


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha60150011Vc1xPohProcessor.c
 *
 * Created Date: Jun 17, 2014
 *
 * Description : VC1x POH processor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/poh/v2/ThaVc1xPohProcessorV2Internal.h"
#include "Tha60150011PohReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60150011Vc1xPohProcessor
    {
    tThaVc1xPohProcessorV2 super;
    }tTha60150011Vc1xPohProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaVc1xPohProcessorV2Methods m_ThaVc1xPohProcessorV2Override;
static tThaDefaultPohProcessorV2Methods m_ThaDefaultPohProcessorV2Override;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 PohCpeVtCtrl(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpeCtrl;
    }

static uint32 PohCpeVtPathOhGrabber1(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpePathOhGrabber1;
    }

static uint32 PohRegVtAlarmStatus(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpeAlarmStatus;
    }

static uint32 PohRegVtAlarmIntrSticky(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohRegAlarmIntrSticky;
    }

static uint32 PohRegVtAlarmIntrEnable(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpeAlarmIntrEnable;
    }

static uint32 PohRegVtBipErrCnt(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohCpeBipErrCnt;
    }

static uint32 PohRegVtReiPErrCnt(ThaVc1xPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohRegReiErrCnt;
    }

static uint32 PohCapturedTtiBaseAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohTtiCaptureBaseAddr;
    }

static uint32 PohTxTtiBaseAddress(ThaDefaultPohProcessorV2 self)
    {
    AtUnused(self);
    return cPohTtiTxBaseAddr;
    }

static uint32 RxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    uint8 stsInSlice, slice;

    /* Pointer adjust counters belong to module OCN */
    if (ThaSdhChannel2HwMasterStsId(mVcGet(self), cThaModuleOcn, &slice, &stsInSlice) == cAtOk)
        return (uint32)(slice << 13) + (uint32)(stsInSlice << 5) + VtFlatId((ThaDefaultPohProcessorV2)self) +
               (read2Clear ? 0 : 0x800) + (isDecrease ? 1024 : 0) + mPohPartOffset(self);

    return 0;
    }

static uint32 TxPointerAdjustCounterOffset(ThaDefaultPohProcessorV2 self, eBool read2Clear, eBool isDecrease)
    {
    /* Same as RX */
    return RxPointerAdjustCounterOffset(self, read2Clear, isDecrease);
    }

static void OverrideThaVc1xPohProcessorV2(ThaVc1xPohProcessorV2 self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVc1xPohProcessorV2Override,
                                  mMethodsGet(self),
                                  sizeof(m_ThaVc1xPohProcessorV2Override));

        mMethodOverride(m_ThaVc1xPohProcessorV2Override, PohCpeVtCtrl);
        mMethodOverride(m_ThaVc1xPohProcessorV2Override, PohCpeVtPathOhGrabber1);
        mMethodOverride(m_ThaVc1xPohProcessorV2Override, PohRegVtAlarmStatus);
        mMethodOverride(m_ThaVc1xPohProcessorV2Override, PohRegVtAlarmIntrSticky);
        mMethodOverride(m_ThaVc1xPohProcessorV2Override, PohRegVtAlarmIntrEnable);
        mMethodOverride(m_ThaVc1xPohProcessorV2Override, PohRegVtBipErrCnt);
        mMethodOverride(m_ThaVc1xPohProcessorV2Override, PohRegVtReiPErrCnt);
        }

    mMethodsSet(self, &m_ThaVc1xPohProcessorV2Override);
    }

static void OverrideThaDefaultPohProcessorV2(ThaVc1xPohProcessorV2 self)
    {
    ThaDefaultPohProcessorV2 processor = (ThaDefaultPohProcessorV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal,
                                  &m_ThaDefaultPohProcessorV2Override,
                                  mMethodsGet(processor),
                                  sizeof(m_ThaDefaultPohProcessorV2Override));
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohCapturedTtiBaseAddress);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, PohTxTtiBaseAddress);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, RxPointerAdjustCounterOffset);
        mMethodOverride(m_ThaDefaultPohProcessorV2Override, TxPointerAdjustCounterOffset);
        }

    mMethodsSet(processor, &m_ThaDefaultPohProcessorV2Override);
    }

static void Override(ThaVc1xPohProcessorV2 self)
    {
    OverrideThaVc1xPohProcessorV2(self);
    OverrideThaDefaultPohProcessorV2(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011Vc1xPohProcessor);
    }

static ThaPohProcessor ObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVc1xPohProcessorV2ObjectInit((ThaVc1xPohProcessorV2)self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override((ThaVc1xPohProcessorV2)self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha60150011Vc1xPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProcessor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newProcessor, vc);
    }


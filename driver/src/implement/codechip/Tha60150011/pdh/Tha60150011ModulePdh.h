/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH module name
 * 
 * File        : Tha60150011ModulePdh.h
 * 
 * Created Date: Mar 26, 2015
 *
 * Description : 60150011 Module header file
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011MODULEPDH_H_
#define _THA60150011MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModulePdh * Tha60150011ModulePdh;

typedef struct tTha60150011ModulePdhMethods
    {
    eBool (*JitterAttenuatorIsSupported)(Tha60150011ModulePdh self);
    }tTha60150011ModulePdhMethods;

typedef struct tTha60150011ModulePdh
    {
    tThaStmModulePdh super;
    const tTha60150011ModulePdhMethods *methods;
    }tTha60150011ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh Tha60150011ModulePdhObjectInit(AtModulePdh self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011MODULEPDH_H_ */


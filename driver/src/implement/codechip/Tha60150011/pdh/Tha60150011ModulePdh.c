/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60150011ModulePdh.c
 *
 * Created Date: Jun 23, 2014
 *
 * Description : PDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60150011ModulePdh.h"
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/pdh/ThaModulePdh.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/pdh/ThaPdhDe3.h"
#include "../man/Tha60150011Device.h"

/*--------------------------- Define -----------------------------------------*/
#define  cJitterAttenuatorGlobalConfiguration 0x00770003
#define  cJitterAttenuatorGlobalEnMask        cBit0
#define  cJitterAttenuatorGlobalEnShift       0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60150011ModulePdh)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60150011ModulePdhMethods m_methods;

/* Override */
static tAtModulePdhMethods     m_AtModulePdhOverride;
static tThaModulePdhMethods    m_ThaModulePdhOverride;
static tThaStmModulePdhMethods m_ThaStmModulePdhOverride;
static tAtModuleMethods        m_AtModuleOverride;

/* Save super implementations */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DefaultOffset(ThaStmModulePdh self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint8 slice, hwSts;
    int32 sliceOffset;

    /*
     * PDH Slice0 : sts #0,2,4,...46 (stsid in each oc24 slice is still 0,1,2,3,...,23)
     * PDH Slice1 : sts #1,3,5,...47 (stsid in each oc24 slice is still 0,1,2,3,...,23)
     * PDH Address = 0x7XXXXX + Slice*0x100000
     */
    ThaSdhChannelHwStsGet(channel, cAtModulePdh, stsId, &slice, &hwSts);
    sliceOffset = ThaModulePdhSliceOffset((ThaModulePdh)self, slice);

    return (uint32)((hwSts << 5) + (vtgId << 2) + vtId + sliceOffset);
    }

static int32 OffsetByHwSts(ThaStmModulePdh self, uint8 stsHwId, uint8 slice)
    {
    AtUnused(self);
    return stsHwId + ThaModulePdhSliceOffset((ThaModulePdh)self, slice);
    }

static int32 OffsetBySwSts(ThaStmModulePdh self, AtSdhChannel sdhVc, uint8 sts)
    {
    uint8 slice, hwSts;
    int32 sliceOffset;

    ThaSdhChannelHwStsGet(sdhVc, cAtModulePdh, sts, &slice, &hwSts);
    sliceOffset = ThaModulePdhSliceOffset((ThaModulePdh)self, slice);

    return hwSts + sliceOffset;
    }

static uint32 RegStsVtDemapCcatEnMask(ThaStmModulePdh self)
    {
    AtUnused(self);
    return cBit5;
    }

static uint8  RegStsVtDemapCcatEnShift(ThaStmModulePdh self)
    {
    AtUnused(self);
    return 5;
    }

static uint32 RegStsVTDemapCcatMasterMask(ThaStmModulePdh self)
    {
    AtUnused(self);
    return cBit4_0;
    }

static uint8  RegStsVTDemapCcatMasterShift(ThaStmModulePdh self)
    {
    AtUnused(self);
    return 0;
    }

static AtPdhDe1 De2De1Create(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id)
    {
    AtUnused(de2);
    return ThaPdhDe2De1New(de1Id, self);
    }

static uint32 De1RxFrmrPerChnCurrentAlmBase(ThaModulePdh self)
    {
    AtUnused(self);
    return 0x55800;
    }

static uint32 De1RxFrmrPerChnInterruptAlmBase(ThaModulePdh self)
    {
    AtUnused(self);
    return 0x55400;
    }

static uint32 DS3E3RxFrmrperChnIntrEnCtrlBase(ThaModulePdh self)
    {
    AtUnused(self);
    return 0x40580;
    }

static uint32 DS3E3RxFrmrperChnIntrStatCtrlBase(ThaModulePdh self)
    {
    AtUnused(self);
    return 0x405A0;
    }

static uint32 DS3E3RxFrmrperChnCurStatCtrlBase(ThaModulePdh self)
    {
    AtUnused(self);
    return 0x405C0;
    }

static uint32 DS1E1J1TxFrmrSignalingBufferRegister(ThaModulePdh self)
    {
    AtUnused(self);
    return 0x007C8000;
    }

static eBool HasM13(ThaModulePdh self, uint8 partId)
    {
    /* FIXME: Need to check if ALU image support this */
    AtUnused(self);
    AtUnused(partId);
    return cAtTrue;
    }

static int32 SliceOffset(ThaModulePdh self, uint32 sliceId)
    {
    AtUnused(self);
    return (int32)(sliceId * 0x100000UL);
    }

static uint8 NumSlices(ThaModulePdh self)
    {
    AtUnused(self);
    return 2;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(self);
    if (((localAddress >= 0x00700000) && (localAddress <= 0x7FFFFF)) || /* slice 0 */
        ((localAddress >= 0x00800000) && (localAddress <= 0x8FFFFF)))   /* slice 1 */
        return cAtTrue;

    return cAtFalse;
    }

static eBool JitterAttenuatorIsSupported(Tha60150011ModulePdh self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceVersionNumber(device) >= Tha60150011DeviceStartVersionSupportJitterAttenuator(device))
        return cAtTrue;

    return cAtFalse;
    }

static void JitterAttenuatorEnableOnSlice(AtModule self, uint8 slice)
    {
    uint32 address = cJitterAttenuatorGlobalConfiguration + (slice * 0x100000UL);
    uint32 regVal  = mModuleHwRead(self, address);

    mRegFieldSet(regVal, cJitterAttenuatorGlobalEn, 1);
    mModuleHwWrite(self, address, regVal);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    if (mMethodsGet(mThis(self))->JitterAttenuatorIsSupported(mThis(self)))
        {
        JitterAttenuatorEnableOnSlice(self, 0);
        JitterAttenuatorEnableOnSlice(self, 1);
        }

    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(pdhModule), sizeof(m_ThaModulePdhOverride));

        mMethodOverride(m_ThaModulePdhOverride, HasM13);
        mMethodOverride(m_ThaModulePdhOverride, SliceOffset);
        mMethodOverride(m_ThaModulePdhOverride, De1RxFrmrPerChnCurrentAlmBase);
        mMethodOverride(m_ThaModulePdhOverride, De1RxFrmrPerChnInterruptAlmBase);
        mMethodOverride(m_ThaModulePdhOverride, DS3E3RxFrmrperChnIntrStatCtrlBase);
        mMethodOverride(m_ThaModulePdhOverride, DS3E3RxFrmrperChnCurStatCtrlBase);
        mMethodOverride(m_ThaModulePdhOverride, DS1E1J1TxFrmrSignalingBufferRegister);
        mMethodOverride(m_ThaModulePdhOverride, NumSlices);
        mMethodOverride(m_ThaModulePdhOverride, DS3E3RxFrmrperChnIntrEnCtrlBase);
        }

    mMethodsSet(pdhModule, &m_ThaModulePdhOverride);
    }

static void OverrideThaStmModulePdh(AtModulePdh self)
    {
    ThaStmModulePdh stmPdh = (ThaStmModulePdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmModulePdhOverride, mMethodsGet(stmPdh), sizeof(m_ThaStmModulePdhOverride));

        mMethodOverride(m_ThaStmModulePdhOverride, DefaultOffset);
        mMethodOverride(m_ThaStmModulePdhOverride, OffsetByHwSts);
        mMethodOverride(m_ThaStmModulePdhOverride, OffsetBySwSts);

        /* Bit fields */
        mBitFieldOverride(ThaStmModulePdh, m_ThaStmModulePdhOverride, RegStsVtDemapCcatEn)
        mBitFieldOverride(ThaStmModulePdh, m_ThaStmModulePdhOverride, RegStsVTDemapCcatMaster)
        }

    mMethodsSet(stmPdh, &m_ThaStmModulePdhOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(self), sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, De2De1Create);
        }
    }

static void OverrideAtModule(AtModulePdh self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtModule(self);
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    OverrideThaStmModulePdh(self);
    }

static void MethodsInit(AtModulePdh self)
    {
    Tha60150011ModulePdh module = (Tha60150011ModulePdh)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, JitterAttenuatorIsSupported);
        }

    mMethodsSet(module, &m_methods);
    }

AtModulePdh Tha60150011ModulePdhObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tTha60150011ModulePdh));

    /* Super constructor */
    if (ThaStmModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha60150011ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, sizeof(tTha60150011ModulePdh));
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModulePdhObjectInit(newModule, device);
    }

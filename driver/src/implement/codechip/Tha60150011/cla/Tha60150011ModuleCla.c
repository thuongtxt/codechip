/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60150011ModuleCla.c
 *
 * Created Date: Jun 25, 2014
 *
 * Description : CLA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60150011ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleClaPwV2Methods m_ThaModuleClaPwV2Override;
static tThaModuleClaMethods     m_ThaModuleClaOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 CDRPwLookupCtrlReg(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return 0x443000;
    }

static uint32 CDRPwLookupCtrlCdrDisMask(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cBit10;
    }

static uint8  CDRPwLookupCtrlCdrDisShift(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return 10;
    }

static uint32 CDRPwLookupCtrlLineIdMask(ThaModuleClaPwV2 self)
    {
    AtUnused(self);
    return cBit9_0;
    }

static void OverrideThaModuleClaPwV2(AtModule self)
    {
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaPwV2Override, mMethodsGet(claModule), sizeof(m_ThaModuleClaPwV2Override));

        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlReg);
        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlCdrDisMask);
        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlCdrDisShift);
        mMethodOverride(m_ThaModuleClaPwV2Override, CDRPwLookupCtrlLineIdMask);
        }

    mMethodsSet(claModule, &m_ThaModuleClaPwV2Override);
    }

static ThaClaPwController PwControllerCreate(ThaModuleCla self)
    {
    return Tha60150011ClaPwControllerNew(self);
    }

static void OverrideThaModuleCla(AtModule self)
    {
    ThaModuleCla claModule = (ThaModuleCla)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleClaOverride, mMethodsGet(claModule), sizeof(m_ThaModuleClaOverride));

        mMethodOverride(m_ThaModuleClaOverride, PwControllerCreate);
        }

    mMethodsSet(claModule, &m_ThaModuleClaOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleClaPwV2(self);
    OverrideThaModuleCla(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ModuleCla);
    }

AtModule Tha60150011ModuleClaObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleClaObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60150011ModuleClaNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModuleClaObjectInit(newModule, device);
    }


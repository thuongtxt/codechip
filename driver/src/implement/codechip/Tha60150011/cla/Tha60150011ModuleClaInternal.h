/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60150011ModuleClaInternal.h
 * 
 * Created Date: May 6, 2015
 *
 * Description : CLA Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011MODULECLAINTERNAL_H_
#define _THA60150011MODULECLAINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/cla/ThaStmPwProductModuleCla.h"
#include "../../default/ThaStmPwProduct/cla/ThaStmPwProductClaPwController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ClaPwController * Tha60150011ClaPwController;

typedef struct tTha60150011ModuleCla
    {
    tThaStmPwProductModuleCla super;
    }tTha60150011ModuleCla;

typedef struct tTha60150011ClaPwControllerMethods
    {
    uint32 (*PwMefOverMplsControl)(Tha60150011ClaPwController);
    }tTha60150011ClaPwControllerMethods;

typedef struct tTha60150011ClaPwController
    {
    tThaStmPwProductClaPwController super;
    const tTha60150011ClaPwControllerMethods *methods;
    }tTha60150011ClaPwController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60150011ModuleClaObjectInit(AtModule self, AtDevice device);
ThaClaPwController Tha60150011ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla);
uint32 Tha60150011ClaPwControllerPwMefOverMplsControl(Tha60150011ClaPwController self);

#endif /* _THA60150011MODULECLAINTERNAL_H_ */


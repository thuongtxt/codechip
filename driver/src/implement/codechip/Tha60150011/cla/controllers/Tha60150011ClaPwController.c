/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60150011ClaPwController.c
 *
 * Created Date: Jun 25, 2014
 *
 * Description : CLA module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../cdr/Tha60150011ModuleCdr.h"
#include "../../../../default/cla/pw/ThaModuleClaPw.h"
#include "../../../../default/cla/pw/ThaModuleClaPwV2Reg.h"
#include "../../man/Tha60150011Device.h"
#include "../Tha60150011ModuleClaInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cPwCdrSliceMask  cBit11
#define cPwCdrSliceShift 11

#define cThaRegClaPwMefOverMplsControl               0x449800

#define cThaRegClaRxPwMEFoMPLSExpectedECIDHeadMask   cBit6_0
#define cThaRegClaRxPwMEFoMPLSExpectedECIDHeadShift  0
#define cThaRegClaRxPwMEFoMPLSExpectedECIDHeadIndex  2

#define cThaRegClaRxPwMEFoMPLSExpectedECIDTailMask   cBit31_19
#define cThaRegClaRxPwMEFoMPLSExpectedECIDTailShift  19
#define cThaRegClaRxPwMEFoMPLSExpectedECIDTailIndex  1

#define cThaRegClaRxPwMEFoMPLSDaValueTailMask        cBit18_0
#define cThaRegClaRxPwMEFoMPLSDaValueTailShift       0
#define cThaRegClaRxPwMEFoMPLSDaValueTailDwordIndex  1

#define cThaRegClaRxPwMEFoMPLSDaValueHeadMask        cBit31_3
#define cThaRegClaRxPwMEFoMPLSDaValueHeadShift       3
#define cThaRegClaRxPwMEFoMPLSDaValueHeadDwordIndex  0

#define cThaRegClaRxPwMEFoMPLSDaCheckEnMask          cBit2
#define cThaRegClaRxPwMEFoMPLSDaCheckEnShift         2
#define cThaRegClaRxPwMEFoMPLSDaCheckEnDwordIndex    0

#define cThaRegClaRxPwMEFoMPLSEcidCheckEnMask        cBit1
#define cThaRegClaRxPwMEFoMPLSEcidCheckEnShift       1
#define cThaRegClaRxPwMEFoMPLSEcidCheckEnDwordIndex  0

#define cThaRegClaRxPwMEFoMPLSEnMask                 cBit0
#define cThaRegClaRxPwMEFoMPLSEnShift                0
#define cThaRegClaRxPwMEFoMPLSEnDwordIndex           0

/*--------------------------- Macros -----------------------------------------*/
#define mClaModule(self) ThaClaControllerModuleGet((ThaClaController)self)
#define mThis(self) ((Tha60150011ClaPwController)self)

#define mRo(clear) ((clear) ? 0 : 1)
#define cPwRxPktCnt(ro)        (0x520000UL + (ro) * 0x800UL)
#define cPwRxMalformPktCnt(ro) (0x52C000UL + (ro) * 0x800UL)
#define cPwRxStrayPktCnt(ro)   (0x52D000UL + (ro) * 0x800UL)
#define cPwRxLbitPktCnt(ro)    (0x523000UL + (ro) * 0x800UL)
#define cPwRxRbitPktCnt(ro)    (0x526000UL + (ro) * 0x800UL)
#define cPwRxMbitOrNbitCnt(ro) (0x524000UL + (ro) * 0x800UL)
#define cPwRxPbitPktCnt(ro)    (0x525000UL + (ro) * 0x800UL)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60150011ClaPwControllerMethods m_methods;

/* Override */
static tThaClaControllerMethods   m_ThaClaControllerOverride;
static tThaClaPwControllerMethods m_ThaClaPwControllerOverride;

static const tThaClaPwControllerMethods *m_ThaClaPwControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet CdrEnable(ThaClaPwController self, AtPw pw, uint32 cdrId, eBool enable)
    {
    uint32 mask, shift;
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)mClaModule(self);
    uint32 address = ThaModuleClaPwV2CDRPwLookupCtrlReg(claModule) + mClaPwOffset(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModuleCla);

    mask  = ThaModuleClaPwV2CDRPwLookupCtrlCdrDisMask(claModule);
    shift = ThaModuleClaPwV2CDRPwLookupCtrlCdrDisShift(claModule);
    mFieldIns(&regVal, mask, shift, enable ? 0 : 1);

    mask  = ThaModuleClaPwV2CDRPwLookupCtrlLineIdMask(claModule);
    shift = ThaModuleClaPwV2CDRPwLookupCtrlLineIdShift(claModule);
    mFieldIns(&regVal, mask, shift, cdrId);

    mChannelHwWrite(pw, address, regVal, cThaModuleCla);

    return cAtOk;
    }

static eBool CdrIsEnabled(ThaClaPwController self, AtPw pw)
    {
    uint32 disableMask;
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)mClaModule(self);
    uint32 address = ThaModuleClaPwV2CDRPwLookupCtrlReg(claModule) + mClaPwOffset(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModuleCla);

    disableMask = ThaModuleClaPwV2CDRPwLookupCtrlCdrDisMask(claModule);
    return (regVal & disableMask) ? cAtFalse : cAtTrue;
    }

static eAtRet PayloadSizeSet(ThaClaPwController self, AtPw pwAdapter, uint16 payloadSize)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = ThaClaPwControllerV2PwTypeCtrlAddress((ThaClaPwControllerV2)self, pwAdapter) + mClaPwOffset(self, pwAdapter);
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)mClaModule(self);
    mChannelHwLongRead(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    mPolyRegFieldSet(longRegVal[cThaRegClaRxEthPwLenDwordIndex], claModule, ClaPwTypePwLen, payloadSize);
    mChannelHwLongWrite(pwAdapter, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static uint32 RxPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxMalformedPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxMalformPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxStrayPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxStrayPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxLbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxLbitPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxRbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxRbitPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxMbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw,  cPwRxMbitOrNbitCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxNbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxMbitOrNbitCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 RxPbitPacketsGet(ThaClaPwController self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cPwRxPbitPktCnt(mRo(clear)) + mClaPwOffset(self, pw), cThaModulePwPmc);
    }

static uint32 ClaHbceFlowIdMask(ThaClaPwController self)
    {
    AtUnused(self);
    return cBit23_14;
    }

static uint8 ClaHbceFlowIdShift(ThaClaPwController self)
    {
    AtUnused(self);
    return 14;
    }

static AtPwMefPsn MefPsn(AtPw pw)
    {
    AtPwPsn topPsn = AtPwPsnGet(ThaPwAdapterPwGet((ThaPwAdapter)pw));
    AtPwPsn currentPsn = topPsn;
    if (AtPwPsnTypeGet(topPsn) == cAtPwPsnTypeMef)
        return (AtPwMefPsn)topPsn;

    while ((currentPsn = AtPwPsnLowerPsnGet(currentPsn)) != NULL)
        {
        if (AtPwPsnTypeGet(topPsn) == cAtPwPsnTypeMef)
            return (AtPwMefPsn)topPsn;
        }

    return NULL;
    }

static eBool MefOverMplsExpectedEcidCompareIsSupport(ThaClaController self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)ThaClaControllerModuleGet(self));
    if (ThaDeviceIsEp((ThaDevice)device) || AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;
    return AtDeviceVersionNumber(device) >= Tha60150011StartVersionSupportMefOverMplsExpectedEcidComparison(device);
    }

static uint32 PwMefOverMplsControl(Tha60150011ClaPwController self)
    {
    AtUnused(self);
    return cThaRegClaPwMefOverMplsControl;
    }

static eAtRet MefOverMplsEnable(ThaClaController self, AtPw pw, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = mMethodsGet(mThis(self))->PwMefOverMplsControl(mThis(self)) + mClaPwOffset(self, pw);

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    if (enable)
        {
        uint32 dMacTailVal, dMacHeadVal;
        uint8 smac[cAtMacAddressLen];
        AtPwMefPsn mefPsn = MefPsn(pw);

        if (mefPsn == NULL)
            return cAtErrorNullPointer;

        AtPwMefPsnExpectedMacGet(mefPsn, smac);
        dMacTailVal = (uint32)(smac[0] << 11) | (uint32)(smac[1] << 3) | ((smac[2] & cBit7_5) >> 5);
        dMacHeadVal = smac[5] | (uint32)(smac[4] << 8) | (uint32)(smac[3] << 16) | (uint32)((smac[2] & cBit4_0) << 24);

        mRegFieldSet(longRegVal[cThaRegClaRxPwMEFoMPLSDaValueTailDwordIndex], cThaRegClaRxPwMEFoMPLSDaValueTail, dMacTailVal);
        mRegFieldSet(longRegVal[cThaRegClaRxPwMEFoMPLSDaValueHeadDwordIndex], cThaRegClaRxPwMEFoMPLSDaValueHead, dMacHeadVal);
        mRegFieldSet(longRegVal[cThaRegClaRxPwMEFoMPLSDaCheckEnDwordIndex], cThaRegClaRxPwMEFoMPLSDaCheckEn, 1);
        mRegFieldSet(longRegVal[cThaRegClaRxPwMEFoMPLSEcidCheckEnDwordIndex], cThaRegClaRxPwMEFoMPLSEcidCheckEn, 1);

        if (MefOverMplsExpectedEcidCompareIsSupport(self))
            {
            uint32 expectedEcid = AtPwMefPsnExpectedEcIdGet(mefPsn);
            mRegFieldSet(longRegVal[cThaRegClaRxPwMEFoMPLSExpectedECIDTailIndex], cThaRegClaRxPwMEFoMPLSExpectedECIDTail, expectedEcid);
            mRegFieldSet(longRegVal[cThaRegClaRxPwMEFoMPLSExpectedECIDHeadIndex], cThaRegClaRxPwMEFoMPLSExpectedECIDHead, expectedEcid >> 13);
            }
        }

    mRegFieldSet(longRegVal[cThaRegClaRxPwMEFoMPLSEnDwordIndex], cThaRegClaRxPwMEFoMPLSEn, mBoolToBin(enable));
    mChannelHwLongWrite(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);

    return cAtOk;
    }

static uint32 MefOverMplsExpectedEcidGet(ThaClaController self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = mMethodsGet(mThis(self))->PwMefOverMplsControl(mThis(self)) + mClaPwOffset(self, pw);
    uint32 expectedEcid = 0;

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    expectedEcid = mRegField(longRegVal[cThaRegClaRxPwMEFoMPLSExpectedECIDTailIndex], cThaRegClaRxPwMEFoMPLSExpectedECIDTail);
    expectedEcid = expectedEcid | (mRegField(longRegVal[cThaRegClaRxPwMEFoMPLSExpectedECIDHeadIndex], cThaRegClaRxPwMEFoMPLSExpectedECIDHead) << 13);

    return expectedEcid;
    }

static eAtRet MefOverMplsExpectedMacGet(ThaClaController self, AtPw pw, uint8 *expectedMac)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regAddr = mMethodsGet(mThis(self))->PwMefOverMplsControl(mThis(self)) + mClaPwOffset(self, pw);
    uint32 dMacTailVal, dMacHeadVal;

    if (expectedMac == NULL)
        return cAtErrorNullPointer;

    mChannelHwLongRead(pw, regAddr, longRegVal, cThaLongRegMaxSize, cThaModuleCla);
    dMacTailVal = mRegField(longRegVal[cThaRegClaRxPwMEFoMPLSDaValueTailDwordIndex], cThaRegClaRxPwMEFoMPLSDaValueTail);
    dMacHeadVal = mRegField(longRegVal[cThaRegClaRxPwMEFoMPLSDaValueHeadDwordIndex], cThaRegClaRxPwMEFoMPLSDaValueHead);

    expectedMac[0] = (uint8)(dMacTailVal >> 11);
    expectedMac[1] = (uint8)(dMacTailVal >> 3);
    expectedMac[2] = (uint8)((dMacTailVal << 5) | (dMacHeadVal >> 24));
    expectedMac[3] = (uint8)(dMacHeadVal >> 16);
    expectedMac[4] = (uint8)(dMacHeadVal >> 8);
    expectedMac[5] = (uint8)(dMacHeadVal);

    return cAtOk;
    }

static eAtRet CdrCircuitSliceSet(ThaClaPwController self, AtPw pw, uint8 slice)
    {
    ThaModuleClaPwV2 claModule = (ThaModuleClaPwV2)mClaModule(self);
    uint32 address = ThaModuleClaPwV2CDRPwLookupCtrlReg(claModule) + mClaPwOffset(self, pw);
    uint32 regVal = mChannelHwRead(pw, address, cThaModuleCla);

    mFieldIns(&regVal, cPwCdrSliceMask, cPwCdrSliceShift, slice);
    mChannelHwWrite(pw, address, regVal, cThaModuleCla);

    return cAtOk;
    }

static void MethodsInit(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        mMethodOverride(m_methods, PwMefOverMplsControl);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static eBool PwLookupModeIsSupported(ThaClaPwController self, AtPw pw, eThaPwLookupMode lookupMode)
    {
    AtUnused(self);
    AtUnused(pw);
    if (lookupMode == cThaPwLookupModePsn)
        return cAtTrue;

    return cAtFalse;
    }

static void PwRegsShow(ThaClaPwController self, AtPw pw)
    {
    uint32 offset = mClaPwOffset(self, pw);
    uint32 address;

    m_ThaClaPwControllerMethods->PwRegsShow(self, pw);

    address = mMethodsGet(mThis(self))->PwMefOverMplsControl(mThis(self)) + offset;
    ThaModuleClaLongRegDisplay(pw, "CLA MEF over MPLS control", address);
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaClaPwControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, m_ThaClaPwControllerMethods, sizeof(m_ThaClaPwControllerOverride));
        mMethodOverride(m_ThaClaPwControllerOverride, CdrEnable);
        mMethodOverride(m_ThaClaPwControllerOverride, CdrIsEnabled);
        mMethodOverride(m_ThaClaPwControllerOverride, PayloadSizeSet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxMalformedPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxStrayPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxLbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxRbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxMbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxNbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, RxPbitPacketsGet);
        mMethodOverride(m_ThaClaPwControllerOverride, CdrCircuitSliceSet);
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupModeIsSupported);
        mMethodOverride(m_ThaClaPwControllerOverride, PwRegsShow);

        mBitFieldOverride(ThaClaPwController, m_ThaClaPwControllerOverride, ClaHbceFlowId)
        }

    mMethodsSet(self, &m_ThaClaPwControllerOverride);
    }

static void OverrideThaClaController(ThaClaPwController self)
    {
    ThaClaController controller = (ThaClaController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaControllerOverride, mMethodsGet(controller), sizeof(m_ThaClaControllerOverride));
        mMethodOverride(m_ThaClaControllerOverride, MefOverMplsEnable);
        mMethodOverride(m_ThaClaControllerOverride, MefOverMplsExpectedEcidGet);
        mMethodOverride(m_ThaClaControllerOverride, MefOverMplsExpectedMacGet);
        }

    mMethodsSet(controller, &m_ThaClaControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaController(self);
    OverrideThaClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ClaPwController);
    }

ThaClaPwController Tha60150011ClaPwControllerObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    MethodsInit(self);

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha60150011ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ClaPwControllerObjectInit(newController, cla);
    }

uint32 Tha60150011ClaPwControllerPwMefOverMplsControl(Tha60150011ClaPwController self)
    {
    return (self) ? mMethodsGet(self)->PwMefOverMplsControl(self) : 0x0;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : RAM
 * 
 * File        : Tha60150011Qdr.h
 * 
 * Created Date: Jul 9, 2015
 *
 * Description : QDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011QDR_H_
#define _THA60150011QDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/ram/AtRamInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011Qdr
    {
    tAtRam super;
    }tTha60150011Qdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtRam Tha60150011QdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId);
AtRam Tha60150011QdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011QDR_H_ */


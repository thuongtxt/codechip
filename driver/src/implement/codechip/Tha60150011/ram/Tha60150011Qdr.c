/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60150011Qdr.c
 *
 * Created Date: Jul 9, 2015
 *
 * Description : QDR
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtDeviceInternal.h"
#include "../../../../generic/ram/AtRamInternal.h"
#include "../../Tha60150011/man/Tha60150011DeviceReg.h"
#include "../man/Tha60150011Device.h"
#include "Tha60150011Qdr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtRamMethods m_AtRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtHal Hal(AtRam self)
    {
    return AtIpCoreHalGet(AtRamIpCoreGet(self));
    }

static AtDevice Device(AtRam self)
    {
    return AtModuleDeviceGet((AtModule)AtRamModuleGet(self));
    }

static uint32 PrbsValue(AtRam self)
    {
    uint32 prbsVal;
    uint8 qdrId = AtRamIdGet(self);
    AtHal hal = Hal(self);
    uint32 regVal;

    regVal = AtHalRead(hal, cThaQdr2TestStatus);
    mFieldGet(regVal,
              cThaQDRTestDataStatusMask(qdrId),
              cThaQDRTestDataStatusShift(qdrId),
              uint32,
              &prbsVal);

    return prbsVal;
    }

static eBool IsSimulated(AtRam self)
    {
    return AtDeviceIsSimulated(Device(self));
    }

static eBool PrbsIsWorking(AtRam self)
    {
    uint32 startPrbsVal, currentPrbsVal;
    AtOsal osal = AtSharedDriverOsalGet();

    startPrbsVal = PrbsValue(self);
    if (IsSimulated(self))
        return cAtTrue;
    mMethodsGet(osal)->USleep(osal, 1000);
    currentPrbsVal = PrbsValue(self);

    return (startPrbsVal == currentPrbsVal) ? cAtFalse : cAtTrue;
    }

static eBool IsGood(AtRam self)
    {
    uint32 regVal;
    AtHal hal = Hal(self);
    AtOsal osal = AtSharedDriverOsalGet();
    AtDriver driver = AtDriverSharedDriverGet();
    uint8 qdrId = AtRamIdGet(self);
    AtDevice device = Device(self);

    /* Clear sticky */
    AtHalWrite(hal, cThaFpgaSticky, cThaFpgaStickyQDRTestErrorMask(qdrId)   |
                                    cThaFpgaStickyQDRTestNotDoneMask(qdrId));

    if (!Tha60150011StickyIsGood(device, cThaFpgaStickyQDRParityErrorMask(qdrId)))
        {
        AtDriverLog(driver, cAtLogLevelWarning, "Qdr#%u Parity error\r\n", qdrId + 1);
        return cAtFalse;
        }

    AtHalWrite(hal, cThaQdr2TestControl, 0);
    AtHalWrite(hal, cThaQdr2TestControl, cThaQDRTestEnableMask(qdrId));
    if (!IsSimulated(self))
        mMethodsGet(osal)->USleep(osal, 1000000);

    if (!PrbsIsWorking(self))
        {
        AtHalWrite(hal, cThaQdr2TestControl, 0);
        AtDriverLog(driver, cAtLogLevelWarning, "Qdr#%u PRBS is not working\r\n", qdrId);
        return cAtFalse;
        }

    regVal = AtHalRead(hal, cThaFpgaSticky);
    if (!IsSimulated(self))
        {
        if (regVal & cThaFpgaStickyQDRTestErrorMask(qdrId))
            {
            AtHalWrite(hal, cThaQdr2TestControl, 0);
            AtDriverLog(driver, cAtLogLevelWarning, "Qdr#%u test error\r\n", qdrId);
            return cAtFalse;
            }
        }

    AtHalWrite(hal, cThaQdr2TestControl, 0);
    return cAtTrue;
    }

static eAtModuleRamRet MemoryTest(AtRam self, uint32 startAddress, uint32 endAddress, uint32 *firstErrorAddress, uint32 *memTestErrorCount)
    {
    AtUnused(startAddress);
    AtUnused(endAddress);
    AtUnused(firstErrorAddress);
    AtUnused(memTestErrorCount);
    return AtRamIsGood(self) ? cAtOk : cAtErrorMemoryTestFail;
    }

static eAtModuleRamRet InitStatusGet(AtRam self)
    {
    /* It is better to check if calibration is done or not. But this project does
     * not have corresponding register */
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtRam(AtRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtRamOverride, mMethodsGet(self), sizeof(m_AtRamOverride));

        mMethodOverride(m_AtRamOverride, MemoryTest);
        mMethodOverride(m_AtRamOverride, IsGood);
        mMethodOverride(m_AtRamOverride, InitStatusGet);
        }

    mMethodsSet(self, &m_AtRamOverride);
    }

static void Override(AtRam self)
    {
    OverrideAtRam(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011Qdr);
    }

AtRam Tha60150011QdrObjectInit(AtRam self, AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtRamObjectInit(self, ramModule, core, ramId) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtRam Tha60150011QdrNew(AtModuleRam ramModule, AtIpCore core, uint8 ramId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtRam newDdr = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDdr == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011QdrObjectInit(newDdr, ramModule, core, ramId);
    }

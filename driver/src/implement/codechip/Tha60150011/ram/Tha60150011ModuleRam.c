/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : RAM
 *
 * File        : Tha60150011ModuleRam.c
 *
 * Created Date: Dec 9, 2014
 *
 * Description : RAM module of product 60150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60150011ModuleRam.h"
#include "Tha60150011Qdr.h"
#include "../man/Tha60150011DeviceReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleRamMethods  m_AtModuleRamOverride;
static tThaModuleRamMethods m_ThaModuleRamOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ModuleRam);
    }

static uint8 NumDdrGet(AtModuleRam self)
    {
    AtUnused(self);
    return 1;
    }

static uint8 NumZbtGet(AtModuleRam self)
    {
    AtUnused(self);
    return 0;
    }

static uint8 NumQdrGet(AtModuleRam self)
    {
    AtUnused(self);
    return 2;
    }

static AtRam QdrCreate(AtModuleRam self, AtIpCore core, uint8 qdrId)
    {
    return Tha60150011QdrNew(self, core, qdrId);
    }

static uint32 DdrUserClock(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 100000000;
    }

static const char *DdrTypeString(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return "DDR3";
    }

static uint32 QdrUserClock(AtModuleRam self, uint8 qdrId)
    {
    AtUnused(self);
    AtUnused(qdrId);
    return 155520000;
    }

static uint32 DDRUserClockValueStatusRegister(ThaModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return cThaFPGADDRUserClock100MhzValueStatus;
    }

static uint32 QDRUserClockValueStatusRegister(ThaModuleRam self, uint8 qdrId)
    {
    AtUnused(self);
    return (0xF00068UL + qdrId);
    }

static const char *QDRUserClockName(ThaModuleRam self, uint8 qdrId)
    {
    AtUnused(self);
    AtUnused(qdrId);
    return "Feed Back";
    }

static uint32 DdrAddressBusSizeGet(AtModuleRam self, uint8 ddrId)
    {
    AtUnused(self);
    AtUnused(ddrId);
    return 23;
    }

static void OverrideAtModuleRam(AtModuleRam self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleRamOverride, mMethodsGet(self), sizeof(m_AtModuleRamOverride));

        mMethodOverride(m_AtModuleRamOverride, NumDdrGet);
        mMethodOverride(m_AtModuleRamOverride, NumZbtGet);
        mMethodOverride(m_AtModuleRamOverride, NumQdrGet);
        mMethodOverride(m_AtModuleRamOverride, QdrCreate);
        mMethodOverride(m_AtModuleRamOverride, DdrUserClock);
        mMethodOverride(m_AtModuleRamOverride, DdrTypeString);
        mMethodOverride(m_AtModuleRamOverride, QdrUserClock);
        mMethodOverride(m_AtModuleRamOverride, DdrAddressBusSizeGet);
        }

    mMethodsSet(self, &m_AtModuleRamOverride);
    }

static void OverrideThaModuleRam(AtModuleRam self)
    {
    ThaModuleRam ramModule = (ThaModuleRam)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleRamOverride, mMethodsGet(ramModule), sizeof(m_ThaModuleRamOverride));

        mMethodOverride(m_ThaModuleRamOverride, DDRUserClockValueStatusRegister);
        mMethodOverride(m_ThaModuleRamOverride, QDRUserClockValueStatusRegister);
        mMethodOverride(m_ThaModuleRamOverride, QDRUserClockName);
        }

    mMethodsSet(ramModule, &m_ThaModuleRamOverride);
    }

static void Override(AtModuleRam self)
    {
    OverrideAtModuleRam(self);
    OverrideThaModuleRam(self);
    }

AtModuleRam Tha60150011ModuleRamObjectInit(AtModuleRam self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleRamObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleRam Tha60150011ModuleRamNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleRam newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModuleRamObjectInit(newModule, device);
    }

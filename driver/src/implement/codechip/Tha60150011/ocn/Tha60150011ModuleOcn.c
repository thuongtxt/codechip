/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha60150011ModuleOcn.c
 *
 * Created Date: Jun 14, 2014
 *
 * Description : OCN module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/sdh/AtSdhChannelInternal.h"
#include "../../../default/ocn/ThaModuleOcnInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "../man/Tha60150011Device.h"
#include "Tha60150011ModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleOcnMethods m_ThaModuleOcnOverride;

/* Super implementation */
static const tThaModuleOcnMethods *m_ThaModuleOcnMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

/* Table HW map from flat ID 0-47 to slice and local in slice table in OC48 or STM-16
 * when VC4-4c is supported.
 * =================================================================================
 *         GROUP 16                                        GROUP 16                         GROUP 16
 * +---------+---------+---------+            +---------+---------+---------+     +---------+---------+---------+
 * |FLAT STS | SLICE   |LOCAL STS|            |FLAT STS |SLICE    |LOCAL STS|     |FLAT STS | SLICE   |LOCAL STS|
 * +---------+---------+---------+---+        +---------+---------+---------+     +---------+---------+---------+
 * |   0     |   0     |   0     |   |        |   16    |   0     |   8     |     |   32    |   0     |   16    |
 * +---------+---------+---------+   |        +---------+---------+---------+     +---------+---------+---------+
 * |   1     |   0     |   2     |   |        |   17    |   0     |   10    |     |   33    |   0     |   18    |
 * +---------+---------+---------+ group 4    +---------+---------+---------+     +---------+---------+---------+
 * |   2     |   0     |   4     |   |        |   18    |   0     |   12    |     |   34    |   0     |   20    |
 * +---------+---------+---------+   |        +---------+---------+---------+     +---------+---------+---------+
 * |   3     |   0     |   6     |   |        |   19    |   0     |   14    |     |   35    |   0     |   22    |
 * +---------+---------+---------+---+        +---------+---------+---------+     +---------+---------+---------+
 * |   4     |   0     |   1     |   |        |   20    |   0     |   9     |     |   36    |   0     |   17    |
 * +---------+---------+---------+   |        +---------+---------+---------+     +---------+---------+---------+
 * |   5     |   0     |   3     |   |        |   21    |   0     |   11    |     |   37    |   0     |   19    |
 * +---------+---------+---------+ group 4    +---------+---------+---------+     +---------+---------+---------+
 * |   6     |   0     |   5     |   |        |   22    |   0     |   13    |     |   38    |   0     |   21    |
 * +---------+---------+---------+   |        +---------+---------+---------+     +---------+---------+---------+
 * |   7     |   0     |   7     |   |        |   23    |   0     |   15    |     |   39    |   0     |   23    |
 * +---------+---------+---------+---+        +---------+---------+---------+     +---------+---------+---------+
 * |   8     |   1     |   0     |   |        |   24    |   1     |   8     |     |   40    |   1     |   16    |
 * +---------+---------+---------+   |        +---------+---------+---------+     +---------+---------+---------+
 * |   9     |   1     |   2     |   |        |   25    |   1     |   10    |     |   41    |   1     |   18    |
 * +---------+---------+---------+ group 4    +---------+---------+---------+     +---------+---------+---------+
 * |   10    |   1     |   4     |   |        |   26    |   1     |   12    |     |   42    |   1     |   20    |
 * +---------+---------+---------+   |        +---------+---------+---------+     +---------+---------+---------+
 * |   11    |   1     |   6     |   |        |   27    |   1     |   14    |     |   43    |   1     |   22    |
 * +---------+---------+---------+---+        +---------+---------+---------+     +---------+---------+---------+
 * |   12    |   1     |   1     |   |        |   28    |   1     |   9     |     |   44    |   1     |   17    |
 * +---------+---------+---------+   |        +---------+---------+---------+     +---------+---------+---------+
 * |   13    |   1     |   3     |   |        |   29    |   1     |   11    |     |   45    |   1     |   19    |
 * +---------+---------+---------+ group 4    +---------+---------+---------+     +---------+---------+---------+
 * |   14    |   1     |   5     |   |        |   30    |   1     |   13    |     |   46    |   1     |   21    |
 * +---------+---------+---------+   |        +---------+---------+---------+     +---------+---------+---------+
 * |   15    |   1     |   7     |   |        |   31    |   1     |   15    |     |   47    |   1     |   23    |
 * +---------+---------+---------+---+        +---------+---------+---------+     +---------+---------+---------+
 */
static eBool Vc4_4cIsSupported(ThaModuleOcn self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    /* Note: on standby driver, cannot read register to determine if this feature
     * is supported. At the time the following line is added, hardware supports
     * this. */
    if (AtModuleInAccessible((AtModule)self))
        return cAtTrue;

    if (AtDeviceAllFeaturesAvailableInSimulation(device) || ThaDeviceIsEp((ThaDevice)device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= Tha60150011StartVersionSupportVc4_4c(device))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 HwFlatIdToLocalStsInSlice(ThaModuleOcn self, uint8 hwFlatId, uint8 *localStsInSlice)
    {
    if (Vc4_4cIsSupported(self))
        {
        uint8 localInGroup16Sts = (hwFlatId % 16);
        uint8 indexOfGroup16Sts = (hwFlatId / 16);

        uint8 localInGroup4Sts = (localInGroup16Sts % 4);
        uint8 indexOfGroup4Sts = (localInGroup16Sts / 4);

        *localStsInSlice = (uint8)((indexOfGroup4Sts % 2) + (localInGroup4Sts * 2) + (indexOfGroup16Sts * 8));
        return (hwFlatId / 8) % 2;
        }
    
    *localStsInSlice = (hwFlatId / 2);
    return hwFlatId % 2;
    }

static uint8 LineConvertToHwSts(AtSdhChannel self, uint8 sts1Id)
    {
    AtUnused(self);
    return sts1Id;
    }

static uint8 Aug16ConvertToHwSts(AtSdhChannel self, uint8 sts1Id)
    {
    AtUnused(self);
    return sts1Id;
    }

/*
 * +-------+---+---+---+---+----+----+----+----+----+----+----+----+
 * |SW-STS | 0 | 1 | 2 | 3 | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 |
 * +-------+---+---+---+---+----+----+----+----+----+----+----+----+
 * |HW-STS | 0 | 1 | 2 | 3 | 16 | 17 | 18 | 19 | 32 | 33 | 34 | 35 |
 * +-------+---+---+---+---+----+----+----+----+----+----+----+----+
 */
static uint8 Aug4ConvertToHwSts(AtSdhChannel self, uint8 sts1Id)
    {
    uint8 localIdInChannel = sts1Id % 12; /* 0 - 11 */
    uint8 aug4Id = sts1Id / 12; /* 0 - 3 */
    AtUnused(self);
    return (uint8)((localIdInChannel / 4) * 16 + (localIdInChannel % 4) + (aug4Id * 4));
    }

static uint8 Aug1ConvertToHwSts(AtSdhChannel self, uint8 sts1Id)
    {
    /*
     * 0,1,2 -> 0,16,32
     * 3,4,5 -> 1,17,33
     * 6,7,8 -> 2,18,34
     *  ....
     */
    AtUnused(self);
    return (uint8)((sts1Id % 3) * 16 + (sts1Id / 3));
    }

uint8 Stm16SdhChannelSwStsId2HwFlatId(ThaModuleOcn self, AtSdhChannel channel, uint8 sts1Id)
    {
    eAtSdhChannelType layer = AtSdhChannelTypeGet(channel);

    AtUnused(self);

    if (layer == cAtSdhChannelTypeLine)
        return LineConvertToHwSts(channel, sts1Id);

    else if ((layer == cAtSdhChannelTypeAug16)   ||
             (layer == cAtSdhChannelTypeAu4_16c) ||
             (layer == cAtSdhChannelTypeVc4_16c))
        return Aug16ConvertToHwSts(channel, sts1Id);

    else if ((layer == cAtSdhChannelTypeAug4)  ||
             (layer == cAtSdhChannelTypeAu4_4c)||
             (layer == cAtSdhChannelTypeVc4_4c))
        return Aug4ConvertToHwSts(channel, sts1Id);

    return Aug1ConvertToHwSts(channel, sts1Id);
    }

static eAtRet StsIdSw2Hw(ThaModuleOcn self, AtSdhChannel channel, eAtModule moduleId, uint8 sts1Id, uint8 *pHwSliceId, uint8 *pHwStsInSlice)
    {
    uint8 hwFlatId;
    AtUnused(moduleId);

    hwFlatId = Stm16SdhChannelSwStsId2HwFlatId(self, channel, sts1Id);
    *pHwSliceId = HwFlatIdToLocalStsInSlice(self, hwFlatId, pHwStsInSlice);

    return cAtOk;
    }

static uint8 NumSlices(ThaModuleOcn self)
    {
    AtUnused(self);
    return 2;
    }

static uint8 NumStsInOneSlice(ThaModuleOcn self)
    {
    AtUnused(self);
    return 24;
    }

static uint32 RxStsPiMastIdMask(ThaModuleOcn self)
    {
    AtUnused(self);
    return cBit4_0;
    }

static eAtRet ChannelStsIdSw2HwGet(ThaModuleOcn self, AtSdhChannel channel, eAtModule phyModule, uint8 swSts, uint8* sliceId, uint8 *hwStsInSlice)
    {
    eThaPhyModule phyModule_ = phyModule;
    eAtRet ret = m_ThaModuleOcnMethods->ChannelStsIdSw2HwGet(self, channel, phyModule, swSts, sliceId, hwStsInSlice);
    if (phyModule_ == cThaModulePoh)
        *hwStsInSlice = (uint8)((*hwStsInSlice << 1) + *sliceId);

    return ret;
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleOcnMethods = mMethodsGet(ocnModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, m_ThaModuleOcnMethods, sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, StsIdSw2Hw);
        mMethodOverride(m_ThaModuleOcnOverride, NumSlices);
        mMethodOverride(m_ThaModuleOcnOverride, NumStsInOneSlice);
        mMethodOverride(m_ThaModuleOcnOverride, RxStsPiMastIdMask);
        mMethodOverride(m_ThaModuleOcnOverride, ChannelStsIdSw2HwGet);
        }

    mMethodsSet(ocnModule, &m_ThaModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleOcn(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ModuleOcn);
    }

AtModule Tha60150011ModuleOcnObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60150011ModuleOcnNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModuleOcnObjectInit(newModule, device);
    }

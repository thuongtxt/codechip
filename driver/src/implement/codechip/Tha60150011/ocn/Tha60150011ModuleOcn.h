/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha60150011ModuleOcn.h
 * 
 * Created Date: May 6, 2015
 *
 * Description : 60150011 module OCN declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011MODULEOCN_H_
#define _THA60150011MODULEOCN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ocn/ThaModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModuleOcn
    {
    tThaModuleOcn super;
    }tTha60150011ModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60150011ModuleOcnObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011MODULEOCN_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha60150011ModulePweInternal.h
 * 
 * Created Date: May 6, 2015
 *
 * Description : 60150011 PWE internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011MODULEPWEINTERNAL_H_
#define _THA60150011MODULEPWEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../ThaStmPw/pwe/ThaStmPwModulePweV2.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModulePwe * Tha60150011ModulePwe;

typedef struct tTha60150011ModulePweMethods
    {
    eBool (*JitterAttenuatorIsSupported)(Tha60150011ModulePwe self);
    }tTha60150011ModulePweMethods;

typedef struct tTha60150011ModulePwe
    {
    tThaStmPwModulePweV2 super;
    const tTha60150011ModulePweMethods *methods;
    }tTha60150011ModulePwe;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60150011ModulePweObjectInit(AtModule self, AtDevice device);

#endif /* _THA60150011MODULEPWEINTERNAL_H_ */


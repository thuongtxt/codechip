/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PWE
 *
 * File        : Tha60150011ModulePwe.c
 *
 * Created Date: Aug 6, 2014
 *
 * Description : Module PWE of 60150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60150011ModulePweInternal.h"
#include "../../../default/pwe/ThaPweRegV2.h"
#include "AtPwCep.h"
#include "../man/Tha60150011Device.h"

/*--------------------------- Define -----------------------------------------*/
/* Two new fields are added to register cThaRegPWPdhTxRBitControl */
#define cThaPwPdhTxPwCepTypeMask  cBit4_3
#define cThaPwPdhTxPwCepTypeShift 3

#define cThaPwPdhTxPwCepEnMask    cBit2
#define cThaPwPdhTxPwCepEnShift   2

#define cRegPmcPwTxPktCnt(ro)     (0x510000UL + (ro) * 0x800UL)
#define cRegPmcPwTxPldOctCnt(ro)  (0x511000UL + (ro) * 0x800UL)
#define cRegPmcPwTxLbitPktCnt(ro) (0x512000UL + (ro) * 0x800UL)
#define cRegPmcPwTxRbitPktCnt(ro) (0x513000UL + (ro) * 0x800UL)
#define cRegPmcPwTxMorNbitCnt(ro) (0x514000UL + (ro) * 0x800UL)
#define cRegPmcPwTxPbitCnt(ro)    (0x515000UL + (ro) * 0x800UL)

#define cTxPwSuppressEnMask  cBit7
#define cTxPwSuppressEnShift 7
#define cMbitValCtrlMask     cBit4_3
#define cMbitValCtrlShift    3
#define cTxPwAutoMbitMask    cBit6
#define cTxPwAutoMbitShift   6
#define cTxPwAutoLbitMask    cBit5
#define cTxPwAutoLbitShift   5

#define cTxPwJitterAttenuatorEnMask    cBit18
#define cTxPwJitterAttenuatorEnShift   18

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60150011ModulePwe)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60150011ModulePweMethods m_methods;

/* Override */
static tThaModulePweMethods m_ThaModulePweOverride;

/* Save super implementations */
static const tThaModulePweMethods *m_ThaModulePweMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 PwCepTypeGet(AtPw pwAdapter, eAtPwType pwType)
    {
    AtPwCep pwCep;
    AtSdhChannel sdhVc;
    eAtSdhChannelType vcType;

    if (pwType != cAtPwTypeCEP)
        return 0;

    pwCep = (AtPwCep)ThaPwAdapterPwGet((ThaPwAdapter)pwAdapter);
    if (AtPwCepModeGet(pwCep) != cAtPwCepModeFractional)
        return 0;

    sdhVc = (AtSdhChannel)AtPwBoundCircuitGet(pwAdapter);
    vcType = AtSdhChannelTypeGet(sdhVc);

    if (vcType == cAtSdhChannelTypeVc3)
        return 1;

    if (vcType == cAtSdhChannelTypeVc4)
        return 2;

    return 0;
    }

static eAtRet PwTypeSet(ThaModulePwe self, AtPw pwAdapter, eAtPwType pwType)
    {
    eAtRet ret;
    uint32 regAddr, regVal, offset;

    if (self == NULL)
        return cAtErrorNullPointer;

    /* Let super do first */
    ret = m_ThaModulePweMethods->PwTypeSet(self, pwAdapter, pwType);
    if (ret != cAtOk)
        return ret;

    offset = mMethodsGet(self)->PwPweDefaultOffset(self, pwAdapter);
    regAddr = cThaRegPWPdhTxRBitControl + offset;
    regVal  = mChannelHwRead(pwAdapter, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cThaPwPdhTxPwCepEn, (pwType == cAtPwTypeCEP) ? 1 : 0);
    mRegFieldSet(regVal, cThaPwPdhTxPwCepType, PwCepTypeGet(pwAdapter, pwType));
    mChannelHwWrite(pwAdapter, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static uint32 PDHPWAddingProtocolToPreventBurst(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return 0x200003;
    }

static uint32 PDHPWAddingPwMask(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cBit15_4;
    }

static uint32 PWPdhPlaPldSizeCtrl(ThaModulePwe self)
    {
    AtUnused(self);
    return 0x202000;
    }

static uint32 PWPdhPlaEngStat(ThaModulePwe self)
    {
    AtUnused(self);
    return 0x202800;
    }

static uint32 PDHPWPlaSignalingInverseLookupControl(ThaModulePwe self)
    {
    AtUnused(self);
    return 0x203000;
    }

static ThaModulePw PwModule(ThaModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static eAtRet PwNxDs0Set(ThaModulePwe self, AtPw pw, AtPdhNxDS0 nxDs0)
    {
    /* Temporarily ignore this method until this product supports CAS */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(nxDs0);
    return cAtOk;
    }

static uint32 PwPlaDefaultOffset(ThaModulePwe self, AtPw pw)
    {
    ThaModulePw pwModule = PwModule(self);
    uint8 slice = ThaPwAdapterPweCircuitSlice((ThaPwAdapter)pw, AtPwBoundCircuitGet(pw));
    return AtChannelHwIdGet((AtChannel)pw) + (slice * 0x010000UL) + ThaModulePwePartOffset(self, ThaModulePwPartOfPw(pwModule, pw));
    }

static uint32 PwTxPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cRegPmcPwTxPktCnt(mRo(clear)) + mPwPweOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwTxBytesGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cRegPmcPwTxPldOctCnt(mRo(clear)) + mPwPweOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwTxLbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cRegPmcPwTxLbitPktCnt(mRo(clear)) + mPwPweOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwTxRbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cRegPmcPwTxRbitPktCnt(mRo(clear)) + mPwPweOffset(self, pw), cThaModulePwPmc);
    }

static uint32 PwTxMbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cRegPmcPwTxMorNbitCnt((clear) ? 0 : 1) + mPwPweOffset(self, pw), cThaModulePwe);
    }

static uint32 PwTxPbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cRegPmcPwTxPbitCnt((clear) ? 0 : 1) + mPwPweOffset(self, pw), cThaModulePwe);
    }

static uint32 PwTxNbitPacketsGet(ThaModulePwe self, AtPw pw, eBool clear)
    {
    return mChannelHwRead(pw, cRegPmcPwTxMorNbitCnt((clear) ? 0 : 1) + mPwPweOffset(self, pw), cThaModulePwe);
    }

static eAtRet PwHeaderCepEbmEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    /* This product does not support CEP fractional */
    AtUnused(self);
    AtUnused(pw);
    AtUnused(enable);
    return cAtOk;
    }

static eAtRet PwCwAutoTxNPBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    /* This feature has not been supported */
    AtUnused(self);
    AtUnused(pw);
    return enable ? cAtOk : cAtErrorModeNotSupport;
    }

static eBool PwCwAutoTxNPBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtTrue;
    }

static eAtRet PwSupressEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cTxPwSuppressEn, enable ? 1 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwSupressIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    return (regVal & cTxPwSuppressEnMask) ? cAtTrue : cAtFalse;
    }

static eAtRet PwTxMBitForce(ThaModulePwe self, AtPw pw, eBool force)
    {
    uint32 regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    mRegFieldSet(regVal, cMbitValCtrl, (force) ? 2 : 0);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwTxMBitIsForced(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    uint32 regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);

    return (mRegField(regVal, cMbitValCtrl) == 2) ? cAtTrue : cAtFalse;
    }

static eAtRet PwCwAutoTxMBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cTxPwAutoMbit, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxMBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cTxPwAutoMbitMask) ? cAtFalse : cAtTrue;
    }

static eAtRet PwCwAutoTxLBitEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    mRegFieldSet(regVal, cTxPwAutoLbit, enable ? 0 : 1);
    mChannelHwWrite(pw, regAddr, regVal, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxLBitIsEnabled(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, regVal;

    regAddr = mMethodsGet(self)->PwTxEnCtrl(self) + mPwPweOffset(self, pw);
    regVal  = mChannelHwRead(pw, regAddr, cThaModulePwe);
    return (regVal & cTxPwAutoLbitMask) ? cAtFalse : cAtTrue;
    }

static eAtRet PwRamStatusClear(ThaModulePwe self, AtPw pw)
    {
    uint32 regAddr, offset;
    uint32 longReg[cThaLongRegMaxSize];
    AtOsal osal;

    /* Offset need circuit, if circuit is NULL, do nothing */
    if (AtPwBoundCircuitGet(pw) == NULL)
        return cAtOk;

    offset = mPwPlaOffset(self, pw);
    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, longReg, 0, sizeof(longReg));

    regAddr = mMethodsGet(self)->PWPdhPlaEngStat(self) + offset;
    mChannelHwLongWrite(pw, regAddr, longReg, cThaLongRegMaxSize, cThaModulePwe);

    return cAtOk;
    }

static eBool PwCwAutoTxNPBitIsConfigurable(ThaModulePwe self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void PwPlaRegsShow(ThaModulePwe self, AtPw pw)
    {
    if (AtPwBoundCircuitGet(pw) == NULL)
        {
        AtPrintc(cSevInfo, "   - Pw has not bound circuit\r\n");
        return;
        }

    m_ThaModulePweMethods->PwPlaRegsShow(self, pw);
    }

static eBool JitterAttenuatorIsSupported(Tha60150011ModulePwe self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceVersionNumber(device) >= Tha60150011DeviceStartVersionSupportJitterAttenuator(device))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PwJitterAttenuatorEnable(ThaModulePwe self, AtPw pw, eBool enable)
    {
    uint32 address, regVal;

    if (!mMethodsGet(mThis(self))->JitterAttenuatorIsSupported(mThis(self)))
        return cAtOk;

    address = mMethodsGet(self)->PWPdhPlaPldSizeCtrl(self) + mPwPlaOffset(self, pw);
    regVal = mChannelHwRead(pw, address, cThaModulePwe);
    mRegFieldSet(regVal, cTxPwJitterAttenuatorEn, mBoolToBin(enable));

    mChannelHwWrite(pw, address, regVal, cThaModulePwe);

    return cAtOk;
    }

static void OverrideThaModulePwe(AtModule self)
    {
    ThaModulePwe pweModule = (ThaModulePwe)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePweMethods = mMethodsGet(pweModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePweOverride, m_ThaModulePweMethods, sizeof(m_ThaModulePweOverride));

        mMethodOverride(m_ThaModulePweOverride, PwTypeSet);
        mMethodOverride(m_ThaModulePweOverride, PDHPWAddingProtocolToPreventBurst);
        mMethodOverride(m_ThaModulePweOverride, PDHPWAddingPwMask);
        mMethodOverride(m_ThaModulePweOverride, PWPdhPlaPldSizeCtrl);
        mMethodOverride(m_ThaModulePweOverride, PWPdhPlaEngStat);
        mMethodOverride(m_ThaModulePweOverride, PwPlaDefaultOffset);
        mMethodOverride(m_ThaModulePweOverride, PwNxDs0Set);
        mMethodOverride(m_ThaModulePweOverride, PwTxPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxBytesGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxLbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxRbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxMbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxPbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwTxNbitPacketsGet);
        mMethodOverride(m_ThaModulePweOverride, PwHeaderCepEbmEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxNPBitIsConfigurable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxNPBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwSupressEnable);
        mMethodOverride(m_ThaModulePweOverride, PwSupressIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwTxMBitForce);
        mMethodOverride(m_ThaModulePweOverride, PwTxMBitIsForced);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxMBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxMBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxLBitEnable);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxLBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PDHPWPlaSignalingInverseLookupControl);
        mMethodOverride(m_ThaModulePweOverride, PwCwAutoTxNPBitIsEnabled);
        mMethodOverride(m_ThaModulePweOverride, PwRamStatusClear);
        mMethodOverride(m_ThaModulePweOverride, PwPlaRegsShow);
        mMethodOverride(m_ThaModulePweOverride, PwJitterAttenuatorEnable);
        }

    mMethodsSet(pweModule, &m_ThaModulePweOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModulePwe(self);
    }

static void MethodsInit(AtModule self)
    {
    Tha60150011ModulePwe module = (Tha60150011ModulePwe)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, JitterAttenuatorIsSupported);
        }

    mMethodsSet(module, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ModulePwe);
    }

AtModule Tha60150011ModulePweObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwModulePweV2ObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60150011ModulePweNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModulePweObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60150011SSKeyChecker.c
 *
 * Created Date: Jan 06, 2015
 *
 * Description : SSKey checker used for 60150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtSSKeyCheckerInternal.h"
#include "../../../default/man/ThaSSKeyChecker.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60150011SSKeyChecker
    {
    tAtSSKeyChecker super;
    }tTha60150011SSKeyChecker;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSSKeyCheckerMethods m_AtSSKeyCheckerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011SSKeyChecker);
    }

static uint32 StickyAddress(AtSSKeyChecker self)
    {
    AtUnused(self);
    return 0x140006;
    }

static uint32 CodeMsbAddress(AtSSKeyChecker self)
    {
    AtUnused(self);
    return 0x140001;
    }

static uint32 CodeLsbAddress(AtSSKeyChecker self)
    {
    AtUnused(self);
    return 0x140002;
    }

static uint32 CodeExpectedMsb(AtSSKeyChecker self)
    {
    AtUnused(self);
    return 0x02600000;
    }

static uint32 CodeExpectedLsb(AtSSKeyChecker self)
    {
    AtUnused(self);
    return 0x10562001;
    }

static eBool CodeMatch(AtSSKeyChecker self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self->device, 0);

    /* Check if code match */
    if ((AtHalRead(hal, mMethodsGet(self)->CodeMsbAddress(self)) == mMethodsGet(self)->CodeExpectedMsb(self)) &&
        (AtHalRead(hal, mMethodsGet(self)->CodeLsbAddress(self)) == mMethodsGet(self)->CodeExpectedLsb(self)))
        return cAtTrue;

    return cAtFalse;
    }

/* TODO: move back to super method when HW is ready */
static eAtRet Check(AtSSKeyChecker self)
    {
    const uint8 cCheckingTime = 20;
    AtHal hal = AtDeviceIpCoreHalGet(self->device, 0);
    uint32 address = mMethodsGet(self)->StickyAddress(self);
    uint8 i;

    if (!CodeMatch(self))
        return cAtErrorInvalidSSkey;

    /* Ignore the first time */
    AtHalWrite(hal, 0x140000, 0x10);
    AtHalRead(hal, address);

    /* Read many times and make sure that the key is stable (0) */
    for (i = 0; i < cCheckingTime; i++)
        {
        if (AtHalRead(hal, address) != 0)
            return cAtErrorInvalidSSkey;
        }

    AtHalWrite(hal, 0x140000, 0x0);
    return cAtOk;
    }

static void OverrideAtSSKeyChecker(AtSSKeyChecker self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSSKeyCheckerOverride, mMethodsGet(self), sizeof(m_AtSSKeyCheckerOverride));

        mMethodOverride(m_AtSSKeyCheckerOverride, StickyAddress);
        mMethodOverride(m_AtSSKeyCheckerOverride, CodeMsbAddress);
        mMethodOverride(m_AtSSKeyCheckerOverride, CodeLsbAddress);
        mMethodOverride(m_AtSSKeyCheckerOverride, CodeExpectedMsb);
        mMethodOverride(m_AtSSKeyCheckerOverride, CodeExpectedLsb);
        mMethodOverride(m_AtSSKeyCheckerOverride, Check);
        }

    mMethodsSet(self, &m_AtSSKeyCheckerOverride);
    }

static void Override(AtSSKeyChecker self)
    {
    OverrideAtSSKeyChecker(self);
    }

static AtSSKeyChecker ObjectInit(AtSSKeyChecker self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (AtSSKeyCheckerObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSSKeyChecker Tha60150011SSKeyCheckerNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSSKeyChecker newChecker = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChecker == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChecker, device);
    }

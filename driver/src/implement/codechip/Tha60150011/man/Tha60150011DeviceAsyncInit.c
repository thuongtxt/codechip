/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60150011DeviceAsyncInit.c
 *
 * Created Date: Aug 16, 2016
 *
 * Description : To handle device async initializing
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtIpCoreInternal.h"
#include "Tha60150011DeviceInternal.h"
#include "Tha60150011DeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/
#define cSsKeyStableTimeInMs	3000
#define cNumUsPerMs             1000
#define cStableTimeInMsAfterSemCheck 10
#define cTimeInMsWaitForFpgaStable 500

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60150011Device)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef enum eDeviceInitState
    {
    cDeviceInitStateOldSequenceInit = 0,
    cDeviceInitStateEntranceToInit,
    cDeviceInitStateWaitForFpgaStable,
    cDeviceInitStateDiagnostic,
    cDeviceInitStateSsKeypCheck,
    cDeviceInitStateM13Init,
    cDeviceInitStateSupperFirstInit,
    cDeviceInitStateSetup,
    cDeviceInitStateSupperSecondInit,
    cDeviceInitStateActivate,
    cDeviceInitStateEyeScan,
    cDeviceInitStateSdhSerdesReset,
    cDeviceInitStateSemCheck,
    cDeviceInitStatePostWaitStable,
    cDeviceInitStatePost
    }eDeviceInitState;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void StateReset(uint32 *state)
    {
    *state = cDeviceInitStateOldSequenceInit;
    }

static eAtRet OldSequenceInitStateMove(AtDevice self, uint32 *state)
    {
    if (!Tha60150011NewStartupSequenceIsSupported(self))
        return Tha60150011SuperAsyncInit(self);
    else
        *state = cDeviceInitStateEntranceToInit;

    return cAtErrorAgain;
    }

static void EntranceToInit(AtDevice self)
    {
    AtModuleEth ethModule;
    ThaDevice device;

    device = (ThaDevice)self;
    device->resetIsInProgress = cAtTrue;

    /* As hardware recommend, Ethernet ports should be de-activated before
     * initializing device. Device initialization process will activate these
     * ports */
    ethModule = (AtModuleEth)AtDeviceModuleGet(self, cAtModuleEth);
    ThaModuleEthAllPortMacsActivate(ethModule, cAtFalse);
    }

static eAtRet EntranceToInitStateMove(AtDevice self, tAtOsalCurTime *prevTime, uint32 *state)
    {
    EntranceToInit(self);
    AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "EntranceToInit");
    *state = cDeviceInitStateWaitForFpgaStable;
    AtOsalCurTimeGet(prevTime);
    AtDeviceAsyncInitNeedFixDelayTimeSet(self, cTimeInMsWaitForFpgaStable * cNumUsPerMs);
    return cAtRetNeedDelay;
    }

static eAtRet WaitForFpgaStableStateMove(AtDevice self, tAtOsalCurTime *prevTime, uint32 *state)
    {
    /* Wait 500ms for FPGA stable */
    if (AtDeviceAccessible(self))
        {
        tAtOsalCurTime curTime;
	    uint32 diffTimeInMs;

	    AtOsalCurTimeGet(&curTime);
	    diffTimeInMs = AtOsalDifferenceTimeInMs(&curTime, prevTime);
        if (diffTimeInMs >= cTimeInMsWaitForFpgaStable)
            {
            AtDeviceAsyncInitNeedFixDelayTimeSet(self, 0);
            *state = cDeviceInitStateDiagnostic;
            }
        else
            {
            AtDeviceAsyncInitNeedFixDelayTimeSet(self, (cTimeInMsWaitForFpgaStable - diffTimeInMs) * cNumUsPerMs);
            return cAtRetNeedDelay;
            }
        }
    else
        *state = cDeviceInitStateDiagnostic;
    return cAtErrorAgain;
    }

static eAtRet DiagnosticStateMove(AtDevice self, tAtOsalCurTime *prevTime, uint32 *state)
    {
    eAtRet ret = cAtOk;

    if (Tha60150011DeviceShouldCheckDiagnostic(self))
        {
        ret = Tha60150011DeviceDiagnosticAsyncInit(self);
        AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "DeviceDiagnostic");
        if ((ret != cAtOk) && (!AtDeviceAsyncRetValIsInState(ret)))
            {
            StateReset(state);
            return ret;
            }
        else if (AtDeviceAsyncRetValIsInState(ret))
            return ret;
        }

    *state = cDeviceInitStateSsKeypCheck;
    AtOsalCurTimeGet(prevTime);
    AtDeviceAsyncInitNeedFixDelayTimeSet(self, cSsKeyStableTimeInMs * cNumUsPerMs);

    return cAtErrorAgain;
    }

static eAtRet SsKeyCheckStateMove(AtDevice self, tAtOsalCurTime *prevTime, uint32 *state)
    {
    eAtRet ret = cAtOk;

    /* Check SSKey, give the key engine a moment so that it can run and
     * update status */
    if (mMethodsGet(self)->NeedCheckSSKey(self))
        {
        tAtOsalCurTime curTime;
	    uint32 diffTimeInMs;

	    AtOsalCurTimeGet(&curTime);
	    diffTimeInMs = AtOsalDifferenceTimeInMs(&curTime, (const void*)&prevTime);
        if (diffTimeInMs >= cSsKeyStableTimeInMs)
            {
            AtDeviceAsyncInitNeedFixDelayTimeSet(self, 0);
            ret = self->methods->SSKeyCheck(self);
            AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "SSKeyCheck");
            if (ret != cAtOk)
                {
                AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation, "Check SS key fail\r\n");
                StateReset(state);
                return ret;
                }
            }
        else
            {
            AtDeviceAsyncInitNeedFixDelayTimeSet(self, (cSsKeyStableTimeInMs - diffTimeInMs)*cNumUsPerMs);
            return cAtRetNeedDelay;
            }
        }

    *state = cDeviceInitStateM13Init;
    return cAtErrorAgain;
    }

static eAtRet M13InitStateMove(AtDevice self, uint32 *state)
    {
    Tha60150011DeviceM13Init(self);
    AtDeviceAccessTimeCountSinceLastProfileCheck(self, cAtTrue, AtSourceLocation, "M13Init");

    if (mThis(self)->deviceIsActivated)
        *state = cDeviceInitStateSupperFirstInit;
    else
        *state = cDeviceInitStateSetup;

    return cAtErrorAgain;
    }

static eAtRet SupperFistInitStateMove(AtDevice self, uint32 *state)
    {
    /* Just try to re-initialize all modules to stop all traffics to hardware
     * before starting initializing sequence. After soft reset, modules can be
     * in unknown state. It's time to give all of modules a fresh state */
    eAtRet ret = mMethodsGet(self)->AllModulesAsyncInit(self);
    if (ret == cAtOk)
        {
        *state = cDeviceInitStateSetup;
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
        StateReset(state);

    return ret;
    }

static eAtRet SetupStateMove(AtDevice self, uint32 *state)
    {
    eAtRet ret = AtDeviceAllModulesSetup(self);

    if (ret == cAtOk)
        {
        *state = cDeviceInitStateSupperSecondInit;
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
        {
        StateReset(state);
        AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation, "All modules setup fail\r\n");
        }

    return ret;
    }

static eAtRet SupperSecondInitStateMove(AtDevice self, uint32 *state)
    {
    /* Just try to re-initialize all modules to stop all traffics to hardware
     * before starting initializing sequence. After soft reset, modules can be
     * in unknown state. It's time to give all of modules a fresh state */
    eAtRet ret = mMethodsGet(self)->AllModulesAsyncInit(self);
    if (ret == cAtOk)
        {
        *state = cDeviceInitStateActivate;
        ret = cAtErrorAgain;
        }
    else if (!AtDeviceAsyncRetValIsInState(ret))
        StateReset(state);

    return ret;
    }

static eAtRet ActivateStateMove(AtDevice self, uint32 *state)
    {
    /* It's time to activate all cores */
    eAtRet ret = AtDeviceAllCoresActivate(self, cAtTrue);
    if(ret != cAtOk)
        {
        AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation, "All cores activate fail\r\n");
        StateReset(state);
        return ret;
        }
    *state = cDeviceInitStateEyeScan;
    return cAtErrorAgain;
    }

static eAtRet EyeScanStateMove(AtDevice self, uint32 *state)
    {
    /* Make eye-scan state machine to default state */
    AtUnused(self);
    *state = cDeviceInitStateSdhSerdesReset;
    return cAtErrorAgain;
    }

static eAtRet SdhSerdesResetStateMove(AtDevice self, uint32 *state)
    {
    eAtRet ret = Tha60150011DeviceSdhSerdesReset(self);
    if (ret != cAtOk)
        {
        AtDeviceLog(self, cAtLogLevelWarning, AtSourceLocation, "SDH serdes reset fail\r\n");
        StateReset(state);
        return ret;
        }

    *state = cDeviceInitStateSemCheck;
    return cAtErrorAgain;
    }

static eAtRet SemCheckStateMove(AtDevice self, tAtOsalCurTime *prevTime, uint32 *state)
    {
    Tha60150011DeviceSemCheck(self);
    *state = cDeviceInitStatePostWaitStable;
    AtOsalCurTimeGet(prevTime);
    AtDeviceAsyncInitNeedFixDelayTimeSet(self, cStableTimeInMsAfterSemCheck*cNumUsPerMs);
    return cAtErrorAgain;
    }

static eAtRet PostWaitStableStateMove(AtDevice self, tAtOsalCurTime *prevTime, uint32 *state)
    {
    /* Wait 10ms for FPGA stable */
    if (AtDeviceAccessible(self))
        {
        tAtOsalCurTime curTime;
	    uint32 diffTimeInMs;

	    AtOsalCurTimeGet(&curTime);
	    diffTimeInMs = AtOsalDifferenceTimeInMs(&curTime, prevTime);
        if (diffTimeInMs >= cStableTimeInMsAfterSemCheck)
            {
            AtDeviceAsyncInitNeedFixDelayTimeSet(self, 0);
            *state = cDeviceInitStatePost;
            }
        else
            {
            AtDeviceAsyncInitNeedFixDelayTimeSet(self, (cStableTimeInMsAfterSemCheck - diffTimeInMs)*cNumUsPerMs);
            return cAtRetNeedDelay;
            }
        }
    else
        *state = cDeviceInitStatePost;
    return cAtErrorAgain;
    }

static eAtRet PostStateMove(AtDevice self, uint32 *state)
    {
    ThaDevice device;

    device = (ThaDevice)self;
    device->resetIsInProgress = cAtFalse;
    StateReset(state);
    return cAtOk;
    }

static void AsyncInitStateSet(AtDevice self, uint32 state)
    {
    mThis(self)->asyncInitState = state;
    }

static uint32 AsyncInitStateGet(AtDevice self)
    {
    return mThis(self)->asyncInitState;
    }

static void AsyncInitPrevTimeSet(AtDevice self, tAtOsalCurTime *time)
    {
    AtOsalMemCpy(&mThis(self)->prevStateTime, time, sizeof(tAtOsalCurTime));
    }

static void AsyncInitPrevTimeGet(AtDevice self, tAtOsalCurTime *time)
    {
    AtOsalMemCpy(time, &mThis(self)->prevStateTime, sizeof(tAtOsalCurTime));
    }

static eBool Activated(AtDevice self)
    {
    return AtIpCoreIsActivated(AtDeviceIpCoreGet(self, 0));
    }

eAtRet Tha60150011DeviceAsyncInit(AtDevice self)
    {
    uint32 state = AsyncInitStateGet(self);
    tAtOsalCurTime prevTime, profileTime;
    eAtRet ret;
    AsyncInitPrevTimeGet(self, &prevTime);

    AtOsalCurTimeGet(&profileTime);

    /* Device activated status information is used for optimized logic, to match with AtDeviceInit sequence,
     * it must be get at the very first step, and only get once */
    if (!mThis(self)->didGetDeviceIsActived)
        {
        mThis(self)->deviceIsActivated = Activated(self);
        mThis(self)->didGetDeviceIsActived = cAtTrue;
        }

    switch (state)
        {
        case cDeviceInitStateOldSequenceInit:
            ret = OldSequenceInitStateMove(self, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "OldSequenceInitStateMove");
            break;
        case cDeviceInitStateEntranceToInit:
            ret = EntranceToInitStateMove(self, &prevTime, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "EntranceToInitStateMove");
            break;
        case cDeviceInitStateWaitForFpgaStable:
            ret = WaitForFpgaStableStateMove(self, &prevTime, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "WaitForFpgaStableStateMove");
            break;
        case cDeviceInitStateDiagnostic:
            ret = DiagnosticStateMove(self, &prevTime, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "DiagnosticStateMove");
            break;
        case cDeviceInitStateSsKeypCheck:
            ret = SsKeyCheckStateMove(self, &prevTime, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "SsKeyCheckStateMove");
            break;
        case cDeviceInitStateM13Init:
            ret = M13InitStateMove(self, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "M13InitStateMove");
            break;
        case cDeviceInitStateSupperFirstInit:
            ret = SupperFistInitStateMove(self, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "SupperFistInitStateMove");
            break;
        case cDeviceInitStateSetup:
            ret = SetupStateMove(self, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "SetupStateMove");
            break;
        case cDeviceInitStateSupperSecondInit:
            ret = SupperSecondInitStateMove(self, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "SupperSecondInitStateMove");
            break;
        case cDeviceInitStateActivate:
            ret = ActivateStateMove(self, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "ActivateStateMove");
            break;
        case cDeviceInitStateEyeScan:
            ret = EyeScanStateMove(self, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "EyeScanStateMove");
            break;
        case cDeviceInitStateSdhSerdesReset:
            ret = SdhSerdesResetStateMove(self, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "SdhSerdesResetStateMove");
            break;
        case cDeviceInitStateSemCheck:
            ret = SemCheckStateMove(self, &prevTime, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "SemCheckStateMove");
            break;
        case cDeviceInitStatePostWaitStable:
            ret = PostWaitStableStateMove(self, &prevTime, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "PostWaitStableStateMove");
            break;
        case cDeviceInitStatePost:
            ret = PostStateMove(self, &state);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "PostStateMove");
            break;
        default:
            StateReset(&state);
            ret = cAtErrorDevFail;
        }

    AsyncInitStateSet(self, state);
    AsyncInitPrevTimeSet(self, &prevTime);

    /* If async init done, reset flag to get device is activated next time */
    if (!AtDeviceAsyncRetValIsInState(ret))
        mThis(self)->didGetDeviceIsActived = cAtFalse;

    return ret;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device
 * 
 * File        : Tha60150011DeviceInternal.h
 * 
 * Created Date: Mar 27, 2015
 *
 * Description : 60150011 device definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011DEVICEINTERNAL_H_
#define _THA60150011DEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60030081/man/Tha60030081Device.h"
#include "Tha60150011Device.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/
#define mNoneDebuggerPrint(color, string)                                     \
        if (!debugger)                                                         \
            AtPrintc(color, string);

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011Device * Tha60150011Device;

typedef struct tRamMarginParams
    {
    uint32   readLeftMargin;
    uint32   readRightMargin;
    uint32   writeLeftMargin;
    uint32   writeRightMargin;
    }tRamMarginParams;

typedef struct tTha60150011DeviceMethods
    {
    eBool  (*ShouldShowCommonDebugInfo)(AtDevice self);
    eBool  (*NewStartupSequenceIsSupported)(AtDevice self);
    eAtRet (*HalTest)(AtDevice self);
    eBool  (*OcnClockIsGood)(AtDevice self);
    uint8  (*NumCoreClocks)(Tha60150011Device self);
    uint32 (*CoreClockValueInHz)(Tha60150011Device self, uint8 core);
    void   (*StickyDisplay)(Tha60150011Device self);
    void   (*StatusDisplay)(Tha60150011Device self);
    void   (*ClockStatusDisplay)(Tha60150011Device self);
    void   (*RamMarginDisplay)(Tha60150011Device self);
    void   (*OcnClockStatusDisplay)(Tha60150011Device self);
    uint32 (*XGMIIClock156_25MhzValueStatusRegister)(Tha60150011Device self, uint8 clockId);
    const char *(*XGMIIClockName)(Tha60150011Device self, uint8 clockId);
    uint8  (*NumXGMIIClock156_25Mhz)(Tha60150011Device self);
    eBool  (*DdrCalibIsGood)(AtDevice self);
    eAtRet (*QdrDiagnostic)(AtDevice self);
    eBool  (*PllIsLocked)(AtDevice self);
    eBool  (*EthIntfClockIsGood)(AtDevice self);
    uint32 (*ClockPcieSwingRangeInPpm)(Tha60150011Device self);
    eBool  (*NeedCoreResetWhenDiagCheck)(Tha60150011Device self);
    uint32 (*StartVersionSupportJitterAttenuator)(Tha60150011Device self);
    eBool  (*SemUartIsSupported)(Tha60150011Device self);
    eAtRet (*ResetStatusCheck)(Tha60150011Device self);
    eAtRet (*AsyncResetStatusCheck)(Tha60150011Device self);
    eAtRet (*SemClockProvide)(Tha60150011Device self);
    eAtRet (*SdhSerdesReset)(AtDevice self);
    eBool  (*ShouldSemCheck)(Tha60150011Device self);
    void (*HwFlush)(Tha60150011Device self);
    }tTha60150011DeviceMethods;

typedef struct tTha60150011Device
    {
    tTha60030081Device super;
    const tTha60150011DeviceMethods *methods;

    tRamMarginParams ddrMargin;
    tRamMarginParams qdrMargin[2];
    uint32 maxQdrMarginDetectTime;

    eBool qdrEyeIsCentered[2];
    uint32 minReadLeftTaps[2];
    uint32 minReadRightTaps[2];
    uint32 minWriteLeftTaps[2];
    uint32 minWriteRightTaps[2];

    /* Async init */
    uint32 asyncInitState;
    tAtOsalCurTime prevStateTime;
    uint32 diagAsyncState;
    eBool didReset;
    eBool deviceIsActivated;
    eBool didGetDeviceIsActived;
    }tTha60150011Device;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60150011DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011DEVICEINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device 60150011
 * 
 * File        : Tha60150011DeviceReg.h
 * 
 * Created Date: Dec 5, 2014
 *
 * Description : Device 60150011 registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011DEVICEREG_H_
#define _THA60150011DEVICEREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
/* FPGA Sticky */
#define cThaFpgaSticky 0x00F00050

#define cThaFpgaStickyQDRTestErrorMask(qdrId)     (((qdrId) == 0) ? cBit13 : cBit15)
#define cThaFpgaStickyQDRTestErrorShift(qdrId)    (((qdrId) == 0) ? 13 : 15)

#define cThaFpgaStickyQDRParityErrorMask(qdrId)   (((qdrId) == 0) ? cBit12 : cBit14)
#define cThaFpgaStickyQDRParityErrorShift(qdrId)  (((qdrId) == 0) ? 12 : 14)

#define cThaFpgaStickyOcnSerdesTxResetStateMask   cBit11
#define cThaFpgaStickyOcnSerdesTxResetStateShift  11

#define cThaFpgaStickyOcnSerdesRxResetStateMask   cBit10
#define cThaFpgaStickyOcnSerdesRxResetStateShift  10

#define cThaFpgaStickyCKOcnRef155NotLockedMask    cBit9
#define cThaFpgaStickyCKOcnRef155NotLockedShift   9

#define cThaFpgaStickyQDRTestNotDoneMask(qdrId)             (((qdrId) == 0) ? cBit7 : cBit8)
#define cThaFpgaStickyQDRTestNotDoneShift(qdrId)            (((qdrId) == 0) ? 7 : 8)

#define cThaFpgaStickyDDR3CalibFailMask                     cBit6
#define cThaFpgaStickyDDR3CalibFailShift                    6

#define cThaFpgaStickyCKXgmii156NotLockedMask               cBit5
#define cThaFpgaStickyCKXgmii156NotLockedShift              5

#define cThaFpgaStickyCKQdrNo2FBNotLockedMask               cBit4
#define cThaFpgaStickyCKQdrNo2FBNotLockedShift              4

#define cThaFpgaStickyCKQdrNo1FBNotLockedMask               cBit3
#define cThaFpgaStickyCKQdrNo1FBNotLockedShift              3

#define cThaFpgaStickyCKCore2NotLockedMask                  cBit2
#define cThaFpgaStickyCKCore2NotLockedShift                 2

#define cThaFpgaStickyCKCore1NotLockedMask                  cBit1
#define cThaFpgaStickyCKCore1NotLockedShift                 1

#define cThaFpgaStickyCK155NotLockedMask                    cBit0
#define cThaFpgaStickyCK155NotLockedShift                   0

/* FPGA Status */
#define cThaFpgaCurrentStatus                               0x00F00060
#define cThaFpgaCurrentStatusXgmiiLinkUpMask                cBit12
#define cThaFpgaCurrentStatusXgmiiLinkUpShift               12

#define cThaFpgaCurrentStatusOcnSerdesTxNormalStateMask     cBit11
#define cThaFpgaCurrentStatusOcnSerdesTxNormalStateShift    11

#define cThaFpgaCurrentStatusOcnSerdesRxNormalStateMask     cBit10
#define cThaFpgaCurrentStatusOcnSerdesRxNormalStateShift    10

#define cThaFpgaCurrentStatusCKOcnRef155LockedMask          cBit9
#define cThaFpgaCurrentStatusCKOcnRef155LockedShift         9

#define cThaFpgaCurrentStatusDDR3CalibDoneMask              cBit6
#define cThaFpgaCurrentStatusDDR3CalibDoneShift             6

#define cThaFpgaCurrentStatusCKXgmii156LockedMask           cBit5
#define cThaFpgaCurrentStatusCKXgmii156LockedShift          5

#define cThaFpgaCurrentStatusCKQdrNo2FBLockedMask           cBit4
#define cThaFpgaCurrentStatusCKQdrNo2FBLockedShift          4

#define cThaFpgaCurrentStatusCKQdrNo1FBLockedMask           cBit3
#define cThaFpgaCurrentStatusCKQdrNo1FBLockedShift          3

#define cThaFpgaCurrentStatusCKCore2LockedMask              cBit2
#define cThaFpgaCurrentStatusCKCore2LockedShift             2

#define cThaFpgaCurrentStatusCKCore1LockedMask              cBit1
#define cThaFpgaCurrentStatusCKCore1LockedShift             1

#define cThaFpgaCurrentStatusCK155LockedMask                cBit0
#define cThaFpgaCurrentStatusCK155LockedShift               0

/* QDR2 Testing Control */
#define cThaQdr2TestControl             0xf00040
#define cThaQDRTestEnableMask(qdrId)    (((qdrId) == 0) ? cBit0 : cBit1)
#define cThaQDRTestEnableShift(qdrId)   (((qdrId) == 0) ? 0 : 1)

/* QDR2 Testing Status */
#define cThaQdr2TestStatus                0xf0006f
#define cThaQDRTestDataStatusMask(qdrId)  (((qdrId) == 0) ? cBit15_0 : cBit31_16)
#define cThaQDRTestDataStatusShift(qdrId) (((qdrId) == 0) ? 0 : 16)

/* FPGA clock */
#define cThaFPGAXGMIIClock156_25MhzValueStatus           0xF00061
#define cThaFPGAOCNTxClock155_52MhzValueStatus           0xF00062
#define cThaFPGAOCNRxClock155_52MhzValueStatus           0xF00063
#define cThaFPGACore1Clock100MhzValueStatus              0xF00064
#define cThaFPGACore2Clock100MhzValueStatus              0xF00065
#define cThaFPGADDRUserClock100MhzValueStatus            0xF00066
#define cThaFPGAPCIEUserClock62_5MhzValueStatus          0xF00067
#define cThaFPGAQDRNo1FeedBackClock155_52MhzValueStatus  0xF00068
#define cThaFPGAQDRNo2FeedBackClock155_52MhzValueStatus  0xF00069

/* DDR margin check */
#define cDdrMarginCheckControl         0xF00013
#define cDdrMarginCheckingDoneMask     cBit0
#define cDdrMarginCheckingDoneShift    0
#define cDdrMarginCheckingEnMask       cBit3
#define cDdrMarginCheckingEnShift      3
#define cDdrMarginCheckingTriggerMask  cBit4
#define cDdrMarginCheckingTriggerShift 4
#define cDdrMarginCheckingOpMask       cBit5
#define cDdrMarginCheckingOpShift      5
#define cDdrMarginCheckingResetMask    cBit31
#define cDdrMarginCheckingResetShift   31

#define cDdrReadMargin                 0xF00014
#define cDdrWriteMargin                0xF00015
#define cDdrMarginLeftMask             cBit31_16
#define cDdrMarginLeftShift            16
#define cDdrMarginRightMask            cBit15_0
#define cDdrMarginRightShift           0

/* QDR margin check */
#define cQdrMarginDetectControl        0xF00017
#define cQdrMarginDetectTriggerMask    cBit8
#define cQdrMarginDetectTriggerShift   8
#define cQdrMarginDetectQdrIdMask      cBit5
#define cQdrMarginDetectQdrIdShift     5
#define cQdrMarginDetectOperationMask  cBit4
#define cQdrMarginDetectOperationShift 4
#define cQdrMarginDetectDoneMask       cBit0
#define cQdrMarginDetectDoneShift      0

#define cQdrLeftMargin                 0xF00018
#define cQdrRightMargin                0xF00019

#define cQdrPhaseShiftControl          0xF0001B
#define cQdrIncrOrDecrEnMask           cBit8
#define cQdrIncrOrDecrEnShift          8
#define cQdrIncrOrDecrModeMask         cBit9
#define cQdrIncrOrDecrModeShift        9
#define cQdrManualShiftEnMask          cBit0
#define cQdrManualShiftEnShift         0
#define cQdrManualShiftTriggerMask     cBit4
#define cQdrManualShiftTriggerShift    4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _THA60150011DEVICEREG_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60150011Device.c
 *
 * Created Date: Jun 18, 2014
 *
 * Description : Product 60030041
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSemController.h"
#include "../../../../util/coder/AtCoderUtil.h"
#include "../../../../generic/man/AtIpCoreInternal.h"
#include "../../../default/physical/ThaSemControllerInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/eth/ThaModuleEth.h"
#include "../physical/Tha60150011SdhLineSerdesController.h"
#include "../pdh/Tha60150011ModulePdh.h"
#include "Tha60150011DeviceInternal.h"
#include "Tha60150011DeviceReg.h"
#include "Tha60150011Device.h"
#include "Tha60150011DeviceAsyncInit.h"

/*--------------------------- Define -----------------------------------------*/
#define cQdrMarginReadCheckingMode 0
#define cQdrMarginWriteCheckingMode 1

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60150011Device)self)

#define mSuccessAssert(calling)                                                \
    do                                                                         \
        {                                                                      \
        eAtRet ret_ = calling;                                                 \
        if (ret_ != cAtOk)                                                     \
            return ret_;                                                       \
        }while(0)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60150011DeviceMethods m_methods;

/* Override */
static tAtObjectMethods  m_AtObjectOverride;
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static const tAtObjectMethods  *m_AtObjectMethods   = NULL;
static const tAtDeviceMethods  *m_AtDeviceMethods   = NULL;
static const tThaDeviceMethods *m_ThaDeviceMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/
static eAtRet QdrEyeCenter(AtDevice self, uint8 qdrId, uint8 checkingMode);

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    /* Public modules */
    if (moduleId  == cAtModuleSdh)    return (AtModule)Tha60150011ModuleSdhNew(self);
    if (moduleId  == cAtModulePdh)    return (AtModule)Tha60150011ModulePdhNew(self);
    if (moduleId  == cAtModuleEth)    return (AtModule)Tha60150011ModuleEthNew(self);
    if (moduleId  == cAtModulePktAnalyzer) return (AtModule)Tha60150011ModulePktAnalyzerNew(self);
    if (moduleId  == cAtModulePw)     return (AtModule)Tha60150011ModulePwNew(self);
    if (moduleId  == cAtModuleRam)    return (AtModule)Tha60150011ModuleRamNew(self);

    /* Private modules */
    if (phyModule == cThaModulePoh)   return Tha60150011ModulePohNew(self);
    if (phyModule == cThaModuleOcn)   return Tha60150011ModuleOcnNew(self);
    if (phyModule == cThaModuleCdr)   return Tha60150011ModuleCdrNew(self);
    if (phyModule == cThaModuleMap)   return Tha60150011ModuleMapNew(self);
    if (phyModule == cThaModuleDemap) return Tha60150011ModuleDemapNew(self);
    if (phyModule == cThaModuleCla)   return Tha60150011ModuleClaNew(self);
    if (phyModule == cThaModulePda)   return Tha60150011ModulePdaNew(self);
    if (phyModule == cThaModulePwe)   return Tha60150011ModulePweNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePw,
                                                 cAtModuleEth,
                                                 cAtModuleRam,
                                                 cAtModuleSdh,
                                                 cAtModulePktAnalyzer};

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 moduleValue = moduleId;

    switch (moduleValue)
        {
        case cAtModulePdh:         return cAtTrue;
        case cAtModulePw:          return cAtTrue;
        case cAtModuleEth:         return cAtTrue;
        case cAtModuleRam:         return cAtTrue;
        case cAtModuleSdh:         return cAtTrue;
        case cAtModulePktAnalyzer: return cAtTrue;
        case cAtModuleBer:         return cAtFalse;
        case cAtModuleClock:       return cAtFalse;
        case cAtModulePrbs:        return cAtFalse;

        default:
            return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
        }
    }

static uint32 StartVersionSupportNewStartup(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x14, 0x11, 0x07, 0x26);
    }

static eBool CurrentAlarmIsGood(AtDevice self, uint32 bitMask)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal = AtHalRead(hal, cThaFpgaCurrentStatus);
    return (regVal & bitMask) ? cAtTrue : cAtFalse;
    }

static eBool DdrCalibCurrentStatusIsGood(AtDevice self)
    {
    return CurrentAlarmIsGood(self, cThaFpgaCurrentStatusDDR3CalibDoneMask);
    }

static eBool DdrCalibStickyIsGood(AtDevice self)
    {
    return Tha60150011StickyIsGood(self, cThaFpgaStickyDDR3CalibFailMask);
    }

static eBool DdrCalibIsGood(AtDevice self)
    {
    if (!DdrCalibStickyIsGood(self))
        return cAtFalse;

    if (!DdrCalibCurrentStatusIsGood(self))
        return cAtFalse;

    return cAtTrue;
    }

static eBool PllIsLocked(AtDevice self)
    {
    /* Sticky */
    if (!Tha60150011StickyIsGood(self, cThaFpgaStickyCKXgmii156NotLockedMask))
        return cAtFalse;

    if (!Tha60150011StickyIsGood(self, cThaFpgaStickyCKQdrNo2FBNotLockedMask))
        return cAtFalse;

    if (!Tha60150011StickyIsGood(self, cThaFpgaStickyCKQdrNo1FBNotLockedMask))
        return cAtFalse;

    if (!Tha60150011StickyIsGood(self, cThaFpgaStickyCKCore2NotLockedMask))
        return cAtFalse;

    if (!Tha60150011StickyIsGood(self, cThaFpgaStickyCKCore1NotLockedMask))
        return cAtFalse;

    if (!Tha60150011StickyIsGood(self, cThaFpgaStickyCK155NotLockedMask))
        return cAtFalse;

    /* Alarm */
    if (!CurrentAlarmIsGood(self, cThaFpgaCurrentStatusCKXgmii156LockedMask))
        return cAtFalse;

    if (!CurrentAlarmIsGood(self, cThaFpgaCurrentStatusCKQdrNo2FBLockedMask))
        return cAtFalse;

    if (!CurrentAlarmIsGood(self, cThaFpgaCurrentStatusCKQdrNo1FBLockedMask))
        return cAtFalse;

    if (!CurrentAlarmIsGood(self, cThaFpgaCurrentStatusCKCore2LockedMask))
        return cAtFalse;

    if (!CurrentAlarmIsGood(self, cThaFpgaCurrentStatusCKCore1LockedMask))
        return cAtFalse;

    if (!CurrentAlarmIsGood(self, cThaFpgaCurrentStatusCK155LockedMask))
        return cAtFalse;

    return cAtTrue;
    }

static eBool QdrsAreGood(AtDevice self)
    {
    AtModuleRam ramModule = (AtModuleRam)AtDeviceModuleGet(self, cAtModuleRam);
    uint8 numQdrs = AtModuleRamNumQdrGet(ramModule);
    uint8 qdr_i;

    for (qdr_i = 0; qdr_i < numQdrs; qdr_i++)
        {
        AtRam qdr = AtModuleRamQdrGet(ramModule, qdr_i);
        if (!AtRamIsGood(qdr))
            return cAtFalse;
        }

    return cAtTrue;
    }

static eBool OcnClockIsGood(AtDevice self)
    {
    if (!Tha60150011StickyIsGood(self, cThaFpgaStickyCKOcnRef155NotLockedMask))
        return cAtFalse;

    if (!CurrentAlarmIsGood(self, cThaFpgaCurrentStatusCKOcnRef155LockedMask))
        return cAtFalse;

    return cAtTrue;
    }

static eBool EthIntfClockIsGood(AtDevice self)
    {
    if (!Tha60150011StickyIsGood(self, cThaFpgaStickyCKXgmii156NotLockedMask))
        return cAtFalse;

    if (!CurrentAlarmIsGood(self, cThaFpgaCurrentStatusCKXgmii156LockedMask))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet HalTest(AtDevice self)
    {
    uint16 i;
    const uint16 cNumberOfHalTest = 1000;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);

    for (i = 0; i < cNumberOfHalTest; i++)
        {
        if (AtHalDiagMemTest(hal) == cAtFalse)
            return cAtErrorHalFail;
        }

    /* Do not need to expect product code for these platforms */
    if (ThaDeviceIsEp((ThaDevice)self) ||
        AtDeviceIsSimulated(self))
        return cAtOk;

    for (i = 0; i < cNumberOfHalTest; i++)
        {
        if (AtHalRead(hal, 0) != ThaDeviceExpectedProductCode((ThaDevice)self))
            return cAtErrorHalFail;
        }

    return cAtOk;
    }

static eBool NewStartupSequenceIsSupported(AtDevice self)
    {
    if (AtDeviceVersionNumber(self) >= StartVersionSupportNewStartup(self))
        return cAtTrue;

    return cAtFalse;
    }

static void M13Init(AtDevice self)
    {
    ThaModulePdh pdhModule = (ThaModulePdh)AtDeviceModuleGet(self, cAtModulePdh);
    if (pdhModule)
        {
        ThaModulePdhM13Init(pdhModule);
        ThaPdhStsMapDemapInit(pdhModule);
        }
    }

static eBool HasQdr(AtDevice self)
	{
    AtModuleRam ramModule;
	if (ThaDeviceIsEp((ThaDevice)self))
		return cAtFalse;

	ramModule = (AtModuleRam)AtDeviceModuleGet(self, cAtModuleRam);
	return (AtModuleRamNumQdrGet(ramModule) > 0) ? cAtTrue : cAtFalse;
	}

static eAtRet QdrCheckingTrigger(AtDevice self)
    {
    tAtOsalCurTime curTime, startTime;
    static const uint32 cMarginCheckTimeoutInMs = 1000;
    AtOsal osal = AtSharedDriverOsalGet();

    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal = AtHalRead(hal, cQdrMarginDetectControl);
    uint32 elapse = 0;
    eBool checkingSuccess = cAtFalse;

    /* Trigger */
    mRegFieldSet(regVal, cQdrMarginDetectTrigger, 1);
    AtHalWrite(hal, cQdrMarginDetectControl, regVal);

    /* Wait until checking done */
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapse < cMarginCheckTimeoutInMs)
        {
        regVal = AtHalRead(hal, cQdrMarginDetectControl);
        if ((regVal & cQdrMarginDetectDoneMask))
            {
            checkingSuccess = cAtTrue;
            if (mThis(self)->maxQdrMarginDetectTime < elapse)
                mThis(self)->maxQdrMarginDetectTime = elapse;

            break;
            }

        /* Calculate elapse time and retry */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    /* Clear trigger bit */
    mRegFieldSet(regVal, cQdrMarginDetectTrigger, 0);
    AtHalWrite(hal, cQdrMarginDetectControl, regVal);

    return (checkingSuccess) ? cAtOk : cAtError;
    }

static eAtRet QdrTapsDetect(AtDevice self, uint32 *leftTaps, uint32 *rightTaps)
    {
    AtHal hal;

    if (QdrCheckingTrigger(self) != cAtOk)
        return cAtError;

    hal = AtDeviceIpCoreHalGet(self, 0);
    *leftTaps = AtHalRead(hal, cQdrLeftMargin);
    *rightTaps = AtHalRead(hal, cQdrRightMargin);

    if ((*leftTaps == 0) || (*rightTaps == 0))
        return cAtError;

    return cAtOk;
    }

static eAtRet QdrEyeReadCenter(AtDevice self, uint8 qdrId)
    {
    eAtRet ret = QdrEyeCenter(self, qdrId, cQdrMarginReadCheckingMode);
    ret |= QdrTapsDetect(self, &(mThis(self)->minReadLeftTaps[qdrId]), &(mThis(self)->minReadRightTaps[qdrId]));

    return ret;
    }

static eAtRet QdrEyeWriteCenter(AtDevice self, uint8 qdrId)
    {
    eAtRet ret = QdrEyeCenter(self, qdrId, cQdrMarginWriteCheckingMode);
    ret |= QdrTapsDetect(self, &(mThis(self)->minWriteLeftTaps[qdrId]), &(mThis(self)->minWriteRightTaps[qdrId]));

    return ret;
    }

static eAtRet QdrEyeReadWriteCenter(AtDevice self, uint8 qdrId)
    {
    eAtRet ret;

    ret = QdrEyeReadCenter(self, qdrId);
    ret |= QdrEyeWriteCenter(self, qdrId);

    mThis(self)->qdrEyeIsCentered[qdrId] = cAtTrue;
    return ret;
    }

static void AllQdrsEyeCentering(AtDevice self)
    {
    if (AtDeviceIsSimulated(self))
        return;

    /* Always center here because soft reset will make eyes back to default */
    QdrEyeReadWriteCenter(self, 0);
    QdrEyeReadWriteCenter(self, 1);
    }

static eAtRet SdhSerdesReset(AtDevice self)
    {
    /* This product has only one line */
    AtSdhLine line;
    AtSerdesController serdesController;
    AtModuleSdh  moduleSdh = (AtModuleSdh)AtDeviceModuleGet(self, cAtModuleSdh);

    if (AtModuleSdhMaxLinesGet(moduleSdh) == 0)
        return cAtOk;

    /* Only reset SERDES when it is powerup, otherwise, timeout error code will be returned */
    line = AtModuleSdhLineGet(moduleSdh, 0);
    serdesController = AtSdhLineSerdesController(line);
    if (AtSerdesControllerIsControllable(serdesController) && (!AtSerdesControllerPowerIsDown(serdesController)))
        return AtSerdesControllerReset(serdesController);

    return cAtOk;
    }

static eAtRet ClearSemAlarm(AtSemController self)
    {
    uint32 alarm = AtSemControllerAlarmHistoryClear(self);
    alarm = AtSemControllerAlarmHistoryGet(self);

    /* Ignore heartbeat because it always raise */
    alarm = alarm & (uint32)(~cAtSemControllerAlarmHeartbeat);
    if (alarm != cAtSemControllerAlarmNone)
        return cAtError;

    return cAtOk;
    }

static eAtRet QdrDiagnostic(AtDevice self)
    {
    AtDriver driver = AtDeviceDriverGet(self);
    AllQdrsEyeCentering(self);
    if (!QdrsAreGood(self))
        {
        AtDriverLog(driver, cAtLogLevelWarning, "Qdr test fail\r\n");
        return cAtErrorDevFail;
        }

    return cAtOk;
    }

static eBool NeedCoreResetWhenDiagCheck(Tha60150011Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet CorePllCheck(AtDevice self)
    {
    AtDriver driver = AtDeviceDriverGet(self);

    if (mMethodsGet(mThis(self))->PllIsLocked(self))
        return cAtOk;

    AtDriverLog(driver, cAtLogLevelWarning, "PLL is not locked\r\n");
    return cAtErrorDevicePllNotLocked;
    }

static eAtRet RamCheck(AtDevice self)
    {
    eAtRet ret = cAtOk;
    AtDriver driver = AtDeviceDriverGet(self);

    if (HasQdr(self))
        {
        ret = mMethodsGet(mThis(self))->QdrDiagnostic(self);
        if (ret != cAtOk)
            return ret;
        }

    if (!mMethodsGet(mThis(self))->DdrCalibIsGood(self))
        {
        AtDriverLog(driver, cAtLogLevelWarning, "Ddr calib fail\r\n");
        return cAtErrorDdrCalibFail;
        }

    return cAtOk;
    }

static eAtRet DiagCoreReset(AtDevice self)
    {
    eAtRet ret;
    AtDriver driver = AtDeviceDriverGet(self);

    if (!mMethodsGet(mThis(self))->NeedCoreResetWhenDiagCheck(mThis(self)))
        return cAtOk;

    ret = AtDeviceAllCoresReset(self);
    if (ret != cAtOk)
        {
        AtDriverLog(driver, cAtLogLevelWarning, "All cores reset fail\r\n");
        return ret;
        }

    return cAtOk;
    }

static eAtRet DiagCoreAsyncReset(AtDevice self)
    {
    eAtRet ret;
    AtDriver driver = AtDeviceDriverGet(self);

    if (!mMethodsGet(mThis(self))->NeedCoreResetWhenDiagCheck(mThis(self)))
        return cAtOk;

    ret = AtDeviceAllCoresAsyncReset(self);
    if (ret != cAtOk && !AtDeviceAsyncRetValIsInState(ret))
        AtDriverLog(driver, cAtLogLevelWarning, "All cores async reset fail\r\n");

    return ret;
    }

static eAtRet ClockCheck(AtDevice self)
    {
    AtDriver driver = AtDeviceDriverGet(self);

    if (!mMethodsGet(mThis(self))->OcnClockIsGood(self))
        {
        AtDriverLog(driver, cAtLogLevelWarning, "OCN clock fail\r\n");
        return cAtErrorClockUnstable;
        }

    if (!mMethodsGet(mThis(self))->EthIntfClockIsGood(self))
        {
        AtDriverLog(driver, cAtLogLevelWarning, "Ethernet interface clock fail\r\n");
        return cAtErrorXfiPcsNotLocked;
        }

    return cAtOk;
    }

static eAtRet HalCheck(AtDevice self)
    {
    eAtRet ret = mMethodsGet(mThis(self))->HalTest(self);

    if (ret != cAtOk)
        {
        AtDriver driver = AtDeviceDriverGet(self);
        AtDriverLog(driver, cAtLogLevelWarning, "HAL test fail\r\n");
        return ret;
        }

    return cAtOk;
    }

static eAtRet ResetStatusCheck(Tha60150011Device self)
    {
    AtDevice device = (AtDevice)self;

    mSuccessAssert(CorePllCheck(device));
    mSuccessAssert(RamCheck(device));
    mSuccessAssert(ClockCheck(device));

    return cAtOk;
    }

static eAtRet AsyncResetStatusCheck(Tha60150011Device self)
    {
    return ResetStatusCheck(self);
    }

static eAtRet HwReset(AtDevice self)
    {
    eAtRet ret = m_AtDeviceMethods->HwReset(self);
    if (ret != cAtOk)
        return ret;

    return mMethodsGet(mThis(self))->ResetStatusCheck(mThis(self));
    }

static eAtRet AsyncHwReset(AtDevice self)
    {
    eAtRet ret;

    if (!mThis(self)->didReset)
        {
        ret = m_AtDeviceMethods->AsyncHwReset(self);
        if (ret != cAtOk)
            return ret;

        mThis(self)->didReset = cAtTrue;
        }

    ret = mMethodsGet(mThis(self))->AsyncResetStatusCheck(mThis(self));
    if (!AtDeviceAsyncRetValIsInState(ret))
        mThis(self)->didReset = cAtFalse;

    return ret;
    }

static AtSemController SemControllerObjectCreate(AtDevice self, uint32 semId)
    {
    return ThaSemControllerNew(self, semId);
    }

static eBool SemControllerIsSupported(AtDevice self)
    {
    if (ThaDeviceIsEp((ThaDevice)self))
        return cAtFalse;

    if (AtDeviceAllFeaturesAvailableInSimulation(self))
        return cAtTrue;

    if (AtDeviceVersionNumber(self) >= Tha60150011StartVersionSupportSemValidation(self))
        return cAtTrue;

    return cAtFalse;
    }

static uint8 NumSemControllersGet(AtDevice self)
    {
    AtUnused(self);
    return 1;
    }

static eAtRet DeviceDiagnostic(AtDevice self)
    {
    mSuccessAssert(HalCheck(self));
    mSuccessAssert(CorePllCheck(self));
    mSuccessAssert(DiagCoreReset(self));
    mSuccessAssert(RamCheck(self));
    mSuccessAssert(ClockCheck(self));

    return cAtOk;
    }

static eBool ShouldCheckDiagnostic(AtDevice self)
    {
    if (AtDeviceInAccessible(self))
        return cAtFalse;
    return AtDeviceDiagnosticCheckIsEnabled(self);
    }

static eBool  ShouldSemCheck(Tha60150011Device self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet SemCheck(AtDevice self)
    {
    eAtRet ret = cAtOk;
    AtSemController semController;
    AtDriver driver = AtDriverSharedDriverGet();

    if (!mMethodsGet(mThis(self))->ShouldSemCheck(mThis(self)))
        return cAtOk;

    /* SEM controller is lazy created, so should call like this to create it
     * on standby driver */
    semController = AtDeviceSemControllerGet(self);
    if (semController == NULL)
        return cAtOk;

    /* Cannot check SEM on standby driver as write operation is disabled */
    if (AtDeviceInAccessible(self))
        return cAtOk;

    ret = mMethodsGet(mThis(self))->SemClockProvide(mThis(self));
    if (ret != cAtOk)
        return ret;

    ret = AtSemControllerMoveObservationModeBeforeActiveChecking(semController);
    if (ret != cAtOk)
        return ret;

    /* SEM function must be active */
    if (!AtSemControllerIsActive(semController))
        {
        AtDriverLog(driver, cAtLogLevelWarning, "SEM IP is not active\r\n");
        return cAtErrorNotActivate;
        }

    ret = ClearSemAlarm(semController);
    if (ret != cAtOk)
        {
        AtDriverLog(driver, cAtLogLevelWarning, "Can not clear SEM alarm\r\n");
        return ret;
        }

    return cAtOk;
    }

static eBool Activated(AtDevice self)
    {
    return AtIpCoreIsActivated(AtDeviceIpCoreGet(self, 0));
    }

static eAtRet Init(AtDevice self)
    {
    AtModuleEth ethModule;
    eAtRet ret;
    ThaDevice device;
    AtOsal osal;
    AtDriver driver;
    eBool simulated;

    /* IMPORTANT: device activating check should be done before diagnostic, as
     * the diagnostic operation can reset the device and make device active
     * become 0, so optimized logic may think device has just been programmed
     * and do not clean up at the first time. */
    eBool isActivated = Activated(self);

    if (!Tha60150011NewStartupSequenceIsSupported(self))
        return m_AtDeviceMethods->Init(self);

    ret = cAtOk;
    device = (ThaDevice)self;
    osal = AtSharedDriverOsalGet();
    driver = AtDeviceDriverGet(self);

    device->resetIsInProgress = cAtTrue;

    /* As hardware recommend, Ethernet ports should be de-activated before
     * initializing device. Device initialization process will activate these
     * ports */
    ethModule = (AtModuleEth)AtDeviceModuleGet(self, cAtModuleEth);
    ThaModuleEthAllPortMacsActivate(ethModule, cAtFalse);

    /* Wait 500ms for FPGA stable */
    simulated = AtDeviceIsSimulated(self);
    if (AtDeviceAccessible(self) && (!simulated))
        mMethodsGet(osal)->USleep(osal, 500000);

    if (ShouldCheckDiagnostic(self))
        {
        ret = DeviceDiagnostic(self);
        if (ret != cAtOk)
            return ret;
        }

    /* Check SSKey, give the key engine a moment so that it can run and
     * update status */
    if (mMethodsGet(self)->NeedCheckSSKey(self))
        {
        mMethodsGet(osal)->Sleep(osal, 3);
        ret |= self->methods->SSKeyCheck(self);
        }

    if (ret != cAtOk)
        {
        AtDriverLog(driver, cAtLogLevelWarning, "Check SS key fail\r\n");
        return ret;
        }

    /* Just try to re-initialize all modules to stop all traffics to hardware
     * before starting initializing sequence. After soft reset, modules can be
     * in unknown state. It's time to give all of modules a fresh state */
    if (isActivated)
        self->methods->AllModulesInit(self);

    M13Init(self);

    /* Flushing all registers. */
    Tha60150011DeviceHwFlush(device);

    ret |= AtDeviceAllModulesSetup(self);
    if (ret != cAtOk)
        {
        AtDriverLog(driver, cAtLogLevelWarning, "All modules setup fail\r\n");
        return ret;
        }

    ret |= self->methods->AllModulesInit(self);
    if (ret != cAtOk)
        {
        AtDriverLog(driver, cAtLogLevelWarning, "All modules init fail\r\n");
        return ret;
        }

    /* It's time to activate all cores */
    ret |= AtDeviceAllCoresActivate(self, cAtTrue);
    if(ret != cAtOk)
        {
        AtDriverLog(driver, cAtLogLevelWarning, "All cores activate fail\r\n");
        return ret;
        }

    /* Reset serdes */
    ret |= mMethodsGet(mThis(self))->SdhSerdesReset(self);
    if (ret != cAtOk)
        {
        AtDriverLogWithFileLine(driver, cAtLogLevelCritical, AtSourceLocation, "SDH serdes reset fail\r\n");
        return ret;
        }

    SemCheck(self);

    if (AtDeviceAccessible(self) && (!simulated))
        mMethodsGet(osal)->USleep(osal, 10000);
    device->resetIsInProgress = cAtFalse;

    return ret;
    }

static void DebugPrint(AtDevice self, AtDebugger debugger, const char *debugString, uint32 bitMask, eBool (*StatusGet)(AtDevice self, uint32 bitMask))
    {
    eBool statusIsGood = StatusGet(self, bitMask);
    eAtSevLevel severity = statusIsGood ? cSevInfo : cSevCritical;

    if (debugger)
        {
        uint32 bufferSize;
        char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);
        AtDebuggerEntryAdd(debugger, AtDebugEntryStringFormatNew(severity, buffer, bufferSize, debugString, "%s", statusIsGood ? "OK" : "FAIL"));
        return;
        }
    AtPrintc(cSevNormal, "%-30s: ", debugString);
    AtPrintc(severity, "%s\r\n", statusIsGood ? "OK" : "FAIL");
    }

static void BasicPutString(AtDebugger debugger, const char *debugString, uint32 value)
    {
    if (debugger)
        {
        uint32 bufferSize;
        char *buffer = AtDebuggerCharBuffer(debugger, &bufferSize);
        AtSnprintf(buffer, bufferSize, "%d", value);
        AtDebuggerEntryAdd(debugger, AtDebugEntryNew(cSevNormal, debugString, buffer));
        return;
        }
    AtPrintc(cSevNormal, "%-30s: ", debugString);
    AtPrintc(cSevInfo, "%d\r\n", value);
    }

static void MarginDebugPrint(AtDevice self, AtDebugger debugger, const char *debugString, uint32 value)
    {
    AtUnused(self);
    BasicPutString(debugger, debugString, value);
    }

static uint8 NumCoreClocks(Tha60150011Device self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 CoreClockValueInHz(Tha60150011Device self, uint8 core)
    {
    AtUnused(self);
    AtUnused(core);
    return 100000000;
    }

static uint32 CoreClockValueRegister(Tha60150011Device self, uint8 core)
    {
    AtUnused(self);
    return (core == 0) ? cThaFPGACore1Clock100MhzValueStatus : cThaFPGACore2Clock100MhzValueStatus;
    }

static void CoreClockDisplay(Tha60150011Device self, AtDebugger debugger)
    {
    uint8 numCores = mMethodsGet(self)->NumCoreClocks(self);
    uint8 core_i;

    for (core_i = 0; core_i < numCores; core_i++)
        {
        char buffer[64];
        uint32 expectedClockValue = mMethodsGet(self)->CoreClockValueInHz(self, core_i);
        AtSnprintf(buffer, sizeof(buffer) - 1,
                   "Core %u Clock %uMhz Value Status ",
                   core_i + 1,
                   expectedClockValue / 1000000);
        ThaDeviceClockResultPrint((AtDevice)self, debugger, buffer, CoreClockValueRegister(self, core_i), expectedClockValue, 5);
        }
    }

static void DdrUserClockDisplay(AtDevice self, AtDebugger debugger)
    {
    ThaModuleRam ramModule = (ThaModuleRam)AtDeviceModuleGet(self, cAtModuleRam);
    ThaModuleRamDdrUserClockDisplay(ramModule, debugger);
    }

static void QdrUserClockDisplay(AtDevice self, AtDebugger debugger)
    {
    ThaModuleRam ramModule = (ThaModuleRam)AtDeviceModuleGet(self, cAtModuleRam);
    ThaModuleRamQdrUserClockDisplay(ramModule, debugger);
    }

static AtHal Hal(Tha60150011Device self)
    {
    return AtDeviceIpCoreHalGet((AtDevice)self, 0);
    }

static void StickyDisplay(Tha60150011Device self)
    {
    AtDevice device = (AtDevice)self;
    AtDebugger debugger = AtDeviceDebuggerGet((AtDevice)self);

    if (!debugger)
        {
        AtPrintc(cSevNormal, "\r\n");
        AtPrintc(cSevInfo, "FPGA sticky\r\n");
        AtPrintc(cSevInfo, "========================================================\r\n");
        AtPrintc(cSevNormal, "Raw value: 0x%x\r\n", AtHalRead(Hal(self), cThaFpgaSticky));
        }

    DebugPrint(device, debugger, "CK155", cThaFpgaStickyCK155NotLockedMask, Tha60150011StickyIsGood);
    DebugPrint(device, debugger, "CKCore1", cThaFpgaStickyCKCore1NotLockedMask, Tha60150011StickyIsGood);
    DebugPrint(device, debugger, "CKCore2", cThaFpgaStickyCKCore2NotLockedMask, Tha60150011StickyIsGood);
    DebugPrint(device, debugger, "CKQdrNo1FB", cThaFpgaStickyCKQdrNo1FBNotLockedMask, Tha60150011StickyIsGood);
    DebugPrint(device, debugger, "CKQdrNo2FB", cThaFpgaStickyCKQdrNo2FBNotLockedMask, Tha60150011StickyIsGood);
    DebugPrint(device, debugger, "CKXgmii156", cThaFpgaStickyCKXgmii156NotLockedMask, Tha60150011StickyIsGood);
    DebugPrint(device, debugger, "DDR3Calib", cThaFpgaStickyDDR3CalibFailMask, Tha60150011StickyIsGood);
    DebugPrint(device, debugger, "CKOcnRef155", cThaFpgaStickyCKOcnRef155NotLockedMask, Tha60150011StickyIsGood);
    DebugPrint(device, debugger, "OcnSerdesRxResetState", cThaFpgaStickyOcnSerdesRxResetStateMask, Tha60150011StickyIsGood);
    DebugPrint(device, debugger, "OcnSerdesTxResetState", cThaFpgaStickyOcnSerdesTxResetStateMask, Tha60150011StickyIsGood);
    DebugPrint(device, debugger, "QDRNo1Parity", cThaFpgaStickyQDRParityErrorMask(0), Tha60150011StickyIsGood);
    DebugPrint(device, debugger, "QDRNo2Parity", cThaFpgaStickyQDRParityErrorMask(1), Tha60150011StickyIsGood);
    }

static void StatusDisplay(Tha60150011Device self)
    {
    AtDevice device = (AtDevice)self;
    AtDebugger debugger = AtDeviceDebuggerGet((AtDevice)self);

    if (!debugger)
        {
        AtPrintc(cSevNormal, "\r\n");
        AtPrintc(cSevInfo, "FPGA status\r\n");
        AtPrintc(cSevInfo, "========================================================\r\n");
        AtPrintc(cSevNormal, "Raw value: 0x%x\r\n", AtHalRead(Hal(self), cThaFpgaCurrentStatus));
        }

    DebugPrint(device, debugger, "CK155", cThaFpgaCurrentStatusCK155LockedMask, CurrentAlarmIsGood);
    DebugPrint(device, debugger, "CKCore1", cThaFpgaCurrentStatusCKCore1LockedMask, CurrentAlarmIsGood);
    DebugPrint(device, debugger, "CKCore2", cThaFpgaCurrentStatusCKCore2LockedMask, CurrentAlarmIsGood);
    DebugPrint(device, debugger, "CKQdrNo1FB", cThaFpgaCurrentStatusCKQdrNo1FBLockedMask, CurrentAlarmIsGood);
    DebugPrint(device, debugger, "CKQdrNo2FB", cThaFpgaCurrentStatusCKQdrNo2FBLockedMask, CurrentAlarmIsGood);
    DebugPrint(device, debugger, "CKXgmii156", cThaFpgaCurrentStatusCKXgmii156LockedMask, CurrentAlarmIsGood);
    DebugPrint(device, debugger, "DDR3Calib", cThaFpgaCurrentStatusDDR3CalibDoneMask, CurrentAlarmIsGood);
    DebugPrint(device, debugger, "CKOcnRef155Locked", cThaFpgaCurrentStatusCKOcnRef155LockedMask, CurrentAlarmIsGood);
    DebugPrint(device, debugger, "OcnSerdesRxNormalState", cThaFpgaCurrentStatusOcnSerdesRxNormalStateMask, CurrentAlarmIsGood);
    DebugPrint(device, debugger, "OcnSerdesTxNormalState", cThaFpgaCurrentStatusOcnSerdesTxNormalStateMask, CurrentAlarmIsGood);
    DebugPrint(device, debugger, "XgmiiLinkUp", cThaFpgaCurrentStatusXgmiiLinkUpMask, CurrentAlarmIsGood);
    }

static void OcnClockStatusDisplay(Tha60150011Device self)
    {
    AtDevice device = (AtDevice)self;
    AtDebugger debugger = AtDeviceDebuggerGet((AtDevice)self);
    ThaDeviceClockResultPrint(device, debugger, "OCN Tx Clock 155.52Mhz Value Status", cThaFPGAOCNTxClock155_52MhzValueStatus, 155520000, 5);
    ThaDeviceClockResultPrint(device, debugger, "OCN Rx Clock 155.52Mhz Value Status", cThaFPGAOCNRxClock155_52MhzValueStatus, 155520000, 5);
    }

static uint32 XGMIIClock156_25MhzValueStatusRegister(Tha60150011Device self, uint8 clockId)
    {
    AtUnused(self);
    AtUnused(clockId);
    return cThaFPGAXGMIIClock156_25MhzValueStatus;
    }

static uint8 NumXGMIIClock156_25Mhz(Tha60150011Device self)
    {
    AtUnused(self);
    return 1;
    }

static const char *XGMIIClockName(Tha60150011Device self, uint8 clockId)
    {
    AtUnused(self);
    AtUnused(clockId);
    return "XGMII";
    }

static void XGMIIClockDislay(Tha60150011Device self, AtDebugger debugger)
    {
    AtDevice device = (AtDevice)self;
    uint8 numClocks = mMethodsGet(self)->NumXGMIIClock156_25Mhz(self);
    uint8 clock_i;

    for (clock_i = 0; clock_i < numClocks; clock_i++)
        {
        char buf[64];
        uint32 regAddr = mMethodsGet(self)->XGMIIClock156_25MhzValueStatusRegister(self, clock_i);
        AtSnprintf(buf, sizeof(buf) - 1, "%s Clock 156.25Mhz Value Status", mMethodsGet(self)->XGMIIClockName(self, clock_i));
        ThaDeviceClockResultPrint(device, debugger, buf, regAddr, 156250000, 30);
        }
    }

static uint32 ClockPcieSwingRangeInPpm(Tha60150011Device self)
    {
    AtUnused(self);
    return 30;
    }

static void ClockStatusDisplay(Tha60150011Device self)
    {
    AtDevice device = (AtDevice)self;
    AtDebugger debugger = AtDeviceDebuggerGet((AtDevice)self);
    uint32 ppmSwingRange = mMethodsGet(self)->ClockPcieSwingRangeInPpm(self);

    if (!debugger)
        {
        AtPrintc(cSevNormal, "\r\n");
        AtPrintc(cSevInfo, "FPGA clock status\r\n");
        AtPrintc(cSevInfo, "========================================================\r\n");
        }

    XGMIIClockDislay(self, debugger);

    mMethodsGet(self)->OcnClockStatusDisplay(self);

    CoreClockDisplay(mThis(self), debugger);
    DdrUserClockDisplay(device, debugger);
    ThaDeviceClockResultPrint(device, debugger, "PCIE User Clock 62.5Mhz Value Status", cThaFPGAPCIEUserClock62_5MhzValueStatus, 62500000, ppmSwingRange);
    QdrUserClockDisplay(device, debugger);
    }

static void RamMarginDisplay(Tha60150011Device self)
    {
    AtDevice device = (AtDevice)self;
    AtDebugger debugger = AtDeviceDebuggerGet((AtDevice)self);

    if (!debugger)
        {
        AtPrintc(cSevNormal, "\r\n");
        AtPrintc(cSevInfo, "QDR 1 margin information\r\n");
        AtPrintc(cSevInfo, "========================================================\r\n");
        }
    MarginDebugPrint(device, debugger, "QDR#1 Read left minimum taps", mThis(self)->minReadLeftTaps[0]);
    MarginDebugPrint(device, debugger, "QDR#1 Read right minimum taps", mThis(self)->minReadRightTaps[0]);
    MarginDebugPrint(device, debugger, "QDR#1 Write left minimum taps", mThis(self)->minWriteLeftTaps[0]);
    MarginDebugPrint(device, debugger, "QDR#1 Write right minimum taps", mThis(self)->minWriteRightTaps[0]);

    if (!debugger)
        {
        AtPrintc(cSevInfo, "QDR 2 margin information\r\n");
        AtPrintc(cSevInfo, "========================================================\r\n");
        }
    MarginDebugPrint(device, debugger, "QDR#2 Read left minimum taps", mThis(self)->minReadLeftTaps[1]);
    MarginDebugPrint(device, debugger, "QDR#2 Read right minimum taps", mThis(self)->minReadRightTaps[1]);
    MarginDebugPrint(device, debugger, "QDR#2 Write left minimum taps", mThis(self)->minWriteLeftTaps[1]);
    MarginDebugPrint(device, debugger, "QDR#2 Write right minimum taps", mThis(self)->minWriteRightTaps[1]);
    }

static eBool ShouldShowCommonDebugInfo(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void Debug(AtDevice self)
    {
    AtHal hal;
    AtDebugger debugger = AtDeviceDebuggerGet(self);

    if (!Tha60150011NewStartupSequenceIsSupported(self))
        {
        m_AtDeviceMethods->Debug(self);
        return;
        }

    if (mMethodsGet(mThis(self))->ShouldShowCommonDebugInfo(self))
        m_AtDeviceMethods->Debug(self);

    hal = Hal(mThis(self));

    mMethodsGet(mThis(self))->StickyDisplay(mThis(self));
    mMethodsGet(mThis(self))->StatusDisplay(mThis(self));
    mMethodsGet(mThis(self))->ClockStatusDisplay(mThis(self));
    mMethodsGet(mThis(self))->RamMarginDisplay(mThis(self));

    mNoneDebuggerPrint(cSevNormal, "\r\n");

    /* Clear sticky */
    AtHalWrite(hal, cThaFpgaSticky, 0xFFFFFFFF);
    }

static eBool WarmRestoreIsSupported(AtDevice self)
    {
    return Tha60150011NewStartupSequenceIsSupported(self);
    }

static uint8 NumPartsOfModule(ThaDevice self, uint32 moduleId)
    {
    uint32 moduleId_ = (uint32)moduleId; /* Just to make compiler be happy */

    AtUnused(self);

    /* These modules are divided into 2 parts, each part corresponding to 1 OCN slice */
    if ((moduleId_ == cThaModuleCdr)   ||
        (moduleId_ == cAtModulePdh)    ||
        (moduleId_ == cThaModuleDemap) ||
        (moduleId_ == cThaModuleMap))
        return 2;

    return 1;
    }

/* In this product, part Id is slice ID */
static uint32 ModulePartOffset(ThaDevice self, eAtModule moduleId, uint8 sliceId)
    {
    uint32 moduleId_ = (uint32)moduleId; /* Just to make compiler be happy */

    AtUnused(self);

    if (moduleId_ == cThaModuleCdr)
        return (0x040000UL * sliceId);

    if (moduleId_ == cAtModulePdh)
        return (0x100000UL * sliceId);

    if ((moduleId_ == cThaModuleDemap) ||
        (moduleId_ == cThaModuleMap))
        return (0x020000UL * sliceId);

    return 0x0;
    }

static eBool NeedCheckSSKey(AtDevice self)
    {
    if (ThaDeviceIsEp((ThaDevice)self) || AtDeviceIsSimulated(self))
    	return cAtFalse;
    	
    /* Only check with FPGA version B */
    if (Tha60150011DeviceFpgaIsVersionB(self))
        return cAtTrue;

    return cAtFalse;
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return Tha60150011SSKeyCheckerNew(self);
    }

static eAtRet AsyncInit(AtDevice self)
    {
    return Tha60150011DeviceAsyncInit(self);
    }

static eAtRet SemClockProvide(Tha60150011Device self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, Debug);
        mMethodOverride(m_AtDeviceOverride, WarmRestoreIsSupported);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, NeedCheckSSKey);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        mMethodOverride(m_AtDeviceOverride, HwReset);
        mMethodOverride(m_AtDeviceOverride, SemControllerObjectCreate);
        mMethodOverride(m_AtDeviceOverride, SemControllerIsSupported);
        mMethodOverride(m_AtDeviceOverride, NumSemControllersGet);
        mMethodOverride(m_AtDeviceOverride, AsyncInit);
        mMethodOverride(m_AtDeviceOverride, AsyncHwReset);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static uint8 MaxNumParts(ThaDevice self)
    {
    AtUnused(self);
    return 1;
    }

static ThaVersionReader VersionReaderCreate(ThaDevice self)
    {
    return Tha60150011VersionReaderNew((AtDevice)self);
    }

static uint32 StartVersionSupportJitterAttenuator(Tha60150011Device self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader((AtDevice)self), 0x15, 0x09, 0x17, 0x9b);
    }

static eBool SemUartIsSupported(Tha60150011Device self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void HwFlush(Tha60150011Device self)
    {
    AtUnused(self);
    /* Let subclass do */
    }

static void MethodsInit(AtDevice self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, NewStartupSequenceIsSupported);
        mMethodOverride(m_methods, HalTest);
        mMethodOverride(m_methods, OcnClockIsGood);
        mMethodOverride(m_methods, NumCoreClocks);
        mMethodOverride(m_methods, CoreClockValueInHz);
        mMethodOverride(m_methods, StickyDisplay);
        mMethodOverride(m_methods, StatusDisplay);
        mMethodOverride(m_methods, ClockStatusDisplay);
        mMethodOverride(m_methods, RamMarginDisplay);
        mMethodOverride(m_methods, OcnClockStatusDisplay);
        mMethodOverride(m_methods, XGMIIClock156_25MhzValueStatusRegister);
        mMethodOverride(m_methods, ShouldShowCommonDebugInfo);
        mMethodOverride(m_methods, DdrCalibIsGood);
        mMethodOverride(m_methods, NumXGMIIClock156_25Mhz);
        mMethodOverride(m_methods, XGMIIClockName);
        mMethodOverride(m_methods, QdrDiagnostic);
        mMethodOverride(m_methods, PllIsLocked);
        mMethodOverride(m_methods, EthIntfClockIsGood);
        mMethodOverride(m_methods, ClockPcieSwingRangeInPpm);
        mMethodOverride(m_methods, NeedCoreResetWhenDiagCheck);
        mMethodOverride(m_methods, StartVersionSupportJitterAttenuator);
        mMethodOverride(m_methods, SemUartIsSupported);
        mMethodOverride(m_methods, ResetStatusCheck);
        mMethodOverride(m_methods, AsyncResetStatusCheck);
        mMethodOverride(m_methods, SemClockProvide);
        mMethodOverride(m_methods, SdhSerdesReset);
        mMethodOverride(m_methods, ShouldSemCheck);
        mMethodOverride(m_methods, HwFlush);
        }

    mMethodsSet(mThis(self), &m_methods);
    }

static uint32 StartVersionSupportDdrMarginCheck(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x03, 0x18, 0x79);
    }

static eBool DdrMarginCheckingIsSupported(AtDevice self)
    {
    if ((ThaDeviceIsEp((ThaDevice)self)) || AtDeviceAllFeaturesAvailableInSimulation(self))
        return cAtFalse;

    if (AtDeviceVersionNumber(self) >= StartVersionSupportDdrMarginCheck(self))
        return cAtTrue;

    return cAtFalse;
    }

static void DdrMarginCheckingStop(AtDevice self)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);

    AtHalWrite(hal, cDdrMarginCheckControl, cBit31);
    AtHalWrite(hal, cDdrMarginCheckControl, 0);
    }

static void DdrCheckingModeSelect(AtDevice self, uint8 checkingMode)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal = 0;

    /* Select checking mode */
    mRegFieldSet(regVal, cDdrMarginCheckingEn, 1);
    mRegFieldSet(regVal, cDdrMarginCheckingOp, checkingMode);

    /* Clear trigger and sticky */
    mRegFieldSet(regVal, cDdrMarginCheckingTrigger, 0);
    mRegFieldSet(regVal, cDdrMarginCheckingDone, 1);

    AtHalWrite(hal, cDdrMarginCheckControl, regVal);
    }

static eAtRet DdrCheckingTrigger(AtDevice self)
    {
    tAtOsalCurTime curTime, startTime;
    static const uint32 cMarginCheckTimeoutInMs = 40000;
    static const uint32 cMarginCheckCpuRestTimeInSec = 1;
    AtOsal osal = AtSharedDriverOsalGet();

    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal = AtHalRead(hal, cDdrMarginCheckControl);
    uint32 elapse = 0;
    eBool checkingSuccess = cAtFalse;

    mRegFieldSet(regVal, cDdrMarginCheckingTrigger, 1);
    AtHalWrite(hal, cDdrMarginCheckControl, regVal);

    /* Wait until checking done */
    mMethodsGet(osal)->CurTimeGet(osal, &startTime);
    while (elapse < cMarginCheckTimeoutInMs)
        {
        regVal = AtHalRead(hal, cDdrMarginCheckControl);
        if ((regVal & cDdrMarginCheckingDoneMask))
            {
            checkingSuccess = cAtTrue;
            break;
            }

        /* Calculate elapse time and retry */
        mMethodsGet(osal)->CurTimeGet(osal, &curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);

        mMethodsGet(osal)->Sleep(osal, cMarginCheckCpuRestTimeInSec);
        }

    return (checkingSuccess) ? cAtOk : cAtError;
    }

static eAtRet DdrReadTapsDetect(AtDevice self, uint16 *leftTaps, uint16 *rightTaps)
    {
    static const uint8 cReadCheckingMode = 1;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal;

    DdrCheckingModeSelect(self, cReadCheckingMode);
    if (DdrCheckingTrigger(self) != cAtOk)
        return cAtError;

    regVal = AtHalRead(hal, cDdrReadMargin);
    if (regVal == 0)
        return cAtError;

    *leftTaps  = (uint16)mRegField(regVal, cDdrMarginLeft);
    *rightTaps = (uint16)mRegField(regVal, cDdrMarginRight);

    return cAtOk;
    }

static eAtRet DdrWriteTapsDetect(AtDevice self, uint16 *leftTaps, uint16 *rightTaps)
    {
    static const uint8 cWriteCheckingMode = 0;
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal;

    DdrCheckingModeSelect(self, cWriteCheckingMode);
    if (DdrCheckingTrigger(self) != cAtOk)
        return cAtError;

    regVal = AtHalRead(hal, cDdrWriteMargin);
    if (regVal == 0)
        return cAtError;

    *leftTaps  = (uint16)mRegField(regVal, cDdrMarginLeft);
    *rightTaps = (uint16)mRegField(regVal, cDdrMarginRight);

    return cAtOk;
    }

static uint32 DdrMarginCalculateFromTaps(uint16 tapCount)
    {
    static const uint32 cClockPeriodInPs = 2500;
    return (tapCount * cClockPeriodInPs) / 128;
    }

static tRamMarginParams* DdrMargin(AtDevice self)
    {
    return &(mThis(self)->ddrMargin);
    }

static eAtRet DdrReadMarginDetect(AtDevice self)
    {
    uint16 leftTaps = 0;
    uint16 rightTaps = 0;
    tRamMarginParams* margins = DdrMargin(self);

    if (DdrReadTapsDetect(self, &leftTaps, &rightTaps) != cAtOk)
        return cAtError;

    margins->readLeftMargin = DdrMarginCalculateFromTaps(leftTaps);
    margins->readRightMargin = DdrMarginCalculateFromTaps(rightTaps);

    return cAtOk;
    }

static eAtRet DdrWriteMarginDetect(AtDevice self)
    {
    uint16 leftTaps = 0;
    uint16 rightTaps = 0;
    tRamMarginParams* margins = DdrMargin(self);

    if (DdrWriteTapsDetect(self, &leftTaps, &rightTaps) != cAtOk)
        return cAtError;

    margins->writeLeftMargin = DdrMarginCalculateFromTaps(leftTaps);
    margins->writeRightMargin = DdrMarginCalculateFromTaps(rightTaps);

    return cAtOk;
    }

static uint32 StartVersionSupportQdrMarginCheck(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x03, 0x28, 0x81);
    }

static eBool QdrMarginCheckingIsSupported(AtDevice self)
    {
    if ((ThaDeviceIsEp((ThaDevice)self)) || AtDeviceAllFeaturesAvailableInSimulation(self))
        return cAtFalse;

    if (AtDeviceVersionNumber(self) >= StartVersionSupportQdrMarginCheck(self))
        return cAtTrue;

    return cAtFalse;
    }

static eBool QdrIdIsValid(uint8 qdrId)
    {
    return (qdrId <= 1) ? cAtTrue : cAtFalse;
    }

static uint32 QdrMarginCalculateFromTaps(uint32 tapCount)
    {
    static const uint32 cClockInputPeriodInPs = 6430; /* Corresponding to 155.52Mhz */
    static const uint32 cDIVCLK_DIVIDE = 1;
    static const uint32 cCLKFBOUT_MULT_F = 6;

    uint32 Tvco = (cDIVCLK_DIVIDE * cClockInputPeriodInPs) / cCLKFBOUT_MULT_F;
    return (tapCount * (Tvco / 56));
    }

static void QdrCheckingModeSelect(AtDevice self, uint8 qdrId, uint8 checkingMode)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal = 0;

    /* Select qdr and checking mode */
    mRegFieldSet(regVal, cQdrMarginDetectQdrId, qdrId);
    mRegFieldSet(regVal, cQdrMarginDetectOperation, checkingMode);

    /* Clear trigger and sticky */
    mRegFieldSet(regVal, cQdrMarginDetectTrigger, 0);
    mRegFieldSet(regVal, cQdrMarginDetectDone, 1);

    AtHalWrite(hal, cQdrMarginDetectControl, regVal);
    }

static tRamMarginParams* QdrMargin(AtDevice self, uint8 qdrId)
    {
    return &(mThis(self)->qdrMargin[qdrId]);
    }

static eAtRet QdrReadMarginDetect(AtDevice self, uint8 qdrId)
    {
    uint32 leftTaps = 0;
    uint32 rightTaps = 0;
    tRamMarginParams* margins = QdrMargin(self, qdrId);

    QdrCheckingModeSelect(self, qdrId, cQdrMarginReadCheckingMode);
    if (QdrTapsDetect(self, &leftTaps, &rightTaps) != cAtOk)
        return cAtError;

    leftTaps = mMin(leftTaps, mThis(self)->minReadLeftTaps[qdrId]);
    rightTaps = mMin(rightTaps, mThis(self)->minReadRightTaps[qdrId]);

    margins->readLeftMargin = QdrMarginCalculateFromTaps(leftTaps);
    margins->readRightMargin = QdrMarginCalculateFromTaps(rightTaps);

    return cAtOk;
    }

static eAtRet QdrWriteMarginDetect(AtDevice self, uint8 qdrId)
    {
    uint32 leftTaps = 0;
    uint32 rightTaps = 0;
    tRamMarginParams* margins = QdrMargin(self, qdrId);

    QdrCheckingModeSelect(self, qdrId, cQdrMarginWriteCheckingMode);
    if (QdrTapsDetect(self, &leftTaps, &rightTaps) != cAtOk)
        return cAtError;

    leftTaps = mMin(leftTaps, mThis(self)->minWriteLeftTaps[qdrId]);
    rightTaps = mMin(rightTaps, mThis(self)->minWriteRightTaps[qdrId]);

    margins->writeLeftMargin = QdrMarginCalculateFromTaps(leftTaps);
    margins->writeRightMargin = QdrMarginCalculateFromTaps(rightTaps);

    return cAtOk;
    }

static void PhaseShiftTrigger(AtDevice self, uint32 regVal)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);

    mRegFieldSet(regVal, cQdrManualShiftTrigger, 1);
    AtHalWrite(hal, cQdrPhaseShiftControl, regVal);

    /* Delay 12 clock for HW to finish, as HW recommended, read action takes > 16 clock
     * so just read to have a delay */
    regVal = AtHalRead(hal, cQdrPhaseShiftControl);

    /* Clear trigger bit for next time */
    mRegFieldSet(regVal, cQdrManualShiftTrigger, 0);
    AtHalWrite(hal, cQdrPhaseShiftControl, regVal);
    }

static void QdrPhaseShift(AtDevice self, uint32 phaseOffset, eBool isIncrease)
    {
    AtHal hal = AtDeviceIpCoreHalGet(self, 0);
    uint32 regVal = 0;
    uint32 phase_i;

    /* Select mode */
    mRegFieldSet(regVal, cQdrIncrOrDecrEn, 1);
    mRegFieldSet(regVal, cQdrIncrOrDecrMode, (isIncrease) ? 1 : 0);
    mRegFieldSet(regVal, cQdrManualShiftEn, 1);
    AtHalWrite(hal, cQdrPhaseShiftControl, regVal);

    for (phase_i = 0; phase_i < phaseOffset; phase_i++)
        PhaseShiftTrigger(self, regVal);

    /* Need to clear control reg */
    AtHalWrite(hal, cQdrPhaseShiftControl, 0x0);
    }

static eAtRet QdrEyeCenter(AtDevice self, uint8 qdrId, uint8 checkingMode)
    {
    uint32 leftTaps, minLeftTaps = cBit31_0;
    uint32 rightTaps, minRightTaps = cBit31_0;
    uint32 phasesOffset;
    eBool isIncrease;
    const uint8 cMaxNumTimeDetectMinTaps = 10;
    uint8 detectTime;

    QdrCheckingModeSelect(self, qdrId, checkingMode);
    for (detectTime = 0; detectTime < cMaxNumTimeDetectMinTaps; detectTime++)
        {
        if (QdrTapsDetect(self, &leftTaps, &rightTaps) != cAtOk)
            return cAtError;

        if (minLeftTaps > leftTaps)
            minLeftTaps = leftTaps;

        if (minRightTaps > rightTaps)
            minRightTaps = rightTaps;
        }

    if (minLeftTaps > minRightTaps)
        {
        phasesOffset = (minLeftTaps - minRightTaps) / 2;
        isIncrease = cAtFalse;
        }
    else
        {
        phasesOffset = (minRightTaps - minLeftTaps) / 2;
        isIncrease = cAtTrue;
        }

    QdrPhaseShift(self, phasesOffset, isIncrease);
    return cAtOk;
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeNone(ddrMargin);
    mEncodeNone(qdrMargin);
    mEncodeNone(maxQdrMarginDetectTime);
    mEncodeNone(qdrEyeIsCentered);
    mEncodeNone(minReadLeftTaps);
    mEncodeNone(minReadRightTaps);
    mEncodeNone(minWriteLeftTaps);
    mEncodeNone(minWriteRightTaps);
    mEncodeNone(asyncInitState);
    mEncodeNone(diagAsyncState);
    mEncodeNone(prevStateTime);
    mEncodeNone(diagAsyncState);
    mEncodeNone(didReset);
    mEncodeNone(deviceIsActivated);
    mEncodeNone(didGetDeviceIsActived);
    }

static void OverrideAtObject(AtDevice self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaDevice(AtDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, m_ThaDeviceMethods, sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, MaxNumParts);
        mMethodOverride(m_ThaDeviceOverride, NumPartsOfModule);
        mMethodOverride(m_ThaDeviceOverride, ModulePartOffset);
        mMethodOverride(m_ThaDeviceOverride, VersionReaderCreate);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    OverrideAtDevice(self);
    OverrideThaDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011Device);
    }

AtDevice Tha60150011DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60030081DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60150011DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011DeviceObjectInit(newDevice, driver, productCode);
    }

uint32 Tha60150011DeviceStartVersionSupportRtpFrequencyLargerThan16Bits(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x03, 0x10, 0x77);
    }

eBool Tha60150011DeviceDdrCalibIsGood(AtDevice self)
    {
    if (self == NULL)
        return cAtFalse;
    return mMethodsGet(mThis(self))->DdrCalibIsGood(self);
    }

eBool Tha60150011DeviceFpgaIsVersionB(AtDevice self)
    {
    /* FPGA version B will have last octet >= 0x60 */
    uint32 version = AtDeviceVersionNumber(self);
    if ((version & cBit7_0) >= 0x60)
        return cAtTrue;

    return cAtFalse;
    }

/*
 * Sequence of this process should be strictly followed:
 * - Step 1: Detect write (also enable checking)
 * - Step 2: Detect read
 * - Step 3: Stop detecting
 *
 * Other words, detect write and read must come as a couple
 */
eAtRet Tha60150011DeviceDdrMarginDetect(AtDevice self)
    {
    eAtRet ret;

    if (!DdrMarginCheckingIsSupported(self))
        return cAtErrorModeNotSupport;

    ret = DdrWriteMarginDetect(self);
    ret |= DdrReadMarginDetect(self);

    DdrMarginCheckingStop(self);
    return ret;
    }

uint32 Tha60150011DeviceDdrReadLeftMargin(AtDevice self)
    {
    tRamMarginParams* margins = DdrMargin(self);

    if (margins->readLeftMargin != 0)
        return margins->readLeftMargin;

    if (Tha60150011DeviceDdrMarginDetect(self) != cAtOk)
        return 0;

    return margins->readLeftMargin;
    }

uint32 Tha60150011DeviceDdrReadRightMargin(AtDevice self)
    {
    tRamMarginParams* margins = DdrMargin(self);
    if (margins->readRightMargin != 0)
        return margins->readRightMargin;

    if (Tha60150011DeviceDdrMarginDetect(self) != cAtOk)
        return 0;

    return margins->readRightMargin;
    }

uint32 Tha60150011DeviceDdrWriteLeftMargin(AtDevice self)
    {
    tRamMarginParams* margins = DdrMargin(self);
    if (margins->writeLeftMargin != 0)
        return margins->writeLeftMargin;

    if (Tha60150011DeviceDdrMarginDetect(self) != cAtOk)
        return 0;

    return margins->writeLeftMargin;
    }

uint32 Tha60150011DeviceDdrWriteRightMargin(AtDevice self)
    {
    tRamMarginParams* margins = DdrMargin(self);
    if (margins->writeRightMargin != 0)
        return margins->writeRightMargin;

    if (Tha60150011DeviceDdrMarginDetect(self) != cAtOk)
        return 0;

    return margins->writeRightMargin;
    }

eAtRet Tha60150011DeviceQdrTest(AtDevice self, uint8 qdrId)
    {
    AtModuleRam ramModule;
    AtRam ram;

    if (self == NULL)
        return cAtErrorNullPointer;

    ramModule = (AtModuleRam)AtDeviceModuleGet(self, cAtModuleRam);
    ram = AtModuleRamQdrGet(ramModule, qdrId);

    return AtRamIsGood(ram) ? cAtOk : cAtError;
    }

eAtRet Tha60150011DeviceQdrMarginDetect(AtDevice self, uint8 qdrId)
    {
    eAtRet ret = cAtOk;

    if (!QdrMarginCheckingIsSupported(self))
        return cAtErrorModeNotSupport;

    if (!QdrIdIsValid(qdrId))
        return cAtErrorInvlParm;

    if (mThis(self)->qdrEyeIsCentered[qdrId] == cAtFalse)
        ret = QdrEyeReadWriteCenter(self, qdrId);

    ret |= QdrWriteMarginDetect(self, qdrId);
    ret |= QdrReadMarginDetect(self, qdrId);

    return ret;
    }

uint32 Tha60150011DeviceQdrReadLeftMargin(AtDevice self, uint8 qdrId)
    {
    tRamMarginParams* margins = QdrMargin(self, qdrId);

    if (margins->readLeftMargin != 0)
        return margins->readLeftMargin;

    if (Tha60150011DeviceQdrMarginDetect(self, qdrId) != cAtOk)
        return 0;

    return margins->readLeftMargin;
    }

uint32 Tha60150011DeviceQdrReadRightMargin(AtDevice self, uint8 qdrId)
    {
    tRamMarginParams* margins = QdrMargin(self, qdrId);

    if (margins->readRightMargin != 0)
        return margins->readRightMargin;

    if (Tha60150011DeviceQdrMarginDetect(self, qdrId) != cAtOk)
        return 0;

    return margins->readRightMargin;
    }

uint32 Tha60150011DeviceQdrWriteLeftMargin(AtDevice self, uint8 qdrId)
    {
    tRamMarginParams* margins = QdrMargin(self, qdrId);

    if (margins->writeLeftMargin != 0)
        return margins->writeLeftMargin;

    if (Tha60150011DeviceQdrMarginDetect(self, qdrId) != cAtOk)
        return 0;

    return margins->writeLeftMargin;
    }

uint32 Tha60150011DeviceQdrWriteRightMargin(AtDevice self, uint8 qdrId)
    {
    tRamMarginParams* margins = QdrMargin(self, qdrId);

    if (margins->writeRightMargin != 0)
        return margins->writeRightMargin;

    if (Tha60150011DeviceQdrMarginDetect(self, qdrId) != cAtOk)
        return 0;

    return margins->writeRightMargin;
    }

uint32 Tha60150011StartVersionSupportMefOverMplsExpectedEcidComparison(AtDevice device)
    {
    if (ThaDeviceIsEp((ThaDevice)device) || AtDeviceAllFeaturesAvailableInSimulation(device))
        return 0;

    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x02, 0x03, 0x67);
    }

uint32 Tha60150011StartVersionSupportVc4_4c(AtDevice device)
    {
    if (ThaDeviceIsEp((ThaDevice)device) || AtDeviceAllFeaturesAvailableInSimulation(device))
        return 0;

    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x14, 0x12, 0x24, 0x39);
    }

uint32 Tha60150011StartVersionSupportSemValidation(AtDevice self)
    {
    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(self), 0x15, 0x03, 0x28, 0x81);
    }

uint32 Tha60150011QdrMarginDetectMaxElapseTime(AtDevice self)
    {
    return mThis(self)->maxQdrMarginDetectTime;
    }

eBool Tha60150011StickyIsGood(AtDevice self, uint32 bitMask)
    {
    static const uint32 cStickyCheckTimeoutMs = 1000;
    return ThaDeviceStickyIsGood(self, cThaFpgaSticky, bitMask, cStickyCheckTimeoutMs);
    }

uint32 Tha60150011DeviceStartVersionSupportJitterAttenuator(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->StartVersionSupportJitterAttenuator(mThis(self));
    return cInvalidUint32;
    }

eAtRet Tha60150011SuperAsyncInit(AtDevice self)
    {
    return m_AtDeviceMethods->AsyncInit(self);
    }

eAtRet Tha60150011DeviceSemCheck(AtDevice self)
    {
    if (self)
        return SemCheck(self);
    return cAtErrorNullPointer;
    }

eBool Tha60150011NewStartupSequenceIsSupported(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->NewStartupSequenceIsSupported(self);
    return cAtFalse;
    }

eBool Tha60150011DeviceShouldCheckDiagnostic(AtDevice self)
    {
    if (self)
        return ShouldCheckDiagnostic(self);
    return cAtFalse;
    }

eAtRet Tha60150011DeviceDiagnostic(AtDevice self)
    {
    if (self)
        return DeviceDiagnostic(self);
    return cAtErrorNullPointer;
    }

void Tha60150011DeviceM13Init(AtDevice self)
    {
    if (self)
        M13Init(self);
    }

eAtRet Tha60150011DeviceSdhSerdesReset(AtDevice self)
    {
    if (self)
        return SdhSerdesReset(self);
    return cAtErrorNullPointer;
    }

eBool Tha60150011DeviceSemUartIsSupported(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->SemUartIsSupported(mThis(self));
    return cAtFalse;
    }

void Tha60150011DeviceClockStatusDisplay(AtDevice self)
    {
    if (self)
        mMethodsGet(mThis(self))->ClockStatusDisplay(mThis(self));
    }

eAtRet Tha60150011DeviceDiagnosticHalCheck(AtDevice self)
    {
    mSuccessAssert(HalCheck(self));


    return cAtOk;
    }

eAtRet Tha60150011DeviceDiagnosticCorePllCheck(AtDevice self)
    {
    mSuccessAssert(CorePllCheck(self));

    return cAtOk;
    }

eAtRet Tha60150011DeviceDiagnosticDiagCoreReset(AtDevice self)
    {
    mSuccessAssert(DiagCoreAsyncReset(self));

    return cAtOk;
    }

eAtRet Tha60150011DeviceDiagnosticRamCheck(AtDevice self)
    {
    mSuccessAssert(RamCheck(self));

    return cAtOk;
    }

eAtRet Tha60150011DeviceDiagnosticClockCheck(AtDevice self)
    {
    mSuccessAssert(ClockCheck(self));

    return cAtOk;
    }

eAtRet Tha60150011DeviceSemClockProvide(AtDevice self)
    {
    if (self)
        return mMethodsGet(mThis(self))->SemClockProvide(mThis(self));
    return cAtErrorNullPointer;
    }

eAtRet Tha60150011DeviceInit(AtDevice self)
    {
    return Init(self);
    }

void Tha60150011DeviceHwFlush(ThaDevice self)
    {
    if (self)
        mMethodsGet(mThis(self))->HwFlush(mThis(self));
    }

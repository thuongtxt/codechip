/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60150011DeviceAsyncInit.h
 * 
 * Created Date: Aug 16, 2016
 *
 * Description : To handle async init
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011DEVICEASYNCINIT_H_
#define _THA60150011DEVICEASYNCINIT_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eAtRet Tha60150011SuperAsyncInit(AtDevice self);
eAtRet Tha60150011DeviceAsyncInit(AtDevice self);
eAtRet Tha60150011DeviceSemCheck(AtDevice self);
eBool Tha60150011NewStartupSequenceIsSupported(AtDevice self);
eBool Tha60150011DeviceShouldCheckDiagnostic(AtDevice self);
eAtRet Tha60150011DeviceDiagnostic(AtDevice self);
void Tha60150011DeviceM13Init(AtDevice self);
eAtRet Tha60150011DeviceEyeScanInit(AtDevice self);
eAtRet Tha60150011DeviceSdhSerdesReset(AtDevice self);

eAtRet Tha60150011DeviceDiagnosticHalCheck(AtDevice self);
eAtRet Tha60150011DeviceDiagnosticCorePllCheck(AtDevice self);
eAtRet Tha60150011DeviceDiagnosticDiagCoreReset(AtDevice self);
eAtRet Tha60150011DeviceDiagnosticRamCheck(AtDevice self);
eAtRet Tha60150011DeviceDiagnosticClockCheck(AtDevice self);
eAtRet Tha60150011DeviceDiagnosticAsyncInit(AtDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011DEVICEASYNCINIT_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60150011VersionReader.c
 *
 * Created Date: Mar 4, 2015
 *
 * Description : Version reader of device 60150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/versionreader/ThaVersionReaderInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaVersionReaderMethods m_ThaVersionReaderOverride;

/* Super implement */
static const tThaVersionReaderMethods *m_ThaVersionReaderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static char *VersionDescription(ThaVersionReader self, char *buffer, uint32 bufferLen)
    {
    uint32 versionValue;
    AtOsal osal = AtSharedDriverOsalGet();
    uint8 version, day, month, year;

    /* Get version value from hardware */
    versionValue = ThaVersionReaderVersionNumber(self);

    /* Make its description */
    if (bufferLen < 20)
        return NULL;

    mMethodsGet(osal)->MemInit(osal, buffer, 0, bufferLen);
    version  = (uint8)mRegField(versionValue, cVersionHH);
    day      = (uint8)mRegField(versionValue, cVersionDD);
    month    = (uint8)mRegField(versionValue, cVersionMM);
    year     = (uint8)mRegField(versionValue, cVersionYY);

    AtSprintf(buffer, "%d.%d (on 20%d%d/%d%d/%d%d)",
              mLastNible(version) , mFirstNible(version),
              mLastNible(year)    , mFirstNible(year),
              mLastNible(month)   , mFirstNible(month),
              mLastNible(day)     , mFirstNible(day));

    return buffer;
    }

static uint32 HardwareVersion(ThaVersionReader self)
    {
    uint32 versionValue = ThaVersionReaderVersionNumber(self);
    uint32 version = (uint8)mRegField(versionValue, cVersionHH);
    uint32 major = mLastNible(version);
    uint32 minor = mFirstNible(version);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(major, minor, 0);
    }

static void OverrideThaVersionReader(ThaVersionReader self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaVersionReaderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaVersionReaderOverride, m_ThaVersionReaderMethods, sizeof(m_ThaVersionReaderOverride));

        mMethodOverride(m_ThaVersionReaderOverride, VersionDescription);
        mMethodOverride(m_ThaVersionReaderOverride, HardwareVersion);
        }

    mMethodsSet(self, &m_ThaVersionReaderOverride);
    }

static void Override(ThaVersionReader self)
    {
    OverrideThaVersionReader(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tThaVersionReaderPw);
    }

ThaVersionReader Tha60150011VersionReaderObjectInit(ThaVersionReader self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (device == NULL)
        return NULL;

    /* Super constructor */
    if (ThaVersionReaderPwObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaVersionReader Tha60150011VersionReaderNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaVersionReader newReader = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newReader == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011VersionReaderObjectInit(newReader, device);
    }

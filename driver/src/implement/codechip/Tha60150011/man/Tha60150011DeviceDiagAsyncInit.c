/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha60150011DeviceDiagAsyncInit.c
 *
 * Created Date: Jan 9, 2017
 *
 * Description : Implementation for diag state of device async init.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60150011DeviceInternal.h"
#include "Tha60150011DeviceAsyncInit.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60150011Device)self)

/*--------------------------- Local typedefs ---------------------------------*/

typedef enum eDeviceDiagnosticAsyncState
    {
    cDeviceDiagnosticAsyncStateStart = 0,
    cDeviceDiagnosticAsyncState_HalTest = cDeviceDiagnosticAsyncStateStart,
    cDeviceDiagnosticAsyncState_CorePllCheck,
    cDeviceDiagnosticAsyncState_DiagCoreReset,
    cDeviceDiagnosticAsyncState_RamCheck,
    cDeviceDiagnosticAsyncState_ClockCheck
    }eDeviceDiagnosticAsyncState;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static void DiagStateSet(AtDevice self, uint32 state)
    {
    mThis(self)->diagAsyncState = state;
    }

static uint32 DiagStateGet(AtDevice self)
    {
    return mThis(self)->diagAsyncState;
    }

static void DiagStateReset(AtDevice self)
    {
    mThis(self)->diagAsyncState = cDeviceDiagnosticAsyncStateStart;
    }

static eAtRet DiagStateMoveSoak(AtDevice self, eAtRet retCodeToCheck, uint32 newState)
    {
    if (retCodeToCheck == cAtOk)
        {
        DiagStateSet(self, newState);
        return cAtErrorAgain;
        }

    else if ((retCodeToCheck != cAtOk) && (!AtDeviceAsyncRetValIsInState(retCodeToCheck)))
        {
        DiagStateReset(self);
        return retCodeToCheck;
        }

    return retCodeToCheck;
    }

static eAtRet DiagStateMoveEnd(AtDevice self, eAtRet retCodeToCheck)
    {
    if (retCodeToCheck == cAtOk)
        {
        DiagStateReset(self);
        return cAtOk;
        }

    else if ((retCodeToCheck != cAtOk) && (!AtDeviceAsyncRetValIsInState(retCodeToCheck)))
        {
        DiagStateReset(self);
        return retCodeToCheck;
        }

    return retCodeToCheck;
    }

eAtRet Tha60150011DeviceDiagnosticAsyncInit(AtDevice self)
    {
    uint32 state = DiagStateGet(self);
    tAtOsalCurTime profileTime;
    eAtRet ret;

    AtOsalCurTimeGet(&profileTime);

    switch (state)
        {
        case cDeviceDiagnosticAsyncState_HalTest:
            ret = Tha60150011DeviceDiagnosticHalCheck(self);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "Tha60150011DeviceDiagnosticHalCheck");
            return DiagStateMoveSoak(self, ret, cDeviceDiagnosticAsyncState_CorePllCheck);
        case cDeviceDiagnosticAsyncState_CorePllCheck:
            ret = Tha60150011DeviceDiagnosticCorePllCheck(self);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "Tha60150011DeviceDiagnosticCorePllCheck");
            return DiagStateMoveSoak(self, ret, cDeviceDiagnosticAsyncState_DiagCoreReset);
        case cDeviceDiagnosticAsyncState_DiagCoreReset:
            ret = Tha60150011DeviceDiagnosticDiagCoreReset(self);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "Tha60150011DeviceDiagnosticDiagCoreReset");
            return DiagStateMoveSoak(self, ret, cDeviceDiagnosticAsyncState_RamCheck);
        case cDeviceDiagnosticAsyncState_RamCheck:
            ret = Tha60150011DeviceDiagnosticRamCheck(self);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "AtDeviceAccessTimeInMsSinceLastProfileCheck");
            return DiagStateMoveSoak(self, ret, cDeviceDiagnosticAsyncState_ClockCheck);
            break;
        case cDeviceDiagnosticAsyncState_ClockCheck:
            ret = Tha60150011DeviceDiagnosticClockCheck(self);
            AtDeviceAccessTimeInMsSinceLastProfileCheck(self, cAtTrue, &profileTime, AtSourceLocation, "Tha60150011DeviceDiagnosticClockCheck");
            return DiagStateMoveEnd(self, ret);
        default:
            DiagStateReset(self);
            ret = cAtErrorDevFail;
        }

    return ret;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60150011Device.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : Product 60150011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011DEVICE_H_
#define _THA60150011DEVICE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtDevice.h"
#include "../../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
eBool Tha60150011DeviceFpgaIsVersionB(AtDevice self);
eAtRet Tha60150011DeviceQdrTest(AtDevice self, uint8 qdrId);
eBool Tha60150011DeviceDdrCalibIsGood(AtDevice self);
void  Tha60150011DeviceClockStatusDisplay(AtDevice self);

uint32 Tha60150011DeviceStartVersionSupportRtpFrequencyLargerThan16Bits(AtDevice self);
eAtRet Tha60150011DeviceDdrMarginDetect(AtDevice self);
uint32 Tha60150011DeviceDdrReadLeftMargin(AtDevice self);
uint32 Tha60150011DeviceDdrReadRightMargin(AtDevice self);
uint32 Tha60150011DeviceDdrWriteLeftMargin(AtDevice self);
uint32 Tha60150011DeviceDdrWriteRightMargin(AtDevice self);
eAtRet Tha60150011DeviceQdrEyeCenter(AtDevice self, uint8 qdrId);
eAtRet Tha60150011DeviceQdrMarginDetect(AtDevice self, uint8 qdrId);
uint32 Tha60150011DeviceQdrReadLeftMargin(AtDevice self, uint8 qdrId);
uint32 Tha60150011DeviceQdrReadRightMargin(AtDevice self, uint8 qdrId);
uint32 Tha60150011DeviceQdrWriteLeftMargin(AtDevice self, uint8 qdrId);
uint32 Tha60150011DeviceQdrWriteRightMargin(AtDevice self, uint8 qdrId);
uint32 Tha60150011QdrMarginDetectMaxElapseTime(AtDevice self);
uint32 Tha60150011StartVersionSupportMefOverMplsExpectedEcidComparison(AtDevice device);
uint32 Tha60150011StartVersionSupportVc4_4c(AtDevice device);
uint32 Tha60150011StartVersionSupportSemValidation(AtDevice self);
uint32 Tha60150011DeviceStartVersionSupportJitterAttenuator(AtDevice self);
eBool Tha60150011DeviceSemUartIsSupported(AtDevice self);

eBool Tha60150011StickyIsGood(AtDevice self, uint32 bitMask);
eAtRet Tha60150011DeviceSemClockProvide(AtDevice self);
eAtRet Tha60150011DeviceInit(AtDevice self);
void Tha60150011DeviceHwFlush(ThaDevice self);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011DEVICE_H_ */


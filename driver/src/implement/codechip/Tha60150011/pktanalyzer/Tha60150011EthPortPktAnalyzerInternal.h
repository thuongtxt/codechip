/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Pkt Analyzer
 *
 * File        : Tha60150011EthPortPktAnalyzerInternal.h
 *
 * Created Date: Nov 25, 2016 
 *
 * Description :
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011ETHPORTPKTANALYZERINTERNAL_H_
#define _THA60150011ETHPORTPKTANALYZERINTERNAL_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/pktanalyzer/ThaStmPwProductEthPortPktAnalyzerInternal.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011EthPortPktAnalyzer
    {
    tThaStmPwProductEthPortPktAnalyzer super;
    }tTha60150011EthPortPktAnalyzer;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer Tha60150011EthPortPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port);

#ifdef __cplusplus
}
#endif

#endif /* _THA60150011ETHPORTPKTANALYZERINTERNAL_H_ */

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha60150011ModulePktAnalyzerInternal.h
 * 
 * Created Date: Oct 14, 2016
 *
 * Description : Packet analyzer
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011MODULEPKTANALYZERINTERNAL_H_
#define _THA60150011MODULEPKTANALYZERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/pktanalyzer/ThaStmPwProductModulePktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModulePktAnalyzer
    {
    tThaStmPwProductModulePktAnalyzer super;
    }tTha60150011ModulePktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePktAnalyzer Tha60150011ModulePktAnalyzerObjectInit(AtModulePktAnalyzer self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011MODULEPKTANALYZERINTERNAL_H_ */


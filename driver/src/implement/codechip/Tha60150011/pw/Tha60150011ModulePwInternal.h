/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60150011ModulePwInternal.h
 * 
 * Created Date: Apr 9, 2015
 *
 * Description : PW module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011MODULEPWINTERNAL_H_
#define _THA60150011MODULEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/pw/ThaStmPwProductModulePw.h"
#include "Tha60150011ModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModulePwMethods
    {
    eBool (*HwStorageIsSupported)(Tha60150011ModulePw self);
    eBool (*RtpFrequencyLargerThan16BitsIsSupported)(Tha60150011ModulePw self);
    }tTha60150011ModulePwMethods;

typedef struct tTha60150011ModulePw
    {
    tThaStmPwProductModulePw super;
    const tTha60150011ModulePwMethods *methods;
    }tTha60150011ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha60150011ModulePwObjectInit(AtModulePw self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011MODULEPWINTERNAL_H_ */


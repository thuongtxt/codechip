/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60150011ModulePw.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : PW module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011MODULEPW_H_
#define _THA60150011MODULEPW_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtModulePw.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModulePw * Tha60150011ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60150011PwNEPerformanceMornitoringGet(AtPw pw);
uint32 Tha60150011PwFEPerformanceMornitoringGet(AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011MODULEPW_H_ */


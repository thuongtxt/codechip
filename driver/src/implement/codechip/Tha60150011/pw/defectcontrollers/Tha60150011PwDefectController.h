/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha60150011PwDefectController.h
 * 
 * Created Date: Aug 11, 2015
 *
 * Description : PW defect controller of 60150011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011PWDEFECTCONTROLLER_H_
#define _THA60150011PWDEFECTCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/defectcontrollers/ThaPwDefectControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011PwDefectController * Tha60150011PwDefectController;

typedef struct tThaPwThresholdConfigCache
    {
    uint16 bufferOverrunDefectThreshold;
    uint16 bufferUnderrunDefectThreshold;
    uint16 remotePacketLossThreshold;
    uint16 strayPacketThreshold;
    uint16 malformPacketThreshold;
    uint16 missingPacketThreshold;
    uint32 excessiveLostThreshold;
    uint32 timeInSecOfExcessiveLostThreshold;
    uint16 misConnectionThreshold;
    }tThaPwThresholdConfigCache;

typedef struct tTha60150011PwDefectControllerMethods
    {
    uint32 (*DefaultThreshold)(Tha60150011PwDefectController self);
    }tTha60150011PwDefectControllerMethods;

typedef struct tTha60150011PwDefectController
    {
    tThaPwDefectController super;
    const tTha60150011PwDefectControllerMethods *methods;
    tThaPwThresholdConfigCache *cache;
    }tTha60150011PwDefectController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwDefectController Tha60150011PwDefectControllerObjectInit(ThaPwDefectController self, AtPw pw);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011PWDEFECTCONTROLLER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module PW
 *
 * File        : Tha60150011PwDefectController.c
 *
 * Created Date: Sep 4, 2014
 *
 * Description : Defect controller of product 60150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../default/man/ThaDeviceInternal.h"
#include "../../../../default/pw/ThaModulePw.h"
#include "../../../../default/pda/ThaModulePda.h"
#include "Tha60150011PwDefectReg.h"
#include "Tha60150011PwDefectController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60150011PwDefectController)((void*)self))

#define mEncodeCache(fieldName) AtCoderEncodeUInt(encoder, object->cache->fieldName, "cache."#fieldName);

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60150011PwDefectControllerMethods m_methods;

/* Override */
static tAtPwMethods                   m_AtPwOverride;
static tAtObjectMethods               m_AtObjectOverride;
static tThaPwDefectControllerMethods  m_ThaPwDefectControllerOverride;
static tAtChannelMethods              m_AtChannelOverride;

/* Save super's implementation */
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;
static const tAtPwMethods      *m_AtPwMethods      = NULL;
static const tAtChannelMethods *m_AtChannelMethods = NULL;
static const tThaPwDefectControllerMethods *m_ThaPwDefectControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static void Serialize(AtObject self, AtCoder encoder)
    {
    Tha60150011PwDefectController object = (Tha60150011PwDefectController)self;

    m_AtObjectMethods->Serialize(self, encoder);

    if (object->cache == NULL)
        return;

    mEncodeCache(bufferOverrunDefectThreshold);
    mEncodeCache(bufferUnderrunDefectThreshold);
    mEncodeCache(remotePacketLossThreshold);
    mEncodeCache(strayPacketThreshold);
    mEncodeCache(malformPacketThreshold);
    mEncodeCache(missingPacketThreshold);
    mEncodeCache(excessiveLostThreshold);
    mEncodeCache(timeInSecOfExcessiveLostThreshold);
    mEncodeCache(misConnectionThreshold);
    }

static tThaPwThresholdConfigCache *CacheCreate(Tha60150011PwDefectController self)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize = sizeof(tThaPwThresholdConfigCache);
    tThaPwThresholdConfigCache *newCache = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    AtUnused(self);
    if (newCache == NULL)
        return NULL;

    /* Clear memory */
    mMethodsGet(osal)->MemInit(osal, newCache, 0, memorySize);
    return newCache;
    }

static tThaPwThresholdConfigCache* DefectControllerCache(Tha60150011PwDefectController self)
    {
    if (self == NULL)
        return NULL;

    if (self->cache == NULL)
        self->cache = CacheCreate(self);

    return self->cache;
    }

static uint32 PartOffset(AtPw self)
    {
    ThaDevice device = (ThaDevice)AtChannelDeviceGet((AtChannel)self);
    ThaModulePw modulePw = (ThaModulePw)AtChannelModuleGet((AtChannel)self);

    return ThaDeviceModulePartOffset(device, cAtModulePw, ThaModulePwPartOfPw(modulePw, (AtPw)mAdapter(self)));
    }

static uint32 PwDefaultOffset(AtPw self)
    {
    return AtChannelHwIdGet((AtChannel)mAdapter(self)) + PartOffset(self);
    }

static eAtRet PwTypeSet(ThaPwDefectController self, eAtPwType pwType)
    {
    uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset((AtPw)self);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
    mRegFieldSet(longRegVal[0], cPmSecPwCep, (pwType == cAtPwTypeCEP) ? 1 : 0);
    mChannelHwLongWrite(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

    return cAtOk;
    }

static eAtRet AllThresholdsApply(ThaPwDefectController self)
    {
    tThaPwThresholdConfigCache *cache;
    AtPw pw;

    eAtRet ret = m_ThaPwDefectControllerMethods->AllThresholdsApply(self);
    if (ret != cAtOk)
        return ret;

    cache = DefectControllerCache(mThis(self));
    if (cache == NULL)
        return cAtErrorNullPointer;

    pw = (AtPw)self;

    if (cache->bufferUnderrunDefectThreshold)
        AtPwJitterBufferUnderrunDefectThresholdSet(pw, cache->bufferUnderrunDefectThreshold);
    if (cache->bufferOverrunDefectThreshold)
        AtPwJitterBufferOverrunDefectThresholdSet(pw, cache->bufferOverrunDefectThreshold);
    if (cache->remotePacketLossThreshold)
        AtPwRemotePacketLossDefectThresholdSet(pw, cache->remotePacketLossThreshold);
    if (cache->strayPacketThreshold)
        AtPwStrayPacketDefectThresholdSet(pw, cache->strayPacketThreshold);
    if (cache->malformPacketThreshold)
        AtPwMalformedPacketDefectThresholdSet(pw, cache->malformPacketThreshold);
    if (cache->excessiveLostThreshold)
        AtPwExcessivePacketLossRateDefectThresholdSet(pw, cache->excessiveLostThreshold, cache->timeInSecOfExcessiveLostThreshold);
    if (cache->missingPacketThreshold)
        AtPwMissingPacketDefectThresholdSet(pw, cache->missingPacketThreshold);
    if (cache->misConnectionThreshold)
        AtPwMisConnectionDefectThresholdSet(pw, cache->misConnectionThreshold);

    return cAtOk;
    }

static void CacheDelete(Tha60150011PwDefectController self)
    {
    AtOsal osal;

    if (self->cache == NULL)
        return;

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, self->cache);
    self->cache = NULL;
    }

static void Delete(AtObject self)
    {
    CacheDelete(mThis(self));

    /* Fully delete itself */
    m_AtObjectMethods->Delete(self);
    }

static eAtModulePwRet JitterBufferOverrunDefectThresholdHwSet(AtPw self, uint32 numEvent)
    {
    uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
    uint32 longRegVal[cThaLongRegMaxSize];

	mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
	mRegFieldSet(longRegVal[0], cPmSecPwOverrunThres, numEvent);
	mChannelHwLongWrite(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

    return cAtOk;
    }

static eAtModulePwRet JitterBufferOverrunDefectThresholdSet(AtPw self, uint32 numOverrunEvent)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
        if (cache)
            cache->bufferOverrunDefectThreshold = (uint16)numOverrunEvent;
        return cAtOk;
        }

    if (AtPwJitterBufferOverrunDefectThresholdGet(self) == numOverrunEvent)
        return cAtOk;

    return JitterBufferOverrunDefectThresholdHwSet(self, numOverrunEvent);
    }

static uint32 JitterBufferOverrunDefectThresholdHwGet(AtPw self)
    {
    uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
    uint32 longRegVal[cThaLongRegMaxSize];

	mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
	return mRegField(longRegVal[0], cPmSecPwOverrunThres);
    }

static uint32 JitterBufferOverrunDefectThresholdGet(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
        if (cache)
            return cache->bufferOverrunDefectThreshold;

        return 0;
        }

    return JitterBufferOverrunDefectThresholdHwGet(self);
    }

static eAtModulePwRet JitterBufferUnderrunDefectThresholdHwSet(AtPw self, uint32 numEvent)
    {
    uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
    uint32 longRegVal[cThaLongRegMaxSize];

	mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
	mRegFieldSet(longRegVal[0], cPmSecPwUnderrunThres, numEvent);
	mChannelHwLongWrite(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

    return cAtOk;
    }

static eAtModulePwRet JitterBufferUnderrunDefectThresholdSet(AtPw self, uint32 numUnderrunEvent)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
        if (cache)
            cache->bufferUnderrunDefectThreshold = (uint16)numUnderrunEvent;
        return cAtOk;
        }

    if (AtPwJitterBufferUnderrunDefectThresholdGet(self) == numUnderrunEvent)
        return cAtOk;

    return JitterBufferUnderrunDefectThresholdHwSet(self, numUnderrunEvent);
    }

static uint32 JitterBufferUnderrunDefectThresholdHwGet(AtPw self)
    {
    uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
    uint32 longRegVal[cThaLongRegMaxSize];

	mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
	return mRegField(longRegVal[0], cPmSecPwUnderrunThres);
    }

static uint32 JitterBufferUnderrunDefectThresholdGet(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
        if (cache)
            return cache->bufferUnderrunDefectThreshold;

        return 0;
        }

    return JitterBufferUnderrunDefectThresholdHwGet(self);
    }

static uint32 DefectHwGet(AtChannel self)
    {
    uint32 defects = 0;
    uint32 address = cPwPerformanceMonitoringDefectAndAlarm + PwDefaultOffset((AtPw)self);
    uint32 longRegVal[cThaLongRegMaxSize];
    mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

    if (longRegVal[1] & cRxStrayPacketDefect)
        defects |= cAtPwAlarmTypeStrayPacket;
    if (longRegVal[1] & cRxMalformPacketDefect)
        defects |= cAtPwAlarmTypeMalformedPacket;
    if (longRegVal[1] & cRxRbitPacketDefect)
        defects |= cAtPwAlarmTypeRBit;
    if (longRegVal[1] & cRxPbitPacketDefect)
        defects |= cAtPwAlarmTypePBit;
    if (longRegVal[1] & cRxNbitPacketDefect)
        defects |= cAtPwAlarmTypeNBit;
    if (longRegVal[1] & cRxLbitPacketDefect)
        defects |= cAtPwAlarmTypeLBit;
    if (longRegVal[1] & cUnderrunEventDefect)
        defects |= cAtPwAlarmTypeJitterBufferUnderrun;
    if (longRegVal[1] & cOverrunEventDefect)
        defects |= cAtPwAlarmTypeJitterBufferOverrun;
    if (longRegVal[1] & cLostPacketSyncDefect)
        defects |= cAtPwAlarmTypeLops;
    if (longRegVal[1] & cLostPacketDefect)
        defects |= cAtPwAlarmTypeMissingPacket;
    if (longRegVal[1] & cMisConnectionDefect)
        defects |= cAtpwAlarmTypeMisConnection;

    return defects;
    }

static uint32 DefectGet(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return DefectHwGet(self);
    }

static uint32 DefectHistoryHwGet(AtChannel self, eBool read2Clear)
    {
    uint32 defects = 0;
    uint32 address = cPwPerformanceMonitoringDefectAndAlarm + PwDefaultOffset((AtPw)self);
    uint32 longRegVal[cThaLongRegMaxSize];
    mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

    if (longRegVal[0] & cRxStrayPacketSticky)
        defects |= cAtPwAlarmTypeStrayPacket;
    if (longRegVal[0] & cRxMalformPacketSticky)
        defects |= cAtPwAlarmTypeMalformedPacket;
    if (longRegVal[0] & cRxRbitPacketSticky)
        defects |= cAtPwAlarmTypeRBit;
    if (longRegVal[0] & cRxPbitPacketSticky)
        defects |= cAtPwAlarmTypePBit;
    if (longRegVal[0] & cRxNbitPacketSticky)
        defects |= cAtPwAlarmTypeNBit;
    if (longRegVal[0] & cRxLbitPacketSticky)
        defects |= cAtPwAlarmTypeLBit;
    if (longRegVal[0] & cUnderrunEventSticky)
        defects |= cAtPwAlarmTypeJitterBufferUnderrun;
    if (longRegVal[0] & cOverrunEventSticky)
        defects |= cAtPwAlarmTypeJitterBufferOverrun;
    if (longRegVal[0] & cLostPacketSyncSticky)
        defects |= cAtPwAlarmTypeLops;
    if (longRegVal[0] & cLostPacketSticky)
        defects |= cAtPwAlarmTypeMissingPacket;
    if (longRegVal[0] & cMisConnectionSticky)
        defects |= cAtpwAlarmTypeMisConnection;

    if (read2Clear)
        mChannelHwLongWrite(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

    return defects;
    }

static uint32 DefectHistoryGet(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return DefectHistoryHwGet(self, cAtFalse);
    }

static uint32 DefectHistoryClear(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return DefectHistoryHwGet(self, cAtTrue);
    }

static uint32 AlarmHwGet(AtChannel self)
    {
    uint32 alarms = 0;
    uint32 address = cPwPerformanceMonitoringDefectAndAlarm + PwDefaultOffset((AtPw)self);
    uint32 longRegVal[cThaLongRegMaxSize];
    mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

    if (longRegVal[0] & cRxStrayPacketAlarm)
        alarms |= cAtPwAlarmTypeStrayPacket;
    if (longRegVal[0] & cRxMalformPacketAlarm)
        alarms |= cAtPwAlarmTypeMalformedPacket;
    if (longRegVal[0] & cRxRbitPacketAlarm)
        alarms |= cAtPwAlarmTypeRBit;
    if (longRegVal[0] & cRxPbitPacketAlarm)
        alarms |= cAtPwAlarmTypePBit;
    if (longRegVal[0] & cRxNbitPacketAlarm)
        alarms |= cAtPwAlarmTypeNBit;
    if (longRegVal[0] & cRxLbitPacketAlarm)
        alarms |= cAtPwAlarmTypeLBit;
    if (longRegVal[0] & cUnderrunEventAlarm)
        alarms |= cAtPwAlarmTypeJitterBufferUnderrun;
    if (longRegVal[0] & cOverrunEventAlarm)
        alarms |= cAtPwAlarmTypeJitterBufferOverrun;
    if (longRegVal[0] & cLostPacketSyncAlarm)
        alarms |= cAtPwAlarmTypeLops;
    if (longRegVal[0] & cLostPacketAlarm)
        alarms |= cAtPwAlarmTypeMissingPacket;
    if (longRegVal[0] & cMisConnectionAlarm)
        alarms |= cAtpwAlarmTypeMisConnection;

    return alarms;
    }

static uint32 AlarmGet(AtChannel self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter((AtPw)self)))
        return 0;

    return AlarmHwGet(self);
    }

static uint32 AlarmHistoryGet(AtChannel self)
    {
    /* This product support history for defect only */
    AtUnused(self);
    return 0;
    }

static uint32 AlarmHistoryClear(AtChannel self)
    {
    /* This product support history for defect only */
    AtUnused(self);
    return 0;
    }

static uint32 DefaultThreshold(Tha60150011PwDefectController self)
    {
    AtModulePw modulePw = (AtModulePw)AtChannelModuleGet((AtChannel)self);
    return AtModulePwDefaultLopsSetThreshold(modulePw);
    }

static eAtModulePwRet DefaultThresholdSet(ThaPwDefectController self,
                                          uint32 (*ThresholdGet)(AtPw),
                                          eAtModulePwRet (*ThresholdSet)(AtPw self, uint32 numPackets))
    {
    AtPw pw = (AtPw)self;
    uint32 currentThreshold;

    currentThreshold = ThresholdGet(pw);
    if (currentThreshold == 0)
        currentThreshold = mMethodsGet(mThis(self))->DefaultThreshold(mThis(self));

    return ThresholdSet(pw, currentThreshold);
    }

static eAtRet StrayPacketDefectEnable(ThaPwDefectController self, eBool enable)
	{
    uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset((AtPw)self);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
    mRegFieldSet(longRegVal[0], cPmSecPwStrayDefectEn, mBoolToBin(enable));
    mChannelHwLongWrite(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

    return cAtOk;
	}

static eAtRet DefaultExcessivePacketLossRateDefectThresholdSet(ThaPwDefectController self)
    {
    const uint32 cDefaultExcessivePacketLossRateTimeInSec = 1;
    AtPw pw = (AtPw)self;
    uint32 currentThreshold, currentTime;

    currentThreshold = AtPwExcessivePacketLossRateDefectThresholdGet(pw, &currentTime);
    if ((currentThreshold == 0) || (currentTime == 0))
        {
        currentThreshold = mMethodsGet(mThis(self))->DefaultThreshold(mThis(self));
        currentTime = cDefaultExcessivePacketLossRateTimeInSec;
        }

    return AtPwExcessivePacketLossRateDefectThresholdSet(pw, currentThreshold, currentTime);
    }

static eAtRet DefaultSet(ThaPwDefectController self)
	{
    eAtModulePwRet ret = cAtOk;

    ret |= DefaultThresholdSet(self, AtPwStrayPacketDefectThresholdGet, AtPwStrayPacketDefectThresholdSet);
    ret |= DefaultThresholdSet(self, AtPwMalformedPacketDefectThresholdGet, AtPwMalformedPacketDefectThresholdSet);
    ret |= DefaultThresholdSet(self, AtPwRemotePacketLossDefectThresholdGet, AtPwRemotePacketLossDefectThresholdSet);
    ret |= DefaultThresholdSet(self, AtPwJitterBufferOverrunDefectThresholdGet, AtPwJitterBufferOverrunDefectThresholdSet);
    ret |= DefaultThresholdSet(self, AtPwJitterBufferUnderrunDefectThresholdGet, AtPwJitterBufferUnderrunDefectThresholdSet);
    ret |= DefaultThresholdSet(self, AtPwMisConnectionDefectThresholdGet, AtPwMisConnectionDefectThresholdSet);

	ret |= StrayPacketDefectEnable(self, cAtTrue);
	ret |= DefaultExcessivePacketLossRateDefectThresholdSet(self);

	if (AtPwTypeGet((AtPw)self) == cAtPwTypeCEP)
	    ret |= DefaultThresholdSet(self, AtPwMissingPacketDefectThresholdGet, AtPwMissingPacketDefectThresholdSet);

	return ret;
	}

static eAtModulePwRet RemotePacketLossDefectThresholdHwSet(AtPw self, uint32 numPackets)
    {
	uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
	uint32 longRegVal[cThaLongRegMaxSize];

	mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
	mRegFieldSet(longRegVal[0], cPmSecPwRdiThres, numPackets);
	mChannelHwLongWrite(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

	return cAtOk;
    }

static eAtModulePwRet RemotePacketLossDefectThresholdSet(AtPw self, uint32 numPackets)
    {
	if (ThaPwAdapterIsLogicPw(mAdapter(self)))
		{
		tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
		if (cache)
			cache->remotePacketLossThreshold = (uint16)numPackets;
		return cAtOk;
		}

	if (AtPwRemotePacketLossDefectThresholdGet(self) == numPackets)
        return cAtOk;

	return RemotePacketLossDefectThresholdHwSet(self, numPackets);
    }

static uint32 RemotePacketLossDefectThresholdHwGet(AtPw self)
    {
	uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
	uint32 longRegVal[cThaLongRegMaxSize];

	mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
	return mRegField(longRegVal[0], cPmSecPwRdiThres);
    }

static uint32 RemotePacketLossDefectThresholdGet(AtPw self)
    {
	if (ThaPwAdapterIsLogicPw(mAdapter(self)))
		{
		tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
		if (cache)
			return cache->remotePacketLossThreshold;

		return 0;
		}

	return RemotePacketLossDefectThresholdHwGet(self);
    }

static eAtModulePwRet StrayPacketDefectThresholdHwSet(AtPw self, uint32 numPackets)
    {
	uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
	uint32 longRegVal[cThaLongRegMaxSize];

	mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
	mRegFieldSet(longRegVal[0], cPmSecPwStrayTailThres, numPackets & cBit2_0);
	mRegFieldSet(longRegVal[1], cPmSecPwStrayHeadThres, numPackets >> 3);
	mChannelHwLongWrite(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

    return cAtOk;
    }

static eAtModulePwRet StrayPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
	if (ThaPwAdapterIsLogicPw(mAdapter(self)))
		{
		tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
		if (cache)
			cache->strayPacketThreshold = (uint16)numPackets;
		return cAtOk;
		}

	if (AtPwStrayPacketDefectThresholdGet(self) == numPackets)
        return cAtOk;

	return StrayPacketDefectThresholdHwSet(self, numPackets);
    }

static uint32 StrayPacketDefectThresholdHwGet(AtPw self)
    {
	uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
	uint32 longRegVal[cThaLongRegMaxSize];

	mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
	return ((mRegField(longRegVal[1], cPmSecPwStrayHeadThres) << 3) |
		    (mRegField(longRegVal[0], cPmSecPwStrayTailThres) & cBit2_0));
    }

static uint32 StrayPacketDefectThresholdGet(AtPw self)
    {
	if (ThaPwAdapterIsLogicPw(mAdapter(self)))
		{
		tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
		if (cache)
			return cache->strayPacketThreshold;

		return 0;
		}

	return StrayPacketDefectThresholdHwGet(self);
    }

static eAtModulePwRet MalformedPacketDefectThresholdHwSet(AtPw self, uint32 numPackets)
    {
	uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
	uint32 longRegVal[cThaLongRegMaxSize];

	mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
	mRegFieldSet(longRegVal[1], cPmSecPwMalformThres, numPackets);
	mChannelHwLongWrite(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

    return cAtOk;
    }

static eAtModulePwRet MalformedPacketDefectThresholdSet(AtPw self, uint32 numPackets)
    {
	if (ThaPwAdapterIsLogicPw(mAdapter(self)))
		{
		tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
		if (cache)
			cache->malformPacketThreshold = (uint16)numPackets;
		return cAtOk;
		}

	if (AtPwMalformedPacketDefectThresholdGet(self) == numPackets)
        return cAtOk;

	return MalformedPacketDefectThresholdHwSet(self, numPackets);
    }

static uint32 MalformedPacketDefectThresholdHwGet(AtPw self)
    {
	uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
	uint32 longRegVal[cThaLongRegMaxSize];

	mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
	return (mRegField(longRegVal[1], cPmSecPwMalformThres));
    }

static uint32 MalformedPacketDefectThresholdGet(AtPw self)
    {
	if (ThaPwAdapterIsLogicPw(mAdapter(self)))
		{
		tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
		if (cache)
			return cache->malformPacketThreshold;

		return 0;
		}

	return MalformedPacketDefectThresholdHwGet(self);
    }

static eAtModulePwRet ExcessivePacketLossRateDefectThresholdHwSet(AtPw self, uint32 numPackets)
    {
	uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
	uint32 longRegVal[cThaLongRegMaxSize];

	mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
	mRegFieldSet(longRegVal[1], cPmSecPwExcessiveLostThres, numPackets);
	mChannelHwLongWrite(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

    return cAtOk;
    }

static uint32 ExcessivePacketLossRateDefectThresholdHwGet(AtPw self)
    {
    uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
    return mRegField(longRegVal[1], cPmSecPwExcessiveLostThres);
    }

static eAtModulePwRet ExcessivePacketLossRateDefectThresholdSet(AtPw self, uint32 numPackets, uint32 timeInSecond)
    {
	tThaPwThresholdConfigCache *cache;
    uint32 numPacketInOneSec;

	if (timeInSecond == 0)
		return cAtErrorInvlParm;

	cache = DefectControllerCache(mThis(self));
	if (cache)
	    {
	    cache->excessiveLostThreshold = numPackets;
		cache->timeInSecOfExcessiveLostThreshold = timeInSecond;
	    }

	if (ThaPwAdapterIsLogicPw(mAdapter(self)))
		return cAtOk;

	/* Hw only support amount time is one second */
	numPacketInOneSec = numPackets / timeInSecond;
    if (ExcessivePacketLossRateDefectThresholdHwGet(self) == numPacketInOneSec)
        return cAtOk;

    return ExcessivePacketLossRateDefectThresholdHwSet(self, numPacketInOneSec);
    }

static uint32 ExcessivePacketLossRateDefectThresholdGet(AtPw self, uint32* timeInSecond)
    {
	tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));

	if (timeInSecond)
	    *timeInSecond = cache ? cache->timeInSecOfExcessiveLostThreshold : 0;

	if (cache)
		return cache->excessiveLostThreshold;

    return 0;
    }

static eBool LopsClearThresholdIsSupported(AtPw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePwRet LopsClearThresholdSet(AtPw self, uint32 numPackets)
    {
    AtUnused(self);
    AtUnused(numPackets);
    return cAtErrorModeNotSupport;
    }

static uint32 LopsClearThresholdGet(AtPw self)
    {
    AtUnused(self);
    return 0;
    }

static eAtModulePwRet MisConnectionDefectThresholdHwSet(AtPw self, uint32 numPackets)
    {
    uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
    mRegFieldSet(longRegVal[1], cPmSecPwMisConnectionThresTail, numPackets);
    mRegFieldSet(longRegVal[2], cPmSecPwMisConnectionThresHead, numPackets >> 7);
    mChannelHwLongWrite(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);

    return cAtOk;
    }

static eAtModulePwRet MisConnectionDefectThresholdSet(AtPw self, uint32 numPackets)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
        if (cache)
            cache->misConnectionThreshold = (uint16)numPackets;
        return cAtOk;
        }

    if (AtPwMisConnectionDefectThresholdGet(self) == numPackets)
        return cAtOk;

    return MisConnectionDefectThresholdHwSet(self, numPackets);
    }

static uint32 MisConnectionDefectThresholdHwGet(AtPw self)
    {
    uint32 address = cPwPerformanceMonitoringThresholdControl + PwDefaultOffset(self);
    uint32 longRegVal[cThaLongRegMaxSize];

    mChannelHwLongRead(self, address, longRegVal, cThaLongRegMaxSize, cThaModulePwPmc);
    return ((mRegField(longRegVal[2], cPmSecPwMisConnectionThresHead) << 7) | mRegField(longRegVal[1], cPmSecPwMisConnectionThresTail));
    }

static uint32 MisConnectionDefectThresholdGet(AtPw self)
    {
    if (ThaPwAdapterIsLogicPw(mAdapter(self)))
        {
        tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
        if (cache)
            return cache->misConnectionThreshold;

        return 0;
        }

    return MisConnectionDefectThresholdHwGet(self);
    }

static eAtRet WarmRestore(AtChannel self)
    {
    AtPw pw = (AtPw)self;
    tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));

    if (cache)
        {
        /* All we can get from HW about this threshold */
        cache->excessiveLostThreshold = ExcessivePacketLossRateDefectThresholdHwGet(pw);
        cache->timeInSecOfExcessiveLostThreshold = 1;
        }

    return cAtOk;
    }

static eBool MissingPacketDefectThresholdIsSupported(AtPw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet MissingPacketDefectThresholdSave(ThaPwDefectController self, uint32 numPackets)
    {
    tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
    if (cache)
        cache->missingPacketThreshold = (uint16)numPackets;
    return cAtOk;
    }

static uint32 MissingPacketDefectThresholdDbGet(ThaPwDefectController self)
    {
    tThaPwThresholdConfigCache *cache = DefectControllerCache(mThis(self));
    if (cache)
        return cache->missingPacketThreshold;

    return 0;
    }

static void OverrideAtPw(ThaPwDefectController self)
    {
    AtPw pw = (AtPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPwMethods = mMethodsGet(pw);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPwOverride, m_AtPwMethods, sizeof(m_AtPwOverride));

        mMethodOverride(m_AtPwOverride, JitterBufferOverrunDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, JitterBufferOverrunDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, JitterBufferUnderrunDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, JitterBufferUnderrunDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, RemotePacketLossDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, RemotePacketLossDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, StrayPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, StrayPacketDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, MalformedPacketDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MalformedPacketDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, ExcessivePacketLossRateDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, ExcessivePacketLossRateDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdSet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdGet);
        mMethodOverride(m_AtPwOverride, MisConnectionDefectThresholdSet);
        mMethodOverride(m_AtPwOverride, MisConnectionDefectThresholdGet);
        mMethodOverride(m_AtPwOverride, LopsClearThresholdIsSupported);
        mMethodOverride(m_AtPwOverride, MissingPacketDefectThresholdIsSupported);
        }

    mMethodsSet(pw, &m_AtPwOverride);
    }

static void OverrideAtObject(ThaPwDefectController self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPwDefectController(ThaPwDefectController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPwDefectControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwDefectControllerOverride, m_ThaPwDefectControllerMethods, sizeof(m_ThaPwDefectControllerOverride));

        mMethodOverride(m_ThaPwDefectControllerOverride, PwTypeSet);
        mMethodOverride(m_ThaPwDefectControllerOverride, AllThresholdsApply);
        mMethodOverride(m_ThaPwDefectControllerOverride, DefaultSet);
        mMethodOverride(m_ThaPwDefectControllerOverride, MissingPacketDefectThresholdSave);
        mMethodOverride(m_ThaPwDefectControllerOverride, MissingPacketDefectThresholdDbGet);
        }

    mMethodsSet(self, &m_ThaPwDefectControllerOverride);
    }

static void OverrideAtChannel(ThaPwDefectController self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, DefectGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryGet);
        mMethodOverride(m_AtChannelOverride, DefectHistoryClear);
        mMethodOverride(m_AtChannelOverride, AlarmGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryGet);
        mMethodOverride(m_AtChannelOverride, AlarmHistoryClear);
        mMethodOverride(m_AtChannelOverride, WarmRestore);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(ThaPwDefectController self)
    {
    OverrideAtObject(self);
    OverrideAtPw(self);
    OverrideThaPwDefectController(self);
    OverrideAtChannel(self);
    }

static void MethodsInit(ThaPwDefectController self)
    {
    Tha60150011PwDefectController controller = (Tha60150011PwDefectController)self;
    
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, DefaultThreshold);
        }

    mMethodsSet(controller, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011PwDefectController);
    }

ThaPwDefectController Tha60150011PwDefectControllerObjectInit(ThaPwDefectController self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwDefectControllerObjectInit(self, pw) == NULL)
        return NULL;

    /* Override */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwDefectController Tha60150011PwDefectControllerNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwDefectController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011PwDefectControllerObjectInit(newController, pw);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Module PW
 * 
 * File        : Tha60150011PwDefectReg.h
 * 
 * Created Date: Sep 5, 2014
 *
 * Description : Defect registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011PWDEFECTREG_H_
#define _THA60150011PWDEFECTREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#define cPwPerformanceMonitoringThresholdControl 0x532000

#define cPmSecPwMisConnectionThresHeadMask   cBit1_0 /* bit 65-64 */
#define cPmSecPwMisConnectionThresHeadShift  0

#define cPmSecPwMisConnectionThresTailMask   cBit31_25 /* bit63-57 */
#define cPmSecPwMisConnectionThresTailShift  25

#define cPmSecPwExcessiveLostThresMask   cBit24_15 /* bit 56-47 */
#define cPmSecPwExcessiveLostThresShift  15

#define cPmSecPwMalformThresMask         cBit14_6 /* bit 46-38 */
#define cPmSecPwMalformThresShift        6

#define cPmSecPwStrayHeadThresMask       cBit5_0 /* bit 37-32 */
#define cPmSecPwStrayHeadThresShift      0

#define cPmSecPwStrayTailThresMask       cBit31_29
#define cPmSecPwStrayTailThresShift      29

#define cPmSecPwRdiThresMask             cBit28_20
#define cPmSecPwRdiThresShift            20

#define cPmSecPwOverrunThresMask         cBit19_11
#define cPmSecPwOverrunThresShift        11

#define cPmSecPwUnderrunThresMask        cBit10_2
#define cPmSecPwUnderrunThresShift       2

#define cPmSecPwStrayDefectEnMask        cBit1
#define cPmSecPwStrayDefectEnShift       1

#define cPmSecPwCepMask                  cBit0
#define cPmSecPwCepShift                 0

#define cPwPerformanceMonitoringDefectAndAlarm 0x531000
#define cMisConnectionDefect      cBit13 /* cBit45 */
#define cRxOutSeqReorPacketDefect cBit12 /* cBit44 */
#define cRxOutSeqDropPacketDefect cBit11 /* cBit43 */
#define cRxStrayPacketDefect      cBit10 /* cBit42 */
#define cRxMalformPacketDefect    cBit9  /* cBit41 */
#define cRxRbitPacketDefect       cBit8  /* cBit40 */
#define cRxPbitPacketDefect       cBit7  /* cBit39 */
#define cRxNbitPacketDefect       cBit6  /* cBit38 */
#define cRxLbitPacketDefect       cBit5  /* cBit37 */
#define cUnderrunEventDefect      cBit4  /* cBit36 */
#define cOverrunEventDefect       cBit3  /* cBit35 */
#define cLostPacketSyncDefect     cBit2  /* cBit34 */
#define cLostPacketStateDefect    cBit1  /* cBit33 */
#define cLostPacketDefect         cBit0  /* cBit32 */

#define cMisConnectionAlarm       cBit29
#define cRxOutSeqReorPacketAlarm  cBit28
#define cRxOutSeqDropPacketAlarm  cBit27
#define cRxStrayPacketAlarm       cBit26
#define cRxMalformPacketAlarm     cBit25
#define cRxRbitPacketAlarm        cBit24
#define cRxPbitPacketAlarm        cBit23
#define cRxNbitPacketAlarm        cBit22
#define cRxLbitPacketAlarm        cBit21
#define cUnderrunEventAlarm       cBit20
#define cOverrunEventAlarm        cBit19
#define cLostPacketSyncAlarm      cBit18
#define cLostPacketStateAlarm     cBit17
#define cLostPacketAlarm          cBit16

#define cMisConnectionSticky      cBit13
#define cRxOutSeqReorPacketSticky cBit12
#define cRxOutSeqDropPacketSticky cBit11
#define cRxStrayPacketSticky      cBit10
#define cRxMalformPacketSticky    cBit9
#define cRxRbitPacketSticky       cBit8
#define cRxPbitPacketSticky       cBit7
#define cRxNbitPacketSticky       cBit6
#define cRxLbitPacketSticky       cBit5
#define cUnderrunEventSticky      cBit4
#define cOverrunEventSticky       cBit3
#define cLostPacketSyncSticky     cBit2
#define cLostPacketStateSticky    cBit1
#define cLostPacketSticky         cBit0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/

#endif /* _THA60150011PWDEFECTREG_H_ */


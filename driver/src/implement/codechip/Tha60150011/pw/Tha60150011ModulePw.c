/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module Pw
 *
 * File        : Tha60150011ModulePw.c
 *
 * Created Date: Aug 29, 2014
 *
 * Description : PW module of product 60150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaPdhVcDe1Internal.h"
#include "../cdr/Tha60150011ModuleCdr.h"
#include "../man/Tha60150011Device.h"
#include "Tha60150011ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* There are 36 bits for each pseudowire
 * - Bit [19:0] for PW's label
 * - Bit [20] for PW's ethernet port ID validation
 * - Bit [31-21] for store PW ID of DE1 in case of SAToP
 *    + Bit 21 for PW ID validation
 *    + Bit 31-22 for PW ID
 * - Bit [32] for pw enable
 */
#define cPwStorageAddress            0x444000
#define cPwLabelMask                 cBit19_0
#define cPwLabelShift                0

#define cPwEthPortIdStoreValidMask   cBit20
#define cPwEthPortIdStoreValidShift  20

#define cDe1BoundPwHwIdValidMask     cBit21
#define cDe1BoundPwHwIdValidShift    21

#define cDe1BoundPwHwIdMask          cBit31_22
#define cDe1BoundPwHwIdShift         22

#define cPwEnableMask                cBit0 /* Bit 32 */
#define cPwEnableShift               0

#define cPwBufferSizeDisparityMask      cBit15_1 /* Bit 47:33 */
#define cPwBufferSizeDisparityShift     1
#define cPwBufferSizeDisparitySignMask  cBit16   /* Bit 48 */
#define cPwBufferSizeDisparitySignShift 16


#define cPwBufferDelayDisparityMask      cBit30_17 /* Bit 62:49 */
#define cPwBufferDelayDisparityShift     17
#define cPwBufferDelayDisparitySignMask  cBit31 /* Bit 63 */
#define cPwBufferDelayDisparitySignShift 31


#define cValidId   1
#define cInvalidId 0

#define cPwPerformanceMonitoringReg 0x530000
#define cPmSecPwNSMask  cBit14
#define cPmSecPwNSShift 14
#define cPmSecPwFSMask  cBit30
#define cPmSecPwFSShift 30

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60150011ModulePw)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60150011ModulePwMethods m_methods;

/* Override */
static tThaModulePwMethods m_ThaModulePwOverride;
static tAtModulePwMethods  m_AtModulePwOverride;
static tAtModuleMethods    m_AtModuleOverride;

/* Save super implementation */
static const tAtModuleMethods   *m_AtModuleMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/
extern eBool Tha60150011DeviceFpgaIsVersionB(AtDevice self);
extern uint32 Tha60150011DeviceStartVersionSupportRtpFrequencyLargerThan16Bits(AtDevice self);

/*--------------------------- Implementation ---------------------------------*/
static eBool PwMbitCounterIsSupported(ThaModulePw self)
    {
    /* This product support M bit from the beginning */
    AtUnused(self);
    return cAtTrue;
    }

static AtPwCep CepObjectCreate(AtModulePw self, uint32 pwId, eAtPwCepMode mode)
    {
    if (mode == cAtPwCepModeFractional)
        return NULL;

    return ThaModulePwCepObjectCreate(self, pwId, mode);
    }

static ThaPwDefectController DefectControllerCreate(ThaModulePw self, AtPw pw)
    {
    AtUnused(self);
    return Tha60150011PwDefectControllerNew(pw);
    }
    
static eBool DynamicPwAllocation(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool MefOverMplsIsSupported(ThaModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static ThaModuleCdr CdrModule(AtModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModuleCdr)AtDeviceModuleGet(device, cThaModuleCdr);
    }

static eBool RtpFrequencyLargerThan16BitsIsSupported(Tha60150011ModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (AtDeviceVersionNumber(device) >= Tha60150011DeviceStartVersionSupportRtpFrequencyLargerThan16Bits(device))
        return cAtTrue;

    return cAtFalse;
    }

static eBool RtpTimestampFrequencyIsSupported(AtModulePw self, uint32 khz)
    {
    /*
     * This product support following values of frequency
     *   - 155.52 MHz
     *   - 77.76 MHz
     *   - Values less than 16bits and multiple of 8 khz
     */

    /* Less than 16bits and multiple of 8 khz */
    if (khz <= cBit15_0)
        {
        if ((khz % 8) == 0)
            return cAtTrue;

        AtModuleLog((AtModule)self, cAtLogLevelWarning, AtSourceLocation, "Frequency must be multiple of 8Khz\r\n");
        return cAtFalse;
        }

    if (!mMethodsGet(mThis(self))->RtpFrequencyLargerThan16BitsIsSupported(mThis(self)))
        {
        AtModuleLog((AtModule)self, cAtLogLevelWarning, AtSourceLocation, "This version of FPGA do not support frequency larger than %lu\r\n", cBit15_0);
        return cAtFalse;
        }

    /* Larger than 16bits, just support 155.52 MHz and 77.76 MHz */
    if ((khz == 155520) || (khz == 77760))
        return cAtTrue;

    AtModuleLog((AtModule)self, cAtLogLevelWarning, AtSourceLocation, "Only support 155520khz, 77760khz and smaller than %lu\r\n", cBit15_0);
    return cAtFalse;
    }

static eAtModulePwRet CepRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    return Tha60150011ModuleCdrCepDcrRtpTimestampFrequencySet(CdrModule(self), khz);
    }

static uint32 CepRtpTimestampFrequencyGet(AtModulePw self)
    {
    return Tha60150011ModuleCdrCepDcrRtpTimestampFrequencyGet(CdrModule(self));
    }

static eAtModulePwRet CesRtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    return Tha60150011ModuleCdrCESoPDcrRtpTimestampFrequencySet(CdrModule(self), khz);
    }

static uint32 CesRtpTimestampFrequencyGet(AtModulePw self)
    {
    return Tha60150011ModuleCdrCESoPDcrRtpTimestampFrequencyGet(CdrModule(self));
    }

static eAtModulePwRet RtpTimestampFrequencySet(AtModulePw self, uint32 khz)
    {
    eAtRet ret = cAtOk;
    ret |= Tha60150011ModuleCdrCESoPDcrRtpTimestampFrequencySet(CdrModule(self), khz);
    ret |= Tha60150011ModuleCdrCepDcrRtpTimestampFrequencySet(CdrModule(self), khz);
    return ret;
    }

static uint32 RtpTimestampFrequencyGet(AtModulePw self)
    {
    return Tha60150011ModuleCdrCepDcrRtpTimestampFrequencyGet(CdrModule(self));
    }

static void ResetPwHwStorage(AtModule self)
    {
    uint32 pw_i;
    uint32 longRegVal[cThaLongRegMaxSize];
    AtIpCore ipCore = AtDeviceIpCoreGet(AtModuleDeviceGet(self), 0);

    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)) == cAtFalse)
        return;

    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
    for (pw_i = 0; pw_i < AtModulePwMaxPwsGet((AtModulePw)self); pw_i++)
        mModuleHwLongWrite(self, cPwStorageAddress + pw_i, longRegVal, cThaLongRegMaxSize, ipCore);
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);

    /* Reset HW storage */
    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)))
        ResetPwHwStorage(self);

    return ret;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static uint32 PwLabelGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address;

    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)) == cAtFalse)
        return 0;

    address = cPwStorageAddress + AtChannelHwIdGet((AtChannel)pw);
    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    return mRegField(longRegVal[0], cPwLabel);
    }

static eAtRet PwLabelSaveToHwStorage(ThaModulePw self, AtPw pw, uint32 pwLabel)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address;

    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)) == cAtFalse)
        return cAtOk;

    address = cPwStorageAddress + AtChannelHwIdGet((AtChannel)pw);
    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    mRegFieldSet(longRegVal[0], cPwLabel, pwLabel);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);

    return cAtOk;
    }

static uint8 PwEthPortIdGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address;

    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)) == cAtFalse)
        return 0;

    address = cPwStorageAddress + AtChannelHwIdGet((AtChannel)pw);
    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);

    /* Only have 1 ethernet port */
    if (mRegField(longRegVal[0], cPwEthPortIdStoreValid) == cValidId)
        return 0;

    return cThaPwInvalidEthPortId;
    }

static eAtRet PwEthPortIdSaveToHwStorage(ThaModulePw self, AtPw pw, uint8 ethPortId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address;

    AtUnused(ethPortId);

    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)) == cAtFalse)
        return cAtOk;

    address = cPwStorageAddress + AtChannelHwIdGet((AtChannel)pw);
    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    mRegFieldSet(longRegVal[0], cPwEthPortIdStoreValid, cValidId);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);

    return cAtOk;
    }

static eAtRet PwEthPortIdHwStorageClear(ThaModulePw self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address;

    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)) == cAtFalse)
        return cAtOk;

    address = cPwStorageAddress + AtChannelHwIdGet((AtChannel)pw);
    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    mRegFieldSet(longRegVal[0], cPwEthPortIdStoreValid, cInvalidId);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);

    return cAtOk;
    }

/*
 * This product support E1 only, so assume this DE1 is E1
 * First 504 registers save for slice 0
 * Next 504 registers save for slice 1
 * (HW only support 1024 registers for using as storage correspond to 1024 PW)
 */
static uint32 De1PwSavePosition(ThaPdhDe1 self)
    {
    uint8 slice, hwIdInSlice;
    AtSdhChannel sdhVc = (AtSdhChannel)AtPdhChannelVcInternalGet((AtPdhChannel)self);

    ThaPdhChannelHwIdGet((AtPdhChannel)self, cAtModulePdh, &slice, &hwIdInSlice);
    return (hwIdInSlice * 21UL) + (AtSdhChannelTug2Get(sdhVc) * 3UL) + AtSdhChannelTu1xGet(sdhVc) + (504UL * slice);
    }

static uint16 De1BoundPwIdGetFromHwStorage(ThaModulePw self, AtPdhDe1 de1)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address;

    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)) == cAtFalse)
        return 0;

    address = cPwStorageAddress + De1PwSavePosition((ThaPdhDe1)de1);
    mChannelHwLongRead(de1, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    if (mRegField(longRegVal[0], cDe1BoundPwHwIdValid) == cInvalidId)
        return cThaModulePwInvalidPwId;

    return (uint16)mRegField(longRegVal[0], cDe1BoundPwHwId);
    }

static eAtRet De1BoundPwIdSaveToHwStorage(ThaModulePw self, AtPdhDe1 de1, uint16 pwHwId)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address;

    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)) == cAtFalse)
        return cAtOk;

    address = cPwStorageAddress + De1PwSavePosition((ThaPdhDe1)de1);
    mChannelHwLongRead(de1, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    mRegFieldSet(longRegVal[0], cDe1BoundPwHwIdValid, cValidId);
    mRegFieldSet(longRegVal[0], cDe1BoundPwHwId, pwHwId);
    mChannelHwLongWrite(de1, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);

    return cAtOk;
    }

static eAtRet De1BoundPwIdHwStorageClear(ThaModulePw self, AtPdhDe1 de1)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address;

    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)) == cAtFalse)
        return cAtOk;

    address = cPwStorageAddress + De1PwSavePosition((ThaPdhDe1)de1);
    mChannelHwLongRead(de1, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    mRegFieldSet(longRegVal[0], cDe1BoundPwHwIdValid, cInvalidId);
    mChannelHwLongWrite(de1, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);

    return cAtOk;
    }

static eBool PwEnableGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address;

    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)) == cAtFalse)
        return cAtFalse;

    address = cPwStorageAddress + AtChannelHwIdGet((AtChannel)pw);
    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    return (mRegField(longRegVal[1], cPwEnable)) ? cAtTrue : cAtFalse;
    }

static eAtRet PwEnableSaveToHwStorage(ThaModulePw self, AtPw pw, eBool enable)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address;

    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)) == cAtFalse)
        return cAtOk;

    address = cPwStorageAddress + AtChannelHwIdGet((AtChannel)pw);
    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    mRegFieldSet(longRegVal[1], cPwEnable, (enable) ? 1 : 0);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);

    return cAtOk;
    }

static uint32 StartVersionSupportBufferDisparityHwStorage(AtDevice device)
    {
    if (Tha60150011DeviceFpgaIsVersionB(device))
        return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x01, 0x08, 0x65);

    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x01, 0x10, 0x41);
    }

static eBool BufferDisparityHwStorageIsSupported(ThaModulePw self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);

    if (mMethodsGet(mThis(self))->HwStorageIsSupported(mThis(self)) == cAtFalse)
        return cAtFalse;

    if (AtDeviceAllFeaturesAvailableInSimulation(device))
        return cAtTrue;

    if (AtDeviceVersionNumber(device) >= StartVersionSupportBufferDisparityHwStorage(device))
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet PwBufferSizeDisparitySaveToHwStorage(ThaModulePw self, AtPw pw, int disparityInUs)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address, absolute;

    if (!BufferDisparityHwStorageIsSupported(self))
        return cAtOk;

    address = cPwStorageAddress + AtChannelHwIdGet((AtChannel)pw);
    absolute = AtStdAbs(AtStdSharedStdGet(), disparityInUs);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    mRegFieldSet(longRegVal[1], cPwBufferSizeDisparitySign, (disparityInUs > 0) ? 1 : 0);
    mRegFieldSet(longRegVal[1], cPwBufferSizeDisparity, absolute);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);

    return cAtOk;
    }

static int PwBufferSizeDisparityGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address, absolute;
    uint32 isPositive;

    if (!BufferDisparityHwStorageIsSupported(self))
        return 0;

    address = cPwStorageAddress + AtChannelHwIdGet((AtChannel)pw);
    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    isPositive = mRegField(longRegVal[1], cPwBufferSizeDisparitySign);
    absolute = mRegField(longRegVal[1], cPwBufferSizeDisparity);

    return (int)((isPositive == 1) ? absolute : (0 - absolute));
    }

static eAtRet PwBufferDelayDisparitySaveToHwStorage(ThaModulePw self, AtPw pw, int disparityInUs)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address, absolute;

    if (!BufferDisparityHwStorageIsSupported(self))
        return cAtOk;

    address = cPwStorageAddress + AtChannelHwIdGet((AtChannel)pw);
    absolute = AtStdAbs(AtStdSharedStdGet(), disparityInUs);

    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    mRegFieldSet(longRegVal[1], cPwBufferDelayDisparitySign, (disparityInUs > 0) ? 1 : 0);
    mRegFieldSet(longRegVal[1], cPwBufferDelayDisparity, absolute);
    mChannelHwLongWrite(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);

    return cAtOk;
    }

static int PwBufferDelayDisparityGetFromHwStorage(ThaModulePw self, AtPw pw)
    {
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 address, absolute;
    uint32 isPositive;

    if (!BufferDisparityHwStorageIsSupported(self))
        return 0;

    address = cPwStorageAddress + AtChannelHwIdGet((AtChannel)pw);
    mChannelHwLongRead(pw, address, longRegVal, cThaLongRegMaxSize, cAtModulePw);
    isPositive = mRegField(longRegVal[1], cPwBufferDelayDisparitySign);
    absolute = mRegField(longRegVal[1], cPwBufferDelayDisparity);

    return (int)((isPositive == 1) ? absolute : (0 - absolute));
    }

static eBool RtpIsEnabledAsDefault(AtModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 1024;
    }

static eBool UseLbitPacketsForAcrDcr(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet ResetPw(ThaModulePw self, AtPw pw)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModuleCos cosModule = (ThaModuleCos)AtDeviceModuleGet(device, cThaModuleCos);

    ThaModuleCosPwHeaderLengthSet(cosModule, (AtPw)ThaPwAdapterGet(pw), 0);
    return cAtOk;
    }

static eBool HwStorageIsSupported(Tha60150011ModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 DefaultLopsSetThreshold(AtModulePw self)
    {
    AtUnused(self);
    return 300;
    }

static void OverrideAtModule(AtModulePw self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideThaModulePw(AtModulePw self)
    {
    ThaModulePw pwModule = (ThaModulePw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(pwModule), sizeof(m_ThaModulePwOverride));
        mMethodOverride(m_ThaModulePwOverride, PwMbitCounterIsSupported);
        mMethodOverride(m_ThaModulePwOverride, DefectControllerCreate);
        mMethodOverride(m_ThaModulePwOverride, DynamicPwAllocation);
        mMethodOverride(m_ThaModulePwOverride, MefOverMplsIsSupported);
        mMethodOverride(m_ThaModulePwOverride, PwLabelGetFromHwStorage);
        mMethodOverride(m_ThaModulePwOverride, PwLabelSaveToHwStorage);
        mMethodOverride(m_ThaModulePwOverride, PwEthPortIdSaveToHwStorage);
        mMethodOverride(m_ThaModulePwOverride, PwEthPortIdGetFromHwStorage);
        mMethodOverride(m_ThaModulePwOverride, PwEthPortIdHwStorageClear);
        mMethodOverride(m_ThaModulePwOverride, De1BoundPwIdGetFromHwStorage);
        mMethodOverride(m_ThaModulePwOverride, De1BoundPwIdSaveToHwStorage);
        mMethodOverride(m_ThaModulePwOverride, De1BoundPwIdHwStorageClear);
        mMethodOverride(m_ThaModulePwOverride, PwEnableGetFromHwStorage);
        mMethodOverride(m_ThaModulePwOverride, PwEnableSaveToHwStorage);
        mMethodOverride(m_ThaModulePwOverride, UseLbitPacketsForAcrDcr);
        mMethodOverride(m_ThaModulePwOverride, PwBufferSizeDisparitySaveToHwStorage);
        mMethodOverride(m_ThaModulePwOverride, PwBufferSizeDisparityGetFromHwStorage);
        mMethodOverride(m_ThaModulePwOverride, PwBufferDelayDisparitySaveToHwStorage);
        mMethodOverride(m_ThaModulePwOverride, PwBufferDelayDisparityGetFromHwStorage);
        mMethodOverride(m_ThaModulePwOverride, ResetPw);
        }

    mMethodsSet(pwModule, &m_ThaModulePwOverride);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, mMethodsGet(self), sizeof(m_AtModulePwOverride));

        mMethodOverride(m_AtModulePwOverride, CepObjectCreate);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, CepRtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, CepRtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, CesRtpTimestampFrequencySet);
        mMethodOverride(m_AtModulePwOverride, CesRtpTimestampFrequencyGet);
        mMethodOverride(m_AtModulePwOverride, RtpIsEnabledAsDefault);
        mMethodOverride(m_AtModulePwOverride, RtpTimestampFrequencyIsSupported);
        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        mMethodOverride(m_AtModulePwOverride, DefaultLopsSetThreshold);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideThaModulePw(self);
    OverrideAtModulePw(self);
    OverrideAtModule(self);
    }

static void MethodsInit(AtModulePw self)
    {
    Tha60150011ModulePw pwModule = (Tha60150011ModulePw)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HwStorageIsSupported);
        mMethodOverride(m_methods, RtpFrequencyLargerThan16BitsIsSupported);
        }

    mMethodsSet(pwModule, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ModulePw);
    }

AtModulePw Tha60150011ModulePwObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha60150011ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModulePwObjectInit(newModule, device);
    }

uint32 Tha60150011PwNEPerformanceMornitoringGet(AtPw pw)
    {
    uint32 regVal = mChannelHwRead(pw, cPwPerformanceMonitoringReg + AtChannelHwIdGet((AtChannel)pw), cThaModuleCla);

    /* If Suspect second bit raise, clear it by write this bit 1 */
    if (regVal & cPmSecPwNSMask)
        mChannelHwWrite(pw, cPwPerformanceMonitoringReg + AtChannelHwIdGet((AtChannel)pw), cPmSecPwNSMask, cThaModuleCla);

    return (regVal & cBit15_0);
    }

uint32 Tha60150011PwFEPerformanceMornitoringGet(AtPw pw)
    {
    uint32 regVal = mChannelHwRead(pw, cPwPerformanceMonitoringReg + AtChannelHwIdGet((AtChannel)pw), cThaModuleCla);

    /* If Suspect second bit raise, clear it by write this bit 1 */
    if (regVal & cPmSecPwFSMask)
        mChannelHwWrite(pw, cPwPerformanceMonitoringReg + AtChannelHwIdGet((AtChannel)pw), cPmSecPwFSMask, cThaModuleCla);

    return ((regVal >> 16) & cBit15_0);
    }

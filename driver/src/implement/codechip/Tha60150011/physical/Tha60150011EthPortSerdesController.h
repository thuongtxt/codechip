/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60150011EthPortSerdesController.h
 * 
 * Created Date: Jun 1, 2016
 *
 * Description : SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011ETHPORTSERDESCONTROLLER_H_
#define _THA60150011ETHPORTSERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/
AtSerdesController Tha60150011EthPortSerdesControllerNew(AtEthPort ethPort, uint32 serdesId);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011ETHPORTSERDESCONTROLLER_H_ */


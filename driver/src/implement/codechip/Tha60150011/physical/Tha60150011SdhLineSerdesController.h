/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Physical
 * 
 * File        : Tha60150011SdhLineSerdesController.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : SERDES controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011SDHLINESERDESCONTROLLER_H_
#define _THA60150011SDHLINESERDESCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/physical/ThaSdhLineSerdesControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011SdhLineSerdesController
    {
    tThaSdhLineSerdesController super;
    }tTha60150011SdhLineSerdesController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
void Tha60150011SdhLineSerdesControllerTransmitDifferentialSet(AtSerdesController self, uint8 value);
eAtRet Tha60150011SdhLineSerdesControllerReset(AtSerdesController self);
AtSerdesController Tha60150011SdhLineSerdesControllerObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011SDHLINESERDESCONTROLLER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Physical
 *
 * File        : Tha60150011SdhLineSerdesController.c
 *
 * Created Date: Jan 6, 2015
 *
 * Description : SDH line serdes controller of 60150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60150011SdhLineSerdesController.h"

/*--------------------------- Define -----------------------------------------*/
#define cSerdesControl       0xF00042
#define cNormalOperation     0x0
#define cNearEndPCSLoopback  0x1
#define cNearEndPMALoopback  0x2
#define cFarEndPMALoopback   0x4
#define cFarEndPCSLoopback   0x6
#define cSerdesTxDifferentialMask  cBit6_3
#define cSerdesTxDifferentialShift 3
#define cSerdesLoopbackMask        cBit2_0
#define cSerdesLoopbackShift       0
#define cSerdesResetMask      cBit31
#define cSerdesResetShift     31

/*
0xF00020
[00:00]     TX0 Reset Done
[01:01]     TX1 Reset Done
[02:02]     TX2 Reset Done
[03:02]     TX3 Reset Done
[04:04]     RX0 Reset Done
[05:05]     RX1 Reset Done
[06:06]     RX2 Reset Done
[07:07]     RX3 Reset Done
[09:08]     TX0 status buffer
[11:10]     TX1 status buffer
[13:12]     TX2 status buffer
[15:14]     TX3 status buffer
[18:16]     RX0 status buffer
[22:20]     RX1 status buffer
[26:24]     RX2 status buffer
[30:28]     RX3 status buffer

Note :
Reset done : 1 is done
TX Status Buffer :
    TXBUFSTATUS[1]: TX buffer overflow or underflow status. When TXBUFSTATUS[1] is set High, it remains High until the TX buffer is reset.
    1: TX FIFO has overflow or underflow.
    0: No TX FIFO overflow or underflow error.
TXBUFSTATUS[0]: TX buffer fullness.
    1: TX FIFO is at least half full.
    0: TX FIFO is less than half full.

RX Status Buffer :
    000b: Nominal condition.
    001b: Number of bytes in the buffer are less than CLK_COR_MIN_LAT
    010b: Number of bytes in the buffer are greater than CLK_COR_MAX_LAT
    101b: RX elastic buffer underflow
    110b: RX elastic buffer overflow
*/

#define cSerdesStatus         0xF00020
#define cTX0ResetDoneMask     cBit0
#define cTX1ResetDoneMask     cBit1
#define cTX2ResetDoneMask     cBit2
#define cTX3ResetDoneMask     cBit3
#define cRX0ResetDoneMask     cBit4
#define cRX1ResetDoneMask     cBit5
#define cRX2ResetDoneMask     cBit6
#define cRX3ResetDoneMask     cBit7
#define cTX0BufferFullnessMask cBit8
#define cTX0BufferStatusMask cBit9

#define cTX1statusbufferMask  cBit11_10
#define cTX2statusbufferMask  cBit13_12
#define cTX3statusbufferMask  cBit15_14
#define cRX0statusbufferMask  cBit18_16
#define cRX0statusbufferShift 16
#define cRX1statusbufferMask  cBit22_20
#define cRX2statusbufferMask  cBit26_24
#define cRX3statusbufferMask  cBit30_28

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtSerdesControllerMethods m_AtSerdesControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool CanLoopback(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    uint32 startVersionSupportLoopback;
    AtDevice device;

    AtUnused(loopbackMode);

    if (!enable)
        return cAtTrue;

    device = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    startVersionSupportLoopback = ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x01, 0x05, 0x63);

    if (AtDeviceVersionNumber(device) >= startVersionSupportLoopback)
        return cAtTrue;

    return cAtFalse;
    }

static eAtRet LoopbackHwSet(AtSerdesController self, eAtLoopbackMode loopback)
    {
    uint32 fieldVal;
    uint32 regVal = AtSerdesControllerRead(self, cSerdesControl, cAtModuleSdh);

    if (loopback == cAtLoopbackModeLocal)
        fieldVal = cNearEndPMALoopback;
    else if (loopback == cAtLoopbackModeRemote)
        fieldVal = cFarEndPMALoopback;
    else if (loopback == cAtLoopbackModeRelease)
        fieldVal = cNormalOperation;
    else
        return cAtErrorInvlParm;

    mRegFieldSet(regVal, cSerdesLoopback, fieldVal);
    AtSerdesControllerWrite(self, cSerdesControl, regVal, cAtModuleSdh);
    return cAtOk;
    }

static eAtLoopbackMode LoopbackHwGet(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cSerdesControl, cAtModuleSdh);
    uint32 fieldVal = mRegField(regVal, cSerdesLoopback);

    if (fieldVal == cNearEndPMALoopback)
        return cAtLoopbackModeLocal;

    if (fieldVal == cFarEndPMALoopback)
        return cAtLoopbackModeRemote;

    return cAtLoopbackModeRelease;
    }

static eAtRet LoopbackEnable(AtSerdesController self, eAtLoopbackMode loopbackMode, eBool enable)
    {
    if (loopbackMode == cAtLoopbackModeRelease)
        return cAtErrorInvlParm;

    if (enable)
        return LoopbackHwSet(self, loopbackMode);

    /* If disable, release if loopbackMode is same as current loopback mode, otherwise do nothing */
    if (loopbackMode == LoopbackHwGet(self))
        return LoopbackHwSet(self, cAtLoopbackModeRelease);

    return cAtOk;
    }

static eBool LoopbackIsEnabled(AtSerdesController self, eAtLoopbackMode loopbackMode)
    {
    if (loopbackMode == LoopbackHwGet(self))
        return cAtTrue;

    return cAtFalse;
    }

static uint32 StartVersionSupportReset(AtDevice device)
    {
    if (ThaDeviceIsEp((ThaDevice)device))
        return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x06, 0x16, 0x18);

    return ThaVersionReaderPwVersionBuild(ThaDeviceVersionReader(device), 0x15, 0x06, 0x11, 0x91);
    }

static eBool NewResetIsSupported(AtSerdesController self)
    {
    AtDevice device  = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));
    if (AtDeviceVersionNumber(device) >= StartVersionSupportReset(device))
        return cAtTrue;

    return cAtFalse;
    }

static void Debug(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cSerdesStatus, cAtModuleSdh);
    uint8 bitVal;

    if (!NewResetIsSupported(self))
        {
        AtPrintc(cSevNormal,  "\r\nThis version does not support debug information\r\n");
        return;
        }

    AtPrintc(cSevNormal,  "\r\nTX Buffer status: \r\n");
    if (regVal & cTX0BufferFullnessMask)
        AtPrintc(cSevWarning, " - TX FIFO is at least half full\r\n");
    else
        AtPrintc(cSevNormal,  " - TX FIFO is less than half full\r\n");

    if (regVal & cTX0BufferStatusMask)
        AtPrintc(cSevCritical," - TX FIFO has overflow or underflow.\r\n");
    else
        AtPrintc(cSevNormal,  " - No TX FIFO overflow or underflow.\r\n");

    AtPrintc(cSevNormal,  "\r\nRX Buffer status: \r\n");
    bitVal = (uint8)mRegField(regVal, cRX0statusbuffer);
    switch (bitVal)
        {
        case 0:
            AtPrintc(cSevNormal,  " - Normal condition.\r\n");
            break;
        case 1:
            AtPrintc(cSevWarning, " - Number of bytes in the buffer are less than minimum threshold of elastic buffer\r\n");
            break;
        case 2:
            AtPrintc(cSevWarning, " - Number of bytes in the buffer are greater than maximum threshold of elastic buffer\r\n");
            break;
        case 5:
            AtPrintc(cSevWarning, " - RX elastic buffer underflow\r\n");
            break;
        case 6:
            AtPrintc(cSevWarning, " - RX elastic buffer overflow\r\n");
            break;
        default:
            AtPrintc(cSevWarning, " - Unknown error\r\n");
        }

    AtPrintc(cSevWarning, "\r\n");
    }

static void ResetTrigger(AtSerdesController self)
    {
    uint32 normalVal = AtSerdesControllerRead(self, cSerdesControl, cAtModuleSdh);
    AtSerdesControllerWrite(self, cSerdesControl, normalVal | cBit31, cAtModuleSdh);
    AtSerdesControllerWrite(self, cSerdesControl, normalVal, cAtModuleSdh);
    }

static eAtRet WaitForResetDone(AtSerdesController self)
    {
    const uint8 cResetTimeoutInMs = 100;
    tAtOsalCurTime curTime, startTime;
    uint32 elapse = 0;
    uint32 regVal = 0;

    AtOsalCurTimeGet(&startTime);
    while (elapse < cResetTimeoutInMs)
        {
        regVal = AtSerdesControllerRead(self, cSerdesStatus, cAtModuleSdh);
        if ((regVal & cTX0ResetDoneMask) && (regVal & cRX0ResetDoneMask))
            return cAtOk;

        /* Calculate elapse time and retry */
        AtOsalCurTimeGet(&curTime);
        elapse = mTimeIntervalInMsGet(startTime, curTime);
        }

    return cAtErrorSerdesResetTimeout;
    }

static eAtRet SerdesStatus(AtSerdesController self)
    {
    uint32 regVal = AtSerdesControllerRead(self, cSerdesStatus, cAtModuleSdh);
    uint8 bitVal;

    if (regVal & cTX0BufferStatusMask)
        return cAtErrorSerdesTxOverFlow | cAtErrorSerdesTxUnderFlow;

    bitVal = (uint8)mRegField(regVal, cRX0statusbuffer);
    if (bitVal == 0)
        return cAtOk;

    if (bitVal == 5)
        return cAtErrorSerdesRxUnderFlow;

    if (bitVal == 6)
        return cAtErrorSerdesRxOverFlow;

    return cAtErrorSerdesFail;
    }

static eAtRet OldReset(AtSerdesController self)
    {
    uint32 normalVal = AtSerdesControllerRead(self, cSerdesControl, cAtModuleSdh);
    AtSerdesControllerWrite(self, cSerdesControl, normalVal | cBit31, cAtModuleSdh);
    AtOsalUSleep(100000);
    AtSerdesControllerWrite(self, cSerdesControl, normalVal, cAtModuleSdh);
    AtOsalUSleep(100000);

    return cAtOk;
    }

static eAtRet Reset(AtSerdesController self)
    {
    return Tha60150011SdhLineSerdesControllerReset(self);
    }

static eBool EyeScanIsSupported(AtSerdesController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 EyeScanDrpBaseAddress(AtSerdesController self)
    {
    AtUnused(self);
    return 0xF40000;
    }

static void OverrideAtSerdesController(AtSerdesController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtSerdesControllerOverride, mMethodsGet(self), sizeof(m_AtSerdesControllerOverride));

        mMethodOverride(m_AtSerdesControllerOverride, CanLoopback);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackEnable);
        mMethodOverride(m_AtSerdesControllerOverride, LoopbackIsEnabled);
        mMethodOverride(m_AtSerdesControllerOverride, Debug);
        mMethodOverride(m_AtSerdesControllerOverride, Reset);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanIsSupported);
        mMethodOverride(m_AtSerdesControllerOverride, EyeScanDrpBaseAddress);
        }

    mMethodsSet(self, &m_AtSerdesControllerOverride);
    }

static void Override(AtSerdesController self)
    {
    OverrideAtSerdesController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011SdhLineSerdesController);
    }

AtSerdesController Tha60150011SdhLineSerdesControllerObjectInit(AtSerdesController self, AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaSdhLineSerdesControllerObjectInit(self, sdhLine, serdesId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSerdesController Tha60150011SdhLineSerdesControllerNew(AtSdhLine sdhLine, uint32 serdesId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSerdesController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011SdhLineSerdesControllerObjectInit(newController, sdhLine, serdesId);
    }

void Tha60150011SdhLineSerdesControllerTransmitDifferentialSet(AtSerdesController self, uint8 value)
    {
    uint32 regVal = AtSerdesControllerRead(self, cSerdesControl, cAtModuleSdh);
    mRegFieldSet(regVal, cSerdesTxDifferential, value);
    AtSerdesControllerWrite(self, cSerdesControl, regVal, cAtModuleSdh);
    }

eAtRet Tha60150011SdhLineSerdesControllerReset(AtSerdesController self)
    {
    AtDevice device  = AtChannelDeviceGet(AtSerdesControllerPhysicalPortGet(self));

    /* Nothing to do when simulate */
    if (AtDeviceIsSimulated(device))
       return cAtOk;

    if (!NewResetIsSupported(self))
       return OldReset(self);

    ResetTrigger(self);
    if (WaitForResetDone(self) != cAtOk)
        return cAtErrorSerdesResetTimeout;

    return SerdesStatus(self);
    }

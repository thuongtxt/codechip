/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60150011CdrDebuggerInternal.h
 * 
 * Created Date: Sep 10, 2015
 *
 * Description : 60150011 CDR debugger internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60150011_CDR_DEBUGGER_THA60150011CDRDEBUGGERINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60150011_CDR_DEBUGGER_THA60150011CDRDEBUGGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/cdr/debugger/ThaCdrDebuggerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011CdrDebugger * Tha60150011CdrDebugger;
typedef struct tTha60150011CdrDebugger
    {
    tThaCdrDebugger super;
    AtList pwHeaderContentList;
    }tTha60150011CdrDebugger;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrDebugger Tha60150011CdrDebuggerObjectInit(ThaCdrDebugger self);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60150011_CDR_DEBUGGER_THA60150011CDRDEBUGGERINTERNAL_H_ */


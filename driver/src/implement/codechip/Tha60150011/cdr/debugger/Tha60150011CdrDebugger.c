/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60150011CdrDebugger.c
 *
 * Created Date: Jun 13, 2015
 *
 * Description : CDR debugger of 60150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../../util/coder/AtCoderUtil.h"
#include "../../../../default/cdr/ThaModuleCdrStm.h"
#include "Tha60150011CdrDebuggerInternal.h"
#include "AtNumber.h"

/*--------------------------- Define -----------------------------------------*/
#define cCdrDebugBaseAddress          0x2a3000

#define cCdrDebugTriggerReg           0x0
#define cCdrPwCwCaptureTriggerMask    cBit20
#define cCdrPwCwCaptureTriggerShift   20
#define cCdrDebugTriggerMask          cBit16
#define cCdrDebugTriggerShift         16
#define cCdrDebugChannelIdMask        cBit15_0
#define cCdrDebugChannelIdShift       0

#define cCdrLockOutReg                0x1
#define cLockoutHoldoverMask          cBit7_0
#define cLockoutHoldoverShift         0
#define cLockoutRateoutMask           cBit15_8
#define cLockoutRateoutShift          8
#define cLockoutLatencyOvershutMask   cBit23_16
#define cLockoutLatencyOvershutShift  16
#define cLockoutLatencyUndershutMask  cBit31_24
#define cLockoutLatencyUndershutShift 24

#define cCdrLockGoodReg           0x2
#define cEnterFreezeMask          cBit7_0
#define cEnterFreezeShift         0
#define cEnterRapidMask           cBit15_8
#define cEnterRapidShift          8
#define cEnterLockMask            cBit23_16
#define cEnterLockShift           16
#define cLockIntervalMask         cBit31_24
#define cLockIntervalShift        24

#define cCdrErrorReg              0x3
#define cOutOfFrequencyMask       cBit7_0
#define cOutOfFrequencyShift      0
#define cPacketTimeoutMask        cBit15_8
#define cPacketTimeoutShift       8
#define cTimestampErrorMask       cBit23_16
#define cTimestampErrorShift      16
#define cSequenceErrorMask        cBit31_24
#define cSequenceErrorShift       24

#define cCdrClockStateReg         0x4
#define cCdrClockStateMask        cBit3_0
#define cCdrClockStateShift       0

#define cAcrIncomePacketPdvMin    0x8
#define cAcrIncomePacketPdvMax    0x9
#define cAcrIncomePacketPdvCur    0xa

#define cDcrRawNcoLockupMin       0x8
#define cDcrRawNcoLockupMax       0x9
#define cDcrRawNcoLockupCur       0xa

#define cAcrPrimaryFifoLengthMin     0xc
#define cAcrPrimaryFifoLengthMax     0xd
#define cAcrPrimaryFifoLengthCur     0xe

#define cDcrNcoLockupAfterFilterMin  0xc
#define cDcrNcoLockupAfterFilterMax  0xd
#define cDcrNcoLockupAfterFilterCur  0xe

#define cPrimaryNcoValMin         0x18
#define cPrimaryNcoValMax         0x19
#define cPrimaryNcoValCur         0x1a

#define cSecondaryFifoLengthMin   0x1c
#define cSecondaryFifoLengthMax   0x1d
#define cSecondaryFifoLengthCur   0x1e

#define cSecondaryNcoValMin       0x2c
#define cSecondaryNcoValMax       0x2d
#define cSecondaryNcoValCur       0x2e

#define cSecondaryAverageIn64SecIntervalMin 0x34
#define cSecondaryAverageIn64SecIntervalMax 0x35
#define cSecondaryAverageIn64SecIntervalCur 0x36

#define cHoldOverNcoValMin 0x38
#define cHoldOverNcoValMax 0x39
#define cHoldOverNcoValCur 0x3a

/* Basic NCO value corresponding to system clk 155Mhz */
#define cBasicNcoValE1   3817748707UL
#define cBasicNcoValDs1  3837632815UL
#define cBasicNcoValVt2  4175662648UL
#define cBasicNcoValVt15 4135894433UL
#define cBasicNcoValE3   2912117977UL
#define cBasicNcoValDs3  3790634015UL
#define cBasicNcoValTu3  4148547956UL
#define cBasicNcoValSts1 4246160849UL

#define c100NcoValPpbOffsetE1    209
#define c100NcoValPpbOffsetDS1   208
#define c100NcoValPpbOffsetVT2   194
#define c100NcoValPpbOffsetVT15  194
#define c100NcoValPpbOffsetE3    274
#define c100NcoValPpbOffsetDS3   209
#define c100NcoValPpbOffsetTU3   194
#define c100NcoValPpbOffsetSTS1  194

#define c1000NcoValPpbOffsetE1    262
#define c1000NcoValPpbOffsetDS1   260
#define c1000NcoValPpbOffsetVT2   239
#define c1000NcoValPpbOffsetVT15  241
#define c1000NcoValPpbOffsetE3    343
#define c1000NcoValPpbOffsetDS3   263
#define c1000NcoValPpbOffsetTU3   239
#define c1000NcoValPpbOffsetSTS1  239

#define cMaxNumberOfCwContent 256

#define cPwCwContentReg       0x2a3100
#define cPwCwSequenceMask     cBit15_0
#define cPwCwSequenceShift    0
#define cPwRtpSequenceMask    cBit31_16
#define cPwRtpSequenceShift   16
#define cPwRtpTimestampMask   cBit31_0 /* 63:32 */
#define cPwRtpTimestampShift  0
#define cPwRtpTimestampDw     1
#define cPwPacketErrorMask    cBit0 /* 64 */
#define cPwPacketErrorShift   0
#define cPwPacketErrorDw      2
#define cPwHoEnableMask       cBit1
#define cPwHoEnableShift      1
#define cPwErrorMask          cBit0
#define cPwErrorShift         0

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60150011CdrDebugger)self)
#define mTimingBase(self, controller) ThaCdrDebuggerTimingBaseAddress(self, controller)

/* Function to calculate ppm will return n * 100ppm
   for example returned value 155 corresponding to 1.55 PPM */
#define mPpmUnit 100

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tThaPwHeaderContent
    {
    uint16 sequenceNumber;
    uint16 rtpSequenceNumber;
    uint32 rtpTimeStamp;
    eBool  hoIsEnabled;
    eBool  error;
    }tThaPwHeaderContent;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrDebuggerMethods m_ThaCdrDebuggerOverride;
static tAtObjectMethods       m_AtObjectOverride;

static const tAtObjectMethods * m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TimingBaseAddress(ThaCdrDebugger self, ThaCdrController controller)
    {
    ThaModuleCdr module = ThaCdrControllerModuleGet(controller);
    AtChannel channel = ThaCdrControllerChannelGet(controller);
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel((ThaSdhChannelCdrController)controller, channel);

    AtUnused(self);
    return cCdrDebugBaseAddress + ThaModuleCdrStmSliceOffset(module, sdhChannel, ThaCdrControllerChannelSliceGet(controller, channel));
    }

static uint32 PwHeaderBaseAddress(ThaCdrDebugger self, ThaCdrController controller)
    {
    ThaModuleCdr module = ThaCdrControllerModuleGet(controller);
    AtChannel channel = ThaCdrControllerChannelGet(controller);
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel((ThaSdhChannelCdrController)controller, channel);

    AtUnused(self);
    return cPwCwContentReg + ThaModuleCdrStmSliceOffset(module, sdhChannel, ThaCdrControllerChannelSliceGet(controller, channel));
    }

static int32 PpmCalculate(ThaCdrDebugger self, uint32 ncoValue, uint32 baseNco, uint32 _100NcoOffsetPpb)
    {
    uint32 unsignOffset, intergerPart, remaining;
    uint32 absPpm;

    AtUnused(self);

    /* Original formular: N in ppm = (((ncoValue - baseNco) / 100 ) * _100NcoOffsetPpb) / 1000
     * Ppm will be calculated in 2 decimal digits precision, for example returned value 155 corresponding to 1.55 PPM */
    unsignOffset = (ncoValue > baseNco) ? (ncoValue - baseNco) : (baseNco - ncoValue);
    intergerPart = unsignOffset / 1000;
    remaining    = unsignOffset % 1000;
    absPpm = (intergerPart * _100NcoOffsetPpb) + (remaining *_100NcoOffsetPpb / 1000);

    return (ncoValue > baseNco) ? (int32)absPpm : (int32)(0 - absPpm);
    }

static int32 E1PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpmCalculate(self, ncoValue, cBasicNcoValE1, c100NcoValPpbOffsetE1);
    }

static int32 Ds1PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpmCalculate(self, ncoValue, cBasicNcoValDs1, c100NcoValPpbOffsetDS1);
    }

static int32 Vc12PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpmCalculate(self, ncoValue, cBasicNcoValVt2, c100NcoValPpbOffsetVT2);
    }

static int32 Vc11PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpmCalculate(self, ncoValue, cBasicNcoValVt15, c100NcoValPpbOffsetVT15);
    }

static int32 E3PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpmCalculate(self, ncoValue, cBasicNcoValE3, c100NcoValPpbOffsetE3);
    }

static int32 Ds3PpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpmCalculate(self, ncoValue, cBasicNcoValDs3, c100NcoValPpbOffsetDS3);
    }

static int32 Tu3VcPpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpmCalculate(self, ncoValue, cBasicNcoValTu3, c100NcoValPpbOffsetTU3);
    }

static int32 HoVcPpmCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpmCalculate(self, ncoValue, cBasicNcoValSts1, c100NcoValPpbOffsetSTS1);
    }

static int32 PpbCalculate(ThaCdrDebugger self, uint32 ncoValue, uint32 baseNco, uint32 _1000NcoOffsetPpb)
    {
    uint32 unsignOffset;
    uint32 absPpm;

    AtUnused(self);

    unsignOffset = (ncoValue > baseNco) ? (ncoValue - baseNco) : (baseNco - ncoValue);

    absPpm = (unsignOffset * _1000NcoOffsetPpb) / 1000;

    return (ncoValue > baseNco) ? (int32)absPpm : (int32)(0 - absPpm);
    }

static int32 E1PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpbCalculate(self, ncoValue, cBasicNcoValE1, c1000NcoValPpbOffsetE1);
    }

static int32 Ds1PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpbCalculate(self, ncoValue, cBasicNcoValDs1, c1000NcoValPpbOffsetDS1);
    }

static int32 Vc12PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpbCalculate(self, ncoValue, cBasicNcoValVt2, c1000NcoValPpbOffsetVT2);
    }

static int32 Vc11PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpbCalculate(self, ncoValue, cBasicNcoValVt15, c1000NcoValPpbOffsetVT15);
    }

static int32 E3PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpbCalculate(self, ncoValue, cBasicNcoValE3, c1000NcoValPpbOffsetE3);
    }

static int32 Ds3PpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpmCalculate(self, ncoValue, cBasicNcoValDs3, c1000NcoValPpbOffsetDS3);
    }

static int32 Tu3VcPpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpbCalculate(self, ncoValue, cBasicNcoValTu3, c1000NcoValPpbOffsetTU3);
    }

static int32 HoVcPpbCalculate(ThaCdrDebugger self, uint32 ncoValue)
    {
    return PpbCalculate(self, ncoValue, cBasicNcoValSts1, c1000NcoValPpbOffsetSTS1);
    }

static uint32 HoldOverPerChannelAddress(ThaCdrDebugger self, ThaCdrController controller)
    {
    ThaModuleCdr module = ThaCdrControllerModuleGet(controller);
    AtUnused(self);
    return ThaModuleCdrAdjustHoldoverValueStatusRegister(module) +
           ThaCdrControllerTimingStatusOffset(controller);
    }

static void DebugTrigger(ThaCdrDebugger self, ThaCdrController controller)
    {
    uint32 regAddress = cCdrDebugTriggerReg + mTimingBase(self, controller);
    uint32 regVal = ThaCdrControllerRead(controller, regAddress, cThaModuleCdr);
    uint32 newChannel = ThaCdrControllerChannelIdFlat(controller, ThaCdrControllerChannelGet(controller));
    const uint8 cDelayTimeInMs = 100;

    mRegFieldSet(regVal, cCdrDebugChannelId, newChannel);

    /* Write 1 then 0 to trigger bit */
    ThaCdrControllerWrite(controller, regAddress, regVal | cCdrDebugTriggerMask, cThaModuleCdr);
    ThaCdrControllerWrite(controller, regAddress, regVal, cThaModuleCdr);

    /* Delay for debug engine to update */
    AtOsalUSleep(cDelayTimeInMs * 1000UL);
    }

static uint32 Abs(int32 signedValue)
    {
    return AtStdAbs(AtStdSharedStdGet(), signedValue);
    }

static void NcoValuePrint(ThaCdrController controller, uint32 address)
    {
    uint32 regVal = ThaCdrControllerRead(controller, address, cThaModuleCdr);
    int32 _100PpmValue = ThaCdrControllerPpmCalculate(controller, regVal);
    uint32 ppmInterger = Abs(_100PpmValue / 100);
    uint32 ppmDecimal  = Abs(_100PpmValue % 100);

    if (_100PpmValue < 0)
        AtPrintc(cSevNormal, "-%s.%02u ppm (0x%08X = %u)\r\n", AtNumberDigitGroupingString(ppmInterger), ppmDecimal, address, regVal);
    else
        AtPrintc(cSevNormal, "%s.%02u ppm (0x%08X = %u)\r\n", AtNumberDigitGroupingString(ppmInterger), ppmDecimal, address, regVal);
    }

static void PpbNcoValuePrint(ThaCdrController controller, uint32 address)
    {
    uint32 regVal = ThaCdrControllerRead(controller, address, cThaModuleCdr);
    int32 ppbValue = ThaCdrControllerPpbCalculate(controller, regVal);

    if (ppbValue < 0)
        AtPrintc(cSevNormal, "-%s ppb (0x%08X = %u)\r\n", AtNumberDigitGroupingString(Abs(ppbValue)), address, regVal);
    else
        AtPrintc(cSevNormal, "%s ppb (0x%08X = %u)\r\n", AtNumberDigitGroupingString(Abs(ppbValue)), address, regVal);
    }

static void ValuePrint(ThaCdrController controller, uint32 address)
    {
    uint32 regVal = ThaCdrControllerRead(controller, address, cThaModuleCdr);
    AtPrintc(cSevNormal, "%u (0x%08X = %u)\r\n", regVal, address, regVal);
    }

static void TitlePrint(const char* title)
    {
    AtPrintc(cSevNormal, "%-30s: ", title);
    }

static void OneCounterPrint(uint32 color, const char* title, uint32 value)
    {
    TitlePrint(title);
    AtPrintc(color, "%u\r\n", value);
    }

static void GoodCountersPrint(ThaCdrDebugger self, ThaCdrController controller)
    {
    uint32 address = cCdrLockGoodReg + mTimingBase(self, controller);
    uint32 regVal = ThaCdrControllerRead(controller, address, cThaModuleCdr);

    TitlePrint("* Good counters");
    AtPrintc(cSevNormal, "(0x%08X = 0x%08X)\r\n", address, regVal);
    OneCounterPrint(cSevInfo, "  - Enter freeze", mRegField(regVal, cEnterFreeze));
    OneCounterPrint(cSevInfo, "  - Enter rapid", mRegField(regVal, cEnterRapid));
    OneCounterPrint(cSevInfo, "  - Enter lock", mRegField(regVal, cEnterLock));
    OneCounterPrint(cSevInfo, "  - Lock 60s interval", mRegField(regVal, cLockInterval));
    }

static void BadCountersPrint(ThaCdrDebugger self, ThaCdrController controller)
    {
    uint32 address = cCdrLockOutReg + mTimingBase(self, controller);
    uint32 regVal = ThaCdrControllerRead(controller, address, cThaModuleCdr);

    TitlePrint("* Bad counters");
    AtPrintc(cSevNormal, "(0x%08X = 0x%08X)\r\n", address, regVal);
    OneCounterPrint(cSevWarning, "  - Hold-over", mRegField(regVal, cLockoutHoldover));
    OneCounterPrint(cSevWarning, "  - Out-rate", mRegField(regVal, cLockoutRateout));
    OneCounterPrint(cSevWarning, "  - Latency over-shut", mRegField(regVal, cLockoutLatencyOvershut));
    OneCounterPrint(cSevWarning, "  - Latency under-shut", mRegField(regVal, cLockoutLatencyUndershut));
    }

static void ErrorCountersPrint(ThaCdrDebugger self, ThaCdrController controller)
    {
    uint32 address = cCdrErrorReg + mTimingBase(self, controller);
    uint32 regVal = ThaCdrControllerRead(controller, address, cThaModuleCdr);

    TitlePrint("* Error counters");
    AtPrintc(cSevNormal, "(0x%08X = 0x%08X)\r\n", address, regVal);
    OneCounterPrint(cSevCritical, "  - Out of sequence", mRegField(regVal, cOutOfFrequency));
    OneCounterPrint(cSevCritical, "  - Packet timeout", mRegField(regVal, cPacketTimeout));
    OneCounterPrint(cSevCritical, "  - Time stamp error", mRegField(regVal, cTimestampError));
    OneCounterPrint(cSevCritical, "  - Sequence error", mRegField(regVal, cSequenceError));
    }

static void NcoPrint(ThaCdrDebugger self, ThaCdrController controller)
    {
    uint32 baseAddress = mTimingBase(self, controller);

    AtPrintc(cSevNormal, "* Primary NCO:\r\n");
    TitlePrint("  - Minimum"); NcoValuePrint(controller, cPrimaryNcoValMin + baseAddress);
    TitlePrint("  - Maximum"); NcoValuePrint(controller, cPrimaryNcoValMax + baseAddress);
    TitlePrint("  - Current"); NcoValuePrint(controller, cPrimaryNcoValCur + baseAddress);

    AtPrintc(cSevNormal, "* Secondary NCO:\r\n");
    TitlePrint("  - Minimum"); NcoValuePrint(controller, cSecondaryNcoValMin + baseAddress);
    TitlePrint("  - Maximum"); NcoValuePrint(controller, cSecondaryNcoValMax + baseAddress);
    TitlePrint("  - Current"); NcoValuePrint(controller, cSecondaryNcoValCur + baseAddress);

    AtPrintc(cSevNormal, "* Hold over NCO:\r\n");
    TitlePrint("  - Minimum"); NcoValuePrint(controller, cHoldOverNcoValMin + baseAddress);
    TitlePrint("  - Maximum"); NcoValuePrint(controller, cHoldOverNcoValMax + baseAddress);
    TitlePrint("  - Current"); NcoValuePrint(controller, cHoldOverNcoValCur + baseAddress);

    AtPrintc(cSevNormal, "* Secondary average in interval 60 sec:\r\n");
    TitlePrint("  - Minimum"); NcoValuePrint(controller, cSecondaryAverageIn64SecIntervalMin + baseAddress);
    TitlePrint("  - Maximum"); NcoValuePrint(controller, cSecondaryAverageIn64SecIntervalMax + baseAddress);
    TitlePrint("  - Current"); NcoValuePrint(controller, cSecondaryAverageIn64SecIntervalCur + baseAddress);

    if (ThaCdrControllerTimingModeGet(controller) == cAtTimingModeDcr)
        {
        AtPrintc(cSevNormal, "* Raw NCO lock-up:\r\n");
        TitlePrint("  - Minimum"); NcoValuePrint(controller, cDcrRawNcoLockupMin + baseAddress);
        TitlePrint("  - Maximum"); NcoValuePrint(controller, cDcrRawNcoLockupMax + baseAddress);
        TitlePrint("  - Current"); NcoValuePrint(controller, cDcrRawNcoLockupCur + baseAddress);

        AtPrintc(cSevNormal, "* Filtered NCO lock-up:\r\n");
        TitlePrint("  - Minimum"); NcoValuePrint(controller, cDcrNcoLockupAfterFilterMin + baseAddress);
        TitlePrint("  - Maximum"); NcoValuePrint(controller, cDcrNcoLockupAfterFilterMax + baseAddress);
        TitlePrint("  - Current"); NcoValuePrint(controller, cDcrNcoLockupAfterFilterCur + baseAddress);
        }
    }

static void StatePrint(ThaCdrDebugger self, ThaCdrController controller)
    {
    uint32 regVal = ThaCdrControllerRead(controller, cCdrClockStateReg + mTimingBase(self, controller), cThaModuleCdr);
    uint8 hwState = (uint8)mRegField(regVal, cCdrClockState);
    TitlePrint("* Main clock state");
    AtPrintc(cSevNormal, "%s (%d)\r\n", mMethodsGet(self)->HwClockMainState2String(self, hwState), hwState);
    }

static void DebugWithTrigger(ThaCdrDebugger self, ThaCdrController controller)
    {
    uint32 baseAddress = mTimingBase(self, controller);

    /* Trigger channel */
    DebugTrigger(self, controller);

    StatePrint(self, controller);
    NcoPrint(self, controller);
    GoodCountersPrint(self, controller);
    BadCountersPrint(self, controller);
    ErrorCountersPrint(self, controller);

    if (ThaCdrControllerTimingModeGet(controller) == cAtTimingModeAcr)
        {
        AtPrintc(cSevNormal, "* Income packet PDV in second:\r\n");
        TitlePrint("  - Minimum"); ValuePrint(controller, cAcrIncomePacketPdvMin + baseAddress);
        TitlePrint("  - Maximum"); ValuePrint(controller, cAcrIncomePacketPdvMax + baseAddress);
        TitlePrint("  - Current"); ValuePrint(controller, cAcrIncomePacketPdvCur + baseAddress);

        AtPrintc(cSevNormal, "* Primary FIFO length after filter:\r\n");
        TitlePrint("  - Minimum"); ValuePrint(controller, cAcrPrimaryFifoLengthMin + baseAddress);
        TitlePrint("  - Maximum"); ValuePrint(controller, cAcrPrimaryFifoLengthMax + baseAddress);
        TitlePrint("  - Current"); ValuePrint(controller, cAcrPrimaryFifoLengthCur + baseAddress);
        }

    AtPrintc(cSevNormal, "* Secondary FIFO length:\r\n");
    TitlePrint("  - Minimum"); ValuePrint(controller, cSecondaryFifoLengthMin + baseAddress);
    TitlePrint("  - Maximum"); ValuePrint(controller, cSecondaryFifoLengthMax + baseAddress);
    TitlePrint("  - Current"); ValuePrint(controller, cSecondaryFifoLengthCur + baseAddress);
    }

static void DebugWithHoldOverPerChannel(ThaCdrDebugger self, ThaCdrController controller)
    {
    uint32 address = ThaCdrDebuggerHoldOverPerChannelAddress(self, controller);
    PpbNcoValuePrint(controller, address);
    }

static eBool IsHoldOverPerChannelIsSupported(ThaCdrDebugger self)
    {
    return ThaCdrDebuggerHoldOverPerChannelIsSupported(self);
    }

static void Debug(ThaCdrDebugger self, ThaCdrController controller)
    {
    eAtTimingMode timingMode = ThaCdrControllerTimingModeGet(controller);

    AtPrintc(cSevNormal, "* CDR engine ID: %d\r\n", ThaCdrControllerIdGet(controller));

    if ((timingMode != cAtTimingModeAcr) && (timingMode != cAtTimingModeDcr))
        return;

    if (!IsHoldOverPerChannelIsSupported(self))
        DebugWithTrigger(self, controller);
    else
        DebugWithHoldOverPerChannel(self, controller);
    }

static AtList PwCwContentListGet(ThaCdrDebugger self)
    {
    if (mThis(self)->pwHeaderContentList == NULL)
        mThis(self)->pwHeaderContentList = AtListCreate(cMaxNumberOfCwContent);

    return mThis(self)->pwHeaderContentList;
    }

static tThaPwHeaderContent* ContentAtIndexGet(AtList contentList, uint32 index)
    {
    tThaPwHeaderContent* content = (tThaPwHeaderContent*)AtListObjectGet(contentList, index);
    if (content == NULL)
        {
        content = AtOsalMemAlloc(sizeof(tThaPwHeaderContent));
        if (AtListObjectAdd(contentList, (AtObject)content) != cAtOk)
            {
            AtOsalMemFree(content);
            return NULL;
            }
        }

    AtOsalMemInit(content, cBit7_0, sizeof(tThaPwHeaderContent));
    return content;
    }

static void DebugPwHeaderTrigger(ThaCdrDebugger self, ThaCdrController controller, AtPw pw)
    {
    uint32 regAddress = cCdrDebugTriggerReg + mTimingBase(self, controller);
    uint32 regVal = ThaCdrControllerRead(controller, regAddress, cThaModuleCdr);
    uint32 newChannel = mMethodsGet(self)->DebugPwHeaderId(self, controller, pw);
    const uint8 cDelayTimeInMs = 100;

    mRegFieldSet(regVal, cCdrDebugChannelId, newChannel);

    /* Write 1 then 0 to trigger bit */
    ThaCdrControllerWrite(controller, regAddress, regVal | cCdrDebugTriggerMask, cThaModuleCdr);
    ThaCdrControllerWrite(controller, regAddress, regVal, cThaModuleCdr);

    /* Delay for debug engine to update */
    AtOsalUSleep(cDelayTimeInMs * 1000UL);
    }

static AtList PwHeaderCapture(ThaCdrDebugger self, ThaCdrController controller, AtPw pw)
    {
    uint16 content_i;
    AtList contentList = PwCwContentListGet(self);
    uint32 regVal[cThaLongRegMaxSize], address;
    tThaPwHeaderContent* content;
    uint32 baseAddress = ThaCdrDebuggerPwHeaderBaseAddress(self, controller);

    DebugPwHeaderTrigger(self, controller, pw);
    for (content_i = 0; content_i < cMaxNumberOfCwContent; content_i++)
        {
        address = (uint32)(baseAddress + content_i);
        ThaCdrControllerLongRead(controller, address, regVal, cThaLongRegMaxSize, cThaModuleCdr);
        content = ContentAtIndexGet(contentList, content_i);

        if ((content == NULL) || (regVal[cPwPacketErrorDw] & cPwPacketErrorMask))
            continue;

        content->sequenceNumber    = (uint16)mRegField(regVal[0], cPwCwSequence);
        content->rtpSequenceNumber = (uint16)mRegField(regVal[0], cPwRtpSequence);
        content->rtpTimeStamp      = regVal[cPwRtpTimestampDw];
        content->hoIsEnabled       = (eBool)mRegField(regVal[2], cPwHoEnable);
        content->error             = (eBool)mRegField(regVal[2], cPwError);
        }

    return contentList;
    }

static void PwHeaderContentPrint(tThaPwHeaderContent* content, uint16 content_i)
    {
    AtPrintc(cSevNormal, "%3hu:    %04hX          %04hX           %08X\r\n", content_i, content->sequenceNumber, content->rtpSequenceNumber, content->rtpTimeStamp);
    }

static void AllPwHeaderContentPrint(AtList contentList)
    {
    uint16 content_i;
    tThaPwHeaderContent* content;
    AtPrintc(cSevNormal, "* PW Header in hex:\r\n");
    AtPrintc(cSevInfo,   "Packet  CW Sequence   RTP sequence   RTP timestamp\r\n");
    for (content_i = 0; content_i < AtListLengthGet(contentList); content_i++)
        {
        content = (tThaPwHeaderContent*)AtListObjectGet(contentList, content_i);
        PwHeaderContentPrint(content, content_i);
        }
    }

static void PwHeaderDebug(ThaCdrDebugger self, ThaCdrController controller, AtPw pw)
    {
    AtList pwCwInfoList = PwHeaderCapture(self, controller, pw);
    AllPwHeaderContentPrint(pwCwInfoList);
    }

static void DeleteAllPwCwContentsList(Tha60150011CdrDebugger self)
    {
    tThaPwHeaderContent* content;
    while ((content = (tThaPwHeaderContent*)AtListObjectRemoveAtIndex(mThis(self)->pwHeaderContentList, 0)) != NULL)
        AtOsalMemFree(content);

    AtObjectDelete((AtObject)(self->pwHeaderContentList));
    self->pwHeaderContentList = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteAllPwCwContentsList(mThis(self));
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60150011CdrDebugger *object = (tTha60150011CdrDebugger *)self;

    m_AtObjectMethods->Serialize(self, encoder);
    mEncodeList(pwHeaderContentList);
    }

static void OverrideThaCdrDebugger(ThaCdrDebugger self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrDebuggerOverride, mMethodsGet(self), sizeof(m_ThaCdrDebuggerOverride));

        mMethodOverride(m_ThaCdrDebuggerOverride, Debug);
        mMethodOverride(m_ThaCdrDebuggerOverride, E1PpmCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, Ds1PpmCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, E3PpmCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, Ds3PpmCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, Vc12PpmCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, Vc11PpmCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, Tu3VcPpmCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, HoVcPpmCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, PwHeaderDebug);
        mMethodOverride(m_ThaCdrDebuggerOverride, TimingBaseAddress);
        mMethodOverride(m_ThaCdrDebuggerOverride, PwHeaderBaseAddress);
        mMethodOverride(m_ThaCdrDebuggerOverride, E1PpbCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, Ds1PpbCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, E3PpbCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, Ds3PpbCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, Vc12PpbCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, Vc11PpbCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, Tu3VcPpbCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, HoVcPpbCalculate);
        mMethodOverride(m_ThaCdrDebuggerOverride, HoldOverPerChannelAddress);
        }

    mMethodsSet(self, &m_ThaCdrDebuggerOverride);
    }

static void OverrideAtObject(ThaCdrDebugger self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void Override(ThaCdrDebugger self)
    {
    OverrideThaCdrDebugger(self);
    OverrideAtObject(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011CdrDebugger);
    }

ThaCdrDebugger Tha60150011CdrDebuggerObjectInit(ThaCdrDebugger self)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaCdrDebuggerObjectInit(self) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrDebugger Tha60150011CdrDebuggerNew(void)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrDebugger newDebugger = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDebugger == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011CdrDebuggerObjectInit(newDebugger);
    }

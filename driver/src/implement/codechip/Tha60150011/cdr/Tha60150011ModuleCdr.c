/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60150011ModuleCdr.c
 *
 * Created Date: Jun 20, 2014
 *
 * Description : CDR module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/pdh/AtPdhDe3Internal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "../../../default/cdr/ThaModuleCdrStm.h"
#include "../../../default/cdr/ThaModuleCdrStmReg.h"
#include "../../../default/cdr/ThaModuleCdrReg.h"
#include "Tha60150011ModuleCdrInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegCDRStsTimeCtrl 0x0280800
#define cVC3CDRChidMask    cBit29_20
#define cVC3CDRChidShift   20
#define cRegCDRVtTimeCtrl  0x0280C00
#define cVTCDRChidMask     cBit21_12
#define cVTCDRChidShift    12

#define cRtpTimestamp155MhzOr77MhzSelect 0x0290002

#define mThis(self) ((Tha60150011ModuleCdr)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60150011ModuleCdrMethods m_methods;

/* Override */
static tThaModuleCdrStmMethods m_ThaModuleCdrStmOverride;
static tThaModuleCdrMethods    m_ThaModuleCdrOverride;

/* Save super implementation */
static const tThaModuleCdrStmMethods *m_ThaModuleCdrStmMethods = NULL;
static const tThaModuleCdrMethods    *m_ThaModuleCdrMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 MaxNumSts(ThaModuleCdr self)
    {
    AtUnused(self);
    return 48;
    }

static uint32 StsTimingControlReg(ThaModuleCdrStm self, AtSdhChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return cRegCDRStsTimeCtrl;
    }

static uint32 StsCdrChannelIdMask(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cVC3CDRChidMask;
    }

static uint8 StsCdrChannelIdShift(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cVC3CDRChidShift;
    }

static uint32 VtTimingControlReg(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cRegCDRVtTimeCtrl;
    }

static uint32 VtCdrChannelIdMask(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cVTCDRChidMask;
    }

static uint8 VtCdrChannelIdShift(ThaModuleCdrStm self)
    {
    AtUnused(self);
    return cVTCDRChidShift;
    }

static uint32 SliceOffset(ThaModuleCdr self, AtSdhChannel channel, uint8 slice)
    {
    AtUnused(self);
    AtUnused(channel);
    return slice * 0x40000UL;
    }

static uint32 LineModeCtrlOffset(Tha60150011ModuleCdr self, uint32 sliceOffset, uint8 hwStsId, uint8 partId)
    {
    uint8 idOfGroup12Sts;

    /* In one slice:
     * 0,2,4,...,22  -> group = 0
     * 1,3,5,...,23 -> group = 1
     */
    idOfGroup12Sts = (hwStsId % 2);
    return (sliceOffset + idOfGroup12Sts + ThaModuleCdrPartOffset((ThaModuleCdr)self, partId));
    }

static uint32 SdhChannelLineModeCtrlOffset(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwStsId)
    {
    return mMethodsGet(mThis(self))->LineModeCtrlOffset(mThis(self), ThaModuleCdrStmSliceOffset(self, channel, slice), hwStsId, ThaModuleSdhPartOfChannel(channel));
    }

static uint32 De3LineModeCtrlOffset(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwStsId)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPdhChannelVcInternalGet(channel);

    if (sdhChannel)
        return SdhChannelLineModeCtrlOffset(self, sdhChannel, slice, hwStsId);

    return mMethodsGet(mThis(self))->LineModeCtrlOffset(mThis(self), ThaModuleCdrStmSliceOffset(self, NULL, slice), hwStsId, 0);
    }

static uint32 De2LineModeCtrlOffset(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwStsId)
    {
    AtUnused(channel);
    return mMethodsGet(mThis(self))->LineModeCtrlOffset(mThis(self), ThaModuleCdrStmSliceOffset(self, NULL, slice), hwStsId, 0);
    }

static uint8 LocalHwStsInGroup12(Tha60150011ModuleCdr self, uint8 hwSts)
    {
    AtUnused(self);
    return hwSts / 2;
    }

static eAtRet Vc3CepModeEnable(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, eBool enable)
    {
    uint32 longRegValue[cThaLongRegMaxSize];
    uint32 regAddr;
    uint8 hwStsLocalInGroup12;

    /* There are 2 line mode control registers in each slice.
     * One for 12 odd STSs and one for 12 even STSs
     */
    regAddr = mMethodsGet(self)->CDRLineModeControlRegister(self) +
              SdhChannelLineModeCtrlOffset(self, channel, slice, hwSts);
    hwStsLocalInGroup12 = mMethodsGet(mThis(self))->LocalHwStsInGroup12(mThis(self), hwSts);

    mChannelHwLongRead(channel, regAddr, longRegValue, cThaLongRegMaxSize, cThaModuleCdr);
    mFieldIns(&longRegValue[cThaStsVC3MdDwIndex],
              cThaStsVC3MdMask(hwStsLocalInGroup12),
              cThaStsVC3MdShift(hwStsLocalInGroup12),
              mBoolToBin(enable));
    mChannelHwLongWrite(channel, regAddr, longRegValue, cThaLongRegMaxSize, cThaModuleCdr);

    return cAtOk;
    }

static uint8 StsMode(ThaModuleCdr self, AtSdhChannel channel, eThaOcnVc3PldType payloadType)
    {
    AtPdhDe3 pdhChannel = (AtPdhDe3)AtSdhChannelMapChannelGet(channel);
    AtUnused(self);

    if (pdhChannel != NULL)
        {
        if (ThaSdhAuVcMapDe3LiuIsEnabled((ThaSdhAuVc)channel))
            return 1;

        return AtPdhDe3IsChannelized(pdhChannel) ? 0 : 1;
        }

    if (payloadType == cThaOcnVc3Pld7Tug2)
        return 0;

    return 1;
    }

static eAtRet HwStsPldSet(Tha60150011ModuleCdr self,
                     AtChannel channel,
                     uint8 hwStsMd,
                     uint8 hwSts,
                     uint8 hwPayloadType,
                     uint32 lineModeCtrlOffset)
    {
    uint32 longRegValue[cThaLongRegMaxSize];
    uint32 regAddr;
    uint8 hwStsLocalInGroup12;
    ThaModuleCdr module = (ThaModuleCdr)self;

    /* There are 2 line mode control registers in each slice.
     * One for 12 odd STSs and one for 12 even STSs
     */
    regAddr = mMethodsGet(module)->CDRLineModeControlRegister(module) +
              lineModeCtrlOffset;
    hwStsLocalInGroup12 = mMethodsGet(self)->LocalHwStsInGroup12(self, hwSts);

    mChannelHwLongRead(channel, regAddr, longRegValue, cThaLongRegMaxSize, cThaModuleCdr);
    mFieldIns(&longRegValue[hwStsLocalInGroup12 / 4],
              cThaStsStsMdMask(hwStsLocalInGroup12),
              cThaStsStsMdShift(hwStsLocalInGroup12),
              hwStsMd);
    mFieldIns(&longRegValue[3],
              cThaStsDE3ModMask(hwStsLocalInGroup12),
              cThaStsDE3ModShift(hwStsLocalInGroup12),
              hwPayloadType);
    mChannelHwLongWrite(channel, regAddr, longRegValue, cThaLongRegMaxSize, cThaModuleCdr);

    return cAtOk;
    }

static eAtRet LineModeHwSet(Tha60150011ModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, uint8 hwStsMode, uint8 hwPayloadType)
    {
    ThaModuleCdr module = (ThaModuleCdr)self;
    uint32 lineModeCtrlOffset = SdhChannelLineModeCtrlOffset(module, channel, slice, hwSts);
    return mMethodsGet(self)->HwStsPldSet(self, (AtChannel)channel, hwStsMode, hwSts, hwPayloadType, lineModeCtrlOffset);
    }

static eAtRet LineModePayloadSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, eThaOcnVc3PldType payloadType)
    {
    uint8 hwPayloadType = (payloadType == cThaOcnVc3PldDs3) ? 0x1 : 0x0;
    uint8 hwStsMode = StsMode(self, channel, payloadType);
    AtPdhChannel pdhChannel = (AtPdhChannel)AtSdhChannelMapChannelGet(channel);
    if (pdhChannel != NULL)
        hwPayloadType = AtPdhDe3IsE3((AtPdhDe3)pdhChannel) ? 0 : 1;
    return mMethodsGet(mThis(self))->LineModeHwSet(mThis(self), channel, slice, hwSts, hwStsMode, hwPayloadType);
    }

static uint8 HwVtType(eThaOcnVtgTug2PldType payloadType)
    {
    if ((payloadType == cThaOcnVtgTug2PldVt15) ||
        (payloadType == cThaOcnVtgTug2PldDs1))
        return 1;
    return 0;
    }

static eAtRet HwVtgPldSet(Tha60150011ModuleCdr self, AtChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType, uint32 lineModeCtrlOffset)
    {
    uint32 longReg[cThaLongRegMaxSize];
    uint32 regAddr;
    uint8 hwStsLocalInGroup12 = mMethodsGet(mThis(self))->LocalHwStsInGroup12(mThis(self), hwSts);

    AtUnused(slice);

    regAddr = mMethodsGet((ThaModuleCdr)self)->CDRLineModeControlRegister((ThaModuleCdr)self) + lineModeCtrlOffset;
    mChannelHwLongRead(channel, regAddr, longReg, cThaLongRegMaxSize, cThaModuleCdr);
    mFieldIns(&longReg[hwStsLocalInGroup12 / 4],
              cThaStsVtTypeMask(hwStsLocalInGroup12, vtgId),
              cThaStsVtTypeShift(hwStsLocalInGroup12, vtgId),
              HwVtType(payloadType));
    mChannelHwLongWrite(channel, regAddr, longReg, cThaLongRegMaxSize, cThaModuleCdr);

    return cAtOk;
    }

static eAtRet AllDe2sDefaultSet(ThaModuleCdr self, AtPdhChannel de3, uint8 slice, uint8 hwSts)
    {
    eThaOcnVtgTug2PldType de2PayloadType = cThaOcnVtgTug2PldE1;
    uint8 de2Id;
    eAtRet ret = cAtOk;
    uint32 lineModeCtrlOffset = De3LineModeCtrlOffset(self, de3, slice, hwSts);

    if (AtPdhDe3IsDs1Channelized((AtPdhDe3)de3))
        de2PayloadType = cThaOcnVtgTug2PldDs1;

    for (de2Id = 0; de2Id < 7; de2Id++)
        ret |= mMethodsGet(mThis(self))->HwVtgPldSet(mThis(self), (AtChannel)de3, slice, hwSts, de2Id, de2PayloadType, lineModeCtrlOffset);

    return ret;
    }

static eAtRet De3PldSet(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwSts, eThaOcnVc3PldType payloadType)
    {
    uint8 stsMd = AtPdhDe3IsChannelized((AtPdhDe3)channel) ? 0: 0x1;
    uint32 lineModeCtrlOffset = De3LineModeCtrlOffset(self, channel, slice, hwSts);
    uint8 hwPayloadType = (payloadType == cThaOcnVc3PldDs3) ? 0x1 : 0x0;
    eAtRet ret = mMethodsGet(mThis(self))->HwStsPldSet(mThis(self), (AtChannel)channel, stsMd, hwSts, hwPayloadType, lineModeCtrlOffset);

    if (!AtPdhDe3IsChannelized((AtPdhDe3)channel))
        return ret;

    return AllDe2sDefaultSet(self, channel, slice, hwSts);
    }

static eAtRet VtgPldSet(ThaModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType)
    {
    return mMethodsGet(mThis(self))->HwVtgPldSet(mThis(self), (AtChannel)channel, slice, hwSts, vtgId, payloadType, SdhChannelLineModeCtrlOffset(self, channel, slice, hwSts));
    }

static eAtRet De2PldSet(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType)
    {
    return mMethodsGet(mThis(self))->HwVtgPldSet(mThis(self), (AtChannel)channel, slice, hwSts, vtgId, payloadType, De2LineModeCtrlOffset(self, channel, slice, hwSts));
    }

static uint32 EngineTimingCtrlRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return 0x02A0800;
    }

static eBool OnlySupportPrcForDcr(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 AdjustStateStatusRegister(ThaModuleCdr self, ThaCdrController controller)
    {
    AtUnused(self);
    AtUnused(controller);
    return 0x02A1000;
    }

static uint32 ControllerDefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    AtChannel channel = ThaCdrControllerChannelGet(controller);
    uint8 slice = ThaCdrControllerChannelSliceGet(controller, channel);
    ThaSdhChannelCdrController sdhCdrController = (ThaSdhChannelCdrController)controller;
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel(sdhCdrController, channel);
    AtUnused(self);

    /* SdhChannel may be NULL in case of LIU DE3-DE1, but in context of this product, it is not cared */
    return ThaModuleCdrStmSliceOffset(self, sdhChannel, slice) + ThaCdrControllerIdGet(controller);
    }

static uint32 HoVcDefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    uint8 slice, sts;
    ThaSdhChannelCdrController sdhCdrController = (ThaSdhChannelCdrController)controller;
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel(sdhCdrController, ThaCdrControllerChannelGet(controller));
    AtUnused(self);
    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleCdr, &slice, &sts);
    return sts + ThaModuleCdrStmSliceOffset(self, sdhChannel, slice) + ThaCdrControllerPartOffset(controller);
    }

static uint32 Vc1xDefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    ThaSdhChannelCdrController sdhCdrController = (ThaSdhChannelCdrController)controller;
    AtSdhChannel sdhChannel = ThaSdhChannelCdrControllerSdhChannel(sdhCdrController, ThaCdrControllerChannelGet(controller));
    uint8 slice, sts;
    uint32 sliceOffset;

    /*
     * CDR Slice0 : sts #0,2,4,...46 (stsid in each oc24 slice is still 0,1,2,3,...,23)
     * CDR Slice1 : sts #1,3,5,...47 (stsid in each oc24 slice is still 0,1,2,3,...,23)
     * CDR Address = (0x280000 -  0x2BFFFF) + Slice*0x40000
     * where Slice (0-1)
     */
    ThaSdhChannel2HwMasterStsId(sdhChannel, cThaModuleCdr, &slice, &sts);
    sliceOffset = ThaModuleCdrStmSliceOffset(self, sdhChannel, slice);
    return sliceOffset + (uint32)(sts << 5) + (uint32)(AtSdhChannelTug2Get(sdhChannel) << 2) + AtSdhChannelTu1xGet(sdhChannel) + ThaCdrControllerPartOffset(controller);
    }

static uint32 De3DefaultOffset(ThaModuleCdr self, ThaCdrController controller)
    {
    uint8 slice = 0, sts = 0;
    uint32 sliceOffset;
    AtPdhChannel de3 = (AtPdhChannel)ThaCdrControllerChannelGet(controller);

    ThaPdhChannelHwIdGet(de3, cThaModuleCdr, &slice, &sts);
    sliceOffset = ThaModuleCdrStmSliceOffset(self, (AtSdhChannel)AtPdhChannelVcInternalGet(de3), slice);

    return sliceOffset + sts + ThaCdrControllerPartOffset(controller);
    }

static eBool VtAsyncMapIsSupported(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet DcrRtpTimestampFrequencyLessThan16BitsSet(ThaModuleCdr self, uint32 khz, uint32 offset)
    {
    uint32 regAddr = cThaRegDCRRtpFreqCfg + offset;
    uint32 regVal = mModuleHwRead(self, regAddr);

    mFieldIns(&regVal, cThaDcrRtpFreqCfgMask, cThaDcrRtpFreqCfgShift, khz);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet DcrRtpTimestampFrequencyLargerThan16BitsSet(ThaModuleCdr self, uint32 khz, uint32 offset)
    {
    uint32 regAddr = cRtpTimestamp155MhzOr77MhzSelect + offset;
    uint32 regVal  = mModuleHwRead(self, regAddr);
    mRegFieldSet(regVal, cRtpTimestamp155MhzOr77Mhz, cDcrRtpFrequency155Mhz / khz);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet DcrRtpTimestampFrequencyLessThan16BitsSelect(ThaModuleCdr self, uint32 offset)
    {
    uint8 const cDcrPrcSourceIsPrcRefClock = 0x0;
    uint32 regAddr = cThaRegDCRprCSourceSelectCfg + offset;
    uint32 regVal = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cThaDcrPrcDirectMode, cRtpTimestampFrequencyLessThan16BitsMode);
    mRegFieldSet(regVal, cThaDcrPrcSourceSel, cDcrPrcSourceIsPrcRefClock);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static eAtRet DcrRtpTimestampFrequencyLargerThan16BitsSelect(ThaModuleCdr self, uint32 offset)
    {
    uint8 const cDcrPrcSourceIsSystemClock = 0x1;
    uint32 regAddr = cThaRegDCRprCSourceSelectCfg + offset;
    uint32 regVal  = mModuleHwRead(self, regAddr);

    mRegFieldSet(regVal, cThaDcrPrcDirectMode, cRtpTimestampFrequencyLargerThan16BitsMode);

    /* HW recommend, must select system timing mode in this case */
    mRegFieldSet(regVal, cThaDcrPrcSourceSel, cDcrPrcSourceIsSystemClock);
    mModuleHwWrite(self, regAddr, regVal);

    return cAtOk;
    }

static ThaCdrDebugger DebuggerObjectCreate(ThaModuleCdr self)
    {
    AtUnused(self);
    return Tha60150011CdrDebuggerNew();
    }

static eAtRet HwStsModeSet(Tha60150011ModuleCdr self,  AtChannel channel, uint8 hwStsMd, uint8 hwSts, uint32 lineModeCtrlOffset)
    {
    uint32 longRegValue[cThaLongRegMaxSize];
    uint32 regAddr;
    ThaModuleCdr module = (ThaModuleCdr)self;

    uint8 hwStsLocalInGroup12 = mMethodsGet(self)->LocalHwStsInGroup12(self, hwSts);

    /* There are 2 line mode control registers in each slice.
     * One for 12 odd STSs and one for 12 even STSs
     */
    regAddr = mMethodsGet(module)->CDRLineModeControlRegister(module) + lineModeCtrlOffset;

    mChannelHwLongRead(channel, regAddr, longRegValue, cThaLongRegMaxSize, cThaModuleCdr);
    mFieldIns(&longRegValue[hwStsLocalInGroup12 / 4],
              cThaStsStsMdMask(hwStsLocalInGroup12),
              cThaStsStsMdShift(hwStsLocalInGroup12),
              hwStsMd);
    mChannelHwLongWrite(channel, regAddr, longRegValue, cThaLongRegMaxSize, cThaModuleCdr);

    return cAtOk;
    }

static eAtRet De3UnChannelizedModeSet(ThaModuleCdr self, AtPdhChannel channel, uint8 slice, uint8 hwSts, eBool isUnChannelized)
    {
    uint8 stsMd = isUnChannelized ? 1: 0;
    uint32 lineModeCtrlOffset = De3LineModeCtrlOffset(self, channel, slice, hwSts);
    return mMethodsGet(mThis(self))->HwStsModeSet(mThis(self), (AtChannel)channel, stsMd, hwSts, lineModeCtrlOffset);
    }

static eAtRet ChannelizeDe3(ThaModuleCdr self, AtPdhDe3 de3, eBool channelized)
    {
    uint8 hwPayloadType = AtPdhDe3IsDs3(de3) ? 0x1 : 0x0;
    uint8 hwStsMode = channelized ? 0 : 1;
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcGet((AtPdhChannel)de3);
    uint8 hwSlice, hwSts;
    eAtRet ret = cAtOk;

    ThaSdhChannelHwStsGet(vc, cThaModuleCdr, AtSdhChannelSts1Get(vc), &hwSlice, &hwSts);

    ret |= m_ThaModuleCdrMethods->ChannelizeDe3(self, de3, channelized);
    ret |= mMethodsGet(mThis(self))->LineModeHwSet(mThis(self), vc, hwSlice, hwSts, hwStsMode, hwPayloadType);

    return ret;
    }

static uint32 AdjustHoldoverValueStatusRegister(ThaModuleCdr self)
    {
    AtUnused(self);
    return 0x21800;
    }

static void OverrideThaModuleCdrStm(AtModule self)
    {
    ThaModuleCdrStm module = (ThaModuleCdrStm)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrStmMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrStmOverride, m_ThaModuleCdrStmMethods, sizeof(m_ThaModuleCdrStmOverride));

        mMethodOverride(m_ThaModuleCdrStmOverride, MaxNumSts);
        mMethodOverride(m_ThaModuleCdrStmOverride, StsTimingControlReg);
        mMethodOverride(m_ThaModuleCdrStmOverride, StsCdrChannelIdMask);
        mMethodOverride(m_ThaModuleCdrStmOverride, StsCdrChannelIdShift);
        mMethodOverride(m_ThaModuleCdrStmOverride, VtTimingControlReg);
        mMethodOverride(m_ThaModuleCdrStmOverride, VtCdrChannelIdMask);
        mMethodOverride(m_ThaModuleCdrStmOverride, VtCdrChannelIdShift);
        mMethodOverride(m_ThaModuleCdrStmOverride, Vc3CepModeEnable);
        mMethodOverride(m_ThaModuleCdrStmOverride, LineModePayloadSet);
        mMethodOverride(m_ThaModuleCdrStmOverride, De3PldSet);
        mMethodOverride(m_ThaModuleCdrStmOverride, De2PldSet);
        mMethodOverride(m_ThaModuleCdrStmOverride, VtgPldSet);
        mMethodOverride(m_ThaModuleCdrStmOverride, SliceOffset);
        mMethodOverride(m_ThaModuleCdrStmOverride, De3UnChannelizedModeSet);
        }

    mMethodsSet(module, &m_ThaModuleCdrStmOverride);
    }

static eBool ClockSubStateIsSupported(ThaModuleCdr self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, m_ThaModuleCdrMethods, sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, EngineTimingCtrlRegister);
        mMethodOverride(m_ThaModuleCdrOverride, ControllerDefaultOffset);
        mMethodOverride(m_ThaModuleCdrOverride, AdjustStateStatusRegister);
        mMethodOverride(m_ThaModuleCdrOverride, HoVcDefaultOffset);
        mMethodOverride(m_ThaModuleCdrOverride, Vc1xDefaultOffset);
        mMethodOverride(m_ThaModuleCdrOverride, VtAsyncMapIsSupported);
        mMethodOverride(m_ThaModuleCdrOverride, OnlySupportPrcForDcr);
        mMethodOverride(m_ThaModuleCdrOverride, De3DefaultOffset);
        mMethodOverride(m_ThaModuleCdrOverride, DebuggerObjectCreate);
        mMethodOverride(m_ThaModuleCdrOverride, ClockSubStateIsSupported);
        mMethodOverride(m_ThaModuleCdrOverride, ChannelizeDe3);
        mMethodOverride(m_ThaModuleCdrOverride, AdjustHoldoverValueStatusRegister);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCdr(self);
    OverrideThaModuleCdrStm(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ModuleCdr);
    }

static void MethodsInit(AtModule self)
    {
    Tha60150011ModuleCdr cdrModule = (Tha60150011ModuleCdr)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, LineModeCtrlOffset);
        mMethodOverride(m_methods, LocalHwStsInGroup12);
        mMethodOverride(m_methods, LineModeHwSet);
        mMethodOverride(m_methods, HwVtgPldSet);
        mMethodOverride(m_methods, HwStsPldSet);
        mMethodOverride(m_methods, HwStsModeSet);
        }

    mMethodsSet(cdrModule, &m_methods);
    }

AtModule Tha60150011ModuleCdrObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleCdrStmObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60150011ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModuleCdrObjectInit(newModule, device);
    }

static uint32 CepRtpTimeStampOffset(ThaModuleCdr self)
    {
    AtUnused(self);
    return 0x40000;
    }

static eBool DcrRtpTimestampFrequencyIsLargerThan16Bits(ThaModuleCdr self, uint32 offset)
    {
    uint32 regAddr = cThaRegDCRprCSourceSelectCfg + offset;
    uint32 regVal = mModuleHwRead(self, regAddr);

    return (mRegField(regVal, cThaDcrPrcDirectMode) == 0x1) ? cAtTrue : cAtFalse;
    }

static uint32 DcrRtpTimestampFrequencyLessThan16BitsGet(ThaModuleCdr self, uint32 offset)
    {
    uint32 regVal, frequency;

    regVal = mModuleHwRead(self, cThaRegDCRRtpFreqCfg + offset);
    mFieldGet(regVal, cThaDcrRtpFreqCfgMask, cThaDcrRtpFreqCfgShift, uint32, &frequency);

    return frequency;
    }

static uint32 DcrRtpTimestampFrequencyLargerThan16BitsGet(ThaModuleCdr self, uint32 offset)
    {
    uint32 regVal = mModuleHwRead(self, cRtpTimestamp155MhzOr77MhzSelect + offset);

    return cDcrRtpFrequency155Mhz / mRegField(regVal, cRtpTimestamp155MhzOr77Mhz);
    }

static eAtRet DcrRtpTimestampFrequencySet(ThaModuleCdr self, uint32 khz, uint32 offset)
    {
    eAtRet ret = cAtOk;

    if (!ThaModuleCdrHasDcrTimingMode(self))
        return cAtOk;

    if (khz <= cBit15_0)
        {
        ret |= DcrRtpTimestampFrequencyLessThan16BitsSet(self, khz, offset);
        ret |= DcrRtpTimestampFrequencyLessThan16BitsSelect(self, offset);
        return ret;
        }

    if ((khz != cDcrRtpFrequency155Mhz) && (khz != cDcrRtpFrequency77Mhz))
        return cAtErrorModeNotSupport;

    ret |= DcrRtpTimestampFrequencyLargerThan16BitsSet(self, khz, offset);
    ret |= DcrRtpTimestampFrequencyLargerThan16BitsSelect(self, offset);

    return ret;
    }

static uint32 DcrRtpTimestampFrequencyGet(ThaModuleCdr self, uint32 offset)
    {
    if (!ThaModuleCdrHasDcrTimingMode(self))
        return 0x0;

    if (DcrRtpTimestampFrequencyIsLargerThan16Bits(self, offset))
        return DcrRtpTimestampFrequencyLargerThan16BitsGet(self, offset);

    return DcrRtpTimestampFrequencyLessThan16BitsGet(self, offset);
    }

eAtRet Tha60150011ModuleCdrCepDcrRtpTimestampFrequencySet(ThaModuleCdr self, uint32 khz)
    {
    return DcrRtpTimestampFrequencySet(self, khz, CepRtpTimeStampOffset(self));
    }

uint32 Tha60150011ModuleCdrCepDcrRtpTimestampFrequencyGet(ThaModuleCdr self)
    {
    return DcrRtpTimestampFrequencyGet(self, CepRtpTimeStampOffset(self));
    }

eAtRet Tha60150011ModuleCdrCESoPDcrRtpTimestampFrequencySet(ThaModuleCdr self, uint32 khz)
    {
    return DcrRtpTimestampFrequencySet(self, khz, 0);
    }

uint32 Tha60150011ModuleCdrCESoPDcrRtpTimestampFrequencyGet(ThaModuleCdr self)
    {
    return DcrRtpTimestampFrequencyGet(self, 0);
    }

uint8 Tha60150011ModuleCdrHwVtType(eThaOcnVtgTug2PldType payloadType)
    {
    return HwVtType(payloadType);
    }

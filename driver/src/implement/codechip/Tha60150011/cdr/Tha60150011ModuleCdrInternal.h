/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60150011ModuleCdrInternal.h
 * 
 * Created Date: Apr 21, 2015
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011MODULECDRINTERNAL_H_
#define _THA60150011MODULECDRINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cdr/ThaModuleCdrInternal.h"
#include "../../../default/cdr/controllers/ThaVcDe3CdrControllerInternal.h"
#include "Tha60150011ModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/
/* DCR PRC Source Select Configuration  */
#define cThaDcrPrcDirectModeMask  cBit3
#define cThaDcrPrcDirectModeShift 3

#define cRtpTimestamp155MhzOr77MhzMask   cBit15_0
#define cRtpTimestamp155MhzOr77MhzShift  0

#define cDcrRtpFrequency155Mhz 155520
#define cDcrRtpFrequency77Mhz  77760

#define cRtpTimestampFrequencyLessThan16BitsMode   0
#define cRtpTimestampFrequencyLargerThan16BitsMode 1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModuleCdrMethods
    {
    uint32 (*LineModeCtrlOffset)(Tha60150011ModuleCdr self, uint32 sliceOffset, uint8 hwStsId, uint8 partId);
    uint8  (*LocalHwStsInGroup12)(Tha60150011ModuleCdr self, uint8 hwSts);
    eAtRet (*LineModeHwSet)(Tha60150011ModuleCdr self, AtSdhChannel channel, uint8 slice, uint8 hwSts, uint8 hwStsMode, uint8 hwPayloadType);
    eAtRet (*HwVtgPldSet)(Tha60150011ModuleCdr self, AtChannel channel, uint8 slice, uint8 hwSts, uint8 vtgId, eThaOcnVtgTug2PldType payloadType, uint32 lineModeCtrlOffset);
    eAtRet (*HwStsPldSet)(Tha60150011ModuleCdr self, AtChannel channel, uint8 hwStsMd, uint8 hwSts, uint8 hwPayloadType, uint32 lineModeCtrlOffset);
    eAtRet (*HwStsModeSet)(Tha60150011ModuleCdr self,  AtChannel channel, uint8 hwStsMd, uint8 hwSts, uint32 lineModeCtrlOffset);
    }tTha60150011ModuleCdrMethods;

typedef struct tTha60150011ModuleCdr
    {
    tThaModuleCdrStm super;
    const tTha60150011ModuleCdrMethods *methods;
    }tTha60150011ModuleCdr;

typedef struct tTha60150011VcDe3CdrController
    {
    tThaVcDe3CdrController super;
    }tTha60150011VcDe3CdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60150011ModuleCdrObjectInit(AtModule self, AtDevice device);
ThaCdrController Tha60150011VcDe3CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

#endif /* _THA60150011MODULECDRINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60150011ModuleCdr.h
 * 
 * Created Date: Jun 25, 2014
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha60150011MODULECDR_H_
#define _Tha60150011MODULECDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"
#include "../../../default/cdr/ThaModuleCdr.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModuleCdr * Tha60150011ModuleCdr;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Util */
uint32 Tha60150011CdrControllerDefaultOffset(ThaCdrController self);
eAtRet Tha60150011ModuleCdrCepDcrRtpTimestampFrequencySet(ThaModuleCdr self, uint32 khz);
uint32 Tha60150011ModuleCdrCepDcrRtpTimestampFrequencyGet(ThaModuleCdr self);
eAtRet Tha60150011ModuleCdrCESoPDcrRtpTimestampFrequencySet(ThaModuleCdr self, uint32 khz);
uint32 Tha60150011ModuleCdrCESoPDcrRtpTimestampFrequencyGet(ThaModuleCdr self);
uint8 Tha60150011ModuleCdrHwVtType(eThaOcnVtgTug2PldType payloadType);

#endif /* _Tha60150011MODULECDR_H_ */


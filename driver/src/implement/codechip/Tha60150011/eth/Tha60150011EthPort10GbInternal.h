/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60150011EthPort10GbInternal.h
 * 
 * Created Date: Mar 28, 2015
 *
 * Description : Ethernet port 10G of 60150011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60150011_ETH_THA60150011ETHPORT10GBINTERNAL_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60150011_ETH_THA60150011ETHPORT10GBINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/eth/ThaEthPortInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011EthPort10Gb
    {
    tThaEthPort10Gb super;
    }tTha60150011EthPort10Gb;

/*--------------------------- Forward declarations ---------------------------*/
AtEthPort Tha60150011EthPort10GbObjectInit(AtEthPort self, uint8 portId, AtModuleEth module);

/*--------------------------- Entries ----------------------------------------*/

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60150011_ETH_THA60150011ETHPORT10GBINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH module name
 * 
 * File        : Tha60150011ModuleEth.h
 * 
 * Created Date: Mar 26, 2015
 *
 * Description : 60160011 ETH module header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011MODULEETH_H_
#define _THA60150011MODULEETH_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/eth/ThaStmPwProductModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModuleEth
    {
    tThaStmPwProductModuleEth super;
    }tTha60150011ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60150011ModuleEthObjectInit(AtModuleEth self, AtDevice device);
AtModuleEth Tha60150011ModuleEthNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011MODULEETH_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module Ethernet
 *
 * File        : Tha60150011EthPort10Gb.c
 *
 * Created Date: Oct 14, 2014
 *
 * Description : Ethernet port 10G of 60150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60150011EthPort10GbInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60150011EthPort10Gb *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

static tAtChannelMethods      m_AtChannelOverride;
static tThaEthPort10GbMethods m_ThaEthPort10GbOverride;

static const tAtChannelMethods      *m_AtChannelMethod       = NULL;
static const tThaEthPort10GbMethods *m_ThaEthPort10GbMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool SupportCounters(ThaEthPort10Gb self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool CounterIsSupported(AtChannel self, uint16 counterType)
    {
    return AtEthPortCounterIsSupported(self, counterType);
    }

static void StandbyIpgDefaultSet(AtChannel self)
    {
    AtEthPort port;

    if (AtChannelAccessible(self) && !AtDeviceIsSimulated(AtChannelDeviceGet(self)))
        return;

    /* Without this, HA testing will be fail at bandwidth calculation database */
    port = (AtEthPort)self;
    if (AtEthPortTxIpgIsConfigurable(port))
        AtEthPortTxIpgSet(port, ThaEthPort10GbDefaultTxIpgGet((ThaEthPort10Gb)self));
    if (AtEthPortRxIpgIsConfigurable(port))
        AtEthPortRxIpgSet(port, ThaEthPort10GbDefaultRxIpgGet((ThaEthPort10Gb)self));
    }

static eAtRet Init(AtChannel self)
    {
    StandbyIpgDefaultSet(self);

    /* Hw recommended, do not init anything, just need to disable higig */
    return AtEthPortHiGigEnable((AtEthPort)self, cAtFalse);
    }

static void OverrideThaEthPort10Gb(AtEthPort self)
    {
    ThaEthPort10Gb port10gb = (ThaEthPort10Gb)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_ThaEthPort10GbMethods = mMethodsGet(port10gb);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthPort10GbOverride, m_ThaEthPort10GbMethods, sizeof(tThaEthPort10GbMethods));
        mMethodOverride(m_ThaEthPort10GbOverride, SupportCounters);
        }

    mMethodsSet(port10gb, &m_ThaEthPort10GbOverride);
    }

static void OverrideAtChannel(AtEthPort self)
    {
    AtChannel channel = (AtChannel)self;
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtChannelMethod = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethod, sizeof(tAtChannelMethods));
        mMethodOverride(m_AtChannelOverride, CounterIsSupported);
        mMethodOverride(m_AtChannelOverride, Init);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void Override(AtEthPort self)
    {
    OverrideThaEthPort10Gb(self);
    OverrideAtChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011EthPort10Gb);
    }

AtEthPort Tha60150011EthPort10GbObjectInit(AtEthPort self, uint8 portId, AtModuleEth module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPort10GbObjectInit((AtEthPort)self, portId, module) == NULL)
        return NULL;

    /* Initialize method */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtEthPort Tha60150011EthPort10GbNew(uint8 portId, AtModuleEth module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtEthPort newPort = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPort == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011EthPort10GbObjectInit(newPort, portId, module);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Ethernet
 * 
 * File        : Tha60150011ModuleEthInternal.h
 * 
 * Created Date: Mar 28, 2015
 *
 * Description : Module ethernet of 60150011
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60150011_ETH_THA60150011MODULEETHINTERNAL_C_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60150011_ETH_THA60150011MODULEETHINTERNAL_C_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/eth/ThaStmPwProductModuleEth.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus

extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModuleEth
    {
    tThaStmPwProductModuleEth super;
    }tTha60150011ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/
AtModuleEth Tha60150011ModuleEthObjectInit(AtModuleEth self, AtDevice device);

/*--------------------------- Entries ----------------------------------------*/
#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA60150011_ETH_THA60150011MODULEETHINTERNAL_C_ */


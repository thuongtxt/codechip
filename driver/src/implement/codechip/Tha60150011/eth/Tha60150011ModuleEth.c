/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module ethernet
 *
 * File        : Tha60150011ModuleEth.c
 *
 * Created Date: Aug 13, 2014
 *
 * Description : Ethernet module for product 60150011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthPort.h"
#include "Tha60150011ModuleEth.h"
#include "../physical/Tha60150011EthPortSerdesController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleEthMethods m_ThaModuleEthOverride;
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tAtModuleMethods     m_AtModuleOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    return Tha60150011EthPort10GbNew(portId, self);
    }

static eBool PortPllCanBeChecked(ThaModuleEth self, uint8 portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return cAtFalse;
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
    AtUnused(self);
    return 1;
    }

static AtSerdesController PortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort)
    {
    uint32 portId = AtChannelIdGet((AtChannel)ethPort);
    AtUnused(self);
    return Tha60150011EthPortSerdesControllerNew(ethPort, portId);
    }

static eBool Port10GbSupported(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool PortShouldByPassFcs(ThaModuleEth self, uint8 portId)
    {
    AtUnused(self);
    AtUnused(portId);
    return cAtTrue;
    }

static AtEthFlowControl PortFlowControlCreate(AtModuleEth self, AtEthPort port)
    {
    AtUnused(self);
    AtUnused(port);
    return NULL;
    }

static eBool SerdesHasMdio(AtModuleEth self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 SerdesMdioBaseAddress(AtModuleEth self, AtSerdesController serdes)
    {
    AtUnused(self);
    AtUnused(serdes);
    return 0xf50000;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    AtUnused(localAddress);
    AtUnused(self);

    /* This module has no long register */
    return cAtFalse;
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, HasRegister);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        mMethodOverride(m_AtModuleEthOverride, PortFlowControlCreate);
        mMethodOverride(m_AtModuleEthOverride, SerdesMdioBaseAddress);
        mMethodOverride(m_AtModuleEthOverride, SerdesHasMdio);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(AtModuleEth self)
    {
    ThaModuleEth ethModule = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(ethModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, PortPllCanBeChecked);
        mMethodOverride(m_ThaModuleEthOverride, Port10GbSupported);
        mMethodOverride(m_ThaModuleEthOverride, PortShouldByPassFcs);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesControllerCreate);
        }

    mMethodsSet(ethModule, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideThaModuleEth(self);
    OverrideAtModuleEth(self);
    OverrideAtModule(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ModuleEth);
    }

AtModuleEth Tha60150011ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60150011ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModuleEthObjectInit(newModule, device);
    }

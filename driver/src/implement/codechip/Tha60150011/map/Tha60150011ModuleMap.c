/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60150011ModuleMap.c
 *
 * Created Date: Jun 24, 2014
 *
 * Description : MAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/sdh/ThaModuleSdh.h"
#include "Tha60150011ModuleMapDemap.h"

/*--------------------------- Define -----------------------------------------*/
#undef cThaMapFrmTypeMask
#undef cThaMapFrmTypeShift
#define cThaMapFrmTypeMask                             cBit16_15
#define cThaMapFrmTypeShift                            15

#undef cThaMapSigTypeMask
#undef cThaMapSigTypeShift
#define cThaMapSigTypeMask                             cBit14_11
#define cThaMapSigTypeShift                            11

#undef cThaMapTimeSrcMastMask
#undef cThaMapTimeSrcMastShift
#define cThaMapTimeSrcMastMask                         cBit10
#define cThaMapTimeSrcMastShift                        10

#undef cThaMapTimeSrcIdMask
#undef cThaMapTimeSrcIdShift
#define cThaMapTimeSrcIdMask                           cBit9_0
#define cThaMapTimeSrcIdShift                          0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;
static tThaModuleMapMethods         m_ThaModuleMapOverride;
static tThaModuleStmMapMethods      m_ThaModuleStmMapOverride;

/* Super implement */
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MapFrmTypeMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapFrmTypeMask;
    }

static uint8 MapFrmTypeShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapFrmTypeShift;
    }

static uint32 MapSigTypeMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapSigTypeMask;
    }

static uint8 MapSigTypeShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapSigTypeShift;
    }

static uint32 MapTimeSrcMastMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapTimeSrcMastMask;
    }

static uint8 MapTimeSrcMastShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapTimeSrcMastShift;
    }

static uint32 MapTimeSrcIdMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapTimeSrcIdMask;
    }

static uint8 MapTimeSrcIdShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cThaMapTimeSrcIdShift;
    }

static uint32 Vc1xDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc1x)
    {
    return ThaModuleAbstractMapVc1xDefaultOffset(self, vc1x) +
           ThaModuleMapPartOffset(self, ThaModuleSdhPartOfChannel((AtSdhChannel)vc1x));
    }

static uint32 AuVcStsDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc, uint8 stsId)
    {
    return ThaModuleAbstractMapAuVcDefaultOffset(self, vc, stsId) +
           ThaModuleMapPartOffset(self, ThaModuleSdhPartOfChannel((AtSdhChannel)vc));
    }

static uint32 De3DefaultOffset(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    return ThaModuleAbstractMapVcDe3DefaultOffset(self, de3);
    }

static uint32 PartOffset(ThaModuleStmMap self, uint8 partId)
    {
    ThaDevice device = (ThaDevice)AtModuleDeviceGet((AtModule)self);
    return ThaDeviceModulePartOffset(device, cThaModuleMap, partId);
    }

static uint32 SdhChannelPartOffset(ThaModuleStmMap self, AtSdhChannel sdhChannel)
    {
    return PartOffset(self, ThaModuleSdhPartOfChannel(sdhChannel));
    }

static uint32 SdhChannelMapLineOffset(ThaModuleStmMap self, AtSdhChannel sdhChannel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint8 slice, hwSts;

    ThaSdhChannelHwStsGet(sdhChannel, cThaModuleMap, stsId, &slice, &hwSts);

    /* 32 * stsid + 4 * vtgid + vtid */
    return (uint32)(hwSts << 5) + (uint32)(vtgId << 2) + vtId +
            ThaModuleAbstractMapSdhSliceOffset((ThaModuleAbstractMap)self, sdhChannel, slice) +
            SdhChannelPartOffset(self, sdhChannel);
    }

static uint32 SdhSliceOffset(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 slice)
    {
    return Tha60150011ModuleMapDemapSliceOffset(self, sdhChannel, slice);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, Vc1xDefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcStsDefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De3DefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, SdhSliceOffset);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static uint32 De1MapChnCtrl(ThaModuleMap self, AtPdhDe1 channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return 0x1B4000;
    }

static void OverrideThaModuleMap(AtModule self)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;

    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleMapOverride));

        /* Override bit fields */
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapFrmType)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapSigType)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapTimeSrcMast)
        mBitFieldOverride(mapModule, m_ThaModuleMapOverride, MapTimeSrcId)

        /* Override register address */
        mMethodOverride(m_ThaModuleMapOverride, De1MapChnCtrl);
        }

    mMethodsSet(mapModule, &m_ThaModuleMapOverride);
    }

static void OverrideThaModuleStmMap(AtModule self)
    {
    ThaModuleStmMap mapModule = (ThaModuleStmMap)self;

    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleStmMapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleStmMapOverride));

        mMethodOverride(m_ThaModuleStmMapOverride, SdhChannelMapLineOffset);
        }

    mMethodsSet(mapModule, &m_ThaModuleStmMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleAbstractMap(self);
    OverrideThaModuleMap(self);
    OverrideThaModuleStmMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ModuleMap);
    }

AtModule Tha60150011ModuleMapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super construction */
    if (ThaModuleStmMapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60150011ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModuleMapObjectInit(newModule, device);
    }

uint32 Tha60150011ModuleMapDemapSliceOffset(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 slice)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return slice * 0x20000UL;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP/DEMAP
 * 
 * File        : Tha60150011ModuleMapDemap.h
 * 
 * Created Date: Mar 1, 2015
 *
 * Description : MAP/DEMAP common declaration
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60150011MODULEMAPDEMAP_H_
#define _THA60150011MODULEMAPDEMAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/map/ThaModuleStmMapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60150011ModuleMap
    {
    tThaModuleStmMap super;
    }tTha60150011ModuleMap;

typedef struct tTha60150011ModuleDemap
    {
    tThaModuleStmDemap super;
    }tTha60150011ModuleDemap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructors */
AtModule Tha60150011ModuleMapObjectInit(AtModule self, AtDevice device);
AtModule Tha60150011ModuleDemapObjectInit(AtModule self, AtDevice device);

/* Common methods */
uint32 Tha60150011ModuleMapDemapSliceOffset(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 slice);

#ifdef __cplusplus
}
#endif
#endif /* _THA60150011MODULEMAPDEMAP_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60150011ModuleDemap.c
 *
 * Created Date: Jun 24, 2014
 *
 * Description : Demap module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/map/ThaModuleStmMapInternal.h"
#include "../../../default/sdh/ThaModuleSdh.h"
#include "Tha60150011ModuleMapDemap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;

/* Super implement */
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*
 * MAP Slice0 : sts #0,2,4,...46 (stsid in each oc24 slice is still 0,1,2,3,...,23)
 * MAP Slice1 : sts #1,3,5,...47 (stsid in each oc24 slice is still 0,1,2,3,...,23)
 * MAP Address = (0x1A0000 -  0x1BFFFF) + Slice*0x20000
 * where Slice (0-1)
 */

static uint32 Vc1xDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc1x)
    {
    return ThaModuleAbstractMapVc1xDefaultOffset(self, vc1x) +
           ThaModuleMapPartOffset(self, ThaModuleSdhPartOfChannel((AtSdhChannel)vc1x));
    }

static uint32 AuVcStsDefaultOffset(ThaModuleAbstractMap self, AtSdhVc vc, uint8 stsId)
    {
    return ThaModuleAbstractMapAuVcDefaultOffset(self, vc, stsId) +
           ThaModuleMapPartOffset(self, ThaModuleSdhPartOfChannel((AtSdhChannel)vc));
    }

static uint32 De3DefaultOffset(ThaModuleAbstractMap self, AtPdhDe3 de3)
    {
    return ThaModuleAbstractMapVcDe3DefaultOffset(self, de3);
    }

static uint32 SdhSliceOffset(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, uint8 slice)
    {
    return Tha60150011ModuleMapDemapSliceOffset(self, sdhChannel, slice);
    }

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, Vc1xDefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcStsDefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, De3DefaultOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, SdhSliceOffset);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleAbstractMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60150011ModuleDemap);
    }

AtModule Tha60150011ModuleDemapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super construction */
    if (ThaModuleStmDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60150011ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60150011ModuleDemapObjectInit(newModule, device);
    }

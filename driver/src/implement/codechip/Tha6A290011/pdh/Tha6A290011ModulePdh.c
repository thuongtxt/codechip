/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290021ModulePdh.c
 *
 * Created Date: Aug 2, 2017
 *
 * Description : PDH module of 6A290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/att/ThaAttPdhManagerInternal.h"
#include "../../Tha60290011/pdh/Tha60290011ModulePdhInternal.h"
#include "Tha6A290011ModulePdhInternal.h"
#include "Tha6A290011PdhAttControllerInternal.h"
#include "Tha6A290011PdhReg.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011ModulePdh
    {
    tTha60290011ModulePdh super;
    }tTha6A290011ModulePdh;

/*--------------------------- Global variables -------------------------------*/
#define mThis(self)              ((tTha6A290011ModulePdh*)self)

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtModulePdhMethods          m_AtModulePdhOverride;
static tThaModulePdhMethods         m_ThaModulePdhOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eAtRet De2DefaultSet(ThaModulePdh self)
    {
    uint8  numSlices    = ThaModulePdhNumSlices(self);
    uint8 slice;
    for (slice =0; slice <numSlices; slice ++)
        {
        uint32 baseAddress = ThaModulePdhSliceBase(self, slice);
        mModuleHwWrite(self, cAf6Reg_rxm12e12_fbe_thrsh_cfg     + baseAddress, 0xFF);
        mModuleHwWrite(self, cAf6Reg_rxm12e12_parity_thrsh_cfg  + baseAddress, 0xFF);
        mModuleHwWrite(self, cAf6Reg_rxm12e12_rai_thrsh_cfg     + baseAddress, 0x7);
        }

    return cAtOk;
    }

static uint32 RxM12E12FrmChnIntrEnCtrlBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_RxM12E12_per_chn_intr_cfg;
    }

static uint32 RxM12E12FrmChnIntrChngStatusBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_RxM12E12_per_chn_intr_stat;
    }

static uint32 RxM12E12FrmChnAlarmCurStatusBase(ThaModulePdh self)
    {
    AtUnused(self);
    return cAf6Reg_RxM12E12_per_chn_curr_stat;
    }

static ThaVersionReader VersionReader(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaDeviceVersionReader(device);
    }

static uint32 StartVersionSupportMdl(ThaModulePdh self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0010);
    }
static eBool NeedMdl(ThaModulePdh self)
    {
    uint32 startVersionHasThis = StartVersionSupportMdl(self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtModule)self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static eBool MdlIsSupported(ThaModulePdh self)
    {
    AtUnused(self);
    if (NeedMdl(self))
        return cAtTrue;
    return cAtFalse;
    }

static eBool DetectDe1LosByAllZeroPattern(ThaModulePdh self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static AtPdhDe2 De2Create(AtModulePdh self, uint32 de2Id)
    {
    return Tha6A290011PdhDe2New(de2Id, self);
    }

static AtPdhDe3 De3Create(AtModulePdh self, uint32 de3Id)
    {
    return Tha6A290011PdhDe3New(de3Id, self);
    }

static AtPdhDe1 De2De1Create(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id)
    {
    AtUnused(de2);
    return Tha6A290011PdhDe2De1New(de1Id, self);
    }

static AtPdhDe1 VcDe1Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    return Tha6A290011PdhVcDe1New((AtSdhVc)sdhVc, self);
    }

static AtPdhDe1 De1Create(AtModulePdh self, uint32 de1Id)
    {
    return Tha6A290011PdhDe1New(de1Id, self);
    }

static AtAttPdhManager AttPdhManagerCreate(AtModulePdh self)
    {
    return Tha6A290011AttPdhManagerNew(self);
    }

static AtObjectAny De1AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290011PdhDe1AttControllerNew(channel);
    }

static AtObjectAny De2AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    AtUnused(channel);
    return Tha6A290011PdhDe2AttControllerNew(channel);
    }

static AtObjectAny De3AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290011PdhDe3AttControllerNew(channel);
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
	mVersionReset(self);
    }

static void OverrideAtObject(AtModulePdh self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(self), sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, De3Create);
        mMethodOverride(m_AtModulePdhOverride, De2Create);
        mMethodOverride(m_AtModulePdhOverride, De2De1Create);
        mMethodOverride(m_AtModulePdhOverride, De1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe1Create);
        mMethodOverride(m_AtModulePdhOverride, AttPdhManagerCreate);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(AtModulePdh self)
    {
    ThaModulePdh module = (ThaModulePdh)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(module), sizeof(m_ThaModulePdhOverride));
        mMethodOverride(m_ThaModulePdhOverride, DetectDe1LosByAllZeroPattern);
        mMethodOverride(m_ThaModulePdhOverride, De1AttControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, De3AttControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, De2AttControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        mMethodOverride(m_ThaModulePdhOverride, MdlIsSupported);
        mMethodOverride(m_ThaModulePdhOverride, RxM12E12FrmChnAlarmCurStatusBase);
        mMethodOverride(m_ThaModulePdhOverride, RxM12E12FrmChnIntrChngStatusBase);
        mMethodOverride(m_ThaModulePdhOverride, RxM12E12FrmChnIntrEnCtrlBase);
        mMethodOverride(m_ThaModulePdhOverride, De2DefaultSet);
        }

    mMethodsSet(module, &m_ThaModulePdhOverride);
    }

static void Override(AtModulePdh self)
    {
    OverrideAtObject(self);
    OverrideAtModulePdh(self);
    OverrideThaModulePdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011ModulePdh);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha6A290011ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

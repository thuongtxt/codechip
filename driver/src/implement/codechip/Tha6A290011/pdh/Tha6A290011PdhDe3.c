/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290011PdhDe3.c
 *
 * Created Date: Sep 14, 2017
 *
 * Description : DE3 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/pdh/Tha60290011ModulePdh.h"
#include "../../Tha60290011/pdh/Tha60290011PdhDe3Internal.h"
#include "Tha6A290011ModulePdhInternal.h"
#include "Tha6A290011PdhAttControllerInternal.h"
#include "../../Tha61031031/prbs/Tha61031031PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha6A290011PdhDe3*)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011PdhDe3
    {
    tTha60290011PdhDe3 super;
    }tTha6A290011PdhDe3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;
static tThaPdhDe3Methods    m_ThaPdhDe3Override;

/* Save super implementation */
static const tAtChannelMethods    *m_AtChannelMethods    = NULL;
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = cAtOk;
    ret = m_AtChannelMethods->TimingSet(self, timingMode, timingSource);
    if (timingMode == cAtTimingModeFreeRunning||timingMode == cAtTimingModeSys)
        AtAttControllerFrequenceOffsetSet(AtChannelAttController(self), 0);
    return ret;
    }

static uint8 DefaultAisAllOnesThreshold(ThaPdhDe3 self)
    {
    /* In DUT, this is 0 for DS3 and 1 for E3.
     * How ever in ATT should be 1 for both DS3 and E3 */
    AtUnused(self);
    return 1;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AtAttControllerInit(AtChannelAttController(self));
    }

static eAtRet De3FramingBindToPwSatop(ThaPdhDe3 self, AtPw pseudowire)
    {
    AtUnused(self);
    AtUnused(pseudowire);
    return cAtOk;
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmTypes, eAtRet (*fForceAlarm)(AtAttController, uint32), eAtRet (*fSetClearAlarm)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 alarmType = alarmTypes & (cBit0 << i);
        if (alarmType)
            {
            AtAttControllerForceAlarmModeSet(attController, alarmType, cAtAttForceAlarmModeContinuous);
            fSetClearAlarm(attController, alarmType);
            ret =  fForceAlarm(attController, alarmType);
            }
        }
    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290011AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerForceAlarm,AtAttControllerTxAlarmsSet);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290011AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerUnForceAlarm, AtAttControllerTxAlarmsClear);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (Tha6A290011AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcibleAlarmsGet(self);

    return cAtPdhDe3AlarmLos | cAtPdhDe3AlarmAis | cAtPdhDe3AlarmRai | cAtPdhDe3AlarmLof;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorTypes, eBool force)
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 errorType = errorTypes & (cBit0 << i);
        if (errorType)
            {
            eAtAttPdhDe3ErrorType attErrorType = errorTypes;
            AtAttControllerForceErrorModeSet(attController, attErrorType, cAtAttForceErrorModeContinuous);
            if (force)
                ret =  AtAttControllerForceError(attController, attErrorType);
            else
                ret = AtAttControllerUnForceError(attController, attErrorType);
            }
        }
    return ret;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290011AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorForce(self, errorType);

    return HelperTxErrorForce(self, errorType, cAtTrue);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290011AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorUnForce(self, errorType);

    return HelperTxErrorForce(self, errorType, cAtFalse);
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    if (Tha6A290011AttPdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcableErrorsGet(self);

    return cAtPdhDe3CounterRei | cAtPdhDe3CounterFBit | cAtPdhDe3CounterCPBit | cAtPdhDe3CounterPBit;
    }

static void OverrideAtChannel(AtPdhDe3 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static eBool IsFrameTypeUnChn(uint16 frameType)
    {
    switch (frameType)
        {
        case cAtPdhDs3Unfrm:
        case cAtPdhDs3FrmCbitUnChn:
        case cAtPdhE3Unfrm:
        case cAtPdhE3Frmg832:
        case cAtPdhE3FrmG751:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    eAtRet ret = cAtOk;
    uint16 pre_frameType =m_AtPdhChannelMethods->FrameTypeGet(self);
    if ( pre_frameType != frameType)
        {
        AtPrbsEngine prbsEngine = AtChannelPrbsEngineGet((AtChannel)self);
        Tha61031031PrbsEngineTdmPwPrbsEngineDelete((Tha61031031PrbsEngineTdmPw)prbsEngine);
        if (IsFrameTypeUnChn(frameType) && IsFrameTypeUnChn(pre_frameType))
            {
            AtPw pw = AtChannelBoundPwGet((AtChannel)self);
            if (pw)
                {
                if (AtPwCircuitUnbind(pw)==cAtOk)
                    {
                    ret = m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
                    AtPwCircuitBind(pw, (AtChannel)self);
                    }
                return ret;
                }
            }
        ret = m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
        }
    return ret;
    }

static void OverrideAtPdhChannel(AtPdhChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(m_AtPdhChannelOverride));
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        }

    mMethodsSet(self, &m_AtPdhChannelOverride);
    }

static void OverrideThaPdhDe3(AtPdhDe3 self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3Override, mMethodsGet(de3), sizeof(m_ThaPdhDe3Override));

        /* Setup methods */
        mMethodOverride(m_ThaPdhDe3Override, De3FramingBindToPwSatop);
        mMethodOverride(m_ThaPdhDe3Override, DefaultAisAllOnesThreshold);
        }

    mMethodsSet(de3, &m_ThaPdhDe3Override);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtChannel(self);
    OverrideAtPdhChannel((AtPdhChannel)self);
    OverrideThaPdhDe3(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011PdhDe3);
    }

static AtPdhDe3 ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60290011PdhDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe3 Tha6A290011PdhDe3New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newDe3, channelId, module);
    }


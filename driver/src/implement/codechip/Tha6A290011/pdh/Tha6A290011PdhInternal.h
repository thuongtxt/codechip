/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha6A290011PdhInternal.h
 * 
 * Created Date: Aug 2, 2017
 *
 * Description : Internal of PDH
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290011PDHINTERNALINTERNAL_H_
#define _THA60290011PDHINTERNALINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaPdhDe1.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 Tha6A290011PdhDe1New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha6A290011PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290011PDHINTERNALINTERNAL_H_ */


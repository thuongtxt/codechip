/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : ThaPdhDe2Internal.h
 * 
 * Created Date: May 03, 2018
 *
 * Description : PDH DE1
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011PDHDE2INTERNAL_H_
#define _THA6A290011PDHDE2INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pdh/ThaPdhDe2Internal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha6A290011PdhDe2
    {
    tThaPdhDe2 super;
    }tTha6A290011PdhDe2;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe2 Tha6A290011PdhDe2ObjectInit(AtPdhDe2 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290011PDHDE2INTERNAL_H_ */


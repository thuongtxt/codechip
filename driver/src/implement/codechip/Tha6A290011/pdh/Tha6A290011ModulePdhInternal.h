/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A290011ModulePdhInternal.h
 * 
 * Created Date: Sep 14, 2017
 *
 * Description : ATT PDH Module Internal
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011MODULEPDHINTERNAL_H_
#define _THA6A290011MODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290011/pdh/Tha60290011ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290011ModulePdh * Tha6A290011ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe3 Tha6A290011PdhDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe2 Tha6A290011PdhDe2New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha6A290011PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module);
AtPdhDe1 Tha6A290011PdhDe2De1New(uint32 channelId, AtModulePdh module);
AtPdhDe3 Tha6A290011PdhDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha6A290011PdhDe1New(uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290011MODULEPDHINTERNAL_H_ */

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date: August 3, 2018                                                 
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _THA6A290011PDHDE1ATTREG_H_
#define _THA6A290011PDHDE1ATTREG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Force F Bit Error Configuration
Reg Addr   : 0x00092000-0x923FF
Reg Formula: 0x00092000 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This register is applicable to configure Force F Bit Error fore DS1/E1 Frame

------------------------------------------------------------------------------*/
#define cRegDs1_upen_fbitcfg                                                                           0x00092000
#define cRegDs1_upen_fbitcfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: fbittim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define cDs1_upen_fbitcfg_fbittim_mod_Mask                                                              cBit27_26
#define cDs1_upen_fbitcfg_fbittim_mod_Shift                                                                    26

/*--------------------------------------
BitField Name: fbiterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define cDs1_upen_fbitcfg_fbiterr_clr_Mask                                                              cBit25_18
#define cDs1_upen_fbitcfg_fbiterr_clr_Shift                                                                    18

/*--------------------------------------
BitField Name: fbiterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cDs1_upen_fbitcfg_fbiterr_set_Mask                                                              cBit17_10
#define cDs1_upen_fbitcfg_fbiterr_set_Shift                                                                    10

/*--------------------------------------
BitField Name: fbiterr_step
BitField Type: RW
BitField Desc: The Total Step to Force FBIT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define cDs1_upen_fbitcfg_fbiterr_step_Mask                                                               cBit9_2
#define cDs1_upen_fbitcfg_fbiterr_step_Shift                                                                    2

/*--------------------------------------
BitField Name: fbitmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for FBIT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define cDs1_upen_fbitcfg_fbitmsk_pos_Mask_01                                                           cBit31_18
#define cDs1_upen_fbitcfg_fbitmsk_pos_Shift_01                                                                 18
#define cDs1_upen_fbitcfg_fbitmsk_pos_Mask_02                                                             cBit1_0
#define cDs1_upen_fbitcfg_fbitmsk_pos_Shift_02                                                                  0

/*--------------------------------------
BitField Name: fbitmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For FBIT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define cDs1_upen_fbitcfg_fbitmsk_ena_Mask                                                                 cBit17
#define cDs1_upen_fbitcfg_fbitmsk_ena_Shift                                                                    17

/*--------------------------------------
BitField Name: fbiterr_num
BitField Type: RW
BitField Desc: The Total FBIT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define cDs1_upen_fbitcfg_fbiterr_num_Mask                                                               cBit16_1
#define cDs1_upen_fbitcfg_fbiterr_num_Shift                                                                     1

/*--------------------------------------
BitField Name: fbitfrc_mod
BitField Type: RW
BitField Desc: Force FBIT Mode 0: One shot ( N events in T the unit time) 1:
continous (fbiterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define cDs1_upen_fbitcfg_fbitfrc_mod_Mask                                                                  cBit0
#define cDs1_upen_fbitcfg_fbitfrc_mod_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Force F Bit Error Status
Reg Addr   : 0x00092400-0x927FF
Reg Formula: 0x00092400 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This register is applicable to see the Force F Bit Error Status

------------------------------------------------------------------------------*/
#define cRegDs1_upen_fbitsta                                                                           0x00092400
#define cRegDs1_upen_fbitsta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: fbitsta_almset
BitField Type: RW
BitField Desc: FBIT alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define cDs1_upen_fbitsta_fbitsta_almset_Mask                                                              cBit27
#define cDs1_upen_fbitsta_fbitsta_almset_Shift                                                                 27

/*--------------------------------------
BitField Name: fbitstaclralrm
BitField Type: RW
BitField Desc: FBIT clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define cDs1_upen_fbitsta_fbitstaclralrm_Mask                                                           cBit22_15
#define cDs1_upen_fbitsta_fbitstaclralrm_Shift                                                                 15

/*--------------------------------------
BitField Name: fbitstasetalrm
BitField Type: RW
BitField Desc: FBIT Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define cDs1_upen_fbitsta_fbitstasetalrm_Mask                                                            cBit14_7
#define cDs1_upen_fbitsta_fbitstasetalrm_Shift                                                                  7

/*--------------------------------------
BitField Name: fbitstatimecnt
BitField Type: RW
BitField Desc: FBIT Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define cDs1_upen_fbitsta_fbitstatimecnt_Mask                                                             cBit6_0
#define cDs1_upen_fbitsta_fbitstatimecnt_Shift                                                                  0

/*--------------------------------------
BitField Name: fbitstafrc_ena
BitField Type: RW
BitField Desc: FBIT Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define cDs1_upen_fbitsta_fbitstafrc_ena_Mask                                                              cBit31
#define cDs1_upen_fbitsta_fbitstafrc_ena_Shift                                                                 31

/*--------------------------------------
BitField Name: fbitstaerrstep
BitField Type: RW
BitField Desc: The Second FBIT Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define cDs1_upen_fbitsta_fbitstaerrstep_Mask                                                           cBit30_23
#define cDs1_upen_fbitsta_fbitstaerrstep_Shift                                                                 23

/*--------------------------------------
BitField Name: fbitstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask FBIT Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define cDs1_upen_fbitsta_fbitstamsk_pos_Mask                                                           cBit22_19
#define cDs1_upen_fbitsta_fbitstamsk_pos_Shift                                                                 19

/*--------------------------------------
BitField Name: fbitstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error FBIT Status
BitField Bits: [18:3]
--------------------------------------*/
#define cDs1_upen_fbitsta_fbitstaerr_cnt_Mask                                                            cBit18_3
#define cDs1_upen_fbitsta_fbitstaerr_cnt_Shift                                                                  3

/*--------------------------------------
BitField Name: fbitstafrc_sta
BitField Type: RW
BitField Desc: FBIT State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define cDs1_upen_fbitsta_fbitstafrc_sta_Mask                                                             cBit2_1
#define cDs1_upen_fbitsta_fbitstafrc_sta_Shift                                                                  1

/*--------------------------------------
BitField Name: fbitstaoneunit
BitField Type: RW
BitField Desc: Force FBIT in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define cDs1_upen_fbitsta_fbitstaoneunit_Mask                                                               cBit0
#define cDs1_upen_fbitsta_fbitstaoneunit_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force CRC Bit Error Configuration
Reg Addr   : 0x00092800-0x92BFF
Reg Formula: 0x00092800 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This register is applicable to configure Force CRC Bit Error

------------------------------------------------------------------------------*/
#define cRegDs1_upen_crc_cfg                                                                           0x00092800
#define cRegDs1_upen_crc_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: crctim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define cDs1_upen_crc_cfg_crctim_mod_Mask                                                               cBit13_12
#define cDs1_upen_crc_cfg_crctim_mod_Shift                                                                     12

/*--------------------------------------
BitField Name: crcerr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define cDs1_upen_crc_cfg_crcerr_clr_Mask                                                                  cBit11
#define cDs1_upen_crc_cfg_crcerr_clr_Shift                                                                     11

/*--------------------------------------
BitField Name: crcerr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define cDs1_upen_crc_cfg_crcerr_set_Mask                                                                  cBit10
#define cDs1_upen_crc_cfg_crcerr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: crcerr_step
BitField Type: RW
BitField Desc: The Total Step to Force CRC Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define cDs1_upen_crc_cfg_crcerr_step_Mask                                                                cBit9_2
#define cDs1_upen_crc_cfg_crcerr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: crcmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for CRC Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define cDs1_upen_crc_cfg_crcmsk_pos_Mask_01                                                            cBit31_18
#define cDs1_upen_crc_cfg_crcmsk_pos_Shift_01                                                                  18
#define cDs1_upen_crc_cfg_crcmsk_pos_Mask_02                                                              cBit1_0
#define cDs1_upen_crc_cfg_crcmsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: crcmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For  CRC Congiguration
BitField Bits: [17]
--------------------------------------*/
#define cDs1_upen_crc_cfg_crcmsk_ena_Mask                                                                  cBit17
#define cDs1_upen_crc_cfg_crcmsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: crcerr_num
BitField Type: RW
BitField Desc: The Total CRC Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define cDs1_upen_crc_cfg_crcerr_num_Mask                                                                cBit16_1
#define cDs1_upen_crc_cfg_crcerr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: crcfrc_mod
BitField Type: RW
BitField Desc: Force CRC Mode 0: One shot 1: continous (crcerr_num = 16 bit
FFFF)
BitField Bits: [0]
--------------------------------------*/
#define cDs1_upen_crc_cfg_crcfrc_mod_Mask                                                                   cBit0
#define cDs1_upen_crc_cfg_crcfrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force CRC Bit Error Status
Reg Addr   : : 0x00092C00-0x92FFF
Reg Formula: : 0x00092C00 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This register is applicable to see the Force CRC Bit Error Status

------------------------------------------------------------------------------*/
#define cRegDs1_upen_cpb_sta                                                                         : 0x00092C00
#define cRegDs1_upen_cpb_sta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: cpbsta_almset
BitField Type: RW
BitField Desc: CRC bit alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define cDs1_upen_cpb_sta_cpbsta_almset_Mask                                                                cBit9
#define cDs1_upen_cpb_sta_cpbsta_almset_Shift                                                                   9

/*--------------------------------------
BitField Name: cpbstaclralrm
BitField Type: RW
BitField Desc: CRC bit clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define cDs1_upen_cpb_sta_cpbstaclralrm_Mask                                                                cBit8
#define cDs1_upen_cpb_sta_cpbstaclralrm_Shift                                                                   8

/*--------------------------------------
BitField Name: cpbstasetalrm
BitField Type: RW
BitField Desc: CRC bit Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define cDs1_upen_cpb_sta_cpbstasetalrm_Mask                                                                cBit7
#define cDs1_upen_cpb_sta_cpbstasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: cpbstatimecnt
BitField Type: RW
BitField Desc: CRC bit Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define cDs1_upen_cpb_sta_cpbstatimecnt_Mask                                                              cBit6_0
#define cDs1_upen_cpb_sta_cpbstatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: cpbstafrc_ena
BitField Type: RW
BitField Desc: CRC bit Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define cDs1_upen_cpb_sta_cpbstafrc_ena_Mask                                                               cBit31
#define cDs1_upen_cpb_sta_cpbstafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: cpbstaerrstep
BitField Type: RW
BitField Desc: The Second CRC bit Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define cDs1_upen_cpb_sta_cpbstaerrstep_Mask                                                            cBit30_23
#define cDs1_upen_cpb_sta_cpbstaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: cpbstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask CRC bit Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define cDs1_upen_cpb_sta_cpbstamsk_pos_Mask                                                            cBit22_19
#define cDs1_upen_cpb_sta_cpbstamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: cpbstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error CRC bit Status
BitField Bits: [18:3]
--------------------------------------*/
#define cDs1_upen_cpb_sta_cpbstaerr_cnt_Mask                                                             cBit18_3
#define cDs1_upen_cpb_sta_cpbstaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: cpbstafrc_sta
BitField Type: RW
BitField Desc: CRC bit State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define cDs1_upen_cpb_sta_cpbstafrc_sta_Mask                                                              cBit2_1
#define cDs1_upen_cpb_sta_cpbstafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: cpbstaoneunit
BitField Type: RW
BitField Desc: Force CRC bit in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define cDs1_upen_cpb_sta_cpbstaoneunit_Mask                                                                cBit0
#define cDs1_upen_cpb_sta_cpbstaoneunit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Fbit Gap Configuration
Reg Addr   : 0x90008-90008
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure Fbit Gap

------------------------------------------------------------------------------*/
#define cRegDs1_upen_fbitgap_cfg                                                                          0x90008

/*--------------------------------------
BitField Name: fbitgap
BitField Type: RW
BitField Desc: F bit Gap Configuration
BitField Bits: [31:0]
--------------------------------------*/
#define cDs1_upen_fbitgap_cfg_fbitgap_Mask                                                               cBit31_0
#define cDs1_upen_fbitgap_cfg_fbitgap_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : F Bit Counter Status
Reg Addr   : 0x97C00-97FFF
Reg Formula: 0x97C00 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This register is applicable to see the  F bit Counter Status

------------------------------------------------------------------------------*/
#define cRegDs1_upen_fbit_sta                                                                             0x97C00

/*--------------------------------------
BitField Name: nfas_enb
BitField Type: RW
BitField Desc: Force NFAS Enable
BitField Bits: [31]
--------------------------------------*/
#define cDs1_upen_fbit_sta_nfas_enb_Mask                                                                   cBit31
#define cDs1_upen_fbit_sta_nfas_enb_Shift                                                                      31

/*--------------------------------------
BitField Name: fas_enb
BitField Type: RW
BitField Desc: Force FAS Enable
BitField Bits: [30]
--------------------------------------*/
#define cDs1_upen_fbit_sta_fas_enb_Mask                                                                    cBit30
#define cDs1_upen_fbit_sta_fas_enb_Shift                                                                       30

/*--------------------------------------
BitField Name: fbitcntsta
BitField Type: RW
BitField Desc: F bit Counter Status
BitField Bits: [29:0]
--------------------------------------*/
#define cDs1_upen_fbit_sta_fbitcntsta_Mask                                                               cBit29_0
#define cDs1_upen_fbit_sta_fbitcntsta_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : CRC Gap Configuration
Reg Addr   : 0x90007-90007
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure CRC Gap

------------------------------------------------------------------------------*/
#define cRegDs1_upen_crcgap_cfg                                                                           0x90007

/*--------------------------------------
BitField Name: crcgap
BitField Type: RW
BitField Desc: CRC Gap Configuration
BitField Bits: [31:0]
--------------------------------------*/
#define cDs1_upen_crcgap_cfg_crcgap_Mask                                                                 cBit31_0
#define cDs1_upen_crcgap_cfg_crcgap_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : CRC Bit Counter Status
Reg Addr   : 0x97800 - 0x00797Bff
Reg Formula: 0x97800 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This register is applicable to see the  CRC bit Counter Status

------------------------------------------------------------------------------*/
#define cRegDs1_upen_crc_gap                                                                              0x97800

/*--------------------------------------
BitField Name: crccntsta
BitField Type: RW
BitField Desc: CRC bit Counter Status
BitField Bits: [31:0]
--------------------------------------*/
#define cDs1_upen_crc_gap_crccntsta_Mask                                                                 cBit31_0
#define cDs1_upen_crc_gap_crccntsta_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Force RAI Alarm Configuration
Reg Addr   : 0x00093000-0x933FF
Reg Formula: 0x00093000 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This register is applicable to configure Force RAI Error force DS1/E1 Frame

------------------------------------------------------------------------------*/
#define cRegDs1_upen_rai_cfg                                                                           0x00093000
#define cRegDs1_upen_rai_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: raitim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define cDs1_upen_rai_cfg_raitim_mod_Mask                                                               cBit27_26
#define cDs1_upen_rai_cfg_raitim_mod_Shift                                                                     26

/*--------------------------------------
BitField Name: raierr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define cDs1_upen_rai_cfg_raierr_clr_Mask                                                               cBit25_18
#define cDs1_upen_rai_cfg_raierr_clr_Shift                                                                     18

/*--------------------------------------
BitField Name: raierr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cDs1_upen_rai_cfg_raierr_set_Mask                                                               cBit17_10
#define cDs1_upen_rai_cfg_raierr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: raierr_step
BitField Type: RW
BitField Desc: The Total Step to Force RAI Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define cDs1_upen_rai_cfg_raierr_step_Mask                                                                cBit9_2
#define cDs1_upen_rai_cfg_raierr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: raimsk_pos
BitField Type: RW
BitField Desc: The Position Mask for RAI Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define cDs1_upen_rai_cfg_raimsk_pos_Mask_01                                                            cBit31_18
#define cDs1_upen_rai_cfg_raimsk_pos_Shift_01                                                                  18
#define cDs1_upen_rai_cfg_raimsk_pos_Mask_02                                                              cBit1_0
#define cDs1_upen_rai_cfg_raimsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: raimsk_ena
BitField Type: RW
BitField Desc: The Data Mask For RAI Congiguration
BitField Bits: [17]
--------------------------------------*/
#define cDs1_upen_rai_cfg_raimsk_ena_Mask                                                                  cBit17
#define cDs1_upen_rai_cfg_raimsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: raierr_num
BitField Type: RW
BitField Desc: The Total RAI Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define cDs1_upen_rai_cfg_raierr_num_Mask                                                                cBit16_1
#define cDs1_upen_rai_cfg_raierr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: raifrc_mod
BitField Type: RW
BitField Desc: Force RAI Mode 0: One shot ( N events in T the unit time) 1:
continous (raierr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define cDs1_upen_rai_cfg_raifrc_mod_Mask                                                                   cBit0
#define cDs1_upen_rai_cfg_raifrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force RAI Alarm Status
Reg Addr   : 0x00093400-0x937FF
Reg Formula: 0x00093400 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This register is applicable to see the Force RAI Error Status

------------------------------------------------------------------------------*/
#define cRegDs1_upen_rai_sta                                                                           0x00093400
#define cRegDs1_upen_rai_sta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: raista_almset
BitField Type: RW
BitField Desc: RAI alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define cDs1_upen_rai_sta_raista_almset_Mask                                                               cBit27
#define cDs1_upen_rai_sta_raista_almset_Shift                                                                  27

/*--------------------------------------
BitField Name: raistaclralrm
BitField Type: RW
BitField Desc: RAI clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define cDs1_upen_rai_sta_raistaclralrm_Mask                                                            cBit22_15
#define cDs1_upen_rai_sta_raistaclralrm_Shift                                                                  15

/*--------------------------------------
BitField Name: raistasetalrm
BitField Type: RW
BitField Desc: RAI Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define cDs1_upen_rai_sta_raistasetalrm_Mask                                                             cBit14_7
#define cDs1_upen_rai_sta_raistasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: raistatimecnt
BitField Type: RW
BitField Desc: RAI Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define cDs1_upen_rai_sta_raistatimecnt_Mask                                                              cBit6_0
#define cDs1_upen_rai_sta_raistatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: raistafrc_ena
BitField Type: RW
BitField Desc: RAI Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define cDs1_upen_rai_sta_raistafrc_ena_Mask                                                               cBit31
#define cDs1_upen_rai_sta_raistafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: raistaerrstep
BitField Type: RW
BitField Desc: The Second RAI Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define cDs1_upen_rai_sta_raistaerrstep_Mask                                                            cBit30_23
#define cDs1_upen_rai_sta_raistaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: raistamsk_pos
BitField Type: RW
BitField Desc: The Position Mask RAI Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define cDs1_upen_rai_sta_raistamsk_pos_Mask                                                            cBit22_19
#define cDs1_upen_rai_sta_raistamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: raistaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error RAI Status
BitField Bits: [18:3]
--------------------------------------*/
#define cDs1_upen_rai_sta_raistaerr_cnt_Mask                                                             cBit18_3
#define cDs1_upen_rai_sta_raistaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: raistafrc_sta
BitField Type: RW
BitField Desc: RAI State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define cDs1_upen_rai_sta_raistafrc_sta_Mask                                                              cBit2_1
#define cDs1_upen_rai_sta_raistafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: raistaoneunit
BitField Type: RW
BitField Desc: Force RAI in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define cDs1_upen_rai_sta_raistaoneunit_Mask                                                                cBit0
#define cDs1_upen_rai_sta_raistaoneunit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force LOS Error Configuration
Reg Addr   : 0x00093800-0x93bFF
Reg Formula: 0x00093800 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This register is applicable to configure Force LOS Error force DS1/E1 Frame

------------------------------------------------------------------------------*/
#define cRegDs1_upen_los_cfg                                                                           0x00093800
#define cRegDs1_upen_los_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: lostim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define cDs1_upen_los_cfg_lostim_mod_Mask                                                               cBit27_26
#define cDs1_upen_los_cfg_lostim_mod_Shift                                                                     26

/*--------------------------------------
BitField Name: loserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define cDs1_upen_los_cfg_loserr_clr_Mask                                                               cBit25_18
#define cDs1_upen_los_cfg_loserr_clr_Shift                                                                     18

/*--------------------------------------
BitField Name: loserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define cDs1_upen_los_cfg_loserr_set_Mask                                                               cBit17_10
#define cDs1_upen_los_cfg_loserr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: loserr_step
BitField Type: RW
BitField Desc: The Total Step to Force LOS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define cDs1_upen_los_cfg_loserr_step_Mask                                                                cBit9_2
#define cDs1_upen_los_cfg_loserr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: losmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define cDs1_upen_los_cfg_losmsk_pos_Mask_01                                                            cBit31_18
#define cDs1_upen_los_cfg_losmsk_pos_Shift_01                                                                  18
#define cDs1_upen_los_cfg_losmsk_pos_Mask_02                                                              cBit1_0
#define cDs1_upen_los_cfg_losmsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: losmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define cDs1_upen_los_cfg_losmsk_ena_Mask                                                                  cBit17
#define cDs1_upen_los_cfg_losmsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: loserr_num
BitField Type: RW
BitField Desc: The Total LOS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define cDs1_upen_los_cfg_loserr_num_Mask                                                                cBit16_1
#define cDs1_upen_los_cfg_loserr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: losfrc_mod
BitField Type: RW
BitField Desc: Force LOS Mode 0: One shot ( N events in T the unit time) 1:
continous (loserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define cDs1_upen_los_cfg_losfrc_mod_Mask                                                                   cBit0
#define cDs1_upen_los_cfg_losfrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force LOS Error Status
Reg Addr   : 0x00093c00-0x93FFF
Reg Formula: 0x00093c00 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This register is applicable to see the Force LOS Error Status

------------------------------------------------------------------------------*/
#define cRegDs1_upen_los_sta                                                                           0x00093c00
#define cRegDs1_upen_los_sta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: LOSsta_almset
BitField Type: RW
BitField Desc: LOS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define cDs1_upen_los_sta_LOSsta_almset_Mask                                                               cBit27
#define cDs1_upen_los_sta_LOSsta_almset_Shift                                                                  27

/*--------------------------------------
BitField Name: LOSstaclralrm
BitField Type: RW
BitField Desc: LOS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define cDs1_upen_los_sta_LOSstaclralrm_Mask                                                            cBit22_15
#define cDs1_upen_los_sta_LOSstaclralrm_Shift                                                                  15

/*--------------------------------------
BitField Name: LOSstasetalrm
BitField Type: RW
BitField Desc: LOS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define cDs1_upen_los_sta_LOSstasetalrm_Mask                                                             cBit14_7
#define cDs1_upen_los_sta_LOSstasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: LOSstatimecnt
BitField Type: RW
BitField Desc: LOS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define cDs1_upen_los_sta_LOSstatimecnt_Mask                                                              cBit6_0
#define cDs1_upen_los_sta_LOSstatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: LOSstafrc_ena
BitField Type: RW
BitField Desc: LOS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define cDs1_upen_los_sta_LOSstafrc_ena_Mask                                                               cBit31
#define cDs1_upen_los_sta_LOSstafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: LOSstaerrstep
BitField Type: RW
BitField Desc: The Second LOS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define cDs1_upen_los_sta_LOSstaerrstep_Mask                                                            cBit30_23
#define cDs1_upen_los_sta_LOSstaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: LOSstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask LOS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define cDs1_upen_los_sta_LOSstamsk_pos_Mask                                                            cBit22_19
#define cDs1_upen_los_sta_LOSstamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: LOSstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error LOS Status
BitField Bits: [18:3]
--------------------------------------*/
#define cDs1_upen_los_sta_LOSstaerr_cnt_Mask                                                             cBit18_3
#define cDs1_upen_los_sta_LOSstaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: LOSstafrc_sta
BitField Type: RW
BitField Desc: LOS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define cDs1_upen_los_sta_LOSstafrc_sta_Mask                                                              cBit2_1
#define cDs1_upen_los_sta_LOSstafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: LOSstaoneunit
BitField Type: RW
BitField Desc: Force LOS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define cDs1_upen_los_sta_LOSstaoneunit_Mask                                                                cBit0
#define cDs1_upen_los_sta_LOSstaoneunit_Shift                                                                   0

#endif /* _THA6A290011PDHDE1ATTREG_H_ */

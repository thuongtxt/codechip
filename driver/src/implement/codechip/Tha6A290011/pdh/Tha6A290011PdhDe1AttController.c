/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : Tha6A290011PdhDe1AttController.c
 *
 * Created Date: Sep 16, 2017
 *
 * Description : DE1 ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaPdhDe2De1.h"
#include "../../Tha6A210031/pdh/Tha6A210031PdhAttControllerInternal.h"
#include "Tha6A290011PdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttControllerMethods                m_AtAttControllerOverride;
static tThaAttControllerMethods               m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;
static tTha6A210031PdhDe1AttControllerMethods m_Tha6A210031PdhDe1AttControllerOverride;

/* Save super implementation */
static const tAtAttControllerMethods       *m_AtAttControllerMethods       = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 StartVersionSupportStep16bit(ThaAttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0012);
    }
static uint32 StartVersionSupportTwoCRCForce(Tha6A210031PdhDe1AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    }

static uint32 StartVersionSupportMfas(Tha6A210031PdhDe1AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0010);
    }

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x2, 0x2222);
    }

static eAtRet De1CrcGapSet(AtAttController self, uint16 frameType)
    {
    /*Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    AtChannel channel = AtAttControllerChannelGet(self);
    uint32   regAddr = mMethodsGet(controller)->RealAddress(controller, 0x97800);
    AtUnused(frameType);
    mChannelHwWrite(channel, regAddr, 0, cAtModulePdh);*/
    AtUnused(self);
    AtUnused(frameType);
    return cAtOk;
    }

static uint32 SignalingOffset(Tha6A210031PdhDe1AttController self, AtPdhChannel channel, uint8 tsId)
    {
    uint8 slice, hwSts;
    uint8 hwDe2Id, hwDe1Id;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)channel);
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet(channel);
    AtUnused(self);
    if (vc != NULL)
        {
        uint32 tug2Id = (uint32)AtSdhChannelTug2Get(vc);
        uint32 tu1xId = (uint32)AtSdhChannelTu1xGet(vc);
        uint32 flatIdInSts = 0;
        if  (AtPdhDe1IsE1((AtPdhDe1)channel))
            flatIdInSts =  tug2Id* 96UL + tu1xId * 32UL;
        else
            flatIdInSts =  tug2Id * 96UL + tu1xId * 24UL;
        ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice, &hwSts);
        return ThaModulePdhSliceBase(pdhModule, slice) + hwSts * 672UL + flatIdInSts + tsId;
        }

    ThaPdhDe2De1HwIdGet((ThaPdhDe1)channel, &slice, &hwSts, &hwDe2Id, &hwDe1Id, cAtModulePdh);
    if  (AtPdhDe1IsE1((AtPdhDe1)channel))
        return ThaModulePdhSliceBase(pdhModule, slice) + hwSts * 672UL + hwDe2Id * 96UL + hwDe1Id * 32UL + tsId;
    return ThaModulePdhSliceBase(pdhModule, slice) + hwSts * 672UL + hwDe2Id * 96UL + hwDe1Id * 24UL + tsId;
    }

static uint32 SignalingDumpRealAddressVal(Tha6A210031PdhDe1AttController self, uint8 tsId)
    {
    uint8 slice, hwSts;
    uint8 hwDe2Id, hwDe1Id;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet((AtAttController)self);
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet(channel);

    if (vc != NULL)
        {
        uint32 tug2Id = (uint32)AtSdhChannelTug2Get(vc);
        uint32 tu1xId = (uint32)AtSdhChannelTu1xGet(vc);
        uint32 flatIdInSts=0;
        if  (AtPdhDe1IsE1((AtPdhDe1)channel))
            flatIdInSts = tug2Id * 96UL + tu1xId*32UL;
        else
            flatIdInSts = tug2Id * 96UL + tu1xId*24UL;
        ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice, &hwSts);
        return hwSts * 672UL + flatIdInSts + tsId;
        }

    ThaPdhDe2De1HwIdGet((ThaPdhDe1)channel, &slice, &hwSts, &hwDe2Id, &hwDe1Id, cAtModulePdh);
    if  (AtPdhDe1IsE1((AtPdhDe1)channel))
        return hwSts * 672UL + hwDe2Id * 96UL + hwDe1Id*32UL + tsId;
    return hwSts * 672UL + hwDe2Id * 96UL + hwDe1Id*24UL + tsId;
    }

static uint32 RealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwSts;
    uint8 hwDe2Id, hwDe1Id;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)channel);
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet(channel);

    if (vc != NULL)
        {
        uint32 tug2Id = (uint32)AtSdhChannelTug2Get(vc);
        uint32 tu1xId = (uint32)AtSdhChannelTu1xGet(vc);
        uint32 flatIdInSts = tug2Id * 4 + tu1xId;
        ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice, &hwSts);
        return regAddr + ThaModulePdhSliceBase(pdhModule, slice) + hwSts * 32UL + flatIdInSts;
        }

    ThaPdhDe2De1HwIdGet((ThaPdhDe1)channel, &slice, &hwSts, &hwDe2Id, &hwDe1Id, cAtModulePdh);
    return regAddr + ThaModulePdhSliceBase(pdhModule, slice) + hwSts * 32UL + hwDe2Id * 4UL + hwDe1Id;
    }

static eAtRet RxSignalingEnable(AtAttController self, uint8 tsId, eBool enable)
    {
    /* Reconfigure E1 Ds0Mask */
    AtChannel channel = AtAttControllerChannelGet(self);
    uint32 tsIdSw;
    tsIdSw = AtPdhDe1SignalingDs0MaskGet((AtPdhDe1)channel);
    mFieldIns(&tsIdSw, cBit0 << tsId, tsId, enable ? 1 : 0);
    AtPdhDe1SignalingDs0MaskSet((AtPdhDe1)channel, tsIdSw);

    return m_AtAttControllerMethods->RxSignalingEnable(self, tsId, enable);
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, mMethodsGet(self), sizeof(m_AtAttControllerOverride));
        mMethodOverride(m_AtAttControllerOverride, De1CrcGapSet);
        mMethodOverride(m_AtAttControllerOverride, RxSignalingEnable);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportStep16bit);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void OverrideTha6A210031PdhDe1AttController(Tha6A210031PdhDe1AttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe1AttControllerOverride, mMethodsGet(self), sizeof(m_Tha6A210031PdhDe1AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe1AttControllerOverride, StartVersionSupportMfas);
        mMethodOverride(m_Tha6A210031PdhDe1AttControllerOverride, StartVersionSupportTwoCRCForce);
        mMethodOverride(m_Tha6A210031PdhDe1AttControllerOverride, SignalingDumpRealAddressVal);
        mMethodOverride(m_Tha6A210031PdhDe1AttControllerOverride, SignalingOffset);
        }

    mMethodsSet(self, &m_Tha6A210031PdhDe1AttControllerOverride);
    }


static void OverrideTha6A210031PdhDe3AttController(AtAttController self)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(controller), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        }

    mMethodsSet(controller, &m_Tha6A210031PdhDe3AttControllerOverride);
    }


static void Override(AtAttController self)
    {
    OverrideAtAttController((AtAttController) self);
    OverrideThaAttController((ThaAttController) self);
    OverrideTha6A210031PdhDe3AttController(self);
    OverrideTha6A210031PdhDe1AttController((Tha6A210031PdhDe1AttController) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011PdhDe1AttController);
    }

AtAttController Tha6A290011PdhDe1AttControllerObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031PdhDe1AttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290011PdhDe1AttControllerNew(AtChannel de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A290011PdhDe1AttControllerObjectInit(controller, de1);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A290011AttPdhDe3.c
 *
 * Created Date: Sep 15, 2017
 *
 * Description : ATT PDH DE3 controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/att/ThaAttController.h"
#include "Tha6A290011PdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
/*static tAtAttControllerMethods                m_AtAttControllerOverride;*/
static tTha6A210031PdhDe3AttControllerMethods    m_Tha6A210031PdhDe3AttControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x2, 0x2222);
    }

static uint32 StartVersionSupportE3SameAsDs3(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0010);
    }

static void OverrideTha6A210031PdhDe3AttController(Tha6A210031PdhDe3AttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(self), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportE3SameAsDs3);
        }

    mMethodsSet(self, &m_Tha6A210031PdhDe3AttControllerOverride);
    }


static void Override(AtAttController self)
    {
    AtUnused(self);
    OverrideTha6A210031PdhDe3AttController((Tha6A210031PdhDe3AttController)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011PdhDe3AttController);
    }

AtAttController Tha6A290011PdhDe3AttControllerObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031PdhDe3AttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290011PdhDe3AttControllerNew(AtChannel de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A290011PdhDe3AttControllerObjectInit(controller, de3);
    }

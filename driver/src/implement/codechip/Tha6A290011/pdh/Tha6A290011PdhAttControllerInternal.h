/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A290011AttControllerInternal.h
 * 
 * Created Date: Sep 17, 2016
 *
 * Description : ATT controller header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011PDHATTCONTROLLERINTERNAL_H_
#define _THA6A290011PDHATTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A210031/pdh/Tha6A210031PdhAttControllerInternal.h"
#include "Tha6A290011ModulePdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290011PdhDe3AttController
    {
    tTha6A210031PdhDe3AttController super;
    }tTha6A290011PdhDe3AttController;

typedef struct tTha6A290011PdhDe2AttController
    {
    tTha6A210031PdhDe3AttController super;
    }tTha6A290011PdhDe2AttController;


typedef struct tTha6A290011PdhDe1AttController
    {
    tTha6A210031PdhDe1AttController super;
    }tTha6A290011PdhDe1AttController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttController Tha6A290011PdhDe3AttControllerNew(AtChannel de3);
AtAttController Tha6A290011PdhDe2AttControllerNew(AtChannel de2);
AtAttController Tha6A290011PdhDe1AttControllerNew(AtChannel de1);
AtAttController Tha6A290011PdhDe3AttControllerObjectInit(AtAttController self, AtChannel channel);
AtAttController Tha6A290011PdhDe2AttControllerObjectInit(AtAttController self, AtChannel channel);
AtAttController Tha6A290011PdhDe1AttControllerObjectInit(AtAttController self, AtChannel channel);

eBool Tha6A290011AttPdhForceLogicFromDut(AtChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290011PDHATTCONTROLLERINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date: August 3, 2018                                                 
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _THA6A290011PDHDE3ATTREG_H_
#define _THA6A290011PDHDE3ATTREG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Force F Bit Error Configuration
Reg Addr   : 0x00080800-0x00080817
Reg Formula: 0x00080800 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force F Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_fbitcfg                                                                           0x00080800
#define cReg_upen_fbitcfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: fbittim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_fbitcfg_fbittim_mod_Mask                                                              cBit27_26
#define c_upen_fbitcfg_fbittim_mod_Shift                                                                    26

/*--------------------------------------
BitField Name: fbiterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_fbitcfg_fbiterr_clr_Mask                                                              cBit25_18
#define c_upen_fbitcfg_fbiterr_clr_Shift                                                                    18

/*--------------------------------------
BitField Name: fbiterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_fbitcfg_fbiterr_set_Mask                                                              cBit17_10
#define c_upen_fbitcfg_fbiterr_set_Shift                                                                    10

/*--------------------------------------
BitField Name: fbiterr_step
BitField Type: RW
BitField Desc: The Total Step to Force FBIT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_fbitcfg_fbiterr_step_Mask                                                               cBit9_2
#define c_upen_fbitcfg_fbiterr_step_Shift                                                                    2

/*--------------------------------------
BitField Name: fbitmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for FBIT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_fbitcfg_fbitmsk_pos_Mask_01                                                           cBit31_18
#define c_upen_fbitcfg_fbitmsk_pos_Shift_01                                                                 18
#define c_upen_fbitcfg_fbitmsk_pos_Mask_02                                                             cBit1_0
#define c_upen_fbitcfg_fbitmsk_pos_Shift_02                                                                  0

/*--------------------------------------
BitField Name: fbitmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For FBIT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_fbitcfg_fbitmsk_ena_Mask                                                                 cBit17
#define c_upen_fbitcfg_fbitmsk_ena_Shift                                                                    17

/*--------------------------------------
BitField Name: fbiterr_num
BitField Type: RW
BitField Desc: The Total FBIT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_fbitcfg_fbiterr_num_Mask                                                               cBit16_1
#define c_upen_fbitcfg_fbiterr_num_Shift                                                                     1

/*--------------------------------------
BitField Name: fbitfrc_mod
BitField Type: RW
BitField Desc: Force FBIT Mode 0: One shot ( N events in T the unit time) 1:
continous (fbiterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_fbitcfg_fbitfrc_mod_Mask                                                                  cBit0
#define c_upen_fbitcfg_fbitfrc_mod_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Force F Bit Error Status
Reg Addr   : 0x00080820-0x00080837
Reg Formula: 0x00080820 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to see the Force F Bit Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_fbitsta                                                                           0x00080820
#define cReg_upen_fbitsta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: fbitsta_almset
BitField Type: RW
BitField Desc: FBIT alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_fbitsta_fbitsta_almset_Mask                                                              cBit27
#define c_upen_fbitsta_fbitsta_almset_Shift                                                                 27

/*--------------------------------------
BitField Name: fbitstaclralrm
BitField Type: RW
BitField Desc: FBIT clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_fbitsta_fbitstaclralrm_Mask                                                           cBit22_15
#define c_upen_fbitsta_fbitstaclralrm_Shift                                                                 15

/*--------------------------------------
BitField Name: fbitstasetalrm
BitField Type: RW
BitField Desc: FBIT Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_fbitsta_fbitstasetalrm_Mask                                                            cBit14_7
#define c_upen_fbitsta_fbitstasetalrm_Shift                                                                  7

/*--------------------------------------
BitField Name: fbitstatimecnt
BitField Type: RW
BitField Desc: FBIT Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_fbitsta_fbitstatimecnt_Mask                                                             cBit6_0
#define c_upen_fbitsta_fbitstatimecnt_Shift                                                                  0

/*--------------------------------------
BitField Name: fbitstafrc_ena
BitField Type: RW
BitField Desc: FBIT Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_fbitsta_fbitstafrc_ena_Mask                                                              cBit31
#define c_upen_fbitsta_fbitstafrc_ena_Shift                                                                 31

/*--------------------------------------
BitField Name: fbitstaerrstep
BitField Type: RW
BitField Desc: The Second FBIT Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_fbitsta_fbitstaerrstep_Mask                                                           cBit30_23
#define c_upen_fbitsta_fbitstaerrstep_Shift                                                                 23

/*--------------------------------------
BitField Name: fbitstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask FBIT Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_fbitsta_fbitstamsk_pos_Mask                                                           cBit22_19
#define c_upen_fbitsta_fbitstamsk_pos_Shift                                                                 19

/*--------------------------------------
BitField Name: fbitstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error FBIT Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_fbitsta_fbitstaerr_cnt_Mask                                                            cBit18_3
#define c_upen_fbitsta_fbitstaerr_cnt_Shift                                                                  3

/*--------------------------------------
BitField Name: fbitstafrc_sta
BitField Type: RW
BitField Desc: FBIT State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_fbitsta_fbitstafrc_sta_Mask                                                             cBit2_1
#define c_upen_fbitsta_fbitstafrc_sta_Shift                                                                  1

/*--------------------------------------
BitField Name: fbitstaoneunit
BitField Type: RW
BitField Desc: Force FBIT in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_fbitsta_fbitstaoneunit_Mask                                                               cBit0
#define c_upen_fbitsta_fbitstaoneunit_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force CP Bit Error Configuration
Reg Addr   : 0x00080880-0x00080897
Reg Formula: 0x00080880 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force CP Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_cpb_cfg                                                                           0x00080880
#define cReg_upen_cpb_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: cpbtim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_cpb_cfg_cpbtim_mod_Mask                                                               cBit13_12
#define c_upen_cpb_cfg_cpbtim_mod_Shift                                                                     12

/*--------------------------------------
BitField Name: cpberr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_cpb_cfg_cpberr_clr_Mask                                                                  cBit11
#define c_upen_cpb_cfg_cpberr_clr_Shift                                                                     11

/*--------------------------------------
BitField Name: cpberr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_cpb_cfg_cpberr_set_Mask                                                                  cBit10
#define c_upen_cpb_cfg_cpberr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: cpberr_step
BitField Type: RW
BitField Desc: The Total Step to Force CP Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_cpb_cfg_cpberr_step_Mask                                                                cBit9_2
#define c_upen_cpb_cfg_cpberr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: cpbmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for CP Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_cpb_cfg_cpbmsk_pos_Mask_01                                                            cBit31_18
#define c_upen_cpb_cfg_cpbmsk_pos_Shift_01                                                                  18
#define c_upen_cpb_cfg_cpbmsk_pos_Mask_02                                                              cBit1_0
#define c_upen_cpb_cfg_cpbmsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: cpbmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For  CP Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_cpb_cfg_cpbmsk_ena_Mask                                                                  cBit17
#define c_upen_cpb_cfg_cpbmsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: cpberr_num
BitField Type: RW
BitField Desc: The Total CP Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_cpb_cfg_cpberr_num_Mask                                                                cBit16_1
#define c_upen_cpb_cfg_cpberr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: cpbfrc_mod
BitField Type: RW
BitField Desc: Force CP Mode 0: One shot 1: continous (cpberr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_cpb_cfg_cpbfrc_mod_Mask                                                                   cBit0
#define c_upen_cpb_cfg_cpbfrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force CP Bit Error Status
Reg Addr   : 0x000808a0-0x000808b7
Reg Formula: 0x000808a0 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to see the Force CP Bit Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_cpb_sta                                                                           0x000808a0
#define cReg_upen_cpb_sta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: cpbsta_almset
BitField Type: RW
BitField Desc: CP bit alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_cpb_sta_cpbsta_almset_Mask                                                                cBit9
#define c_upen_cpb_sta_cpbsta_almset_Shift                                                                   9

/*--------------------------------------
BitField Name: cpbstaclralrm
BitField Type: RW
BitField Desc: CP bit clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_cpb_sta_cpbstaclralrm_Mask                                                                cBit8
#define c_upen_cpb_sta_cpbstaclralrm_Shift                                                                   8

/*--------------------------------------
BitField Name: cpbstasetalrm
BitField Type: RW
BitField Desc: CP bit Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_cpb_sta_cpbstasetalrm_Mask                                                                cBit7
#define c_upen_cpb_sta_cpbstasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: cpbstatimecnt
BitField Type: RW
BitField Desc: CP bit Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_cpb_sta_cpbstatimecnt_Mask                                                              cBit6_0
#define c_upen_cpb_sta_cpbstatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: cpbstafrc_ena
BitField Type: RW
BitField Desc: CP bit Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_cpb_sta_cpbstafrc_ena_Mask                                                               cBit31
#define c_upen_cpb_sta_cpbstafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: cpbstaerrstep
BitField Type: RW
BitField Desc: The Second CP bit Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_cpb_sta_cpbstaerrstep_Mask                                                            cBit30_23
#define c_upen_cpb_sta_cpbstaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: cpbstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask CP bit Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_cpb_sta_cpbstamsk_pos_Mask                                                            cBit22_19
#define c_upen_cpb_sta_cpbstamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: cpbstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error CP bit Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_cpb_sta_cpbstaerr_cnt_Mask                                                             cBit18_3
#define c_upen_cpb_sta_cpbstaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: cpbstafrc_sta
BitField Type: RW
BitField Desc: CP bit State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_cpb_sta_cpbstafrc_sta_Mask                                                              cBit2_1
#define c_upen_cpb_sta_cpbstafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: cpbstaoneunit
BitField Type: RW
BitField Desc: Force CP bit in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_cpb_sta_cpbstaoneunit_Mask                                                                cBit0
#define c_upen_cpb_sta_cpbstaoneunit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI Bit Error Configuration
Reg Addr   : 0x00080900-0x00080917
Reg Formula: 0x00080900 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force RDI Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_rdi_cfg                                                                           0x00080900
#define cReg_upen_rdi_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: rditim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_rdi_cfg_rditim_mod_Mask                                                               cBit27_26
#define c_upen_rdi_cfg_rditim_mod_Shift                                                                     26

/*--------------------------------------
BitField Name: rdierr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_rdi_cfg_rdierr_clr_Mask                                                               cBit25_18
#define c_upen_rdi_cfg_rdierr_clr_Shift                                                                     18

/*--------------------------------------
BitField Name: rdierr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_rdi_cfg_rdierr_set_Mask                                                               cBit17_10
#define c_upen_rdi_cfg_rdierr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: rdierr_step
BitField Type: RW
BitField Desc: The Total Step to Force RDI Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_rdi_cfg_rdierr_step_Mask                                                                cBit9_2
#define c_upen_rdi_cfg_rdierr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: rdimsk_pos
BitField Type: RW
BitField Desc: The Position Mask for RDI Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_rdi_cfg_rdimsk_pos_Mask_01                                                            cBit31_18
#define c_upen_rdi_cfg_rdimsk_pos_Shift_01                                                                  18
#define c_upen_rdi_cfg_rdimsk_pos_Mask_02                                                              cBit1_0
#define c_upen_rdi_cfg_rdimsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: rdimsk_ena
BitField Type: RW
BitField Desc: The Data Mask For RDI Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_rdi_cfg_rdimsk_ena_Mask                                                                  cBit17
#define c_upen_rdi_cfg_rdimsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: rdierr_num
BitField Type: RW
BitField Desc: The Total RDI Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_rdi_cfg_rdierr_num_Mask                                                                cBit16_1
#define c_upen_rdi_cfg_rdierr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: rdifrc_mod
BitField Type: RW
BitField Desc: Force RDI Mode 0: One shot ( N events in T the unit time) 1:
continous (rdierr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_rdi_cfg_rdifrc_mod_Mask                                                                   cBit0
#define c_upen_rdi_cfg_rdifrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI Bit Error Status
Reg Addr   : 0x00080920-0x00080937
Reg Formula: 0x00080920 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to see the Force RDI Bit Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_rdi_sta                                                                           0x00080920
#define cReg_upen_rdi_sta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: rdista_almset
BitField Type: RW
BitField Desc: RDI alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_rdi_sta_rdista_almset_Mask                                                               cBit27
#define c_upen_rdi_sta_rdista_almset_Shift                                                                  27

/*--------------------------------------
BitField Name: rdistaclralrm
BitField Type: RW
BitField Desc: RDI clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_rdi_sta_rdistaclralrm_Mask                                                            cBit22_15
#define c_upen_rdi_sta_rdistaclralrm_Shift                                                                  15

/*--------------------------------------
BitField Name: rdistasetalrm
BitField Type: RW
BitField Desc: RDI Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_rdi_sta_rdistasetalrm_Mask                                                             cBit14_7
#define c_upen_rdi_sta_rdistasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: rdistatimecnt
BitField Type: RW
BitField Desc: RDI Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_rdi_sta_rdistatimecnt_Mask                                                              cBit6_0
#define c_upen_rdi_sta_rdistatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: rdistafrc_ena
BitField Type: RW
BitField Desc: RDI Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_rdi_sta_rdistafrc_ena_Mask                                                               cBit31
#define c_upen_rdi_sta_rdistafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: rdistaerrstep
BitField Type: RW
BitField Desc: The Second RDI Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_rdi_sta_rdistaerrstep_Mask                                                            cBit30_23
#define c_upen_rdi_sta_rdistaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: rdistamsk_pos
BitField Type: RW
BitField Desc: The Position Mask RDI Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_rdi_sta_rdistamsk_pos_Mask                                                            cBit22_19
#define c_upen_rdi_sta_rdistamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: rdistaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error RDI Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_rdi_sta_rdistaerr_cnt_Mask                                                             cBit18_3
#define c_upen_rdi_sta_rdistaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: rdistafrc_sta
BitField Type: RW
BitField Desc: RDI State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_rdi_sta_rdistafrc_sta_Mask                                                              cBit2_1
#define c_upen_rdi_sta_rdistafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: rdistaoneunit
BitField Type: RW
BitField Desc: Force RDI in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_rdi_sta_rdistaoneunit_Mask                                                                cBit0
#define c_upen_rdi_sta_rdistaoneunit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force P Bit Error Configuration
Reg Addr   : 0x00080a00-0x00080a17
Reg Formula: 0x00080a00 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force P Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_pbit_cfg                                                                          0x00080a00
#define cReg_upen_pbit_cfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: pbittim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_pbit_cfg_pbittim_mod_Mask                                                             cBit13_12
#define c_upen_pbit_cfg_pbittim_mod_Shift                                                                   12

/*--------------------------------------
BitField Name: pbiterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_pbit_cfg_pbiterr_clr_Mask                                                                cBit11
#define c_upen_pbit_cfg_pbiterr_clr_Shift                                                                   11

/*--------------------------------------
BitField Name: pbiterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_pbit_cfg_pbiterr_set_Mask                                                                cBit10
#define c_upen_pbit_cfg_pbiterr_set_Shift                                                                   10

/*--------------------------------------
BitField Name: pbiterr_step
BitField Type: RW
BitField Desc: The Total Step to Force P Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_pbit_cfg_pbiterr_step_Mask                                                              cBit9_2
#define c_upen_pbit_cfg_pbiterr_step_Shift                                                                   2

/*--------------------------------------
BitField Name: pbitmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for P Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_pbit_cfg_pbitmsk_pos_Mask_01                                                          cBit31_18
#define c_upen_pbit_cfg_pbitmsk_pos_Shift_01                                                                18
#define c_upen_pbit_cfg_pbitmsk_pos_Mask_02                                                            cBit1_0
#define c_upen_pbit_cfg_pbitmsk_pos_Shift_02                                                                 0

/*--------------------------------------
BitField Name: pbitmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For  P Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_pbit_cfg_pbitmsk_ena_Mask                                                                cBit17
#define c_upen_pbit_cfg_pbitmsk_ena_Shift                                                                   17

/*--------------------------------------
BitField Name: pbiterr_num
BitField Type: RW
BitField Desc: The Total P Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_pbit_cfg_pbiterr_num_Mask                                                              cBit16_1
#define c_upen_pbit_cfg_pbiterr_num_Shift                                                                    1

/*--------------------------------------
BitField Name: pbitfrc_mod
BitField Type: RW
BitField Desc: Force P Mode 0: One shot 1: continous (pbiterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_pbit_cfg_pbitfrc_mod_Mask                                                                 cBit0
#define c_upen_pbit_cfg_pbitfrc_mod_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Force P Bit Error Status
Reg Addr   : 0x00080a20-0x00080a37
Reg Formula: 0x00080a20 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to see the Force P Bit Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_pbit_sta                                                                          0x00080a20
#define cReg_upen_pbit_sta_WidthVal                                                                         64

/*--------------------------------------
BitField Name: pbitsta_almset
BitField Type: RW
BitField Desc: P bit alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_pbit_sta_pbitsta_almset_Mask                                                              cBit9
#define c_upen_pbit_sta_pbitsta_almset_Shift                                                                 9

/*--------------------------------------
BitField Name: pbitstaclralrm
BitField Type: RW
BitField Desc: P bit clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_pbit_sta_pbitstaclralrm_Mask                                                              cBit8
#define c_upen_pbit_sta_pbitstaclralrm_Shift                                                                 8

/*--------------------------------------
BitField Name: pbitstasetalrm
BitField Type: RW
BitField Desc: P bit Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_pbit_sta_pbitstasetalrm_Mask                                                              cBit7
#define c_upen_pbit_sta_pbitstasetalrm_Shift                                                                 7

/*--------------------------------------
BitField Name: pbitstatimecnt
BitField Type: RW
BitField Desc: P bit Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_pbit_sta_pbitstatimecnt_Mask                                                            cBit6_0
#define c_upen_pbit_sta_pbitstatimecnt_Shift                                                                 0

/*--------------------------------------
BitField Name: pbitstafrc_ena
BitField Type: RW
BitField Desc: P bit Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_pbit_sta_pbitstafrc_ena_Mask                                                             cBit31
#define c_upen_pbit_sta_pbitstafrc_ena_Shift                                                                31

/*--------------------------------------
BitField Name: pbitstaerrstep
BitField Type: RW
BitField Desc: The Second P bit Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_pbit_sta_pbitstaerrstep_Mask                                                          cBit30_23
#define c_upen_pbit_sta_pbitstaerrstep_Shift                                                                23

/*--------------------------------------
BitField Name: pbitstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask P bit Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_pbit_sta_pbitstamsk_pos_Mask                                                          cBit22_19
#define c_upen_pbit_sta_pbitstamsk_pos_Shift                                                                19

/*--------------------------------------
BitField Name: pbitstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error P bit Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_pbit_sta_pbitstaerr_cnt_Mask                                                           cBit18_3
#define c_upen_pbit_sta_pbitstaerr_cnt_Shift                                                                 3

/*--------------------------------------
BitField Name: pbitstafrc_sta
BitField Type: RW
BitField Desc: P bit State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_pbit_sta_pbitstafrc_sta_Mask                                                            cBit2_1
#define c_upen_pbit_sta_pbitstafrc_sta_Shift                                                                 1

/*--------------------------------------
BitField Name: pbitstaoneunit
BitField Type: RW
BitField Desc: Force P bit in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_pbit_sta_pbitstaoneunit_Mask                                                              cBit0
#define c_upen_pbit_sta_pbitstaoneunit_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force FA Byte Error Configuration
Reg Addr   : 0x00080a80-0x00080a97
Reg Formula: 0x00080a80 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force FA Byte Error

------------------------------------------------------------------------------*/
#define cReg_upen_faB_cfg                                                                           0x00080a80
#define cReg_upen_faB_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: fabtim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_faB_cfg_fabtim_mod_Mask                                                               cBit27_26
#define c_upen_faB_cfg_fabtim_mod_Shift                                                                     26

/*--------------------------------------
BitField Name: faberr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_faB_cfg_faberr_clr_Mask                                                               cBit25_18
#define c_upen_faB_cfg_faberr_clr_Shift                                                                     18

/*--------------------------------------
BitField Name: faberr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_faB_cfg_faberr_set_Mask                                                               cBit17_10
#define c_upen_faB_cfg_faberr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: faberr_step
BitField Type: RW
BitField Desc: The Total Step to Force FAB Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_faB_cfg_faberr_step_Mask                                                                cBit9_2
#define c_upen_faB_cfg_faberr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: fabmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for FAB Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_faB_cfg_fabmsk_pos_Mask_01                                                            cBit31_18
#define c_upen_faB_cfg_fabmsk_pos_Shift_01                                                                  18
#define c_upen_faB_cfg_fabmsk_pos_Mask_02                                                              cBit1_0
#define c_upen_faB_cfg_fabmsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: fabmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For FAB Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_faB_cfg_fabmsk_ena_Mask                                                                  cBit17
#define c_upen_faB_cfg_fabmsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: faberr_num
BitField Type: RW
BitField Desc: The Total FAB Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_faB_cfg_faberr_num_Mask                                                                cBit16_1
#define c_upen_faB_cfg_faberr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: fabfrc_mod
BitField Type: RW
BitField Desc: Force FAB Mode 0: One shot ( N events in T the unit time) 1:
continous (faberr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_faB_cfg_fabfrc_mod_Mask                                                                   cBit0
#define c_upen_faB_cfg_fabfrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force FA Byte Error Status
Reg Addr   : 0x00080aa0-0x00080ab7
Reg Formula: 0x00080aa0 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to see the Force FA Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_faB_sta                                                                           0x00080aa0
#define cReg_upen_faB_sta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: fabsta_almset
BitField Type: RW
BitField Desc: FAB alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_faB_sta_fabsta_almset_Mask                                                               cBit27
#define c_upen_faB_sta_fabsta_almset_Shift                                                                  27

/*--------------------------------------
BitField Name: fabstaclralrm
BitField Type: RW
BitField Desc: FAB clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_faB_sta_fabstaclralrm_Mask                                                            cBit22_15
#define c_upen_faB_sta_fabstaclralrm_Shift                                                                  15

/*--------------------------------------
BitField Name: fabstasetalrm
BitField Type: RW
BitField Desc: FAB Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_faB_sta_fabstasetalrm_Mask                                                             cBit14_7
#define c_upen_faB_sta_fabstasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: fabstatimecnt
BitField Type: RW
BitField Desc: FAB Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_faB_sta_fabstatimecnt_Mask                                                              cBit6_0
#define c_upen_faB_sta_fabstatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: fabstafrc_ena
BitField Type: RW
BitField Desc: FAB Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_faB_sta_fabstafrc_ena_Mask                                                               cBit31
#define c_upen_faB_sta_fabstafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: fabstaerrstep
BitField Type: RW
BitField Desc: The Second FAB Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_faB_sta_fabstaerrstep_Mask                                                            cBit30_23
#define c_upen_faB_sta_fabstaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: fabstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask FAB Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_faB_sta_fabstamsk_pos_Mask                                                            cBit22_19
#define c_upen_faB_sta_fabstamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: fabstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error FAB Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_faB_sta_fabstaerr_cnt_Mask                                                             cBit18_3
#define c_upen_faB_sta_fabstaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: fabstafrc_sta
BitField Type: RW
BitField Desc: FAB State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_faB_sta_fabstafrc_sta_Mask                                                              cBit2_1
#define c_upen_faB_sta_fabstafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: fabstaoneunit
BitField Type: RW
BitField Desc: Force FAB in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_faB_sta_fabstaoneunit_Mask                                                                cBit0
#define c_upen_faB_sta_fabstaoneunit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP8 Byte Error Configuration
Reg Addr   : 0x00080b00-0x00080b17
Reg Formula: 0x00080b00 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force BIP8 Byte Error

------------------------------------------------------------------------------*/
#define cReg_upen_bip8_cfg                                                                          0x00080b00
#define cReg_upen_bip8_cfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: biptim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_bip8_cfg_biptim_mod_Mask                                                              cBit13_12
#define c_upen_bip8_cfg_biptim_mod_Shift                                                                    12

/*--------------------------------------
BitField Name: biperr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_bip8_cfg_biperr_clr_Mask                                                                 cBit11
#define c_upen_bip8_cfg_biperr_clr_Shift                                                                    11

/*--------------------------------------
BitField Name: biperr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_bip8_cfg_biperr_set_Mask                                                                 cBit10
#define c_upen_bip8_cfg_biperr_set_Shift                                                                    10

/*--------------------------------------
BitField Name: biperr_step
BitField Type: RW
BitField Desc: The Total Step to Force BIP Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_bip8_cfg_biperr_step_Mask                                                               cBit9_2
#define c_upen_bip8_cfg_biperr_step_Shift                                                                    2

/*--------------------------------------
BitField Name: bipmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for BIP Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_bip8_cfg_bipmsk_pos_Mask_01                                                           cBit31_18
#define c_upen_bip8_cfg_bipmsk_pos_Shift_01                                                                 18
#define c_upen_bip8_cfg_bipmsk_pos_Mask_02                                                             cBit1_0
#define c_upen_bip8_cfg_bipmsk_pos_Shift_02                                                                  0

/*--------------------------------------
BitField Name: bipmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For  BIP Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_bip8_cfg_bipmsk_ena_Mask                                                                 cBit17
#define c_upen_bip8_cfg_bipmsk_ena_Shift                                                                    17

/*--------------------------------------
BitField Name: biperr_num
BitField Type: RW
BitField Desc: The Total BIP Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_bip8_cfg_biperr_num_Mask                                                               cBit16_1
#define c_upen_bip8_cfg_biperr_num_Shift                                                                     1

/*--------------------------------------
BitField Name: bipfrc_mod
BitField Type: RW
BitField Desc: Force BIP Mode 0: One shot 1: continous (biperr_num = 16 bit
FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_bip8_cfg_bipfrc_mod_Mask                                                                  cBit0
#define c_upen_bip8_cfg_bipfrc_mod_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP8 Byte Error Status
Reg Addr   : 0x00080b20-0x00080b37
Reg Formula: 0x00080b20 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to see the Force BIP8 Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_bip8_sta                                                                          0x00080b20
#define cReg_upen_bip8_sta_WidthVal                                                                         64

/*--------------------------------------
BitField Name: bipsta_almset
BitField Type: RW
BitField Desc: BIP  alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_bip8_sta_bipsta_almset_Mask                                                               cBit9
#define c_upen_bip8_sta_bipsta_almset_Shift                                                                  9

/*--------------------------------------
BitField Name: bipstaclralrm
BitField Type: RW
BitField Desc: BIP  clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_bip8_sta_bipstaclralrm_Mask                                                               cBit8
#define c_upen_bip8_sta_bipstaclralrm_Shift                                                                  8

/*--------------------------------------
BitField Name: bipstasetalrm
BitField Type: RW
BitField Desc: BIP  Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_bip8_sta_bipstasetalrm_Mask                                                               cBit7
#define c_upen_bip8_sta_bipstasetalrm_Shift                                                                  7

/*--------------------------------------
BitField Name: bipstatimecnt
BitField Type: RW
BitField Desc: BIP  Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_bip8_sta_bipstatimecnt_Mask                                                             cBit6_0
#define c_upen_bip8_sta_bipstatimecnt_Shift                                                                  0

/*--------------------------------------
BitField Name: bipstafrc_ena
BitField Type: RW
BitField Desc: BIP  Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_bip8_sta_bipstafrc_ena_Mask                                                              cBit31
#define c_upen_bip8_sta_bipstafrc_ena_Shift                                                                 31

/*--------------------------------------
BitField Name: bipstaerrstep
BitField Type: RW
BitField Desc: The Second BIP  Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_bip8_sta_bipstaerrstep_Mask                                                           cBit30_23
#define c_upen_bip8_sta_bipstaerrstep_Shift                                                                 23

/*--------------------------------------
BitField Name: bipstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask BIP  Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_bip8_sta_bipstamsk_pos_Mask                                                           cBit22_19
#define c_upen_bip8_sta_bipstamsk_pos_Shift                                                                 19

/*--------------------------------------
BitField Name: bipstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error BIP  Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_bip8_sta_bipstaerr_cnt_Mask                                                            cBit18_3
#define c_upen_bip8_sta_bipstaerr_cnt_Shift                                                                  3

/*--------------------------------------
BitField Name: bipstafrc_sta
BitField Type: RW
BitField Desc: BIP  State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_bip8_sta_bipstafrc_sta_Mask                                                             cBit2_1
#define c_upen_bip8_sta_bipstafrc_sta_Shift                                                                  1

/*--------------------------------------
BitField Name: bipstaoneunit
BitField Type: RW
BitField Desc: Force BIP  in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_bip8_sta_bipstaoneunit_Mask                                                               cBit0
#define c_upen_bip8_sta_bipstaoneunit_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force REI Bit Error Configuration
Reg Addr   : 0x00080b80-0x00080b97
Reg Formula: 0x00080b80 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force REI Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_rei_cfg                                                                           0x00080b80
#define cReg_upen_rei_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: reitim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_rei_cfg_reitim_mod_Mask                                                               cBit13_12
#define c_upen_rei_cfg_reitim_mod_Shift                                                                     12

/*--------------------------------------
BitField Name: reierr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_rei_cfg_reierr_clr_Mask                                                                  cBit11
#define c_upen_rei_cfg_reierr_clr_Shift                                                                     11

/*--------------------------------------
BitField Name: reierr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_rei_cfg_reierr_set_Mask                                                                  cBit10
#define c_upen_rei_cfg_reierr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: reierr_step
BitField Type: RW
BitField Desc: The Total Step to Force REI Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_rei_cfg_reierr_step_Mask                                                                cBit9_2
#define c_upen_rei_cfg_reierr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: reimsk_pos
BitField Type: RW
BitField Desc: The Position Mask for REI Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_rei_cfg_reimsk_pos_Mask_01                                                            cBit31_18
#define c_upen_rei_cfg_reimsk_pos_Shift_01                                                                  18
#define c_upen_rei_cfg_reimsk_pos_Mask_02                                                              cBit1_0
#define c_upen_rei_cfg_reimsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: reimsk_ena
BitField Type: RW
BitField Desc: The Data Mask For  REI Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_rei_cfg_reimsk_ena_Mask                                                                  cBit17
#define c_upen_rei_cfg_reimsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: reierr_num
BitField Type: RW
BitField Desc: The Total REI Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_rei_cfg_reierr_num_Mask                                                                cBit16_1
#define c_upen_rei_cfg_reierr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: reifrc_mod
BitField Type: RW
BitField Desc: Force REI Mode 0: One shot 1: continous (reierr_num = 16 bit
FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_rei_cfg_reifrc_mod_Mask                                                                   cBit0
#define c_upen_rei_cfg_reifrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force REI Bit Error Status
Reg Addr   : 0x00080ba0-0x00080bb7
Reg Formula: 0x00080ba0 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to see the Force REI Bit Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_rei_sta                                                                           0x00080ba0
#define cReg_upen_rei_sta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: reista_almset
BitField Type: RW
BitField Desc: REI bit alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_rei_sta_reista_almset_Mask                                                                cBit9
#define c_upen_rei_sta_reista_almset_Shift                                                                   9

/*--------------------------------------
BitField Name: reistaclralrm
BitField Type: RW
BitField Desc: REI bit clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_rei_sta_reistaclralrm_Mask                                                                cBit8
#define c_upen_rei_sta_reistaclralrm_Shift                                                                   8

/*--------------------------------------
BitField Name: reistasetalrm
BitField Type: RW
BitField Desc: REI bit Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_rei_sta_reistasetalrm_Mask                                                                cBit7
#define c_upen_rei_sta_reistasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: reistatimecnt
BitField Type: RW
BitField Desc: REI bit Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_rei_sta_reistatimecnt_Mask                                                              cBit6_0
#define c_upen_rei_sta_reistatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: reistafrc_ena
BitField Type: RW
BitField Desc: REI bit Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_rei_sta_reistafrc_ena_Mask                                                               cBit31
#define c_upen_rei_sta_reistafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: reistaerrstep
BitField Type: RW
BitField Desc: The Second REI bit Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_rei_sta_reistaerrstep_Mask                                                            cBit30_23
#define c_upen_rei_sta_reistaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: reistamsk_pos
BitField Type: RW
BitField Desc: The Position Mask REI bit Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_rei_sta_reistamsk_pos_Mask                                                            cBit22_19
#define c_upen_rei_sta_reistamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: reistaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error REI bit Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_rei_sta_reistaerr_cnt_Mask                                                             cBit18_3
#define c_upen_rei_sta_reistaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: reistafrc_sta
BitField Type: RW
BitField Desc: REI bit State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_rei_sta_reistafrc_sta_Mask                                                              cBit2_1
#define c_upen_rei_sta_reistafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: reistaoneunit
BitField Type: RW
BitField Desc: Force REI bit in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_rei_sta_reistaoneunit_Mask                                                                cBit0
#define c_upen_rei_sta_reistaoneunit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force NR Byte Error Configuration
Reg Addr   : 0x00080c00-0x00080c17
Reg Formula: 0x00080c00 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force NR Byte Error

------------------------------------------------------------------------------*/
#define cReg_upen_nrB_cfg                                                                           0x00080c00
#define cReg_upen_nrB_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: nrbtim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_nrB_cfg_nrbtim_mod_Mask                                                               cBit13_12
#define c_upen_nrB_cfg_nrbtim_mod_Shift                                                                     12

/*--------------------------------------
BitField Name: nrberr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_nrB_cfg_nrberr_clr_Mask                                                                  cBit11
#define c_upen_nrB_cfg_nrberr_clr_Shift                                                                     11

/*--------------------------------------
BitField Name: nrberr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_nrB_cfg_nrberr_set_Mask                                                                  cBit10
#define c_upen_nrB_cfg_nrberr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: nrberr_step
BitField Type: RW
BitField Desc: The Total Step to Force NR Byte Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_nrB_cfg_nrberr_step_Mask                                                                cBit9_2
#define c_upen_nrB_cfg_nrberr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: nrbmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for NR Byte Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_nrB_cfg_nrbmsk_pos_Mask_01                                                            cBit31_18
#define c_upen_nrB_cfg_nrbmsk_pos_Shift_01                                                                  18
#define c_upen_nrB_cfg_nrbmsk_pos_Mask_02                                                              cBit1_0
#define c_upen_nrB_cfg_nrbmsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: nrbmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For  NR Byte Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_nrB_cfg_nrbmsk_ena_Mask                                                                  cBit17
#define c_upen_nrB_cfg_nrbmsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: nrberr_num
BitField Type: RW
BitField Desc: The Total NR Byte Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_nrB_cfg_nrberr_num_Mask                                                                cBit16_1
#define c_upen_nrB_cfg_nrberr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: nrbfrc_mod
BitField Type: RW
BitField Desc: Force NR Byte Mode 0: One shot 1: continous (nrberr_num = 16 bit
FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_nrB_cfg_nrbfrc_mod_Mask                                                                   cBit0
#define c_upen_nrB_cfg_nrbfrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force NR Byte Error Status
Reg Addr   : 0x00080c20-0x00080c37
Reg Formula: 0x00080c20 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force NR Byte Error

------------------------------------------------------------------------------*/
#define cReg_upen_nrB_sta                                                                           0x00080c20
#define cReg_upen_nrB_sta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: nrbsta_almset
BitField Type: RW
BitField Desc: NR bit alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_nrB_sta_nrbsta_almset_Mask                                                                cBit9
#define c_upen_nrB_sta_nrbsta_almset_Shift                                                                   9

/*--------------------------------------
BitField Name: nrbstaclralrm
BitField Type: RW
BitField Desc: NR bit clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_nrB_sta_nrbstaclralrm_Mask                                                                cBit8
#define c_upen_nrB_sta_nrbstaclralrm_Shift                                                                   8

/*--------------------------------------
BitField Name: nrbstasetalrm
BitField Type: RW
BitField Desc: NR bit Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_nrB_sta_nrbstasetalrm_Mask                                                                cBit7
#define c_upen_nrB_sta_nrbstasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: nrbstatimecnt
BitField Type: RW
BitField Desc: NR bit Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_nrB_sta_nrbstatimecnt_Mask                                                              cBit6_0
#define c_upen_nrB_sta_nrbstatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: nrbstafrc_ena
BitField Type: RW
BitField Desc: NR bit Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_nrB_sta_nrbstafrc_ena_Mask                                                               cBit31
#define c_upen_nrB_sta_nrbstafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: nrbstaerrstep
BitField Type: RW
BitField Desc: The Second NR bit Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_nrB_sta_nrbstaerrstep_Mask                                                            cBit30_23
#define c_upen_nrB_sta_nrbstaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: nrbstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask NR bit Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_nrB_sta_nrbstamsk_pos_Mask                                                            cBit22_19
#define c_upen_nrB_sta_nrbstamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: nrbstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error NR bit Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_nrB_sta_nrbstaerr_cnt_Mask                                                             cBit18_3
#define c_upen_nrB_sta_nrbstaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: nrbstafrc_sta
BitField Type: RW
BitField Desc: NR bit State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_nrB_sta_nrbstafrc_sta_Mask                                                              cBit2_1
#define c_upen_nrB_sta_nrbstafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: nrbstaoneunit
BitField Type: RW
BitField Desc: Force NR bit in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_nrB_sta_nrbstaoneunit_Mask                                                                cBit0
#define c_upen_nrB_sta_nrbstaoneunit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force GC Byte Error Configuration
Reg Addr   : 0x00080c80-0x00080c97
Reg Formula: 0x00080c80 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force GC Byte Error

------------------------------------------------------------------------------*/
#define cReg_upen_gcB_cfg                                                                           0x00080c80
#define cReg_upen_gcB_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: gcbtim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_gcB_cfg_gcbtim_mod_Mask                                                               cBit13_12
#define c_upen_gcB_cfg_gcbtim_mod_Shift                                                                     12

/*--------------------------------------
BitField Name: gcberr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_gcB_cfg_gcberr_clr_Mask                                                                  cBit11
#define c_upen_gcB_cfg_gcberr_clr_Shift                                                                     11

/*--------------------------------------
BitField Name: gcberr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_gcB_cfg_gcberr_set_Mask                                                                  cBit10
#define c_upen_gcB_cfg_gcberr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: gcberr_step
BitField Type: RW
BitField Desc: The Total Step to Force GC Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_gcB_cfg_gcberr_step_Mask                                                                cBit9_2
#define c_upen_gcB_cfg_gcberr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: gcbmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for GC Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_gcB_cfg_gcbmsk_pos_Mask_01                                                            cBit31_18
#define c_upen_gcB_cfg_gcbmsk_pos_Shift_01                                                                  18
#define c_upen_gcB_cfg_gcbmsk_pos_Mask_02                                                              cBit1_0
#define c_upen_gcB_cfg_gcbmsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: gcbmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For  GC Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_gcB_cfg_gcbmsk_ena_Mask                                                                  cBit17
#define c_upen_gcB_cfg_gcbmsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: gcberr_num
BitField Type: RW
BitField Desc: The Total GC Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_gcB_cfg_gcberr_num_Mask                                                                cBit16_1
#define c_upen_gcB_cfg_gcberr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: gcbfrc_mod
BitField Type: RW
BitField Desc: Force GC Mode 0: One shot 1: continous (gcberr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_gcB_cfg_gcbfrc_mod_Mask                                                                   cBit0
#define c_upen_gcB_cfg_gcbfrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force GC Byte Error Status
Reg Addr   : 0x00080ca0-0x00080cb7
Reg Formula: 0x00080ca0 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to see Force GC Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_gcB_sta                                                                           0x00080ca0
#define cReg_upen_gcB_sta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: gcbsta_almset
BitField Type: RW
BitField Desc: GC bit alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_gcB_sta_gcbsta_almset_Mask                                                                cBit9
#define c_upen_gcB_sta_gcbsta_almset_Shift                                                                   9

/*--------------------------------------
BitField Name: gcbstaclralrm
BitField Type: RW
BitField Desc: GC bit clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_gcB_sta_gcbstaclralrm_Mask                                                                cBit8
#define c_upen_gcB_sta_gcbstaclralrm_Shift                                                                   8

/*--------------------------------------
BitField Name: gcbstasetalrm
BitField Type: RW
BitField Desc: GC bit Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_gcB_sta_gcbstasetalrm_Mask                                                                cBit7
#define c_upen_gcB_sta_gcbstasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: gcbstatimecnt
BitField Type: RW
BitField Desc: GC bit Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_gcB_sta_gcbstatimecnt_Mask                                                              cBit6_0
#define c_upen_gcB_sta_gcbstatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: gcbstafrc_ena
BitField Type: RW
BitField Desc: GC bit Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_gcB_sta_gcbstafrc_ena_Mask                                                               cBit31
#define c_upen_gcB_sta_gcbstafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: gcbstaerrstep
BitField Type: RW
BitField Desc: The Second GC bit Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_gcB_sta_gcbstaerrstep_Mask                                                            cBit30_23
#define c_upen_gcB_sta_gcbstaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: gcbstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask GC bit Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_gcB_sta_gcbstamsk_pos_Mask                                                            cBit22_19
#define c_upen_gcB_sta_gcbstamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: gcbstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error GC bit Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_gcB_sta_gcbstaerr_cnt_Mask                                                             cBit18_3
#define c_upen_gcB_sta_gcbstaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: gcbstafrc_sta
BitField Type: RW
BitField Desc: GC bit State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_gcB_sta_gcbstafrc_sta_Mask                                                              cBit2_1
#define c_upen_gcB_sta_gcbstafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: gcbstaoneunit
BitField Type: RW
BitField Desc: Force GC bit in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_gcB_sta_gcbstaoneunit_Mask                                                                cBit0
#define c_upen_gcB_sta_gcbstaoneunit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Configuration n second to force in DS3
Reg Addr   : 0x00080f80-0x00080f80
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure n second in  DS3

------------------------------------------------------------------------------*/
#define cReg_upennsecds3                                                                            0x00080f80

/*--------------------------------------
BitField Name: unsed
BitField Type: RW
BitField Desc: unsed
BitField Bits: [31:17]
--------------------------------------*/
#define c_upennsecds3_unsed_Mask                                                                     cBit31_17
#define c_upennsecds3_unsed_Shift                                                                           17

/*--------------------------------------
BitField Name: frcpfrmen
BitField Type: RW
BitField Desc: Force Error per frame enable
BitField Bits: [16]
--------------------------------------*/
#define c_upennsecds3_frcpfrmen_Mask                                                                    cBit16
#define c_upennsecds3_frcpfrmen_Shift                                                                       16

/*--------------------------------------
BitField Name: cfgnsecds3
BitField Type: RW
BitField Desc: The second number in DS Congiguration
BitField Bits: [15:0]
--------------------------------------*/
#define c_upennsecds3_cfgnsecds3_Mask                                                                 cBit15_0
#define c_upennsecds3_cfgnsecds3_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Force LOS Bit Error Configuration
Reg Addr   : 0x00080d00-0x00080d17
Reg Formula: 0x00080d00 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force LOS Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_los_cfg                                                                           0x00080d00
#define cReg_upen_los_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: lostim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_los_cfg_lostim_mod_Mask                                                               cBit27_26
#define c_upen_los_cfg_lostim_mod_Shift                                                                     26

/*--------------------------------------
BitField Name: loserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_los_cfg_loserr_clr_Mask                                                               cBit25_18
#define c_upen_los_cfg_loserr_clr_Shift                                                                     18

/*--------------------------------------
BitField Name: loserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_los_cfg_loserr_set_Mask                                                               cBit17_10
#define c_upen_los_cfg_loserr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: loserr_step
BitField Type: RW
BitField Desc: The Total Step to Force LOS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_los_cfg_loserr_step_Mask                                                                cBit9_2
#define c_upen_los_cfg_loserr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: losmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_los_cfg_losmsk_pos_Mask_01                                                            cBit31_18
#define c_upen_los_cfg_losmsk_pos_Shift_01                                                                  18
#define c_upen_los_cfg_losmsk_pos_Mask_02                                                              cBit1_0
#define c_upen_los_cfg_losmsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: losmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_los_cfg_losmsk_ena_Mask                                                                  cBit17
#define c_upen_los_cfg_losmsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: loserr_num
BitField Type: RW
BitField Desc: The Total LOS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_los_cfg_loserr_num_Mask                                                                cBit16_1
#define c_upen_los_cfg_loserr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: losfrc_mod
BitField Type: RW
BitField Desc: Force LOS Mode 0: One shot ( N events in T the unit time) 1:
continous (loserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_los_cfg_losfrc_mod_Mask                                                                   cBit0
#define c_upen_los_cfg_losfrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force LOS Bit Error Status
Reg Addr   : 0x00080d20-0x00080d37
Reg Formula: 0x00080d20 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to see the Force LOS Bit Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_los_sta                                                                           0x00080d20
#define cReg_upen_los_sta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: lossta_almset
BitField Type: RW
BitField Desc: LOS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_los_sta_lossta_almset_Mask                                                               cBit27
#define c_upen_los_sta_lossta_almset_Shift                                                                  27

/*--------------------------------------
BitField Name: losstaclralrm
BitField Type: RW
BitField Desc: LOS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_los_sta_losstaclralrm_Mask                                                            cBit22_15
#define c_upen_los_sta_losstaclralrm_Shift                                                                  15

/*--------------------------------------
BitField Name: losstasetalrm
BitField Type: RW
BitField Desc: LOS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_los_sta_losstasetalrm_Mask                                                             cBit14_7
#define c_upen_los_sta_losstasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: losstatimecnt
BitField Type: RW
BitField Desc: LOS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_los_sta_losstatimecnt_Mask                                                              cBit6_0
#define c_upen_los_sta_losstatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: losstafrc_ena
BitField Type: RW
BitField Desc: LOS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_los_sta_losstafrc_ena_Mask                                                               cBit31
#define c_upen_los_sta_losstafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: losstaerrstep
BitField Type: RW
BitField Desc: The Second LOS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_los_sta_losstaerrstep_Mask                                                            cBit30_23
#define c_upen_los_sta_losstaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: losstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask LOS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_los_sta_losstamsk_pos_Mask                                                            cBit22_19
#define c_upen_los_sta_losstamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: losstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error LOS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_los_sta_losstaerr_cnt_Mask                                                             cBit18_3
#define c_upen_los_sta_losstaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: losstafrc_sta
BitField Type: RW
BitField Desc: LOS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_los_sta_losstafrc_sta_Mask                                                              cBit2_1
#define c_upen_los_sta_losstafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: losstaoneunit
BitField Type: RW
BitField Desc: Force LOS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_los_sta_losstaoneunit_Mask                                                                cBit0
#define c_upen_los_sta_losstaoneunit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS Bit Error Configuration
Reg Addr   : 0x00080d80-0x00080e97
Reg Formula: 0x00080d80 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force AIS Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_ais_ds3_cfg                                                                       0x00080d80
#define cReg_upen_ais_ds3_cfg_WidthVal                                                                      64

/*--------------------------------------
BitField Name: aistim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_ais_ds3_cfg_aistim_mod_Mask                                                           cBit27_26
#define c_upen_ais_ds3_cfg_aistim_mod_Shift                                                                 26

/*--------------------------------------
BitField Name: aiserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_ais_ds3_cfg_aiserr_clr_Mask                                                           cBit25_18
#define c_upen_ais_ds3_cfg_aiserr_clr_Shift                                                                 18

/*--------------------------------------
BitField Name: aiserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_ais_ds3_cfg_aiserr_set_Mask                                                           cBit17_10
#define c_upen_ais_ds3_cfg_aiserr_set_Shift                                                                 10

/*--------------------------------------
BitField Name: aiserr_step
BitField Type: RW
BitField Desc: The Total Step to Force AIS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_ais_ds3_cfg_aiserr_step_Mask                                                            cBit9_2
#define c_upen_ais_ds3_cfg_aiserr_step_Shift                                                                 2

/*--------------------------------------
BitField Name: aismsk_pos
BitField Type: RW
BitField Desc: The Position Mask for AIS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_ais_ds3_cfg_aismsk_pos_Mask_01                                                        cBit31_18
#define c_upen_ais_ds3_cfg_aismsk_pos_Shift_01                                                              18
#define c_upen_ais_ds3_cfg_aismsk_pos_Mask_02                                                          cBit1_0
#define c_upen_ais_ds3_cfg_aismsk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: aismsk_ena
BitField Type: RW
BitField Desc: The Data Mask For AIS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_ais_ds3_cfg_aismsk_ena_Mask                                                              cBit17
#define c_upen_ais_ds3_cfg_aismsk_ena_Shift                                                                 17

/*--------------------------------------
BitField Name: aiserr_num
BitField Type: RW
BitField Desc: The Total AIS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_ais_ds3_cfg_aiserr_num_Mask                                                            cBit16_1
#define c_upen_ais_ds3_cfg_aiserr_num_Shift                                                                  1

/*--------------------------------------
BitField Name: aisfrc_mod
BitField Type: RW
BitField Desc: Force AIS Mode 0: One shot ( N events in T the unit time) 1:
continous (aiserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_ais_ds3_cfg_aisfrc_mod_Mask                                                               cBit0
#define c_upen_ais_ds3_cfg_aisfrc_mod_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS Bit Error Status
Reg Addr   : 0x00080da0-0x00080eb7
Reg Formula: 0x00080da0 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to see the Force AIS Bit Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_ais_ds3_sta                                                                       0x00080da0
#define cReg_upen_ais_ds3_sta_WidthVal                                                                      64

/*--------------------------------------
BitField Name: aissta_almset
BitField Type: RW
BitField Desc: AIS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_ais_ds3_sta_aissta_almset_Mask                                                           cBit27
#define c_upen_ais_ds3_sta_aissta_almset_Shift                                                              27

/*--------------------------------------
BitField Name: aisstaclralrm
BitField Type: RW
BitField Desc: AIS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_ais_ds3_sta_aisstaclralrm_Mask                                                        cBit22_15
#define c_upen_ais_ds3_sta_aisstaclralrm_Shift                                                              15

/*--------------------------------------
BitField Name: aisstasetalrm
BitField Type: RW
BitField Desc: AIS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_ais_ds3_sta_aisstasetalrm_Mask                                                         cBit14_7
#define c_upen_ais_ds3_sta_aisstasetalrm_Shift                                                               7

/*--------------------------------------
BitField Name: aisstatimecnt
BitField Type: RW
BitField Desc: AIS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_ais_ds3_sta_aisstatimecnt_Mask                                                          cBit6_0
#define c_upen_ais_ds3_sta_aisstatimecnt_Shift                                                               0

/*--------------------------------------
BitField Name: aisstafrc_ena
BitField Type: RW
BitField Desc: AIS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_ais_ds3_sta_aisstafrc_ena_Mask                                                           cBit31
#define c_upen_ais_ds3_sta_aisstafrc_ena_Shift                                                              31

/*--------------------------------------
BitField Name: aisstaerrstep
BitField Type: RW
BitField Desc: The Second AIS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_ais_ds3_sta_aisstaerrstep_Mask                                                        cBit30_23
#define c_upen_ais_ds3_sta_aisstaerrstep_Shift                                                              23

/*--------------------------------------
BitField Name: aisstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask AIS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_ais_ds3_sta_aisstamsk_pos_Mask                                                        cBit22_19
#define c_upen_ais_ds3_sta_aisstamsk_pos_Shift                                                              19

/*--------------------------------------
BitField Name: aisstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error AIS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_ais_ds3_sta_aisstaerr_cnt_Mask                                                         cBit18_3
#define c_upen_ais_ds3_sta_aisstaerr_cnt_Shift                                                               3

/*--------------------------------------
BitField Name: aisstafrc_sta
BitField Type: RW
BitField Desc: AIS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_ais_ds3_sta_aisstafrc_sta_Mask                                                          cBit2_1
#define c_upen_ais_ds3_sta_aisstafrc_sta_Shift                                                               1

/*--------------------------------------
BitField Name: aisstaoneunit
BitField Type: RW
BitField Desc: Force AIS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_ais_ds3_sta_aisstaoneunit_Mask                                                            cBit0
#define c_upen_ais_ds3_sta_aisstaoneunit_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS Bit Error Configuration
Reg Addr   : 0x00080e80-0x00080e97
Reg Formula: 0x00080e80 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force AIS Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_ais_e3_cfg                                                                        0x00080e80
#define cReg_upen_ais_e3_cfg_WidthVal                                                                       64

/*--------------------------------------
BitField Name: ais_e3tim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_e3tim_mod_Mask                                                         cBit27_26
#define c_upen_ais_e3_cfg_ais_e3tim_mod_Shift                                                               26

/*--------------------------------------
BitField Name: ais_e3err_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_e3err_clr_Mask                                                         cBit25_18
#define c_upen_ais_e3_cfg_ais_e3err_clr_Shift                                                               18

/*--------------------------------------
BitField Name: ais_e3err_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_e3err_set_Mask                                                         cBit17_10
#define c_upen_ais_e3_cfg_ais_e3err_set_Shift                                                               10

/*--------------------------------------
BitField Name: ais_e3err_step
BitField Type: RW
BitField Desc: The Total Step to Force AIS_E3 Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_e3err_step_Mask                                                          cBit9_2
#define c_upen_ais_e3_cfg_ais_e3err_step_Shift                                                               2

/*--------------------------------------
BitField Name: ais_e3msk_pos
BitField Type: RW
BitField Desc: The Position Mask for AIS_E3 Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_e3msk_pos_Mask_01                                                      cBit31_18
#define c_upen_ais_e3_cfg_ais_e3msk_pos_Shift_01                                                            18
#define c_upen_ais_e3_cfg_ais_e3msk_pos_Mask_02                                                        cBit1_0
#define c_upen_ais_e3_cfg_ais_e3msk_pos_Shift_02                                                             0

/*--------------------------------------
BitField Name: ais_e3msk_ena
BitField Type: RW
BitField Desc: The Data Mask For AIS_E3 Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_e3msk_ena_Mask                                                            cBit17
#define c_upen_ais_e3_cfg_ais_e3msk_ena_Shift                                                               17

/*--------------------------------------
BitField Name: ais_e3err_num
BitField Type: RW
BitField Desc: The Total AIS_E3 Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_e3err_num_Mask                                                          cBit16_1
#define c_upen_ais_e3_cfg_ais_e3err_num_Shift                                                                1

/*--------------------------------------
BitField Name: ais_e3frc_mod
BitField Type: RW
BitField Desc: Force AIS_E3 Mode 0: One shot ( N events in T the unit time) 1:
continous (ais_e3err_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_e3frc_mod_Mask                                                             cBit0
#define c_upen_ais_e3_cfg_ais_e3frc_mod_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS Bit Error Status
Reg Addr   : 0x00080ea0-0x00080eb7
Reg Formula: 0x00080ea0 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to see the Force AIS Bit Error Status

------------------------------------------------------------------------------*/
#define cAttReg_upen_ais_e3_sta                                                                        0x00080ea0
#define cAttReg_upen_ais_e3_sta_WidthVal                                                                       64

/*--------------------------------------
BitField Name: ais_e3sta_almset
BitField Type: RW
BitField Desc: AIS_E3 alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3sta_almset_Mask                                                         cBit27
#define c_upen_ais_e3_sta_ais_e3sta_almset_Shift                                                            27

/*--------------------------------------
BitField Name: ais_e3staclralrm
BitField Type: RW
BitField Desc: AIS_E3 clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3staclralrm_Mask                                                      cBit22_15
#define c_upen_ais_e3_sta_ais_e3staclralrm_Shift                                                            15

/*--------------------------------------
BitField Name: ais_e3stasetalrm
BitField Type: RW
BitField Desc: AIS_E3 Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3stasetalrm_Mask                                                       cBit14_7
#define c_upen_ais_e3_sta_ais_e3stasetalrm_Shift                                                             7

/*--------------------------------------
BitField Name: ais_e3statimecnt
BitField Type: RW
BitField Desc: AIS_E3 Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3statimecnt_Mask                                                        cBit6_0
#define c_upen_ais_e3_sta_ais_e3statimecnt_Shift                                                             0

/*--------------------------------------
BitField Name: ais_e3stafrc_ena
BitField Type: RW
BitField Desc: AIS_E3 Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3stafrc_ena_Mask                                                         cBit31
#define c_upen_ais_e3_sta_ais_e3stafrc_ena_Shift                                                            31

/*--------------------------------------
BitField Name: ais_e3staerrstep
BitField Type: RW
BitField Desc: The Second AIS_E3 Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3staerrstep_Mask                                                      cBit30_23
#define c_upen_ais_e3_sta_ais_e3staerrstep_Shift                                                            23

/*--------------------------------------
BitField Name: ais_e3stamsk_pos
BitField Type: RW
BitField Desc: The Position Mask AIS_E3 Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3stamsk_pos_Mask                                                      cBit22_19
#define c_upen_ais_e3_sta_ais_e3stamsk_pos_Shift                                                            19

/*--------------------------------------
BitField Name: ais_e3staerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error AIS_E3 Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3staerr_cnt_Mask                                                       cBit18_3
#define c_upen_ais_e3_sta_ais_e3staerr_cnt_Shift                                                             3

/*--------------------------------------
BitField Name: ais_e3stafrc_sta
BitField Type: RW
BitField Desc: AIS_E3 State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3stafrc_sta_Mask                                                        cBit2_1
#define c_upen_ais_e3_sta_ais_e3stafrc_sta_Shift                                                             1

/*--------------------------------------
BitField Name: ais_e3staoneunit
BitField Type: RW
BitField Desc: Force AIS_E3 in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3staoneunit_Mask                                                          cBit0
#define c_upen_ais_e3_sta_ais_e3staoneunit_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Force FEBE Error Configuration
Reg Addr   : 0x00080980-0x00080997
Reg Formula: 0x00080980 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to configure Force FEBE Error

------------------------------------------------------------------------------*/
#define cReg_upen_febe_cfg                                                                          0x00080980
#define cReg_upen_febe_cfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: febetim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_febe_cfg_febetim_mod_Mask                                                             cBit13_12
#define c_upen_febe_cfg_febetim_mod_Shift                                                                   12

/*--------------------------------------
BitField Name: febeerr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_febe_cfg_febeerr_clr_Mask                                                                cBit11
#define c_upen_febe_cfg_febeerr_clr_Shift                                                                   11

/*--------------------------------------
BitField Name: febeerr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_febe_cfg_febeerr_set_Mask                                                                cBit10
#define c_upen_febe_cfg_febeerr_set_Shift                                                                   10

/*--------------------------------------
BitField Name: febeerr_step
BitField Type: RW
BitField Desc: The Total Step to Force FEBE Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_febe_cfg_febeerr_step_Mask                                                              cBit9_2
#define c_upen_febe_cfg_febeerr_step_Shift                                                                   2

/*--------------------------------------
BitField Name: febemsk_pos
BitField Type: RW
BitField Desc: The Position Mask for FEBE Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_febe_cfg_febemsk_pos_Mask_01                                                          cBit31_18
#define c_upen_febe_cfg_febemsk_pos_Shift_01                                                                18
#define c_upen_febe_cfg_febemsk_pos_Mask_02                                                            cBit1_0
#define c_upen_febe_cfg_febemsk_pos_Shift_02                                                                 0

/*--------------------------------------
BitField Name: febemsk_ena
BitField Type: RW
BitField Desc: The Data Mask For  FEBE Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_febe_cfg_febemsk_ena_Mask                                                                cBit17
#define c_upen_febe_cfg_febemsk_ena_Shift                                                                   17

/*--------------------------------------
BitField Name: febeerr_num
BitField Type: RW
BitField Desc: The Total FEBE Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_febe_cfg_febeerr_num_Mask                                                              cBit16_1
#define c_upen_febe_cfg_febeerr_num_Shift                                                                    1

/*--------------------------------------
BitField Name: febefrc_mod
BitField Type: RW
BitField Desc: Force FEBE Mode 0: One shot 1: continous (febeerr_num = 16 bit
FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_febe_cfg_febefrc_mod_Mask                                                                 cBit0
#define c_upen_febe_cfg_febefrc_mod_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Force FEBE Error Status
Reg Addr   : 0x000809a0-0x000809b7
Reg Formula: 0x000809a0 + $ds3id
    Where  : 
           + $ds3id(0-23): DS3 Index
Reg Desc   : 
This register is applicable to see the Force FEBE Error Status

------------------------------------------------------------------------------*/
#define cAttReg_upen_febe_sta                                                                          0x000809a0
#define cAttReg_upen_febe_sta_WidthVal                                                                         64

/*--------------------------------------
BitField Name: febesta_almset
BitField Type: RW
BitField Desc: FEBE bit alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_febe_sta_febesta_almset_Mask                                                              cBit9
#define c_upen_febe_sta_febesta_almset_Shift                                                                 9

/*--------------------------------------
BitField Name: febestaclralrm
BitField Type: RW
BitField Desc: FEBE bit clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_febe_sta_febestaclralrm_Mask                                                              cBit8
#define c_upen_febe_sta_febestaclralrm_Shift                                                                 8

/*--------------------------------------
BitField Name: febestasetalrm
BitField Type: RW
BitField Desc: FEBE bit Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_febe_sta_febestasetalrm_Mask                                                              cBit7
#define c_upen_febe_sta_febestasetalrm_Shift                                                                 7

/*--------------------------------------
BitField Name: febestatimecnt
BitField Type: RW
BitField Desc: FEBE bit Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_febe_sta_febestatimecnt_Mask                                                            cBit6_0
#define c_upen_febe_sta_febestatimecnt_Shift                                                                 0

/*--------------------------------------
BitField Name: febestafrc_ena
BitField Type: RW
BitField Desc: FEBE bit Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_febe_sta_febestafrc_ena_Mask                                                             cBit31
#define c_upen_febe_sta_febestafrc_ena_Shift                                                                31

/*--------------------------------------
BitField Name: febestaerrstep
BitField Type: RW
BitField Desc: The Second FEBE bit Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_febe_sta_febestaerrstep_Mask                                                          cBit30_23
#define c_upen_febe_sta_febestaerrstep_Shift                                                                23

/*--------------------------------------
BitField Name: febestamsk_pos
BitField Type: RW
BitField Desc: The Position Mask FEBE bit Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_febe_sta_febestamsk_pos_Mask                                                          cBit22_19
#define c_upen_febe_sta_febestamsk_pos_Shift                                                                19

/*--------------------------------------
BitField Name: febestaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error FEBE bit Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_febe_sta_febestaerr_cnt_Mask                                                           cBit18_3
#define c_upen_febe_sta_febestaerr_cnt_Shift                                                                 3

/*--------------------------------------
BitField Name: febestafrc_sta
BitField Type: RW
BitField Desc: FEBE bit State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_febe_sta_febestafrc_sta_Mask                                                            cBit2_1
#define c_upen_febe_sta_febestafrc_sta_Shift                                                                 1

/*--------------------------------------
BitField Name: febestaoneunit
BitField Type: RW
BitField Desc: Force FEBE bit in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_febe_sta_febestaoneunit_Mask                                                              cBit0
#define c_upen_febe_sta_febestaoneunit_Shift                                                                 0

#endif /* _THA6A290011PDHDE3ATTREG_H_ */

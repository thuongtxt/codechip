/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A290011AttSdhManager.c
 *
 * Created Date: Sep 09, 2017
 *
 * Author      :
 *
 * Description : ATT Sdh Manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha6A210031/att/Tha6A210031AttSdhManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A290011AttSdhManager)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011AttSdhManager* Tha6A290011AttSdhManager;
typedef struct tTha6A290011AttSdhManager
    {
    tTha6A210031AttSdhManager super;
    }tTha6A290011AttSdhManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttSdhManagerMethods m_AtAttSdhManagerOverride;

/* Save super implementations */
static const tAtAttSdhManagerMethods  *m_AtAttSdhManagerMethods  = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eBool    HasForcePointer(AtAttSdhManager self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet LineAttInit(AtAttSdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet HoPathAttInit(AtAttSdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet LoPathAttInit(AtAttSdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtAttSdhManager(AtAttSdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttSdhManagerMethods =  mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttSdhManagerOverride, m_AtAttSdhManagerMethods, sizeof(m_AtAttSdhManagerOverride));
        mMethodOverride(m_AtAttSdhManagerOverride, LineAttInit);
        mMethodOverride(m_AtAttSdhManagerOverride, LoPathAttInit);
        mMethodOverride(m_AtAttSdhManagerOverride, HoPathAttInit);
        mMethodOverride(m_AtAttSdhManagerOverride, HasForcePointer);
        }

    mMethodsSet(self, &m_AtAttSdhManagerOverride);
    }

static void Override(Tha6A290011AttSdhManager self)
    {
    OverrideAtAttSdhManager((AtAttSdhManager)self);
    }

static AtAttSdhManager ObjectInit(AtAttSdhManager self, AtModuleSdh sdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaAttSdhManager));

    /* Super constructor */
    if (Tha6A210031AttSdhManagerObjectInit(self, sdh) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtAttSdhManager Tha6A290011AttSdhManagerNew(AtModuleSdh sdh)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttSdhManager newObject = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaAttSdhManager));
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newObject, sdh);
    }

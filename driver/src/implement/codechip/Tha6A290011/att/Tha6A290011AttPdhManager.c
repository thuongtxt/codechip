/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha60290011AttPdhManager.c
 *
 * Created Date: Sep 09, 2019
 *
 * Author      :
 *
 * Description : ATT Pdh Manager
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha6A210031/att/Tha6A210031AttPdhManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A290011AttPdhManager)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011AttPdhManager* Tha6A290011AttPdhManager;
typedef struct tTha6A290011AttPdhManager
    {
    tTha6A210031AttPdhManager super;
    }tTha6A290011AttPdhManager;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttPdhManagerMethods m_AtAttPdhManagerOverride;

/* Save super implementations */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/





static void OverrideAtAttPdhManager(AtAttPdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttPdhManagerOverride, mMethodsGet(self), sizeof(m_AtAttPdhManagerOverride));
        /*mMethodOverride(m_AtAttPdhManagerOverride, De3AttInit);
        mMethodOverride(m_AtAttPdhManagerOverride, De1AttInit);*/
        }

    mMethodsSet(self, &m_AtAttPdhManagerOverride);
    }

static void Override(Tha6A290011AttPdhManager self)
    {
    OverrideAtAttPdhManager((AtAttPdhManager)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011AttPdhManager);
    }

static AtAttPdhManager ObjectInit(AtAttPdhManager self, AtModulePdh pdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031AttPdhManagerObjectInit(self, pdh) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtAttPdhManager Tha6A290011AttPdhManagerNew(AtModulePdh pdh)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttPdhManager newObject = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newObject, pdh);
    }



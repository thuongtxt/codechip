/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : Interrupt Controller
 *
 * File        : Tha6A290021InterruptControllerInternal.h
 *
 * Created Date: April 11, 2018
 *
 * Description : Interrupt Controller representation.
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011INTERRUPTCONTROLLER_H_
#define _THA6A290011INTERRUPTCONTROLLER_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/man/Tha60290011InterruptController.h"

#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290011InterruptController
    {
	tTha60290011InterruptController super;
    } tTha6A290011InterruptController;
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/
ThaIntrController Tha6A290011IntrControllerNew(AtIpCore core);
ThaIntrController Tha6A290011IntrControllerObjectInit(ThaIntrController self, AtIpCore ipCore);

#ifdef __cplusplus
}
#endif

#endif /* _THA6A290011INTERRUPTCONTROLLER_H_ */

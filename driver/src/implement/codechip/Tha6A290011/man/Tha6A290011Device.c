/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha6A290011Device.c
 *
 * Created Date: Oct 8, 2015
 *
 * Description : Tha60210031 Device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/man/Tha60290011DeviceInternal.h"
#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/cla/ThaModuleCla.h"
#include "../pwe/Tha6A290011ModulePwe.h"
#include "Tha6A290011InterruptController.h"
/*
#include "../ocn/Tha6A290011ModuleOcn.h"
#include "../pwe/Tha6A290011ModulePwe.h"
*/
/*#include "../ram/Tha6A290011ModuleRam.h"*/
/*#include "Tha6A290011InterruptController.h"*/

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011Device
    {
    tTha60290011Device super;
    }tTha6A290011Device;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods          m_AtDeviceOverride;
static tThaDeviceMethods         m_ThaDeviceOverride;
static tTha60290011DeviceMethods m_Tha60290011DeviceOverride;
static tTha60210031DeviceMethods m_Tha60210031DeviceOverride;

/* Super implementations */
static const tAtDeviceMethods *m_AtDeviceMethods = NULL;
static const tTha60210031DeviceMethods *m_Tha60210031DeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool RamIdIsValid(AtDevice self, uint32 ramId)
    {
    AtUnused(self);
    if (ramId==33)
        return cAtFalse;
    else if (ramId==67)
        return cAtFalse;
    else if (ramId>=63 && ramId<=65)
        return cAtFalse;
    else if (ramId>=69 && ramId<=81)
        return cAtFalse;
    return cAtTrue;
    }

static eBool ClockDebugIsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool StickyDebugIsSupported(ThaDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool EthSohTransparentIsReady(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool De1RetimingIsSupported(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtDeviceRole RoleGet(AtDevice self)
    {
    AtUnused(self);
    return cAtDeviceRoleActive;
    }

static void UnbindAllPws(AtDevice self)
    {
    uint32 pwId;
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(self, cAtModulePw);
    if (pwModule)
       {
       for (pwId = 0; pwId < AtModulePwMaxPwsGet(pwModule); pwId++)
           {
           AtPw pw = AtModulePwGetPw(pwModule, pwId);
           if (pw != NULL)
               AtPwCircuitUnbind(pw);
           }
       }
    }

static eAtRet Init(AtDevice self)
    {
    UnbindAllPws(self);
    return m_AtDeviceMethods->Init(self);
    }

static eBool ErrorGeneratorIsSupported(ThaDevice self)
    {
    ThaVersionReader versionReader                     = ThaDeviceVersionReader((AtDevice)self);
    uint32           hwVersion                         = ThaVersionReaderHardwareVersionAndBuiltNumber(versionReader);
    uint32           startVersionSupportErrorGenerator = ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0011);
    return (hwVersion >= startVersionSupportErrorGenerator) ? cAtTrue : cAtFalse;
    }

static eAtRet HwFlushAtt(ThaDevice self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool ShouldSwitchToPmc(ThaDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 DefaultCounterModule(ThaDevice self, uint32 moduleId)
    {
    AtUnused(self);
    return moduleId;
    }


static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = (eThaPhyModule)moduleId;

    if (moduleId  == cAtModulePrbs) return (AtModule)Tha6A290011ModulePrbsNew(self);
    if (moduleId  == cAtModuleSdh)  return (AtModule)Tha6A290011ModuleSdhNew(self);
    if (moduleId  == cAtModulePw)   return (AtModule)Tha6A290011ModulePwNew(self);
    if (moduleId  == cAtModulePdh)  return (AtModule)Tha6A290011ModulePdhNew(self);
    if (moduleId  == cAtModuleEth)  return (AtModule)Tha6A290011ModuleEthNew(self);
    if (moduleId  == cAtModuleSur)  return (AtModule)Tha6A290011ModuleSurNew(self);

    /* PhyModule */
    if (phyModule  == cThaModulePoh)   return Tha6A290011ModulePohNew(self);
    if (phyModule  == cThaModuleDemap) return Tha6A290011ModuleDemapNew(self);
    if (phyModule  == cThaModuleMap)   return Tha6A290011ModuleMapNew(self);
    if (phyModule  == cThaModulePwe)   return Tha6A290011ModulePweNew(self);
    if (phyModule  == cThaModuleOcn)   return Tha6A290011ModuleOcnNew(self);
    if (phyModule  == cThaModuleCla)   return Tha6A290011ModuleClaNew(self);
    if (phyModule  == cThaModuleCdr)   return Tha6A290011ModuleCdrNew(self);
    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eAtRet AllModulesInit(AtDevice self)
    {
    eAtRet ret = cAtOk;

    ret |= AtDeviceModuleInit(self, cAtModuleXc);
    ret |= AtDeviceModuleInit(self, cAtModuleSdh);
    ret |= AtDeviceModuleInit(self, cAtModulePdh);
    ret |= AtDeviceModuleInit(self, cAtModuleEncap);
    ret |= AtDeviceModuleInit(self, cAtModuleRam);
    ret |= AtDeviceModuleInit(self, cAtModulePw);
    ret |= AtDeviceModuleInit(self, cAtModuleClock);
    ret |= AtDeviceModuleInit(self, cAtModuleConcate);

    /* Initialize all physical modules */
    /*ret |= AtDeviceModuleInit(self, cThaModuleCdr);*/
    ret |= AtDeviceModuleInit(self, cThaModuleOcn);
    ret |= AtDeviceModuleInit(self, cThaModulePoh);

    return ret;
    }

static eBool DiagnosticModeIsEnabled(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static ThaIntrController IntrControllerCreate(ThaDevice self, AtIpCore core)
    {
    AtUnused(self);
    return Tha6A290011IntrControllerNew(core);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, Init);
        mMethodOverride(m_AtDeviceOverride, RoleGet);
        mMethodOverride(m_AtDeviceOverride, AllModulesInit);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, DiagnosticModeIsEnabled);
        mMethodOverride(m_AtDeviceOverride, RamIdIsValid);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(ThaDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(self), sizeof(tThaDeviceMethods));

        mMethodOverride(m_ThaDeviceOverride, IntrControllerCreate);
        mMethodOverride(m_ThaDeviceOverride, ShouldSwitchToPmc);
        mMethodOverride(m_ThaDeviceOverride, DefaultCounterModule);
        mMethodOverride(m_ThaDeviceOverride, ErrorGeneratorIsSupported);
        mMethodOverride(m_ThaDeviceOverride, HwFlushAtt);
        mMethodOverride(m_ThaDeviceOverride, ClockDebugIsSupported);
        mMethodOverride(m_ThaDeviceOverride, StickyDebugIsSupported);
        }

    mMethodsSet(self, &m_ThaDeviceOverride);
    }


static void OverrideTha60290011Device(Tha60290011Device self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60290011DeviceOverride, mMethodsGet(self), sizeof(tTha60290011DeviceMethods));

        mMethodOverride(m_Tha60290011DeviceOverride, De1RetimingIsSupported);
        mMethodOverride(m_Tha60290011DeviceOverride, EthSohTransparentIsReady);
        }

    mMethodsSet(self, &m_Tha60290011DeviceOverride);
    }

static void PweHwFlush(Tha60210031Device self)
    {
    AtUnused(self);
    }

static void OverrideTha60210031Device(AtDevice self)
    {
    Tha60210031Device device = (Tha60210031Device)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210031DeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031DeviceOverride, m_Tha60210031DeviceMethods, sizeof(m_Tha60210031DeviceOverride));

        mMethodOverride(m_Tha60210031DeviceOverride, PweHwFlush);
        }

    mMethodsSet(device, &m_Tha60210031DeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    OverrideThaDevice((ThaDevice)self);
    OverrideTha60290011Device((Tha60290011Device) self);
    OverrideTha60210031Device(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha6A290011DeviceNew(AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    return ObjectInit(newDevice, driver, productCode);
    }

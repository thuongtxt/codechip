/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A290011PrbsEnginePw.c
 *
 * Created Date: Oct 9, 2015
 *
 * Description : Tha6A290011 PRBS PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290011PrbsEngineInternal.h"
#include "../../../../generic/common/AtChannelInternal.h"
#include "../../Tha6A210031/prbs/Tha6A210031ModulePrbsReg.h"
#include "../../Tha6A210031/prbs/Tha6A210031ModulePrbsInternal.h"
#include "../../Tha6A210031/prbs/Tha6A210031ModulePrbs.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/pw/adapters/ThaPwAdapter.h"
#include "../../../default/pw/activator/ThaHwPw.h"
#include "Tha6A290011ModulePrbs.h"
#include "Tha6A290011ModulePrbsReg.h"

/*--------------------------- Define -----------------------------------------*/
#define c_prbslomon_PrbsLoMonErr_Mask                                     cBit17
#define c_prbslomon_PrbsLoMonErr_Shift                                        17
#define c_prbslomon_PrbsLoMonSync_Mask                                    cBit16
#define c_prbslomon_PrbsLoMonSync_Shift                                       16
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;
static tTha61031031PrbsEngineTdmPwMethods m_Tha61031031PrbsEnginePwOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint16 DisruptionLongRead(AtPrbsEngine self, uint32 address, uint32 *regVal, uint16 bufferLen)
    {
    regVal[0] = mChannelHwRead(AtPrbsEngineChannelGet(self), address, cAtModulePrbs);
    regVal[1] = mChannelHwRead(AtPrbsEngineChannelGet(self), 0x21, cAtModulePrbs);
    regVal[2] = mChannelHwRead(AtPrbsEngineChannelGet(self), 0x22, cAtModulePrbs);
    regVal[3] = mChannelHwRead(AtPrbsEngineChannelGet(self), 0x23, cAtModulePrbs);
    return bufferLen;
    }

static uint16 DisruptionLongWrite(AtPrbsEngine self, uint32 address, uint32 *regVal, uint16 bufferLen)
    {
    mChannelHwWrite(AtPrbsEngineChannelGet(self), 0x11, regVal[1], cAtModulePrbs);
    mChannelHwWrite(AtPrbsEngineChannelGet(self), 0x12, regVal[2], cAtModulePrbs);
    mChannelHwWrite(AtPrbsEngineChannelGet(self), 0x13, regVal[3], cAtModulePrbs);
    mChannelHwWrite(AtPrbsEngineChannelGet(self), address, regVal[0], cAtModulePrbs);
    return bufferLen;
    }

static eBool SeparateTwoDirections(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedSetDefaultMode(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static Tha6A210031ModulePrbs PrbsModule(AtPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    return (Tha6A210031ModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011PrbsEnginePw);
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) +
                                 Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsEnableMask(prbs, cAtFalse, cAtTrue);
    if (regFieldMask != cInvalidUint32)
        {
        uint32 regFieldShift = AtRegMaskToShift(regFieldMask);

        mRegFieldSet(regVal, regField, mBoolToBin(enable));
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    ((tTha6A210031PrbsEnginePw*)self)->txEnable = enable;
    return cAtOk;
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) +
                                 Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsEnableMask(prbs, cAtFalse, cAtTrue);
    if (regFieldMask != cInvalidUint32)
        {
        uint32 regFieldShift = AtRegMaskToShift(regFieldMask);
        return mBinToBool(mRegField(regVal, regField));
        }
    return ((tTha6A210031PrbsEnginePw*)self)->txEnable;
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) +
                                 Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask  = Tha6A210031ModulePrbsEnableMask(prbs, cAtFalse, cAtFalse);
    if (regFieldMask != cInvalidUint32)
        {
        uint32 regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal, regField, mBoolToBin(enable));
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    ((tTha6A210031PrbsEnginePw*)self)->rxEnable = enable;
    return cAtOk;
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsEnableMask(prbs, cAtFalse, cAtFalse);
    if (regFieldMask != cInvalidUint32)
        {
        uint32 regFieldShift = AtRegMaskToShift(regFieldMask);
        return mBinToBool(mRegField(regVal, regField));
        }
    return ((tTha6A210031PrbsEnginePw*)self)->rxEnable;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    eAtModulePrbsRet ret = AtPrbsEngineTxEnable(self, enable);
    ret |= AtPrbsEngineRxEnable(self, enable);
    return ret;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    eBool txEnable = AtPrbsEngineTxIsEnabled(self);
    eBool rxEnable = AtPrbsEngineRxIsEnabled(self);
    return (eBool)((txEnable != rxEnable) ? cAtFalse : txEnable);
    }

static eBool ModeIsSupported(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    Tha6A210031ModulePrbs prbs     = PrbsModule(self);
    AtUnused(self);
    return (Tha6A210031ModulePrbsModeToHwValue(prbs, prbsMode) != cAtPrbsModeInvalid) ? cAtTrue : cAtFalse;
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    Tha6A210031ModulePrbs prbs     = PrbsModule(self);
    uint32                hwMode   = Tha6A210031ModulePrbsModeToHwValue(prbs, prbsMode);
    if (hwMode != cInvalidUint32)
        {
        uint32 regAddr             = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
        uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask        = Tha6A210031ModulePrbsModeMask(prbs, cAtFalse, cAtTrue);
        uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);

        mRegFieldSet(regVal, regField, hwMode);

        /* Write 3 time rx fix pattern is requested by hw(Ms Khuyen),
         * when change prbs mode some channel can't write because RTL code priority engine */
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

        ((tTha6A210031PrbsEnginePw*)self)->txPrbsMode = prbsMode;

        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsModeMask(prbs, cAtFalse, cAtTrue);
    uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);
    uint8  hwMode              = (uint8)mRegField(regVal, regField);
    return Tha6A210031ModulePrbsModeFromHwValue(prbs,((tTha6A210031PrbsEnginePw*)self)->txPrbsMode, hwMode, 0);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    Tha6A210031ModulePrbs prbs     = PrbsModule(self);
    uint32                hwMode   = Tha6A210031ModulePrbsModeToHwValue(prbs, prbsMode);
    if (hwMode != cInvalidUint32)
        {
        uint32 regAddr             = Tha6A210031ModulePrbsMonReg(prbs, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
        uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask        = Tha6A210031ModulePrbsModeMask(prbs, cAtFalse, cAtFalse);
        uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);

        mRegFieldSet(regVal, regField, hwMode);

        /* Write 3 time rx fix pattern is requested by hw(Ms Khuyen),
         * when change prbs mode some channel can't write because RTL code priority engine */
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

        ((tTha6A210031PrbsEnginePw*)self)->rxPrbsMode = prbsMode;

        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsMonReg(prbs, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsModeMask(prbs, cAtFalse, cAtFalse);
    uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);
    uint8  hwMode              = (uint8)mRegField(regVal, regField);

    return Tha6A210031ModulePrbsModeFromHwValue(prbs, ((tTha6A210031PrbsEnginePw*)self)->rxPrbsMode,  hwMode, 0);
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtModulePrbsRet ret = cAtOk;
    ret |= AtPrbsEngineTxModeSet(self, prbsMode);
    ret |= AtPrbsEngineRxModeSet(self, prbsMode);
    return ret;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    eAtPrbsMode txMode = AtPrbsEngineTxIsEnabled(self);
    eAtPrbsMode rxMode = AtPrbsEngineRxIsEnabled(self);
    return (txMode != rxMode) ? cAtPrbsModeInvalid : txMode;
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsFixPatternReg(prbs, cAtFalse, cAtTrue) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsFixPatternMask(prbs, cAtFalse, cAtTrue);
    uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);

    mRegFieldSet(regVal, regField, Tha6A210031ModulePrbsConvertSwToHwFixedPattern(prbs,((tTha6A210031PrbsEnginePw*)self)->txPrbsMode, fixedPattern));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsFixPatternReg(prbs, cAtFalse, cAtTrue) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsFixPatternMask(prbs, cAtFalse, cAtTrue);
    uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);

    return Tha6A210031ModulePrbsConvertHwToSwFixedPattern(prbs, ((tTha6A210031PrbsEnginePw*)self)->txPrbsMode, mRegField(regVal, regField));
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr = Tha6A210031ModulePrbsFixPatternReg(prbs, cAtFalse, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsFixPatternMask(prbs, cAtFalse, cAtFalse);
    uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);

    mRegFieldSet(regVal, regField, Tha6A210031ModulePrbsConvertSwToHwFixedPattern(prbs,((tTha6A210031PrbsEnginePw*)self)->rxPrbsMode, fixedPattern));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);

    return cAtOk;
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsFixPatternReg(prbs, cAtFalse, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsFixPatternMask(prbs, cAtFalse, cAtFalse);
    uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);

    return Tha6A210031ModulePrbsConvertHwToSwFixedPattern(prbs,  ((tTha6A210031PrbsEnginePw*)self)->rxPrbsMode, mRegField(regVal, regField));
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    eAtModulePrbsRet ret = AtPrbsEngineTxFixedPatternSet(self, fixedPattern);
    ret |= AtPrbsEngineRxFixedPatternSet(self, fixedPattern);
    return ret;
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    uint32 txFixedPattern = AtPrbsEngineTxFixedPatternGet(self);
    uint32 rxFixedPattern = AtPrbsEngineRxFixedPatternGet(self);
    return (txFixedPattern != rxFixedPattern) ? cInvalidUint32 : txFixedPattern;
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsInvertMask(prbs, cAtFalse, cAtTrue);
    if (regFieldMask != cInvalidUint32)
        {
        uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);

        mRegFieldSet(regVal, regField, mBoolToBin(invert));
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return cAtOk;
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsInvertMask(prbs, cAtFalse, cAtTrue);
    if (regFieldMask != cInvalidUint32)
        {
        uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);
        return mBinToBool(mRegField(regVal, regField));
        }
    return cAtFalse;
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsMonReg(prbs, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsInvertMask(prbs, cAtFalse, cAtFalse);
    if (regFieldMask != cInvalidUint32)
        {
        uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal, regField, mBoolToBin(invert));
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return cAtOk;
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsMonReg(prbs, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsInvertMask(prbs, cAtFalse, cAtFalse);
    if (regFieldMask != cInvalidUint32)
        {
        uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);
        return mBinToBool(mRegField(regVal, regField));
        }
    return cAtFalse;
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    eAtModulePrbsRet ret = AtPrbsEngineTxInvert(self, invert);
    ret |= AtPrbsEngineRxInvert(self, invert);
    return ret;
    }

static eBool IsInverted(AtPrbsEngine self)
    {
    eBool txInvert = AtPrbsEngineTxIsInverted(self);
    eBool rxInvert = AtPrbsEngineRxIsInverted(self);
    return (eBool)((txInvert != rxInvert) ? cAtFalse : txInvert);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsErrFrcMask(prbs, cAtFalse);
    if (regFieldMask != cInvalidUint32)
        {
        uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);

        mRegFieldSet(regVal, regField, mBoolToBin(force));
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + Tha6A210031PrbsEnginePwDefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask        = Tha6A210031ModulePrbsErrFrcMask(prbs, cAtFalse);
    if (regFieldMask != cInvalidUint32)
        {
        uint32 regFieldShift       = AtRegMaskToShift(regFieldMask);

        return mBinToBool(mRegField(regVal, regField));
        }
    return cAtFalse;
    }


static uint32 CounterAddress(uint16 counterType)
    {
    if (counterType == cAtPrbsEngineCounterRxBit)
        return cThaRegPrbsGoodCounter;

    return cThaRegPrbsErrorCounter;
    }


static uint32 HwCounterRead2Clear(AtPrbsEngine self, uint16 counterType, eBool clear)
    {
    uint32 regAddr = CounterAddress(counterType) + Tha6A210031PrbsEnginePwDefaultOffset(self) + 2048*mBoolToBin(!clear);
    return AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    }

static uint32 HwCounterGet_FixPattern4bytes(AtPrbsEngine self, uint16 counterType, eBool r2c, eBool isLack)
    {
    uint32 offset  =  Tha6A210031PrbsEnginePwDefaultOffset(self);
    if (offset==cInvalidUint32)
        return 0;

    else
        {
        Tha6A210031ModulePrbs prbs = PrbsModule(self);
        uint32 hwAddress    = Tha6A210031ModulePrbsCounterReg(prbs, cAtFalse, r2c);
        uint32 regAddr      = hwAddress + offset;
        uint32 regVal       = 0;
        uint32 regFieldMask = 0, regFieldShift = 0;

        if (isLack && counterType==cAtPrbsEngineCounterRxSync)
            {
            regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
            ((tTha6A210031PrbsEnginePw*)self)->lackCounterValue = regVal;
            }
        else
            regVal = ((tTha6A210031PrbsEnginePw*)self)->lackCounterValue;

        regFieldMask   = Tha6A210031ModulePrbsCounterMask(prbs, counterType);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        return mRegField(regVal, regField);
        }
    }

static uint32 HwCounterGet(AtPrbsEngine self, uint16 counterType, eBool r2c, eBool isLack)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    if (!mMethodsGet(prbs)->NeedFixedPattern4bytes(prbs))
        return HwCounterRead2Clear(self, counterType, r2c);

    return HwCounterGet_FixPattern4bytes(self, counterType, r2c, isLack);
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    if (AtPrbsEngineCounterIsSupported(self, counterType))
        return HwCounterGet(self, counterType, cAtFalse, cAtTrue);
    return 0;
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    if (AtPrbsEngineCounterIsSupported(self, counterType))
        return HwCounterGet(self, counterType, cAtTrue, cAtTrue);
    return 0;
    }

static eBool CounterIsSupported(AtPrbsEngine self, uint16 counterType)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    return Tha6A210031ModulePrbsCounterIsSupported(prbs, counterType);
    }

static eAtModulePrbsRet AllCountersLatchAndClear(AtPrbsEngine self, eBool clear)
    {
    uint16 counterType_i = 0;
    uint16 counterType[] = {cAtPrbsEngineCounterRxSync,
                            cAtPrbsEngineCounterRxErrorFrame};
    for (counterType_i=0; counterType_i< mCount(counterType); counterType_i++)
        {
        if (AtPrbsEngineCounterIsSupported(self, counterType[counterType_i]))
            HwCounterGet(self, counterType[counterType_i], clear, cAtTrue);
        }

    return cAtOk;
    }

static uint32 AlarmRead2Clear(AtPrbsEngine self, eBool clear)
    {
    AtUnused(clear);
    /* Work around: use counter for sticky */
    if (AtPrbsEngineCounterClear(self, cAtPrbsEngineCounterRxBitError) != 0)
        return cAtPrbsEngineAlarmTypeError;

    return cAtPrbsEngineAlarmTypeNone;
    }

static uint32 LoPathPrbsEngineAlarmRead_FixPattern4bytes(AtPrbsEngine self, eBool readHw)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 base      = Tha6A210031ModulePrbsMonReg(prbs, cAtFalse);
    uint32 address   = base + Tha6A210031PrbsEnginePwDefaultOffset(self);
    if (readHw)
        {
        uint32 regValue  = AtPrbsEngineRead(self, address, cAtModulePrbs);
        self->regValue_stickyand_status =regValue;
        }
    return self->regValue_stickyand_status;
    }

static uint32 PathPrbsEngineAlarmGet_FixPattern4bytes(AtPrbsEngine self)
    {
    uint32 regValue  = LoPathPrbsEngineAlarmRead_FixPattern4bytes(self, cAtTrue);
    if ((regValue & c_prbslomon_PrbsLoMonSync_Mask) == 0)
        return cAtPrbsEngineAlarmTypeLossSync;

    return 0;
    }


static uint32 AlarmGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    if (!mMethodsGet(prbs)->NeedFixedPattern4bytes(prbs))
        return AlarmRead2Clear(self, cAtFalse);
    else
        return PathPrbsEngineAlarmGet_FixPattern4bytes(self);
    }

static uint32 PathPrbsEngineAlarmClear_FixPattern4bytes(AtPrbsEngine self)
    {
    uint32 regValue  = LoPathPrbsEngineAlarmRead_FixPattern4bytes(self, cAtFalse);
    if (regValue & c_prbslomon_PrbsLoMonErr_Mask)
        return cAtPrbsEngineAlarmTypeError;
    return 0;
    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    if (!mMethodsGet(prbs)->NeedFixedPattern4bytes(prbs))
        return AlarmRead2Clear(self, cAtTrue);
    return PathPrbsEngineAlarmClear_FixPattern4bytes(self);
    }

static eBool NeedSetDefaultPrbsLoss(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule((AtPrbsEngine)self);
    return Tha6A210031ModulePrbsLossIsSuppported(prbs);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, SeparateTwoDirections);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, ModeIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, IsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, CounterIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, AllCountersLatchAndClear);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionLongRead);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionLongWrite);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultMode);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultPrbsLoss);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideTha61031031PrbsEnginePw(Tha61031031PrbsEngineTdmPw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEnginePwOverride, mMethodsGet(self), sizeof(m_Tha61031031PrbsEnginePwOverride));


        }

    mMethodsSet(self, &m_Tha61031031PrbsEnginePwOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideTha61031031PrbsEnginePw((Tha61031031PrbsEngineTdmPw) self);
    }

AtPrbsEngine Tha6A290011PrbsEngineObjectInit(AtPrbsEngine self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031PrbsEnginePwObjectInit(self, pw) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A290011PrbsEnginePwNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290011PrbsEngineObjectInit(newEngine, pw);
    }

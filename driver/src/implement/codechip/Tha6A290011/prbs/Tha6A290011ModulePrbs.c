/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210031ModulePrbs.c
 *
 * Created Date: Oct 8, 2015
 *
 * Description : Tha6A210031 module PRBS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290011ModulePrbsInternal.h"
#include "Tha6A290011ModulePrbsReg.h"
#include "../../Tha6A210031/prbs/Tha6A210031ModulePrbsReg.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "Tha6A290011PrbsEngineInternal.h"
#include "Tha6A290011ModulePrbsLossTimerReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cAf6Reg_prbsgenfxptdatlo                                        0x1BC400
#define cAf6Reg_prbsmonfxptdatlo                                        0x1AC400
#define cAf6Reg_prbsgenfxptdatlo_2048                                   0x1BC800
#define cAf6Reg_prbsmonfxptdatlo_2048                                   0x1AC800
#define cAf6_prbsgenfxptdat_fxptdat_Mask                                cBit31_0
#define cAf6_prbsmonfxptdat_fxptdat_Mask                                cBit31_0

/* data gen mode. 2'b0: Round-trip delay 2'b1: Fix-pattern 2'b2: PRBS mode 2'b3: Sequence mode */
#define cAf6_prbsgenctrl4lo_cfgmode3_Mask                                cBit1_0
#define cAf6_prbsmonctrl4lo_cfgmode3_Mask                                cBit1_0

#define cAf6_moncntro_goodcnt_Mask                                     cBit31_16
#define cAf6_moncntro_goodcnt_Shift                                           16
#define cAf6_moncntro_badcnt_Mask                                       cBit15_0
#define cAf6_moncntro_badcnt_Shift                                             0

#define cAf6Reg_moncntr2clo                                             0x1AD800
#define cAf6Reg_moncntrolo                                              0x1AD000
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePrbsMethods          m_AtModulePrbsOverride;
static tTha61031031ModulePrbsMethods m_Tha61031031ModulePrbsOverride;
static tTha6A210031ModulePrbsMethods m_Tha6A210031ModulePrbsOverride;

static const tTha6A210031ModulePrbsMethods *m_Tha6A210031ModulePrbsMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static ThaVersionReader VersionReader(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet(self);
    return ThaDeviceVersionReader(device);
    }

static uint32 StartVersionSupportPrbsDataMode(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0013);
    }

static eBool PrbsDataModeIsSupported(Tha6A210031ModulePrbs self)
    {
    uint32 startVersionHasThis = StartVersionSupportPrbsDataMode(self);
    uint32 currentVersion =
            ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtModule) self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 StartVersionSupportShiftUp(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0014);
    }

static uint32 PrbsLossExpectMaxTimeMaskLo(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return c_upen_tim_cfg_prbs_tim_cfg_Mask_01;
    }
static uint32 PrbsLossExpectMaxTimeMaskHo(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return c_upen_tim_cfg_prbs_tim_cfg_Mask_02;
    }

static uint32 PrbsLossCurTimeMask(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return c_upen_tim_sta_tim_out_Mask;
    }

static uint32 PrbsLossSigThresMask(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return c_upen_tim_cfg_vld_max_cfg_Mask;
    }

static uint32 PrbsSyncDectectThresMask(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return c_upen_tim_cfg_prbs_max_cfg_Mask;
    }

static uint32 PrbsLossDectectThresMask(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return c_upen_tim_cfg_loss_max_cfg_Mask;
    }

static uint32 PrbsLossUnitMask(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return c_upen_mls_cfg_unit_tim_Mask;
    }

static uint32 PrbsLossTimerUnitReg(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    if (Tha6A210031ModulePrbsNeedShiftUp(self))
        return 0x1A7802;
    return cReg_upen_mls_cfg;
    }

static uint32 PrbsLossTimerCfgReg(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    if (Tha6A210031ModulePrbsNeedShiftUp(self))
        return 0x1A7800;
    return cReg_upen_tim_cfg;
    }

static uint32 PrbsLossTimerStatReg(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cReg_upen_tim_sta;
    }

static uint32 StartVersionSupportFixedPattern4bytes(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0011);
    }

static uint32 StartVersionSupportDelay(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0011);
    }

static uint32 StartVersionSupportPrbsLoss(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0012);
    }

static uint32 PrbsModeMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        {
        if (isHo)
            return cInvalidUint32;
        return isTxDirection?cThaRegPrbsLoGenCtlDataModMask: cThaRegPrbsLoMonCtlDataModMask;
        }

    if (isHo)
        return cInvalidUint32;
    return isTxDirection?cAf6_prbsgenctrl4lo_cfgmode3_Mask: cAf6_prbsmonctrl4lo_cfgmode3_Mask;
    }

static uint32 PrbsFixPatternMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        {
        if (isHo)
            return isTxDirection? cInvalidUint32: cInvalidUint32;
        return isTxDirection ?cThaRegPrbsLoGenCtlFixPattMask: cThaRegPrbsLoMonCtlFixPattMask;
        }

    if (isHo)
        return cInvalidUint32;
    return isTxDirection ?cAf6_prbsgenfxptdat_fxptdat_Mask: cAf6_prbsmonfxptdat_fxptdat_Mask;
    }

static uint32 PrbsEnableMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        {
        if (isHo)
            return isTxDirection ? cInvalidUint32: cInvalidUint32;
        return isTxDirection ?cThaRegPrbsLoGenCtlEnbMask: cThaRegPrbsLoMonCtlEnbMask;
        }
    return cInvalidUint32;
    }

static uint32 PrbsInvertMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        {
        AtUnused(self);
        if (isHo)
            return cInvalidUint32;
        return isTxDirection ?cThaRegPrbsLoGenCtlInvertMask: cThaRegPrbsLoMonCtlInvertMask;
        }
    return cInvalidUint32;
    }

static uint32 PrbsErrFrcMask(Tha6A210031ModulePrbs self, eBool isHo)
    {
    AtUnused(self);
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        {
        if (isHo)
            return cInvalidUint32;
        return cThaRegPrbsLoGenCtlErrFrcMask;
        }
    return cInvalidUint32;
    }

static uint8 PrbsModeToHwValue_FixPatern1Byte(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    switch ((uint32)prbsMode)
        {
        case cAtPrbsModePrbsFixedPattern1Byte: return 0;
        case cAtPrbsModePrbsSeq:               return 1;
        case cAtPrbsModePrbs11:                return 2;
        case cAtPrbsModePrbs15:                return 3;
        case cAtPrbsModePrbs20StdO151:         return 4;
        case cAtPrbsModePrbs23:                return 5;
        case cAtPrbsModePrbs31:                return 6;

        default:                               return cInvalidUint8;
        }
    }

static uint32 ConvertSwToHwFixedPattern_FixedPattern4bytes(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    uint8 i=0;
    uint32 value = 0;
    AtUnused(self);
    if (prbsMode == cAtPrbsModePrbsFixedPattern1Byte)
        {
        for (i=0; i<4; i++)
            {
            if (i == 0)
                value = (fixedPattern & cBit7_0);
            else
                value = (value << 8) | (fixedPattern & cBit7_0);
            }
        }
    else if (prbsMode == cAtPrbsModePrbsFixedPattern2Bytes)
        {
        for (i=0; i<2; i++)
            {
            if (i == 0)
                value = (fixedPattern & cBit15_0);
            else
                value = (value << 16) | (fixedPattern & cBit15_0);
            }
        }
    else if (prbsMode == cAtPrbsModePrbsFixedPattern3Bytes)
        value = fixedPattern & cBit23_0;
    else
        value = fixedPattern;

    return value;
    }

static uint32 ConvertSwToHwFixedPattern(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        return m_Tha6A210031ModulePrbsMethods->ConvertSwToHwFixedPattern(self, prbsMode, fixedPattern);
    else
        return ConvertSwToHwFixedPattern_FixedPattern4bytes(self, prbsMode, fixedPattern);
    }

static uint32 ConvertHwToSwFixedPattern_FixedPattern4bytes(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    uint32 value = fixedPattern;

    AtUnused(self);
    if (prbsMode == cAtPrbsModePrbsFixedPattern1Byte)
        value = fixedPattern & cBit7_0;
    else if (prbsMode == cAtPrbsModePrbsFixedPattern2Bytes)
        value = fixedPattern & cBit15_0;
    else if (prbsMode == cAtPrbsModePrbsFixedPattern3Bytes)
        value = (fixedPattern <<8) & cBit23_0;
    return value;
    }

static uint32 ConvertHwToSwFixedPattern(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        return m_Tha6A210031ModulePrbsMethods->ConvertHwToSwFixedPattern(self, prbsMode, fixedPattern);
    else
        return ConvertHwToSwFixedPattern_FixedPattern4bytes(self, prbsMode, fixedPattern);
    }

static uint8 PrbsModeToHwValue(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode)
    {

    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        return PrbsModeToHwValue_FixPatern1Byte(self, prbsMode);

    if (prbsMode == cAtPrbsModePrbsFixedPattern3Bytes)
        return 0;
    if (prbsMode == cAtPrbsModePrbsFixedPattern1Byte||
        prbsMode == cAtPrbsModePrbsFixedPattern2Bytes||
        prbsMode == cAtPrbsModePrbsFixedPattern4Bytes)
        return 1;
    if (prbsMode == cAtPrbsModePrbs15)
        return 2;
    if (prbsMode == cAtPrbsModePrbsSeq)
        return 3;
    return 0;
    }

static eAtPrbsMode PrbsModeFromHwValue_FixPatern1Byte(uint8 mode)
    {
    switch ((uint8)mode)
        {
        case 0: return cAtPrbsModePrbsFixedPattern1Byte;
        case 1: return cAtPrbsModePrbsSeq              ;
        case 2: return cAtPrbsModePrbs11               ;
        case 3: return cAtPrbsModePrbs15               ;
        case 4: return cAtPrbsModePrbs20StdO151        ;
        case 5: return cAtPrbsModePrbs23               ;
        case 6: return cAtPrbsModePrbs31               ;
        default:return cAtPrbsModeInvalid;
        }
    }

static eAtPrbsMode PrbsModeFromHwValue_FixedPattern4bytes(uint8 mode, eAtPrbsMode catchedPrbsMode)
    {
    if (mode == 0)
        return cAtPrbsModePrbsFixedPattern3Bytes;
    if (mode == 1)
        return catchedPrbsMode;
    if (mode == 2)
        return cAtPrbsModePrbs15;
    if (mode == 3)
        return cAtPrbsModePrbsSeq;
    return cAtPrbsModePrbs15;
    }

static eAtPrbsMode PrbsModeFromHwValue(Tha6A210031ModulePrbs self, eAtPrbsMode catchedPrbsMode, uint8 mode, uint8 step)
    {
    AtUnused(step);
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        return PrbsModeFromHwValue_FixPatern1Byte(mode);
    return PrbsModeFromHwValue_FixedPattern4bytes(mode, catchedPrbsMode);
    }

static uint32 PrbsFixPatternReg(Tha6A210031ModulePrbs self, eBool isHo, eBool isTx)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        return isTx? Tha6A210031ModulePrbsGenReg(self, isHo): Tha6A210031ModulePrbsMonReg(self, isHo);
    if (Tha6A210031ModulePrbsNeedShiftUp(self))
        {
        if (isHo)
            return cInvalidUint32;
        return isTx? cAf6Reg_prbsgenfxptdatlo_2048: cAf6Reg_prbsmonfxptdatlo_2048;
        }
    else
        {
        if (isHo)
            return cInvalidUint32;
        return isTx? cAf6Reg_prbsgenfxptdatlo: cAf6Reg_prbsmonfxptdatlo;
        }
    }

static eBool CounterIsSupported_FixedPattern1byte(Tha6A210031ModulePrbs self, uint16 counterType)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtPrbsEngineCounterRxBit:      return cAtTrue;
        case cAtPrbsEngineCounterRxBitError: return cAtTrue;

        default: return cAtFalse;
        }
    }

static eBool CounterIsSupported_FixedPattern4bytes(Tha6A210031ModulePrbs self, uint16 counterType)
    {
    AtUnused(self);
    switch (counterType)
        {
        case cAtPrbsEngineCounterRxSync:       return cAtTrue;
        case cAtPrbsEngineCounterRxErrorFrame: return cAtTrue;
        default:                               return cAtFalse;
        }
    return cAtFalse;
    }

static eBool CounterIsSupported(Tha6A210031ModulePrbs self, uint16 counterType)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        return CounterIsSupported_FixedPattern1byte(self, counterType);
    return CounterIsSupported_FixedPattern4bytes(self, counterType);
    }

static uint32 PrbsCounterMask(Tha6A210031ModulePrbs self, uint16 counterType)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        return cInvalidUint32;

    switch (counterType)
        {
        case cAtPrbsEngineCounterRxSync:       return cAf6_moncntro_goodcnt_Mask;
        case cAtPrbsEngineCounterRxErrorFrame: return cAf6_moncntro_badcnt_Mask;
        default: return cInvalidUint32;
        }
    return cInvalidUint32;
    }

static uint32 PrbsCounterReg(Tha6A210031ModulePrbs self, eBool isHo, eBool r2c)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        return cInvalidUint32;

    if (isHo)
        return cInvalidUint32;
    return r2c ? cAf6Reg_moncntr2clo : cAf6Reg_moncntrolo;
    }

static eBool AllChannelsSupported(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtPrbsEngine De1PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A290011PrbsEngineDe1New(de1);
    }

static AtPrbsEngine NxDs0PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A290011PrbsEngineNxDs0New(nxDs0);
    }

static AtPrbsEngine PwPrbsEngineObjectCreate(Tha61031031ModulePrbs self, AtPw pw)
    {
    AtUnused(self);
    return Tha6A290011PrbsEnginePwNew(pw);
    }

static void OverrideTha6A210031ModulePrbs(Tha6A210031ModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6A210031ModulePrbsMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031ModulePrbsOverride, m_Tha6A210031ModulePrbsMethods , sizeof(m_Tha6A210031ModulePrbsOverride));

        mMethodOverride(m_Tha6A210031ModulePrbsOverride, StartVersionSupportDelay);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, StartVersionSupportFixedPattern4bytes);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsModeMask);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsFixPatternMask);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsEnableMask);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsInvertMask);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsErrFrcMask);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsModeToHwValue);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsModeFromHwValue);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsFixPatternReg);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, ConvertSwToHwFixedPattern);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, ConvertHwToSwFixedPattern);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, CounterIsSupported);

        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsCounterMask);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsCounterReg);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsLossTimerUnitReg);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsLossTimerCfgReg);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsLossTimerStatReg);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsLossExpectMaxTimeMaskLo);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsLossExpectMaxTimeMaskHo);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, StartVersionSupportShiftUp);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsLossCurTimeMask);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsLossSigThresMask);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsSyncDectectThresMask);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsLossDectectThresMask);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsLossUnitMask);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, StartVersionSupportPrbsLoss);
        mMethodOverride(m_Tha6A210031ModulePrbsOverride, PrbsDataModeIsSupported);
        }

    mMethodsSet(self, &m_Tha6A210031ModulePrbsOverride);
    }

static void OverrideTha61031031ModulePrbs(Tha61031031ModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031ModulePrbsOverride, mMethodsGet(self), sizeof(m_Tha61031031ModulePrbsOverride));

        mMethodOverride(m_Tha61031031ModulePrbsOverride, PwPrbsEngineObjectCreate);
        }

    mMethodsSet(self, &m_Tha61031031ModulePrbsOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, mMethodsGet(self), sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, De1PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, NxDs0PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, AllChannelsSupported);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtModulePrbs((AtModulePrbs) self);
    OverrideTha61031031ModulePrbs((Tha61031031ModulePrbs)self);
    OverrideTha6A210031ModulePrbs((Tha6A210031ModulePrbs) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011ModulePrbs);
    }

static AtModulePrbs ObjectInit(AtModulePrbs self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha6A290011ModulePrbsNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    return ObjectInit(newModule, device);
    }

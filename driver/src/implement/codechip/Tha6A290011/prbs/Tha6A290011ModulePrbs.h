/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210031ModulePrbsInternal.h
 * 
 * Created Date: Dec 8, 2015
 *
 * Description : Module PRBS internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011MODULEPRBS_H_
#define _THA6A290011MODULEPRBS_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290011ModulePrbs * Tha6A290011ModulePrbs;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha6A290011ModulePrbsNew(AtDevice device);
AtPrbsEngine Tha6A290011PrbsEnginePwNew(AtPw pw);
AtPrbsEngine Tha6A290011PrbsEngineDe1New(AtPdhDe1 de1);
AtPrbsEngine Tha6A290011PrbsEngineNxDs0New(AtPdhNxDS0 nxDs0);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290011MODULEPRBS_H_ */


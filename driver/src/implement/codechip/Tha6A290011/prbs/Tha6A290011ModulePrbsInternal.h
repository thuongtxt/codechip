/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A290011ModulePrbsInternal.h
 * 
 * Created Date: Jun 7, 2018
 *
 * Description : Module PRBS internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011MODULEPRBSINTERNAL_H_
#define _THA6A290011MODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A210031/prbs/Tha6A210031ModulePrbsInternal.h"
#include "Tha6A290011ModulePrbs.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290011ModulePrbs
    {
    tTha6A210031ModulePrbs super;
    }tTha6A290011ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/


#ifdef __cplusplus
}
#endif
#endif /* _THA6A290011MODULEPRBSINTERNAL_H_ */


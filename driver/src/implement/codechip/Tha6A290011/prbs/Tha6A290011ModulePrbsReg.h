/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210031ModulePrbsReg.h
 * 
 * Created Date: Oct 9, 2015
 *
 * Description : PRBS register description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011MODULEPRBSREG_H_
#define _THA6A290011MODULEPRBSREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*
#define cThaPrbsEnable 0x247000
#define cThaPrbsStatus 0x247800
*/

#define cThaRegPrbsErrorCounter          0x1AD000
#define cThaRegPrbsGoodCounter           0x1AD400

#define cThaRegPrbsLoMonStat             0x1AC800
#define cThaRegPrbsLoMonStatSynMask      cBit0
#define cThaRegPrbsLoMonStatSynShift     0

#define cThaRegPrbsLoMonStitcky          0x1AF020
#define cThaRegPrbsLoMonStickyErrMask    cBit1
#define cThaRegPrbsLoMonStickyErrShift   1

#define cThaRegPrbsLoGenCtlDataModMask    cBit3_1
#define cThaRegPrbsLoGenCtlDataModShift   1

#define cThaRegPrbsLoGenCtlFixPattMask    cBit11_4
#define cThaRegPrbsLoGenCtlFixPattShift   4

#define cThaRegPrbsLoGenCtlInvertMask     cBit12
#define cThaRegPrbsLoGenCtlInvertShift    12

#define cThaRegPrbsLoGenCtlErrFrcMask     cBit13
#define cThaRegPrbsLoGenCtlErrFrcShift    13

#define cThaRegPrbsLoMonCtlEnbMask        cBit0
#define cThaRegPrbsLoMonCtlEnbShift       0

#define cThaRegPrbsLoMonCtlDataModMask    cBit3_1
#define cThaRegPrbsLoMonCtlDataModShift   1

#define cThaRegPrbsLoMonCtlFixPattMask    cBit11_4
#define cThaRegPrbsLoMonCtlFixPattShift   4

#define cThaRegPrbsLoMonCtlInvertMask     cBit12
#define cThaRegPrbsLoMonCtlInvertShift    12

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210031_PRBS_THA6A290011MODULEPRBSREG_H_ */


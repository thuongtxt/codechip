/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210021PrbsEngineDe1.c
 *
 * Created Date: Dec 23, 2015
 *
 * Description : DE1 PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290011PrbsEngineInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaPdhDe1.h"
#include "../../Tha6A290021/prbs/Tha6A290021PrbsEngineDe1Internal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8                      m_methodsInit = 0;

/* Override */
static tTha61031031PrbsEngineTdmPwMethods m_Tha61031031PrbsEngineOverride;
static tAtPrbsEngineMethods               m_AtPrbsEngineOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods  *m_AtPrbsEngineMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eBool IsUnFrameMode(uint16 frameType)
    {
    if ((frameType == cAtPdhE1UnFrm) || (frameType == cAtPdhDs1J1UnFrm))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsDs1FrameMode(uint16 frameValue)
    {
    if ((frameValue == cAtPdhDs1FrmSf) ||
        (frameValue == cAtPdhDs1FrmEsf) ||
        (frameValue == cAtPdhDs1FrmDDS) ||
        (frameValue == cAtPdhDs1FrmSLC))
        return cAtTrue;

    return cAtFalse;
    }

static AtChannel CircuitToBind(Tha61031031PrbsEngineTdmPw self, AtChannel channel)
    {
    uint32 nxDs0BitMask;
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)channel);
    uint32 cDs0MaskOfE1ForPrbs = cBit31_1;
    uint32 cDs0MaskOfDs1ForPrbs = cBit23_0;

    if (frameType == cAtPdhDe1FrameUnknown)
        {
        mChannelLog(channel, cAtLogLevelWarning, "Frame type of circuit is unknown");
        return NULL;
        }

    if (IsUnFrameMode(frameType))
        return channel;

    nxDs0BitMask = IsDs1FrameMode(frameType) ? cDs0MaskOfDs1ForPrbs : cDs0MaskOfE1ForPrbs;
    AtPdhDe1NxDs0Create((AtPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self), nxDs0BitMask);
    return (AtChannel)AtPdhDe1NxDs0Get((AtPdhDe1)channel, nxDs0BitMask);
    }

static AtPw PwCreate(Tha61031031PrbsEngineTdmPw self, uint32 pwId)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    if (IsUnFrameMode(AtPdhChannelFrameTypeGet((AtPdhChannel)channel)))
        return (AtPw)AtModulePwSAToPCreate(pwModule, (uint16)pwId);

    /* When E1 is in frame mode, we should create a CESoP instead of SAToP */
    return (AtPw)AtModulePwCESoPCreate(pwModule, (uint16)pwId, cAtPwCESoPModeBasic);
    }

static eAtModulePrbsRet TxErrorRateSet(AtPrbsEngine self, eAtBerRate errorRate)
    {
    return Tha6A290021PrbsEngineDe1TxErrorRateSet(self, errorRate);
    }

static eAtBerRate TxErrorRateGet(AtPrbsEngine self)
    {
    return Tha6A290021PrbsEngineDe1TxErrorRateGet(self);
    }

static eBool ErrorForcingRateIsSupported(AtPrbsEngine self, uint32 errorRate)
    {
    return Tha6A290021PrbsEngineDe1ErrorForcingRateIsSupported(self, errorRate);
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    return Tha6A290021PrbsEngineDe1ErrorForce(self, force);
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    return Tha6A290021PrbsEngineDe1ErrorIsForced(self);
    }

static void OverrideTha61031031PrbsEngine(Tha61031031PrbsEngineTdmPw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEngineOverride, mMethodsGet(self), sizeof(m_Tha61031031PrbsEngineOverride));

        mMethodOverride(m_Tha61031031PrbsEngineOverride, CircuitToBind);
        mMethodOverride(m_Tha61031031PrbsEngineOverride, PwCreate);
        }

    mMethodsSet(self, &m_Tha61031031PrbsEngineOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxErrorRateGet);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForcingRateIsSupported);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }


static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011PrbsEngineDe1);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideTha61031031PrbsEngine((Tha61031031PrbsEngineTdmPw)self);
    OverrideAtPrbsEngine((AtPrbsEngine)self);
    }

static AtPrbsEngine Tha6A290011PrbsEngineDe1ObjectInit(AtPrbsEngine self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031PrbsEngineDe1ObjectInit(self, de1) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A290011PrbsEngineDe1New(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290011PrbsEngineDe1ObjectInit(newEngine, de1);
    }



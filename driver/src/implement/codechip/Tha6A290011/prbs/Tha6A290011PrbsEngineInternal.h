/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210021PrbsEngineInternal.h
 * 
 * Created Date: Dec 24, 2015
 *
 * Description : PRBS Engine
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011PRBSENGINEINTERNAL_H_
#define _THA6A290011PRBSENGINEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha6A210031/prbs/Tha6A210031PrbsEnginePwInternal.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290011PrbsEngine *Tha6A290011PrbsEngine;
typedef struct tTha6A290011PrbsEnginePw
    {
    tTha6A210031PrbsEnginePw super;
    }tTha6A290011PrbsEnginePw;

typedef struct tTha6A290011PrbsEngineDe1
    {
    tTha6A210031PrbsEngineDe1 super;
    }tTha6A290011PrbsEngineDe1;

typedef struct tTha6A290011PrbsEngineNxDs0
    {
    tTha6A210031PrbsEngineNxDs0 super;
    }tTha6A290011PrbsEngineNxDs0;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPrbsEngine Tha6A290011PrbsEngineObjectInit(AtPrbsEngine self, AtPw pw);
AtPrbsEngine Tha6A290011PrbsEngineDe1New(AtPdhDe1 de1);
AtPrbsEngine Tha6A290011PrbsEngineNxDs0New(AtPdhNxDS0 nxDs0);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290011PRBSENGINEINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A290011AttControllerInternal.h
 * 
 * Created Date: Sep 7, 2017
 *
 * Description : ATT SDH controller internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011SDHATTCONTROLLERINTERNAL_H_
#define _THA6A290011SDHATTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
/*#include "../pdh/Tha6A290021PdhAttControllerInternal.h"*/
#include "../../../../../include/att/AtAttController.h"
#include "Tha6A290011ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttController Tha6A290011SdhLineAttControllerNew(AtChannel sdhLine);
AtAttController Tha6A290011SdhPathAttControllerNew(AtChannel sdhPath);
AtAttController Tha6A290011SdhVc1xAttControllerNew(AtChannel sdhPath);
eBool Tha6A290011AttSdhForceLogicFromDut(AtChannel self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290011SDHATTCONTROLLERINTERNAL_H_ */


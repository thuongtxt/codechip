/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A290011SdhPathAttController.c
 *
 * Created Date: Sep 9, 2017
 *
 * Description : SDH path ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha6A210031/sdh/Tha6A210031SdhAttControllerInternal.h"
#include "Tha6A290011SdhAttControllerInternal.h"
#include "Tha6A290011SdhAttAdjPtrReg.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011SdhPathAttController
    {
    tTha6A210031SdhPathAttController super;
    uint32          pointerAdjDuration; /* in ms */
    }tTha6A290011SdhPathAttController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaAttControllerMethods                m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 StartVersionSupportDataMask8bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtSdhPathCounterTypeBip)
        return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0013);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportDataMask2bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtSdhPathCounterTypeBip)
        return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportConvertStep(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0013);
    }

static uint32 StartVersionSupportPointerAdj(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0015);
    }

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x2, 0x2222);
    }

static eBool IsHoStsPointerAdj(Tha6A210031PdhDe3AttController self)/* AU3/VC3 */
    {
    AtChannel channel = AtAttControllerChannelGet((AtAttController)self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet((AtSdhChannel)channel);
    if (channelType == cAtSdhChannelTypeAu3)
        {
        AtSdhChannel sub = AtSdhChannelSubChannelGet((AtSdhChannel)channel, 0);
        uint8 mapType = AtSdhChannelMapTypeGet((AtSdhChannel)sub);
        if (mapType == cAtSdhVcMapTypeVc3MapC3)
           return cAtFalse;
        return cAtTrue;
        }
    else if (channelType == cAtSdhChannelTypeVc3)
        {
        AtSdhChannel parent = AtSdhChannelParentChannelGet((AtSdhChannel)channel);
        if  (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeAu3)
            {
            uint8 mapType = AtSdhChannelMapTypeGet((AtSdhChannel)channel);
            if (mapType == cAtSdhVcMapTypeVc3Map7xTug2s||
                mapType == cAtSdhVcMapTypeVc3MapDe3    ||
                mapType == cAtSdhVcMapTypeVc1xMapC1x   ||
                mapType == cAtSdhVcMapTypeVc1xMapDe1 )
                return cAtTrue;
            }
        }
    return cAtFalse;
    }

static uint32 RegPointerAdj(Tha6A210031PdhDe3AttController self)
    {
    if (IsHoStsPointerAdj(self))
        return cAf6Reg_stshoadj; /* VC3 store 7VTG2, VC3 STORE DE3 */
    else
        return cAf6Reg_stsloadj; /* TU3->VC3/E3/DS3 VC11 VC12*/
    }

static uint32 PointerRealAddressHoSts(Tha6A210031PdhDe3AttController self, ThaModuleOcn ocnModule, uint32 regAddr, uint8 slice, uint8 hwStsInSlice)
    {
    AtUnused(self);
    if (    (hwStsInSlice >= 4  && hwStsInSlice <= 7)  ||
            (hwStsInSlice >= 20 && hwStsInSlice <= 23) ||
            (hwStsInSlice >= 36 && hwStsInSlice <= 39)    )
        return regAddr + (hwStsInSlice + 4UL) + 0x2000UL * slice + Tha60210011ModuleOcnBaseAddress(ocnModule);
    return regAddr + hwStsInSlice + 0x2000UL * slice + Tha60210011ModuleOcnBaseAddress(ocnModule);
    }

static uint32 PointerRealAddressLoSts(Tha6A210031PdhDe3AttController self, ThaModuleOcn ocnModule, uint32 regAddr, uint8 slice, uint8 hwStsInSlice)
    {
    AtUnused(self);
    if ( (hwStsInSlice >= 4  && hwStsInSlice <= 7)  ||
         (hwStsInSlice >= 20 && hwStsInSlice <= 23) ||
         (hwStsInSlice >= 36 && hwStsInSlice <= 39)    )
        return regAddr + 0x20UL * (hwStsInSlice + 4UL) + 0x2000UL * slice + Tha60210011ModuleOcnBaseAddress(ocnModule);
    return regAddr + 0x20UL * hwStsInSlice  + 0x2000UL * slice + Tha60210011ModuleOcnBaseAddress(ocnModule);
    }

static uint32 PointerRealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice = 0, hwStsInSlice = 0;
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    if (ThaSdhChannel2HwMasterStsId(channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        {
        if (IsHoStsPointerAdj(self))
            return PointerRealAddressHoSts(self, ocnModule, regAddr, slice, hwStsInSlice);
        else
            return PointerRealAddressLoSts(self, ocnModule, regAddr, slice, hwStsInSlice);
        }
    return cInvalidUint32;
    }

static uint32 PointerAdjTypeMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(isError);
    AtUnused(errorType);
    return cAf6_stshoadj_rditype_Mask;
    }

static uint8  SwPointerAdjToHw(Tha6A210031PdhDe3AttController self, uint32 pointerAdjType)
    {
    AtUnused(self);
    if (pointerAdjType==cAtSdhPathPointerAdjTypeIncrease)
        return 3;
    return 4;
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportDataMask8bit);
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportDataMask2bit);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void OverrideTha6A210031PdhDe3AttController(Tha6A210031PdhDe3AttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(self), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportPointerAdj);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegPointerAdj);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PointerRealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, SwPointerAdjToHw);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PointerAdjTypeMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportConvertStep);
        }

    mMethodsSet(self, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideThaAttController((ThaAttController) self);
    OverrideTha6A210031PdhDe3AttController((Tha6A210031PdhDe3AttController)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011SdhPathAttController);
    }

static AtAttController ObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031SdhPathAttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290011SdhPathAttControllerNew(AtChannel sdhPath)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(controller, sdhPath);
    }

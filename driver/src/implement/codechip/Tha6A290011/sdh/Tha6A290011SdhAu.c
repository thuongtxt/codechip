/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A290011SdhAu.c
 *
 * Created Date: Sep 8, 2017
 *
 * Description : SDH AU
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/sdh/Tha60290011SdhAuInternal.h"
#include "Tha6A290011ModuleSdhInternal.h"
#include "Tha6A290011SdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290011SdhAu *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011SdhAu
    {
    tTha60290011LineAu super;
    }tTha6A290011SdhAu;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods            m_AtChannelOverride;
/*static tTha6029SdhAuMethods         m_Tha6029SdhAuOverride;*/

/* Save super implementation */
static const tAtChannelMethods     *m_AtChannelMethods   = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = cAtOk;
    ret = m_AtChannelMethods->TimingSet(self, timingMode, timingSource);
    if (timingMode == cAtTimingModeFreeRunning||timingMode == cAtTimingModeSys)
        AtAttControllerFrequenceOffsetSet(AtChannelAttController(self), 0);
    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AtAttControllerInit(AtChannelAttController(self));
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmTypes, eAtRet (*fForceAlarm)(AtAttController, uint32), eAtRet (*fSetClearAlarm)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
       {
       uint32 alarmType = alarmTypes & (cBit0 << i);
       if (alarmType)
           {
           AtAttControllerForceAlarmModeSet(attController, alarmType, cAtAttForceAlarmModeContinuous);
           fSetClearAlarm(attController, alarmType);
           ret |= fForceAlarm(attController, alarmType);
           }
       }
    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290011AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerForceAlarm,AtAttControllerTxAlarmsSet);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290011AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerUnForceAlarm,AtAttControllerTxAlarmsClear);
    }


static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (Tha6A290011AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcibleAlarmsGet(self);

    return cAtSdhPathAlarmUneq | cAtSdhPathAlarmRdi;
    }
static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorTypes, eAtRet (*fForceError)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 errorType = errorTypes & (cBit0 << i);
        if (errorType)
            {
            AtAttControllerForceErrorModeSet(attController, errorType, cAtAttForceErrorModeContinuous);
            ret |=  fForceError(attController, errorType);
            }
        }
    return ret;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290011AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerForceError);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290011AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorUnForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerUnForceError);
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    if (Tha6A290011AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcableErrorsGet(self);

    return cAtSdhPathCounterTypeBip | cAtSdhPathCounterTypeRei;
    }


static void OverrideAtChannel(AtChannel self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        }

    mMethodsSet(self, &m_AtChannelOverride);
    }
/*
static void OverrideTha6029SdhAu(Tha6029SdhAu self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        mMethodsGet(osal)->MemCpy(osal, &m_Tha6029SdhAuOverride, mMethodsGet(self), sizeof(m_Tha6029SdhAuOverride));

        mMethodOverride(m_Tha6029SdhAuOverride, SetSsDefaultValueToFalse);
        }

    mMethodsSet(self, &m_Tha6029SdhAuOverride);
    }*/

static void Override(AtSdhAu self)
    {
    OverrideAtChannel((AtChannel)self);
    /*OverrideTha6029SdhAu((Tha6029SdhAu) self);*/
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011SdhAu);
    }

static AtSdhAu ObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011SdhAuObjectInit(self, channelId, channelType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhAu Tha6A290011SdhAuNew(uint32 auId, uint8 auType, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhAu self = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (self == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(self, auId, auType, module);
    }


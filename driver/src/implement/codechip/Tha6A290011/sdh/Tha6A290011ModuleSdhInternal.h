/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6A290011ModuleSdhInternal.h
 * 
 * Created Date: Sep 8, 2017
 *
 * Description : Module SDH internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011MODULESDHINTERNAL_H_
#define _THA6A290011MODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/sdh/Tha60290021ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290011ModuleSdh * Tha6A290011ModuleSdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAu Tha6A290011SdhAuNew(uint32 auId, uint8 auType, AtModuleSdh module);
AtSdhLine Tha6A290011SdhLineNew(uint32 channelId, AtModuleSdh module);
AtSdhVc Tha6A290011SdhVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha6A290011SdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhTu Tha6A290011SdhTu1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290011MODULESDHINTERNAL_H_ */


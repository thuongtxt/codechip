/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A290011SdhLine.c
 *
 * Created Date: Sep 8, 2017
 *
 * Description : SDH line concrete code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/att/ThaAttSdhManagerInternal.h"
#include "../../Tha60290011/sdh/Tha60290011SdhLineInternal.h"
#include "Tha6A290011ModuleSdhInternal.h"
#include "Tha6A290011SdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290011SdhLine *)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011SdhLine
    {
    tTha60290011SdhLine super;
    }tTha6A290011SdhLine;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtChannelMethods      m_AtChannelOverride;

/* Save super implementation */
static const tAtObjectMethods      *m_AtObjectMethods    = NULL;
static const tAtChannelMethods     *m_AtChannelMethods   = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/


static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AtAttControllerInit(AtChannelAttController(self));
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmTypes, eAtRet (*fForceAlarm)(AtAttController, uint32), eAtRet (*fSetClearAlarm)(AtAttController, uint32))
    {
    uint32 i=0;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i = 0; i < 32; i++)
        {
        uint32 alarmType = alarmTypes & (cBit0 << i);
        if (alarmType)
            {
            AtAttControllerForceAlarmModeSet(attController, alarmType, cAtAttForceAlarmModeContinuous);
            fSetClearAlarm(attController, alarmType);
            ret = fForceAlarm(attController, alarmType);
            }
        }
    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290011AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerForceAlarm, AtAttControllerTxAlarmsSet);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    if (Tha6A290011AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxAlarmUnForce(self, alarmType);

    return HelperTxAlarmForce(self, alarmType, AtAttControllerUnForceAlarm,AtAttControllerTxAlarmsClear);
    }


static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    if (Tha6A290011AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcibleAlarmsGet(self);

    return cAtSdhLineAlarmLos | cAtSdhLineAlarmAis | cAtSdhLineAlarmRdi | cAtSdhLineAlarmLof;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorTypes, eAtRet (*fForceError)(AtAttController, uint32))
    {
    AtAttController attController = AtChannelAttController(self);
    AtAttControllerForceErrorModeSet(attController, errorTypes, cAtAttForceErrorModeContinuous);
    return fForceError(attController, errorTypes);
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290011AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerForceError);
    }

static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    if (Tha6A290011AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxErrorUnForce(self, errorType);

    return HelperTxErrorForce(self, errorType, AtAttControllerUnForceError);
    }

static uint32 TxForcableErrorsGet(AtChannel self)
    {
    if (Tha6A290011AttSdhForceLogicFromDut(self))
        return m_AtChannelMethods->TxForcableErrorsGet(self);

    return cAtSdhLineCounterTypeB1 | cAtSdhLineCounterTypeB2 | cAtSdhLineCounterTypeRei;
    }

static void OverrideAtObject(AtObject self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void OverrideAtChannel(AtChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(tAtChannelMethods));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        }

    mMethodsSet(self, &m_AtChannelOverride);
    }

static void Override(AtSdhLine self)
    {
    OverrideAtObject((AtObject)self);
    OverrideAtChannel((AtChannel)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011SdhLine);
    }

static AtSdhLine ObjectInit(AtSdhLine self, uint32 channelId, AtModuleSdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011SdhLineObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtSdhLine Tha6A290011SdhLineNew(uint32 channelId, AtModuleSdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtSdhLine newLine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLine == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLine, channelId, module);
    }

eBool Tha6A290011AttSdhForceLogicFromDut(AtChannel self)
    {
    AtModuleSdh module = (AtModuleSdh)AtChannelModuleGet(self);
    ThaAttSdhManager attManger = (ThaAttSdhManager)AtModuleSdhAttManager(module);
    return ThaAttSdhManagerDebugForcingLogicGet(attManger);
    }

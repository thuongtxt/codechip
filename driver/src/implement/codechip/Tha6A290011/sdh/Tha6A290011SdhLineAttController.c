/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A290011SdhLineAttController.c
 *
 * Created Date: Sep 9, 2019
 *
 * Description : SDH line ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha6A210031/sdh/Tha6A210031SdhAttControllerInternal.h"
#include "Tha6A290011SdhAttControllerInternal.h"
#include "Tha6A290011SdhAttReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegAtt_ais_force_new        0x21980

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011SdhLineAttController
    {
    tTha6A210031SdhLineAttController super;
    }tTha6A290011SdhLineAttController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaAttControllerMethods m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;
static tTha6A210031SdhLineAttControllerMethods m_Tha6A210031SdhLineAttControllerOverride;

static const tTha6A210031PdhDe3AttControllerMethods *m_Tha6A210031PdhDe3AttControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eBool  IsTwoB2Force(Tha6A210031SdhLineAttController self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportAlarmUnitConfig(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0011);
    }

static uint32 StartVersionSupportDataMask8bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtSdhLineCounterTypeB1||errorType == cAtSdhLineCounterTypeB2)
        return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0011);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportStep14bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtSdhLineCounterTypeB1||errorType == cAtSdhLineCounterTypeB2)
        return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0011);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x2, 0x2222);
    }

static uint32 RegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhLineAlarmAis:
            return cReg_upen_laiscfg;
        default:
            return m_Tha6A210031PdhDe3AttControllerMethods->RegAddressFromAlarmType(self, alarmType);
        }
    }

static eBool  AlarmIsFromError(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if (alarmType==cAtSdhLineAlarmLof || alarmType==cAtSdhLineAlarmRdi || alarmType==cAtSdhLineAlarmAis)
        return cAtTrue;
    return cAtFalse;
    }

/*
 LOF, LOS: 01240fc ( current value: 32'h1C713 for 1ms, 32'h38E1 for 125us)
 AIS,RDI:  012100e ( current value: 32'h12EBB for 1ms, 32'h25D for 125us)
 */
static uint32 RegAddressAlarmUnit(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    if (mMethodsGet(self)->IsNeedAlarmUnitConfig(self))
        {
        switch (alarmType)
           {
            case cAtSdhLineAlarmLos:
            case cAtSdhLineAlarmLof:
                return 0x253c0;
            case cAtSdhLineAlarmAis:
            case cAtSdhLineAlarmRdi:
                return 0x21bc0;
            default:
                return cInvalidUint32;
           }
        }
    return cInvalidUint32;
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));

        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportStep14bit);
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportDataMask8bit);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }


static void OverrideTha6A210031PdhDe3AttController(Tha6A210031PdhDe3AttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha6A210031PdhDe3AttControllerMethods =  mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(self), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIsFromError);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmUnitConfig);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressAlarmUnit);
        }

    mMethodsSet(self, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void OverrideTha6A210031SdhLineAttController(Tha6A210031SdhLineAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031SdhLineAttControllerOverride, mMethodsGet(self), sizeof(m_Tha6A210031SdhLineAttControllerOverride));

        mMethodOverride(m_Tha6A210031SdhLineAttControllerOverride, IsTwoB2Force);
        }

    mMethodsSet(self, &m_Tha6A210031SdhLineAttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideThaAttController((ThaAttController) self);
    OverrideTha6A210031PdhDe3AttController((Tha6A210031PdhDe3AttController) self);
    OverrideTha6A210031SdhLineAttController((Tha6A210031SdhLineAttController) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011SdhLineAttController);
    }

static AtAttController ObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031SdhLineAttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290011SdhLineAttControllerNew(AtChannel sdhLine)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(controller, sdhLine);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A290021ModuleSdh.c
 *
 * Created Date: Aug 2, 2017
 *
 * Description : SDH module of 6A290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/att/ThaAttSdhManagerInternal.h"
#include "../../Tha60290011/sdh/Tha60290011ModuleSdhInternal.h"
#include "Tha6A290011ModuleSdhInternal.h"
#include "Tha6A290011SdhAttControllerInternal.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A290011ModuleSdh*)self)

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011ModuleSdh
    {
    tTha60290011ModuleSdh super;
    }tTha6A290011ModuleSdh;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tAtModuleSdhMethods          m_AtModuleSdhOverride;
static tThaModuleSdhMethods          m_ThaModuleSdhOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods = NULL;
static const tAtModuleSdhMethods    *m_AtModuleSdhMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtObjectAny LineAttControllerCreate(ThaModuleSdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290011SdhLineAttControllerNew(channel);
    }
static AtObjectAny PathAttControllerCreate(ThaModuleSdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290011SdhPathAttControllerNew(channel);
    }
static AtObjectAny Vc1xAttControllerCreate(ThaModuleSdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A290011SdhVc1xAttControllerNew(channel);
    }

static AtSdhChannel ChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    if (channelType == cAtSdhChannelTypeLine)
        return (AtSdhChannel)Tha6A290011SdhLineNew(channelId, self);

    if (channelType == cAtSdhChannelTypeAu3)
        return (AtSdhChannel)Tha6A290011SdhAuNew(channelId, channelType, self);

    if (channelType == cAtSdhChannelTypeVc3)
        return (AtSdhChannel)Tha6A290011SdhVcNew(channelId, channelType, self);

    if ((channelType == cAtSdhChannelTypeTu12) || (channelType == cAtSdhChannelTypeTu11))
        return (AtSdhChannel)Tha6A290011SdhTu1xNew(channelId, channelType, self);

    if ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11))
        return (AtSdhChannel)Tha6A290011SdhVc1xNew(channelId, channelType, self);

    return m_AtModuleSdhMethods->ChannelCreate(self, lineId, parent, channelType, channelId);
    }

static eAtRet AllChannelsInterruptDisable(AtModule self)
    {
    AtUnused(self);
    return cAtOk;
    }

static AtAttSdhManager AttSdhManagerCreate(AtModuleSdh self)
    {
    return Tha6A290011AttSdhManagerNew(self);
    }

static void OverrideAtObject(AtModuleSdh self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        /*mMethodOverride(m_AtObjectOverride, Delete);*/
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, ChannelCreate);
        mMethodOverride(m_AtModuleSdhOverride, AttSdhManagerCreate);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void OverrideThaModuleSdh(ThaModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, mMethodsGet(self), sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, LineAttControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, PathAttControllerCreate);
        mMethodOverride(m_ThaModuleSdhOverride, Vc1xAttControllerCreate);
        }

    mMethodsSet(self, &m_ThaModuleSdhOverride);
    }

static void OverrideAtModule(AtModuleSdh self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, mMethodsGet(module), sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, AllChannelsInterruptDisable);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh((ThaModuleSdh) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011ModuleSdh);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha6A290011ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date: August 3, 2018                                                 
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef __REG_ATT_OCN_REG_H_
#define __REG_ATT_OCN_REG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : Force LOS Error Configuration
Reg Addr   : 0x25000-0x2501F
Reg Formula: 0x25000 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force LOS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_loscfg                                                                               0x25000
#define cReg_upen_loscfg_WidthVal                                                                           64

/*--------------------------------------
BitField Name: lostim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_loscfg_lostim_mod_Mask                                                                cBit27_26
#define c_upen_loscfg_lostim_mod_Shift                                                                      26

/*--------------------------------------
BitField Name: loserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_loscfg_loserr_clr_Mask                                                                cBit25_18
#define c_upen_loscfg_loserr_clr_Shift                                                                      18

/*--------------------------------------
BitField Name: loserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_loscfg_loserr_set_Mask                                                                cBit17_10
#define c_upen_loscfg_loserr_set_Shift                                                                      10

/*--------------------------------------
BitField Name: loserr_step
BitField Type: RW
BitField Desc: The Total Step to Force LOS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_loscfg_loserr_step_Mask                                                                 cBit9_2
#define c_upen_loscfg_loserr_step_Shift                                                                      2

/*--------------------------------------
BitField Name: losmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_loscfg_losmsk_pos_Mask_01                                                             cBit31_18
#define c_upen_loscfg_losmsk_pos_Shift_01                                                                   18
#define c_upen_loscfg_losmsk_pos_Mask_02                                                               cBit1_0
#define c_upen_loscfg_losmsk_pos_Shift_02                                                                    0

/*--------------------------------------
BitField Name: losmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_loscfg_losmsk_ena_Mask                                                                   cBit17
#define c_upen_loscfg_losmsk_ena_Shift                                                                      17

/*--------------------------------------
BitField Name: loserr_num
BitField Type: RW
BitField Desc: The Total LOS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_loscfg_loserr_num_Mask                                                                 cBit16_1
#define c_upen_loscfg_loserr_num_Shift                                                                       1

/*--------------------------------------
BitField Name: losfrc_mod
BitField Type: RW
BitField Desc: Force LOS Mode 0: One shot ( N events in T the unit time) 1:
continous (loserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_loscfg_losfrc_mod_Mask                                                                    cBit0
#define c_upen_loscfg_losfrc_mod_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Force LOS Error Status
Reg Addr   : 0x25020-0x2503F
Reg Formula: 0x25020 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force LOS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_lossta                                                                               0x25020
#define cReg_upen_lossta_WidthVal                                                                           64

/*--------------------------------------
BitField Name: lossta_almset
BitField Type: RW
BitField Desc: LOS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_lossta_lossta_almset_Mask                                                                cBit27
#define c_upen_lossta_lossta_almset_Shift                                                                   27

/*--------------------------------------
BitField Name: losstaclralrm
BitField Type: RW
BitField Desc: LOS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_lossta_losstaclralrm_Mask                                                             cBit22_15
#define c_upen_lossta_losstaclralrm_Shift                                                                   15

/*--------------------------------------
BitField Name: losstasetalrm
BitField Type: RW
BitField Desc: LOS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_lossta_losstasetalrm_Mask                                                              cBit14_7
#define c_upen_lossta_losstasetalrm_Shift                                                                    7

/*--------------------------------------
BitField Name: losstatimecnt
BitField Type: RW
BitField Desc: LOS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_lossta_losstatimecnt_Mask                                                               cBit6_0
#define c_upen_lossta_losstatimecnt_Shift                                                                    0

/*--------------------------------------
BitField Name: losstafrc_ena
BitField Type: RW
BitField Desc: LOS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_lossta_losstafrc_ena_Mask                                                                cBit31
#define c_upen_lossta_losstafrc_ena_Shift                                                                   31

/*--------------------------------------
BitField Name: losstaerrstep
BitField Type: RW
BitField Desc: The Second LOS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_lossta_losstaerrstep_Mask                                                             cBit30_23
#define c_upen_lossta_losstaerrstep_Shift                                                                   23

/*--------------------------------------
BitField Name: losstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask LOS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_lossta_losstamsk_pos_Mask                                                             cBit22_19
#define c_upen_lossta_losstamsk_pos_Shift                                                                   19

/*--------------------------------------
BitField Name: losstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error LOS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_lossta_losstaerr_cnt_Mask                                                              cBit18_3
#define c_upen_lossta_losstaerr_cnt_Shift                                                                    3

/*--------------------------------------
BitField Name: losstafrc_sta
BitField Type: RW
BitField Desc: LOS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_lossta_losstafrc_sta_Mask                                                               cBit2_1
#define c_upen_lossta_losstafrc_sta_Shift                                                                    1

/*--------------------------------------
BitField Name: losstaoneunit
BitField Type: RW
BitField Desc: Force LOS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lossta_losstaoneunit_Mask                                                                 cBit0
#define c_upen_lossta_losstaoneunit_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Force LOF Error Configuration
Reg Addr   : 0x25040-0x2505F
Reg Formula: 0x25040 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force LOF Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lofcfg                                                                               0x25040
#define cReg_upen_lofcfg_WidthVal                                                                           64

/*--------------------------------------
BitField Name: loftim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_lofcfg_loftim_mod_Mask                                                                cBit27_26
#define c_upen_lofcfg_loftim_mod_Shift                                                                      26

/*--------------------------------------
BitField Name: loferr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_lofcfg_loferr_clr_Mask                                                                cBit25_18
#define c_upen_lofcfg_loferr_clr_Shift                                                                      18

/*--------------------------------------
BitField Name: loferr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_lofcfg_loferr_set_Mask                                                                cBit17_10
#define c_upen_lofcfg_loferr_set_Shift                                                                      10

/*--------------------------------------
BitField Name: loferr_step
BitField Type: RW
BitField Desc: The Total Step to Force LOF Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_lofcfg_loferr_step_Mask                                                                 cBit9_2
#define c_upen_lofcfg_loferr_step_Shift                                                                      2

/*--------------------------------------
BitField Name: lofmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOF Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_lofcfg_lofmsk_pos_Mask_01                                                             cBit31_18
#define c_upen_lofcfg_lofmsk_pos_Shift_01                                                                   18
#define c_upen_lofcfg_lofmsk_pos_Mask_02                                                               cBit1_0
#define c_upen_lofcfg_lofmsk_pos_Shift_02                                                                    0

/*--------------------------------------
BitField Name: lofmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOF Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_lofcfg_lofmsk_ena_Mask                                                                   cBit17
#define c_upen_lofcfg_lofmsk_ena_Shift                                                                      17

/*--------------------------------------
BitField Name: loferr_num
BitField Type: RW
BitField Desc: The Total LOF Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_lofcfg_loferr_num_Mask                                                                 cBit16_1
#define c_upen_lofcfg_loferr_num_Shift                                                                       1

/*--------------------------------------
BitField Name: loffrc_mod
BitField Type: RW
BitField Desc: Force LOF Mode 0: One shot ( N events in T the unit time) 1:
continous (loferr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lofcfg_loffrc_mod_Mask                                                                    cBit0
#define c_upen_lofcfg_loffrc_mod_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Force LOF Error Status
Reg Addr   : 0x25060-0x2507F
Reg Formula: 0x25060 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force LOF Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_lofsta                                                                               0x25060
#define cReg_upen_lofsta_WidthVal                                                                           64

/*--------------------------------------
BitField Name: lofsta_almset
BitField Type: RW
BitField Desc: LOF alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_lofsta_lofsta_almset_Mask                                                                cBit27
#define c_upen_lofsta_lofsta_almset_Shift                                                                   27

/*--------------------------------------
BitField Name: lofstaclralrm
BitField Type: RW
BitField Desc: LOF clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_lofsta_lofstaclralrm_Mask                                                             cBit22_15
#define c_upen_lofsta_lofstaclralrm_Shift                                                                   15

/*--------------------------------------
BitField Name: lofstasetalrm
BitField Type: RW
BitField Desc: LOF Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_lofsta_lofstasetalrm_Mask                                                              cBit14_7
#define c_upen_lofsta_lofstasetalrm_Shift                                                                    7

/*--------------------------------------
BitField Name: lofstatimecnt
BitField Type: RW
BitField Desc: LOF Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_lofsta_lofstatimecnt_Mask                                                               cBit6_0
#define c_upen_lofsta_lofstatimecnt_Shift                                                                    0

/*--------------------------------------
BitField Name: lofstafrc_ena
BitField Type: RW
BitField Desc: LOF Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_lofsta_lofstafrc_ena_Mask                                                                cBit31
#define c_upen_lofsta_lofstafrc_ena_Shift                                                                   31

/*--------------------------------------
BitField Name: lofstaerrstep
BitField Type: RW
BitField Desc: The Second LOF Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_lofsta_lofstaerrstep_Mask                                                             cBit30_23
#define c_upen_lofsta_lofstaerrstep_Shift                                                                   23

/*--------------------------------------
BitField Name: lofstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask LOF Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_lofsta_lofstamsk_pos_Mask                                                             cBit22_19
#define c_upen_lofsta_lofstamsk_pos_Shift                                                                   19

/*--------------------------------------
BitField Name: lofstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error LOF Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_lofsta_lofstaerr_cnt_Mask                                                              cBit18_3
#define c_upen_lofsta_lofstaerr_cnt_Shift                                                                    3

/*--------------------------------------
BitField Name: lofstafrc_sta
BitField Type: RW
BitField Desc: LOF State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_lofsta_lofstafrc_sta_Mask                                                               cBit2_1
#define c_upen_lofsta_lofstafrc_sta_Shift                                                                    1

/*--------------------------------------
BitField Name: lofstaoneunit
BitField Type: RW
BitField Desc: Force LOF in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lofsta_lofstaoneunit_Mask                                                                 cBit0
#define c_upen_lofsta_lofstaoneunit_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Force Line AIS Error Configuration
Reg Addr   : 0x21840-0x2509F
Reg Formula: 0x21840 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force Line AIS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_laiscfg                                                                              0x21840
#define cReg_upen_laiscfg_WidthVal                                                                          32

/*--------------------------------------
BitField Name: aistim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_laiscfg_aistim_mod_Mask                                                               cBit27_26
#define c_upen_laiscfg_aistim_mod_Shift                                                                     26

/*--------------------------------------
BitField Name: aiserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_laiscfg_aiserr_clr_Mask                                                               cBit25_18
#define c_upen_laiscfg_aiserr_clr_Shift                                                                     18

/*--------------------------------------
BitField Name: aiserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_laiscfg_aiserr_set_Mask                                                               cBit17_10
#define c_upen_laiscfg_aiserr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: aiserr_step
BitField Type: RW
BitField Desc: The Total Step to Force AIS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_laiscfg_aiserr_step_Mask                                                                cBit9_2
#define c_upen_laiscfg_aiserr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: aismsk_pos
BitField Type: RW
BitField Desc: The Position Mask for AIS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_laiscfg_aismsk_pos_Mask_01                                                            cBit31_18
#define c_upen_laiscfg_aismsk_pos_Shift_01                                                                  18
#define c_upen_laiscfg_aismsk_pos_Mask_02                                                              cBit1_0
#define c_upen_laiscfg_aismsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: aismsk_ena
BitField Type: RW
BitField Desc: The Data Mask For AIS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_laiscfg_aismsk_ena_Mask                                                                  cBit17
#define c_upen_laiscfg_aismsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: aiserr_num
BitField Type: RW
BitField Desc: The Total AIS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_laiscfg_aiserr_num_Mask                                                                cBit16_1
#define c_upen_laiscfg_aiserr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: aisfrc_mod
BitField Type: RW
BitField Desc: Force AIS Mode 0: One shot ( N events in T the unit time) 1:
continous (aiserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_laiscfg_aisfrc_mod_Mask                                                                   cBit0
#define c_upen_laiscfg_aisfrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force Line AIS Error Status
Reg Addr   : 0x21860-0x250BF
Reg Formula: 0x21860 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force Line AIS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_laissta                                                                              0x21860
#define cReg_upen_laissta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: lofsta_almset
BitField Type: RW
BitField Desc: LOF alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_laissta_lofsta_almset_Mask                                                               cBit27
#define c_upen_laissta_lofsta_almset_Shift                                                                  27

/*--------------------------------------
BitField Name: lofstaclralrm
BitField Type: RW
BitField Desc: LOF clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_laissta_lofstaclralrm_Mask                                                            cBit22_15
#define c_upen_laissta_lofstaclralrm_Shift                                                                  15

/*--------------------------------------
BitField Name: lofstasetalrm
BitField Type: RW
BitField Desc: LOF Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_laissta_lofstasetalrm_Mask                                                             cBit14_7
#define c_upen_laissta_lofstasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: lofstatimecnt
BitField Type: RW
BitField Desc: LOF Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_laissta_lofstatimecnt_Mask                                                              cBit6_0
#define c_upen_laissta_lofstatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: lofstafrc_ena
BitField Type: RW
BitField Desc: LOF Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_laissta_lofstafrc_ena_Mask                                                               cBit31
#define c_upen_laissta_lofstafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: lofstaerrstep
BitField Type: RW
BitField Desc: The Second LOF Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_laissta_lofstaerrstep_Mask                                                            cBit30_23
#define c_upen_laissta_lofstaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: lofstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask LOF Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_laissta_lofstamsk_pos_Mask                                                            cBit22_19
#define c_upen_laissta_lofstamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: lofstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error LOF Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_laissta_lofstaerr_cnt_Mask                                                             cBit18_3
#define c_upen_laissta_lofstaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: lofstafrc_sta
BitField Type: RW
BitField Desc: LOF State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_laissta_lofstafrc_sta_Mask                                                              cBit2_1
#define c_upen_laissta_lofstafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: lofstaoneunit
BitField Type: RW
BitField Desc: Force LOF in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_laissta_lofstaoneunit_Mask                                                                cBit0
#define c_upen_laissta_lofstaoneunit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force B1 Byte Error Configuration
Reg Addr   : 0x250c0-0x250DF
Reg Formula: 0x250c0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force B1 Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_B1bytcfg                                                                             0x250c0
#define cReg_upen_B1bytcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: b1ftim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ftim_mod_Mask                                                              cBit13_12
#define c_upen_B1bytcfg_b1ftim_mod_Shift                                                                    12

/*--------------------------------------
BitField Name: b1ferr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ferr_clr_Mask                                                                 cBit11
#define c_upen_B1bytcfg_b1ferr_clr_Shift                                                                    11

/*--------------------------------------
BitField Name: b1ferr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ferr_set_Mask                                                                 cBit10
#define c_upen_B1bytcfg_b1ferr_set_Shift                                                                    10

/*--------------------------------------
BitField Name: b1ferr_step
BitField Type: RW
BitField Desc: The Total Step to Force B1 Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ferr_step_Mask                                                               cBit9_2
#define c_upen_B1bytcfg_b1ferr_step_Shift                                                                    2

/*--------------------------------------
BitField Name: b1fmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for B1 Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_B1bytcfg_b1fmsk_pos_Mask_01                                                           cBit31_18
#define c_upen_B1bytcfg_b1fmsk_pos_Shift_01                                                                 18
#define c_upen_B1bytcfg_b1fmsk_pos_Mask_02                                                             cBit1_0
#define c_upen_B1bytcfg_b1fmsk_pos_Shift_02                                                                  0

/*--------------------------------------
BitField Name: b1fmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For  B1 Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_B1bytcfg_b1fmsk_ena_Mask                                                                 cBit17
#define c_upen_B1bytcfg_b1fmsk_ena_Shift                                                                    17

/*--------------------------------------
BitField Name: b1ferr_num
BitField Type: RW
BitField Desc: The Total B1 Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ferr_num_Mask                                                               cBit16_1
#define c_upen_B1bytcfg_b1ferr_num_Shift                                                                     1

/*--------------------------------------
BitField Name: b1ffrc_mod
BitField Type: RW
BitField Desc: Force B1 Mode 0: One shot 1: continous (b1ferr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_B1bytcfg_b1ffrc_mod_Mask                                                                  cBit0
#define c_upen_B1bytcfg_b1ffrc_mod_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Force B1 Byte Error Status
Reg Addr   : 0x250e0-0x250FF
Reg Formula: 0x250e0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force B1 Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_B1bytsta                                                                             0x250e0
#define cReg_upen_B1bytsta_WidthVal                                                                         64

/*--------------------------------------
BitField Name: b1fsta_almset
BitField Type: RW
BitField Desc: B1F alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_B1bytsta_b1fsta_almset_Mask                                                               cBit9
#define c_upen_B1bytsta_b1fsta_almset_Shift                                                                  9

/*--------------------------------------
BitField Name: b1fstaclralrm
BitField Type: RW
BitField Desc: B1F clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_B1bytsta_b1fstaclralrm_Mask                                                               cBit8
#define c_upen_B1bytsta_b1fstaclralrm_Shift                                                                  8

/*--------------------------------------
BitField Name: b1fstasetalrm
BitField Type: RW
BitField Desc: B1F Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_B1bytsta_b1fstasetalrm_Mask                                                               cBit7
#define c_upen_B1bytsta_b1fstasetalrm_Shift                                                                  7

/*--------------------------------------
BitField Name: b1fstatimecnt
BitField Type: RW
BitField Desc: B1F Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_B1bytsta_b1fstatimecnt_Mask                                                             cBit6_0
#define c_upen_B1bytsta_b1fstatimecnt_Shift                                                                  0

/*--------------------------------------
BitField Name: b1fstafrc_ena
BitField Type: RW
BitField Desc: B1F Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_B1bytsta_b1fstafrc_ena_Mask                                                              cBit31
#define c_upen_B1bytsta_b1fstafrc_ena_Shift                                                                 31

/*--------------------------------------
BitField Name: b1fstaerrstep
BitField Type: RW
BitField Desc: The Second B1F Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_B1bytsta_b1fstaerrstep_Mask                                                           cBit30_23
#define c_upen_B1bytsta_b1fstaerrstep_Shift                                                                 23

/*--------------------------------------
BitField Name: b1fstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask B1F Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_B1bytsta_b1fstamsk_pos_Mask                                                           cBit22_19
#define c_upen_B1bytsta_b1fstamsk_pos_Shift                                                                 19

/*--------------------------------------
BitField Name: b1fstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error B1F Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_B1bytsta_b1fstaerr_cnt_Mask                                                            cBit18_3
#define c_upen_B1bytsta_b1fstaerr_cnt_Shift                                                                  3

/*--------------------------------------
BitField Name: b1fstafrc_sta
BitField Type: RW
BitField Desc: B1F State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_B1bytsta_b1fstafrc_sta_Mask                                                             cBit2_1
#define c_upen_B1bytsta_b1fstafrc_sta_Shift                                                                  1

/*--------------------------------------
BitField Name: b1fstaoneunit
BitField Type: RW
BitField Desc: Force B1F in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_B1bytsta_b1fstaoneunit_Mask                                                               cBit0
#define c_upen_B1bytsta_b1fstaoneunit_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force Line RDI Error Configuration
Reg Addr   : 0x21800-0x2181F
Reg Formula: 0x21800 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force Line RDI Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lrdicfg                                                                              0x21800
#define cReg_upen_lrdicfg_WidthVal                                                                          32

/*--------------------------------------
BitField Name: rditim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_lrdicfg_rditim_mod_Mask                                                               cBit27_26
#define c_upen_lrdicfg_rditim_mod_Shift                                                                     26

/*--------------------------------------
BitField Name: rdierr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_lrdicfg_rdierr_clr_Mask                                                               cBit25_18
#define c_upen_lrdicfg_rdierr_clr_Shift                                                                     18

/*--------------------------------------
BitField Name: rdierr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_lrdicfg_rdierr_set_Mask                                                               cBit17_10
#define c_upen_lrdicfg_rdierr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: rdierr_step
BitField Type: RW
BitField Desc: The Total Step to Force RDI Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_lrdicfg_rdierr_step_Mask                                                                cBit9_2
#define c_upen_lrdicfg_rdierr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: rdimsk_pos
BitField Type: RW
BitField Desc: The Position Mask for RDI Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_lrdicfg_rdimsk_pos_Mask_01                                                            cBit31_18
#define c_upen_lrdicfg_rdimsk_pos_Shift_01                                                                  18
#define c_upen_lrdicfg_rdimsk_pos_Mask_02                                                              cBit1_0
#define c_upen_lrdicfg_rdimsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: rdimsk_ena
BitField Type: RW
BitField Desc: The Data Mask For RDI Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_lrdicfg_rdimsk_ena_Mask                                                                  cBit17
#define c_upen_lrdicfg_rdimsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: rdierr_num
BitField Type: RW
BitField Desc: The Total RDI Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_lrdicfg_rdierr_num_Mask                                                                cBit16_1
#define c_upen_lrdicfg_rdierr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: rdifrc_mod
BitField Type: RW
BitField Desc: Force RDI Mode 0: One shot ( N events in T the unit time) 1:
continous (rdierr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lrdicfg_rdifrc_mod_Mask                                                                   cBit0
#define c_upen_lrdicfg_rdifrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force Line RDI Error Status
Reg Addr   : 0x21820-0x2183F
Reg Formula: 0x21820 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force Line RDI Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_lrdista                                                                              0x21820
#define cReg_upen_lrdista_WidthVal                                                                          64

/*--------------------------------------
BitField Name: rdista_almset
BitField Type: RW
BitField Desc: RDI alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_lrdista_rdista_almset_Mask                                                               cBit27
#define c_upen_lrdista_rdista_almset_Shift                                                                  27

/*--------------------------------------
BitField Name: rdistaclralrm
BitField Type: RW
BitField Desc: RDI clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_lrdista_rdistaclralrm_Mask                                                            cBit22_15
#define c_upen_lrdista_rdistaclralrm_Shift                                                                  15

/*--------------------------------------
BitField Name: rdistasetalrm
BitField Type: RW
BitField Desc: RDI Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_lrdista_rdistasetalrm_Mask                                                             cBit14_7
#define c_upen_lrdista_rdistasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: rdistatimecnt
BitField Type: RW
BitField Desc: RDI Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_lrdista_rdistatimecnt_Mask                                                              cBit6_0
#define c_upen_lrdista_rdistatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: rdistafrc_ena
BitField Type: RW
BitField Desc: RDI Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_lrdista_rdistafrc_ena_Mask                                                               cBit31
#define c_upen_lrdista_rdistafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: rdistaerrstep
BitField Type: RW
BitField Desc: The Second RDI Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_lrdista_rdistaerrstep_Mask                                                            cBit30_23
#define c_upen_lrdista_rdistaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: rdistamsk_pos
BitField Type: RW
BitField Desc: The Position Mask RDI Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_lrdista_rdistamsk_pos_Mask                                                            cBit22_19
#define c_upen_lrdista_rdistamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: rdistaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error RDI Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_lrdista_rdistaerr_cnt_Mask                                                             cBit18_3
#define c_upen_lrdista_rdistaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: rdistafrc_sta
BitField Type: RW
BitField Desc: RDI State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_lrdista_rdistafrc_sta_Mask                                                              cBit2_1
#define c_upen_lrdista_rdistafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: rdistaoneunit
BitField Type: RW
BitField Desc: Force RDI in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lrdista_rdistaoneunit_Mask                                                                cBit0
#define c_upen_lrdista_rdistaoneunit_Shift                                                                   0

/*--------------------------------------
BitField Name: aistim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_lrdista_aistim_mod_Mask                                                               cBit27_26
#define c_upen_lrdista_aistim_mod_Shift                                                                     26

/*--------------------------------------
BitField Name: aiserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_lrdista_aiserr_clr_Mask                                                               cBit25_18
#define c_upen_lrdista_aiserr_clr_Shift                                                                     18

/*--------------------------------------
BitField Name: aiserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_lrdista_aiserr_set_Mask                                                               cBit17_10
#define c_upen_lrdista_aiserr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: aiserr_step
BitField Type: RW
BitField Desc: The Total Step to Force AIS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_lrdista_aiserr_step_Mask                                                                cBit9_2
#define c_upen_lrdista_aiserr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: aismsk_pos
BitField Type: RW
BitField Desc: The Position Mask for AIS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_lrdista_aismsk_pos_Mask_01                                                            cBit31_18
#define c_upen_lrdista_aismsk_pos_Shift_01                                                                  18
#define c_upen_lrdista_aismsk_pos_Mask_02                                                              cBit1_0
#define c_upen_lrdista_aismsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: aismsk_ena
BitField Type: RW
BitField Desc: The Data Mask For AIS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_lrdista_aismsk_ena_Mask                                                                  cBit17
#define c_upen_lrdista_aismsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: aiserr_num
BitField Type: RW
BitField Desc: The Total AIS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_lrdista_aiserr_num_Mask                                                                cBit16_1
#define c_upen_lrdista_aiserr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: rdista_almset
BitField Type: RW
BitField Desc: RDI alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_lrdista_rdista_almset_Mask                                                               cBit27
#define c_upen_lrdista_rdista_almset_Shift                                                                  27

/*--------------------------------------
BitField Name: rdistaclralrm
BitField Type: RW
BitField Desc: RDI clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_lrdista_rdistaclralrm_Mask                                                            cBit22_15
#define c_upen_lrdista_rdistaclralrm_Shift                                                                  15

/*--------------------------------------
BitField Name: rdistasetalrm
BitField Type: RW
BitField Desc: RDI Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_lrdista_rdistasetalrm_Mask                                                             cBit14_7
#define c_upen_lrdista_rdistasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: rdistatimecnt
BitField Type: RW
BitField Desc: RDI Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_lrdista_rdistatimecnt_Mask                                                              cBit6_0
#define c_upen_lrdista_rdistatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: rdistafrc_ena
BitField Type: RW
BitField Desc: RDI Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_lrdista_rdistafrc_ena_Mask                                                               cBit31
#define c_upen_lrdista_rdistafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: rdistaerrstep
BitField Type: RW
BitField Desc: The Second RDI Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_lrdista_rdistaerrstep_Mask                                                            cBit30_23
#define c_upen_lrdista_rdistaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: rdistamsk_pos
BitField Type: RW
BitField Desc: The Position Mask RDI Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_lrdista_rdistamsk_pos_Mask                                                            cBit22_19
#define c_upen_lrdista_rdistamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: rdistaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error RDI Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_lrdista_rdistaerr_cnt_Mask                                                             cBit18_3
#define c_upen_lrdista_rdistaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: rdistafrc_sta
BitField Type: RW
BitField Desc: RDI State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_lrdista_rdistafrc_sta_Mask                                                              cBit2_1
#define c_upen_lrdista_rdistafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: rdistaoneunit
BitField Type: RW
BitField Desc: Force RDI in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lrdista_rdistaoneunit_Mask                                                                cBit0
#define c_upen_lrdista_rdistaoneunit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force Path REI Error Configuration
Reg Addr   : 0x21880-0x2189F
Reg Formula: 0x21880 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force Path REI Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_preicfg                                                                              0x21880
#define cReg_upen_preicfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: reitim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_preicfg_reitim_mod_Mask                                                               cBit13_12
#define c_upen_preicfg_reitim_mod_Shift                                                                     12

/*--------------------------------------
BitField Name: reierr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_preicfg_reierr_clr_Mask                                                                  cBit11
#define c_upen_preicfg_reierr_clr_Shift                                                                     11

/*--------------------------------------
BitField Name: reierr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_preicfg_reierr_set_Mask                                                                  cBit10
#define c_upen_preicfg_reierr_set_Shift                                                                     10

/*--------------------------------------
BitField Name: reierr_step
BitField Type: RW
BitField Desc: The Total Step to Force REI Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_preicfg_reierr_step_Mask                                                                cBit9_2
#define c_upen_preicfg_reierr_step_Shift                                                                     2

/*--------------------------------------
BitField Name: reimsk_pos
BitField Type: RW
BitField Desc: The Position Mask for REI Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_preicfg_reimsk_pos_Mask_01                                                            cBit31_18
#define c_upen_preicfg_reimsk_pos_Shift_01                                                                  18
#define c_upen_preicfg_reimsk_pos_Mask_02                                                              cBit1_0
#define c_upen_preicfg_reimsk_pos_Shift_02                                                                   0

/*--------------------------------------
BitField Name: reimsk_ena
BitField Type: RW
BitField Desc: The Data Mask For REI Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_preicfg_reimsk_ena_Mask                                                                  cBit17
#define c_upen_preicfg_reimsk_ena_Shift                                                                     17

/*--------------------------------------
BitField Name: reierr_num
BitField Type: RW
BitField Desc: The Total REI Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_preicfg_reierr_num_Mask                                                                cBit16_1
#define c_upen_preicfg_reierr_num_Shift                                                                      1

/*--------------------------------------
BitField Name: reifrc_mod
BitField Type: RW
BitField Desc: Force REI Mode 0: One shot 1: continous (reierr_num = 16 bit
FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_preicfg_reifrc_mod_Mask                                                                   cBit0
#define c_upen_preicfg_reifrc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force Path REI Error Status
Reg Addr   : 0x218a0-0x218bF
Reg Formula: 0x218a0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force Path REI Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_preista                                                                              0x218a0
#define cReg_upen_preista_WidthVal                                                                          64

/*--------------------------------------
BitField Name: reista_almset
BitField Type: RW
BitField Desc: REI alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_preista_reista_almset_Mask                                                                cBit9
#define c_upen_preista_reista_almset_Shift                                                                   9

/*--------------------------------------
BitField Name: reistaclralrm
BitField Type: RW
BitField Desc: REI clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_preista_reistaclralrm_Mask                                                                cBit8
#define c_upen_preista_reistaclralrm_Shift                                                                   8

/*--------------------------------------
BitField Name: reistasetalrm
BitField Type: RW
BitField Desc: REI Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_preista_reistasetalrm_Mask                                                                cBit7
#define c_upen_preista_reistasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: reistatimecnt
BitField Type: RW
BitField Desc: REI Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_preista_reistatimecnt_Mask                                                              cBit6_0
#define c_upen_preista_reistatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: reistafrc_ena
BitField Type: RW
BitField Desc: REI Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_preista_reistafrc_ena_Mask                                                               cBit31
#define c_upen_preista_reistafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: reistaerrstep
BitField Type: RW
BitField Desc: The Second REI Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_preista_reistaerrstep_Mask                                                            cBit30_23
#define c_upen_preista_reistaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: reistamsk_pos
BitField Type: RW
BitField Desc: The Position Mask REI Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_preista_reistamsk_pos_Mask                                                            cBit22_19
#define c_upen_preista_reistamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: reistaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error REI Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_preista_reistaerr_cnt_Mask                                                             cBit18_3
#define c_upen_preista_reistaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: reistafrc_sta
BitField Type: RW
BitField Desc: REI State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_preista_reistafrc_sta_Mask                                                              cBit2_1
#define c_upen_preista_reistafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: rdista_almset
BitField Type: RW
BitField Desc: RDI alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_preista_rdista_almset_Mask                                                                cBit9
#define c_upen_preista_rdista_almset_Shift                                                                   9

/*--------------------------------------
BitField Name: rdistaclralrm
BitField Type: RW
BitField Desc: RDI clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_preista_rdistaclralrm_Mask                                                                cBit8
#define c_upen_preista_rdistaclralrm_Shift                                                                   8

/*--------------------------------------
BitField Name: rdistasetalrm
BitField Type: RW
BitField Desc: RDI Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_preista_rdistasetalrm_Mask                                                                cBit7
#define c_upen_preista_rdistasetalrm_Shift                                                                   7

/*--------------------------------------
BitField Name: rdistatimecnt
BitField Type: RW
BitField Desc: RDI Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_preista_rdistatimecnt_Mask                                                              cBit6_0
#define c_upen_preista_rdistatimecnt_Shift                                                                   0

/*--------------------------------------
BitField Name: rdistafrc_ena
BitField Type: RW
BitField Desc: RDI Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_preista_rdistafrc_ena_Mask                                                               cBit31
#define c_upen_preista_rdistafrc_ena_Shift                                                                  31

/*--------------------------------------
BitField Name: rdistaerrstep
BitField Type: RW
BitField Desc: The Second RDI Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_preista_rdistaerrstep_Mask                                                            cBit30_23
#define c_upen_preista_rdistaerrstep_Shift                                                                  23

/*--------------------------------------
BitField Name: rdistamsk_pos
BitField Type: RW
BitField Desc: The Position Mask RDI Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_preista_rdistamsk_pos_Mask                                                            cBit22_19
#define c_upen_preista_rdistamsk_pos_Shift                                                                  19

/*--------------------------------------
BitField Name: rdistaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error RDI Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_preista_rdistaerr_cnt_Mask                                                             cBit18_3
#define c_upen_preista_rdistaerr_cnt_Shift                                                                   3

/*--------------------------------------
BitField Name: rdistafrc_sta
BitField Type: RW
BitField Desc: RDI State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_preista_rdistafrc_sta_Mask                                                              cBit2_1
#define c_upen_preista_rdistafrc_sta_Shift                                                                   1

/*--------------------------------------
BitField Name: rdistaoneunit
BitField Type: RW
BitField Desc: Force RDI in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_preista_rdistaoneunit_Mask                                                                cBit0
#define c_upen_preista_rdistaoneunit_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP-8 Error Configuration
Reg Addr   : 0x21900-0x2191f
Reg Formula: 0x21900 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force BIP-8 Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_bip8cfg                                                                              0x21900
#define cReg_upen_bip8cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: bip8tim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_bip8cfg_bip8tim_mod_Mask                                                              cBit13_12
#define c_upen_bip8cfg_bip8tim_mod_Shift                                                                    12

/*--------------------------------------
BitField Name: bip8err_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_bip8cfg_bip8err_clr_Mask                                                                 cBit11
#define c_upen_bip8cfg_bip8err_clr_Shift                                                                    11

/*--------------------------------------
BitField Name: bip8err_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_bip8cfg_bip8err_set_Mask                                                                 cBit10
#define c_upen_bip8cfg_bip8err_set_Shift                                                                    10

/*--------------------------------------
BitField Name: cfgbip8err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force BIP-8 Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_bip8cfg_cfgbip8err_step_Mask                                                            cBit9_2
#define c_upen_bip8cfg_cfgbip8err_step_Shift                                                                 2

/*--------------------------------------
BitField Name: cfgbip8msk_pos
BitField Type: RW
BitField Desc: The Position Mask for  BIP-8 Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_bip8cfg_cfgbip8msk_pos_Mask_01                                                        cBit31_18
#define c_upen_bip8cfg_cfgbip8msk_pos_Shift_01                                                              18
#define c_upen_bip8cfg_cfgbip8msk_pos_Mask_02                                                          cBit1_0
#define c_upen_bip8cfg_cfgbip8msk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfgbip8msk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  BIP-8 Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_bip8cfg_cfgbip8msk_dat_Mask                                                              cBit17
#define c_upen_bip8cfg_cfgbip8msk_dat_Shift                                                                 17

/*--------------------------------------
BitField Name: cfgbip8err_num
BitField Type: RW
BitField Desc: The Total BIP-8 Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_bip8cfg_cfgbip8err_num_Mask                                                            cBit16_1
#define c_upen_bip8cfg_cfgbip8err_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgbip8frc_1sen
BitField Type: RW
BitField Desc: Force BIP-8 in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_bip8cfg_cfgbip8frc_1sen_Mask                                                              cBit0
#define c_upen_bip8cfg_cfgbip8frc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP-8 Error Status
Reg Addr   : 0x21920-0x2193f
Reg Formula: 0x21920 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force BIP-8 Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_bip8sta                                                                              0x21920
#define cReg_upen_bip8sta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: bip8sta_almset
BitField Type: RW
BitField Desc: BIP8 alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_bip8sta_bip8sta_almset_Mask                                                               cBit9
#define c_upen_bip8sta_bip8sta_almset_Shift                                                                  9

/*--------------------------------------
BitField Name: bip8staclralrm
BitField Type: RW
BitField Desc: BIP8 clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_bip8sta_bip8staclralrm_Mask                                                               cBit8
#define c_upen_bip8sta_bip8staclralrm_Shift                                                                  8

/*--------------------------------------
BitField Name: bip8stasetalrm
BitField Type: RW
BitField Desc: BIP8 Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_bip8sta_bip8stasetalrm_Mask                                                               cBit7
#define c_upen_bip8sta_bip8stasetalrm_Shift                                                                  7

/*--------------------------------------
BitField Name: bip8statimecnt
BitField Type: RW
BitField Desc: BIP8 Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_bip8sta_bip8statimecnt_Mask                                                             cBit6_0
#define c_upen_bip8sta_bip8statimecnt_Shift                                                                  0

/*--------------------------------------
BitField Name: bip8stafrc_ena
BitField Type: RW
BitField Desc: BIP8 Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_bip8sta_bip8stafrc_ena_Mask                                                              cBit31
#define c_upen_bip8sta_bip8stafrc_ena_Shift                                                                 31

/*--------------------------------------
BitField Name: bip8staerrstep
BitField Type: RW
BitField Desc: The Second BIP8 Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_bip8sta_bip8staerrstep_Mask                                                           cBit30_23
#define c_upen_bip8sta_bip8staerrstep_Shift                                                                 23

/*--------------------------------------
BitField Name: bip8stamsk_pos
BitField Type: RW
BitField Desc: The Position Mask BIP8 Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_bip8sta_bip8stamsk_pos_Mask                                                           cBit22_19
#define c_upen_bip8sta_bip8stamsk_pos_Shift                                                                 19

/*--------------------------------------
BitField Name: bip8staerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error BIP8 Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_bip8sta_bip8staerr_cnt_Mask                                                            cBit18_3
#define c_upen_bip8sta_bip8staerr_cnt_Shift                                                                  3

/*--------------------------------------
BitField Name: bip8stafrc_sta
BitField Type: RW
BitField Desc: BIP8 State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_bip8sta_bip8stafrc_sta_Mask                                                             cBit2_1
#define c_upen_bip8sta_bip8stafrc_sta_Shift                                                                  1

/*--------------------------------------
BitField Name: bip8staoneunit
BitField Type: RW
BitField Desc: Force BIP8 in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_bip8sta_bip8staoneunit_Mask                                                               cBit0
#define c_upen_bip8sta_bip8staoneunit_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force K Byte Error Configuration
Reg Addr   : 0x21940-0x2195f
Reg Formula: 0x21940 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force K Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_Kbytcfg                                                                              0x21940
#define cReg_upen_Kbytcfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: Kbyttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_Kbytcfg_Kbyttim_mod_Mask                                                              cBit13_12
#define c_upen_Kbytcfg_Kbyttim_mod_Shift                                                                    12

/*--------------------------------------
BitField Name: Kbyterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_Kbytcfg_Kbyterr_clr_Mask                                                                 cBit11
#define c_upen_Kbytcfg_Kbyterr_clr_Shift                                                                    11

/*--------------------------------------
BitField Name: Kbyterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_Kbytcfg_Kbyterr_set_Mask                                                                 cBit10
#define c_upen_Kbytcfg_Kbyterr_set_Shift                                                                    10

/*--------------------------------------
BitField Name: cfgKbyterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force K Byte Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_Kbytcfg_cfgKbyterr_step_Mask                                                            cBit9_2
#define c_upen_Kbytcfg_cfgKbyterr_step_Shift                                                                 2

/*--------------------------------------
BitField Name: cfgKbytmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  K Byte Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_Kbytcfg_cfgKbytmsk_pos_Mask_01                                                        cBit31_18
#define c_upen_Kbytcfg_cfgKbytmsk_pos_Shift_01                                                              18
#define c_upen_Kbytcfg_cfgKbytmsk_pos_Mask_02                                                          cBit1_0
#define c_upen_Kbytcfg_cfgKbytmsk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfgKbytmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  K Byte Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_Kbytcfg_cfgKbytmsk_dat_Mask                                                              cBit17
#define c_upen_Kbytcfg_cfgKbytmsk_dat_Shift                                                                 17

/*--------------------------------------
BitField Name: cfgKbyterr_num
BitField Type: RW
BitField Desc: The Total K Byte Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_Kbytcfg_cfgKbyterr_num_Mask                                                            cBit16_1
#define c_upen_Kbytcfg_cfgKbyterr_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgKbytfrc_1sen
BitField Type: RW
BitField Desc: Force K Byte in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_Kbytcfg_cfgKbytfrc_1sen_Mask                                                              cBit0
#define c_upen_Kbytcfg_cfgKbytfrc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force K Byte Error Status
Reg Addr   : 0x21960-0x2197F
Reg Formula: 0x21960 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force K Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_Kbytsta                                                                              0x21960
#define cReg_upen_Kbytsta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: Kbytsta_almset
BitField Type: RW
BitField Desc: K byte alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_Kbytsta_Kbytsta_almset_Mask                                                               cBit9
#define c_upen_Kbytsta_Kbytsta_almset_Shift                                                                  9

/*--------------------------------------
BitField Name: Kbytstaclralrm
BitField Type: RW
BitField Desc: K byte clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_Kbytsta_Kbytstaclralrm_Mask                                                               cBit8
#define c_upen_Kbytsta_Kbytstaclralrm_Shift                                                                  8

/*--------------------------------------
BitField Name: Kbytstasetalrm
BitField Type: RW
BitField Desc: K byte Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_Kbytsta_Kbytstasetalrm_Mask                                                               cBit7
#define c_upen_Kbytsta_Kbytstasetalrm_Shift                                                                  7

/*--------------------------------------
BitField Name: Kbytstatimecnt
BitField Type: RW
BitField Desc: K byte Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_Kbytsta_Kbytstatimecnt_Mask                                                             cBit6_0
#define c_upen_Kbytsta_Kbytstatimecnt_Shift                                                                  0

/*--------------------------------------
BitField Name: Kbytstafrc_ena
BitField Type: RW
BitField Desc: K byte Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_Kbytsta_Kbytstafrc_ena_Mask                                                              cBit31
#define c_upen_Kbytsta_Kbytstafrc_ena_Shift                                                                 31

/*--------------------------------------
BitField Name: Kbytstaerrstep
BitField Type: RW
BitField Desc: The Second K byte Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_Kbytsta_Kbytstaerrstep_Mask                                                           cBit30_23
#define c_upen_Kbytsta_Kbytstaerrstep_Shift                                                                 23

/*--------------------------------------
BitField Name: Kbytstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask K byte Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_Kbytsta_Kbytstamsk_pos_Mask                                                           cBit22_19
#define c_upen_Kbytsta_Kbytstamsk_pos_Shift                                                                 19

/*--------------------------------------
BitField Name: Kbytstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error K byte Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_Kbytsta_Kbytstaerr_cnt_Mask                                                            cBit18_3
#define c_upen_Kbytsta_Kbytstaerr_cnt_Shift                                                                  3

/*--------------------------------------
BitField Name: Kbytstafrc_sta
BitField Type: RW
BitField Desc: K byte State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_Kbytsta_Kbytstafrc_sta_Mask                                                             cBit2_1
#define c_upen_Kbytsta_Kbytstafrc_sta_Shift                                                                  1

/*--------------------------------------
BitField Name: Kbytstaoneunit
BitField Type: RW
BitField Desc: Force K byte in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_Kbytsta_Kbytstaoneunit_Mask                                                               cBit0
#define c_upen_Kbytsta_Kbytstaoneunit_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force S Byte Error Configuration
Reg Addr   : 0x21980-0x2199f
Reg Formula: 0x21980 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force S Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_Sbytcfg                                                                              0x21980
#define cReg_upen_Sbytcfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: Sbyttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_Sbytcfg_Sbyttim_mod_Mask                                                              cBit13_12
#define c_upen_Sbytcfg_Sbyttim_mod_Shift                                                                    12

/*--------------------------------------
BitField Name: Sbyterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_Sbytcfg_Sbyterr_clr_Mask                                                                 cBit11
#define c_upen_Sbytcfg_Sbyterr_clr_Shift                                                                    11

/*--------------------------------------
BitField Name: Sbyterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_Sbytcfg_Sbyterr_set_Mask                                                                 cBit10
#define c_upen_Sbytcfg_Sbyterr_set_Shift                                                                    10

/*--------------------------------------
BitField Name: cfgSbyterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force S Byte Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_Sbytcfg_cfgSbyterr_step_Mask                                                            cBit9_2
#define c_upen_Sbytcfg_cfgSbyterr_step_Shift                                                                 2

/*--------------------------------------
BitField Name: cfgSbytmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  S Byte Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_Sbytcfg_cfgSbytmsk_pos_Mask_01                                                        cBit31_18
#define c_upen_Sbytcfg_cfgSbytmsk_pos_Shift_01                                                              18
#define c_upen_Sbytcfg_cfgSbytmsk_pos_Mask_02                                                          cBit1_0
#define c_upen_Sbytcfg_cfgSbytmsk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfgSbytmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  S Byte Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_Sbytcfg_cfgSbytmsk_dat_Mask                                                              cBit17
#define c_upen_Sbytcfg_cfgSbytmsk_dat_Shift                                                                 17

/*--------------------------------------
BitField Name: cfgSbyterr_num
BitField Type: RW
BitField Desc: The Total S Byte Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_Sbytcfg_cfgSbyterr_num_Mask                                                            cBit16_1
#define c_upen_Sbytcfg_cfgSbyterr_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgSbytfrc_1sen
BitField Type: RW
BitField Desc: Force S Byte in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_Sbytcfg_cfgSbytfrc_1sen_Mask                                                              cBit0
#define c_upen_Sbytcfg_cfgSbytfrc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force S Byte Error Status
Reg Addr   : 0x219a0-0x219bf
Reg Formula: 0x219a0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force S Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_Sbytsta                                                                              0x219a0
#define cReg_upen_Sbytsta_WidthVal                                                                          64

/*--------------------------------------
BitField Name: Sbytsta_almset
BitField Type: RW
BitField Desc: S byte alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_Sbytsta_Sbytsta_almset_Mask                                                               cBit9
#define c_upen_Sbytsta_Sbytsta_almset_Shift                                                                  9

/*--------------------------------------
BitField Name: Sbytstaclralrm
BitField Type: RW
BitField Desc: S byte clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_Sbytsta_Sbytstaclralrm_Mask                                                               cBit8
#define c_upen_Sbytsta_Sbytstaclralrm_Shift                                                                  8

/*--------------------------------------
BitField Name: Sbytstasetalrm
BitField Type: RW
BitField Desc: S byte Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_Sbytsta_Sbytstasetalrm_Mask                                                               cBit7
#define c_upen_Sbytsta_Sbytstasetalrm_Shift                                                                  7

/*--------------------------------------
BitField Name: Sbytstatimecnt
BitField Type: RW
BitField Desc: S byte Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_Sbytsta_Sbytstatimecnt_Mask                                                             cBit6_0
#define c_upen_Sbytsta_Sbytstatimecnt_Shift                                                                  0

/*--------------------------------------
BitField Name: Sbytstafrc_ena
BitField Type: RW
BitField Desc: S byte Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_Sbytsta_Sbytstafrc_ena_Mask                                                              cBit31
#define c_upen_Sbytsta_Sbytstafrc_ena_Shift                                                                 31

/*--------------------------------------
BitField Name: Sbytstaerrstep
BitField Type: RW
BitField Desc: The Second S byte Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_Sbytsta_Sbytstaerrstep_Mask                                                           cBit30_23
#define c_upen_Sbytsta_Sbytstaerrstep_Shift                                                                 23

/*--------------------------------------
BitField Name: Sbytstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask S byte Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_Sbytsta_Sbytstamsk_pos_Mask                                                           cBit22_19
#define c_upen_Sbytsta_Sbytstamsk_pos_Shift                                                                 19

/*--------------------------------------
BitField Name: Sbytstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error S byte Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_Sbytsta_Sbytstaerr_cnt_Mask                                                            cBit18_3
#define c_upen_Sbytsta_Sbytstaerr_cnt_Shift                                                                  3

/*--------------------------------------
BitField Name: Sbytstafrc_sta
BitField Type: RW
BitField Desc: S byte State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_Sbytsta_Sbytstafrc_sta_Mask                                                             cBit2_1
#define c_upen_Sbytsta_Sbytstafrc_sta_Shift                                                                  1

/*--------------------------------------
BitField Name: Sbytstaoneunit
BitField Type: RW
BitField Desc: Force S byte in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_Sbytsta_Sbytstaoneunit_Mask                                                               cBit0
#define c_upen_Sbytsta_Sbytstaoneunit_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force B2 Byte Error Configuration
Reg Addr   : 0x219c0-0x219df
Reg Formula: 0x219c0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force B2 Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_B2bytcfg                                                                             0x219c0
#define cReg_upen_B2bytcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: b2ftim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_B2bytcfg_b2ftim_mod_Mask                                                              cBit13_12
#define c_upen_B2bytcfg_b2ftim_mod_Shift                                                                    12

/*--------------------------------------
BitField Name: b2ferr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_B2bytcfg_b2ferr_clr_Mask                                                                 cBit11
#define c_upen_B2bytcfg_b2ferr_clr_Shift                                                                    11

/*--------------------------------------
BitField Name: b2ferr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_B2bytcfg_b2ferr_set_Mask                                                                 cBit10
#define c_upen_B2bytcfg_b2ferr_set_Shift                                                                    10

/*--------------------------------------
BitField Name: cfgB2byterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force B2 Byte Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_B2bytcfg_cfgB2byterr_step_Mask                                                          cBit9_2
#define c_upen_B2bytcfg_cfgB2byterr_step_Shift                                                               2

/*--------------------------------------
BitField Name: cfgB2bytmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  B2 Byte Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_B2bytcfg_cfgB2bytmsk_pos_Mask_01                                                      cBit31_18
#define c_upen_B2bytcfg_cfgB2bytmsk_pos_Shift_01                                                            18
#define c_upen_B2bytcfg_cfgB2bytmsk_pos_Mask_02                                                        cBit1_0
#define c_upen_B2bytcfg_cfgB2bytmsk_pos_Shift_02                                                             0

/*--------------------------------------
BitField Name: cfgB2bytmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  B2 Byte Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_B2bytcfg_cfgB2bytmsk_dat_Mask                                                            cBit17
#define c_upen_B2bytcfg_cfgB2bytmsk_dat_Shift                                                               17

/*--------------------------------------
BitField Name: cfgB2byterr_num
BitField Type: RW
BitField Desc: The Total B2 Byte Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_B2bytcfg_cfgB2byterr_num_Mask                                                          cBit16_1
#define c_upen_B2bytcfg_cfgB2byterr_num_Shift                                                                1

/*--------------------------------------
BitField Name: cfgB2bytfrc_1sen
BitField Type: RW
BitField Desc: Force B2 Byte in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_B2bytcfg_cfgB2bytfrc_1sen_Mask                                                            cBit0
#define c_upen_B2bytcfg_cfgB2bytfrc_1sen_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force B2 Byte Error Status
Reg Addr   : 0x219e0-0x219ff
Reg Formula: 0x219e0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force B2 Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_B2bytsta                                                                             0x219e0
#define cReg_upen_B2bytsta_WidthVal                                                                         64

/*--------------------------------------
BitField Name: b2fsta_almset
BitField Type: RW
BitField Desc: B2F alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_B2bytsta_b2fsta_almset_Mask                                                               cBit9
#define c_upen_B2bytsta_b2fsta_almset_Shift                                                                  9

/*--------------------------------------
BitField Name: b2fstaclralrm
BitField Type: RW
BitField Desc: B2F clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_B2bytsta_b2fstaclralrm_Mask                                                               cBit8
#define c_upen_B2bytsta_b2fstaclralrm_Shift                                                                  8

/*--------------------------------------
BitField Name: b2fstasetalrm
BitField Type: RW
BitField Desc: B2F Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_B2bytsta_b2fstasetalrm_Mask                                                               cBit7
#define c_upen_B2bytsta_b2fstasetalrm_Shift                                                                  7

/*--------------------------------------
BitField Name: b2fstatimecnt
BitField Type: RW
BitField Desc: B2F Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_B2bytsta_b2fstatimecnt_Mask                                                             cBit6_0
#define c_upen_B2bytsta_b2fstatimecnt_Shift                                                                  0

/*--------------------------------------
BitField Name: b2fstafrc_ena
BitField Type: RW
BitField Desc: B2F Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_B2bytsta_b2fstafrc_ena_Mask                                                              cBit31
#define c_upen_B2bytsta_b2fstafrc_ena_Shift                                                                 31

/*--------------------------------------
BitField Name: b2fstaerrstep
BitField Type: RW
BitField Desc: The Second B2F Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_B2bytsta_b2fstaerrstep_Mask                                                           cBit30_23
#define c_upen_B2bytsta_b2fstaerrstep_Shift                                                                 23

/*--------------------------------------
BitField Name: b2fstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask B2F Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_B2bytsta_b2fstamsk_pos_Mask                                                           cBit22_19
#define c_upen_B2bytsta_b2fstamsk_pos_Shift                                                                 19

/*--------------------------------------
BitField Name: b2fstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error B2F Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_B2bytsta_b2fstaerr_cnt_Mask                                                            cBit18_3
#define c_upen_B2bytsta_b2fstaerr_cnt_Shift                                                                  3

/*--------------------------------------
BitField Name: b2fstafrc_sta
BitField Type: RW
BitField Desc: B2F State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_B2bytsta_b2fstafrc_sta_Mask                                                             cBit2_1
#define c_upen_B2bytsta_b2fstafrc_sta_Shift                                                                  1

/*--------------------------------------
BitField Name: b2fstaoneunit
BitField Type: RW
BitField Desc: Force B2F in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_B2bytsta_b2fstaoneunit_Mask                                                               cBit0
#define c_upen_B2bytsta_b2fstaoneunit_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer VT Error Configuration
Reg Addr   : 0x64000-0x643FF
Reg Formula: 0x64000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force Loss Of Pointer VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_LOPvtcfg                                                                             0x64000
#define cReg_upen_LOPvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: lopvttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvttim_mod_Mask                                                            cBit27_26
#define c_upen_LOPvtcfg_lopvttim_mod_Shift                                                                  26

/*--------------------------------------
BitField Name: lopvterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvterr_clr_Mask                                                            cBit25_18
#define c_upen_LOPvtcfg_lopvterr_clr_Shift                                                                  18

/*--------------------------------------
BitField Name: lopvterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvterr_set_Mask                                                            cBit17_10
#define c_upen_LOPvtcfg_lopvterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: lopvterr_step
BitField Type: RW
BitField Desc: The Total Step to Force LOP VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvterr_step_Mask                                                             cBit9_2
#define c_upen_LOPvtcfg_lopvterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: lopvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOP VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_LOPvtcfg_lopvtmsk_pos_Shift_01                                                               18
#define c_upen_LOPvtcfg_lopvtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_LOPvtcfg_lopvtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: lopvtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOP VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvtmsk_ena_Mask                                                               cBit17
#define c_upen_LOPvtcfg_lopvtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: lopvterr_num
BitField Type: RW
BitField Desc: The Total LOP VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvterr_num_Mask                                                             cBit16_1
#define c_upen_LOPvtcfg_lopvterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: lopvtfrc_mod
BitField Type: RW
BitField Desc: Force LOP VT Mode 0: One shot ( N events in T the unit time) 1:
continous (lopvterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvtfrc_mod_Mask                                                                cBit0
#define c_upen_LOPvtcfg_lopvtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer VT Error Status
Reg Addr   : 0x64400-0x647FF
Reg Formula: 0x64400 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force Loss Of Pointer VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_LOPvtsta                                                                             0x64400
#define cReg_upen_LOPvtsta_WidthVal                                                                         64

/*--------------------------------------
BitField Name: lopvtsta_almset
BitField Type: RW
BitField Desc: LOP VT alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_LOPvtsta_lopvtsta_almset_Mask                                                            cBit27
#define c_upen_LOPvtsta_lopvtsta_almset_Shift                                                               27

/*--------------------------------------
BitField Name: lopvtstaclralrm
BitField Type: RW
BitField Desc: LOP VT clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_LOPvtsta_lopvtstaclralrm_Mask                                                         cBit22_15
#define c_upen_LOPvtsta_lopvtstaclralrm_Shift                                                               15

/*--------------------------------------
BitField Name: lopvtstasetalrm
BitField Type: RW
BitField Desc: LOP VT Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_LOPvtsta_lopvtstasetalrm_Mask                                                          cBit14_7
#define c_upen_LOPvtsta_lopvtstasetalrm_Shift                                                                7

/*--------------------------------------
BitField Name: lopvtstatimecnt
BitField Type: RW
BitField Desc: LOP VT Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_LOPvtsta_lopvtstatimecnt_Mask                                                           cBit6_0
#define c_upen_LOPvtsta_lopvtstatimecnt_Shift                                                                0

/*--------------------------------------
BitField Name: lopvtstafrc_ena
BitField Type: RW
BitField Desc: LOP VT Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_LOPvtsta_lopvtstafrc_ena_Mask                                                            cBit31
#define c_upen_LOPvtsta_lopvtstafrc_ena_Shift                                                               31

/*--------------------------------------
BitField Name: lopvtstaerrstep
BitField Type: RW
BitField Desc: The Second LOP VT Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_LOPvtsta_lopvtstaerrstep_Mask                                                         cBit30_23
#define c_upen_LOPvtsta_lopvtstaerrstep_Shift                                                               23

/*--------------------------------------
BitField Name: lopvtstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask LOP VT Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_LOPvtsta_lopvtstamsk_pos_Mask                                                         cBit22_19
#define c_upen_LOPvtsta_lopvtstamsk_pos_Shift                                                               19

/*--------------------------------------
BitField Name: lopvtstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error LOP VT Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_LOPvtsta_lopvtstaerr_cnt_Mask                                                          cBit18_3
#define c_upen_LOPvtsta_lopvtstaerr_cnt_Shift                                                                3

/*--------------------------------------
BitField Name: lopvtstafrc_sta
BitField Type: RW
BitField Desc: LOP VT State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_LOPvtsta_lopvtstafrc_sta_Mask                                                           cBit2_1
#define c_upen_LOPvtsta_lopvtstafrc_sta_Shift                                                                1

/*--------------------------------------
BitField Name: lopvtstaoneunit
BitField Type: RW
BitField Desc: Force LOP VT in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOPvtsta_lopvtstaoneunit_Mask                                                             cBit0
#define c_upen_LOPvtsta_lopvtstaoneunit_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS VT Error Configuration
Reg Addr   : 0x64800-0x64BFF
Reg Formula: 0x64800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force AIS VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_AISvtcfg                                                                             0x64800
#define cReg_upen_AISvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: aisvttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvttim_mod_Mask                                                            cBit27_26
#define c_upen_AISvtcfg_aisvttim_mod_Shift                                                                  26

/*--------------------------------------
BitField Name: aisvterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvterr_clr_Mask                                                            cBit25_18
#define c_upen_AISvtcfg_aisvterr_clr_Shift                                                                  18

/*--------------------------------------
BitField Name: aisvterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvterr_set_Mask                                                            cBit17_10
#define c_upen_AISvtcfg_aisvterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: aisvterr_step
BitField Type: RW
BitField Desc: The Total Step to Force AIS VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvterr_step_Mask                                                             cBit9_2
#define c_upen_AISvtcfg_aisvterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: aisvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for AIS VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_AISvtcfg_aisvtmsk_pos_Shift_01                                                               18
#define c_upen_AISvtcfg_aisvtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_AISvtcfg_aisvtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: aisvtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For AIS VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvtmsk_ena_Mask                                                               cBit17
#define c_upen_AISvtcfg_aisvtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: aisvterr_num
BitField Type: RW
BitField Desc: The Total AIS VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvterr_num_Mask                                                             cBit16_1
#define c_upen_AISvtcfg_aisvterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: aisvtfrc_mod
BitField Type: RW
BitField Desc: Force AIS VT Mode 0: One shot ( N events in T the unit time) 1:
continous (aisvterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_AISvtcfg_aisvtfrc_mod_Mask                                                                cBit0
#define c_upen_AISvtcfg_aisvtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS VT Error Status
Reg Addr   : 0x64C00-0x64FFF
Reg Formula: 0x64C00 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force AIS VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_AISvtsta                                                                             0x64C00
#define cReg_upen_AISvtsta_WidthVal                                                                         64

/*--------------------------------------
BitField Name: aisvtsta_almset
BitField Type: RW
BitField Desc: AIS VT alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_AISvtsta_aisvtsta_almset_Mask                                                            cBit27
#define c_upen_AISvtsta_aisvtsta_almset_Shift                                                               27

/*--------------------------------------
BitField Name: aisvtstaclralrm
BitField Type: RW
BitField Desc: AIS VT clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_AISvtsta_aisvtstaclralrm_Mask                                                         cBit22_15
#define c_upen_AISvtsta_aisvtstaclralrm_Shift                                                               15

/*--------------------------------------
BitField Name: aisvtstasetalrm
BitField Type: RW
BitField Desc: AIS VT Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_AISvtsta_aisvtstasetalrm_Mask                                                          cBit14_7
#define c_upen_AISvtsta_aisvtstasetalrm_Shift                                                                7

/*--------------------------------------
BitField Name: aisvtstatimecnt
BitField Type: RW
BitField Desc: AIS VT Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_AISvtsta_aisvtstatimecnt_Mask                                                           cBit6_0
#define c_upen_AISvtsta_aisvtstatimecnt_Shift                                                                0

/*--------------------------------------
BitField Name: aisvtstafrc_ena
BitField Type: RW
BitField Desc: AIS VT Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_AISvtsta_aisvtstafrc_ena_Mask                                                            cBit31
#define c_upen_AISvtsta_aisvtstafrc_ena_Shift                                                               31

/*--------------------------------------
BitField Name: aisvtstaerrstep
BitField Type: RW
BitField Desc: The Second AIS VT Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_AISvtsta_aisvtstaerrstep_Mask                                                         cBit30_23
#define c_upen_AISvtsta_aisvtstaerrstep_Shift                                                               23

/*--------------------------------------
BitField Name: aisvtstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask AIS VT Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_AISvtsta_aisvtstamsk_pos_Mask                                                         cBit22_19
#define c_upen_AISvtsta_aisvtstamsk_pos_Shift                                                               19

/*--------------------------------------
BitField Name: aisvtstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error AIS VT Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_AISvtsta_aisvtstaerr_cnt_Mask                                                          cBit18_3
#define c_upen_AISvtsta_aisvtstaerr_cnt_Shift                                                                3

/*--------------------------------------
BitField Name: aisvtstafrc_sta
BitField Type: RW
BitField Desc: AIS VT State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_AISvtsta_aisvtstafrc_sta_Mask                                                           cBit2_1
#define c_upen_AISvtsta_aisvtstafrc_sta_Shift                                                                1

/*--------------------------------------
BitField Name: aisvtstaoneunit
BitField Type: RW
BitField Desc: Force AIS VT in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_AISvtsta_aisvtstaoneunit_Mask                                                             cBit0
#define c_upen_AISvtsta_aisvtstaoneunit_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ VT Error Configuration
Reg Addr   : 0x65000-0x653FF
Reg Formula: 0x65000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force UEQ VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_UEQvtcfg                                                                             0x65000
#define cReg_upen_UEQvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: ueqvttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvttim_mod_Mask                                                            cBit27_26
#define c_upen_UEQvtcfg_ueqvttim_mod_Shift                                                                  26

/*--------------------------------------
BitField Name: ueqvterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvterr_clr_Mask                                                            cBit25_18
#define c_upen_UEQvtcfg_ueqvterr_clr_Shift                                                                  18

/*--------------------------------------
BitField Name: ueqvterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvterr_set_Mask                                                            cBit17_10
#define c_upen_UEQvtcfg_ueqvterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: ueqvterr_step
BitField Type: RW
BitField Desc: The Total Step to Force UEQ VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvterr_step_Mask                                                             cBit9_2
#define c_upen_UEQvtcfg_ueqvterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: ueqvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for UEQ VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_UEQvtcfg_ueqvtmsk_pos_Shift_01                                                               18
#define c_upen_UEQvtcfg_ueqvtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_UEQvtcfg_ueqvtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: ueqvtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For UEQ VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvtmsk_ena_Mask                                                               cBit17
#define c_upen_UEQvtcfg_ueqvtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: ueqvterr_num
BitField Type: RW
BitField Desc: The Total UEQ VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvterr_num_Mask                                                             cBit16_1
#define c_upen_UEQvtcfg_ueqvterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: ueqvtfrc_mod
BitField Type: RW
BitField Desc: Force UEQ VT Mode 0: One shot ( N events in T the unit time) 1:
continous (ueqvterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_UEQvtcfg_ueqvtfrc_mod_Mask                                                                cBit0
#define c_upen_UEQvtcfg_ueqvtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ VT Error Status
Reg Addr   : 0x65400-0x657FF
Reg Formula: 0x65400 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force UEQ VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_UEQvtsta                                                                             0x65400
#define cReg_upen_UEQvtsta_WidthVal                                                                         64

/*--------------------------------------
BitField Name: ueqvtsta_almset
BitField Type: RW
BitField Desc: UEQ VT alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_UEQvtsta_ueqvtsta_almset_Mask                                                            cBit27
#define c_upen_UEQvtsta_ueqvtsta_almset_Shift                                                               27

/*--------------------------------------
BitField Name: ueqvtstaclralrm
BitField Type: RW
BitField Desc: UEQ VT clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_UEQvtsta_ueqvtstaclralrm_Mask                                                         cBit22_15
#define c_upen_UEQvtsta_ueqvtstaclralrm_Shift                                                               15

/*--------------------------------------
BitField Name: ueqvtstasetalrm
BitField Type: RW
BitField Desc: UEQ VT Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_UEQvtsta_ueqvtstasetalrm_Mask                                                          cBit14_7
#define c_upen_UEQvtsta_ueqvtstasetalrm_Shift                                                                7

/*--------------------------------------
BitField Name: ueqvtstatimecnt
BitField Type: RW
BitField Desc: UEQ VT Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_UEQvtsta_ueqvtstatimecnt_Mask                                                           cBit6_0
#define c_upen_UEQvtsta_ueqvtstatimecnt_Shift                                                                0

/*--------------------------------------
BitField Name: ueqvtstafrc_ena
BitField Type: RW
BitField Desc: UEQ VT Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_UEQvtsta_ueqvtstafrc_ena_Mask                                                            cBit31
#define c_upen_UEQvtsta_ueqvtstafrc_ena_Shift                                                               31

/*--------------------------------------
BitField Name: ueqvtstaerrstep
BitField Type: RW
BitField Desc: The Second UEQ VT Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_UEQvtsta_ueqvtstaerrstep_Mask                                                         cBit30_23
#define c_upen_UEQvtsta_ueqvtstaerrstep_Shift                                                               23

/*--------------------------------------
BitField Name: ueqvtstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask UEQ VT Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_UEQvtsta_ueqvtstamsk_pos_Mask                                                         cBit22_19
#define c_upen_UEQvtsta_ueqvtstamsk_pos_Shift                                                               19

/*--------------------------------------
BitField Name: ueqvtstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error UEQ VT Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_UEQvtsta_ueqvtstaerr_cnt_Mask                                                          cBit18_3
#define c_upen_UEQvtsta_ueqvtstaerr_cnt_Shift                                                                3

/*--------------------------------------
BitField Name: ueqvtstafrc_sta
BitField Type: RW
BitField Desc: UEQ VT State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_UEQvtsta_ueqvtstafrc_sta_Mask                                                           cBit2_1
#define c_upen_UEQvtsta_ueqvtstafrc_sta_Shift                                                                1

/*--------------------------------------
BitField Name: ueqvtstaoneunit
BitField Type: RW
BitField Desc: Force UEQ VT in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_UEQvtsta_ueqvtstaoneunit_Mask                                                             cBit0
#define c_upen_UEQvtsta_ueqvtstaoneunit_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP VT Error Configuration
Reg Addr   : 0x65800-0x65BFF
Reg Formula: 0x65800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force BIP VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_BIPvtcfg                                                                             0x65800
#define cReg_upen_BIPvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: bipvttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvttim_mod_Mask                                                            cBit13_12
#define c_upen_BIPvtcfg_bipvttim_mod_Shift                                                                  12

/*--------------------------------------
BitField Name: bipvterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvterr_clr_Mask                                                               cBit11
#define c_upen_BIPvtcfg_bipvterr_clr_Shift                                                                  11

/*--------------------------------------
BitField Name: bipvterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvterr_set_Mask                                                               cBit10
#define c_upen_BIPvtcfg_bipvterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: bipvterr_step
BitField Type: RW
BitField Desc: The Total Step to Force BIP at VT  Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvterr_step_Mask                                                             cBit9_2
#define c_upen_BIPvtcfg_bipvterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: bipvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for BIP at VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_BIPvtcfg_bipvtmsk_pos_Shift_01                                                               18
#define c_upen_BIPvtcfg_bipvtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_BIPvtcfg_bipvtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: bipvtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For BIP at VT1 Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvtmsk_ena_Mask                                                               cBit17
#define c_upen_BIPvtcfg_bipvtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: bipvterr_num
BitField Type: RW
BitField Desc: The Total BIP at VT Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvterr_num_Mask                                                             cBit16_1
#define c_upen_BIPvtcfg_bipvterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: bipvtfrc_mod
BitField Type: RW
BitField Desc: Force BIP at VT Mode 0: One shot 1: continous (bipvterr_num = 16
bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPvtcfg_bipvtfrc_mod_Mask                                                                cBit0
#define c_upen_BIPvtcfg_bipvtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP VT Error Status
Reg Addr   : 0x65C00-0x65FFF
Reg Formula: 0x65C00 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force BIP VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_BIPvtsta                                                                             0x65C00
#define cReg_upen_BIPvtsta_WidthVal                                                                         64

/*--------------------------------------
BitField Name: bipstssta_almset
BitField Type: RW
BitField Desc: BIP STS alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_BIPvtsta_bipstssta_almset_Mask                                                            cBit9
#define c_upen_BIPvtsta_bipstssta_almset_Shift                                                               9

/*--------------------------------------
BitField Name: bipstsstaclralrm
BitField Type: RW
BitField Desc: BIP STS clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_BIPvtsta_bipstsstaclralrm_Mask                                                            cBit8
#define c_upen_BIPvtsta_bipstsstaclralrm_Shift                                                               8

/*--------------------------------------
BitField Name: bipstsstasetalrm
BitField Type: RW
BitField Desc: BIP STS Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_BIPvtsta_bipstsstasetalrm_Mask                                                            cBit7
#define c_upen_BIPvtsta_bipstsstasetalrm_Shift                                                               7

/*--------------------------------------
BitField Name: bipstsstatimecnt
BitField Type: RW
BitField Desc: BIP STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_BIPvtsta_bipstsstatimecnt_Mask                                                          cBit6_0
#define c_upen_BIPvtsta_bipstsstatimecnt_Shift                                                               0

/*--------------------------------------
BitField Name: bipstsstafrc_ena
BitField Type: RW
BitField Desc: BIP STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_BIPvtsta_bipstsstafrc_ena_Mask                                                           cBit31
#define c_upen_BIPvtsta_bipstsstafrc_ena_Shift                                                              31

/*--------------------------------------
BitField Name: bipstsstaerrstep
BitField Type: RW
BitField Desc: The Second BIP STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_BIPvtsta_bipstsstaerrstep_Mask                                                        cBit30_23
#define c_upen_BIPvtsta_bipstsstaerrstep_Shift                                                              23

/*--------------------------------------
BitField Name: bipstsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask BIP STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_BIPvtsta_bipstsstamsk_pos_Mask                                                        cBit22_19
#define c_upen_BIPvtsta_bipstsstamsk_pos_Shift                                                              19

/*--------------------------------------
BitField Name: bipstsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error BIP STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_BIPvtsta_bipstsstaerr_cnt_Mask                                                         cBit18_3
#define c_upen_BIPvtsta_bipstsstaerr_cnt_Shift                                                               3

/*--------------------------------------
BitField Name: bipstsstafrc_sta
BitField Type: RW
BitField Desc: BIP STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_BIPvtsta_bipstsstafrc_sta_Mask                                                          cBit2_1
#define c_upen_BIPvtsta_bipstsstafrc_sta_Shift                                                               1

/*--------------------------------------
BitField Name: bipstsstaoneunit
BitField Type: RW
BitField Desc: Force BIP STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPvtsta_bipstsstaoneunit_Mask                                                            cBit0
#define c_upen_BIPvtsta_bipstsstaoneunit_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force REI VT Error Configuration
Reg Addr   : 0x66000-0x663FF
Reg Formula: 0x66000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force REI VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_REIvtcfg                                                                             0x66000
#define cReg_upen_REIvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: reivttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_REIvtcfg_reivttim_mod_Mask                                                            cBit13_12
#define c_upen_REIvtcfg_reivttim_mod_Shift                                                                  12

/*--------------------------------------
BitField Name: reivterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_REIvtcfg_reivterr_clr_Mask                                                               cBit11
#define c_upen_REIvtcfg_reivterr_clr_Shift                                                                  11

/*--------------------------------------
BitField Name: reivterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_REIvtcfg_reivterr_set_Mask                                                               cBit10
#define c_upen_REIvtcfg_reivterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: reivterr_step
BitField Type: RW
BitField Desc: The Total Step to Force REI at VT  Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_REIvtcfg_reivterr_step_Mask                                                             cBit9_2
#define c_upen_REIvtcfg_reivterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: reivtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for REI at VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_REIvtcfg_reivtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_REIvtcfg_reivtmsk_pos_Shift_01                                                               18
#define c_upen_REIvtcfg_reivtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_REIvtcfg_reivtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: reivtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For REI at VT1 Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_REIvtcfg_reivtmsk_ena_Mask                                                               cBit17
#define c_upen_REIvtcfg_reivtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: reivterr_num
BitField Type: RW
BitField Desc: The Total REI at VT Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_REIvtcfg_reivterr_num_Mask                                                             cBit16_1
#define c_upen_REIvtcfg_reivterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: reivtfrc_mod
BitField Type: RW
BitField Desc: Force REI at VT Mode 0: One shot 1: continous (reivterr_num = 16
bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIvtcfg_reivtfrc_mod_Mask                                                                cBit0
#define c_upen_REIvtcfg_reivtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force REI VT Error Status
Reg Addr   : 0x66400-0x667FF
Reg Formula: 0x66400 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force REI VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_REIvtsta                                                                             0x66400
#define cReg_upen_REIvtsta_WidthVal                                                                         64

/*--------------------------------------
BitField Name: reivtsta_almset
BitField Type: RW
BitField Desc: REIVT alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_REIvtsta_reivtsta_almset_Mask                                                             cBit9
#define c_upen_REIvtsta_reivtsta_almset_Shift                                                                9

/*--------------------------------------
BitField Name: reivtstaclralrm
BitField Type: RW
BitField Desc: REIVT clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_REIvtsta_reivtstaclralrm_Mask                                                             cBit8
#define c_upen_REIvtsta_reivtstaclralrm_Shift                                                                8

/*--------------------------------------
BitField Name: reivtstasetalrm
BitField Type: RW
BitField Desc: REIVT Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_REIvtsta_reivtstasetalrm_Mask                                                             cBit7
#define c_upen_REIvtsta_reivtstasetalrm_Shift                                                                7

/*--------------------------------------
BitField Name: reivtstatimecnt
BitField Type: RW
BitField Desc: REIVT Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_REIvtsta_reivtstatimecnt_Mask                                                           cBit6_0
#define c_upen_REIvtsta_reivtstatimecnt_Shift                                                                0

/*--------------------------------------
BitField Name: reivtstafrc_ena
BitField Type: RW
BitField Desc: REIVT Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_REIvtsta_reivtstafrc_ena_Mask                                                            cBit31
#define c_upen_REIvtsta_reivtstafrc_ena_Shift                                                               31

/*--------------------------------------
BitField Name: reivtstaerrstep
BitField Type: RW
BitField Desc: The Second REIVT Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_REIvtsta_reivtstaerrstep_Mask                                                         cBit30_23
#define c_upen_REIvtsta_reivtstaerrstep_Shift                                                               23

/*--------------------------------------
BitField Name: reivtstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask REIVT Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_REIvtsta_reivtstamsk_pos_Mask                                                         cBit22_19
#define c_upen_REIvtsta_reivtstamsk_pos_Shift                                                               19

/*--------------------------------------
BitField Name: reivtstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error REIVT Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_REIvtsta_reivtstaerr_cnt_Mask                                                          cBit18_3
#define c_upen_REIvtsta_reivtstaerr_cnt_Shift                                                                3

/*--------------------------------------
BitField Name: reivtstafrc_sta
BitField Type: RW
BitField Desc: REIVT State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_REIvtsta_reivtstafrc_sta_Mask                                                           cBit2_1
#define c_upen_REIvtsta_reivtstafrc_sta_Shift                                                                1

/*--------------------------------------
BitField Name: reivtstaoneunit
BitField Type: RW
BitField Desc: Force REIVT in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIvtsta_reivtstaoneunit_Mask                                                             cBit0
#define c_upen_REIvtsta_reivtstaoneunit_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI VT Error Configuration
Reg Addr   : 0x66800-0x66BFF
Reg Formula: 0x66800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force RDI VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_RDIvtcfg                                                                             0xa2800

/*--------------------------------------
BitField Name: rdivttype
BitField Type: RW
BitField Desc: RDI Type at low path 1000: E-RDI P Enable 0100: E-RDI S Enable
0010: E-RDI C Enable 0001: RDI Enable
BitField Bits: [63:60]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivttype_Mask                                                               cBit31_28
#define c_upen_RDIvtcfg_rdivttype_Shift                                                                     28

/*--------------------------------------
BitField Name: rdivttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivttim_mod_Mask                                                            cBit27_26
#define c_upen_RDIvtcfg_rdivttim_mod_Shift                                                                  26

/*--------------------------------------
BitField Name: rdivterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivterr_clr_Mask                                                            cBit25_18
#define c_upen_RDIvtcfg_rdivterr_clr_Shift                                                                  18

/*--------------------------------------
BitField Name: rdivterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivterr_set_Mask                                                            cBit17_10
#define c_upen_RDIvtcfg_rdivterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: rdivterr_step
BitField Type: RW
BitField Desc: The Total Step to Force RDI VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivterr_step_Mask                                                             cBit9_2
#define c_upen_RDIvtcfg_rdivterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: rdivtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for RDI VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_RDIvtcfg_rdivtmsk_pos_Shift_01                                                               18
#define c_upen_RDIvtcfg_rdivtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_RDIvtcfg_rdivtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: rdivtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For RDI VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivtmsk_ena_Mask                                                               cBit17
#define c_upen_RDIvtcfg_rdivtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: rdivterr_num
BitField Type: RW
BitField Desc: The Total RDI VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivterr_num_Mask                                                             cBit16_1
#define c_upen_RDIvtcfg_rdivterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: rdivtfrc_mod
BitField Type: RW
BitField Desc: Force RDI VT Mode 0: One shot ( N events in T the unit time) 1:
continous (rdivterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivtfrc_mod_Mask                                                                cBit0
#define c_upen_RDIvtcfg_rdivtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI VT Error Status
Reg Addr   : 0x66C00-0x66FFF
Reg Formula: 0x66C00 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force RDI VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_RDIvtsta                                                                             0x66C00
#define cReg_upen_RDIvtsta_WidthVal                                                                         64

/*--------------------------------------
BitField Name: rdivtsta_almset
BitField Type: RW
BitField Desc: RDI VT alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_RDIvtsta_rdivtsta_almset_Mask                                                            cBit27
#define c_upen_RDIvtsta_rdivtsta_almset_Shift                                                               27

/*--------------------------------------
BitField Name: rdivtstaclralrm
BitField Type: RW
BitField Desc: RDI VT clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_RDIvtsta_rdivtstaclralrm_Mask                                                         cBit22_15
#define c_upen_RDIvtsta_rdivtstaclralrm_Shift                                                               15

/*--------------------------------------
BitField Name: rdivtstasetalrm
BitField Type: RW
BitField Desc: RDI VT Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_RDIvtsta_rdivtstasetalrm_Mask                                                          cBit14_7
#define c_upen_RDIvtsta_rdivtstasetalrm_Shift                                                                7

/*--------------------------------------
BitField Name: rdivtstatimecnt
BitField Type: RW
BitField Desc: RDI VT Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_RDIvtsta_rdivtstatimecnt_Mask                                                           cBit6_0
#define c_upen_RDIvtsta_rdivtstatimecnt_Shift                                                                0

/*--------------------------------------
BitField Name: rdivtstafrc_ena
BitField Type: RW
BitField Desc: RDI VT Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_RDIvtsta_rdivtstafrc_ena_Mask                                                            cBit31
#define c_upen_RDIvtsta_rdivtstafrc_ena_Shift                                                               31

/*--------------------------------------
BitField Name: rdivtstaerrstep
BitField Type: RW
BitField Desc: The Second RDI VT Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_RDIvtsta_rdivtstaerrstep_Mask                                                         cBit30_23
#define c_upen_RDIvtsta_rdivtstaerrstep_Shift                                                               23

/*--------------------------------------
BitField Name: rdivtstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask RDI VT Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_RDIvtsta_rdivtstamsk_pos_Mask                                                         cBit22_19
#define c_upen_RDIvtsta_rdivtstamsk_pos_Shift                                                               19

/*--------------------------------------
BitField Name: rdivtstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error RDI VT Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_RDIvtsta_rdivtstaerr_cnt_Mask                                                          cBit18_3
#define c_upen_RDIvtsta_rdivtstaerr_cnt_Shift                                                                3

/*--------------------------------------
BitField Name: rdivtstafrc_sta
BitField Type: RW
BitField Desc: RDI VT State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_RDIvtsta_rdivtstafrc_sta_Mask                                                           cBit2_1
#define c_upen_RDIvtsta_rdivtstafrc_sta_Shift                                                                1

/*--------------------------------------
BitField Name: rdivtstaoneunit
BitField Type: RW
BitField Desc: Force RDI VT in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RDIvtsta_rdivtstaoneunit_Mask                                                             cBit0
#define c_upen_RDIvtsta_rdivtstaoneunit_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force RFI VT Error Configuration
Reg Addr   : 0x67000-0x673FF
Reg Formula: 0x67000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force RFI VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_RFIvtcfg                                                                             0x67000
#define cReg_upen_RFIvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: rdivttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_RFIvtcfg_rdivttim_mod_Mask                                                            cBit27_26
#define c_upen_RFIvtcfg_rdivttim_mod_Shift                                                                  26

/*--------------------------------------
BitField Name: rdivterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_RFIvtcfg_rdivterr_clr_Mask                                                            cBit25_18
#define c_upen_RFIvtcfg_rdivterr_clr_Shift                                                                  18

/*--------------------------------------
BitField Name: rdivterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_RFIvtcfg_rdivterr_set_Mask                                                            cBit17_10
#define c_upen_RFIvtcfg_rdivterr_set_Shift                                                                  10

/*--------------------------------------
BitField Name: rdivterr_step
BitField Type: RW
BitField Desc: The Total Step to Force RDI VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_RFIvtcfg_rdivterr_step_Mask                                                             cBit9_2
#define c_upen_RFIvtcfg_rdivterr_step_Shift                                                                  2

/*--------------------------------------
BitField Name: rdivtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for RDI VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_RFIvtcfg_rdivtmsk_pos_Mask_01                                                         cBit31_18
#define c_upen_RFIvtcfg_rdivtmsk_pos_Shift_01                                                               18
#define c_upen_RFIvtcfg_rdivtmsk_pos_Mask_02                                                           cBit1_0
#define c_upen_RFIvtcfg_rdivtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: rdivtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For RDI VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_RFIvtcfg_rdivtmsk_ena_Mask                                                               cBit17
#define c_upen_RFIvtcfg_rdivtmsk_ena_Shift                                                                  17

/*--------------------------------------
BitField Name: rdivterr_num
BitField Type: RW
BitField Desc: The Total RDI VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_RFIvtcfg_rdivterr_num_Mask                                                             cBit16_1
#define c_upen_RFIvtcfg_rdivterr_num_Shift                                                                   1

/*--------------------------------------
BitField Name: rdivtfrc_mod
BitField Type: RW
BitField Desc: Force RDI VT Mode 0: One shot ( N events in T the unit time) 1:
continous (rdivterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RFIvtcfg_rdivtfrc_mod_Mask                                                                cBit0
#define c_upen_RFIvtcfg_rdivtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force RFI VT Error Status
Reg Addr   : 0x67C00-0x67FFF
Reg Formula: 0x67C00 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force RFI VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_RFIvtsta                                                                             0x67C00
#define cReg_upen_RFIvtsta_WidthVal                                                                         64

/*--------------------------------------
BitField Name: rfivtsta_almset
BitField Type: RW
BitField Desc: RFI VT alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_RFIvtsta_rfivtsta_almset_Mask                                                            cBit27
#define c_upen_RFIvtsta_rfivtsta_almset_Shift                                                               27

/*--------------------------------------
BitField Name: rfivtstaclralrm
BitField Type: RW
BitField Desc: RFI VT clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_RFIvtsta_rfivtstaclralrm_Mask                                                         cBit22_15
#define c_upen_RFIvtsta_rfivtstaclralrm_Shift                                                               15

/*--------------------------------------
BitField Name: rfivtstasetalrm
BitField Type: RW
BitField Desc: RFI VT Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_RFIvtsta_rfivtstasetalrm_Mask                                                          cBit14_7
#define c_upen_RFIvtsta_rfivtstasetalrm_Shift                                                                7

/*--------------------------------------
BitField Name: rfivtstatimecnt
BitField Type: RW
BitField Desc: RFI VT Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_RFIvtsta_rfivtstatimecnt_Mask                                                           cBit6_0
#define c_upen_RFIvtsta_rfivtstatimecnt_Shift                                                                0

/*--------------------------------------
BitField Name: rfivtstafrc_ena
BitField Type: RW
BitField Desc: RFI VT Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_RFIvtsta_rfivtstafrc_ena_Mask                                                            cBit31
#define c_upen_RFIvtsta_rfivtstafrc_ena_Shift                                                               31

/*--------------------------------------
BitField Name: rfivtstaerrstep
BitField Type: RW
BitField Desc: The Second RFI VT Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_RFIvtsta_rfivtstaerrstep_Mask                                                         cBit30_23
#define c_upen_RFIvtsta_rfivtstaerrstep_Shift                                                               23

/*--------------------------------------
BitField Name: rfivtstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask RFI VT Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_RFIvtsta_rfivtstamsk_pos_Mask                                                         cBit22_19
#define c_upen_RFIvtsta_rfivtstamsk_pos_Shift                                                               19

/*--------------------------------------
BitField Name: rfivtstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error RFI VT Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_RFIvtsta_rfivtstaerr_cnt_Mask                                                          cBit18_3
#define c_upen_RFIvtsta_rfivtstaerr_cnt_Shift                                                                3

/*--------------------------------------
BitField Name: rfivtstafrc_sta
BitField Type: RW
BitField Desc: RFI VT State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_RFIvtsta_rfivtstafrc_sta_Mask                                                           cBit2_1
#define c_upen_RFIvtsta_rfivtstafrc_sta_Shift                                                                1

/*--------------------------------------
BitField Name: rfivtstaoneunit
BitField Type: RW
BitField Desc: Force RFI VT in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RFIvtsta_rfivtstaoneunit_Mask                                                             cBit0
#define c_upen_RFIvtsta_rfivtstaoneunit_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Configuration the unit time to force in VT
Reg Addr   : 0x67c01-0x67c01
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure n second in  VT

------------------------------------------------------------------------------*/
#define cReg_upennsecvt                                                                                0x67c01

/*--------------------------------------
BitField Name: cfgmsecvt
BitField Type: RW
BitField Desc: Configuration the unit time  in VT Congiguration
BitField Bits: [31:0]
--------------------------------------*/
#define c_upennsecvt_cfgmsecvt_Mask                                                                   cBit31_0
#define c_upennsecvt_cfgmsecvt_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer STS Error Configuration
Reg Addr   : 0x23200-0x2321F
Reg Formula: 0x23200 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force Loss Of Pointer STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_LOPstscfg                                                                            0x23200
#define cReg_upen_LOPstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: lopststim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_LOPstscfg_lopststim_mod_Mask                                                          cBit27_26
#define c_upen_LOPstscfg_lopststim_mod_Shift                                                                26

/*--------------------------------------
BitField Name: lopstserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstserr_clr_Mask                                                          cBit25_18
#define c_upen_LOPstscfg_lopstserr_clr_Shift                                                                18

/*--------------------------------------
BitField Name: lopstserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstserr_set_Mask                                                          cBit17_10
#define c_upen_LOPstscfg_lopstserr_set_Shift                                                                10

/*--------------------------------------
BitField Name: lopstserr_step
BitField Type: RW
BitField Desc: The Total Step to Force LOP STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstserr_step_Mask                                                           cBit9_2
#define c_upen_LOPstscfg_lopstserr_step_Shift                                                                2

/*--------------------------------------
BitField Name: lopstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOP STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstsmsk_pos_Mask_01                                                       cBit31_18
#define c_upen_LOPstscfg_lopstsmsk_pos_Shift_01                                                             18
#define c_upen_LOPstscfg_lopstsmsk_pos_Mask_02                                                         cBit1_0
#define c_upen_LOPstscfg_lopstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: lopstsmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOP STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstsmsk_ena_Mask                                                             cBit17
#define c_upen_LOPstscfg_lopstsmsk_ena_Shift                                                                17

/*--------------------------------------
BitField Name: lopstserr_num
BitField Type: RW
BitField Desc: The Total LOP STS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstserr_num_Mask                                                           cBit16_1
#define c_upen_LOPstscfg_lopstserr_num_Shift                                                                 1

/*--------------------------------------
BitField Name: lopstsfrc_mod
BitField Type: RW
BitField Desc: Force LOP STS Mode 0: One shot ( N events in T the unit time) 1:
continous (lopstserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstsfrc_mod_Mask                                                              cBit0
#define c_upen_LOPstscfg_lopstsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer STS STS Error Status
Reg Addr   : 0x23220-0x2323F
Reg Formula: 0x23220 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force Loss Of Pointer STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_LOPstssta                                                                            0x23220
#define cReg_upen_LOPstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: lopstssta_almset
BitField Type: RW
BitField Desc: LOP STS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_LOPstssta_lopstssta_almset_Mask                                                          cBit27
#define c_upen_LOPstssta_lopstssta_almset_Shift                                                             27

/*--------------------------------------
BitField Name: lopstsstaclralrm
BitField Type: RW
BitField Desc: LOP STS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstaclralrm_Mask                                                       cBit22_15
#define c_upen_LOPstssta_lopstsstaclralrm_Shift                                                             15

/*--------------------------------------
BitField Name: lopstsstasetalrm
BitField Type: RW
BitField Desc: LOP STS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstasetalrm_Mask                                                        cBit14_7
#define c_upen_LOPstssta_lopstsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: lopstsstatimecnt
BitField Type: RW
BitField Desc: LOP STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstatimecnt_Mask                                                         cBit6_0
#define c_upen_LOPstssta_lopstsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: lopstsstafrc_ena
BitField Type: RW
BitField Desc: LOP STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstafrc_ena_Mask                                                          cBit31
#define c_upen_LOPstssta_lopstsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: lopstsstaerrstep
BitField Type: RW
BitField Desc: The Second LOP STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstaerrstep_Mask                                                       cBit30_23
#define c_upen_LOPstssta_lopstsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: lopstsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask LOP STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_LOPstssta_lopstsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: lopstsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error LOP STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_LOPstssta_lopstsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: lopstsstafrc_sta
BitField Type: RW
BitField Desc: LOP STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_LOPstssta_lopstsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: lopstsstaoneunit
BitField Type: RW
BitField Desc: Force LOP STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOPstssta_lopstsstaoneunit_Mask                                                           cBit0
#define c_upen_LOPstssta_lopstsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS STS Error Configuration
Reg Addr   : 0x23240-0x2325F
Reg Formula: 0x23240 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force AIS STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_AISstscfg                                                                            0x23240
#define cReg_upen_AISstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: aisststim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_AISstscfg_aisststim_mod_Mask                                                          cBit27_26
#define c_upen_AISstscfg_aisststim_mod_Shift                                                                26

/*--------------------------------------
BitField Name: aisstserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_AISstscfg_aisstserr_clr_Mask                                                          cBit25_18
#define c_upen_AISstscfg_aisstserr_clr_Shift                                                                18

/*--------------------------------------
BitField Name: aisstserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_AISstscfg_aisstserr_set_Mask                                                          cBit17_10
#define c_upen_AISstscfg_aisstserr_set_Shift                                                                10

/*--------------------------------------
BitField Name: aisstserr_step
BitField Type: RW
BitField Desc: The Total Step to Force AIS STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_AISstscfg_aisstserr_step_Mask                                                           cBit9_2
#define c_upen_AISstscfg_aisstserr_step_Shift                                                                2

/*--------------------------------------
BitField Name: aisstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for AIS STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_AISstscfg_aisstsmsk_pos_Mask_01                                                       cBit31_18
#define c_upen_AISstscfg_aisstsmsk_pos_Shift_01                                                             18
#define c_upen_AISstscfg_aisstsmsk_pos_Mask_02                                                         cBit1_0
#define c_upen_AISstscfg_aisstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: aisstsmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For AIS STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_AISstscfg_aisstsmsk_ena_Mask                                                             cBit17
#define c_upen_AISstscfg_aisstsmsk_ena_Shift                                                                17

/*--------------------------------------
BitField Name: aisstserr_num
BitField Type: RW
BitField Desc: The Total AIS STS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_AISstscfg_aisstserr_num_Mask                                                           cBit16_1
#define c_upen_AISstscfg_aisstserr_num_Shift                                                                 1

/*--------------------------------------
BitField Name: aisstsfrc_mod
BitField Type: RW
BitField Desc: Force AIS STS Mode 0: One shot ( N events in T the unit time) 1:
continous (aisstserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_AISstscfg_aisstsfrc_mod_Mask                                                              cBit0
#define c_upen_AISstscfg_aisstsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS STS Error Status
Reg Addr   : 0x23260-0x2327F
Reg Formula: 0x23260 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force AIS STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_AISstssta                                                                            0x23260
#define cReg_upen_AISstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: aisstssta_almset
BitField Type: RW
BitField Desc: AIS STS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_AISstssta_aisstssta_almset_Mask                                                          cBit27
#define c_upen_AISstssta_aisstssta_almset_Shift                                                             27

/*--------------------------------------
BitField Name: aisstsstaclralrm
BitField Type: RW
BitField Desc: AIS STS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstaclralrm_Mask                                                       cBit22_15
#define c_upen_AISstssta_aisstsstaclralrm_Shift                                                             15

/*--------------------------------------
BitField Name: aisstsstasetalrm
BitField Type: RW
BitField Desc: AIS STS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstasetalrm_Mask                                                        cBit14_7
#define c_upen_AISstssta_aisstsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: aisstsstatimecnt
BitField Type: RW
BitField Desc: AIS STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstatimecnt_Mask                                                         cBit6_0
#define c_upen_AISstssta_aisstsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: aisstsstafrc_ena
BitField Type: RW
BitField Desc: AIS STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstafrc_ena_Mask                                                          cBit31
#define c_upen_AISstssta_aisstsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: aisstsstaerrstep
BitField Type: RW
BitField Desc: The Second AIS STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstaerrstep_Mask                                                       cBit30_23
#define c_upen_AISstssta_aisstsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: aisstsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask AIS STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_AISstssta_aisstsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: aisstsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error AIS STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_AISstssta_aisstsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: aisstsstafrc_sta
BitField Type: RW
BitField Desc: AIS STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_AISstssta_aisstsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: aisstsstaoneunit
BitField Type: RW
BitField Desc: Force AIS STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_AISstssta_aisstsstaoneunit_Mask                                                           cBit0
#define c_upen_AISstssta_aisstsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ STS Error Configuration
Reg Addr   : 0x23280-0x2329F
Reg Formula: 0x23280 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force UEQ STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_UEQstscfg                                                                            0x23280
#define cReg_upen_UEQstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: ueqststim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqststim_mod_Mask                                                          cBit27_26
#define c_upen_UEQstscfg_ueqststim_mod_Shift                                                                26

/*--------------------------------------
BitField Name: ueqstserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstserr_clr_Mask                                                          cBit25_18
#define c_upen_UEQstscfg_ueqstserr_clr_Shift                                                                18

/*--------------------------------------
BitField Name: ueqstserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstserr_set_Mask                                                          cBit17_10
#define c_upen_UEQstscfg_ueqstserr_set_Shift                                                                10

/*--------------------------------------
BitField Name: ueqstserr_step
BitField Type: RW
BitField Desc: The Total Step to Force UEQ STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstserr_step_Mask                                                           cBit9_2
#define c_upen_UEQstscfg_ueqstserr_step_Shift                                                                2

/*--------------------------------------
BitField Name: ueqstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for UEQ STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstsmsk_pos_Mask_01                                                       cBit31_18
#define c_upen_UEQstscfg_ueqstsmsk_pos_Shift_01                                                             18
#define c_upen_UEQstscfg_ueqstsmsk_pos_Mask_02                                                         cBit1_0
#define c_upen_UEQstscfg_ueqstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: ueqstsmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For UEQ STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstsmsk_ena_Mask                                                             cBit17
#define c_upen_UEQstscfg_ueqstsmsk_ena_Shift                                                                17

/*--------------------------------------
BitField Name: ueqstserr_num
BitField Type: RW
BitField Desc: The Total UEQ STS Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstserr_num_Mask                                                           cBit16_1
#define c_upen_UEQstscfg_ueqstserr_num_Shift                                                                 1

/*--------------------------------------
BitField Name: ueqstsfrc_mod
BitField Type: RW
BitField Desc: Force UEQ STS Mode 0: One shot ( N events in T the unit time) 1:
continous (ueqstserr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_UEQstscfg_ueqstsfrc_mod_Mask                                                              cBit0
#define c_upen_UEQstscfg_ueqstsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ STS Error Status
Reg Addr   : 0x232a0-0x232bF
Reg Formula: 0x232a0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force UEQ STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_UEQstssta                                                                            0x232a0
#define cReg_upen_UEQstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: aisstssta_almset
BitField Type: RW
BitField Desc: AIS STS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_UEQstssta_aisstssta_almset_Mask                                                          cBit27
#define c_upen_UEQstssta_aisstssta_almset_Shift                                                             27

/*--------------------------------------
BitField Name: aisstsstaclralrm
BitField Type: RW
BitField Desc: AIS STS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstaclralrm_Mask                                                       cBit22_15
#define c_upen_UEQstssta_aisstsstaclralrm_Shift                                                             15

/*--------------------------------------
BitField Name: aisstsstasetalrm
BitField Type: RW
BitField Desc: AIS STS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstasetalrm_Mask                                                        cBit14_7
#define c_upen_UEQstssta_aisstsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: aisstsstatimecnt
BitField Type: RW
BitField Desc: AIS STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstatimecnt_Mask                                                         cBit6_0
#define c_upen_UEQstssta_aisstsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: aisstsstafrc_ena
BitField Type: RW
BitField Desc: AIS STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstafrc_ena_Mask                                                          cBit31
#define c_upen_UEQstssta_aisstsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: aisstsstaerrstep
BitField Type: RW
BitField Desc: The Second AIS STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstaerrstep_Mask                                                       cBit30_23
#define c_upen_UEQstssta_aisstsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: aisstsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask AIS STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_UEQstssta_aisstsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: aisstsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error AIS STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_UEQstssta_aisstsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: aisstsstafrc_sta
BitField Type: RW
BitField Desc: AIS STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_UEQstssta_aisstsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: aisstsstaoneunit
BitField Type: RW
BitField Desc: Force AIS STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_UEQstssta_aisstsstaoneunit_Mask                                                           cBit0
#define c_upen_UEQstssta_aisstsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP STS Error Configuration
Reg Addr   : 0x232c0-0x232dF
Reg Formula: 0x232c0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force BIP STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_BIPstscfg                                                                            0x232c0
#define cReg_upen_BIPstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: bipststim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_BIPstscfg_bipststim_mod_Mask                                                          cBit13_12
#define c_upen_BIPstscfg_bipststim_mod_Shift                                                                12

/*--------------------------------------
BitField Name: bipstserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstserr_clr_Mask                                                             cBit11
#define c_upen_BIPstscfg_bipstserr_clr_Shift                                                                11

/*--------------------------------------
BitField Name: bipstserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstserr_set_Mask                                                             cBit10
#define c_upen_BIPstscfg_bipstserr_set_Shift                                                                10

/*--------------------------------------
BitField Name: bipstserr_step
BitField Type: RW
BitField Desc: The Total Step to Force BIP STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstserr_step_Mask                                                           cBit9_2
#define c_upen_BIPstscfg_bipstserr_step_Shift                                                                2

/*--------------------------------------
BitField Name: bipstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for BIP STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstsmsk_pos_Mask_01                                                       cBit31_18
#define c_upen_BIPstscfg_bipstsmsk_pos_Shift_01                                                             18
#define c_upen_BIPstscfg_bipstsmsk_pos_Mask_02                                                         cBit1_0
#define c_upen_BIPstscfg_bipstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: bipstsmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For BIP STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstsmsk_ena_Mask                                                             cBit17
#define c_upen_BIPstscfg_bipstsmsk_ena_Shift                                                                17

/*--------------------------------------
BitField Name: bipstserr_num
BitField Type: RW
BitField Desc: The Total BIP STS Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstserr_num_Mask                                                           cBit16_1
#define c_upen_BIPstscfg_bipstserr_num_Shift                                                                 1

/*--------------------------------------
BitField Name: bipstsfrc_mod
BitField Type: RW
BitField Desc: Force BIP STS Mode 0: One shot 1: continous (bipstserr_num = 16
bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPstscfg_bipstsfrc_mod_Mask                                                              cBit0
#define c_upen_BIPstscfg_bipstsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP STS Error Status
Reg Addr   : 0x232e0-0x232FF
Reg Formula: 0x232e0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force BIP STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_BIPstssta                                                                            0x232e0
#define cReg_upen_BIPstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: reistssta_almset
BitField Type: RW
BitField Desc: REI STS alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_BIPstssta_reistssta_almset_Mask                                                           cBit9
#define c_upen_BIPstssta_reistssta_almset_Shift                                                              9

/*--------------------------------------
BitField Name: reistsstaclralrm
BitField Type: RW
BitField Desc: REI STS clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstaclralrm_Mask                                                           cBit8
#define c_upen_BIPstssta_reistsstaclralrm_Shift                                                              8

/*--------------------------------------
BitField Name: reistsstasetalrm
BitField Type: RW
BitField Desc: REI STS Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstasetalrm_Mask                                                           cBit7
#define c_upen_BIPstssta_reistsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: reistsstatimecnt
BitField Type: RW
BitField Desc: REI STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstatimecnt_Mask                                                         cBit6_0
#define c_upen_BIPstssta_reistsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: reistsstafrc_ena
BitField Type: RW
BitField Desc: REI STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstafrc_ena_Mask                                                          cBit31
#define c_upen_BIPstssta_reistsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: reistsstaerrstep
BitField Type: RW
BitField Desc: The Second REI STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstaerrstep_Mask                                                       cBit30_23
#define c_upen_BIPstssta_reistsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: reistsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask REI STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_BIPstssta_reistsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: reistsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error REI STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_BIPstssta_reistsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: reistsstafrc_sta
BitField Type: RW
BitField Desc: REI STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_BIPstssta_reistsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: reistsstaoneunit
BitField Type: RW
BitField Desc: Force REI STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPstssta_reistsstaoneunit_Mask                                                           cBit0
#define c_upen_BIPstssta_reistsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force REI STS Error Configuration
Reg Addr   : 0x23300-0x2331F
Reg Formula: 0x23300 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force REI STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_REIstscfg                                                                            0x23300
#define cReg_upen_REIstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: bipststim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [45:44]
--------------------------------------*/
#define c_upen_REIstscfg_bipststim_mod_Mask                                                          cBit13_12
#define c_upen_REIstscfg_bipststim_mod_Shift                                                                12

/*--------------------------------------
BitField Name: bipstserr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [43]
--------------------------------------*/
#define c_upen_REIstscfg_bipstserr_clr_Mask                                                             cBit11
#define c_upen_REIstscfg_bipstserr_clr_Shift                                                                11

/*--------------------------------------
BitField Name: bipstserr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [42]
--------------------------------------*/
#define c_upen_REIstscfg_bipstserr_set_Mask                                                             cBit10
#define c_upen_REIstscfg_bipstserr_set_Shift                                                                10

/*--------------------------------------
BitField Name: bipstserr_step
BitField Type: RW
BitField Desc: The Total Step to Force BIP STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_REIstscfg_bipstserr_step_Mask                                                           cBit9_2
#define c_upen_REIstscfg_bipstserr_step_Shift                                                                2

/*--------------------------------------
BitField Name: bipstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for BIP STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_REIstscfg_bipstsmsk_pos_Mask_01                                                       cBit31_18
#define c_upen_REIstscfg_bipstsmsk_pos_Shift_01                                                             18
#define c_upen_REIstscfg_bipstsmsk_pos_Mask_02                                                         cBit1_0
#define c_upen_REIstscfg_bipstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: bipstsmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For BIP STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_REIstscfg_bipstsmsk_ena_Mask                                                             cBit17
#define c_upen_REIstscfg_bipstsmsk_ena_Shift                                                                17

/*--------------------------------------
BitField Name: bipstserr_num
BitField Type: RW
BitField Desc: The Total BIP STS Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_REIstscfg_bipstserr_num_Mask                                                           cBit16_1
#define c_upen_REIstscfg_bipstserr_num_Shift                                                                 1

/*--------------------------------------
BitField Name: bipstsfrc_mod
BitField Type: RW
BitField Desc: Force BIP STS Mode 0: One shot 1: continous (bipstserr_num = 16
bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIstscfg_bipstsfrc_mod_Mask                                                              cBit0
#define c_upen_REIstscfg_bipstsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force REI STS Error Status
Reg Addr   : 0x23320-0x2333F
Reg Formula: 0x23320 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force REI STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_REIstssta                                                                            0x23320
#define cReg_upen_REIstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: reistssta_almset
BitField Type: RW
BitField Desc: REISTS alarm set Status
BitField Bits: [41]
--------------------------------------*/
#define c_upen_REIstssta_reistssta_almset_Mask                                                           cBit9
#define c_upen_REIstssta_reistssta_almset_Shift                                                              9

/*--------------------------------------
BitField Name: reistsstaclralrm
BitField Type: RW
BitField Desc: REISTS clear alarm CounterStatus
BitField Bits: [40]
--------------------------------------*/
#define c_upen_REIstssta_reistsstaclralrm_Mask                                                           cBit8
#define c_upen_REIstssta_reistsstaclralrm_Shift                                                              8

/*--------------------------------------
BitField Name: reistsstasetalrm
BitField Type: RW
BitField Desc: REISTS Set alarm Counter Status
BitField Bits: [39]
--------------------------------------*/
#define c_upen_REIstssta_reistsstasetalrm_Mask                                                           cBit7
#define c_upen_REIstssta_reistsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: reistsstatimecnt
BitField Type: RW
BitField Desc: REISTS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_REIstssta_reistsstatimecnt_Mask                                                         cBit6_0
#define c_upen_REIstssta_reistsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: reistsstafrc_ena
BitField Type: RW
BitField Desc: REISTS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_REIstssta_reistsstafrc_ena_Mask                                                          cBit31
#define c_upen_REIstssta_reistsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: reistsstaerrstep
BitField Type: RW
BitField Desc: The Second REISTS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_REIstssta_reistsstaerrstep_Mask                                                       cBit30_23
#define c_upen_REIstssta_reistsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: reistsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask REISTS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_REIstssta_reistsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_REIstssta_reistsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: reistsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error REISTS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_REIstssta_reistsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_REIstssta_reistsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: reistsstafrc_sta
BitField Type: RW
BitField Desc: REISTS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_REIstssta_reistsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_REIstssta_reistsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: reistsstaoneunit
BitField Type: RW
BitField Desc: Force REISTS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIstssta_reistsstaoneunit_Mask                                                           cBit0
#define c_upen_REIstssta_reistsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI STS Error Configuration
Reg Addr   : 0x23340-0x2335F
Reg Formula: 0x23340 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force RDI STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_RDIstscfg                                                                            0x28280

/*--------------------------------------
BitField Name: rdiststype
BitField Type: RW
BitField Desc: RDI Type at high path 1000: E-RDI P Enable 0100: E-RDI S Enable
0010: E-RDI C Enable 0001: RDI Enable
BitField Bits: [63:60]
--------------------------------------*/
#define c_upen_RDIstscfg_rdiststype_Mask                                                             cBit31_28
#define c_upen_RDIstscfg_rdiststype_Shift                                                                   28

/*--------------------------------------
BitField Name: rdivttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivttim_mod_Mask                                                           cBit27_26
#define c_upen_RDIstscfg_rdivttim_mod_Shift                                                                 26

/*--------------------------------------
BitField Name: rdivterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivterr_clr_Mask                                                           cBit25_18
#define c_upen_RDIstscfg_rdivterr_clr_Shift                                                                 18

/*--------------------------------------
BitField Name: rdivterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivterr_set_Mask                                                           cBit17_10
#define c_upen_RDIstscfg_rdivterr_set_Shift                                                                 10

/*--------------------------------------
BitField Name: rdivterr_step
BitField Type: RW
BitField Desc: The Total Step to Force RDI VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivterr_step_Mask                                                            cBit9_2
#define c_upen_RDIstscfg_rdivterr_step_Shift                                                                 2

/*--------------------------------------
BitField Name: rdivtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for RDI VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivtmsk_pos_Mask_01                                                        cBit31_18
#define c_upen_RDIstscfg_rdivtmsk_pos_Shift_01                                                              18
#define c_upen_RDIstscfg_rdivtmsk_pos_Mask_02                                                          cBit1_0
#define c_upen_RDIstscfg_rdivtmsk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: rdivtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For RDI VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivtmsk_ena_Mask                                                              cBit17
#define c_upen_RDIstscfg_rdivtmsk_ena_Shift                                                                 17

/*--------------------------------------
BitField Name: rdivterr_num
BitField Type: RW
BitField Desc: The Total RDI VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivterr_num_Mask                                                            cBit16_1
#define c_upen_RDIstscfg_rdivterr_num_Shift                                                                  1

/*--------------------------------------
BitField Name: rdivtfrc_mod
BitField Type: RW
BitField Desc: Force RDI VT Mode 0: One shot ( N events in T the unit time) 1:
continous (rdivterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RDIstscfg_rdivtfrc_mod_Mask                                                               cBit0
#define c_upen_RDIstscfg_rdivtfrc_mod_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI STS Error Status
Reg Addr   : 0x23360-0x2337F
Reg Formula: 0x23360 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force RDI STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_RDIstssta                                                                            0x23360
#define cReg_upen_RDIstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: rdistssta_almset
BitField Type: RW
BitField Desc: RDI STS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_RDIstssta_rdistssta_almset_Mask                                                          cBit27
#define c_upen_RDIstssta_rdistssta_almset_Shift                                                             27

/*--------------------------------------
BitField Name: rdistsstaclralrm
BitField Type: RW
BitField Desc: RDI STS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstaclralrm_Mask                                                       cBit22_15
#define c_upen_RDIstssta_rdistsstaclralrm_Shift                                                             15

/*--------------------------------------
BitField Name: rdistsstasetalrm
BitField Type: RW
BitField Desc: RDI STS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstasetalrm_Mask                                                        cBit14_7
#define c_upen_RDIstssta_rdistsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: rdistsstatimecnt
BitField Type: RW
BitField Desc: RDI STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstatimecnt_Mask                                                         cBit6_0
#define c_upen_RDIstssta_rdistsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: rdistsstafrc_ena
BitField Type: RW
BitField Desc: RDI STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstafrc_ena_Mask                                                          cBit31
#define c_upen_RDIstssta_rdistsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: rdistsstaerrstep
BitField Type: RW
BitField Desc: The Second RDI STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstaerrstep_Mask                                                       cBit30_23
#define c_upen_RDIstssta_rdistsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: rdistsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask RDI STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_RDIstssta_rdistsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: rdistsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error RDI STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_RDIstssta_rdistsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: rdistsstafrc_sta
BitField Type: RW
BitField Desc: RDI STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_RDIstssta_rdistsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: rdistsstaoneunit
BitField Type: RW
BitField Desc: Force RDI STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RDIstssta_rdistsstaoneunit_Mask                                                           cBit0
#define c_upen_RDIstssta_rdistsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Force LOM STS Error Configuration
Reg Addr   : 0x23380-0x2335F
Reg Formula: 0x23380 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force LOM STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_LOMstscfg                                                                            0x23380
#define cReg_upen_LOMstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: lomvttim_mod
BitField Type: RW
BitField Desc: Timer mode 00: every valid 01: 1ms 10: 10ms 11:100ms
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvttim_mod_Mask                                                           cBit27_26
#define c_upen_LOMstscfg_lomvttim_mod_Shift                                                                 26

/*--------------------------------------
BitField Name: lomvterr_clr
BitField Type: RW
BitField Desc: Timer is clear
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvterr_clr_Mask                                                           cBit25_18
#define c_upen_LOMstscfg_lomvterr_clr_Shift                                                                 18

/*--------------------------------------
BitField Name: lomvterr_set
BitField Type: RW
BitField Desc: Timer is set 0: Force Error x: Force Alarm
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvterr_set_Mask                                                           cBit17_10
#define c_upen_LOMstscfg_lomvterr_set_Shift                                                                 10

/*--------------------------------------
BitField Name: lomvterr_step
BitField Type: RW
BitField Desc: The Total Step to Force LOM VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvterr_step_Mask                                                            cBit9_2
#define c_upen_LOMstscfg_lomvterr_step_Shift                                                                 2

/*--------------------------------------
BitField Name: lomvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for LOM VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvtmsk_pos_Mask_01                                                        cBit31_18
#define c_upen_LOMstscfg_lomvtmsk_pos_Shift_01                                                              18
#define c_upen_LOMstscfg_lomvtmsk_pos_Mask_02                                                          cBit1_0
#define c_upen_LOMstscfg_lomvtmsk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: lomvtmsk_ena
BitField Type: RW
BitField Desc: The Data Mask For LOM VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvtmsk_ena_Mask                                                              cBit17
#define c_upen_LOMstscfg_lomvtmsk_ena_Shift                                                                 17

/*--------------------------------------
BitField Name: lomvterr_num
BitField Type: RW
BitField Desc: The Total LOM VT Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvterr_num_Mask                                                            cBit16_1
#define c_upen_LOMstscfg_lomvterr_num_Shift                                                                  1

/*--------------------------------------
BitField Name: lomvtfrc_mod
BitField Type: RW
BitField Desc: Force LOM VT Mode 0: One shot ( N events in T the unit time) 1:
continous (lomvterr_num = 16 bit FFFF)
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOMstscfg_lomvtfrc_mod_Mask                                                               cBit0
#define c_upen_LOMstscfg_lomvtfrc_mod_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force LOM STS Error Status
Reg Addr   : 0x23360-0x2337F
Reg Formula: 0x23360 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force LOM STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_LOMstssta                                                                            0x23360
#define cReg_upen_LOMstssta_WidthVal                                                                        64

/*--------------------------------------
BitField Name: lomstssta_almset
BitField Type: RW
BitField Desc: LOM STS alarm set Status
BitField Bits: [59]
--------------------------------------*/
#define c_upen_LOMstssta_lomstssta_almset_Mask                                                          cBit27
#define c_upen_LOMstssta_lomstssta_almset_Shift                                                             27

/*--------------------------------------
BitField Name: lomstsstaclralrm
BitField Type: RW
BitField Desc: LOM STS clear alarm CounterStatus
BitField Bits: [54:47]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstaclralrm_Mask                                                       cBit22_15
#define c_upen_LOMstssta_lomstsstaclralrm_Shift                                                             15

/*--------------------------------------
BitField Name: lomstsstasetalrm
BitField Type: RW
BitField Desc: LOM STS Set alarm Counter Status
BitField Bits: [46:39]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstasetalrm_Mask                                                        cBit14_7
#define c_upen_LOMstssta_lomstsstasetalrm_Shift                                                              7

/*--------------------------------------
BitField Name: lomstsstatimecnt
BitField Type: RW
BitField Desc: LOM STS Timer Status
BitField Bits: [38:32]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstatimecnt_Mask                                                         cBit6_0
#define c_upen_LOMstssta_lomstsstatimecnt_Shift                                                              0

/*--------------------------------------
BitField Name: lomstsstafrc_ena
BitField Type: RW
BitField Desc: LOM STS Force Enable Status
BitField Bits: [31]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstafrc_ena_Mask                                                          cBit31
#define c_upen_LOMstssta_lomstsstafrc_ena_Shift                                                             31

/*--------------------------------------
BitField Name: lomstsstaerrstep
BitField Type: RW
BitField Desc: The Second LOM STS Counter Status
BitField Bits: [30:23]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstaerrstep_Mask                                                       cBit30_23
#define c_upen_LOMstssta_lomstsstaerrstep_Shift                                                             23

/*--------------------------------------
BitField Name: lomstsstamsk_pos
BitField Type: RW
BitField Desc: The Position Mask LOM STS Counter Status
BitField Bits: [22:19]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstamsk_pos_Mask                                                       cBit22_19
#define c_upen_LOMstssta_lomstsstamsk_pos_Shift                                                             19

/*--------------------------------------
BitField Name: lomstsstaerr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error LOM STS Status
BitField Bits: [18:3]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstaerr_cnt_Mask                                                        cBit18_3
#define c_upen_LOMstssta_lomstsstaerr_cnt_Shift                                                              3

/*--------------------------------------
BitField Name: lomstsstafrc_sta
BitField Type: RW
BitField Desc: LOM STS State Machine
BitField Bits: [2:1]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstafrc_sta_Mask                                                         cBit2_1
#define c_upen_LOMstssta_lomstsstafrc_sta_Shift                                                              1

/*--------------------------------------
BitField Name: lomstsstaoneunit
BitField Type: RW
BitField Desc: Force LOM STS in One milisecond Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOMstssta_lomstsstaoneunit_Mask                                                           cBit0
#define c_upen_LOMstssta_lomstsstaoneunit_Shift                                                              0


/*------------------------------------------------------------------------------
Reg Name   : Configuration n second to force in STS
Reg Addr   : 0xa233e1-0xa233e1
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure n second in  STS

------------------------------------------------------------------------------*/
#define cReg_upennsecsts                                                                              0xa233e1

/*--------------------------------------
BitField Name: cfgnsecsts
BitField Type: RW
BitField Desc: The second number in STS Congiguration
BitField Bits: [31:0]
--------------------------------------*/
#define c_upennsecsts_cfgnsecsts_Mask                                                                 cBit31_0
#define c_upennsecsts_cfgnsecsts_Shift                                                                       0

#endif /* __REG_ATT_OCN_REG_H_ */

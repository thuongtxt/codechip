/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : Tha6A290011SdhVc1xAttController.c
 *
 * Created Date: Sep 9, 2017
 *
 * Description : VC1x ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha6A210031/sdh/Tha6A210031SdhAttControllerInternal.h"
#include "Tha6A290011SdhAttControllerInternal.h"
#include "Tha6A290011SdhAttAdjPtrReg.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011SdhVc1xAttController
    {
    tTha6A210031SdhVc1xAttController super;
    uint32          pointerAdjDuration; /* in ms */
    }tTha6A290011SdhVc1xAttController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tThaAttControllerMethods                m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportDataMask8bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtSdhPathCounterTypeBip)
        return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0013);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportDataMask2bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtSdhPathCounterTypeBip)
        return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportConvertStep(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0013);
    }

static uint32 StartVersionSupportPointerAdj(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0015);
    }

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x2, 0x2222);
    }

static uint32 RegPointerAdj(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return cAf6Reg_stsloadj;
    }

static uint8  SwPointerAdjToHw(Tha6A210031PdhDe3AttController self, uint32 pointerAdjType)
    {
    AtUnused(self);
    if (pointerAdjType==cAtSdhPathPointerAdjTypeIncrease)
        return 3;
    return 4;
    }

static uint32 PointerRealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwStsInSlice;
    uint32 vtgId = 0, vtId = 0;
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);

    if ((channelType == cAtSdhChannelTypeTu12) || (channelType == cAtSdhChannelTypeTu11))
        {
        vtId = AtChannelIdGet((AtChannel)channel);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(channel));
        }
    else if ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11))
        {
        AtSdhChannel tu1x = AtSdhChannelParentChannelGet(channel);
        vtId = AtChannelIdGet((AtChannel)tu1x);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(tu1x));
        }

    if (ThaSdhChannel2HwMasterStsId(channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        {
        return regAddr + 0x20UL*hwStsInSlice + slice * 0x2000UL + vtgId * 4UL + vtId + Tha60210011ModuleOcnBaseAddress(ocnModule);
        }

    return cInvalidUint32;
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportDataMask8bit);
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportDataMask2bit);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void OverrideTha6A210031PdhDe3AttController(Tha6A210031PdhDe3AttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(self), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportPointerAdj);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegPointerAdj);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PointerRealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, SwPointerAdjToHw);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportConvertStep);
        }

    mMethodsSet(self, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideThaAttController((ThaAttController) self);
    OverrideTha6A210031PdhDe3AttController((Tha6A210031PdhDe3AttController)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011SdhVc1xAttController);
    }

static AtAttController ObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031SdhVc1xAttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A290011SdhVc1xAttControllerNew(AtChannel sdhPath)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(controller, sdhPath);
    }

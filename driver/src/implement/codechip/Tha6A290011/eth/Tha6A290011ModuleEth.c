/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha6A290011ModuleEth.c
 *
 * Created Date: Aug 3, 2017
 *
 * Description : ETH module of 6A290011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/eth/Tha60290011ModuleEthInternal.h"
#include "../../../default/eth/ThaEthPort.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011ModuleEth
    {
    tTha60290011ModuleEth super;
    }tTha6A290011ModuleEth;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleEthMethods m_ThaModuleEthOverride;
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tAtModuleMethods     m_AtModuleOverride;

/* Save super implementations */
static const tThaModuleEthMethods *m_ThaModuleEthMethods = NULL;
static const tAtModuleEthMethods  *m_AtModuleEthMethods = NULL;
static const tAtModuleMethods     *m_AtModuleMethods    = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedMultipleLanes(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint16 SVlanDefaultTpid(AtModuleEth self)
    {
    AtUnused(self);
    return cAtModuleEthSVlanTpid;
    }

static eAtRet Init(AtModule self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, SVlanDefaultTpid);
        /*mMethodOverride(m_AtModuleEthOverride, PortCreate);*/
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(ThaModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleEthMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(self), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, NeedMultipleLanes);
        }

    mMethodsSet(self, &m_ThaModuleEthOverride);
    }

static void OverrideAtModule(AtModuleEth self)
    {
    AtModule module = (AtModule)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModule(self);
    OverrideAtModuleEth(self);
    OverrideThaModuleEth((ThaModuleEth) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011ModuleEth);
    }

static AtModuleEth ObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha6A290011ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

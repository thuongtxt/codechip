/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : DEMAP
 *
 * File        : Tha6A290011ModuleDeMap.c
 *
 * Created Date: Sep 18, 2017
 *
 * Description : Demap module of 6A290011
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/map/Tha60290011ModuleMapDemap.h"
#include "../../Tha60210031/map/Tha60210031ModuleMapDemap.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011ModuleDemap
    {
    tTha60290011ModuleMap super;
    }tTha6A290011ModuleDemap;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;
static tThaModuleDemapMethods       m_ThaModuleDemapOverride;

/* Save super implementation */
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;
static const tThaModuleDemapMethods       *m_ThaModuleDemapMethods       = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DmapFirstTsMask(ThaModuleDemap self)
    {
    if (Tha60210031ModuleDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit15;
    return m_ThaModuleDemapMethods->DmapFirstTsMask(self);
    }

static uint8 DmapFirstTsShift(ThaModuleDemap self)
    {
    if (Tha60210031ModuleDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 15;
    return m_ThaModuleDemapMethods->DmapFirstTsShift(self);
    }

static uint32 DmapChnTypeMask(ThaModuleDemap self)
    {
    if (Tha60210031ModuleDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit14_12;
    return m_ThaModuleDemapMethods->DmapChnTypeMask(self);
    }

static uint8 DmapChnTypeShift(ThaModuleDemap self)
    {
    if (Tha60210031ModuleDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 12;
    return m_ThaModuleDemapMethods->DmapChnTypeShift(self);
    }

static uint32 DmapTsEnMask(ThaModuleDemap self)
    {
    if (Tha60210031ModuleDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit11;
    return m_ThaModuleDemapMethods->DmapTsEnMask(self);
    }

static uint8 DmapTsEnShift(ThaModuleDemap self)
    {
    if (Tha60210031ModuleDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 11;
    return m_ThaModuleDemapMethods->DmapTsEnShift(self);
    }

static uint32 DmapPwIdMask(ThaModuleDemap self)
    {
    if (Tha60210031ModuleDemapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit10_0;
    return m_ThaModuleDemapMethods->DmapPwIdMask(self);
    }

/*static eBool NeedEnableAfterBindToPw(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return cAtTrue;
    }*/

static uint8 PwCepHwChannelType(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 7;
    }

static void OverrideThaModuleDemap(ThaModuleDemap self)
    {

    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleDemapMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDemapOverride, m_ThaModuleDemapMethods, sizeof(m_ThaModuleDemapOverride));

        /* Override register address */
        mMethodOverride(m_ThaModuleDemapOverride, DmapFirstTsMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapFirstTsShift);
        mMethodOverride(m_ThaModuleDemapOverride, DmapChnTypeMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapChnTypeShift);
        mMethodOverride(m_ThaModuleDemapOverride, DmapTsEnMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapTsEnShift);
        mMethodOverride(m_ThaModuleDemapOverride, DmapPwIdMask);
        }

    mMethodsSet(self, &m_ThaModuleDemapOverride);
    }
static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, PwCepHwChannelType);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleAbstractMap(self);
    OverrideThaModuleDemap((ThaModuleDemap) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011ModuleDemap);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A290011ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

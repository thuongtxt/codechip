/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60210031ModuleMap.c
 *
 * Created Date: Jun 18, 2015
 *
 * Description : MAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290011/map/Tha60290011ModuleMapDemap.h"
#include "../../Tha60210031/map/Tha60210031ModuleMapDemap.h"
/*--------------------------- Define -----------------------------------------*/
#define cThaMapTmDs1E1IdleEnableMask                 cBit28
#define cThaMapTmDs1E1IdleEnableShift                28
#define cThaMapTmDs1E1IdleCodeMask                   cBit27_20
#define cThaMapTmDs1E1IdleCodeShift                  20

#define cThaMapFirstTsMask     cBit14
#define cThaMapFirstTsShift    14

#define cThaMapChnTypeMask     cBit13_11
#define cThaMapChnTypeShift    11

#define cThaMapTsEnMask        cBit10
#define cThaMapTsEnShift       10

#define cThaMapPWIdFieldMask   cBit9_0
#define cThaMapPWIdFieldShift  0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011ModuleMap
    {
    tTha60290011ModuleMap super;
    }tTha6A290011ModuleMap;
/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleMapMethods         m_ThaModuleMapOverride;

/* Save Super Implementation */
static const tThaModuleMapMethods         *m_ThaModuleMapMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 MapChnTypeMask(ThaModuleMap self)
    {
    if (Tha60210031ModuleMapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit14_12;
    return m_ThaModuleMapMethods->MapChnTypeMask(self);
    }

static uint8  MapChnTypeShift(ThaModuleMap self)
    {
    if (Tha60210031ModuleMapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 12;
    return m_ThaModuleMapMethods->MapChnTypeShift(self);
    }

static uint32 MapFirstTsMask(ThaModuleMap self)
    {
    if (Tha60210031ModuleMapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit15;
    return m_ThaModuleMapMethods->MapFirstTsMask(self);
    }

static uint8  MapFirstTsShift(ThaModuleMap self)
    {
    if (Tha60210031ModuleMapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 15;
    return m_ThaModuleMapMethods->MapFirstTsShift(self);
    }

static uint32 MapTsEnMask(ThaModuleMap self)
    {
    if (Tha60210031ModuleMapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit11;
    return m_ThaModuleMapMethods->MapTsEnMask(self);
    }

static uint8  MapTsEnShift(ThaModuleMap self)
    {
    if (Tha60210031ModuleMapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 11;
    return m_ThaModuleMapMethods->MapTsEnShift(self);
    }

static uint32 MapPWIdFieldMask(ThaModuleMap self)
    {
    if (Tha60210031ModuleMapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return cBit10_0;
    return m_ThaModuleMapMethods->MapPWIdFieldMask(self);
    }

static uint8 MapPWIdFieldShift(ThaModuleMap self)
    {
    if (Tha60210031ModuleMapBitFieldsNeedShiftUp((ThaModuleAbstractMap)self))
        return 0;
    return m_ThaModuleMapMethods->MapPWIdFieldShift(self);
    }

static void OverrideThaModuleMap(ThaModuleMap self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleMapMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, mMethodsGet(self), sizeof(m_ThaModuleMapOverride));

        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapChnType)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapFirstTs)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTsEn)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapPWIdField)
        }

    mMethodsSet(self, &m_ThaModuleMapOverride);
    }

static void Override(ThaModuleMap self)
    {
    OverrideThaModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011ModuleMap);
    }

static AtModule Tha6A290011ModuleMapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Override */
    Override((ThaModuleMap)self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A290011ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290011ModuleMapObjectInit(newModule, device);
    }

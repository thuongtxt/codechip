/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CLA
 *
 * File        : Tha60290011ClaPwController.c
 *
 * Created Date: Dec 14, 2016
 *
 * Description : Concrete class of 60290011 CLA controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../Tha60290011/cla/controllers/Tha60290011ClaPwControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011ClaPwController
    {
    tTha60290011ClaPwController super;
    }tTha6A290011ClaPwController;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaPwControllerMethods m_ThaClaPwControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eAtRet PwLookupRemove(ThaClaPwController self, AtPw pwAdapter, eBool applyHardware)
    {
    AtUnused(self);
    AtUnused(pwAdapter);
    AtUnused(applyHardware);

    return cAtOk;
    }

static void OverrideThaClaPwController(ThaClaPwController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaPwControllerOverride, mMethodsGet(self), sizeof(m_ThaClaPwControllerOverride));
        mMethodOverride(m_ThaClaPwControllerOverride, PwLookupRemove);
        }
    mMethodsSet(self, &m_ThaClaPwControllerOverride);
    }

static void Override(ThaClaPwController self)
    {
    OverrideThaClaPwController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011ClaPwController);
    }

static ThaClaPwController ObjectInit(ThaClaPwController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ClaPwControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaPwController Tha6A290011ClaPwControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaPwController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newController, cla);
    }

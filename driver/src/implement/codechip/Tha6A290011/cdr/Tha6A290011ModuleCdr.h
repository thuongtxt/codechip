/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha6A290021ModuleCdr.h
 * 
 * Created Date: July 28, 2018
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha6A290011MODULECDR_H_
#define _Tha6A290011MODULECDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290011/cdr/Tha60290011ModuleCdrInternal.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290011ModuleCdr
    {
    tTha60290011ModuleCdr super;
    }tTha6A290011ModuleCdr;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha6A290011ModuleCdrObjectInit(AtModule self, AtDevice device);
AtModule Tha6A290011ModuleCdrNew(AtDevice device);
ThaCdrController Tha6A290011VcDe3CdrControllerNew(uint32 engineId, AtChannel channel);
ThaCdrController Tha6A290011HoVcCdrControllerNew(uint32 engineId, AtChannel channel);

#endif /* _Tha6A290021MODULECDR_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : ThaDe2De1CdrControllerInternal.h
 * 
 * Created Date: July 05, 2018
 *
 * Description : DE2's DE1 CDR controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011DE2DE1CDRCONTROLLERINTERNAL_H_
#define _THA6A290011DE2DE1CDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cdr/controllers/ThaDe2De1CdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290011De2De1CdrController * Tha6A290011De2De1CdrController;

typedef struct tTha6A290011De2De1CdrController
    {
    tThaDe2De1CdrController super;
    }tTha6A290011De2De1CdrController;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController Tha6A290011De2De1CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);
ThaCdrController Tha6A290011De2De1CdrControllerNew(uint32 engineId, AtChannel channel);
#ifdef __cplusplus
}
#endif
#endif /* _THA6A290011DE2DE1CDRCONTROLLERINTERNAL_H_ */


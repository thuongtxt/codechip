/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : Tha60290021ModuleCdr.c
 *
 * Created Date: Apr 24, 2015
 *
 * Description : CDR
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A290011ModuleCdr.h"

#include "Tha6A290011De2De1CdrControllerInternal.h"
#include "Tha6A290011Vc1xCdrControllerInternal.h"
#include "Tha6A290011VcDe1CdrControllerInternal.h"
#include "Tha6A290011VcDe3CdrControllerInternal.h"
#include "../../../default/cdr/ThaModuleCdrStm.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleCdrMethods    m_ThaModuleCdrOverride;

/* Save super implementation */
static const tThaModuleCdrMethods    *m_ThaModuleCdrMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static ThaCdrController VcDe3CdrControllerCreate(ThaModuleCdr self, AtPdhDe3 de3)
    {
    uint8 slice = 0, sts = 0;
    uint32 engineId;

    ThaPdhChannelHwIdGet((AtPdhChannel)de3, cThaModuleCdr, &slice, &sts);
    engineId = (sts * 32UL);
    AtUnused(self);

    return Tha6A290011VcDe3CdrControllerNew(engineId, (AtChannel)de3);
    }

static ThaCdrController HoVcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    return Tha6A290011HoVcCdrControllerNew(ThaModuleCdrStmEngineIdOfHoVc(self, vc), (AtChannel)vc);
    }

static ThaCdrController Tu3VcCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    AtUnused(self);
    AtUnused(vc);
    return NULL;
    }

static ThaCdrController Vc1xCdrControllerCreate(ThaModuleCdr self, AtSdhVc vc)
    {
    return Tha6A290011Vc1xCdrControllerNew(ThaModuleCdrStmEngineIdOfVc1x(self, vc), (AtChannel)vc);
    }

static  ThaCdrController VcDe1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
    return Tha6A290011VcDe1CdrControllerNew(ThaModuleCdrStmEngineIdOfDe1(self, de1), (AtChannel)de1);
    }

static  ThaCdrController De2De1CdrControllerCreate(ThaModuleCdr self, AtPdhDe1 de1)
    {
    uint32 engineId = ThaPdhDe1FlatId((ThaPdhDe1)de1);
    AtUnused(self);
    return Tha6A290011De2De1CdrControllerNew(engineId, (AtChannel)de1);
    }

static void OverrideThaModuleCdr(AtModule self)
    {
    ThaModuleCdr cdrModule = (ThaModuleCdr)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleCdrMethods = mMethodsGet(cdrModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleCdrOverride, m_ThaModuleCdrMethods, sizeof(m_ThaModuleCdrOverride));

        mMethodOverride(m_ThaModuleCdrOverride, HoVcCdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, Tu3VcCdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, Vc1xCdrControllerCreate);

        mMethodOverride(m_ThaModuleCdrOverride, VcDe3CdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, De2De1CdrControllerCreate);
        mMethodOverride(m_ThaModuleCdrOverride, VcDe1CdrControllerCreate);
        }

    mMethodsSet(cdrModule, &m_ThaModuleCdrOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleCdr(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011ModuleCdr);
    }

AtModule Tha6A290011ModuleCdrObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleCdrObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A290011ModuleCdrNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290011ModuleCdrObjectInit(newModule, device);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha6A290011VcDe3CdrControllerInternal.h
 * 
 * Created Date: July 5, 2018
 *
 * Description : 6A290021 VC DE3 CDR controller internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011VCDE3CDRCONTROLLERINTERNAL_H_
#define _THA6A290011VCDE3CDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cdr/controllers/ThaVcDe3CdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A290011VcDe3CdrController * Tha6A290011VcDe3CdrController;
typedef struct tTha6A290011VcDe3CdrController
    {
    tThaVcDe3CdrController super;

    /* Private */
    }tTha6A290011VcDe3CdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController Tha6A290011VcDe3CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);
ThaCdrController Tha6A290011VcDe3CdrControllerNew(uint32 engineId, AtChannel channel);

#endif /* _THA6A290011VCDE3CDRCONTROLLERINTERNAL_H_ */


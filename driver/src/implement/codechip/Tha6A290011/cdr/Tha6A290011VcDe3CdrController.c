/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : CDR
 *
 * File        : ThaVcDe3CdrController.c
 *
 * Created Date: July 05, 2018
 *
 * Description : CDR controller for DE3 that is mapped to VC-3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "Tha6A290011VcDe3CdrControllerInternal.h"
/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha6A290011VcDe3CdrController)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaCdrControllerMethods           m_ThaCdrControllerOverride;
static tThaSdhChannelCdrControllerMethods m_ThaSdhChannelCdrControllerOverride;

/* Save super implementation */
static const tThaSdhChannelCdrControllerMethods          *m_ThaSdhChannelCdrControllerMethods          = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPw AcrDcrTimingSource(ThaCdrController self)
    {
    AtUnused(self);
    return NULL;
    }

static AtPw CurrentAcrDcrTimingSource(ThaCdrController self)
    {
    AtUnused(self);
    return NULL;
    }

static eBool FreeRunningIsSupported(ThaCdrController self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static AtSdhChannel SdhChannel(ThaSdhChannelCdrController self, AtChannel channel)
	{
	AtSdhChannel sdhChannel = m_ThaSdhChannelCdrControllerMethods->SdhChannel(self, channel); /* call super to get sdh channel, this would be faceplate VC */
	if (sdhChannel)
		{
		AtModule sdhModule = (AtModule)AtChannelModuleGet((AtChannel)sdhChannel);
		if (AtModuleTypeGet(sdhModule) == cAtModuleSdh)
			{
			if (Tha60290021ModuleSdhLineIsFaceplate((AtModuleSdh)sdhModule, AtSdhChannelLineGet(sdhChannel)))
				{
				AtSdhChannel srcChannel = (AtSdhChannel)AtChannelSourceGet((AtChannel)sdhChannel);
				if (srcChannel)
					return srcChannel;
				}
			}
		}
	return sdhChannel;
	}

static void OverrideThaSdhChannelCdrController(ThaSdhChannelCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaSdhChannelCdrControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaSdhChannelCdrControllerOverride, m_ThaSdhChannelCdrControllerMethods, sizeof(m_ThaSdhChannelCdrControllerOverride));

        mMethodOverride(m_ThaSdhChannelCdrControllerOverride, SdhChannel);

        }

    mMethodsSet(self, &m_ThaSdhChannelCdrControllerOverride);
    }

static void OverrideThaCdrController(ThaCdrController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaCdrControllerOverride, mMethodsGet(self), sizeof(m_ThaCdrControllerOverride));

        mMethodOverride(m_ThaCdrControllerOverride, FreeRunningIsSupported);
        mMethodOverride(m_ThaCdrControllerOverride, CurrentAcrDcrTimingSource);
        mMethodOverride(m_ThaCdrControllerOverride, AcrDcrTimingSource);

        }

    mMethodsSet(self, &m_ThaCdrControllerOverride);
    }

static void Override(ThaCdrController self)
    {
    OverrideThaCdrController(self);
    OverrideThaSdhChannelCdrController((ThaSdhChannelCdrController)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011VcDe3CdrController);
    }

ThaCdrController Tha6A290011VcDe3CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaVcDe3CdrControllerObjectInit(self, engineId, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaCdrController Tha6A290011VcDe3CdrControllerNew(uint32 engineId, AtChannel channel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaCdrController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha6A290011VcDe3CdrControllerObjectInit(newController, engineId, channel);
    }


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PWE
 * 
 * File        : Tha6A290011ModulePwe.h
 * 
 * Created Date: Nov 6, 2015
 *
 * Description : 6A290011 PWE interface
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A290011MODULEPWE_H_
#define _THA6A290011MODULEPWE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290011/pwe/Tha60290011ModulePweInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha6A290011ModulePweNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A290011MODULEPWE_H_ */


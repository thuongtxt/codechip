/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha6A290021ModulePw.c
 *
 * Created Date: Sep 9, 2015
 *
 * Description : Pseudowire module of 6A290021
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha6A210031/pw/Tha6A210031ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011ModulePw
    {
    tTha6A210031ModulePw super;
    }tTha6A290011ModulePw;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModulePwMethods m_ThaModulePwOverride;
static tAtModulePwMethods  m_AtModulePwOverride;
static tTha60210031ModulePwMethods m_Tha60210031ModulePwOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionHas1344PwsPerSts24Slice(Tha60210031ModulePw self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x2, 0x8, 0x0014);
    }

static eBool NeedClearDebug(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool NeedDoReConfig(ThaModulePw self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 MaxPwsGet(AtModulePw self)
    {
    AtUnused(self);
    return 4096;
    }

static AtPw PwKBytePwGet(ThaModulePw self)
    {
    AtUnused(self);
    return NULL;
    }

static eAtRet ResetPw(ThaModulePw self, AtPw pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return cAtOk;
    }

static eBool DynamicPwAllocation(ThaModulePw self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static void OverrideTha60210031ModulePw(Tha60210031ModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210031ModulePwOverride, mMethodsGet(self), sizeof(m_Tha60210031ModulePwOverride));

        mMethodOverride(m_Tha60210031ModulePwOverride, StartVersionHas1344PwsPerSts24Slice);
        }

    mMethodsSet(self, &m_Tha60210031ModulePwOverride);
    }

static void OverrideThaModulePw(ThaModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePwOverride, mMethodsGet(self), sizeof(m_ThaModulePwOverride));

        mMethodOverride(m_ThaModulePwOverride, ResetPw);
        mMethodOverride(m_ThaModulePwOverride, PwKBytePwGet);
        mMethodOverride(m_ThaModulePwOverride, NeedDoReConfig);
        mMethodOverride(m_ThaModulePwOverride, DynamicPwAllocation);
        mMethodOverride(m_ThaModulePwOverride, NeedClearDebug);
        }

    mMethodsSet(self, &m_ThaModulePwOverride);
    }

static void OverrideAtModulePw(AtModulePw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePwOverride, mMethodsGet(self), sizeof(m_AtModulePwOverride));

        mMethodOverride(m_AtModulePwOverride, MaxPwsGet);
        }

    mMethodsSet(self, &m_AtModulePwOverride);
    }

static void Override(AtModulePw self)
    {
    OverrideAtModulePw(self);
    OverrideThaModulePw((ThaModulePw)self);
    OverrideTha60210031ModulePw((Tha60210031ModulePw) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011ModulePw);
    }

static AtModulePw ObjectInit(AtModulePw self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031ModulePwObjectInit((AtModulePw)self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePw Tha6A290011ModulePwNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePw newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha6A290011ModuleOcn.c
 *
 * Created Date: Aug 2, 2016
 *
 * Description : OCN source code
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60290011/ocn/Tha60290011ModuleOcnInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A290011ModuleOcn
    {
    tTha60290011ModuleOcn super;
    }tTha6A290011ModuleOcn;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleOcnMethods         m_ThaModuleOcnOverride;
static tTha60210011ModuleOcnMethods m_Tha60210011ModuleOcnOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TxLoStsPayloadHwFormula(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(channel);
    return (32768UL * hwSlice) + hwSts;
    }

static uint32 TxHwStsDefaultOffset(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(channel);
    return (1024UL * hwSlice) + hwSts;
    }

static uint32 TxStsVtDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint8 hwSlice, hwSts;
    AtUnused(self);

    if (ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &hwSlice, &hwSts) == cAtOk)
        return Tha60210011ModuleOcnBaseAddress((ThaModuleOcn)self) + (32768UL * hwSlice) + (32UL * hwSts) + (4UL * vtgId) + vtId;

    return cBit31_0;
    }

static eAtRet SohOverEthInit(ThaModuleOcn self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideTha60210011ModuleOcn(Tha60210011ModuleOcn self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleOcnOverride, mMethodsGet(self), sizeof(m_Tha60210011ModuleOcnOverride));

        mMethodOverride(m_Tha60210011ModuleOcnOverride, TxHwStsDefaultOffset);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, TxLoStsPayloadHwFormula);
        }

    mMethodsSet(self, &m_Tha60210011ModuleOcnOverride);
    }

static void OverrideThaModuleOcn(AtModule self)
    {
    ThaModuleOcn ocnModule = (ThaModuleOcn) self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, mMethodsGet(ocnModule), sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, SohOverEthInit);
        mMethodOverride(m_ThaModuleOcnOverride, TxStsVtDefaultOffset);
        }

    mMethodsSet(ocnModule, &m_ThaModuleOcnOverride);
    }

static void Override(AtModule self)
    {
    OverrideThaModuleOcn(self);
    OverrideTha60210011ModuleOcn((Tha60210011ModuleOcn)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A290011ModuleOcn);
    }

static AtModule ObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60290011ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A290011ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newModule, device);
    }

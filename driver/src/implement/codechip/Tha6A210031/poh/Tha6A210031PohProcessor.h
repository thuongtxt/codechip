/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : POH
 * 
 * File        : Tha6A210031PohProcessor.h
 * 
 * Created Date: Jan 6, 2016
 *
 * Description : POH processor
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210031_POH_THA6A210031POHPROCESSOR_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210031_POH_THA6A210031POHPROCESSOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/poh/Tha60210031PohProcessorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPohProcessor Tha6A210031AuVcPohProcessorNew(AtSdhVc vc);
ThaPohProcessor Tha6A210031Vc1xPohProcessorNew(AtSdhVc vc);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210031_POH_THA6A210031POHPROCESSOR_H_ */


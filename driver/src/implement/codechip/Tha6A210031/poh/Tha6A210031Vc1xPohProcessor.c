/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : POH
 *
 * File        : Tha6A210031Vc1xPohProcessor.c
 *
 * Created Date: Jan 5, 2016
 *
 * Description : VC1x POH Processor implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60210031/poh/Tha60210031ModulePohReg.h"
#include "Tha6A210031PohProcessor.h"

/*--------------------------- Define -----------------------------------------*/
#define cRegK4V5RdiForceMask      cBit19
#define cRegK4V5RdiForceShift     19

#define cRegK4V5RdiValueMask      cBit18_15
#define cRegK4V5RdiValueShift     15

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210031Vc1xPohProcessor
    {
    tTha60210031Vc1xPohProcessor super;
    }tTha6A210031Vc1xPohProcessor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods                    m_AtChannelOverride;
static tTha60210011AuVcPohProcessorMethods  m_Tha60210011AuVcPohProcessorOverride;

/* Save super implementation */
static const tAtChannelMethods                   *m_AtChannelMethods    = NULL;
static const tTha60210011AuVcPohProcessorMethods * m_Tha60210011AuVcPohProcessorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsErdiType(uint32 alarmType)
    {
    if ((alarmType == cAtSdhPathAlarmErdiC) ||
        (alarmType == cAtSdhPathAlarmErdiP) ||
        (alarmType == cAtSdhPathAlarmErdiS))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsRdiType(uint32 alarmType)
    {
    if (IsErdiType(alarmType))
        return cAtTrue;

    return (alarmType == cAtSdhPathAlarmRdi) ? cAtTrue : cAtFalse;
    }

static uint8 K4V5RdiValue(uint32 alarmType)
    {
    if (alarmType == cAtSdhPathAlarmRdi)
        return cBit3;

    if (alarmType == cAtSdhPathAlarmErdiC)
        return cBit3_1;

    if (alarmType == cAtSdhPathAlarmErdiP)
        return cBit1;

    if (alarmType == cAtSdhPathAlarmErdiS)
        return cBit3_2 | cBit0;

    return 0;
    }

static eAtSdhPathAlarmType K4V5RdiValueToRdiAlarm(uint8 g1RdiVal)
    {
    if (g1RdiVal == cBit3)
        return cAtSdhPathAlarmRdi;

    if (g1RdiVal == cBit3_1)
        return cAtSdhPathAlarmErdiC;

    if (g1RdiVal == cBit1)
        return cAtSdhPathAlarmErdiP;

    if (g1RdiVal == (cBit3_2 | cBit0))
        return cAtSdhPathAlarmErdiS;

    return 0;
    }

static eAtRet HwTxAlarmForce(Tha60210011AuVcPohProcessor self, uint32 alarmType, eBool enable)
    {
    uint32 regAddr, regVal;

    if (IsRdiType(alarmType) == cAtFalse)
        return m_Tha60210011AuVcPohProcessorMethods->HwTxAlarmForce(self, alarmType, enable);

    if (AtSdhPathERdiIsEnabled((AtSdhPath)self) && (alarmType == cAtSdhPathAlarmRdi))
        return cAtErrorModeNotSupport;

    if ((AtSdhPathERdiIsEnabled((AtSdhPath)self) == cAtFalse) && (IsErdiType(alarmType)))
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(self)->TerminateInsertControlRegAddr(self) +
              mMethodsGet(self)->TerminateInsertControlRegOffset(self);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    mRegFieldSet(regVal, cRegK4V5RdiValue, K4V5RdiValue(alarmType));
    mRegFieldSet(regVal, cRegK4V5RdiForce, mBoolToBin(enable));
    mChannelHwWrite(self, regAddr, regVal, cThaModulePoh);

    return cAtOk;
    }

static uint32 TxForcedAlarmGet(AtChannel self)
    {
    uint32 regAddr, regVal;
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    uint32 forcedAlarm = m_AtChannelMethods->TxForcedAlarmGet(self);

    regAddr = mMethodsGet(processor)->TerminateInsertControlRegAddr(processor) +
              mMethodsGet(processor)->TerminateInsertControlRegOffset(processor);
    regVal  = mChannelHwRead(self, regAddr, cThaModulePoh);
    if (mRegField(regVal, cRegK4V5RdiForce) == 0)
        return forcedAlarm;

    return forcedAlarm | K4V5RdiValueToRdiAlarm((uint8)mRegField(regVal, cRegK4V5RdiValue));
    }

static void OverrideAtChannel(ThaPohProcessor self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);

        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));
        mMethodOverride(m_AtChannelOverride, TxForcedAlarmGet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static void OverrideTha60210011AuVcPohProcessor(ThaPohProcessor self)
    {
    Tha60210011AuVcPohProcessor processor = (Tha60210011AuVcPohProcessor)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011AuVcPohProcessorMethods = mMethodsGet(processor);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011AuVcPohProcessorOverride, m_Tha60210011AuVcPohProcessorMethods, sizeof(m_Tha60210011AuVcPohProcessorOverride));

        mMethodOverride(m_Tha60210011AuVcPohProcessorOverride, HwTxAlarmForce);
        }

    mMethodsSet(processor, &m_Tha60210011AuVcPohProcessorOverride);
    }

static void Override(ThaPohProcessor self)
    {
    OverrideAtChannel(self);
    OverrideTha60210011AuVcPohProcessor(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031Vc1xPohProcessor);
    }

static ThaPohProcessor Tha6A210031Vc1xPohProcessorObjectInit(ThaPohProcessor self, AtSdhVc vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031Vc1xPohProcessorObjectInit(self, vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPohProcessor Tha6A210031Vc1xPohProcessorNew(AtSdhVc vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPohProcessor newProcessor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A210031Vc1xPohProcessorObjectInit(newProcessor, vc);
    }

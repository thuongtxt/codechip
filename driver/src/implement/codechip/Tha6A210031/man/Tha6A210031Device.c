/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAN
 *
 * File        : Tha6A210031Device.c
 *
 * Created Date: Oct 8, 2015
 *
 * Description : Tha60210031 Device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/man/Tha60210031Device.h"
#include "../../Tha61031031/prbs/Tha61031031ModulePrbs.h"
#include "../eth/Tha6A210031ModuleEth.h"
#include "../sur/Tha6A210031ModuleSur.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tAtObjectMethods  m_AtObjectOverride;
static tTha60150011DeviceMethods m_Tha60150011DeviceOverride;

/* Super implementations */
static const tAtDeviceMethods  *m_AtDeviceMethods  = NULL;
static const tAtObjectMethods  *m_AtObjectMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModulePrbs)     return (AtModule)Tha6A210031ModulePrbsNew(self);
    if (moduleId  == cAtModuleEth)      return (AtModule)Tha6A210031ModuleEthNew(self);
    if (moduleId  == cAtModulePw)       return (AtModule)Tha6A210031ModulePwNew(self);
    if (moduleId  == cAtModulePdh)      return (AtModule)Tha6A210031ModulePdhNew(self);
    if (moduleId  == cAtModuleSdh)      return (AtModule)Tha6A210031ModuleSdhNew(self);
    if (moduleId  == cAtModuleSur)      return (AtModule)Tha6A210031ModuleSurNew(self);

    if (phyModule  == cThaModulePoh)    return (AtModule)Tha6A210031ModulePohNew(self);
    /*if (phyModule  == cThaModuleOcn)    return (AtModule)Tha6A210031ModuleOcnNew(self);*/
    if (phyModule  == cThaModuleCdr)    return (AtModule)Tha6A210031ModuleCdrNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool  DdrCalibIsGood(AtDevice self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eAtRet QdrDiagnostic(AtDevice self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    uint32 _moduleId = (uint32)moduleId;

    AtUnused(self);
    switch (_moduleId)
        {
        case cThaModuleDemap: return cAtTrue;
        case cThaModuleMap:   return cAtTrue;
        case cThaModuleOcn:   return cAtTrue;
        case cThaModulePoh:   return cAtTrue;
        case cThaModuleCdr:   return cAtTrue;
        default:
            break;
        }

    switch (_moduleId)
        {
        case cAtModulePdh:    return cAtTrue;
        case cAtModuleSdh :   return cAtTrue;
        case cAtModulePw:     return cAtTrue;
        case cAtModulePrbs:   return cAtTrue;
        case cAtModuleEth:    return cAtTrue;
        case cAtModuleSur:    return cAtTrue;

        default:
            break;
        }

    return cAtFalse;
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModuleSdh,
                                                 cAtModulePdh,
                                                 cAtModulePrbs,
                                                 cAtModulePw,
                                                 cAtModuleSur,
                                                 cAtModuleEth};

    if (numModules)
        *numModules = mCount(supportedModules);

    AtUnused(self);
    return supportedModules;
    }

static eBool ShouldCheckHwResource(AtDevice self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static void OverrideTha60150011Device(Tha60150011Device self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60150011DeviceOverride, mMethodsGet(self), sizeof(m_Tha60150011DeviceOverride));
        mMethodOverride(m_Tha60150011DeviceOverride, QdrDiagnostic);
        mMethodOverride(m_Tha60150011DeviceOverride, DdrCalibIsGood);
        }

    mMethodsSet(self, &m_Tha60150011DeviceOverride);
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, m_AtDeviceMethods, sizeof(tAtDeviceMethods));

        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ShouldCheckHwResource);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static const char *ToString(AtObject self)
    {
    static char string[32];

    AtSprintf(string, "Device 6A210031: 0x%p", (void *)self);
    return string;
    }

static void OverrideAtObject(AtDevice self)
    {
    AtOsal osal = AtSharedDriverOsalGet();

    if (!m_methodsInit)
        {
        m_AtObjectMethods = mMethodsGet((AtObject)self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, ToString);
        }

    mMethodsGet((AtObject)self) = &m_AtObjectOverride;
    }

static void Override(AtDevice self)
    {
    OverrideAtObject(self);
    OverrideAtDevice(self);
    OverrideTha60150011Device((Tha60150011Device)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60210031Device);
    }

static AtDevice ObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (Tha60210031DeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha6A210031DeviceNew(AtDriver driver, uint32 productCode)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    return ObjectInit(newDevice, driver, productCode);
    }

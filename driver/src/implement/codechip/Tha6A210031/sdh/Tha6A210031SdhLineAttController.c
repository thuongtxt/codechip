/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A210031SdhLineAttController.c
 *
 * Created Date: Jul 6, 2016
 *
 * Description : SDH line ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhLine.h"
#include "../../../default/ocn/ThaModuleOcnInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "Tha6A210031SdhAttReg.h"
#include "Tha6A210031SdhAttControllerInternal.h"
#include "../../../default/att/ThaAttSdhManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((Tha6A210031SdhLineAttController)self)
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6A210031SdhLineAttControllerMethods m_methods;

/* Override */
static tAtAttControllerMethods                m_AtAttControllerOverride;
static tThaAttControllerMethods               m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;

static const tAtAttControllerMethods *m_AtAttControllerMethods = NULL;
static const tThaAttControllerMethods * m_ThaAttControllerMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 AlarmUnitToHwValue(Tha6A210031PdhDe3AttController self, uint32 alarmType, eAlarmUnit unit)
    {
    AtSdhLine      line = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);
    eAtSdhLineRate rate = AtSdhLineRateGet(line);

    AtUnused(alarmType);
    if (rate >= cAtSdhLineRateStm64)
        return (unit ==cAtAlarmUnit1ms)? 0x1C713: 0x38E1;
    return (unit ==cAtAlarmUnit1ms)? 0x12EBB: 0x25D;
    }

static eBool  IsB1(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType==cAtSdhLineCounterTypeB1)
        return cAtTrue;
    return cAtFalse;
    }

static eBool  UseAdjust(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType==cAtSdhLineCounterTypeB2)
        return cAtTrue;
    return cAtFalse;
    }
static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x9, 0x4000);
    }

static uint32 StartVersionSupportAlarmUnitConfig(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    }

static uint32 StartVersionSupportStep14bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    }

static uint32 StartVersionSupportTwoB2Force(Tha6A210031SdhLineAttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    }

static eBool  IsTwoB2Force(Tha6A210031SdhLineAttController self)
    {
    ThaVersionReader version = ThaAttControllerVersionReader((ThaAttController)self);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(version);
    AtUnused(self);

    if (hwVersion >= mMethodsGet(self)->StartVersionSupportTwoB2Force(self))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 RegAddressFromErrorType2nd(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cInvalidUint32;
    }

static uint32 StatusRegAddressFromErrorType2nd(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cInvalidUint32;
    }

static uint32 RegAddressFromErrorType3rd(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cInvalidUint32;
    }

static uint32 MinStep(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return 1;
    }

static float MaxErrorNum_1s(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    float NFramePerSecond = ThaAttControllerNFramePerSecond((ThaAttController) self);

    AtUnused(errorType);
    return NFramePerSecond;
    }

static float MaxErrorNum(Tha6A210031PdhDe3AttController self, uint32 errorType, uint32 T/*ms*/)
    {
    float NFramePerSecond = ThaAttControllerNFramePerSecond((ThaAttController) self);

    AtUnused(errorType);
    return (NFramePerSecond * (float)T)/1000;
    }

static uint32 MaxStep(ThaAttController self, uint32 isError, uint32 errorType, uint32 Nrate, uint32 NbitPerFrame)
    {
    AtUnused(self);
    if (isError == cThaAttForceError)
        {
        if (mMethodsGet(self)->IsDataMask8bit(self, errorType))
            {
            if (ThaAttControllerIsStep14bit(self, errorType))
                {
                if ((Nrate >=cAtNRate1E7 && NbitPerFrame == cAtNbitPerFrameStm4) ||
                    (Nrate > cAtNRate1E7)) /* 10e-8 and 10e-9 */
                    {
                    return 0x3FFF;   /* 0x16383 */
                    }
                }
            }
        }
    return 255;
    }

static uint32 ChannelSpeed(ThaAttController self)
    {
	AtSdhLine line = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);
	eAtSdhLineRate rate = AtSdhLineRateGet(line);
    if (rate != cAtSdhLineRateInvalid)
        {
    	switch ((uint32)rate)
			{
			case cAtSdhLineRateStm0:
				return 51840;   /* 51840/8 */
			case cAtSdhLineRateStm1:
				return 155520;  /* 155520/8 */
			case cAtSdhLineRateStm4:
				return 622080;  /* 622080/8 */
			case cAtSdhLineRateStm16:
				return 2488320; /* 2488320/8 */
			case cAtSdhLineRateStm64:
				return 9953280; /* 9953280/8 */
			default:
				return 51840;
			}
        }
    return 51840;
    }

static eBool NeedRemoveSoh(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    return errorType == cAtSdhLineCounterTypeB2 ? cAtTrue: cAtFalse;
    }

static uint32 MaxNumberofBytePerFrame(ThaAttController self, uint32 errorType)
    {
    AtSdhLine       line    = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);
    eAtSdhLineRate  rate    = AtSdhLineRateGet(line);

    /* Replace x3 by x9 */
    AtUnused(errorType);
    if (rate != cAtSdhLineRateInvalid)
        {
        switch ((uint32)rate)
            {
            case cAtSdhLineRateStm0:
                return 1;   /* 51840/8  | 3 rows *3 (columns)bytes  = 810 bytes   = 6480 bit | Time 1 frame = 125 microseconds */
            case cAtSdhLineRateStm1:
                return 3;   /* 155520/8 | 9 rows *3 (columns)bytes = 2430 bytes  = 19440 bit| Time 1 frame = 125 microseconds */
            case cAtSdhLineRateStm4:
                return 13;  /* 622080/8 | 36rows *3 (columns)bytes = 9720 bytes  = 77760 bit| Time 1 frame = 125 microseconds */
            case cAtSdhLineRateStm16:
                return 48; /* 2488320/8| 144 rows *3 */
            case cAtSdhLineRateStm64:
                return 192; /* 9953280/8| 576 rows *3*/
            default:
                return 192;
            }
        }
    return 192; /* 56 (Overhead) + 4704 (Payload)*/
    }

static uint32 NbitPerFrameSoh(ThaAttController self)
    {
    AtSdhLine       line    = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);
    eAtSdhLineRate  rate    = AtSdhLineRateGet(line);
    AtChannel       channel = AtAttControllerChannelGet((AtAttController)self);
    AtModule        module  = AtChannelModuleGet(channel);
    ThaAttSdhManager mngr    = (ThaAttSdhManager)AtModuleSdhAttManager((AtModuleSdh)module);
    uint8           col     = ThaAttSdhManagerSohColRemoveGet(mngr);
    /* Replace x3 by x9 */
    if (rate != cAtSdhLineRateInvalid)
        {
        switch ((uint32)rate)
            {
            case cAtSdhLineRateStm0:
                return 3*(uint32)col*8;   /* 51840/8  | 3 rows *3 (columns)bytes  = 810 bytes   = 6480 bit | Time 1 frame = 125 microseconds */
            case cAtSdhLineRateStm1:
                return 9*(uint32)col*8;   /* 155520/8 | 9 rows *3 (columns)bytes = 2430 bytes  = 19440 bit| Time 1 frame = 125 microseconds */
            case cAtSdhLineRateStm4:
                return 36*(uint32)col*8;  /* 622080/8 | 36rows *3 (columns)bytes = 9720 bytes  = 77760 bit| Time 1 frame = 125 microseconds */
            case cAtSdhLineRateStm16:
                return 144*(uint32)col*8; /* 2488320/8| 144 rows *3 */
            case cAtSdhLineRateStm64:
                return 576*(uint32)col*8; /* 9953280/8| 576 rows *3*/
            default:
                return 6480;
            }
        }
    return 6480; /* 56 (Overhead) + 4704 (Payload)*/
    }

static uint32 NbitPerFrame(ThaAttController self)
    {
    AtSdhLine line = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);

    eAtSdhLineRate rate = AtSdhLineRateGet(line);
    if (rate != cAtSdhLineRateInvalid)
        {
        switch ((uint32)rate)
            {
            case cAtSdhLineRateStm0:
                return cAtNbitPerFrameStm0;   /* 51840/8  | 9 rows *90 (columns)bytes  = 810 bytes   = 6480 bit | Time 1 frame = 125 microseconds */
            case cAtSdhLineRateStm1:
            	return cAtNbitPerFrameStm1;  /* 155520/8 | 9 rows *270 (columns)bytes = 2430 bytes  = 19440 bit| Time 1 frame = 125 microseconds */
            case cAtSdhLineRateStm4:
            	return cAtNbitPerFrameStm4;  /* 622080/8 | 36rows *270 (columns)bytes = 9720 bytes  = 77760 bit| Time 1 frame = 125 microseconds */
            case cAtSdhLineRateStm16:
            	return cAtNbitPerFrameStm16; /* 2488320/8 | 144 */
            case cAtSdhLineRateStm64:
            	return cAtNbitPerFrameStm64; /* 9953280/8| 576 */
            default:
                return cAtNbitPerFrameStm0;
            }
        }
    return cAtNbitPerFrameStm0; /* 56 (Overhead) + 4704 (Payload)*/
    }

static eBool ErrorIsFromAlarm(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtFalse;
    }

static uint32 HwStepMaskHo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(isError);
    if (isError == cThaAttForceAlarm)
        {
        return c_upen_B1bytcfg_cfgB1byterr_step_Mask;
        }
    else if (isError ==cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            {
            if (ThaAttControllerIsStep14bit((ThaAttController)self, errorType))
                return cBit22_9;

            return cBit16_9;
            }
        return c_upen_B1bytcfg_cfgB1byterr_step_Mask;
        }
    return cBit31_0;
    }

static uint32 HwStepMaskLo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(isError);
    return cBit31_0;
    }


static uint32 PositionMaskConfigurationMaskHo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError ==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_B1bytcfg_cfgB1bytmsk_pos_Mask_02;

        if (mMethodsGet(self)->AlarmIsFromError(self, errorType))
            return c_upen_B1bytcfg_cfgB1bytmsk_pos_Mask_02;
        return 0;
        }
    else if (isError ==cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit8_0;
        return c_upen_B1bytcfg_cfgB1bytmsk_pos_Mask_02;
        }
    else
        return c_upen_B1bytcfg_cfgB1bytmsk_pos_Mask_02;
    }

static uint32 PositionMaskConfigurationMaskLo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError ==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_B1bytcfg_cfgB1bytmsk_pos_Mask_01;

        if (mMethodsGet(self)->AlarmIsFromError(self, errorType))
            return c_upen_lofcfg_lof_msk_pos_Mask;
        return c_upen_loscfg_los_msk_pos_Mask;
        }
    else if (isError ==cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit31_25;
        return c_upen_B1bytcfg_cfgB1bytmsk_pos_Mask_01;
        }
    else
        return c_upen_B1bytcfg_cfgB1bytmsk_pos_Mask_01;
    }

static uint32 DataMaskConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError ==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_B1bytcfg_cfgB1bytmsk_dat_Mask;

        if (mMethodsGet(self)->AlarmIsFromError(self, errorType))
            return c_upen_lofcfg_lof_frc_ena_Mask;
        return c_upen_loscfg_los_frc_ena_Mask;
        }
    else if (isError ==cThaAttForceError)
    	{
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit24_17;
    	return c_upen_B1bytcfg_cfgB1bytmsk_dat_Mask;
    	}
    else
        return c_upen_B1bytcfg_cfgB1bytmsk_dat_Mask;
    }

static uint32 NumberOfErrorsMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError ==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_B1bytcfg_cfgB1byterr_num_Mask;

        if (mMethodsGet(self)->AlarmIsFromError(self, errorType))
            return c_upen_lofcfg_lof_evtin1s_Mask;
        return c_upen_loscfg_los_evtin1s_Mask;
        }

    return c_upen_B1bytcfg_cfgB1byterr_num_Mask;
    }

static uint32 DurationConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError ==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return 0;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_lofcfg_lof_sec_num_Mask;
        return c_upen_loscfg_los_sec_num_Mask;
        }
    return 0;
    }

static uint32 HwForceModeConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError ==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return c_upen_B1bytcfg_cfgB1bytfrc_1sen_Mask;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_lofcfg_lof_frc_mod_Mask;
        return c_upen_loscfg_los_frc_mod_Mask;
        }

    return c_upen_B1bytcfg_cfgB1bytfrc_1sen_Mask;
    }

static eBool  AlarmIsFromError(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if (alarmType==cAtSdhLineAlarmLof || alarmType==cAtSdhLineAlarmRdi)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 RegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhLineAlarmLos:
            return cReg_upen_loscfg_Base;
        case cAtSdhLineAlarmAis:
            return cReg_upen_laiscfg;
        case cAtSdhLineAlarmLof:
            return cReg_upen_lofcfg;
        case cAtSdhLineAlarmRdi:
            return cReg_upen_lrdicfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhLineAlarmLos:
            return cReg_upen_lossta_Base;
        case cAtSdhLineAlarmLof:
            return cReg_upen_lofsta;
        case cAtSdhLineAlarmAis:
            return cReg_upen_laissta;
        case cAtSdhLineAlarmRdi:
            return cReg_upen_lrdista;
        default:
            return cBit31_0;
        }
    }

static uint32 RealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);

    if (ThaSdhChannel2HwMasterStsId(channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        {
        if (regAddr == cReg_upen_cpu_idr_st)
            return regAddr + 0x400UL * slice + Tha60210011ModuleOcnBaseAddress(ocnModule);
        return regAddr + hwStsInSlice + 0x400UL * slice + Tha60210011ModuleOcnBaseAddress(ocnModule);
        }
    return cInvalidUint32;
    }

static uint8 AlarmTypeIndex(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhLineAlarmLos:
            return 0;
        case cAtSdhLineAlarmAis:
            return 1;
        case cAtSdhLineAlarmLof:
            return 2;
        case cAtSdhLineAlarmRdi:
            return 3;
        default:
            return 0;
        }
    }

static eAtRet CheckAlarmNumEvent(Tha6A210031PdhDe3AttController self, uint32 alarmType, uint32 numEvent)
	{
	AtUnused(self);
	AtUnused(alarmType);
	AtUnused(numEvent);
	return cAtOk;
	}

static eAtRet SpecificAlarmConfig(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtOk;
    }
static uint8 NumAlarmTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 4;
    }

static eAtRet InitErrorDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i = 0;
    eAtRet ret = cAtOk;
    static const uint32 cAtSdhLineCounterTypes[] = {cAtSdhLineCounterTypeB1, cAtSdhLineCounterTypeB2, cAtSdhLineCounterTypeRei};
    for (i = 0; i < mMethodsGet(self)->NumErrorTypes(self); i++)
        {
        if (AtAttControllerErrorForceIsSupported((AtAttController)self, cAtSdhLineCounterTypes[i]))
            {
            uint8 errorTypeIndex=0;
            ret |= AtAttControllerUnForceError((AtAttController)self, cAtSdhLineCounterTypes[i]);
            ret |= AtAttControllerForceErrorModeSet((AtAttController)self, cAtSdhLineCounterTypes[i],cAtAttForceErrorModeContinuous);
            errorTypeIndex = mMethodsGet(self)->ErrorTypeIndex(self, cAtSdhLineCounterTypes[i]);
            self->errorInfo[errorTypeIndex].errorStep = mMethodsGet(self)->MinStep(self, cAtSdhLineCounterTypes[i]);/*At the receiver, window to detect LOF is 3ms. Apply for the current design you must config step_cfg must >= 29 to avoid LOF set.*/
            ret |= AtAttControllerForceErrorNumErrorsSet((AtAttController)self, cAtSdhLineCounterTypes[i], 0);
            ret |= AtAttControllerForceErrorBitPositionSet((AtAttController)self, cAtSdhLineCounterTypes[i], cBit15_0);
            }
        }



    return ret;
    }

static const char* AlarmIndexToString(uint8 alarmIndex)
    {
    static const char* cAttSdhLineAlarmTypeStr[] = {"LOS","AIS","LOF","RDI"};
    if (alarmIndex >= mCount(cAttSdhLineAlarmTypeStr))
        return cAttSdhLineAlarmTypeStr[0];
    return cAttSdhLineAlarmTypeStr[alarmIndex];
    }

static eAtRet InitAlarmDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i = 0;
    eAtRet ret = cAtOk;
    static const uint32 cAtSdhLineAlarmTypes[] = {cAtSdhLineAlarmLos, cAtSdhLineAlarmAis, cAtSdhLineAlarmLof, cAtSdhLineAlarmRdi};
    for (i = 0; i < mMethodsGet(self)->NumAlarmTypes(self); i++)
            {
            if (AtAttControllerAlarmForceIsSupported((AtAttController)self, cAtSdhLineAlarmTypes[i]))
                {
                ret |= AtAttControllerUnForceAlarm((AtAttController)self, cAtSdhLineAlarmTypes[i]);
                ret |= AtAttControllerForceAlarmModeSet((AtAttController)self, cAtSdhLineAlarmTypes[i], cAtAttForceAlarmModeContinuous);
                ret |= AtAttControllerForceAlarmNumEventSet((AtAttController)self, cAtSdhLineAlarmTypes[i], 1);
                }
            }
    return ret;
    }

static uint32 RegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhLineErrorB1:
            return cReg_upen_B1bytcfg_Base;
        case cAtSdhLineErrorB2:
            return cReg_upen_B2bytcfg;
        case cAtSdhLineErrorRei:
            return cReg_upen_preicfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhLineErrorB1:
            return cReg_upen_B1bytsta;
        case cAtSdhLineErrorB2:
            return cReg_upen_B2bytsta;
        default:
            return cBit31_0;
        }
    }

static uint8 ErrorTypeIndex(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhLineErrorB1:
            return 0;
        case cAtSdhLineErrorB2:
            return 1;
        case cAtSdhLineCounterTypeRei:
            return 2;
        default:
            return 0;
        }
    }

static uint8 NumErrorTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 3;
    }

static eBool AlarmForceIsSupported(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhLineAlarmLos:
        case cAtSdhLineAlarmAis:
        case cAtSdhLineAlarmLof:
        case cAtSdhLineAlarmRdi:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eBool ErrorForceIsSupported(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhLineErrorB1:
        case cAtSdhLineErrorB2:
        case cAtSdhLineErrorRei:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint16 LongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    return m_AtAttControllerMethods->LongRead(self, regAddr, dataBuffer, bufferLen);
    }

static eAtRet CalculateStepAndPositionMaskForRate2ndB2(Tha6A210031SdhLineAttController self, uint32 errorType, uint32 Nrate,
                                                       uint16 *err_step,     uint16 *positionMask,     uint8 *frc_en_mask,
                                                       uint16 *err_step_2nd, uint16 *positionMask_2nd, uint8 *frc_en_mask_2nd,
                                                       uint16 *err_step_3rd, uint16 *positionMask_3rd, uint8 *frc_en_mask_3rd)
    {
    eBool      debug = cAtFalse;
    tRateInfo  array;
    uint8      msk[9]= { 0x0, 0x1,  0x3,    0x7,    0xF,    0x1F,   0x3F,   0x7F,   0xFF};
    eBool      local_NeedToRemoveSoh  = ThaAttControllerNeedRemoveSoh((ThaAttController)self, errorType);
    uint32     local_NbitPerFrameSoh  = local_NeedToRemoveSoh?ThaAttControllerNbitPerFrameSoh((ThaAttController)self):0;
    uint32     local_NbitPerFrame     = ThaAttControllerNbitPerFrame((ThaAttController)self) - local_NbitPerFrameSoh;
    uint32     local_MaxStep          = ThaAttControllerMaxStep((ThaAttController)self, cThaAttForceError,  errorType, Nrate, local_NbitPerFrame);
    uint32     s = 0, p=0, m=0, max_pos=16, s_2nd = 0, p_2nd=0, m_2nd=0, s_3rd = 0, p_3rd=0, m_3rd=0;
    uint32     MinPos = 1;
    double     A = (1 * 1.0f) / ((double)Nrate * 1.0f);
    double     ss = 0;
    double     tempMinVal = (double)999999999999*1.0f;

    s = 1; s_3rd = 1;
    p = 16;p_3rd = 16;
    m_3rd = 8;
    for (m=1; m<(8+1); m++)
        {
        for (s_2nd = 1; s_2nd < (local_MaxStep + 1); s_2nd++)
            {
            double T = (double)max_pos * (double)s * (double)s_2nd * (double)local_NbitPerFrame * 1.0f;
            if (Nrate == cAtNRate1E3 && ThaAttControllerNbitPerFrame((ThaAttController)self)==cAtNbitPerFrameStm1)
                {
                T = (double)max_pos * (double)s * (double)s_2nd * s_3rd * (double)local_NbitPerFrame * 1.0f;
                }
            for (p_2nd=MinPos; p_2nd<(max_pos+1); p_2nd++)
                {
                for (m_2nd=1; m_2nd<(8+1); m_2nd++)
                    {
                    double value = ((double)p     * (double)m         *  (double)s_2nd)/T +
                                   ((double)p_2nd * (double)m_2nd     *  (double)s)/T;
                    double diff  = (double)(fabs((double)(value-A)));
                    if (Nrate == cAtNRate1E3 && ThaAttControllerNbitPerFrame((ThaAttController)self)==cAtNbitPerFrameStm1)
                        {
                        value = ((double)p     * (double)m     *  (double)s_2nd *  (double)s_3rd)/T +
                                ((double)p_2nd * (double)m_2nd *  (double)s     *  (double)s_3rd)/T +
                                ((double)p_3rd * (double)m_3rd *  (double)s     *  (double)s_2nd)/T;
                        diff  = (double)(fabs((double)(value-A)));
                        }
                    if (diff < tempMinVal)
                        {
                        tempMinVal              = diff;
                        array.step              = (uint16)s;
                        array.positionMask      = (uint16)p;
                        array.frc_en_mask       = (uint8) m;
                        array.step_mini         = (uint16)s_2nd;
                        array.positionMask_mini = (uint16)p_2nd;
                        array.frc_en_mask_mini  = (uint8) m_2nd;
                        if (Nrate == cAtNRate1E3 && ThaAttControllerNbitPerFrame((ThaAttController)self)==cAtNbitPerFrameStm1)
                            {
                            array.step_3rd          = (uint16)s_3rd;
                            array.positionMask_3rd  = (uint16)p_3rd;
                            array.frc_en_mask_3rd   = (uint8) m_3rd;
                            }
                        else
                            {
                            array.step_3rd          = (uint16)0;
                            array.positionMask_3rd  = (uint16)0;
                            array.frc_en_mask_3rd   = (uint8) 0;
                            }
                        array.value             = value;
                        array.diff              = diff;
                        array.max_pos           = (uint8)max_pos;
                        if (debug)
                            AtPrintf("step    =%03d, PositionMask_1st=%02d, frc_en_mask_1st=%d,             \r\n"
                                     "step_2nd=%03d, PositionMask_2nd=%02d, frc_en_mask_2nd=%d, \r\n"
                                     "step_3rd=%03d, PositionMask_3rd=%02d, frc_en_mask_3rd=%d, \r\n"
                                     "A=%.12f value=%.12f, diff=%.12f                           \r\n",
                                     s,      p,     m,
                                     s_2nd , p_2nd, m_2nd,
                                     s_3rd , p_3rd, m_3rd,
                                     A,      value, diff);
                        }
                    }
                }
            }
        }

    *positionMask     = mMethodsGet((ThaAttController)self)->PositionMaskGet(array.max_pos, array.positionMask);
    *err_step         = array.step;
    *frc_en_mask      = msk[array.frc_en_mask];

    *positionMask_2nd = mMethodsGet((ThaAttController)self)->PositionMaskGet(array.max_pos, array.positionMask_mini);
    *err_step_2nd     = array.step_mini;
    *frc_en_mask_2nd  = msk[array.frc_en_mask_mini];

    *positionMask_3rd = mMethodsGet((ThaAttController)self)->PositionMaskGet(array.max_pos, array.positionMask_3rd);
    *err_step_3rd     = array.step_3rd;
    *frc_en_mask_3rd  = msk[array.frc_en_mask_3rd];

    ss =(double) fabs((double)(array.value - A))/A;

    if (debug || mMethodsGet((ThaAttController)self)->IsCheckRateShow((ThaAttController)self))
        AtPrintf("===>step_1st=%03d, PositionMask_1st=%02d, frc_en_mask_1st=%d, \r\n"
                 "    step_2nd=%03d, PositionMask_2nd=%02d, frc_en_mask_2nd=%d, \r\n"
                 "    step_3rd=%03d, PositionMask_3rd=%02d, frc_en_mask_3rd=%d, \r\n"
                 "    A=%.12f value=%.12f, diff=%.12f max_pos=%d ss=%.12f       \r\n",
                 array.step ,      array.positionMask,      array.frc_en_mask,
                 array.step_mini , array.positionMask_mini, array.frc_en_mask_mini,
                 array.step_3rd,   array.positionMask_3rd,  array.frc_en_mask_3rd,
                 A, array.value,   array.diff,              array.max_pos, ss);

    if (local_NbitPerFrame == cAtNbitPerFrameStm64)/* STM64*/
        {
        if (Nrate < cAtNRate1E5 && mMethodsGet((ThaAttController)self)->IsCheckRate((ThaAttController)self)) /*  Error for 1e-2,1e-3,1e-4 */
            return cAtErrorModeNotSupport;
        return cAtOk;
        }

    if (ss > 0.15)
        {
        if (! mMethodsGet((ThaAttController)self)->IsCheckRate((ThaAttController)self))
            return cAtOk;
        return cAtErrorModeNotSupport;
        }
    return cAtOk;
    }

static eBool Need2ndB2Force(Tha6A210031SdhLineAttController self, uint32 errorType, uint32 rate)
    {
    AtSdhLine line = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);
    eAtSdhLineRate line_rate = AtSdhLineRateGet(line);
    if (errorType == cAtSdhLineCounterTypeB2)
        {
        if ( (line_rate == cAtSdhLineRateStm1  &&  rate == cAtBerRate1E3) ||
             (line_rate == cAtSdhLineRateStm1  && rate == cAtBerRate1E4)  ||
             (line_rate == cAtSdhLineRateStm64 && rate == cAtBerRate1E5)  )
            {
            return cAtTrue;
            }
        }
    return cAtFalse;
    }

static eBool Need3rdB2Force(Tha6A210031SdhLineAttController self, uint32 errorType)
    {
    AtSdhLine line = (AtSdhLine)AtAttControllerChannelGet((AtAttController)self);
    eAtSdhLineRate line_rate = AtSdhLineRateGet(line);
    if (errorType == cAtSdhLineCounterTypeB2)
        {
        if ((line_rate == cAtSdhLineRateStm1))
            {
            return cAtTrue;
            }
        }
    return cAtFalse;
    }

static eAtRet HwForceErrorRate2ndB2(Tha6A210031SdhLineAttController self, uint32 errorType, uint32 rate)
    {
    eAtRet ret = cAtOk;

    if (AtAttControllerErrorForceIsSupported((AtAttController)self, errorType))
        {
        Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
        uint16 err_step       = 0, err_step_mini=0,    err_step_3rd      = 0;
        uint16 positionMask   = 0, positionMask_mini,  positionMask_3rd  = 0;
        uint8  errorDataMask  = 0, errorDataMask_mini, errorDataMask_3rd = 0;
        uint32 Nrate          = mMethodsGet((ThaAttController)self)->CalculateNRate((ThaAttController)self, rate);
        uint32 errorTypeIndex = mMethodsGet(att)->ErrorTypeIndex(att, errorType);
        ret = CalculateStepAndPositionMaskForRate2ndB2(self,           errorType,          Nrate,
                                                       &err_step,      &positionMask,      &errorDataMask,
                                                       &err_step_mini, &positionMask_mini, &errorDataMask_mini,
                                                       &err_step_3rd,  &positionMask_3rd,  &errorDataMask_3rd);
        if (ret != cAtOk)
            return ret;

        att->errorInfo[errorTypeIndex].errorPositionMask      = positionMask;
        att->errorInfo[errorTypeIndex].errorStep              = err_step;
        att->errorInfo[errorTypeIndex].errorDataMask          = errorDataMask;
        att->errorInfo[errorTypeIndex].errorMiniPositionMask  = positionMask_mini;
        att->errorInfo[errorTypeIndex].errorMiniStep          = err_step_mini;
        att->errorInfo[errorTypeIndex].errorMiniDataMask      = errorDataMask_mini;
        att->errorInfo[errorTypeIndex].error3rdPositionMask   = positionMask_3rd;
        att->errorInfo[errorTypeIndex].error3rdStep           = err_step_3rd;
        att->errorInfo[errorTypeIndex].error3rdDataMask       = errorDataMask_3rd;
        mMethodsGet(att)->ForceErrorHelper(att, errorType, cAtAttForceTypeContinuous);
        mMethodsGet(att)->ForceErrorHelper2nd(att, errorType, cAtAttForceTypeContinuous);
        if (Need3rdB2Force(self, errorType))
            mMethodsGet(att)->ForceErrorHelper3rd(att, errorType, cAtAttForceTypeContinuous);

        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

static eAtRet HwForceErrorRate(ThaAttController self, uint32 errorType, uint32 rate)
    {
    if (mMethodsGet(mThis(self))->IsTwoB2Force(mThis(self)))
        {
        eAtRet ret = cAtOk;
        if  (Need2ndB2Force(mThis(self), errorType, rate))
            ret |= HwForceErrorRate2ndB2(mThis(self), errorType, rate);
        else
            {
            Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
            ret |= m_ThaAttControllerMethods->HwForceErrorRate(self, errorType, rate);
            mMethodsGet(att)->ErrorForcing2ndDataMaskSet(att, errorType, 0);
            if (Need3rdB2Force(mThis(self), errorType))
                mMethodsGet(att)->ErrorForcing3rdDataMaskSet(att, errorType, 0);
            }
        return ret;
        }
    return m_ThaAttControllerMethods->HwForceErrorRate(self, errorType, rate);
    }

static eAtRet ForceErrorRate(AtAttController self, uint32 errorType, eAtBerRate rate)
    {
    /* Duration of N frames*/
    if (mMethodsGet(mThis(self))->IsTwoB2Force(mThis(self)))
        {
        eAtRet ret = cAtOk;
        if  (Need2ndB2Force(mThis(self), errorType, rate))
            {
            Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
            uint16 err_step           = 0, err_step_mini           = 0, err_step_3rd;
            uint16 positionMask       = 0, positionMask_mini       = 0, positionMask_3rd;
            uint8  errorCrcDs1EsfMask = 0, errorCrcDs1EsfMask_mini = 0, errorCrcDs1EsfMask_3rd;
            uint32 errorTypeIndex     = 0;
            uint32 Nrate              = 0;

            if (!AtAttControllerErrorForceIsSupported(self, errorType))
                return cAtErrorModeNotSupport;
            mMethodsGet(att)->ErrorForcingDataMaskSet(att, errorType, 0);
            mMethodsGet(att)->ErrorForcing2ndDataMaskSet(att, errorType, 0);
            errorTypeIndex  = mMethodsGet(att)->ErrorTypeIndex(att, errorType);
            att->errorRate[errorTypeIndex]  = rate;

            Nrate = mMethodsGet((ThaAttController)self)->CalculateNRate((ThaAttController)self, rate);
            ret |= CalculateStepAndPositionMaskForRate2ndB2(mThis(self),    errorType,          Nrate,
                                                            &err_step,      &positionMask,      &errorCrcDs1EsfMask,
                                                            &err_step_mini, &positionMask_mini, &errorCrcDs1EsfMask_mini,
                                                            &err_step_3rd,  &positionMask_3rd,  &errorCrcDs1EsfMask_3rd);
            return ret;
            }
        }
    return m_AtAttControllerMethods->ForceErrorRate(self, errorType, rate);
    }


static void MethodsInit(Tha6A210031SdhLineAttController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, StartVersionSupportTwoB2Force);
        mMethodOverride(m_methods, IsTwoB2Force);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttControllerMethods =  mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride,m_AtAttControllerMethods, sizeof(m_AtAttControllerOverride));


        mMethodOverride(m_AtAttControllerOverride, AlarmForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, ErrorForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, LongRead);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorRate);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static eAtRet DefaultSet(ThaAttController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaAttControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, m_ThaAttControllerMethods, sizeof(m_ThaAttControllerOverride));
        mMethodOverride(m_ThaAttControllerOverride, DefaultSet);
        mMethodOverride(m_ThaAttControllerOverride, NbitPerFrame);
        mMethodOverride(m_ThaAttControllerOverride, ChannelSpeed);
        mMethodOverride(m_ThaAttControllerOverride, MaxStep);
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportStep14bit);
        mMethodOverride(m_ThaAttControllerOverride, NeedRemoveSoh);
        mMethodOverride(m_ThaAttControllerOverride, NbitPerFrameSoh);
        mMethodOverride(m_ThaAttControllerOverride, UseAdjust);
        mMethodOverride(m_ThaAttControllerOverride, IsB1);
        mMethodOverride(m_ThaAttControllerOverride, MaxNumberofBytePerFrame);
        mMethodOverride(m_ThaAttControllerOverride, HwForceErrorRate);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void OverrideTha6A210031PdhDe3AttController(AtAttController self)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(controller), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwStepMaskHo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwStepMaskLo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PositionMaskConfigurationMaskHo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PositionMaskConfigurationMaskLo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, DataMaskConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumberOfErrorsMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, DurationConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwForceModeConfigurationMask);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIsFromError);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumAlarmTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIndexToString);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, InitAlarmDefault);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, SpecificAlarmConfig);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, CheckAlarmNumEvent);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmUnitToHwValue);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmUnitConfig);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType2nd);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType3rd);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromErrorType2nd);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ErrorTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ErrorIsFromAlarm);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, InitErrorDefault);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumErrorTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MinStep);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MaxErrorNum_1s);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MaxErrorNum);
        }

    mMethodsSet(controller, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtAttController(self);
    OverrideThaAttController((ThaAttController)self);
    OverrideTha6A210031PdhDe3AttController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031SdhLineAttController);
    }

AtAttController Tha6A210031SdhLineAttControllerObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031PdhDe3AttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit((Tha6A210031SdhLineAttController) self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A210031SdhLineAttControllerNew(AtChannel sdhLine)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A210031SdhLineAttControllerObjectInit(controller, sdhLine);
    }

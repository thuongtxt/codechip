/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A210031SdhPathAttController.c
 *
 * Created Date: Jul 8, 2016
 *
 * Description : SDH path ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhPath.h"
#include "../../../default/ocn/ThaModuleOcnInternal.h"
#include "../../../default/sdh/ThaSdhVc.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../pdh/Tha6A210031PdhAttControllerInternal.h"
#include "Tha6A210031SdhAttReg.h"
#include "Tha6A210031SdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttControllerMethods                m_AtAttControllerOverride;
static tThaAttControllerMethods               m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;

/* Save super implementation */
static const tAtAttControllerMethods *m_AtAttControllerMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8  NumPointerAdjTypes(Tha6A210031PdhDe3AttController self)
	{
	AtUnused(self);
	return 2;
	}

static uint32 MinStep(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return 1;
    }

static float MaxErrorNum_1s(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    float NFramePerSecond = ThaAttControllerNFramePerSecond((ThaAttController) self);

    AtUnused(errorType);
    return NFramePerSecond;
    }

static float MaxErrorNum(Tha6A210031PdhDe3AttController self, uint32 errorType, uint32 T/*ms*/)
    {
    float NFramePerSecond = ThaAttControllerNFramePerSecond((ThaAttController) self);

    AtUnused(errorType);
    return (NFramePerSecond * (float)T)/1000;
    }
static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x9, 0x4000);
    }

static eBool ErrorIsFromAlarm(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtFalse;
    }

static uint32 HwStepMaskHo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        return c_upen_BIPstscfg_cfgBIPstserr_step_Mask;
        }
    else if (isError == cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit16_9;

        return c_upen_BIPstscfg_cfgBIPstserr_step_Mask;
        }
    return c_upen_BIPstscfg_cfgBIPstserr_step_Mask;
    }

static uint32 HwStepMaskLo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(isError);
    return cBit31_0;
    }

static uint32 PositionMaskConfigurationMaskHo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return c_upen_BIPstscfg_cfgBIPstsmsk_pos_Mask_02;

        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_BIPstscfg_cfgBIPstsmsk_pos_Mask_02;
        return 0;
        }
    else if (isError == cThaAttForceError)
       {
       if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
           return cBit8_0;
       return c_upen_BIPstscfg_cfgBIPstsmsk_pos_Mask_02;
       }
    return c_upen_BIPstscfg_cfgBIPstsmsk_pos_Mask_02;
    }

static uint8  HwRdiType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);

    switch (alarmType)
        {
        case cAtSdhPathAlarmRdi:  return cBit0;
        case cAtSdhPathAlarmErdiC:return cBit1;
        case cAtSdhPathAlarmErdiS:return cBit2;
        case cAtSdhPathAlarmErdiP:return cBit3;
        default:
            return 0;
        }
    }

static uint32 RdiTypeMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return cBit31_28;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return cBit3_0;
        }
    return 0;
    }

static uint32 PositionMaskConfigurationMaskLo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return c_upen_BIPstscfg_cfgBIPstsmsk_pos_Mask_01;

        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_LOPstscfg_lopsts_msk_pos_Mask;
        return c_upen_AISstscfg_AISstsmsk_pos_Mask;
        }
    else if (isError == cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit31_25;
        return c_upen_BIPstscfg_cfgBIPstsmsk_pos_Mask_01;
        }
    return c_upen_BIPstscfg_cfgBIPstsmsk_pos_Mask_01;
    }

static uint32 DataMaskConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return c_upen_BIPstscfg_cfgBIPstsmsk_dat_Mask;

        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_LOPstscfg_lopsts_frc_ena_Mask;
        return c_upen_AISstscfg_AISstsfrc_ena_Mask;
        }
    else if (isError == cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit24_17;
        return c_upen_BIPstscfg_cfgBIPstsmsk_dat_Mask;
        }
    return c_upen_BIPstscfg_cfgBIPstsmsk_dat_Mask;
    }

static uint32 NumberOfErrorsMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError ==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return c_upen_BIPstscfg_cfgBIPstserr_num_Mask;

        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_LOPstscfg_lopsts_evtin1s_Mask;
        return c_upen_AISstscfg_AISstsestsin1s_Mask;
        }

    return c_upen_BIPstscfg_cfgBIPstserr_num_Mask;
    }

static uint32 DurationConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return 0;

        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_LOPstscfg_lopsts_sec_num_Mask;
        return c_upen_AISstscfg_AISstssec_num_Mask;
        }
    return 0;
    }

static uint32 HwForceModeConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError ==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
             return c_upen_BIPstscfg_cfgBIPstsfrc_1sen_Mask;

        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_LOPstscfg_lopsts_frc_mod_Mask;
        return c_upen_AISstscfg_AISstsfrc_mod_Mask;
        }

    return c_upen_BIPstscfg_cfgBIPstsfrc_1sen_Mask;
    }

static eBool AlarmIsSpecialRdi(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if  (   alarmType==cAtSdhPathAlarmRdi ||
            alarmType==cAtSdhPathAlarmErdiC||
            alarmType==cAtSdhPathAlarmErdiS||
            alarmType==cAtSdhPathAlarmErdiP)
        return cAtTrue;
    return cAtFalse;
    }

static eBool  AlarmIsFromError(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if (mMethodsGet(self)->AlarmIsSpecialRdi(self, alarmType)||alarmType==cAtSdhPathAlarmLop||alarmType==cAtSdhPathAlarmLom)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 RegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
            return cReg_upen_AISstscfg;
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiC:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiS:
            return cReg_upen_RDIstscfg;
        case cAtSdhPathAlarmLop:
            return cReg_upen_LOPstscfg;
        case cAtSdhPathAlarmUneq:
            return cReg_upen_UEQstscfg;
        case cAtSdhPathAlarmLom:
            return cReg_upen_LOMstscfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathAlarmAis:
            return cReg_upen_AISstssta;
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiC:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiS:
            return cReg_upen_RDIstssta;
        case cAtSdhPathAlarmLop:
            return cReg_upen_LOPstssta;
        case cAtSdhPathAlarmUneq:
            return cReg_upen_UEQstssta;
        case cAtSdhPathAlarmLom:
            return cReg_upen_LOMstssta;
        default:
            return cBit31_0;
        }
    }

static uint32 RealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwStsInSlice;
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    if (ThaSdhChannel2HwMasterStsId(channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return regAddr + hwStsInSlice + 1024UL * slice + Tha60210011ModuleOcnBaseAddress(ocnModule);
    return cInvalidUint32;
    }


static eAtRet CheckAlarmNumEvent(Tha6A210031PdhDe3AttController self, uint32 alarmType, uint32 numEvent)
	{
	AtUnused(self);
	AtUnused(alarmType);
	AtUnused(numEvent);
	return cAtOk;
	}

static eAtRet SpecificAlarmConfig(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtOk;
    }

static uint8 AlarmTypeIndex(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
            return 0;
        case cAtSdhPathAlarmRdi:
            return 1;
        case cAtSdhPathAlarmLop:
            return 2;
        case cAtSdhPathAlarmUneq:
            return 3;
        case cAtSdhPathAlarmErdiC:
            return 4;
        case cAtSdhPathAlarmErdiP:
            return 5;
        case cAtSdhPathAlarmErdiS:
            return 6;
        case cAtSdhPathAlarmLom:
            return 7;
        default:
            return 0;
        }
    }

static uint8 NumAlarmTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 RegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathErrorBip:
            return cReg_upen_BIPstscfg;
        case cAtSdhPathErrorRei:
            return cReg_upen_REIstscfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathErrorBip:
            return cReg_upen_BIPstssta;
        case cAtSdhPathErrorRei:
            return cReg_upen_REIstssta;
        default:
            return cBit31_0;
        }
    }

static uint8 ErrorTypeIndex(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathErrorBip:
            return 0;
        case cAtSdhPathErrorRei:
            return 1;
        default:
            return 0;
        }
    }

static uint8 NumErrorTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 2;
    }

static eBool  RateConvertStepIsSupport(ThaAttController self)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaAttControllerVersionReader((ThaAttController) self));
    AtUnused(self);

    if (hwVersion >= mMethodsGet(controller)->StartVersionSupportConvertStep(controller))
        return cAtTrue;
    return cAtFalse;
    }
static eBool  RateCalibVc3(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (Nrate == cAtNRate1E3)
        {*err_step=1;         *PositionMask = 13; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E4)
        {*err_step=10;        *PositionMask = 13; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E5)
        {*err_step=100;       *PositionMask = 13; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E6)
        {*err_step=249;       *PositionMask = 5; *frc_en_mask=5; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E7)
        {*err_step=100;       *PositionMask = 1; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E8)
        {*err_step=99/*998*/; *PositionMask = 1; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
    if (Nrate == cAtNRate1E9)
        {*err_step=99/*9978*/; *PositionMask = 1; *frc_en_mask=1; *hs =100; ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalibVc4(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (Nrate == cAtNRate1E4)
        {*err_step=1;          *PositionMask = 6; *frc_en_mask=5; *hs =1; ret = cAtTrue;} /* *PositionMask = 5; *frc_en_mask=6; 9.98 1e-4*/
    if (Nrate == cAtNRate1E5)
        {*err_step=1;          *PositionMask = 1; *frc_en_mask=3; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E6)
        {*err_step=163;        *PositionMask = 7; *frc_en_mask=7; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E7)
        {*err_step=133;        *PositionMask = 4; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E8)
        {*err_step=66/*332*/;  *PositionMask = 2; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
    if (Nrate == cAtNRate1E9)
        {*err_step=26/*26607*/; *PositionMask = 8; *frc_en_mask=1; *hs =100; ret = cAtTrue;}
    return ret;
    }
static eBool  RateCalibVc4_2c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (Nrate == cAtNRate1E4)
        {*err_step=1;          *PositionMask = 10; *frc_en_mask=6; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E5)
        {*err_step=1;        *PositionMask = 1; *frc_en_mask=6; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E6)
        {*err_step=163;        *PositionMask = 14; *frc_en_mask=7; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E7)
        {*err_step=133;        *PositionMask = 4; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E8)
        {*err_step=166/*332*/;  *PositionMask = 1; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E9)
        {*err_step=166/*26607*/; *PositionMask = 1; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalibVc4_3c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (Nrate == cAtNRate1E4)
        {*err_step=1;          *PositionMask = 15; *frc_en_mask=6; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E5)
        {*err_step=1;        *PositionMask = 3; *frc_en_mask=3; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E6)
        {*err_step=133;        *PositionMask = 18; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E7)
        {*err_step=133;        *PositionMask = 2; *frc_en_mask=6; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E8)
        {*err_step=111/*332*/;  *PositionMask = 1; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E9)
        {*err_step=111/*26607*/; *PositionMask = 1; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalibVc4_4c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (Nrate == cAtNRate1E4)
        {*err_step=1;          *PositionMask = 15; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E5)
        {*err_step=1;        *PositionMask = 2; *frc_en_mask=6; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E6)
        {*err_step=64;        *PositionMask = 11; *frc_en_mask=7; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E7)
        {*err_step=133;        *PositionMask = 2; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E8)
        {*err_step=83/*332*/;  *PositionMask = 1; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E9)
        {*err_step=83/*830*/; *PositionMask = 1; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalibVc4_5c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (Nrate == cAtNRate1E5)
        {*err_step=1;          *PositionMask = 16; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E6)
        {*err_step=1;        *PositionMask = 3; *frc_en_mask=5; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E7)
        {*err_step=85;        *PositionMask = 16; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E8)
        {*err_step=133;        *PositionMask = 4; *frc_en_mask=5; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E9)
        {*err_step=133/*332*/;  *PositionMask = 5; *frc_en_mask=5; *hs =10; ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalibVc4_6c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (Nrate == cAtNRate1E5)
        {*err_step=1;        *PositionMask = 3; *frc_en_mask=6; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E6)
        {*err_step=71;        *PositionMask = 16; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E7)
        {*err_step=194;        *PositionMask = 5; *frc_en_mask=7; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E8)
        {*err_step=111;  *PositionMask = 2; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E9)
        {*err_step=111/*1110*/; *PositionMask = 2; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalibVc4_7c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (Nrate == cAtNRate1E5)
        {*err_step=1;        *PositionMask = 3; *frc_en_mask=7; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E6)
        {*err_step=19;        *PositionMask = 5; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E7)
        {*err_step=19;        *PositionMask = 1; *frc_en_mask=4; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E8)
        {*err_step=95;  *PositionMask = 2; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E9)
        {*err_step=95/*950*/; *PositionMask = 2; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalibVc4_8c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (Nrate == cAtNRate1E5)
        {*err_step=1;        *PositionMask = 3; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E6)
        {*err_step=32;        *PositionMask = 11; *frc_en_mask=7; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E7)
        {*err_step=133;        *PositionMask = 4; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E8)
        {*err_step=208;  *PositionMask = 5; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E9)
        {*err_step=208/*2080*/; *PositionMask = 5; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalibVc4_9c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (Nrate == cAtNRate1E5)
        {*err_step=1;        *PositionMask = 9; *frc_en_mask=3; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E6)
        {*err_step=24;        *PositionMask = 13; *frc_en_mask=5; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E7)
        {*err_step=133;        *PositionMask = 6; *frc_en_mask=6; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E8)
        {*err_step=37;  *PositionMask = 1; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E9)
        {*err_step=37/*370*/; *PositionMask = 1; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalibVc4_10c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (Nrate == cAtNRate1E5)
        {*err_step=1;        *PositionMask = 5; *frc_en_mask=6; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E6)
        {*err_step=1;        *PositionMask = 1; *frc_en_mask=3; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E7)
        {*err_step=163;        *PositionMask = 7; *frc_en_mask=7; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E8)
        {*err_step=133;  *PositionMask = 4; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
    if (Nrate == cAtNRate1E9)
        {*err_step=133 /*1330*/; *PositionMask = 4; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalibVc4_11c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
  {
  eBool ret = cAtFalse;
  AtUnused(self);
  if (Nrate == cAtNRate1E5)
      {*err_step=1;        *PositionMask = 11; *frc_en_mask=3; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E6)
      {*err_step=29;        *PositionMask = 12; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E7)
      {*err_step=127;        *PositionMask = 6; *frc_en_mask=7; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E8)
      {*err_step=121;  *PositionMask = 4; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E9)
      {*err_step=121/*1210*/; *PositionMask = 4; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
  return ret;
  }

static eBool  RateCalibVc4_12c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
  {
  eBool ret = cAtFalse;
  AtUnused(self);
  if (Nrate == cAtNRate1E4)
        {*err_step=1;        *PositionMask = 16; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E5)
      {*err_step=1;        *PositionMask = 6; *frc_en_mask=6; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E6)
      {*err_step=18;        *PositionMask = 13; *frc_en_mask=5; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E7)
      {*err_step=97;        *PositionMask = 5; *frc_en_mask=7; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E8)
      {*err_step=194;  *PositionMask = 7; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E9)
      {*err_step=255/*370*/; *PositionMask = 1; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
  return ret;
  }

static eBool  RateCalibVc4_13c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
  {
  eBool ret = cAtFalse;
  AtUnused(self);
  if (Nrate == cAtNRate1E5)
      {*err_step=1;        *PositionMask = 13; *frc_en_mask=3; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E6)
      {*err_step=23;        *PositionMask = 15; *frc_en_mask=6; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E7)
      {*err_step=197;        *PositionMask = 11; *frc_en_mask=7; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E8)
      {*err_step=179;  *PositionMask = 7; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
  if (Nrate == cAtNRate1E9)
      {*err_step=179/*1790*/; *PositionMask = 7; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
  return ret;
  }

static eBool  RateCalibVc4_14c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
  {
  eBool ret = cAtFalse;
  AtUnused(self);
  if (Nrate == cAtNRate1E5)
      {*err_step=1;        *PositionMask = 6; *frc_en_mask=7; *hs =1; ret = cAtTrue;}
  else if (Nrate == cAtNRate1E6)
      {*err_step=19;        *PositionMask = 10; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
  else if (Nrate == cAtNRate1E7)
      {*err_step=19;        *PositionMask = 1; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
  else if (Nrate == cAtNRate1E8)
      {*err_step=95;  *PositionMask = 4; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
  else if (Nrate == cAtNRate1E9)
      {*err_step=238/*238*/; *PositionMask = 1; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
  return ret;
  }

static eBool  RateCalibVc4_15c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (Nrate == cAtNRate1E4)
        {*err_step=1;        *PositionMask = 16; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
    else if (Nrate == cAtNRate1E5)
        {*err_step=1;        *PositionMask = 9; *frc_en_mask=5; *hs =1; ret = cAtTrue;}
    else if (Nrate == cAtNRate1E6)
        {*err_step=2;        *PositionMask = 3; *frc_en_mask=3; *hs =1; ret = cAtTrue;}
    else if (Nrate == cAtNRate1E7)
        {*err_step=133;        *PositionMask = 10; *frc_en_mask=6; *hs =1; ret = cAtTrue;}
    else if (Nrate == cAtNRate1E8)
        {*err_step=133;  *PositionMask = 1; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
    else if (Nrate == cAtNRate1E9)
        {*err_step=222/*370*/; *PositionMask = 1; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalibVc4_16c(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
  {
  eBool ret = cAtFalse;
  AtUnused(self);
  if (Nrate == cAtNRate1E5)
      {*err_step=1;        *PositionMask = 6; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
  else if (Nrate == cAtNRate1E6)
      {*err_step=16;        *PositionMask = 11; *frc_en_mask=7; *hs =1; ret = cAtTrue;}
  else if (Nrate == cAtNRate1E7)
      {*err_step=133;        *PositionMask = 8; *frc_en_mask=8; *hs =1; ret = cAtTrue;}
  else if (Nrate == cAtNRate1E8)
      {*err_step=187;  *PositionMask = 9; *frc_en_mask=1; *hs =1; ret = cAtTrue;}
  else if (Nrate == cAtNRate1E9)
      {*err_step=208; *PositionMask = 1; *frc_en_mask=1; *hs =10; ret = cAtTrue;}
  return ret;
  }

static eBool  RateCalib(ThaAttController self, uint32 NbitPerFrame, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;
    AtUnused(self);
    if (NbitPerFrame == 783*8)/* AU3VC3*/
        return RateCalibVc3(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*8)/* AU4VC4*/
        return RateCalibVc4(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*2*8)
        return RateCalibVc4_2c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*3*8)
        return RateCalibVc4_3c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*4*8)
        return RateCalibVc4_4c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*5*8)
        return RateCalibVc4_5c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*6*8)
        return RateCalibVc4_6c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*7*8)
        return RateCalibVc4_7c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*8*8)
        return RateCalibVc4_8c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*9*8)
        return RateCalibVc4_9c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*10*8)
        return RateCalibVc4_10c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*11*8)
        return RateCalibVc4_11c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*12*8)
        return RateCalibVc4_12c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*13*8)
        return RateCalibVc4_13c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*14*8)
        return RateCalibVc4_14c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*15*8)
        return RateCalibVc4_15c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    if (NbitPerFrame == 783*3*16*8)
        return RateCalibVc4_16c(self, Nrate, err_step, PositionMask, frc_en_mask, hs);
    return ret;
    }

static eAtRet  RateConvertStepVcX(ThaAttController self, uint32 Nrate, uint8 mode, uint16 *err_step)
    {
    uint16 convert_step = 0;

    AtUnused(self);
    if (Nrate == cAtNRate1E3)
        convert_step = 0;
    else if (Nrate == cAtNRate1E4)
        convert_step = 1;
    else if (Nrate == cAtNRate1E5)
        convert_step = 2;
    else if (Nrate == cAtNRate1E6)
        convert_step = 3;
    else if (Nrate == cAtNRate1E7)
        convert_step = 4;
    else if (Nrate == cAtNRate1E8)
        convert_step = 5;
    else if (Nrate == cAtNRate1E9)
        convert_step = 6;

    *err_step = (uint16)(mode << 3UL) | convert_step;
    return cAtOk;
    }

static eAtRet  RateConvertStep(ThaAttController self, uint32 NbitPerFrame, uint32 Nrate, uint16 *err_step)
    {
    if (mMethodsGet(self)->RateConvertStepIsSupport(self))
        {
        if (NbitPerFrame == 783*8)/* AU3VC3*/
            return RateConvertStepVcX(self, Nrate, 0, err_step);
        else if (NbitPerFrame == 783*3*8)
            return RateConvertStepVcX(self, Nrate, 1, err_step);
        else if (NbitPerFrame == 783*3*2*8)
            return RateConvertStepVcX(self, Nrate, 2, err_step);
        else if (NbitPerFrame == 783*3*3*8)
            return RateConvertStepVcX(self, Nrate, 3, err_step);
        else if (NbitPerFrame == 783*3*4*8)
            return RateConvertStepVcX(self, Nrate, 4, err_step);
        else if (NbitPerFrame == 783*3*5*8)
            return RateConvertStepVcX(self, Nrate, 5, err_step);
        else if (NbitPerFrame == 783*3*6*8)
            return RateConvertStepVcX(self, Nrate, 6, err_step);
        else if (NbitPerFrame == 783*3*7*8)
            return RateConvertStepVcX(self, Nrate, 7, err_step);
        else if (NbitPerFrame == 783*3*8*8)
            return RateConvertStepVcX(self, Nrate, 8, err_step);
        else if (NbitPerFrame == 783*3*9*8)
            return RateConvertStepVcX(self, Nrate, 9, err_step);
        else if (NbitPerFrame == 783*3*10*8)
            return RateConvertStepVcX(self, Nrate, 10, err_step);
        else if (NbitPerFrame == 783*3*11*8)
            return RateConvertStepVcX(self, Nrate, 11, err_step);
        else if (NbitPerFrame == 783*3*12*8)
            return RateConvertStepVcX(self, Nrate, 12, err_step);
        else if (NbitPerFrame == 783*3*13*8)
            return RateConvertStepVcX(self, Nrate, 13, err_step);
        else if (NbitPerFrame == 783*3*14*8)
            return RateConvertStepVcX(self, Nrate, 14, err_step);
        else if (NbitPerFrame == 783*3*15*8)
            return RateConvertStepVcX(self, Nrate, 15, err_step);
        else if (NbitPerFrame == 783*3*16*8)
            return RateConvertStepVcX(self, Nrate, 16, err_step);
        }
    return cAtOk;
    }

static uint32 NbitPerFrame(ThaAttController self)
    {
    AtSdhChannel path = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    eAtSdhChannelType type = AtSdhChannelTypeGet(path);

    switch ((uint32)type)
        {
        case cAtSdhChannelTypeAu4_64c:
        case cAtSdhChannelTypeVc4_64c:
            return 64*783*3*8; /* VC12 */
        case cAtSdhChannelTypeAu4_16c:
        case cAtSdhChannelTypeVc4_16c:
            return 16*783*3*8; /* VC12 */
        case cAtSdhChannelTypeAu4_4c:
        case cAtSdhChannelTypeVc4_4c:
            return 4*783*3*8; /* VC12 */
        case cAtSdhChannelTypeAu4:
        case cAtSdhChannelTypeVc4:
            return 783*3*8; /* VC12 */
        case cAtSdhChannelTypeAu3:
        case cAtSdhChannelTypeVc3:
            return 783*8; /* VC11 */
        case cAtSdhChannelTypeAu4_16nc:
        case cAtSdhChannelTypeVc4_16nc:
        case cAtSdhChannelTypeAu4_nc:
        case cAtSdhChannelTypeVc4_nc:
            {
            uint8 numsts = AtSdhChannelNumSts(path);
            return (uint32)(783UL*(uint32)numsts*8UL); /* VC12 */
            }
        default:
            return 783*8;
        }
    return 783*8;
    }

static uint32 ChannelSpeed(ThaAttController self)
    {
    AtSdhChannel path = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(path);
    switch ((uint32)channelType)
        {
        case cAtSdhChannelTypeAu4_64c:
        case cAtSdhChannelTypeVc4_64c:
            return 64*149760;
        case cAtSdhChannelTypeAu4_16c:
        case cAtSdhChannelTypeVc4_16c:
            return 16*149760;
        case cAtSdhChannelTypeAu4_4c:
        case cAtSdhChannelTypeVc4_4c:
            return 4*149760;
        case cAtSdhChannelTypeAu4:
        case cAtSdhChannelTypeVc4:
            return 149760;
        case cAtSdhChannelTypeAu3:
        case cAtSdhChannelTypeVc3:
            return 48384;
        case cAtSdhChannelTypeAu4_16nc:
        case cAtSdhChannelTypeVc4_16nc:
        case cAtSdhChannelTypeAu4_nc:
        case cAtSdhChannelTypeVc4_nc:
            {
            uint8 numsts = AtSdhChannelNumSts(path);
            return (uint32)(48384UL*(uint32)numsts); /* VC12 */
            }
        default:
            return 0;
        }
    return 0;
    }

static eAtRet InitErrorDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i=0;
    eAtRet ret = cAtOk;
    static const uint32 cAtSdhPathErrorTypes[] = {cAtSdhPathErrorBip, cAtSdhPathErrorRei};
    uint8 errorTypeIndex=0;

    for (i = 0; i < mMethodsGet(self)->NumErrorTypes(self); i++)
        {
        if (AtAttControllerErrorForceIsSupported((AtAttController)self, cAtSdhPathErrorTypes[i]))
            {
            ret |= AtAttControllerUnForceError((AtAttController)self, cAtSdhPathErrorTypes[i]);
            ret |= AtAttControllerForceErrorModeSet((AtAttController)self, cAtSdhPathErrorTypes[i],cAtAttForceErrorModeContinuous);
            ret |= AtAttControllerForceErrorNumErrorsSet((AtAttController)self, cAtSdhPathErrorTypes[i], 1);
            ret |= AtAttControllerForceErrorBitPositionSet((AtAttController)self, cAtSdhPathErrorTypes[i], cBit31_0);
            ret |= AtAttControllerForceErrorDataMaskSet((AtAttController)self, cAtSdhPathErrorTypes[i], 1);
            errorTypeIndex = mMethodsGet(self)->ErrorTypeIndex(self, cAtSdhPathErrorTypes[i]);
            self->errorInfo[errorTypeIndex].errorStep = mMethodsGet(self)->MinStep(self, cAtSdhPathErrorTypes[i]);
            }
        }
    return ret;
    }

static const char* AlarmIndexToString(uint8 alarmIndex)
    {
    static const char* cAttSdhPathAlarmTypeStr[] = {"AIS", "RDI"};
    if (alarmIndex >= mCount(cAttSdhPathAlarmTypeStr))
        return cAttSdhPathAlarmTypeStr[0];
    return cAttSdhPathAlarmTypeStr[alarmIndex];
    }

static eAtRet InitAlarmDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i = 0;
    eAtRet ret = cAtOk;
    static const uint32 cAtSdhPathAlarmTypes[] = {cAtSdhPathAlarmAis, cAtSdhPathAlarmRdi, cAtSdhPathAlarmErdiC, cAtSdhPathAlarmErdiP, cAtSdhPathAlarmErdiS, cAtSdhPathAlarmLom};
    for (i = 0; i < mMethodsGet(self)->NumAlarmTypes(self); i++)
        {
        if (AtAttControllerAlarmForceIsSupported((AtAttController)self, cAtSdhPathAlarmTypes[i]))
            {
            ret |= AtAttControllerUnForceAlarm((AtAttController)self, cAtSdhPathAlarmTypes[i]);
            ret |= AtAttControllerForceAlarmModeSet((AtAttController)self, cAtSdhPathAlarmTypes[i],cAtAttForceAlarmModeContinuous);
            ret |= AtAttControllerForceAlarmNumEventSet((AtAttController)self, cAtSdhPathAlarmTypes[i], 1);
            }
        }
    return ret;
    }

static eBool AlarmForceIsSupported(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiC:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmLop:
        case cAtSdhPathAlarmUneq:
        case cAtSdhPathAlarmLom:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eBool ErrorForceIsSupported(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathErrorBip:
        case cAtSdhPathErrorRei:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }
static uint16 Tha6A210031SdhPathAttControllerLongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    /* HW still is not available at VT */
#if 0
    uint8 timeretry = 200, count=0;
    uint16 ret = m_AtAttControllerMethods->LongRead(self, regAddr, dataBuffer, bufferLen);
    ret = m_AtAttControllerMethods->LongRead(self, cReg_upen_cpu_idr_st_Base, dataBuffer, bufferLen);
    while(!( dataBuffer[1] & c_upen_cpu_idr_st_val_vld_Mask))
        {
        count++;
        ret = m_AtAttControllerMethods->LongRead(self, cReg_upen_cpu_idr_st_Base, dataBuffer, bufferLen);
        if (count >= timeretry) break;
        }
    mFieldIns(&dataBuffer[1], c_upen_cpu_idr_st_val_vld_Mask, c_upen_cpu_idr_st_val_vld_Shift, 0);
    return ret;
#endif
    return m_AtAttControllerMethods->LongRead(self, regAddr, dataBuffer, bufferLen);
    }

static uint16 LongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    if (0x21000 ==(regAddr & 0xFF000))
        return m_AtAttControllerMethods->LongRead(self, regAddr, dataBuffer, bufferLen);
    else
        return Tha6A210031SdhPathAttControllerLongRead(self, regAddr, dataBuffer, bufferLen);
    }

static eBool IsTu3(AtSdhChannel self)
    {
    eAtSdhChannelType type = AtSdhChannelTypeGet(self);

    if (type == cAtSdhChannelTypeVc3)
        {
		AtSdhChannel parent = AtSdhChannelParentChannelGet(self);
		return (AtSdhChannelTypeGet(parent) == cAtSdhChannelTypeTu3);
        }

    return cAtFalse;
    }

static ThaCdrController CdrControllerGet(ThaAttController self)
    {
    AtSdhVc vcPath = (AtSdhVc)AtAttControllerChannelGet((AtAttController)self);
    return ThaSdhVcCdrControllerGet(vcPath);
    }

static uint32 FrequenceOffsetBase(ThaAttController self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    return IsTu3(channel) ? cBaseNcoValTu3 : cBaseNcoValSts1;
    }

static uint32 FrequenceOffsetStep(ThaAttController self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    return (uint32) (IsTu3(channel) ? cNcoValPpbOffsetTU3 : cNcoValPpbOffsetSTS1);
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttControllerMethods =  mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, m_AtAttControllerMethods, sizeof(m_AtAttControllerOverride));


        mMethodOverride(m_AtAttControllerOverride, AlarmForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, ErrorForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, LongRead);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static eAtRet DefaultSet(ThaAttController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideThaAttController(ThaAttController self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));

        mMethodOverride(m_ThaAttControllerOverride, CdrControllerGet);
        mMethodOverride(m_ThaAttControllerOverride, FrequenceOffsetBase);
        mMethodOverride(m_ThaAttControllerOverride, FrequenceOffsetStep);
        mMethodOverride(m_ThaAttControllerOverride, ChannelSpeed);
        mMethodOverride(m_ThaAttControllerOverride, NbitPerFrame);
        mMethodOverride(m_ThaAttControllerOverride, RateCalib);
        mMethodOverride(m_ThaAttControllerOverride, DefaultSet);
        mMethodOverride(m_ThaAttControllerOverride, RateConvertStep);
        mMethodOverride(m_ThaAttControllerOverride, RateConvertStepIsSupport);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void OverrideTha6A210031PdhDe3AttController(AtAttController self)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(controller), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwStepMaskHo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwStepMaskLo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PositionMaskConfigurationMaskHo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PositionMaskConfigurationMaskLo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, DataMaskConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumberOfErrorsMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, DurationConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwForceModeConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MinStep);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MaxErrorNum_1s);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MaxErrorNum);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIsFromError);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIsSpecialRdi);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwRdiType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RdiTypeMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumAlarmTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, InitAlarmDefault);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIndexToString);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, SpecificAlarmConfig);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, CheckAlarmNumEvent);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ErrorTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ErrorIsFromAlarm);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumErrorTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, InitErrorDefault);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumPointerAdjTypes);
        }

    mMethodsSet(controller, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtAttController(self);
    OverrideThaAttController((ThaAttController)self);
    OverrideTha6A210031PdhDe3AttController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031SdhPathAttController);
    }

AtAttController Tha6A210031SdhPathAttControllerObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031PdhDe3AttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A210031SdhPathAttControllerNew(AtChannel sdhPath)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A210031SdhPathAttControllerObjectInit(controller, sdhPath);
    }

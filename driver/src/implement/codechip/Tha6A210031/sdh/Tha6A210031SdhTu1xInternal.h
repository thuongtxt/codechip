/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6A210031SdhTu1xInternal.h
 * 
 * Created Date: Sep 8, 2017
 *
 * Description : SDH Tu1x internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031SDHTU1XINTERNAL_H_
#define _THA6A210031SDHTU1XINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineTuInternal.h"
#include "Tha6A210031SdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A210031SdhTu1x
    {
    tTha60210011Tfi5LineTu super;

    /* Private data */
    AtAttController attController;
    }tTha6A210031SdhTu1x;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhTu Tha6A210031SdhTu1xObjectInit(AtSdhTu self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031SDHTU1XINTERNAL_H_ */


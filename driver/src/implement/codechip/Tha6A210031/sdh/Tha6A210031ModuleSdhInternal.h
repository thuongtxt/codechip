/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6A210031ModuleSdhInternal.h
 * 
 * Created Date: Jul 8, 2016
 *
 * Description : Module SDH internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031MODULESDHINTERNAL_H_
#define _THA6A210031MODULESDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/sdh/Tha60210031ModuleSdhInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAu Tha6A210031SdhAuNew(uint32 auId, uint8 auType, AtModuleSdh module);
AtSdhLine Tha6A210031SdhLineNew(uint32 channelId, AtModuleSdh module);
AtSdhVc Tha6A210031SdhVcNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhVc Tha6A210031SdhVc1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);
AtSdhTu Tha6A210031SdhTu1xNew(uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031MODULESDHINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A210031AttControllerInternal.h
 * 
 * Created Date: Jul 8, 2016
 *
 * Description : ATT SDH controller internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031SDHATTCONTROLLERINTERNAL_H_
#define _THA6A210031SDHATTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../pdh/Tha6A210031PdhAttControllerInternal.h"
#include <math.h>
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A210031SdhLineAttController * Tha6A210031SdhLineAttController;
typedef struct tTha6A210031SdhLineAttControllerMethods
    {
    uint32 (*StartVersionSupportTwoB2Force)(Tha6A210031SdhLineAttController self);
    eBool  (*IsTwoB2Force)(Tha6A210031SdhLineAttController self);
    }tTha6A210031SdhLineAttControllerMethods;


typedef struct tTha6A210031SdhLineAttController
    {
    tTha6A210031PdhDe3AttController super;
    const tTha6A210031SdhLineAttControllerMethods *methods;
    }tTha6A210031SdhLineAttController;

typedef struct tTha6A210031SdhPathAttController
    {
    tTha6A210031PdhDe3AttController super;
    }tTha6A210031SdhPathAttController;

typedef struct tTha6A210031SdhVc1xAttController
    {
    tTha6A210031PdhDe3AttController super;
    }tTha6A210031SdhVc1xAttController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttController Tha6A210031SdhLineAttControllerNew(AtChannel sdhLine);
AtAttController Tha6A210031SdhPathAttControllerNew(AtChannel sdhPath);
AtAttController Tha6A210031SdhVc1xAttControllerNew(AtChannel sdhPath);

AtAttController Tha6A210031SdhLineAttControllerObjectInit(AtAttController self, AtChannel channel);
AtAttController Tha6A210031SdhPathAttControllerObjectInit(AtAttController self, AtChannel channel);
AtAttController Tha6A210031SdhVc1xAttControllerObjectInit(AtAttController self, AtChannel channel);
eBool Tha6A210031AttSdhForceLogicFromDut(AtChannel self);

uint16 Tha6A210031SdhLineAttControllerLongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031SDHATTCONTROLLERINTERNAL_H_ */


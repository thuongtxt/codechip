/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6A210031SdhLineAttReg.h
 * 
 * Created Date: Jul 7, 2016
 *
 * Description : SDH ATT register description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031SDHLINEATTREG_H_
#define _THA6A210031SDHLINEATTREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : Force LOS Error Configuration
Reg Addr   : 0x25000-0x2501F
Reg Formula: 0x25000 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force LOS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_loscfg_Base                                                                          0x25000
#define cReg_upen_loscfg(SliceId, StsId)                                (0x25000UL+512UL*(SliceId)+(StsId))
#define cReg_upen_loscfg_WidthVal                                                                           64
#define cReg_upen_loscfg_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: los_msk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 LOS Event
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_loscfg_los_msk_pos_Mask                                                               cBit30_15
#define c_upen_loscfg_los_msk_pos_Shift                                                                     15

/*--------------------------------------
BitField Name: los_sec_num
BitField Type: RW
BitField Desc: The T second LOS number
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_loscfg_los_sec_num_Mask                                                                cBit14_7
#define c_upen_loscfg_los_sec_num_Shift                                                                      7

/*--------------------------------------
BitField Name: los_evtin1s
BitField Type: RW
BitField Desc: The T second LOS number
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_loscfg_los_evtin1s_Mask                                                                 cBit6_3
#define c_upen_loscfg_los_evtin1s_Shift                                                                      3

/*--------------------------------------
BitField Name: los_frc_ena
BitField Type: RW
BitField Desc: Force Alarm LOS Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_loscfg_los_frc_ena_Mask                                                                   cBit2
#define c_upen_loscfg_los_frc_ena_Shift                                                                      2

/*--------------------------------------
BitField Name: los_frc_mod
BitField Type: RW
BitField Desc: Force Alarm LOS mode 00: Set Alarm in T second 01: Set or clear
1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_loscfg_los_frc_mod_Mask                                                                 cBit1_0
#define c_upen_loscfg_los_frc_mod_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force LOS Error Status
Reg Addr   : 0x25020-0x2503F
Reg Formula: 0x25020 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force LOS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_lossta_Base                                                                          0x25020
#define cReg_upen_lossta(SliceId, StsId)                                (0x25020UL+512UL*(SliceId)+(StsId))
#define cReg_upen_lossta_WidthVal                                                                           32
#define cReg_upen_lossta_WriteMask                                                                         0x0

/*--------------------------------------
BitField Name: los_mskposst
BitField Type: RW
BitField Desc: The Position Index LOS Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_lossta_los_mskposst_Mask                                                              cBit20_17
#define c_upen_lossta_los_mskposst_Shift                                                                    17

/*--------------------------------------
BitField Name: los_seccntst
BitField Type: RW
BitField Desc: The Event LOS Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_lossta_los_seccntst_Mask                                                               cBit16_9
#define c_upen_lossta_los_seccntst_Shift                                                                     9

/*--------------------------------------
BitField Name: losmseccntst
BitField Type: RW
BitField Desc: The Second LOS Counter Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_lossta_losmseccntst_Mask                                                                cBit8_5
#define c_upen_lossta_losmseccntst_Shift                                                                     5

/*--------------------------------------
BitField Name: los_updcntst
BitField Type: RW
BitField Desc: The Position Mask LOS Counter Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_lossta_los_updcntst_Mask                                                                cBit4_3
#define c_upen_lossta_los_updcntst_Shift                                                                     3

/*--------------------------------------
BitField Name: lostimmsecst
BitField Type: RW
BitField Desc: The Remain Force Error LOS Status
BitField Bits: [02]
--------------------------------------*/
#define c_upen_lossta_lostimmsecst_Mask                                                                  cBit2
#define c_upen_lossta_lostimmsecst_Shift                                                                     2


/*--------------------------------------
BitField Name: lostim_secst
BitField Type: RW
BitField Desc: LOS Error Status
BitField Bits: [01]
--------------------------------------*/
#define c_upen_lossta_lostim_secst_Mask                                                                  cBit1
#define c_upen_lossta_lostim_secst_Shift                                                                     1

/*--------------------------------------
BitField Name: lososecenbst
BitField Type: RW
BitField Desc: Force LOS in One Second Enable Status
BitField Bits: [00]
--------------------------------------*/
#define c_upen_lossta_lososecenbst_Mask                                                                  cBit0
#define c_upen_lossta_lososecenbst_Shift                                                                     0

/*------------------------------------------------------------------------------
Reg Name   : Force LOF Error Configuration
Reg Addr   : 0x25040-0x2505F
Reg Formula: 0x25040 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force LOF Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lofcfg                                                                               0x25040
#define cReg_upen_lofcfg_WidthVal                                                                           64

/*--------------------------------------
BitField Name: loferr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force A1/A2 Bit Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_lofcfg_loferr_step_Mask                                                                cBit11_4
#define c_upen_lofcfg_loferr_step_Shift                                                                      4

/*--------------------------------------
BitField Name: lofmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  A1/A2 Bit Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_lofcfg_lofmsk_pos_Mask_01                                                             cBit31_20
#define c_upen_lofcfg_lofmsk_pos_Shift_01                                                                   20
#define c_upen_lofcfg_lofmsk_pos_Mask_02                                                               cBit3_0
#define c_upen_lofcfg_lofmsk_pos_Shift_02                                                                    0

/*--------------------------------------
BitField Name: lofmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For  A1/A2 Bit Congiguration
BitField Bits: [19]
--------------------------------------*/
#define c_upen_lofcfg_lofmsk_dat_Mask                                                                   cBit19
#define c_upen_lofcfg_lofmsk_dat_Shift                                                                      19

/*--------------------------------------
BitField Name: loferr_num
BitField Type: RW
BitField Desc: The Total A1/A2 Bit Error Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_lofcfg_loferr_num_Mask                                                                 cBit18_3
#define c_upen_lofcfg_loferr_num_Shift                                                                       3

/*--------------------------------------
BitField Name: loffrc_mod
BitField Type: RW
BitField Desc: Force LOF t Mode 000:N error� (N = 16bits),N in (Tms)� (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_lofcfg_loffrc_mod_Mask                                                                  cBit2_0
#define c_upen_lofcfg_loffrc_mod_Shift                                                                       0


#define c_upen_lofcfg_lof_msk_pos_Mask                                                               cBit31_16
#define c_upen_lofcfg_lof_msk_pos_Shift                                                                     16
#define c_upen_lofcfg_lof_sec_num_Mask                                                                cBit15_8
#define c_upen_lofcfg_lof_sec_num_Shift                                                                      8
#define c_upen_lofcfg_lof_evtin1s_Mask                                                                 cBit7_4
#define c_upen_lofcfg_lof_evtin1s_Shift                                                                      4
#define c_upen_lofcfg_lof_frc_ena_Mask                                                                   cBit3
#define c_upen_lofcfg_lof_frc_ena_Shift                                                                      3
#define c_upen_lofcfg_lof_frc_mod_Mask                                                                 cBit2_0
#define c_upen_lofcfg_lof_frc_mod_Shift                                                                      0
/*------------------------------------------------------------------------------
Reg Name   : Force LOF Error Status
Reg Addr   : 0x25060-0x2507F
Reg Formula: 0x25060 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force LOF Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_lofsta                                                                               0x25060
#define cReg_upen_lofsta_WidthVal                                                                           32

/*--------------------------------------
BitField Name: staloferr_stepcnt
BitField Type: RW
BitField Desc: The Event LOF Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_lofsta_staloferr_stepcnt_Mask                                                         cBit29_22
#define c_upen_lofsta_staloferr_stepcnt_Shift                                                               22

/*--------------------------------------
BitField Name: stalofmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask LOF Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_lofsta_stalofmsk_poscnt_Mask                                                          cBit21_18
#define c_upen_lofsta_stalofmsk_poscnt_Shift                                                                18

/*--------------------------------------
BitField Name: staloferr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error LOF Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_lofsta_staloferr_cnt_Mask                                                              cBit17_2
#define c_upen_lofsta_staloferr_cnt_Shift                                                                    2

/*--------------------------------------
BitField Name: staloffrc_err
BitField Type: RW
BitField Desc: LOF Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_lofsta_staloffrc_err_Mask                                                                 cBit1
#define c_upen_lofsta_staloffrc_err_Shift                                                                    1

/*--------------------------------------
BitField Name: stalofosecenb
BitField Type: RW
BitField Desc: Force LOF in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lofsta_stalofosecenb_Mask                                                                 cBit0
#define c_upen_lofsta_stalofosecenb_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Force Line AIS Error Configuration
Reg Addr   : 0x25080-0x2509F
Reg Formula: 0x25080 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force Line AIS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_laiscfg                                                                              0x25080
#define cReg_upen_laiscfg_WidthVal                                                                          32

/*--------------------------------------
BitField Name: ais_msk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 LOS Event
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_laiscfg_ais_msk_pos_Mask                                                              cBit30_15
#define c_upen_laiscfg_ais_msk_pos_Shift                                                                    15

/*--------------------------------------
BitField Name: ais_sec_num
BitField Type: RW
BitField Desc: The T second LOS number
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_laiscfg_ais_sec_num_Mask                                                               cBit14_7
#define c_upen_laiscfg_ais_sec_num_Shift                                                                     7

/*--------------------------------------
BitField Name: ais_evtin1s
BitField Type: RW
BitField Desc: The T second LOS number
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_laiscfg_ais_evtin1s_Mask                                                                cBit6_3
#define c_upen_laiscfg_ais_evtin1s_Shift                                                                     3

/*--------------------------------------
BitField Name: ais_frc_ena
BitField Type: RW
BitField Desc: Force Alarm LOS Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_laiscfg_ais_frc_ena_Mask                                                                  cBit2
#define c_upen_laiscfg_ais_frc_ena_Shift                                                                     2

/*--------------------------------------
BitField Name: ais_frc_mod
BitField Type: RW
BitField Desc: Force Alarm LOS mode 00: Set Alarm in T second 01: Set or clear
1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_laiscfg_ais_frc_mod_Mask                                                                cBit1_0
#define c_upen_laiscfg_ais_frc_mod_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Force Line AIS Error Status
Reg Addr   : 0x250a0-0x250BF
Reg Formula: 0x250a0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force Line AIS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_laissta                                                                              0x250a0
#define cReg_upen_laissta_WidthVal                                                                          32

/*--------------------------------------
BitField Name: ais_mskposst
BitField Type: RW
BitField Desc: The Position Index AIS Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_laissta_ais_mskposst_Mask                                                             cBit20_17
#define c_upen_laissta_ais_mskposst_Shift                                                                   17

/*--------------------------------------
BitField Name: ais_seccntst
BitField Type: RW
BitField Desc: The Event AIS Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_laissta_ais_seccntst_Mask                                                              cBit16_9
#define c_upen_laissta_ais_seccntst_Shift                                                                    9

/*--------------------------------------
BitField Name: aismseccntst
BitField Type: RW
BitField Desc: The Second AIS Counter Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_laissta_aismseccntst_Mask                                                               cBit8_5
#define c_upen_laissta_aismseccntst_Shift                                                                    5

/*--------------------------------------
BitField Name: ais_updcntst
BitField Type: RW
BitField Desc: The Position Mask AIS Counter Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_laissta_ais_updcntst_Mask                                                               cBit4_3
#define c_upen_laissta_ais_updcntst_Shift                                                                    3

/*--------------------------------------
BitField Name: aistimmsecst
BitField Type: RW
BitField Desc: The Remain Force Error AIS Status
BitField Bits: [02]
--------------------------------------*/
#define c_upen_laissta_aistimmsecst_Mask                                                                 cBit2
#define c_upen_laissta_aistimmsecst_Shift                                                                    2

/*--------------------------------------
BitField Name: aistim_secst
BitField Type: RW
BitField Desc: The AIS Error Status
BitField Bits: [01]
--------------------------------------*/
#define c_upen_laissta_aistim_secst_Mask                                                                 cBit1
#define c_upen_laissta_aistim_secst_Shift                                                                    1

/*--------------------------------------
BitField Name: aisosecenbst
BitField Type: RW
BitField Desc: Force AIS in One Second Enable  Status
BitField Bits: [00]
--------------------------------------*/
#define c_upen_laissta_aisosecenbst_Mask                                                                 cBit0
#define c_upen_laissta_aisosecenbst_Shift                                                                    0


/*------------------------------------------------------------------------------
Reg Name   : Force B1 Byte Error Configuration
Reg Addr   : 0x250c0-0x250DF
Reg Formula: 0x250c0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force B1 Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_B1bytcfg_Base                                                                     0x250c0
#define cReg_upen_B1bytcfg(SliceId, StsId)                              (0x250c0UL+512UL*(SliceId)+(StsId))
#define cReg_upen_B1bytcfg_WidthVal                                                                         64
#define cReg_upen_B1bytcfg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: cfgB1byterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force B1 Byte Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_B1bytcfg_cfgB1byterr_step_Mask                                                       cBit9_2
#define c_upen_B1bytcfg_cfgB1byterr_step_Shift                                                            2

/*--------------------------------------
BitField Name: cfgB1bytmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  B1 Byte Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_B1bytcfg_cfgB1bytmsk_pos_Mask_01                                                      cBit31_18
#define c_upen_B1bytcfg_cfgB1bytmsk_pos_Shift_01                                                            18
#define c_upen_B1bytcfg_cfgB1bytmsk_pos_Mask_02                                                     cBit1_0
#define c_upen_B1bytcfg_cfgB1bytmsk_pos_Shift_02                                                             0

/*--------------------------------------
BitField Name: cfgB1bytmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  B1 Byte Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_B1bytcfg_cfgB1bytmsk_dat_Mask                                                            cBit17
#define c_upen_B1bytcfg_cfgB1bytmsk_dat_Shift                                                               17

/*--------------------------------------
BitField Name: cfgB1byterr_num
BitField Type: RW
BitField Desc: The Total B1 Byte Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_B1bytcfg_cfgB1byterr_num_Mask                                                          cBit16_1
#define c_upen_B1bytcfg_cfgB1byterr_num_Shift                                                                1

/*--------------------------------------
BitField Name: cfgB1bytfrc_1sen
BitField Type: RW
BitField Desc: Force B1 Byte in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_B1bytcfg_cfgB1bytfrc_1sen_Mask                                                            cBit0
#define c_upen_B1bytcfg_cfgB1bytfrc_1sen_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force B1 Byte Error Status
Reg Addr   : 0x250e0-0x250FF
Reg Formula: 0x250e0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force B1 Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_B1bytsta                                                                             0x250e0
#define cReg_upen_B1bytsta_WidthVal                                                                         32

/*--------------------------------------
BitField Name: staB1byterr_stepcnt
BitField Type: RW
BitField Desc: The Event B1 Byte Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_B1bytsta_staB1byterr_stepcnt_Mask                                                  cBit29_22
#define c_upen_B1bytsta_staB1byterr_stepcnt_Shift                                                        22

/*--------------------------------------
BitField Name: staB1bytmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask B1 Byte Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_B1bytsta_staB1bytmsk_poscnt_Mask                                                   cBit21_18
#define c_upen_B1bytsta_staB1bytmsk_poscnt_Shift                                                            18

/*--------------------------------------
BitField Name: staB1byterr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error B1 Byte Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_B1bytsta_staB1byterr_cnt_Mask                                                          cBit17_2
#define c_upen_B1bytsta_staB1byterr_cnt_Shift                                                                2

/*--------------------------------------
BitField Name: staB1bytfrc_err
BitField Type: RW
BitField Desc: B1 Byte Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_B1bytsta_staB1bytfrc_err_Mask                                                             cBit1
#define c_upen_B1bytsta_staB1bytfrc_err_Shift                                                                1

/*--------------------------------------
BitField Name: staB1bytosecenb
BitField Type: RW
BitField Desc: Force B1 Byte in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_B1bytsta_staB1bytosecenb_Mask                                                             cBit0
#define c_upen_B1bytsta_staB1bytosecenb_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force Line RDI Error Configuration
Reg Addr   : 0x21800-0x2181F
Reg Formula: 0x21800 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force Line RDI Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_lrdicfg                                                                              0x21800
#define cReg_upen_lrdicfg_WidthVal                                                                          32

/*--------------------------------------
BitField Name: lrdierr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force Alarm RDI Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_lrdicfg_lrdierr_step_Mask                                                              cBit11_4
#define c_upen_lrdicfg_lrdierr_step_Shift                                                                    4

/*--------------------------------------
BitField Name: lrdimsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  Force Alarm RDI Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_lrdicfg_lrdimsk_pos_Mask_01                                                           cBit31_20
#define c_upen_lrdicfg_lrdimsk_pos_Shift_01                                                                 20
#define c_upen_lrdicfg_lrdimsk_pos_Mask_02                                                             cBit3_0
#define c_upen_lrdicfg_lrdimsk_pos_Shift_02                                                                  0

/*--------------------------------------
BitField Name: lrdimsk_dat
BitField Type: RW
BitField Desc: The Data Mask For Force Alarm RDI Congiguration
BitField Bits: [19]
--------------------------------------*/
#define c_upen_lrdicfg_lrdimsk_dat_Mask                                                                 cBit19
#define c_upen_lrdicfg_lrdimsk_dat_Shift                                                                    19

/*--------------------------------------
BitField Name: lrdierr_num
BitField Type: RW
BitField Desc: The Force Alarm RDI Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_lrdicfg_lrdierr_num_Mask                                                               cBit18_3
#define c_upen_lrdicfg_lrdierr_num_Shift                                                                     3

/*--------------------------------------
BitField Name: lrdifrc_mod
BitField Type: RW
BitField Desc: Force Alarm RDI Mode 000:N error� (N = 16bits),N in (Tms)� (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_lrdicfg_lrdifrc_mod_Mask                                                                cBit2_0
#define c_upen_lrdicfg_lrdifrc_mod_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Force Line RDI Error Status
Reg Addr   : 0x21820-0x2183F
Reg Formula: 0x21820 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force Line RDI Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_lrdista                                                                              0x21820
#define cReg_upen_lrdista_WidthVal                                                                          32

/*--------------------------------------
BitField Name: LineRDI_mskposst
BitField Type: RW
BitField Desc: The Position Index RDI Line Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_lrdista_LineRDI_mskposst_Mask                                                         cBit20_17
#define c_upen_lrdista_LineRDI_mskposst_Shift                                                               17

/*--------------------------------------
BitField Name: LineRDI_seccntst
BitField Type: RW
BitField Desc: The Event Counter RDI Line Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_lrdista_LineRDI_seccntst_Mask                                                          cBit16_9
#define c_upen_lrdista_LineRDI_seccntst_Shift                                                                9

/*--------------------------------------
BitField Name: LineRDImseccntst
BitField Type: RW
BitField Desc: The Second Counter RDI Line Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_lrdista_LineRDImseccntst_Mask                                                           cBit8_5
#define c_upen_lrdista_LineRDImseccntst_Shift                                                                5

/*--------------------------------------
BitField Name: LineRDI_updcntst
BitField Type: RW
BitField Desc: Updated RDI Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_lrdista_LineRDI_updcntst_Mask                                                           cBit4_3
#define c_upen_lrdista_LineRDI_updcntst_Shift                                                                3

/*--------------------------------------
BitField Name: LineRDItimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond RDI Line Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_lrdista_LineRDItimmsecst_Mask                                                             cBit2
#define c_upen_lrdista_LineRDItimmsecst_Shift                                                                2

/*--------------------------------------
BitField Name: LineRDItim_secst
BitField Type: RW
BitField Desc: Posedge Second RDI Line Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_lrdista_LineRDItim_secst_Mask                                                             cBit1
#define c_upen_lrdista_LineRDItim_secst_Shift                                                                1

/*--------------------------------------
BitField Name: LineRDIosecenbst
BitField Type: RW
BitField Desc: Force RDI Line  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_lrdista_LineRDIosecenbst_Mask                                                             cBit0
#define c_upen_lrdista_LineRDIosecenbst_Shift                                                                0

/*------------------------------------------------------------------------------
Reg Name   : Force Path REI Error Configuration
Reg Addr   : 0x21880-0x2189F
Reg Formula: 0x21880 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force Path REI Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_preicfg                                                                              0x21880
#define cReg_upen_preicfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: cfgpreierr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force Path REI Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_preicfg_cfgpreierr_step_Mask                                                            cBit9_2
#define c_upen_preicfg_cfgpreierr_step_Shift                                                                 2

/*--------------------------------------
BitField Name: cfgpreimsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  Path REI Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_preicfg_cfgpreimsk_pos_Mask_01                                                        cBit31_18
#define c_upen_preicfg_cfgpreimsk_pos_Shift_01                                                              18
#define c_upen_preicfg_cfgpreimsk_pos_Mask_02                                                          cBit1_0
#define c_upen_preicfg_cfgpreimsk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfgpreimsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  Path REI Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_preicfg_cfgpreimsk_dat_Mask                                                              cBit17
#define c_upen_preicfg_cfgpreimsk_dat_Shift                                                                 17

/*--------------------------------------
BitField Name: cfgpreierr_num
BitField Type: RW
BitField Desc: The Total Path REI Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_preicfg_cfgpreierr_num_Mask                                                            cBit16_1
#define c_upen_preicfg_cfgpreierr_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgpreifrc_1sen
BitField Type: RW
BitField Desc: Force Path REI in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_preicfg_cfgpreifrc_1sen_Mask                                                              cBit0
#define c_upen_preicfg_cfgpreifrc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force Path REI Error Status
Reg Addr   : 0x218a0-0x218bF
Reg Formula: 0x218a0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force Path REI Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_preista                                                                              0x218a0
#define cReg_upen_preista_WidthVal                                                                          32

/*--------------------------------------
BitField Name: stapreierr_stepcnt
BitField Type: RW
BitField Desc: The Event Path REI Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_preista_stapreierr_stepcnt_Mask                                                       cBit29_22
#define c_upen_preista_stapreierr_stepcnt_Shift                                                             22

/*--------------------------------------
BitField Name: stapreimsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask Path REI Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_preista_stapreimsk_poscnt_Mask                                                        cBit21_18
#define c_upen_preista_stapreimsk_poscnt_Shift                                                              18

/*--------------------------------------
BitField Name: stapreierr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error Path REI Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_preista_stapreierr_cnt_Mask                                                            cBit17_2
#define c_upen_preista_stapreierr_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: stapreifrc_err
BitField Type: RW
BitField Desc: Path REI Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_preista_stapreifrc_err_Mask                                                               cBit1
#define c_upen_preista_stapreifrc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: stapreiosecenb
BitField Type: RW
BitField Desc: Force Path REI in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_preista_stapreiosecenb_Mask                                                               cBit0
#define c_upen_preista_stapreiosecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP-8 Error Configuration
Reg Addr   : 0x21900-0x2191f
Reg Formula: 0x21900 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force BIP-8 Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_bip8cfg                                                                              0x21900
#define cReg_upen_bip8cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: cfgbip8err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force BIP-8 Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_bip8cfg_cfgbip8err_step_Mask                                                            cBit9_2
#define c_upen_bip8cfg_cfgbip8err_step_Shift                                                                 2

/*--------------------------------------
BitField Name: cfgbip8msk_pos
BitField Type: RW
BitField Desc: The Position Mask for  BIP-8 Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_bip8cfg_cfgbip8msk_pos_Mask_01                                                        cBit31_18
#define c_upen_bip8cfg_cfgbip8msk_pos_Shift_01                                                              18
#define c_upen_bip8cfg_cfgbip8msk_pos_Mask_02                                                          cBit1_0
#define c_upen_bip8cfg_cfgbip8msk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfgbip8msk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  BIP-8 Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_bip8cfg_cfgbip8msk_dat_Mask                                                              cBit17
#define c_upen_bip8cfg_cfgbip8msk_dat_Shift                                                                 17

/*--------------------------------------
BitField Name: cfgbip8err_num
BitField Type: RW
BitField Desc: The Total BIP-8 Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_bip8cfg_cfgbip8err_num_Mask                                                            cBit16_1
#define c_upen_bip8cfg_cfgbip8err_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgbip8frc_1sen
BitField Type: RW
BitField Desc: Force BIP-8 in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_bip8cfg_cfgbip8frc_1sen_Mask                                                              cBit0
#define c_upen_bip8cfg_cfgbip8frc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP-8 Error Status
Reg Addr   : 0x21920-0x2193f
Reg Formula: 0x21920 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force BIP-8 Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_bip8sta                                                                              0x21920
#define cReg_upen_bip8sta_WidthVal                                                                          32

/*--------------------------------------
BitField Name: stabip8err_stepcnt
BitField Type: RW
BitField Desc: The Event BIP-8 Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_bip8sta_stabip8err_stepcnt_Mask                                                       cBit29_22
#define c_upen_bip8sta_stabip8err_stepcnt_Shift                                                             22

/*--------------------------------------
BitField Name: stabip8msk_poscnt
BitField Type: RW
BitField Desc: The Position Mask BIP-8 Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_bip8sta_stabip8msk_poscnt_Mask                                                        cBit21_18
#define c_upen_bip8sta_stabip8msk_poscnt_Shift                                                              18

/*--------------------------------------
BitField Name: stabip8err_cnt
BitField Type: RW
BitField Desc: The Remain Force Error BIP-8 Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_bip8sta_stabip8err_cnt_Mask                                                            cBit17_2
#define c_upen_bip8sta_stabip8err_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: stabip8frc_err
BitField Type: RW
BitField Desc: BIP-8 Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_bip8sta_stabip8frc_err_Mask                                                               cBit1
#define c_upen_bip8sta_stabip8frc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: stabip8osecenb
BitField Type: RW
BitField Desc: Force BIP-8 in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_bip8sta_stabip8osecenb_Mask                                                               cBit0
#define c_upen_bip8sta_stabip8osecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force K Byte Error Configuration
Reg Addr   : 0x21940-0x2195f
Reg Formula: 0x21940 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force K Byte Error for STS-1 Frame
------------------------------------------------------------------------------*/
#define cReg_upen_Kbytcfg                                                                              0x21940
#define cReg_upen_Kbytcfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: cfgKbyterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force K Byte Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_Kbytcfg_cfgKbyterr_step_Mask                                                            cBit9_2
#define c_upen_Kbytcfg_cfgKbyterr_step_Shift                                                                 2

/*--------------------------------------
BitField Name: cfgKbytmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  K Byte Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_Kbytcfg_cfgKbytmsk_pos_Mask_01                                                        cBit31_18
#define c_upen_Kbytcfg_cfgKbytmsk_pos_Shift_01                                                              18
#define c_upen_Kbytcfg_cfgKbytmsk_pos_Mask_02                                                          cBit1_0
#define c_upen_Kbytcfg_cfgKbytmsk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfgKbytmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  K Byte Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_Kbytcfg_cfgKbytmsk_dat_Mask                                                              cBit17
#define c_upen_Kbytcfg_cfgKbytmsk_dat_Shift                                                                 17

/*--------------------------------------
BitField Name: cfgKbyterr_num
BitField Type: RW
BitField Desc: The Total K Byte Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_Kbytcfg_cfgKbyterr_num_Mask                                                            cBit16_1
#define c_upen_Kbytcfg_cfgKbyterr_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgKbytfrc_1sen
BitField Type: RW
BitField Desc: Force K Byte in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_Kbytcfg_cfgKbytfrc_1sen_Mask                                                              cBit0
#define c_upen_Kbytcfg_cfgKbytfrc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force K Byte Error Status
Reg Addr   : 0x21960-0x2197F
Reg Formula: 0x21960 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force K Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_Kbytsta                                                                              0x21960
#define cReg_upen_Kbytsta_WidthVal                                                                          32

/*--------------------------------------
BitField Name: staKbyterr_stepcnt
BitField Type: RW
BitField Desc: The Event K Byte Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_Kbytsta_staKbyterr_stepcnt_Mask                                                       cBit29_22
#define c_upen_Kbytsta_staKbyterr_stepcnt_Shift                                                             22

/*--------------------------------------
BitField Name: staKbytmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask K Byte Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_Kbytsta_staKbytmsk_poscnt_Mask                                                        cBit21_18
#define c_upen_Kbytsta_staKbytmsk_poscnt_Shift                                                              18

/*--------------------------------------
BitField Name: staKbyterr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error K Byte Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_Kbytsta_staKbyterr_cnt_Mask                                                            cBit17_2
#define c_upen_Kbytsta_staKbyterr_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: staKbytfrc_err
BitField Type: RW
BitField Desc: K Byte Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_Kbytsta_staKbytfrc_err_Mask                                                               cBit1
#define c_upen_Kbytsta_staKbytfrc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: staKbytosecenb
BitField Type: RW
BitField Desc: Force K Byte in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_Kbytsta_staKbytosecenb_Mask                                                               cBit0
#define c_upen_Kbytsta_staKbytosecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force S Byte Error Configuration
Reg Addr   : 0x21980-0x2199f
Reg Formula: 0x21980 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force S Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_Sbytcfg                                                                              0x21980
#define cReg_upen_Sbytcfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: cfgSbyterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force S Byte Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_Sbytcfg_cfgSbyterr_step_Mask                                                            cBit9_2
#define c_upen_Sbytcfg_cfgSbyterr_step_Shift                                                                 2

/*--------------------------------------
BitField Name: cfgSbytmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  S Byte Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_Sbytcfg_cfgSbytmsk_pos_Mask_01                                                        cBit31_18
#define c_upen_Sbytcfg_cfgSbytmsk_pos_Shift_01                                                              18
#define c_upen_Sbytcfg_cfgSbytmsk_pos_Mask_02                                                          cBit1_0
#define c_upen_Sbytcfg_cfgSbytmsk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfgSbytmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  S Byte Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_Sbytcfg_cfgSbytmsk_dat_Mask                                                              cBit17
#define c_upen_Sbytcfg_cfgSbytmsk_dat_Shift                                                                 17

/*--------------------------------------
BitField Name: cfgSbyterr_num
BitField Type: RW
BitField Desc: The Total S Byte Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_Sbytcfg_cfgSbyterr_num_Mask                                                            cBit16_1
#define c_upen_Sbytcfg_cfgSbyterr_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgSbytfrc_1sen
BitField Type: RW
BitField Desc: Force S Byte in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_Sbytcfg_cfgSbytfrc_1sen_Mask                                                              cBit0
#define c_upen_Sbytcfg_cfgSbytfrc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force S Byte Error Status
Reg Addr   : 0x219a0-0x219bf
Reg Formula: 0x219a0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force S Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_Sbytsta                                                                              0x219a0
#define cReg_upen_Sbytsta_WidthVal                                                                          32

/*--------------------------------------
BitField Name: staSbyterr_stepcnt
BitField Type: RW
BitField Desc: The Event S Byte Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_Sbytsta_staSbyterr_stepcnt_Mask                                                       cBit29_22
#define c_upen_Sbytsta_staSbyterr_stepcnt_Shift                                                             22

/*--------------------------------------
BitField Name: staSbytmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask S Byte Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_Sbytsta_staSbytmsk_poscnt_Mask                                                        cBit21_18
#define c_upen_Sbytsta_staSbytmsk_poscnt_Shift                                                              18

/*--------------------------------------
BitField Name: staSbyterr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error S Byte Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_Sbytsta_staSbyterr_cnt_Mask                                                            cBit17_2
#define c_upen_Sbytsta_staSbyterr_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: staSbytfrc_err
BitField Type: RW
BitField Desc: S Byte Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_Sbytsta_staSbytfrc_err_Mask                                                               cBit1
#define c_upen_Sbytsta_staSbytfrc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: staSbytosecenb
BitField Type: RW
BitField Desc: Force S Byte in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_Sbytsta_staSbytosecenb_Mask                                                               cBit0
#define c_upen_Sbytsta_staSbytosecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force REI Error Configuration
Reg Addr   : 0x21880-0x219df
Reg Formula: 0x219c0 + 512*SliceId + StsId
    Where  :
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   :
This register is applicable to configure Force REI Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_preicfg                                                                              0x21880
#define cReg_upen_preicfg_WidthVal                                                                          64



/*------------------------------------------------------------------------------
Reg Name   : Force B2 Byte Error Configuration
Reg Addr   : 0x219c0-0x219df
Reg Formula: 0x219c0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force B2 Byte Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_B2bytcfg                                                                             0x219c0
#define cReg_upen_B2bytcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: cfgB2byterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force B2 Byte Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_B2bytcfg_cfgB2byterr_step_Mask                                                          cBit9_2
#define c_upen_B2bytcfg_cfgB2byterr_step_Shift                                                               2

/*--------------------------------------
BitField Name: cfgB2bytmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  B2 Byte Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_B2bytcfg_cfgB2bytmsk_pos_Mask_01                                                      cBit31_18
#define c_upen_B2bytcfg_cfgB2bytmsk_pos_Shift_01                                                            18
#define c_upen_B2bytcfg_cfgB2bytmsk_pos_Mask_02                                                        cBit1_0
#define c_upen_B2bytcfg_cfgB2bytmsk_pos_Shift_02                                                             0

/*--------------------------------------
BitField Name: cfgB2bytmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  B2 Byte Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_B2bytcfg_cfgB2bytmsk_dat_Mask                                                            cBit17
#define c_upen_B2bytcfg_cfgB2bytmsk_dat_Shift                                                               17

/*--------------------------------------
BitField Name: cfgB2byterr_num
BitField Type: RW
BitField Desc: The Total B2 Byte Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_B2bytcfg_cfgB2byterr_num_Mask                                                          cBit16_1
#define c_upen_B2bytcfg_cfgB2byterr_num_Shift                                                                1

/*--------------------------------------
BitField Name: cfgB2bytfrc_1sen
BitField Type: RW
BitField Desc: Force B2 Byte in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_B2bytcfg_cfgB2bytfrc_1sen_Mask                                                            cBit0
#define c_upen_B2bytcfg_cfgB2bytfrc_1sen_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force B2 Byte Error Status
Reg Addr   : 0x219e0-0x219ff
Reg Formula: 0x219e0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force B2 Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_B2bytsta                                                                             0x219e0
#define cReg_upen_B2bytsta_WidthVal                                                                         32

/*--------------------------------------
BitField Name: staB2byterr_stepcnt
BitField Type: RW
BitField Desc: The Event B2 Byte Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_B2bytsta_staB2byterr_stepcnt_Mask                                                     cBit29_22
#define c_upen_B2bytsta_staB2byterr_stepcnt_Shift                                                           22

/*--------------------------------------
BitField Name: staB2bytmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask B2 Byte Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_B2bytsta_staB2bytmsk_poscnt_Mask                                                      cBit21_18
#define c_upen_B2bytsta_staB2bytmsk_poscnt_Shift                                                            18

/*--------------------------------------
BitField Name: staB2byterr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error B2 Byte Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_B2bytsta_staB2byterr_cnt_Mask                                                          cBit17_2
#define c_upen_B2bytsta_staB2byterr_cnt_Shift                                                                2

/*--------------------------------------
BitField Name: staB2bytfrc_err
BitField Type: RW
BitField Desc: B2 Byte Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_B2bytsta_staB2bytfrc_err_Mask                                                             cBit1
#define c_upen_B2bytsta_staB2bytfrc_err_Shift                                                                1

/*--------------------------------------
BitField Name: staB2bytosecenb
BitField Type: RW
BitField Desc: Force B2 Byte in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_B2bytsta_staB2bytosecenb_Mask                                                             cBit0
#define c_upen_B2bytsta_staB2bytosecenb_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : CPU  status
Reg Addr   : 0x253ff-0x253ff
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to read Address Indirect

------------------------------------------------------------------------------*/
#define cReg_upen_cpu_idr_st                                                                           0x253ff
/*--------------------------------------
BitField Name: val_vld
BitField Type: RO
BitField Desc: The value is valid
BitField Bits: [63]
--------------------------------------*/
#define c_upen_cpu_idr_st_val_vld_Mask                                                               cBit31
#define c_upen_cpu_idr_st_val_vld_Shift                                                                  31

/*--------------------------------------
BitField Name: add_val
BitField Type: RW
BitField Desc: The address value
BitField Bits: [62:0]
--------------------------------------*/
#define c_upen_cpu_idr_st_add_val_Mask_01                                                          cBit31_0
#define c_upen_cpu_idr_st_add_val_Shift_01                                                                0
#define c_upen_cpu_idr_st_add_val_Mask_02                                                          cBit30_0
#define c_upen_cpu_idr_st_add_val_Shift_02                                                                0

/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer VT Error Configuration
Reg Addr   : 0x64000-0x643FF
Reg Formula: 0x64000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force Loss Of Pointer VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_LOPvtcfg                                                                             0x64000
#define cReg_upen_LOPvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: lopvterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force LOP VT
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvterr_step_Mask                                                            cBit11_4
#define c_upen_LOPvtcfg_lopvterr_step_Shift                                                                  4

/*--------------------------------------
BitField Name: lopvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  LOP VT
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvtmsk_pos_Mask_01                                                         cBit31_20
#define c_upen_LOPvtcfg_lopvtmsk_pos_Shift_01                                                               20
#define c_upen_LOPvtcfg_lopvtmsk_pos_Mask_02                                                           cBit3_0
#define c_upen_LOPvtcfg_lopvtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: lopvtmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For LOP VT
BitField Bits: [19]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvtmsk_dat_Mask                                                               cBit19
#define c_upen_LOPvtcfg_lopvtmsk_dat_Shift                                                                  19

/*--------------------------------------
BitField Name: lopvterr_num
BitField Type: RW
BitField Desc: The total LOP VT Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvterr_num_Mask                                                             cBit18_3
#define c_upen_LOPvtcfg_lopvterr_num_Shift                                                                   3

/*--------------------------------------
BitField Name: lopvtfrc_mod
BitField Type: RW
BitField Desc: Force LOP VT t Mode 000:N error� (N = 16bits),N in (Tms)� (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_LOPvtcfg_lopvtfrc_mod_Mask                                                              cBit2_0
#define c_upen_LOPvtcfg_lopvtfrc_mod_Shift                                                                   0


#define c_upen_LOPvtcfg_lopvt_msk_pos_Mask                                                               cBit31_16
#define c_upen_LOPvtcfg_lopvt_msk_pos_Shift                                                                     16
#define c_upen_LOPvtcfg_lopvt_sec_num_Mask                                                                cBit15_8
#define c_upen_LOPvtcfg_lopvt_sec_num_Shift                                                                      8
#define c_upen_LOPvtcfg_lopvt_evtin1s_Mask                                                                 cBit7_4
#define c_upen_LOPvtcfg_lopvt_evtin1s_Shift                                                                      4
#define c_upen_LOPvtcfg_lopvt_frc_ena_Mask                                                                   cBit3
#define c_upen_LOPvtcfg_lopvt_frc_ena_Shift                                                                      3
#define c_upen_LOPvtcfg_lopvt_frc_mod_Mask                                                                 cBit2_0
#define c_upen_LOPvtcfg_lopvt_frc_mod_Shift                                                                      0
/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer VT Error Status
Reg Addr   : 0x64400-0x647FF
Reg Formula: 0x64400 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force Loss Of Pointer VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_LOPvtsta                                                                             0x64400
#define cReg_upen_LOPvtsta_WidthVal                                                                         32

/*--------------------------------------
BitField Name: LOPvt_mskposst
BitField Type: RW
BitField Desc: The Position Index LOP VT Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvt_mskposst_Mask                                                          cBit20_17
#define c_upen_LOPvtsta_LOPvt_mskposst_Shift                                                                17

/*--------------------------------------
BitField Name: LOPvt_seccntst
BitField Type: RW
BitField Desc: The Event Counter LOP VT Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvt_seccntst_Mask                                                           cBit16_9
#define c_upen_LOPvtsta_LOPvt_seccntst_Shift                                                                 9

/*--------------------------------------
BitField Name: LOPvtmseccntst
BitField Type: RW
BitField Desc: The Second Counter LOP VT Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvtmseccntst_Mask                                                            cBit8_5
#define c_upen_LOPvtsta_LOPvtmseccntst_Shift                                                                 5

/*--------------------------------------
BitField Name: LOPvt_updcntst
BitField Type: RW
BitField Desc: Updated LOP Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvt_updcntst_Mask                                                            cBit4_3
#define c_upen_LOPvtsta_LOPvt_updcntst_Shift                                                                 3

/*--------------------------------------
BitField Name: LOPvttimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond LOP VT Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvttimmsecst_Mask                                                              cBit2
#define c_upen_LOPvtsta_LOPvttimmsecst_Shift                                                                 2

/*--------------------------------------
BitField Name: LOPvttim_secst
BitField Type: RW
BitField Desc: Posedge Second LOP VT Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvttim_secst_Mask                                                              cBit1
#define c_upen_LOPvtsta_LOPvttim_secst_Shift                                                                 1

/*--------------------------------------
BitField Name: LOPvtosecenbst
BitField Type: RW
BitField Desc: Force Loss Of Pointer VT  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOPvtsta_LOPvtosecenbst_Mask                                                              cBit0
#define c_upen_LOPvtsta_LOPvtosecenbst_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS VT Error Configuration
Reg Addr   : 0x64800-0x64BFF
Reg Formula: 0x64800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force AIS VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_AISvtcfg                                                                             0x64800
#define cReg_upen_AISvtcfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: AISvtmsk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 event AIS VT
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_AISvtcfg_AISvtmsk_pos_Mask                                                            cBit30_15
#define c_upen_AISvtcfg_AISvtmsk_pos_Shift                                                                  15

/*--------------------------------------
BitField Name: AISvtsec_num
BitField Type: RW
BitField Desc: The  T second to force alarm  Loss Of Pointer VT
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_AISvtcfg_AISvtsec_num_Mask                                                             cBit14_7
#define c_upen_AISvtcfg_AISvtsec_num_Shift                                                                   7

/*--------------------------------------
BitField Name: AISvtevtin1s
BitField Type: RW
BitField Desc: The event  Loss Of Pointer VT in one second
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_AISvtcfg_AISvtevtin1s_Mask                                                              cBit6_3
#define c_upen_AISvtcfg_AISvtevtin1s_Shift                                                                   3

/*--------------------------------------
BitField Name: AISvtfrc_ena
BitField Type: RW
BitField Desc: Force Alarm Loss Of Pointer VT Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_AISvtcfg_AISvtfrc_ena_Mask                                                                cBit2
#define c_upen_AISvtcfg_AISvtfrc_ena_Shift                                                                   2

/*--------------------------------------
BitField Name: AISvtfrc_mod
BitField Type: RW
BitField Desc: Force Loss Of Pointer VT in One second Mode 00: Set Alarm in T
second 01: Set or clear 1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_AISvtcfg_AISvtfrc_mod_Mask                                                              cBit1_0
#define c_upen_AISvtcfg_AISvtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS VT Error Status
Reg Addr   : 0x64C00-0x64FFF
Reg Formula: 0x64C00 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force AIS VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_AISvtsta_Base                                                                        0x64C00
#define cReg_upen_AISvtsta(SliceId, StsId, VtgId, VtId)               (0x64C00UL+16384UL*(SliceId)+32UL*(StsId)+4UL*(VtgId)+(VtId))
#define cReg_upen_AISvtsta_WidthVal                                                                         32
#define cReg_upen_AISvtsta_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: AISvt_mskposst
BitField Type: RW
BitField Desc: The Position Index AIS VT Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_AISvtsta_AISvt_mskposst_Mask                                                          cBit20_17
#define c_upen_AISvtsta_AISvt_mskposst_Shift                                                                17

/*--------------------------------------
BitField Name: AISvt_seccntst
BitField Type: RW
BitField Desc: The Event Counter AIS VT Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_AISvtsta_AISvt_seccntst_Mask                                                           cBit16_9
#define c_upen_AISvtsta_AISvt_seccntst_Shift                                                                 9

/*--------------------------------------
BitField Name: AISvtmseccntst
BitField Type: RW
BitField Desc: The Second Counter AIS VT Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_AISvtsta_AISvtmseccntst_Mask                                                            cBit8_5
#define c_upen_AISvtsta_AISvtmseccntst_Shift                                                                 5

/*--------------------------------------
BitField Name: AISvt_updcntst
BitField Type: RW
BitField Desc: Updated AIS Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_AISvtsta_AISvt_updcntst_Mask                                                            cBit4_3
#define c_upen_AISvtsta_AISvt_updcntst_Shift                                                                 3

/*--------------------------------------
BitField Name: AISvttimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond AIS VT Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_AISvtsta_AISvttimmsecst_Mask                                                              cBit2
#define c_upen_AISvtsta_AISvttimmsecst_Shift                                                                 2

/*--------------------------------------
BitField Name: AISvttim_secst
BitField Type: RW
BitField Desc: Posedge Second AIS VT Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_AISvtsta_AISvttim_secst_Mask                                                              cBit1
#define c_upen_AISvtsta_AISvttim_secst_Shift                                                                 1

/*--------------------------------------
BitField Name: AISvtosecenbst
BitField Type: RW
BitField Desc: Force Loss Of Pointer VT  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_AISvtsta_AISvtosecenbst_Mask                                                              cBit0
#define c_upen_AISvtsta_AISvtosecenbst_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ VT Error Configuration
Reg Addr   : 0x65000-0x653FF
Reg Formula: 0x65000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force UEQ VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_UEQvtcfg_Base                                                                        0x65000
#define cReg_upen_UEQvtcfg(SliceId, StsId, VtgId, VtId)            (0x65000UL+16384UL*(SliceId)+32UL*(StsId)+4UL*(VtgId)+(VtId))
#define cReg_upen_UEQvtcfg_WidthVal                                                                         64
#define cReg_upen_UEQvtcfg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: UEQvtmsk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 event UEQ VT
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_UEQvtcfg_UEQvtmsk_pos_Mask                                                            cBit30_15
#define c_upen_UEQvtcfg_UEQvtmsk_pos_Shift                                                                  15

/*--------------------------------------
BitField Name: UEQvtsec_num
BitField Type: RW
BitField Desc: The  T second to force alarm  UEQ VT
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_UEQvtcfg_UEQvtsec_num_Mask                                                             cBit14_7
#define c_upen_UEQvtcfg_UEQvtsec_num_Shift                                                                   7

/*--------------------------------------
BitField Name: UEQvtevtin1s
BitField Type: RW
BitField Desc: The event  UEQ VT in one second
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_UEQvtcfg_UEQvtevtin1s_Mask                                                              cBit6_3
#define c_upen_UEQvtcfg_UEQvtevtin1s_Shift                                                                   3

/*--------------------------------------
BitField Name: UEQvtfrc_ena
BitField Type: RW
BitField Desc: Force Alarm UEQ VT Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_UEQvtcfg_UEQvtfrc_ena_Mask                                                                cBit2
#define c_upen_UEQvtcfg_UEQvtfrc_ena_Shift                                                                   2

/*--------------------------------------
BitField Name: UEQvtfrc_mod
BitField Type: RW
BitField Desc: Force UEQ VT in One second Mode 00: Set Alarm in T second 01: Set
or clear 1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_UEQvtcfg_UEQvtfrc_mod_Mask                                                              cBit1_0
#define c_upen_UEQvtcfg_UEQvtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ VT Error Status
Reg Addr   : 0x65400-0x657FF
Reg Formula: 0x65400 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force UEQ VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_UEQvtsta                                                                             0x65400
#define cReg_upen_UEQvtsta_WidthVal                                                                         32

/*--------------------------------------
BitField Name: UEQvt_mskposst
BitField Type: RW
BitField Desc: The Position Index UEQ VT Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvt_mskposst_Mask                                                          cBit20_17
#define c_upen_UEQvtsta_UEQvt_mskposst_Shift                                                                17

/*--------------------------------------
BitField Name: UEQvt_seccntst
BitField Type: RW
BitField Desc: The Event Counter UEQ VT Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvt_seccntst_Mask                                                           cBit16_9
#define c_upen_UEQvtsta_UEQvt_seccntst_Shift                                                                 9

/*--------------------------------------
BitField Name: UEQvtmseccntst
BitField Type: RW
BitField Desc: The Second Counter UEQ VT Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvtmseccntst_Mask                                                            cBit8_5
#define c_upen_UEQvtsta_UEQvtmseccntst_Shift                                                                 5

/*--------------------------------------
BitField Name: UEQvt_updcntst
BitField Type: RW
BitField Desc: Updated UEQ Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvt_updcntst_Mask                                                            cBit4_3
#define c_upen_UEQvtsta_UEQvt_updcntst_Shift                                                                 3

/*--------------------------------------
BitField Name: UEQvttimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond UEQ VT Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvttimmsecst_Mask                                                              cBit2
#define c_upen_UEQvtsta_UEQvttimmsecst_Shift                                                                 2

/*--------------------------------------
BitField Name: UEQvttim_secst
BitField Type: RW
BitField Desc: Posedge Second UEQ VT Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvttim_secst_Mask                                                              cBit1
#define c_upen_UEQvtsta_UEQvttim_secst_Shift                                                                 1

/*--------------------------------------
BitField Name: UEQvtosecenbst
BitField Type: RW
BitField Desc: Force UEQ VT  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_UEQvtsta_UEQvtosecenbst_Mask                                                              cBit0
#define c_upen_UEQvtsta_UEQvtosecenbst_Shift                                                                 0

/*------------------------------------------------------------------------------
Reg Name   : Force BIP VT Error Configuration
Reg Addr   : 0x65800-0x65BFF
Reg Formula: 0x65800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force BIP VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_BIPvtcfg_Base                                                                     0x65800
#define cReg_upen_BIPvtcfg(SliceId, StsId, VtgId, VtId)            (0x65800UL+16384UL*(SliceId)+32UL*(StsId)+4UL*(VtgId)+(VtId))
#define cReg_upen_BIPvtcfg_WidthVal                                                                      64

/*--------------------------------------
BitField Name: cfgBIPvterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force BIP VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_BIPvtcfg_cfgBIPvterr_step_Mask                                                       cBit9_2
#define c_upen_BIPvtcfg_cfgBIPvterr_step_Shift                                                            2

/*--------------------------------------
BitField Name: cfgBIPvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  BIP VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Mask_01                                                   cBit31_18
#define c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Shift_01                                                         18
#define c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Mask_02                                                     cBit1_0
#define c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Shift_02                                                          0

/*--------------------------------------
BitField Name: cfgBIPvtmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  BIP VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_BIPvtcfg_cfgBIPvtmsk_dat_Mask                                                         cBit17
#define c_upen_BIPvtcfg_cfgBIPvtmsk_dat_Shift                                                            17

/*--------------------------------------
BitField Name: cfgBIPvterr_num
BitField Type: RW
BitField Desc: The Total BIP VT Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_BIPvtcfg_cfgBIPvterr_num_Mask                                                       cBit16_1
#define c_upen_BIPvtcfg_cfgBIPvterr_num_Shift                                                                1

/*--------------------------------------
BitField Name: cfgBIPvtfrc_1sen
BitField Type: RW
BitField Desc: Force BIP VT in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPvtcfg_cfgBIPvtfrc_1sen_Mask                                                         cBit0
#define c_upen_BIPvtcfg_cfgBIPvtfrc_1sen_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP VT Error Status
Reg Addr   : 0x65C00-0x65FFF
Reg Formula: 0x65C00 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force BIP VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_BIPvtsta                                                                             0x65C00
#define cReg_upen_BIPvtsta_WidthVal                                                                      32

/*--------------------------------------
BitField Name: staBIPvterr_stepcnt
BitField Type: RW
BitField Desc: The Event BIP VT Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_BIPvtsta_staBIPvterr_stepcnt_Mask                                                  cBit29_22
#define c_upen_BIPvtsta_staBIPvterr_stepcnt_Shift                                                        22

/*--------------------------------------
BitField Name: staBIPvtmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask BIP VT Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_BIPvtsta_staBIPvtmsk_poscnt_Mask                                                   cBit21_18
#define c_upen_BIPvtsta_staBIPvtmsk_poscnt_Shift                                                         18

/*--------------------------------------
BitField Name: staBIPvterr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error BIP VT Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_BIPvtsta_staBIPvterr_cnt_Mask                                                       cBit17_2
#define c_upen_BIPvtsta_staBIPvterr_cnt_Shift                                                             2

/*--------------------------------------
BitField Name: staBIPvtfrc_err
BitField Type: RW
BitField Desc: BIP VT Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_BIPvtsta_staBIPvtfrc_err_Mask                                                          cBit1
#define c_upen_BIPvtsta_staBIPvtfrc_err_Shift                                                             1

/*--------------------------------------
BitField Name: staBIPvtosecenb
BitField Type: RW
BitField Desc: Force BIP VT in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPvtsta_staBIPvtosecenb_Mask                                                          cBit0
#define c_upen_BIPvtsta_staBIPvtosecenb_Shift                                                             0

/*------------------------------------------------------------------------------
Reg Name   : Force REI VT Error Configuration
Reg Addr   : 0x66000-0x663FF
Reg Formula: 0x66000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force REI VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_REIvtcfg                                                                             0x66000
#define cReg_upen_REIvtcfg_WidthVal                                                                      64

/*--------------------------------------
BitField Name: cfgREIvterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force REI VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_REIvtcfg_cfgREIvterr_step_Mask                                                       cBit9_2
#define c_upen_REIvtcfg_cfgREIvterr_step_Shift                                                            2

/*--------------------------------------
BitField Name: cfgREIvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  REI VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_REIvtcfg_cfgREIvtmsk_pos_Mask_01                                                   cBit31_18
#define c_upen_REIvtcfg_cfgREIvtmsk_pos_Shift_01                                                         18
#define c_upen_REIvtcfg_cfgREIvtmsk_pos_Mask_02                                                     cBit1_0
#define c_upen_REIvtcfg_cfgREIvtmsk_pos_Shift_02                                                          0

/*--------------------------------------
BitField Name: cfgREIvtmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  REI VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_REIvtcfg_cfgREIvtmsk_dat_Mask                                                         cBit17
#define c_upen_REIvtcfg_cfgREIvtmsk_dat_Shift                                                            17

/*--------------------------------------
BitField Name: cfgREIvterr_num
BitField Type: RW
BitField Desc: The Total REI VT Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_REIvtcfg_cfgREIvterr_num_Mask                                                       cBit16_1
#define c_upen_REIvtcfg_cfgREIvterr_num_Shift                                                             1

/*--------------------------------------
BitField Name: cfgREIvtfrc_1sen
BitField Type: RW
BitField Desc: Force REI VT in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIvtcfg_cfgREIvtfrc_1sen_Mask                                                         cBit0
#define c_upen_REIvtcfg_cfgREIvtfrc_1sen_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force REI VT Error Status
Reg Addr   : 0x66400-0x667FF
Reg Formula: 0x66400 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force REI VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_REIvtsta                                                                             0x66400
#define cReg_upen_REIvtsta_WidthVal                                                                      32

/*--------------------------------------
BitField Name: staREIvterr_stepcnt
BitField Type: RW
BitField Desc: The Event REI VT Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_REIvtsta_staREIvterr_stepcnt_Mask                                                  cBit29_22
#define c_upen_REIvtsta_staREIvterr_stepcnt_Shift                                                        22

/*--------------------------------------
BitField Name: staREIvtmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask REI VT Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_REIvtsta_staREIvtmsk_poscnt_Mask                                                   cBit21_18
#define c_upen_REIvtsta_staREIvtmsk_poscnt_Shift                                                            18

/*--------------------------------------
BitField Name: staREIvterr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error REI VT Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_REIvtsta_staREIvterr_cnt_Mask                                                       cBit17_2
#define c_upen_REIvtsta_staREIvterr_cnt_Shift                                                             2

/*--------------------------------------
BitField Name: staREIvtfrc_err
BitField Type: RW
BitField Desc: REI VT Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_REIvtsta_staREIvtfrc_err_Mask                                                          cBit1
#define c_upen_REIvtsta_staREIvtfrc_err_Shift                                                             1

/*--------------------------------------
BitField Name: staREIvtosecenb
BitField Type: RW
BitField Desc: Force REI VT in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIvtsta_staREIvtosecenb_Mask                                                          cBit0
#define c_upen_REIvtsta_staREIvtosecenb_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI VT Error Configuration
Reg Addr   : 0x66800-0x66BFF
Reg Formula: 0x66800 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force RDI VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_RDIvtcfg                                                                             0x66800
#define cReg_upen_RDIvtcfg_WidthVal                                                                      64

/*--------------------------------------
BitField Name: rdivterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force rdi VT
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivterr_step_Mask                                                            cBit11_4
#define c_upen_RDIvtcfg_rdivterr_step_Shift                                                                  4

/*--------------------------------------
BitField Name: rdivtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  rdi VT
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivtmsk_pos_Mask_01                                                         cBit31_20
#define c_upen_RDIvtcfg_rdivtmsk_pos_Shift_01                                                               20
#define c_upen_RDIvtcfg_rdivtmsk_pos_Mask_02                                                           cBit3_0
#define c_upen_RDIvtcfg_rdivtmsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: rdivtmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For rdi VT
BitField Bits: [19]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivtmsk_dat_Mask                                                               cBit19
#define c_upen_RDIvtcfg_rdivtmsk_dat_Shift                                                                  19

/*--------------------------------------
BitField Name: rdivterr_num
BitField Type: RW
BitField Desc: The total rdi VT Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivterr_num_Mask                                                             cBit18_3
#define c_upen_RDIvtcfg_rdivterr_num_Shift                                                                   3

/*--------------------------------------
BitField Name: rdivtfrc_mod
BitField Type: RW
BitField Desc: Force rdi VT t Mode 000:N error� (N = 16bits),N in (Tms)� (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_RDIvtcfg_rdivtfrc_mod_Mask                                                              cBit2_0
#define c_upen_RDIvtcfg_rdivtfrc_mod_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI VT Error Status
Reg Addr   : 0x66C00-0x66FFF
Reg Formula: 0x66C00 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force RDI VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_RDIvtsta                                                                             0x66C00
#define cReg_upen_RDIvtsta_WidthVal                                                                      32

/*--------------------------------------
BitField Name: RDIvt_mskposst
BitField Type: RW
BitField Desc: The Position Index RDI VT Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvt_mskposst_Mask                                                          cBit20_17
#define c_upen_RDIvtsta_RDIvt_mskposst_Shift                                                                17

/*--------------------------------------
BitField Name: RDIvt_seccntst
BitField Type: RW
BitField Desc: The Event Counter RDI VT Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvt_seccntst_Mask                                                           cBit16_9
#define c_upen_RDIvtsta_RDIvt_seccntst_Shift                                                                 9

/*--------------------------------------
BitField Name: RDIvtmseccntst
BitField Type: RW
BitField Desc: The Second Counter RDI VT Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvtmseccntst_Mask                                                            cBit8_5
#define c_upen_RDIvtsta_RDIvtmseccntst_Shift                                                                 5

/*--------------------------------------
BitField Name: RDIvt_updcntst
BitField Type: RW
BitField Desc: Updated RDI Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvt_updcntst_Mask                                                            cBit4_3
#define c_upen_RDIvtsta_RDIvt_updcntst_Shift                                                                 3

/*--------------------------------------
BitField Name: RDIvttimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond RDI VT Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvttimmsecst_Mask                                                              cBit2
#define c_upen_RDIvtsta_RDIvttimmsecst_Shift                                                                 2

/*--------------------------------------
BitField Name: RDIvttim_secst
BitField Type: RW
BitField Desc: Posedge Second RDI VT Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvttim_secst_Mask                                                              cBit1
#define c_upen_RDIvtsta_RDIvttim_secst_Shift                                                                 1

/*--------------------------------------
BitField Name: RDIvtosecenbst
BitField Type: RW
BitField Desc: Force RDI VT  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RDIvtsta_RDIvtosecenbst_Mask                                                              cBit0
#define c_upen_RDIvtsta_RDIvtosecenbst_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force RFI VT Error Configuration
Reg Addr   : 0x67000-0x673FF
Reg Formula: 0x67000 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to configure Force RFI VT Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_RFIvtcfg                                                                             0x67000
#define cReg_upen_RFIvtcfg_WidthVal                                                                      64

/*--------------------------------------
BitField Name: cfgRFIvterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force RFI VT Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_RFIvtcfg_cfgRFIvterr_step_Mask                                                       cBit9_2
#define c_upen_RFIvtcfg_cfgRFIvterr_step_Shift                                                            2

/*--------------------------------------
BitField Name: cfgRFIvtmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  RFI VT Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_RFIvtcfg_cfgRFIvtmsk_pos_Mask_01                                                   cBit31_18
#define c_upen_RFIvtcfg_cfgRFIvtmsk_pos_Shift_01                                                         18
#define c_upen_RFIvtcfg_cfgRFIvtmsk_pos_Mask_02                                                     cBit1_0
#define c_upen_RFIvtcfg_cfgRFIvtmsk_pos_Shift_02                                                          0

/*--------------------------------------
BitField Name: cfgRFIvtmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  RFI VT Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_RFIvtcfg_cfgRFIvtmsk_dat_Mask                                                         cBit17
#define c_upen_RFIvtcfg_cfgRFIvtmsk_dat_Shift                                                            17

/*--------------------------------------
BitField Name: cfgRFIvterr_num
BitField Type: RW
BitField Desc: The Total RFI VT Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_RFIvtcfg_cfgRFIvterr_num_Mask                                                       cBit16_1
#define c_upen_RFIvtcfg_cfgRFIvterr_num_Shift                                                             1

/*--------------------------------------
BitField Name: cfgRFIvtfrc_1sen
BitField Type: RW
BitField Desc: Force RFI VT in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RFIvtcfg_cfgRFIvtfrc_1sen_Mask                                                         cBit0
#define c_upen_RFIvtcfg_cfgRFIvtfrc_1sen_Shift                                                            0


/*------------------------------------------------------------------------------
Reg Name   : Force RFI VT Error Status
Reg Addr   : 0x67C00-0x67FFF
Reg Formula: 0x67C00 + 16384*SliceId + 32*StsId + 4*VtgId + VtId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23):
           + $VtgId(0-6):
           + $VtId(0-3)
Reg Desc   : 
This register is applicable to see the Force RFI VT Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_RFIvtsta_Base                                                                     0x67C00
#define cReg_upen_RFIvtsta(SliceId, StsId, VtgId, VtId)            (0x67C00UL+16384UL*(SliceId)+32UL*(StsId)+4UL*(VtgId)+(VtId))
#define cReg_upen_RFIvtsta_WidthVal                                                                      32

/*--------------------------------------
BitField Name: staRFIvterr_stepcnt
BitField Type: RW
BitField Desc: The Event RFI VT Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_RFIvtsta_staRFIvterr_stepcnt_Mask                                                  cBit29_22
#define c_upen_RFIvtsta_staRFIvterr_stepcnt_Shift                                                        22

/*--------------------------------------
BitField Name: staRFIvtmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask RFI VT Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_RFIvtsta_staRFIvtmsk_poscnt_Mask                                                   cBit21_18
#define c_upen_RFIvtsta_staRFIvtmsk_poscnt_Shift                                                            18

/*--------------------------------------
BitField Name: staRFIvterr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error RFI VT Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_RFIvtsta_staRFIvterr_cnt_Mask                                                       cBit17_2
#define c_upen_RFIvtsta_staRFIvterr_cnt_Shift                                                             2

/*--------------------------------------
BitField Name: staRFIvtfrc_err
BitField Type: RW
BitField Desc: RFI VT Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_RFIvtsta_staRFIvtfrc_err_Mask                                                          cBit1
#define c_upen_RFIvtsta_staRFIvtfrc_err_Shift                                                             1

/*--------------------------------------
BitField Name: staRFIvtosecenb
BitField Type: RW
BitField Desc: Force RFI VT in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RFIvtsta_staRFIvtosecenb_Mask                                                          cBit0
#define c_upen_RFIvtsta_staRFIvtosecenb_Shift                                                             0


/*------------------------------------------------------------------------------
Reg Name   : Configuration n second to force in VT
Reg Addr   : 0x67c01-0x67c01
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure n second in  VT

------------------------------------------------------------------------------*/
#define cReg_upennsecvt                                                                                0x67c01

/*--------------------------------------
BitField Name: cfgnsecvt
BitField Type: RW
BitField Desc: The second number in VT Congiguration
BitField Bits: [31:0]
--------------------------------------*/
#define c_upennsecvt_cfgnsecvt_Mask                                                                cBit31_0
#define c_upennsecvt_cfgnsecvt_Shift                                                                      0

/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer STS Error Configuration
Reg Addr   : 0x23200-0x2321F
Reg Formula: 0x23200 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force Loss Of Pointer STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_LOPstscfg                                                                            0x23200
#define cReg_upen_LOPstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: lopstserr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force LOP STS
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstserr_step_Mask                                                          cBit11_4
#define c_upen_LOPstscfg_lopstserr_step_Shift                                                                4

/*--------------------------------------
BitField Name: lopstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  LOP STS
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstsmsk_pos_Mask_01                                                       cBit31_20
#define c_upen_LOPstscfg_lopstsmsk_pos_Shift_01                                                             20
#define c_upen_LOPstscfg_lopstsmsk_pos_Mask_02                                                         cBit3_0
#define c_upen_LOPstscfg_lopstsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: lopstsmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For LOP STS
BitField Bits: [19]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstsmsk_dat_Mask                                                             cBit19
#define c_upen_LOPstscfg_lopstsmsk_dat_Shift                                                                19

/*--------------------------------------
BitField Name: lopstserr_num
BitField Type: RW
BitField Desc: The total LOP STS Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstserr_num_Mask                                                           cBit18_3
#define c_upen_LOPstscfg_lopstserr_num_Shift                                                                 3

/*--------------------------------------
BitField Name: lopstsfrc_mod
BitField Type: RW
BitField Desc: Force LOP STS t Mode 000:N error� (N = 16bits),N in (Tms)� (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_LOPstscfg_lopstsfrc_mod_Mask                                                            cBit2_0
#define c_upen_LOPstscfg_lopstsfrc_mod_Shift                                                                 0

#define c_upen_LOPstscfg_lopsts_msk_pos_Mask                                                               cBit31_16
#define c_upen_LOPstscfg_lopsts_msk_pos_Shift                                                                     16
#define c_upen_LOPstscfg_lopsts_sec_num_Mask                                                                cBit15_8
#define c_upen_LOPstscfg_lopsts_sec_num_Shift                                                                      8
#define c_upen_LOPstscfg_lopsts_evtin1s_Mask                                                                 cBit7_4
#define c_upen_LOPstscfg_lopsts_evtin1s_Shift                                                                      4
#define c_upen_LOPstscfg_lopsts_frc_ena_Mask                                                                   cBit3
#define c_upen_LOPstscfg_lopsts_frc_ena_Shift                                                                      3
#define c_upen_LOPstscfg_lopsts_frc_mod_Mask                                                                 cBit2_0
#define c_upen_LOPstscfg_lopsts_frc_mod_Shift                                                                      0

/*------------------------------------------------------------------------------
Reg Name   : Force Loss Of Pointer STS STS Error Status
Reg Addr   : 0x23220-0x2323F
Reg Formula: 0x23220 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force Loss Of Pointer STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_LOPstssta                                                                            0x23220
#define cReg_upen_LOPstssta_WidthVal                                                                        32

/*--------------------------------------
BitField Name: LOPsts_mskposst
BitField Type: RW
BitField Desc: The Position Index LOP STS Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_LOPstssta_LOPsts_mskposst_Mask                                                        cBit20_17
#define c_upen_LOPstssta_LOPsts_mskposst_Shift                                                              17

/*--------------------------------------
BitField Name: LOPsts_seccntst
BitField Type: RW
BitField Desc: The Event Counter LOP STS Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_LOPstssta_LOPsts_seccntst_Mask                                                         cBit16_9
#define c_upen_LOPstssta_LOPsts_seccntst_Shift                                                               9

/*--------------------------------------
BitField Name: LOPstsmseccntst
BitField Type: RW
BitField Desc: The Second Counter LOP STS Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_LOPstssta_LOPstsmseccntst_Mask                                                          cBit8_5
#define c_upen_LOPstssta_LOPstsmseccntst_Shift                                                               5

/*--------------------------------------
BitField Name: LOPsts_updcntst
BitField Type: RW
BitField Desc: Updated LOP Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_LOPstssta_LOPsts_updcntst_Mask                                                          cBit4_3
#define c_upen_LOPstssta_LOPsts_updcntst_Shift                                                               3

/*--------------------------------------
BitField Name: LOPststimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond LOP STS Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_LOPstssta_LOPststimmsecst_Mask                                                            cBit2
#define c_upen_LOPstssta_LOPststimmsecst_Shift                                                               2

/*--------------------------------------
BitField Name: LOPststim_secst
BitField Type: RW
BitField Desc: Posedge Second LOP STS Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_LOPstssta_LOPststim_secst_Mask                                                            cBit1
#define c_upen_LOPstssta_LOPststim_secst_Shift                                                               1

/*--------------------------------------
BitField Name: LOPstsosecenbst
BitField Type: RW
BitField Desc: Force Loss Of Pointer STS  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_LOPstssta_LOPstsosecenbst_Mask                                                            cBit0
#define c_upen_LOPstssta_LOPstsosecenbst_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS STS Error Configuration
Reg Addr   : 0x23240-0x2325F
Reg Formula: 0x23240 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force AIS STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_AISstscfg                                                                            0x23240
#define cReg_upen_AISstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: AISstsmsk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 event AIS STS
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_AISstscfg_AISstsmsk_pos_Mask                                                          cBit30_15
#define c_upen_AISstscfg_AISstsmsk_pos_Shift                                                                15

/*--------------------------------------
BitField Name: AISstssec_num
BitField Type: RW
BitField Desc: The  T second to force alarm  Loss Of Pointer STS
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_AISstscfg_AISstssec_num_Mask                                                           cBit14_7
#define c_upen_AISstscfg_AISstssec_num_Shift                                                                 7

/*--------------------------------------
BitField Name: AISstsestsin1s
BitField Type: RW
BitField Desc: The event  Loss Of Pointer STS in one second
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_AISstscfg_AISstsestsin1s_Mask                                                           cBit6_3
#define c_upen_AISstscfg_AISstsestsin1s_Shift                                                                3

/*--------------------------------------
BitField Name: AISstsfrc_ena
BitField Type: RW
BitField Desc: Force Alarm Loss Of Pointer STS Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_AISstscfg_AISstsfrc_ena_Mask                                                              cBit2
#define c_upen_AISstscfg_AISstsfrc_ena_Shift                                                                 2

/*--------------------------------------
BitField Name: AISstsfrc_mod
BitField Type: RW
BitField Desc: Force Loss Of Pointer STS in One second Mode 00: Set Alarm in T
second 01: Set or clear 1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_AISstscfg_AISstsfrc_mod_Mask                                                            cBit1_0
#define c_upen_AISstscfg_AISstsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS STS Error Status
Reg Addr   : 0x23260-0x2327F
Reg Formula: 0x23260 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force AIS STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_AISstssta                                                                            0x23260
#define cReg_upen_AISstssta_WidthVal                                                                        32

/*--------------------------------------
BitField Name: AISsts_mskposst
BitField Type: RW
BitField Desc: The Position Index AIS STS Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_AISstssta_AISsts_mskposst_Mask                                                        cBit20_17
#define c_upen_AISstssta_AISsts_mskposst_Shift                                                              17

/*--------------------------------------
BitField Name: AISsts_seccntst
BitField Type: RW
BitField Desc: The Event Counter AIS STS Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_AISstssta_AISsts_seccntst_Mask                                                         cBit16_9
#define c_upen_AISstssta_AISsts_seccntst_Shift                                                               9

/*--------------------------------------
BitField Name: AISstsmseccntst
BitField Type: RW
BitField Desc: The Second Counter AIS STS Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_AISstssta_AISstsmseccntst_Mask                                                          cBit8_5
#define c_upen_AISstssta_AISstsmseccntst_Shift                                                               5

/*--------------------------------------
BitField Name: AISsts_updcntst
BitField Type: RW
BitField Desc: Updated AIS Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_AISstssta_AISsts_updcntst_Mask                                                          cBit4_3
#define c_upen_AISstssta_AISsts_updcntst_Shift                                                               3

/*--------------------------------------
BitField Name: AISststimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond AIS STS Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_AISstssta_AISststimmsecst_Mask                                                            cBit2
#define c_upen_AISstssta_AISststimmsecst_Shift                                                               2

/*--------------------------------------
BitField Name: AISststim_secst
BitField Type: RW
BitField Desc: Posedge Second AIS STS Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_AISstssta_AISststim_secst_Mask                                                            cBit1
#define c_upen_AISstssta_AISststim_secst_Shift                                                               1

/*--------------------------------------
BitField Name: AISstsosecenbst
BitField Type: RW
BitField Desc: Force Loss Of Pointer STS  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_AISstssta_AISstsosecenbst_Mask                                                            cBit0
#define c_upen_AISstssta_AISstsosecenbst_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ STS Error Configuration
Reg Addr   : 0x23280-0x2329F
Reg Formula: 0x23280 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force UEQ STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_UEQstscfg                                                                            0x23280
#define cReg_upen_UEQstscfg_WidthVal                                                                        32

/*--------------------------------------
BitField Name: UEQstsmsk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 event UEQ STS
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_UEQstscfg_UEQstsmsk_pos_Mask                                                          cBit30_15
#define c_upen_UEQstscfg_UEQstsmsk_pos_Shift                                                                15

/*--------------------------------------
BitField Name: UEQstssec_num
BitField Type: RW
BitField Desc: The  T second to force alarm  UEQ STS
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_UEQstscfg_UEQstssec_num_Mask                                                           cBit14_7
#define c_upen_UEQstscfg_UEQstssec_num_Shift                                                                 7

/*--------------------------------------
BitField Name: UEQstsestsin1s
BitField Type: RW
BitField Desc: The event  UEQ STS in one second
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_UEQstscfg_UEQstsestsin1s_Mask                                                           cBit6_3
#define c_upen_UEQstscfg_UEQstsestsin1s_Shift                                                                3

/*--------------------------------------
BitField Name: UEQstsfrc_ena
BitField Type: RW
BitField Desc: Force Alarm UEQ STS Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_UEQstscfg_UEQstsfrc_ena_Mask                                                              cBit2
#define c_upen_UEQstscfg_UEQstsfrc_ena_Shift                                                                 2

/*--------------------------------------
BitField Name: UEQstsfrc_mod
BitField Type: RW
BitField Desc: Force UEQ STS in One second Mode 00: Set Alarm in T second 01:
Set or clear 1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_UEQstscfg_UEQstsfrc_mod_Mask                                                            cBit1_0
#define c_upen_UEQstscfg_UEQstsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force UEQ STS Error Status
Reg Addr   : 0x232a0-0x232bF
Reg Formula: 0x232a0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force UEQ STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_UEQstssta                                                                            0x232a0
#define cReg_upen_UEQstssta_WidthVal                                                                        32

/*--------------------------------------
BitField Name: UEQsts_mskposst
BitField Type: RW
BitField Desc: The Position Index UEQ STS Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_UEQstssta_UEQsts_mskposst_Mask                                                        cBit20_17
#define c_upen_UEQstssta_UEQsts_mskposst_Shift                                                              17

/*--------------------------------------
BitField Name: UEQsts_seccntst
BitField Type: RW
BitField Desc: The Event Counter UEQ STS Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_UEQstssta_UEQsts_seccntst_Mask                                                         cBit16_9
#define c_upen_UEQstssta_UEQsts_seccntst_Shift                                                               9

/*--------------------------------------
BitField Name: UEQstsmseccntst
BitField Type: RW
BitField Desc: The Second Counter UEQ STS Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_UEQstssta_UEQstsmseccntst_Mask                                                          cBit8_5
#define c_upen_UEQstssta_UEQstsmseccntst_Shift                                                               5

/*--------------------------------------
BitField Name: UEQsts_updcntst
BitField Type: RW
BitField Desc: Updated UEQ Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_UEQstssta_UEQsts_updcntst_Mask                                                          cBit4_3
#define c_upen_UEQstssta_UEQsts_updcntst_Shift                                                               3

/*--------------------------------------
BitField Name: UEQststimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond UEQ STS Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_UEQstssta_UEQststimmsecst_Mask                                                            cBit2
#define c_upen_UEQstssta_UEQststimmsecst_Shift                                                               2

/*--------------------------------------
BitField Name: UEQststim_secst
BitField Type: RW
BitField Desc: Posedge Second UEQ STS Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_UEQstssta_UEQststim_secst_Mask                                                            cBit1
#define c_upen_UEQstssta_UEQststim_secst_Shift                                                               1

/*--------------------------------------
BitField Name: UEQstsosecenbst
BitField Type: RW
BitField Desc: Force UEQ STS  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_UEQstssta_UEQstsosecenbst_Mask                                                            cBit0
#define c_upen_UEQstssta_UEQstsosecenbst_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP STS Error Configuration
Reg Addr   : 0x232c0-0x232dF
Reg Formula: 0x232c0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force BIP STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_BIPstscfg                                                                            0x232c0
#define cReg_upen_BIPstscfg_WidthVal                                                                     64

/*--------------------------------------
BitField Name: cfgBIPstserr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force BIP STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_BIPstscfg_cfgBIPstserr_step_Mask                                                     cBit9_2
#define c_upen_BIPstscfg_cfgBIPstserr_step_Shift                                                          2

/*--------------------------------------
BitField Name: cfgBIPstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  BIP STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_BIPstscfg_cfgBIPstsmsk_pos_Mask_01                                                 cBit31_18
#define c_upen_BIPstscfg_cfgBIPstsmsk_pos_Shift_01                                                       18
#define c_upen_BIPstscfg_cfgBIPstsmsk_pos_Mask_02                                                   cBit1_0
#define c_upen_BIPstscfg_cfgBIPstsmsk_pos_Shift_02                                                        0

/*--------------------------------------
BitField Name: cfgBIPstsmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  BIP STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_BIPstscfg_cfgBIPstsmsk_dat_Mask                                                       cBit17
#define c_upen_BIPstscfg_cfgBIPstsmsk_dat_Shift                                                          17

/*--------------------------------------
BitField Name: cfgBIPstserr_num
BitField Type: RW
BitField Desc: The Total BIP STS Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_BIPstscfg_cfgBIPstserr_num_Bit_Start                                                       1
#define c_upen_BIPstscfg_cfgBIPstserr_num_Bit_End                                                        16
#define c_upen_BIPstscfg_cfgBIPstserr_num_Mask                                                     cBit16_1
#define c_upen_BIPstscfg_cfgBIPstserr_num_Shift                                                           1

/*--------------------------------------
BitField Name: cfgBIPstsfrc_1sen
BitField Type: RW
BitField Desc: Force BIP STS in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPstscfg_cfgBIPstsfrc_1sen_Mask                                                       cBit0
#define c_upen_BIPstscfg_cfgBIPstsfrc_1sen_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP STS Error Status
Reg Addr   : 0x232e0-0x232FF
Reg Formula: 0x232e0 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force BIP STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_BIPstssta                                                                            0x232e0
#define cReg_upen_BIPstssta_WidthVal                                                                     32

/*--------------------------------------
BitField Name: staBIPstserr_stepcnt
BitField Type: RW
BitField Desc: The Event BIP STS Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_BIPstssta_staBIPstserr_stepcnt_Mask                                                cBit29_22
#define c_upen_BIPstssta_staBIPstserr_stepcnt_Shift                                                      22

/*--------------------------------------
BitField Name: staBIPstsmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask BIP STS Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_BIPstssta_staBIPstsmsk_poscnt_Mask                                                 cBit21_18
#define c_upen_BIPstssta_staBIPstsmsk_poscnt_Shift                                                       18

/*--------------------------------------
BitField Name: staBIPstserr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error BIP STS Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_BIPstssta_staBIPstserr_cnt_Mask                                                     cBit17_2
#define c_upen_BIPstssta_staBIPstserr_cnt_Shift                                                           2

/*--------------------------------------
BitField Name: staBIPstsfrc_err
BitField Type: RW
BitField Desc: BIP STS Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_BIPstssta_staBIPstsfrc_err_Mask                                                        cBit1
#define c_upen_BIPstssta_staBIPstsfrc_err_Shift                                                           1

/*--------------------------------------
BitField Name: staBIPstsosecenb
BitField Type: RW
BitField Desc: Force BIP STS in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_BIPstssta_staBIPstsosecenb_Mask                                                        cBit0
#define c_upen_BIPstssta_staBIPstsosecenb_Shift                                                           0


/*------------------------------------------------------------------------------
Reg Name   : Force REI STS Error Configuration
Reg Addr   : 0x23300-0x2331F
Reg Formula: 0x23300 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force REI STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_REIstscfg                                                                            0x23300
#define cReg_upen_REIstscfg_WidthVal                                                                     64

/*--------------------------------------
BitField Name: cfgREIstserr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force REI STS Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_REIstscfg_cfgREIstserr_step_Mask                                                     cBit9_2
#define c_upen_REIstscfg_cfgREIstserr_step_Shift                                                          2

/*--------------------------------------
BitField Name: cfgREIstsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  REI STS Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_REIstscfg_cfgREIstsmsk_pos_Mask_01                                                 cBit31_18
#define c_upen_REIstscfg_cfgREIstsmsk_pos_Shift_01                                                       18
#define c_upen_REIstscfg_cfgREIstsmsk_pos_Mask_02                                                   cBit1_0
#define c_upen_REIstscfg_cfgREIstsmsk_pos_Shift_02                                                        0

/*--------------------------------------
BitField Name: cfgREIstsmsk_dat
BitField Type: RW
BitField Desc: The Data Mask Fpr  REI STS Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_REIstscfg_cfgREIstsmsk_dat_Mask                                                       cBit17
#define c_upen_REIstscfg_cfgREIstsmsk_dat_Shift                                                          17

/*--------------------------------------
BitField Name: cfgREIstserr_num
BitField Type: RW
BitField Desc: The Total REI STS Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_REIstscfg_cfgREIstserr_num_Mask                                                     cBit16_1
#define c_upen_REIstscfg_cfgREIstserr_num_Shift                                                           1

/*--------------------------------------
BitField Name: cfgREIstsfrc_1sen
BitField Type: RW
BitField Desc: Force REI STS in One second Enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIstscfg_cfgREIstsfrc_1sen_Mask                                                       cBit0
#define c_upen_REIstscfg_cfgREIstsfrc_1sen_Shift                                                          0


/*------------------------------------------------------------------------------
Reg Name   : Force REI STS Error Status
Reg Addr   : 0x23320-0x2333F
Reg Formula: 0x23320 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force REI STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_REIstssta                                                                            0x23320
#define cReg_upen_REIstssta_WidthVal                                                                     32

/*--------------------------------------
BitField Name: staREIstserr_stepcnt
BitField Type: RW
BitField Desc: The Event REI STS Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_REIstssta_staREIstserr_stepcnt_Mask                                                cBit29_22
#define c_upen_REIstssta_staREIstserr_stepcnt_Shift                                                      22

/*--------------------------------------
BitField Name: staREIstsmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask REI STS Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_REIstssta_staREIstsmsk_poscnt_Mask                                                 cBit21_18
#define c_upen_REIstssta_staREIstsmsk_poscnt_Shift                                                       18

/*--------------------------------------
BitField Name: staREIstserr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error REI STS Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_REIstssta_staREIstserr_cnt_Mask                                                     cBit17_2
#define c_upen_REIstssta_staREIstserr_cnt_Shift                                                           2

/*--------------------------------------
BitField Name: staREIstsfrc_err
BitField Type: RW
BitField Desc: REI STS Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_REIstssta_staREIstsfrc_err_Mask                                                        cBit1
#define c_upen_REIstssta_staREIstsfrc_err_Shift                                                           1

/*--------------------------------------
BitField Name: staREIstsosecenb
BitField Type: RW
BitField Desc: Force REI STS in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_REIstssta_staREIstsosecenb_Mask                                                        cBit0
#define c_upen_REIstssta_staREIstsosecenb_Shift                                                           0

#define cReg_upen_LOMstscfg                                                                            0x23380
#define cReg_upen_LOMstssta                                                                            0x23360
/*------------------------------------------------------------------------------
Reg Name   : Force RDI STS Error Configuration
Reg Addr   : 0x23340-0x2335F
Reg Formula: 0x23340 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to configure Force RDI STS Error for STS-1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_RDIstscfg                                                                            0x23340
#define cReg_upen_RDIstscfg_WidthVal                                                                        64

/*--------------------------------------
BitField Name: rdistserr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force rdi STS
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_RDIstscfg_rdistserr_step_Mask                                                          cBit11_4
#define c_upen_RDIstscfg_rdistserr_step_Shift                                                                4

/*--------------------------------------
BitField Name: rdistsmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  rdi STS
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_RDIstscfg_rdistsmsk_pos_Mask_01                                                       cBit31_20
#define c_upen_RDIstscfg_rdistsmsk_pos_Shift_01                                                             20
#define c_upen_RDIstscfg_rdistsmsk_pos_Mask_02                                                         cBit3_0
#define c_upen_RDIstscfg_rdistsmsk_pos_Shift_02                                                              0

/*--------------------------------------
BitField Name: rdistsmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For rdi STS
BitField Bits: [19]
--------------------------------------*/
#define c_upen_RDIstscfg_rdistsmsk_dat_Mask                                                             cBit19
#define c_upen_RDIstscfg_rdistsmsk_dat_Shift                                                                19

/*--------------------------------------
BitField Name: rdistserr_num
BitField Type: RW
BitField Desc: The total rdi STS Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_RDIstscfg_rdistserr_num_Mask                                                           cBit18_3
#define c_upen_RDIstscfg_rdistserr_num_Shift                                                                 3

/*--------------------------------------
BitField Name: rdistsfrc_mod
BitField Type: RW
BitField Desc: Force rdi STS t Mode 000:N error� (N = 16bits),N in (Tms)� (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_RDIstscfg_rdistsfrc_mod_Mask                                                            cBit2_0
#define c_upen_RDIstscfg_rdistsfrc_mod_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI STS Error Status
Reg Addr   : 0x23360-0x2337F
Reg Formula: 0x23360 + 512*SliceId + StsId
    Where  : 
           + $SliceId(0-1): 0: Ec1Id: 0.1..23, 1: Ec1Id: 24.25..47
           + $StsId(0-23)
Reg Desc   : 
This register is applicable to see the Force RDI STS Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_RDIstssta                                                                            0x23360
#define cReg_upen_RDIstssta_WidthVal                                                                     32

/*--------------------------------------
BitField Name: RDIsts_mskposst
BitField Type: RW
BitField Desc: The Position Index RDI STS Counter Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_RDIstssta_RDIsts_mskposst_Mask                                                        cBit20_17
#define c_upen_RDIstssta_RDIsts_mskposst_Shift                                                              17

/*--------------------------------------
BitField Name: RDIsts_seccntst
BitField Type: RW
BitField Desc: The Event Counter RDI STS Counter Status
BitField Bits: [16:9]
--------------------------------------*/
#define c_upen_RDIstssta_RDIsts_seccntst_Mask                                                         cBit16_9
#define c_upen_RDIstssta_RDIsts_seccntst_Shift                                                               9

/*--------------------------------------
BitField Name: RDIstsmseccntst
BitField Type: RW
BitField Desc: The Second Counter RDI STS Status
BitField Bits: [8:5]
--------------------------------------*/
#define c_upen_RDIstssta_RDIstsmseccntst_Mask                                                          cBit8_5
#define c_upen_RDIstssta_RDIstsmseccntst_Shift                                                               5

/*--------------------------------------
BitField Name: RDIsts_updcntst
BitField Type: RW
BitField Desc: Updated RDI Status
BitField Bits: [4:3]
--------------------------------------*/
#define c_upen_RDIstssta_RDIsts_updcntst_Mask                                                          cBit4_3
#define c_upen_RDIstssta_RDIsts_updcntst_Shift                                                               3

/*--------------------------------------
BitField Name: RDIststimmsecst
BitField Type: RW
BitField Desc: Posedge MiliSecond RDI STS Status
BitField Bits: [2]
--------------------------------------*/
#define c_upen_RDIstssta_RDIststimmsecst_Mask                                                            cBit2
#define c_upen_RDIstssta_RDIststimmsecst_Shift                                                               2

/*--------------------------------------
BitField Name: RDIststim_secst
BitField Type: RW
BitField Desc: Posedge Second RDI STS Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_RDIstssta_RDIststim_secst_Mask                                                            cBit1
#define c_upen_RDIstssta_RDIststim_secst_Shift                                                               1

/*--------------------------------------
BitField Name: RDIstsosecenbst
BitField Type: RW
BitField Desc: Force RDI STS  Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_RDIstssta_RDIstsosecenbst_Mask                                                            cBit0
#define c_upen_RDIstssta_RDIstsosecenbst_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Configuration n second to force in STS
Reg Addr   : 0xa233e1-0xa233e1
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure n second in  STS

------------------------------------------------------------------------------*/
#define cReg_upennsecsts                                                                              0xa233e1

/*--------------------------------------
BitField Name: cfgnsecsts
BitField Type: RW
BitField Desc: The second number in STS Congiguration
BitField Bits: [31:0]
--------------------------------------*/
#define c_upennsecsts_cfgnsecsts_Mask                                                                 cBit31_0
#define c_upennsecsts_cfgnsecsts_Shift                                                                       0



#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031SDHLINEATTREG_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6A210031SdhAuInternal.h
 * 
 * Created Date: Sep 8, 2017
 *
 * Description : SDH Au internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031SDHAUINTERNAL_H_
#define _THA6A210031SDHAUINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011Tfi5LineAuInternal.h"
#include "Tha6A210031SdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A210031SdhAu
    {
    tTha60210011Tfi5LineAu super;

    /* Private data */
    AtAttController attController;
    }tTha6A210031SdhAu;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSdhAu Tha6A210031SdhAuObjectInit(AtSdhAu self, uint32 channelId, uint8 channelType, AtModuleSdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031SDHAUINTERNAL_H_ */


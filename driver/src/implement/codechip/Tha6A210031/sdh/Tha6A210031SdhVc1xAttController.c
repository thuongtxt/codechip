/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : Tha6A210031SdhVc1xAttController.c
 *
 * Created Date: Jul 19, 2016
 *
 * Description : VC1x ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhPath.h"
#include "AtSdhChannel.h"
#include "../../../default/ocn/ThaModuleOcnInternal.h"
#include "../../../default/sdh/ThaSdhVc.h"
#include "../../../default/man/ThaDevice.h"
#include "../../Tha60210011/ocn/Tha60210011ModuleOcn.h"
#include "../pdh/Tha6A210031PdhAttControllerInternal.h"
#include "Tha6A210031SdhAttReg.h"
#include "Tha6A210031SdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttControllerMethods                m_AtAttControllerOverride;
static tThaAttControllerMethods               m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;

/* Save super implementation */
static const tAtAttControllerMethods *m_AtAttControllerMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static uint32 StartVersionSupportPointerAdj(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 0xFFFFFFFF;
    }

static uint8  NumPointerAdjTypes(Tha6A210031PdhDe3AttController self)
	{
	AtUnused(self);
	return 2;
	}

static uint32 MinStep(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return 1;
    }

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x9, 0x4000);
    }


static eBool ErrorIsFromAlarm(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtFalse;
    }

static uint32 HwStepMaskHo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        return c_upen_BIPvtcfg_cfgBIPvterr_step_Mask;
        }
    else if (isError == cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask2bit((ThaAttController)self, errorType))
            return cBit10_3;
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit16_9;
        return c_upen_BIPvtcfg_cfgBIPvterr_step_Mask;
        }
    return c_upen_BIPvtcfg_cfgBIPvterr_step_Mask;
    }

static uint32 HwStepMaskLo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(isError);
    return cBit31_0;
    }

static uint32 PositionMaskConfigurationMaskHo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError ==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Mask_02;

        if (mMethodsGet(self)->AlarmIsFromError(self, errorType))
            return c_upen_BIPstscfg_cfgBIPstsmsk_pos_Mask_02;
        return 0;
        }
    else if (isError == cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask2bit((ThaAttController)self, errorType))
            return cBit2_0;
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
           return cBit8_0;
        return c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Mask_02;
        }
    return c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Mask_02;
    }

static uint32 PositionMaskConfigurationMaskLo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Mask_01;

        if (mMethodsGet(self)->AlarmIsFromError(self, errorType))
            return c_upen_LOPvtcfg_lopvt_msk_pos_Mask;
        return c_upen_AISvtcfg_AISvtmsk_pos_Mask;
        }
    else if (isError == cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask2bit((ThaAttController)self, errorType))
            return cBit31_19;
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit31_25;
        return c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Mask_01;
        }
    return c_upen_BIPvtcfg_cfgBIPvtmsk_pos_Mask_01;
    }

static uint32 DataMaskConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_BIPvtcfg_cfgBIPvtmsk_dat_Mask;

        if (mMethodsGet(self)->AlarmIsFromError(self, errorType))
            return c_upen_LOPvtcfg_lopvt_frc_ena_Mask;
        return c_upen_AISvtcfg_AISvtfrc_ena_Mask;
        }
    else if (isError == cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask2bit((ThaAttController)self, errorType))
            return cBit18_17;
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit24_17;
        return c_upen_BIPvtcfg_cfgBIPvtmsk_dat_Mask;/* 1 bit [17] */
        }
    return c_upen_BIPvtcfg_cfgBIPvtmsk_dat_Mask;
    }

static uint32 NumberOfErrorsMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_BIPvtcfg_cfgBIPvterr_num_Mask;

        if (mMethodsGet(self)->AlarmIsFromError(self, errorType))
            return c_upen_LOPvtcfg_lopvt_evtin1s_Mask;
        return c_upen_AISvtcfg_AISvtevtin1s_Mask;
        }

    return c_upen_BIPvtcfg_cfgBIPvterr_num_Mask;
    }

static uint32 DurationConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return 0;

        if (mMethodsGet(self)->AlarmIsFromError(self, errorType))
            return c_upen_LOPvtcfg_lopvt_sec_num_Mask;
        return c_upen_AISvtcfg_AISvtsec_num_Mask;
        }
    return 0;
    }

static uint32 HwForceModeConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_BIPvtcfg_cfgBIPvtfrc_1sen_Mask;

        if (mMethodsGet(self)->AlarmIsFromError(self, errorType))
            return c_upen_LOPvtcfg_lopvt_frc_mod_Mask;
        return c_upen_AISvtcfg_AISvtfrc_mod_Mask;
        }

    return c_upen_BIPvtcfg_cfgBIPvtfrc_1sen_Mask;
    }

static uint8  HwRdiType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);

    switch (alarmType)
        {
        case cAtSdhPathAlarmRdi:return cBit0;
        case cAtSdhPathAlarmErdiC:return cBit1;
        case cAtSdhPathAlarmErdiS:return cBit2;
        case cAtSdhPathAlarmErdiP:return cBit3;
        default:
            return 0;
        }
    }

static uint32 RdiTypeMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError==cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return cBit31_28;
        if (mMethodsGet(self)->AlarmIsFromError(self, errorType))
            return cBit3_0;
        }
    return 0;
    }

/*static eBool IsAlarmForceV2(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if (alarmType ==cAtSdhPathAlarmLop)
        return cAtTrue;
    return cAtTrue;
    }*/

static eBool AlarmIsSpecialRdi(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if  (   alarmType == cAtSdhPathAlarmRdi   ||
            alarmType == cAtSdhPathAlarmErdiC ||
            alarmType == cAtSdhPathAlarmErdiS ||
            alarmType == cAtSdhPathAlarmErdiP)
        return cAtTrue;
    return cAtFalse;
    }

static eBool  AlarmIsFromError(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if  (   mMethodsGet(self)->AlarmIsSpecialRdi(self, alarmType)||
            alarmType==cAtSdhPathAlarmLop ||
            alarmType==cAtSdhPathAlarmRfi)
        return cAtTrue;
    return cAtFalse;
    }


static uint32 RegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
            return cReg_upen_AISvtcfg;
        case cAtSdhPathAlarmLop:
            return cReg_upen_LOPvtcfg;
        case cAtSdhPathAlarmUneq:
            return cReg_upen_UEQvtcfg_Base;
        case cAtSdhPathAlarmRfi:
            return cReg_upen_RFIvtcfg;
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiC:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
            return cReg_upen_RDIvtcfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathAlarmAis:
            return cReg_upen_AISvtsta_Base;
        case cAtSdhPathAlarmLop:
            return cReg_upen_LOPvtsta;
        case cAtSdhPathAlarmUneq:
            return cReg_upen_UEQvtsta;
        case cAtSdhPathAlarmRfi:
            return cReg_upen_RFIvtsta_Base;
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiC:
        case cAtSdhPathAlarmErdiS:
        case cAtSdhPathAlarmErdiP:
            return cReg_upen_RDIvtsta;
        default:
            return cBit31_0;
        }
    }


static eAtRet CheckAlarmNumEvent(Tha6A210031PdhDe3AttController self, uint32 alarmType, uint32 numEvent)
	{
	AtUnused(self);
	AtUnused(alarmType);
	AtUnused(numEvent);
	return cAtOk;
	}

static eAtRet SpecificAlarmConfig(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtOk;
    }

static uint8 AlarmTypeIndex(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
            return 0;
        case cAtSdhPathAlarmLop:
            return 1;
        case cAtSdhPathAlarmUneq:
            return 2;
        case cAtSdhPathAlarmRfi:
            return 3;
        case cAtSdhPathAlarmRdi:
            return 4;
        case cAtSdhPathAlarmErdiC:
            return 5;
        case cAtSdhPathAlarmErdiP:
            return 6;
        case cAtSdhPathAlarmErdiS:
            return 7;
        default:
            return 0;
        }
    }

static uint8 NumAlarmTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 8;
    }

static uint32 RealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwStsInSlice;
    uint32 vtgId = 0, vtId = 0;
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);

    if ((channelType == cAtSdhChannelTypeTu12) || (channelType == cAtSdhChannelTypeTu11))
        {
        vtId = AtChannelIdGet((AtChannel)channel);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(channel));
        }
    else if ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11))
        {
        AtSdhChannel tu1x = AtSdhChannelParentChannelGet(channel);
        vtId = AtChannelIdGet((AtChannel)tu1x);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(tu1x));
        }
    
    if (ThaSdhChannel2HwMasterStsId(channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return regAddr  + (32768UL * slice) + (32UL*hwStsInSlice) + vtgId * 4UL + vtId + Tha60210011ModuleOcnBaseAddress(ocnModule);

    return cInvalidUint32;
    }

static uint32 PointerRealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwStsInSlice;
    uint32 vtgId = 0, vtId = 0;
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModuleOcn ocnModule = (ThaModuleOcn)AtDeviceModuleGet(AtChannelDeviceGet((AtChannel)channel), cThaModuleOcn);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(channel);

    if ((channelType == cAtSdhChannelTypeTu12) || (channelType == cAtSdhChannelTypeTu11))
        {
        vtId = AtChannelIdGet((AtChannel)channel);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(channel));
        }
    else if ((channelType == cAtSdhChannelTypeVc12) || (channelType == cAtSdhChannelTypeVc11))
        {
        AtSdhChannel tu1x = AtSdhChannelParentChannelGet(channel);
        vtId = AtChannelIdGet((AtChannel)tu1x);
        vtgId = AtChannelIdGet((AtChannel)AtSdhChannelParentChannelGet(tu1x));
        }

    if (ThaSdhChannel2HwMasterStsId(channel, cThaModulePoh, &slice, &hwStsInSlice) == cAtOk)
        return regAddr  + (32768UL * slice) + (32UL*hwStsInSlice) + vtgId * 4UL + vtId + Tha60210011ModuleOcnBaseAddress(ocnModule);

    return cInvalidUint32;
    }

static eAtRet InitErrorDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i;
    eAtRet ret = cAtOk;
    static const uint32 cAtSdhPathErrorTypes[] = {cAtSdhPathErrorBip, cAtSdhPathErrorRei};


    for (i = 0; i < mCount(cAtSdhPathErrorTypes); i++)
        {
        if (AtAttControllerErrorForceIsSupported((AtAttController)self, cAtSdhPathErrorTypes[i]))
            {
            ret |= AtAttControllerUnForceError((AtAttController)self, cAtSdhPathErrorTypes[i]);
            ret |= AtAttControllerForceErrorModeSet((AtAttController)self, cAtSdhPathErrorTypes[i],cAtAttForceErrorModeContinuous);
            ret |= AtAttControllerForceErrorNumErrorsSet((AtAttController)self, cAtSdhPathErrorTypes[i], 1);
            ret |= AtAttControllerForceErrorBitPositionSet((AtAttController)self, cAtSdhPathErrorTypes[i], cBit31_0);
            ret |= AtAttControllerForceErrorDataMaskSet((AtAttController)self, cAtSdhPathErrorTypes[i], 1);
            }
        }


    return ret;
    }
static eAtRet InitAlarmDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i;
    eAtRet ret = cAtOk;
    static const uint32 cAtSdhPathAlarmTypes[] = {cAtSdhPathAlarmAis, cAtSdhPathAlarmLop, cAtSdhPathAlarmUneq};
    for (i = 0; i < mCount(cAtSdhPathAlarmTypes); i++)
       {
       if (AtAttControllerAlarmForceIsSupported((AtAttController)self, cAtSdhPathAlarmTypes[i]))
           {
           ret |= AtAttControllerUnForceAlarm((AtAttController)self, cAtSdhPathAlarmTypes[i]);
           ret |= AtAttControllerForceAlarmModeSet((AtAttController)self, cAtSdhPathAlarmTypes[i],cAtAttForceAlarmModeContinuous);
           ret |= AtAttControllerForceAlarmNumEventSet((AtAttController)self, cAtSdhPathAlarmTypes[i], 1);
           }
       }
    return ret;
    }
static eBool AlarmForceIsSupported(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
        case cAtSdhPathAlarmLop:
        case cAtSdhPathAlarmUneq:
        case cAtSdhPathAlarmRfi:
        case cAtSdhPathAlarmRdi:
        case cAtSdhPathAlarmErdiC:
        case cAtSdhPathAlarmErdiP:
        case cAtSdhPathAlarmErdiS:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint32 RegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathCounterTypeBip:
            return cReg_upen_BIPvtcfg_Base;
        case cAtSdhPathCounterTypeRei:
            return cReg_upen_REIvtcfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathCounterTypeBip:
            return cReg_upen_BIPvtsta;
        case cAtSdhPathCounterTypeRei:
            return cReg_upen_REIvtsta;
        default:
            return cBit31_0;
        }
    }

static uint8 ErrorTypeIndex(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathCounterTypeBip:
            return 0;
        case cAtSdhPathCounterTypeRei:
            return 1;
        default:
            return 0;
        }
    }

static eBool ErrorForceIsSupported(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtSdhPathCounterTypeBip:
        case cAtSdhPathCounterTypeRei:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static uint8 NumErrorTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 2;
    }

static uint16 Tha6A210031SdhVc1xAttControllerLongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    /* HW still is not available at VT */
#if 0
    uint8 timeretry = 200, count=0;
    uint16 ret = m_AtAttControllerMethods->LongRead(self, regAddr, dataBuffer, bufferLen);
    ret = m_AtAttControllerMethods->LongRead(self, cReg_upen_cpu_idr_st_Base, dataBuffer, bufferLen);
    while(!( dataBuffer[1] & c_upen_cpu_idr_st_val_vld_Mask))
        {
        count++;
        ret = m_AtAttControllerMethods->LongRead(self, cReg_upen_cpu_idr_st_Base, dataBuffer, bufferLen);
        if (count >= timeretry) break;
        }
    mFieldIns(&dataBuffer[1], c_upen_cpu_idr_st_val_vld_Mask, c_upen_cpu_idr_st_val_vld_Shift, 0);
    return ret;
#endif
    return m_AtAttControllerMethods->LongRead(self, regAddr, dataBuffer, bufferLen);
    }

static uint16 LongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    if (0x21000 ==(regAddr & 0xFF000))
        return m_AtAttControllerMethods->LongRead(self, regAddr, dataBuffer, bufferLen);
    else
    return Tha6A210031SdhVc1xAttControllerLongRead(self, regAddr, dataBuffer, bufferLen);
    }

static eBool IsVc11(AtSdhChannel self)
    {
    eAtSdhChannelType type = AtSdhChannelTypeGet(self);
    return (type == cAtSdhChannelTypeVc11);
    }

static AtSdhVc VcPathGet(ThaAttController self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    eAtSdhChannelType type = AtSdhChannelTypeGet(channel);
    if ((type == cAtSdhChannelTypeTu11) || (type == cAtSdhChannelTypeTu12))
        {
        return (AtSdhVc)AtSdhChannelSubChannelGet(channel, 0);
        }
    return (AtSdhVc)channel;
    }
static ThaCdrController CdrControllerGet(ThaAttController self)
    {
    AtSdhVc vcPath = VcPathGet(self);
    return ThaSdhVcCdrControllerGet(vcPath);
    }

static uint32 FrequenceOffsetBase(ThaAttController self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    return IsVc11(channel) ? cBaseNcoValVt15 : cBaseNcoValVt2;
    }

static uint32 FrequenceOffsetStep(ThaAttController self)
    {
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    return IsVc11(channel) ? cNcoValPpbOffsetVT15 : cNcoValPpbOffsetVT2;
    }

static eBool  RateCalibVc11(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;

    AtUnused(self);
    if (Nrate == cAtNRate1E3)
       {*err_step=1;      *PositionMask = 13;  *frc_en_mask=1; *hs=1;ret = cAtTrue;}
    if (Nrate == cAtNRate1E4)
       {*err_step=3;      *PositionMask = 2;   *frc_en_mask=2; *hs=1;ret = cAtTrue;}
    if (Nrate == cAtNRate1E5)
       {*err_step=15;     *PositionMask = 2;   *frc_en_mask=1; *hs=1;ret = cAtTrue;}
    if (Nrate == cAtNRate1E6)
       {*err_step=75;     *PositionMask = 1;   *frc_en_mask=1; *hs=1;ret = cAtTrue;}
    if (Nrate == cAtNRate1E7)
       {*err_step=75;     *PositionMask = 1;   *frc_en_mask=1; *hs=10;ret = cAtTrue;}
    if (Nrate == cAtNRate1E8)
       {*err_step=75/*7512*/;   *PositionMask = 1;   *frc_en_mask=1; *hs=100;ret = cAtTrue;}
    if (Nrate == cAtNRate1E9)
       {*err_step=751/*75120*/; *PositionMask = 1;   *frc_en_mask=1; *hs=100;ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalibVc12(ThaAttController self, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;

    AtUnused(self);
    if (Nrate == cAtNRate1E3)
       {*err_step=1;      *PositionMask = 9;  *frc_en_mask=2; *hs=1;ret = cAtTrue;}
    else if (Nrate == cAtNRate1E4)
       {*err_step=5;      *PositionMask = 9;  *frc_en_mask=1; *hs=1;ret = cAtTrue;}
    else if (Nrate == cAtNRate1E5)
       {*err_step=67;     *PositionMask = 6;  *frc_en_mask=2; *hs=1;ret = cAtTrue;}
    else if (Nrate == cAtNRate1E6)
       {*err_step=223;    *PositionMask = 2;  *frc_en_mask=2; *hs=1;ret = cAtTrue;}
    else if (Nrate == cAtNRate1E7)
       {*err_step=56 /*557*/;     *PositionMask = 1;  *frc_en_mask=1; *hs=10;ret = cAtTrue;}
    else if (Nrate == cAtNRate1E8)
       {*err_step=56 /*5575*/;     *PositionMask = 1;  *frc_en_mask=1; *hs=100;ret = cAtTrue;}
    else if (Nrate == cAtNRate1E9)
       {*err_step=558 /*55750*/;     *PositionMask = 1;  *frc_en_mask=1; *hs=100;ret = cAtTrue;}
    return ret;
    }

static eBool  RateCalib(ThaAttController self, uint32 NbitPerFrame, uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask, uint8 *hs)
    {
    eBool ret = cAtFalse;

    AtUnused(self);
    if (NbitPerFrame == 26*4*8)/* AU3VC3*/
        return RateCalibVc11(self, Nrate, err_step, PositionMask, frc_en_mask, hs);

    if (NbitPerFrame == 35*4*8)/* AU3VC3*/
        return RateCalibVc12(self, Nrate, err_step, PositionMask, frc_en_mask, hs);

    return ret;
    }

static eAtRet  RateConvertStepVc1x(ThaAttController self, uint32 Nrate, uint8 mode, uint16 *err_step)
    {
    uint16 convert_step = 0;

    AtUnused(self);
    if (Nrate == cAtNRate1E3)
        convert_step = 0;
    else if (Nrate == cAtNRate1E4)
        convert_step = 1;
    else if (Nrate == cAtNRate1E5)
        convert_step = 2;
    else if (Nrate == cAtNRate1E6)
        convert_step = 3;
    else if (Nrate == cAtNRate1E7)
        convert_step = 4;
    else if (Nrate == cAtNRate1E8)
        convert_step = 5;
    else if (Nrate == cAtNRate1E9)
        convert_step = 6;

    *err_step = (uint16)(mode << 3UL) | convert_step;
    return cAtOk;
    }

static eAtRet  RateConvertStep(ThaAttController self, uint32 NbitPerFrame, uint32 Nrate, uint16 *err_step)
    {
    if (mMethodsGet(self)->RateConvertStepIsSupport(self))
        {
        if (NbitPerFrame == 26*4*8)/* AU3VC3*/
            return RateConvertStepVc1x(self, Nrate, 0, err_step);
        else if (NbitPerFrame == 35*4*8)
            return RateConvertStepVc1x(self, Nrate, 1, err_step);
        }
    return cAtOk;
    }

static eBool  RateConvertStepIsSupport(ThaAttController self)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaAttControllerVersionReader((ThaAttController) self));
    AtUnused(self);

    if (hwVersion >= mMethodsGet(controller)->StartVersionSupportConvertStep(controller))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 NbitPerFrame(ThaAttController self)
    {
    AtSdhChannel path = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    eAtSdhChannelType type = AtSdhChannelTypeGet(path);
    switch ((uint32)type)
        {
        case cAtSdhChannelTypeTu12:
        case cAtSdhChannelTypeVc12:
            return 35*4*8; /* VC12 */
        case cAtSdhChannelTypeTu11:
        case cAtSdhChannelTypeVc11:
            return 26*4*8; /* VC11 */
        default:
            return 26*4*8;
        }
    return 26*4*8;
    }

static uint32 ChannelSpeed(ThaAttController self)
    {
    AtSdhChannel path = (AtSdhChannel)AtAttControllerChannelGet((AtAttController)self);
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(path);
    switch ((uint32)channelType)
        {
        case cAtSdhChannelTypeVc12:
            return 2176;
        case cAtSdhChannelTypeVc11:
            return 1600;
        default:
            return 0;
        }
    return 0;
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttControllerMethods =  mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, m_AtAttControllerMethods, sizeof(m_AtAttControllerOverride));


        mMethodOverride(m_AtAttControllerOverride, AlarmForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, ErrorForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, LongRead);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static eAtRet DefaultSet(ThaAttController self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));

        mMethodOverride(m_ThaAttControllerOverride, CdrControllerGet);
        mMethodOverride(m_ThaAttControllerOverride, FrequenceOffsetBase);
        mMethodOverride(m_ThaAttControllerOverride, FrequenceOffsetStep);
        mMethodOverride(m_ThaAttControllerOverride, ChannelSpeed);
        mMethodOverride(m_ThaAttControllerOverride, DefaultSet);
        mMethodOverride(m_ThaAttControllerOverride, RateCalib);
        mMethodOverride(m_ThaAttControllerOverride, RateConvertStepIsSupport);
        mMethodOverride(m_ThaAttControllerOverride, RateConvertStep);
        mMethodOverride(m_ThaAttControllerOverride, NbitPerFrame);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void OverrideTha6A210031PdhDe3AttController(AtAttController self)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(controller), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PointerRealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwStepMaskHo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwStepMaskLo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PositionMaskConfigurationMaskHo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PositionMaskConfigurationMaskLo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, DataMaskConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumberOfErrorsMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, DurationConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwForceModeConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MinStep);

        /*mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, IsAlarmForceV2);*/
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIsFromError);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIsSpecialRdi);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwRdiType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RdiTypeMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumAlarmTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, InitAlarmDefault);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, SpecificAlarmConfig);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, CheckAlarmNumEvent);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ErrorTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ErrorIsFromAlarm);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumErrorTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, InitErrorDefault);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumPointerAdjTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportPointerAdj);
        }

    mMethodsSet(controller, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtAttController(self);
    OverrideThaAttController((ThaAttController)self);
    OverrideTha6A210031PdhDe3AttController(self);
    }
static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031SdhVc1xAttController);
    }

AtAttController Tha6A210031SdhVc1xAttControllerObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031PdhDe3AttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A210031SdhVc1xAttControllerNew(AtChannel sdhPath)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A210031SdhVc1xAttControllerObjectInit(controller, sdhPath);
    }

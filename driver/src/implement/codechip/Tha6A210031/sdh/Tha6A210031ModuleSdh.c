/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha6A210031ModuleSdh.c
 *
 * Created Date: Jul 6, 2016
 *
 * Description : Module SDH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A210031ModuleSdhInternal.h"
#include "../../../default/att/ThaAttSdhManagerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210031ModuleSdh
    {
    tTha60210031ModuleSdh super;
    }tTha6A210031ModuleSdh;

/*--------------------------- Global variables -------------------------------*/
#define mThis(self)              ((tTha6A210031ModuleSdh*)self)

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtModuleSdhMethods  m_AtModuleSdhOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods     = NULL;
static const tAtModuleSdhMethods    *m_AtModuleSdhMethods  = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtSdhChannel ChannelCreate(AtModuleSdh self, uint8 lineId, AtSdhChannel parent, uint8 channelType, uint8 channelId)
    {
    AtUnused(parent);
    AtUnused(lineId);
    if (channelType == cAtSdhChannelTypeLine)
        return (AtSdhChannel)Tha6A210031SdhLineNew(channelId, (AtModuleSdh)self);

    if (channelType == cAtSdhChannelTypeAu3)
        return (AtSdhChannel)Tha6A210031SdhAuNew(channelId, channelType, (AtModuleSdh)self);

    if (channelType == cAtSdhChannelTypeVc3)
        return (AtSdhChannel)Tha6A210031SdhVcNew(channelId, channelType, (AtModuleSdh)self);

    if ((channelType == cAtSdhChannelTypeTu12) || (channelType == cAtSdhChannelTypeTu11))
        return (AtSdhChannel)Tha6A210031SdhTu1xNew(channelId, channelType, (AtModuleSdh)self);

    if ((channelType == cAtSdhChannelTypeVc12) || (channelType ==cAtSdhChannelTypeVc11))
        return (AtSdhChannel)Tha6A210031SdhVc1xNew(channelId, channelType, (AtModuleSdh)self);

    return m_AtModuleSdhMethods->ChannelCreate(self, lineId, parent, channelType, channelId);
    }

static AtAttSdhManager AttSdhManagerCreate(AtModuleSdh self)
    {
    return Tha6A210031AttSdhManagerNew(self);
    }

static void OverrideAtObject(AtModuleSdh self)
    {
    AtObject object = (AtObject)self;

    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        /*mMethodOverride(m_AtObjectOverride, Delete);*/
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, ChannelCreate);
        mMethodOverride(m_AtModuleSdhOverride, AttSdhManagerCreate);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtObject(self);
    OverrideAtModuleSdh(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031ModuleSdh);
    }

static AtModuleSdh ObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha6A210031ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

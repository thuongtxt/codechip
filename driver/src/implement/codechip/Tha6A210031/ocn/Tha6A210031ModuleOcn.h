/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : OCN
 * 
 * File        : Tha6A210031ModuleOcn.h
 * 
 * Created Date: Apr 26, 2017
 *
 * Description : OCN module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031MODULEOCN_H_
#define _THA6A210031MODULEOCN_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ocn/ThaModuleOcn.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif


/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A210031ModuleOcn * Tha6A210031ModuleOcn;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha6A210031ModuleOcnNew(AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031MODULEOCN_H_ */


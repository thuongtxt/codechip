/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : OCN
 *
 * File        : Tha6A210031ModuleOcn.c
 *
 * Created Date: Apr 26, 2017
 *
 * Description : OCN module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A210031ModuleOcnInternal.h"
#include "../../../default/man/ThaDevice.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A210031ModuleOcn)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleOcnMethods         m_ThaModuleOcnOverride;
static tTha60210011ModuleOcnMethods m_Tha60210011ModuleOcnOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TxLoStsPayloadHwFormula(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(channel);
    return (32768UL * hwSlice) + hwSts;
    }

static uint32 TxHwStsDefaultOffset(Tha60210011ModuleOcn self, AtSdhChannel channel, uint8 hwSlice, uint8 hwSts)
    {
    AtUnused(self);
    AtUnused(channel);
    return (1024UL * hwSlice) + hwSts;
    }

static uint32 TxStsVtDefaultOffset(ThaModuleOcn self, AtSdhChannel channel, uint8 stsId, uint8 vtgId, uint8 vtId)
    {
    uint8 hwSlice, hwSts;
    AtUnused(self);

    if (ThaSdhChannelHwStsGet(channel, cThaModuleOcn, stsId, &hwSlice, &hwSts) == cAtOk)
        return Tha60210011ModuleOcnBaseAddress((ThaModuleOcn)self) + (32768UL * hwSlice) + (32UL * hwSts) + (4UL * vtgId) + vtId;

    return cBit31_0;
    }

static void OverrideTha60210011ModuleOcn(Tha60210011ModuleOcn self)
    {
    
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleOcnOverride, mMethodsGet(self), sizeof(m_Tha60210011ModuleOcnOverride));

        mMethodOverride(m_Tha60210011ModuleOcnOverride, TxHwStsDefaultOffset);
        mMethodOverride(m_Tha60210011ModuleOcnOverride, TxLoStsPayloadHwFormula);
        }

    mMethodsSet(self, &m_Tha60210011ModuleOcnOverride);
    }

static void OverrideThaModuleOcn(ThaModuleOcn self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleOcnOverride, mMethodsGet(self), sizeof(m_ThaModuleOcnOverride));

        mMethodOverride(m_ThaModuleOcnOverride, TxStsVtDefaultOffset);
        }

    mMethodsSet(self, &m_ThaModuleOcnOverride);
    }

static void Override(Tha6A210031ModuleOcn self)
    {
    OverrideThaModuleOcn((ThaModuleOcn)self);
    OverrideTha60210011ModuleOcn((Tha60210011ModuleOcn)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031ModuleOcn);
    }

AtModule Tha6A210031ModuleOcnObjectInit(AtModule self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleOcnObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtModule Tha6A210031ModuleOcnNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A210031ModuleOcnObjectInit(newModule, device);
    }

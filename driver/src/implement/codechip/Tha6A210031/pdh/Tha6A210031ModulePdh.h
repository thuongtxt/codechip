/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha6A210031ModulePdh.h
 * 
 * Created Date: Dec 14, 2015
 *
 * Description : PDH module header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210031_PDH_THA6A210031MODULEPDH_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210031_PDH_THA6A210031MODULEPDH_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe3 Tha6A210031PdhDe3New(uint32 channelId, AtModulePdh module);
AtPdhDe1 Tha6A210031PdhVcDe1New(AtSdhVc vc1x, AtModulePdh module);
AtPdhDe1 Tha6A210031PdhDe2De1New(uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210031_PDH_THA6A210031MODULEPDH_H_ */


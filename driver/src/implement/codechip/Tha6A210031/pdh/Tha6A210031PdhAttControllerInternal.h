/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A210031AttControllerInternal.h
 * 
 * Created Date: May 17, 2016
 *
 * Description : ATT controller header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031PDHATTCONTROLLERINTERNAL_H_
#define _THA6A210031PDHATTCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../generic/att/AtAttControllerInternal.h"
#include "../../../../generic/att/AtAttExpectInternal.h"
#include "../../../../implement/default/att/ThaAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

#define cRegCfgErrNumMask                                                                cBit8_1
#define cRegCfgErrNumShift                                                                     1

#define cRegCfgFrc1SecondEnbMask                                                           cBit0
#define cRegCfgFrc1SecondEnbShift                                                              0

#define cRegStatusFrcErrMask                                                               cBit1
#define cRegStatusFrcErrShift                                                                  1

#define cRegStatusOneSecondEnbMask                                                         cBit0
#define cRegStatusOneSecondEnbShift                                                            0

#define cAtAttMaxStep            255
#define cAtAttMaxBitPosition     0xFFFF
/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A210031PdhDe3AttController  * Tha6A210031PdhDe3AttController;
typedef struct tTha6A210031PdhDe1AttController  * Tha6A210031PdhDe1AttController;
typedef enum eAlarmUnit
    {
    cAtAlarmUnit1ms,   /* one milisecond */
    cAtAlarmUnit125us, /* one microsecond */
    }eAlarmUnit;
typedef struct tErrorInfo
    {
    uint16               errorPositionMask; /* Maximum 16 position */
    uint8                errorDataMask; /* 0..1 */
    uint32               errorStep;     /* in frames */
    uint8                errorCrcDs1EsfMask;
    uint16               errorMiniPositionMask; /* Maximum 16 position */
    uint8                errorMiniDataMask;     /* 0..1 */
    uint32               errorMiniStep;         /* in frames */
    uint8                errorMiniCrcDs1EsfMask;

    uint16               error3rdPositionMask; /* Maximum 16 position */
    uint8                error3rdDataMask;     /* 0..1 */
    uint32               error3rdStep;         /* in frames */
    }tErrorInfo;

typedef struct tAlarmInfo
    {
    uint16 step;
    uint16 set;
    uint16 clear;
    uint16 timermode;
    uint16 bitPosition;
    uint16 forceType;
    uint16 dataMask;
    uint16 numErrors;
    eAlarmUnit  unit;
    }tAlarmInfo;

typedef  struct tPointerInfo
    {
    uint32          pointerAdjStep;
    uint16          pointerAdjPositionMask;
    uint8           pointerAdjDataMask; /* 0..1 */
    eAtAttForcePointerState pointerAdjG783State;
    }tPointerInfo;

typedef struct tTha6A210031PdhDe3AttControllerMethods
    {
    eAtRet (*InitErrorDefault)(Tha6A210031PdhDe3AttController self);
    uint32 (*RegAddressFromErrorType)(Tha6A210031PdhDe3AttController self, uint32 errorType);
    uint32 (*RegAddressFromErrorType2nd)(Tha6A210031PdhDe3AttController self, uint32 errorType);
    uint32 (*RegAddressFromErrorType3rd)(Tha6A210031PdhDe3AttController self, uint32 errorType);
    uint32 (*StatusRegAddressFromErrorType)(Tha6A210031PdhDe3AttController self, uint32 errorType);
    uint32 (*StatusRegAddressFromErrorType2nd)(Tha6A210031PdhDe3AttController self, uint32 errorType);
    uint32 (*RealAddress)(Tha6A210031PdhDe3AttController self, uint32 regAddr);
    uint32 (*PointerRealAddress)(Tha6A210031PdhDe3AttController self, uint32 regAddr);
    uint32 (*PointerRealAddress2nd)(Tha6A210031PdhDe3AttController self, uint32 regAddr);
    uint8 (*ErrorTypeIndex)(Tha6A210031PdhDe3AttController self, uint32 errorType);
    eBool (*ErrorIsFromAlarm)(Tha6A210031PdhDe3AttController self, uint32 errorType);
    uint8 (*NumErrorTypes)(Tha6A210031PdhDe3AttController self);
    eBool (*DataMaskIsValid)(Tha6A210031PdhDe3AttController self, uint32 errorType, uint32 dataMask);

    eAtRet (*InitAlarmDefault)(Tha6A210031PdhDe3AttController self);
    eAtRet (*InitPointerAdjDefault)(Tha6A210031PdhDe3AttController self);
    eBool  (*AlarmIsFromError)(Tha6A210031PdhDe3AttController self, uint32 alarmType);
    eBool  (*AlarmIsSpecialRdi)(Tha6A210031PdhDe3AttController self, uint32 alarmType);
    uint32 (*MinStep)(Tha6A210031PdhDe3AttController self, uint32 errorType);
    float  (*MaxErrorNum_1s)(Tha6A210031PdhDe3AttController self, uint32 errorType);
    float  (*MaxErrorNum)(Tha6A210031PdhDe3AttController self, uint32 errorType, uint32 T);
	uint32 (*SetMask)(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType);
	uint32 (*ClearMask)(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType);
	uint32 (*TimerModeMask)(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType);
    uint32 (*HwStepMaskHo)(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType);
    uint32 (*HwStepMaskLo)(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType);
    uint32 (*PositionMaskConfigurationMaskHo)(Tha6A210031PdhDe3AttController self,  uint32 isError,uint32 errorType);
    uint32 (*PositionMaskConfigurationMaskLo)(Tha6A210031PdhDe3AttController self,  uint32 isError,uint32 errorType);
    uint32 (*RdiTypeMask)(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType);
    uint8  (*HwRdiType)(Tha6A210031PdhDe3AttController self, uint32 alarmType);
    uint32 (*DataMaskConfigurationMask)(Tha6A210031PdhDe3AttController self,  uint32 isError,uint32 errorType);
    uint32 (*NumberOfErrorsMask)(Tha6A210031PdhDe3AttController self,  uint32 isError,uint32 errorType);
    uint32 (*DurationConfigurationMask)(Tha6A210031PdhDe3AttController self,  uint32 isError,uint32 errorType);
    uint32 (*HwForceModeConfigurationMask)(Tha6A210031PdhDe3AttController self,  uint32 isError,uint32 errorType);
    void   (*ForceErrorHelper)(Tha6A210031PdhDe3AttController self, uint32 errorType, eAtAttForceType forceType);
    eAtRet (*DefaultDs1DataMask)(Tha6A210031PdhDe3AttController self, uint32 errorType);
    void   (*ForceErrorHelper2nd)(Tha6A210031PdhDe3AttController self, uint32 errorType, eAtAttForceType forceType);
    eAtRet (*DefaultDs1DataMask2nd)(Tha6A210031PdhDe3AttController self, uint32 errorType);
    void   (*ForceErrorHelper3rd)(Tha6A210031PdhDe3AttController self, uint32 errorType, eAtAttForceType forceType);
    eAtRet (*ErrorForcing2ndDataMaskSet)(Tha6A210031PdhDe3AttController self, eAtAttPdhDe3ErrorType errorType, uint32 dataMask);
    eAtRet (*ErrorForcing3rdDataMaskSet)(Tha6A210031PdhDe3AttController self, eAtAttPdhDe3ErrorType errorType, uint32 dataMask);

    const char* (*AlarmIndexToString)(uint8 numAlarmTypes);
    uint32 (*RegAddressAlarmUnit)(Tha6A210031PdhDe3AttController self, uint32 alarmType);
    uint32 (*RegAddressFromAlarmType)(Tha6A210031PdhDe3AttController self,  uint32 alarmType);
    uint32 (*StatusRegAddressFromAlarmType)(Tha6A210031PdhDe3AttController self, uint32 alarmType);
    uint8  (*AlarmTypeIndex)(Tha6A210031PdhDe3AttController self, uint32 alarmType);
    uint8  (*NumAlarmTypes)(Tha6A210031PdhDe3AttController self);
    eAtRet (*SpecificAlarmConfig)(Tha6A210031PdhDe3AttController self, uint32 alarmType);
    eAtRet (*AlarmUnitSet)(Tha6A210031PdhDe3AttController self, uint32 alarmType, eAlarmUnit unit);
    eBool  (*IsNeedAlarmUnitConfig)(Tha6A210031PdhDe3AttController self);
    uint32 (*AlarmUnitToHwValue)(Tha6A210031PdhDe3AttController self, uint32 alarmType, eAlarmUnit unit);

    eBool  (*IsErrorByte)(Tha6A210031PdhDe3AttController self, eAtAttPdhDe3ErrorType errorType);
    uint32 (*ForceOffset)(Tha6A210031PdhDe3AttController self);
    eBool  (*IsAlarmForceV2)(Tha6A210031PdhDe3AttController self, uint32 alarmType);
    eAtRet (*CheckAlarmNumEvent)(Tha6A210031PdhDe3AttController self, uint32 alarmType, uint32 numEvent);
    eAtRet (*CheckAlarmNumFrame)(Tha6A210031PdhDe3AttController self, uint32 alarmType, uint32 numFrame);
    eBool  (*IsErrorForceV2)(Tha6A210031PdhDe3AttController self, uint32 errorType);
    uint32 (*StartVersionSupportAlarmForceV2)(Tha6A210031PdhDe3AttController self);
    uint32 (*StartVersionSupportAlarmUnitConfig)(Tha6A210031PdhDe3AttController self);
    eBool  (*IsE3SameAsDs3)(Tha6A210031PdhDe3AttController self);
    uint32 (*StartVersionSupportE3SameAsDs3)(Tha6A210031PdhDe3AttController self);

    uint32 (*StartVersionSupportPointerAdj)(Tha6A210031PdhDe3AttController self);
    uint32 (*StartVersionSupportPointerAdjV2)(Tha6A210031PdhDe3AttController self);
    uint8  (*NumPointerAdjTypes)(Tha6A210031PdhDe3AttController self);
    uint32 (*RegPointerAdj)(Tha6A210031PdhDe3AttController self);
    uint32 (*RegPointerAdj2nd)(Tha6A210031PdhDe3AttController self);
    uint32 (*PointerAdjTypeMask)(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType);
    uint8  (*SwPointerAdjToHw)(Tha6A210031PdhDe3AttController self, uint32 pointerAdjType);

    uint32 (*StartVersionSupportConvertStep)(Tha6A210031PdhDe3AttController self);
    eAtRet (*ErrorForcingDataMaskSet)(Tha6A210031PdhDe3AttController self, eAtAttPdhDe3ErrorType errorType, uint32 dataMask);
    uint32 (*HwForceErrorMode)(Tha6A210031PdhDe3AttController self, uint32 errorType, uint32 forceType);
    eBool (*IsAisUneqOptimizeSupported)(Tha6A210031PdhDe3AttController self, uint32 alarmType);
    }tTha6A210031PdhDe3AttControllerMethods;

typedef struct tTha6A210031PdhDe3AttController
    {
    tThaAttController super;
    const tTha6A210031PdhDe3AttControllerMethods *methods;

    /* Private data */
    eAtAttForceType *errorForceType; /* oneshot/continous/none*/
    eAtAttForceErrorMode *errorMode;
    eAtBerRate           *errorRate;
    uint32               *errorDuration; /* in ms */
    uint32               *numErrors;     /* 0..65535 */
    tErrorInfo           *errorInfo;

    eAtAttForceAlarmMode *alarmMode;
    uint32               *alarmDuration;      /* in seconds */
    uint32               *alarmNumEvent;
    uint32               *alarmNumFrame;
    tAlarmInfo           *alarmInfo; /* step, set, clear, timer_mode... */
    AtAttExpectChannel   *rx;

    eAtAttForcePointerAdjMode *pointerAdjMode;
    uint32          *pointerAdjDuration; /* in ms */
    uint32          *pointerAdjNumEvent;
    tPointerInfo    *pointerAdjInfo;
    }tTha6A210031PdhDe3AttController;

typedef struct tTha6A210031PdhDe1AttControllerMethods
	{
	eBool  (*IsMfas)(Tha6A210031PdhDe1AttController self);
	uint32 (*StartVersionSupportMfas)(Tha6A210031PdhDe1AttController self);
	uint32 (*StartVersionSupportTwoCRCForce)(Tha6A210031PdhDe1AttController self);
	uint32 (*RegAddressTxMaskCrcDs1Esf)(Tha6A210031PdhDe1AttController self, uint32 errorType);
	uint32 (*TxMaskCrcDs1EsfMaskBig)(Tha6A210031PdhDe1AttController self,  uint32 isError,uint32 errorType);
	uint32 (*TxMaskCrcDs1EsfMaskMini)(Tha6A210031PdhDe1AttController self,  uint32 isError,uint32 errorType);
	uint32 (*SignalingOffset)(Tha6A210031PdhDe1AttController self, AtPdhChannel channel, uint8 tsId);
	 uint32 (*SignalingDumpRealAddressVal)(Tha6A210031PdhDe1AttController self, uint8 tsId);
	}tTha6A210031PdhDe1AttControllerMethods;

typedef struct tTha6A210031PdhDe1AttController
    {
    tTha6A210031PdhDe3AttController super;
    const tTha6A210031PdhDe1AttControllerMethods *methods;
    eAtAttSignalingMode txSigMode;
    eAtAttSignalingMode rxSigMode;
    }tTha6A210031PdhDe1AttController;

typedef struct tTha6A210031PdhDe2AttController
    {
    tTha6A210031PdhDe3AttController super;
    }tTha6A210031PdhDe2AttController;


/*--------------------------- Forward declarations ---------------------------*/
eBool Tha6A210031PdhDe3AttControllerIsAisUneqOptimizeSupported(Tha6A210031PdhDe3AttController self, uint32 alarmType);

/*--------------------------- Entries ----------------------------------------*/
AtAttController Tha6A210031PdhDe3AttControllerNew(AtChannel de3);
AtAttController Tha6A210031PdhDe1AttControllerNew(AtChannel de1);
AtAttController Tha6A210031PdhDe3AttControllerObjectInit(AtAttController self, AtChannel channel);
AtAttController Tha6A210031PdhDe1AttControllerObjectInit(AtAttController self, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031PDHATTCONTROLLERINTERNAL_H_ */


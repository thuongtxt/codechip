/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A210031ModulePdh.c
 *
 * Created Date: Dec 14, 2015
 *
 * Description : PDH module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../Tha60210031/pdh/Tha60210031ModulePdh.h"
#include "Tha6A210031ModulePdh.h"
#include "Tha6A210031PdhDe3AttReg.h"
#include "Tha6A210031PdhAttControllerInternal.h"
#include "../../../default/att/ThaAttPdhManagerInternal.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210031ModulePdh * Tha6A210031ModulePdh;

typedef struct tTha6A210031ModulePdh
    {
    tTha60210031ModulePdh super;
    }tTha6A210031ModulePdh;

/*--------------------------- Global variables -------------------------------*/
#define mThis(self)              ((tTha6A210031ModulePdh*)self)

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods       m_AtObjectOverride;
static tAtModulePdhMethods    m_AtModulePdhOverride;
static tThaModulePdhMethods   m_ThaModulePdhOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtPdhDe3 De3Create(AtModulePdh self, uint32 de3Id)
    {
    return Tha6A210031PdhDe3New(de3Id, self);
    }

static AtPdhDe1 De2De1Create(AtModulePdh self, AtPdhDe2 de2, uint32 de1Id)
    {
    AtUnused(de2);
    return Tha6A210031PdhDe2De1New(de1Id, self);
    }

static AtPdhDe1 VcDe1Create(AtModulePdh self, AtSdhChannel sdhVc)
    {
    return Tha6A210031PdhVcDe1New((AtSdhVc)sdhVc, self);
    }

static AtAttPdhManager AttPdhManagerCreate(AtModulePdh self)
    {
    return Tha6A210031AttPdhManagerNew(self);
    }

static AtObjectAny De1AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A210031PdhDe1AttControllerNew(channel);
    }

static AtObjectAny De3AttControllerCreate(ThaModulePdh self, AtChannel channel)
    {
    AtUnused(self);
    return Tha6A210031PdhDe3AttControllerNew(channel);
    }

static uint32 StartVersionSupportPrm(ThaModulePdh self)
    {
    mVersionReset(self);
    }

static void OverrideAtObject(AtObject self)
    {
    /* Initialize implementation structure (if not initialize yet) */
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void OverrideAtModulePdh(AtModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePdhOverride, mMethodsGet(self), sizeof(m_AtModulePdhOverride));

        mMethodOverride(m_AtModulePdhOverride, De3Create);
        mMethodOverride(m_AtModulePdhOverride, De2De1Create);
        mMethodOverride(m_AtModulePdhOverride, VcDe1Create);
        mMethodOverride(m_AtModulePdhOverride, AttPdhManagerCreate);
        }

    mMethodsSet(self, &m_AtModulePdhOverride);
    }

static void OverrideThaModulePdh(ThaModulePdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePdhOverride, mMethodsGet(self), sizeof(m_ThaModulePdhOverride));
        mMethodOverride(m_ThaModulePdhOverride, De1AttControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, De3AttControllerCreate);
        mMethodOverride(m_ThaModulePdhOverride, StartVersionSupportPrm);
        }

    mMethodsSet(self, &m_ThaModulePdhOverride);
    }
static void Override(AtModulePdh self)
    {
    OverrideAtObject((AtObject)self);
    OverrideAtModulePdh(self);
    OverrideThaModulePdh((ThaModulePdh) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031ModulePdh);
    }

static AtModulePdh ObjectInit(AtModulePdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModulePdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePdh Tha6A210031ModulePdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

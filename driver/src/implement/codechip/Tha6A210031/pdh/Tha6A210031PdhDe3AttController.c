/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A210031AttPdhDe3.c
 *
 * Created Date: Dec 8, 2015
 *
 * Description : ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/att/AtAttControllerInternal.h"
#include "../../../../generic/pdh/AtPdhDe3Internal.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/att/ThaAttSdhManagerInternal.h"
#include "../../../default/att/ThaAttPdhManager.h"
#include "Tha6A210031ModulePdh.h"
#include "Tha6A210031PdhDe3AttReg.h"
#include "Tha6A210031PdhAttControllerInternal.h"
#include "../../../../../include/util/AtList.h"
#include "../sdh/Tha6A210031ModuleSdhInternal.h"
#include <stdlib.h>
#include <math.h>  /*floor(x) */
/*--------------------------- Define -----------------------------------------*/
#define cRegStatusErrStepCntMask                                                       cBit22_15
#define cRegStatusErrStepCntShift                                                             15

#define cRegStatusMskPosCntMask                                                        cBit14_10
#define cRegStatusMskPosCntShift                                                              10

#define cRegStatusErrCntMask                                                             cBit9_2
#define cRegStatusErrCntShift                                                                  2
#define cAtMaxNumEvent 7
#define cAtMaxNumFrame 25500
#define cAtNumDw       4
#define cAttAisUneqOptimizeTypeMask                                                    cBit29_28
#define cAttAisUneqOptimizeTypeShift                                                   28

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A210031PdhDe3AttController *)self)
/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6A210031PdhDe3AttControllerMethods m_methods;

/* Override */
static tAtObjectMethods m_AtObjectOverride;
static tAtAttControllerMethods m_AtAttControllerOverride;
static tThaAttControllerMethods m_ThaAttControllerOverride;

/* Save super implementation */
static const tAtAttControllerMethods * m_AtAttControllerMethods = NULL;
static const tAtObjectMethods * m_AtObjectMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportConvertStep(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    }

static uint32 StartVersionSupportPointerAdjV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportPointerAdj(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 0xFFFFFFFF;
    }

static uint8 NumPointerAdjTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 0;
    }


static uint32 SetMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(isError);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        return c_upen_cpb_cfg_cfgcpb_err_timerset_Mask;
    return c_upen_cpb_cfg_cfgcpb_err_timerset_Mask;
    }

static uint32 ClearMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(isError);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        return c_upen_cpb_cfg_cfgcpb_err_timerclear_Mask;
    return c_upen_cpb_cfg_cfgcpb_err_timerclear_Mask;

    }

static uint32 TimerModeMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(isError);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        return c_upen_cpb_cfg_cfgcpb_err_timermode_Mask;
    else if (isError == cThaAttForceError)
        {
        return c_upen_cpb_cfg_cfgcpb_err_timermode_Mask;
        }
    return c_upen_cpb_cfg_cfgcpb_err_timermode_Mask;
    }

static uint32 RdiTypeMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(isError);
    AtUnused(errorType);
    return 0;
    }

static eAtRet De1CrcGapSet(AtAttController self, uint16 frameType)
    {
	AtUnused(self);
	AtUnused(frameType);
	return cAtOk;
    }

static eAtRet RxAlarmCaptureModeSet(AtAttController self, uint32 alarmType, eAtAttForceAlarmMode mode)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelModeSet(mThis(self)->rx[alarmTypeIndex], mode);
    }

static eAtAttForceAlarmMode RxAlarmCaptureModeGet(AtAttController self, uint32 alarmType)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelModeGet(mThis(self)->rx[alarmTypeIndex]);
    }

static eAtRet RxAlarmCaptureNumEventSet(AtAttController self, uint32 alarmType, uint32 duration)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelNumEventSet(mThis(self)->rx[alarmTypeIndex], duration);
    }

static uint32 RxAlarmCaptureNumEventGet(AtAttController self, uint32 alarmType)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelNumEventGet(mThis(self)->rx[alarmTypeIndex]);
    }

static eAtRet RxAlarmCaptureDurationSet(AtAttController self, uint32 alarmType, uint32 duration)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelDurationSet(mThis(self)->rx[alarmTypeIndex], duration);
    }

static uint32 RxAlarmCaptureDurationGet(AtAttController self, uint32 alarmType)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelDurationGet(mThis(self)->rx[alarmTypeIndex]);
    }

static AtList RecordTime(AtAttController self, uint32 alarmType)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return NULL;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);

    return AtAttExpectChannelTimeList(mThis(self)->rx[alarmTypeIndex]);
    }

static eAtRet RecordTimeAdd(AtAttController self, uint32 alarmType, const char* state)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    AtPrintf("RecordTimeAdd %s\n", state);
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelRecordTimeAdd(mThis(self)->rx[alarmTypeIndex], state);
    }

static eAtRet RecordTimeClear(AtAttController self, uint32 alarmType)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelRecordTimeClear(mThis(self)->rx[alarmTypeIndex]);
    }

static uint32 RecordTimeNumSet(AtAttController self, uint32 alarmType)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelRecordTimeNumSet(mThis(self)->rx[alarmTypeIndex]);
    }

static uint32 RecordTimeNumClear(AtAttController self, uint32 alarmType)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelRecordTimeNumClear(mThis(self)->rx[alarmTypeIndex]);
    }

static uint32 RecordTimeDiffGet(AtAttController self, uint32 alarmType)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelRecordTimeDiffGet(mThis(self)->rx[alarmTypeIndex]);
    }

static eBool RecordTimeIsOkToClear(AtAttController self, uint32 alarmType)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelIsOkToClear(mThis(self)->rx[alarmTypeIndex]);
    }

static eBool RecordTimeIsOk(AtAttController self, uint32 alarmType)
    {
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    return AtAttExpectChannelIsOk(mThis(self)->rx[alarmTypeIndex]);
    }

static eBool  AlarmIsSpecialRdi(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtFalse;
    }

static eBool  AlarmIsFromError(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if (alarmType == cAtPdhDe3AlarmLof || alarmType == cAtPdhDe3AlarmRai)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 MinStep(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtAttPdhDe3ErrorTypeFbit ||
        errorType == cAtAttPdhDe3ErrorTypeFabyte)
        return 29;
    else
        return 1;
    }

static float MaxErrorNum_1s(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    float NFramePerSecond = ThaAttControllerNFramePerSecond((ThaAttController) self);
    if (errorType == cAtAttPdhDe3ErrorTypeFbit ||
        errorType == cAtAttPdhDe3ErrorTypeFabyte)
        return (NFramePerSecond *3)/1000;
    else
        return NFramePerSecond;
    }

static float MaxErrorNum(Tha6A210031PdhDe3AttController self, uint32 errorType, uint32 T/*ms*/)
    {
    float NFramePerSecond  = ThaAttControllerNFramePerSecond((ThaAttController) self); /* = 9.398 frame*/
    uint32 WINDOW_ADEN     = 3; /* 3ms*/
     /* = 33*/
    if (errorType == cAtAttPdhDe3ErrorTypeFbit ||
        errorType == cAtAttPdhDe3ErrorTypeFabyte)
        return (float) T / (float) WINDOW_ADEN; /* T ms */
    return (NFramePerSecond * (float)T)/1000;
    }

static uint32 RegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtAttPdhDe3ErrorTypeFbit:
            return cReg_upen_fbitcfg_Base;
        case cAtAttPdhDe3ErrorTypeCpbit:
            return cReg_upen_cpb_cfg_Base;
        case cAtAttPdhDe3ErrorTypePbit:
            return cReg_upen_pbit_cfg_Base;
        case cAtAttPdhDe3ErrorTypeFabyte:
            return cReg_upen_faB_cfg_Base;
        case cAtAttPdhDe3ErrorTypeBip8byte:
            return cReg_upen_bip8_cfg_Base;
        case cAtAttPdhDe3ErrorTypeReibit:
            return cReg_upen_rei_cfg_Base;
        case cAtAttPdhDe3ErrorTypeNrbyte:
            return cReg_upen_nrB_cfg_Base;
        case cAtAttPdhDe3ErrorTypeGcbyte:
            return cReg_upen_gcB_cfg_Base;
        case cAtAttPdhDe3ErrorTypeFebe:
            return cReg_upen_febe_cfg;
        case cAtAttPdhDe3ErrorTypeNone:
        default:
            return cBit31_0;
        }
    }

static uint32 RegAddressFromErrorType3rd(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cBit31_0;
    }

static uint32 RegAddressFromErrorType2nd(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cBit31_0;
    }

static uint32 StatusRegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtAttPdhDe3ErrorTypeFbit:
            return cReg_upen_fbitsta_Base;
        case cAtAttPdhDe3ErrorTypeCpbit:
            return cReg_upen_cpb_sta_Base;
        case cAtAttPdhDe3ErrorTypePbit:
            return cReg_upen_pbit_sta_Base;
        case cAtAttPdhDe3ErrorTypeFabyte:
            return cReg_upen_faB_sta_Base;
        case cAtAttPdhDe3ErrorTypeBip8byte:
            return cReg_upen_bip8_sta_Base;
        case cAtAttPdhDe3ErrorTypeReibit:
            return cReg_upen_rei_sta_Base;
        case cAtAttPdhDe3ErrorTypeNrbyte:
            return cReg_upen_nrB_sta_Base;
        case cAtAttPdhDe3ErrorTypeGcbyte:
            return cReg_upen_gcB_sta_Base;
        case cAtAttPdhDe3ErrorTypeFebe:
            return cAttReg_upen_febe_sta;
        case cAtAttPdhDe3ErrorTypeNone:
        default:
            return cBit31_0;
        }
    }

static eBool IsErrorByte(Tha6A210031PdhDe3AttController self, eAtAttPdhDe3ErrorType errorType)
    {
    AtUnused(self);
    if ((errorType == cAtAttPdhDe3ErrorTypeFabyte) ||
        (errorType == cAtAttPdhDe3ErrorTypeBip8byte) ||
        (errorType == cAtAttPdhDe3ErrorTypeNrbyte) ||
        (errorType == cAtAttPdhDe3ErrorTypeGcbyte))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 HwStepMaskHo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_cpb_cfg_cfgcpb_err_step_Mask;

        return c_upen_fbitcfg_cfgfbiterr_step_Mask;
        }
    else if (isError == cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit16_9; /* Step 8 bits */
        else
            {
            if (mMethodsGet(mThis(self))->ErrorIsFromAlarm(self, errorType))
                return c_upen_fbitcfg_cfgfbiterr_step_Mask;
            return c_upen_cpb_cfg_cfgcpb_err_step_Mask;
            }
        }
    else
        return c_upen_cpb_cfg_cfgcpb_err_step_Mask;
    }

static uint32 HwStepMaskLo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_cpb_cfg_cfgcpb_err_step_Mask;

        return c_upen_fbitcfg_cfgfbiterr_step_Mask;
        }
    else if (isError == cThaAttForceError)
        {
        if (mMethodsGet(mThis(self))->ErrorIsFromAlarm(self, errorType))
            return c_upen_fbitcfg_cfgfbiterr_step_Mask;
        return c_upen_cpb_cfg_cfgcpb_err_step_Mask;
        }
    else
        return c_upen_cpb_cfg_cfgcpb_err_step_Mask;
    }

static uint32 PositionMaskConfigurationMaskHo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_cpb_cfg_cfgcpb_msk_pos_Mask_Ho;

        if (mMethodsGet(mThis(self))->AlarmIsFromError(self, errorType))
            return c_upen_fbitcfg_cfgfbitmsk_pos_Mask_Ho;
        return 0;
        }
    else if (isError == cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit8_0;
        if (mMethodsGet(mThis(self))->ErrorIsFromAlarm(self, errorType))
            return c_upen_fbitcfg_cfgfbitmsk_pos_Mask_Ho;
        return c_upen_cpb_cfg_cfgcpb_msk_pos_Mask_Ho;
        }
    else
        return c_upen_cpb_cfg_cfgcpb_msk_pos_Mask_Ho;
    }

static uint32 PositionMaskConfigurationMaskLo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);

    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_cpb_cfg_cfgcpb_msk_pos_Mask_Lo;

        if (mMethodsGet(mThis(self))->AlarmIsFromError(self, errorType))
            return c_upen_lof_cfg_cfg_msk_pos_Mask;

        return c_upen_los_cfg_cfg_msk_pos_Mask;
        }
    else if (isError == cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit31_25;
        else
            {
            if (mMethodsGet(mThis(self))->ErrorIsFromAlarm(self, errorType))
                return c_upen_fbitcfg_cfgfbitmsk_pos_Mask_Lo;
            return c_upen_cpb_cfg_cfgcpb_msk_pos_Mask_Lo;
            }
        }
    else
        return c_upen_cpb_cfg_cfgcpb_msk_pos_Mask_Lo;
    }

static uint32 DataMaskConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_cpb_cfg_cfgcpb_msk_dat_Mask;

        if (mMethodsGet(mThis(self))->AlarmIsFromError(self, errorType))
            return c_upen_lof_cfg_cfg_frc_ena_Mask;

        return c_upen_los_cfg_cfg_frc_ena_Mask;
        }
    else if (isError == cThaAttForceError)
        {
        if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            return cBit24_17;
        else
            {
            if (mMethodsGet(mThis(self))->ErrorIsFromAlarm(self, errorType))
                return c_upen_fbitcfg_cfgfbitmsk_dat_Mask;
            return c_upen_cpb_cfg_cfgcpb_msk_dat_Mask;
            }
        }
    else
        return c_upen_cpb_cfg_cfgcpb_msk_dat_Mask;
    }

static uint32 NumberOfErrorsMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_cpb_cfg_cfgcpb_err_num_Mask;

        if (mMethodsGet(mThis(self))->AlarmIsFromError(self, errorType))
            return c_upen_lof_cfg_cfg_evtin1s_Mask;
        return c_upen_los_cfg_cfg_evtin1s_Mask;
        }
    else if (isError == cThaAttForceError)
        {
        if (mMethodsGet(mThis(self))->ErrorIsFromAlarm(self, errorType))
            return c_upen_fbitcfg_cfgfbiterr_num_Mask;
        return c_upen_cpb_cfg_cfgcpb_err_num_Mask;
        }
    else
        return c_upen_cpb_cfg_cfgcpb_err_num_Mask;
    }

static uint32 DurationConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(isError);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return cInvalidUint32;

		if (mMethodsGet(mThis(self))->AlarmIsFromError(self, errorType))
			return c_upen_lof_cfg_cfg_sec_num_Mask;
        }
    return c_upen_los_cfg_cfg_sec_num_Mask;
    }

static uint32 HwForceModeConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(isError);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self, errorType))
            return c_upen_cpb_cfg_cfgcpb_frc_1sen_Mask;

        if (mMethodsGet(mThis(self))->AlarmIsFromError(self, errorType))
            return c_upen_lof_cfg_cfg_frc_mod_Mask;
        return c_upen_los_cfg_cfg_frc_mod_Mask;
        }
    else if (isError == cThaAttForceError)
        {
        if (mMethodsGet(mThis(self))->ErrorIsFromAlarm(self, errorType))
            return c_upen_fbitcfg_cfg_frcfbitmod_Mask;
        return c_upen_cpb_cfg_cfgcpb_frc_1sen_Mask;
        }
    else
        return c_upen_cpb_cfg_cfgcpb_frc_1sen_Mask;
    }

static uint32 RealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwSts;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet((AtAttController) self);
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel) channel);

    ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice, &hwSts);
    return regAddr + ThaModulePdhSliceBase(pdhModule, slice) + hwSts;
    }

static uint32 PointerRealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwSts;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet((AtAttController) self);
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel) channel);

    ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice, &hwSts);
    return regAddr + ThaModulePdhSliceBase(pdhModule, slice) + hwSts;
    }

static uint32 PointerRealAddress2nd(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    AtUnused(self);
    AtUnused(regAddr);
    return cInvalidUint32;
    }

static uint16 LongNumDw(AtAttController self)
    {
    AtUnused(self);
    return 2;
    }

static uint16 LongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    return mChannelHwLongRead(channel, mMethodsGet(mThis(self))->RealAddress(mThis(self), regAddr), dataBuffer, bufferLen, cAtModulePdh);
    }

static uint16 LongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    return mChannelHwLongWrite(channel, mMethodsGet(mThis(self))->RealAddress(mThis(self), regAddr), dataBuffer, bufferLen, cAtModulePdh);
    }

static uint16 PointerLongRead(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    return mChannelHwLongRead(channel, mMethodsGet(mThis(self))->PointerRealAddress(mThis(self), regAddr), dataBuffer, bufferLen, cAtModulePdh);
    }

static uint16 PointerLongWrite(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    return mChannelHwLongWrite(channel, mMethodsGet(mThis(self))->PointerRealAddress(mThis(self), regAddr), dataBuffer, bufferLen, cAtModulePdh);
    }

static uint16 PointerLongWrite2nd(AtAttController self, uint32 regAddr, uint32 *dataBuffer, uint16 bufferLen)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    return mChannelHwLongWrite(channel, mMethodsGet(mThis(self))->PointerRealAddress2nd(mThis(self), regAddr), dataBuffer, bufferLen, cAtModulePdh);
    }

static uint32 Read(AtAttController self, uint32 regAddr)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    return mChannelHwRead(channel, mMethodsGet(mThis(self))->RealAddress(mThis(self), regAddr), cAtModulePdh);
    }

static void Write(AtAttController self, uint32 regAddr, uint32 value)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    mChannelHwWrite(channel, mMethodsGet(mThis(self))->RealAddress(mThis(self), regAddr), value, cAtModulePdh);
    }

static eAtRet HwDataMaskSet(AtAttController self, uint32 isError, uint32 errorType, uint32 dataMask, uint32 regAddr)
    {
    uint16 numDw   = mMethodsGet(self)->LongNumDw(self);
    uint32 regFieldMask = 0, regFieldShift = 0;

    if (numDw > 1)
        {
        uint32 longRegVal[cAtNumDw];
        AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
        mMethodsGet(self)->LongRead(self, regAddr, longRegVal, cAtNumDw);
        regFieldMask = mMethodsGet(mThis(self))->DataMaskConfigurationMask(mThis(self), isError, errorType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[0], regField, dataMask);

        regFieldMask = mMethodsGet(mThis(self))->HwForceModeConfigurationMask(mThis(self), isError, errorType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[0], regField, isError ? 0 : 4);

        regFieldMask = mMethodsGet(mThis(self))->NumberOfErrorsMask(mThis(self), isError, errorType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[0], regField, 0);
        mMethodsGet(self)->LongWrite(self, regAddr, longRegVal, cAtNumDw);
        }

    return cAtOk;
    }

static eAtRet PointerHwDataMaskSet(AtAttController self, uint32 isError, uint32 errorType, uint32 dataMask, uint32 regAddr)
    {
    uint16 numDw   = mMethodsGet(self)->LongNumDw(self);
    uint32 regFieldMask = 0, regFieldShift = 0;

    if (numDw > 1)
        {
        uint32 longRegVal[cAtNumDw];
        AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
        mMethodsGet(self)->PointerLongRead(self, regAddr, longRegVal, cAtNumDw);
        regFieldMask = mMethodsGet(mThis(self))->DataMaskConfigurationMask(mThis(self), isError, errorType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[0], regField, dataMask);

        regFieldMask = mMethodsGet(mThis(self))->HwForceModeConfigurationMask(mThis(self), isError, errorType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[0], regField, isError ? 0 : 4);

        regFieldMask = mMethodsGet(mThis(self))->NumberOfErrorsMask(mThis(self), isError, errorType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[0], regField, 0);
        mMethodsGet(self)->PointerLongWrite(self, regAddr, longRegVal, cAtNumDw);

        regAddr = mMethodsGet(mThis(self))->RegPointerAdj2nd(mThis(self));
        if (regAddr!= cInvalidUint32)
            mMethodsGet(self)->PointerLongWrite2nd(self, regAddr, longRegVal, cAtNumDw);
        }

    return cAtOk;
    }

static eBool ErrorIsFromAlarm(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (mMethodsGet(self)->IsErrorForceV2(self, errorType))
		return cAtFalse;
    if (errorType==cAtAttPdhDe3ErrorTypeFbit)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 HwForceErrorMode(Tha6A210031PdhDe3AttController self, uint32 errorType, uint32 forceType)
    {
    AtUnused(self);
    AtUnused(errorType);

    if (forceType == cAtAttForceTypeContinuous)
        {
    	if (mMethodsGet((Tha6A210031PdhDe3AttController)self)->ErrorIsFromAlarm((Tha6A210031PdhDe3AttController) self, errorType))
    		return 2;
        return 1;
        }

    return 0;
    }

static uint32 RegMaskWidth(uint32 mask)
    {
    uint32 bitIndex;
    uint8 width = 0;

    for (bitIndex = 0; bitIndex < 32; bitIndex++)
        {
        if ((cBit0 << bitIndex) & mask)
            width++;
        }

    return width;
    }

static uint8 ErrorTypeIndex(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtAttPdhDe3ErrorTypeFbit:
            return 0;
        case cAtAttPdhDe3ErrorTypeCpbit:
            return 1;
        case cAtAttPdhDe3ErrorTypePbit:
            return 2;
        case cAtAttPdhDe3ErrorTypeFabyte:
            return 3;
        case cAtAttPdhDe3ErrorTypeBip8byte:
            return 4;
        case cAtAttPdhDe3ErrorTypeReibit:
            return 5;
        case cAtAttPdhDe3ErrorTypeNrbyte:
            return 6;
        case cAtAttPdhDe3ErrorTypeGcbyte:
            return 7;
        case cAtAttPdhDe3ErrorTypeFebe:
            return 8;
        case cAtAttPdhDe3ErrorTypeNone:
        default:
            return 0;
        }
    }

static uint8 NumErrorTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 9;
    }

static eAtRet ErrorForcingDataMaskSet(Tha6A210031PdhDe3AttController self, eAtAttPdhDe3ErrorType errorType, uint32 dataMask)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->RegAddressFromErrorType(mThis(self), errorType);
    if (regAddr == cInvalidUint32)
        return cAtOk;
    return HwDataMaskSet((AtAttController)self, cThaAttForceError,  errorType, dataMask, regAddr);
    }

static eAtRet ErrorForcing2ndDataMaskSet(Tha6A210031PdhDe3AttController self, eAtAttPdhDe3ErrorType errorType, uint32 dataMask)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->RegAddressFromErrorType2nd(mThis(self), errorType);
    if (regAddr == cInvalidUint32)
        return cAtOk;
    return HwDataMaskSet((AtAttController)self, cThaAttForceError,  errorType, dataMask, regAddr);
    }

static eAtRet ErrorForcing3rdDataMaskSet(Tha6A210031PdhDe3AttController self, eAtAttPdhDe3ErrorType errorType, uint32 dataMask)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->RegAddressFromErrorType3rd(mThis(self), errorType);
    if (regAddr == cInvalidUint32)
        return cAtOk;
    return HwDataMaskSet((AtAttController)self, cThaAttForceError,  errorType, dataMask, regAddr);
    }

static void ForceErrorHelper2nd(Tha6A210031PdhDe3AttController self, uint32 errorType, eAtAttForceType forceType)
    {
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    uint16 numDw          = mMethodsGet((AtAttController)self)->LongNumDw((AtAttController)self);
    uint32 longRegVal[cAtNumDw];
    uint32 regFieldMask, regFieldShift;
    uint32 numErrors;
    uint32 regAddr        = mMethodsGet(mThis(self))->RegAddressFromErrorType2nd(mThis(self), errorType);
    uint8  errorTypeIndex = mMethodsGet(att)->ErrorTypeIndex(att, errorType);
    uint32 bitPosition    = att->errorInfo[errorTypeIndex].errorMiniPositionMask;
    uint8  dataMask       = att->errorInfo[errorTypeIndex].errorMiniDataMask;
    uint16 errorStep      = (uint16)att->errorInfo[errorTypeIndex].errorMiniStep;
    att->errorForceType[errorTypeIndex] = forceType;

    /* Unforce before do a new forcing */
    if (forceType != cAtAttForceTypeNone)
        mMethodsGet(self)->ErrorForcing2ndDataMaskSet(self, errorType, 0);

    mMethodsGet((AtAttController)self)->LongRead((AtAttController)self, regAddr, longRegVal, cAtNumDw);

    /* Error Step */
    if (numDw > 1)
        {
        regFieldMask     = mMethodsGet(att)->TimerModeMask(att, cThaAttForceError, errorType);
        regFieldShift    = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, 0);

        regFieldMask     = mMethodsGet(att)->ClearMask(att, cThaAttForceError, errorType);
        regFieldShift    = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, 0);

        regFieldMask     = mMethodsGet(att)->SetMask(att, cThaAttForceError, errorType);
        regFieldShift    = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, 0);

        regFieldMask = mMethodsGet(att)->HwStepMaskHo(att, cThaAttForceError, errorType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, errorStep);
        }

    /* Configure bit-mask position: 16 position */
    if (numDw > 1)
        {
        uint32 bitPositionHwVal = 0;
        uint32 regMaskWidth     = 0;
        regFieldMask     = mMethodsGet(att)->PositionMaskConfigurationMaskHo(att, cThaAttForceError, errorType);
        regFieldShift    = AtRegMaskToShift(regFieldMask);

        regMaskWidth     = RegMaskWidth(mMethodsGet(att)->PositionMaskConfigurationMaskLo(att, cThaAttForceError, errorType));
        bitPositionHwVal = bitPosition >> regMaskWidth;
        mRegFieldSet(longRegVal[1], regField, bitPositionHwVal);
        }

    regFieldMask = mMethodsGet(att)->PositionMaskConfigurationMaskLo(att, cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, bitPosition);

    /* Configure ForceMode  */
    regFieldMask = mMethodsGet(att)->HwForceModeConfigurationMask(att, cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, mMethodsGet(att)->HwForceErrorMode(att, errorType, forceType));

    /* Configure datamask (0..1) */
    regFieldMask = mMethodsGet(att)->DataMaskConfigurationMask(att, cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    if (forceType != cAtAttForceTypeNone)
        mRegFieldSet(longRegVal[0], regField, dataMask);

    /* Number of Error */
    regFieldMask = mMethodsGet(att)->NumberOfErrorsMask(att, cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    numErrors = (forceType == cAtAttForceTypeContinuous) ? (regFieldMask >> regFieldShift) : att->numErrors[errorTypeIndex];
    mRegFieldSet(longRegVal[0], regField, numErrors);
    mMethodsGet((AtAttController)self)->LongWrite((AtAttController)self, regAddr, longRegVal, cAtNumDw);
    }

static void ForceErrorHelper3rd(Tha6A210031PdhDe3AttController self, uint32 errorType, eAtAttForceType forceType)
    {
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    uint16 numDw          = mMethodsGet((AtAttController)self)->LongNumDw((AtAttController)self);
    uint32 longRegVal[cAtNumDw];
    uint32 regFieldMask, regFieldShift;
    uint32 numErrors;
    uint32 regAddr        = mMethodsGet(mThis(self))->RegAddressFromErrorType3rd(mThis(self), errorType);
    uint8  errorTypeIndex = mMethodsGet(att)->ErrorTypeIndex(att, errorType);
    uint32 bitPosition    = att->errorInfo[errorTypeIndex].error3rdPositionMask;
    uint8  dataMask       = att->errorInfo[errorTypeIndex].error3rdDataMask;
    uint16 errorStep      = (uint16)att->errorInfo[errorTypeIndex].error3rdStep;
    att->errorForceType[errorTypeIndex] = forceType;

    /* Unforce before do a new forcing */
    if (forceType != cAtAttForceTypeNone)
        mMethodsGet(self)->ErrorForcing3rdDataMaskSet(self, errorType, 0);

    mMethodsGet((AtAttController)self)->LongRead((AtAttController)self, regAddr, longRegVal, cAtNumDw);

    /* Error Step */
    if (numDw > 1)
        {
        regFieldMask     = mMethodsGet(att)->TimerModeMask(att, cThaAttForceError, errorType);
        regFieldShift    = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, 0);

        regFieldMask     = mMethodsGet(att)->ClearMask(att, cThaAttForceError, errorType);
        regFieldShift    = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, 0);

        regFieldMask     = mMethodsGet(att)->SetMask(att, cThaAttForceError, errorType);
        regFieldShift    = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, 0);

        regFieldMask = mMethodsGet(att)->HwStepMaskHo(att, cThaAttForceError, errorType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, errorStep);
        }

    /* Configure bit-mask position: 16 position */
    if (numDw > 1)
        {
        uint32 bitPositionHwVal = 0;
        uint32 regMaskWidth     = 0;

        regFieldMask     = mMethodsGet(att)->PositionMaskConfigurationMaskHo(att, cThaAttForceError, errorType);
        regFieldShift    = AtRegMaskToShift(regFieldMask);

        regMaskWidth     = RegMaskWidth(mMethodsGet(att)->PositionMaskConfigurationMaskLo(att, cThaAttForceError, errorType));
        bitPositionHwVal = bitPosition >> regMaskWidth;
        mRegFieldSet(longRegVal[1], regField, bitPositionHwVal);
        }

    regFieldMask = mMethodsGet(att)->PositionMaskConfigurationMaskLo(att, cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, bitPosition);

    /* Configure ForceMode  */
    regFieldMask = mMethodsGet(att)->HwForceModeConfigurationMask(att, cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, mMethodsGet(att)->HwForceErrorMode(att, errorType, forceType));

    /* Configure datamask (0..1) */
    regFieldMask = mMethodsGet(att)->DataMaskConfigurationMask(att, cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    if (forceType != cAtAttForceTypeNone)
        mRegFieldSet(longRegVal[0], regField, dataMask);

    /* Number of Error */
    regFieldMask = mMethodsGet(att)->NumberOfErrorsMask(att, cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    numErrors = (forceType == cAtAttForceTypeContinuous) ? (regFieldMask >> regFieldShift) : att->numErrors[errorTypeIndex];
    mRegFieldSet(longRegVal[0], regField, numErrors);
    mMethodsGet((AtAttController)self)->LongWrite((AtAttController)self, regAddr, longRegVal, cAtNumDw);
    }

static eAtRet DefaultDs1DataMask(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtOk;
    }

static uint32 AlarmUnitToHwValue(Tha6A210031PdhDe3AttController self, uint32 alarmType, eAlarmUnit unit)
    {
    AtUnused(self);
    AtUnused(alarmType);
    AtUnused(unit);
    return cInvalidUint32;
    }

static eAtRet AlarmUnitSet(Tha6A210031PdhDe3AttController self, uint32 alarmType,  eAlarmUnit unit)
    {
    uint32 address =  mMethodsGet(mThis(self))->RegAddressAlarmUnit(mThis(self), alarmType);

    if (address!= cInvalidUint32)
        {
        uint32 hwValue = mMethodsGet(mThis(self))->AlarmUnitToHwValue(self, alarmType, unit);
        mMethodsGet((AtAttController)self)->Write((AtAttController) self,address, hwValue);
        }

    return cAtOk;
    }

static eBool IsNeedAlarmUnitConfig(Tha6A210031PdhDe3AttController self)
    {
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaAttControllerVersionReader((ThaAttController) self));
    AtUnused(self);

    if (hwVersion >= mMethodsGet(mThis(self))->StartVersionSupportAlarmUnitConfig(mThis(self)))
        return cAtTrue;
    return cAtFalse;
    }

static void ForceErrorHelper(Tha6A210031PdhDe3AttController self, /*eAtAttPdhDe3ErrorType*/ uint32 errorType, eAtAttForceType forceType)
    {
    uint16 numDw          = mMethodsGet((AtAttController)self)->LongNumDw((AtAttController)self);
    uint32 longRegVal[cAtNumDw]={0,0,0,0};
    uint32 regFieldMask=0, regFieldShift=0;
    uint32 numErrors      = 0;
    uint32 regAddr        = mMethodsGet(mThis(self))->RegAddressFromErrorType(mThis(self), errorType);
    uint8  errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    uint32 bitPosition    = mThis(self)->errorInfo[errorTypeIndex].errorPositionMask;
    uint8  dataMask       = mThis(self)->errorInfo[errorTypeIndex].errorDataMask;
    uint16 errorStep      = (uint16)mThis(self)->errorInfo[errorTypeIndex].errorStep;

    mThis(self)->errorForceType[errorTypeIndex] = forceType;

    /* Unforce before do a new forcing */
    if (forceType != cAtAttForceTypeNone)
        mMethodsGet(mThis(self))->ErrorForcingDataMaskSet(mThis(self), errorType, 0);

    mMethodsGet(mThis(self))->DefaultDs1DataMask(mThis(self), errorType);

    mMethodsGet((AtAttController)self)->LongRead((AtAttController)self, regAddr, longRegVal, cAtNumDw);

    /* Error Step */
    if (numDw > 1)
        {
        regFieldMask     = mMethodsGet(self)->TimerModeMask(self, cThaAttForceError, errorType);
        regFieldShift    = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, 0);
        mRegFieldSet(longRegVal[1], c_upen_cpb_cfg_cfgcpb_err_timerclear_, 0);
        mRegFieldSet(longRegVal[1], c_upen_cpb_cfg_cfgcpb_err_timerset_, 0);

        if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
            {
            uint32 errorStepHwVal   = 0;
            uint32 regMaskWidth     = 0;

            regFieldMask = mMethodsGet(self)->HwStepMaskHo(self, cThaAttForceError, errorType);
            regFieldShift = AtRegMaskToShift(regFieldMask);
            regMaskWidth     = RegMaskWidth(mMethodsGet(self)->HwStepMaskLo(self, cThaAttForceError, errorType));
            errorStepHwVal = (uint32)errorStep >> regMaskWidth;
            mRegFieldSet(longRegVal[1], regField, errorStepHwVal);
            }
        else
            {
            regFieldMask = mMethodsGet(self)->HwStepMaskHo(self, cThaAttForceError, errorType);
            regFieldShift = AtRegMaskToShift(regFieldMask);
            mRegFieldSet(longRegVal[1], regField, errorStep);
            }
        }

    /* Configure bit-mask position: 16 position */
    if (numDw > 1)
        {
        uint32 bitPositionHwVal = 0;
        uint32 regMaskWidth     = 0;

        if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
            {
            regFieldMask = mMethodsGet(mThis(self))->HwStepMaskLo(mThis(self), cThaAttForceError, errorType);
            regFieldShift = AtRegMaskToShift(regFieldMask);
            mRegFieldSet(longRegVal[0], regField, errorStep);
            }
        else
            {
            regFieldMask     = mMethodsGet(mThis(self))->PositionMaskConfigurationMaskHo(mThis(self), cThaAttForceError, errorType);
            regFieldShift    = AtRegMaskToShift(regFieldMask);

            regMaskWidth     = RegMaskWidth(mMethodsGet(mThis(self))->PositionMaskConfigurationMaskLo(mThis(self), cThaAttForceError, errorType));
            bitPositionHwVal = bitPosition >> regMaskWidth;
            mRegFieldSet(longRegVal[1], regField, bitPositionHwVal);
            }
        }

    regFieldMask = mMethodsGet(mThis(self))->PositionMaskConfigurationMaskLo(mThis(self), cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, bitPosition);

    /* Configure ForceMode  */
    regFieldMask = mMethodsGet(mThis(self))->HwForceModeConfigurationMask(mThis(self), cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, mMethodsGet(mThis(self))->HwForceErrorMode(mThis(self), errorType, forceType));

    /* Configure datamask (0..1) */
    regFieldMask = mMethodsGet(mThis(self))->DataMaskConfigurationMask(mThis(self), cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    if (forceType != cAtAttForceTypeNone)
        mRegFieldSet(longRegVal[0], regField, dataMask);

    /* Number of Error */
    regFieldMask = mMethodsGet(mThis(self))->NumberOfErrorsMask(mThis(self), cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    numErrors = (forceType == cAtAttForceTypeContinuous) ? (regFieldMask >> regFieldShift) : mThis(self)->numErrors[errorTypeIndex];
    mRegFieldSet(longRegVal[0], regField, numErrors);

    mMethodsGet((AtAttController)self)->LongWrite((AtAttController)self, regAddr, longRegVal, cAtNumDw);

    }

static uint32 MakeRoundUint32(float A)
    {
    uint32 roundVal  = ((uint32)(AtSize)A);
    float  absolte_value      = (float)fabs((double)(A-(float)roundVal));
    return (uint32)((absolte_value > 0.5)? roundVal+1 : roundVal);
    }

static eAtRet DebugPossibleCase(eBool isDebug, uint32 T, uint32 NERROR, uint32 NFramePerSecond, uint16 *r_CONFIG_STEP, uint16 *r_CONFIG_MASK_POSITION, uint32 MAX_STEP, uint32 MIN_STEP)
    {
    uint8  n          = 1;
    uint8  bestcase   = 1;
    uint32 diff       = T;
    /*                     0       1       2       3       4       5       6       7       8       9      10      11      12      13      14      15,     16 */
    uint32 position[17] = {0, 0x4000, 0x4040, 0x4440, 0x4444, 0x5248, 0x5449, 0x54A5, 0x5AAA, 0x7555, 0x7AD5, 0x7B5B, 0x6FDB, 0x7FDB, 0x7FFB, 0x7FFF, 0xFFFF };

    for (n = 1; n < 15; n++)
        {
        uint32  CONFIG_STEP          = (uint32) ((((((float) T * (float) n) / 16) / (float) NERROR) * (float) NFramePerSecond) / 1000);/* = 220*/
        uint32  CONFIG_MASK_POSITION = position[n];
        uint32  TALL_CHECK;
        uint32  local_diff           = T;
        if (CONFIG_STEP > MAX_STEP)
            break;

        if (CONFIG_STEP < MIN_STEP)
            continue;

        if (isDebug)
            {
            AtPrintf("   %02d %-21s = %d frames\n", n,  "CONFIG_STEP",          CONFIG_STEP);
            AtPrintf("   %2s %-21s = 0x%x\n",       "", "CONFIG_MASK_POSITION", CONFIG_MASK_POSITION);
            AtPrintf("   %2s %-21s = %d ms\n",      "", "T_CHECK",              (uint32) ((1000 * (float) CONFIG_STEP) / (float) NFramePerSecond));
            AtPrintf("   %2s %-21s = %d ms\n",      "", "T16_CHECK",            (uint32) ((1000 * 16 * (float) CONFIG_STEP) / (float) NFramePerSecond));
            }
        TALL_CHECK = (uint32)((1000*16*(float)NERROR*(float) CONFIG_STEP) / (float) (NFramePerSecond * n));

        if (isDebug)
            {
            AtPrintf("   %2s %-21s = %d ms\n", "", "TALL_CHECK", TALL_CHECK);
            }

        local_diff = (TALL_CHECK < T)? (T-TALL_CHECK): (TALL_CHECK-T);
        if (local_diff < diff)
            {
            diff     = local_diff;
            bestcase = n;
            }
        }

    {
        *r_CONFIG_STEP          = (uint16) ((((((float) T*(float)bestcase)/16)/(float) NERROR) * (float) NFramePerSecond) / 1000);/* = 220*/
        *r_CONFIG_MASK_POSITION = (uint16) position[bestcase];
        if (isDebug)
           {
           AtPrintf("==>Bestcase=%d\n", bestcase);
           AtPrintf("   %02d %-21s = %d frames\n", n,  "CONFIG_STEP",          *r_CONFIG_STEP);
           AtPrintf("   %2s %-21s = 0x%x\n",       "", "CONFIG_MASK_POSITION", *r_CONFIG_MASK_POSITION);
           }

    }
    return cAtOk;
    }

static eAtRet AttConvert(uint32 MAX_STEP, uint32 MIN_STEP, uint16 *CONFIG_STEP, uint16 *CONFIG_MASK_POSITION)
    {
    uint32 rCONFIG_STEP          = *CONFIG_STEP;
    uint16 rCONFIG_MASK_POSITION = *CONFIG_MASK_POSITION;
    uint8  n                     = 1;

    while (rCONFIG_STEP < MIN_STEP)
        {
        if (n > 3)
            return cAtErrorInvlParm;
        if ((uint32) (rCONFIG_STEP * 2) > MAX_STEP)
            return cAtErrorInvlParm;

        rCONFIG_STEP = (uint8) (rCONFIG_STEP * 2);
        if (n==1) rCONFIG_MASK_POSITION = 0x4040;
        if (n==2) rCONFIG_MASK_POSITION = 0x4444;
        if (n==3) rCONFIG_MASK_POSITION = 0x5555;
        n = (uint8)(n + 1);
        }
    *CONFIG_STEP = (uint16) rCONFIG_STEP;
    *CONFIG_MASK_POSITION = rCONFIG_MASK_POSITION;
    return cAtOk;
    }

static eAtRet CalculateStepAndPositionMask(AtAttController self, uint32 isError, uint32 errorType, eBool isDebug, uint32 T, uint32 NERROR, uint16 *err_step, uint16 *PositionMask)
    {
    uint32 RATE                 = ThaAttControllerChannelSpeed((ThaAttController) self);
    uint32 NbitPerFrame         = ThaAttControllerNbitPerFrame((ThaAttController) self);
    uint32 NFramePerSecond      = ((RATE * 1000) / NbitPerFrame); /* = 9.398 frame*/
    float  MAX_ERROR            = mMethodsGet(mThis(self))->MaxErrorNum(mThis(self), errorType, T);
    uint16 CONFIG_STEP          = 0;
    uint16 CONFIG_MASK_POSITION = 0;
    uint32 MAX_STEP             = ThaAttControllerMaxStep((ThaAttController) self, isError, errorType, 0, 0);
    uint32 MIN_STEP             = (mMethodsGet(mThis(self))->MinStep(mThis(self), errorType));
    float  maxErrorNum1s        = mMethodsGet(mThis(self))->MaxErrorNum_1s(mThis(self), errorType);/* =28*/

    if (isDebug)
        {
        AtPrintf("%-21s = %d ms\n",               "T",                T);
        AtPrintf("%-21s = %d \n",                 "numErrors/Frames", NERROR);
        AtPrintf("%-21s = %d bit/s (FIXED)\n",    "RATE",             RATE * 1000);
        AtPrintf("%-21s = %d bit/frame(FIXED)\n", "NbitPerFrame",     NbitPerFrame);
        AtPrintf("%-21s = %d (FIXED)\n",          "NFramePerSecond",  NFramePerSecond);
        AtPrintf("%-21s = %d frames\n",           "MIN_Step",         MIN_STEP);
        AtPrintf("%-21s = %d frames\n",           "MAX_Step",         MAX_STEP);
        AtPrintf("%-21s = %d frames\n",           "MaxErrorNum_1s",   (uint32)maxErrorNum1s);
        }

    if ((float) NERROR <= MAX_ERROR)
        {
        float STEP_CONFIG_1ERR = MAX_ERROR / (float) NERROR; /* 2 loi cach nhau bao nhieu frame */
        if (STEP_CONFIG_1ERR >= (float)MIN_STEP)
            {
            if (STEP_CONFIG_1ERR <= MAX_STEP) /*In range */
                {
                CONFIG_STEP = (uint16) STEP_CONFIG_1ERR; /* = 33*/
                CONFIG_MASK_POSITION = 0xFFFF;         /* Full Mask */
                }
            else if (STEP_CONFIG_1ERR > MAX_STEP) /* greater than range */
                {
                float MAX_T_1_POSITIONMASK = ((float) (MAX_STEP * 16 * 1000) / ((float) NFramePerSecond));
                if ((T / NERROR) > MAX_T_1_POSITIONMASK/* (27.1 * 16)*/)
                    {
                    if (isDebug)
                        {
                        float MAX_TIME = MAX_T_1_POSITIONMASK * (float)NERROR;
                        AtPrintf("ERROR: Can not force Error   in this CASE.\n");
                        AtPrintf("%4s    FORCE %05d ERROR      in MAX_TIME = %d ms\n", "*", NERROR,                           (uint32)MAX_TIME);
                        AtPrintf("%4s or FORCE MIN_ERROR=%05d  in TIME     = %d ms\n",   "*", (uint32)((float)(T * NERROR) / MAX_TIME), T);
                        return cAtErrorInvlParm;
                        }
                    CONFIG_STEP = (uint16) MAX_STEP;
                    CONFIG_MASK_POSITION = 0x4000;
                    }
                else
                    {
                    eAtRet ret = cAtOk;
                    if (isDebug)
                        {
                        float EACH_BIT_MASK_LAST = (float) ((((float) T / 16)) / ((float) NERROR));/* = 23.4....*/
                        float T_ONE_ERROR = (float) T / (float) NERROR;
                        AtPrintf("%-21s = %.3f ms\n", "T_ONE_ERROR",        T_ONE_ERROR);
                        AtPrintf("%-21s = %.3f ms\n", "EACH_BIT_MASK_LAST", EACH_BIT_MASK_LAST);
                        }
                    CONFIG_STEP          = (uint8) MakeRoundUint32(((((float) T / 16) / (float) NERROR) * (float) NFramePerSecond) / 1000);/* = 220*/
                    CONFIG_MASK_POSITION = 0x4000;
                    ret = AttConvert(MAX_STEP, MIN_STEP, &CONFIG_STEP, &CONFIG_MASK_POSITION);
                    if (ret != cAtOk)
                        {
                        if (isDebug)
                            AtPrintf("Info: Some possible case.\n");

                        ret = DebugPossibleCase(isDebug, T, NERROR, NFramePerSecond, &CONFIG_STEP, &CONFIG_MASK_POSITION, MAX_STEP, MIN_STEP);
                        if (ret != cAtOk)
                            return ret;
                        }

                    }
                }
            }
        }
    else if (T)
        {
        AtPrintf("ERROR: Can not force Error in this CASE.\n");
        AtPrintf("In T=%dms MAX_ERROR =%.3f\n", T, MAX_ERROR);
        CONFIG_STEP = (uint16) MAX_STEP;
        CONFIG_MASK_POSITION = 0x4000;
        return cAtErrorInvlParm;
        }
    if (isDebug)
        {
        AtPrintf("%-21s = %d frames\n", "CONFIG_STEP", CONFIG_STEP);
        AtPrintf("%-21s = 0x%x\n", "CONFIG_MASK_POSITION", CONFIG_MASK_POSITION);
        }

    *err_step     = CONFIG_STEP;
    *PositionMask = CONFIG_MASK_POSITION;
    return cAtOk;
    }

static eAtRet DebugErrorParameters(AtAttController self, uint32 isError, uint32 errorType, uint32 numErrors, uint32 T)
    {
    uint16  err_step     = 0;
    uint16 PositionMask = 0;
    return CalculateStepAndPositionMask(self, isError, errorType, cAtTrue, T, numErrors, &err_step, &PositionMask);
    }

static const char* ErrorModeToString(uint32 mode)
    {
    switch (mode)
        {
        case cAtAttForceErrorModeContinuous:
            return "continous";
        case cAtAttForceErrorModeRate:
            return "rate";
        case cAtAttForceErrorModeOneshot:
            return "oneshot";
        case cAtAttForceErrorModeNerrorInT:
            return "nerror_in_t";
        case cAtAttForceErrorModeConsecutive:
        	return "consecutive";
        default:
            return "unknown";
        }
    }

static eAtRet DebugError(AtAttController self, uint32 errorType)
    {
    uint32 errorTypeIndex = 0;

    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;

    errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
        {
        uint32 numErrors = mThis(self)->numErrors[errorTypeIndex];
        uint32 T         = mThis(self)->errorDuration[errorTypeIndex];
        DebugErrorParameters(self, cThaAttForceError, errorType, numErrors, T);
        AtPrintf("%-21s = %s\n",        "Error Force Mode", ErrorModeToString(mThis(self)->errorMode[errorTypeIndex]));
        AtPrintf("%-21s = %d ms\n",     "Duration",         T);
        AtPrintf("%-21s = %d errors\n", "Number of Error",  numErrors);
        }
    return cAtOk;
    }

static const char* AlarmModeToString(uint32 mode)
    {
    switch (mode)
        {
        case cAtAttForceAlarmModeContinuous:
            return "continous";
        case cAtAttForceAlarmModeSetInT:
            return "set_in_t";
        case cAtAttForceAlarmModeSetInNFrame:
            return "set_in_nframe";
        case cAtAttForceAlarmModeNeventInT:
            return "nevent_in_t";
        default:
            return "unknown";
        }
    }

static eAtRet DebugAlarm(AtAttController self, uint32 alarmType)
    {
    if (AtAttControllerAlarmForceIsSupported(self, alarmType))
        {
        uint32 alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
        uint32 numAlarms      = mThis(self)->alarmNumEvent[alarmTypeIndex];
        uint32 T              = mThis(self)->alarmDuration[alarmTypeIndex];
        DebugErrorParameters(self, cThaAttForceAlarm,  alarmType, numAlarms, T);
        AtPrintf("%-21s = %s\n",       "Alarm Force Mode", AlarmModeToString(mThis(self)->alarmMode[alarmTypeIndex]));
        AtPrintf("%-21s = %d\n",       "Number of events", mThis(self)->alarmNumEvent[alarmTypeIndex]);
        AtPrintf("%-21s = %d\n",       "Duration",         mThis(self)->alarmDuration[alarmTypeIndex]);
        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

static eAtRet ForceErrorModeSet(AtAttController self, uint32 errorType, eAtAttForceErrorMode mode)
    {
    eAtRet ret            = cAtOk;
    uint8  errorTypeIndex = 0;

    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;

    mMethodsGet(mThis(self))->ErrorForcingDataMaskSet(mThis(self), errorType, 0);
    errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    mThis(self)->errorMode[errorTypeIndex] = mode;
    mThis(self)->errorInfo[errorTypeIndex].errorDataMask = 1;

    mThis(self)->errorForceType[errorTypeIndex] = cAtAttForceTypeNone;
    return ret;
    }

static eAtRet ForceErrorNumErrorsSet(AtAttController self, uint32 errorType, uint32 numErrors)
    {
    uint32 errorTypeIndex = 0;
    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;
    if (numErrors > 65535)
        return cAtErrorOutOfRangParm;
    mMethodsGet(mThis(self))->ErrorForcingDataMaskSet(mThis(self), errorType, 0);
    errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    mThis(self)->numErrors[errorTypeIndex] = numErrors;
    mThis(self)->errorInfo[errorTypeIndex].errorDataMask = 1;
    return cAtOk;
    }

static eBool UseAdjust(ThaAttController self, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    return cAtFalse;
    }

static eBool NeedAnotherCalculate(uint32 NbitPerFrame, uint32 Nrate, uint32 s, double *k)
    {
    AtUnused(s);
    *k = 10000*1.0f;
    if (NbitPerFrame == cAtNbitPerFrameStm0)
        {
        if (Nrate == cAtNRate1E8 || Nrate == cAtNRate1E9)
            *k = 100*1.0f;
        return cAtTrue;
        }
    else if (NbitPerFrame == cAtNbitPerFrameStm64)
        {
        if (Nrate == cAtNRate1E6 || Nrate == cAtNRate1E7)
            *k = 100000*1.0f;
        if (Nrate == cAtNRate1E8 || Nrate == cAtNRate1E9)
            *k = 1000000*1.0f;
        return cAtTrue;
        }
    else if (   (NbitPerFrame == cAtNbitPerFrameStm16) &&
                (Nrate == cAtNRate1E8 || Nrate == cAtNRate1E9)  )
        {
        return cAtTrue;
        }
    return cAtFalse;
    }

static eAtRet CalculateStepAndPositionMaskForRate8bit(AtAttController self, uint32 errorType,  uint32 Nrate, uint16 *err_step, uint16 *PositionMask, uint8 *frc_en_mask)
    {
    eBool debug=cAtFalse;
    tRateInfo  array;
    uint8      hs = 1;
    uint8      msk[9]          = { 0x0, 0x1,  0x3,    0x7,    0xF,    0x1F,   0x3F,   0x7F,   0xFF};
    eBool      NeedToRemoveSoh  = ThaAttControllerNeedRemoveSoh((ThaAttController)self, errorType);
    uint32     NbitPerFrameSoh  = NeedToRemoveSoh?ThaAttControllerNbitPerFrameSoh((ThaAttController)self):0;
    uint32     NbitPerFrame     = ThaAttControllerNbitPerFrame((ThaAttController)self) - NbitPerFrameSoh;
    uint32     MaxStep          = ThaAttControllerMaxStep((ThaAttController)self, cThaAttForceError,  errorType, Nrate, NbitPerFrame);
    uint32     s = 0, p=0, m=0, max_pos;
    uint32     MinPos = 1;
    double     A = (1 * 1.0f) / ((double)Nrate * 1.0f);
    double     ss = 0;
    double     tempMinVal = (double)999999999999*1.0f;
    eBool      needOther  = cAtFalse;
    double     k=1;
    eBool      isCalib = mMethodsGet((ThaAttController)self)->RateCalib((ThaAttController)self, NbitPerFrame, Nrate, &array.step, &array.positionMask, &array.frc_en_mask, &hs);
    if (!isCalib)
        {
        for (max_pos=16; max_pos<17; max_pos++)
            {
            for (s = 1; s < (MaxStep + 1); s++)
                {
                double T = (double)max_pos * (double)s * (double)NbitPerFrame * 1.0f;
                for (p=MinPos; p<(max_pos+1); p++)
                    {
                    for (m=1; m<(8+1); m++)
                        {
                        double value = ((double)p * (double)m * 1.0f)/T;
                        double diff  = (double)(fabs((double)(value-A)));
                        if (NbitPerFrame==35*4*8||NbitPerFrame==26*4*8)
                            {
                            if (m>2)
                                continue;
                            }
                        needOther = NeedAnotherCalculate(NbitPerFrame, Nrate, s, &k);
                        if (needOther)
                            {
                            A     = k/((double)Nrate * 1.0f);
                            T     = (double)(1.0f*(double)max_pos*(double)s*(((double)NbitPerFrame* 1.0f)/k));
                            value = ((double)p*(double)m * 1.0f)/ (((double)16 * (double)s * 1.0f) * (((double)NbitPerFrame*1.0f)/k));
                            diff  = ((double)(fabs((double)(value-A))) * k)/k;
                            }
                        else
                            {
                            if (Nrate > cAtNRate1E7)
                                {
                                if (m !=1)
                                    continue;
                                }

                            }

                        if (needOther)
                            {
                            A     = A/k;
                            value = value/k;
                            }

                        if (diff < tempMinVal)
                            {
                            tempMinVal         = diff;
                            array.step         = (uint16)s;
                            array.positionMask = (uint16)p;
                            array.frc_en_mask  = (uint8)m;
                            array.value        = value;
                            array.diff         = diff;
                            array.max_pos      = (uint8)max_pos;
                            if (debug)
                                AtPrintf("step=%03d, PositionMask=%02d, frc_en_mask=%d, \r\n"
                                         "A=%.12f value=%.12f, diff=%.12f               \r\n",
                                         s , p, m, A, value, diff);
                            }

                        }
                    }
                }
            }
        }
    else
        {
        double T     = (double)16 * (double)array.step* (double)hs * (double)NbitPerFrame * 1.0f;
        double value = ((double)array.positionMask * (double)array.frc_en_mask * 1.0f)/T;
        array.diff  = (double)(fabs((double)(value-A)));
        array.max_pos = 16;
        A     = 1/Nrate;
        }
    *PositionMask = mMethodsGet((ThaAttController)self)->PositionMaskGet(array.max_pos, array.positionMask);
    *err_step     = array.step;


    needOther = NeedAnotherCalculate(NbitPerFrame, Nrate, array.step, &k);
    *frc_en_mask  = msk[array.frc_en_mask];
    ss =(double) fabs((double)(array.value - A))/A;
    if (needOther)
        {
        ss =(double) fabs((double)(array.value*k - A*k))/(A*k);
        }


    if (debug || mMethodsGet((ThaAttController)self)->IsCheckRateShow((ThaAttController)self))
        AtPrintf("===>step=%03d, PositionMask=%02d, frc_en_mask=%d,   \r\n"
                 "A=%.12f value=%.12f, diff=%.12f max_pos=%d ss=%.12f \r\n",
                 array.step*hs , array.positionMask, array.frc_en_mask,
                 A, array.value,array.diff, array.max_pos, ss);

    if (NbitPerFrame == cAtNbitPerFrameStm64)/* STM64*/
        {
        if (Nrate < cAtNRate1E5 && mMethodsGet((ThaAttController)self)->IsCheckRate((ThaAttController)self)) /*  Error for 1e-2,1e-3,1e-4 */
            return cAtErrorModeNotSupport;

        if (Nrate == cAtNRate1E5 &&
            mMethodsGet((ThaAttController)self)->IsCheckRate((ThaAttController)self) &&
            mMethodsGet((ThaAttController)self)->IsB1((ThaAttController)self,errorType))
            return cAtErrorModeNotSupport;

        return cAtOk;
        }

    if (isCalib)
        {
        eAtRet convertRet= mMethodsGet((ThaAttController)self)->RateConvertStep((ThaAttController)self, NbitPerFrame, Nrate, err_step);
        if (debug || mMethodsGet((ThaAttController)self)->IsCheckRateShow((ThaAttController)self))
            AtPrintf("===>RateConvertStep: step=0x%03X, PositionMask=0x%02X, frc_en_mask=0x%X\r\n",
                      *err_step, *PositionMask, *frc_en_mask);

        return convertRet;
        }

    if (ss > 0.15)
        {
        if (! mMethodsGet((ThaAttController)self)->IsCheckRate((ThaAttController)self))
            return cAtOk;
        return cAtErrorModeNotSupport;
        }
    return cAtOk;
    }

static eAtRet CalculateStepAndPositionMaskForRate(AtAttController self, uint32 errorType, uint32 Nrate, uint16 *err_step, uint16 *PositionMask)
    {
    tRateInfo  array;
    eBool      debug = cAtFalse;
    uint32     s = 0, p=0, m=1, max_pos=0;
    uint32     NbitPerFrame = ThaAttControllerNbitPerFrame((ThaAttController) self);
    uint32     MaxStep      = ThaAttControllerMaxStep((ThaAttController)self, cThaAttForceError,  errorType, Nrate, NbitPerFrame);
    double     A = ((double)1/(double)Nrate);
    double     ss = 0;
    double     tempMinVal = (double)999999999999;
    for (max_pos=16; max_pos<17; max_pos++)
        {
        for (s = 1; s < (MaxStep + 1); s++)
            {
            double T = (double)(max_pos*s*NbitPerFrame);
            for (p=1; p<(max_pos+1); p++)
                {
                double value = (((double)p * (double)m * 1.0f)/T);
                double diff  = (double)(fabs((double)(value-A)));
                if (diff < tempMinVal)
                    {
                    tempMinVal = diff;
                    array.step         = (uint16)s;
                    array.positionMask = (uint16)p;
                    array.frc_en_mask  = (uint8)m;
                    array.value        = value;
                    array.diff         = diff;
                    array.max_pos      = (uint8)max_pos;
                    if (debug)
                        AtPrintf("step=%03d, PositionMask=%02d, frc_en_mask=%d, \r\n"
                                 "A=%.12f value=%.12f, diff=%.12f               \r\n",
                                 s , p, m, A, value, diff);
                    }
                }
            }
        }

    *PositionMask = mMethodsGet((ThaAttController)self)->PositionMaskGet(array.max_pos, array.positionMask);
    *err_step     = array.step;
    ss =(double) fabs((double)(array.value - A))/A;
    if (debug || mMethodsGet((ThaAttController)self)->IsCheckRateShow((ThaAttController)self))
        AtPrintf("===>step=%03d, PositionMask=%02d, frc_en_mask=%d,   \r\n"
                 "A=%.12f value=%.12f, diff=%.12f max_pos=%d ss=%.12f \r\n",
                 array.step , array.positionMask, array.frc_en_mask,
                 A, array.value,array.diff, array.max_pos, ss);

    if (ss > 0.15)
        {
        if (!mMethodsGet((ThaAttController)self)->IsCheckRate((ThaAttController)self))
            return cAtOk;
        return cAtErrorModeNotSupport;
        }

    return cAtOk;
    }

static eAtRet CalculateStepAndPositionMaskForRateStep16bit(AtAttController self, uint32 errorType, uint32 Nrate, uint16 *err_step, uint16 *PositionMask)
    {
    eBool isE1 = mMethodsGet((ThaAttController)self)->IsE1((ThaAttController)self);
    uint32     s = Nrate/(isE1?16:8);/* 8 bit datamask. */
    uint32     MaxStep = ThaAttControllerMaxStep((ThaAttController)self, cThaAttForceError,  errorType, Nrate, 0);
    *PositionMask = 1;
    *err_step     = (uint16)s;
    if (s > MaxStep)
        {
        return cAtErrorModeNotSupport;
        }

    return cAtOk;
    }

static eAtRet HwForceErrorRate(ThaAttController self, uint32 errorType, uint32 rate)
    {
    eAtRet ret = cAtOk;

    if (AtAttControllerErrorForceIsSupported((AtAttController)self, errorType))
        {
        uint16 err_step       = 0;
        uint16 positionMask   = 0;
        uint8  frc_en_mask    = 1;
        uint32 Nrate          = mMethodsGet((ThaAttController)self)->CalculateNRate((ThaAttController)self, rate);
        uint32 errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
        if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
            ret = CalculateStepAndPositionMaskForRateStep16bit((AtAttController)self, errorType, Nrate, &err_step, &positionMask);
        else if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
            ret = CalculateStepAndPositionMaskForRate8bit((AtAttController)self, errorType, Nrate, &err_step, &positionMask, &frc_en_mask);
        else
            ret = CalculateStepAndPositionMaskForRate((AtAttController)self, errorType, Nrate, &err_step, &positionMask);

        if (ret != cAtOk)
            return ret;
        mThis(self)->errorInfo[errorTypeIndex].errorDataMask     = frc_en_mask;
        mThis(self)->errorInfo[errorTypeIndex].errorPositionMask = positionMask;
        mThis(self)->errorInfo[errorTypeIndex].errorStep         = err_step;
        mMethodsGet(mThis(self))->ForceErrorHelper(mThis(self), errorType, cAtAttForceTypeContinuous);

        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

static eAtRet ForceErrorRate(AtAttController self,
                             uint32 errorType,
                             eAtBerRate rate)
    {
    /* Duration of N frames*/
	eAtRet ret = cAtOk;
	uint16 err_step = 0;
	uint16 PositionMask;
    uint32 errorTypeIndex = 0;
    uint32 Nrate;
    uint8 frc_en_mask = 0;
    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;
    mMethodsGet(mThis(self))->ErrorForcingDataMaskSet(mThis(self), errorType, 0);
    errorTypeIndex  = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    mThis(self)->errorRate[errorTypeIndex]  = rate;

    /* Check rate and return error */
    Nrate = mMethodsGet((ThaAttController)self)->CalculateNRate((ThaAttController)self, rate);
    if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
        ret = CalculateStepAndPositionMaskForRateStep16bit(self, errorType, Nrate, &err_step, &PositionMask);
    else if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
    	ret = CalculateStepAndPositionMaskForRate8bit(self, errorType, Nrate, &err_step, &PositionMask, &frc_en_mask);
    else
    	ret = CalculateStepAndPositionMaskForRate(self, errorType, Nrate, &err_step, &PositionMask);

    return ret;
    }

static eAtRet ForceErrorDuration(AtAttController self, uint32 errorType, uint32 duration)
    {
    uint32 errorTypeIndex = 0;
    uint32 numError = 0;
    eAtRet ret = cAtOk;
    uint32 prevousDuration = 0;

    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;

    mMethodsGet(mThis(self))->ErrorForcingDataMaskSet(mThis(self), errorType, 0);
    errorTypeIndex  = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    numError        = mThis(self)->numErrors[errorTypeIndex];
    prevousDuration = mThis(self)->errorDuration[errorTypeIndex];
    mThis(self)->errorDuration[errorTypeIndex]           = duration;
    mThis(self)->errorInfo[errorTypeIndex].errorDataMask = 1;

    if (duration != 0 && numError != 0)
        {
        uint16 err_step     = 0;
        uint16 PositionMask = 0;

        ret = CalculateStepAndPositionMask(self, cThaAttForceError, errorType, cAtFalse, duration, mThis(self)->numErrors[errorTypeIndex], &err_step, &PositionMask);
        if (ret != cAtOk)
            {
            mThis(self)->errorDuration[errorTypeIndex] = prevousDuration;
            return ret;
            }

        mThis(self)->errorInfo[errorTypeIndex].errorPositionMask  = PositionMask;
        mThis(self)->errorInfo[errorTypeIndex].errorStep          = err_step;
        }
    else
        {
        mThis(self)->errorInfo[errorTypeIndex].errorPositionMask  = 0xFFFF;
        }

    return ret;
    }

static uint32 ForceErrorStepGet(AtAttController self, uint32 errorType)
    {
    uint16 numDw   = mMethodsGet(self)->LongNumDw(self);
    uint32 longRegVal[cAtNumDw];
    uint32 regAddr = 0, regFieldMask  = 0;
    uint32 step    = 0, regFieldShift = 0;

    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;

    regAddr = mMethodsGet(mThis(self))->RegAddressFromErrorType(mThis(self), errorType);
    AtOsalMemInit(longRegVal, 0, sizeof(longRegVal));
    mMethodsGet(self)->LongRead(self, regAddr, longRegVal, cAtNumDw);
    if (numDw > 1)
        {
        regFieldMask  = mMethodsGet(mThis(self))->HwStepMaskHo(mThis(self), cThaAttForceError, errorType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        step = mRegField(longRegVal[1], regField);

        if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
            {
            uint32 stepLoHwVal    = 0;
            regFieldMask  = mMethodsGet(mThis(self))->HwStepMaskLo(mThis(self), cThaAttForceError, errorType);
            regFieldShift = AtRegMaskToShift(regFieldMask);
            stepLoHwVal = mRegField(longRegVal[0], regField);
            step = (step << regFieldShift)|stepLoHwVal;
            }
        }

    return step;
    }

static eAtRet ForceErrorStepSet(AtAttController self, uint32 errorType, uint32 step)
    {
    uint32 errorTypeIndex = 0;
    eAtAttForceType forceType;

    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;

    errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    forceType = mThis(self)->errorForceType[errorTypeIndex];
    mThis(self)->errorInfo[errorTypeIndex].errorStep = step;
    mMethodsGet(mThis(self))->ForceErrorHelper(mThis(self), errorType, forceType);

    return cAtOk;
    }

static eAtRet ForceErrorStep2Set(AtAttController self, uint32 errorType, uint32 step)
    {
    uint32 errorTypeIndex = 0;
    eAtAttForceType forceType;

    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;

    errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    forceType = mThis(self)->errorForceType[errorTypeIndex];
    mThis(self)->errorInfo[errorTypeIndex].errorMiniStep = step;
    mMethodsGet(mThis(self))->ForceErrorHelper2nd(mThis(self), errorType, forceType);

    return cAtOk;
    }

static eAtRet ForceErrorBitPositionSet(AtAttController self, uint32 errorType, uint32 bitPosition)
    {
    uint32 errorTypeIndex = 0;
    eAtAttForceType forceType;

    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;

    errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    forceType = mThis(self)->errorForceType[errorTypeIndex];
    mThis(self)->errorInfo[errorTypeIndex].errorPositionMask = (uint16)bitPosition;
    mMethodsGet(mThis(self))->ForceErrorHelper(mThis(self), errorType, forceType);

    return cAtOk;
    }

static eAtRet ForceErrorBitPosition2Set(AtAttController self, uint32 errorType, uint32 bitPosition)
    {
    uint32 errorTypeIndex = 0;
    eAtAttForceType forceType;

    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;

    errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    forceType = mThis(self)->errorForceType[errorTypeIndex];
    mThis(self)->errorInfo[errorTypeIndex].errorMiniPositionMask = (uint16)bitPosition;
    mMethodsGet(mThis(self))->ForceErrorHelper2nd(mThis(self), errorType, forceType);

    return cAtOk;
    }

static eBool DataMaskIsValid(Tha6A210031PdhDe3AttController self, uint32 errorType, uint32 dataMask)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (ThaAttControllerIsDataMask8bit((ThaAttController)self, errorType))
        return (dataMask <= 255) ? cAtTrue : cAtFalse;
    return (dataMask <= 1) ? cAtTrue : cAtFalse;
    }

static eAtRet ForceErrorCrcMaskSet(AtAttController self, uint32 errorType, uint32 crcMask)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(crcMask);
    return cAtOk;
    }

static eAtRet ForceErrorCrcMask2Set(AtAttController self, uint32 errorType, uint32 crcMask)
    {
    AtUnused(self);
    AtUnused(errorType);
    AtUnused(crcMask);
    return cAtOk;
    }

static eAtRet ForceErrorDataMaskSet(AtAttController self, uint32 errorType, uint32 dataMask)
    {
    uint32 errorTypeIndex = 0;
    eAtAttForceType forceType= cAtAttForceTypeNone;
    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;
    if (mMethodsGet(mThis(self))->DataMaskIsValid(mThis(self), errorType, dataMask) == cAtFalse)
        return cAtErrorOutOfRangParm;
    errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    forceType = mThis(self)->errorForceType[errorTypeIndex];
    mThis(self)->errorInfo[errorTypeIndex].errorDataMask = (uint8)dataMask;
    mMethodsGet(mThis(self))->ForceErrorHelper(mThis(self), errorType, forceType);
    return cAtOk;
    }

static eAtRet ForceError(AtAttController self, uint32 errorType)
    {
    uint32 errorTypeIndex = 0;
    uint32 duration       = 0;
    uint32 numErrors      = 0;
    eAtRet ret            = cAtOk;
    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;
    mMethodsGet(mThis(self))->ErrorForcingDataMaskSet(mThis(self), errorType, 0);
    errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    duration = mThis(self)->errorDuration[errorTypeIndex];
    numErrors = mThis(self)->numErrors[errorTypeIndex];
    if (mThis(self)->errorMode[errorTypeIndex]
            == cAtAttForceErrorModeContinuous)
        {
        if (mThis(self)->numErrors[errorTypeIndex] == 0)
            AtAttControllerForceErrorNumErrorsSet(self, errorType, 1);
        if(!( mThis(self)->errorInfo[errorTypeIndex].errorStep))
        	mThis(self)->errorInfo[errorTypeIndex].errorStep          = mMethodsGet(mThis(self))->MinStep(mThis(self), errorType);
        mThis(self)->errorInfo[errorTypeIndex].errorPositionMask  = 0xFFFF;
        mThis(self)->errorMode[errorTypeIndex]                    = cAtAttForceErrorModeContinuous;
        mMethodsGet(mThis(self))->ForceErrorHelper(mThis(self), errorType, cAtAttForceTypeContinuous);
        }

    if (mThis(self)->errorMode[errorTypeIndex] == cAtAttForceErrorModeRate)
        ret = mMethodsGet((ThaAttController)self)->HwForceErrorRate((ThaAttController)self, errorType, mThis(self)->errorRate[errorTypeIndex]);

    if (mThis(self)->errorMode[errorTypeIndex] == cAtAttForceErrorModeNerrorInT)
        {
        if (duration != 0 && numErrors != 0)
            {
            uint16 err_step = 0;
            uint16 PositionMask = 0;
            ret = CalculateStepAndPositionMask(self, cThaAttForceError, errorType, cAtFalse, duration, mThis(self)->numErrors[errorTypeIndex], &err_step, &PositionMask);
            if (ret != cAtOk)
                return ret;
            mThis(self)->errorInfo[errorTypeIndex].errorPositionMask = PositionMask;
            mThis(self)->errorInfo[errorTypeIndex].errorStep = err_step;
            mMethodsGet(mThis(self))->ForceErrorHelper(mThis(self), errorType, cAtAttForceTypeOneShot);
            }
        else if (duration == 0 && numErrors != 0)
            {
        	if(!(mThis(self)->errorInfo[errorTypeIndex].errorStep))
        		mThis(self)->errorInfo[errorTypeIndex].errorStep = mMethodsGet(mThis(self))->MinStep(mThis(self), errorType);
            mThis(self)->errorInfo[errorTypeIndex].errorPositionMask = 0xFFFF;
            mMethodsGet(mThis(self))->ForceErrorHelper(mThis(self), errorType, cAtAttForceTypeOneShot);
            }
        else
            return cAtErrorInvlParm;
        }

    if (mThis(self)->errorMode[errorTypeIndex] == cAtAttForceErrorModeConsecutive)
		{
    	mThis(self)->errorInfo[errorTypeIndex].errorStep = 1;
    	mThis(self)->errorInfo[errorTypeIndex].errorPositionMask = 0xFFFF;
    	mMethodsGet(mThis(self))->ForceErrorHelper(mThis(self), errorType, cAtAttForceTypeOneShot);
		}

    if (mThis(self)->errorMode[errorTypeIndex] == cAtAttForceErrorModeOneshot)
        {
    	if(!(mThis(self)->errorInfo[errorTypeIndex].errorStep))
    		mThis(self)->errorInfo[errorTypeIndex].errorStep = mMethodsGet(mThis(self))->MinStep(mThis(self), errorType);

        mThis(self)->errorInfo[errorTypeIndex].errorPositionMask = 0xFFFF;
        mMethodsGet(mThis(self))->ForceErrorHelper(mThis(self), errorType, cAtAttForceTypeOneShot);
        }
    return ret;
    }

static eAtRet ForceErrorContinuous(AtAttController self, uint32 errorType)
    {
    uint32 errorTypeIndex = 0;

    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;
    errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    if (mThis(self)->numErrors[errorTypeIndex] == 0)
        AtAttControllerForceErrorNumErrorsSet(self, errorType, 1);
    mThis(self)->errorInfo[errorTypeIndex].errorStep = mMethodsGet(mThis(self))->MinStep(mThis(self), errorType);
    mThis(self)->errorInfo[errorTypeIndex].errorPositionMask = 0xFFFF;
    mThis(self)->errorMode[errorTypeIndex] = cAtAttForceErrorModeContinuous;
    return cAtOk;
    }

static eAtRet UnForceError(AtAttController self, uint32 errorType)
    {
    uint32 errorTypeIndex = 0;

    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;

    errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType);
    mMethodsGet(mThis(self))->ErrorForcingDataMaskSet(mThis(self), errorType, 0);
    mMethodsGet(mThis(self))->ErrorForcing2ndDataMaskSet(mThis(self), errorType, 0);
    mMethodsGet(mThis(self))->ErrorForcing3rdDataMaskSet(mThis(self), errorType, 0);
    mThis(self)->errorForceType[errorTypeIndex] = cAtAttForceTypeNone;
    return cAtOk;
    }

static eAtRet ForceErrorGet(AtAttController self, uint32 errorType, tAtAttForceErrorConfiguration *configuration)
    {
    uint32 longRegVal[cAtNumDw];
    uint32 regFieldMask, regFieldShift, regMaskWidth;
    uint8 isContinous, hwDataMask;
    uint32 regAddr;

    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;

    regFieldMask               = mMethodsGet(mThis(self))->PositionMaskConfigurationMaskHo(mThis(self), cThaAttForceError, errorType);
    regFieldShift              = AtRegMaskToShift(regFieldMask);
    configuration->bitPosition = mRegField(longRegVal[1], regField);

    regFieldMask               = mMethodsGet(mThis(self))->PositionMaskConfigurationMaskLo(mThis(self), cThaAttForceError, errorType);
    regFieldShift              = AtRegMaskToShift(regFieldMask);
    regMaskWidth               = RegMaskWidth(regFieldMask);
    configuration->bitPosition = (configuration->bitPosition << regMaskWidth) | mRegField(longRegVal[0], regField);

    configuration->dataMask    = mThis(self)->errorInfo[mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), errorType)].errorDataMask;

    regFieldMask = mMethodsGet(mThis(self))->DataMaskConfigurationMask(mThis(self), cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    hwDataMask = (uint8)mRegField(longRegVal[0], regField);

    regFieldMask = mMethodsGet(mThis(self))->HwForceModeConfigurationMask(mThis(self), cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    isContinous = (uint8)mRegField(longRegVal[0], regField);

    regAddr = mMethodsGet(mThis(self))->RegAddressFromErrorType(mThis(self), errorType);
    mMethodsGet(self)->LongRead(self, regAddr, longRegVal, cAtNumDw);
    regFieldMask = mMethodsGet(mThis(self))->NumberOfErrorsMask(mThis(self), cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    configuration->numErrors = mRegField(longRegVal[0], regField);

    if (hwDataMask == 0)
        configuration->forceType = cAtAttForceTypeNone;
    else
        {
        if (isContinous)
            configuration->forceType = cAtAttForceTypeContinuous;
        else
            configuration->forceType = cAtAttForceTypeOneShot;
        }

    return cAtOk;
    }

static eAtRet ForceErrorStatusGet(AtAttController self, uint32 errorType, tAtAttForceErrorStatus *status)
    {
    uint32 statusRegAddr;
    uint32 regVal;

    if (!AtAttControllerErrorForceIsSupported(self, errorType))
        return cAtErrorModeNotSupport;

    statusRegAddr = mMethodsGet(mThis(self))->StatusRegAddressFromErrorType(mThis(self), errorType);
    regVal = mMethodsGet(self)->Read(self, statusRegAddr);
    status->remainForceErrorStatus = mRegField(regVal, cRegStatusErrCnt);
    status->positionMaskStatus = mRegField(regVal, cRegStatusMskPosCnt);
    status->errorStatus = (uint8) mRegField(regVal, cRegStatusFrcErr);
    status->oneSecondEnableStatus = (uint8) mRegField(regVal, cRegStatusOneSecondEnb);

    return cAtOk;
    }

static eAtRet SetUpError(AtAttController self)
    {
    uint8 numErrorTypes = mMethodsGet(mThis(self))->NumErrorTypes(mThis(self));
    if (numErrorTypes != 0)
        {
        if (mThis(self)->errorForceType == NULL)
            mThis(self)->errorForceType = AtOsalMemAlloc(numErrorTypes * sizeof(eAtAttForceType));
        if (mThis(self)->numErrors == NULL)
            mThis(self)->numErrors = AtOsalMemAlloc(numErrorTypes * sizeof(uint32));
        if (mThis(self)->errorDuration == NULL)
            mThis(self)->errorDuration = AtOsalMemAlloc(numErrorTypes * sizeof(uint32));
        if (mThis(self)->errorInfo == NULL)
            mThis(self)->errorInfo = AtOsalMemAlloc(numErrorTypes * sizeof(tErrorInfo));
        if (mThis(self)->errorMode == NULL)
            mThis(self)->errorMode = AtOsalMemAlloc(numErrorTypes * sizeof(eAtAttForceErrorMode));
        if (mThis(self)->errorRate == NULL)
            mThis(self)->errorRate = AtOsalMemAlloc(numErrorTypes * sizeof(eAtBerRate));



        AtOsalMemInit(mThis(self)->errorForceType, 0, numErrorTypes * sizeof(eAtAttForceType));
        AtOsalMemInit(mThis(self)->numErrors, 0, numErrorTypes * sizeof(uint32));
        AtOsalMemInit(mThis(self)->errorDuration, 0, numErrorTypes * sizeof(uint32));
        AtOsalMemInit(mThis(self)->errorMode, 0, numErrorTypes * sizeof(eAtAttForceErrorMode));
        AtOsalMemInit(mThis(self)->errorRate, 0, numErrorTypes * sizeof(eAtBerRate));
        }
    return cAtOk;
    }

static eAtRet SetUpPointerAdj(AtAttController self)
    {
    uint8 numPointerAdjTypes = mMethodsGet(mThis(self))->NumPointerAdjTypes(mThis(self));
    if (numPointerAdjTypes != 0)
        {
        if (mThis(self)->pointerAdjNumEvent == NULL)
            mThis(self)->pointerAdjNumEvent = AtOsalMemAlloc(numPointerAdjTypes * sizeof(uint32));
        if (mThis(self)->pointerAdjDuration == NULL)
            mThis(self)->pointerAdjDuration = AtOsalMemAlloc(numPointerAdjTypes * sizeof(uint32));
        if (mThis(self)->pointerAdjMode == NULL)
            mThis(self)->pointerAdjMode = AtOsalMemAlloc(numPointerAdjTypes * sizeof(eAtAttForcePointerAdjMode));
        if (mThis(self)->pointerAdjInfo == NULL)
            mThis(self)->pointerAdjInfo = AtOsalMemAlloc(numPointerAdjTypes * sizeof(tPointerInfo));

        AtOsalMemInit(mThis(self)->pointerAdjNumEvent, 0, numPointerAdjTypes * sizeof(uint32));
        AtOsalMemInit(mThis(self)->pointerAdjDuration, 0, numPointerAdjTypes * sizeof(uint32));
        AtOsalMemInit(mThis(self)->pointerAdjMode, 0, numPointerAdjTypes * sizeof(eAtAttForcePointerAdjMode));
        AtOsalMemInit(mThis(self)->pointerAdjInfo, 0, numPointerAdjTypes * sizeof(tPointerInfo));
        }

    return cAtOk;
    }

static const char* AlarmIndexToString(uint8 alarmIndex)
    {
    static const char* cAttPdhDe3AlarmTypeStr[] = {"LOS","AIS","RAI","LOF"};
    if (alarmIndex >= mCount(cAttPdhDe3AlarmTypeStr))
            return cAttPdhDe3AlarmTypeStr[0];
    return cAttPdhDe3AlarmTypeStr[alarmIndex];
    }

static eAtRet SetUpAlarmRx(AtAttController self, uint8 numAlarmTypes)
    {
    uint8 i=0;
    if (mThis(self)->rx == NULL)
        {
        mThis(self)->rx = AtOsalMemAlloc(numAlarmTypes * sizeof(AtAttExpectChannel));
        AtOsalMemInit(mThis(self)->rx, 0, numAlarmTypes * sizeof(AtAttExpectChannel));
        for (i=0; i<numAlarmTypes; i++)
            {
            AtChannel channel = AtAttControllerChannelGet((AtAttController)self);
            mThis(self)->rx[i] = AtAttExpectChannelNew(channel, mMethodsGet(mThis(self))->AlarmIndexToString(i));
            }
        }
    return cAtOk;
    }

static eAtRet SetUpAlarm(AtAttController self)
    {
    uint8 numAlarmTypes = mMethodsGet(mThis(self))->NumAlarmTypes(mThis(self));

    if (numAlarmTypes != 0)
        {
            if (mThis(self)->alarmDuration == NULL)
                mThis(self)->alarmDuration =  AtOsalMemAlloc(numAlarmTypes * sizeof(uint32));
            if (mThis(self)->alarmInfo     == NULL)
                mThis(self)->alarmInfo     =  AtOsalMemAlloc(numAlarmTypes * sizeof(tAlarmInfo));
            if (mThis(self)->alarmMode     == NULL)
                mThis(self)->alarmMode     =  AtOsalMemAlloc(numAlarmTypes * sizeof(eAtAttForceAlarmMode));
            if (mThis(self)->alarmNumEvent == NULL)
                mThis(self)->alarmNumEvent =  AtOsalMemAlloc(numAlarmTypes * sizeof(uint32));
            if (mThis(self)->alarmNumFrame == NULL)
                mThis(self)->alarmNumFrame =  AtOsalMemAlloc(numAlarmTypes * sizeof(uint32));

            AtOsalMemInit(mThis(self)->alarmDuration, 0, numAlarmTypes * sizeof(uint32));
            AtOsalMemInit(mThis(self)->alarmInfo,     0, numAlarmTypes * sizeof(tAlarmInfo));
            AtOsalMemInit(mThis(self)->alarmMode,     0, numAlarmTypes * sizeof(eAtAttForceAlarmMode));
            AtOsalMemInit(mThis(self)->alarmNumEvent, 0, numAlarmTypes * sizeof(uint32));
            AtOsalMemInit(mThis(self)->alarmNumFrame, 0, numAlarmTypes * sizeof(uint32));
            SetUpAlarmRx(self, numAlarmTypes);
            }

    return cAtOk;
    }

static eAtRet SetUp(AtAttController self)
    {
    SetUpError(self);
    SetUpAlarm(self);
    SetUpPointerAdj(self);
    return cAtOk;
    }

static eAtRet InitErrorDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i;
    eAtRet ret = cAtOk;
    static const uint32 cAttPdhDe3ErrorTypeVal[] = {cAtAttPdhDe3ErrorTypeFbit, cAtAttPdhDe3ErrorTypeCpbit, cAtAttPdhDe3ErrorTypePbit,
                                                    cAtAttPdhDe3ErrorTypeFabyte, cAtAttPdhDe3ErrorTypeBip8byte, cAtAttPdhDe3ErrorTypeReibit,
                                                    cAtAttPdhDe3ErrorTypeNrbyte, cAtAttPdhDe3ErrorTypeGcbyte, cAtAttPdhDe3ErrorTypeFebe};

    for (i = 0; i < mCount(cAttPdhDe3ErrorTypeVal); i++)
        {
        if (AtAttControllerErrorForceIsSupported((AtAttController)self,cAttPdhDe3ErrorTypeVal[i]))
            {
            uint8 errorTypeIndex;
            ret |= AtAttControllerUnForceError((AtAttController)self, cAttPdhDe3ErrorTypeVal[i]);
            ret |= AtAttControllerForceErrorModeSet((AtAttController)self, cAttPdhDe3ErrorTypeVal[i],cAtAttForceErrorModeContinuous);
            errorTypeIndex = mMethodsGet(mThis(self))->ErrorTypeIndex(mThis(self), cAttPdhDe3ErrorTypeVal[i]);
            mThis(self)->errorInfo[errorTypeIndex].errorStep = mMethodsGet(mThis(self))->MinStep(mThis(self), cAttPdhDe3ErrorTypeVal[i]);/*At the receiver, window to detect LOF is 3ms. Apply for the current design you must config step_cfg must >= 29 to avoid LOF set.*/
            ret |= AtAttControllerForceErrorNumErrorsSet((AtAttController)self, cAttPdhDe3ErrorTypeVal[i], 1);
            ret |= AtAttControllerForceErrorBitPositionSet((AtAttController)self, cAttPdhDe3ErrorTypeVal[i], 0xFFFF);
            }
        }


    return ret;
    }

static eAtRet InitAlarmDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i;
    eAtRet ret = cAtOk;
    static const uint32 cAttPdhDe3AlarmTypeVal[] = {cAtPdhDe3AlarmLos,cAtPdhDe3AlarmAis,cAtPdhDe3AlarmRai,cAtPdhDe3AlarmLof};
    for (i = 0; i < mCount(cAttPdhDe3AlarmTypeVal); i++)
        {
        if (AtAttControllerAlarmForceIsSupported((AtAttController)self,cAttPdhDe3AlarmTypeVal[i]))
            {
            ret |= AtAttExpectChannelInit(mThis(self)->rx[i]);
            ret |= AtAttControllerUnForceAlarm((AtAttController)self, cAttPdhDe3AlarmTypeVal[i]);
            ret |= AtAttControllerForceAlarmModeSet((AtAttController)self, cAttPdhDe3AlarmTypeVal[i], cAtAttForceAlarmModeContinuous);
            ret |= AtAttControllerForceAlarmNumEventSet((AtAttController)self, cAttPdhDe3AlarmTypeVal[i], 0);
            if (cAttPdhDe3AlarmTypeVal[i] == cAtPdhDe3AlarmLos||cAttPdhDe3AlarmTypeVal[i] == cAtPdhDe3AlarmAis)
                {
                ret |= AtAttControllerForceAlarmDurationSet((AtAttController)self, cAttPdhDe3AlarmTypeVal[i], 1);
                }
            }
        }
    return ret;
    }



static eBool IsPointerAdj(Tha6A210031PdhDe3AttController self)
    {
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaAttControllerVersionReader((ThaAttController) self));
    AtUnused(self);

    if (hwVersion >= mMethodsGet(mThis(self))->StartVersionSupportPointerAdj(mThis(self)))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet InitPointerAdjDefault(Tha6A210031PdhDe3AttController self)
    {
    eAtRet ret = cAtOk;
    if (IsPointerAdj(self))
       {
       uint32 i;
       static const eAtSdhPathPointerAdjType cAttPointAdjTypeVal[] =
            {
            cAtSdhPathPointerAdjTypeIncrease, cAtSdhPathPointerAdjTypeDecrease
            };
       if (mMethodsGet(mThis(self))->NumPointerAdjTypes(mThis(self)) != 0)
            {
            for (i = 0; i < mCount(cAttPointAdjTypeVal); i++)
                {
                AtAttControllerUnForcePointerAdj((AtAttController) self,
                                                 cAttPointAdjTypeVal[i]);
                }
            }
       }
    return ret;
    }

static eAtRet InitDb(ThaAttController self)
    {
    eAtRet ret = cAtOk;
    ret |= mMethodsGet(mThis(self))->InitErrorDefault(mThis(self));
    ret |= mMethodsGet(mThis(self))->InitAlarmDefault(mThis(self));
    ret |= mMethodsGet(mThis(self))->InitPointerAdjDefault(mThis(self));
    return ret;
    }

static void DeleteError(AtObject self)
    {
    /* Error */
    if (mThis(self)->errorForceType)
        AtOsalMemFree(mThis(self)->errorForceType);
    if (mThis(self)->numErrors)
        AtOsalMemFree(mThis(self)->numErrors);
    if (mThis(self)->errorDuration)
        AtOsalMemFree(mThis(self)->errorDuration);
    if (mThis(self)->errorInfo)
        AtOsalMemFree(mThis(self)->errorInfo);
    if (mThis(self)->errorMode)
        AtOsalMemFree(mThis(self)->errorMode);
    if (mThis(self)->errorRate)
        AtOsalMemFree(mThis(self)->errorRate);

    mThis(self)->errorForceType         = NULL;

    mThis(self)->numErrors              = NULL;
    mThis(self)->errorDuration          = NULL;
    mThis(self)->errorInfo              = NULL;
    mThis(self)->errorMode              = NULL;
    mThis(self)->errorRate              = NULL;
    }

static void DeletePointerAdj(AtObject self)
    {
    /* Error */
    AtChannel channel = AtAttControllerChannelGet((AtAttController)self);
    AtModule module = AtChannelModuleGet(channel);
    if (AtModuleTypeGet(module)== cAtModuleSdh)
        {
        uint32 mode_i, pointerType_i;
        ThaAttSdhManager mngr = (ThaAttSdhManager)AtModuleSdhAttManager((AtModuleSdh) module);
        eAtAttForcePointerAdjMode modes[] = {cAtAttForcePointerAdjModeG783a,
                                             cAtAttForcePointerAdjModeG783b,
                                             cAtAttForcePointerAdjModeG783c,
                                             cAtAttForcePointerAdjModeG783d,
                                             cAtAttForcePointerAdjModeG783e,
                                             cAtAttForcePointerAdjModeG783f,
                                             cAtAttForcePointerAdjModeG783gPart1,
                                             cAtAttForcePointerAdjModeG783gPart2,
                                             cAtAttForcePointerAdjModeG783gPart3,
                                             cAtAttForcePointerAdjModeG783gPart4,
                                             cAtAttForcePointerAdjModeG783hParta,
                                             cAtAttForcePointerAdjModeG783hPartb,
                                             cAtAttForcePointerAdjModeG783hPartc,
                                             cAtAttForcePointerAdjModeG783hPartd,
                                             cAtAttForcePointerAdjModeG783i,
                                             cAtAttForcePointerAdjModeG783jParta,
                                             cAtAttForcePointerAdjModeG783jPartb,
                                             cAtAttForcePointerAdjModeG783jPartc,
                                             cAtAttForcePointerAdjModeG783jPartd};
        for (mode_i =0; mode_i < mCount(modes); mode_i++)
            {
            eAtSdhPathPointerAdjType pointerTypes[] = {cAtSdhPathPointerAdjTypeIncrease, cAtSdhPathPointerAdjTypeDecrease};
            for (pointerType_i =0; pointerType_i < mCount(pointerTypes); pointerType_i++)
                ThaAttSdhManagerForcePointerChannelRemove(mngr, (AtAttController)self, modes[mode_i], pointerTypes[pointerType_i], cAtFalse);
            }
        }

    if (mThis(self)->pointerAdjNumEvent)
        AtOsalMemFree(mThis(self)->pointerAdjNumEvent);
    if (mThis(self)->pointerAdjDuration)
        AtOsalMemFree(mThis(self)->pointerAdjDuration);
    if (mThis(self)->pointerAdjMode)
        AtOsalMemFree(mThis(self)->pointerAdjMode);
    if (mThis(self)->pointerAdjInfo)
        AtOsalMemFree(mThis(self)->pointerAdjInfo);

    mThis(self)->pointerAdjNumEvent     = NULL;
    mThis(self)->pointerAdjDuration     = NULL;
    mThis(self)->pointerAdjMode         = NULL;
    mThis(self)->pointerAdjInfo         = NULL;
    }

static void DeleteAlarmRxExpect(AtObject self)
    {
    if (mThis(self)->rx)
        {
        uint8 alarmTypeIndex=0;
        uint8 numAlarmTypes = mMethodsGet(mThis(self))->NumAlarmTypes(mThis(self));
        for (alarmTypeIndex = 0; alarmTypeIndex<numAlarmTypes; alarmTypeIndex++)
            {
            if (mThis(self)->rx[alarmTypeIndex])
                AtObjectDelete((AtObject)mThis(self)->rx[alarmTypeIndex]);
            mThis(self)->rx[alarmTypeIndex] = NULL;
            }
        AtOsalMemFree(mThis(self)->rx);
        }
    mThis(self)->rx = NULL;
    }

static void DeleteAlarm(AtObject self)
    {
    if (mThis(self)->alarmDuration)
        AtOsalMemFree(mThis(self)->alarmDuration);
    if (mThis(self)->alarmInfo)
        AtOsalMemFree(mThis(self)->alarmInfo);
    if (mThis(self)->alarmMode)
        AtOsalMemFree(mThis(self)->alarmMode);
    if (mThis(self)->alarmNumEvent)
        AtOsalMemFree(mThis(self)->alarmNumEvent);
    if (mThis(self)->alarmNumFrame)
        AtOsalMemFree(mThis(self)->alarmNumFrame);

    mThis(self)->alarmDuration      = NULL;
    mThis(self)->alarmInfo          = NULL;
    mThis(self)->alarmMode          = NULL;
    mThis(self)->alarmNumEvent      = NULL;
    mThis(self)->alarmNumFrame      = NULL;
    DeleteAlarmRxExpect(self);
    }

static void Delete(AtObject self)
    {
    DeleteError(self);
    DeleteAlarm(self);
    DeletePointerAdj(self);
    m_AtObjectMethods->Delete(self);
    }

static eBool De3IsDs3(Tha6A210031PdhDe3AttController self)
    {
    AtPdhDe3 de3 = (AtPdhDe3)AtAttControllerChannelGet((AtAttController)self);
    return AtPdhDe3IsDs3(de3);
    }

static uint32 StartVersionSupportE3SameAsDs3(Tha6A210031PdhDe3AttController self)
	{
	AtUnused(self);
	return 0xFFFFFFFF;
	}

static eBool IsE3SameAsDs3(Tha6A210031PdhDe3AttController self)
	{
	uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaAttControllerVersionReader((ThaAttController) self));
	AtUnused(self);

	if (hwVersion >= mMethodsGet(mThis(self))->StartVersionSupportE3SameAsDs3(mThis(self)))
	    return cAtTrue;
	return cAtFalse;
	}

static uint32 RegAddressAlarmUnit(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return 0xFFFFFFFF;
    }

static uint32 RegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtPdhDe3AlarmLof:
            {
            if (De3IsDs3(self))
                return cReg_upen_fbitcfg_Base;
            return cReg_upen_faB_cfg_Base;
            }
        case cAtPdhDe3AlarmLos:
            return cAttReg_upen_los_cfg;
        case cAtPdhDe3AlarmAis:
            {
            if (mMethodsGet(mThis(self))->IsE3SameAsDs3(self))
                return cAttReg_upen_ais_e3_cfg;
            else
                {
                if (De3IsDs3(self))
                    return cAttReg_upen_ais_cfg;
                return cAttReg_upen_ais_e3_cfg;
                }
            }
        case cAtPdhDe3AlarmRai:
            return cReg_upen_rdi_cfg_Base;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtPdhDe3AlarmLof:
            {
            if  (De3IsDs3(self))
            	return cReg_upen_fbitsta_Base;
            return cReg_upen_faB_sta_Base;
            }

        case cAtPdhDe3AlarmLos:
            return cAttReg_upen_los_sta;

        case cAtPdhDe3AlarmAis:
            {
			if (mMethodsGet(mThis(self))->IsE3SameAsDs3(self))
				return cAttReg_upen_ais_e3_sta;
            if (De3IsDs3(self))
                return cAttReg_upen_ais_sta;
            return cAttReg_upen_ais_e3_sta;
            }

        case cAtPdhDe3AlarmRai:
            return cReg_upen_rdi_sta_Base;

        default:
            return cBit31_0;
        }
    }

static uint8 AlarmTypeIndex(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtPdhDe3AlarmLos:
            return 0;
        case cAtPdhDe3AlarmAis:
            return 1;
        case cAtPdhDe3AlarmRai:
            return 2;
        case cAtPdhDe3AlarmLof:
            return 3;
        default:
            return 0;
        }
    }

static uint8 NumAlarmTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 4;
    }

static eAtRet CheckAlarmNumEvent(Tha6A210031PdhDe3AttController self, uint32 alarmType, uint32 numEvent)
	{
	AtUnused(self);
	if (alarmType == cAtPdhDe3AlarmRai)
		{
		if (numEvent > 1)
            return cAtErrorInvlParm;
        }
    return cAtOk;
    }

static eAtRet CheckAlarmNumFrame(Tha6A210031PdhDe3AttController self, uint32 alarmType, uint32 numFrame)
    {
    AtUnused(self);
    AtUnused(alarmType);

    if (numFrame > 0 && numFrame <= cAtMaxNumFrame)
        return cAtOk;

    AtPrintf("Error: numEvent must be [1..15] events\n");
    return cAtErrorInvlParm;
    }

static eAtRet SpecificAlarmConfig(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtOk;
    }

static uint32 CoefForceAlarmDuration(AtAttController self)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    AtModule module = AtChannelModuleGet(channel);
    if (AtModuleTypeGet(module)== cAtModuleSdh)
        {
        ThaAttSdhManager mngr = (ThaAttSdhManager)AtModuleSdhAttManager((AtModuleSdh) module);
        eAttSdhForceAlarmDurationUnit unit = ThaAttSdhManagerAlarmDurationUnitGet(mngr);
        return (uint32)(unit == cAttSdhForceAlarmDurationUnitSecond? 1000: 1);
        }
    else
        {
        ThaAttPdhManager mngr = (ThaAttPdhManager)AtModulePdhAttManager((AtModulePdh) module);
        eAttPdhForceAlarmDurationUnit unit = ThaAttPdhManagerAlarmDurationUnitGet(mngr);
        return (uint32)(unit == cAttPdhForceAlarmDurationUnitSecond? 1000: 1);
        }
    }

static eAtRet AlarmForcingDurationCheck(AtAttController self, uint32 alarmType, uint32 duration)
    {
    uint32 coef = CoefForceAlarmDuration(self);

    AtUnused(alarmType);
    AtUnused(duration);
    if (duration > (coef==1000?255:255000))
        return cAtErrorOutOfRangParm;

    return cAtOk;
    }

static eAtRet AlarmForcingDataMaskSet(AtAttController self, uint32 alarmType, uint32 dataMask)
    {
    uint32 regAddr = mMethodsGet(mThis(self))->RegAddressFromAlarmType(mThis(self), alarmType);
    return HwDataMaskSet(self, cThaAttForceAlarm,  alarmType, dataMask, regAddr);
    }

static uint16 AlarmHwPositionMaskLo(eAtAttForceAlarmMode mode, uint32 numEvent)
    {
    if (mode == cAtAttForceAlarmModeContinuous)
        return 0x0;
    else if (mode == cAtAttForceAlarmModeSetInT)
        return 0x0;
    else if (mode == cAtAttForceAlarmModeNeventInT)
        {
        /*uint16 mask[] = {0,0x8000,0x8080,0x8880,0x8888,0x9248,0x94A4,0xAA4A,0xAAAA};*/
        uint16 mask[] = {0,0x0080,0x0810,0x2082,0x2222,0x4894,0x4952,0x5A94,0x5AAA};
        if (numEvent >= 1 && numEvent <= cAtMaxNumEvent)
            return mask[numEvent];
        else
            return 0xAAAA;
        }
    return 0;
    }

static uint32 AlarmHwForceMode(eAtAttForceAlarmMode mode)
    {
    if (mode==cAtAttForceAlarmModeContinuous)
        return 1;
    else if (mode==cAtAttForceAlarmModeSetInT)
        return 0;
    else if (mode==cAtAttForceAlarmModeNeventInT)
        return 2;
    return 0;
    }

static uint32 LofHwForceMode(eAtAttForceAlarmMode mode)
    {
    if (mode==cAtAttForceAlarmModeContinuous)
        return 2; /*01x*/
    else if (mode==cAtAttForceAlarmModeSetInT)
        return 4;/*10x*/
    else if (mode==cAtAttForceAlarmModeNeventInT)
        return 7; /*111*/
    return 0;
    }

static uint8 HwRdiType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return 0;
    }

static uint32 StartVersionSupportDataMask8bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(errorType);
    AtUnused(self);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportDataMask2bit(ThaAttController self, uint32 errorType)
    {
    AtUnused(errorType);
    AtUnused(self);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportStep16bit(ThaAttController self)
    {
    AtUnused(self);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x9, 0x4100);
    }

static uint32 StartVersionSupportAlarmUnitConfig(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    }

static eBool IsAlarmForceV2(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(ThaAttControllerVersionReader((ThaAttController) self));

    AtUnused(self);
    AtUnused(alarmType);
    if (hwVersion >= mMethodsGet(mThis(self))->StartVersionSupportAlarmForceV2(mThis(self)))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsAisUneqOptimizeSupported(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtFalse;
    }

static eBool IsDataMask8bit(ThaAttController self, uint32 errorType)
	{
	ThaVersionReader version = ThaAttControllerVersionReader((ThaAttController) self);
	uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(version);

	AtUnused(self);
	AtUnused(errorType);
	if (hwVersion >= mMethodsGet(self)->StartVersionSupportDataMask8bit(self, errorType))
	    return cAtTrue;
	return cAtFalse;
	}

static eBool IsDataMask2bit(ThaAttController self, uint32 errorType)
    {
    ThaVersionReader version = ThaAttControllerVersionReader((ThaAttController) self);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(version);

    AtUnused(self);
    AtUnused(errorType);
    if (hwVersion >= mMethodsGet(self)->StartVersionSupportDataMask2bit(self, errorType))
        return cAtTrue;
    return cAtFalse;
    }

static eBool IsErrorForceV2(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    ThaVersionReader version = ThaAttControllerVersionReader((ThaAttController) self);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(version);

    AtUnused(self);
    AtUnused(errorType);
    if (hwVersion >= mMethodsGet(mThis(self))->StartVersionSupportAlarmForceV2(mThis(self)))
        return cAtTrue;
    return cAtFalse;
    }

static uint16 CalAlarmSet(AtAttController self, eAtAttForceAlarmMode mode, uint32 duration, uint16 *timer_mode, uint16 *numerror)
    {
    uint32 T = duration;
    uint32 d = 1;
    uint16 set = 0;
    uint16 heso = 1;

    AtUnused(self);
    *numerror = 1;
    if (mode == cAtAttForceAlarmModeSetInT)
        {
        if (T > 25500 && T <= 65535)
            {
            d           = 1 * heso;
            *timer_mode = 1;
            set         = 1;
            *numerror   = (uint16)(T/d);
            return set;
            }
        else if (T > 65535 && T <= 655350)
            {
            d = (uint16) (10 * heso);
            *timer_mode = 2;
            set = 1;
            *numerror = (uint16) (T / d);
            return set;
            }
        else if (T > 655350 && T <= 6553500)
            {
            d = (uint16) (100 * heso);
            *timer_mode = 3;
            set = 1;
            *numerror = (uint16) (T / d);
            return set;
            }
        }
    if (T >= 2550 && T < 25500)
        {
        d = (uint16) (100 * heso);
        *timer_mode = 3;
        }
    else if (T >= 255 && T < 2550)
        {
        d = (uint16) (10 * heso);
        *timer_mode = 2;
        }
    else if (T > 0 && T < 255)
        {
        d = (uint16) (1 * heso);
        *timer_mode = 1;
        }

    set = (uint16) (T / d);

    return set;
    }

static uint16 CalAlarmSetInNFrameSet(AtAttController self, eAtAttForceAlarmMode mode, uint32 numeFrame, uint16 *timer_mode)
    {
    uint32 d    = 1;
    uint16 heso = 1;
    uint32 value;
    float   value1;
    float diffmin, diff1;

    AtUnused(self);
    AtUnused(mode);
    /*-------------------------------------------------*/
    if (numeFrame > 2550)
        {
        float value100 = (float)numeFrame/(float)100;
        float diff100  = ((float)(fabs((double)(numeFrame-(double)(MakeRoundUint32((float)value100)*100)))));
        diffmin  = diff100;
        d        = (uint32)(100 * heso);
        value    = (uint32)MakeRoundUint32(value100);
        }
    /*-------------------------------------------------*/
    if (numeFrame > 255)
        {
        float value10  = (float)numeFrame/(float)10;
        float diff10   = ((float)(fabs((double)(numeFrame-(double)(MakeRoundUint32((float)value10)*10)))));
        if (diffmin > diff10)
            {
            diffmin = diff10;
            d       = (uint32)(10 * heso);
            value   = (uint32)MakeRoundUint32(value10);
            }
        }
    /*-------------------------------------------------*/
    value1   = (float)numeFrame/(float)1;
    diff1    = ((float)(fabs((double)(numeFrame-(double)(MakeRoundUint32((float)value1)*(double)1)))));
    if (diffmin > diff1)
        {
        diffmin = diff1;
        d       = (uint32)(1 * heso);
        value   = (uint32)MakeRoundUint32(value1);
        }
    /*-------------------------------------------------*/
    if (d==100)
        *timer_mode = 3;
    else if (d==10)
        *timer_mode = 2;
    else
        *timer_mode = 1;

    return (uint16)value;
    }

static uint32 AlarmAisUneqOtimizeTypeSw2Hw(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtSdhPathAlarmAis:
            return 1;
        case cAtSdhPathAlarmUneq:
            return 2;
        default: return 0;
        }
    }

static void HwForceAlarmAisUneqOtimize(AtAttController self, uint32 alarmType, uint32 *longRegVal)
    {
    mRegFieldSet(longRegVal[1], cAttAisUneqOptimizeType, AlarmAisUneqOtimizeTypeSw2Hw(self, alarmType));
    }

static void SwForceV2Alarm(AtAttController self, eBool en, uint32 alarmType)
    {
    uint8 alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    eAtAttForceAlarmMode mode = mThis(self)->alarmMode[alarmTypeIndex];
    uint32 numEvent  = mThis(self)->alarmNumEvent[alarmTypeIndex];
    uint32 duration  = mThis(self)->alarmDuration[alarmTypeIndex];
    tAlarmInfo info;
    uint32 coef = CoefForceAlarmDuration(self);

    AtOsalMemInit(&info, 0, sizeof(tAlarmInfo));
    info.step = 1;
    if (mode == cAtAttForceAlarmModeContinuous)
        {
        /* clear[]=0x0  set [49:42] = 0xFF, los_errnum [16_1]= 0xFFFF, dataMask[17] = en/dis force_mode [0] = 1.*/
        info.set         = 0xFF;
        info.clear       = info.set;
        info.bitPosition = 0xFFFF;
        info.dataMask    = en ? 1 : 0;
        info.numErrors   = 0xFFFF;
        info.forceType   = 1; /* [0] = 1 is continous*/
        info.unit = cAtAlarmUnit1ms;
        }
    else if (mode == cAtAttForceAlarmModeSetInT)
        {
        info.set         = CalAlarmSet(self, mode, duration*coef, &info.timermode, &info.numErrors);
        info.clear       = 0;
        info.bitPosition = 0xFFFF;
        info.dataMask    = en?1:0;
        info.forceType   = 0; /* [0] = 0 is oneshot*/
        info.unit = cAtAlarmUnit1ms;
        }
    else if (mode == cAtAttForceAlarmModeSetInNFrame)
        {
        uint32 numFrame  = mThis(self)->alarmNumFrame[alarmTypeIndex];
        info.set         = (uint16)CalAlarmSetInNFrameSet(self, mode, numFrame, &info.timermode);
        info.clear       = info.set;
        info.bitPosition = 0xFFFF;
        info.dataMask    = en?1:0;
        info.forceType   = 0; /* [0] = 0 is oneshot */
        info.numErrors   = 1;
        info.unit = cAtAlarmUnit125us;
        }
    else if (mode == cAtAttForceAlarmModeNeventInT)
        {
        uint16 num  = 0;
        info.numErrors   = (uint16)(numEvent * duration);
        info.set         = CalAlarmSet(self, mode, (uint32)((duration*coef)/((uint32)info.numErrors*2)), &info.timermode, &num);
        info.clear       = info.set;
        info.bitPosition = 0xFFFF;
        info.dataMask    = en?1:0;
        info.forceType   = 0; /* [0] = 0 is oneshot*/
        info.unit = cAtAlarmUnit1ms;
        }
    AtOsalMemCpy(&mThis(self)->alarmInfo[alarmTypeIndex], &info, sizeof(tAlarmInfo));
    }


static eAtRet HwForceV2Alarm(AtAttController self, eBool en, uint32 alarmType)
    {
    uint32 longRegVal[cAtNumDw];
    uint16 numDw          = mMethodsGet(self)->LongNumDw(self);
    uint8  alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    uint32 regAddr        = mMethodsGet(mThis(self))->RegAddressFromAlarmType(mThis(self), alarmType);
    uint16 bitPosition    = mThis(self)->alarmInfo[alarmTypeIndex].bitPosition;
    eAlarmUnit unit       =  mThis(self)->alarmInfo[alarmTypeIndex].unit;
    uint32 regFieldMask   = 0, regFieldShift = 0;

    if (en)
        AlarmForcingDataMaskSet(self, alarmType, 0);
    mMethodsGet(mThis(self))->AlarmUnitSet(mThis(self), alarmType, unit);
    mMethodsGet(self)->LongRead(self, regAddr, longRegVal, cAtNumDw);
    if (!en)
        {
        longRegVal[1] = 0;
        longRegVal[0] = 0;
        mMethodsGet(self)->LongWrite(self, regAddr, longRegVal, cAtNumDw);
        return cAtOk;
        }

    if (numDw > 1)
        {
        mRegFieldSet(longRegVal[1], c_upen_cpb_cfg_cfgcpb_err_timermode_,  mThis(self)->alarmInfo[alarmTypeIndex].timermode);
        mRegFieldSet(longRegVal[1], c_upen_cpb_cfg_cfgcpb_err_timerclear_, mThis(self)->alarmInfo[alarmTypeIndex].clear);
        mRegFieldSet(longRegVal[1], c_upen_cpb_cfg_cfgcpb_err_timerset_,   mThis(self)->alarmInfo[alarmTypeIndex].set);

        regFieldMask  = mMethodsGet(mThis(self))->HwStepMaskHo(mThis(self), cThaAttForceAlarm, 0);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, mThis(self)->alarmInfo[alarmTypeIndex].step);

        if (mMethodsGet(mThis(self))->AlarmIsSpecialRdi(mThis(self), alarmType))
            {
            regFieldMask  = mMethodsGet(mThis(self))->RdiTypeMask(mThis(self), cThaAttForceAlarm, alarmType);
            regFieldShift = AtRegMaskToShift(regFieldMask);
            mRegFieldSet(longRegVal[1], regField, mMethodsGet(mThis(self))->HwRdiType(mThis(self), alarmType));
            }
        }

    /* Configure bit-mask position: 16 position: [33:18]*/
    if (numDw > 1)
        {
        uint32 bitPositionHwVal = 0;
        uint32 regMaskWidth     = 0;

        /*[33:32]*/
        regFieldMask     = mMethodsGet(mThis(self))->PositionMaskConfigurationMaskHo(mThis(self), cThaAttForceAlarm, alarmType);
        regFieldShift    = AtRegMaskToShift(regFieldMask);

        regMaskWidth     = RegMaskWidth(mMethodsGet(mThis(self))->PositionMaskConfigurationMaskLo(mThis(self), cThaAttForceAlarm, alarmType));
        bitPositionHwVal = (uint32) (bitPosition >> regMaskWidth);
        mRegFieldSet(longRegVal[1], regField, bitPositionHwVal);
        }

    /*[31:18]*/
    regFieldMask = mMethodsGet(mThis(self))->PositionMaskConfigurationMaskLo(mThis(self), cThaAttForceAlarm, alarmType); 
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, bitPosition);

    /* Configure datamask (0..1) [17] */
    regFieldMask = mMethodsGet(mThis(self))->DataMaskConfigurationMask(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, mThis(self)->alarmInfo[alarmTypeIndex].dataMask);

    /* Number of Error [16:1] */
    regFieldMask = mMethodsGet(mThis(self))->NumberOfErrorsMask(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, mThis(self)->alarmInfo[alarmTypeIndex].numErrors);

    /* Configure one second enable [0] */
    regFieldMask = mMethodsGet(mThis(self))->HwForceModeConfigurationMask(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, mThis(self)->alarmInfo[alarmTypeIndex].forceType);

    if (Tha6A210031PdhDe3AttControllerIsAisUneqOptimizeSupported(mThis(self), alarmType))
        HwForceAlarmAisUneqOtimize(self, alarmType, longRegVal);

    mMethodsGet(self)->LongWrite(self, regAddr, longRegVal, cAtNumDw);
    mMethodsGet(mThis(self))->SpecificAlarmConfig(mThis(self), alarmType);
    return cAtOk;
    }

static void SwForceAlarm(AtAttController self, eBool en, uint32 alarmType)
    {
    uint8 alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    uint32 numEvent      = mThis(self)->alarmNumEvent[alarmTypeIndex];
    eAtAttForceAlarmMode mode = mThis(self)->alarmMode[alarmTypeIndex];
    tAlarmInfo info;
    AtOsalMemInit(&info, 0, sizeof(tAlarmInfo));
    info.dataMask    = en ? 1 : 0;
    info.bitPosition = AlarmHwPositionMaskLo(mode, numEvent);
    if (mMethodsGet(mThis(self))->AlarmIsFromError(mThis(self), alarmType))
        info.forceType = (uint16) LofHwForceMode(mode);
    else
        info.forceType = (uint16) AlarmHwForceMode(mode);
    AtOsalMemCpy(&mThis(self)->alarmInfo[alarmTypeIndex], &info, sizeof(tAlarmInfo));
    }

static uint32 RdiAlarmsGet(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);

    if (alarmType & cAtSdhPathAlarmRdi)
        return cAtSdhPathAlarmRdi;

    else if (alarmType & cAtSdhPathAlarmErdiS)
        return cAtSdhPathAlarmErdiS;

    else if (alarmType & cAtSdhPathAlarmErdiC)
         return cAtSdhPathAlarmErdiC;

    else if (alarmType & cAtSdhPathAlarmErdiP)
        return cAtSdhPathAlarmErdiP;

    else
        return 0;
    }

static eAtRet VcxERdiAlarmForce(AtAttController self, uint32 alarmType)
    {
    uint32 g1RdiValue = mMethodsGet(mThis(self))->HwRdiType(mThis(self), alarmType);
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet(self);
    uint32  g1Value = (uint32)AtSdhChannelTxOverheadByteGet(channel, cAtSdhPathOverheadByteG1);
    uint32 g1RdiMask = cBit3_1;
    uint32 g1RdiShift = 1;
    mRegFieldSet(g1Value, g1Rdi, g1RdiValue);
    AtSdhChannelTxOverheadByteSet(channel, cAtSdhPathOverheadByteG1, (uint8)g1Value);
    return cAtOk;
    }

static eAtRet Vc1xERdiAlarmForce(AtAttController self, uint32 alarmType)
    {
    uint32 overheadByte;
    uint32 k4RdiMask, k4RdiShift, v5RdiMask, v5RdiShift;
    uint32 k4v5RdiValue = mMethodsGet(mThis(self))->HwRdiType(mThis(self), alarmType);
    AtSdhChannel channel = (AtSdhChannel)AtAttControllerChannelGet(self);

    /* Set value K4 */
    overheadByte = (uint32)AtSdhChannelTxOverheadByteGet(channel, cAtSdhPathOverheadByteK4);
    k4RdiMask = cBit3_1;
    k4RdiShift = 1;
    mRegFieldSet(overheadByte, k4Rdi, k4v5RdiValue);
    AtSdhChannelTxOverheadByteSet(channel, cAtSdhPathOverheadByteK4, (uint8)overheadByte);

    /* Set value V5 */
    overheadByte = (uint32)AtSdhChannelTxOverheadByteGet(channel, cAtSdhPathOverheadByteV5);
    v5RdiMask = cBit0;
    v5RdiShift = 0;
    mRegFieldSet(overheadByte, v5Rdi, (k4v5RdiValue & cBit3) >> 3);
    AtSdhChannelTxOverheadByteSet(channel, cAtSdhPathOverheadByteV5, (uint8)overheadByte);

    return cAtOk;
    }

static eAtRet ERdiAlarmForce(AtAttController self, uint32 alarmType)
    {
    AtSdhChannel sdhChannel = (AtSdhChannel)AtAttControllerChannelGet(self);

    if (AtSdhChannelIsHoVc(sdhChannel))
        return VcxERdiAlarmForce(self, alarmType);

    return Vc1xERdiAlarmForce(self, alarmType);
    }

static eAtRet HwForceAlarm(AtAttController self, eBool en, uint32 alarmType)
    {
    eAtRet ret = cAtOk;
    uint32 longRegVal[cAtNumDw];
    uint32 rdiMasks;
    uint32 regFieldMask  = 0, regFieldShift = 0;
    uint8 alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    uint32 regAddr       = mMethodsGet(mThis(self))->RegAddressFromAlarmType(mThis(self), alarmType);
    eAtAttForceAlarmMode mode = mThis(self)->alarmMode[alarmTypeIndex];
    uint16 PositionMask  = 0;
    uint32 numEvent      = mThis(self)->alarmNumEvent[alarmTypeIndex];
    uint32 duration      = mThis(self)->alarmDuration[alarmTypeIndex];

    if (en && mode != cAtAttForceAlarmModeContinuous)
        AlarmForcingDataMaskSet(self, alarmType, 0);

    mMethodsGet(self)->LongRead(self, regAddr, longRegVal, cAtNumDw);
    longRegVal[1] = 0;

    PositionMask = mThis(self)->alarmInfo[alarmTypeIndex].bitPosition;
    regFieldMask = mMethodsGet(mThis(self))->PositionMaskConfigurationMaskLo(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, PositionMask);

    regFieldMask = mMethodsGet(mThis(self))->HwForceModeConfigurationMask(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, mThis(self)->alarmInfo[alarmTypeIndex].forceType);

    regFieldMask = mMethodsGet(mThis(self))->NumberOfErrorsMask(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, numEvent);

    regFieldMask = mMethodsGet(mThis(self))->DataMaskConfigurationMask(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, mThis(self)->alarmInfo[alarmTypeIndex].dataMask);

    if ((rdiMasks = RdiAlarmsGet(mThis(self), alarmType)))
        ret |= ERdiAlarmForce(self, en ? rdiMasks : 0);

    regFieldMask = mMethodsGet(mThis(self))->DurationConfigurationMask(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, duration);

    mMethodsGet(self)->LongWrite(self, regAddr, longRegVal, cAtNumDw);
    mMethodsGet(mThis(self))->SpecificAlarmConfig(mThis(self), alarmType);
    return ret;
    }

static eAtRet ForceAlarm(AtAttController self, uint32 alarmType)
    {
    eAtRet ret = cAtOk;

    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;

    if (mMethodsGet(mThis(self))->IsAlarmForceV2(mThis(self), alarmType))
        {
        /* Note Version 2 is the version that support force counter and alarm same module */
        SwForceV2Alarm(self, cAtTrue, alarmType);
        HwForceV2Alarm(self, cAtTrue, alarmType);
        }
    else
        {
        SwForceAlarm(self, cAtTrue, alarmType);
        HwForceAlarm(self, cAtTrue, alarmType);
        }
    return ret;
    }

static eAtRet ForceAlarmModeSet(AtAttController self, uint32 alarmType, eAtAttForceAlarmMode mode)
    {
    eAtRet ret            = cAtOk;
    uint8  alarmTypeIndex = 0;

    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);

    if (mThis(self)->alarmMode[alarmTypeIndex] != mode)
    	{
        AlarmForcingDataMaskSet(self, alarmType, 0);
		if (mMethodsGet(mThis(self))->IsAlarmForceV2(mThis(self), alarmType))
			HwForceV2Alarm(self, cAtFalse, alarmType);
        }

    mThis(self)->alarmMode[alarmTypeIndex] = mode;
    return ret;
    }

static eAtRet AlarmHwNumEventSet(AtAttController self, uint32 alarmType, uint32 alarmTypeIndex)
    {
    uint32 regFieldMask  = 0, regFieldShift = 0;
    uint32 regAddr       = mMethodsGet(mThis(self))->RegAddressFromAlarmType(mThis(self), alarmType);
    uint32 numEvent      = mThis(self)->alarmNumEvent[alarmTypeIndex];
    uint32 longRegVal[cAtNumDw];
    mMethodsGet(self)->LongRead(self, regAddr, longRegVal, cAtNumDw);

    regFieldMask = mMethodsGet(mThis(self))->NumberOfErrorsMask(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, numEvent);
    mMethodsGet(self)->LongWrite(self, regAddr, longRegVal, cAtNumDw);
    return cAtOk;
    }

static eAtRet ForceAlarmNumEventSet(AtAttController self, uint32 alarmType, uint32 numEvent)
    {
    eAtRet ret = cAtOk;
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    AlarmForcingDataMaskSet(self, alarmType, 0);
    ret = mMethodsGet(mThis(self))->CheckAlarmNumEvent(mThis(self), alarmType, numEvent);
    if (ret != cAtOk)
    	{
    	mThis(self)->alarmNumEvent[alarmTypeIndex] = 1;
    	return ret;
    	}

    if (mMethodsGet(mThis(self))->IsAlarmForceV2(mThis(self), alarmType))
        HwForceV2Alarm(self, cAtFalse, alarmType);
    else
        {
        if (numEvent < 1 || numEvent > cAtMaxNumEvent)
            {
            AtPrintf("Error: numEvent must be [1..8] events\n");
            return cAtErrorOutOfRangParm;
            }
        AlarmHwNumEventSet(self, alarmType, alarmTypeIndex);
        }

    mThis(self)->alarmNumEvent[alarmTypeIndex] = numEvent;
    return ret;
    }

static eAtRet ForceAlarmNumFrameSet(AtAttController self, uint32 alarmType, uint32 numFrame)
    {
    eAtRet ret = cAtOk;
    uint32 alarmTypeIndex = 0;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    AlarmForcingDataMaskSet(self, alarmType, 0);
    ret = mMethodsGet(mThis(self))->CheckAlarmNumFrame(mThis(self), alarmType, numFrame);
    if (ret != cAtOk)
        {
        mThis(self)->alarmNumEvent[alarmTypeIndex] = 1;
        return ret;
        }

    HwForceV2Alarm(self, cAtFalse, alarmType);
    mThis(self)->alarmNumFrame[alarmTypeIndex] = numFrame;
    return ret;
    }

static eAtRet ForceAlarmDurationSet(AtAttController self, uint32 alarmType, uint32 duration)
    {
    uint32 alarmTypeIndex = 0;
    eAtRet ret            = cAtOk;
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    AlarmForcingDataMaskSet(self, alarmType, 0);
    if (mMethodsGet(mThis(self))->IsAlarmForceV2(mThis(self), alarmType))
        HwForceV2Alarm(self, cAtFalse, alarmType);
    else
        {
        ret = AlarmForcingDurationCheck(self, alarmType, (uint32)(duration));
        if (ret != cAtOk)
            {
            AtPrintf("Error: Duration must be [1..255] second\n");
            return ret;
            }
        }
    mThis(self)->alarmDuration[alarmTypeIndex] = duration;
    return cAtOk;
    }

static eAtRet ForceAlarmStepSet(AtAttController self, uint32 alarmType, uint32 step)
    {
    uint32 alarmTypeIndex = 0;

    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;
    if (step > cAtAttMaxStep)
        return cAtErrorOutOfRangParm;
    AlarmForcingDataMaskSet(self, alarmType, 0);
    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    mThis(self)->alarmInfo[alarmTypeIndex].step = (uint8)step;

    if (mMethodsGet(mThis(self))->IsAlarmForceV2(mThis(self), alarmType))
        HwForceV2Alarm(self, cAtTrue, alarmType);
    else
        HwForceAlarm(self, cAtTrue, alarmType);
    return cAtOk;
    }

static eAtRet ForceAlarmBitPositionSet(AtAttController self, uint32 alarmType, uint32 bitPosition)
    {
    eAtRet ret = cAtOk;
    uint8 alarmTypeIndex;

    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;

    if (bitPosition > cAtAttMaxBitPosition)
        return cAtErrorOutOfRangParm;

    alarmTypeIndex = mMethodsGet(mThis(self))->AlarmTypeIndex(mThis(self), alarmType);
    mThis(self)->alarmInfo[alarmTypeIndex].bitPosition = (uint16) bitPosition;

    if (mMethodsGet(mThis(self))->IsAlarmForceV2(mThis(self), alarmType))
        HwForceV2Alarm(self, cAtTrue, alarmType);
    else
        HwForceAlarm(self, cAtTrue, alarmType);

    return ret;
    }

static eAtRet ForceAlarmContinuous(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtErrorModeNotSupport;
    }

static eAtRet ForceAlarmPerSecond(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    AtUnused(alarmType);
    return cAtErrorModeNotSupport;
    }

static eAtRet UnForceAlarm(AtAttController self, uint32 alarmType)
    {
    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;

    if (mMethodsGet(mThis(self))->IsAlarmForceV2(mThis(self), alarmType))
        HwForceV2Alarm(self, cAtFalse, alarmType);
    else
        HwForceAlarm(self, cAtFalse, alarmType);
    return cAtOk;
    }

static eAtRet ForceAlarmGet(AtAttController self, uint32 alarmType, tAtAttForceAlarmConfiguration *configuration)
    {
    uint32 longRegVal[cAtNumDw];
    uint32 regFieldMask = 0, regFieldShift = 0;
    uint8  isContinous  = 0, hwDataMask    = 0;
    uint32 regAddr = 0;

    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;

    regFieldMask = mMethodsGet(mThis(self))->NumberOfErrorsMask(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift = AtRegMaskToShift(regFieldMask);

    regAddr = mMethodsGet(mThis(self))->RegAddressFromAlarmType(mThis(self), alarmType);
    mMethodsGet(self)->LongRead(self, regAddr, longRegVal, cAtNumDw);
    configuration->numEvent = mRegField(longRegVal[0], regField);

    regFieldMask = mMethodsGet(mThis(self))->DataMaskConfigurationMask(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    hwDataMask = (uint8)mRegField(longRegVal[0], regField);

    regFieldMask = mMethodsGet(mThis(self))->HwForceModeConfigurationMask(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    isContinous = (uint8)mRegField(longRegVal[0], regField);

    if (hwDataMask == 0)
        configuration->forceType = cAtAttForceTypeNone;
    else
    	{
    	if (isContinous)
    		configuration->forceType = cAtAttForceTypeContinuous;
    	else
    		configuration->forceType = cAtAttForceTypeOneShot;
    	}
    regFieldMask            = mMethodsGet(mThis(self))->DurationConfigurationMask(mThis(self), cThaAttForceAlarm, alarmType);
    regFieldShift           = AtRegMaskToShift(regFieldMask);
    configuration->duration = (uint8)mRegField(longRegVal[0], regField);
    return cAtOk;
    }

static eAtRet ForceAlarmStatusGet(AtAttController self, uint32 alarmType, tAtAttForceAlarmStatus *status)
    {
    uint32 statusRegAddr;
    uint32 regVal;

    if (!AtAttControllerAlarmForceIsSupported(self, alarmType))
        return cAtErrorModeNotSupport;

    statusRegAddr = mMethodsGet(mThis(self))->StatusRegAddressFromAlarmType(mThis(self), alarmType);
    regVal                   = mMethodsGet(self)->Read(self, statusRegAddr);
    status->positionIndex    = mRegField(regVal, c_upen_ais_sta_aismsk_posst_);
    status->eventCounter     = mRegField(regVal, c_upen_ais_sta_ais_seccntst_);
    status->secondCounter    = mRegField(regVal, c_upen_ais_sta_aismseccntst_);
    status->updatedStatus    = mRegField(regVal, c_upen_ais_sta_aisupd_cntst_);
    status->miliSecondStatus = mRegField(regVal, c_upen_ais_sta_aistimmsecst_);
    status->secondStatus     = mRegField(regVal, c_upen_ais_sta_aistim_secst_);
    status->enableStatus     = mRegField(regVal, c_upen_ais_sta_aisosecenbst_);

    return cAtOk;
    }

static eBool AlarmForceIsSupported(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtPdhDe3AlarmLos:
        case cAtPdhDe3AlarmAis:
        case cAtPdhDe3AlarmRai:
        case cAtPdhDe3AlarmLof:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eBool ErrorForceIsSupported(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtAttPdhDe3ErrorTypeFbit:
        case cAtAttPdhDe3ErrorTypeCpbit:
        case cAtAttPdhDe3ErrorTypePbit:
        case cAtAttPdhDe3ErrorTypeFabyte:
        case cAtAttPdhDe3ErrorTypeBip8byte:
        case cAtAttPdhDe3ErrorTypeReibit:
        case cAtAttPdhDe3ErrorTypeNrbyte:
        case cAtAttPdhDe3ErrorTypeGcbyte:
        case cAtAttPdhDe3ErrorTypeFebe:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static ThaCdrController CdrControllerGet(ThaAttController self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)AtAttControllerChannelGet((AtAttController)self);
    return ThaPdhDe3CdrControllerGet(de3);
    }

static uint32 FrequenceOffsetBase(ThaAttController self)
    {
    AtPdhDe3 de3 = (AtPdhDe3)AtAttControllerChannelGet((AtAttController)self);
    return AtPdhDe3IsE3(de3) ? cBaseNcoValE3 : cBaseNcoValDs3;
    }

static uint32 FrequenceOffsetStep(ThaAttController self)
    {
    AtPdhDe3 de3 = (AtPdhDe3)AtAttControllerChannelGet((AtAttController)self);
    return AtPdhDe3IsE3(de3) ? cNcoValPpbOffsetE3 : cNcoValPpbOffsetDS3;
    }

static uint32 NbitPerFrame(ThaAttController self)
    {
    AtPdhDe3 de3 = (AtPdhDe3)AtAttControllerChannelGet((AtAttController)self);
    if (AtPdhDe3IsE3(de3))
        {
        uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de3);
        switch (frameType)
            {
            case cAtPdhE3Frmg832:
                return 4296; /* (59*9 + 6) * 8 */
            case cAtPdhE3FrmG751:
                return 1536; /* 384 * 4 */
            default:
                return 3400;
            }
        }
    return 4760; /* 56 (Overhead) + 4704 (Payload)*/
    }

static uint32 ChannelSpeed(ThaAttController self)
    {
    AtPdhDe3 de3 = (AtPdhDe3)AtAttControllerChannelGet((AtAttController)self);
    if (AtPdhDe3IsE3(de3))
        {
        return 34368;
        }
    return 44736;
    }

static eAtRet PointerAdjForcingHwDataMaskSet(AtAttController self, eAtAttPdhDe3ErrorType errorType, uint32 dataMask)
    {
    eAtRet ret = cAtOk;
    uint32 regAddr = mMethodsGet(mThis(self))->RegPointerAdj(mThis(self));
    ret= PointerHwDataMaskSet(self, cThaAttForcePointerAdj, errorType, dataMask, regAddr);
    return ret;
    }

static eAtRet HwForcePointerAdj(AtAttController self, eBool en, uint32 pointerType)
    {
    eAtRet ret = cAtOk;
    uint32 longRegVal[cAtNumDw];
    uint32 regFieldMask = 0, regFieldShift = 0;
    uint8 pointerAdjTypeIndex = (uint8) pointerType;
    uint32 regAddr = mMethodsGet(mThis(self))->RegPointerAdj(mThis(self));
    eAtAttForcePointerAdjMode mode = mThis(self)->pointerAdjMode[pointerAdjTypeIndex];
    uint8 dataMask = en ? 1 : 0;
    uint32 hwForceMode = 0, numDw = 0;
    uint32 numEvent     = mThis(self)->pointerAdjNumEvent[pointerAdjTypeIndex];
    uint32 step         = mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjStep;
    uint16 PositionMask = mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjPositionMask;

    if (en && mode != cAtAttForcePointerAdjModeContinuous)
        AtAttControllerPointerAdjForcingHwDataMaskSet(self, pointerType, 0);

    if (en && mode == cAtAttForcePointerAdjModeContinuous)
        hwForceMode = 1;

    numDw = mMethodsGet(self)->PointerLongRead(self, regAddr, longRegVal, cAtNumDw);
    if (numDw > 1)
        {
        uint32 regMaskWidth = 0, bitPositionHwVal = 0;
        uint32 type = mMethodsGet(mThis(self))->SwPointerAdjToHw(mThis(self), pointerType);

        regFieldMask = mMethodsGet(mThis(self))->PointerAdjTypeMask(mThis(self), cThaAttForcePointerAdj, pointerType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, type);

        regFieldMask = mMethodsGet(mThis(self))->SetMask(mThis(self), cThaAttForcePointerAdj, pointerType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, 0);
        regFieldMask = mMethodsGet(mThis(self))->ClearMask(mThis(self), cThaAttForcePointerAdj, pointerType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, 0);
        regFieldMask = mMethodsGet(mThis(self))->TimerModeMask(mThis(self), cThaAttForcePointerAdj, pointerType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, 0);

        regFieldMask = mMethodsGet(mThis(self))->HwStepMaskHo(mThis(self), cThaAttForcePointerAdj, pointerType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(longRegVal[1], regField, step);

        regFieldMask = mMethodsGet(mThis(self))->PositionMaskConfigurationMaskHo(mThis(self), cThaAttForcePointerAdj, pointerType);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        regMaskWidth = RegMaskWidth( mMethodsGet(mThis(self))->PositionMaskConfigurationMaskLo(mThis(self), cThaAttForcePointerAdj, pointerType));
        bitPositionHwVal = (uint32) PositionMask >> regMaskWidth;
        mRegFieldSet(longRegVal[1], regField, bitPositionHwVal);
        }

    regFieldMask = mMethodsGet(mThis(self))->PositionMaskConfigurationMaskLo(mThis(self), cThaAttForcePointerAdj, pointerType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, PositionMask);

    regFieldMask = mMethodsGet(mThis(self))->DataMaskConfigurationMask(mThis(self), cThaAttForcePointerAdj, pointerType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, dataMask);

    regFieldMask = mMethodsGet(mThis(self))->NumberOfErrorsMask(mThis(self), cThaAttForcePointerAdj, pointerType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, numEvent);

    regFieldMask = mMethodsGet(mThis(self))->HwForceModeConfigurationMask(mThis(self), cThaAttForcePointerAdj, pointerType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(longRegVal[0], regField, hwForceMode);

    mMethodsGet(self)->PointerLongWrite(self, regAddr, longRegVal, cAtNumDw);

    regAddr = mMethodsGet(mThis(self))->RegPointerAdj2nd(mThis(self));
    if (regAddr!= cInvalidUint32)
        {
        mMethodsGet(self)->PointerLongWrite2nd(self, regAddr, longRegVal, cAtNumDw);
        }
    return ret;
    }

static uint32 RegPointerAdj(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }
static uint32 RegPointerAdj2nd(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 PointerAdjTypeMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(isError);
    AtUnused(errorType);
    return cInvalidUint32/*cAf6_hocepadj_rditype_Mask*/;
    }

static uint8 SwPointerAdjToHw(Tha6A210031PdhDe3AttController self, uint32 pointerAdjType)
    {
    AtUnused(self);
    AtUnused(pointerAdjType);
    return cInvalidUint8;
    }

static eAtRet ForcePointerAdjModeSet(AtAttController self, uint32 pointerType, eAtAttForcePointerAdjMode mode)
    {
    eAtRet ret = cAtOk;
    uint8 pointerTypeIndex = (uint8) pointerType;
    if (!IsPointerAdj(mThis(self)))
        return cAtErrorNotImplemented;

    AtAttControllerPointerAdjForcingHwDataMaskSet(self, pointerType, 0);
    mThis(self)->pointerAdjMode[pointerTypeIndex] = mode;
    mThis(self)->pointerAdjInfo[pointerTypeIndex].pointerAdjDataMask = 1;
    mThis(self)->pointerAdjInfo[pointerTypeIndex].pointerAdjG783State = cAtAttForcePointerStateWaitConfig;
    return ret;
    }

static eAtAttForcePointerAdjMode ForcePointerAdjModeGet(AtAttController self, uint32 pointerType)
    {
    eAtRet ret = cAtOk;
    uint8 pointerTypeIndex = (uint8) pointerType;
    if (!IsPointerAdj(mThis(self)))
        return cAtAttForcePointerAdjModeOneshot;
    return mThis(self)->pointerAdjMode[pointerTypeIndex];
    return ret;
    }

static eAtRet ForcePointerAdjNumEventSet(AtAttController self, uint32 pointerType, uint32 numEvent)
    {
    uint8 pointerAdjTypeIndex = (uint8) pointerType;
    if (!IsPointerAdj(mThis(self)))
        return cAtErrorNotImplemented;
    AtAttControllerPointerAdjForcingHwDataMaskSet(self, pointerType, 0);
    mThis(self)->pointerAdjNumEvent[pointerAdjTypeIndex] = numEvent;
    return cAtOk;
    }

static eAtRet ForcePointerAdjDurationSet(AtAttController self, uint32 pointerType, uint32 duration)
    {
    uint8 pointerAdjTypeIndex = (uint8) pointerType;

    if (!IsPointerAdj(mThis(self)))
        return cAtErrorNotImplemented;
    AtAttControllerPointerAdjForcingHwDataMaskSet(self, pointerType, 0);
    mThis(self)->pointerAdjDuration[pointerAdjTypeIndex] = duration;
    return cAtOk;
    }

static uint32 ForcePointerAdjDurationGet(AtAttController self, uint32 pointerType)
    {
    uint8 pointerAdjTypeIndex = (uint8) pointerType;

    if (!IsPointerAdj(mThis(self)))
        return 0;
    return mThis(self)->pointerAdjDuration[pointerAdjTypeIndex];
    }

static eAtRet ForcePointerAdjDataMaskSet(AtAttController self, uint32 pointerType, uint32 dataMask)
    {
    uint32 pointerAdjTypeIndex = (uint8) pointerType;

    if (!IsPointerAdj(mThis(self)))
        return cAtErrorModeNotSupport;
    if (mMethodsGet(mThis(self))->DataMaskIsValid(mThis(self), pointerType, dataMask) == cAtFalse)
        return cAtErrorOutOfRangParm;

    mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjDataMask = (uint8)dataMask;
    HwForcePointerAdj(self, cAtTrue, pointerType);
    return cAtOk;
    }

static eAtRet ForcePointerAdjBitPositionSet(AtAttController self, uint32 pointerType, uint32 bitPosition)
    {
    uint32 pointerAdjTypeIndex = (uint8) pointerType;

    if (!IsPointerAdj(mThis(self)))
        return cAtErrorModeNotSupport;
    mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjPositionMask = (uint8)bitPosition;
    return cAtOk;
    }

static eAtRet ForcePointerAdjStepSet(AtAttController self, uint32 pointerType, uint32 step)
    {
    uint32 pointerAdjTypeIndex = (uint8) pointerType;

    if (!IsPointerAdj(mThis(self)))
        return cAtErrorModeNotSupport;
    mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjStep = (uint8)step;
    return cAtOk;
    }

static eBool CanForce(AtChannel channel, eAtAttForcePointerAdjMode mode)
    {
    AtUnused(channel);
    AtUnused(mode);
    return cAtTrue;
    }
static eAtRet ForcePointerAdjOppositeOneshot(AtAttController self, uint8 pointerAdjTypeIndex)
    {
    if (pointerAdjTypeIndex==cAtSdhPathPointerAdjTypeIncrease)
        {
        mThis(self)->pointerAdjInfo[cAtSdhPathPointerAdjTypeDecrease].pointerAdjG783State = cAtAttForcePointerStateForceOneshot;
        mThis(self)->pointerAdjMode[cAtSdhPathPointerAdjTypeDecrease] = cAtAttForcePointerAdjModeOneshot;
        }
    else
        {
        mThis(self)->pointerAdjInfo[cAtSdhPathPointerAdjTypeIncrease].pointerAdjG783State = cAtAttForcePointerStateForceOneshot;
        mThis(self)->pointerAdjMode[cAtSdhPathPointerAdjTypeIncrease] = cAtAttForcePointerAdjModeOneshot;
        }

    mThis(self)->pointerAdjInfo[cAtSdhPathPointerAdjTypeIncrease].pointerAdjStep         = 1;
    mThis(self)->pointerAdjInfo[cAtSdhPathPointerAdjTypeIncrease].pointerAdjPositionMask = 0xFFFF;
    mThis(self)->pointerAdjInfo[cAtSdhPathPointerAdjTypeDecrease].pointerAdjStep         = 1;
    mThis(self)->pointerAdjInfo[cAtSdhPathPointerAdjTypeDecrease].pointerAdjPositionMask = 0xFFFF;
    return cAtOk;
    }
static eAtRet ForcePointerAdj(AtAttController self, uint32 pointerType)
    {
    eAtRet ret = cAtOk;
    uint8 pointerAdjTypeIndex = (uint8) pointerType;
    eAtAttForcePointerAdjMode mode = mThis(self)->pointerAdjMode[pointerAdjTypeIndex];
    uint32 duration = mThis(self)->pointerAdjDuration[pointerAdjTypeIndex];
    uint32 numEvent = mThis(self)->pointerAdjNumEvent[pointerAdjTypeIndex];

    if (!IsPointerAdj(mThis(self)))
        return cAtErrorNotImplemented;

    if (mode == cAtAttForcePointerAdjModeContinuous)
        {
        if (mThis(self)->pointerAdjNumEvent[pointerAdjTypeIndex] == 0)
            AtAttControllerForcePointerAdjNumEventSet(self, pointerType, 1);
        if(!(mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjStep))
        	mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjStep = mMethodsGet(mThis(self))->MinStep(mThis(self), pointerType);
        mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjPositionMask = 0x1111;
        mThis(self)->pointerAdjNumEvent[pointerAdjTypeIndex] = 0xFFFF;
        ret = HwForcePointerAdj(self, cAtTrue, pointerType);
        }

    else if (mode == cAtAttForcePointerAdjModeNeventInT)
        {
        if (duration != 0 && numEvent != 0)
            {
            uint16 err_step = 0;
            uint16 PositionMask = 0;

            ret = CalculateStepAndPositionMask(self, cThaAttForcePointerAdj, pointerType, cAtFalse, duration, mThis(self)->pointerAdjNumEvent[pointerAdjTypeIndex], &err_step, &PositionMask);
            if (ret != cAtOk)
                return ret;

            mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjPositionMask = PositionMask;
            mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjStep = err_step;
            ret = HwForcePointerAdj(self, cAtTrue, pointerType);
            }
        else if (duration == 0 && numEvent != 0)
            {
            mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjStep =
            mMethodsGet(mThis(self))->MinStep(mThis(self), pointerType);
            mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjPositionMask = 0xFFFF;
            ret = HwForcePointerAdj(self, cAtTrue, pointerType);
            }
        else
            return cAtErrorInvlParm;
        }

    else if (mode == cAtAttForcePointerAdjModeConsecutive)
        {
        mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjStep = 1;
        mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjPositionMask = 0xFFFF;
        ret = HwForcePointerAdj(self, cAtTrue, pointerType);
        }

    else if (mode == cAtAttForcePointerAdjModeOneshot)
        {
    	if(!(mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjStep))
    		mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjStep = mMethodsGet(mThis(self))->MinStep(mThis(self), pointerType);
        mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjPositionMask = 0x1111;
        ret = HwForcePointerAdj(self, cAtTrue, pointerType);
        }
    else if (mode == cAtAttForcePointerAdjModeG783a     ||
             mode == cAtAttForcePointerAdjModeG783b     ||
             mode == cAtAttForcePointerAdjModeG783c     ||
             mode == cAtAttForcePointerAdjModeG783d     ||
             mode == cAtAttForcePointerAdjModeG783e     ||
             mode == cAtAttForcePointerAdjModeG783f     ||
             mode == cAtAttForcePointerAdjModeG783gPart1||
             mode == cAtAttForcePointerAdjModeG783gPart2||
             mode == cAtAttForcePointerAdjModeG783gPart3||
             mode == cAtAttForcePointerAdjModeG783gPart4||
             mode == cAtAttForcePointerAdjModeG783hParta||
             mode == cAtAttForcePointerAdjModeG783hPartb||
             mode == cAtAttForcePointerAdjModeG783hPartc||
             mode == cAtAttForcePointerAdjModeG783hPartd||
             mode == cAtAttForcePointerAdjModeG783i     ||
             mode == cAtAttForcePointerAdjModeG783jParta||
             mode == cAtAttForcePointerAdjModeG783jPartb||
             mode == cAtAttForcePointerAdjModeG783jPartc||
             mode == cAtAttForcePointerAdjModeG783jPartd)
        {
        if (mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjG783State == cAtAttForcePointerStateWaitConfig)
            {
            AtChannel channel = AtAttControllerChannelGet(self);
            AtModule module = AtChannelModuleGet(channel);

            if (!CanForce(channel, mode))
                return cAtErrorNotControllable;

            if (mode == cAtAttForcePointerAdjModeG783b)
                mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjStep         = 16;
            else if (mode == cAtAttForcePointerAdjModeG783f)
                mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjStep         = 4;
            else if (mode == cAtAttForcePointerAdjModeG783a||mode == cAtAttForcePointerAdjModeG783d)
                ForcePointerAdjOppositeOneshot(self, pointerAdjTypeIndex);
            else
                mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjStep         = 1;
            mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjPositionMask = 0xFFFF;
            if (AtModuleTypeGet(module)== cAtModuleSdh)
                {
                ThaAttSdhManager mngr = (ThaAttSdhManager)AtModuleSdhAttManager((AtModuleSdh) module);
                AtAttControllerPointerAdjForcingHwDataMaskSet(self, pointerType, 0);
                ThaAttSdhManagerForcePointerChannelAdd(mngr, self, mode, pointerType);
                mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjG783State = cAtAttForcePointerStateForceOneshot;
                }
            }
        }

    return ret;
    }

static eAtRet G783ForcePointerAdj(AtAttController self, uint32 pointerType)
    {
    uint8 pointerAdjTypeIndex = (uint8) pointerType;
    if (mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjG783State == cAtAttForcePointerStateForceOneshot)
        {
        return HwForcePointerAdj(self, cAtTrue, pointerType);
        }
    return cAtOk;
    }

static eAtRet HwUnForcePointerAdj(AtAttController self, uint32 pointerType)
    {
    uint8 pointerAdjTypeIndex = (uint8) pointerType;
    mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjG783State = cAtAttForcePointerStateWaitConfig;
    return HwForcePointerAdj(self, cAtFalse, pointerType);
    }

static eAtRet UnForcePointerAdj(AtAttController self, uint32 pointerType)
    {
    uint8 pointerAdjTypeIndex = (uint8) pointerType;
    eAtAttForcePointerAdjMode mode = mThis(self)->pointerAdjMode[pointerAdjTypeIndex];
    if (!IsPointerAdj(mThis(self)))
        return cAtErrorNotImplemented;

    if (mode == cAtAttForcePointerAdjModeContinuous ||
        mode == cAtAttForcePointerAdjModeNeventInT  ||
        mode == cAtAttForcePointerAdjModeOneshot ||
        mode == cAtAttForcePointerAdjModeConsecutive)
        return HwForcePointerAdj(self, cAtFalse, pointerType);
    else
        {
        if ((mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjG783State) == cAtAttForcePointerStateForceOneshot)
            {
            AtChannel channel = AtAttControllerChannelGet(self);
            AtModule module = AtChannelModuleGet(channel);
            if (AtModuleTypeGet(module)== cAtModuleSdh)
                {
                ThaAttSdhManager mngr = (ThaAttSdhManager)AtModuleSdhAttManager((AtModuleSdh) module);
                mThis(self)->pointerAdjInfo[pointerAdjTypeIndex].pointerAdjG783State = cAtAttForcePointerStateStopForce;
                ThaAttSdhManagerForcePointerChannelRemove(mngr, self, mode, pointerType, cAtTrue);
                }
            }
        else
            HwUnForcePointerAdj(self, pointerType);
        return cAtOk;
        }
    }

static void OverrideAtObject(AtObject self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(self, &m_AtObjectOverride);
    }

static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, mMethodsGet(self), sizeof(m_AtAttControllerOverride));

        mMethodOverride(m_AtAttControllerOverride, ForceErrorDataMaskSet);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorCrcMaskSet);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorCrcMask2Set);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorStepSet);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorStep2Set);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorStepGet);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorBitPositionSet);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorBitPosition2Set);

        mMethodOverride(m_AtAttControllerOverride, ForceErrorModeSet);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorRate);
        mMethodOverride(m_AtAttControllerOverride, DebugError);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorDuration);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorNumErrorsSet);
        mMethodOverride(m_AtAttControllerOverride, ForceError);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorContinuous);
        mMethodOverride(m_AtAttControllerOverride, UnForceError);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorGet);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorStatusGet);
        mMethodOverride(m_AtAttControllerOverride, ErrorForceIsSupported);

        mMethodOverride(m_AtAttControllerOverride, AlarmForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, DebugAlarm);
        mMethodOverride(m_AtAttControllerOverride, ForceAlarm);
        mMethodOverride(m_AtAttControllerOverride, ForceAlarmModeSet);
        mMethodOverride(m_AtAttControllerOverride, ForceAlarmNumEventSet);
        mMethodOverride(m_AtAttControllerOverride, ForceAlarmNumFrameSet);
        mMethodOverride(m_AtAttControllerOverride, ForceAlarmDurationSet);
        mMethodOverride(m_AtAttControllerOverride, ForceAlarmStepSet);
        mMethodOverride(m_AtAttControllerOverride, ForceAlarmContinuous);
        mMethodOverride(m_AtAttControllerOverride, ForceAlarmPerSecond);
        mMethodOverride(m_AtAttControllerOverride, ForceAlarmBitPositionSet);
        mMethodOverride(m_AtAttControllerOverride, UnForceAlarm);
        mMethodOverride(m_AtAttControllerOverride, ForceAlarmGet);
        mMethodOverride(m_AtAttControllerOverride, ForceAlarmStatusGet);
        mMethodOverride(m_AtAttControllerOverride, RxAlarmCaptureDurationSet);
        mMethodOverride(m_AtAttControllerOverride, RxAlarmCaptureDurationGet);
        mMethodOverride(m_AtAttControllerOverride, RxAlarmCaptureModeSet);
        mMethodOverride(m_AtAttControllerOverride, RxAlarmCaptureModeGet);
        mMethodOverride(m_AtAttControllerOverride, RxAlarmCaptureNumEventSet);
        mMethodOverride(m_AtAttControllerOverride, RxAlarmCaptureNumEventGet);
        mMethodOverride(m_AtAttControllerOverride, RecordTime);
        mMethodOverride(m_AtAttControllerOverride, RecordTimeAdd);
        mMethodOverride(m_AtAttControllerOverride, RecordTimeClear);
        mMethodOverride(m_AtAttControllerOverride, RecordTimeNumSet);
        mMethodOverride(m_AtAttControllerOverride, RecordTimeNumClear);
        mMethodOverride(m_AtAttControllerOverride, RecordTimeDiffGet);
        mMethodOverride(m_AtAttControllerOverride, RecordTimeIsOkToClear);
        mMethodOverride(m_AtAttControllerOverride, RecordTimeIsOk);

        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjModeSet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjModeGet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjNumEventSet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjDurationSet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjDurationGet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdj);
        mMethodOverride(m_AtAttControllerOverride, G783ForcePointerAdj);
        mMethodOverride(m_AtAttControllerOverride, UnForcePointerAdj);
        mMethodOverride(m_AtAttControllerOverride, HwUnForcePointerAdj);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjDataMaskSet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjBitPositionSet);
        mMethodOverride(m_AtAttControllerOverride, ForcePointerAdjStepSet);
        mMethodOverride(m_AtAttControllerOverride, PointerAdjForcingHwDataMaskSet);

        mMethodOverride(m_AtAttControllerOverride, SetUp);
        mMethodOverride(m_AtAttControllerOverride, LongNumDw);
        mMethodOverride(m_AtAttControllerOverride, LongRead);
        mMethodOverride(m_AtAttControllerOverride, LongWrite);
        mMethodOverride(m_AtAttControllerOverride, PointerLongRead);
        mMethodOverride(m_AtAttControllerOverride, PointerLongWrite);
        mMethodOverride(m_AtAttControllerOverride, PointerLongWrite2nd);
        mMethodOverride(m_AtAttControllerOverride, Read);
        mMethodOverride(m_AtAttControllerOverride, Write);
        mMethodOverride(m_AtAttControllerOverride, De1CrcGapSet);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static void OverrideThaAttController(ThaAttController self)
    {

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, mMethodsGet(self), sizeof(m_ThaAttControllerOverride));

        mMethodOverride(m_ThaAttControllerOverride, CdrControllerGet);
        mMethodOverride(m_ThaAttControllerOverride, FrequenceOffsetBase);
        mMethodOverride(m_ThaAttControllerOverride, FrequenceOffsetStep);
        mMethodOverride(m_ThaAttControllerOverride, ChannelSpeed);
        mMethodOverride(m_ThaAttControllerOverride, NbitPerFrame);
        mMethodOverride(m_ThaAttControllerOverride, InitDb);
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportDataMask8bit);
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportDataMask2bit);
        mMethodOverride(m_ThaAttControllerOverride, IsDataMask8bit);
        mMethodOverride(m_ThaAttControllerOverride, IsDataMask2bit);
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportStep16bit);
        mMethodOverride(m_ThaAttControllerOverride, UseAdjust);
        mMethodOverride(m_ThaAttControllerOverride, HwForceErrorRate);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void Override(AtAttController self)
    {
    OverrideAtObject((AtObject)self);
    OverrideAtAttController(self);
    OverrideThaAttController((ThaAttController)self);
    }

static void MethodsInit(AtAttController self)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController) self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, DataMaskIsValid);
        mMethodOverride(m_methods, RealAddress);
        mMethodOverride(m_methods, PointerRealAddress);
        mMethodOverride(m_methods, PointerRealAddress2nd);
        mMethodOverride(m_methods, MinStep);
        mMethodOverride(m_methods, MaxErrorNum_1s);
        mMethodOverride(m_methods, MaxErrorNum);
        mMethodOverride(m_methods, SetMask);
        mMethodOverride(m_methods, ClearMask);
        mMethodOverride(m_methods, TimerModeMask);
        mMethodOverride(m_methods, RdiTypeMask);
        mMethodOverride(m_methods, HwStepMaskHo);
        mMethodOverride(m_methods, HwStepMaskLo);
        mMethodOverride(m_methods, NumberOfErrorsMask);
        mMethodOverride(m_methods, PositionMaskConfigurationMaskHo);
        mMethodOverride(m_methods, PositionMaskConfigurationMaskLo);
        mMethodOverride(m_methods, DataMaskConfigurationMask);
        mMethodOverride(m_methods, DurationConfigurationMask);
        mMethodOverride(m_methods, HwForceModeConfigurationMask);
        mMethodOverride(m_methods, DefaultDs1DataMask);

        mMethodOverride(m_methods, IsErrorByte);
        mMethodOverride(m_methods, RegAddressFromErrorType);
        mMethodOverride(m_methods, RegAddressFromErrorType2nd);
        mMethodOverride(m_methods, RegAddressFromErrorType3rd);
        mMethodOverride(m_methods, StatusRegAddressFromErrorType);
        mMethodOverride(m_methods, ErrorTypeIndex);
        mMethodOverride(m_methods, ErrorIsFromAlarm);
        mMethodOverride(m_methods, NumErrorTypes);
        mMethodOverride(m_methods, InitErrorDefault);
        mMethodOverride(m_methods, IsErrorForceV2);
        mMethodOverride(m_methods, ForceErrorHelper);
        mMethodOverride(m_methods, ForceErrorHelper2nd);
        mMethodOverride(m_methods, ForceErrorHelper3rd);
        mMethodOverride(m_methods, ErrorForcing2ndDataMaskSet);
        mMethodOverride(m_methods, ErrorForcing3rdDataMaskSet);

        mMethodOverride(m_methods, AlarmIsFromError);
        mMethodOverride(m_methods, AlarmIsSpecialRdi);
        mMethodOverride(m_methods, HwRdiType);
        mMethodOverride(m_methods, RegAddressAlarmUnit);
        mMethodOverride(m_methods, AlarmUnitToHwValue);
        mMethodOverride(m_methods, RegAddressFromAlarmType);
        mMethodOverride(m_methods, StatusRegAddressFromAlarmType);
        mMethodOverride(m_methods, AlarmTypeIndex);
        mMethodOverride(m_methods, AlarmIndexToString);
        mMethodOverride(m_methods, InitAlarmDefault);
        mMethodOverride(m_methods, NumAlarmTypes);
        mMethodOverride(m_methods, SpecificAlarmConfig);
        mMethodOverride(m_methods, CheckAlarmNumEvent);
        mMethodOverride(m_methods, CheckAlarmNumFrame);
        mMethodOverride(m_methods, IsAlarmForceV2);
        mMethodOverride(m_methods, StartVersionSupportAlarmForceV2);
        mMethodOverride(m_methods, StartVersionSupportE3SameAsDs3);
        mMethodOverride(m_methods, IsE3SameAsDs3);
        mMethodOverride(m_methods, StartVersionSupportAlarmUnitConfig);
        mMethodOverride(m_methods, IsNeedAlarmUnitConfig);
        mMethodOverride(m_methods, AlarmUnitSet);

        mMethodOverride(m_methods, InitPointerAdjDefault);
        mMethodOverride(m_methods, StartVersionSupportPointerAdj);
        mMethodOverride(m_methods, StartVersionSupportPointerAdjV2);
        mMethodOverride(m_methods, StartVersionSupportConvertStep);
        mMethodOverride(m_methods, NumPointerAdjTypes);
        mMethodOverride(m_methods, RegPointerAdj);
        mMethodOverride(m_methods, RegPointerAdj2nd);
        mMethodOverride(m_methods, PointerAdjTypeMask);
        mMethodOverride(m_methods, SwPointerAdjToHw);
        mMethodOverride(m_methods, HwForceErrorMode);
        mMethodOverride(m_methods, ErrorForcingDataMaskSet);
        mMethodOverride(m_methods, IsAisUneqOptimizeSupported);
        }

    mMethodsSet(controller, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031PdhDe3AttController);
    }

AtAttController Tha6A210031PdhDe3AttControllerObjectInit(AtAttController self,
                                                         AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaAttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A210031PdhDe3AttControllerNew(AtChannel de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A210031PdhDe3AttControllerObjectInit(controller, de3);
    }

eBool Tha6A210031PdhDe3AttControllerIsAisUneqOptimizeSupported(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    if (self)
        return mMethodsGet(self)->IsAisUneqOptimizeSupported(self, alarmType);
    return cAtFalse;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha6A210031AttReg.h
 * 
 * Created Date: Dec 8, 2015
 *
 * Description : ATT register description header file
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031ATTREG_H_
#define _THA6A210031ATTREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : Force F Bit Error Configuration
Reg Addr   : 0x00080800-0x00080817
Reg Formula: 0x00080800 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force F Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_fbitcfg_Base                                                                      0x00080800
#define cReg_upen_fbitcfg(ds3id)                                                          (0x00080800UL+(ds3id))
#define cReg_upen_fbitcfg_WidthVal                                                                          64
#define cReg_upen_fbitcfg_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: cfgfbiterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force F Bit Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_fbitcfg_cfgfbiterr_step_Mask                                                           cBit11_4
#define c_upen_fbitcfg_cfgfbiterr_step_Shift                                                                 4

/*--------------------------------------
BitField Name: cfgfbitmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  F Bit Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_fbitcfg_cfgfbitmsk_pos_Mask_Lo                                                        cBit31_20
#define c_upen_fbitcfg_cfgfbitmsk_pos_Shift_Lo                                                              20
#define c_upen_fbitcfg_cfgfbitmsk_pos_Mask_Ho                                                          cBit3_0
#define c_upen_fbitcfg_cfgfbitmsk_pos_Shift_Ho                                                               0


/*--------------------------------------
BitField Name: cfgfbitmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For  F Bit Congiguration
BitField Bits: [19]
--------------------------------------*/
#define c_upen_fbitcfg_cfgfbitmsk_dat_Mask                                                              cBit19
#define c_upen_fbitcfg_cfgfbitmsk_dat_Shift                                                                 19

/*--------------------------------------
BitField Name: cfgfbiterr_num
BitField Type: RW
BitField Desc: The Total F Bit Error Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_fbitcfg_cfgfbiterr_num_Mask                                                            cBit18_3
#define c_upen_fbitcfg_cfgfbiterr_num_Shift                                                                  3

/*--------------------------------------
BitField Name: cfg_frcfbitmod
BitField Type: RW
BitField Desc: Force F Bit Mode 000:N error� (N = 16bits),N in (Tms)� (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_fbitcfg_cfg_frcfbitmod_Mask                                                             cBit2_0
#define c_upen_fbitcfg_cfg_frcfbitmod_Shift                                                                  0

/*
//Field: [31:16] %% cfgfbit_msk_pos  %% Mask Position for 16 Event  %%  RW %% 0x0 %% 0x0
//# Field: [15:8]  %% cfgfbit_sec_num  %% The T second number  %%  RW %% 0x0 %% 0x0
//# Field: [7:4]   %% cfgfbit_evtin1s  %% The even number in a second set in T second  %%  RW %% 0x0 %% 0x0
//# Field: [3]     %% cfgfbit_frc_ena  %% Force Alarm LOF Enable  %%  RW %% 0x0 %% 0x0
//# Field: [2:0]   %% cfgfbit_frc_mod  %% Force Alarm LOF mode %%  RW  %% 0x0 %% 0x0
 */
#define c_upen_lof_cfg_cfg_msk_pos_Mask                                                              cBit31_16
#define c_upen_lof_cfg_cfg_msk_pos_Shift                                                                    16
#define c_upen_lof_cfg_cfg_sec_num_Mask                                                               cBit15_8
#define c_upen_lof_cfg_cfg_sec_num_Shift                                                                     8
#define c_upen_lof_cfg_cfg_evtin1s_Mask                                                                cBit7_4
#define c_upen_lof_cfg_cfg_evtin1s_Shift                                                                     4
#define c_upen_lof_cfg_cfg_frc_ena_Mask                                                                  cBit3
#define c_upen_lof_cfg_cfg_frc_ena_Shift                                                                     3
#define c_upen_lof_cfg_cfg_frc_mod_Mask                                                                cBit2_0
#define c_upen_lof_cfg_cfg_frc_mod_Shift                                                                     0



/*------------------------------------------------------------------------------
Reg Name   : Force F Bit Error Status
Reg Addr   : 0x00080820-0x00080837
Reg Formula: 0x00080820 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to see the Force F Bit Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_fbitsta_Base                                                                      0x00080820
#define cReg_upen_fbitsta(ds3id)                                                          (0x00080820UL+(ds3id))
#define cReg_upen_fbitsta_WidthVal                                                                          32
#define cReg_upen_fbitsta_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: stafbiterr_stepcnt
BitField Type: RW
BitField Desc: The Event F Bit Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_fbitsta_stafbiterr_stepcnt_Mask                                                    cBit29_22
#define c_upen_fbitsta_stafbiterr_stepcnt_Shift                                                          22

/*--------------------------------------
BitField Name: stafbitmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask F Bit Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_fbitsta_stafbitmsk_poscnt_Mask                                                     cBit21_18
#define c_upen_fbitsta_stafbitmsk_poscnt_Shift                                                           18

/*--------------------------------------
BitField Name: stafbiterr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error F Bit Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_fbitsta_stafbiterr_cnt_Mask                                                            cBit17_2
#define c_upen_fbitsta_stafbiterr_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: stafbitfrc_err
BitField Type: RW
BitField Desc: F Bit Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_fbitsta_stafbitfrc_err_Mask                                                               cBit1
#define c_upen_fbitsta_stafbitfrc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: stafbitosecenb
BitField Type: RW
BitField Desc: Force F bit in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_fbitsta_stafbitosecenb_Mask                                                               cBit0
#define c_upen_fbitsta_stafbitosecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force CP Bit Error Configuration
Reg Addr   : 0x00080880-0x00080897
Reg Formula: 0x00080880 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force CP Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_cpb_cfg_Base                                                                      0x00080880
#define cReg_upen_cpb_cfg(ds3id)                                                     (0x00080880UL+(ds3id))
#define cReg_upen_cpb_cfg_WidthVal                                                                          64
#define cReg_upen_cpb_cfg_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: cfgcpb_err_clear
BitField Type: RW
BitField Desc: The Clear to Force CP Bit Error Congiguration
BitField Bits: [59:58]
--------------------------------------*/
#define c_upen_cpb_cfg_cfgcpb_err_timermode_Mask                                                    cBit27_26
#define c_upen_cpb_cfg_cfgcpb_err_timermode_Shift                                                          26

/*--------------------------------------
BitField Name: cfgcpb_err_clear
BitField Type: RW
BitField Desc: The Clear to Force CP Bit Error Congiguration
BitField Bits: [57:50]
--------------------------------------*/
#define c_upen_cpb_cfg_cfgcpb_err_timerclear_Mask                                                         cBit25_18
#define c_upen_cpb_cfg_cfgcpb_err_timerclear_Shift                                                               18

/*--------------------------------------
BitField Name: cfgcpb_err_set
BitField Type: RW
BitField Desc: The Set to Force CP Bit Error Congiguration
BitField Bits: [49:42]
--------------------------------------*/
#define c_upen_cpb_cfg_cfgcpb_err_timerset_Mask                                                           cBit17_10
#define c_upen_cpb_cfg_cfgcpb_err_timerset_Shift                                                                 10

/*--------------------------------------
BitField Name: cfgcpb_err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force CP Bit Error Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_cpb_cfg_cfgcpb_err_step_Mask                                                            cBit9_2
#define c_upen_cpb_cfg_cfgcpb_err_step_Shift                                                                 2

/*--------------------------------------
BitField Name: cfgcpb_msk_pos
BitField Type: RW
BitField Desc: The Position Mask for CP Bit Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_cpb_cfg_cfgcpb_msk_pos_Mask_Lo                                                     cBit31_18
#define c_upen_cpb_cfg_cfgcpb_msk_pos_Shift_Lo                                                           18
#define c_upen_cpb_cfg_cfgcpb_msk_pos_Mask_Ho                                                       cBit1_0
#define c_upen_cpb_cfg_cfgcpb_msk_pos_Shift_Ho                                                            0

/*--------------------------------------
BitField Name: cfgcpb_msk_dat
BitField Type: RW
BitField Desc: The Data Mask for CP Bit Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_cpb_cfg_cfgcpb_msk_dat_Mask                                                           cBit17
#define c_upen_cpb_cfg_cfgcpb_msk_dat_Shift                                                              17

/*--------------------------------------
BitField Name: cfgcpb_err_num
BitField Type: RW
BitField Desc: The Total CP Bit Error Number Congiguration
BitField Bits: [16:1]
--------------------------------------*/
#define c_upen_cpb_cfg_cfgcpb_err_num_Mask                                                         cBit16_1
#define c_upen_cpb_cfg_cfgcpb_err_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgcpb_frc_1sen
BitField Type: RW
BitField Desc: Force CP Bit in One second enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_cpb_cfg_cfgcpb_frc_1sen_Mask                                                              cBit0
#define c_upen_cpb_cfg_cfgcpb_frc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force CP Bit Error Status
Reg Addr   : 0x000808a0-0x000808b7
Reg Formula: 0x000808a0 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to see the Force CP Bit Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_cpb_sta_Base                                                                      0x000808a0
#define cReg_upen_cpb_sta(ds3id)                                                     (0x000808a0UL+(ds3id))
#define cReg_upen_cpb_sta_WidthVal                                                                          32
#define cReg_upen_cpb_sta_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: stacpb_err_stepcnt
BitField Type: RW
BitField Desc: The Event CP Bit Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_cpb_sta_stacpb_err_stepcnt_Mask                                                    cBit29_22
#define c_upen_cpb_sta_stacpb_err_stepcnt_Shift                                                          22

/*--------------------------------------
BitField Name: stacpb_msk_poscnt
BitField Type: RW
BitField Desc: The Position Mask CP Bit Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_cpb_sta_stacpb_msk_poscnt_Mask                                                     cBit21_18
#define c_upen_cpb_sta_stacpb_msk_poscnt_Shift                                                           18

/*--------------------------------------
BitField Name: stacpb_err_cnt
BitField Type: RW
BitField Desc: The Remain Force CP Bit Error Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_cpb_sta_stacpb_err_cnt_Mask                                                         cBit17_2
#define c_upen_cpb_sta_stacpb_err_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: stacpb_frc_err
BitField Type: RW
BitField Desc: CP Bit Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_cpb_sta_stacpb_frc_err_Mask                                                               cBit1
#define c_upen_cpb_sta_stacpb_frc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: stacpb_osecenb
BitField Type: RW
BitField Desc: Force CP Bit in One Second Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_cpb_sta_stacpb_osecenb_Mask                                                               cBit0
#define c_upen_cpb_sta_stacpb_osecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI Bit Error Configuration
Reg Addr   : 0x00080900-0x00080917
Reg Formula: 0x00080900 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force RDI Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_rdi_cfg_Base                                                                      0x00080900
#define cReg_upen_rdi_cfg(ds3id)                                                     (0x00080900UL+(ds3id))
#define cReg_upen_rdi_cfg_WidthVal                                                                          64
#define cReg_upen_rdi_cfg_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: cfgrdi_err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force RDI Bit Error Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_rdi_cfg_cfgrdi_err_step_Mask                                                         cBit9_2
#define c_upen_rdi_cfg_cfgrdi_err_step_Shift                                                              2

/*--------------------------------------
BitField Name: cfgrdi_msk_pos
BitField Type: RW
BitField Desc: The Position Mask for RDI Bit Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_rdi_cfg_cfgrdi_msk_pos_Mask_01                                                     cBit31_18
#define c_upen_rdi_cfg_cfgrdi_msk_pos_Shift_01                                                           18
#define c_upen_rdi_cfg_cfgrdi_msk_pos_Mask_02                                                       cBit1_0
#define c_upen_rdi_cfg_cfgrdi_msk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfgrdi_msk_dat
BitField Type: RW
BitField Desc: The Data Mask For RDI Bit Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_rdi_cfg_cfgrdi_msk_dat_Mask                                                           cBit17
#define c_upen_rdi_cfg_cfgrdi_msk_dat_Shift                                                              17

/*--------------------------------------
BitField Name: cfgrdi_err_num
BitField Type: RW
BitField Desc: The Total RDI Bit Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_rdi_cfg_cfgrdi_err_num_Mask                                                         cBit16_1
#define c_upen_rdi_cfg_cfgrdi_err_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgrdi_frc_1sen
BitField Type: RW
BitField Desc: Force RDI Bit in One second enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_rdi_cfg_cfgrdi_frc_1sen_Mask                                                              cBit0
#define c_upen_rdi_cfg_cfgrdi_frc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force RDI Bit Error Status
Reg Addr   : 0x00080920-0x00080937
Reg Formula: 0x00080920 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to see the Force RDI Bit Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_rdi_sta_Base                                                                      0x00080920
#define cReg_upen_rdi_sta(ds3id)                                                     (0x00080920UL+(ds3id))
#define cReg_upen_rdi_sta_WidthVal                                                                          32
#define cReg_upen_rdi_sta_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: stardi_err_stepcnt
BitField Type: RW
BitField Desc: The Event RDI Bit Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_rdi_sta_stardi_err_stepcnt_Mask                                                    cBit29_22
#define c_upen_rdi_sta_stardi_err_stepcnt_Shift                                                          22

/*--------------------------------------
BitField Name: stardi_msk_poscnt
BitField Type: RW
BitField Desc: The Position Mask RDI Bit Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_rdi_sta_stardi_msk_poscnt_Mask                                                     cBit21_18
#define c_upen_rdi_sta_stardi_msk_poscnt_Shift                                                           18

/*--------------------------------------
BitField Name: stardi_err_cnt
BitField Type: RW
BitField Desc: The Remain Force RDI Bit Error Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_rdi_sta_stardi_err_cnt_Mask                                                         cBit17_2
#define c_upen_rdi_sta_stardi_err_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: stardi_frc_err
BitField Type: RW
BitField Desc: RDI Bit Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_rdi_sta_stardi_frc_err_Mask                                                               cBit1
#define c_upen_rdi_sta_stardi_frc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: stardi_osecenb
BitField Type: RW
BitField Desc: Force RDI Bit One Second Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_rdi_sta_stardi_osecenb_Mask                                                               cBit0
#define c_upen_rdi_sta_stardi_osecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force P Bit Error Configuration
Reg Addr   : 0x00080a00-0x00080a17
Reg Formula: 0x00080a00 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force P Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_pbit_cfg_Base                                                                     0x00080a00
#define cReg_upen_pbit_cfg(ds3id)                                                    (0x00080a00UL+(ds3id))
#define cReg_upen_pbit_cfg_WidthVal                                                                         64
#define cReg_upen_pbit_cfg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: cfgpbit_err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force P Bit Error Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_pbit_cfg_cfgpbit_err_step_Mask                                                       cBit9_2
#define c_upen_pbit_cfg_cfgpbit_err_step_Shift                                                            2

/*--------------------------------------
BitField Name: cfgpbit_msk_pos
BitField Type: RW
BitField Desc: The Position Mask for P Bit Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_pbit_cfg_cfgpbit_msk_pos_Mask_01                                                   cBit31_18
#define c_upen_pbit_cfg_cfgpbit_msk_pos_Shift_01                                                         18
#define c_upen_pbit_cfg_cfgpbit_msk_pos_Mask_02                                                     cBit1_0
#define c_upen_pbit_cfg_cfgpbit_msk_pos_Shift_02                                                             0

/*--------------------------------------
BitField Name: cfgpbit_msk_dat
BitField Type: RW
BitField Desc: The Data Mask For P Bit Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_pbit_cfg_cfgpbit_msk_dat_Bit_Start                                                        17
#define c_upen_pbit_cfg_cfgpbit_msk_dat_Bit_End                                                          17
#define c_upen_pbit_cfg_cfgpbit_msk_dat_Mask                                                         cBit17
#define c_upen_pbit_cfg_cfgpbit_msk_dat_Shift                                                            17

/*--------------------------------------
BitField Name: cfgpbit_err_num
BitField Type: RW
BitField Desc: The Total P Bit Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_pbit_cfg_cfgpbit_err_num_Mask                                                       cBit16_1
#define c_upen_pbit_cfg_cfgpbit_err_num_Shift                                                                1

/*--------------------------------------
BitField Name: cfgpbit_frc_1sen
BitField Type: RW
BitField Desc: Force P Bit in One second enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_pbit_cfg_cfgpbit_frc_1sen_Mask                                                            cBit0
#define c_upen_pbit_cfg_cfgpbit_frc_1sen_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force P Bit Error Status
Reg Addr   : 0x00080a20-0x00080a37
Reg Formula: 0x00080a20 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to see the Force P Bit Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_pbit_sta_Base                                                                     0x00080a20
#define cReg_upen_pbit_sta(ds3id)                                                    (0x00080a20UL+(ds3id))
#define cReg_upen_pbit_sta_WidthVal                                                                         32
#define cReg_upen_pbit_sta_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: stapbit_err_stepcnt
BitField Type: RW
BitField Desc: The Event P Bit Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_pbit_sta_stapbit_err_stepcnt_Mask                                                  cBit29_22
#define c_upen_pbit_sta_stapbit_err_stepcnt_Shift                                                        22

/*--------------------------------------
BitField Name: stapbit_msk_poscnt
BitField Type: RW
BitField Desc: The Position Mask P Bit Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_pbit_sta_stapbit_msk_poscnt_Mask                                                   cBit21_18
#define c_upen_pbit_sta_stapbit_msk_poscnt_Shift                                                         18

/*--------------------------------------
BitField Name: stapbit_err_cnt
BitField Type: RW
BitField Desc: The Remain Force P Bit Error Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_pbit_sta_stapbit_err_cnt_Mask                                                       cBit17_2
#define c_upen_pbit_sta_stapbit_err_cnt_Shift                                                                2

/*--------------------------------------
BitField Name: stapbit_frc_err
BitField Type: RW
BitField Desc: P Bit Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_pbit_sta_stapbit_frc_err_Mask                                                             cBit1
#define c_upen_pbit_sta_stapbit_frc_err_Shift                                                                1

/*--------------------------------------
BitField Name: stapbit_osecenb
BitField Type: RW
BitField Desc: Force P Bit One Second Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_pbit_sta_stapbit_osecenb_Mask                                                             cBit0
#define c_upen_pbit_sta_stapbit_osecenb_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force FA Byte Error Configuration
Reg Addr   : 0x00080a80-0x00080a97
Reg Formula: 0x00080a80 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force FA Byte Error

------------------------------------------------------------------------------*/
#define cReg_upen_faB_cfg_Base                                                                      0x00080a80
#define cReg_upen_faB_cfg(ds3id)                                                     (0x00080a80UL+(ds3id))
#define cReg_upen_faB_cfg_WidthVal                                                                          64
#define cReg_upen_faB_cfg_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: cfgfaB_err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force FA Byte Error Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_faB_cfg_cfgfaB_err_step_Mask                                                            cBit9_2
#define c_upen_faB_cfg_cfgfaB_err_step_Shift                                                                 2

/*--------------------------------------
BitField Name: cfgfaB_msk_pos
BitField Type: RW
BitField Desc: The Position Mask for FA Byte Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_faB_cfg_cfgfaB_msk_pos_Mask_Lo                                                     cBit31_18
#define c_upen_faB_cfg_cfgfaB_msk_pos_Shift_Lo                                                           18
#define c_upen_faB_cfg_cfgfaB_msk_pos_Mask_Ho                                                       cBit1_0
#define c_upen_faB_cfg_cfgfaB_msk_pos_Shift_Ho                                                            0

/*--------------------------------------
BitField Name: cfgfaB_msk_dat
BitField Type: RW
BitField Desc: The Data Mask For FA Byte Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_faB_cfg_cfgfaB_msk_dat_Mask                                                              cBit17
#define c_upen_faB_cfg_cfgfaB_msk_dat_Shift                                                              17

/*--------------------------------------
BitField Name: cfgfaB_err_num
BitField Type: RW
BitField Desc: The Total FA Byte Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_faB_cfg_cfgfaB_err_num_Mask                                                         cBit16_1
#define c_upen_faB_cfg_cfgfaB_err_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgfaB_frc_1sen
BitField Type: RW
BitField Desc: Force FA Byte in One second enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_faB_cfg_cfgfaB_frc_1sen_Mask                                                              cBit0
#define c_upen_faB_cfg_cfgfaB_frc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force FA Byte Error Status
Reg Addr   : 0x00080aa0-0x00080ab7
Reg Formula: 0x00080aa0 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to see the Force FA Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_faB_sta_Base                                                                      0x00080aa0
#define cReg_upen_faB_sta(ds3id)                                                     (0x00080aa0UL+(ds3id))
#define cReg_upen_faB_sta_WidthVal                                                                          32
#define cReg_upen_faB_sta_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: stafaB_err_stepcnt
BitField Type: RW
BitField Desc: The Event FA Byte Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_faB_sta_stafaB_err_stepcnt_Mask                                                    cBit29_22
#define c_upen_faB_sta_stafaB_err_stepcnt_Shift                                                          22

/*--------------------------------------
BitField Name: stafaB_msk_poscnt
BitField Type: RW
BitField Desc: The Position Mask FA Byte Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_faB_sta_stafaB_msk_poscnt_Mask                                                     cBit21_18
#define c_upen_faB_sta_stafaB_msk_poscnt_Shift                                                           18

/*--------------------------------------
BitField Name: stafaB_err_cnt
BitField Type: RW
BitField Desc: The Remain Force FA Byte Error Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_faB_sta_stafaB_err_cnt_Mask                                                         cBit17_2
#define c_upen_faB_sta_stafaB_err_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: stafaB_frc_err
BitField Type: RW
BitField Desc: FA Byte Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_faB_sta_stafaB_frc_err_Mask                                                               cBit1
#define c_upen_faB_sta_stafaB_frc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: stafaB_osecenb
BitField Type: RW
BitField Desc: Force FA Byte One Second Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_faB_sta_stafaB_osecenb_Mask                                                               cBit0
#define c_upen_faB_sta_stafaB_osecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP8 Byte Error Configuration
Reg Addr   : 0x00080b00-0x00080b17
Reg Formula: 0x00080b00 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force BIP8 Byte Error

------------------------------------------------------------------------------*/
#define cReg_upen_bip8_cfg_Base                                                                     0x00080b00
#define cReg_upen_bip8_cfg(ds3id)                                                    (0x00080b00UL+(ds3id))
#define cReg_upen_bip8_cfg_WidthVal                                                                         64
#define cReg_upen_bip8_cfg_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: cfgbip8_err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force BIP8 Byte Error Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_bip8_cfg_cfgbip8_err_step_Mask                                                          cBit9_2
#define c_upen_bip8_cfg_cfgbip8_err_step_Shift                                                               2

/*--------------------------------------
BitField Name: cfgbip8_msk_pos
BitField Type: RW
BitField Desc: The Position Mask for BIP8 Byte Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_bip8_cfg_cfgbip8_msk_pos_Mask_01                                                      cBit31_18
#define c_upen_bip8_cfg_cfgbip8_msk_pos_Shift_01                                                            18
#define c_upen_bip8_cfg_cfgbip8_msk_pos_Mask_02                                                        cBit1_0
#define c_upen_bip8_cfg_cfgbip8_msk_pos_Shift_02                                                             0

/*--------------------------------------
BitField Name: cfgbip8_msk_dat
BitField Type: RW
BitField Desc: The Data Mask For BIP8 Byte Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_bip8_cfg_cfgbip8_msk_dat_Mask                                                            cBit17
#define c_upen_bip8_cfg_cfgbip8_msk_dat_Shift                                                            17

/*--------------------------------------
BitField Name: cfgbip8_err_num
BitField Type: RW
BitField Desc: The Total BIP8 Byte Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_bip8_cfg_cfgbip8_err_num_Mask                                                       cBit16_1
#define c_upen_bip8_cfg_cfgbip8_err_num_Shift                                                                1

/*--------------------------------------
BitField Name: cfgbip8_frc_1sen
BitField Type: RW
BitField Desc: Force BIP8 Byte in One second enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_bip8_cfg_cfgbip8_frc_1sen_Mask                                                            cBit0
#define c_upen_bip8_cfg_cfgbip8_frc_1sen_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force BIP8 Byte Error Status
Reg Addr   : 0x00080b20-0x00080b37
Reg Formula: 0x00080b20 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to see the Force BIP8 Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_bip8_sta_Base                                                                     0x00080b20
#define cReg_upen_bip8_sta(ds3id)                                                    (0x00080b20UL+(ds3id))
#define cReg_upen_bip8_sta_WidthVal                                                                         32
#define cReg_upen_bip8_sta_WriteMask                                                                       0x0

/*--------------------------------------
BitField Name: stabip8_err_stepcnt
BitField Type: RW
BitField Desc: The Event BIP8 Byte Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_bip8_sta_stabip8_err_stepcnt_Mask                                                  cBit29_22
#define c_upen_bip8_sta_stabip8_err_stepcnt_Shift                                                        22

/*--------------------------------------
BitField Name: stabip8_msk_poscnt
BitField Type: RW
BitField Desc: The Position Mask BIP8 Byte Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_bip8_sta_stabip8_msk_poscnt_Mask                                                   cBit21_18
#define c_upen_bip8_sta_stabip8_msk_poscnt_Shift                                                         18

/*--------------------------------------
BitField Name: stabip8_err_cnt
BitField Type: RW
BitField Desc: The Remain Force BIP8 Byte Error Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_bip8_sta_stabip8_err_cnt_Mask                                                       cBit17_2
#define c_upen_bip8_sta_stabip8_err_cnt_Shift                                                                2

/*--------------------------------------
BitField Name: stabip8_frc_err
BitField Type: RW
BitField Desc: BIP8 Byte Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_bip8_sta_stabip8_frc_err_Mask                                                             cBit1
#define c_upen_bip8_sta_stabip8_frc_err_Shift                                                                1

/*--------------------------------------
BitField Name: stabip8_osecenb
BitField Type: RW
BitField Desc: Force BIP8 Byte One Second Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_bip8_sta_stabip8_osecenb_Mask                                                             cBit0
#define c_upen_bip8_sta_stabip8_osecenb_Shift                                                                0


/*------------------------------------------------------------------------------
Reg Name   : Force REI Bit Error Configuration
Reg Addr   : 0x00080b80-0x00080b97
Reg Formula: 0x00080b80 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force REI Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_rei_cfg_Base                                                                      0x00080b80
#define cReg_upen_rei_cfg(ds3id)                                                     (0x00080b80UL+(ds3id))
#define cReg_upen_rei_cfg_WidthVal                                                                          64
#define cReg_upen_rei_cfg_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: cfgrei_err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force REI Bit Error Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_rei_cfg_cfgrei_err_step_Mask                                                         cBit9_2
#define c_upen_rei_cfg_cfgrei_err_step_Shift                                                              2

/*--------------------------------------
BitField Name: cfgrei_msk_pos
BitField Type: RW
BitField Desc: The Position Mask for REI Bit Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_rei_cfg_cfgrei_msk_pos_Mask_01                                                     cBit31_18
#define c_upen_rei_cfg_cfgrei_msk_pos_Shift_01                                                           18
#define c_upen_rei_cfg_cfgrei_msk_pos_Mask_02                                                       cBit1_0
#define c_upen_rei_cfg_cfgrei_msk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfgrei_msk_dat
BitField Type: RW
BitField Desc: The Data Mask For REI Bit Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_rei_cfg_cfgrei_msk_dat_Mask                                                           cBit17
#define c_upen_rei_cfg_cfgrei_msk_dat_Shift                                                              17

/*--------------------------------------
BitField Name: cfgrei_err_num
BitField Type: RW
BitField Desc: The Total REI Bit Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_rei_cfg_cfgrei_err_num_Mask                                                         cBit16_1
#define c_upen_rei_cfg_cfgrei_err_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgrei_frc_1sen
BitField Type: RW
BitField Desc: Force REI Bit in One second enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_rei_cfg_cfgrei_frc_1sen_Mask                                                              cBit0
#define c_upen_rei_cfg_cfgrei_frc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force REI Bit Error Status
Reg Addr   : 0x00080ba0-0x00080bb7
Reg Formula: 0x00080ba0 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to see the Force REI Bit Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_rei_sta_Base                                                                      0x00080ba0
#define cReg_upen_rei_sta(ds3id)                                                     (0x00080ba0UL+(ds3id))
#define cReg_upen_rei_sta_WidthVal                                                                          32
#define cReg_upen_rei_sta_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: starei_err_stepcnt
BitField Type: RW
BitField Desc: The Event REI Bit Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_rei_sta_starei_err_stepcnt_Mask                                                    cBit29_22
#define c_upen_rei_sta_starei_err_stepcnt_Shift                                                          22

/*--------------------------------------
BitField Name: starei_msk_poscnt
BitField Type: RW
BitField Desc: The Position Mask REI Bit Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_rei_sta_starei_msk_poscnt_Mask                                                     cBit21_18
#define c_upen_rei_sta_starei_msk_poscnt_Shift                                                           18

/*--------------------------------------
BitField Name: starei_err_cnt
BitField Type: RW
BitField Desc: The Remain Force REI Bit Error Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_rei_sta_starei_err_cnt_Mask                                                         cBit17_2
#define c_upen_rei_sta_starei_err_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: starei_frc_err
BitField Type: RW
BitField Desc: REI Bit Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_rei_sta_starei_frc_err_Mask                                                               cBit1
#define c_upen_rei_sta_starei_frc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: starei_osecenb
BitField Type: RW
BitField Desc: Force REI Bit One Second Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_rei_sta_starei_osecenb_Mask                                                               cBit0
#define c_upen_rei_sta_starei_osecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force NR Byte Error Configuration
Reg Addr   : 0x00080c00-0x00080c17
Reg Formula: 0x00080c00 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force NR Byte Error

------------------------------------------------------------------------------*/
#define cReg_upen_nrB_cfg_Base                                                                      0x00080c00
#define cReg_upen_nrB_cfg(ds3id)                                                     (0x00080c00UL+(ds3id))
#define cReg_upen_nrB_cfg_WidthVal                                                                          64
#define cReg_upen_nrB_cfg_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: cfgnrB_err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force NR Byte Error Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_nrB_cfg_cfgnrB_err_step_Mask                                                            cBit9_2
#define c_upen_nrB_cfg_cfgnrB_err_step_Shift                                                                 2

/*--------------------------------------
BitField Name: cfgnrB_msk_pos
BitField Type: RW
BitField Desc: The Position Mask for NR Byte Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_nrB_cfg_cfgnrB_msk_pos_Mask_01                                                        cBit31_18
#define c_upen_nrB_cfg_cfgnrB_msk_pos_Shift_01                                                              18
#define c_upen_nrB_cfg_cfgnrB_msk_pos_Mask_02                                                          cBit1_0
#define c_upen_nrB_cfg_cfgnrB_msk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfgnrB_msk_dat
BitField Type: RW
BitField Desc: The Data Mask For NR Byte Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_nrB_cfg_cfgnrB_msk_dat_Mask                                                              cBit17
#define c_upen_nrB_cfg_cfgnrB_msk_dat_Shift                                                              17

/*--------------------------------------
BitField Name: cfgnrB_err_num
BitField Type: RW
BitField Desc: The Total NR Byte Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_nrB_cfg_cfgnrB_err_num_Mask                                                         cBit16_1
#define c_upen_nrB_cfg_cfgnrB_err_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgnrB_frc_1sen
BitField Type: RW
BitField Desc: Force NR Byte in One second enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_nrB_cfg_cfgnrB_frc_1sen_Mask                                                              cBit0
#define c_upen_nrB_cfg_cfgnrB_frc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force NR Byte Error Status
Reg Addr   : 0x00080c20-0x00080c37
Reg Formula: 0x00080c20 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force NR Byte Error

------------------------------------------------------------------------------*/
#define cReg_upen_nrB_sta_Base                                                                      0x00080c20
#define cReg_upen_nrB_sta(ds3id)                                                     (0x00080c20UL+(ds3id))
#define cReg_upen_nrB_sta_WidthVal                                                                          32
#define cReg_upen_nrB_sta_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: stanrB_err_stepcnt
BitField Type: RW
BitField Desc: The Event NR Byte Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_nrB_sta_stanrB_err_stepcnt_Mask                                                    cBit29_22
#define c_upen_nrB_sta_stanrB_err_stepcnt_Shift                                                          22

/*--------------------------------------
BitField Name: stanrB_msk_poscnt
BitField Type: RW
BitField Desc: The Position Mask NR Byte Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_nrB_sta_stanrB_msk_poscnt_Mask                                                     cBit21_18
#define c_upen_nrB_sta_stanrB_msk_poscnt_Shift                                                           18

/*--------------------------------------
BitField Name: stanrB_err_cnt
BitField Type: RW
BitField Desc: The Remain Force NR Byte Error Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_nrB_sta_stanrB_err_cnt_Mask                                                         cBit17_2
#define c_upen_nrB_sta_stanrB_err_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: stanrB_frc_err
BitField Type: RW
BitField Desc: NR Byte Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_nrB_sta_stanrB_frc_err_Mask                                                               cBit1
#define c_upen_nrB_sta_stanrB_frc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: stanrB_osecenb
BitField Type: RW
BitField Desc: Force NR Byte One Second Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_nrB_sta_stanrB_osecenb_Mask                                                               cBit0
#define c_upen_nrB_sta_stanrB_osecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force GC Byte Error Configuration
Reg Addr   : 0x00080c80-0x00080c97
Reg Formula: 0x00080c80 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force GC Byte Error

------------------------------------------------------------------------------*/
#define cReg_upen_gcB_cfg_Base                                                                      0x00080c80
#define cReg_upen_gcB_cfg(ds3id)                                                     (0x00080c80UL+(ds3id))
#define cReg_upen_gcB_cfg_WidthVal                                                                          64
#define cReg_upen_gcB_cfg_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: cfggcB_err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force GC Byte Error Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_gcB_cfg_cfggcB_err_step_Mask                                                            cBit9_2
#define c_upen_gcB_cfg_cfggcB_err_step_Shift                                                                 2

/*--------------------------------------
BitField Name: cfggcB_msk_pos
BitField Type: RW
BitField Desc: The Position Mask for GC Byte Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_gcB_cfg_cfggcB_msk_pos_Mask_01                                                        cBit31_18
#define c_upen_gcB_cfg_cfggcB_msk_pos_Shift_01                                                              18
#define c_upen_gcB_cfg_cfggcB_msk_pos_Mask_02                                                          cBit1_0
#define c_upen_gcB_cfg_cfggcB_msk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfggcB_msk_dat
BitField Type: RW
BitField Desc: The Data Mask For GC Byte Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_gcB_cfg_cfggcB_msk_dat_Mask                                                              cBit17
#define c_upen_gcB_cfg_cfggcB_msk_dat_Shift                                                              17

/*--------------------------------------
BitField Name: cfggcB_err_num
BitField Type: RW
BitField Desc: The Total GC Byte Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_gcB_cfg_cfggcB_err_num_Mask                                                         cBit16_1
#define c_upen_gcB_cfg_cfggcB_err_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfggcB_frc_1sen
BitField Type: RW
BitField Desc: Force GC Byte in One second enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_gcB_cfg_cfggcB_frc_1sen_Mask                                                              cBit0
#define c_upen_gcB_cfg_cfggcB_frc_1sen_Shift                                                                 0


/*------------------------------------------------------------------------------
Reg Name   : Force GC Byte Error Status
Reg Addr   : 0x00080ca0-0x00080cb7
Reg Formula: 0x00080ca0 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to see Force GC Byte Error Status

------------------------------------------------------------------------------*/
#define cReg_upen_gcB_sta_Base                                                                      0x00080ca0
#define cReg_upen_gcB_sta(ds3id)                                                     (0x00080ca0UL+(ds3id))
#define cReg_upen_gcB_sta_WidthVal                                                                          32
#define cReg_upen_gcB_sta_WriteMask                                                                        0x0

/*--------------------------------------
BitField Name: stagcB_err_stepcnt
BitField Type: RW
BitField Desc: The Event GC Byte Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_gcB_sta_stagcB_err_stepcnt_Mask                                                       cBit29_22
#define c_upen_gcB_sta_stagcB_err_stepcnt_Shift                                                             22

/*--------------------------------------
BitField Name: stagcB_msk_poscnt
BitField Type: RW
BitField Desc: The Position Mask GC Byte Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_gcB_sta_stagcB_msk_poscnt_Mask                                                        cBit21_18
#define c_upen_gcB_sta_stagcB_msk_poscnt_Shift                                                              18

/*--------------------------------------
BitField Name: stagcB_err_cnt
BitField Type: RW
BitField Desc: The Remain Force GC Byte Error Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_gcB_sta_stagcB_err_cnt_Mask                                                            cBit17_2
#define c_upen_gcB_sta_stagcB_err_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: stagcB_frc_err
BitField Type: RW
BitField Desc: GC Byte Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_gcB_sta_stagcB_frc_err_Mask                                                               cBit1
#define c_upen_gcB_sta_stagcB_frc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: stagcB_osecenb
BitField Type: RW
BitField Desc: Force GC Byte One Second Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_gcB_sta_stagcB_osecenb_Mask                                                               cBit0
#define c_upen_gcB_sta_stagcB_osecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Configuration n second to force in DS3
Reg Addr   : 0x00080f80-0x00080f80
Reg Formula:
    Where  :
Reg Desc   :
This register is applicable to configure n second in  DS3

------------------------------------------------------------------------------*/
#define cReg_upennsecds3_Base                                                                    0x00080f80
#define cReg_upennsecds3                                                                       0x00080f80UL
#define cReg_upennsecds3_WidthVal                                                                        32
#define cReg_upennsecds3_WriteMask                                                                        0x0

#define c_upennsecds3_frcpfrmen_Mask                                                                 cBit16
#define c_upennsecds3_frcpfrmen_Shift                                                                    16

/*--------------------------------------
BitField Name: cfgnsecds3
BitField Type: RW
BitField Desc: The second number in DS Congiguration
BitField Bits: [15:0]
--------------------------------------*/
#define c_upennsecds3_cfgnsecds3_Mask                                                              cBit15_0
#define c_upennsecds3_cfgnsecds3_Shift                                                                    0

/*------------------------------------------------------------------------------
Reg Name   : Force LOS Bit Error Configuration
Reg Addr   : 0x00080d00-0x00080d17
Reg Formula: 0x00080d00 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force LOS Bit Error

------------------------------------------------------------------------------*/
#define cAttReg_upen_los_cfg                                                                           0x00080d00
#define cReg_upen_los_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: cfg_msk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 Event
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_los_cfg_cfg_msk_pos_Mask                                                              cBit30_15
#define c_upen_los_cfg_cfg_msk_pos_Shift                                                                    15

/*--------------------------------------
BitField Name: cfg_sec_num
BitField Type: RW
BitField Desc: The T second number
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_los_cfg_cfg_sec_num_Mask                                                               cBit14_7
#define c_upen_los_cfg_cfg_sec_num_Shift                                                                     7

/*--------------------------------------
BitField Name: cfg_evtin1s
BitField Type: RW
BitField Desc: The even number in a second set in T second
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_los_cfg_cfg_evtin1s_Mask                                                                cBit6_3
#define c_upen_los_cfg_cfg_evtin1s_Shift                                                                     3

/*--------------------------------------
BitField Name: cfg_frc_ena
BitField Type: RW
BitField Desc: Force Alarm LOS Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_los_cfg_cfg_frc_ena_Mask                                                                  cBit2
#define c_upen_los_cfg_cfg_frc_ena_Shift                                                                     2

/*--------------------------------------
BitField Name: cfg_frc_mod
BitField Type: RW
BitField Desc: Force Alarm LOS mode 00: Set Alarm in T second 01: Set or clear
1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_los_cfg_cfg_frc_mod_Mask                                                                cBit1_0
#define c_upen_los_cfg_cfg_frc_mod_Shift                                                                     0



/*------------------------------------------------------------------------------
Reg Name   : Force LOS Bit Error Status
Reg Addr   : 0x00080d20-0x00080d37
Reg Formula: 0x00080d20 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to see the Force LOS Bit Error Status

------------------------------------------------------------------------------*/
#define cAttReg_upen_los_sta                                                                           0x00080d20
#define cReg_upen_los_sta_WidthVal                                                                          32

/*--------------------------------------
BitField Name: sta_msk_pos
BitField Type: RW
BitField Desc: The Position Index Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_los_sta_sta_msk_pos_Mask                                                              cBit20_17
#define c_upen_los_sta_sta_msk_pos_Shift                                                                    17

/*--------------------------------------
BitField Name: sta_seccnt
BitField Type: RW
BitField Desc: The Event Counter Status
BitField Bits: [16:09]
--------------------------------------*/
#define c_upen_los_sta_sta_seccnt_Mask                                                                cBit16_9
#define c_upen_los_sta_sta_seccnt_Shift                                                                      9

/*--------------------------------------
BitField Name: stamseccnt
BitField Type: RW
BitField Desc: The Second Counter Status
BitField Bits: [08:05]
--------------------------------------*/
#define c_upen_los_sta_stamseccnt_Mask                                                                 cBit8_5
#define c_upen_los_sta_stamseccnt_Shift                                                                      5

/*--------------------------------------
BitField Name: sta_upd_cnt
BitField Type: RW
BitField Desc: Updated Status
BitField Bits: [04:03]
--------------------------------------*/
#define c_upen_los_sta_sta_upd_cnt_Mask                                                                cBit4_3
#define c_upen_los_sta_sta_upd_cnt_Shift                                                                     3

/*--------------------------------------
BitField Name: statimmsec
BitField Type: RW
BitField Desc: Posegde Second Status
BitField Bits: [02]
--------------------------------------*/
#define c_upen_los_sta_statimmsec_Mask                                                                   cBit2
#define c_upen_los_sta_statimmsec_Shift                                                                      2

/*--------------------------------------
BitField Name: statim_sec
BitField Type: RW
BitField Desc: Posegde Second Status
BitField Bits: [01]
--------------------------------------*/
#define c_upen_los_sta_statim_sec_Mask                                                                   cBit1
#define c_upen_los_sta_statim_sec_Shift                                                                      1

/*--------------------------------------
BitField Name: staosecenb
BitField Type: RW
BitField Desc: Force LOS  Enable Status
BitField Bits: [00]
--------------------------------------*/
#define c_upen_los_sta_staosecenb_Mask                                                                   cBit0
#define c_upen_los_sta_staosecenb_Shift                                                                      0


/*------------------------------------------------------------------------------
Reg Name   : Force AIS Bit Error Configuration for DS3
Reg Addr   : 0x00080e80-0x00080e97
Reg Formula: 0x00080e80 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force AIS Bit Error

------------------------------------------------------------------------------*/
#define cAttReg_upen_ais_cfg                                                                        0x00080d80
#define cReg_upen_ais_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: ais_msk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 AIS Event
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_ais_cfg_ais_msk_pos_Mask                                                              cBit30_15
#define c_upen_ais_cfg_ais_msk_pos_Shift                                                                    15

/*--------------------------------------
BitField Name: ais_sec_num
BitField Type: RW
BitField Desc: The T second number to force AIS
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_ais_cfg_ais_sec_num_Mask                                                               cBit14_7
#define c_upen_ais_cfg_ais_sec_num_Shift                                                                     7

/*--------------------------------------
BitField Name: ais_evtin1s
BitField Type: RW
BitField Desc: The even AIS number in a second set in T second
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_ais_cfg_ais_evtin1s_Mask                                                                cBit6_3
#define c_upen_ais_cfg_ais_evtin1s_Shift                                                                     3

/*--------------------------------------
BitField Name: ais_frc_ena
BitField Type: RW
BitField Desc: Force Alarm AIS Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_ais_cfg_ais_frc_ena_Mask                                                                  cBit2
#define c_upen_ais_cfg_ais_frc_ena_Shift                                                                     2

/*--------------------------------------
BitField Name: ais_frc_mod
BitField Type: RW
BitField Desc: Force Alarm AIS mode 00: Set Alarm in T second 01: Set or clear
1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_ais_cfg_ais_frc_mod_Mask                                                                cBit1_0
#define c_upen_ais_cfg_ais_frc_mod_Shift                                                                     0

/*------------------------------------------------------------------------------
Reg Name   : Force AIS Bit Error Configuration
Reg Addr   : 0x00080e80-0x00080e97
Reg Formula: 0x00080e80 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force AIS Bit Error

------------------------------------------------------------------------------*/
#define cAttReg_upen_ais_e3_cfg                                                                       0x00080e80
#define cReg_upen_ais_e3_cfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: ais_e3_msk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 AIS Event
BitField Bits: [30:15]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_e3_msk_pos_Mask                                                              cBit30_15
#define c_upen_ais_e3_cfg_ais_e3_msk_pos_Shift                                                                    15

/*--------------------------------------
BitField Name: ais_e3_sec_num
BitField Type: RW
BitField Desc: The T second number to force AIS
BitField Bits: [14:7]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_e3_sec_num_Mask                                                               cBit14_7
#define c_upen_ais_e3_cfg_ais_e3_sec_num_Shift                                                                     7

/*--------------------------------------
BitField Name: ais_e3_evtin1s
BitField Type: RW
BitField Desc: The even AIS number in a second set in T second
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_evtin1s_Mask                                                                cBit6_3
#define c_upen_ais_e3_cfg_ais_evtin1s_Shift                                                                     3

/*--------------------------------------
BitField Name: ais_frc_ena
BitField Type: RW
BitField Desc: Force Alarm AIS Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_frc_ena_Mask                                                                  cBit2
#define c_upen_ais_e3_cfg_ais_frc_ena_Shift                                                                     2

/*--------------------------------------
BitField Name: ais_frc_mod
BitField Type: RW
BitField Desc: Force Alarm AIS mode 00: Set Alarm in T second 01: Set or clear
1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_ais_e3_cfg_ais_frc_mod_Mask                                                                cBit1_0
#define c_upen_ais_e3_cfg_ais_frc_mod_Shift                                                                     0

/*------------------------------------------------------------------------------
Reg Name   : Force AIS Bit Error Status for DS3
Reg Addr   : 0x00080da0-0x00080db7
Reg Formula: 0x00080da0 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to see the Force AIS Bit Error Status

------------------------------------------------------------------------------*/
#define cAttReg_upen_ais_sta                                                                           0x00080da0
#define cAttReg_upen_ais_sta_WidthVal                                                                          32

/*--------------------------------------
BitField Name: aismsk_posst
BitField Type: RW
BitField Desc: The Position Index AIS Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_ais_sta_aismsk_posst_Mask                                                             cBit20_17
#define c_upen_ais_sta_aismsk_posst_Shift                                                                   17

/*--------------------------------------
BitField Name: ais_seccntst
BitField Type: RW
BitField Desc: The Event AIS Counter Status
BitField Bits: [16:09]
--------------------------------------*/
#define c_upen_ais_sta_ais_seccntst_Mask                                                              cBit16_9
#define c_upen_ais_sta_ais_seccntst_Shift                                                                    9

/*--------------------------------------
BitField Name: aismseccntst
BitField Type: RW
BitField Desc: The Second AIS Counter Status
BitField Bits: [08:05]
--------------------------------------*/
#define c_upen_ais_sta_aismseccntst_Mask                                                               cBit8_5
#define c_upen_ais_sta_aismseccntst_Shift                                                                    5

/*--------------------------------------
BitField Name: aisupd_cntst
BitField Type: RW
BitField Desc: AIS Updated Status
BitField Bits: [04:03]
--------------------------------------*/
#define c_upen_ais_sta_aisupd_cntst_Mask                                                               cBit4_3
#define c_upen_ais_sta_aisupd_cntst_Shift                                                                    3

/*--------------------------------------
BitField Name: aistimmsecst
BitField Type: RW
BitField Desc: Posegde MiliSecond AIS Status
BitField Bits: [02]
--------------------------------------*/
#define c_upen_ais_sta_aistimmsecst_Mask                                                                 cBit2
#define c_upen_ais_sta_aistimmsecst_Shift                                                                    2

/*--------------------------------------
BitField Name: aistim_secst
BitField Type: RW
BitField Desc: Posegde Second ASI Status
BitField Bits: [01]
--------------------------------------*/
#define c_upen_ais_sta_aistim_secst_Mask                                                                 cBit1
#define c_upen_ais_sta_aistim_secst_Shift                                                                    1

/*--------------------------------------
BitField Name: aisosecenbst
BitField Type: RW
BitField Desc: Force AIS  Enable Status
BitField Bits: [00]
--------------------------------------*/
#define c_upen_ais_sta_aisosecenbst_Mask                                                                 cBit0
#define c_upen_ais_sta_aisosecenbst_Shift                                                                    0

/*------------------------------------------------------------------------------
Reg Name   : Force AIS Bit Error Status
Reg Addr   : 0x00080ea0-0x00080eb7
Reg Formula: 0x00080ea0 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to see the Force AIS Bit Error Status

------------------------------------------------------------------------------*/
#define cAttReg_upen_ais_e3_sta                                                                           0x00080ea0
#define cAttReg_upen_ais_e3_sta_WidthVal                                                                          32

/*--------------------------------------
BitField Name: aismsk_posst
BitField Type: RW
BitField Desc: The Position Index AIS Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3msk_posst_Mask                                                         cBit20_17
#define c_upen_ais_e3_sta_ais_e3msk_posst_Shift                                                               17

/*--------------------------------------
BitField Name: ais_seccntst
BitField Type: RW
BitField Desc: The Event AIS Counter Status
BitField Bits: [16:09]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_seccntst_Mask                                                              cBit16_9
#define c_upen_ais_e3_sta_ais_seccntst_Shift                                                                    9

/*--------------------------------------
BitField Name: aismseccntst
BitField Type: RW
BitField Desc: The Second AIS Counter Status
BitField Bits: [08:05]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3mseccntst_Mask                                                               cBit8_5
#define c_upen_ais_e3_sta_ais_e3mseccntst_Shift                                                                    5

/*--------------------------------------
BitField Name: ais_e3upd_cntst
BitField Type: RW
BitField Desc: AIS Updated Status
BitField Bits: [04:03]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3upd_cntst_Mask                                                               cBit4_3
#define c_upen_ais_e3_sta_ais_e3upd_cntst_Shift                                                                    3

/*--------------------------------------
BitField Name: aistimmsecst
BitField Type: RW
BitField Desc: Posegde MiliSecond AIS Status
BitField Bits: [02]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3timmsecst_Mask                                                                 cBit2
#define c_upen_ais_e3_sta_ais_e3timmsecst_Shift                                                                    2

/*--------------------------------------
BitField Name: ais_e3tim_secst
BitField Type: RW
BitField Desc: Posegde Second ASI Status
BitField Bits: [01]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3tim_secst_Mask                                                                 cBit1
#define c_upen_ais_e3_sta_ais_e3tim_secst_Shift                                                                    1

/*--------------------------------------
BitField Name: ais_e3osecenbst
BitField Type: RW
BitField Desc: Force AIS  Enable Status
BitField Bits: [00]
--------------------------------------*/
#define c_upen_ais_e3_sta_ais_e3osecenbst_Mask                                                                 cBit0
#define c_upen_ais_e3_sta_ais_e3osecenbst_Shift                                                                    0

/*------------------------------------------------------------------------------
Reg Name   : Force FEBE Error Configuration
Reg Addr   : 0x00080980-0x00080997
Reg Formula: 0x00080980 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to configure Force FEBE Error

------------------------------------------------------------------------------*/
#define cReg_upen_febe_cfg                                                                          0x00080980
#define cReg_upen_febe_cfg_WidthVal                                                                         64

/*--------------------------------------
BitField Name: cfgfebe_err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force FEBE Error Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_febe_cfg_cfgfebe_err_step_Mask                                                          cBit9_2
#define c_upen_febe_cfg_cfgfebe_err_step_Shift                                                               2

/*--------------------------------------
BitField Name: cfgfebe_msk_pos
BitField Type: RW
BitField Desc: The Position Mask for FEBE Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_febe_cfg_cfgfebe_msk_pos_Mask_01                                                      cBit31_18
#define c_upen_febe_cfg_cfgfebe_msk_pos_Shift_01                                                            18
#define c_upen_febe_cfg_cfgfebe_msk_pos_Mask_02                                                        cBit1_0
#define c_upen_febe_cfg_cfgfebe_msk_pos_Shift_02                                                             0

/*--------------------------------------
BitField Name: cfgfebe_msk_dat
BitField Type: RW
BitField Desc: The Data Mask For FEBE Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_febe_cfg_cfgfebe_msk_dat_Mask                                                            cBit17
#define c_upen_febe_cfg_cfgfebe_msk_dat_Shift                                                               17

/*--------------------------------------
BitField Name: cfgfebe_err_num
BitField Type: RW
BitField Desc: The Total FEBE Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_febe_cfg_cfgfebe_err_num_Mask                                                          cBit16_1
#define c_upen_febe_cfg_cfgfebe_err_num_Shift                                                                1

/*--------------------------------------
BitField Name: cfgfebe_frc_1sen
BitField Type: RW
BitField Desc: Force FEBE in One second enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_febe_cfg_cfgfebe_frc_1sen_Mask                                                            cBit0
#define c_upen_febe_cfg_cfgfebe_frc_1sen_Shift                                                               0


/*------------------------------------------------------------------------------
Reg Name   : Force FEBE Error Status
Reg Addr   : 0x000809a0-0x000809b7
Reg Formula: 0x000809a0 + $ds3id
    Where  :
           + $ds3id(0-23): DS3 Index
Reg Desc   :
This register is applicable to see the Force FEBE Error Status

------------------------------------------------------------------------------*/
#define cAttReg_upen_febe_sta                                                                          0x000809a0
#define cAttReg_upen_febe_sta_WidthVal                                                                         32

/*--------------------------------------
BitField Name: stafebe_err_stepcnt
BitField Type: RW
BitField Desc: The Event FEBE Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_febe_sta_stafebe_err_stepcnt_Mask                                                     cBit29_22
#define c_upen_febe_sta_stafebe_err_stepcnt_Shift                                                           22

/*--------------------------------------
BitField Name: stafebe_msk_poscnt
BitField Type: RW
BitField Desc: The Position Mask FEBE Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_febe_sta_stafebe_msk_poscnt_Mask                                                      cBit21_18
#define c_upen_febe_sta_stafebe_msk_poscnt_Shift                                                            18

/*--------------------------------------
BitField Name: stafebe_err_cnt
BitField Type: RW
BitField Desc: The Remain Force FEBE Error Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_febe_sta_stafebe_err_cnt_Mask                                                          cBit17_2
#define c_upen_febe_sta_stafebe_err_cnt_Shift                                                                2

/*--------------------------------------
BitField Name: stafebe_frc_err
BitField Type: RW
BitField Desc: FEBE Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_febe_sta_stafebe_frc_err_Mask                                                             cBit1
#define c_upen_febe_sta_stafebe_frc_err_Shift                                                                1

/*--------------------------------------
BitField Name: stafebe_osecenb
BitField Type: RW
BitField Desc: Force FEBE One Second Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_febe_sta_stafebe_osecenb_Mask                                                             cBit0
#define c_upen_febe_sta_stafebe_osecenb_Shift                                                                0

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210031_PRBS_THA6A210031ATTREG_H_ */


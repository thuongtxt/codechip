/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A210031AttControllerInternal.h
 * 
 * Created Date: May 17, 2016
 *
 * Description : ATT controller header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031PDHDE3INTERNAL_H_
#define _THA6A210031PDHDE3INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhDe3Internal.h"
#include "Tha6A210031PdhAttControllerInternal.h"

typedef struct tTha6A210031PdhDe3
    {
    tTha60210031PdhDe3 super;
    }tTha6A210031PdhDe3;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe3 Tha6A210031PdhDe3ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module);
AtPdhDe3 Tha6A210031PdhDe3New(uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031PDHDE3INTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A210031AttControllerInternal.h
 * 
 * Created Date: May 17, 2016
 *
 * Description : ATT controller header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031PDHDE3INTERNAL_H_
#define _THA6A210031PDHDE3INTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031PdhDe2De1Internal.h"
#include "Tha6A210031PdhAttControllerInternal.h"

typedef struct tTha6A210031PdhDe2De1
    {
    tTha60210031PdhDe2De1 super;
    } tTha6A210031PdhDe2De1;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPdhDe1 Tha6A210031PdhDe2De1ObjectInit(AtPdhDe1 self, uint32 channelId, AtModulePdh module);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031PDHDE3INTERNAL_H_ */


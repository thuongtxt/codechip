/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2016 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ATT
 *
 * File        : Tha6A210031PdhDe1AttController.c
 *
 * Created Date: May 16, 2016
 *
 * Description : DE1 ATT controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/att/AtAttControllerInternal.h"
#include "../../../../generic/pdh/AtPdhDe1Internal.h"
#include "../../../../generic/man/AtDriverInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/pdh/ThaPdhDe2De1.h"
#include "Tha6A210031ModulePdh.h"
#include "Tha6A210031PdhDe1AttReg.h"
#include "Tha6A210031PdhAttControllerInternal.h"
#include <math.h>
/*--------------------------- Define -----------------------------------------*/
#define cNumErrorType 2
#define cAtNumDw      2
#define cRegStatusErrStepCntMask                                                       cBit30_23
#define cRegStatusErrStepCntShift                                                             23

#define cRegStatusMskPosCntMask                                                        cBit22_18
#define cRegStatusMskPosCntShift                                                              18

#define cRegStatusErrCntMask                                                             cBit17_2
#define cRegStatusErrCntShift                                                                  2

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A210031PdhDe1AttController *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6A210031PdhDe1AttControllerMethods m_methods;

/* Override */
static tAtAttControllerMethods m_AtAttControllerOverride;
static tThaAttControllerMethods m_ThaAttControllerOverride;
static tTha6A210031PdhDe3AttControllerMethods m_Tha6A210031PdhDe3AttControllerOverride;

/* Save super implementation */
static const tAtAttControllerMethods  * m_AtAttControllerMethods = NULL;
static const tThaAttControllerMethods * m_ThaAttControllerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 StartVersionSupportMfas(Tha6A210031PdhDe1AttController self)
	{
	AtUnused(self);
	return 0xFFFFFFFF;
	}

static uint32 StartVersionSupportTwoCRCForce(Tha6A210031PdhDe1AttController self)
    {
    AtUnused(self);
    return 0xFFFFFFFF;
    }

static uint32 StartVersionSupportStep16bit(ThaAttController self)
    {
    AtUnused(self);
    return 0xFFFFFFFF;
    }

static eBool  IsMfas(Tha6A210031PdhDe1AttController self)
	{
	ThaVersionReader version = ThaAttControllerVersionReader((ThaAttController)self);
	uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(version);
	AtUnused(self);

	if (hwVersion >= mMethodsGet(mThis(self))->StartVersionSupportMfas(mThis(self)))
		return cAtTrue;
	return cAtFalse;
	}

static eBool  IsTwoCRCForce(Tha6A210031PdhDe1AttController self)
    {
    ThaVersionReader version = ThaAttControllerVersionReader((ThaAttController)self);
    uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(version);
    AtUnused(self);

    if (hwVersion >= mMethodsGet(mThis(self))->StartVersionSupportTwoCRCForce(mThis(self)))
        return cAtTrue;
    return cAtFalse;
    }

static uint32 StartVersionSupportAlarmForceV2(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0x6, 0x9, 0x4100);
    }

static uint32 MinStep(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType == cAtPdhDe1CounterFe)
        return 29;
    else
        return 1;
    }


static eBool  IsE1(ThaAttController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtAttControllerChannelGet((AtAttController)self);
    return AtPdhDe1IsE1(de1) ? cAtTrue : cAtFalse;
    }

static uint32 MaxStep(ThaAttController self, uint32 isError, uint32 errorType, uint32 Nrate, uint32 NbitPerFrame)
    {
    AtUnused(self);
    AtUnused(Nrate);
    AtUnused(NbitPerFrame);
    if (isError == cThaAttForceError)
        {
        if (ThaAttControllerIsStep16bit(self, errorType))
            return 0xFFFF;   /* 0x65535 */

        }
    return 255;
    }

static eBool IsStep16bit(ThaAttController self, uint32 errorType)
    {
    if ( errorType==cAtPdhDe1CounterCrc)
        {
        ThaVersionReader version = ThaAttControllerVersionReader((ThaAttController) self);
        uint32 hwVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(version);
        if (hwVersion >= mMethodsGet(self)->StartVersionSupportStep16bit(self))
            return cAtTrue;
        }
    return cAtFalse;
    }

static float MaxErrorNum_1s(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    float NFramePerSecond = ThaAttControllerNFramePerSecond((ThaAttController) self);

    AtUnused(errorType);
    if (errorType == cAtPdhDe1CounterFe)
        return ((float)NFramePerSecond *3)/1000;
    else
        return NFramePerSecond;
    }

static float MaxErrorNum(Tha6A210031PdhDe3AttController self, uint32 errorType, uint32 T/*ms*/)
    {
    float NFramePerSecond  = ThaAttControllerNFramePerSecond((ThaAttController) self); /* = 9.398 frame*/
    uint32 WINDOW_ADEN     = 3; /* 3ms*/

    AtUnused(errorType);
    if (errorType == cAtPdhDe1CounterFe)
        return (float) T / (float) WINDOW_ADEN; /* T ms */
    return (NFramePerSecond *(float)T)/1000;
    }

static eBool  AlarmIsFromError(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if (alarmType==cAtPdhDe1AlarmLof   ||
        alarmType==cAtPdhDe1AlarmRai   ||
        alarmType==cAtPdhDe1AlarmSigRai||
        alarmType==cAtPdhDe1AlarmSigLof)
        return cAtTrue;
    return cAtFalse;
    }

static uint32 RegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtPdhDe1ErrorFe:
            if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
                return cDs1Reg_upen_fbitcfg;
            return cDs1Reg_upen_fbitcfg;

        case cAtPdhDe1ErrorCrc:
            if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
                return cReg_upen_crc_cfg;
            if (IsTwoCRCForce((Tha6A210031PdhDe1AttController) self))
                return 0x00098800;
            return cReg_upen_crc_cfg;

        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromErrorType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtPdhDe1ErrorFe:
            if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
                return cDs1Reg_upen_fbitsta;
            else if (IsTwoCRCForce((Tha6A210031PdhDe1AttController) self))
                return 0x984000;
            return cDs1Reg_upen_fbitsta;

        case cAtPdhDe1ErrorCrc:
            if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
                return cDs1Reg_upen_crc_sta;
            else if (IsTwoCRCForce((Tha6A210031PdhDe1AttController) self))
                return 0x00098C00;
            return cDs1Reg_upen_crc_sta;

        default:
            return cBit31_0;
        }
    }

#define cAtNumCrcDs1EsfMaskBigMax  4
#define cAtNumCrcDs1EsfMaskMiniMax 2
static void InitValueToReduceCalculate(uint32 Nrate, eBool *needCalculate,
                                       uint32 *s, uint32 *p, uint32 *crcDs1Esf_m,
                                       uint32 *s_2nd, uint32 *p_2nd, uint32 *crcDs1Esf_m_2nd)
    {
    *needCalculate = cAtFalse;
    if (Nrate==cAtNRate1E3)
        {
        *s            = 1; *p      = 14; *crcDs1Esf_m      = 3;
        *s_2nd  = 1; *p_2nd  = 16; *crcDs1Esf_m_2nd  = 2;
        }
    else if (Nrate==cAtNRate1E4)
        {
        *s = 10;            *p      = 15; *crcDs1Esf_m      = 4;
        *s_2nd  = 17; *p_2nd  = 12; *crcDs1Esf_m_2nd  = 2;
        }
    else if (Nrate==cAtNRate1E5)
        {
        *s = 100;           *p      = 16; *crcDs1Esf_m      = 4;
        *s_2nd  = 89; *p_2nd  = 9;  *crcDs1Esf_m_2nd  = 1;
        }
    else if (Nrate==cAtNRate1E6)
        {
        *s = 255;   *p=11;     *crcDs1Esf_m    =1;
        *s_2nd=226, *p_2nd=07, *crcDs1Esf_m_2nd=1;
        }
    else
        {
        *s = 255;
        *needCalculate = cAtTrue;
        }
    }
static eAtRet CalculateStepAndPositionMaskForRateCrcDs1Esf(Tha6A210031PdhDe1AttController self, uint32 errorType, uint32 Nrate,
                                                     uint16 *err_step, uint16 *PositionMask, uint8 *errorCrcDs1EsfMask,
                                                     uint16 *err_step_mini, uint16 *PositionMask_mini, uint8 *errorCrcDs1EsfMask_mini)
    {
    eBool      debug  = cAtFalse, needCalculate = cAtFalse;
    tRateInfo  array;
    uint8      msk[cAtNumCrcDs1EsfMaskBigMax + 1]       = { 0x0, 0x1,   0x3,    0x7,    0xF};
    uint8      msk_mini[cAtNumCrcDs1EsfMaskMiniMax + 1] = { 0x0, 0x10,  0x30};
    uint32     s            = 0,      p      = 0, crcDs1Esf_m       = 0,     max_pos=16;
    uint32     s_2nd        = 0,      p_2nd  = 0, crcDs1Esf_m_2nd   = 0;
    uint32     NbitPerFrame = ThaAttControllerNbitPerFrame((ThaAttController) self);
    uint32     maxStep      = ThaAttControllerMaxStep((ThaAttController)self, cThaAttForceError,  errorType, Nrate, NbitPerFrame);
    double     A            = ((double)1/(double)Nrate);
    double     ss           = 0;
    double     tempMinVal   = (double)999999999999;

    InitValueToReduceCalculate(Nrate,&needCalculate,
                               &s,           &p,     &crcDs1Esf_m,
                               &s_2nd, &p_2nd, &crcDs1Esf_m_2nd);
    if (needCalculate)
        {
        for (crcDs1Esf_m = 1; crcDs1Esf_m < cAtNumCrcDs1EsfMaskBigMax+1; crcDs1Esf_m++)
            {
            for (p=1; p<(max_pos+1); p++)
                {
                for (s_2nd = 1; s_2nd < (maxStep + 1); s_2nd++)
                    {
                    for (crcDs1Esf_m_2nd = 0; crcDs1Esf_m_2nd < (cAtNumCrcDs1EsfMaskMiniMax+ 1); crcDs1Esf_m_2nd++)
                        {
                        for (p_2nd=0; p_2nd<(max_pos+1); p_2nd++)
                            {
                            double T      = (double)(max_pos * s * s_2nd* NbitPerFrame)*1.0f;
                            double value  = (((double)p      * (double)crcDs1Esf_m     * (double)s_2nd*1.0f)/T)       +
                                            (((double)p_2nd * (double)crcDs1Esf_m_2nd* (double)s*1.0f)/T);
                            double diff   = fabs(value-A);
                            if (diff < tempMinVal)
                                {
                                tempMinVal = diff;
                                array.step              = (uint16)s;
                                array.positionMask      = (uint16)p;
                                array.frc_en_mask       = (uint8) crcDs1Esf_m;
                                array.step_mini         = (uint16)s_2nd;
                                array.positionMask_mini = (uint16)p_2nd;
                                array.frc_en_mask_mini  = (uint8) crcDs1Esf_m_2nd;
                                array.value             = value;
                                array.diff              = diff;
                                array.max_pos           = (uint8)max_pos;
                                if (debug)
                                    AtPrintf("step=%03d, PositionMask=%02d, frc_en_mask=%d,                \r\n"
                                             "step_mini=%03d, PositionMask_mini=%02d, frc_en_mask_mini=%d, \r\n"
                                             "A=%.12f value=%.12f, diff=%.12f                               \r\n",
                                             s , p, crcDs1Esf_m,
                                             s_2nd , p_2nd, crcDs1Esf_m_2nd,
                                             A, value, diff);
                                }

                            }
                        }
                    }
                }
            }
        }
    else
        {
        double T      = (double)(max_pos * s * s_2nd* NbitPerFrame)*1.0f;
        double value  = (((double)p      * (double)crcDs1Esf_m     * (double)s_2nd*1.0f)/T)       +
                        (((double)p_2nd * (double)crcDs1Esf_m_2nd* (double)s*1.0f)/T);
        double diff   = fabs(value-A);
        if (diff < tempMinVal)
            {
            tempMinVal = diff;
            array.step              = (uint16)s;
            array.positionMask      = (uint16)p;
            array.frc_en_mask       = (uint8) crcDs1Esf_m;
            array.step_mini         = (uint16)s_2nd;
            array.positionMask_mini = (uint16)p_2nd;
            array.frc_en_mask_mini  = (uint8) crcDs1Esf_m_2nd;
            array.value             = value;
            array.diff              = diff;
            array.max_pos           = (uint8)max_pos;
            if (debug)
                AtPrintf("step=%03d, PositionMask=%02d, frc_en_mask=%d,                \r\n"
                         "step_mini=%03d, PositionMask_mini=%02d, frc_en_mask_mini=%d, \r\n"
                         "A=%.12f value=%.12f, diff=%.12f                               \r\n",
                         s , p, crcDs1Esf_m,
                         s_2nd , p_2nd, crcDs1Esf_m_2nd,
                         A, value, diff);
            }
        }

    *PositionMask            = mMethodsGet((ThaAttController)self)->PositionMaskGet(array.max_pos, array.positionMask);
    *err_step                = array.step;
    *errorCrcDs1EsfMask      = msk[array.frc_en_mask];
    *PositionMask_mini       = mMethodsGet((ThaAttController)self)->PositionMaskGet(array.max_pos, array.positionMask_mini);
    *err_step_mini           = array.step_mini;
    *errorCrcDs1EsfMask_mini = msk_mini[array.frc_en_mask_mini];
    ss =(double) fabs((double)(array.value - A))/A;
    if (debug || mMethodsGet((ThaAttController)self)->IsCheckRateShow((ThaAttController)self))
        {
        AtPrintf("===>Big_step=%03d, BigPositionMask=%02d, BigCrcDs1EsfMask=%d, "
                 "    mini_step=%03d, miniPositionMask=%02d, mini_CrcDs1EsfMask=%d, "
                 "    A=%.12f value=%.12f, diff=%.12f max_pos=%d ss=%.12f\n",
                  array.step ,      array.positionMask,      array.frc_en_mask,
                  array.step_mini , array.positionMask_mini, array.frc_en_mask_mini,
                  A, array.value,array.diff, array.max_pos, ss);
        }

    if (ss > 0.15)
        {
        if (!mMethodsGet((ThaAttController)self)->IsCheckRate((ThaAttController)self))
            return cAtOk;
        return cAtErrorModeNotSupport;
        }

    return cAtOk;
    }

static eAtRet ForceErrorCrcMaskSet(AtAttController self, uint32 errorType, uint32 crcMask)
    {
    if (errorType == cAtPdhDe1CounterCrc)
        {
        Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
        uint8  errorTypeIndex = mMethodsGet(att)->ErrorTypeIndex(att, errorType);
        eAtAttForceErrorMode forceType = att->errorForceType[errorTypeIndex];
        att->errorInfo[errorTypeIndex].errorCrcDs1EsfMask     = (uint8)crcMask;
        mMethodsGet(att)->ForceErrorHelper(att, errorType, forceType);
        }
    return cAtOk;
    }

static eAtRet ForceErrorCrcMask2Set(AtAttController self, uint32 errorType, uint32 crcMask)
    {
    if (errorType == cAtPdhDe1CounterCrc)
        {
        Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
        uint8  errorTypeIndex = mMethodsGet(att)->ErrorTypeIndex(att, errorType);
        eAtAttForceErrorMode forceType = att->errorForceType[errorTypeIndex];
        att->errorInfo[errorTypeIndex].errorMiniCrcDs1EsfMask     = (uint8)crcMask;
        mMethodsGet(att)->ForceErrorHelper2nd(att, errorType, forceType);
        }
    return cAtOk;
    }

static eAtRet DefaultDs1DataMask(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    if (IsTwoCRCForce((Tha6A210031PdhDe1AttController) self))
        {
        if (errorType == cAtPdhDe1CounterCrc)
            {
            if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
                return cAtOk;
            else{
                Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
                uint32 regFieldMask   = 0, regFieldShift = 0;
                uint32 regAddr        = 0, regVal=0;
                uint8  errorTypeIndex = mMethodsGet(att)->ErrorTypeIndex(att, errorType);
                if (att->errorMode[errorTypeIndex] != cAtAttForceErrorModeRate)
                    {
                    att->errorInfo[errorTypeIndex].errorCrcDs1EsfMask     = 1;
                    att->errorInfo[errorTypeIndex].errorMiniCrcDs1EsfMask = 0;

                    regAddr        = mMethodsGet(mThis(self))->RegAddressTxMaskCrcDs1Esf(mThis(self), errorType);
                    regVal         = mMethodsGet((AtAttController)self)->Read((AtAttController)self, regAddr);

                    regFieldMask = mMethodsGet(mThis(self))->TxMaskCrcDs1EsfMaskBig(mThis(self), cThaAttForceError, errorType);
                    regFieldShift = AtRegMaskToShift(regFieldMask);
                    mRegFieldSet(regVal, regField, att->errorInfo[errorTypeIndex].errorCrcDs1EsfMask);

                    regFieldMask = mMethodsGet(mThis(self))->TxMaskCrcDs1EsfMaskMini(mThis(self), cThaAttForceError, errorType);
                    regFieldShift = AtRegMaskToShift(regFieldMask);
                    mRegFieldSet(regVal, regField, att->errorInfo[errorTypeIndex].errorMiniCrcDs1EsfMask);

                    mMethodsGet((AtAttController)self)->Write((AtAttController)self, regAddr, regVal);
                    }
                }
            }
        else
            return cAtOk;
        }
    return cAtOk;
    }

static eAtRet AlarmUnitSet(Tha6A210031PdhDe3AttController self, uint32 alarmType, eAlarmUnit unit)
    {
    AtUnused(self);
    AtUnused(alarmType);
    AtUnused(unit);
    return cAtOk;
    }

static uint32 RegAddressTxMaskCrcDs1Esf(Tha6A210031PdhDe1AttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType==cAtPdhDe1CounterCrc)
        {
        if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
            return cBit31_0;
        else if (IsTwoCRCForce((Tha6A210031PdhDe1AttController) self))
            return 0x97800;
        }
    return cBit31_0;
    }

static uint32 TxMaskCrcDs1EsfMaskBig(Tha6A210031PdhDe1AttController self,  uint32 isError,uint32 errorType)
    {
    AtUnused(self);
    if (isError == cThaAttForceError)
        {
        if (errorType==cAtPdhDe1CounterCrc)
            {
            if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
                return cBit31_0;
            else if (IsTwoCRCForce((Tha6A210031PdhDe1AttController) self))
                return cBit11_6;
            }
        }
    return cBit31_0;
    }

static uint32 TxMaskCrcDs1EsfMaskMini(Tha6A210031PdhDe1AttController self,  uint32 isError,uint32 errorType)
    {
    AtUnused(self);
    if (isError == cThaAttForceError)
        {
        if (errorType==cAtPdhDe1CounterCrc)
            {
            if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
                return cBit31_0;
            else if (IsTwoCRCForce((Tha6A210031PdhDe1AttController) self))
                return cBit5_0;
            }
        }
    return cBit31_0;
    }

static uint32 RegAddressFromErrorType2nd(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType==cAtPdhDe1CounterCrc)
        {
        if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
            return cBit31_0;
        else if (IsTwoCRCForce((Tha6A210031PdhDe1AttController) self))
            return 0x0009A000;
        }
    return cBit31_0;
    }

static uint32 StatusRegAddressFromErrorType2nd(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    if (errorType==cAtPdhDe1CounterCrc)
        {
        if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
            return cBit31_0;
        else if (IsTwoCRCForce((Tha6A210031PdhDe1AttController) self))
            return 0x0009A400;
        }
    return cBit31_0;
    }

static void ForceErrorHelperMini(Tha6A210031PdhDe1AttController self, uint32 errorType, eAtAttForceType forceType)
    {
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    uint32 regFieldMask   = 0, regFieldShift = 0;
    uint32 regAddr        = 0, regVal=0;
    uint8  errorTypeIndex = mMethodsGet(att)->ErrorTypeIndex(att, errorType);

    mMethodsGet(att)->ForceErrorHelper2nd(att, errorType, forceType);

    regAddr        = mMethodsGet(mThis(self))->RegAddressTxMaskCrcDs1Esf(mThis(self), errorType);
    regVal         = mMethodsGet((AtAttController)self)->Read((AtAttController)self, regAddr);
    regFieldMask = mMethodsGet(mThis(self))->TxMaskCrcDs1EsfMaskMini(mThis(self), cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(regVal, regField, att->errorInfo[errorTypeIndex].errorMiniCrcDs1EsfMask);
    mMethodsGet((AtAttController)self)->Write((AtAttController)self, regAddr, regVal);
    }


static void ForceErrorHelperBig(Tha6A210031PdhDe1AttController self, /*eAtAttPdhDe3ErrorType*/ uint32 errorType, eAtAttForceType forceType)
    {
    Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
    uint32 regFieldMask   = 0, regFieldShift = 0;
    uint32 regVal         = 0;
    uint32 regAddr        = mMethodsGet(mThis(self))->RegAddressTxMaskCrcDs1Esf(mThis(self), errorType);
    uint8  errorTypeIndex = mMethodsGet(att)->ErrorTypeIndex(att, errorType);

    att->errorForceType[errorTypeIndex] = forceType;
    mMethodsGet(att)->ForceErrorHelper(att, errorType, forceType);

    /* Ds1 MASK */
    regVal         = mMethodsGet((AtAttController)self)->Read((AtAttController)self, regAddr);
    regFieldMask = mMethodsGet(mThis(self))->TxMaskCrcDs1EsfMaskBig(mThis(self), cThaAttForceError, errorType);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(regVal, regField, att->errorInfo[errorTypeIndex].errorCrcDs1EsfMask);
    mMethodsGet((AtAttController)self)->Write((AtAttController)self, regAddr, regVal);
    }


static eAtRet HwForceErrorRateCrcDs1Esf(Tha6A210031PdhDe1AttController self, uint32 errorType, uint32 rate)
    {
    eAtRet ret = cAtOk;
    if (AtAttControllerErrorForceIsSupported((AtAttController)self, errorType))
        {
        Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
        uint16 err_step       = 0, err_step_mini=0;
        uint16 PositionMask   = 0, PositionMask_mini;
        uint8  errorCrcDs1EsfMask = 0, errorCrcDs1EsfMask_mini;
        uint32 Nrate          = mMethodsGet((ThaAttController)self)->CalculateNRate((ThaAttController)self, rate);
        uint32 errorTypeIndex = mMethodsGet(att)->ErrorTypeIndex(att, errorType);
        ret = CalculateStepAndPositionMaskForRateCrcDs1Esf(self, errorType, Nrate,
                                                           &err_step,      &PositionMask,      &errorCrcDs1EsfMask,
                                                           &err_step_mini, &PositionMask_mini, &errorCrcDs1EsfMask_mini);
        if (ret != cAtOk)
            return ret;
        att->errorInfo[errorTypeIndex].errorDataMask          = 1;
        att->errorInfo[errorTypeIndex].errorPositionMask      = PositionMask;
        att->errorInfo[errorTypeIndex].errorStep              = err_step;
        att->errorInfo[errorTypeIndex].errorCrcDs1EsfMask     = errorCrcDs1EsfMask;
        att->errorInfo[errorTypeIndex].errorMiniDataMask      = 1;
        att->errorInfo[errorTypeIndex].errorMiniPositionMask  = PositionMask_mini;
        att->errorInfo[errorTypeIndex].errorMiniStep          = err_step_mini;
        att->errorInfo[errorTypeIndex].errorMiniCrcDs1EsfMask = errorCrcDs1EsfMask_mini;
        ForceErrorHelperBig(mThis(self),  errorType, cAtAttForceTypeContinuous);
        ForceErrorHelperMini(mThis(self), errorType, cAtAttForceTypeContinuous);

        return cAtOk;
        }

    return cAtErrorModeNotSupport;
    }

static eBool IsCrcDs1Esf(Tha6A210031PdhDe1AttController self, uint32 errorType)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtAttControllerChannelGet((AtAttController)self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de1);
    if  (errorType == cAtPdhDe1CounterCrc &&
        (frameType == cAtPdhDs1FrmEsf ||
         frameType == cAtPdhJ1FrmEsf  ||
         frameType == cAtPdhE1MFCrc))
        return cAtTrue;
    return cAtFalse;
    }

static eAtRet HwForceErrorRate(ThaAttController self, uint32 errorType, uint32 rate)
    {
    if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
        return m_ThaAttControllerMethods->HwForceErrorRate(self, errorType, rate);
    if (IsTwoCRCForce(mThis(self)))
        {
        eAtRet ret = cAtOk;
        if  (IsCrcDs1Esf(mThis(self), errorType))
            ret |= HwForceErrorRateCrcDs1Esf(mThis(self), errorType, rate);
        else
            {
            Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
            ret |= m_ThaAttControllerMethods->HwForceErrorRate(self, errorType, rate);
            mMethodsGet(att)->ErrorForcing2ndDataMaskSet(att, errorType, 0);
            }
        return ret;
        }
    return m_ThaAttControllerMethods->HwForceErrorRate(self, errorType, rate);
    }

static eAtRet ForceErrorRate(AtAttController self,
                             uint32 errorType,
                             eAtBerRate rate)
    {
    /* Duration of N frames*/
    if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
        return m_AtAttControllerMethods->ForceErrorRate(self, errorType, rate);
    else if (IsTwoCRCForce(mThis(self)))
        {
        eAtRet ret = cAtOk;
        if  (IsCrcDs1Esf(mThis(self), errorType))
            {
            Tha6A210031PdhDe3AttController att = (Tha6A210031PdhDe3AttController)self;
            uint16 err_step       = 0, err_step_mini       = 0;
            uint16 PositionMask   = 0, PositionMask_mini   = 0;
            uint8  errorCrcDs1EsfMask = 0, errorCrcDs1EsfMask_mini = 0;
            uint32 errorTypeIndex = 0;
            uint32 Nrate;

            if (!AtAttControllerErrorForceIsSupported(self, errorType))
                return cAtErrorModeNotSupport;
            mMethodsGet(att)->ErrorForcingDataMaskSet(att, errorType, 0);
            mMethodsGet(att)->ErrorForcing2ndDataMaskSet(att, errorType, 0);
            errorTypeIndex  = mMethodsGet(att)->ErrorTypeIndex(att, errorType);
            att->errorRate[errorTypeIndex]  = rate;

            Nrate = mMethodsGet((ThaAttController)self)->CalculateNRate((ThaAttController)self, rate);
            ret |= CalculateStepAndPositionMaskForRateCrcDs1Esf(mThis(self), errorType, Nrate,
                                                                &err_step, &PositionMask, &errorCrcDs1EsfMask,
                                                                &err_step_mini, &PositionMask_mini, &errorCrcDs1EsfMask_mini);
            return ret;
            }
        }
    return m_AtAttControllerMethods->ForceErrorRate(self, errorType, rate);
    }

static uint32 RealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwSts;
    uint8 hwDe2Id, hwDe1Id;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)channel);
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet(channel);

    if (vc != NULL)
        {
        AtSdhChannel tu1x = AtSdhChannelParentChannelGet(vc);
        AtSdhChannel tug2 = AtSdhChannelParentChannelGet(tu1x);
        uint32 flatIdInSts = AtChannelIdGet((AtChannel)tug2) * 4 + AtChannelIdGet((AtChannel)tu1x);
        ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice, &hwSts);
        return regAddr + ThaModulePdhSliceBase(pdhModule, slice) + hwSts * 32UL + flatIdInSts;
        }

    ThaPdhDe2De1HwIdGet((ThaPdhDe1)channel, &slice, &hwSts, &hwDe2Id, &hwDe1Id, cAtModulePdh);
    return regAddr + ThaModulePdhSliceBase(pdhModule, slice) + hwSts * 32UL + hwDe2Id * 4UL + hwDe1Id;
    }

static uint32 DumpRealAddress(Tha6A210031PdhDe3AttController self, uint32 regAddr)
    {
    uint8 slice, hwSts;
    uint8 hwDe2Id, hwDe1Id;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet((AtAttController)self);
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)channel);
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet(channel);

    if (vc != NULL)
        {
        ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice, &hwSts);
        return regAddr + ThaModulePdhSliceBase(pdhModule, slice);
        }

    ThaPdhDe2De1HwIdGet((ThaPdhDe1)channel, &slice, &hwSts, &hwDe2Id, &hwDe1Id, cAtModulePdh);
    return regAddr + ThaModulePdhSliceBase(pdhModule, slice);
    }

static uint32 SignalingDumpRealAddressVal(Tha6A210031PdhDe1AttController self, uint8 tsId)
    {
    uint8 slice, hwSts;
    uint8 hwDe2Id, hwDe1Id;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet((AtAttController)self);
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet(channel);

    if (vc != NULL)
        {
        AtSdhChannel tu1x = AtSdhChannelParentChannelGet(vc);
        AtSdhChannel tug2 = AtSdhChannelParentChannelGet(tu1x);
        uint32 flatIdInSts=0;
        if  (AtPdhDe1IsE1((AtPdhDe1)channel))
            flatIdInSts = AtChannelIdGet((AtChannel)tug2) * 96UL + AtChannelIdGet((AtChannel)tu1x)*32UL;
        else
            flatIdInSts = AtChannelIdGet((AtChannel)tug2) * 96UL + AtChannelIdGet((AtChannel)tu1x)*24UL;
        ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice, &hwSts);
        return hwSts * 672UL + flatIdInSts + tsId;
        }

    ThaPdhDe2De1HwIdGet((ThaPdhDe1)channel, &slice, &hwSts, &hwDe2Id, &hwDe1Id, cAtModulePdh);
    if  (AtPdhDe1IsE1((AtPdhDe1)channel))
        return hwSts * 672UL + hwDe2Id * 96UL + hwDe1Id*32UL + tsId;
    return hwSts * 672UL + hwDe2Id * 96UL + hwDe1Id*24UL + tsId;
    }


static eBool ErrorIsFromAlarm(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    if (mMethodsGet(self)->IsErrorForceV2(self,errorType))
		return cAtFalse;
	if (errorType==cAtPdhDe1CounterFe)
		return cAtTrue;
    return cAtFalse;
    }

static uint8 ErrorTypeIndex(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtPdhDe1ErrorFe:
            return 0;
        case cAtPdhDe1ErrorCrc:
            return 1;
        default:
            return 0;
        }
    }

static uint32 ForceOffset(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return cPdhForceOffset;
    }

static eAtRet InitErrorDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i=0;
    eAtRet ret = cAtOk;
    static const uint32 cAttPdhDe1ErrorTypeVal[] = {cAtPdhDe1ErrorFe, cAtPdhDe1ErrorCrc};

    for (i = 0; i < mCount(cAttPdhDe1ErrorTypeVal); i++)
        {
        if (AtAttControllerErrorForceIsSupported((AtAttController)self, cAttPdhDe1ErrorTypeVal[i]))
            {
            uint8 errorTypeIndex;
            ret |= AtAttControllerUnForceError((AtAttController)self, cAttPdhDe1ErrorTypeVal[i]);
            ret |= AtAttControllerForceErrorModeSet((AtAttController)self, cAttPdhDe1ErrorTypeVal[i],cAtAttForceErrorModeContinuous);
            errorTypeIndex = mMethodsGet(self)->ErrorTypeIndex(self, cAttPdhDe1ErrorTypeVal[i]);
            self->errorInfo[errorTypeIndex].errorStep = mMethodsGet(self)->MinStep(self, cAttPdhDe1ErrorTypeVal[i]);/*At the receiver, window to detect LOF is 3ms. Apply for the current design you must config step_cfg must >= 29 to avoid LOF set.*/
            ret |= AtAttControllerForceErrorNumErrorsSet((AtAttController)self, cAttPdhDe1ErrorTypeVal[i], 1);
            ret |= AtAttControllerForceErrorBitPositionSet((AtAttController)self, cAttPdhDe1ErrorTypeVal[i], 0xFFFF);
            if (cAttPdhDe1ErrorTypeVal[i]==cAtPdhDe1ErrorFe)
            	{
                ret |= AtAttControllerDe1ForceFbitTypeSet((AtAttController)self, cAtAttPdhDe1FbitTypeFas, cAtTrue);
                ret |= AtAttControllerDe1ForceFbitTypeSet((AtAttController)self, cAtAttPdhDe1FbitTypeNfas, cAtTrue);
            	}
            }
        }
    return ret;
    }

static const char* AlarmIndexToString(uint8 alarmIndex)
    {
    static const char* cAttPdhDe1AlarmTypeStr[] = {"LOS","AIS","RAI","LOF", "OOF"};
    if (alarmIndex > mCount(cAttPdhDe1AlarmTypeStr))
        return cAttPdhDe1AlarmTypeStr[0];
    return cAttPdhDe1AlarmTypeStr[alarmIndex];
    }

static eAtRet InitAlarmDefault(Tha6A210031PdhDe3AttController self)
    {
    uint32 i=0;
    eAtRet ret = cAtOk;
    static const uint32 cAttPdhDe1AlarmTypeVal[] = {cAtPdhDe1AlarmLos,
                                                    cAtPdhDe1AlarmAis,
                                                    cAtPdhDe1AlarmRai,
                                                    cAtPdhDe1AlarmLof,
                                                    cAtPdhDe1AlarmSigRai,
                                                    cAtPdhDe1AlarmSigLof};
    for (i = 0; i < mCount(cAttPdhDe1AlarmTypeVal); i++)
        {
        if (AtAttControllerAlarmForceIsSupported((AtAttController)self,cAttPdhDe1AlarmTypeVal[i]))
            {
            ret |= AtAttControllerUnForceAlarm((AtAttController)self, cAttPdhDe1AlarmTypeVal[i]);
            ret |= AtAttControllerForceAlarmModeSet((AtAttController)self, cAttPdhDe1AlarmTypeVal[i], cAtAttForceAlarmModeContinuous);
            ret |= AtAttControllerForceAlarmNumEventSet((AtAttController)self, cAttPdhDe1AlarmTypeVal[i], 1);
            }
        }
    return ret;
    }
static uint8 NumErrorTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 2;
    }

static uint32 HwStepMaskHo(Tha6A210031PdhDe3AttController self,uint32 isError,  uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return c_upen_crc_cfg_cfgcrc_err_step_Mask;
        return c_upen_fbitcfg_cfgfbiterr_step_Mask;
        }
    else
        {
        if (mMethodsGet(self)->ErrorIsFromAlarm(self,errorType))
            {
            return c_upen_fbitcfg_cfgfbiterr_step_Mask;
            }
        else
            {
            if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
                return cBit9_0;
            return c_upen_crc_cfg_cfgcrc_err_step_Mask;
            }
        }
    }

static uint32 HwStepMaskLo(Tha6A210031PdhDe3AttController self,uint32 isError,  uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceError)
        {
        if (mMethodsGet(self)->ErrorIsFromAlarm(self,errorType))
            {
            return c_upen_crc_cfg_cfgcrc_err_step_Mask;
            }
        else
            {
            if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
                return cBit31_26;
            return c_upen_fbitcfg_cfgfbiterr_step_Mask;
            }
        }
    return cBit31_0;
    }

static uint32 PositionMaskConfigurationMaskHo(Tha6A210031PdhDe3AttController self,  uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return c_upen_crc_cfg_cfgcrc_msk_pos_Mask_02;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_fbitcfg_cfgfbitmsk_pos_Mask_02;
        return 0;
        }
    else
    	{
		if (mMethodsGet(self)->ErrorIsFromAlarm(self,errorType))
		    {
			return c_upen_fbitcfg_cfgfbitmsk_pos_Mask_02;
		    }
		else
		    {
		    if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
		        return cBit31_0;
		    return c_upen_crc_cfg_cfgcrc_msk_pos_Mask_02;
		    }
    	}
    }

static uint32 PositionMaskConfigurationMaskLo(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return c_upen_crc_cfg_cfgcrc_msk_pos_Mask_01;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_lof_cfg_cfg_msk_pos_Mask;
        return cDs1_upen_los_cfg_cfg_msk_pos_Mask;
        }
    else
    	{
		if (mMethodsGet(self)->ErrorIsFromAlarm(self,errorType))
		    {
			return c_upen_fbitcfg_cfgfbitmsk_pos_Mask_01;
		    }
		else
		    {
		    if (ThaAttControllerIsStep16bit((ThaAttController) self, errorType))
                return cBit25_18;
		    return c_upen_crc_cfg_cfgcrc_msk_pos_Mask_01;
		    }
    	}
    }

static uint32 DataMaskConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return c_upen_crc_cfg_cfgcrc_msk_dat_Mask;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_lof_cfg_cfg_frc_ena_Mask;
        return c_upen_los_cfg_cfg_frc_ena_Mask;
        }
    if (mMethodsGet(self)->ErrorIsFromAlarm(self,errorType))
        return c_upen_fbitcfg_cfgfbitmsk_dat_Mask;
    return c_upen_crc_cfg_cfgcrc_msk_dat_Mask;
    }

static uint32 NumberOfErrorsMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return c_upen_crc_cfg_cfgcrc_err_num_Mask;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_lof_cfg_cfg_evtin1s_Mask;
        return c_upen_los_cfg_cfg_evtin1s_Mask;
        }
    if (mMethodsGet(self)->ErrorIsFromAlarm(self,errorType))
        return c_upen_fbitcfg_cfgfbiterr_num_Mask;
    return c_upen_crc_cfg_cfgcrc_err_num_Mask;
    }

static uint32 HwForceModeConfigurationMask(Tha6A210031PdhDe3AttController self, uint32 isError, uint32 errorType)
    {
    AtUnused(self);
    AtUnused(isError);
    AtUnused(errorType);
    if (isError == cThaAttForceAlarm)
        {
        if (mMethodsGet(self)->IsAlarmForceV2(self,errorType))
            return c_upen_crc_cfg_cfgcrc_frc_1sen_Mask;
        if (mMethodsGet(self)->AlarmIsFromError(self,errorType))
            return c_upen_lof_cfg_cfg_frc_mod_Mask;
        return c_upen_los_cfg_cfg_frc_mod_Mask;
        }
    if (mMethodsGet(self)->ErrorIsFromAlarm(self,errorType))
        return c_upen_fbitcfg_cfg_frcfbitmod_Mask;
    return c_upen_crc_cfg_cfgcrc_frc_1sen_Mask;
    }

static eAtRet ForceErrorStatusGet(AtAttController self, uint32 errorType, tAtAttForceErrorStatus *status)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    uint32 statusRegAddr = mMethodsGet(controller)->StatusRegAddressFromErrorType(controller, errorType);
    uint32 regVal = mMethodsGet(self)->Read(self, statusRegAddr);
    status->remainForceErrorStatus = mRegField(regVal, cRegStatusErrCnt);
    status->positionMaskStatus = mRegField(regVal, cRegStatusMskPosCnt);
    status->errorStatus = (uint8)mRegField(regVal, cRegStatusFrcErr);
    status->oneSecondEnableStatus = (uint8)mRegField(regVal, cRegStatusOneSecondEnb);

    return cAtOk;
    }

static uint32 RegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtPdhDe1AlarmLos:
        case cAtPdhDe1AlarmAis:
            return cDs1Reg_upen_los_cfg;
        case cAtPdhDe1AlarmSigRai:
        case cAtPdhDe1AlarmRai:
            return cReg_upen_rai_cfg;
        case cAtPdhDe1AlarmSigLof:
        case cAtPdhDe1AlarmLof:
            return cDs1Reg_upen_fbitcfg;
        default:
            return cBit31_0;
        }
    }

static uint32 StatusRegAddressFromAlarmType(Tha6A210031PdhDe3AttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtPdhDe1AlarmLos:
        case cAtPdhDe1AlarmAis:
            return cDs1Reg_upen_los_sta;
        case cAtPdhDe1AlarmSigRai:
        case cAtPdhDe1AlarmRai:
            return cDs1Reg_upen_rai_sta;
        case cAtPdhDe1AlarmSigLof:
        case cAtPdhDe1AlarmLof:
            return cDs1Reg_upen_fbitsta;
        default:
            return cBit31_0;
        }
    }

static uint8 AlarmTypeIndex(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtPdhDe1AlarmLos:
            return 0;
        case cAtPdhDe1AlarmAis:
            return 1;
        case cAtPdhDe1AlarmRai:
            return 2;
        case cAtPdhDe1AlarmLof:
            return 3;
        case cAtPdhDe1AlarmSigRai:
            return 4;
        case cAtPdhDe1AlarmSigLof:
            return 5;
        default:
            return 0;
        }
    }

static uint8 NumAlarmTypes(Tha6A210031PdhDe3AttController self)
    {
    AtUnused(self);
    return 6;
    }

static eAtRet CheckAlarmNumEvent(Tha6A210031PdhDe3AttController self, uint32 alarmType, uint32 numEvent)
	{
	AtUnused(self);
	if (alarmType == cAtPdhDe1AlarmRai)
		{
		if (numEvent > 1)
			return cAtErrorInvlParm;
		}
	return cAtOk;
	}

static eAtRet SpecificAlarmConfig(Tha6A210031PdhDe3AttController self, uint32 alarmType)
    {
    AtUnused(self);
    if (alarmType==cAtPdhDe1AlarmLos||
        alarmType==cAtPdhDe1AlarmAis)
        {
        AtAttController _self =(AtAttController) self;
        uint32 regAddr  = 0x94000;
        uint32 regVal   = mMethodsGet(_self)->Read(_self, regAddr);
        uint32 aisMask  = cBit25;
        uint32 aisShift = 25;
        uint8  value    = (alarmType==cAtPdhDe1AlarmAis)?1:0;
        mRegFieldSet(regVal, ais, value);
        mMethodsGet(_self)->Write(_self, regAddr, regVal);
        }
    return cAtOk;
    }

static eBool AlarmForceIsSupported(AtAttController self, uint32 alarmType)
    {
    AtUnused(self);
    switch (alarmType)
        {
        case cAtPdhDe1AlarmLos:
        case cAtPdhDe1AlarmAis:
        case cAtPdhDe1AlarmRai:
        case cAtPdhDe1AlarmLof:
        case cAtPdhDe1AlarmSigRai:
        case cAtPdhDe1AlarmSigLof:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eBool ErrorForceIsSupported(AtAttController self, uint32 errorType)
    {
    AtUnused(self);
    switch (errorType)
        {
        case cAtPdhDe1ErrorFe:
        case cAtPdhDe1ErrorCrc:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }


static eAtRet De1CrcGapSet(AtAttController self, uint16 frameType)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    AtChannel channel = AtAttControllerChannelGet(self);
    uint32   regAddr = mMethodsGet(controller)->RealAddress(controller, cDs1Reg_upen_crc_gap);
    uint32   regVal=0, val = 0;

    regVal = mChannelHwRead(channel,regAddr, cAtModulePdh);

    if (frameType==cAtPdhDs1FrmEsf ||
        frameType==cAtPdhJ1FrmEsf)
    	val = 0x253;

    mFieldIns(&regVal, cBit15_0,0, val);
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePdh);
    return cAtOk;
    }

static void FbitTypeToMaskShift(AtAttController self, uint32 type, uint32 *mask, uint32 *shift)
	{
	switch (type)
		{
		case cAtAttPdhDe1FbitTypeFas:
			*mask = c_upen_fbit_sta_fas_enb_Mask;
			*shift = c_upen_fbit_sta_fas_enb_Shift;
			break;

		case cAtAttPdhDe1FbitTypeNfas:
			*mask = c_upen_fbit_sta_nfas_enb_Mask;
			*shift = c_upen_fbit_sta_nfas_enb_Shift;
			break;

		case cAtAttPdhDe1FbitTypeCrcMfas:
			if (mMethodsGet(mThis(self))->IsMfas(mThis(self)))
				{
				*mask = c_upen_fbit_sta_crcmfas_enb_Mask;
				*shift = c_upen_fbit_sta_crcmfas_enb_Shift;
				}
			else
				{
				*mask = cInvalidUint32;
				*shift = cInvalidUint32;
				}
			break;

		case cAtAttPdhDe1FbitTypeMfas:
			if (mMethodsGet(mThis(self))->IsMfas(mThis(self)))
				{
				*mask = c_upen_fbit_sta_mfas_enb_Mask;
				*shift = c_upen_fbit_sta_mfas_enb_Shift;
				}
			else
				{
				*mask = cInvalidUint32;
				*shift = cInvalidUint32;
				}
			break;

		default:
			*mask = cInvalidUint32;
			*shift = cInvalidUint32;
			break;
		}
	}

static eAtRet De1FbitTypeSet(AtAttController self, uint32 type, eBool en)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    AtChannel channel = AtAttControllerChannelGet(self);
    uint32   regAddr = mMethodsGet(controller)->RealAddress(controller, cDs1Reg_upen_fbit_sta);
    uint32   regVal=0, mask=0, shift=0;

    regVal = mChannelHwRead(channel,regAddr, cAtModulePdh);
    FbitTypeToMaskShift(self, type, &mask, &shift);
    if (mask==cInvalidUint32)
    	return cAtError;

	mFieldIns(&regVal, mask, shift, en?1:0);
    mChannelHwWrite(channel, regAddr, regVal, cAtModulePdh);
    return cAtOk;
    }


static eBool De1FbitTypeGet(AtAttController self, uint32 type)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    AtChannel channel = AtAttControllerChannelGet(self);
    uint32   regAddr = mMethodsGet(controller)->RealAddress(controller, cDs1Reg_upen_fbit_sta);
    uint32   regVal=0, mask=0, shift=0;
    uint8    value;

    regVal = mChannelHwRead(channel,regAddr, cAtModulePdh);
	FbitTypeToMaskShift(self, type, &mask, &shift);
	if (mask==cInvalidUint32)
		return cAtFalse;

    mFieldGet(regVal, mask, shift, uint8, &value);
    AtUnused(self);
    return value?cAtTrue:cAtFalse;
    }

static ThaCdrController CdrControllerGet(ThaAttController self)
    {
    ThaPdhDe1 de1 = (ThaPdhDe1)AtAttControllerChannelGet((AtAttController)self);
    return ThaPdhDe1CdrControllerGet(de1);
    }

static uint32 FrequenceOffsetBase(ThaAttController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtAttControllerChannelGet((AtAttController)self);
    return AtPdhDe1IsE1(de1) ? cBaseNcoValE1 : cBaseNcoValDs1;
    }

static uint32 FrequenceOffsetStep(ThaAttController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtAttControllerChannelGet((AtAttController)self);
    return AtPdhDe1IsE1(de1) ? cNcoValPpbOffsetE1 : cNcoValPpbOffsetDS1;
    }

static uint32 NbitPerFrame(ThaAttController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtAttControllerChannelGet((AtAttController)self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de1);
    if (AtPdhDe1IsE1(de1))
        {
        switch (frameType)
            {
            case cAtPdhE1Frm:
                return 256; /* */
            case cAtPdhE1MFCrc:
                return 256*16; /*  */
            default:
                return 3400;
            }
        return 256;
        }
    else
    	{
		switch (frameType)
			{
		    case cAtPdhJ1FrmEsf:
			case cAtPdhDs1FrmEsf:
				return 193*24; /* */
			case cAtPdhDs1FrmSf:
			case cAtPdhJ1FrmSf:
				return 193*12; /*  */
			default:
				return 193;
			}
		return 193; /* 1 + 192 */
    	}
    }

static uint32 ChannelSpeed(ThaAttController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtAttControllerChannelGet((AtAttController)self);
    if (AtPdhDe1IsE1(de1))
        {
        return 2048;
        }
    return 1544;
    }

static uint8 Sw2HwSignalingMode(uint32 sigMode)
    {
    switch (sigMode)
            {
            case cAtAttSignalingModeSequence : return 1;
            case cAtAttSignalingModeFixedAB    : return 2;
            case cAtAttSignalingModeFixedABCD  : return 2;
            case cAtAttSignalingModePrbs    : return 3;
            default :
                return cInvalidUint8;
            }
    }
static eAtAttSignalingMode Hw2SwSignalingMode(uint8 value, eAtAttSignalingMode swMode)
    {
    switch (value)
        {
        case 1 : return cAtAttSignalingModeSequence;
        case 2 : return swMode;
            /*  cAtAttSignalingModeFixedAB
                cAtAttSignalingModeFixedABCD */
        case 3 : return cAtAttSignalingModePrbs;
        default:
            return cAtAttSignalingModeInvalid;
        }
    }

static uint32 SignalingOffset(Tha6A210031PdhDe1AttController self, AtPdhChannel channel, uint8 tsId)
    {
    uint8 slice, hwSts;
    uint8 hwDe2Id, hwDe1Id;
    ThaModulePdh pdhModule = (ThaModulePdh)AtChannelModuleGet((AtChannel)channel);
    AtSdhChannel vc = (AtSdhChannel)AtPdhChannelVcInternalGet(channel);
    AtUnused(self);
    if (vc != NULL)
        {
        AtSdhChannel tu1x = AtSdhChannelParentChannelGet(vc);
        AtSdhChannel tug2 = AtSdhChannelParentChannelGet(tu1x);
        uint32 flatIdInSts = 0;
        if  (AtPdhDe1IsE1((AtPdhDe1)channel))
            flatIdInSts = AtChannelIdGet((AtChannel)tug2) * 96UL + AtChannelIdGet((AtChannel)tu1x) * 32UL;
        else
            flatIdInSts = AtChannelIdGet((AtChannel)tug2) * 96UL + AtChannelIdGet((AtChannel)tu1x) * 24UL;
        ThaPdhChannelHwIdGet(channel, cAtModulePdh, &slice, &hwSts);
        return ThaModulePdhSliceBase(pdhModule, slice) + hwSts * 672UL + flatIdInSts + tsId;
        }

    ThaPdhDe2De1HwIdGet((ThaPdhDe1)channel, &slice, &hwSts, &hwDe2Id, &hwDe1Id, cAtModulePdh);
    if  (AtPdhDe1IsE1((AtPdhDe1)channel))
        return ThaModulePdhSliceBase(pdhModule, slice) + hwSts * 672UL + hwDe2Id * 96UL + hwDe1Id * 32UL + tsId;
    return ThaModulePdhSliceBase(pdhModule, slice) + hwSts * 672UL + hwDe2Id * 96UL + hwDe1Id * 24UL + tsId;
    }

static eAtRet TxSignalingPatternSet(AtAttController self, uint8 tsId, eAtAttSignalingMode sigMode, uint8 threshold, uint8 sigPattern)
    {
    uint8 hwSigPattern = 0;
    uint32 regAddrTx, regVal;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet(self);

    if (    (AtPdhDe1IsE1((AtPdhDe1)channel)) &&
            ((tsId == 16)|| (tsId == 0))
        )
        return cAtAttSignalingModeInvalid;

    if (sigMode == cAtAttSignalingModeFixedAB||sigMode == cAtAttSignalingModeFixedABCD)
        hwSigPattern =  sigPattern;

    /* Tx signaling prbs */
    regAddrTx = cAf6Reg_dej1_tx_framer_sign_gen_ctrl + mMethodsGet(mThis(self))->SignalingOffset(mThis(self), channel, tsId);

    regVal = mChannelHwRead(channel, regAddrTx, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigGenMd_, Sw2HwSignalingMode(sigMode));
    if (sigMode == cAtAttSignalingModeFixedAB)
        mRegFieldSet(regVal, cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigFixPatAB_, hwSigPattern);
    else
        mRegFieldSet(regVal, cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigFixPat_, hwSigPattern);
    mRegFieldSet(regVal, cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigRptThres_, threshold);

    mChannelHwWrite(channel, regAddrTx, regVal, cAtModuleSdh);
    mThis(self)->txSigMode = sigMode;
    return cAtOk;
    }

static eAtRet RxExpectedSignalingPatternSet(AtAttController self, uint8 tsId, eAtAttSignalingMode sigMode, uint8 threshold, uint8 sigPattern)
    {
    uint8 hwSigPattern = 0;
    uint32 regAddrRx, regVal;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet(self);

    if (    (AtPdhDe1IsE1((AtPdhDe1)channel)) &&
            ((tsId == 16)|| (tsId == 0))
        )
        return cAtAttSignalingModeInvalid;

    if (sigMode == cAtAttSignalingModeFixedAB||sigMode == cAtAttSignalingModeFixedABCD)
        hwSigPattern =  sigPattern;

    /* Tx signaling prbs */
    regAddrRx = cAf6Reg_dej1_tx_framer_sign_mon_ctrl + mMethodsGet(mThis(self))->SignalingOffset(mThis(self),channel, tsId);
    regVal = mChannelHwRead(channel, regAddrRx, cAtModuleSdh);
    mRegFieldSet(regVal, cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigGenMd_, Sw2HwSignalingMode(sigMode));
    if (sigMode == cAtAttSignalingModeFixedAB)
        mRegFieldSet(regVal, cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigFixPatAB_, hwSigPattern);
    else
        mRegFieldSet(regVal, cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigFixPat_, hwSigPattern);
    mRegFieldSet(regVal, cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigRptThres_, threshold);
    mChannelHwWrite(channel, regAddrRx, regVal, cAtModuleSdh);
    mThis(self)->rxSigMode = sigMode;
    return cAtOk;
    }

static eAtAttSignalingMode TxSignalingPatternGet(AtAttController self, uint8 tsId, uint8 *sigPattern, uint8 *thresold)
    {
    uint32 regAddr, regVal;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet(self);
    AtUnused(sigPattern);

    if (    (AtPdhDe1IsE1((AtPdhDe1)channel)) &&
            ((tsId == 16)|| (tsId == 0))
       )
        return cAtAttSignalingModeInvalid;

    regAddr = cAf6Reg_dej1_tx_framer_sign_gen_ctrl + mMethodsGet(mThis(self))->SignalingOffset(mThis(self), channel, tsId);
    regVal = mChannelHwRead(channel, regAddr, cAtModuleSdh);
    if (mThis(self)->txSigMode == cAtAttSignalingModeFixedAB)
        *sigPattern = (uint8)mRegField(regVal,cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigFixPatAB_);
    else
        *sigPattern = (uint8)mRegField(regVal,cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigFixPat_);
    *thresold = (uint8)mRegField(regVal,cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigRptThres_);

    return Hw2SwSignalingMode(mRegField(regVal,cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigGenMd_), mThis(self)->txSigMode);
    }

static eAtAttSignalingMode RxExpectedSignalingPatternGet(AtAttController self, uint8 tsId, uint8 *sigPattern, uint8 *thresold)
    {
    uint32 regAddr, regVal;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet(self);
    AtUnused(sigPattern);

    if (    (AtPdhDe1IsE1((AtPdhDe1)channel)) &&
            ((tsId == 16)|| (tsId == 0))
       )
        return cAtAttSignalingModeInvalid;

    regAddr = cAf6Reg_dej1_tx_framer_sign_mon_ctrl + mMethodsGet(mThis(self))->SignalingOffset(mThis(self),channel, tsId);
    regVal = mChannelHwRead(channel, regAddr, cAtModuleSdh);
    if (mThis(self)->rxSigMode == cAtAttSignalingModeFixedAB)
        *sigPattern = (uint8)mRegField(regVal,cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigFixPatAB_);
    else
        *sigPattern = (uint8)mRegField(regVal,cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigFixPat_);
    *thresold = (uint8)mRegField(regVal,cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigRptThres_);

    return Hw2SwSignalingMode(mRegField(regVal,cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigGenMd_), mThis(self)->rxSigMode);
    }
static uint8 ConvertToRealTimeSlot(AtAttController self, uint8 tsId)
    {
    AtChannel channel = AtAttControllerChannelGet(self);
    uint8 realTsId;
    if (AtPdhDe1IsE1((AtPdhDe1)channel))
            {
            if (tsId<16)
                realTsId = (uint8)(tsId  - 1);
            else
                realTsId = (uint8)(tsId  - 2);
            }
        else
            {
            realTsId = (uint8)(tsId);
            }
    return realTsId;
    }
static eAtRet TxSignalingEnable(AtAttController self, uint8 tsId, eBool enable)
    {
    uint32 regAddrTx, regVal, BypassVal;
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    AtChannel channel = AtAttControllerChannelGet(self);
    uint32 mask = cBit0;
    uint8 realTsId = ConvertToRealTimeSlot(self, tsId);

    if (    (AtPdhDe1IsE1((AtPdhDe1)channel)) &&
            ((tsId == 16)|| (tsId == 0))
       )
        return cAtAttSignalingModeInvalid;

    regAddrTx = mMethodsGet(controller)->RealAddress(controller, cAf6Reg_dej1_tx_framer_sign_insertion_ctrl);
    regVal = mChannelHwRead(channel, regAddrTx, cAtModuleSdh);
    BypassVal = mRegField(regVal, cAf6_dej1_rx_framer_sign_insertion_ctrl_RxDE1SigBypass_);
    mRegFieldSet(regVal, cAf6_dej1_rx_framer_sign_insertion_ctrl_RxDE1SigBypass_, enable ? (BypassVal & (~(mask << realTsId))) : (BypassVal | (mask << realTsId)));
    BypassVal = mRegField(regVal, cAf6_dej1_rx_framer_sign_insertion_ctrl_RxDE1SigBypass_);
    mRegFieldSet(regVal, cAf6_dej1_rx_framer_sign_insertion_ctrl_RxDE1SigMfrmEn_, (BypassVal ^ cBit29_0) ? 0x1 : 0x0);
    mChannelHwWrite(channel, regAddrTx, regVal, cAtModuleSdh);
    return cAtFalse;
    }

static eAtRet RxSignalingEnable(AtAttController self, uint8 tsId, eBool enable)
    {
    uint32 regAddrRx, regVal, BypassVal;
    eAtRet ret = cAtOk;
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    AtChannel channel = AtAttControllerChannelGet(self);
    uint32 mask = cBit0, tsIdSw;
    uint8 realTsId=ConvertToRealTimeSlot(self, tsId);

    if (    (AtPdhDe1IsE1((AtPdhDe1)channel)) &&
            ((tsId == 16)|| (tsId == 0))
       )
        return cAtAttSignalingModeInvalid;

    regAddrRx = mMethodsGet(controller)->RealAddress(controller, cAf6Reg_dej1_rx_framer_sign_insertion_ctrl);

    regVal = mChannelHwRead(channel, regAddrRx, cAtModuleSdh);
    BypassVal = mRegField(regVal, cAf6_dej1_rx_framer_sign_insertion_ctrl_RxDE1SigBypass_);
    mRegFieldSet(regVal, cAf6_dej1_rx_framer_sign_insertion_ctrl_RxDE1SigBypass_, enable ? (BypassVal & (~(mask << realTsId))) : (BypassVal | (mask << realTsId)));
    BypassVal = mRegField(regVal, cAf6_dej1_rx_framer_sign_insertion_ctrl_RxDE1SigBypass_);
    mRegFieldSet(regVal, cAf6_dej1_rx_framer_sign_insertion_ctrl_RxDE1SigMfrmEn_, (BypassVal ^ cBit29_0) ? 0x1 : 0x0);

    mChannelHwWrite(channel, regAddrRx, regVal, cAtModuleSdh);

    /* Reconfigure E1 Ds0Mask */
    tsIdSw = AtPdhDe1SignalingDs0MaskGet((AtPdhDe1)channel);
    mFieldIns(&tsIdSw, cBit0 << tsId, tsId, enable ? 1 : 0);
    ret = AtPdhDe1SignalingDs0MaskSet((AtPdhDe1)channel, tsIdSw);
    if (ret != cAtOk)
        return ret;

    if ((regVal == cBit29_0) || (tsIdSw == 0))
        ThaPdhDe1DbSignalingEnabled((ThaPdhDe1)channel, cAtFalse);
    else
        ThaPdhDe1DbSignalingEnabled((ThaPdhDe1)channel, cAtTrue);

    return cAtFalse;
    }

static eBool TxSignalingIsEnabled(AtAttController self, uint8 tsId)
    {
    uint32 regAddr, regVal;
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    AtChannel channel = AtAttControllerChannelGet(self);
    uint32 signalingEnMask,signalingEnShift;
    uint8 realTsId=ConvertToRealTimeSlot(self, tsId);
    uint8 hwEnVal=0;
    if (    (AtPdhDe1IsE1((AtPdhDe1)channel)) &&
            ((tsId == 16)|| (tsId == 0))
       )
        return cAtAttSignalingModeInvalid;

    signalingEnMask = cBit0 << realTsId;
    signalingEnShift = realTsId;

    regAddr = mMethodsGet(controller)->RealAddress(controller, cAf6Reg_dej1_tx_framer_sign_insertion_ctrl);
    regVal = mChannelHwRead(channel, regAddr, cAtModuleSdh);
    hwEnVal = (uint8)mRegField(regVal, signalingEn);

    return hwEnVal? cAtFalse: cAtTrue;
    }

static eBool RxSignalingIsEnabled(AtAttController self, uint8 tsId)
    {
    uint32 regAddr, regVal;
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    AtChannel channel = AtAttControllerChannelGet(self);
    uint32 signalingEnMask,signalingEnShift;
    uint8 realTsId=ConvertToRealTimeSlot(self, tsId);
    uint8 hwEnVal=0;
    if (    (AtPdhDe1IsE1((AtPdhDe1)channel)) &&
            ((tsId == 16)|| (tsId == 0))
       )
        return cAtAttSignalingModeInvalid;

    signalingEnMask = cBit0 << realTsId;
    signalingEnShift = realTsId;

    regAddr = mMethodsGet(controller)->RealAddress(controller, cAf6Reg_dej1_rx_framer_sign_insertion_ctrl);
    regVal = mChannelHwRead(channel, regAddr, cAtModuleSdh);
    hwEnVal = (uint8)mRegField(regVal, signalingEn);

    return hwEnVal? cAtFalse: cAtTrue;
    }

/*static eBool IsDs1Sf(AtAttController self)
    {
    AtPdhDe1 de1 = (AtPdhDe1)AtAttControllerChannelGet((AtAttController)self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)de1);
    if  (frameType == cAtPdhDs1FrmSf)
        return cAtTrue;
    return cAtFalse;
    }*/

static eAtRet TxSignalingDump(AtAttController self, uint8 tsId, uint8 *value, uint8 length)
    {
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet(self);

    if (length==1)
        {
        uint32 regAddr = cAf6Reg_dej1_tx_framer_sign_mon_sta + mMethodsGet(mThis(self))->SignalingOffset(mThis(self),channel, tsId);
        uint32 regVal = mChannelHwRead(channel, regAddr, cAtModuleSdh);
        value[0] = (uint8)mRegField(regVal,cAf6_dej1_tx_framer_sign_mon_sta_RxPattern_);

        }
    return cAtOk;
    }

static eAtRet RxSignalingDump(AtAttController self, uint8 tsId, uint8 *value, uint8 length)
    {
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet(self);

    if (length==1)
        {
        uint32 regAddr = cAf6Reg_dej1_tx_framer_sign_mon_sta + mMethodsGet(mThis(self))->SignalingOffset(mThis(self),channel, tsId);
        uint32 regVal = mChannelHwRead(channel, regAddr, cAtModuleSdh);
        value[0] = (uint8)mRegField(regVal,cAf6_dej1_tx_framer_sign_mon_sta_RxPattern_);
        }
    return cAtOk;
    }

static eAtRet TxSignalingDumpDebug(AtAttController self, uint8 tsId, uint8 *value, uint8 length)
    {
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet(self);
    uint16 frameType = AtPdhChannelFrameTypeGet(channel);
    uint32 regVal = 0;
    uint32 regAddr = 0;
    uint32 i = 0, adddress_val;
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    uint8 realTsId=tsId/*ConvertToRealTimeSlot(self, tsId)*/;

    AtUnused(tsId);
    regAddr = DumpRealAddress(controller, cAf6Reg_ds0_tx_sign_glb_cfg);
    regVal = mChannelHwRead(channel, regAddr, cAtModuleSdh);

    adddress_val = mMethodsGet(mThis(self))->SignalingDumpRealAddressVal(mThis(self), realTsId);
    mFieldIns(&regVal, cAf6_ds0_tx_sign_glb_cfg_Ds0TxSignDumpBit_Mask, cAf6_ds0_tx_sign_glb_cfg_Ds0TxSignDumpBit_Shift, 0);
    mFieldIns(&regVal, cAf6_ds0_tx_sign_glb_cfg_Ds0TxSigIdSel_Mask, cAf6_ds0_tx_sign_glb_cfg_Ds0TxSigIdSel_Shift, adddress_val);
    mChannelHwWrite(channel, regAddr, regVal, cAtModuleSdh);

    mFieldIns(&regVal, cAf6_ds0_tx_sign_glb_cfg_Ds0TxSignDumpBit_Mask, cAf6_ds0_tx_sign_glb_cfg_Ds0TxSignDumpBit_Shift, 1);
    mChannelHwWrite(channel, regAddr, regVal, cAtModuleSdh);

    AtOsalUSleep(96000); /* Sleep 96ms */
    AtOsalMemInit(value, 0, length* sizeof(uint8));
    for (i=0; i<length; i++)
        {
        if (i < 32)
            {
            regVal = mChannelHwRead(channel, DumpRealAddress(controller,cAf6Reg_ds0_tx_sign_dump_pat + i), cAtModuleSdh);
            value[i] = (uint8)regVal;
            if (frameType == cAtPdhDs1FrmSf)
                value[i] = (uint8)(regVal >> 2);
            }
        }
    return cAtOk;
    }

static eAtRet RxSignalingDumpDebug(AtAttController self, uint8 tsId, uint8 *value, uint8 length)
    {
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    uint32 regVal = 0, adddress_val;
    uint32 regAddr = 0;
    uint32 i = 0;
    uint8 realTsId=tsId/*ConvertToRealTimeSlot(self, tsId)*/;

    AtUnused(tsId);
    regAddr = DumpRealAddress(controller, cAf6Reg_ds0_rx_sign_glb_cfg);
    regVal = mChannelHwRead(channel, regAddr, cAtModuleSdh);
    adddress_val = mMethodsGet(mThis(self))->SignalingDumpRealAddressVal(mThis(self), realTsId);

    mFieldIns(&regVal, cAf6_ds0_rx_sign_glb_cfg_Ds0RxSignSelDumpBit_Mask, cAf6_ds0_rx_sign_glb_cfg_Ds0RxSignSelDumpBit_Shift, AtPdhDe1IsE1((AtPdhDe1)channel)?2:0);
    mFieldIns(&regVal, cAf6_ds0_rx_sign_glb_cfg_Ds0RxSignDumpBit_Mask,    cAf6_ds0_rx_sign_glb_cfg_Ds0RxSignDumpBit_Shift,    0);
    mFieldIns(&regVal, cAf6_ds0_rx_sign_glb_cfg_Ds0RxSigIdSel_Mask,       cAf6_ds0_rx_sign_glb_cfg_Ds0RxSigIdSel_Shift,       adddress_val);
    mChannelHwWrite(channel, regAddr, regVal, cAtModuleSdh);

    mFieldIns(&regVal, cAf6_ds0_rx_sign_glb_cfg_Ds0RxSignDumpBit_Mask, cAf6_ds0_rx_sign_glb_cfg_Ds0RxSignDumpBit_Shift, 1);
    mChannelHwWrite(channel, regAddr, regVal, cAtModuleSdh);

    AtOsalUSleep(96000); /* Sleep 96ms */
    AtOsalMemInit(value, 0, length* sizeof(uint8));
    for (i=0; i<length; i++)
        {
        if (i < 32)
            {
            regVal = mChannelHwRead(channel, DumpRealAddress(controller,cAf6Reg_ds0_rx_sign_dump_pat + i), cAtModuleSdh);
            value[i] = (uint8)regVal;
            }
        }
    return cAtOk;
    }

static eBool SignalingLopAlarmGet(AtAttController self, uint8 tsId)
    {
    uint32 regAddr, regVal;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet(self);

    regAddr = cAf6Reg_ds0_rx_sign_per_chn_curr_stat + mMethodsGet(mThis(self))->SignalingOffset(mThis(self),channel, tsId);
    regVal = mChannelHwRead(channel, regAddr, cAtModuleSdh);

    return mBinToBool(mRegField(regVal, cAf6_ds0_rx_sign_per_chn_curr_stat_Ds0SignLopCurrSta_));
    }

static uint32 SignalingLopStickyGet(AtAttController self, uint8 tsId, eBool r2c)
    {
    uint32 regAddr, regVal;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet(self);
    uint32 ret = 0;

    regAddr = cAf6Reg_ds0_rx_sign_per_chn_intr_stat + mMethodsGet(mThis(self))->SignalingOffset(mThis(self),channel, tsId);
    regVal = mChannelHwRead(channel, regAddr, cAtModuleSdh);
    if (mRegField(regVal, cAf6_ds0_rx_sign_per_chn_curr_stat_Ds0SignLopCurrSta_))
        ret |= cAtSignalingLop;
    if (mRegField(regVal, cAf6_ds0_rx_sign_per_chn_curr_stat_Ds0SignRpeCurrSta_))
        ret |= cAtSignalingRpe;
    if (r2c)
        mChannelHwWrite(channel, regAddr, regVal, cAtModuleSdh);
    return ret;
    }

static eBool SignalingFramerStickyGet(AtAttController self, uint8 tsId, eBool r2c)
    {
    uint32 regAddr = 0, regVal=0, address_val=0;
    AtPdhChannel channel = (AtPdhChannel)AtAttControllerChannelGet(self);
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    uint8 realTsId=/*ConvertToRealTimeSlot(self, tsId)*/tsId;
    eBool ret = cAtFalse;

    regAddr = DumpRealAddress(controller, cAf6Reg_ds0_rx_sign_glb_cfg);
    regVal = mChannelHwRead(channel, regAddr, cAtModuleSdh);
    address_val = mMethodsGet(mThis(self))->SignalingDumpRealAddressVal(mThis(self), realTsId);
    mFieldIns(&regVal, cAf6_ds0_rx_sign_glb_cfg_Ds0RxSignSelDumpBit_Mask, cAf6_ds0_rx_sign_glb_cfg_Ds0RxSignSelDumpBit_Shift, 0);
    mFieldIns(&regVal, cAf6_ds0_rx_sign_glb_cfg_Ds0RxSigIdSel_Mask, cAf6_ds0_rx_sign_glb_cfg_Ds0RxSigIdSel_Shift, address_val);
    mChannelHwWrite(channel, regAddr, regVal, cAtModuleSdh);

    regAddr = DumpRealAddress(controller, cAf6Reg_ds0_tx_sign_stk);
    regVal = mChannelHwRead(channel, regAddr, cAtModuleSdh);
    ret = mBinToBool(mRegField(regVal, cAf6_ds0_tx_sign_stk_Ds0TxSigStk_));
    if (r2c)
        mChannelHwWrite(channel, regAddr, regVal, cAtModuleSdh);

    return ret;
    }
static eAtRet ForceAlarm(AtAttController self, uint32 alarmType)
    {
    if (alarmType==cAtPdhDe1AlarmSigLof||
        alarmType==cAtPdhDe1AlarmSigRai)
        {
        AtAttControllerDe1ForceFbitTypeSet(self, cAtAttPdhDe1FbitTypeFas, cAtFalse);
        AtAttControllerDe1ForceFbitTypeSet(self, cAtAttPdhDe1FbitTypeNfas, cAtFalse);
        AtAttControllerDe1ForceFbitTypeSet(self, cAtAttPdhDe1FbitTypeMfas, cAtTrue);
        }
    else
        {
        AtAttControllerDe1ForceFbitTypeSet(self, cAtAttPdhDe1FbitTypeMfas, cAtFalse);
        AtAttControllerDe1ForceFbitTypeSet(self, cAtAttPdhDe1FbitTypeFas, cAtTrue);
        AtAttControllerDe1ForceFbitTypeSet(self, cAtAttPdhDe1FbitTypeNfas, cAtTrue);
        }
    return m_AtAttControllerMethods->ForceAlarm(self, alarmType);
    }

static eAtRet UnForceAlarm(AtAttController self, uint32 alarmType)
    {
    eAtRet ret = m_AtAttControllerMethods->UnForceAlarm(self, alarmType);
    if (alarmType==cAtPdhDe1AlarmSigLof||
        alarmType==cAtPdhDe1AlarmSigRai)
        {
        AtAttControllerDe1ForceFbitTypeSet(self, cAtAttPdhDe1FbitTypeMfas, cAtFalse);
        }
    else {
        AtAttControllerDe1ForceFbitTypeSet(self, cAtAttPdhDe1FbitTypeFas, cAtFalse);
        AtAttControllerDe1ForceFbitTypeSet(self, cAtAttPdhDe1FbitTypeNfas, cAtFalse);
    }
    return ret;
    }
static void OverrideAtAttController(AtAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtAttControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttControllerOverride, mMethodsGet(self), sizeof(m_AtAttControllerOverride));

        mMethodOverride(m_AtAttControllerOverride, ForceAlarm);
        mMethodOverride(m_AtAttControllerOverride, UnForceAlarm);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorStatusGet);
        mMethodOverride(m_AtAttControllerOverride, AlarmForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, ErrorForceIsSupported);
        mMethodOverride(m_AtAttControllerOverride, De1FbitTypeSet);
        mMethodOverride(m_AtAttControllerOverride, De1FbitTypeGet);
        mMethodOverride(m_AtAttControllerOverride, De1CrcGapSet);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorRate);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorCrcMaskSet);
        mMethodOverride(m_AtAttControllerOverride, ForceErrorCrcMask2Set);
        mMethodOverride(m_AtAttControllerOverride, TxSignalingPatternSet);
        mMethodOverride(m_AtAttControllerOverride, RxExpectedSignalingPatternSet);
        mMethodOverride(m_AtAttControllerOverride, TxSignalingPatternGet);
        mMethodOverride(m_AtAttControllerOverride, RxExpectedSignalingPatternGet);
        mMethodOverride(m_AtAttControllerOverride, TxSignalingEnable);
        mMethodOverride(m_AtAttControllerOverride, RxSignalingEnable);
        mMethodOverride(m_AtAttControllerOverride, SignalingLopAlarmGet);
        mMethodOverride(m_AtAttControllerOverride, SignalingLopStickyGet);
        mMethodOverride(m_AtAttControllerOverride, SignalingFramerStickyGet);
        mMethodOverride(m_AtAttControllerOverride, TxSignalingIsEnabled);
        mMethodOverride(m_AtAttControllerOverride, RxSignalingIsEnabled);
        mMethodOverride(m_AtAttControllerOverride, TxSignalingDump);
        mMethodOverride(m_AtAttControllerOverride, RxSignalingDump);
        mMethodOverride(m_AtAttControllerOverride, TxSignalingDumpDebug);
        mMethodOverride(m_AtAttControllerOverride, RxSignalingDumpDebug);
        }

    mMethodsSet(self, &m_AtAttControllerOverride);
    }

static void OverrideThaAttController(ThaAttController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaAttControllerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttControllerOverride, m_ThaAttControllerMethods, sizeof(m_ThaAttControllerOverride));

        mMethodOverride(m_ThaAttControllerOverride, CdrControllerGet);
        mMethodOverride(m_ThaAttControllerOverride, FrequenceOffsetBase);
        mMethodOverride(m_ThaAttControllerOverride, FrequenceOffsetStep);
        mMethodOverride(m_ThaAttControllerOverride, ChannelSpeed);
        mMethodOverride(m_ThaAttControllerOverride, NbitPerFrame);
        mMethodOverride(m_ThaAttControllerOverride, HwForceErrorRate);
        mMethodOverride(m_ThaAttControllerOverride, MaxStep);
        mMethodOverride(m_ThaAttControllerOverride, IsE1);
        mMethodOverride(m_ThaAttControllerOverride, StartVersionSupportStep16bit);
        mMethodOverride(m_ThaAttControllerOverride, IsStep16bit);
        }

    mMethodsSet(self, &m_ThaAttControllerOverride);
    }

static void OverrideTha6A210031PdhDe3AttController(AtAttController self)
    {
    Tha6A210031PdhDe3AttController controller = (Tha6A210031PdhDe3AttController)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha6A210031PdhDe3AttControllerOverride, mMethodsGet(controller), sizeof(m_Tha6A210031PdhDe3AttControllerOverride));

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromErrorType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RealAddress);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ErrorTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ErrorIsFromAlarm);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumErrorTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, InitErrorDefault);

        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MinStep);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MaxErrorNum_1s);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, MaxErrorNum);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwStepMaskHo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwStepMaskLo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PositionMaskConfigurationMaskHo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, PositionMaskConfigurationMaskLo);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, DataMaskConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumberOfErrorsMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, HwForceModeConfigurationMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromErrorType2nd);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromErrorType2nd);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, InitAlarmDefault);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIsFromError);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, RegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StatusRegAddressFromAlarmType);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmTypeIndex);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmIndexToString);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, NumAlarmTypes);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, ForceOffset);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, SpecificAlarmConfig);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, CheckAlarmNumEvent);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, StartVersionSupportAlarmForceV2);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, DefaultDs1DataMask);
        mMethodOverride(m_Tha6A210031PdhDe3AttControllerOverride, AlarmUnitSet);
        }

    mMethodsSet(controller, &m_Tha6A210031PdhDe3AttControllerOverride);
    }

static void MethodsInit(Tha6A210031PdhDe1AttController self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        /* Setup methods */
        mMethodOverride(m_methods, IsMfas);
        mMethodOverride(m_methods, StartVersionSupportMfas);
        mMethodOverride(m_methods, StartVersionSupportTwoCRCForce);
        mMethodOverride(m_methods, RegAddressTxMaskCrcDs1Esf);
        mMethodOverride(m_methods, TxMaskCrcDs1EsfMaskBig);
        mMethodOverride(m_methods, TxMaskCrcDs1EsfMaskMini);
        mMethodOverride(m_methods, SignalingOffset);
        mMethodOverride(m_methods, SignalingDumpRealAddressVal);
        }

    mMethodsSet(self, &m_methods);
    }
static void Override(AtAttController self)
    {
    OverrideAtAttController(self);
    OverrideThaAttController((ThaAttController)self);
    OverrideTha6A210031PdhDe3AttController(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031PdhDe1AttController);
    }

AtAttController Tha6A210031PdhDe1AttControllerObjectInit(AtAttController self, AtChannel channel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6A210031PdhDe3AttControllerObjectInit(self, channel) == NULL)
        return NULL;

    /* Setup class */
    MethodsInit(mThis(self));
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtAttController Tha6A210031PdhDe1AttControllerNew(AtChannel de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttController controller = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return Tha6A210031PdhDe1AttControllerObjectInit(controller, de3);
    }

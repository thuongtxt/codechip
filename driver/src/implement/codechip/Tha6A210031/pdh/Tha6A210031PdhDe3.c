/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A210031PdhDe3.c
 *
 * Created Date: Dec 14, 2015
 *
 * Description : DE3 implementation
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/pdh/Tha60210031ModulePdh.h"
#include "../../Tha60210031/pdh/Tha60210031PdhDe3Internal.h"
#include "Tha6A210031ModulePdh.h"
#include "Tha6A210031PdhAttControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha6A210031PdhDe3*)self)

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210031PdhDe3
    {
    tTha60210031PdhDe3 super;
    AtAttController attController;
    }tTha6A210031PdhDe3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtChannelMethods    m_AtChannelOverride;
static tAtPdhChannelMethods m_AtPdhChannelOverride;
static tThaPdhDe3Methods    m_ThaPdhDe3Override;

/* Save super implementation */
static const tAtChannelMethods  *m_AtChannelMethods    = NULL;
static const tAtPdhChannelMethods *m_AtPdhChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eAtRet TimingSet(AtChannel self, eAtTimingMode timingMode, AtChannel timingSource)
    {
    eAtRet ret = cAtOk;
    ret = m_AtChannelMethods->TimingSet(self, timingMode, timingSource);
    if (timingMode == cAtTimingModeFreeRunning||timingMode == cAtTimingModeSys)
        AtAttControllerFrequenceOffsetSet(AtChannelAttController(self), 0);
    return ret;
    }

static eAtRet Init(AtChannel self)
    {
    eAtRet ret = m_AtChannelMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return AtAttControllerInit(AtChannelAttController(self));
    }

static eAtRet De3FramingBindToPwSatop(ThaPdhDe3 self, AtPw pseudowire)
    {
    AtUnused(self);
    AtUnused(pseudowire);
    return cAtOk;
    }

static eAtRet HelperTxAlarmForce(AtChannel self, uint32 alarmTypes, eAtRet (*fForceAlarm)(AtAttController, uint32))
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i=0; i<32; i++)
        {
        uint32 alarmType = alarmTypes & (cBit0 << i);
        if (alarmType)
            {
            AtAttControllerForceAlarmModeSet(attController, alarmType, cAtAttForceAlarmModeContinuous);
            ret =  fForceAlarm(attController, alarmType);
            }
        }
    return ret;
    }

static eAtRet TxAlarmForce(AtChannel self, uint32 alarmType)
    {
    return HelperTxAlarmForce(self, alarmType, AtAttControllerForceAlarm);
    }

static eAtRet TxAlarmUnForce(AtChannel self, uint32 alarmType)
    {
    return HelperTxAlarmForce(self, alarmType, AtAttControllerUnForceAlarm);
    }

static uint32 TxForcibleAlarmsGet(AtChannel self)
    {
    AtUnused(self);
    return cAtPdhDe3AlarmLos|cAtPdhDe3AlarmAis|cAtPdhDe3AlarmRai|cAtPdhDe3AlarmLof;
    }

static eAtRet HelperTxErrorForce(AtChannel self, uint32 errorTypes, eBool force)
    {
    uint32 i;
    eAtRet ret = cAtOk;
    AtAttController attController = AtChannelAttController(self);
    for (i=0; i<32; i++)
        {
        uint32 errorType = errorTypes & (cBit0 << i);
        if (errorType)
            {
            eAtAttPdhDe3ErrorType attErrorType = errorTypes;
            /*if (errorType == cAtPdhDe3CounterRei) attErrorType  = cAtAttPdhDe3ErrorTypeReibit;
            if (errorType == cAtPdhDe3CounterFBit)attErrorType  = cAtAttPdhDe3ErrorTypeFbit;
            if (errorType == cAtPdhDe3CounterCPBit)attErrorType = cAtAttPdhDe3ErrorTypeCpbit;
            if (errorType == cAtPdhDe3CounterPBit)attErrorType  = cAtAttPdhDe3ErrorTypePbit;*/
            AtAttControllerForceErrorModeSet(attController, attErrorType, cAtAttForceErrorModeContinuous);
            if (force)
                ret =  AtAttControllerForceError(attController, attErrorType);
            else
                ret = AtAttControllerUnForceError(attController, attErrorType);
            }
        }
    return ret;
    }

static eAtRet TxErrorForce(AtChannel self, uint32 errorType)
    {
    return HelperTxErrorForce(self, errorType, cAtTrue);
    }
static eAtRet TxErrorUnForce(AtChannel self, uint32 errorType)
    {
    return HelperTxErrorForce(self, errorType, cAtFalse);
    }


static uint32 TxForcableErrorsGet(AtChannel self)
    {
    AtUnused(self);
    return cAtPdhDe3CounterRei|cAtPdhDe3CounterFBit|cAtPdhDe3CounterCPBit|cAtPdhDe3CounterPBit;
    }

static void OverrideAtChannel(AtPdhDe3 self)
    {
    AtChannel channel = (AtChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_AtChannelOverride, m_AtChannelMethods, sizeof(m_AtChannelOverride));

        mMethodOverride(m_AtChannelOverride, Init);
        mMethodOverride(m_AtChannelOverride, TxAlarmForce);
        mMethodOverride(m_AtChannelOverride, TxAlarmUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcibleAlarmsGet);
        mMethodOverride(m_AtChannelOverride, TxErrorForce);
        mMethodOverride(m_AtChannelOverride, TxErrorUnForce);
        mMethodOverride(m_AtChannelOverride, TxForcableErrorsGet);
        mMethodOverride(m_AtChannelOverride, TimingSet);
        }

    mMethodsSet(channel, &m_AtChannelOverride);
    }

static eBool IsFrameTypeUnChn(uint16 frameType)
    {
    switch (frameType)
        {
        case cAtPdhDs3Unfrm:
        case cAtPdhDs3FrmCbitUnChn:
        case cAtPdhE3Unfrm:
        case cAtPdhE3Frmg832:
        case cAtPdhE3FrmG751:
            return cAtTrue;
        default:
            return cAtFalse;
        }
    }

static eAtRet FrameTypeSet(AtPdhChannel self, uint16 frameType)
    {
    eAtRet ret = cAtOk;
    uint16 pre_frameType =m_AtPdhChannelMethods->FrameTypeGet(self);
    if ( pre_frameType != frameType)
        {
        if (IsFrameTypeUnChn(frameType) && IsFrameTypeUnChn(pre_frameType))
            {
            AtPw pw = AtChannelBoundPwGet((AtChannel)self);
            if (pw)
                {
                if (AtPwCircuitUnbind(pw)==cAtOk)
                    {
                    ret = m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
                    AtPwCircuitBind(pw, (AtChannel)self);
                    }
                return ret;
                }
            }
        ret = m_AtPdhChannelMethods->FrameTypeSet(self, frameType);
        }
    return ret;
    }

static void OverrideAtPdhChannel(AtPdhChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPdhChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPdhChannelOverride, m_AtPdhChannelMethods, sizeof(m_AtPdhChannelOverride));
        mMethodOverride(m_AtPdhChannelOverride, FrameTypeSet);
        }

    mMethodsSet(self, &m_AtPdhChannelOverride);
    }

static void OverrideThaPdhDe3(AtPdhDe3 self)
    {
    ThaPdhDe3 de3 = (ThaPdhDe3)self;
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPdhDe3Override, mMethodsGet(de3), sizeof(m_ThaPdhDe3Override));

        /* Setup methods */
        mMethodOverride(m_ThaPdhDe3Override, De3FramingBindToPwSatop);
        }

    mMethodsSet(de3, &m_ThaPdhDe3Override);
    }

static void Override(AtPdhDe3 self)
    {
    OverrideAtChannel(self);
    OverrideAtPdhChannel((AtPdhChannel)self);
    OverrideThaPdhDe3(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031PdhDe3);
    }

static AtPdhDe3 ObjectInit(AtPdhDe3 self, uint32 channelId, AtModulePdh module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Supper constructor */
    if (Tha60210031PdhDe3ObjectInit(self, channelId, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPdhDe3 Tha6A210031PdhDe3New(uint32 channelId, AtModulePdh module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPdhDe3 newDe3 = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDe3 == NULL)
        return NULL;

    /* construct it */
    return ObjectInit(newDe3, channelId, module);
    }


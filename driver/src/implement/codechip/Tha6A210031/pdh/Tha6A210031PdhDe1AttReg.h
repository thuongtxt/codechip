/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ATT
 * 
 * File        : Tha6A210031PdhDe1AttReg.h
 * 
 * Created Date: May 16, 2017
 *
 * Description : ATT register description header file
 * 
 * Notes       : None
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031PDHDE1ATTREG_H_
#define _THA6A210031PDHDE1ATTREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
#define cPdhForceOffset   0x700000
/*------------------------------------------------------------------------------
Reg Name   : Force F Bit Error Configuration
Reg Addr   : 0x00092000-0x923FF
Reg Formula: 0x00092000 +  32*de3id + 4*de2id + de1id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
This register is applicable to configure Force F Bit Error fore DS1/E1 Frame

------------------------------------------------------------------------------*/
#define cDs1Reg_upen_fbitcfg                                                                           0x00092000
#define cDs1Reg_upen_fbitcfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: cfgfbiterr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force F Bit Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_fbitcfg_cfgfbiterr_step_Mask                                                           cBit11_4
#define c_upen_fbitcfg_cfgfbiterr_step_Shift                                                                 4

/*--------------------------------------
BitField Name: cfgfbitmsk_pos
BitField Type: RW
BitField Desc: The Position Mask for  F Bit Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_fbitcfg_cfgfbitmsk_pos_Mask_01                                                        cBit31_20
#define c_upen_fbitcfg_cfgfbitmsk_pos_Shift_01                                                              20
#define c_upen_fbitcfg_cfgfbitmsk_pos_Mask_02                                                          cBit3_0
#define c_upen_fbitcfg_cfgfbitmsk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfgfbitmsk_dat
BitField Type: RW
BitField Desc: The Data Mask For  F Bit Congiguration
BitField Bits: [19]
--------------------------------------*/
#define c_upen_fbitcfg_cfgfbitmsk_dat_Mask                                                              cBit19
#define c_upen_fbitcfg_cfgfbitmsk_dat_Shift                                                                 19

/*--------------------------------------
BitField Name: cfgfbiterr_num
BitField Type: RW
BitField Desc: The Total F Bit Error Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_fbitcfg_cfgfbiterr_num_Mask                                                            cBit18_3
#define c_upen_fbitcfg_cfgfbiterr_num_Shift                                                                  3

/*--------------------------------------
BitField Name: cfg_frcfbitmod
BitField Type: RW
BitField Desc: Force F Bit Mode 000:N error� (N = 16bits),N in (Tms)� (N =
16bits, Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts <
256s) 11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_fbitcfg_cfg_frcfbitmod_Mask                                                             cBit2_0
#define c_upen_fbitcfg_cfg_frcfbitmod_Shift                                                                  0


#define c_upen_lof_cfg_cfg_msk_pos_Mask                                                              cBit31_16
#define c_upen_lof_cfg_cfg_msk_pos_Shift                                                                    16
#define c_upen_lof_cfg_cfg_sec_num_Mask                                                               cBit15_8
#define c_upen_lof_cfg_cfg_sec_num_Shift                                                                     8
#define c_upen_lof_cfg_cfg_evtin1s_Mask                                                                cBit7_4
#define c_upen_lof_cfg_cfg_evtin1s_Shift                                                                     4
#define c_upen_lof_cfg_cfg_frc_ena_Mask                                                                  cBit3
#define c_upen_lof_cfg_cfg_frc_ena_Shift                                                                     3
#define c_upen_lof_cfg_cfg_frc_mod_Mask                                                                cBit2_0
#define c_upen_lof_cfg_cfg_frc_mod_Shift                                                                     0



/*------------------------------------------------------------------------------
Reg Name   : Force F Bit Error Status
Reg Addr   : 0x00092400-0x927FF
Reg Formula: 0x00092400 +  32*de3id + 4*de2id + de1id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
This register is applicable to see the Force F Bit Error Status

------------------------------------------------------------------------------*/
#define cDs1Reg_upen_fbitsta                                                                           0x00092400
#define cDs1Reg_upen_fbitsta_WidthVal                                                                          32

/*--------------------------------------
BitField Name: stafbiterr_stepcnt
BitField Type: RW
BitField Desc: The Event F Bit Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_fbitsta_stafbiterr_stepcnt_Mask                                                       cBit29_22
#define c_upen_fbitsta_stafbiterr_stepcnt_Shift                                                             22

/*--------------------------------------
BitField Name: stafbitmsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask F Bit Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_fbitsta_stafbitmsk_poscnt_Mask                                                        cBit21_18
#define c_upen_fbitsta_stafbitmsk_poscnt_Shift                                                              18

/*--------------------------------------
BitField Name: stafbiterr_cnt
BitField Type: RW
BitField Desc: The Remain Force Error F Bit Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_fbitsta_stafbiterr_cnt_Mask                                                            cBit17_2
#define c_upen_fbitsta_stafbiterr_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: stafbitfrc_err
BitField Type: RW
BitField Desc: F Bit Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_fbitsta_stafbitfrc_err_Mask                                                               cBit1
#define c_upen_fbitsta_stafbitfrc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: stafbitosecenb
BitField Type: RW
BitField Desc: Force F bit in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_fbitsta_stafbitosecenb_Mask                                                               cBit0
#define c_upen_fbitsta_stafbitosecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force CRC Bit Error Configuration
Reg Addr   : 0x00092800-0x92BFF
Reg Formula: 0x00092800 +  32*de3id + 4*de2id + de1id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
This register is applicable to configure Force CRC Bit Error

------------------------------------------------------------------------------*/
#define cReg_upen_crc_cfg                                                                           0x00092800
#define cReg_upen_crc_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: cfgcrc_err_step
BitField Type: RW
BitField Desc: The Total Event Number to Force CRC Bit Error Congiguration
BitField Bits: [41:34]
--------------------------------------*/
#define c_upen_crc_cfg_cfgcrc_err_step_Mask                                                         cBit9_2
#define c_upen_crc_cfg_cfgcrc_err_step_Shift                                                              2

/*--------------------------------------
BitField Name: cfgcrc_msk_pos
BitField Type: RW
BitField Desc: The Position Mask for CRC Bit Congiguration
BitField Bits: [33:18]
--------------------------------------*/
#define c_upen_crc_cfg_cfgcrc_msk_pos_Mask_01                                                        cBit31_18
#define c_upen_crc_cfg_cfgcrc_msk_pos_Shift_01                                                              18
#define c_upen_crc_cfg_cfgcrc_msk_pos_Mask_02                                                       cBit1_0
#define c_upen_crc_cfg_cfgcrc_msk_pos_Shift_02                                                               0

/*--------------------------------------
BitField Name: cfgcrc_msk_dat
BitField Type: RW
BitField Desc: The Data Mask for CRC Bit Congiguration
BitField Bits: [17]
--------------------------------------*/
#define c_upen_crc_cfg_cfgcrc_msk_dat_Mask                                                              cBit17
#define c_upen_crc_cfg_cfgcrc_msk_dat_Shift                                                                 17

/*--------------------------------------
BitField Name: cfgcrc_err_num
BitField Type: RW
BitField Desc: The Total CRC Bit Error Number Congiguration
BitField Bits: [16:01]
--------------------------------------*/
#define c_upen_crc_cfg_cfgcrc_err_num_Mask                                                            cBit16_1
#define c_upen_crc_cfg_cfgcrc_err_num_Shift                                                                  1

/*--------------------------------------
BitField Name: cfgcrc_frc_1sen
BitField Type: RW
BitField Desc: Force CRC Bit in One second enable Configuration
BitField Bits: [0]
--------------------------------------*/
#define c_upen_crc_cfg_cfgcrc_frc_1sen_Mask                                                              cBit0
#define c_upen_crc_cfg_cfgcrc_frc_1sen_Shift                                                                 0

/*------------------------------------------------------------------------------
Reg Name   : Fbit Gap Configuration
Reg Addr   : 0x90008-90008
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure Fbit Gap

------------------------------------------------------------------------------*/
#define cReg_upen_fbitgap_cfg                                                                          0x90008

/*--------------------------------------
BitField Name: fbitgap
BitField Type: RW
BitField Desc: F bit Gap Configuration
BitField Bits: [31:0]
--------------------------------------*/
#define c_upen_fbitgap_cfg_fbitgap_Mask                                                               cBit31_0
#define c_upen_fbitgap_cfg_fbitgap_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : F Bit Counter Status
Reg Addr   : 0x97C00-97FFF
Reg Formula: 0x97C00 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This register is applicable to see the  F bit Counter Status

------------------------------------------------------------------------------*/
#define cDs1Reg_upen_fbit_sta                                                                             0x97C00

/*--------------------------------------
BitField Name: nfas_enb
BitField Type: RW
BitField Desc: Force NFAS Enable
BitField Bits: [31]
--------------------------------------*/
#define c_upen_fbit_sta_nfas_enb_Mask                                                                   cBit31
#define c_upen_fbit_sta_nfas_enb_Shift                                                                      31

/*--------------------------------------
BitField Name: fas_enb
BitField Type: RW
BitField Desc: Force FAS Enable
BitField Bits: [30]
--------------------------------------*/
#define c_upen_fbit_sta_fas_enb_Mask                                                                    cBit30
#define c_upen_fbit_sta_fas_enb_Shift                                                                       30

#define c_upen_fbit_sta_crcmfas_enb_Mask                                                                cBit29
#define c_upen_fbit_sta_crcmfas_enb_Shift                                                                   29

#define c_upen_fbit_sta_mfas_enb_Mask                                                                   cBit28
#define c_upen_fbit_sta_mfas_enb_Shift                                                                      28

/*--------------------------------------
BitField Name: fbitcntsta
BitField Type: RW
BitField Desc: F bit Counter Status
BitField Bits: [29:0]
--------------------------------------*/
#define c_upen_fbit_sta_fbitcntsta_Mask                                                               cBit29_0
#define c_upen_fbit_sta_fbitcntsta_Shift                                                                     0


/*------------------------------------------------------------------------------
Reg Name   : Force CRC Bit Error Status
Reg Addr   : 0x00092C00-0x92FFF
Reg Formula: 0x00092C00 +  32*de3id + 4*de2id + de1id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
This register is applicable to see the Force CRC Bit Error Status

------------------------------------------------------------------------------*/
#define cDs1Reg_upen_crc_sta                                                                           0x00092C00
#define cDs1Reg_upen_crc_sta_WidthVal                                                                          32

/*--------------------------------------
BitField Name: stacrc_err_stepcnt
BitField Type: RW
BitField Desc: The Event CRC Bit Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_crc_sta_stacrc_err_stepcnt_Mask                                                       cBit29_22
#define c_upen_crc_sta_stacrc_err_stepcnt_Shift                                                             22

/*--------------------------------------
BitField Name: stacrc_msk_poscnt
BitField Type: RW
BitField Desc: The Position Mask CRC Bit Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_crc_sta_stacrc_msk_poscnt_Mask                                                        cBit21_18
#define c_upen_crc_sta_stacrc_msk_poscnt_Shift                                                              18

/*--------------------------------------
BitField Name: stacrc_err_cnt
BitField Type: RW
BitField Desc: The Remain Force CRC Bit Error Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_crc_sta_stacrc_err_cnt_Mask                                                            cBit17_2
#define c_upen_crc_sta_stacrc_err_cnt_Shift                                                                  2

/*--------------------------------------
BitField Name: stacrc_frc_err
BitField Type: RW
BitField Desc: CRC Bit Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_crc_sta_stacrc_frc_err_Mask                                                               cBit1
#define c_upen_crc_sta_stacrc_frc_err_Shift                                                                  1

/*--------------------------------------
BitField Name: stacrc_osecenb
BitField Type: RW
BitField Desc: Force CRC Bit in One Second Enable Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_crc_sta_stacrc_osecenb_Mask                                                               cBit0
#define c_upen_crc_sta_stacrc_osecenb_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : CRC Gap Configuration
Reg Addr   : 0x90007-90007
Reg Formula: 
    Where  : 
Reg Desc   : 
This register is applicable to configure CRC Gap

------------------------------------------------------------------------------*/
#define cReg_upen_crcgap_cfg                                                                           0x90007

/*--------------------------------------
BitField Name: crcgap
BitField Type: RW
BitField Desc: CRC Gap Configuration
BitField Bits: [31:0]
--------------------------------------*/
#define c_upen_crcgap_cfg_crcgap_Mask                                                                 cBit31_0
#define c_upen_crcgap_cfg_crcgap_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : CRC Bit Counter Status
Reg Addr   : 0x97800 - 0x00797Bff
Reg Formula: 0x97800 +  32*de3id + 4*de2id + de1id
    Where  : 
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   : 
This register is applicable to see the  CRC bit Counter Status

------------------------------------------------------------------------------*/
#define cDs1Reg_upen_crc_gap                                                                              0x97800

/*--------------------------------------
BitField Name: crccntsta
BitField Type: RW
BitField Desc: CRC bit Counter Status
BitField Bits: [31:0]
--------------------------------------*/
#define c_upen_crc_gap_crccntsta_Mask                                                                 cBit31_0
#define c_upen_crc_gap_crccntsta_Shift                                                                       0


/*------------------------------------------------------------------------------
Reg Name   : Force RAI Error Configuration
Reg Addr   : 0x00093000-0x933FF
Reg Formula: 0x00093000 +  32*de3id + 4*de2id + de1id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
This register is applicable to configure Force RAI Error force DS1/E1 Frame

------------------------------------------------------------------------------*/
#define cReg_upen_rai_cfg                                                                           0x00093000
#define cReg_upen_rai_cfg_WidthVal                                                                          64

/*--------------------------------------
BitField Name: cfgraierr_step
BitField Type: RW
BitField Desc: The Total Event Number to Force RAI Congiguration
BitField Bits: [43:36]
--------------------------------------*/
#define c_upen_rai_cfg_cfgraierr_step_Mask                                                            cBit11_4
#define c_upen_rai_cfg_cfgraierr_step_Shift                                                                  4

/*--------------------------------------
BitField Name: cfgraimsk_pos
BitField Type: RW
BitField Desc: The Position Mask for RAI Congiguration
BitField Bits: [35:20]
--------------------------------------*/
#define c_upen_rai_cfg_cfgraimsk_pos_Mask_01                                                         cBit31_20
#define c_upen_rai_cfg_cfgraimsk_pos_Shift_01                                                               20
#define c_upen_rai_cfg_cfgraimsk_pos_Mask_02                                                           cBit3_0
#define c_upen_rai_cfg_cfgraimsk_pos_Shift_02                                                                0

/*--------------------------------------
BitField Name: cfgraimsk_dat
BitField Type: RW
BitField Desc: The Data Mask For RAI Congiguration
BitField Bits: [19]
--------------------------------------*/
#define c_upen_rai_cfg_cfgraimsk_dat_Mask                                                               cBit19
#define c_upen_rai_cfg_cfgraimsk_dat_Shift                                                                  19

/*--------------------------------------
BitField Name: cfgraierr_num
BitField Type: RW
BitField Desc: The Total RAI Error Number Congiguration
BitField Bits: [18:03]
--------------------------------------*/
#define c_upen_rai_cfg_cfgraierr_num_Mask                                                             cBit18_3
#define c_upen_rai_cfg_cfgraierr_num_Shift                                                                   3

/*--------------------------------------
BitField Name: cfg_frcfbitmod
BitField Type: RW
BitField Desc: Force RAI Mode 000:N error� (N = 16bits),N in (Tms)� (N = 16bits,
Tms 16 bits) 001: N error persecond 01x:Set / Clear 10x:Set in Ts (Ts < 256s)
11x:One event/N event (64ms) in one sec repeat Ts
BitField Bits: [02:00]
--------------------------------------*/
#define c_upen_rai_cfg_cfg_frcfbitmod_Mask                                                             cBit2_0
#define c_upen_rai_cfg_cfg_frcfbitmod_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Force RAI Error Status
Reg Addr   : 0x00093400-0x937FF
Reg Formula: 0x00093400 +  32*de3id + 4*de2id + de1id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
This register is applicable to see the Force RAI Error Status

------------------------------------------------------------------------------*/
#define cDs1Reg_upen_rai_sta                                                                           0x00093400
#define cDs1Reg_upen_rai_sta_WidthVal                                                                          32

/*--------------------------------------
BitField Name: staraierr_stepcnt
BitField Type: RW
BitField Desc: The Event RAI Counter Status
BitField Bits: [29:22]
--------------------------------------*/
#define c_upen_rai_sta_staraierr_stepcnt_Mask                                                     cBit29_22
#define c_upen_rai_sta_staraierr_stepcnt_Shift                                                           22

/*--------------------------------------
BitField Name: staraimsk_poscnt
BitField Type: RW
BitField Desc: The Position Mask RAI Counter Status
BitField Bits: [21:18]
--------------------------------------*/
#define c_upen_rai_sta_staraimsk_poscnt_Mask                                                      cBit21_18
#define c_upen_rai_sta_staraimsk_poscnt_Shift                                                               18

/*--------------------------------------
BitField Name: staraierr_cnt
BitField Type: RW
BitField Desc: The Remain Force RAI Error Number Status
BitField Bits: [17:2]
--------------------------------------*/
#define c_upen_rai_sta_staraierr_cnt_Mask                                                             cBit17_2
#define c_upen_rai_sta_staraierr_cnt_Shift                                                                   2

/*--------------------------------------
BitField Name: staraifrc_err
BitField Type: RW
BitField Desc: RAI Error Status
BitField Bits: [1]
--------------------------------------*/
#define c_upen_rai_sta_staraifrc_err_Mask                                                                cBit1
#define c_upen_rai_sta_staraifrc_err_Shift                                                                   1

/*--------------------------------------
BitField Name: staraiosecenb
BitField Type: RW
BitField Desc: Force RAI in One Second Enable  Status
BitField Bits: [0]
--------------------------------------*/
#define c_upen_rai_sta_staraiosecenb_Mask                                                                cBit0
#define c_upen_rai_sta_staraiosecenb_Shift                                                                   0

/*------------------------------------------------------------------------------
Reg Name   : Force LOS Error Configuration
Reg Addr   : 0x00093800-0x93bFF
Reg Formula: 0x00093800 +  32*de3id + 4*de2id + de1id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
This register is applicable to configure Force LOS Error force DS1/E1 Frame

------------------------------------------------------------------------------*/
#define cDs1Reg_upen_los_cfg                                                                           0x00093800
#define cDs1Reg_upen_los_cfg_WidthVal                                                                          32

/*--------------------------------------
BitField Name: cfg_msk_pos
BitField Type: RW
BitField Desc: Mask Position for 16 Event
BitField Bits: [30:15]
--------------------------------------*/
#define cDs1_upen_los_cfg_cfg_msk_pos_Mask                                                              cBit30_15
#define cDs1_upen_los_cfg_cfg_msk_pos_Shift                                                                    15

/*--------------------------------------
BitField Name: cfg_sec_num
BitField Type: RW
BitField Desc: The T second number
BitField Bits: [14:7]
--------------------------------------*/
#define cDs1_upen_los_cfg_cfg_sec_num_Mask                                                               cBit14_7
#define cDs1_upen_los_cfg_cfg_sec_num_Shift                                                                     7

/*--------------------------------------
BitField Name: cfg_evtin1s
BitField Type: RW
BitField Desc: The even number in a second set in T second
BitField Bits: [6:3]
--------------------------------------*/
#define c_upen_los_cfg_cfg_evtin1s_Mask                                                                cBit6_3
#define c_upen_los_cfg_cfg_evtin1s_Shift                                                                     3

/*--------------------------------------
BitField Name: cfg_frc_ena
BitField Type: RW
BitField Desc: Force Alarm LOS Enable
BitField Bits: [2]
--------------------------------------*/
#define c_upen_los_cfg_cfg_frc_ena_Mask                                                                  cBit2
#define c_upen_los_cfg_cfg_frc_ena_Shift                                                                     2

/*--------------------------------------
BitField Name: cfg_frc_mod
BitField Type: RW
BitField Desc: Force Alarm LOS mode 00: Set Alarm in T second 01: Set or clear
1x set one or N even in T second
BitField Bits: [1:0]
--------------------------------------*/
#define c_upen_los_cfg_cfg_frc_mod_Mask                                                                cBit1_0
#define c_upen_los_cfg_cfg_frc_mod_Shift                                                                     0

/*------------------------------------------------------------------------------
Reg Name   : Force LOS Error Status
Reg Addr   : 0x00093c00-0x93FFF
Reg Formula: 0x00093c00 +  32*de3id + 4*de2id + de1id
    Where  :
           + $de3id(0-23)
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
This register is applicable to see the Force LOS Error Status

------------------------------------------------------------------------------*/
#define cDs1Reg_upen_los_sta                                                                           0x00093c00
#define cDs1Reg_upen_los_sta_WidthVal                                                                          32

/*--------------------------------------
BitField Name: sta_msk_pos
BitField Type: RW
BitField Desc: The Position Index Status
BitField Bits: [20:17]
--------------------------------------*/
#define c_upen_los_sta_sta_msk_pos_Mask                                                              cBit20_17
#define c_upen_los_sta_sta_msk_pos_Shift                                                                    17

/*--------------------------------------
BitField Name: sta_seccnt
BitField Type: RW
BitField Desc: The Event Counter Status
BitField Bits: [16:09]
--------------------------------------*/
#define c_upen_los_sta_sta_seccnt_Mask                                                                cBit16_9
#define c_upen_los_sta_sta_seccnt_Shift                                                                      9

/*--------------------------------------
BitField Name: stamseccnt
BitField Type: RW
BitField Desc: The Second Counter Status
BitField Bits: [08:05]
--------------------------------------*/
#define c_upen_los_sta_stamseccnt_Mask                                                                 cBit8_5
#define c_upen_los_sta_stamseccnt_Shift                                                                      5

/*--------------------------------------
BitField Name: sta_upd_cnt
BitField Type: RW
BitField Desc: Updated Status
BitField Bits: [04:03]
--------------------------------------*/
#define c_upen_los_sta_sta_upd_cnt_Mask                                                                cBit4_3
#define c_upen_los_sta_sta_upd_cnt_Shift                                                                     3

/*--------------------------------------
BitField Name: statimmsec
BitField Type: RW
BitField Desc: Posegde Second Status
BitField Bits: [02]
--------------------------------------*/
#define c_upen_los_sta_statimmsec_Mask                                                                   cBit2
#define c_upen_los_sta_statimmsec_Shift                                                                      2

/*--------------------------------------
BitField Name: statim_sec
BitField Type: RW
BitField Desc: Posegde Second Status
BitField Bits: [01]
--------------------------------------*/
#define c_upen_los_sta_statim_sec_Mask                                                                   cBit1
#define c_upen_los_sta_statim_sec_Shift                                                                      1

/*--------------------------------------
BitField Name: staosecenb
BitField Type: RW
BitField Desc: Force LOS  Enable Status
BitField Bits: [00]
--------------------------------------*/
#define c_upen_los_sta_staosecenb_Mask                                                                   cBit0
#define c_upen_los_sta_staosecenb_Shift                                                                      0

/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling Generator Control
Reg Addr   : 0x000C0000 - 0x000C3FFF
Reg Formula: 0x000C0000 +  1024*de3id + 128*de2id + 32*de1id + tscnt
    Where  :
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
           + $tscnt(0-23): (ds1) or 0 - 31 (e1)
Reg Desc   :
DS1/E1/J1 Tx framer signaling generator control is used to configure for signaling operation modes at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_gen_ctrl                                                        0x000C0000

/*--------------------------------------
BitField Name: TxDE1SigRptThres
BitField Type: RW
BitField Desc: Threshold to repeat ABCD pattern
BitField Bits: [9:6]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigRptThres_Mask                                        cBit9_6
#define cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigRptThres_Shift                                             6
/*--------------------------------------
BitField Name: TxDE1SigFixPatAB
BitField Type: RW
BitField Desc: Fixed AB Pattern
BitField Bits: [11:10]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigFixPatAB_Mask                                          cBit11_10
#define cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigFixPatAB_Shift                                                10

/*--------------------------------------
BitField Name: TxDE1SigFixPat
BitField Type: RW
BitField Desc: Fixed ABCD Pattern
BitField Bits: [5:2]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigFixPat_Mask                                          cBit5_2
#define cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigFixPat_Shift                                               2

/*--------------------------------------
BitField Name: TxDE1SigGenMd
BitField Type: RW
BitField Desc: Generator Mode 00:  Sequence 01:  Fixed 10:  Prbs7
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigGenMd_Mask                                           cBit1_0
#define cAf6_dej1_tx_framer_sign_gen_ctrl_TxDE1SigGenMd_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling Generator Status
Reg Addr   : 0x000C4000 - 0x000C7FFF
Reg Formula: 0x000C4000 +  1024*de3id + 128*de2id + 32*de1id + tscnt
    Where  :
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
           + $tscnt(0-23): (ds1) or 0 - 31 (e1)
Reg Desc   :
HW Status.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_gen_stt                                                         0x000C4000

/*--------------------------------------
BitField Name: TxDE1SigHwStt
BitField Type: RW
BitField Desc: Hw Debug Only
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_gen_stt_TxDE1SigHwStt_Mask                                           cBit11_0
#define cAf6_dej1_tx_framer_sign_gen_stt_TxDE1SigHwStt_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling Monitoring Status
Reg Addr   : 0x000A4000 - 0x000A3FFF
Reg Formula: 0x000A4000 +  1024*de3id + 128*de2id + 32*de1id + tscnt
    Where  :
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
           + $tscnt(0-23): (ds1) or 0 - 31 (e1)
Reg Desc   :
DS1/E1/J1 Rx framer signaling monitor control is used to configure for signaling operation modes at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_mon_sta                                                        0x000A4000

#define cAf6_dej1_tx_framer_sign_mon_sta_RxPattern_Mask                                           cBit9_6
#define cAf6_dej1_tx_framer_sign_mon_sta_RxPattern_Shift                                                6
/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling Monitoring Control
Reg Addr   : 0x000A0000 - 0x000A3FFF
Reg Formula: 0x000A0000 +  1024*de3id + 128*de2id + 32*de1id + tscnt
    Where  :
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
           + $tscnt(0-23): (ds1) or 0 - 31 (e1)
Reg Desc   :
DS1/E1/J1 Rx framer signaling monitor control is used to configure for signaling operation modes at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_mon_ctrl                                                        0x000A0000

/*--------------------------------------
BitField Name: RxDE1SigRptThres
BitField Type: RW
BitField Desc: Threshold to repeat ABCD pattern
BitField Bits: [9:6]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_mon_ctrl_RxDE1SigRptThres_Mask                                        cBit9_6
#define cAf6_dej1_tx_framer_sign_mon_ctrl_RxDE1SigRptThres_Shift                                             6

/*--------------------------------------
BitField Name: RxDE1SigFixPat
BitField Type: RW
BitField Desc: Fixed ABCD Pattern
BitField Bits: [5:2]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_mon_ctrl_RxDE1SigFixPat_Mask                                          cBit5_2
#define cAf6_dej1_tx_framer_sign_mon_ctrl_RxDE1SigFixPat_Shift                                               2

/*--------------------------------------
BitField Name: RxDE1SigGenMd
BitField Type: RW
BitField Desc: Monitor Mode 00:  Sequence 01:  Fixed 10:  Prbs7
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_mon_ctrl_RxDE1SigGenMd_Mask                                           cBit1_0
#define cAf6_dej1_tx_framer_sign_mon_ctrl_RxDE1SigGenMd_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling Monitoring Control
Reg Addr   : 0x000A4000 - 0x000A7FFF
Reg Formula: 0x000A4000 +  1024*de3id + 128*de2id + 32*de1id + tscnt
    Where  :
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
           + $tscnt(0-23): (ds1) or 0 - 31 (e1)
Reg Desc   :
HW Status.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_mon_stt                                                         0x000A4000

/*--------------------------------------
BitField Name: RxDE1SigHwStt
BitField Type: RW
BitField Desc: Hw Debug Only
BitField Bits: [11:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_gen_stt_RxDE1SigHwStt_Mask                                           cBit11_0
#define cAf6_dej1_tx_framer_sign_gen_stt_RxDE1SigHwStt_Shift                                                 0


/*------------------------------------------------------------------------------
Reg Name   : DS0 Rx Framer Signaling Channel Interrupt Enable Control
Reg Addr   : 0x000BFFFE
Reg Formula:
    Where  :
Reg Desc   :
This is the Channel interrupt enable of DS0 Rx Framer Signaling.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds0_rx_sign_chn_intr_cfg                                                            0x000BFFFE
#define cAf6Reg_ds0_rx_sign_chn_intr_cfg_WidthVal                                                           32

/*--------------------------------------
BitField Name: RxDs0SignIntrEn
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS0 0: Disable 1: Enable
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_ds0_rx_sign_chn_intr_cfg_RxDs0SignIntrEn_Mask                                            cBit13_0
#define cAf6_ds0_rx_sign_chn_intr_cfg_RxDs0SignIntrEn_Shift                                                  0


/*------------------------------------------------------------------------------
Reg Name   : DS0 Rx Framer Signaling Channel Interrupt Status
Reg Addr   : 0x000BFFFF
Reg Formula:
    Where  :
Reg Desc   :
This is the Channel interrupt tus of DS0 Rx Framer Signaling.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds0_rx_sign_chn_intr_stat                                                           0x000BFFFF
#define cAf6Reg_ds0_rx_sign_chn_intr_stat_WidthVal                                                          32

/*--------------------------------------
BitField Name: RxDs0SignIntrSta
BitField Type: RW
BitField Desc: Each bit correspond with each channel DS0 0: No Interrupt 1: Have
Interrupt
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_ds0_rx_sign_chn_intr_stat_RxDs0SignIntrSta_Mask                                          cBit13_0
#define cAf6_ds0_rx_sign_chn_intr_stat_RxDs0SignIntrSta_Shift                                                0


/*------------------------------------------------------------------------------
Reg Name   : DS0 Rx Framer Signaling per Channel Interrupt Enable Control
Reg Addr   : 0x000B0000-0x000B3FFF
Reg Formula: 0x000B0000 +  1024*de3id + 128*de2id + 32*de1id + tscnt
    Where  :
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
           + $tscnt(0-23): (ds1) or 0 - 31 (e1)
Reg Desc   :
This is the per Channel interrupt enable of DS0 Rx Framer Signaling.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds0_rx_sign_per_chn_intr_cfg                                                        0x000B0000
#define cAf6Reg_ds0_rx_sign_per_chn_intr_cfg_WidthVal                                                       32

/*--------------------------------------
BitField Name: Ds0SigLopIntrEn
BitField Type: RW
BitField Desc: Set 1 to enable change LOP event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ds0_rx_sign_per_chn_intr_cfg_Ds0SigLopIntrEn_Mask                                           cBit0
#define cAf6_ds0_rx_sign_per_chn_intr_cfg_Ds0SigLopIntrEn_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : DS0 Rx Framer Signalin  per Channel Interrupt Change Status
Reg Addr   : 0x000B4000-0x000B7FFF
Reg Formula: 0x000B4000 +  1024*de3id + 128*de2id + 32*de1id + tscnt
    Where  :
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
           + $tscnt(0-23): (ds1) or 0 - 31 (e1)
Reg Desc   :
This is the per Channel interrupt change status  of DS0 Rx Framer Signaling.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds0_rx_sign_per_chn_intr_stat                                                       0x000B4000
#define cAf6Reg_ds0_rx_sign_per_chn_intr_stat_WidthVal                                                      32

/*--------------------------------------
BitField Name: Ds0SigLop_Intr
BitField Type: RW
BitField Desc: Set 1 if there is a  change LOP event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ds0_rx_sign_per_chn_intr_stat_Ds0SigLop_Intr_Mask                                           cBit0
#define cAf6_ds0_rx_sign_per_chn_intr_stat_Ds0SigLop_Intr_Shift                                              0


/*------------------------------------------------------------------------------
Reg Name   : DS0 Rx Framer Signaling per Channel Current Status
Reg Addr   : 0x000B8000-0x000BBFFF
Reg Formula: 0x000B8000 +  1024*de3id + 128*de2id + 32*de1id + tscnt
    Where  :
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
           + $tscnt(0-23): (ds1) or 0 - 31 (e1)
Reg Desc   :
This is the per Channel interrupt current status  of DS0 Rx Framer Signaling.

------------------------------------------------------------------------------*/
#define cAf6Reg_ds0_rx_sign_per_chn_curr_stat                                                       0x000B8000
#define cAf6Reg_ds0_rx_sign_per_chn_curr_stat_WidthVal                                                      32

/*--------------------------------------
BitField Name: Ds0SignRpeCurrSta
BitField Type: RW
BitField Desc: Repeat error interrupt
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ds0_rx_sign_per_chn_curr_stat_Ds0SignRpeCurrSta_Mask                                        cBit1
#define cAf6_ds0_rx_sign_per_chn_curr_stat_Ds0SignRpeCurrSta_Shift                                           1

/*--------------------------------------
BitField Name: Ds0SignLopCurrSta
BitField Type: RW
BitField Desc: Current status of LOP event to generate an interrupt.
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ds0_rx_sign_per_chn_curr_stat_Ds0SignLopCurrSta_Mask                                        cBit0
#define cAf6_ds0_rx_sign_per_chn_curr_stat_Ds0SignLopCurrSta_Shift                                           0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Rx Framer Signaling Insertion Control
Reg Addr   : 0x00054c00 - 0x00054FFF
Reg Formula: 0x00054c00 +  32*de3id + 4*de2id + de1id
    Where  :
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
DS1/E1/J1 Rx framer signaling insertion control is used to configure for signaling operation modes at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_rx_framer_sign_insertion_ctrl                                                  0x00054c00

/*--------------------------------------
BitField Name: RxDE1SigMfrmEn
BitField Type: RW
BitField Desc: Set 1 to enable the Rx DS1/E1/J1 framer to sync the signaling
multiframe to the incoming data flow. No applicable for DS1
BitField Bits: [30]
--------------------------------------*/
#define cAf6_dej1_rx_framer_sign_insertion_ctrl_RxDE1SigMfrmEn_Mask                                     cBit30
#define cAf6_dej1_rx_framer_sign_insertion_ctrl_RxDE1SigMfrmEn_Shift                                        30

/*--------------------------------------
BitField Name: RxDE1SigBypass
BitField Type: RW
BitField Desc: 30 signaling bypass bit for 32 DS0 in an E1 frame. In DS1 mode,
only 24 LSB bits is used. Set 1 to bypass Signaling insertion in Rx DS1/E1
Framer
BitField Bits: [29:0]
--------------------------------------*/
#define cAf6_dej1_rx_framer_sign_insertion_ctrl_RxDE1SigBypass_Mask                                   cBit29_0
#define cAf6_dej1_rx_framer_sign_insertion_ctrl_RxDE1SigBypass_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : DS1/E1/J1 Tx Framer Signaling Insertion Control
Reg Addr   : 0x00094800 - 0x00094BFF
Reg Formula: 0x00094800 +  32*de3id + 4*de2id + de1id
    Where  :
           + $de3id(0-23):
           + $de2id(0-6):
           + $de1id(0-3):
Reg Desc   :
DS1/E1/J1 Rx framer signaling insertion control is used to configure for signaling operation modes at the DS1/E1 framer.

------------------------------------------------------------------------------*/
#define cAf6Reg_dej1_tx_framer_sign_insertion_ctrl                                                  0x00094800

/*--------------------------------------
BitField Name: TxDE1SigMfrmEn
BitField Type: RW
BitField Desc: Set 1 to enable the Tx DS1/E1/J1 framer to sync the signaling
multiframe to the incoming data flow. No applicable for DS1
BitField Bits: [30]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigMfrmEn_Mask                                     cBit30
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigMfrmEn_Shift                                        30

/*--------------------------------------
BitField Name: TxDE1SigBypass
BitField Type: RW
BitField Desc: 30 signaling bypass bit for 32 DS0 in an E1 frame. In DS1 mode,
only 24 LSB bits is used. Set 1 to bypass Signaling insertion in Tx DS1/E1
Framer
BitField Bits: [29:0]
--------------------------------------*/
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigBypass_Mask                                   cBit29_0
#define cAf6_dej1_tx_framer_sign_insertion_ctrl_TxDE1SigBypass_Shift                                         0


/*------------------------------------------------------------------------------
Reg Name   : DS0 Rx Framer Signaling Global Configuration
Reg Addr   : 0x000ac001
Reg Formula:
    Where  :
Reg Desc   :
This is the global configuration register for the DS0 Rx Framer Signaling

------------------------------------------------------------------------------*/
#define cAf6Reg_ds0_rx_sign_glb_cfg                                                                 0x000ac001

/*--------------------------------------
BitField Name: Ds0RxSignDumpBit
BitField Type: RW
BitField Desc: Selectec Dump 10:   Before detected repeation else: After
detected repetition
BitField Bits: [17:16]
--------------------------------------*/
#define cAf6_ds0_rx_sign_glb_cfg_Ds0RxSignSelDumpBit_Mask                                               cBit17_16
#define cAf6_ds0_rx_sign_glb_cfg_Ds0RxSignSelDumpBit_Shift                                                     16

/*--------------------------------------
BitField Name: Ds0RxSignDumpBit
BitField Type: RW
BitField Desc: Dump Bit
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ds0_rx_sign_glb_cfg_Ds0RxSignDumpBit_Mask                                                  cBit15
#define cAf6_ds0_rx_sign_glb_cfg_Ds0RxSignDumpBit_Shift                                                     15

/*--------------------------------------
BitField Name: Ds0RxSigIdSel
BitField Type: RW
BitField Desc: Channel ID is selected
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_ds0_rx_sign_glb_cfg_Ds0RxSigIdSel_Mask                                                   cBit13_0
#define cAf6_ds0_rx_sign_glb_cfg_Ds0RxSigIdSel_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : DS0 Rx Framer Signaling
Reg Addr   : 0x000a8000 - 0x000a801f
Reg Formula:
    Where  :
Reg Desc   :
This is the dump pattern register for the DS0 Rx Framer Signaling

------------------------------------------------------------------------------*/
#define cAf6Reg_ds0_rx_sign_dump_pat                                                                0x000a8000
#define cAf6Reg_ds0_rx_sign_dump_pat_WidthVal                                                               32

/*--------------------------------------
BitField Name: Ds0RxSigPattern
BitField Type: RW
BitField Desc: ABCD
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_ds0_rx_sign_dump_pat_Ds0RxSigPattern_Mask                                                 cBit3_0
#define cAf6_ds0_rx_sign_dump_pat_Ds0RxSigPattern_Shift                                                      0


/*------------------------------------------------------------------------------
Reg Name   : DS0 Tx Framer Signaling Global Configuration
Reg Addr   : 0x000c8000
Reg Formula:
    Where  :
Reg Desc   :
This is the global configuration register for the DS0 Tx Framer Signaling

------------------------------------------------------------------------------*/
#define cAf6Reg_ds0_tx_sign_glb_cfg                                                                 0x000c8000

/*--------------------------------------
BitField Name: Ds0TxSignDumpBit
BitField Type: RW
BitField Desc: Dump Bit
BitField Bits: [15]
--------------------------------------*/
#define cAf6_ds0_tx_sign_glb_cfg_Ds0TxSignDumpBit_Mask                                                  cBit15
#define cAf6_ds0_tx_sign_glb_cfg_Ds0TxSignDumpBit_Shift                                                     15

/*--------------------------------------
BitField Name: Ds0TxSigIdSel
BitField Type: RW
BitField Desc: Channel ID is selected
BitField Bits: [13:0]
--------------------------------------*/
#define cAf6_ds0_tx_sign_glb_cfg_Ds0TxSigIdSel_Mask                                                   cBit13_0
#define cAf6_ds0_tx_sign_glb_cfg_Ds0TxSigIdSel_Shift                                                         0


/*------------------------------------------------------------------------------
Reg Name   : DS0 Tx Framer Signaling Dump Pattern
Reg Addr   : 0x000cc000 - 0x000cc01f
Reg Formula:
    Where  :
Reg Desc   :
This is the dump pattern register for the DS0 Tx Framer Signaling

------------------------------------------------------------------------------*/
#define cAf6Reg_ds0_tx_sign_dump_pat                                                                0x000cc000
#define cAf6Reg_ds0_tx_sign_dump_pat_WidthVal                                                               32

/*--------------------------------------
BitField Name: Ds0TxSigPattern
BitField Type: RW
BitField Desc: ABCD
BitField Bits: [3:0]
--------------------------------------*/
#define cAf6_ds0_tx_sign_dump_pat_Ds0TxSigPattern_Mask                                                 cBit3_0
#define cAf6_ds0_tx_sign_dump_pat_Ds0TxSigPattern_Shift                                                      0



/*------------------------------------------------------------------------------
Reg Name   : DS0 Tx Framer Signaling Sticky
Reg Addr   : 0x000ac002
Reg Formula:
    Where  :
Reg Desc   :
This is the sticky register for the DS0 Tx Framer Signaling

------------------------------------------------------------------------------*/
#define cAf6Reg_ds0_tx_sign_stk                                                                     0x000ac002
#define cAf6Reg_ds0_tx_sign_stk_WidthVal                                                                    32

/*--------------------------------------
BitField Name: Ds0TxSigStk
BitField Type: RW
BitField Desc:
BitField Bits: [0]
--------------------------------------*/
#define cAf6_ds0_tx_sign_stk_Ds0TxSigStk_Mask                                                            cBit0
#define cAf6_ds0_tx_sign_stk_Ds0TxSigStk_Shift                                                               0
#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031PDHDE1ATTREG_H_ */


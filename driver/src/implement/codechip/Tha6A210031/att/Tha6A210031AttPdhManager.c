/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A210031AttPdhManager.c
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : Default implementation of PDH module of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../pdh/Tha6A210031PdhAttControllerInternal.h"
#include "Tha6A210031AttPdhManagerInternal.h"
#include "../pdh/Tha6A210031PdhDe3AttReg.h"
#include "../pdh/Tha6A210031PdhDe1AttReg.h"


/*--------------------------- Define -----------------------------------------*/
#define cReg_upennsecds1                                                                          0x9000aUL
#define c_upennsecds1_nfas_enb_Mask                                                                  cBit17
#define c_upennsecds1_nfas_enb_Shift                                                                     17
#define c_upennsecds1_fas_enb_Mask                                                                   cBit16
#define c_upennsecds1_fas_enb_Shift                                                                      16
#define c_upennsecds1_cfgnsecds1_Mask                                                              cBit15_0
#define cReg_upen_fbitgap_cfg                                                                      0x90008
#define cReg_upen_crcgap_cfg                                                                       0x90007

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A210031AttPdhManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttPdhManagerMethods m_AtAttPdhManagerOverride;
static tThaAttPdhManagerMethods m_ThaAttPdhManagerOverride;
/* Save super implementations */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/

/*static uint16 LongRead(AtAttPdhManager self, uint32 address, uint32 *regVal, uint16 bufferLen)
    {
    AtModulePdh pdh = self->pdh;
    regVal[0] = mModuleHwRead(pdh, address);
    regVal[1] = mModuleHwRead(pdh, 0x21);
    regVal[2] = mModuleHwRead(pdh, 0x22);
    regVal[3] = mModuleHwRead(pdh, 0x23);
    return bufferLen;
    }*/

static uint16 LongWrite(AtAttPdhManager self, uint32 address, uint32 *regVal, uint16 bufferLen)
    {
    AtModulePdh pdh = self->pdh;
    mModuleHwWrite(pdh, 0x11, regVal[1]);
    mModuleHwWrite(pdh, 0x12, regVal[2]);
    mModuleHwWrite(pdh, 0x13, regVal[3]);
    mModuleHwWrite(pdh, address, regVal[0]);
    return bufferLen;
    }

static eAtRet De3AttClearStatus(AtAttPdhManager self)
    {
    AtModulePdh pdh = self->pdh;
    uint8 slice = 0;
    uint32 dataBuffer[4] = {0};
    for (slice=0; slice <ThaModulePdhNumSlices((ThaModulePdh)pdh); slice++)
        {
        uint32   baseAddress = ThaModulePdhSliceBase((ThaModulePdh)pdh, slice);
        uint32   id = 0;
        for (id=0; id <= 0x17 /*23*/; id++)
            {
            LongWrite(self, cReg_upen_fbitsta(0)      + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cReg_upen_cpb_sta(0)      + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cReg_upen_rdi_sta(0)      + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cReg_upen_pbit_sta(0)     + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cReg_upen_faB_sta(0)      + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cReg_upen_bip8_sta(0)     + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cReg_upen_rei_sta(0)      + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cReg_upen_nrB_sta(0)      + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cReg_upen_gcB_sta(0)      + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cAttReg_upen_los_sta      + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cAttReg_upen_ais_sta      + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cAttReg_upen_ais_e3_sta   + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cAttReg_upen_febe_sta     + baseAddress + id, dataBuffer, 4);
            }
        }
    AtUnused(self);
    return cAtOk;
    }

static eAtRet De1AttClearStatus(AtAttPdhManager self)
    {
    AtModulePdh pdh = self->pdh;
    uint8 slice = 0;
    uint32 dataBuffer[4] = {0};
    for (slice=0; slice <ThaModulePdhNumSlices((ThaModulePdh)pdh); slice++)
        {
        uint32   baseAddress = ThaModulePdhSliceBase((ThaModulePdh)pdh, slice);
        uint32   id = 0;
        for (id=0; id <= 0x17; id++)
            {
            LongWrite(self, cDs1Reg_upen_los_sta   + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cDs1Reg_upen_fbitsta   + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cDs1Reg_upen_fbit_sta  + baseAddress + id, dataBuffer, 4);
            LongWrite(self, cDs1Reg_upen_rai_sta   + baseAddress + id, dataBuffer, 4);
            }
        }
    AtUnused(self);
    return cAtOk;
    }
static eAtRet De1AttInit(AtAttPdhManager self)
    {
    De1AttClearStatus(self);
    return cAtOk;
    }

static eAtRet De3AttInit(AtAttPdhManager self)
    {
    ThaModulePdh pdh = (ThaModulePdh) AtAttPdhManagerModulePdh(self);
    uint32   slice = 0;
    for (slice=0; slice <ThaModulePdhNumSlices(pdh); slice++)
        {
        uint32   baseAddress = ThaModulePdhSliceBase(pdh, slice);
        mModuleHwWrite(pdh, 0x80f80+baseAddress, 0x10001);
        }
    De3AttClearStatus(self);
    return cAtOk;
    }

static eAtRet InitErrorGap(AtAttPdhManager self)
    {
    ThaModulePdh module =(ThaModulePdh)( ((AtAttPdhManager)self)->pdh);
    uint32   slice = 0;
    for (slice=0; slice <ThaModulePdhNumSlices((ThaModulePdh)module); slice++)
        {
        uint32   baseAddress = ThaModulePdhSliceBase((ThaModulePdh) module, slice);
        mModuleHwWrite(module, cReg_upen_crcgap_cfg  + baseAddress, 0x0);
        mModuleHwWrite(module, cReg_upen_fbitgap_cfg + baseAddress, 0x0);
        mModuleHwWrite(module, cReg_upennsecds1 + baseAddress, 0x12EBB);

        }
    return cAtOk;
    }

static eAtRet ForcePerframeSet(AtAttPdhManager self, eBool enable)
    {
    ThaModulePdh pdh     = (ThaModulePdh) AtAttPdhManagerModulePdh(self);
    uint32 regAddr       = cReg_upennsecds3;
    uint32 regFieldMask  = c_upennsecds3_frcpfrmen_Mask;
    AtIpCore core        = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)pdh), 0);
    uint32   slice = 0;
    for (slice=0; slice <ThaModulePdhNumSlices(pdh); slice++)
        {
        uint32   baseAddress = ThaModulePdhSliceBase(pdh, slice);
        uint32   longRegVal[cThaLongRegMaxSize];
        uint32   regFieldShift = AtRegMaskToShift(regFieldMask);
        mModuleHwLongRead(pdh, regAddr +baseAddress, longRegVal, cThaLongRegMaxSize, core);
        mRegFieldSet(longRegVal[0], regField, enable?1:0);
        mModuleHwLongWrite(pdh, regAddr +baseAddress, longRegVal, cThaLongRegMaxSize, core);
        }
    return cAtOk;
    }


static eAtRet ForcePeriodSet(Tha6A210031AttPdhManager self, uint32 numSeconds, uint32 regAddr, uint32 regFieldMask)
    {
    ThaModulePdh module =(ThaModulePdh)( ((AtAttPdhManager)self)->pdh);
    AtIpCore core        = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)module), 0);
    uint32   slice = 0;
    for (slice=0; slice <ThaModulePdhNumSlices((ThaModulePdh)module); slice++)
        {
        uint32   baseAddress = ThaModulePdhSliceBase((ThaModulePdh) module, slice);
        uint32   longRegVal[cThaLongRegMaxSize];
        uint32   regFieldShift = AtRegMaskToShift(regFieldMask);
        mModuleHwLongRead(module, regAddr +baseAddress, longRegVal, cThaLongRegMaxSize, core);
        mRegFieldSet(longRegVal[0], regField, numSeconds);
        mModuleHwLongWrite(module, regAddr +baseAddress, longRegVal, cThaLongRegMaxSize, core);
        }
    return cAtOk;
    }

static uint32 ForcePeriodGet(Tha6A210031AttPdhManager self, uint32 regAddr, uint32 regFieldMask)
    {
    ThaModulePdh module =(ThaModulePdh)( ((AtAttPdhManager)self)->pdh);
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)module), 0);
    uint32   baseAddress = ThaModulePdhBaseAddress((ThaModulePdh) module);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regFieldShift  = AtRegMaskToShift(regFieldMask);
    mModuleHwLongRead(module, regAddr+baseAddress, longRegVal, cThaLongRegMaxSize, core);
    return mRegField(longRegVal[0], regField);
    }

static eAtRet De3ForcePeriodSet(AtAttPdhManager self, uint32 numMiliSeconds)
    {
    if (numMiliSeconds < 1000)
        numMiliSeconds = 1000;
    return ForcePeriodSet(mThis(self),numMiliSeconds, cReg_upennsecds3, c_upennsecds3_cfgnsecds3_Mask );
    }

static uint32 De3ForcePeriodGet(AtAttPdhManager self)
    {
    return ForcePeriodGet(mThis(self),cReg_upennsecds3, c_upennsecds3_cfgnsecds3_Mask);
    }

static eAtRet De1ForcePeriodSet(AtAttPdhManager self, uint32 numMiliSeconds)
    {
    if (numMiliSeconds < 1000)
        numMiliSeconds = 1000;
    return ForcePeriodSet(mThis(self),numMiliSeconds, cReg_upennsecds1, c_upennsecds1_cfgnsecds1_Mask );
    }

static uint32 De1ForcePeriodGet(AtAttPdhManager self)
    {
    return ForcePeriodGet(mThis(self),cReg_upennsecds1, c_upennsecds1_cfgnsecds1_Mask);
    }

static eBool HasDe3Tie(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasDe1Tie(AtAttPdhManager self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet SetResetTimeAllDe1Channel(ThaAttPdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet SetResetTimeAllDe3Channel(ThaAttPdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtAttPdhManager(AtAttPdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttPdhManagerOverride, mMethodsGet(self), sizeof(m_AtAttPdhManagerOverride));
        mMethodOverride(m_AtAttPdhManagerOverride, HasDe3Tie);
        mMethodOverride(m_AtAttPdhManagerOverride, HasDe1Tie);
        mMethodOverride(m_AtAttPdhManagerOverride, De3AttInit);
        mMethodOverride(m_AtAttPdhManagerOverride, De1AttInit);
        mMethodOverride(m_AtAttPdhManagerOverride, De3ForcePeriodSet);
        mMethodOverride(m_AtAttPdhManagerOverride, De3ForcePeriodGet);
        mMethodOverride(m_AtAttPdhManagerOverride, De1ForcePeriodSet);
        mMethodOverride(m_AtAttPdhManagerOverride, De1ForcePeriodGet);
        mMethodOverride(m_AtAttPdhManagerOverride, InitErrorGap);
        mMethodOverride(m_AtAttPdhManagerOverride, ForcePerframeSet);
        }

    mMethodsSet(self, &m_AtAttPdhManagerOverride);
    }

static void OverrideThaAttPdhManager(ThaAttPdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttPdhManagerOverride, mMethodsGet(self), sizeof(m_ThaAttPdhManagerOverride));
        mMethodOverride(m_ThaAttPdhManagerOverride, SetResetTimeAllDe1Channel);
        mMethodOverride(m_ThaAttPdhManagerOverride, SetResetTimeAllDe3Channel);
        }

    mMethodsSet(self, &m_ThaAttPdhManagerOverride);
    }

static void Override(Tha6A210031AttPdhManager self)
    {
    OverrideAtAttPdhManager((AtAttPdhManager)self);
    OverrideThaAttPdhManager((ThaAttPdhManager)self);
    }


AtAttPdhManager Tha6A210031AttPdhManagerObjectInit(AtAttPdhManager self, AtModulePdh pdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaAttPdhManager));

    /* Super constructor */
    if (ThaAttPdhManagerObjectInit(self, pdh) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtAttPdhManager Tha6A210031AttPdhManagerNew(AtModulePdh pdh)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttPdhManager newObject = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaAttPdhManager));
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return Tha6A210031AttPdhManagerObjectInit(newObject, pdh);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : SDH
 * 
 * File        : Tha6A210021AttSdhManagerInternal.h
 * 
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : ATT SDH Manager of 6A210021
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031ATTSDHMANAGERINTERNAL_H_
#define _THA6A210031ATTSDHMANAGERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/att/ThaAttSdhManagerInternal.h"
#include "Tha6A210031AttSdhManager.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha6A210031AttSdhManager
    {
    tThaAttSdhManager super;
    }tTha6A210031AttSdhManager;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtAttSdhManager Tha6A210031AttSdhManagerObjectInit(AtAttSdhManager self, AtModuleSdh sdh);


/* Debugger */

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031ATTSDHMANAGERINTERNAL_H_ */


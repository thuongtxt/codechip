/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PDH
 *
 * File        : Tha6A210031AttPdhManager.c
 *
 * Created Date: July 26, 2017
 *
 * Author      : chaudpt
 *
 * Description : Default implementation of PDH module of Thalassa device
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../sdh/Tha6A210031SdhAttControllerInternal.h"
#include "Tha6A210031AttSdhManagerInternal.h"
#include "../sdh/Tha6A210031SdhAttReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha6A210031AttSdhManager)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtAttSdhManagerMethods m_AtAttSdhManagerOverride;
static tThaAttSdhManagerMethods m_ThaAttSdhManagerOverride;
/* Save super implementations */

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Internal functions -----------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*static uint16 LongRead(AtAttSdhManager self, uint32 address, uint32 *regVal, uint16 bufferLen)
    {
    AtModuleSdh sdh = self->sdh;
    regVal[0] = mModuleHwRead(sdh, address);
    regVal[1] = mModuleHwRead(sdh, 0x21);
    regVal[2] = mModuleHwRead(sdh, 0x22);
    regVal[3] = mModuleHwRead(sdh, 0x23);
    return bufferLen;
    }*/

static uint16 LongWrite(AtAttSdhManager self, uint32 address, uint32 *regVal, uint16 bufferLen)
    {
    AtModuleSdh sdh = self->sdh;
    mModuleHwWrite(sdh, 0x11, regVal[1]);
    mModuleHwWrite(sdh, 0x12, regVal[2]);
    mModuleHwWrite(sdh, 0x13, regVal[3]);
    mModuleHwWrite(sdh, address, regVal[0]);
    return bufferLen;
    }


static eAtRet LineAttInit(AtAttSdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet HoPathAttInit(AtAttSdhManager self)
    {
    uint32       dataBuffer[4]={0};
    uint32 id = 0, baseAddress = 0xa00000UL, slice=0;

    /* Clear high order path error and alarm status */
    for (slice = 0; slice<2; slice++)
        {
        for (id=0; id<= 23; id++)
            {
            baseAddress = 0x100000UL + 512UL*slice + id;
            LongWrite(self, cReg_upen_LOPstssta + baseAddress, dataBuffer, 4);
            LongWrite(self, cReg_upen_AISstssta + baseAddress, dataBuffer, 4);
            LongWrite(self, cReg_upen_RDIstssta + baseAddress, dataBuffer, 4);
            LongWrite(self, cReg_upen_LOMstssta + baseAddress, dataBuffer, 4);
            LongWrite(self, cReg_upen_UEQstssta + baseAddress, dataBuffer, 4);
            }
        }
    return cAtOk;
    }

static eAtRet LoPathAttInitSts(AtAttSdhManager self, uint8 slice, uint8 stsid)
    {
    uint32       baseAddress   = 0xa00000UL, vtgid=0, vtid=0;
    uint32       dataBuffer[4] = {0};

    /* Clear high order path error and alarm status */
    for (vtgid = 0; vtgid <= 6; vtgid ++)
        {
        for (vtid = 0; vtid <= 3; vtid ++)
            {
            baseAddress = 0x100000UL + 16384UL*slice + 32UL*stsid + 4UL*vtgid + vtid;
            LongWrite(self, cReg_upen_AISvtsta_Base + baseAddress, dataBuffer, 4);
            LongWrite(self, cReg_upen_LOPvtsta      + baseAddress, dataBuffer, 4);
            LongWrite(self, cReg_upen_UEQvtsta      + baseAddress, dataBuffer, 4);
            LongWrite(self, cReg_upen_RFIvtsta_Base + baseAddress, dataBuffer, 4);
            LongWrite(self, cReg_upen_RDIvtsta      + baseAddress, dataBuffer, 4);
            }
        }
    return cAtOk;
    }

static eAtRet LoPathAttInit(AtAttSdhManager self)
    {
    uint8 slice, stsid;
    for (slice = 0; slice<2; slice++)
        {
        for (stsid=0; stsid<= 23; stsid++)
            {
            LoPathAttInitSts(self, slice, stsid);
            }
        }
    return cAtOk;
    }

static uint32 OcnBaseAddress(Tha6A210031AttSdhManager self)
    {
    AtUnused(self);
    return (uint32)0xA00000;
    }

static eAtRet ForcePeriodSet(Tha6A210031AttSdhManager self, uint32 numSeconds, uint32 regAddr, uint32 regFieldMask)
    {
    ThaModuleSdh module =(ThaModuleSdh)( ((AtAttSdhManager)self)->sdh);
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)module), 0);
    uint32   baseAddress = OcnBaseAddress(self);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regFieldShift = AtRegMaskToShift(regFieldMask);
    mModuleHwLongRead(module, regAddr+baseAddress, longRegVal, (uint16)cThaLongRegMaxSize, core);
    mRegFieldSet(longRegVal[0], regField, numSeconds);
    mModuleHwLongWrite(module, regAddr+baseAddress, longRegVal, (uint16)cThaLongRegMaxSize, core);
    return cAtOk;
    }

static uint32 ForcePeriodGet(Tha6A210031AttSdhManager self, uint32 regAddr, uint32 regFieldMask)
    {
    ThaModuleSdh module =(ThaModuleSdh)( ((AtAttSdhManager)self)->sdh);
    AtIpCore core = AtDeviceIpCoreGet(AtModuleDeviceGet((AtModule)module), 0);
    uint32   baseAddress = OcnBaseAddress(self);
    uint32 longRegVal[cThaLongRegMaxSize];
    uint32 regFieldShift  = AtRegMaskToShift(regFieldMask);
    mModuleHwLongRead(module, regAddr+baseAddress, longRegVal, (uint16)cThaLongRegMaxSize, core);
    return mRegField(longRegVal[0], regField);
    }

static eAtRet LoForcePeriodSet(AtAttSdhManager self, uint32 numSeconds)
    {
    return ForcePeriodSet(mThis(self),numSeconds, cReg_upennsecvt, c_upennsecvt_cfgnsecvt_Mask );
    }

static uint32 LoForcePeriodGet(AtAttSdhManager self)
    {
    return ForcePeriodGet(mThis(self),cReg_upennsecvt, c_upennsecvt_cfgnsecvt_Mask);
    }

static eAtRet HoForcePeriodSet(AtAttSdhManager self, uint32 numSeconds)
    {
    return ForcePeriodSet(mThis(self),numSeconds, cReg_upennsecsts, c_upennsecsts_cfgnsecsts_Mask );
    }

static uint32 HoForcePeriodGet(AtAttSdhManager self)
    {
    return ForcePeriodGet(mThis(self),cReg_upennsecsts, c_upennsecsts_cfgnsecsts_Mask);
    }

static eBool HasLoTie(AtAttSdhManager self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eBool HasHoTie(AtAttSdhManager self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtRet SetResetTimeAllHoChannel(ThaAttSdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static eAtRet SetResetTimeAllLoChannel(ThaAttSdhManager self)
    {
    AtUnused(self);
    return cAtOk;
    }

static void OverrideAtAttSdhManager(AtAttSdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtAttSdhManagerOverride, mMethodsGet(self), sizeof(m_AtAttSdhManagerOverride));
        mMethodOverride(m_AtAttSdhManagerOverride, HasHoTie);
        mMethodOverride(m_AtAttSdhManagerOverride, HasLoTie);
        mMethodOverride(m_AtAttSdhManagerOverride, HoForcePeriodSet);
        mMethodOverride(m_AtAttSdhManagerOverride, HoForcePeriodGet);
        mMethodOverride(m_AtAttSdhManagerOverride, LoForcePeriodSet);
        mMethodOverride(m_AtAttSdhManagerOverride, LoForcePeriodGet);
        mMethodOverride(m_AtAttSdhManagerOverride, LineAttInit);
        mMethodOverride(m_AtAttSdhManagerOverride, HoPathAttInit);
        mMethodOverride(m_AtAttSdhManagerOverride, LoPathAttInit);
        }

    mMethodsSet(self, &m_AtAttSdhManagerOverride);
    }

static void OverrideThaAttSdhManager(ThaAttSdhManager self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaAttSdhManagerOverride, mMethodsGet(self), sizeof(m_ThaAttSdhManagerOverride));
        mMethodOverride(m_ThaAttSdhManagerOverride, SetResetTimeAllHoChannel);
        mMethodOverride(m_ThaAttSdhManagerOverride, SetResetTimeAllLoChannel);
        }

    mMethodsSet(self, &m_ThaAttSdhManagerOverride);
    }

static void Override(Tha6A210031AttSdhManager self)
    {
    OverrideAtAttSdhManager((AtAttSdhManager)self);
    OverrideThaAttSdhManager((ThaAttSdhManager)self);
    }

AtAttSdhManager Tha6A210031AttSdhManagerObjectInit(AtAttSdhManager self, AtModuleSdh sdh)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, sizeof(tThaAttSdhManager));

    /* Super constructor */
    if (ThaAttSdhManagerObjectInit(self, sdh) == NULL)
        return NULL;

    /* Setup class */
    Override(mThis(self));
    m_methodsInit = 1;

    return self;
    }

AtAttSdhManager Tha6A210031AttSdhManagerNew(AtModuleSdh sdh)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtAttSdhManager newObject = mMethodsGet(osal)->MemAlloc(osal, sizeof(tThaAttSdhManager));
    if (newObject == NULL)
        return NULL;

    /* Construct it */
    return Tha6A210031AttSdhManagerObjectInit(newObject, sdh);
    }



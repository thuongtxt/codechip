/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PW
 *
 * File        : Tha6A210031PwActivator.c
 *
 * Created Date: Dec 16, 2015
 *
 * Description : Activator
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtPdhNxDs0.h"
#include "../../../../default/pdh/ThaModulePdh.h"
#include "../../../../default/ocn/ThaModuleOcn.h"
#include "../../../../../generic/common/AtChannelInternal.h"
#include "Tha6A210031PwActivator.h"

/*--------------------------- Define -----------------------------------------*/
#define mThis(self) ((tTha6A210031PwActivator*)self)
#define cNumSlices  2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210031PwActivator
    {
    tThaPwActivator super;
    ThaBitMask *sliceMasks;
    }tTha6A210031PwActivator;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods              m_AtObjectOverride;
static tThaPwActivatorMethods        m_ThaPwActivatorOverride;

/* Save super implementation */
static const tAtObjectMethods       *m_AtObjectMethods       = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 NumPwsPerSlice(ThaPwActivator self)
    {
    return AtModulePwMaxPwsGet((AtModulePw)ThaPwActivatorModuleGet(self)) / cNumSlices;
    }

static ThaBitMask *CreateMasks(ThaPwActivator self)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    uint32 memorySize;
    ThaBitMask *allSliceMasks;
    uint32 slice_i;

    /* Create masks for all slices */
    memorySize = sizeof(ThaBitMask) * cNumSlices;
    allSliceMasks = mMethodsGet(osal)->MemAlloc(osal, memorySize);
    if (allSliceMasks == NULL)
        return NULL;
    mMethodsGet(osal)->MemInit(osal, allSliceMasks, 0, memorySize);

    /* Create mask for each slices */
    for (slice_i = 0; slice_i < cNumSlices; slice_i++)
        {
        allSliceMasks[slice_i] = ThaBitMaskNew(NumPwsPerSlice(self));
        if (allSliceMasks[slice_i] == NULL)
            {
            uint32 existingSlice_i;

            for (existingSlice_i = 0; existingSlice_i < slice_i; existingSlice_i++)
                AtObjectDelete((AtObject)allSliceMasks[existingSlice_i]);
            mMethodsGet(osal)->MemFree(osal, allSliceMasks);

            return NULL;
            }
        }

    return allSliceMasks;
    }

static ThaBitMask MaskForSlice(ThaPwActivator self, uint8 sliceId)
    {
    if (mThis(self)->sliceMasks == NULL)
        mThis(self)->sliceMasks = CreateMasks(self);

    if (mThis(self)->sliceMasks == NULL)
        return NULL;

    return mThis(self)->sliceMasks[sliceId];
    }

static uint32 FreePwForSlice(ThaPwActivator self, uint32 sliceId)
    {
    ThaBitMask mask = MaskForSlice(self, (uint8)sliceId);
    uint32 freeBit = ThaBitMaskFirstZeroBit(mask);
    return ThaBitMaskBitPositionIsValid(mask, freeBit) ? freeBit : cBit31_0;
    }

static uint8 SliceId(ThaPwAdapter adapter)
    {
    uint8 slice, sts;
    AtPw pw = (AtPw)adapter;
    AtSdhChannel sdhChannel = NULL;
    AtChannel circuit = AtPwBoundCircuitGet(pw);

    if ((AtPwTypeGet(pw) == cAtPwTypeSAToP) || (AtPwTypeGet(pw) == cAtPwTypeCESoP))
        {
        ThaPdhChannelHwIdGet((AtPdhChannel)circuit, cAtModulePrbs, &slice, &sts);
        return slice;
        }
    else if (AtPwTypeGet(pw) == cAtPwTypeCEP)
        sdhChannel = (AtSdhChannel)circuit;

    if (sdhChannel == NULL)
        return cBit7_0;

    ThaSdhChannel2HwMasterStsId(sdhChannel, cAtModulePrbs, &slice, &sts);
    return slice;
    }

static ThaHwPw HwPwAllocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    uint32 sliceFreeHwId, sliceHwId;
    uint8 slice = SliceId(adapter);
    ThaBitMask bitMask = MaskForSlice(self, slice);

    if (AtChannelIdGet((AtChannel)adapter) < NumPwsPerSlice(self))
        {
        if (ThaBitMaskBitVal(bitMask, AtChannelIdGet((AtChannel)adapter)) == 0)
            sliceFreeHwId = AtChannelIdGet((AtChannel)adapter);
        else
            sliceFreeHwId = FreePwForSlice(self, slice);
        }
    else
        sliceFreeHwId = FreePwForSlice(self, slice);

    /* No one left */
    if (sliceFreeHwId == cBit31_0)
        return NULL;

    /* Have one, just use it */
    sliceHwId = (slice * NumPwsPerSlice(self)) + sliceFreeHwId;
    ThaBitMaskSetBit(bitMask, sliceFreeHwId);

    return ThaHwPwNew(adapter, sliceHwId);
    }

static eAtRet HwPwDeallocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    uint32 slice;
    uint32 localPwId;

    ThaHwPw hwPw = ThaPwAdapterHwPwGet(adapter);
    if (hwPw == NULL)
        return cAtOk;

    slice      = ThaHwPwIdGet(hwPw) / NumPwsPerSlice(self);
    localPwId  = ThaHwPwIdGet(hwPw) % NumPwsPerSlice(self);
    ThaBitMaskClearBit(MaskForSlice(self, (uint8)slice), localPwId);
    AtObjectDelete((AtObject)hwPw);

    return cAtOk;
    }

static eAtRet ResourceAllocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    ThaHwPw hwPw;

    /* Resource was allocated */
    if (ThaPwAdapterHwPwGet(adapter))
        return cAtOk;

    hwPw = HwPwAllocate(self, adapter);
    if (hwPw == NULL)
        return cAtErrorRsrcNoAvail;
    ThaPwAdapterHwPwSet(adapter, hwPw);

    return cAtOk;
    }

static eAtRet ResourceDeallocate(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret;

    ret = HwPwDeallocate(self, adapter);
    ThaPwAdapterHwPwSet(adapter, NULL);

    return ret;
    }

static eAtRet PwEthPortSet(ThaPwActivator self, ThaPwAdapter adapter, AtEthPort ethPort)
    {
    AtUnused(self);
    AtUnused(adapter);
    AtUnused(ethPort);
    return cAtOk;
    }

static eAtModulePwRet PwCircuitUnbind(ThaPwActivator self, ThaPwAdapter adapter)
    {
    eAtRet ret;
    AtChannel circuit = AtPwBoundCircuitGet((AtPw)adapter);
    AtUnused(self);
    ret = AtChannelBindToPseudowire(circuit, NULL);
    if (ret == cAtOk)
        ret |= ThaPwAdapterBoundCircuitSet(adapter, NULL);

    ret |= ResourceDeallocate(self, adapter);
    return ret;
    }

static eAtModulePwRet PwCircuitBind(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    eAtRet ret = ThaPwActivatorPwCircuitConnect(self, adapter, circuit);
    ret |= ResourceAllocate(self, adapter);
    return ret;
    }

static eAtModulePwRet PwCircuitConnect(ThaPwActivator self, ThaPwAdapter adapter, AtChannel circuit)
    {
    eAtRet ret = cAtOk;
    AtPw pw = (AtPw)adapter;
    AtUnused(self);

    /* Circuit will know how to bind to PW */
    ret = AtChannelBindToPseudowire(circuit, pw);
    if (ret != cAtOk)
        return ret;

    /* Set configuration on binding */
    ret |= AtChannelBoundPwSet(circuit, ThaPwAdapterPwGet(adapter));
    ret |= ThaPwAdapterBoundCircuitSet(adapter, circuit);

    return ret;
    }

static eBool ShouldControlPwBandwidth(ThaPwActivator self)
    {
    /* Some product need disable this feature will control at concrete */
    AtUnused(self);
    return cAtFalse;
    }

static eBool ShouldControlJitterBuffer(ThaPwActivator self)
    {
    /* Some product need disable this feature will control at concrete */
    AtUnused(self);
    return cAtFalse;
    }

static void DeleteMasks(ThaPwActivator self)
    {
    uint32 slice_i;
    AtOsal osal;

    if (mThis(self)->sliceMasks == NULL)
        return;

    for (slice_i = 0; slice_i < cNumSlices; slice_i++)
        AtObjectDelete((AtObject)mThis(self)->sliceMasks[slice_i]);

    osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemFree(osal, mThis(self)->sliceMasks);
    mThis(self)->sliceMasks = NULL;
    }

static void Delete(AtObject self)
    {
    DeleteMasks((ThaPwActivator)self);
    m_AtObjectMethods->Delete(self);
    }

static void OverrideAtObject(ThaPwActivator self)
    {
    AtObject object = (AtObject)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideThaPwActivator(ThaPwActivator self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPwActivatorOverride, mMethodsGet(self), sizeof(m_ThaPwActivatorOverride));

        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitUnbind);
        mMethodOverride(m_ThaPwActivatorOverride, PwEthPortSet);
        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitBind);
        mMethodOverride(m_ThaPwActivatorOverride, PwCircuitConnect);
        mMethodOverride(m_ThaPwActivatorOverride, ShouldControlPwBandwidth);
        mMethodOverride(m_ThaPwActivatorOverride, ShouldControlJitterBuffer);
        }

    mMethodsSet(self, &m_ThaPwActivatorOverride);
    }

static void Override(ThaPwActivator self)
    {
    OverrideAtObject(self);
    OverrideThaPwActivator(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031PwActivator);
    }

static ThaPwActivator ObjectInit(ThaPwActivator self, AtModulePw pwModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPwActivatorObjectInit(self, pwModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaPwActivator Tha6A210031PwActivatorNew(AtModulePw pwModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaPwActivator newActivator = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newActivator == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newActivator, pwModule);
    }

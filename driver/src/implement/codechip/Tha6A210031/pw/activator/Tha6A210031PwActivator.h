/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PW
 * 
 * File        : Tha6A210031PwActivator.h
 * 
 * Created Date: Dec 17, 2015
 *
 * Description : Activator header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210031_PW_ACTIVATOR_THA6A210031PWACTIVATOR_H_
#define _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210031_PW_ACTIVATOR_THA6A210031PWACTIVATOR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../../default/pw/activator/ThaPwActivatorInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaPwActivator Tha6A210031PwActivatorNew(AtModulePw pwModule);

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210031_PW_ACTIVATOR_THA6A210031PWACTIVATOR_H_ */


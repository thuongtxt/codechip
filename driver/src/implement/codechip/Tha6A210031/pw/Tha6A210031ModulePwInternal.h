/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210031ModulePrbsInternal.h
 * 
 * Created Date: Dec 8, 2015
 *
 * Description : Module PRBS internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031MODULEPWINTERNAL_H_
#define _THA6A210031MODULEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/pw/Tha60210031ModulePwInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A210031ModulePw* Tha6A210031ModulePw;
typedef struct tTha6A210031ModulePw
    {
    tTha60210031ModulePw super;
    }tTha6A210031ModulePw;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePw Tha6A210031ModulePwObjectInit(AtModulePw self, AtDevice device);
AtPrbsEngine Tha6A210031PrbsEnginePwObjectInit(AtPrbsEngine self, AtPw pw);
AtPrbsEngine Tha6A210031PrbsEngineVcNew(AtSdhChannel vc);
AtPrbsEngine Tha6A210031PrbsEngineDe1New(AtPdhDe1 de1);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031MODULEPWINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha6A210031ModuleEth.c
 *
 * Created Date: Oct 9, 2015
 *
 * Description : Tha6A210031 ETH
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/eth/Tha60210031ModuleEth.h"
#include "../../../default/eth/ThaEthPort.h"
#include "Tha6A210031ModuleEth.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210031ModuleEth
    {
    tTha60210031ModuleEth super;
    }tTha6A210031ModuleEth;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthMethods   m_AtModuleEthOverride;
static tThaModuleEthMethods  m_ThaModuleEthOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedMultipleLanes(ThaModuleEth self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtEthPort PortCreate(AtModuleEth self, uint8 portId)
    {
    return Tha6A210031EthPortNew(portId, self);
    }

static void OverrideAtModuleEth(AtModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(self), sizeof(m_AtModuleEthOverride));

        mMethodOverride(m_AtModuleEthOverride, PortCreate);
        }

    mMethodsSet(self, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(ThaModuleEth self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(self), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, NeedMultipleLanes);
        }

    mMethodsSet(self, &m_ThaModuleEthOverride);
    }

static void Override(AtModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideThaModuleEth((ThaModuleEth) self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031ModuleEth);
    }

static AtModuleEth Tha6A210031ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (Tha60210031ModuleEthObjectInit(self, device) == NULL)
        return NULL;

    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha6A210031ModuleEthNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    return Tha6A210031ModuleEthObjectInit(newModule, device);
    }

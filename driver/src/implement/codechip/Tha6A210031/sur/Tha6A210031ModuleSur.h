/*-----------------------------------------------------------------------------
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of Arrive 
 * Technologies. The use, copying, transfer or disclosure of such information is 
 * prohibited except by express written agreement with Arrive Technologies. 
 *
 * Module      : SUR
 *
 * File        : Tha6A210031ModuleSur.h
 *
 * Created Date: Sep 8, 2017
 *
 * Description : Surveillance header
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031MODULESUR_H_
#define _THA6A210031MODULESUR_H_

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210031/sur/Tha60210031ModuleSurInternal.h"


#ifdef __cplusplus
extern "C" {
#endif
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Forward declaration ----------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleSur Tha6A210031ModuleSurNew(AtDevice device);

#ifdef __cplusplus
}
#endif

#endif /* _THA6A210031MODULESUR_H_ */

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module SUR
 *
 * File        : Tha60210031ModuleSur.c
 *
 * Created Date: Aug 12, 2015
 *
 * Description : Ethernet module for product 60210031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A210031ModuleSur.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210031ModuleSur
    {
    tTha60210031ModuleSur super;
    }tTha6A210031ModuleSur;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleHardSurMethods m_ThaModuleHardSurOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
/*static AtSurEngine SdhLineSurEngineObjectCreate(ThaModuleHardSur self, AtChannel line)
    {
    AtUnused(self);
    AtUnused(line);
    return NULL;
    }

static AtSurEngine SdhHoPathEngineObjectCreate(ThaModuleHardSur self, AtChannel path)
    {
    AtUnused(self);
    AtUnused(path);
    return NULL;
    }

static AtSurEngine SdhLoPathEngineObjectCreate(ThaModuleHardSur self, AtChannel path)
    {
    AtUnused(self);
    AtUnused(path);
    return NULL;
    }
static AtSurEngine PdhDe3EngineObjectCreate(ThaModuleHardSur self, AtChannel de3)
    {
    AtUnused(self);
    AtUnused(de3);
    return NULL;
    }*/
static AtSurEngine PwEngineObjectCreate(ThaModuleHardSur self, AtChannel pw)
    {
    AtUnused(self);
    AtUnused(pw);
    return NULL;
    }

static void OverrideThaModuleHardSur(ThaModuleHardSur self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();

        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardSurOverride, mMethodsGet(self), sizeof(m_ThaModuleHardSurOverride));
        /*mMethodOverride(m_ThaModuleHardSurOverride, SdhHoPathEngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, SdhLoPathEngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, SdhLineSurEngineObjectCreate);
        mMethodOverride(m_ThaModuleHardSurOverride, PdhDe3EngineObjectCreate);*/
        mMethodOverride(m_ThaModuleHardSurOverride, PwEngineObjectCreate);
        }

    mMethodsSet(self, &m_ThaModuleHardSurOverride);
    }


static void Override(AtModuleSur self)
    {
    OverrideThaModuleHardSur((ThaModuleHardSur)self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031ModuleSur);
    }

static AtModuleSur ObjectInit(AtModuleSur self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210031ModuleSurObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSur Tha6A210031ModuleSurNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSur newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210021PrbsEngineNxDs0.c
 *
 * Created Date: Dec 24, 2015
 *
 * Description : NxDs0 PRBS Engine
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A210031PrbsEnginePwInternal.h"
#include "Tha6A210031ModulePrbsInternal.h"
#include "../../../default/man/ThaDevice.h"
#include "../../../default/pdh/ThaPdhDe1.h"
#include "AtPdhNxDs0.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8                      m_methodsInit = 0;

/* Override */
static tTha61031031PrbsEngineTdmPwMethods m_Tha61031031PrbsEngineTdmPwOverride;
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedSetDefaultPrbsLoss(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedSetDefaultMode(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }


static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static AtPw PwCreate(Tha61031031PrbsEngineTdmPw self, uint32 pwId)
    {
    AtDevice device = AtChannelDeviceGet(AtPrbsEngineChannelGet((AtPrbsEngine)self));
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    return (AtPw)AtModulePwCESoPCreate(pwModule, (uint16)pwId, cAtPwCESoPModeBasic);
    }

static void OverrideTha61031031PrbsEngineTdmPw(AtPrbsEngine self)
    {
    Tha61031031PrbsEngineTdmPw engine = (Tha61031031PrbsEngineTdmPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEngineTdmPwOverride, mMethodsGet(engine), sizeof(m_Tha61031031PrbsEngineTdmPwOverride));

        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, PwCreate);
        }

    mMethodsSet(engine, &m_Tha61031031PrbsEngineTdmPwOverride);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultMode);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultPrbsLoss);
        }
    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }


static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031PrbsEngineNxDs0);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideTha61031031PrbsEngineTdmPw(self);
    }

AtPrbsEngine Tha6A210031PrbsEngineNxDs0ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031PrbsEngineTdmPwObjectInit(self, (AtChannel)nxDs0) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A210031PrbsEngineNxDs0New(AtPdhNxDS0 nxDs0)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A210031PrbsEngineNxDs0ObjectInit(newEngine, nxDs0);
    }

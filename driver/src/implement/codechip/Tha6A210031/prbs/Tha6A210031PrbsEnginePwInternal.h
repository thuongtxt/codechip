/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210031ModulePrbsInternal.h
 * 
 * Created Date: Dec 8, 2015
 *
 * Description : Module PRBS internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031PRBSENGINEPWINTERNAL_H_
#define _THA6A210031PRBSENGINEPWINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha61150011/prbs/Tha61150011PrbsEngine.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

typedef struct tTha6A210031PrbsEnginePw
    {
    tTha61150011PrbsEnginePw super;
    uint32 expectedFixedPattern;
    eBool txEnable;
    eAtPrbsMode txPrbsMode;
    eBool rxEnable;
    eAtPrbsMode rxPrbsMode;
    uint32 lackCounterValue;
    }tTha6A210031PrbsEnginePw;

typedef struct tTha6A210031PrbsEngineDe1
    {
    tTha6A210031PrbsEnginePw super;
    }tTha6A210031PrbsEngineDe1;

typedef struct tTha6A210031PrbsEngineNxDs0
    {
    tTha6A210031PrbsEnginePw super;
    }tTha6A210031PrbsEngineNxDs0;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha6A210031PrbsEnginePwDefaultOffset(AtPrbsEngine self);
AtPrbsEngine Tha6A210031PrbsEngineDe1ObjectInit(AtPrbsEngine self, AtPdhDe1 de1);
AtPrbsEngine Tha6A210031PrbsEngineNxDs0ObjectInit(AtPrbsEngine self, AtPdhNxDS0 nxDs0);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031PRBSENGINEPWINTERNAL_H_ */


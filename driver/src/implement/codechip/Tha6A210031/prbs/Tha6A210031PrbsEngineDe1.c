/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210031PrbsEngineVc.c
 *
 * Created Date: Dec 8, 2015
 *
 * Description : PRBS engine VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha6A210031PrbsEnginePwInternal.h"
#include "AtSdhChannel.h"
#include "../../../default/ocn/ThaModuleOcnInternal.h"
#include "Tha6A210031ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;
static tTha61031031PrbsEngineTdmPwMethods m_Tha61031031PrbsEngineTdmPwOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods    *m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
#define cDs0MaskOfE1ForPrbs  cBit31_1
#define cDs0MaskOfDs1ForPrbs  cBit23_0
/*--------------------------- Implementation ---------------------------------*/
static eBool NeedSetDefaultPrbsLoss(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedSetDefaultMode(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031PrbsEngineDe1);
    }

static eBool IsUnFrameMode(uint16 frameType)
    {
    if ((frameType == cAtPdhE1UnFrm) || (frameType == cAtPdhDs1J1UnFrm))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsDs1FrameMode(uint16 frameValue)
    {
    if ((frameValue == cAtPdhDs1FrmSf) ||
        (frameValue == cAtPdhDs1FrmEsf) ||
        (frameValue == cAtPdhDs1FrmDDS) ||
        (frameValue == cAtPdhDs1FrmSLC))
        return cAtTrue;

    return cAtFalse;
    }

static AtChannel CircuitToBind(Tha61031031PrbsEngineTdmPw self, AtChannel channel)
    {
    uint32 nxDs0BitMask;
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)channel);

    if (frameType == cAtPdhDe1FrameUnknown)
        {
        mChannelLog(channel, cAtLogLevelWarning, "Frame type of circuit is unknown");
        return NULL;
        }

    if (IsUnFrameMode(frameType))
        return channel;

    nxDs0BitMask = IsDs1FrameMode(frameType) ? cDs0MaskOfDs1ForPrbs : cDs0MaskOfE1ForPrbs;
    AtPdhDe1NxDs0Create((AtPdhDe1)AtPrbsEngineChannelGet((AtPrbsEngine)self), nxDs0BitMask);
    return (AtChannel)AtPdhDe1NxDs0Get((AtPdhDe1)channel, nxDs0BitMask);
    }

static AtPw PwCreate(Tha61031031PrbsEngineTdmPw self, uint32 pwId)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);

    if (IsUnFrameMode(AtPdhChannelFrameTypeGet((AtPdhChannel)channel)))
        return (AtPw)AtModulePwSAToPCreate(pwModule, (uint16)pwId);

    /* When E1 is in frame mode, we should create a CESoP instead of SAToP */
    return (AtPw)AtModulePwCESoPCreate(pwModule, (uint16)pwId, cAtPwCESoPModeBasic);
    }

static eAtRet DataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)channel);
    if (IsUnFrameMode(frameType))
        return m_AtPrbsEngineMethods->DataModeSet(self, prbsMode);
    else
        {
        uint32 nxDs0BitMask = IsDs1FrameMode(frameType) ? cDs0MaskOfDs1ForPrbs : cDs0MaskOfE1ForPrbs;
        AtPdhNxDS0 nxds0 = AtPdhDe1NxDs0Get((AtPdhDe1) channel, nxDs0BitMask);
        if (nxds0)
            {
            AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel) nxds0);
            AtPrbsEngineDataModeSet(engine, prbsMode);
            }
        }
    return cAtOk;
    }

static eAtPrbsDataMode DataModeGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)channel);
    if (IsUnFrameMode(frameType))
       return m_AtPrbsEngineMethods->DataModeGet(self);
    else
        {
        uint32 nxDs0BitMask = IsDs1FrameMode(frameType) ? cDs0MaskOfDs1ForPrbs : cDs0MaskOfE1ForPrbs;
        AtPdhNxDS0 nxds0 = AtPdhDe1NxDs0Get((AtPdhDe1) channel, nxDs0BitMask);
        if (nxds0)
            {
            AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel) nxds0);
            return AtPrbsEngineDataModeGet(engine);
            }
        }
    return cAtPrbsDataMode64kb;
    }
static eAtRet TxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
   uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)channel);
   if (IsUnFrameMode(frameType))
       return m_AtPrbsEngineMethods->TxDataModeSet(self, prbsMode);
   else
       {
       uint32 nxDs0BitMask = IsDs1FrameMode(frameType) ? cDs0MaskOfDs1ForPrbs : cDs0MaskOfE1ForPrbs;
       AtPdhNxDS0 nxds0 = AtPdhDe1NxDs0Get((AtPdhDe1) channel, nxDs0BitMask);
       if (nxds0)
           {
           AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel) nxds0);
           AtPrbsEngineTxDataModeSet(engine, prbsMode);
           }
       }
    return cAtOk;
    }

static eAtPrbsDataMode TxDataModeGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)channel);
    if (IsUnFrameMode(frameType))
       return m_AtPrbsEngineMethods->TxDataModeGet(self);
    else
        {
        uint32 nxDs0BitMask = IsDs1FrameMode(frameType) ? cDs0MaskOfDs1ForPrbs : cDs0MaskOfE1ForPrbs;
        AtPdhNxDS0 nxds0 = AtPdhDe1NxDs0Get((AtPdhDe1) channel, nxDs0BitMask);
        if (nxds0)
            {
            AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel) nxds0);
            return AtPrbsEngineTxDataModeGet(engine);
            }
        }
    return cAtPrbsDataMode64kb;
    }

static eAtRet RxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
   uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)channel);
   if (IsUnFrameMode(frameType))
       return m_AtPrbsEngineMethods->RxDataModeSet(self, prbsMode);
   else
       {
       uint32 nxDs0BitMask = IsDs1FrameMode(frameType) ? cDs0MaskOfDs1ForPrbs : cDs0MaskOfE1ForPrbs;
       AtPdhNxDS0 nxds0 = AtPdhDe1NxDs0Get((AtPdhDe1) channel, nxDs0BitMask);
       if (nxds0)
           {
           AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel) nxds0);
           AtPrbsEngineRxDataModeSet(engine, prbsMode);
           }
       }
    return cAtOk;
    }

static eAtPrbsDataMode RxDataModeGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    uint16 frameType = AtPdhChannelFrameTypeGet((AtPdhChannel)channel);
    if (IsUnFrameMode(frameType))
       return m_AtPrbsEngineMethods->RxDataModeGet(self);
    else
        {
        uint32 nxDs0BitMask = IsDs1FrameMode(frameType) ? cDs0MaskOfDs1ForPrbs : cDs0MaskOfE1ForPrbs;
        AtPdhNxDS0 nxds0 = AtPdhDe1NxDs0Get((AtPdhDe1) channel, nxDs0BitMask);
        if (nxds0)
            {
            AtPrbsEngine engine = AtChannelPrbsEngineGet((AtChannel) nxds0);
            return AtPrbsEngineRxDataModeGet(engine);
            }
        }
    return cAtPrbsDataMode64kb;
    }

static void OverrideTha61031031PrbsEngineTdmPw(AtPrbsEngine self)
    {
    Tha61031031PrbsEngineTdmPw engine = (Tha61031031PrbsEngineTdmPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEngineTdmPwOverride, mMethodsGet(engine), sizeof(m_Tha61031031PrbsEngineTdmPwOverride));

        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, PwCreate);
        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, CircuitToBind);
        }

    mMethodsSet(engine, &m_Tha61031031PrbsEngineTdmPwOverride);
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultMode);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultPrbsLoss);
        mMethodOverride(m_AtPrbsEngineOverride, DataModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, DataModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxDataModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxDataModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxDataModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxDataModeGet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine((AtPrbsEngine)self);
    OverrideTha61031031PrbsEngineTdmPw(self);
    }

AtPrbsEngine Tha6A210031PrbsEngineDe1ObjectInit(AtPrbsEngine self, AtPdhDe1 de1)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031PrbsEngineTdmPwObjectInit(self, (AtChannel)de1) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A210031PrbsEngineDe1New(AtPdhDe1 de1)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A210031PrbsEngineDe1ObjectInit(newEngine, de1);
    }

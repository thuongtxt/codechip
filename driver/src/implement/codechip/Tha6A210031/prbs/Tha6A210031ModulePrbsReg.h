/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210031ModulePrbsReg.h
 * 
 * Created Date: Oct 9, 2015
 *
 * Description : PRBS register description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031MODULEPRBSREG_H_
#define _THA6A210031MODULEPRBSREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*
#define cThaPrbsEnable 0x247000
#define cThaPrbsStatus 0x247800
*/
/*------------------------------------------------------------------------------
Reg Name   : PRBS_LOMAP1 Generator Control
Reg Addr   : 0x1B_C000 - 0x1B_C7FF
Reg Formula: 0x1B_C000 + PwId
    Where  :
           + $PwId(0-2048)
Reg Desc   :
Each register is used to configure PRBS generator at MAP.

------------------------------------------------------------------------------*/
#define cThaRegPrbsLoMapGeneratorControl 0x1BC000
/*--------------------------------------
BitField Name: seqstep
BitField Type: RW
BitField Desc: Sequence Step
BitField Bits: [16:10]
--------------------------------------*/
#define cAtt_prbslogenctl1_gen_cas_en_Mask                                   cBit17
#define cAtt_prbslogenctl1_gen_cas_en_Shift                                      17

/*--------------------------------------
BitField Name: seqstep
BitField Type: RW
BitField Desc: Sequence Step
BitField Bits: [16:10]
--------------------------------------*/
#define cAtt_prbslogenctl1_seqstep_Mask                                cBit16_10
#define cAtt_prbslogenctl1_seqstep_Shift                                      10

/*--------------------------------------
BitField Name: seqdat
BitField Type: RW
BitField Desc: Sequence data
BitField Bits: [9:2]
--------------------------------------*/
#define cAtt_prbslogenctl1_seqdat_Mask                                   cBit9_2
#define cAtt_prbslogenctl1_seqdat_Shift                                        2

/*--------------------------------------
BitField Name: PrbsLoGenCtlSeqMod
BitField Type: RW
BitField Desc: Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode
BitField Bits: [1]
--------------------------------------*/
#define cThaRegPrbsLoGenCtlSeqModMask                                      cBit1
#define cThaRegPrbsLoGenCtlSeqModShift                                         1

/*--------------------------------------
BitField Name: PrbsLoGenCtlEnb
BitField Type: RW
BitField Desc: Enable PRBS generator. 1: Enable 0: Disable
BitField Bits: [0]
--------------------------------------*/
#define cThaRegPrbsLoGenCtlEnbMask                                         cBit0
#define cThaRegPrbsLoGenCtlEnbShift                                            0

/*------------------------------------------------------------------------------
Reg Name   : PRBS_LOMAP1 Monitor
Reg Addr   : 0x1A_C000 - 0x1A_C7FF
Reg Formula: 0x1A_C000 + PwId
    Where  :
           + $PwId(0-2048)
Reg Desc   :
Each register is used for PRBS monitor at DEMAP.

------------------------------------------------------------------------------*/
#define cThaRegPrbsLoMapMonitor          0x1AC000

#define cAtt_prbslomon1_shiftcount_Mask          cBit31_27
#define cAtt_prbslomon1_shiftcount_Shift         27

/*--------------------------------------
BitField Name: PrbsLoMonErr
BitField Type: RC
BitField Desc: PRBS error. 1: Error 0: Not error
BitField Bits: [17]
--------------------------------------*/
#define cThaRegPrbsLoMon_cas_en_Mask          cBit20
#define cThaRegPrbsLoMon_cas_en_Shift         20

/*--------------------------------------
BitField Name: PrbsLoMonErr
BitField Type: RC
BitField Desc: PRBS error. 1: Error 0: Not error
BitField Bits: [17]
--------------------------------------*/
#define cThaRegPrbsLoMonErrMask          cBit17
#define cThaRegPrbsLoMonErrShift         17

/*--------------------------------------
BitField Name: PrbsLoMonSync
BitField Type: RO
BitField Desc: PRBS monitor sync. Only used for PRBS mode 1: Data PRBS sync (Not
error) 0: Not sync
BitField Bits: [16]
--------------------------------------*/
#define cThaRegPrbsLoMonSyncMask         cBit16
#define cThaRegPrbsLoMonSyncShift        16

/*--------------------------------------
BitField Name: seqstep
BitField Type: RW
BitField Desc: Sequence Step
BitField Bits: [15:9]
--------------------------------------*/
#define cAtt_prbslomon1_seqstep_Mask                                                                     cBit15_9
#define cAtt_prbslomon1_seqstep_Shift                                                                           9

/*--------------------------------------
BitField Name: seqdat
BitField Type: RW
BitField Desc: Sequence data
BitField Bits: [8:1]
--------------------------------------*/
#define cAtt_prbslomon1_seqdat_Mask                                                                       cBit8_1
#define cAtt_prbslomon1_seqdat_Shift                                                                            1

/*--------------------------------------
BitField Name: PrbsLoMonCtlSeqMod
BitField Type: RW
BitField Desc: Configure types of PRBS generator . 1: Sequence mode 0: PRBS mode
BitField Bits: [0]
--------------------------------------*/
#define cThaRegPrbsLoMonCtlSeqModMask    cBit0
#define cThaRegPrbsLoMonCtlSeqModShift   0

#ifdef __cplusplus
}
#endif
#endif /* _DRIVER_SRC_IMPLEMENT_CODECHIP_THA6A210031_PRBS_THA6A210031MODULEPRBSREG_H_ */


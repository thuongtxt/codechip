/*------------------------------------------------------------------------------
 *                                                                              
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.                                  
 *                                                                              
 * The information contained herein is confidential property of Arrive          
 * Technologies. The use, copying, transfer or disclosure of such information   
 * is prohibited except by express written agreement with Arrive Technologies.  
 *                                                                              
 * Module      :                                                                
 *                                                                              
 * File        :                                                                
 *                                                                              
 * Created Date: March 28, 2018                                                 
 *                                                                              
 * Description : This file contain all constance definitions of  block.         
 *                                                                              
 * Notes       : None                                                           
 *----------------------------------------------------------------------------*/
#ifndef _Tha6A210031PRBSLOREG_H_
#define _Tha6A210031PRBSLOREG_H_

/*--------------------------- Define -----------------------------------------*/


/*------------------------------------------------------------------------------
Reg Name   : PRBS Generator Control Register For Low Order
Reg Addr   : 0x50_1000
Reg Formula: 0x50_1000 + $slice*8192 + $pwid
    Where  : 
           + $pwid(0-1023):pseudowire id
           + $slice (0-1):slice
Reg Desc   : 
this register is used to config mode for PRBS GEN and get status of PRBS in case of mode PRBS

------------------------------------------------------------------------------*/
#define cAf6Reg_prbsgenctrl4lo                                                                        0x501000

/*--------------------------------------
BitField Name: oprbs
BitField Type: RO
BitField Desc: Current PRBS value
BitField Bits: [16:2]
--------------------------------------*/
#define cAf6_prbsgenctrl4lo_oprbs_Mask                                                                cBit16_2
#define cAf6_prbsgenctrl4lo_oprbs_Shift                                                                      2

/*--------------------------------------
BitField Name: cfgmode3
BitField Type: RW
BitField Desc: data gen mode. 2'b0: Fix-pattern mode 3 bytes 2'b1: Fix-pattern
mode 1-2-4 bytes 2'b2: PRBS mode 2'b3: Sequence mode
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_prbsgenctrl4lo_cfgmode3_Mask                                                              cBit1_0
#define cAf6_prbsgenctrl4lo_cfgmode3_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : PRBS Monitor Control Register For Low Order
Reg Addr   : 0x50_0000
Reg Formula: 0x50_0000 + $slice*8192 + $pwid
    Where  : 
           + $pwid(0-1023):pseudowire id
           + $slice (0-1):slice
Reg Desc   : 
this register is used to config mode for PRBS MON and get monitoring status

------------------------------------------------------------------------------*/
#define cAf6Reg_prbsmonctrl4lo                                                                        0x500000

/*--------------------------------------
BitField Name: ovld
BitField Type: RC
BitField Desc: 1/0 (vld/no vld). No valid means no data received.
BitField Bits: [18]
--------------------------------------*/
#define cAf6_prbsmonctrl4lo_ovld_Mask                                                                   cBit18
#define cAf6_prbsmonctrl4lo_ovld_Shift                                                                      18

/*--------------------------------------
BitField Name: oerr
BitField Type: RC
BitField Desc: 1/0 (error/error free)
BitField Bits: [17]
--------------------------------------*/
#define cAf6_prbsmonctrl4lo_oerr_Mask                                                                   cBit17
#define cAf6_prbsmonctrl4lo_oerr_Shift                                                                      17

/*--------------------------------------
BitField Name: osyn
BitField Type: RO
BitField Desc: successfully synchronized (for PRBS & sequence mode)
BitField Bits: [16]
--------------------------------------*/
#define cAf6_prbsmonctrl4lo_osyn_Mask                                                                   cBit16
#define cAf6_prbsmonctrl4lo_osyn_Shift                                                                      16

/*--------------------------------------
BitField Name: oprbs
BitField Type: RO
BitField Desc: Current PRBS value
BitField Bits: [15:2]
--------------------------------------*/
#define cAf6_prbsmonctrl4lo_oprbs_Mask                                                                cBit15_2
#define cAf6_prbsmonctrl4lo_oprbs_Shift                                                                      2

/*--------------------------------------
BitField Name: cfgmode3
BitField Type: RW
BitField Desc: data mon mode. 2'b0: Fix-pattern mode 3 bytes 2'b1: Fix-pattern
mode 1-2-4 bytes 2'b2: PRBS mode 2'b3: Sequence mode
BitField Bits: [1:0]
--------------------------------------*/
#define cAf6_prbsmonctrl4lo_cfgmode3_Mask                                                              cBit1_0
#define cAf6_prbsmonctrl4lo_cfgmode3_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : PRBS Generator Fix-Pattern Dat
Reg Addr   : 0x50_1400
Reg Formula: 0x50_1400 + $slice*8192 + $pwid
    Where  : 
           + $pwid(0-1023):pseudowire id
           + $slice (0-1):slice
Reg Desc   : 
this register is used to config fix-pattern data for fix-pattern mode

------------------------------------------------------------------------------*/
#define cAf6Reg_prbsgenfxptdatlo                                                                      0x501400

/*--------------------------------------
BitField Name: fxptdat
BitField Type: RW
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prbsgenfxptdatlo_fxptdat_Mask                                                            cBit31_0
#define cAf6_prbsgenfxptdatlo_fxptdat_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : PRBS Monitor Fix-Pattern Dat
Reg Addr   : 0x50_0400
Reg Formula: 0x50_0400 + $slice*8192 + $pwid
    Where  : 
           + $pwid(0-1023):pseudowire id
           + $slice (0-1):slice
Reg Desc   : 
this register is used to config fix-pattern data for fix-pattern mode

------------------------------------------------------------------------------*/
#define cAf6Reg_prbsmonfxptdatlo                                                                      0x500400

/*--------------------------------------
BitField Name: fxptdat
BitField Type: RW
BitField Desc:
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_prbsmonfxptdatlo_fxptdat_Mask                                                            cBit31_0
#define cAf6_prbsmonfxptdatlo_fxptdat_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Round-trip delay channel config
Reg Addr   : 0x50_1800
Reg Formula: 0x50_1800 + $slice*8192
    Where  : 
           + $slice (0-1):slice
Reg Desc   : 
this register is used to config Round-trip delay channel id to be measured.

------------------------------------------------------------------------------*/
#define cAf6Reg_rtmcidcfglo                                                                           0x501800
#define cAf6Reg_rtmcidcfglo_WidthVal                                                                        32

/*--------------------------------------
BitField Name: rtmcidcfg
BitField Type: RW
BitField Desc: pseudo wire id config
BitField Bits: [10:1]
--------------------------------------*/
#define cAf6_rtmcidcfglo_rtmcidcfg_Mask                                                               cBit10_1
#define cAf6_rtmcidcfglo_rtmcidcfg_Shift                                                                     1

/*--------------------------------------
BitField Name: rtmen
BitField Type: RW
BitField Desc: roundtrip delay mode en
BitField Bits: [0]
--------------------------------------*/
#define cAf6_rtmcidcfglo_rtmen_Mask                                                                      cBit0
#define cAf6_rtmcidcfglo_rtmen_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Monitor Counter Read Only
Reg Addr   : 0x50_0800
Reg Formula: 0x50_0800 + $slice*8192 + $pwid
    Where  : 
           + $pwid(0-1023):pseudowire id
           + $slice (0-1):slice
Reg Desc   : 
this register is used to accummulate good and bad monitor counter

------------------------------------------------------------------------------*/
#define cAf6Reg_moncntrolo                                                                            0x500800

/*--------------------------------------
BitField Name: goodcnt
BitField Type: RO
BitField Desc: good counter
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_moncntrolo_goodcnt_Mask                                                                 cBit31_16
#define cAf6_moncntrolo_goodcnt_Shift                                                                       16

/*--------------------------------------
BitField Name: badcnt
BitField Type: RO
BitField Desc: bad counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_moncntrolo_badcnt_Mask                                                                   cBit15_0
#define cAf6_moncntrolo_badcnt_Shift                                                                         0


/*------------------------------------------------------------------------------
Reg Name   : Monitor Counter Read To Clear
Reg Addr   : 0x50_0c00
Reg Formula: 0x50_0c00 + $slice*8192 + $pwid
    Where  : 
           + $pwid(0-1023):pseudowire id
           + $slice (0-1):slice
Reg Desc   : 
this register is used to accummulate good and bad monitor counter

------------------------------------------------------------------------------*/
#define cAf6Reg_moncntr2clo                                                                           0x500c00

/*--------------------------------------
BitField Name: goodcnt
BitField Type: RC
BitField Desc: good counter
BitField Bits: [31:16]
--------------------------------------*/
#define cAf6_moncntr2clo_goodcnt_Mask                                                                cBit31_16
#define cAf6_moncntr2clo_goodcnt_Shift                                                                      16

/*--------------------------------------
BitField Name: badcnt
BitField Type: RC
BitField Desc: bad counter
BitField Bits: [15:0]
--------------------------------------*/
#define cAf6_moncntr2clo_badcnt_Mask                                                                  cBit15_0
#define cAf6_moncntr2clo_badcnt_Shift                                                                        0


/*------------------------------------------------------------------------------
Reg Name   : Max Delay Register Read Only
Reg Addr   : 0x50_1c00
Reg Formula: 0x50_1c00 + $slice*8192
    Where  : 
           + $slice (0-1):slice
Reg Desc   : 
this register is used to accummulate max delay for round trip mode

------------------------------------------------------------------------------*/
#define cAf6Reg_mxdelayregrolo                                                                        0x501c00

/*--------------------------------------
BitField Name: maxdelay
BitField Type: RO
BitField Desc: max delay
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mxdelayregrolo_maxdelay_Mask                                                             cBit31_0
#define cAf6_mxdelayregrolo_maxdelay_Shift                                                                   0


/*------------------------------------------------------------------------------
Reg Name   : Max Delay Register Read toclear
Reg Addr   : 0x50_1c04
Reg Formula: 0x50_1c04 + $slice*8192
    Where  : 
           + $slice (0-1):slice
Reg Desc   : 
this register is used to accummulate max delay for round trip mode

------------------------------------------------------------------------------*/
#define cAf6Reg_mxdelayregr2clo                                                                       0x501c04

/*--------------------------------------
BitField Name: maxdelay
BitField Type: RC
BitField Desc: max delay
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mxdelayregr2clo_maxdelay_Mask                                                            cBit31_0
#define cAf6_mxdelayregr2clo_maxdelay_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Min Delay Register Read Only
Reg Addr   : 0x50_1c01
Reg Formula: 0x50_1c01 + $slice*8192
    Where  : 
           + $slice (0-1):slice
Reg Desc   : 
this register is used to accummulate min delay for round trip mode

------------------------------------------------------------------------------*/
#define cAf6Reg_mindelayregrolo                                                                       0x501c01

/*--------------------------------------
BitField Name: mindelay
BitField Type: RO
BitField Desc: min delay
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mindelayregrolo_mindelay_Mask                                                            cBit31_0
#define cAf6_mindelayregrolo_mindelay_Shift                                                                  0


/*------------------------------------------------------------------------------
Reg Name   : Min Delay Register Read Only
Reg Addr   : 0x50_1c05
Reg Formula: 0x50_1c05 + $slice*8192
    Where  : 
           + $slice (0-1):slice
Reg Desc   : 
this register is used to accummulate min delay for round trip mode

------------------------------------------------------------------------------*/
#define cAf6Reg_mindelayregr2clo                                                                         0x501c05

/*--------------------------------------
BitField Name: mindelay
BitField Type: RC
BitField Desc: min delay
BitField Bits: [31:0]
--------------------------------------*/
#define cAf6_mindelayregr2clo_mindelay_Mask                                                              cBit31_0
#define cAf6_mindelayregr2clo_mindelay_Shift                                                                    0

#endif /* _Tha6A210031PRBSLOREG_H_ */

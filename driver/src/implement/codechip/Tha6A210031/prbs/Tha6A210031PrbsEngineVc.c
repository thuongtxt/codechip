/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210031PrbsEngineVc.c
 *
 * Created Date: Dec 8, 2015
 *
 * Description : PRBS engine VC
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "AtSdhChannel.h"
#include "../../../default/ocn/ThaModuleOcnInternal.h"
#include "../../Tha61031031/prbs/Tha61031031PrbsEngine.h"
#include "Tha6A210031ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210031PrbsEngineVc
    {
    tTha61031031PrbsEngineTdmPw super;
    }tTha6A210031PrbsEngineVc;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;
static tTha61031031PrbsEngineTdmPwMethods m_Tha61031031PrbsEngineTdmPwOverride;

/* Save super implementation */
static const tAtPrbsEngineMethods * m_AtPrbsEngineMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedSetDefaultPrbsLoss(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedSetDefaultMode(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }


static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031PrbsEngineVc);
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static AtPw PwCreate(Tha61031031PrbsEngineTdmPw self, uint32 pwId)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    AtDevice device = AtChannelDeviceGet(channel);
    AtModulePw pwModule = (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    return (AtPw)AtModulePwCepCreate(pwModule, pwId, cAtPwCepModeBasic);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    uint8 sts_i;
    uint8 startSts;
    AtSdhChannel sdhChannel = (AtSdhChannel)AtPrbsEngineChannelGet((AtPrbsEngine)self);
    eAtRet ret = m_AtPrbsEngineMethods->Enable(self, enable);
    if (ret != cAtOk)
        return ret;

    if ((AtSdhChannelTypeGet(sdhChannel) == cAtSdhChannelTypeVc11) ||
        (AtSdhChannelTypeGet(sdhChannel) == cAtSdhChannelTypeVc12))
        ThaOcnVtPohInsertEnable(sdhChannel, (uint8)AtSdhChannelSts1Get(sdhChannel), AtSdhChannelTug2Get(sdhChannel), AtSdhChannelTu1xGet(sdhChannel), cAtTrue);
    else
        {
        startSts = AtSdhChannelSts1Get(sdhChannel);
        for (sts_i = 0; sts_i < AtSdhChannelNumSts(sdhChannel); sts_i++)
            {
            ret |= ThaOcnStsPohInsertEnable(sdhChannel, (uint8)(startSts + sts_i), cAtTrue);
            if (AtSdhChannelTypeGet(sdhChannel) == cAtSdhChannelTypeVc3)
                {
                ThaOcnVtPohInsertEnable(sdhChannel, (uint8)(startSts + sts_i), 0, 0, cAtTrue);
                }
            }
        }

    ret |= AtSdhPathTxPslSet((AtSdhPath)sdhChannel, 0x2);
    ret |= AtSdhPathExpectedPslSet((AtSdhPath)sdhChannel, 0x2);

    return ret;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultMode);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultPrbsLoss);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideTha61031031PrbsEngineTdmPw(AtPrbsEngine self)
    {
    Tha61031031PrbsEngineTdmPw engine = (Tha61031031PrbsEngineTdmPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEngineTdmPwOverride, mMethodsGet(engine), sizeof(m_Tha61031031PrbsEngineTdmPwOverride));

        mMethodOverride(m_Tha61031031PrbsEngineTdmPwOverride, PwCreate);
        }

    mMethodsSet(engine, &m_Tha61031031PrbsEngineTdmPwOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideTha61031031PrbsEngineTdmPw(self);
    }

static AtPrbsEngine Tha6A210031PrbsEngineVcObjectInit(AtPrbsEngine self, AtSdhChannel vc)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61031031PrbsEngineTdmPwObjectInit(self, (AtChannel)vc) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A210031PrbsEngineVcNew(AtSdhChannel vc)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A210031PrbsEngineVcObjectInit(newEngine, vc);
    }

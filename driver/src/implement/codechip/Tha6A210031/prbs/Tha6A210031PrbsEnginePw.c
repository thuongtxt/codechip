/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210031PrbsEnginePw.c
 *
 * Created Date: Oct 9, 2015
 *
 * Description : Tha6A210031 PRBS PW
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/ThaModulePw.h"
#include "../../../default/pw/adapters/ThaPwAdapter.h"
#include "../../../default/pw/activator/ThaHwPw.h"
#include "Tha6A210031ModulePrbsReg.h"
#include "Tha6A210031ModulePrbsInternal.h"
#include "Tha6A210031PrbsEnginePwInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha6A210031PrbsEnginePw *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;
static tTha61150011PrbsEnginePwMethods    m_Tha61150011PrbsEnginePwOverride;
static tTha61031031PrbsEngineTdmPwMethods m_Tha61031031PrbsEnginePwOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/

static eBool NeedSetDefaultPrbsLoss(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedSetDefaultMode(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static Tha6A210031ModulePrbs PrbsModule(AtPrbsEngine self)
    {
    AtDevice device = AtPrbsEngineDeviceGet((AtPrbsEngine)self);
    return (Tha6A210031ModulePrbs) AtDeviceModuleGet(device, cAtModulePrbs);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031PrbsEnginePw);
    }

static uint32 PrbsSliceOffset(Tha61150011PrbsEnginePw self, uint8 slice)
    {
    AtUnused(self);
    return slice * 0x20000UL;
    }

static uint32 DefaultOffset(AtPrbsEngine self)
    {
    uint32 sliceOffset;
    AtPw pw = (AtPw)AtPrbsEngineChannelGet(self);
    Tha61150011PrbsEnginePw engine = (Tha61150011PrbsEnginePw)self;
    ThaHwPw hwPw = ThaPwAdapterHwPwGet(ThaPwAdapterGet(pw));
    uint32 numPwsPerSlice = AtModulePwMaxPwsGet((AtModulePw)AtChannelModuleGet((AtChannel)pw)) / 2;
    uint8 slice = (uint8)(ThaHwPwIdGet(hwPw) / numPwsPerSlice);

    sliceOffset = mMethodsGet(engine)->PrbsSliceOffset(engine, slice);
    return sliceOffset + (ThaHwPwIdGet(hwPw) % numPwsPerSlice);
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + DefaultOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    mRegFieldSet(regVal, cThaRegPrbsLoGenCtlEnb, mBoolToBin(enable));
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + DefaultOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    return mBinToBool(mRegField(regVal, cThaRegPrbsLoGenCtlEnb));
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    uint32 alarms = cAtPrbsEngineAlarmTypeNone;
    uint32 regAddr = cThaRegPrbsLoMapMonitor + DefaultOffset(self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr , cAtModulePrbs);

    if (mRegField(regVal, cThaRegPrbsLoMonSync) == 0)
        alarms |= cAtPrbsEngineAlarmTypeLossSync;

    if (mRegField(regVal, cThaRegPrbsLoMonErr) == 1)
        alarms |= cAtPrbsEngineAlarmTypeError;

    if (AtPrbsModeIsFixedPattern(AtPrbsEngineRxModeGet(self)))
        {
        uint32 expectVal  = AtPrbsEngineRxFixedPatternGet(self);
        uint32 shiftcount = mRegField(regVal, cAtt_prbslomon1_shiftcount_);
        uint32 rxVal      = mRegField(regVal, cAtt_prbslomon1_seqdat_);
        uint32 shiftVal   = (((rxVal << 8)|rxVal)>>shiftcount);
        if ((shiftVal  & 0xFF)  != expectVal)
            alarms |= cAtPrbsEngineAlarmTypeFixPatternError;
        }
    return alarms;
    }

static eAtModulePrbsRet ErrorForce(AtPrbsEngine self, eBool force)
    {
    AtUnused(self);
    AtUnused(force);
    return cAtOk;
    }

static eBool ErrorIsForced(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    eAtRet ret = cAtOk;

    ret |= AtPrbsEngineTxModeSet(self, prbsMode);
    ret |= AtPrbsEngineRxModeSet(self, prbsMode);

    return ret;
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    eAtPrbsMode mode = AtPrbsEngineRxModeGet(self);
    return (mode == AtPrbsEngineTxModeGet(self)) ? mode : cAtPrbsModeInvalid;
    }

static eBool PrbsModeIsValid(eAtPrbsMode prbsMode)
    {
    return ((prbsMode == cAtPrbsModePrbs15) || (prbsMode == cAtPrbsModePrbsSeq)|| (prbsMode == cAtPrbsModePrbsFixedPattern1Byte)) ? cAtTrue : cAtFalse;
    }

static uint8 HwStepFromPrbsMode(eAtPrbsMode prbsMode)
    {
    uint8 step = 0;
    if (prbsMode==cAtPrbsModePrbsSeq)
        step = 1;
    return step;
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regVal;
    uint32 regAddr;
    Tha6A210031ModulePrbs prbs = PrbsModule(self);

    if (PrbsModeIsValid(prbsMode) == cAtFalse)
        return cAtErrorModeNotSupport;

    regAddr = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + DefaultOffset(self);
    regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);

    mRegFieldSet(regVal, cThaRegPrbsLoGenCtlSeqMod, (prbsMode == cAtPrbsModePrbs15) ? 0 : 1);
    mRegFieldSet(regVal, cAtt_prbslogenctl1_seqstep_, HwStepFromPrbsMode(prbsMode));

    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtPrbsMode PrbsModeFromHwValue(uint8 mode, uint8 step)
    {
    if (mode == 0)
        return cAtPrbsModePrbs15;
    else
        return (step ? cAtPrbsModePrbsSeq : cAtPrbsModePrbsFixedPattern1Byte);
    }

static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + DefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint8  mode = (uint8) mRegField(regVal, cThaRegPrbsLoGenCtlSeqMod);
    uint8  step = (uint8) mRegField(regVal, cAtt_prbslogenctl1_seqstep_);
    return PrbsModeFromHwValue(mode, step);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    uint32 regVal;
    uint32 regAddr;
    uint32 regFieldMask, regFieldShift = 0;
    Tha6A210031ModulePrbs prbs = PrbsModule(self);

    mThis(self)->rxPrbsMode = prbsMode;

    if (!Tha6A210031ModulePrbsModeIsSupported(prbs, prbsMode))
        return cAtErrorModeNotSupport;

    regAddr = Tha6A210031ModulePrbsMonReg(prbs, cAtFalse) + DefaultOffset(self);
    regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    regFieldMask   = Tha6A210031ModulePrbsModeMask(prbs, cAtFalse, cAtFalse);
    regFieldShift  = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(regVal, regField, (prbsMode == cAtPrbsModePrbs15) ? 0 : 1);

    regFieldMask   = Tha6A210031ModulePrbsStepMask(prbs, cAtFalse, cAtFalse);
    if (regFieldMask!= cInvalidUint32)
        {
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal, regField,  Tha6A210031ModulePrbsHwStepFromPrbsMode(prbs, prbsMode));
        }
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr = Tha6A210031ModulePrbsMonReg(prbs, cAtFalse) + DefaultOffset(self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask, regFieldShift = 0;
    uint8 mode, step =0;

    regFieldMask   = Tha6A210031ModulePrbsModeMask(prbs, cAtFalse, cAtFalse);
    regFieldShift  = AtRegMaskToShift(regFieldMask);
    mode     = (uint8) mRegField(regVal, regField);

    regFieldMask   = Tha6A210031ModulePrbsStepMask(prbs, cAtFalse, cAtFalse);
    if (regFieldMask!= cInvalidUint32)
        {
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        step     = (uint8)mRegField(regVal, regField);
        }
    return Tha6A210031ModulePrbsModeFromHwValue(prbs, mThis(self)->rxPrbsMode, mode, step);
    }


static eAtRet DataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    eAtRet ret = cAtOk;

    ret |= AtPrbsEngineTxDataModeSet(self, prbsMode);
    ret |= AtPrbsEngineRxDataModeSet(self, prbsMode);

    return ret;
    }

static eAtPrbsDataMode DataModeGet(AtPrbsEngine self)
    {
    eAtPrbsDataMode mode = AtPrbsEngineRxDataModeGet(self);
    return (mode == AtPrbsEngineTxDataModeGet(self)) ? mode : cAtPrbsModeInvalid;
    }


static eAtRet TxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    uint32 regVal;
    uint32 regAddr;
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regFieldMask,regFieldShift;

    if (!Tha6A210031ModulePrbsDataModeIsSupported(prbs))
        return cAtErrorModeNotSupport;
    regAddr = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + DefaultOffset(self);
    regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    regFieldMask   = Tha6A210031ModulePrbsDataModeMask(prbs, cAtFalse, cAtTrue);
    regFieldShift = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(regVal, regField, (prbsMode == cAtPrbsDataMode64kb) ? 0 : 1);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtPrbsDataMode TxDataModeGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr             = Tha6A210031ModulePrbsGenReg(prbs, cAtFalse) + DefaultOffset(self);
    uint32 regVal              = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask,regFieldShift;
    uint8  mode;
    if (!Tha6A210031ModulePrbsDataModeIsSupported(prbs))
        return cAtPrbsDataModeInvalid;

    regFieldMask   = Tha6A210031ModulePrbsDataModeMask(prbs, cAtFalse, cAtTrue);
    regFieldShift  = AtRegMaskToShift(regFieldMask);
    mode = (uint8) mRegField(regVal, regField);
    return mode == 0? cAtPrbsDataMode64kb: cAtPrbsDataMode56kb;
    }

static eAtRet RxDataModeSet(AtPrbsEngine self, eAtPrbsDataMode prbsMode)
    {
    uint32 regVal;
    uint32 regAddr;
    uint32 regFieldMask, regFieldShift = 0;
    Tha6A210031ModulePrbs prbs = PrbsModule(self);

    if (!Tha6A210031ModulePrbsDataModeIsSupported(prbs))
        return cAtErrorModeNotSupport;

    regAddr = Tha6A210031ModulePrbsMonReg(prbs, cAtFalse) + DefaultOffset(self);
    regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    regFieldMask   = Tha6A210031ModulePrbsDataModeMask(prbs, cAtFalse, cAtFalse);
    regFieldShift  = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(regVal, regField, (prbsMode == cAtPrbsDataMode64kb ? 0 : 1));

    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtPrbsDataMode RxDataModeGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr = Tha6A210031ModulePrbsMonReg(prbs, cAtFalse) + DefaultOffset(self);
    uint32 regVal  = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask, regFieldShift = 0;
    uint8 mode;
    if (!Tha6A210031ModulePrbsDataModeIsSupported(prbs))
           return cAtPrbsDataModeInvalid;
    regFieldMask   = Tha6A210031ModulePrbsDataModeMask(prbs, cAtFalse, cAtFalse);
    regFieldShift  = AtRegMaskToShift(regFieldMask);
    mode     = (uint8) mRegField(regVal, regField);

    return mode == 0? cAtPrbsDataMode64kb: cAtPrbsDataMode56kb;
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    eAtModulePrbsRet ret = cAtOk;
    ret |= AtPrbsEngineTxFixedPatternSet(self, fixedPattern);
    ret |= AtPrbsEngineRxFixedPatternSet(self, fixedPattern);
    return ret;
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    return AtPrbsEngineRxFixedPatternGet(self);
    }

static uint32 RxCurrentFixedPatternGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr = Tha6A210031ModulePrbsMonReg(prbs, cAtFalse) + DefaultOffset(self);
    uint32 regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask  = Tha6A210031ModulePrbsFixPatternMask(prbs, cAtFalse, cAtFalse);
    uint32 regFieldShift = AtRegMaskToShift(regFieldMask);

    return mRegField(regVal, regField);
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    eAtModulePrbsRet ret = cAtOk;
    Tha6A210031ModulePrbs prbs = PrbsModule(self);

    uint32 regAddr       = Tha6A210031ModulePrbsMonReg(prbs, cAtFalse) + DefaultOffset(self);
    uint32 regVal        = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask  = Tha6A210031ModulePrbsFixPatternMask(prbs, cAtFalse, cAtTrue);
    uint32 regFieldShift = AtRegMaskToShift(regFieldMask);

    mRegFieldSet(regVal, regField,  fixedPattern);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return ret;
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr       = Tha6A210031ModulePrbsMonReg(prbs, cAtFalse) + DefaultOffset(self);
    uint32 regVal        = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    uint32 regFieldMask  = Tha6A210031ModulePrbsFixPatternMask(prbs, cAtFalse, cAtTrue);
    uint32 regFieldShift = AtRegMaskToShift(regFieldMask);

    return mRegField(regVal, regField);
    }

static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    eAtModulePrbsRet      ret  = cAtOk;
    Tha6A210031ModulePrbs prbs = PrbsModule(self);

    if (!mMethodsGet(prbs)->NeedFixedPattern4bytes(prbs))
        mThis(self)->expectedFixedPattern = fixedPattern;
    else
        {
        uint32 hwAddress = Tha6A210031ModulePrbsFixPatternReg(prbs, cAtFalse, cAtFalse) ;
        uint32 regAddr   = hwAddress + DefaultOffset(self);
        uint32 regVal    = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        uint32 regFieldMask, regFieldShift = 0;

        regFieldMask   = Tha6A210031ModulePrbsFixPatternMask(prbs, cAtFalse, cAtFalse);
        regFieldShift  = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal, regField, Tha6A210031ModulePrbsConvertSwToHwFixedPattern(prbs,mThis(self)->rxPrbsMode, fixedPattern));
        AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return ret;
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);

    if (!mMethodsGet(prbs)->NeedFixedPattern4bytes(prbs))
        return mThis(self)->expectedFixedPattern;

    else
        {
        uint32 regFieldMask, regFieldShift = 0;
        uint32 hwAddress = Tha6A210031ModulePrbsFixPatternReg(prbs, cAtFalse, cAtFalse);
        uint32 regAddr   = hwAddress + DefaultOffset(self);
        uint32 regVal    = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        regFieldMask     = Tha6A210031ModulePrbsFixPatternMask(prbs, cAtFalse, cAtFalse);
        regFieldShift    = AtRegMaskToShift(regFieldMask);
        return Tha6A210031ModulePrbsConvertHwToSwFixedPattern(prbs,mThis(self)->rxPrbsMode, mRegField(regVal, regField));
        }
    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet TxInvert(AtPrbsEngine self, eBool invert)
    {
    AtUnused(self);
    if (invert==cAtTrue)
        return cAtErrorModeNotSupport;
    return cAtOk;
    }

static eBool TxIsInverted(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static eAtModulePrbsRet RxInvert(AtPrbsEngine self, eBool invert)
    {
    return AtPrbsEngineTxInvert(self, invert);
    }

static eBool RxIsInverted(AtPrbsEngine self)
    {
    return AtPrbsEngineTxIsInverted(self);
    }

static eAtModulePrbsRet Invert(AtPrbsEngine self, eBool invert)
    {
    return AtPrbsEngineTxInvert(self, invert);
    }

static eBool IsInverted(AtPrbsEngine self)
    {
    return AtPrbsEngineTxIsInverted(self);
    }

static uint8 PrbsEngineSliceGet(AtPrbsEngine self, AtPw pw)
    {
    ThaHwPw hwPw = ThaPwAdapterHwPwGet(ThaPwAdapterGet(pw));
    uint32 numPwsPerSlice = AtModulePwMaxPwsGet((AtModulePw)AtChannelModuleGet((AtChannel)pw)) / 2;
    uint8 slice = (uint8)(ThaHwPwIdGet(hwPw) / numPwsPerSlice);
    AtUnused(self);
    return slice;
    }

static uint32 MaxDelayReg(AtPrbsEngine self, AtPw pw, eBool r2c)
    {
    Tha6A210031ModulePrbs prbs  = PrbsModule(self);
    uint8                 slice = PrbsEngineSliceGet(self, pw);

    return mMethodsGet(prbs)->PrbsMaxDelayReg(prbs, cAtFalse, r2c, slice);
    }

static uint32 MinDelayReg(AtPrbsEngine self, AtPw pw, eBool r2c)
    {
    Tha6A210031ModulePrbs prbs    = PrbsModule(self);
    uint8                 slice   =  PrbsEngineSliceGet(self, pw);

    return mMethodsGet(prbs)->PrbsMinDelayReg(prbs, cAtFalse, r2c, slice);
    }

static uint32 AverageDelayReg(AtPrbsEngine self, AtPw pw, eBool r2c)
    {
    Tha6A210031ModulePrbs prbs    = PrbsModule(self);
    uint8                 slice   =  PrbsEngineSliceGet(self, pw);

    return mMethodsGet(prbs)->PrbsAverageDelayReg(prbs, cAtFalse, r2c, slice);
    }

static float ConvertToMs(AtPrbsEngine self, uint32 value)
    {
    AtUnused(self);
    return (float)value/155520;
    }

static float DelayGet(AtPrbsEngine self, uint8 mode, eBool r2c)
    {
    AtPw pw = (AtPw)AtPrbsEngineChannelGet(self);
    uint32 regVal = 0;
    uint32 regAddr = MaxDelayReg(self, pw, r2c);

    if (mode == cAf6MinDelay)
        regAddr = MinDelayReg(self, pw, r2c);
    else if (mode == cAf6AverageDelay)
        {
        Tha6A210031ModulePrbs prbs = PrbsModule(self);
        if (Tha6A210031ModulePrbsNeedDelayAverage(prbs))
            regAddr = AverageDelayReg(self, pw, r2c);
        else
            regAddr = cInvalidUint32;
        }
    if (regAddr!= cInvalidUint32)
        {
        regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
        if (mode == cAf6MinDelay && regVal==0xFFFFFFFF)
            regVal = 0; /* Value Maximum to compare */
        return ConvertToMs(self,regVal);
        }

    return 0;
    }


static eAtRet LoHwDelayEnable(AtPrbsEngine self, AtPw pw, eBool enable)
    {
    uint32 offset  =  DefaultOffset(self);
    uint8  slice   =  PrbsEngineSliceGet(self, pw);
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    uint32 regAddr = Tha6A210031ModulePrbsDelayConfigReg(prbs, cAtFalse, slice);
    uint32 regVal    = 0;
    uint32 regFieldMask=0, regFieldShift=0;

    regVal = AtPrbsEngineRead(self, regAddr, cAtModulePrbs);
    regFieldMask   = Tha6A210031ModulePrbsDelayEnableMask(prbs, cAtFalse);
    regFieldShift  = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(regVal, regField, mBoolToBin(enable));

    regFieldMask   = Tha6A210031ModulePrbsDelayIdMask(prbs, cAtFalse);
    regFieldShift  = AtRegMaskToShift(regFieldMask);
    mRegFieldSet(regVal, regField, offset);
    AtPrbsEngineWrite(self, regAddr, regVal, cAtModulePrbs);
    return cAtOk;
    }

static eAtRet DelayEnable(AtPrbsEngine self, eBool enable)
    {
    AtPw pw = (AtPw)AtPrbsEngineChannelGet(self);
    if (pw)
        return LoHwDelayEnable(self, pw, enable);

    return cAtOk;
    }

static eAtRet DisruptionLossSigThresSet(AtPrbsEngine self, uint32 thres)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    if (Tha6A210031ModulePrbsLossIsSuppported(prbs))
        {
        uint32 regVal[4]={0},regFieldMask=0, regFieldShift=0;
        uint32 regAddr       = Tha6A210031ModulePrbsLossTimerCfgReg(prbs) /*+
                               Tha6A210031PrbsEnginePwDefaultOffset(self)*/;

        AtOsalMemInit(regVal, 0, sizeof(regVal));
        AtPrbsEngineDisruptionLongRead(self, regAddr, regVal, 4);
        regFieldMask  = Tha6A210031ModulePrbsLossSigThresMask(prbs);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal[0], regField, thres);
        AtPrbsEngineDisruptionLongWrite(self, regAddr, regVal, 4);
        }
    return cAtOk;
    }

static eAtRet DisruptionPrbsSyncDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    if (Tha6A210031ModulePrbsLossIsSuppported(prbs))
        {
        uint32 regVal[4]={0},regFieldMask=0, regFieldShift=0;
        uint32 regAddr       = Tha6A210031ModulePrbsLossTimerCfgReg(prbs) /*+
                               Tha6A210031PrbsEnginePwDefaultOffset(self)*/;

        AtOsalMemInit(regVal, 0, sizeof(regVal));
        AtPrbsEngineDisruptionLongRead(self, regAddr, regVal, cAtModulePrbs);
        regFieldMask  = Tha6A210031ModulePrbsSyncDectectThresMask(prbs);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal[0], regField, thres);
        AtPrbsEngineDisruptionLongWrite(self, regAddr, regVal, cAtModulePrbs);
        }
    return cAtOk;
    }

static eAtRet DisruptionPrbsLossDectectThresSet(AtPrbsEngine self, uint32 thres)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    if (Tha6A210031ModulePrbsLossIsSuppported(prbs))
        {
        uint32 regVal[4]={0},regFieldMask=0, regFieldShift=0;
        uint32 regAddr       = Tha6A210031ModulePrbsLossTimerCfgReg(prbs) /*+
                               Tha6A210031PrbsEnginePwDefaultOffset(self)*/;

        AtOsalMemInit(regVal, 0, sizeof(regVal));
        AtPrbsEngineDisruptionLongRead(self, regAddr, regVal, 4);
        regFieldMask  = Tha6A210031ModulePrbsLossDectectThresMask(prbs);
        regFieldShift = AtRegMaskToShift(regFieldMask);

        mRegFieldSet(regVal[0], regField, thres);
        AtPrbsEngineDisruptionLongWrite(self, regAddr, regVal, 4);
        }
    return cAtOk;
    }

static eAtRet SetLossUnit(AtPrbsEngine self, uint32 unit)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    if (Tha6A210031ModulePrbsLossIsSuppported(prbs))
        {
        uint32 regVal[4]={0},regFieldMask=0, regFieldShift=0;
        uint32 regAddr       = Tha6A210031ModulePrbsLossTimerUnitReg(prbs)/* +
                               Tha6A210031PrbsEnginePwDefaultOffset(self)*/;

        AtOsalMemInit(regVal, 0, sizeof(regVal));
        AtPrbsEngineDisruptionLongRead(self, regAddr, regVal, 4);
        regFieldMask  = Tha6A210031ModulePrbsLossUnitMask(prbs);
        regFieldShift = AtRegMaskToShift(regFieldMask);

        mRegFieldSet(regVal[0], regField, unit);
        AtPrbsEngineDisruptionLongWrite(self, regAddr, regVal, 4);
        }
    return cAtOk;
    }

static uint32 GetLossUnit(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    if (Tha6A210031ModulePrbsLossIsSuppported(prbs))
        {
        uint32 regVal[4]={0},regFieldMask=0, regFieldShift=0;
        uint32 regAddr       = Tha6A210031ModulePrbsLossTimerUnitReg(prbs)/* +
                               Tha6A210031PrbsEnginePwDefaultOffset(self)*/;

        AtOsalMemInit(regVal, 0, sizeof(regVal));
        AtPrbsEngineDisruptionLongRead(self, regAddr, regVal, 4);
        regFieldMask  = Tha6A210031ModulePrbsLossUnitMask(prbs);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        return mRegField(regVal[0], regField);
        }
    return 0;
    }

static uint32 UnitToHw(AtPrbsEngine self)
    {
    AtUnused(self);
    return 0x12EBB;
    }

static uint32 HwToUnit(AtPrbsEngine self, uint32 hwUnit)
    {
    AtUnused(self);
    if (hwUnit == 0x12EBB)
        return 1;
    return 1;
    }

static uint32 RegMaskWidth(uint32 mask)
    {
    uint32 bitIndex;
    uint8 width = 0;

    for (bitIndex = 0; bitIndex < 32; bitIndex++)
        {
        if ((cBit0 << bitIndex) & mask)
            width++;
        }

    return width;
    }

static eAtRet DisruptionMaxExpectedTimeSet(AtPrbsEngine self, uint32 time)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    if (Tha6A210031ModulePrbsLossIsSuppported(prbs))
        {
        uint32 regVal[4]={0},regFieldMask=0, regFieldShift=0, unit, regMaskWidth;
        uint32 regAddr       = Tha6A210031ModulePrbsLossTimerCfgReg(prbs) /*+
                               Tha6A210031PrbsEnginePwDefaultOffset(self)*/;

        AtOsalMemInit(regVal, 0, sizeof(regVal));
        AtPrbsEngineDisruptionLongRead(self, regAddr, regVal, 4);
        regFieldMask  = Tha6A210031ModulePrbsExpectMaxTimeMaskHo(prbs);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        unit          = UnitToHw(self);

        regMaskWidth  = RegMaskWidth(Tha6A210031ModulePrbsExpectMaxTimeMaskLo(prbs));
        mRegFieldSet(regVal[1], regField, time>>regMaskWidth);

        regFieldMask  = Tha6A210031ModulePrbsExpectMaxTimeMaskLo(prbs);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        mRegFieldSet(regVal[0], regField, time);
        AtPrbsEngineDisruptionLongWrite(self, regAddr, regVal, 4);
        SetLossUnit(self, unit);
        }
    return cAtOk;
    }

static uint32 DisruptionMaxExpectedTimeGet(AtPrbsEngine self)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    if (Tha6A210031ModulePrbsLossIsSuppported(prbs))
        {
        uint32 regVal[4]={0},regFieldMask=0, regFieldShift=0, unit=0, regMaskWidth=0, value=0;
        uint32 regAddr       = Tha6A210031ModulePrbsLossTimerCfgReg(prbs) /*+
                               Tha6A210031PrbsEnginePwDefaultOffset(self)*/;

        AtOsalMemInit(regVal, 0, sizeof(regVal));
        unit          = HwToUnit(self, GetLossUnit(self));
        AtPrbsEngineDisruptionLongRead(self, regAddr, regVal, 4);
        regFieldMask  = Tha6A210031ModulePrbsExpectMaxTimeMaskHo(prbs);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        value = mRegField(regVal[1], regField);

        regFieldMask  = Tha6A210031ModulePrbsExpectMaxTimeMaskLo(prbs);
        regMaskWidth  = RegMaskWidth(regFieldMask);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        return ((value <<regMaskWidth)|mRegField(regVal[0], regField)) * unit;
        }

    return 0;
    }

static uint32 DisruptionCurTimeGet(AtPrbsEngine self, eBool r2c)
    {
    Tha6A210031ModulePrbs prbs = PrbsModule(self);
    if (Tha6A210031ModulePrbsLossIsSuppported(prbs))
        {
        uint32 regVal[4]={0},regFieldMask=0, regFieldShift=0, unit, ret;
        uint32 regAddr       = Tha6A210031ModulePrbsLossTimerStatReg(prbs) +
                               Tha6A210031PrbsEnginePwDefaultOffset(self);

        AtOsalMemInit(regVal, 0, sizeof(regVal));
        AtPrbsEngineDisruptionLongRead(self, regAddr, regVal, 4);
        regFieldMask  = Tha6A210031ModulePrbsLossCurTimeMask(prbs);
        regFieldShift = AtRegMaskToShift(regFieldMask);
        unit = HwToUnit(self, GetLossUnit(self));
        ret =  mRegField(regVal[0], regField) * unit;

        if (r2c)
            {
            mRegFieldSet(regVal[0], regField, 0);
            AtPrbsEngineDisruptionLongWrite(self, regAddr, regVal, 4);
            }

        return ret;
        }
    return 0;
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, mMethodsGet(self), sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorForce);
        mMethodOverride(m_AtPrbsEngineOverride, ErrorIsForced);
        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);

        mMethodOverride(m_AtPrbsEngineOverride, DataModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, DataModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxDataModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxDataModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxDataModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxDataModeGet);


        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxCurrentFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);

        mMethodOverride(m_AtPrbsEngineOverride, Invert);
        mMethodOverride(m_AtPrbsEngineOverride, TxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, RxInvert);
        mMethodOverride(m_AtPrbsEngineOverride, IsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsInverted);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsInverted);

        mMethodOverride(m_AtPrbsEngineOverride, DelayEnable);
        mMethodOverride(m_AtPrbsEngineOverride, DelayGet);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultMode);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultPrbsLoss);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionMaxExpectedTimeSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionMaxExpectedTimeGet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionCurTimeGet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionLossSigThresSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionPrbsSyncDectectThresSet);
        mMethodOverride(m_AtPrbsEngineOverride, DisruptionPrbsLossDectectThresSet);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideTha61150011PrbsEnginePw(AtPrbsEngine self)
    {
    Tha61150011PrbsEnginePw prbs = (Tha61150011PrbsEnginePw)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61150011PrbsEnginePwOverride, mMethodsGet(prbs), sizeof(m_Tha61150011PrbsEnginePwOverride));

        mMethodOverride(m_Tha61150011PrbsEnginePwOverride, PrbsSliceOffset);
        }

    mMethodsSet(prbs, &m_Tha61150011PrbsEnginePwOverride);
    }

static void OverrideTha61031031PrbsEnginePw(Tha61031031PrbsEngineTdmPw self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEnginePwOverride, mMethodsGet(self), sizeof(m_Tha61031031PrbsEnginePwOverride));


        }

    mMethodsSet(self, &m_Tha61031031PrbsEnginePwOverride);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideTha61150011PrbsEnginePw(self);
    OverrideTha61031031PrbsEnginePw((Tha61031031PrbsEngineTdmPw) self);
    }

AtPrbsEngine Tha6A210031PrbsEnginePwObjectInit(AtPrbsEngine self, AtPw pw)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61150011PrbsEnginePwObjectInit(self, pw) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A210031PrbsEnginePwNew(AtPw pw)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newEngine == NULL)
        return NULL;

    /* Construct it */
    return Tha6A210031PrbsEnginePwObjectInit(newEngine, pw);
    }

uint32 Tha6A210031PrbsEnginePwDefaultOffset(AtPrbsEngine self)
    {
    if (self)
        return DefaultOffset(self);

    return cInvalidUint32;
    }

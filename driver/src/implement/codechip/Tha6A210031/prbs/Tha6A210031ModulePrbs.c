/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A210031ModulePrbs.c
 *
 * Created Date: Oct 8, 2015
 *
 * Description : Tha6A210031 module PRBS
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pw/ThaModulePw.h"
#include "Tha6A210031ModulePrbsReg.h"
#include "Tha6A210031ModulePrbsInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"
#include "../../../default/man/ThaDevice.h"
#include "Tha6A210031PrbsHiReg.h"
#include "Tha6A210031PrbsLoReg.h"
/*--------------------------- Define -----------------------------------------*/
#define cAf6_prbsgenfxptdat_fxptdat_Mask                                                              cBit31_0
#define cAf6_prbsgenfxptdat_fxptdat_Shift                                                                    0

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/


/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha6A210031ModulePrbsMethods m_methods;

/* Override */
static tAtModuleMethods              m_AtModuleOverride;
static tAtModulePrbsMethods          m_AtModulePrbsOverride;
static tTha61031031ModulePrbsMethods m_Tha61031031ModulePrbsOverride;


/* Save super implementation */
static const tAtModuleMethods *m_AtModuleMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaVersionReader VersionReader(AtModule self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return ThaDeviceVersionReader(device);
    }

static uint32 StartVersionSupportPrbsLoss(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static eBool  PrbsLossIsSuppported(Tha6A210031ModulePrbs self)
    {
    uint32 startVersionHasThis = mMethodsGet(self)->StartVersionSupportPrbsLoss(self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtModule)self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 PrbsLossTimerUnitReg(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 PrbsLossTimerCfgReg(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 PrbsLossTimerStatReg(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 PrbsLossExpectMaxTimeMaskLo(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }
static uint32 PrbsLossExpectMaxTimeMaskHo(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 PrbsLossCurTimeMask(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 PrbsLossSigThresMask(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 PrbsSyncDectectThresMask(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 PrbsLossDectectThresMask(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 PrbsLossUnitMask(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cInvalidUint32;
    }

static uint32 StartVersionSupportFixedPattern4bytes(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    }

static eBool NeedFixedPattern4bytes(Tha6A210031ModulePrbs self)
    {
    uint32 startVersionHasThis = mMethodsGet(self)->StartVersionSupportFixedPattern4bytes(self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtModule)self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 StartVersionSupportDelay(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    }

static eBool NeedDelay(Tha6A210031ModulePrbs self)
    {
    uint32 startVersionHasThis = mMethodsGet(self)->StartVersionSupportDelay(self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtModule)self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 StartVersionSupportShiftUp(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return ThaVersionReaderHardwareVersionWithBuiltNumberBuild(0xF, 0xF, 0xFFFF);
    }

static eBool NeedShiftUp(Tha6A210031ModulePrbs self)
    {
    uint32 startVersionHasThis = mMethodsGet(self)->StartVersionSupportShiftUp(self);
    uint32 currentVersion = ThaVersionReaderHardwareVersionAndBuiltNumber(VersionReader((AtModule)self));
    return (currentVersion >= startVersionHasThis) ? cAtTrue : cAtFalse;
    }

static uint32 PrbsErrFrcMask(Tha6A210031ModulePrbs self, eBool isHo)
    {
    AtUnused(self);
    if (isHo)
        return cInvalidUint32;
    return cInvalidUint32;
    }

static uint32 PrbsEnableMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    AtUnused(self);
    if (isHo)
        return cInvalidUint32;
    return isTxDirection ?cThaRegPrbsLoGenCtlEnbMask: cInvalidUint32;
    }

static uint32 PrbsInvertMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {    
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        {
	    if (isHo)
        	return cInvalidUint32;
	    return isTxDirection ?cThaRegPrbsLoGenCtlEnbMask: cInvalidUint32;
	    }
	return cInvalidUint32;
    }

static uint32 PrbsModeMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
	if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
		{
	   	if (isHo)
    	   	return cInvalidUint32;
    	return isTxDirection?cThaRegPrbsLoGenCtlSeqModMask: cThaRegPrbsLoMonCtlSeqModMask;
    	}
	return cInvalidUint32;    	
    }

static uint32 PrbsDataModeMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (Tha6A210031ModulePrbsDataModeIsSupported(self))
        {
        if (isHo)
            return cInvalidUint32;
        return isTxDirection?cAtt_prbslogenctl1_gen_cas_en_Mask: cThaRegPrbsLoMon_cas_en_Mask;
        }
    return cInvalidUint32;
    }

static uint32 PrbsStepMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    AtUnused(self);
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        {
        if (isHo)
            return cInvalidUint32;
        return isTxDirection ?cAtt_prbslogenctl1_seqstep_Mask: cAtt_prbslomon1_seqstep_Mask;
        }
    return cInvalidUint32;
    }

static uint32 PrbsFixPatternMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
    	{
        if (isHo)
        	return cInvalidUint32;
	    return isTxDirection ?cAtt_prbslogenctl1_seqdat_Mask: cAtt_prbslomon1_seqdat_Mask;
        }

    if (isHo)
        return cInvalidUint32;
    return isTxDirection ?cAf6_prbsgenfxptdatlo_fxptdat_Mask: cAf6_prbsmonfxptdatlo_fxptdat_Mask;
    }

static uint8 PrbsModeToHwValue(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        {
        AtUnused(self);
        return (prbsMode == cAtPrbsModePrbsSeq) ? 1 : 0;
        }

    if (prbsMode == cAtPrbsModePrbsFixedPattern3Bytes)
        return 0;
    if (prbsMode == cAtPrbsModePrbsFixedPattern1Byte||
        prbsMode == cAtPrbsModePrbsFixedPattern2Bytes||
        prbsMode == cAtPrbsModePrbsFixedPattern4Bytes)
        return 1;
    if (prbsMode == cAtPrbsModePrbs15)
        return 2;
    if (prbsMode == cAtPrbsModePrbsSeq)
        return 3;
    return 0;
    }

static eAtPrbsMode PrbsModeFromHwValue(Tha6A210031ModulePrbs self, eAtPrbsMode catchedPrbsMode, uint8 mode, uint8 step)
    {
    AtUnused(self);
    AtUnused(catchedPrbsMode);
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        {
        if (mode == 0)
            return cAtPrbsModePrbs15;
        else
            return (step ? cAtPrbsModePrbsSeq : cAtPrbsModePrbsFixedPattern1Byte);
        }

    if (mode == 0)
        return cAtPrbsModePrbsFixedPattern3Bytes;
    if (mode == 1)
        return catchedPrbsMode;
    if (mode == 2)
        return cAtPrbsModePrbs15;
    if (mode == 3)
        return cAtPrbsModePrbsSeq;
    return cAtPrbsModePrbs15;
    }

static uint8 HwStepFromPrbsMode(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode)
    {
    uint8 step = 0;

    AtUnused(self);
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        {
        if (prbsMode==cAtPrbsModePrbsSeq)
            step = 1;
        }

    return step;
    }

static uint32 PrbsFixPatternReg(Tha6A210031ModulePrbs self, eBool isHo, eBool isTx)
    {
    if (!mMethodsGet(self)->NeedFixedPattern4bytes(self))
        {
		if (isHo)
        	return isTx? mMethodsGet(self)->PrbsGenReg(self, cAtTrue):mMethodsGet(self)->PrbsMonReg(self, cAtTrue);
	    return isTx? mMethodsGet(self)->PrbsGenReg(self, cAtFalse):mMethodsGet(self)->PrbsMonReg(self, cAtFalse);
        }
    return cInvalidUint32;
    }

static eBool CounterIsSupported(Tha6A210031ModulePrbs self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return cAtFalse;
    }

static uint32 PrbsCounterMask(Tha6A210031ModulePrbs self, uint16 counterType)
    {
    AtUnused(self);
    AtUnused(counterType);
    return cInvalidUint32;
    }

static uint32 PrbsCounterReg(Tha6A210031ModulePrbs self, eBool isHo, eBool r2c)
    {
    AtUnused(self);
    AtUnused(isHo);
    AtUnused(r2c);
    return cInvalidUint32;
    }

static uint32 ConvertSwToHwFixedPattern(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    AtUnused(self);
    AtUnused(prbsMode);
    return fixedPattern;
    }

static uint32 ConvertHwToSwFixedPattern(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    AtUnused(self);
    AtUnused(prbsMode);
    return fixedPattern;
    }

static eBool PrbsModeIsSupported(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode)
    {
    AtUnused(self);
    return ((prbsMode == cAtPrbsModePrbs15)
            || (prbsMode == cAtPrbsModePrbsSeq)
            || (prbsMode == cAtPrbsModePrbsFixedPattern1Byte))
            ? cAtTrue : cAtFalse;
    }

static eBool PrbsDataModeIsSupported(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static uint32 PrbsGenReg(Tha6A210031ModulePrbs self, eBool isHo)
    {
    AtUnused(self);
    if (isHo)
        return cInvalidUint32/*cReg_prbshomon_Base*/;

    if (Tha6A210031ModulePrbsNeedShiftUp(self))
        return cThaRegPrbsLoMapGeneratorControl;
    return cThaRegPrbsLoMapGeneratorControl/*cReg_prbslomon_Base*/;
    }

static uint32 PrbsMonReg(Tha6A210031ModulePrbs self, eBool isHo)
    {
    AtUnused(self);
    if (isHo)
        return cThaRegPrbsLoMapMonitor/*cReg_prbshomon_Base*/;
    return cThaRegPrbsLoMapMonitor/*cReg_prbslomon_Base*/;
    }

static uint8 LoPrbsEngineOc48IdShift(Tha6A210031ModulePrbs self)
    {
    AtUnused(self);
    if (Tha6A210031ModulePrbsNeedShiftUp(self))
        return 21;
    return 19;
    }

static uint8 LoPrbsEngineOc24SliceIdShift(Tha6A210031ModulePrbs self)
    {
    if (Tha6A210031ModulePrbsNeedShiftUp(self))
        return 13;
    return 11;
    }

static uint32 PrbsDelayConfigReg(Tha6A210031ModulePrbs self, eBool isHo, uint8 slice)
    {
    AtUnused(self);
    AtUnused(isHo);
    AtUnused(slice);
    return cInvalidUint32;
    return cAf6Reg_rtmcidcfglo + (uint32) (slice*8192);
    }

static uint32 PrbsDelayIdMask(Tha6A210031ModulePrbs self, eBool isHo)
    {
    AtUnused(self);
    AtUnused(isHo);
    return cInvalidUint32;
    }

static uint32 PrbsDelayEnableMask(Tha6A210031ModulePrbs self, eBool isHo)
    {
    AtUnused(self);
    AtUnused(isHo);
    return cInvalidUint32;
    }

static uint32 PrbsMaxDelayReg(Tha6A210031ModulePrbs self, eBool isHo, eBool r2c, uint8 slice)
    {
    AtUnused(isHo);
    if (mMethodsGet(self)->NeedDelay(self))
        {
        uint32 address = r2c?cAf6Reg_mxdelayregr2clo: cAf6Reg_mxdelayregrolo;
        return address + (uint32) (slice*8192);
        }
    return cInvalidUint32;
    }

static uint32 PrbsMinDelayReg(Tha6A210031ModulePrbs self, eBool isHo, eBool r2c, uint8 slice)
    {
    AtUnused(isHo);
    if (mMethodsGet(self)->NeedDelay(self))
        {
        uint32 address =  r2c?cAf6Reg_mindelayregr2clo: cAf6Reg_mindelayregrolo;
        return address + (uint32) (slice*8192);
        }
    return cInvalidUint32;
    }

static uint32 PrbsAverageDelayReg(Tha6A210031ModulePrbs self, eBool isHo, eBool r2c, uint8 slice)
    {
    AtUnused(isHo);
    if (mMethodsGet(self)->NeedDelay(self))
        {
        uint32 address =  r2c?cAf6Reg_mindelayregr2clo: cAf6Reg_mindelayregrolo;
        return address + (uint32) (slice*8192) + 1;
        }
    return cInvalidUint32;
    }

static AtDevice Device(AtModule self)
    {
    return AtModuleDeviceGet(self);
    }

static void DefaultSet(AtModule self)
    {
    uint32 pw_i;
    uint32 sliceId;
    const uint8 cNumSlice = 2;
    uint32 numPw = AtModulePwMaxPwsGet((AtModulePw)AtDeviceModuleGet(Device(self), cAtModulePw));

    for (sliceId = 0; sliceId < cNumSlice; sliceId++)
        {
        for (pw_i = 0; pw_i < numPw; pw_i++)
            mModuleHwWrite(self, cThaRegPrbsLoMapGeneratorControl + pw_i + 0x1000UL * sliceId, 0x0);
        }
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    DefaultSet(self);
    return cAtOk;
    }

static eAtRet AsyncInit(AtModule self)
    {
    return Init(self);
    }

static AtPrbsEngine PwPrbsEngineObjectCreate(Tha61031031ModulePrbs self, AtPw pw)
    {
    AtUnused(self);
    return Tha6A210031PrbsEnginePwNew(pw);
    }

static uint32 DefaultJitterBuffer(Tha61031031ModulePrbs self)
    {
    AtUnused(self);
    return 32000;
    }

static AtPrbsEngine SdhVcPrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtSdhChannel sdhVc)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A210031PrbsEngineVcNew(sdhVc);
    }

static AtPrbsEngine De1PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe1 de1)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A210031PrbsEngineDe1New(de1);
    }

static AtPrbsEngine NxDs0PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhNxDS0 nxDs0)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A210031PrbsEngineNxDs0New(nxDs0);
    }

static eBool NeedPwInitConfig(AtModulePrbs self)
    {
    AtUnused(self);
    return cAtFalse;
    }

static AtPrbsEngine De3PrbsEngineCreate(AtModulePrbs self, uint32 engineId, AtPdhDe3 de3)
    {
    AtUnused(self);
    AtUnused(engineId);
    return Tha6A210031PrbsEngineDe3New(de3);
    }

static AtPrbsEngine PwPrbsEngineCreate(Tha61031031ModulePrbs self, AtPrbsEngine tdmPw)
    {
    eAtRet ret;
    uint32 freePwId;
    AtChannel realCircuit, circuit;
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    ThaModulePw pwModule = (ThaModulePw)AtDeviceModuleGet(device, cAtModulePw);
    AtModuleEth ethModule = (AtModuleEth)AtDeviceModuleGet(device, cAtModuleEth);
    Tha61031031PrbsEngineTdmPw prbsEngine = (Tha61031031PrbsEngineTdmPw)tdmPw;
    AtPw pw = NULL;
    AtEthPort ethPort;

    /* Find one free PW */
    freePwId = ThaModulePwFreePwGet(pwModule);
    if (freePwId >= AtModulePwMaxPwsGet((AtModulePw)pwModule))
        return NULL;

    /* Create PW */
    pw = mMethodsGet(prbsEngine)->PwCreate(prbsEngine, freePwId);
    if (pw == NULL)
        return NULL;

    circuit = AtPrbsEngineChannelGet((AtPrbsEngine)tdmPw);
    realCircuit = mMethodsGet(prbsEngine)->CircuitToBind(prbsEngine, circuit);
    if (realCircuit == NULL)
        return NULL;

    /* TODO: need to add code to limit number of PW create per slice
     * Note: should use an internal counter */

    ethPort = AtModuleEthPortGet(ethModule, mMethodsGet(prbsEngine)->PartOfChannel(prbsEngine));
    ret = AtPwEthPortSet(pw, ethPort);
    if (ret != cAtOk)
        return NULL;

    ret = AtPwCircuitBind(pw, realCircuit);
    if (ret != cAtOk)
        return NULL;

    return mMethodsGet(self)->PwPrbsEngineObjectCreate(self, pw);
    }

static void MethodsInit(Tha6A210031ModulePrbs self)
    {
    if (!m_methodsInit)
        {
        mMethodOverride(m_methods, NeedDelay);
        mMethodOverride(m_methods, StartVersionSupportDelay);
        mMethodOverride(m_methods, NeedShiftUp);
        mMethodOverride(m_methods, StartVersionSupportShiftUp);
        mMethodOverride(m_methods, NeedFixedPattern4bytes);
        mMethodOverride(m_methods, StartVersionSupportFixedPattern4bytes);
        mMethodOverride(m_methods, PrbsMonReg);
        mMethodOverride(m_methods, PrbsGenReg);
        mMethodOverride(m_methods, LoPrbsEngineOc48IdShift);
        mMethodOverride(m_methods, LoPrbsEngineOc24SliceIdShift);

        mMethodOverride(m_methods, PrbsDelayConfigReg);
        mMethodOverride(m_methods, PrbsDelayIdMask);
        mMethodOverride(m_methods, PrbsDelayEnableMask);
        mMethodOverride(m_methods, PrbsMaxDelayReg);
        mMethodOverride(m_methods, PrbsMinDelayReg);
        mMethodOverride(m_methods, PrbsAverageDelayReg);

        mMethodOverride(m_methods, PrbsModeFromHwValue);
        mMethodOverride(m_methods, PrbsModeToHwValue);
        mMethodOverride(m_methods, HwStepFromPrbsMode);
        mMethodOverride(m_methods, PrbsModeMask);
        mMethodOverride(m_methods, PrbsDataModeMask);
        mMethodOverride(m_methods, PrbsStepMask);
        mMethodOverride(m_methods, PrbsEnableMask);
        mMethodOverride(m_methods, PrbsInvertMask);
        mMethodOverride(m_methods, PrbsErrFrcMask);
        mMethodOverride(m_methods, PrbsFixPatternMask);
        mMethodOverride(m_methods, PrbsFixPatternReg);
        mMethodOverride(m_methods, CounterIsSupported);
        mMethodOverride(m_methods, PrbsCounterReg);
        mMethodOverride(m_methods, PrbsCounterMask);
        mMethodOverride(m_methods, PrbsModeIsSupported);
        mMethodOverride(m_methods, PrbsDataModeIsSupported);
        mMethodOverride(m_methods, ConvertSwToHwFixedPattern);
        mMethodOverride(m_methods, ConvertHwToSwFixedPattern);

        mMethodOverride(m_methods, StartVersionSupportPrbsLoss);
        mMethodOverride(m_methods, PrbsLossIsSuppported);
        mMethodOverride(m_methods, PrbsLossTimerUnitReg);
        mMethodOverride(m_methods, PrbsLossTimerCfgReg);
        mMethodOverride(m_methods, PrbsLossTimerStatReg);
        mMethodOverride(m_methods, PrbsLossExpectMaxTimeMaskLo);
        mMethodOverride(m_methods, PrbsLossExpectMaxTimeMaskHo);
        mMethodOverride(m_methods, PrbsLossCurTimeMask);
        mMethodOverride(m_methods, PrbsLossSigThresMask);
        mMethodOverride(m_methods, PrbsSyncDectectThresMask);
        mMethodOverride(m_methods, PrbsLossDectectThresMask);
        mMethodOverride(m_methods, PrbsLossUnitMask);
        }

    mMethodsSet(self, &m_methods);
    }

static void OverrideAtModule(AtModulePrbs self)
    {
    AtModule module = (AtModule)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, Init);
        mMethodOverride(m_AtModuleOverride, AsyncInit);
        }

    mMethodsSet(module, &m_AtModuleOverride);
    }

static void OverrideAtModulePrbs(AtModulePrbs self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePrbsOverride, mMethodsGet(self), sizeof(m_AtModulePrbsOverride));

        mMethodOverride(m_AtModulePrbsOverride, SdhVcPrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, NeedPwInitConfig);
        mMethodOverride(m_AtModulePrbsOverride, De1PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, De3PrbsEngineCreate);
        mMethodOverride(m_AtModulePrbsOverride, NxDs0PrbsEngineCreate);
        }

    mMethodsSet(self, &m_AtModulePrbsOverride);
    }

static void OverrideTha61031031ModulePrbs(AtModulePrbs self)
    {
    Tha61031031ModulePrbs prbsModule = (Tha61031031ModulePrbs)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031ModulePrbsOverride, mMethodsGet(prbsModule), sizeof(m_Tha61031031ModulePrbsOverride));

        mMethodOverride(m_Tha61031031ModulePrbsOverride, PwPrbsEngineObjectCreate);
        mMethodOverride(m_Tha61031031ModulePrbsOverride, DefaultJitterBuffer);
        mMethodOverride(m_Tha61031031ModulePrbsOverride, PwPrbsEngineCreate);
        }

    mMethodsSet(prbsModule, &m_Tha61031031ModulePrbsOverride);
    }

static void Override(AtModulePrbs self)
    {
    OverrideAtModule(self);
    OverrideAtModulePrbs(self);
    OverrideTha61031031ModulePrbs(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031ModulePrbs);
    }

AtModulePrbs Tha6A210031ModulePrbsObjectInit(AtModulePrbs self, AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    if (Tha61150011ModulePrbsObjectInit(self, device) == NULL)
        return NULL;

    MethodsInit((Tha6A210031ModulePrbs) self);
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePrbs Tha6A210031ModulePrbsNew(AtDevice device)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePrbs newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    return Tha6A210031ModulePrbsObjectInit(newModule, device);
    }

uint32 Tha6A210031ModulePrbsMonReg(Tha6A210031ModulePrbs self, eBool isHo)
    {
    if (self)
        return mMethodsGet(self)->PrbsMonReg(self, isHo);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsGenReg(Tha6A210031ModulePrbs self, eBool isHo)
    {
    if (self)
        return mMethodsGet(self)->PrbsGenReg(self, isHo);
    return cInvalidUint32;
    }

eBool Tha6A210031ModulePrbsNeedShiftUp(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->NeedShiftUp(self);
    return cAtFalse;
    }
eAtPrbsMode Tha6A210031ModulePrbsModeFromHwValue(Tha6A210031ModulePrbs self, eAtPrbsMode catchedPrbsMode, uint8 mode, uint8 step)
    {
    if (self)
        return mMethodsGet(self)->PrbsModeFromHwValue(self, catchedPrbsMode, mode, step);
    return cAtFalse;
    }

uint8 Tha6A210031ModulePrbsHwStepFromPrbsMode(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode)
    {
    if (self)
        return mMethodsGet(self)->HwStepFromPrbsMode(self, prbsMode);
    return 0;
    }

uint8 Tha6A210031ModulePrbsModeToHwValue(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode)
    {
    if (self)
        return mMethodsGet(self)->PrbsModeToHwValue(self, prbsMode);
    return 0;
    }
uint32 Tha6A210031ModulePrbsModeMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (self)
        return mMethodsGet(self)->PrbsModeMask(self, isHo, isTxDirection);
    return 0;
    }

uint32 Tha6A210031ModulePrbsDataModeMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (self)
        return mMethodsGet(self)->PrbsDataModeMask(self, isHo, isTxDirection);
    return 0;
    }

uint32 Tha6A210031ModulePrbsStepMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (self)
        return mMethodsGet(self)->PrbsStepMask(self, isHo, isTxDirection);
    return 0;
    }

uint32 Tha6A210031ModulePrbsEnableMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (self)
        return mMethodsGet(self)->PrbsEnableMask(self, isHo, isTxDirection);
    return 0;
    }

uint32 Tha6A210031ModulePrbsErrFrcMask(Tha6A210031ModulePrbs self, eBool isHo)
    {
    if (self)
        return mMethodsGet(self)->PrbsErrFrcMask(self, isHo);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsFixPatternMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (self)
        return mMethodsGet(self)->PrbsFixPatternMask(self, isHo, isTxDirection);
    return 0;
    }

uint32 Tha6A210031ModulePrbsFixPatternReg(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection)
    {
    if (self)
        return mMethodsGet(self)->PrbsFixPatternReg(self, isHo, isTxDirection);
    return 0;
    }

uint32 Tha6A210031ModulePrbsCounterReg(Tha6A210031ModulePrbs self, eBool isHo, eBool r2c)
    {
    if (self)
        return mMethodsGet(self)->PrbsCounterReg(self, isHo, r2c);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsCounterMask(Tha6A210031ModulePrbs self, uint16 counterType)
    {
    if (self)
        return mMethodsGet(self)->PrbsCounterMask(self, counterType);
    return cInvalidUint32;
    }

eBool Tha6A210031ModulePrbsCounterIsSupported(Tha6A210031ModulePrbs self, uint16 counterType)
    {
    if (self)
        return mMethodsGet(self)->CounterIsSupported(self, counterType);
    return cAtFalse;
    }

uint32 Tha6A210031ModulePrbsDelayConfigReg(Tha6A210031ModulePrbs self, eBool isHo, uint8 slice)
    {
    if (self)
        return mMethodsGet(self)->PrbsDelayConfigReg(self, isHo, slice);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsDelayIdMask(Tha6A210031ModulePrbs self, eBool isHo)
    {
    if (self)
        return mMethodsGet(self)->PrbsDelayIdMask(self, isHo);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsDelayEnableMask(Tha6A210031ModulePrbs self, eBool isHo)
    {
    if (self)
        return mMethodsGet(self)->PrbsDelayEnableMask(self, isHo);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsInvertMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTx)
    {
    if (self)
        return mMethodsGet(self)->PrbsInvertMask(self, isHo, isTx);
    return cInvalidUint32;
    }

eBool Tha6A210031ModulePrbsModeIsSupported(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode)
    {
    if (self)
        return mMethodsGet(self)->PrbsModeIsSupported(self, prbsMode);
    return cAtFalse;
    }

eBool Tha6A210031ModulePrbsDataModeIsSupported(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsDataModeIsSupported(self);
    return cAtFalse;
    }

uint32 Tha6A210031ModulePrbsConvertSwToHwFixedPattern(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    if (self)
        return mMethodsGet(self)->ConvertSwToHwFixedPattern(self, prbsMode, fixedPattern);
    return 0;
    }

uint32 Tha6A210031ModulePrbsConvertHwToSwFixedPattern(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern)
    {
    if (self)
        return mMethodsGet(self)->ConvertHwToSwFixedPattern(self, prbsMode, fixedPattern);
    return 0;
    }

eBool Tha6A210031ModulePrbsNeedDelayAverage(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->NeedDelayAverage(self);
    return 0;
    }

eBool Tha6A210031ModulePrbsLossIsSuppported(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsLossIsSuppported(self);
    return cAtFalse;
    }

uint32 Tha6A210031ModulePrbsLossTimerUnitReg(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsLossTimerUnitReg(self);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsLossTimerCfgReg(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsLossTimerCfgReg(self);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsLossTimerStatReg(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsLossTimerStatReg(self);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsExpectMaxTimeMaskLo(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsLossExpectMaxTimeMaskLo(self);
    return cInvalidUint32;
    }
uint32 Tha6A210031ModulePrbsExpectMaxTimeMaskHo(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsLossExpectMaxTimeMaskHo(self);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsLossCurTimeMask(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsLossCurTimeMask(self);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsLossSigThresMask(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsLossSigThresMask(self);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsSyncDectectThresMask(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsSyncDectectThresMask(self);
    return cInvalidUint32;
    }

uint32 Tha6A210031ModulePrbsLossDectectThresMask(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsLossDectectThresMask(self);
    return cInvalidUint32;
    }
uint32 Tha6A210031ModulePrbsLossUnitMask(Tha6A210031ModulePrbs self)
    {
    if (self)
        return mMethodsGet(self)->PrbsLossUnitMask(self);
    return cInvalidUint32;
    }

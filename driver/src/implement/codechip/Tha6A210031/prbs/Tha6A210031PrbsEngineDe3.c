/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PRBS
 *
 * File        : Tha6A290031PrbsEngineDe3.c
 *
 * Created Date: Apr 20, 2015
 *
 * Description : PRBS engine for DE3
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/pdh/ThaPdhDe3.h"
#include "Tha6A210031ModulePrbsInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha6A210031PrbsEngineDe3
    {
    tTha61150011PrbsEngineDe3 super;
    }tTha6A210031PrbsEngineDe3;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPrbsEngineMethods m_AtPrbsEngineOverride;
static tTha61031031PrbsEngineTdmPwMethods m_Tha61031031PrbsEngineTdmPwOverride;
static const tAtPrbsEngineMethods     *m_AtPrbsEngineMethods = NULL;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool NeedSetDefaultPrbsLoss(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static eBool NeedSetDefaultMode(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint8 MaxDe1Id(AtChannel channel)
    {
    uint16 frameType =AtPdhChannelFrameTypeGet((AtPdhChannel)channel);
    if (AtPdhDe3IsE1Channelized((AtPdhDe3)channel) && frameType != cAtPdhE3FrmG751Chnl16E1s)
        return 3;
    else
        return 4;
    }

static uint8 MaxGroupId(AtChannel channel)
    {
    uint16 frameType =AtPdhChannelFrameTypeGet((AtPdhChannel)channel);
    if (frameType == cAtPdhE3FrmG751Chnl16E1s||frameType == cAtPdhE3FrmG751)
        return 4;
    return 7;
    }

static eBool IsCbitChanelize(AtChannel channel)
    {
    uint16 frameType =AtPdhChannelFrameTypeGet((AtPdhChannel)channel);
    return (AtPdhDe3IsChannelizedFrameType(frameType)/*||frameType == cAtPdhE3FrmG751*/)?cAtTrue:cAtFalse;
    }

static eAtModulePrbsRet DS3ChanelizePrbsEnable (AtChannel channel, eBool enable, eAtModulePrbsRet (*fEnable)(AtPrbsEngine, eBool))
    {
    eAtModulePrbsRet ret = cAtOk;
    if (IsCbitChanelize(channel))
        {
        uint8 de2Id=0, de1Id=0, maxde1Id=MaxDe1Id(channel), maxGroupId =MaxGroupId(channel) ;

        for (de2Id=0; de2Id<maxGroupId;de2Id++)
            {
            for (de1Id=0; de1Id<maxde1Id;de1Id++)
                {
                AtPdhDe1 de1 = AtPdhDe3De1Get((AtPdhDe3)channel, de2Id, de1Id);
                AtPrbsEngine prbs = AtChannelPrbsEngineGet((AtChannel)de1);
                ret |= fEnable(prbs, enable);
                }
            }
        ThaPdhDe3PrbsDe3LevelSet((ThaPdhDe3)channel, enable);
        }
    return ret;
    }

static eBool DS3ChanelizePrbsIsEnabled (AtChannel channel, eBool (*fIsEnabled)(AtPrbsEngine ))
    {
    eBool ret = cAtFalse;
    if (IsCbitChanelize(channel))
        {
        AtPdhDe1 de1 = AtPdhDe3De1Get((AtPdhDe3)channel, 0, 0);
        AtPrbsEngine prbs = AtChannelPrbsEngineGet((AtChannel)de1);
        ret = fIsEnabled(prbs);
        }
    return ret;
    }


static eAtModulePrbsRet DS3ChanelizePrbsModeSet (AtChannel channel, eAtPrbsMode prbsMode, eAtModulePrbsRet (*fModeSet)(AtPrbsEngine , eAtPrbsMode))
    {
    eAtModulePrbsRet ret = cAtOk;
    if (IsCbitChanelize(channel))
        {
        uint8 de2Id=0, de1Id=0, maxde1Id=MaxDe1Id(channel), maxGroupId =MaxGroupId(channel) ;

        for (de2Id=0; de2Id<maxGroupId;de2Id++)
            {
            for (de1Id=0; de1Id<maxde1Id;de1Id++)
                {
                AtPdhDe1 de1 = AtPdhDe3De1Get((AtPdhDe3)channel, de2Id, de1Id);
                AtPrbsEngine prbs = AtChannelPrbsEngineGet((AtChannel)de1);
                ret |= fModeSet(prbs, prbsMode);
                }
            }
        }
    return ret;
    }

static eAtPrbsMode DS3ChanelizePrbsModeGet (AtChannel channel, eAtPrbsMode (*fModeGet)(AtPrbsEngine self))
    {
    eAtPrbsMode mode;
    if (IsCbitChanelize(channel))
        {
        AtPdhDe1 de1 = AtPdhDe3De1Get((AtPdhDe3)channel, 0, 0);
        AtPrbsEngine prbs = AtChannelPrbsEngineGet((AtChannel)de1);
        mode = fModeGet(prbs);
        }
    return mode;
    }


static eAtModulePrbsRet DS3ChanelizePrbsFixedPatternSet (AtChannel channel, uint32 FixedPattern, eAtModulePrbsRet (*fFixedPatternSet)(AtPrbsEngine , uint32))
    {
    eAtModulePrbsRet ret = cAtOk;
    if (IsCbitChanelize(channel))
        {
        uint8 de2Id=0, de1Id=0, maxde1Id=MaxDe1Id(channel), maxGroupId =MaxGroupId(channel);

        for (de2Id=0; de2Id<maxGroupId;de2Id++)
            {
            for (de1Id=0; de1Id<maxde1Id;de1Id++)
                {
                AtPdhDe1 de1 = AtPdhDe3De1Get((AtPdhDe3)channel, de2Id, de1Id);
                AtPrbsEngine prbs = AtChannelPrbsEngineGet((AtChannel)de1);
                ret |= fFixedPatternSet(prbs, FixedPattern);
                }
            }
        }
    return ret;
    }

static eAtPrbsMode DS3ChanelizePrbsFixedPatternGet (AtChannel channel, uint32 (*fFixedPatternGet)(AtPrbsEngine self))
    {
    eAtPrbsMode mode;
    if (IsCbitChanelize(channel))
        {
        AtPdhDe1 de1 = AtPdhDe3De1Get((AtPdhDe3)channel, 0, 0);
        AtPrbsEngine prbs = AtChannelPrbsEngineGet((AtChannel)de1);
        mode = fFixedPatternGet(prbs);
        }
    return mode;
    }

static uint32 DS3ChanelizePrbsAlarmGet (AtChannel channel, uint32 (*fAlarmGet)(AtPrbsEngine self))
    {
    uint32 alarm=0;
    if (IsCbitChanelize(channel))
        {
        uint8 de2Id=0, de1Id=0, maxde1Id=MaxDe1Id(channel), maxGroupId =MaxGroupId(channel);
        for (de2Id=0; de2Id<maxGroupId;de2Id++)
            {
            for (de1Id=0; de1Id<maxde1Id;de1Id++)
                {
                AtPdhDe1 de1 = AtPdhDe3De1Get((AtPdhDe3)channel, de2Id, de1Id);
                AtPrbsEngine prbs = AtChannelPrbsEngineGet((AtChannel)de1);
                alarm |= fAlarmGet(prbs);
                }
            }
        }
    return alarm;
    }

static uint32 DS3ChanelizePrbsCounterGet (AtChannel channel, uint16 counterType, uint32 (*fCounterGet)(AtPrbsEngine, uint16))
    {
    uint32 counter=0;
    if (IsCbitChanelize(channel))
        {
        uint8 de2Id=0, de1Id=0, maxde1Id=MaxDe1Id(channel), maxGroupId =MaxGroupId(channel);
        for (de2Id=0; de2Id<maxGroupId;de2Id++)
            {
            for (de1Id=0; de1Id<maxde1Id;de1Id++)
                {
                AtPdhDe1 de1 = AtPdhDe3De1Get((AtPdhDe3)channel, de2Id, de1Id);
                AtPrbsEngine prbs = AtChannelPrbsEngineGet((AtChannel)de1);
                counter += fCounterGet(prbs, counterType);
                }
            }
        }
    return counter;
    }

static eAtModulePrbsRet Enable(AtPrbsEngine self, eBool enable)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsEnable(channel, enable, AtPrbsEngineEnable);

    return m_AtPrbsEngineMethods->Enable(self, enable);
    }

static eAtModulePrbsRet TxEnable(AtPrbsEngine self, eBool enable)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsEnable(channel, enable, AtPrbsEngineTxEnable);

    return m_AtPrbsEngineMethods->TxEnable(self, enable);
    }

static eAtModulePrbsRet RxEnable(AtPrbsEngine self, eBool enable)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsEnable(channel, enable, AtPrbsEngineRxEnable);

    return m_AtPrbsEngineMethods->RxEnable(self, enable);
    }

static eBool IsEnabled(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsIsEnabled(channel, AtPrbsEngineIsEnabled);

    return m_AtPrbsEngineMethods->IsEnabled(self);
    }

static eBool TxIsEnabled(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsIsEnabled(channel, AtPrbsEngineTxIsEnabled);

    return m_AtPrbsEngineMethods->TxIsEnabled(self);
    }

static eBool RxIsEnabled(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsIsEnabled(channel, AtPrbsEngineRxIsEnabled);

    return m_AtPrbsEngineMethods->RxIsEnabled(self);
    }

static eAtModulePrbsRet ModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsModeSet(channel, prbsMode,AtPrbsEngineModeSet);

    return m_AtPrbsEngineMethods->ModeSet(self, prbsMode);
    }

static eAtModulePrbsRet TxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsModeSet(channel, prbsMode, AtPrbsEngineTxModeSet);

    return m_AtPrbsEngineMethods->TxModeSet(self, prbsMode);
    }

static eAtModulePrbsRet RxModeSet(AtPrbsEngine self, eAtPrbsMode prbsMode)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsModeSet(channel, prbsMode, AtPrbsEngineRxModeSet);

    return m_AtPrbsEngineMethods->RxModeSet(self, prbsMode);
    }

static eAtPrbsMode ModeGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsModeGet(channel, AtPrbsEngineModeGet);

    return m_AtPrbsEngineMethods->ModeGet(self);
    }
static eAtPrbsMode TxModeGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsModeGet(channel, AtPrbsEngineTxModeGet);

    return m_AtPrbsEngineMethods->TxModeGet(self);
    }
static eAtPrbsMode RxModeGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsModeGet(channel, AtPrbsEngineRxModeGet);

    return m_AtPrbsEngineMethods->RxModeGet(self);
    }

static eAtModulePrbsRet FixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsFixedPatternSet(channel, fixedPattern, AtPrbsEngineFixedPatternSet);

    return m_AtPrbsEngineMethods->FixedPatternSet(self, fixedPattern);
    }

static eAtModulePrbsRet TxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsFixedPatternSet(channel, fixedPattern, AtPrbsEngineTxFixedPatternSet);

    return m_AtPrbsEngineMethods->TxFixedPatternSet(self, fixedPattern);
    }
static eAtModulePrbsRet RxFixedPatternSet(AtPrbsEngine self, uint32 fixedPattern)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsFixedPatternSet(channel, fixedPattern, AtPrbsEngineRxFixedPatternSet);

    return m_AtPrbsEngineMethods->RxFixedPatternSet(self, fixedPattern);
    }

static uint32 FixedPatternGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsFixedPatternGet(channel, AtPrbsEngineFixedPatternGet);

    return m_AtPrbsEngineMethods->FixedPatternGet(self);
    }

static uint32 TxFixedPatternGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsFixedPatternGet(channel, AtPrbsEngineTxFixedPatternGet);

    return m_AtPrbsEngineMethods->TxFixedPatternGet(self);
    }

static uint32 RxFixedPatternGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsFixedPatternGet(channel, AtPrbsEngineRxFixedPatternGet);

    return m_AtPrbsEngineMethods->RxFixedPatternGet(self);
    }

static uint32 AlarmGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsAlarmGet(channel, AtPrbsEngineAlarmGet);

    return m_AtPrbsEngineMethods->AlarmGet(self);

    }

static uint32 AlarmHistoryGet(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsAlarmGet(channel, AtPrbsEngineAlarmHistoryGet);

    return m_AtPrbsEngineMethods->AlarmHistoryGet(self);

    }

static uint32 AlarmHistoryClear(AtPrbsEngine self)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsAlarmGet(channel, AtPrbsEngineAlarmHistoryClear);

    return m_AtPrbsEngineMethods->AlarmHistoryClear(self);

    }

static eAtModulePrbsRet MonitoringSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    return (side == cAtPrbsSideTdm) ? cAtOk : cAtErrorModeNotSupport;
    }

static eAtPrbsSide MonitoringSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static eAtModulePrbsRet GeneratingSideSet(AtPrbsEngine self, eAtPrbsSide side)
    {
    AtUnused(self);
    if (side != cAtPrbsSideTdm)
        return cAtErrorModeNotSupport;
    return cAtOk;
    }
static eAtPrbsSide GeneratingSideGet(AtPrbsEngine self)
    {
    AtUnused(self);
    return cAtPrbsSideTdm;
    }

static uint32 CounterGet(AtPrbsEngine self, uint16 counterType)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsCounterGet(channel, counterType, AtPrbsEngineCounterGet);

    return m_AtPrbsEngineMethods->CounterGet(self, counterType);
    }

static uint32 CounterClear(AtPrbsEngine self, uint16 counterType)
    {
    AtChannel channel = AtPrbsEngineChannelGet((AtPrbsEngine)self);
    if (IsCbitChanelize(channel))
        return DS3ChanelizePrbsCounterGet(channel, counterType, AtPrbsEngineCounterClear);

    return m_AtPrbsEngineMethods->CounterClear(self, counterType);
    }

static void OverrideAtPrbsEngine(AtPrbsEngine self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPrbsEngineMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPrbsEngineOverride, m_AtPrbsEngineMethods, sizeof(m_AtPrbsEngineOverride));

        mMethodOverride(m_AtPrbsEngineOverride, Enable);
        mMethodOverride(m_AtPrbsEngineOverride, TxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, RxEnable);
        mMethodOverride(m_AtPrbsEngineOverride, IsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, TxIsEnabled);
        mMethodOverride(m_AtPrbsEngineOverride, RxIsEnabled);

        mMethodOverride(m_AtPrbsEngineOverride, ModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeSet);
        mMethodOverride(m_AtPrbsEngineOverride, ModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxModeGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxModeGet);

        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternSet);
        mMethodOverride(m_AtPrbsEngineOverride, FixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, TxFixedPatternGet);
        mMethodOverride(m_AtPrbsEngineOverride, RxFixedPatternGet);

        mMethodOverride(m_AtPrbsEngineOverride, AlarmGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryGet);
        mMethodOverride(m_AtPrbsEngineOverride, AlarmHistoryClear);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, MonitoringSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideSet);
        mMethodOverride(m_AtPrbsEngineOverride, GeneratingSideGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterGet);
        mMethodOverride(m_AtPrbsEngineOverride, CounterClear);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultMode);
        mMethodOverride(m_AtPrbsEngineOverride, NeedSetDefaultPrbsLoss);
        }

    mMethodsSet(self, &m_AtPrbsEngineOverride);
    }

static void OverrideTha61031031PrbsEngineTdmPw(AtPrbsEngine self)
    {
    Tha61031031PrbsEngineTdmPw engine = (Tha61031031PrbsEngineTdmPw)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_Tha61031031PrbsEngineTdmPwOverride, mMethodsGet(engine), sizeof(m_Tha61031031PrbsEngineTdmPwOverride));


        }

    mMethodsSet(engine, &m_Tha61031031PrbsEngineTdmPwOverride);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha6A210031PrbsEngineDe3);
    }

static void Override(AtPrbsEngine self)
    {
    OverrideAtPrbsEngine(self);
    OverrideTha61031031PrbsEngineTdmPw(self);
    }

static AtPrbsEngine ObjectInit(AtPrbsEngine self, AtPdhDe3 de3)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha61150011PrbsEngineDe3ObjectInit(self, de3) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPrbsEngine Tha6A210031PrbsEngineDe3New(AtPdhDe3 de3)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPrbsEngine newEngine = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());

    /* Construct it */
    return ObjectInit(newEngine, de3);
    }

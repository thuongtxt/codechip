/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210031ModulePrbs.h
 * 
 * Created Date: Dec 3, 2015
 *
 * Description : PRBS module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031MODULEPRBS_H_
#define _THA6A210031MODULEPRBS_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A210031ModulePrbs * Tha6A210031ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha6A210031ModulePrbsNew(AtDevice device);

eBool Tha6A210031ModulePrbsNeedShiftUp(Tha6A210031ModulePrbs self);

uint32 Tha6A210031ModulePrbsMonReg(Tha6A210031ModulePrbs self, eBool isHo);
uint32 Tha6A210031ModulePrbsGenReg(Tha6A210031ModulePrbs self, eBool isHo);

eAtPrbsMode Tha6A210031ModulePrbsModeFromHwValue(Tha6A210031ModulePrbs self, eAtPrbsMode catchedPrbsMode, uint8 mode, uint8 step);
uint8 Tha6A210031ModulePrbsModeToHwValue(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode);
uint8 Tha6A210031ModulePrbsHwStepFromPrbsMode(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode);

uint32 Tha6A210031ModulePrbsModeMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection);
uint32 Tha6A210031ModulePrbsDataModeMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection);
uint32 Tha6A210031ModulePrbsStepMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection);
uint32 Tha6A210031ModulePrbsEnableMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection);
uint32 Tha6A210031ModulePrbsInvertMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection);
uint32 Tha6A210031ModulePrbsFixPatternMask(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection);
uint32 Tha6A210031ModulePrbsFixPatternReg(Tha6A210031ModulePrbs self, eBool isHo, eBool isTxDirection);
uint32 Tha6A210031ModulePrbsErrFrcMask(Tha6A210031ModulePrbs self, eBool isHo);

uint32 Tha6A210031ModulePrbsCounterReg(Tha6A210031ModulePrbs self, eBool isHo, eBool r2c);
uint32 Tha6A210031ModulePrbsCounterMask(Tha6A210031ModulePrbs self, uint16 counterType);
eBool Tha6A210031ModulePrbsCounterIsSupported(Tha6A210031ModulePrbs self, uint16 counterType);

uint32 Tha6A210031ModulePrbsDelayConfigReg(Tha6A210031ModulePrbs self, eBool isHo, uint8 slice);
uint32 Tha6A210031ModulePrbsDelayIdMask(Tha6A210031ModulePrbs self, eBool isHo);
uint32 Tha6A210031ModulePrbsDelayEnableMask(Tha6A210031ModulePrbs self, eBool isHo);
eBool Tha6A210031ModulePrbsModeIsSupported(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode);
eBool Tha6A210031ModulePrbsDataModeIsSupported(Tha6A210031ModulePrbs self);

uint32 Tha6A210031ModulePrbsConvertSwToHwFixedPattern(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern);
uint32 Tha6A210031ModulePrbsConvertHwToSwFixedPattern(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern);
eBool Tha6A210031ModulePrbsNeedDelayAverage(Tha6A210031ModulePrbs self);

eBool  Tha6A210031ModulePrbsLossIsSuppported(Tha6A210031ModulePrbs self);
uint32 Tha6A210031ModulePrbsLossTimerUnitReg(Tha6A210031ModulePrbs self);
uint32 Tha6A210031ModulePrbsLossTimerCfgReg(Tha6A210031ModulePrbs self);
uint32 Tha6A210031ModulePrbsLossTimerStatReg(Tha6A210031ModulePrbs self);
uint32 Tha6A210031ModulePrbsExpectMaxTimeMaskLo(Tha6A210031ModulePrbs self);
uint32 Tha6A210031ModulePrbsExpectMaxTimeMaskHo(Tha6A210031ModulePrbs self);
uint32 Tha6A210031ModulePrbsLossCurTimeMask(Tha6A210031ModulePrbs self);
uint32 Tha6A210031ModulePrbsLossSigThresMask(Tha6A210031ModulePrbs self);
uint32 Tha6A210031ModulePrbsSyncDectectThresMask(Tha6A210031ModulePrbs self);
uint32 Tha6A210031ModulePrbsLossDectectThresMask(Tha6A210031ModulePrbs self);
uint32 Tha6A210031ModulePrbsLossUnitMask(Tha6A210031ModulePrbs self);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031MODULEPRBS_H_ */


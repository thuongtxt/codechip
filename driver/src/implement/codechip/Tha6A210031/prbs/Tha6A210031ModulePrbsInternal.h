/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PRBS
 * 
 * File        : Tha6A210031ModulePrbsInternal.h
 * 
 * Created Date: Dec 8, 2015
 *
 * Description : Module PRBS internal header
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031MODULEPRBSINTERNAL_H_
#define _THA6A210031MODULEPRBSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha61150011/prbs/Tha61150011ModulePrbs.h"
#include "../../Tha61150011/prbs/Tha61150011PrbsEngine.h"
#include "Tha6A210031ModulePrbs.h"
/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A210031ModulePrbsMethods
    {
    uint32 (*StartVersionSupportFixedPattern4bytes)(Tha6A210031ModulePrbs self);
    eBool  (*NeedFixedPattern4bytes)(Tha6A210031ModulePrbs self);

    eBool  (*NeedDelayAverage)(Tha6A210031ModulePrbs self);
    uint32 (*StartVersionSupportShiftUp)(Tha6A210031ModulePrbs self);
    eBool  (*NeedShiftUp)(Tha6A210031ModulePrbs self);
    uint32 (*StartVersionSupportDelay)(Tha6A210031ModulePrbs self);
    eBool  (*NeedDelay)(Tha6A210031ModulePrbs self);

    eAtRet (*SdhLineInit)(Tha6A210031ModulePrbs self);
    eAtRet (*SdhHoPathInit)(Tha6A210031ModulePrbs self);
    eAtRet (*SdhLoPathInit)(Tha6A210031ModulePrbs self);
    eAtRet (*PdhDe3Init)(Tha6A210031ModulePrbs self);
    eAtRet (*PdhDe1Init)(Tha6A210031ModulePrbs self);

    uint32 (*PrbsDelayConfigReg)(Tha6A210031ModulePrbs self, eBool isHo, uint8 slice);
    uint32 (*PrbsDelayIdMask)(Tha6A210031ModulePrbs self, eBool isHo);
    uint32 (*PrbsDelayEnableMask)(Tha6A210031ModulePrbs self, eBool isHo);
    uint32 (*PrbsGenReg)(Tha6A210031ModulePrbs self, eBool isHo);
    uint32 (*PrbsMonReg)(Tha6A210031ModulePrbs self, eBool isHo);
    uint32 (*PrbsMaxDelayReg)(Tha6A210031ModulePrbs self, eBool isHo, eBool r2c, uint8 slice);
    uint32 (*PrbsMinDelayReg)(Tha6A210031ModulePrbs self, eBool isHo, eBool r2c, uint8 slice);
    uint32 (*PrbsAverageDelayReg)(Tha6A210031ModulePrbs self, eBool isHo, eBool r2c, uint8 slice);
    uint8 (*LoPrbsEngineOc48IdShift)(Tha6A210031ModulePrbs self);
    uint8 (*LoPrbsEngineOc24SliceIdShift)(Tha6A210031ModulePrbs self);

    eAtPrbsMode (*PrbsModeFromHwValue)(Tha6A210031ModulePrbs self, eAtPrbsMode catchedPrbsMode, uint8 mode, uint8 step);
    uint8 (*PrbsModeToHwValue)(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode);
    uint8 (*HwStepFromPrbsMode)(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode);
    uint32 (*PrbsModeMask)(Tha6A210031ModulePrbs self, eBool isHo, eBool isTx);
    uint32 (*PrbsDataModeMask)(Tha6A210031ModulePrbs self, eBool isHo, eBool isTx);
    uint32 (*PrbsStepMask)(Tha6A210031ModulePrbs self, eBool isHo, eBool isTx);
    uint32 (*PrbsEnableMask)(Tha6A210031ModulePrbs self, eBool isHo, eBool isTx);
    uint32 (*PrbsFixPatternMask)(Tha6A210031ModulePrbs self, eBool isHo, eBool isTx);
    uint32 (*PrbsFixPatternReg)(Tha6A210031ModulePrbs self, eBool isHo, eBool isTx);
    uint32 (*PrbsInvertMask)(Tha6A210031ModulePrbs self, eBool isHo, eBool isTx);
    uint32 (*PrbsErrFrcMask)(Tha6A210031ModulePrbs self, eBool isHo);

    uint32 (*PrbsCounterReg)(Tha6A210031ModulePrbs self, eBool isHo, eBool r2c);
    uint32 (*PrbsCounterMask)(Tha6A210031ModulePrbs self, uint16 counterType);
    eBool (*CounterIsSupported)(Tha6A210031ModulePrbs self, uint16 counterType);
    eBool (*PrbsModeIsSupported)(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode);
    eBool (*PrbsDataModeIsSupported)(Tha6A210031ModulePrbs self);

    uint32 (*ConvertSwToHwFixedPattern)(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern);
    uint32 (*ConvertHwToSwFixedPattern)(Tha6A210031ModulePrbs self, eAtPrbsMode prbsMode, uint32 fixedPattern);

    uint32 (*StartVersionSupportPrbsLoss)(Tha6A210031ModulePrbs self);
    eBool  (*PrbsLossIsSuppported)(Tha6A210031ModulePrbs self);
    uint32 (*PrbsLossTimerUnitReg)(Tha6A210031ModulePrbs self);
    uint32 (*PrbsLossTimerCfgReg)(Tha6A210031ModulePrbs self);
    uint32 (*PrbsLossTimerStatReg)(Tha6A210031ModulePrbs self);
    uint32 (*PrbsLossExpectMaxTimeMaskLo)(Tha6A210031ModulePrbs self);
    uint32 (*PrbsLossExpectMaxTimeMaskHo)(Tha6A210031ModulePrbs self);
    uint32 (*PrbsLossCurTimeMask)(Tha6A210031ModulePrbs self);
    uint32 (*PrbsLossSigThresMask)(Tha6A210031ModulePrbs self);
    uint32 (*PrbsSyncDectectThresMask)(Tha6A210031ModulePrbs self);
    uint32 (*PrbsLossDectectThresMask)(Tha6A210031ModulePrbs self);
    uint32 (*PrbsLossUnitMask)(Tha6A210031ModulePrbs self);
    }tTha6A210031ModulePrbsMethods;

typedef struct tTha6A210031ModulePrbs
    {
    tTha61150011ModulePrbs super;
    const tTha6A210031ModulePrbsMethods *methods;
    }tTha6A210031ModulePrbs;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePrbs Tha6A210031ModulePrbsObjectInit(AtModulePrbs self, AtDevice device);
AtPrbsEngine Tha6A210031PrbsEnginePwObjectInit(AtPrbsEngine self, AtPw pw);
AtPrbsEngine Tha6A210031PrbsEngineVcNew(AtSdhChannel vc);
AtPrbsEngine Tha6A210031PrbsEngineDe1New(AtPdhDe1 de1);
AtPrbsEngine Tha6A210031PrbsEngineDe3New(AtPdhDe3 de3);
AtPrbsEngine Tha6A210031PrbsEngineNxDs0New(AtPdhNxDS0 nxDs0);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031MODULEPRBSINTERNAL_H_ */


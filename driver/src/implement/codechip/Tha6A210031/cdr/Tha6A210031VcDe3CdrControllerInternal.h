/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha6A210031VcDe3CdrControllerInternal.h
 * 
 * Created Date: Mar 4, 2017
 *
 * Description : 6A210031 VC DE3 CDR controller internal definition
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031VCDE3CDRCONTROLLERINTERNAL_H_
#define _THA6A210031VCDE3CDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/cdr/Tha60210031De3CdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A210031VcDe3CdrController * Tha6A210031VcDe3CdrController;
typedef struct tTha6A210031VcDe3CdrController
    {
    tTha60210031De3CdrController super;

    /* Private */
    }tTha6A210031VcDe3CdrController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaCdrController Tha6A210031VcDe3CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);
ThaCdrController Tha6A210031VcDe3CdrControllerNew(uint32 engineId, AtChannel channel);

#endif /* _THA6A210031VCDE3CDRCONTROLLERINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha60210031De3CdrControllerInternal.h
 * 
 * Created Date: May 23, 2017
 *
 * Description : DE3 CDR
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA6A210031DE3CDRCONTROLLERINTERNAL_H_
#define _THA6A210031DE3CDRCONTROLLERINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/cdr/Tha60210031De3CdrControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A210031De3CdrController
    {
    tTha60210031De3CdrController super;
    }tTha6A210031De3CdrController;

/*--------------------------- Forward declarations ---------------------------*/
ThaCdrController Tha6A210031De3CdrControllerNew(uint32 engineId, AtChannel channel);
/*--------------------------- Entries ----------------------------------------*/
ThaCdrController Tha6A210031De3CdrControllerObjectInit(ThaCdrController self, uint32 engineId, AtChannel channel);

#ifdef __cplusplus
}
#endif
#endif /* _THA6A210031DE3CDRCONTROLLERINTERNAL_H_ */


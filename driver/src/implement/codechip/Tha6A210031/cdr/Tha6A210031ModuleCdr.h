/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CDR
 * 
 * File        : Tha6A210031ModuleCdr.h
 * 
 * Created Date: May 23, 2017
 *
 * Description : CDR module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _Tha6A210031MODULECDR_H_
#define _Tha6A210031MODULECDR_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210031/cdr/Tha60210031ModuleCdrInternal.h"


/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha6A210031ModuleCdr
    {
    tTha60210031ModuleCdr super;
    }tTha6A210031ModuleCdr;


/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha6A210031ModuleCdrObjectInit(AtModule self, AtDevice device);
AtModule Tha6A210031ModuleCdrNew(AtDevice device);
#endif /* _Tha6A210031MODULECDR_H_ */


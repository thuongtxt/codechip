/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : Tha60031032PidTableInternal.h
 * 
 * Created Date: Mar 2, 2015
 *
 * Description : PID table
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032PIDTABLEINTERNAL_H_
#define _THA60031032PIDTABLEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/ppp/ThaPidTableInternal.h"
#include "../../../default/ppp/ThaPidTableEntryInternal.h"
#include "Tha60031032PidTable.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032PidTable
    {
    tThaPidTable super;
    }tTha60031032PidTable;

typedef struct tTha60031032PidTableEntry
    {
    tThaPidTableEntry super;
    }tTha60031032PidTableEntry;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPidTable Tha60031032PidTableObjectInit(AtPidTable self, AtModule module);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031032PIDTABLEINTERNAL_H_ */


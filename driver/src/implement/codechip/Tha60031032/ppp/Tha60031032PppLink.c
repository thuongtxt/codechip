/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60031032PppLink.c
 *
 * Created Date: Aug 20, 2013
 *
 * Description : PPP Link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ppp/ThaStmPppLinkInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031032PppLink
    {
    tThaStmPppLink super;
    }tTha60031032PppLink;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaStmPppLinkMethods m_ThaStmPppLinkOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool OamAddrCtrlIsBypass(ThaStmPppLink self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideThaStmPppLink(AtPppLink self)
    {
    ThaStmPppLink stmLink = (ThaStmPppLink)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPppLinkOverride, mMethodsGet(stmLink), sizeof(m_ThaStmPppLinkOverride));
        mMethodOverride(m_ThaStmPppLinkOverride, OamAddrCtrlIsBypass);
        }

    mMethodsSet(stmLink, &m_ThaStmPppLinkOverride);
    }

static void Override(AtPppLink self)
    {
    OverrideThaStmPppLink(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032PppLink);
    }

static AtPppLink ObjectInit(AtPppLink self, AtHdlcChannel hdlcChannel)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPppLinkObjectInit((AtPppLink)self, hdlcChannel) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPppLink Tha60031032PppLinkNew(AtHdlcChannel hdlcChannel)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPppLink newLink = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newLink == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newLink, hdlcChannel);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : TODO module name
 * 
 * File        : Tha60031032ModulePpp.h
 * 
 * Created Date: Dec 3, 2013
 *
 * Description : TODO Description
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032MODULEPPP_H_
#define _THA60031032MODULEPPP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../ThaStmMlppp/ppp/ThaStmMlpppModulePppInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032ModulePpp
    {
    tThaStmMlpppModulePpp super;
    }tTha60031032ModulePpp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePpp Tha60031032ModulePppObjectInit(AtModulePpp self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031032MODULEPPP_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60031032ModulePpp.c
 *
 * Created Date: Aug 14, 2013
 *
 * Description : PPP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60031032ModulePpp.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModulePppMethods  m_AtModulePppOverride;
static tThaModulePppMethods m_ThaModulePppOverride;

/* Save super implementations */
static const tAtModulePppMethods  *m_AtModulePppMethods  = NULL;
static const tThaModulePppMethods *m_ThaModulePppMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/
/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumOfMpBundleGet(AtModulePpp self)
    {
	AtUnused(self);
    return 256;
    }

static AtPidTable PidTableObjectCreate(AtModulePpp self)
    {
    return Tha60031032PidTableNew((AtModule)self);
    }

static uint32 MpigHighBwMaxNumBlocks(ThaModulePpp self)
    {
	AtUnused(self);
    return 32;
    }

static AtMpBundle MpBundleObjectCreate(AtModulePpp self, uint32 bundleId)
    {
    return Tha60031032MpBundleNew(bundleId, self);
    }

static uint32 NumBlocksForRange(ThaModulePpp self, uint8 rangeId)
    {
    /* This product has burst traffic from main card and range 4 is for VC-4, Vc4_4c and
     * more blocks need be allocated. */
    if (rangeId == 4)
        return 0x3FFF;

    if (rangeId == 7)
        return 0x7FFF;

    return m_ThaModulePppMethods->NumBlocksForRange(self, rangeId);
    }

static uint8 MaxNumQueuesPerLink(ThaModulePpp self)
    {
	AtUnused(self);
    return 8;
    }

static void OverrideAtModulePpp(AtModulePpp self)
    {
    AtModulePpp pppModule = (AtModulePpp)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModulePppMethods = mMethodsGet(pppModule);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModulePppOverride, m_AtModulePppMethods, sizeof(m_AtModulePppOverride));
        mMethodOverride(m_AtModulePppOverride, MaxNumOfMpBundleGet);
        mMethodOverride(m_AtModulePppOverride, PidTableObjectCreate);
        mMethodOverride(m_AtModulePppOverride, MpBundleObjectCreate);
        }

    mMethodsSet(pppModule, &m_AtModulePppOverride);
    }

static void OverrideThaModulePpp(AtModulePpp self)
    {
    ThaModulePpp thaPppModule = (ThaModulePpp)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModulePppMethods = mMethodsGet(thaPppModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModulePppOverride, m_ThaModulePppMethods, sizeof(m_ThaModulePppOverride));
        mMethodOverride(m_ThaModulePppOverride, MpigHighBwMaxNumBlocks);
        mMethodOverride(m_ThaModulePppOverride, NumBlocksForRange);
        mMethodOverride(m_ThaModulePppOverride, MaxNumQueuesPerLink);
        }

    mMethodsSet(thaPppModule, &m_ThaModulePppOverride);
    }

static void Override(AtModulePpp self)
    {
    OverrideAtModulePpp(self);
    OverrideThaModulePpp(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032ModulePpp);
    }

AtModulePpp Tha60031032ModulePppObjectInit(AtModulePpp self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmMlpppModulePppObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModulePpp Tha60031032ModulePppNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModulePpp newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60031032ModulePppObjectInit(newModule, device);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PPP
 * 
 * File        : Tha60031032PidTable.h
 * 
 * Created Date: Dec 5, 2013
 *
 * Description : PID table
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032PIDTABLE_H_
#define _THA60031032PIDTABLE_H_

/*--------------------------- Includes ---------------------------------------*/
#include "AtPidTable.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPidTableEntry Tha60031032PidTableEntryNew(AtPidTable self, uint32 entryIndex);

/* PPP */
eAtRet Tha60031032PppEncapTypeSet(AtPidTable self, uint32 entryIndex, uint8 encapType);
uint8 Tha60031032PidTableEncapTypeGet(AtPidTable self, uint32 entryIndex);
eAtRet Tha60031032PidTablePrioritySet(AtPidTable self, uint32 entryIndex, uint8 priority);
uint8 Tha60031032PidTablePriorityGet(AtPidTable self, uint16 entryIndex);
eAtRet Tha60031032PidTableCfiSet(AtPidTable self, uint32 entryIndex, uint8 cfi);
uint8 Tha60031032PidTableCfiGet(AtPidTable self, uint16 entryIndex);
eBool Tha60031032PidTableEntryIsLcp(AtPidTable self, uint32 entryIndex);

/* MLPPP */
eAtRet Tha60031032MlpppEncapTypeSet(AtPidTable self, uint32 entryIndex, uint8 encapType);
uint8 Tha60031032MlpppEncapTypeGet(AtPidTable self, uint32 entryIndex);

/* c-HDLC */
eAtRet Tha60031032CHdlcEncapTypeSet(AtPidTable self, uint8 encapType);
uint8 Tha60031032CHdlcEncapTypeGet(AtPidTable self);
eAtRet Tha60031032CHdlcPidTableCfiSet(AtPidTable self, uint32 entryIndex, uint8 cfi);
uint8 Tha60031032CHdlcPidTableCfiGet(AtPidTable self, uint32 entryIndex);
eAtRet Tha60031032CHdlcPidTablePrioritySet(AtPidTable self, uint32 entryIndex, uint8 priority);
uint8 Tha60031032CHdlcPidTablePriorityGet(AtPidTable self, uint32 entryIndex);

/* FR */
eAtRet Tha60031032FrEncapTypeSet(AtPidTable self, uint8 encapType);
uint8 Tha60031032FrEncapTypeGet(AtPidTable self);
eAtRet Tha60031032FrPidTablePrioritySet(AtPidTable self, uint8 priority);
uint8 Tha60031032FrPidTablePriorityGet(AtPidTable self);
eAtRet Tha60031032FrPidTableCfiSet(AtPidTable self, uint8 cfi);
uint8 Tha60031032FrPidTableCfiGet(AtPidTable self);

eBool Tha60031032PidTableEntryHasEthType(AtPidTable self, uint16 entryIndex);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031032PIDTABLE_H_ */


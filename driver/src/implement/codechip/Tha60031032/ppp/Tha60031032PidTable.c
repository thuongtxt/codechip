/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60031032PidTable.c
 *
 * Created Date: Sep 26, 2013
 *
 * Description : PID table
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/encap/ThaModuleEncap.h"
#include "Tha60031032PidTableInternal.h"

/*--------------------------- Define -----------------------------------------*/
#define cMpigPriorityMask  cBit3_1
#define cMpigPriorityShift 1
#define cMpigCfiMask       cBit0
#define cMpigCfiShift      0
#define cIsIsPacketType    4
#define cBCPPacketType     5

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPidTableMethods  m_AtPidTableOverride;
static tThaPidTableMethods m_ThaPidTableOverride;

/* To save super implementation */
static const tAtPidTableMethods  *m_AtPidTableMethods  = NULL;
static const tThaPidTableMethods *m_ThaPidTableMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumEntries(AtPidTable self)
    {
	AtUnused(self);
    return 16;
    }

static uint8 NumStandardEntries(ThaPidTable self)
    {
	AtUnused(self);
    return 6;
    }

static eBool IsStandardEntry(ThaPidTable self, uint32 entryIndex)
    {
    return entryIndex < NumStandardEntries(self);
    }

static uint32 EntryDefaultPid(ThaPidTable self, uint32 entryIndex)
    {
    if (entryIndex == 5)
        return 0x0023;

    if (IsStandardEntry(self, entryIndex))
        return m_ThaPidTableMethods->EntryDefaultPid(self, entryIndex);

    return 0x0021;
    }

static uint32 EntryDefaultEthType(ThaPidTable self, uint32 entryIndex)
    {
    if (IsStandardEntry(self, entryIndex))
        return m_ThaPidTableMethods->EntryDefaultEthType(self, entryIndex);

    return 0x0800;
    }

static uint8 DefaultEncapType(AtPidTable self, uint32 entryIndex)
    {
    if ((entryIndex == 0) || (entryIndex == 1)) return 0x01;
    if ((entryIndex == 2) || (entryIndex == 3)) return 0x03;
    if ((entryIndex == 4) || (entryIndex == 5)) return 0x11;
    if (Tha60031032PidTableEntryIsLcp(self, entryIndex))
        return 0x10;

    return 0x01;
    }

static eAtRet EncapTableInit(AtPidTable self)
    {
    uint32 entry_i;
    eAtRet ret = cAtOk;
    static const uint8 cMlpppEncapType = 0xE;

    for (entry_i = 0; entry_i < AtPidTableMaxNumEntries(self); entry_i++)
        {
        ret |= Tha60031032PppEncapTypeSet(self, entry_i, DefaultEncapType(self, entry_i));
        ret |= Tha60031032MlpppEncapTypeSet(self, entry_i, (uint8)cMlpppEncapType);
        }

    return ret;
    }

static uint8 DefaultPriority(AtPidTable self, uint32 entryIndex)
    {
	AtUnused(entryIndex);
	AtUnused(self);
    return 0;
    }

static uint8 DefaultCfi(AtPidTable self, uint32 entryIndex)
    {
	AtUnused(entryIndex);
	AtUnused(self);
    return 0;
    }

static eAtRet PriorityDefaultSet(AtPidTable self)
    {
    uint32 entry_i;
    eAtRet ret = cAtOk;

    for (entry_i = 0; entry_i < AtPidTableMaxNumEntries(self); entry_i++)
        ret |= Tha60031032PidTablePrioritySet(self, (uint16)entry_i, DefaultPriority(self, entry_i));

    return ret;
    }

static eAtRet CfiDefaultSet(AtPidTable self)
    {
    uint32 entry_i;
    eAtRet ret = cAtOk;

    for (entry_i = 0; entry_i < AtPidTableMaxNumEntries(self); entry_i++)
        ret |= Tha60031032PidTableCfiSet(self, (uint16)entry_i, DefaultCfi(self, entry_i));

    return ret;
    }

static eAtRet CHdlcTableInit(AtPidTable self)
    {
    static const uint8 cHdlcEncapType = 0x17;
    uint8 type_i;
    eAtRet ret = cAtOk;
    uint8 hwTypes[] = {0x0, 0x4, 0xF};

    ret |= Tha60031032CHdlcEncapTypeSet(self, cHdlcEncapType);

    for (type_i = 0; type_i < mCount(hwTypes); type_i++)
        {
        ret |= Tha60031032CHdlcPidTableCfiSet(self, hwTypes[type_i], DefaultCfi(self, type_i));
        ret |= Tha60031032CHdlcPidTablePrioritySet(self, hwTypes[type_i], DefaultCfi(self, type_i));
        }

    return ret;
    }

static eBool FrameRelayIsSupported(AtPidTable self)
    {
    AtDevice device = AtModuleDeviceGet(AtPidTableModuleGet(self));
    ThaModuleEncap encapModule = (ThaModuleEncap)AtDeviceModuleGet(device, cAtModuleEncap);
    return ThaModuleEncapFrameRelayIsSupported(encapModule);
    }

static eAtRet FrTableInit(AtPidTable self)
    {
    static const uint8 cFrEncapType = 0x6;
    eAtRet ret = cAtOk;

    ret |= Tha60031032FrEncapTypeSet(self, cFrEncapType);
    ret |= Tha60031032FrPidTablePrioritySet(self, 0x0);
    ret |= Tha60031032FrPidTableCfiSet(self, 0x0);

    return ret;
    }

static eAtRet Init(AtPidTable self)
    {
    eAtRet ret = cAtOk;

    m_AtPidTableMethods->Init(self);

    ret |= EncapTableInit(self);
    ret |= PriorityDefaultSet(self);
    ret |= CfiDefaultSet(self);
    ret |= CHdlcTableInit(self);
    
    if (FrameRelayIsSupported(self))
        ret |= FrTableInit(self);

    return ret;
    }

static uint32 PppClaEncapTypeCtrlRegister(AtPidTable self, uint32 entryIndex)
    {
	AtUnused(self);
    return 0x44C040 + entryIndex;
    }

static uint32 PppMpigEncapTypeCtrlRegister(AtPidTable self, uint32 entryIndex)
    {
	AtUnused(self);
    return 0x840180 + entryIndex;
    }

static uint32 MlpppClaEncapTypeCtrlRegister(AtPidTable self, uint32 entryIndex)
    {
	AtUnused(self);
    return 0x44C070 + entryIndex;
    }

static uint32 MlpppMpigEncapTypeCtrlRegister(AtPidTable self, uint32 entryIndex)
    {
	AtUnused(self);
    return 0x8401A0 + entryIndex;
    }

static uint32 MpigPriorityCfiCtrlRegister(AtPidTable self, uint32 entryIndex)
    {
	AtUnused(self);
    return 0x840190 + entryIndex;
    }

static eBool NeedDefaultSettings(AtPidTable self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 CHdlcClaEncapTypeCtrlRegister(AtPidTable self)
    {
	AtUnused(self);
    return 0x44C07F;
    }

static uint32 CHdlcMpigEncapTypeCtrlRegister(AtPidTable self)
    {
	AtUnused(self);
    return 0x8401C0;
    }

static uint32 CHdlcMpigPriorityCfiCtrlRegister(AtPidTable self, uint32 entryIndex)
    {
	AtUnused(self);
    return 0x8401B0UL + entryIndex;
    }

static uint32 FrClaEncapTypeCtrlRegister(AtPidTable self)
    {
	AtUnused(self);
    return 0x44C080;
    }

static uint32 FrMpigEncapTypeCtrlRegister(AtPidTable self)
    {
	AtUnused(self);
    return 0x8401D0;
    }

static uint32 FrMpigPriorityCfiCtrlRegister(AtPidTable self)
    {
	AtUnused(self);
    return 0x8401D1;
    }

static AtPidTableEntry EntryObjectCreate(AtPidTable self, uint32 entryIndex)
    {
    return Tha60031032PidTableEntryNew(self, entryIndex);
    }

static eBool EntryIsValid(AtPidTableEntry self, uint8 encapType)
    {
    if ((self == NULL) || (encapType > cBit4_0))
        return cAtFalse;

    return cAtTrue;
    }

static eAtRet CfiSet(AtPidTable self, uint32 regAddr, uint32 entryIndex, uint8 cfi)
    {
    AtPidTableEntry entry = AtPidTableEntryGet(self, entryIndex);
    uint32 regVal;

    if (entry == NULL)
        return cAtErrorInvlParm;

    if (cfi > 1)
        return cAtErrorInvlParm;

    regVal  = AtPidTableEntryRead(entry, regAddr, cThaModuleMpig);
    mRegFieldSet(regVal, cMpigCfi, cfi);
    AtPidTableEntryWrite(entry, regAddr, regVal, cThaModuleMpig);

    return cAtOk;
    }

static uint8 CfiGet(AtPidTable self, uint32 regAddr, uint32 entryIndex)
    {
    AtPidTableEntry entry = AtPidTableEntryGet(self, entryIndex);
    uint32 regVal;

    if (entry == NULL)
        return 0;

    regVal = AtPidTableEntryRead(entry, regAddr, cThaModuleMpig);
    return (uint8)mRegField(regVal, cMpigCfi);
    }

static eAtRet PrioritySet(AtPidTable self, uint32 regAddr, uint32 entryIndex, uint8 priority)
    {
    AtPidTableEntry entry = AtPidTableEntryGet(self, entryIndex);
    uint32 regVal;

    if (entry == NULL)
        return cAtErrorInvlParm;

    if (priority > 7)
        return cAtErrorInvlParm;

    regVal  = AtPidTableEntryRead(entry, regAddr, cThaModuleMpig);
    mRegFieldSet(regVal, cMpigPriority, priority);
    AtPidTableEntryWrite(entry, regAddr, regVal, cThaModuleMpig);

    return cAtOk;
    }

static uint8 PriorityGet(AtPidTable self, uint32 regAddr, uint32 entryIndex)
    {
    AtPidTableEntry entry = AtPidTableEntryGet(self, entryIndex);
    uint32 regVal;

    if (entry == NULL)
        return 0;

    regVal  = AtPidTableEntryRead(entry, regAddr, cThaModuleMpig);
    return (uint8)mRegField(regVal, cMpigPriority);
    }

static void OverrideThaPidTable(AtPidTable self)
    {
    ThaPidTable table = (ThaPidTable)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaPidTableMethods = mMethodsGet(table);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaPidTableOverride, m_ThaPidTableMethods, sizeof(m_ThaPidTableOverride));

        mMethodOverride(m_ThaPidTableOverride, EntryDefaultPid);
        mMethodOverride(m_ThaPidTableOverride, EntryDefaultEthType);
        }

    mMethodsSet(table, &m_ThaPidTableOverride);
    }

static void OverrideAtPidTable(AtPidTable self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPidTableMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPidTableOverride, m_AtPidTableMethods, sizeof(m_AtPidTableOverride));

        mMethodOverride(m_AtPidTableOverride, MaxNumEntries);
        mMethodOverride(m_AtPidTableOverride, Init);
        mMethodOverride(m_AtPidTableOverride, NeedDefaultSettings);
        mMethodOverride(m_AtPidTableOverride, EntryObjectCreate);
        }

    mMethodsSet(self, &m_AtPidTableOverride);
    }

static void Override(AtPidTable self)
    {
    OverrideAtPidTable(self);
    OverrideThaPidTable(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032PidTable);
    }

AtPidTable Tha60031032PidTableObjectInit(AtPidTable self, AtModule module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPidTableObjectInit(self, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPidTable Tha60031032PidTableNew(AtModule module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPidTable table = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (table == NULL)
        return NULL;

    return Tha60031032PidTableObjectInit(table, module);
    }

eAtRet Tha60031032PppEncapTypeSet(AtPidTable self, uint32 entryIndex, uint8 encapType)
    {
    AtPidTableEntry entry = AtPidTableEntryGet(self, entryIndex);

    if (!EntryIsValid(entry, encapType))
        return cAtErrorInvlParm;

    AtPidTableEntryWrite(entry, PppClaEncapTypeCtrlRegister(self, entryIndex), encapType, cThaModuleCla);
    AtPidTableEntryWrite(entry, PppMpigEncapTypeCtrlRegister(self, entryIndex), encapType, cThaModuleMpig);

    return cAtOk;
    }

uint8 Tha60031032PidTableEncapTypeGet(AtPidTable self, uint32 entryIndex)
    {
    AtPidTableEntry entry = AtPidTableEntryGet(self, entryIndex);
    return (uint8)(AtPidTableEntryRead(entry, PppClaEncapTypeCtrlRegister(self, entryIndex), cThaModuleCla) & cBit4_0);
    }

eAtRet Tha60031032PidTableCfiSet(AtPidTable self, uint32 entryIndex, uint8 cfi)
    {
    return CfiSet(self, MpigPriorityCfiCtrlRegister(self, (uint8)entryIndex), entryIndex, cfi);
    }

uint8 Tha60031032PidTableCfiGet(AtPidTable self, uint16 entryIndex)
    {
    return CfiGet(self, MpigPriorityCfiCtrlRegister(self, (uint8)entryIndex), entryIndex);
    }

eAtRet Tha60031032PidTablePrioritySet(AtPidTable self, uint32 entryIndex, uint8 priority)
    {
    return PrioritySet(self, MpigPriorityCfiCtrlRegister(self, (uint8)entryIndex), entryIndex, priority);
    }

uint8 Tha60031032PidTablePriorityGet(AtPidTable self, uint16 entryIndex)
    {
    return PriorityGet(self, MpigPriorityCfiCtrlRegister(self, (uint8)entryIndex), entryIndex);
    }

eBool Tha60031032PidTableEntryIsLcp(AtPidTable self, uint32 entryIndex)
    {
    return entryIndex == (AtPidTableMaxNumEntries(self) - 1);
    }

eBool Tha60031032PidTableEntryHasEthType(AtPidTable self, uint16 entryIndex)
    {
    if ((entryIndex == cIsIsPacketType) || (entryIndex == cBCPPacketType))
        return cAtFalse;
    if (Tha60031032PidTableEntryIsLcp(self, entryIndex))
        return cAtFalse;

    return cAtTrue;
    }

eAtRet Tha60031032MlpppEncapTypeSet(AtPidTable self, uint32 entryIndex, uint8 encapType)
    {
    AtPidTableEntry entry = AtPidTableEntryGet(self, entryIndex);

    if (!EntryIsValid(entry, encapType))
        return cAtErrorInvlParm;

    AtPidTableEntryWrite(entry, MlpppClaEncapTypeCtrlRegister(self, entryIndex), encapType, cThaModuleCla);
    AtPidTableEntryWrite(entry, MlpppMpigEncapTypeCtrlRegister(self, entryIndex), encapType, cThaModuleMpig);

    return cAtOk;
    }

uint8 Tha60031032MlpppEncapTypeGet(AtPidTable self, uint32 entryIndex)
    {
    AtPidTableEntry entry = AtPidTableEntryGet(self, entryIndex);
    return AtPidTableEntryRead(entry, MlpppClaEncapTypeCtrlRegister(self, entryIndex), cThaModuleCla) & cBit4_0;
    }

eAtRet Tha60031032CHdlcEncapTypeSet(AtPidTable self, uint8 encapType)
    {
    AtPidTableEntry entry = AtPidTableEntryGet(self, 0);

    if (!EntryIsValid(entry, encapType))
        return cAtErrorInvlParm;

    AtPidTableEntryWrite(entry, CHdlcClaEncapTypeCtrlRegister(self), encapType, cThaModuleCla);
    AtPidTableEntryWrite(entry, CHdlcMpigEncapTypeCtrlRegister(self), encapType, cThaModuleMpig);

    return cAtOk;
    }

uint8 Tha60031032CHdlcEncapTypeGet(AtPidTable self)
    {
    AtPidTableEntry entry = AtPidTableEntryGet(self, 0);
    return AtPidTableEntryRead(entry, CHdlcClaEncapTypeCtrlRegister(self), cThaModuleCla) & cBit4_0;
    }

eAtRet Tha60031032CHdlcPidTableCfiSet(AtPidTable self, uint32 entryIndex, uint8 cfi)
    {
    return CfiSet(self, CHdlcMpigPriorityCfiCtrlRegister(self, entryIndex), entryIndex, cfi);
    }

uint8 Tha60031032CHdlcPidTableCfiGet(AtPidTable self, uint32 entryIndex)
    {
    return CfiGet(self, CHdlcMpigPriorityCfiCtrlRegister(self, entryIndex), entryIndex);
    }

eAtRet Tha60031032CHdlcPidTablePrioritySet(AtPidTable self, uint32 entryIndex, uint8 priority)
    {
    return PrioritySet(self, CHdlcMpigPriorityCfiCtrlRegister(self, entryIndex), entryIndex, priority);
    }

uint8 Tha60031032CHdlcPidTablePriorityGet(AtPidTable self, uint32 entryIndex)
    {
    return PriorityGet(self, CHdlcMpigPriorityCfiCtrlRegister(self, entryIndex), entryIndex);
    }

eAtRet Tha60031032FrEncapTypeSet(AtPidTable self, uint8 encapType)
    {
    AtPidTableEntry entry = AtPidTableEntryGet(self, 0);

    if (!EntryIsValid(entry, encapType))
        return cAtErrorInvlParm;

    AtPidTableEntryWrite(entry, FrClaEncapTypeCtrlRegister(self), encapType, cThaModuleCla);
    AtPidTableEntryWrite(entry, FrMpigEncapTypeCtrlRegister(self), encapType, cThaModuleMpig);

    return cAtOk;
    }

uint8 Tha60031032FrEncapTypeGet(AtPidTable self)
    {
    AtPidTableEntry entry = AtPidTableEntryGet(self, 0);

    if (entry == NULL)
        return 0;

    return AtPidTableEntryRead(entry, FrClaEncapTypeCtrlRegister(self), cThaModuleCla) & cBit4_0;
    }

eAtRet Tha60031032FrPidTablePrioritySet(AtPidTable self, uint8 priority)
    {
    return PrioritySet(self, FrMpigPriorityCfiCtrlRegister(self), 0, priority);
    }

uint8 Tha60031032FrPidTablePriorityGet(AtPidTable self)
    {
    return PriorityGet(self, FrMpigPriorityCfiCtrlRegister(self), 0);
    }

eAtRet Tha60031032FrPidTableCfiSet(AtPidTable self, uint8 cfi)
    {
    return CfiSet(self, FrMpigPriorityCfiCtrlRegister(self), 0, cfi);
    }

uint8 Tha60031032FrPidTableCfiGet(AtPidTable self)
    {
    return CfiGet(self, FrMpigPriorityCfiCtrlRegister(self), 0);
    }

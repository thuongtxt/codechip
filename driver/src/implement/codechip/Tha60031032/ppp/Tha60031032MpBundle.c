/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60031032MpBundle.c
 *
 * Created Date: Jan 2, 2014
 *
 * Description : MLPPP bundle
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ppp/ThaStmMpBundleInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031032MpBundle
    {
    tThaStmMpBundle super;
    }tTha60031032MpBundle;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaMpBundleMethods m_ThaMpBundleOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 MaxNumQueues(ThaMpBundle self)
    {
	AtUnused(self);
    return 8;
    }

static void OverrideThaMpBundle(AtMpBundle self)
    {
    ThaMpBundle mpBundle = (ThaMpBundle)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaMpBundleOverride, mMethodsGet(mpBundle), sizeof(m_ThaMpBundleOverride));
        mMethodOverride(m_ThaMpBundleOverride, MaxNumQueues);
        }

    mMethodsSet(mpBundle, &m_ThaMpBundleOverride);
    }

static void Override(AtMpBundle self)
    {
    OverrideThaMpBundle(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032MpBundle);
    }

static AtMpBundle ObjectInit(AtMpBundle self, uint32 channelId, AtModulePpp module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmMpBundleObjectInit(self, channelId, (AtModule)module) == NULL)
        return NULL;

    /* Override */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtMpBundle Tha60031032MpBundleNew(uint32 channelId, AtModulePpp module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtMpBundle newBundle = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newBundle == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newBundle, channelId, module);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : PPP
 *
 * File        : Tha60031032PidTableEntry.c
 *
 * Created Date: Oct 16, 2013
 *
 * Description : PID table entry
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/ppp/AtPidTableInternal.h"
#include "../../../default/ppp/ThaMpigReg.h"
#include "../../../default/cla/ppp/ThaModuleClaPppReg.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "Tha60031032PidTableInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static char m_methodsInit = 0;

/* Override */
static tAtPidTableEntryMethods m_AtPidTableEntryOverride;

/* Save super implementation */
static const tAtPidTableEntryMethods *m_AtPidTableEntryMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool IsLcpEntry(AtPidTableEntry self)
    {
    return Tha60031032PidTableEntryIsLcp(AtPidTableEntryTableGet(self), (uint16)AtPidTableEntryIndexGet(self));
    }

static uint16 EnableSw2Hw(eBool enable)
    {
    return enable ? 0xFFFF : 0x0;
    }

static eBool EnableHw2Sw(uint32 hwEnableValue)
    {
    return (hwEnableValue == 0xFFFF) ? cAtTrue : cAtFalse;
    }

static eAtRet PsnToTdmEnable(AtPidTableEntry self, eBool enable)
    {
    uint32 regAddr, regVal;

    if (IsLcpEntry(self) && (!enable))
        return cAtErrorNotEditable;

    regAddr = cThaClaEthType2PppPidCtrl + AtPidTableEntryIndexGet(self);
    regVal  = AtPidTableEntryRead(self, regAddr, cThaModuleCla);
    mFieldIns(&regVal, cThaClaEthTypeMaskMask, cThaClaEthTypeMaskShift, EnableSw2Hw(enable));
    AtPidTableEntryWrite(self, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static eBool PsnToTdmIsEnabled(AtPidTableEntry self)
    {
    uint32 regAddr, regVal;

    /* LCP entry is always enabled by hardware */
    if (IsLcpEntry(self))
        return cAtTrue;

    regAddr = cThaClaEthType2PppPidCtrl + AtPidTableEntryIndexGet(self);
    regVal  = AtPidTableEntryRead(self, regAddr, cThaModuleCla);

    return EnableHw2Sw(mRegField(regVal, cThaClaEthTypeMask));
    }

static eAtRet TdmToPsnEnable(AtPidTableEntry self, eBool enable)
    {
    uint32 regAddr, regVal;

    if (IsLcpEntry(self) && (!enable))
        return cAtErrorNotEditable;

    regAddr = cThaRegMPIGDATPppPIDCtrl + AtPidTableEntryIndexGet(self);
    regVal  = AtPidTableEntryRead(self, regAddr, cThaModuleCla);
    mFieldIns(&regVal, cThaMPIGDatPPPPIDMaskMask, cThaMPIGDatPPPPIDMaskShift, EnableSw2Hw(enable));
    AtPidTableEntryWrite(self, regAddr, regVal, cThaModuleCla);

    return cAtOk;
    }

static eBool TdmToPsnIsEnabled(AtPidTableEntry self)
    {
    uint32 regAddr, regVal;

    /* LCP entry is always enabled by hardware */
    if (IsLcpEntry(self))
        return cAtTrue;

    regAddr = cThaRegMPIGDATPppPIDCtrl + AtPidTableEntryIndexGet(self);
    regVal  = AtPidTableEntryRead(self, regAddr, cThaModuleCla);

    return EnableHw2Sw(mRegField(regVal, cThaMPIGDatPPPPIDMask));
    }

static eBool CanChangePid(AtPidTableEntry self)
    {
    return IsLcpEntry(self) ? cAtFalse : cAtTrue;
    }

static eAtRet PsnToTdmPidSet(AtPidTableEntry self, uint32 pid)
    {
    if (CanChangePid(self))
        return m_AtPidTableEntryMethods->PsnToTdmPidSet(self, pid);
    return cAtErrorNotEditable;
    }

static eAtRet TdmToPsnPidSet(AtPidTableEntry self, uint32 pid)
    {
    if (CanChangePid(self))
        return m_AtPidTableEntryMethods->TdmToPsnPidSet(self, pid);
    return cAtErrorNotEditable;
    }

static void OverrideAtPidEntryTable(AtPidTableEntry self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPidTableEntryMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPidTableEntryOverride, m_AtPidTableEntryMethods, sizeof(m_AtPidTableEntryOverride));

        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmPidSet);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnPidSet);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmEnable);
        mMethodOverride(m_AtPidTableEntryOverride, PsnToTdmIsEnabled);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnEnable);
        mMethodOverride(m_AtPidTableEntryOverride, TdmToPsnIsEnabled);
        }

    mMethodsSet(self, &m_AtPidTableEntryOverride);
    }

static void Override(AtPidTableEntry self)
    {
    OverrideAtPidEntryTable(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032PidTableEntry);
    }

static AtPidTableEntry ObjectInit(AtPidTableEntry self, uint32 entryIndex, AtPidTable table)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaPidTableEntryObjectInit(self, entryIndex, table) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPidTableEntry Tha60031032PidTableEntryNew(AtPidTable self, uint32 entryIndex)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPidTableEntry entry = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (entry == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(entry, entryIndex, self);
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60031032EthFlowHeaderProvider.h
 * 
 * Created Date: Oct 8, 2014
 *
 * Description : Header provider
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032ETHFLOWHEADERPROVIDER_H_
#define _THA60031032ETHFLOWHEADERPROVIDER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/eth/controller/ThaEthFlowHeaderProviderInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032EthFlowHeaderProvider * Tha60031032EthFlowHeaderProvider;

typedef struct tTha60031032EthFlowHeaderProviderMethods
    {
    uint32 (*StartVersionWithNcpMlppp)(Tha60031032EthFlowHeaderProvider self);
    }tTha60031032EthFlowHeaderProviderMethods;

typedef struct tTha60031032EthFlowHeaderProvider
    {
    tThaEthFlowHeaderProvider super;
    const tTha60031032EthFlowHeaderProviderMethods *methods;
    }tTha60031032EthFlowHeaderProvider;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaEthFlowHeaderProvider Tha60031032EthFlowHeaderProviderObjectInit(ThaEthFlowHeaderProvider self, AtModuleEth ethModule);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031032ETHFLOWHEADERPROVIDER_H_ */


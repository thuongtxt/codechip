/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60031032ModuleEthInternal.h
 * 
 * Created Date: Aug 22, 2013
 *
 * Description : ETH module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032MODULEETHINTERNAL_H_
#define _THA60031032MODULEETHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/eth/ThaModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032ModuleEth * Tha60031032ModuleEth;

typedef struct tTha60031032ModuleEth
    {
    tThaModuleEthStmPpp super;
    }tTha60031032ModuleEth;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEth Tha60031032ModuleEthObjectInit(AtModuleEth self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031032MODULEETHINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ETH
 * 
 * File        : Tha60031032EthFlowController.h
 * 
 * Created Date: Sep 6, 2013
 *
 * Description : ETH Flow controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032ETHFLOWCONTROLLER_H_
#define _THA60031032ETHFLOWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/eth/controller/ThaEthFlowControllerInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
#define cTha60031032AddrCtrlHdrLength  2

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032PppEthFlowController * Tha60031032PppEthFlowController;

typedef struct tTha60031032PppEthFlowController
    {
    tThaStmPppEthFlowController super;
    }tTha60031032PppEthFlowController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/* Constructor */
ThaEthFlowController Tha60031032PppEthFlowControllerObjectInit(ThaEthFlowController self, AtModuleEth module);

/* utils */
uint8 Tha60031032EthFlowControllerPsnMode(ThaEthFlowController self);
uint32 Tha60031032EthFlowControllerVlanIdGet(ThaEthFlowController self, uint32 vlanId);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031032ETHFLOWCONTROLLER_H_ */


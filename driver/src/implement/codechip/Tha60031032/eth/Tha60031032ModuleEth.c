/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60031032ModuleEth.c
 *
 * Created Date: Aug 14, 2013
 *
 * Description : ETH Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/eth/ThaEthPortInternal.h"
#include "../../../default/physical/ThaSerdesController.h"

#include "Tha60031032ModuleEthInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEthMethods  m_AtModuleEthOverride;
static tThaModuleEthMethods m_ThaModuleEthOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 MaxNumMpFlows(ThaModuleEth self)
    {
	AtUnused(self);
    return 2048;
    }

static uint8 MaxPortsGet(AtModuleEth self)
    {
	AtUnused(self);
    return 2;
    }

static AtSerdesController PortSerdesControllerCreate(ThaModuleEth self, AtEthPort ethPort)
    {
    uint32 portId = AtChannelIdGet((AtChannel)ethPort);
	AtUnused(self);
    if (AtEthPortSpeedGet(ethPort) == cAtEthPortSpeed10G)
        return ThaStmPwProductEthPort10GSerdesControllerNew(ethPort, portId);
    return ThaStmPwProductEthPortSerdesControllerNew(ethPort, portId);
    }

static eAtEthPortInterface PortDefaultInterface(AtModuleEth self, AtEthPort port)
    {
	AtUnused(self);
	AtUnused(port);
    return cAtEthPortInterface1000BaseX;
    }

static eBool PortSerdesPrbsIsSupported(ThaModuleEth self, AtEthPort ethPort)
    {
	AtUnused(ethPort);
	AtUnused(self);
    return cAtTrue;
    }

static ThaEthFlowHeaderProvider FlowHeaderProviderCreate(ThaModuleEth self)
    {
    return Tha60031032EthFlowHeaderProviderNew((AtModuleEth)self);
    }

static eBool UsePortSourceMacForAllFlows(ThaModuleEth self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static void OverrideAtModuleEth(Tha60031032ModuleEth self)
    {
    AtModuleEth ethModule = (AtModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEthOverride, mMethodsGet(ethModule), sizeof(m_AtModuleEthOverride));
        mMethodOverride(m_AtModuleEthOverride, MaxPortsGet);
        mMethodOverride(m_AtModuleEthOverride, PortDefaultInterface);
        }

    mMethodsSet(ethModule, &m_AtModuleEthOverride);
    }

static void OverrideThaModuleEth(Tha60031032ModuleEth self)
    {
    ThaModuleEth thaModule = (ThaModuleEth)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEthOverride, mMethodsGet(thaModule), sizeof(m_ThaModuleEthOverride));

        mMethodOverride(m_ThaModuleEthOverride, MaxNumMpFlows);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesControllerCreate);
        mMethodOverride(m_ThaModuleEthOverride, PortSerdesPrbsIsSupported);
        mMethodOverride(m_ThaModuleEthOverride, FlowHeaderProviderCreate);
        mMethodOverride(m_ThaModuleEthOverride, UsePortSourceMacForAllFlows);
        }

    mMethodsSet(thaModule, &m_ThaModuleEthOverride);
    }

static void Override(Tha60031032ModuleEth self)
    {
    OverrideAtModuleEth(self);
    OverrideThaModuleEth(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032ModuleEth);
    }

AtModuleEth Tha60031032ModuleEthObjectInit(AtModuleEth self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleEthStmPppObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override((Tha60031032ModuleEth)self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEth Tha60031032ModuleEthNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEth newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60031032ModuleEthObjectInit(newModule, device);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : ETH
 *
 * File        : Tha60031032EthFlowHeaderProvider.c
 *
 * Created Date: Nov 28, 2013
 *
 * Description : ETH Flow header provider
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60031032EthFlowHeaderProvider.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60031032EthFlowHeaderProvider)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60031032EthFlowHeaderProviderMethods m_methods;

/* Override */
static tThaEthFlowHeaderProviderMethods m_ThaEthFlowHeaderProviderOverride;

/* Save super implementation */
static const tThaEthFlowHeaderProviderMethods *m_ThaEthFlowHeaderProviderMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint8 *OamFlowPsnArrayTypes(ThaEthFlowHeaderProvider self, uint8 *numPsnType)
    {
    static uint8 arrayType = cThaArrPppCtrlPsn;
	AtUnused(self);
    if (numPsnType)
        *numPsnType = 1;

    return &arrayType;
    }

static AtDevice Device(ThaEthFlowHeaderProvider self)
    {
    AtModuleEth ethModule = ThaEthFlowHeaderProviderModuleGet(self);
    return AtModuleDeviceGet((AtModule)ethModule);
    }

static uint32 StartVersionWithNcpMlppp(Tha60031032EthFlowHeaderProvider self)
    {
	AtUnused(self);
    return cBit31_0;
    }

static eBool NcpMlpppIsSupported(ThaEthFlowHeaderProvider self)
    {
    return AtDeviceVersionNumber(Device(self)) >= mMethodsGet(mThis(self))->StartVersionWithNcpMlppp(mThis(self));
    }

static uint8 *MpFlowPsnArrayTypes(ThaEthFlowHeaderProvider self, uint8 *numPsnType)
    {
    static uint8 arrayTypes[2] = {cThaArrMlpppPktPsn, cThaArrMlpppCtrlPsn};

    if (!NcpMlpppIsSupported(self))
        return m_ThaEthFlowHeaderProviderMethods->MpFlowPsnArrayTypes(self, numPsnType);

    if (numPsnType)
        *numPsnType = mCount(arrayTypes);

    return arrayTypes;
    }

static uint8 PsnMode(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    return 0xB;
    }

static uint16 EthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    /* Default Ethernet header includes DA, SA and Ethernet type */
    return cNumByteOfDestMac + cNumByteOfSourceMac + cNumByteOfEthernetType;
    }

static uint16 OamEthHeaderNoVlanLengthGet(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    return cNumByteOfDestMac + cNumByteOfSourceMac;
    }

static uint32 VlanIdSw2Hw(ThaEthFlowHeaderProvider self, AtEthFlow flow, uint32 vlanId)
    {
    uint32 stmPortId = (vlanId & cBit11_9) >> 1;
    uint32 channelId = (vlanId & cBit7_0);
	AtUnused(flow);
	AtUnused(self);

    return stmPortId | channelId;
    }

static eBool OamFlowHasEthType(ThaEthFlowHeaderProvider self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static void OverrideThaEthFlowHeaderProvider(ThaEthFlowHeaderProvider self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaEthFlowHeaderProviderMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaEthFlowHeaderProviderOverride, m_ThaEthFlowHeaderProviderMethods, sizeof(m_ThaEthFlowHeaderProviderOverride));

        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, PsnMode);
        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, EthHeaderNoVlanLengthGet);
        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, OamEthHeaderNoVlanLengthGet);
        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, VlanIdSw2Hw);
        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, OamFlowPsnArrayTypes);
        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, OamFlowHasEthType);
        mMethodOverride(m_ThaEthFlowHeaderProviderOverride, MpFlowPsnArrayTypes);
        }

    mMethodsSet(self, &m_ThaEthFlowHeaderProviderOverride);
    }

static void Override(ThaEthFlowHeaderProvider self)
    {
    OverrideThaEthFlowHeaderProvider(self);
    }

static void MethodsInit(Tha60031032EthFlowHeaderProvider self)
    {
    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, StartVersionWithNcpMlppp);
        }

    mMethodsSet(self, &m_methods);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032EthFlowHeaderProvider);
    }

ThaEthFlowHeaderProvider Tha60031032EthFlowHeaderProviderObjectInit(ThaEthFlowHeaderProvider self, AtModuleEth ethModule)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthFlowHeaderProviderObjectInit(self, ethModule) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(mThis(self));
    m_methodsInit = 1;

    return self;
    }

ThaEthFlowHeaderProvider Tha60031032EthFlowHeaderProviderNew(AtModuleEth ethModule)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaEthFlowHeaderProvider newProvider = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newProvider == NULL)
        return NULL;

    /* Construct it */
    return Tha60031032EthFlowHeaderProviderObjectInit(newProvider, ethModule);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : SDH
 *
 * File        : Tha60031032ModuleSdh.c
 *
 * Created Date: Aug 19, 2013
 *
 * Description : SDH Module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60031032ModuleSdhInternal.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleSdhMethods              m_AtModuleSdhOverride;
static tThaModuleSdhMethods             m_ThaModuleSdhOverride;
static tThaStmPwProductModuleSdhMethods m_ThaStmPwProductModuleSdhOverride;

/* Save super implementation */
static const tAtModuleSdhMethods *m_AtModuleSdhMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032ModuleSdh);
    }

static eBool SsBitCompareIsEnabledAsDefault(AtModuleSdh self)
    {
	AtUnused(self);
    return cAtFalse;
    }

static uint32 StartVersionSupportsSerdesTimingMode(ThaStmPwProductModuleSdh self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(0x3, 0x1, 0x5);
    }

static uint32 StartVersionSupport24BitsBlockErrorCounter(ThaModuleSdh self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(3, 4, 0);
    }

static void OverrideThaModuleSdh(AtModuleSdh self)
    {
    ThaModuleSdh sdhModule = (ThaModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleSdhOverride, mMethodsGet(sdhModule), sizeof(m_ThaModuleSdhOverride));

        mMethodOverride(m_ThaModuleSdhOverride, StartVersionSupport24BitsBlockErrorCounter);
        }

    mMethodsSet(sdhModule, &m_ThaModuleSdhOverride);
    }

static void OverrideThaStmPwProductModuleSdh(AtModuleSdh self)
    {
    ThaStmPwProductModuleSdh sdhModule = (ThaStmPwProductModuleSdh)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPwProductModuleSdhOverride, mMethodsGet(sdhModule), sizeof(m_ThaStmPwProductModuleSdhOverride));

        mMethodOverride(m_ThaStmPwProductModuleSdhOverride, StartVersionSupportsSerdesTimingMode);
        }

    mMethodsSet(sdhModule, &m_ThaStmPwProductModuleSdhOverride);
    }

static void OverrideAtModuleSdh(AtModuleSdh self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleSdhMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleSdhOverride, m_AtModuleSdhMethods, sizeof(m_AtModuleSdhOverride));

        mMethodOverride(m_AtModuleSdhOverride, SsBitCompareIsEnabledAsDefault);
        }

    mMethodsSet(self, &m_AtModuleSdhOverride);
    }

static void Override(AtModuleSdh self)
    {
    OverrideAtModuleSdh(self);
    OverrideThaModuleSdh(self);
    OverrideThaStmPwProductModuleSdh(self);
    }

AtModuleSdh Tha60031032ModuleSdhObjectInit(AtModuleSdh self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductModuleSdhObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleSdh Tha60031032ModuleSdhNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleSdh newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60031032ModuleSdhObjectInit(newModule, device);
    }

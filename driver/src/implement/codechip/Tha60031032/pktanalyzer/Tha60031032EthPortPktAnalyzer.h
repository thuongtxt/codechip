/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha60031032EthPortPktAnalyzer.h
 * 
 * Created Date: Jul 1, 2014
 *
 * Description : Packet analyzer of product 60031032
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032ETHPORTPKTANALYZER_H_
#define _THA60031032ETHPORTPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pktanalyzer/ThaEthPortPktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032EthPortPktAnalyzer
    {
    tThaEthPortPktAnalyzerV2 super;
    }tTha60031032EthPortPktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPktAnalyzer Tha60031032EthPortPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031032ETHPORTPKTANALYZER_H_ */


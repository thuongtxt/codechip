/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha60031032EthPacketInternal.h
 * 
 * Created Date: Sep 26, 2014
 *
 * Description : ETH packet
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032ETHPACKETINTERNAL_H_
#define _THA60031032ETHPACKETINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/pktanalyzer/ThaStmPwProductEthPacket.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032EthPacket
    {
    tThaStmPwProductEthPacket super;
    }tTha60031032EthPacket;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtPacket Tha60031032EthPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031032ETHPACKETINTERNAL_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : Tha60031032EthPacket.c
 *
 * Created Date: Jun 27, 2013
 *
 * Description : ETH packet
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60031032EthPacketInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaStmPwProductEthPacketMethods m_ThaStmPwProductEthPacketOverride;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static const char* EncapTypeString(ThaStmPwProductEthPacket self, uint8 encapType)
    {
    static char encapTypeStr[8];
	AtUnused(self);

    if (encapType == 0x1)  return "IP";
    if (encapType == 0x3)  return "MPLS";
    if (encapType == 0x11) return "ISIS";
    if (encapType == 0x10) return "PPP OAM";

    AtSprintf(encapTypeStr, "0x%02x", encapType);

    return encapTypeStr;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032EthPacket);
    }

static void OverrideThaStmPwProductEthPacket(AtPacket self)
    {
    ThaStmPwProductEthPacket packet = (ThaStmPwProductEthPacket)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPwProductEthPacketOverride, mMethodsGet(packet), sizeof(m_ThaStmPwProductEthPacketOverride));

        mMethodOverride(m_ThaStmPwProductEthPacketOverride, EncapTypeString);
        }

    mMethodsSet(packet, &m_ThaStmPwProductEthPacketOverride);
    }

static void Override(AtPacket self)
    {
    OverrideThaStmPwProductEthPacket(self);
    }

AtPacket Tha60031032EthPacketObjectInit(AtPacket self, uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductEthPacketObjectInit(self, dataBuffer, length, cacheMode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPacket Tha60031032EthPacketNew(uint8 *dataBuffer, uint32 length, eAtPacketCacheMode cacheMode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtPacket newPacket = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPacket == NULL)
        return NULL;

    /* Construct it */
    return Tha60031032EthPacketObjectInit(newPacket, dataBuffer, length, cacheMode);
    }

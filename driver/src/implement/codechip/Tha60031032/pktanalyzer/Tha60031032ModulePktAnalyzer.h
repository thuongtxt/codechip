/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Packet analyzer
 * 
 * File        : Tha60031032ModulePktAnalyzer.h
 * 
 * Created Date: Jul 1, 2014
 *
 * Description : Packet analyzer module of product 60031031
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032MODULEPKTANALYZER_H_
#define _THA60031032MODULEPKTANALYZER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/pktanalyzer/ThaModulePktAnalyzerInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032ModulePktAnalyzer
    {
    tThaModulePktAnalyzerPw super;
    }tTha60031032ModulePktAnalyzer;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePktAnalyzer Tha60031032ModulePktAnalyzerObjectInit(AtModulePktAnalyzer self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031032MODULEPKTANALYZER_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Packet analyzer
 *
 * File        : Tha60031032EthPortPktAnalyzer.c
 *
 * Created Date: Jun 27, 2013
 *
 * Description : Packet analyzer of product 60031031
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60031032EthPortPktAnalyzer.h"
#include "../../../default/cla/ThaModuleCla.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtPktAnalyzerMethods m_AtPktAnalyzerOverride;

/* Save super implementation */
static const tAtPktAnalyzerMethods *m_AtPktAnalyzerMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032EthPortPktAnalyzer);
    }

static AtPacket PacketCreate(AtPktAnalyzer self, uint8 *data, uint32 length, eAtPktAnalyzerDirection direction)
    {
	AtUnused(direction);
	AtUnused(self);
    /* In this device type, packet always contains 2 VLAN */
    return Tha60031032EthPacketNew(data, length, cAtPacketCacheModeCacheData);
    }

static void OverrideAtPktAnalyzer(AtPktAnalyzer self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtPktAnalyzerMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtPktAnalyzerOverride, mMethodsGet(self), sizeof(m_AtPktAnalyzerOverride));

        mMethodOverride(m_AtPktAnalyzerOverride, PacketCreate);
        }

    mMethodsSet(self, &m_AtPktAnalyzerOverride);
    }

static void Override(AtPktAnalyzer self)
    {
    OverrideAtPktAnalyzer(self);
    }

AtPktAnalyzer Tha60031032EthPortPktAnalyzerObjectInit(AtPktAnalyzer self, AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaEthPortPktAnalyzerObjectInit(self, port) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtPktAnalyzer Tha60031032EthPortPktAnalyzerNew(AtEthPort port)
    {
    AtOsal osal = AtSharedDriverOsalGet();
    AtPktAnalyzer newPktAnalyzer = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newPktAnalyzer == NULL)
        return NULL;

    /* Construct it */
    return Tha60031032EthPortPktAnalyzerObjectInit(newPktAnalyzer, port);
    }

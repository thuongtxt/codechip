/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60031032CommonDeviceInternal.h
 * 
 * Created Date: Sep 26, 2013
 *
 * Description : Product 60031032
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032COMMONDEVICEINTERNAL_H_
#define _THA60031032COMMONDEVICEINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../ThaStmMlppp/man/ThaStmMlpppDeviceInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif
typedef struct tTha60031032CommonDevice * Tha60031032CommonDevice;

typedef struct tTha60031032CommonDeviceMethods
    {
    uint8 dummy;
    }tTha60031032CommonDeviceMethods;

typedef struct tTha60031032CommonDevice
    {
    tThaStmMlpppDevice super;
    const tTha60031032CommonDeviceMethods *methods;

    /* Private */
    }tTha60031032CommonDevice;

typedef struct tTha60031032Device
    {
    tTha60031032CommonDevice super;
    }tTha60031032Device;

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtDevice Tha60031032CommonDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);
AtDevice Tha60031032DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031032COMMONDEVICEINTERNAL_H_ */


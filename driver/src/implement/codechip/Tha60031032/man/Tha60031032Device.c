/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60031032Device.c
 *
 * Created Date: Mar 14, 2015
 *
 * Description : Product 60031032
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/ber/ThaModuleBer.h"
#include "Tha60031032CommonDeviceInternal.h"
#include "../../Tha60031031/man/Tha60031031Device.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;

/* Super implementation */
static const tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    if (moduleId  == cAtModuleBer) return (AtModule)ThaStmModuleBerNew(self, 800);
    if (moduleId  == cAtModulePdh) return (AtModule)Tha60031032ModulePdhNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static eBool ModuleIsSupported(AtDevice self, eAtModule moduleId)
    {
    if (moduleId == cAtModuleBer)
        return cAtTrue;
    return m_AtDeviceMethods->ModuleIsSupported(self, moduleId);
    }

static const eAtModule *AllSupportedModulesGet(AtDevice self, uint8 *numModules)
    {
    static const eAtModule supportedModules[] = {cAtModulePdh,
                                                 cAtModulePpp,
                                                 cAtModuleEncap,
                                                 cAtModuleEth,
                                                 cAtModuleSdh,
                                                 cAtModuleRam,
                                                 cAtModuleBer,
                                                 cAtModuleClock,
                                                 cAtModulePktAnalyzer};
    AtUnused(self);
    if (numModules)
        *numModules = mCount(supportedModules);

    return supportedModules;
    }

static void OverrideAtDevice(AtDevice self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, mMethodsGet(self), sizeof(m_AtDeviceOverride));
        mMethodOverride(m_AtDeviceOverride, AllSupportedModulesGet);
        mMethodOverride(m_AtDeviceOverride, ModuleIsSupported);
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        }

    mMethodsSet(self, &m_AtDeviceOverride);
    }

static void Override(AtDevice self)
    {
    OverrideAtDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032Device);
    }

AtDevice Tha60031032DeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Initialize its super */
    if (Tha60031032CommonDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60031032DeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Constructor */
    return Tha60031032DeviceObjectInit(newDevice, driver, productCode);
    }

/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Device management
 *
 * File        : Tha60031032Device.c
 *
 * Created Date: Aug 13, 2013
 *
 * Description : Product 60031032
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60031032CommonDeviceInternal.h"
#include "../../default/ThaStmPwProduct/man/ThaStmPwProductDevice.h"
#include "../../../default/man/ThaDeviceReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtDeviceMethods  m_AtDeviceOverride;
static tThaDeviceMethods m_ThaDeviceOverride;

/* Super implementation */
static tAtDeviceMethods *m_AtDeviceMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static AtModule ModuleCreate(AtDevice self, eAtModule moduleId)
    {
    eThaPhyModule phyModule = moduleId;

    if (moduleId  == cAtModuleEncap)       return (AtModule)Tha60031032ModuleEncapNew(self);
    if (moduleId  == cAtModulePpp)         return (AtModule)Tha60031032ModulePppNew(self);
    if (moduleId  == cAtModuleEth)         return (AtModule)Tha60031032ModuleEthNew(self);
    if (moduleId  == cAtModulePdh)         return (AtModule)Tha60031032CommonModulePdhNew(self);
    if (moduleId  == cAtModuleRam)         return (AtModule)ThaStmPwProductModuleRamNew(self);
    if (moduleId  == cAtModuleSdh)         return (AtModule)Tha60031032ModuleSdhNew(self);
    if (moduleId  == cAtModuleClock)       return (AtModule)Tha60031032ModuleClockNew(self);
    if (moduleId  == cAtModulePktAnalyzer) return (AtModule)Tha60031032ModulePktAnalyzerNew(self);

    if (phyModule == cThaModuleCdr)        return ThaStmPwProductModuleCdrNew(self);
    if (phyModule == cThaModuleCla)        return Tha60031032ModuleClaPppNew(self);

    return m_AtDeviceMethods->ModuleCreate(self, moduleId);
    }

static uint8 NumPartsOfModule(ThaDevice self, uint32 moduleId)
    {
	AtUnused(moduleId);
	AtUnused(self);
    /* Every module has two parts */
    return 2;
    }

static uint8 MaxNumParts(ThaDevice self)
    {
	AtUnused(self);
    return 2;
    }

static AtSSKeyChecker SSKeyCheckerCreate(AtDevice self)
    {
    return Tha60031032SSKeyCheckerNew(self);
    }

static uint32 PllClkSysStatReg(ThaDevice self)
    {
    return ThaPwCommonProductPllClkSysStatReg(self);
    }

static uint32 PllClkSysStatBitMask(ThaDevice self)
    {
    return ThaPwCommonProductPllClkSysStatBitMask(self);
    }

static eBool CanHaveNewOcn(ThaDevice self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 StartVersionWithNewOcn(ThaDevice self)
    {
    static const uint8 cHardDDR = 3;
    static const uint8 cSoftDDR = 2;

    if (ThaDeviceChipVersionMajorGet(self) == cHardDDR) return ThaVersionReaderVersionBuild(3, 0, 3);
    if (ThaDeviceChipVersionMajorGet(self) == cSoftDDR) return ThaVersionReaderVersionBuild(2, 0, 4);

    /* So far, all of latest release use hard DDR */
    return ThaVersionReaderVersionBuild(3, 0, 3);
    }

static void OverrideAtDevice(Tha60031032CommonDevice self)
    {
    AtDevice device = (AtDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtDeviceMethods = mMethodsGet(device);
        mMethodsGet(osal)->MemCpy(osal, &m_AtDeviceOverride, mMethodsGet(device), sizeof(m_AtDeviceOverride));
        mMethodOverride(m_AtDeviceOverride, ModuleCreate);
        mMethodOverride(m_AtDeviceOverride, SSKeyCheckerCreate);
        }

    mMethodsSet(device, &m_AtDeviceOverride);
    }

static void OverrideThaDevice(Tha60031032CommonDevice self)
    {
    ThaDevice device = (ThaDevice)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaDeviceOverride, mMethodsGet(device), sizeof(m_ThaDeviceOverride));

        mMethodOverride(m_ThaDeviceOverride, NumPartsOfModule);
        mMethodOverride(m_ThaDeviceOverride, MaxNumParts);
        mMethodOverride(m_ThaDeviceOverride, PllClkSysStatReg);
        mMethodOverride(m_ThaDeviceOverride, PllClkSysStatBitMask);
        mMethodOverride(m_ThaDeviceOverride, CanHaveNewOcn);
        mMethodOverride(m_ThaDeviceOverride, StartVersionWithNewOcn);
        }

    mMethodsSet(device, &m_ThaDeviceOverride);
    }

static void Override(Tha60031032CommonDevice self)
    {
    OverrideThaDevice(self);
    OverrideAtDevice(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032CommonDevice);
    }

AtDevice Tha60031032CommonDeviceObjectInit(AtDevice self, AtDriver driver, uint32 productCode)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Initialize its super */
    if (ThaStmMlpppDeviceObjectInit(self, driver, productCode) == NULL)
        return NULL;

    /* Setup class */
    Override((Tha60031032CommonDevice)self);
    m_methodsInit = 1;

    return self;
    }

AtDevice Tha60031032CommonDeviceNew(AtDriver driver, uint32 productCode)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtDevice newDevice = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newDevice == NULL)
        return NULL;

    /* Constructor */
    return Tha60031032CommonDeviceObjectInit(newDevice, driver, productCode);
    }

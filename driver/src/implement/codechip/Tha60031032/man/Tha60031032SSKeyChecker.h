/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Device management
 * 
 * File        : Tha60031032SSKeyChecker.h
 * 
 * Created Date: Mar 6, 2015
 *
 * Description : SSKey checker
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032SSKEYCHECKER_H_
#define _THA60031032SSKEYCHECKER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/man/ThaSSKeyCheckerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032SSKeyChecker
    {
    tThaSSKeyCheckerPpp super;
    }tTha60031032SSKeyChecker;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtSSKeyChecker Tha60031032SSKeyCheckerObjectInit(AtSSKeyChecker self, AtDevice device);

#endif /* _THA60031032SSKEYCHECKER_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : PDH
 * 
 * File        : Tha60031032ModulePdhInternal.h
 * 
 * Created Date: Mar 16, 2015
 *
 * Description : PDH Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032MODULEPDHINTERNAL_H_
#define _THA60031032MODULEPDHINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60031031/pdh/Tha60031031ModulePdh.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032CommonModulePdh
    {
    tThaStmPwProductModulePdh super;
    }tTha60031032CommonModulePdh;

typedef struct tTha60031032ModulePdh
    {
    tTha60031032CommonModulePdh super;
    }tTha60031032ModulePdh;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModulePdh Tha60031032ModulePdhObjectInit(AtModulePdh self, AtDevice device);
AtModulePdh Tha60031032CommonModulePdhObjectInit(AtModulePdh self, AtDevice device);

#endif /* _THA60031032MODULEPDHINTERNAL_H_ */


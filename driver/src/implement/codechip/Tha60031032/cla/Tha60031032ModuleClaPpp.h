/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60031032ModuleClaPpp.h
 * 
 * Created Date: Jan 13, 2015
 *
 * Description : CLA module of product 60031032
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032MODULECLAPPP_H_
#define _THA60031032MODULECLAPPP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cla/ppp/ThaModuleClaPppInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032ModuleClaPpp
    {
    tThaModuleClaPpp super;
    }tTha60031032ModuleClaPpp;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60031032ModuleClaPppObjectInit(AtModule self, AtDevice device);

#endif /* _THA60031032MODULECLAPPP_H_ */


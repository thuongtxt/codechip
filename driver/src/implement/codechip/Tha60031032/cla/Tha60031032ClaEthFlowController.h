/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2015 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : CLA
 * 
 * File        : Tha60031032ClaEthFlowController.h
 * 
 * Created Date: Jan 13, 2015
 *
 * Description : Cla Eth Flow Controller
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032CLAETHFLOWCONTROLLER_H_
#define _THA60031032CLAETHFLOWCONTROLLER_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/cla/controllers/ThaClaEthFlowControllerInternal.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032ClaEthFlowController
    {
    tThaClaEthFlowController super;

    /* Private data */
    }tTha60031032ClaEthFlowController;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
ThaClaEthFlowController Tha60031032ClaEthFlowControllerObjectInit(ThaClaEthFlowController self, ThaModuleCla cla);

#endif /* _THA60031032CLAETHFLOWCONTROLLER_H_ */


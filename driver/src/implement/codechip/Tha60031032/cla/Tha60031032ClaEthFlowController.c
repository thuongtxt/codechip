/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Module Cla
 *
 * File        : Tha60031032ClaEthFlowController.c
 *
 * Created Date: Jan 21, 2014
 *
 * Description : Cla Eth Flow Controller
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60031032ClaEthFlowController.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaClaEthFlowControllerMethods  m_ThaClaEthFlowControllerOverride;

/*--------------------------- Forward declarations ---------------------------*/
uint8 Tha6003PppProductQueueHwPriority(ThaClaEthFlowController self, uint8 queueId);

/*--------------------------- Implementation ---------------------------------*/
static uint16 VlanLookupOffset(ThaClaEthFlowController self, uint16 lkType)
    {
	AtUnused(self);
	AtUnused(self);
    return (lkType == 0) ? 0x800 : 0x0;
    }

static uint8 QueueHwPriority(ThaClaEthFlowController self, uint8 queueId)
    {
    return Tha6003PppProductQueueHwPriority(self, queueId);
    }

static eBool NeedInitializeQueuePriority(ThaClaEthFlowController self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032ClaEthFlowController);
    }

static void OverrideThaClaEthFlowController(ThaClaEthFlowController self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaClaEthFlowControllerOverride, mMethodsGet(self), sizeof(m_ThaClaEthFlowControllerOverride));
        mMethodOverride(m_ThaClaEthFlowControllerOverride, VlanLookupOffset);
        mMethodOverride(m_ThaClaEthFlowControllerOverride, QueueHwPriority);
        mMethodOverride(m_ThaClaEthFlowControllerOverride, NeedInitializeQueuePriority);
        }

    mMethodsSet(self, &m_ThaClaEthFlowControllerOverride);
    }

static void Override(ThaClaEthFlowController self)
    {
    OverrideThaClaEthFlowController(self);
    }

ThaClaEthFlowController Tha60031032ClaEthFlowControllerObjectInit(ThaClaEthFlowController self, ThaModuleCla cla)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaClaEthFlowControllerObjectInit(self, cla) == NULL)
        return NULL;

    /* Setup */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

ThaClaEthFlowController Tha60031032ClaEthFlowControllerNew(ThaModuleCla cla)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    ThaClaEthFlowController newController = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newController == NULL)
        return NULL;

    /* Construct it */
    return Tha60031032ClaEthFlowControllerObjectInit(newController, cla);
    }

uint8 Tha6003PppProductQueueHwPriority(ThaClaEthFlowController self, uint8 queueId)
    {
	AtUnused(self);
    if (queueId == 0) return 0x7;
    if (queueId == 1) return 0x6;
    if (queueId == 2) return 0x5;
    if (queueId == 3) return 0x4;
    if (queueId == 4) return 0x3;
    if (queueId == 5) return 0x2;
    if (queueId == 6) return 0x1;

    /* queue#7 */
    return 0x0;
    }

/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : Clock
 * 
 * File        : Tha60031032ModuleClockInternal.h
 * 
 * Created Date: Sep 3, 2014
 *
 * Description : Clock Module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032MODULECLOCK_H_
#define _THA60031032MODULECLOCK_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../default/ThaStmPwProduct/clock/ThaStmPwProductModuleClock.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032ModuleClock
    {
    tThaStmPwProductModuleClock super;
    }tTha60031032ModuleClock;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleClock Tha60031032ModuleClockObjectInit(AtModuleClock self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031032MODULECLOCK_H_ */


/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Clock
 *
 * File        : Tha60031032ClockExtractor.c
 *
 * Created Date: Sep 3, 2014
 *
 * Description : Clock Extractor
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../default/ThaStmPwProduct/clock/ThaStmPwProductClockExtractor.h"
#include "../../default/ThaStmPwProduct/cdr/ThaStmPwProductModuleCdrReg.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60031032ClockExtractor *)((void*)self))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031032ClockExtractor
    {
    tThaStmPwProductClockExtractor super;

    /* Private data */
    uint8 hwExtractMode;
    }tTha60031032ClockExtractor;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtClockExtractorMethods              m_AtClockExtractorOverride;
static tThaStmPwProductClockExtractorMethods m_ThaStmPwProductClockExtractorOverride;

/* Save super implement */
static const tThaStmPwProductClockExtractorMethods *m_Tha60031031ClockExtractorMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032ClockExtractor);
    }

static eAtModuleClockRet Enable(AtClockExtractor self, eBool enable)
    {
    uint32 refOutValue;
    ThaClockExtractor clockExtractor = (ThaClockExtractor)self;
    uint32 address = cThaRegCDRReferenceSync8KOutputControl + ThaClockExtractorDefaultOffset(clockExtractor);
    uint32 regVal  = AtClockExtractorRead(self, address);

    refOutValue = enable ? mThis(self)->hwExtractMode : cThaRefOutTimeModeUnused;
    mRegFieldSet(regVal, cThaRefOutTimeMode, refOutValue);
    AtClockExtractorWrite(self, address, regVal);

    return cAtOk;
    }

static eBool IsEnabled(AtClockExtractor self)
    {
    ThaClockExtractor clockExtractor = (ThaClockExtractor)self;
    uint32 address = cThaRegCDRReferenceSync8KOutputControl + ThaClockExtractorDefaultOffset(clockExtractor);
    return ((AtClockExtractorRead(self, address) & cThaRefOutTimeModeMask) == cThaRefOutTimeModeUnused) ? cAtFalse : cAtTrue;
    }

static void HwTimingModeDidChange(ThaStmPwProductClockExtractor self, uint8 newMode)
    {
    m_Tha60031031ClockExtractorMethods->HwTimingModeDidChange(self, newMode);
    mThis(self)->hwExtractMode = newMode;
    }

static void OverrideAtClockExtractor(AtClockExtractor self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_AtClockExtractorOverride, mMethodsGet(self), sizeof(m_AtClockExtractorOverride));

        mMethodOverride(m_AtClockExtractorOverride, Enable);
        mMethodOverride(m_AtClockExtractorOverride, IsEnabled);
        }

    mMethodsSet(self, &m_AtClockExtractorOverride);
    }

static void OverrideThaStmPwProductClockExtractor(AtClockExtractor self)
    {
    ThaStmPwProductClockExtractor extractor = (ThaStmPwProductClockExtractor)self;
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60031031ClockExtractorMethods = mMethodsGet(extractor);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaStmPwProductClockExtractorOverride, m_Tha60031031ClockExtractorMethods, sizeof(m_ThaStmPwProductClockExtractorOverride));

        mMethodOverride(m_ThaStmPwProductClockExtractorOverride, HwTimingModeDidChange);
        }

    mMethodsSet(extractor, &m_ThaStmPwProductClockExtractorOverride);
    }

static void Override(AtClockExtractor self)
    {
    OverrideAtClockExtractor(self);
    OverrideThaStmPwProductClockExtractor(self);
    }

static AtClockExtractor ObjectInit(AtClockExtractor self, AtModuleClock clockModule, uint8 extractorId)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaStmPwProductClockExtractorObjectInit(self, clockModule, extractorId) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;
    mThis(self)->hwExtractMode = cThaRefOutTimeModeUnused;

    return self;
    }

AtClockExtractor Tha60031032ClockExtractorNew(AtModuleClock clockModule, uint8 extractorId)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtClockExtractor newExtractor = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newExtractor == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newExtractor, clockModule, extractorId);
    }

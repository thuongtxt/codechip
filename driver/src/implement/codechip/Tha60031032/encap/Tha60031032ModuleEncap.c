/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2012 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60031032ModuleEncap.c
 *
 * Created Date: Aug 14, 2013
 *
 * Description : Encapsulation module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "Tha60031032ModuleEncap.h"
#include "AtHdlcChannel.h"
#include "../../../default/man/versionreader/ThaVersionReader.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtModuleEncapMethods  m_AtModuleEncapOverride;
static tThaModuleEncapMethods m_ThaModuleEncapOverride;

/* Save super implementation */
static const tAtModuleEncapMethods *m_AtModuleEncapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/



/*--------------------------- Implementation ---------------------------------*/
static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032ModuleEncap);
    }

static uint16 MaxChannelsGet(AtModuleEncap self)
    {
	AtUnused(self);
    return 1024;
    }

static AtHdlcChannel HdlcPppChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    if (ThaModuleEncapAcfcSupported((ThaModuleEncap)self))
        return Tha60031032HdlcChannelNew(channelId, cAtHdlcFrmPpp, self);
    return m_AtModuleEncapMethods->HdlcPppChannelObjectCreate(self, channelId);
    }

static AtPppLink PppLinkObjectCreate(AtModuleEncap self, AtHdlcChannel hdlcChannel)
    {
	AtUnused(self);
    return Tha60031032PppLinkNew(hdlcChannel);
    }

static AtHdlcChannel CiscoHdlcChannelObjectCreate(AtModuleEncap self, uint32 channelId)
    {
    if (ThaModuleEncapAcfcSupported((ThaModuleEncap)self))
        return Tha60031032HdlcChannelNew(channelId, cAtHdlcFrmCiscoHdlc, self);
    return m_AtModuleEncapMethods->CiscoHdlcChannelObjectCreate(self, channelId);
    }

static eBool HdlcByteCountersAreSupported(ThaModuleEncap self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static eBool AcfcSupported(ThaModuleEncap self)
    {
	AtUnused(self);
    return cAtTrue;
    }

static uint32 StartDeviceVersionThatSupportsFrameRelay(ThaModuleEncap self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(3, 1, 0);
    }

static uint32 StartDeviceVersionThatSupportsIdleByteCounter(ThaModuleEncap self)
    {
	AtUnused(self);
    return ThaVersionReaderVersionBuild(3, 4, 0);
    }

static void OverrideThaModuleEncap(AtModuleEncap self)
    {
    ThaModuleEncap encap = (ThaModuleEncap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleEncapOverride, mMethodsGet(encap), sizeof(m_ThaModuleEncapOverride));

        mMethodOverride(m_ThaModuleEncapOverride, HdlcByteCountersAreSupported);
        mMethodOverride(m_ThaModuleEncapOverride, AcfcSupported);
        mMethodOverride(m_ThaModuleEncapOverride, StartDeviceVersionThatSupportsFrameRelay);
        mMethodOverride(m_ThaModuleEncapOverride, StartDeviceVersionThatSupportsIdleByteCounter);
        }

    mMethodsSet(encap, &m_ThaModuleEncapOverride);
    }

static void OverrideAtModuleEncap(AtModuleEncap self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleEncapMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleEncapOverride, m_AtModuleEncapMethods, sizeof(m_AtModuleEncapOverride));

        mMethodOverride(m_AtModuleEncapOverride, MaxChannelsGet);
        mMethodOverride(m_AtModuleEncapOverride, PppLinkObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, HdlcPppChannelObjectCreate);
        mMethodOverride(m_AtModuleEncapOverride, CiscoHdlcChannelObjectCreate);
        }

    mMethodsSet(self, &m_AtModuleEncapOverride);
    }

static void Override(AtModuleEncap self)
    {
    OverrideAtModuleEncap(self);
    OverrideThaModuleEncap(self);
    }

AtModuleEncap Tha60031032ModuleEncapObjectInit(AtModuleEncap self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaModuleStmEncapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleEncap Tha60031032ModuleEncapNew(AtDevice device)
    {
    /* Allocate memory for this module */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleEncap newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Constructor */
    return Tha60031032ModuleEncapObjectInit(newModule, device);
    }

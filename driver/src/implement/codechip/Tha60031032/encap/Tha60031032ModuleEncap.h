/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2013 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Tecnologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : ENCAP
 * 
 * File        : Tha60031032ModuleEncap.h
 * 
 * Created Date: Dec 2, 2013
 *
 * Description : Encapsulation module
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60031032MODULEENCAP_H_
#define _THA60031032MODULEENCAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/encap/ThaStmModuleEncapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60031032ModuleEncap
    {
    tThaModuleStmEncap super;
    }tTha60031032ModuleEncap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleEncap Tha60031032ModuleEncapObjectInit(AtModuleEncap self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60031032MODULEENCAP_H_ */


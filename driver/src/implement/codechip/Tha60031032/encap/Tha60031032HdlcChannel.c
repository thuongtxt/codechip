/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2014 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : Encap
 *
 * File        : Tha60031032HdlcChannel.c
 *
 * Created Date: Mar 4, 2014
 *
 * Description : HDLC link
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../default/encap/ThaHdlcChannelInternal.h"
#include "../../../default/man/ThaDeviceInternal.h"
#include "../../../default/ppp/ThaMpegReg.h"
#include "../../../default/ppp/ThaPppLink.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define EgressCompressControl(self) (cThaRegEgressCompressControl + ThaPppLinkOffset(PppLink((ThaHdlcChannel)self)))

/*--------------------------- Local typedefs ---------------------------------*/
typedef struct tTha60031032HdlcChannel
    {
    tThaHdlcChannel super;
    }tTha60031032HdlcChannel;

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtHdlcChannelMethods  m_AtHdlcChannelOverride;
static tThaHdlcChannelMethods m_ThaHdlcChannelOverride;

/* Save super implementation */
static const tAtHdlcChannelMethods  *m_AtHdlcChannelMethods  = NULL;
static const tThaHdlcChannelMethods *m_ThaHdlcChannelMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static ThaPppLink PppLink(ThaHdlcChannel self)
    {
    return (ThaPppLink)AtHdlcChannelHdlcLinkGet((AtHdlcChannel)self);
    }

static eAtRet AddressControlInsertionEnable(ThaHdlcChannel self, eBool insertEnable)
    {
    eAtHdlcFrameType frameType = AtHdlcChannelFrameTypeGet((AtHdlcChannel)self);
    uint32 regAddr = EgressCompressControl(self);
    uint32 regVal  = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    uint8 insert;

    /* Do not use super's logic, this product move this hardware logic to new
     * field of new register */
    m_ThaHdlcChannelMethods->AddressControlInsertionEnable(self, cAtFalse);

    /* In case of cHDLC and Frame relay, the following field must be always be 0 */
    insert = insertEnable ? 1 : 0;
    if ((frameType == cAtHdlcFrmCiscoHdlc) || (frameType == cAtHdlcFrmFr))
        insert = 0;
    mRegFieldSet(regVal, cThaRegMlpppMPEGHdlcAddCtrlInsertEn, insert);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpeg);

    return cAtOk;
    }

static eBool AddressControlInsertionIsEnabled(ThaHdlcChannel self)
    {
    eAtHdlcFrameType frameType = AtHdlcChannelFrameTypeGet((AtHdlcChannel)self);
    uint32 regAddr, regVal;

    if ((frameType == cAtHdlcFrmCiscoHdlc) || (frameType == cAtHdlcFrmFr))
        return cAtTrue;

    regAddr = EgressCompressControl(self);
    regVal  = mChannelHwRead(self, regAddr, cThaModuleMpeg);

    return (regVal & cThaRegMlpppMPEGHdlcAddCtrlInsertEnMask) ? cAtTrue : cAtFalse;
    }

static eAtRet TxAddressControlCompress(AtHdlcChannel self, eBool compress)
    {
    uint32 regAddr, regVal;

    if (AtHdlcChannelFrameTypeGet(self) == cAtHdlcFrmCiscoHdlc)
        return cAtErrorModeNotSupport;

    regAddr = EgressCompressControl((ThaHdlcChannel)self);
    regVal  = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    mRegFieldSet(regVal, cThaRegMlpppMPEGHdlcAcfcEn, compress ? 1 : 0);
    mChannelHwWrite(self, regAddr, regVal, cThaModuleMpeg);

    return cAtOk;
    }

static eBool TxAddressControlIsCompressed(AtHdlcChannel self)
    {
    uint32 regAddr, regVal;

    if (AtHdlcChannelFrameTypeGet(self) == cAtHdlcFrmCiscoHdlc)
        return cAtFalse;

    regAddr = EgressCompressControl((ThaHdlcChannel)self);
    regVal  = mChannelHwRead(self, regAddr, cThaModuleMpeg);
    return (regVal & cThaRegMlpppMPEGHdlcAcfcEnMask) ? cAtTrue : cAtFalse;
    }

static void OverrideAtHdlcChannel(AtHdlcChannel self)
    {
    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtHdlcChannelMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtHdlcChannelOverride, m_AtHdlcChannelMethods, sizeof(m_AtHdlcChannelOverride));

        mMethodOverride(m_AtHdlcChannelOverride, TxAddressControlCompress);
        mMethodOverride(m_AtHdlcChannelOverride, TxAddressControlIsCompressed);
        }

    mMethodsSet(self, &m_AtHdlcChannelOverride);
    }

static void OverrideThaHdlcChannel(AtHdlcChannel self)
    {
    ThaHdlcChannel channel = (ThaHdlcChannel)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaHdlcChannelMethods = mMethodsGet(channel);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaHdlcChannelOverride, mMethodsGet(channel), sizeof(m_ThaHdlcChannelOverride));

        mMethodOverride(m_ThaHdlcChannelOverride, AddressControlInsertionEnable);
        mMethodOverride(m_ThaHdlcChannelOverride, AddressControlInsertionIsEnabled);
        }

    mMethodsSet(channel, &m_ThaHdlcChannelOverride);
    }

static void Override(AtHdlcChannel self)
    {
    OverrideAtHdlcChannel(self);
    OverrideThaHdlcChannel(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60031032HdlcChannel);
    }

static AtHdlcChannel ObjectInit(AtHdlcChannel self, uint32 channelId, eAtHdlcFrameType frameType, AtModuleEncap module)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (ThaHdlcChannelObjectInit(self, channelId, frameType, module) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtHdlcChannel Tha60031032HdlcChannelNew(uint32 channelId, eAtHdlcFrameType frameType, AtModuleEncap module)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtHdlcChannel newChannel = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newChannel == NULL)
        return NULL;

    /* Construct it */
    return ObjectInit(newChannel, channelId, frameType, module);
    }

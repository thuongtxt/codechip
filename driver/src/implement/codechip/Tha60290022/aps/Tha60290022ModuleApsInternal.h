/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2019 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : APS
 * 
 * File        : Tha60290022ModuleApsInternal.h
 * 
 * Created Date: Sep 16, 2019
 *
 * Description : 60290022 Module APS representation
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEAPSINTERNAL_H_
#define _THA60290022MODULEAPSINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60290021/aps/Tha6029ModuleApsInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModuleAps
    {
    tTha6029ModuleAps super;
    }tTha60290022ModuleAps;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModuleAps Tha60290022ModuleApsObjectInit(AtModuleAps self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEAPSINTERNAL_H_ */


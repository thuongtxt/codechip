/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : APS
 *
 * File        : Tha60290022ModuleAps.c
 *
 * Created Date: Jun 1, 2017
 *
 * Description : APS module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../man/Tha60290022Device.h"
#include "Tha60290022ModuleApsInternal.h"

/*--------------------------- Define -----------------------------------------*/
/* WTR resolution */
#define cWtrResolution3Seconds   0
#define cWtrResolution30Seconds  1

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tThaModuleHardApsMethods m_ThaModuleHardApsOverride;

static const tThaModuleHardApsMethods *m_ThaModuleHardApsMethods = NULL;
/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static eBool InterruptIsSupported(ThaModuleHardAps self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 NewWtrTimer2Resolution(ThaModuleHardAps self, uint32 timerValue, uint32 timerMax)
    {
    AtUnused(self);
    if (timerValue <= (timerMax * 3))
        return cWtrResolution3Seconds;

    if (timerValue <= (timerMax * 30))
        return cWtrResolution30Seconds;

    return cInvalidUint32;
    }

static uint32 NewWtrResolution2Value(ThaModuleHardAps self, uint32 resolution)
    {
    AtUnused(self);
    if (resolution == cWtrResolution30Seconds)
        return 30;

    if (resolution == cWtrResolution3Seconds)
        return 3;

    return 0;
    }

static uint32 WtrTimer2Resolution(ThaModuleHardAps self, uint32 timerValue, uint32 timerMax)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (Tha60290022DeviceHasApsWtr3SecondResolution(device))
        return NewWtrTimer2Resolution(self, timerValue, timerMax);
    return m_ThaModuleHardApsMethods->WtrTimer2Resolution(self, timerValue, timerMax);
    }

static uint32 WtrResolution2Value(ThaModuleHardAps self, uint32 resolution)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    if (Tha60290022DeviceHasApsWtr3SecondResolution(device))
        return NewWtrResolution2Value(self, resolution);
    return m_ThaModuleHardApsMethods->WtrResolution2Value(self, resolution);
    }

static void OverrideThaModuleHardAps(AtModuleAps self)
    {
    ThaModuleHardAps module = (ThaModuleHardAps)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleHardApsMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleHardApsOverride, m_ThaModuleHardApsMethods, sizeof(m_ThaModuleHardApsOverride));

        mMethodOverride(m_ThaModuleHardApsOverride, InterruptIsSupported);
        mMethodOverride(m_ThaModuleHardApsOverride, WtrTimer2Resolution);
        mMethodOverride(m_ThaModuleHardApsOverride, WtrResolution2Value);
        }

    mMethodsSet(module, &m_ThaModuleHardApsOverride);
    }

static void Override(AtModuleAps self)
    {
    OverrideThaModuleHardAps(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleAps);
    }

AtModuleAps Tha60290022ModuleApsObjectInit(AtModuleAps self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha6029ModuleApsObjectInit(self, device) == NULL)
        return NULL;

    /* Setup Class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModuleAps Tha60290022ModuleApsNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModuleAps object = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (object == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModuleApsObjectInit(object, device);
    }

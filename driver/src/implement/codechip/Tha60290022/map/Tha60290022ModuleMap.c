/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60290022ModuleMap.c
 *
 * Created Date: Apr 16, 2017
 *
 * Description : MAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../util/coder/AtCoderUtil.h"
#include "Tha60290022ModuleMapInternal.h"
#include "Tha60290022MapDemap.h"
#include "../../Tha60210011/sdh/Tha60210011ModuleSdh.h"
#include "../../Tha60210011/pw/activator/Tha60210011HwPw.h"
#include "../../Tha60210011/pwe/Tha60210011ModulePwe.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../ram/Tha60290022InternalRam.h"
#include "../man/Tha60290022Device.h"
#include "Tha60290022ModuleMapDemapReg.h"
#include "Tha60290022ModuleMapDemapOldReg.h"
#include "Tha60290022MapDemap.h"


/*--------------------------- Define -----------------------------------------*/
#define cNumHolds 3

#define cThaMapTmDs1E1IdleEnableMask                 cBit29
#define cThaMapTmDs1E1IdleEnableShift                29
#define cThaMapTmDs1E1IdleCodeMask                   cBit28_21
#define cThaMapTmDs1E1IdleCodeShift                  21

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((tTha60290022ModuleMap *)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;

/* Override */
static tAtObjectMethods             m_AtObjectOverride;
static tAtModuleMethods             m_AtModuleOverride;
static tThaModuleAbstractMapMethods m_ThaModuleAbstractMapOverride;
static tThaModuleMapMethods         m_ThaModuleMapOverride;
static tTha60210011ModuleMapMethods m_Tha60210011ModuleMapOverride;

/* Save super implementation */
static const tAtObjectMethods             *m_AtObjectMethods             = NULL;
static const tAtModuleMethods             *m_AtModuleMethods             = NULL;
static const tThaModuleAbstractMapMethods *m_ThaModuleAbstractMapMethods = NULL;
static const tThaModuleMapMethods         *m_ThaModuleMapMethods         = NULL;
static const tTha60210011ModuleMapMethods *m_Tha60210011ModuleMapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
mDefineMaskShift(ThaModuleMap, MapTmDs1E1IdleCode)
mDefineMaskShift(ThaModuleMap, MapTmDs1E1IdleEnable)

static uint8 MaxNumStsInLoSlice(Tha60210011ModuleMap self)
    {
    return Tha60290022ModuleMapDemapMaxNumStsInLoSlice((ThaModuleAbstractMap)self);
    }

static uint32 NumberOfMapLoPwPerSlice(Tha60210011ModuleMap self)
    {
    return Tha60290022DeviceNumberOfMapLoPwPerSlice(AtModuleDeviceGet((AtModule)self));
    }

static uint32 MapTmDs1LoopcodeMask(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_line_ctrl_MapDs1LcEn_Mask;
    }

static uint8  MapTmDs1LoopcodeShift(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_line_ctrl_MapDs1LcEn_Shift;
    }

static uint32 MapFrmTypeMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6_map_line_ctrl_MapFrameType_Mask;
    }

static uint8  MapFrmTypeShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6_map_line_ctrl_MapFrameType_Shift;
    }

static uint32 MapSigTypeMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6_map_line_ctrl_MapSigType_Mask;
    }

static uint8  MapSigTypeShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6_map_line_ctrl_MapSigType_Shift;
    }

static uint32 MapTimeSrcMastMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6_map_line_ctrl_MapTimeSrcMaster_Mask;
    }

static uint8  MapTimeSrcMastShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6_map_line_ctrl_MapTimeSrcMaster_Shift;
    }

static uint32 MapTimeSrcIdMask(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6_map_line_ctrl_MapTimeSrcId_Mask;
    }

static uint8  MapTimeSrcIdShift(ThaModuleMap self, AtSdhChannel sdhChannel)
    {
    AtUnused(self);
    AtUnused(sdhChannel);
    return cAf6_map_line_ctrl_MapTimeSrcId_Shift;
    }

static uint32 VcMapChnCtrl(ThaModuleMap self, AtSdhVc vc)
    {
    AtUnused(self);
    AtUnused(vc);
    return cAf6Reg_map_channel_ctrl_Base;
    }

static uint32 MapChnTypeMask(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_MapChannelType_Mask;
    }

static uint8  MapChnTypeShift(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_MapChannelType_Shift;
    }

static uint32 MapFirstTsMask(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_MapFirstTs_Mask;
    }

static uint8  MapFirstTsShift(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_MapFirstTs_Shift;
    }

static uint32 MapTsEnMask(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_MapChEn_Mask;
    }

static uint8  MapTsEnShift(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_MapChEn_Shift;
    }

static uint32 MapPWIdFieldMask(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_MapPWIdField_Mask;
    }

static uint8  MapPWIdFieldShift(ThaModuleMap self)
    {
    AtUnused(self);
    return cAf6_map_channel_ctrl_MapPWIdField_Shift;
    }

static uint32 HoLineVcPwAllocate(Tha60210011ModuleMap self, ThaPwAdapter adapter, uint8 *slice)
    {
    return m_Tha60210011ModuleMapMethods->HoLineVcPwAllocate(self, adapter, slice) * 28;
    }

static uint32 StartMapLoRamId(AtModule self)
    {
    AtModuleSdh moduleSdh = (AtModuleSdh)AtDeviceModuleGet(AtModuleDeviceGet(self), cAtModuleSdh);
    return (uint32)(Tha60210011ModuleSdhMaxNumHoLines(moduleSdh) * 2);
    }

static AtInternalRam InternalRamCreate(AtModule self, uint32 ramId, uint32 localRamId)
    {
    if (localRamId < StartMapLoRamId(self))
        return m_AtModuleMethods->InternalRamCreate(self, ramId, localRamId);
    return Tha60290022InternalRamMapNew(self, ramId, localRamId);
    }

static uint32 LomapChannelCtrlBase(Tha60210011ModuleMap self)
    {
    AtUnused(self);
    return cAf6Reg_map_channel_ctrl_Base;
    }

static AtLongRegisterAccess CreateLongRegisterAccess(AtModule self, uint8 sliceId)
    {
    uint32 holdRegs[cNumHolds];
    uint32 hold_i;
    uint32 sliceOffset = Tha60210011ModuleMapDemapLoSliceOffset((ThaModuleAbstractMap)self, sliceId);

    for (hold_i = 0; hold_i < cNumHolds; hold_i++)
        holdRegs[hold_i] = cAf6Reg_map_cpu_hold_ctrl_Base + hold_i + sliceOffset;

    return AtDefaultLongRegisterAccessNew(holdRegs, cNumHolds, holdRegs, cNumHolds);
    }

static eAtRet LongRegisterAccessesSetup(AtModule self)
    {
    uint8 numSlices = Tha60290022ModuleMapDemapNumLoSlices((ThaModuleAbstractMap)self);
    uint8 slice_i;
    AtLongRegisterAccess *longAccess;

    if (mThis(self)->longRegisterAccesses)
        return cAtOk;

    longAccess = AtOsalMemAlloc(sizeof(AtLongRegisterAccess) * numSlices);
    if (longAccess == NULL)
        return cAtErrorRsrcNoAvail;

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        {
        uint32 slice_j;

        longAccess[slice_i] = CreateLongRegisterAccess(self, slice_i);

        /* Cleanup on failure */
        if (longAccess[slice_i] == NULL)
            {
            for (slice_j = 0; slice_j < slice_i; slice_j++)
                AtObjectDelete((AtObject)longAccess[slice_j]);
            AtOsalMemFree(longAccess);
            }
        }

    mThis(self)->longRegisterAccesses = longAccess;

    return cAtOk;
    }

static eAtRet LongRegisterAccessesDelete(AtModule self)
    {
    uint32 numSlices = Tha60290022ModuleMapDemapNumLoSlices((ThaModuleAbstractMap)self);
    uint32 slice_i;

    if (mThis(self)->longRegisterAccesses == NULL)
        return cAtOk;

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        AtObjectDelete((AtObject)mThis(self)->longRegisterAccesses[slice_i]);

    AtOsalMemFree(mThis(self)->longRegisterAccesses);
    mThis(self)->longRegisterAccesses = NULL;

    return cAtOk;
    }

static eBool HwLogicOptimized(AtModule self)
    {
    return Tha60290022DeviceHwLogicOptimized(AtModuleDeviceGet(self));
    }

static eAtRet Setup(AtModule self)
    {
    eAtRet ret;

    ret = m_AtModuleMethods->Setup(self);
    if (ret != cAtOk)
        return ret;

    if (HwLogicOptimized(self))
        ret |= LongRegisterAccessesSetup(self);

    return ret;
    }

static void Delete(AtObject self)
    {
    LongRegisterAccessesDelete((AtModule)self);
    m_AtObjectMethods->Delete(self);
    }

static void Serialize(AtObject self, AtCoder encoder)
    {
    tTha60290022ModuleMap *object = mThis(self);
    m_AtObjectMethods->Serialize(self, encoder);

    mEncodeObjects(longRegisterAccesses, object->numSliceLongRegisterAccess);
    }

static uint32 AddressSliceId(AtModule self, uint32 localAddress)
    {
    AtUnused(self);

    if (mInRange(localAddress, 0x0800000, 0x083FFFF)) return 0;
    if (mInRange(localAddress, 0x0840000, 0x087FFFF)) return 1;
    if (mInRange(localAddress, 0x0880000, 0x08BFFFF)) return 2;
    if (mInRange(localAddress, 0x08C0000, 0x08FFFFF)) return 3;
    if (mInRange(localAddress, 0x0900000, 0x093FFFF)) return 4;
    if (mInRange(localAddress, 0x0940000, 0x097FFFF)) return 5;
    if (mInRange(localAddress, 0x0980000, 0x09BFFFF)) return 6;
    if (mInRange(localAddress, 0x09C0000, 0x09FFFFF)) return 7;

    return cInvalidUint32;
    }

static eBool HasRegister(AtModule self, uint32 localAddress)
    {
    if (AddressSliceId(self, localAddress) == cInvalidUint32)
        return cAtFalse;

    return cAtTrue;
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    if (HwLogicOptimized(self))
        {
        uint32 sliceId = AddressSliceId(self, localAddress);

        if (mThis(self)->longRegisterAccesses == NULL)
            LongRegisterAccessesSetup(self);

        if (mThis(self)->longRegisterAccesses == NULL)
            return NULL;

        if (sliceId != cInvalidUint32)
            return mThis(self)->longRegisterAccesses[sliceId];
        }

    return m_AtModuleMethods->LongRegisterAccess(self, localAddress);
    }

static AtModuleSdh SdhModule(Tha60210011ModuleMap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModuleSdh)AtDeviceModuleGet(device, cAtModuleSdh);
    }

static uint32 NumSlices(Tha60210011ModuleMap self)
    {
    return Tha60290021ModuleSdhNumUseableTerminatedLines(SdhModule(self));
    }

static ThaModulePwe PweModule(Tha60210011ModuleMap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (ThaModulePwe)AtDeviceModuleGet(device, cThaModulePwe);
    }

static AtModulePw PwModule(Tha60210011ModuleMap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModulePw)AtDeviceModuleGet(device, cAtModulePw);
    }

static uint32 AllPwLocalIdRestore(Tha60210011ModuleMap self)
    {
    uint32 numSlices = NumSlices(self);
    uint32 slice_i, localPw;
    uint32 numLocalPwsPerSlice = mMethodsGet(self)->NumberOfMapLoPwPerSlice(self);
    Tha60210011ModulePwe pweModule = (Tha60210011ModulePwe)PweModule(self);
    AtModulePw pwModule = PwModule(self);

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        {
        for (localPw = 0; localPw < numLocalPwsPerSlice; localPw++)
            {
            uint32 absolutePwId = Tha60210011ModulePweAbsolutePwIdGet(pweModule, slice_i, localPw);
            AtPw pw;
            AtChannel circuit;
            ThaPwAdapter pwAdapter;

            pw = AtModulePwGetPw(pwModule, absolutePwId);
            if (pw == NULL)
                continue;

            circuit = AtPwBoundCircuitGet(pw);
            if (circuit == NULL)
                continue;

            /* Still need to confirm with this module to see if this matches.
             * This is to avoid garbage register values */
            pwAdapter = ThaPwAdapterGet(pw);
            if (Tha60210011HwPwSliceGet(ThaPwAdapterHwPwGet(pwAdapter)) != slice_i)
                continue;

            if (AtChannelBoundPwHwIdGet(circuit) == localPw)
                {
                ThaHwPw hwPw = ThaPwAdapterHwPwGet(pwAdapter);
                ThaBitMask pool = Tha60210011ModuleMapPoolBySlice((ThaModuleMap)self, (uint8)slice_i);
                Tha60210011HwPwTdmIdSet(hwPw, localPw);
                ThaBitMaskSetBit(pool, localPw);
                }
            }
        }

    return 0;
    }

static uint32 StartMapLocalRamId(Tha60210011ModuleMap self)
    {
    AtUnused(self);
    return 16;
    }

static uint32 StopMapLoRamId(AtModule self)
    {
    return Tha60210011ModuleMapStartLoRamId((ThaModuleMap)self) +
           Tha60210011ModuleSdhMaxNumLoLines(SdhModule((Tha60210011ModuleMap)self)) * 3U;
    }

static eBool InternalRamIsReserved(AtModule self, AtInternalRam ram)
    {
    if (AtInternalRamLocalIdGet(ram) < Tha60210011ModuleMapStartLoRamId((ThaModuleMap)self))
        return cAtTrue;

    if (AtInternalRamLocalIdGet(ram) >= StopMapLoRamId(self))
        return cAtTrue;

    return cAtFalse;
    }

static eBool IsVc4_64c(AtSdhVc vc)
    {
    return (AtSdhChannelTypeGet((AtSdhChannel)vc) == cAtSdhChannelTypeVc4_64c) ? cAtTrue : cAtFalse;
    }

static eBool NeedMapConfigureWhenBindingVc4_64cToPseudowire(ThaModuleAbstractMap self)
    {
    return Tha60290022DeviceNeedMapConfigureWhenBindingVc4_64cToPseudowire(AtModuleDeviceGet((AtModule)self));
    }

static eAtRet BindAuVcToPseudowire(ThaModuleAbstractMap self, AtSdhVc vc, AtPw pw)
    {
    if (IsVc4_64c(vc) && NeedMapConfigureWhenBindingVc4_64cToPseudowire(self))
        return ThaModuleStmMapBindAuVcToPseudowire((ThaModuleStmMap)self, vc, pw);

    return m_ThaModuleAbstractMapMethods->BindAuVcToPseudowire(self, vc, pw);
    }

static eAtRet AuVcFrameModeSet(ThaModuleAbstractMap self, AtSdhVc vc)
    {
    if (IsVc4_64c(vc) && NeedMapConfigureWhenBindingVc4_64cToPseudowire(self))
        return ThaModuleStmMapAuVcFrameModeSet((ThaModuleStmMap)self, vc);

    return m_ThaModuleAbstractMapMethods->AuVcFrameModeSet(self, vc);
    }

static eAtRet VcxEncapConnectionEnable(ThaModuleAbstractMap self, AtSdhVc vcx, eBool enable)
    {
    if (IsVc4_64c(vcx) && NeedMapConfigureWhenBindingVc4_64cToPseudowire(self))
        return ThaModuleStmMapVcxEncapConnectionEnable((ThaModuleStmMap)self, vcx, enable);

    return m_ThaModuleAbstractMapMethods->VcxEncapConnectionEnable(self, vcx, enable);
    }

static eBool SdhChannelIsVcx(AtSdhChannel sdhChannel)
    {
    if (AtSdhChannelIsHoVc(sdhChannel))
        return cAtTrue;

    if (AtSdhChannelTypeGet(sdhChannel) == cAtSdhChannelTypeVc3)
        return cAtTrue;

    return cAtFalse;
    }

static eBool SdhChannelIsVc1x(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);
    if ((channelType == cAtSdhChannelTypeVc12)    ||
        (channelType == cAtSdhChannelTypeVc11))
        return cAtTrue;

    return cAtFalse;
    }

static void VtMapLineDisplay(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, const char* regName, uint32 address)
    {
    uint8 sts = AtSdhChannelSts1Get(sdhChannel);
    uint8 vtg = AtSdhChannelTug2Get(sdhChannel);
    uint8 tu  = AtSdhChannelTu1xGet(sdhChannel);

    ThaDeviceRegNameDisplay(regName);
    address = address + ThaModuleStmMapSdhChannelMapLineOffset((ThaModuleStmMap)self, sdhChannel, sts, vtg, tu);
    ThaDeviceChannelRegValueDisplay((AtChannel)sdhChannel, address, cThaModuleMap);
    }

static void StsMultiMapLineDisplay(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, const char* regName, uint32 regBaseAddress)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 address = regBaseAddress + ThaModuleStmMapSdhChannelMapLineOffset((ThaModuleStmMap)self, sdhChannel, (uint8)(startSts + sts_i), 0, 0);
        ThaDeviceChannelRegValueDisplay(channel, address, cThaModuleMap);
        }
    }

static void VtMapChannelDisplay(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, const char* regName, uint32 regBaseAddress)
    {
    uint16 numTimeslots = ThaModuleStmMapDemapNumTimeSlotInVc1x((AtSdhVc)sdhChannel);
    uint16 address_i;
    uint16 numAddress = (uint16)(numTimeslots / 4);

    /* There is product not support this register */
    if (regBaseAddress == cBit31_0)
        return;

    ThaDeviceRegNameDisplay(regName);
    for (address_i = 0; address_i < numAddress; address_i++)
        {
        uint32 address = regBaseAddress + mMethodsGet(self)->Vc1xDefaultOffset(self, (AtSdhVc)sdhChannel) + address_i;
        ThaDeviceChannelRegLongValueDisplay((AtChannel)sdhChannel, address, cThaModuleMap);
        }
    }

static void StsMultiMapChannelDisplay(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, const char* regName, uint32 regBaseAddress)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    /* There is product not support this register */
    if (regBaseAddress == cBit31_0)
        return;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 address = regBaseAddress + mMethodsGet(self)->AuVcStsDefaultOffset(self, (AtSdhVc)sdhChannel, (uint8)(startSts + sts_i));
        ThaDeviceChannelRegLongValueDisplay(channel, address, cThaModuleMap);
        }
    }

static void SdhChannelRegsV2Show(ThaModuleAbstractMap self, AtSdhChannel sdhChannel)
    {
    uint32 address;
    eBool isVcx  = SdhChannelIsVcx(sdhChannel);
    eBool isVc1x = SdhChannelIsVc1x(sdhChannel);

    if (isVcx)
        {
        address = mMethodsGet((ThaModuleStmMap)self)->MapLineCtrl((ThaModuleStmMap)self, sdhChannel);
        StsMultiMapLineDisplay(self, sdhChannel, "Map Line Control", address);

        address = mMethodsGet((ThaModuleMap)self)->VcMapChnCtrl((ThaModuleMap)self, (AtSdhVc)sdhChannel);
        StsMultiMapChannelDisplay(self, sdhChannel, "Map Channel Control", address);
        return;
        }

    if (isVc1x)
        {
        address = mMethodsGet((ThaModuleStmMap)self)->MapLineCtrl((ThaModuleStmMap)self, sdhChannel);
        VtMapLineDisplay(self, sdhChannel, "Map Line Control", address);

        address = mMethodsGet((ThaModuleMap)self)->VcMapChnCtrl((ThaModuleMap)self, (AtSdhVc)sdhChannel);
        VtMapChannelDisplay(self, sdhChannel, "Map Channel Control", address);
        }
    }

static void SdhChannelRegsShow(ThaModuleAbstractMap self, AtSdhChannel sdhChannel)
    {
    if (HwLogicOptimized((AtModule)self))
        {
        SdhChannelRegsV2Show(self, sdhChannel);
        return;
        }

    m_ThaModuleAbstractMapMethods->SdhChannelRegsShow(self, sdhChannel);
    }

static void OverrideAtObject(AtModule self)
    {
    AtObject object = (AtObject)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtObjectMethods = mMethodsGet(object);
        mMethodsGet(osal)->MemCpy(osal, &m_AtObjectOverride, m_AtObjectMethods, sizeof(m_AtObjectOverride));

        mMethodOverride(m_AtObjectOverride, Delete);
        mMethodOverride(m_AtObjectOverride, Serialize);
        }

    mMethodsSet(object, &m_AtObjectOverride);
    }

static void OverrideTha60210011ModuleMap(AtModule self)
    {
    Tha60210011ModuleMap module = (Tha60210011ModuleMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleMapMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleMapOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleMapOverride));

        mMethodOverride(m_Tha60210011ModuleMapOverride, MaxNumStsInLoSlice);
        mMethodOverride(m_Tha60210011ModuleMapOverride, NumberOfMapLoPwPerSlice);
        mMethodOverride(m_Tha60210011ModuleMapOverride, HoLineVcPwAllocate);
        mMethodOverride(m_Tha60210011ModuleMapOverride, LomapChannelCtrlBase);
        mMethodOverride(m_Tha60210011ModuleMapOverride, AllPwLocalIdRestore);
        mMethodOverride(m_Tha60210011ModuleMapOverride, StartMapLocalRamId);
        }

    mMethodsSet(module, &m_Tha60210011ModuleMapOverride);
    }

static void OverrideThaModuleMap(AtModule self)
    {
    ThaModuleMap mapModule = (ThaModuleMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleMapOverride, mMethodsGet(mapModule), sizeof(m_ThaModuleMapOverride));

        mMethodOverride(m_ThaModuleMapOverride, MapFrmTypeMask);
        mMethodOverride(m_ThaModuleMapOverride, MapFrmTypeShift);
        mMethodOverride(m_ThaModuleMapOverride, MapSigTypeMask);
        mMethodOverride(m_ThaModuleMapOverride, MapSigTypeShift);
        mMethodOverride(m_ThaModuleMapOverride, MapTimeSrcMastMask);
        mMethodOverride(m_ThaModuleMapOverride, MapTimeSrcMastShift);
        mMethodOverride(m_ThaModuleMapOverride, MapTimeSrcIdMask);
        mMethodOverride(m_ThaModuleMapOverride, MapTimeSrcIdShift);
        mMethodOverride(m_ThaModuleMapOverride, VcMapChnCtrl);
        mMethodOverride(m_ThaModuleMapOverride, MapChnTypeMask);
        mMethodOverride(m_ThaModuleMapOverride, MapChnTypeShift);
        mMethodOverride(m_ThaModuleMapOverride, MapFirstTsMask);
        mMethodOverride(m_ThaModuleMapOverride, MapFirstTsShift);
        mMethodOverride(m_ThaModuleMapOverride, MapTsEnMask);
        mMethodOverride(m_ThaModuleMapOverride, MapTsEnShift);
        mMethodOverride(m_ThaModuleMapOverride, MapPWIdFieldMask);
        mMethodOverride(m_ThaModuleMapOverride, MapPWIdFieldShift);
        mMethodOverride(m_ThaModuleMapOverride, MapTmDs1LoopcodeMask);
        mMethodOverride(m_ThaModuleMapOverride, MapTmDs1LoopcodeShift);
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTmDs1E1IdleCode)
        mBitFieldOverride(ThaModuleMap, m_ThaModuleMapOverride, MapTmDs1E1IdleEnable)
        }

    mMethodsSet(mapModule, &m_ThaModuleMapOverride);
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, InternalRamCreate);
        mMethodOverride(m_AtModuleOverride, Setup);
        mMethodOverride(m_AtModuleOverride, HasRegister);
        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        mMethodOverride(m_AtModuleOverride, InternalRamIsReserved);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

#include "Tha60290022MapDemapOverride.h"

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, HoSliceOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, NumLoSlices);
        mMethodOverride(m_ThaModuleAbstractMapOverride, ChannelCtrlAddressFormula);
        mMethodOverride(m_ThaModuleAbstractMapOverride, TimeslotAddressFromBase);
        mMethodOverride(m_ThaModuleAbstractMapOverride, TimeslotInfoSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, TimeslotInfoGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcBoundPwHwIdGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, BindAuVcToPseudowire);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcFrameModeSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, VcxEncapConnectionEnable);
        mMethodOverride(m_ThaModuleAbstractMapOverride, SdhChannelRegsShow);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void Override(AtModule self)
    {
    OverrideAtObject(self);
    OverrideAtModule(self);
    OverrideThaModuleAbstractMap(self);
    OverrideThaModuleMap(self);
    OverrideTha60210011ModuleMap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleMap);
    }

AtModule Tha60290022ModuleMapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleMapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290022ModuleMapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModuleMapObjectInit(newModule, device);
    }

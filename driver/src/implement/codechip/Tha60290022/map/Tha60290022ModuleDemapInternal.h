/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2018 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : DEMAP
 * 
 * File        : Tha60290022ModuleDemapInternal.h
 * 
 * Created Date: Jul 30, 2018
 *
 * Description : internal data of 60290022 module DEMAP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEDEMAPINTERNAL_H_
#define _THA60290022MODULEDEMAPINTERNAL_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../Tha60210011/map/Tha60210011ModuleMapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/
typedef struct tTha60290022ModuleDemap *Tha60290022ModuleDemap;

typedef struct tTha60290022ModuleDemapMethods
    {
    uint32 (*HoDemapPWIdFieldMask)(Tha60290022ModuleDemap self);
    uint32 (*HoDemapPWIdFieldShift)(Tha60290022ModuleDemap self);
    }tTha60290022ModuleDemapMethods;

typedef struct tTha60290022ModuleDemap
    {
    tTha60210011ModuleDemap super;
    const tTha60290022ModuleDemapMethods *methods;
    }tTha60290022ModuleDemap;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
AtModule Tha60290022ModuleDemapObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEDEMAPINTERNAL_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP/DEMAP
 * 
 * File        : Tha60290022MapDemap.h
 * 
 * Created Date: Apr 18, 2017
 *
 * Description : Common function for MAP/DEMAP
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MAPDEMAP_H_
#define _THA60290022MAPDEMAP_H_

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/map/ThaModuleAbstractMap.h"
#include "Tha60290022ModuleDemapInternal.h"
#include "Tha60290022ModuleMapInternal.h"

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
uint32 Tha60290022ModuleMapDemapHoSliceOffset(ThaModuleAbstractMap self, uint8 slice);
uint8 Tha60290022ModuleMapDemapNumLoSlices(ThaModuleAbstractMap self);
uint8 Tha60290022ModuleMapDemapMaxNumStsInLoSlice(ThaModuleAbstractMap self);
uint32 Tha60290022ModuleMapDemapHwLogicOptimizedChannelCtrlAddressFormula(ThaModuleAbstractMap self, eThaMapChannelType channelType, uint32 stsid, uint32 vtgid, uint32 vtid, uint32 slotid);
uint32 Tha60290022ModuleMapDemapHwLogicOptimizedTimeslotAddressFromBase(ThaModuleAbstractMap self, uint32 address, uint32 slotid);
eAtRet Tha60290022ModuleMapDemapHwLogicOptimizedTimeslotInfoSet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, const tThaMapTimeslotInfo *slotInfo);
eAtRet Tha60290022ModuleMapDemapHwLogicOptimizedTimeslotInfoGet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, tThaMapTimeslotInfo *slotInfo);
AtModule Tha60290022ModuleDemapObjectInit(AtModule self, AtDevice device);
AtModule Tha60290022ModuleMapObjectInit(AtModule self, AtDevice device);

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MAPDEMAP_H_ */


/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : Tha60290022MapDemapOverride.h
 * 
 * Created Date: Sep 30, 2017
 *
 * Description : MAP/DEMAP common overridden
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Includes ---------------------------------------*/
#include "../../../default/map/ThaModuleAbstractMap.h"
#include "../../Tha60290021/sdh/Tha6029SdhLineSideAuVc.h"
#include "../../Tha60290021/sdh/Tha60290021ModuleSdh.h"
#include "../../Tha60290021/xc/Tha60290021ModuleXc.h"

/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
static uint8 NumLoSlices(ThaModuleAbstractMap self)
    {
    return Tha60290022ModuleMapDemapNumLoSlices(self);
    }

static uint32 HoSliceOffset(ThaModuleAbstractMap self, uint8 slice)
    {
    return Tha60290022ModuleMapDemapHoSliceOffset(self, slice);
    }

static uint32 ChannelCtrlAddressFormula(ThaModuleAbstractMap self, eThaMapChannelType channelType, uint32 stsid, uint32 vtgid, uint32 vtid, uint32 slotid)
    {
    if (HwLogicOptimized((AtModule)self))
        return Tha60290022ModuleMapDemapHwLogicOptimizedChannelCtrlAddressFormula(self, channelType, stsid, vtgid, vtid, slotid);
    return m_ThaModuleAbstractMapMethods->ChannelCtrlAddressFormula(self, channelType, stsid, vtgid, vtid, slotid);
    }

static uint32 TimeslotAddressFromBase(ThaModuleAbstractMap self, uint32 address, uint32 slotid)
    {
    if (HwLogicOptimized((AtModule)self))
        return Tha60290022ModuleMapDemapHwLogicOptimizedTimeslotAddressFromBase(self, address, slotid);
    return m_ThaModuleAbstractMapMethods->TimeslotAddressFromBase(self, address, slotid);
    }

static eAtRet TimeslotInfoSet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, const tThaMapTimeslotInfo *slotInfo)
    {
    if (HwLogicOptimized((AtModule)self))
        return Tha60290022ModuleMapDemapHwLogicOptimizedTimeslotInfoSet(self, channel, timeslot, address, slotInfo);

    return m_ThaModuleAbstractMapMethods->TimeslotInfoSet(self, channel, timeslot, address, slotInfo);
    }

static eAtRet TimeslotInfoGet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, tThaMapTimeslotInfo *slotInfo)
    {
    if (HwLogicOptimized((AtModule)self))
        return Tha60290022ModuleMapDemapHwLogicOptimizedTimeslotInfoGet(self, channel, timeslot, address, slotInfo);

    return m_ThaModuleAbstractMapMethods->TimeslotInfoGet(self, channel, timeslot, address, slotInfo);
    }

static AtModuleXc XcModule(ThaModuleAbstractMap self)
    {
    AtDevice device = AtModuleDeviceGet((AtModule)self);
    return (AtModuleXc)AtDeviceModuleGet(device, cAtModuleXc);
    }

static eBool XcHidingIsActive(ThaModuleAbstractMap self)
    {
    eTha60290021XcHideMode hideMode = Tha60290021ModuleXcHideModeGet(XcModule(self));
    return (hideMode == cTha60290021XcHideModeNone) ? cAtFalse : cAtTrue;
    }

static uint32 AuVcBoundPwHwIdGet(ThaModuleAbstractMap self, AtSdhVc vc, eAtPwCepMode *cepMode)
    {
    AtSdhVc redirectedVc = vc;

    if (XcHidingIsActive(self) && Tha60290021ModuleSdhIsLineSideVc((AtSdhChannel)vc))
        redirectedVc = (AtSdhVc)AtSdhChannelHideChannelGet((AtSdhChannel)vc);

    return m_ThaModuleAbstractMapMethods->AuVcBoundPwHwIdGet(self, redirectedVc, cepMode);
    }


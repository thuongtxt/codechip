/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP
 *
 * File        : Tha60290022ModuleDemap.c
 *
 * Created Date: Apr 16, 2017
 *
 * Description : MAP module
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../Tha60210011/sdh/Tha60210011ModuleSdh.h"
#include "../man/Tha60290022Device.h"
#include "Tha60290022ModuleMapDemapReg.h"
#include "Tha60290022ModuleMapDemapOldReg.h"
#include "Tha60290022ModuleDemapInternal.h"
#include "Tha60290022MapDemap.h"
/*--------------------------- Define -----------------------------------------*/

/*--------------------------- Macros -----------------------------------------*/
#define mThis(self) ((Tha60290022ModuleDemap)self)

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/
static uint8 m_methodsInit = 0;
static tTha60290022ModuleDemapMethods m_methods;

/* Override */
static tAtModuleMethods               m_AtModuleOverride;
static tThaModuleAbstractMapMethods   m_ThaModuleAbstractMapOverride;
static tThaModuleDemapMethods         m_ThaModuleDemapOverride;
static tTha60210011ModuleDemapMethods m_Tha60210011ModuleDemapOverride;

/* Save super implementation */
static const tAtModuleMethods               *m_AtModuleMethods       = NULL;
static const tThaModuleAbstractMapMethods   *m_ThaModuleAbstractMapMethods = NULL;
static const tThaModuleDemapMethods         *m_ThaModuleDemapMethods = NULL;
static const tTha60210011ModuleDemapMethods *m_Tha60210011ModuleDemapMethods = NULL;

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 DmapChnTypeMask(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapChannelType_Mask;
    }

static uint8  DmapChnTypeShift(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapChannelType_Shift;
    }

static uint32 DmapFirstTsMask(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapFirstTs_Mask;
    }

static uint8  DmapFirstTsShift(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapFirstTs_Shift;
    }

static uint32 DmapTsEnMask(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapChEn_Mask;
    }

static uint8  DmapTsEnShift(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapChEn_Shift;
    }

static uint32 DmapPwIdMask(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapPWIdField_Mask;
    }

static uint8  DmapPwIdShift(ThaModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demap_channel_ctrl_DemapPWIdField_Shift;
    }

static uint32 VcDmapChnCtrl(ThaModuleDemap self, AtSdhVc vc)
    {
    Tha60210011ModuleDemap module = (Tha60210011ModuleDemap)self;
    if (mMethodsGet(module)->VcIsHoCep(module, vc))
        return cAf6Reg_demapho_channel_ctrl_Base;
    return cAf6Reg_demap_channel_ctrl_Base;
    }

static eBool IsLongRegDemapCtrl(Tha60210011ModuleDemap self)
    {
    AtUnused(self);
    return cAtTrue;
    }

static uint32 HoVcDemapBusSelectMask(Tha60210011ModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demapho_channel_ctrl_DemapCh_BusSelect_Mask;
    }

static uint32 HoVcDemapBusSelectShift(Tha60210011ModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demapho_channel_ctrl_DemapCh_BusSelect_Shift;
    }

static eBool ShouldEnableTrafficForBert(AtChannel self)
    {
    return AtChannelPrbsEngineIsCreated(self);
    }

static eAtRet BindHoStsToPseudowire(Tha60210011ModuleDemap self, AtSdhVc vc, uint8 hwSlice, uint8 hwSts, AtPw pw)
    {
    ThaModuleAbstractMap abstractMap = (ThaModuleAbstractMap)self;
    eBool enable = mMethodsGet(abstractMap)->NeedEnableAfterBindToPw(abstractMap, pw);
    uint32 regAddr = ThaModuleDemapVcDmapChnCtrl((ThaModuleDemap)self, vc) + ThaModuleAbstractMapHoSliceOffset(abstractMap, hwSlice) + hwSts;
    uint32 regVal  = mChannelHwRead(vc, regAddr, cThaModuleDemap);
    uint32 fieldMask, fieldShift;
    uint32 hwEnabled;

    /* Enabling */
    hwEnabled = pw ? enable : 0UL;

    fieldMask = mMethodsGet(self)->HoVcDemapChannelCtrlDemapChEnMask(self);
    fieldShift = mMethodsGet(self)->HoVcDemapChannelCtrlDemapChEnShift(self);
    mRegFieldSet(regVal, field, hwEnabled);

    /* PW association */
    mRegFieldSet(regVal, cAf6_demapho_channel_ctrl_DemapChannelType_, ThaModuleAbstractMapPwHwChannelType(abstractMap, pw));
    fieldMask = mMethodsGet(mThis(self))->HoDemapPWIdFieldMask(mThis(self));
    fieldShift = mMethodsGet(mThis(self))->HoDemapPWIdFieldShift(mThis(self));
    mRegFieldSet(regVal, field, (pw) ? AtChannelHwIdGet((AtChannel)pw) : 0);

    mChannelHwWrite(vc, regAddr, regVal, cThaModuleDemap);

    return cAtOk;
    }

static eAtRet HoLineVcxEncapConnectionEnable(Tha60210011ModuleDemap self, AtSdhVc vcx, eBool enable)
    {
    ThaModuleAbstractMap demapModule = (ThaModuleAbstractMap)self;
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)vcx;
    uint32 demapChnCtrl = mMethodsGet((ThaModuleDemap)self)->VcDmapChnCtrl((ThaModuleDemap)self, vcx);
    uint8 stsId  = AtSdhChannelSts1Get(sdhChannel);
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint32 enable_Mask = mMethodsGet(self)->HoVcDemapChannelCtrlDemapChEnMask(self);
    uint32 enable_Shift = mMethodsGet(self)->HoVcDemapChannelCtrlDemapChEnShift(self);
    uint32 hoBusSelect_Mask = HoVcDemapBusSelectMask(self);
    uint32 hoBusSelect_Shift = HoVcDemapBusSelectShift(self);

    eBool hoBusIsEnabled = enable;

    if (!enable)
        hoBusIsEnabled = ShouldEnableTrafficForBert((AtChannel)vcx);/* This bit is not only used for HW BERT but also used for PW */

    /* Need to disable hardware before reseting configuration */
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 sts1Id = (uint8)(stsId + sts_i);
        uint8 hwSlice, hwSts;
        uint32 regAddr, regVal;

        if (ThaSdhChannelHwStsGet(sdhChannel, cThaModuleDemap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(sdhChannel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        regAddr = demapChnCtrl + ThaModuleAbstractMapHoSliceOffset(demapModule, hwSlice) + hwSts;
        regVal  = mChannelHwRead(vcx, regAddr, cThaModuleMap);
        mRegFieldSet(regVal, hoBusSelect_, mBoolToBin(hoBusIsEnabled));

        mRegFieldSet(regVal, enable_, mBoolToBin(enable));
        mChannelHwWrite(vcx, regAddr, regVal, cThaModuleMap);
        }

    return cAtOk;
    }

static eBool HoLineVcxEncapConnectionIsEnabled(Tha60210011ModuleDemap self, AtSdhVc vcx)
    {
    ThaModuleAbstractMap demapModule = (ThaModuleAbstractMap)self;
    uint8 sts_i;
    AtSdhChannel sdhChannel = (AtSdhChannel)vcx;
    uint32 demapChnCtrl = mMethodsGet((ThaModuleDemap)self)->VcDmapChnCtrl((ThaModuleDemap)self, vcx);
    uint8 stsId  = AtSdhChannelSts1Get(sdhChannel);
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint32 enableMask = mMethodsGet(self)->HoVcDemapChannelCtrlDemapChEnMask(self);

    /* Need to disable hardware before reseting configuration */
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint8 sts1Id = (uint8)(stsId + sts_i);
        uint8 hwSlice, hwSts;
        uint32 regAddr, regVal;

        if (ThaSdhChannelHwStsGet(sdhChannel, cThaModuleDemap, sts1Id, &hwSlice, &hwSts) != cAtOk)
            mChannelLog(sdhChannel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");

        regAddr = demapChnCtrl + ThaModuleAbstractMapHoSliceOffset(demapModule, hwSlice) + hwSts;
        regVal  = mChannelHwRead(vcx, regAddr, cThaModuleMap);

        if ((regVal & enableMask) == 0)
            return cAtFalse;
        }

    return (numSts != 0) ? cAtTrue : cAtFalse;
    }

static eBool HwLogicOptimized(AtModule self)
    {
    return Tha60290022DeviceHwLogicOptimized(AtModuleDeviceGet(self));
    }

static eBool SdhChannelIsVcx(AtSdhChannel sdhChannel)
    {
    if (AtSdhChannelIsHoVc(sdhChannel))
        return cAtTrue;

    if (AtSdhChannelTypeGet(sdhChannel) == cAtSdhChannelTypeVc3)
        return cAtTrue;

    return cAtFalse;
    }

static eBool SdhChannelIsVc1x(AtSdhChannel sdhChannel)
    {
    eAtSdhChannelType channelType = AtSdhChannelTypeGet(sdhChannel);
    if ((channelType == cAtSdhChannelTypeVc12)    ||
        (channelType == cAtSdhChannelTypeVc11))
        return cAtTrue;

    return cAtFalse;
    }

static void VtDemapChannelDisplay(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, const char* regName, uint32 regBaseAddress)
    {
    uint16 numTimeslots = ThaModuleStmMapDemapNumTimeSlotInVc1x((AtSdhVc)sdhChannel);
    uint16 address_i;
    uint16 numAddress = (uint16)(numTimeslots / 4);

    /* There is product not support this register */
    if (regBaseAddress == cBit31_0)
        return;

    ThaDeviceRegNameDisplay(regName);
    for (address_i = 0; address_i < numAddress; address_i++)
        {
        uint32 address = regBaseAddress + mMethodsGet(self)->Vc1xDefaultOffset(self, (AtSdhVc)sdhChannel) + address_i;
        ThaDeviceChannelRegLongValueDisplay((AtChannel)sdhChannel, address, cThaModuleDemap);
        }
    }

static void StsMultiDemapChannelDisplay(ThaModuleAbstractMap self, AtSdhChannel sdhChannel, const char* regName, uint32 regBaseAddress)
    {
    AtChannel channel = (AtChannel)sdhChannel;
    uint8 numSts = AtSdhChannelNumSts(sdhChannel);
    uint8 startSts = AtSdhChannelSts1Get(sdhChannel);
    uint8 sts_i;

    ThaDeviceRegNameDisplay(regName);
    for (sts_i = 0; sts_i < numSts; sts_i++)
        {
        uint32 address;
        uint8 hwSlice, hwSts;

        if (ThaSdhChannelHwStsGet(sdhChannel, cThaModuleDemap, (uint8)(startSts + sts_i), &hwSlice, &hwSts) != cAtOk)
            {
            mChannelLog(sdhChannel, cAtLogLevelCritical, "Cannot get HW sts ID and slice");
            continue;
            }

        address = regBaseAddress + ThaModuleAbstractMapHoSliceOffset(self, hwSlice) + hwSts;
        ThaDeviceChannelRegValueDisplay(channel, address, cThaModuleDemap);
        }
    }

static void SdhChannelRegsV2Show(ThaModuleAbstractMap self, AtSdhChannel sdhChannel)
    {
    uint32 address;
    eBool isVcx  = SdhChannelIsVcx(sdhChannel);
    eBool isVc1x = SdhChannelIsVc1x(sdhChannel);

    if (!isVcx && !isVc1x)
        return;

    address = mMethodsGet((ThaModuleDemap)self)->VcDmapChnCtrl((ThaModuleDemap)self, (AtSdhVc)sdhChannel);
    if (isVcx)
        StsMultiDemapChannelDisplay(self, sdhChannel, "Demap Channel Control", address);
    if (isVc1x)
        VtDemapChannelDisplay(self, sdhChannel, "Demap Channel Control", address);
    }

static void SdhChannelRegsShow(ThaModuleAbstractMap self, AtSdhChannel sdhChannel)
    {
    if (HwLogicOptimized((AtModule)self))
        {
        SdhChannelRegsV2Show(self, sdhChannel);
        return;
        }

    m_ThaModuleAbstractMapMethods->SdhChannelRegsShow(self, sdhChannel);
    }

static AtLongRegisterAccess LongRegisterAccess(AtModule self, uint32 localAddress)
    {
    if (HwLogicOptimized(self))
        {
        AtDevice device = AtModuleDeviceGet(self);
        AtModule mapModule = AtDeviceModuleGet(device, cThaModuleMap);
        return AtModuleLongRegisterAccess(mapModule, localAddress);
        }

    return m_AtModuleMethods->LongRegisterAccess(self, localAddress);
    }

static eAtRet DemapHoChannelControlPerSliceReset(AtModule self, uint8 sliceId)
    {
    uint32 cNumStsInSlice = Tha60290022ModuleMapDemapMaxNumStsInLoSlice((ThaModuleAbstractMap)self);
    uint32 address, baseAddress = Tha60210011ModuleMapDemapLoSliceOffset((ThaModuleAbstractMap)self, sliceId) + cAf6Reg_demapho_channel_ctrl_Base;
    uint32 stsId;

    for (stsId = 0; stsId < cNumStsInSlice; stsId++)
        {
        address = stsId + baseAddress;
        mModuleHwWrite(self, address, 0);
        }

    return cAtOk;
    }

static eAtRet RegisterReset(AtModule self)
    {
    AtDevice device = (AtDevice)AtModuleDeviceGet(self);
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)AtDeviceModuleGet((AtDevice)device, cThaModuleMap);
    uint8 numSlices = mMethodsGet(mapModule)->NumLoSlices(mapModule);
    uint8 slice_i;
    eAtRet ret = cAtOk;

    for (slice_i = 0; slice_i < numSlices; slice_i++)
        ret |= DemapHoChannelControlPerSliceReset(self, slice_i);

    return ret;
    }

static eAtRet Init(AtModule self)
    {
    eAtRet ret = cAtOk;

    ret = RegisterReset(self);
    if (ret != cAtOk)
        return ret;

    ret = m_AtModuleMethods->Init(self);
    if (ret != cAtOk)
        return ret;

    return ret;
    }

static uint32 HoDemapPWIdFieldMask(Tha60290022ModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demapho_channel_ctrl_DemapPWIdField_Mask;
    }

static uint32 HoDemapPWIdFieldShift(Tha60290022ModuleDemap self)
    {
    AtUnused(self);
    return cAf6_demapho_channel_ctrl_DemapPWIdField_Shift;
    }

static void OverrideAtModule(AtModule self)
    {
    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        m_AtModuleMethods = mMethodsGet(self);
        mMethodsGet(osal)->MemCpy(osal, &m_AtModuleOverride, m_AtModuleMethods, sizeof(m_AtModuleOverride));

        mMethodOverride(m_AtModuleOverride, LongRegisterAccess);
        mMethodOverride(m_AtModuleOverride, Init);
        }

    mMethodsSet(self, &m_AtModuleOverride);
    }

static eBool VcIsHoCep(Tha60210011ModuleDemap self, AtSdhVc vc)
    {
    if (AtSdhChannelTypeGet((AtSdhChannel)vc) == cAtSdhChannelTypeVc3)
        return AtSdhVcStuffIsEnabled(vc);

    return m_Tha60210011ModuleDemapMethods->VcIsHoCep(self, vc);
    }

static void OverrideTha60210011ModuleDemap(AtModule self)
    {
    Tha60210011ModuleDemap module = (Tha60210011ModuleDemap)self;

    if (!m_methodsInit)
        {
        /* Copy super implementation */
        AtOsal osal = AtSharedDriverOsalGet();
        m_Tha60210011ModuleDemapMethods = mMethodsGet(module);
        mMethodsGet(osal)->MemCpy(osal, &m_Tha60210011ModuleDemapOverride, mMethodsGet(module), sizeof(m_Tha60210011ModuleDemapOverride));

        mMethodOverride(m_Tha60210011ModuleDemapOverride, BindHoStsToPseudowire);
        mMethodOverride(m_Tha60210011ModuleDemapOverride, HoLineVcxEncapConnectionEnable);
        mMethodOverride(m_Tha60210011ModuleDemapOverride, HoLineVcxEncapConnectionIsEnabled);
        mMethodOverride(m_Tha60210011ModuleDemapOverride, VcIsHoCep);
        mMethodOverride(m_Tha60210011ModuleDemapOverride, IsLongRegDemapCtrl);
        }

    mMethodsSet(module, &m_Tha60210011ModuleDemapOverride);
    }

static void OverrideThaModuleDemap(AtModule self)
    {
    ThaModuleDemap demapModule = (ThaModuleDemap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleDemapMethods = mMethodsGet(demapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleDemapOverride, mMethodsGet(demapModule), sizeof(m_ThaModuleDemapOverride));

        mMethodOverride(m_ThaModuleDemapOverride, VcDmapChnCtrl);
        mMethodOverride(m_ThaModuleDemapOverride, DmapChnTypeMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapChnTypeShift);
        mMethodOverride(m_ThaModuleDemapOverride, DmapFirstTsMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapFirstTsShift);
        mMethodOverride(m_ThaModuleDemapOverride, DmapTsEnMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapTsEnShift);
        mMethodOverride(m_ThaModuleDemapOverride, DmapPwIdMask);
        mMethodOverride(m_ThaModuleDemapOverride, DmapPwIdShift);
        }

    mMethodsSet(demapModule, &m_ThaModuleDemapOverride);
    }

#include "Tha60290022MapDemapOverride.h"

static void OverrideThaModuleAbstractMap(AtModule self)
    {
    ThaModuleAbstractMap mapModule = (ThaModuleAbstractMap)self;

    if (!m_methodsInit)
        {
        AtOsal osal = AtSharedDriverOsalGet();
        m_ThaModuleAbstractMapMethods = mMethodsGet(mapModule);
        mMethodsGet(osal)->MemCpy(osal, &m_ThaModuleAbstractMapOverride, m_ThaModuleAbstractMapMethods, sizeof(m_ThaModuleAbstractMapOverride));

        mMethodOverride(m_ThaModuleAbstractMapOverride, HoSliceOffset);
        mMethodOverride(m_ThaModuleAbstractMapOverride, NumLoSlices);
        mMethodOverride(m_ThaModuleAbstractMapOverride, ChannelCtrlAddressFormula);
        mMethodOverride(m_ThaModuleAbstractMapOverride, TimeslotAddressFromBase);
        mMethodOverride(m_ThaModuleAbstractMapOverride, TimeslotInfoSet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, TimeslotInfoGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, AuVcBoundPwHwIdGet);
        mMethodOverride(m_ThaModuleAbstractMapOverride, SdhChannelRegsShow);
        }

    mMethodsSet(mapModule, &m_ThaModuleAbstractMapOverride);
    }

static void MethodsInit(AtModule self)
    {
    Tha60290022ModuleDemap module = (Tha60290022ModuleDemap)self;

    if (!m_methodsInit)
        {
        /* Clear memory */
        AtOsal osal = AtSharedDriverOsalGet();
        mMethodsGet(osal)->MemInit(osal, &m_methods, 0, sizeof(m_methods));

        mMethodOverride(m_methods, HoDemapPWIdFieldMask);
        mMethodOverride(m_methods, HoDemapPWIdFieldShift);
        }

    mMethodsSet(module, &m_methods);
    }

static void Override(AtModule self)
    {
    OverrideAtModule(self);
    OverrideThaModuleAbstractMap(self);
    OverrideThaModuleDemap(self);
    OverrideTha60210011ModuleDemap(self);
    }

static uint32 ObjectSize(void)
    {
    return sizeof(tTha60290022ModuleDemap);
    }

AtModule Tha60290022ModuleDemapObjectInit(AtModule self, AtDevice device)
    {
    /* Clear memory */
    AtOsal osal = AtSharedDriverOsalGet();
    mMethodsGet(osal)->MemInit(osal, self, 0, ObjectSize());

    /* Super constructor */
    if (Tha60210011ModuleDemapObjectInit(self, device) == NULL)
        return NULL;

    /* Setup class */
    Override(self);
    MethodsInit(self);
    m_methodsInit = 1;

    return self;
    }

AtModule Tha60290022ModuleDemapNew(AtDevice device)
    {
    /* Allocate memory */
    AtOsal osal = AtSharedDriverOsalGet();
    AtModule newModule = mMethodsGet(osal)->MemAlloc(osal, ObjectSize());
    if (newModule == NULL)
        return NULL;

    /* Construct it */
    return Tha60290022ModuleDemapObjectInit(newModule, device);
    }

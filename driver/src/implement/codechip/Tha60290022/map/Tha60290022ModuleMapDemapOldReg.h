/*-----------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 * 
 * The information contained herein is confidential property of Arrive Technologies.
 * The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Arrive Technologies.
 * 
 * Module      : MAP
 * 
 * File        : Tha60290022ModuleMapDemapOldReg.h
 * 
 * Created Date: Sep 30, 2017
 *
 * Description : Old registers
 * 
 * Notes       : 
 *----------------------------------------------------------------------------*/

#ifndef _THA60290022MODULEMAPDEMAPOLDREG_H_
#define _THA60290022MODULEMAPDEMAPOLDREG_H_

/*--------------------------- Includes ---------------------------------------*/

/*--------------------------- Define -----------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Typedefs ---------------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Entries ----------------------------------------*/
/*------------------------------------------------------------------------------
Reg Name   : Demap Channel Control
Reg Addr   : 0x000000-0x007FFF
Reg Formula: 0x000000 + 672*stsid + 96*vtgid + 24*vtid + slotid
    Where  :
           + $stsid(0-47): is STS identification number)
           + $vtgid(0-6): is VT Group identification number
           + $vtid(0-3): is VT identification number
           + $slotid(0-23): is time slot number. It varies from 0 to 23 in DS1 and from 0 to 31 in E1
Reg Desc   :
The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number.

------------------------------------------------------------------------------*/
#define cAf6Reg_demap_channel_ctrl_Base                                                               0x000000
#define cAf6Reg_demap_channel_ctrl_WidthVal                                                                 32

/*--------------------------------------
BitField Name: DemapFirstTs
BitField Type: RW
BitField Desc: First timeslot indication. Use for CESoP mode only
BitField Bits: [15]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapFirstTs_Mask                                                       cBit15
#define cAf6_demap_channel_ctrl_DemapFirstTs_Shift                                                          15

/*--------------------------------------
BitField Name: DemapChannelType
BitField Type: RW
BitField Desc: Specify which type of mapping for this. Specify which type of
mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP
encapsulation from DS1/E1 2: ATM over DS1/ E1, SONET/SDH (unused) 3: IMA over
DS1/E1, SONET/SDH(unused) 4: HDLC/PPP/LAPS over DS1/E1, SONET/SDH(unused) 5:
MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP encapsulation from SONET/SDH 7:
VCAT/LCAS/PLCP
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapChannelType_Mask                                                cBit14_12
#define cAf6_demap_channel_ctrl_DemapChannelType_Shift                                                      12

/*--------------------------------------
BitField Name: DemapChEn
BitField Type: RW
BitField Desc: PW/Channels Enable
BitField Bits: [11]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapChEn_Mask                                                          cBit11
#define cAf6_demap_channel_ctrl_DemapChEn_Shift                                                             11

/*--------------------------------------
BitField Name: DemapPWIdField
BitField Type: RW
BitField Desc: PW ID field that uses for Encapsulation
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_demap_channel_ctrl_DemapPWIdField_Mask                                                   cBit10_0
#define cAf6_demap_channel_ctrl_DemapPWIdField_Shift                                                         0

/*------------------------------------------------------------------------------
Reg Name   : Map Channel Control
Reg Addr   : 0x018000-0x01FFFF
Reg Formula: 0x018000 + 672*stsid + 96*vtgid + 24*vtid + slotid
    Where  :
           + $stsid(0-47): is STS identification number)
           + $vtgid(0-6): is VT Group identification number
           + $vtid(0-3): is VT identification number
           + $slotid(0-23): is time slot number. It varies from 0 to 23 in DS1 and from 0 to 31 in E1
Reg Desc   :
The registers are used by the hardware to convert channel identification number to STS/VT/PDH identification number. All STS/VT/PDHs of a concatenation are assigned the same channel identification number.

------------------------------------------------------------------------------*/
#define cAf6Reg_map_channel_ctrl_Base                                                                 0x018000
#define cAf6Reg_map_channel_ctrl_WidthVal                                                                   32

/*--------------------------------------
BitField Name: MapFirstTs
BitField Type: RW
BitField Desc: First timeslot indication. Use for CESoP mode only
BitField Bits: [15]
--------------------------------------*/
#define cAf6_map_channel_ctrl_MapFirstTs_Mask                                                           cBit15
#define cAf6_map_channel_ctrl_MapFirstTs_Shift                                                              15

/*--------------------------------------
BitField Name: MapChannelType
BitField Type: RW
BitField Desc: Specify which type of mapping for this. Specify which type of
mapping for this. 0: SAToP encapsulation from DS1/E1, DS3/E3 1: CESoP
encapsulation from DS1/E1 2: CESoP with CAS 3: ATM(unused) 4: HDLC/PPP/LAPS over
DS1/E1, SONET/SDH(unused) 5: MLPPP over DS1/E1, SONET/SDH(unused) 6: CEP
encapsulation from SONET/SDH 7: VCAT/LCAS/PLCP
BitField Bits: [14:12]
--------------------------------------*/
#define cAf6_map_channel_ctrl_MapChannelType_Mask                                                    cBit14_12
#define cAf6_map_channel_ctrl_MapChannelType_Shift                                                          12

/*--------------------------------------
BitField Name: MapChEn
BitField Type: RW
BitField Desc: PW/Channels Enable
BitField Bits: [11]
--------------------------------------*/
#define cAf6_map_channel_ctrl_MapChEn_Mask                                                              cBit11
#define cAf6_map_channel_ctrl_MapChEn_Shift                                                                 11

/*--------------------------------------
BitField Name: MapPWIdField
BitField Type: RW
BitField Desc: PW ID field that uses for Encapsulation.
BitField Bits: [10:0]
--------------------------------------*/
#define cAf6_map_channel_ctrl_MapPWIdField_Mask                                                       cBit10_0
#define cAf6_map_channel_ctrl_MapPWIdField_Shift                                                             0

#ifdef __cplusplus
}
#endif
#endif /* _THA60290022MODULEMAPDEMAPOLDREG_H_ */


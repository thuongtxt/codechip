/*------------------------------------------------------------------------------
 *
 * COPYRIGHT (C) 2017 Arrive Technologies Inc.
 *
 * The information contained herein is confidential property of The Arrive Technologies
 * The use, copying, transfer or disclosure of such information is prohibited
 * except by express written agreement with Arrive Technologies.
 *
 * Module      : MAP/DEMAP
 *
 * File        : Tha60290022MapDemap.c
 *
 * Created Date: Apr 18, 2017
 *
 * Description : Common functions for MAP/DEMAP
 *
 * Notes       : 
 *----------------------------------------------------------------------------*/

/*--------------------------- Include files ----------------------------------*/
#include "../../../../generic/man/AtModuleInternal.h"
#include "../man/Tha60290022Device.h"
#include "Tha60290022MapDemap.h"
#include "Tha60290022ModuleMapDemapReg.h"

/*--------------------------- Define -----------------------------------------*/
#define cNumTimeslotPerRegs 4

/*--------------------------- Macros -----------------------------------------*/

/*--------------------------- Local typedefs ---------------------------------*/

/*--------------------------- Global variables -------------------------------*/

/*--------------------------- Local variables --------------------------------*/

/*--------------------------- Forward declarations ---------------------------*/

/*--------------------------- Implementation ---------------------------------*/
static uint32 TimeslotGroup(uint32 timeslotId)
    {
    return timeslotId / cNumTimeslotPerRegs;
    }

uint32 Tha60290022ModuleMapDemapHoSliceOffset(ThaModuleAbstractMap self, uint8 slice)
    {
    AtUnused(self);
    return (0x0800000 + (slice * 0x40000UL));
    }

uint8 Tha60290022ModuleMapDemapNumLoSlices(ThaModuleAbstractMap self)
    {
    return Tha60290022DeviceMapDemapNumLoSlices(AtModuleDeviceGet((AtModule)self));
    }

uint8 Tha60290022ModuleMapDemapMaxNumStsInLoSlice(ThaModuleAbstractMap self)
    {
    AtUnused(self);
    return 48;
    }

uint32 Tha60290022ModuleMapDemapHwLogicOptimizedChannelCtrlAddressFormula(ThaModuleAbstractMap self, eThaMapChannelType channelType, uint32 stsid, uint32 vtgid, uint32 vtid, uint32 slotid)
    {
    uint32 timeSlotGroup = TimeslotGroup(slotid);

    switch ((uint32)channelType)
        {
        case cThaMapChannelDs1 : return (168 * stsid) + (24 * vtgid) + (6 * vtid) + timeSlotGroup;
        case cThaMapChannelVt15: return (168 * stsid) + (24 * vtgid) + (6 * vtid) + timeSlotGroup;
        case cThaMapChannelE1  : return (168 * stsid) + (24 * vtgid) + (8 * vtid) + timeSlotGroup;
        case cThaMapChannelVt2 : return (168 * stsid) + (24 * vtgid) + (8 * vtid) + timeSlotGroup;
        case cThaMapChannelVt6 : return (168 * stsid) + (24 * vtgid);
        case cThaMapChannelDs3 : return (168 * stsid);
        case cThaMapChannelE3  : return (168 * stsid);
        case cThaMapChannelDe3 : return (168 * stsid);
        case cThaMapChannelSts : return (168 * stsid);
        default:
            AtModuleLog((AtModule)self, cAtLogLevelCritical, AtSourceLocation, "Cannot determine the address formula because of invalid channel type %d\r\n", channelType);
            return cInvalidUint32;
        }
    }

uint32 Tha60290022ModuleMapDemapHwLogicOptimizedTimeslotAddressFromBase(ThaModuleAbstractMap self, uint32 address, uint32 slotid)
    {
    AtUnused(self);
    return address + TimeslotGroup(slotid);
    }

eAtRet Tha60290022ModuleMapDemapHwLogicOptimizedTimeslotInfoSet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, const tThaMapTimeslotInfo *slotInfo)
    {
    uint32 localTsId = timeslot % cNumTimeslotPerRegs;
    uint32 dwordIndex = localTsId / 2;
    uint32 longReg[cThaLongRegMaxSize];

    mChannelHwLongRead(channel, address, longReg, cThaLongRegMaxSize, AtModuleTypeGet((AtModule)self));

    switch (localTsId)
        {
        case 0:
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapChannelType0_, slotInfo->chnType);
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapFirstTs0_, slotInfo->firstTimeslot);
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPwEn0_, slotInfo->enabled);
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPWIdField0_, slotInfo->pwId);
            break;

        case 1:
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapChannelType1_, slotInfo->chnType);
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapFirstTs1_, slotInfo->firstTimeslot);
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPwEn1_, slotInfo->enabled);
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPWIdField1_, slotInfo->pwId);
            break;

        case 2:
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapChannelType2_, slotInfo->chnType);
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapFirstTs2_, slotInfo->firstTimeslot);
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPwEn2_, slotInfo->enabled);
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPWIdField2_, slotInfo->pwId);
            break;

        case 3:
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapChannelType3_, slotInfo->chnType);
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapFirstTs3_, slotInfo->firstTimeslot);
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPwEn3_, slotInfo->enabled);
            mRegFieldSet(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPWIdField3_, slotInfo->pwId);
            break;

        default:
            AtChannelLog(channel, cAtLogLevelCritical, AtSourceLocation, "Unknown local index %d\r\n", localTsId);
            break;
        }

    mChannelHwLongWrite(channel, address, longReg, cThaLongRegMaxSize, AtModuleTypeGet((AtModule)self));

    return cAtOk;
    }

eAtRet Tha60290022ModuleMapDemapHwLogicOptimizedTimeslotInfoGet(ThaModuleAbstractMap self, AtChannel channel, uint32 timeslot, uint32 address, tThaMapTimeslotInfo *slotInfo)
    {
    uint32 localTsId = timeslot % cNumTimeslotPerRegs;
    uint32 dwordIndex = localTsId / 2;
    uint32 longReg[cThaLongRegMaxSize];

    mChannelHwLongRead(channel, address, longReg, cThaLongRegMaxSize, AtModuleTypeGet((AtModule)self));

    switch (localTsId)
        {
        case 0:
            slotInfo->chnType = (uint8)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapChannelType0_);
            slotInfo->firstTimeslot = (uint8)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapFirstTs0_);
            slotInfo->enabled = (uint8)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPwEn0_);
            slotInfo->pwId = (uint16)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPWIdField0_);
            break;

        case 1:
            slotInfo->chnType = (uint8)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapChannelType1_);
            slotInfo->firstTimeslot = (uint8)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapFirstTs1_);
            slotInfo->enabled = (uint8)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPwEn1_);
            slotInfo->pwId = (uint16)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPWIdField1_);
            break;

        case 2:
            slotInfo->chnType = (uint8)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapChannelType2_);
            slotInfo->firstTimeslot = (uint8)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapFirstTs2_);
            slotInfo->enabled = (uint8)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPwEn2_);
            slotInfo->pwId = (uint16)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPWIdField2_);
            break;

        case 3:
            slotInfo->chnType = (uint8)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapChannelType3_);
            slotInfo->firstTimeslot = (uint8)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapFirstTs3_);
            slotInfo->enabled = (uint8)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPwEn3_);
            slotInfo->pwId = (uint16)mRegField(longReg[dwordIndex], cAf6_demap_channel_ctrl_DemapPWIdField3_);
            break;

        default:
            AtChannelLog(channel, cAtLogLevelCritical, AtSourceLocation, "Unknown local index %d\r\n", localTsId);
            break;
        }

    return cAtOk;
    }
